package org.thoughtcrime.securesms.components.settings.app.subscription.errors;

import android.content.Context;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.donations.StripeDeclineCode;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;

/* compiled from: DonationErrorParams.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 \u0011*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0003\u0010\u0011\u0012B;\b\u0002\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0007\u0012\u000e\u0010\b\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0007¢\u0006\u0002\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\b\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0019\u0010\u0006\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000b¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams;", "V", "", MultiselectForwardFragment.DIALOG_TITLE, "", "message", "positiveAction", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$ErrorAction;", "negativeAction", "(IILorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$ErrorAction;Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$ErrorAction;)V", "getMessage", "()I", "getNegativeAction", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$ErrorAction;", "getPositiveAction", "getTitle", "Callback", "Companion", "ErrorAction", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationErrorParams<V> {
    public static final Companion Companion = new Companion(null);
    private final int message;
    private final ErrorAction<V> negativeAction;
    private final ErrorAction<V> positiveAction;
    private final int title;

    /* compiled from: DonationErrorParams.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\bf\u0018\u0000*\u0004\b\u0001\u0010\u00012\u00020\u0002J\u0018\u0010\u0003\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0018\u0010\u0007\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0018\u0010\b\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0018\u0010\t\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0018\u0010\n\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$Callback;", "V", "", "onCancel", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$ErrorAction;", "context", "Landroid/content/Context;", "onContactSupport", "onGoToGooglePay", "onLearnMore", "onOk", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback<V> {
        ErrorAction<V> onCancel(Context context);

        ErrorAction<V> onContactSupport(Context context);

        ErrorAction<V> onGoToGooglePay(Context context);

        ErrorAction<V> onLearnMore(Context context);

        ErrorAction<V> onOk(Context context);
    }

    public /* synthetic */ DonationErrorParams(int i, int i2, ErrorAction errorAction, ErrorAction errorAction2, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, i2, errorAction, errorAction2);
    }

    private DonationErrorParams(int i, int i2, ErrorAction<V> errorAction, ErrorAction<V> errorAction2) {
        this.title = i;
        this.message = i2;
        this.positiveAction = errorAction;
        this.negativeAction = errorAction2;
    }

    public final int getTitle() {
        return this.title;
    }

    public final int getMessage() {
        return this.message;
    }

    public final ErrorAction<V> getPositiveAction() {
        return this.positiveAction;
    }

    public final ErrorAction<V> getNegativeAction() {
        return this.negativeAction;
    }

    /* compiled from: DonationErrorParams.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000*\u0004\b\u0001\u0010\u00012\u00020\u0002B\u001d\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00010\u0006¢\u0006\u0002\u0010\u0007R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$ErrorAction;", "V", "", EmojiSearchDatabase.LABEL, "", "action", "Lkotlin/Function0;", "(ILkotlin/jvm/functions/Function0;)V", "getAction", "()Lkotlin/jvm/functions/Function0;", "getLabel", "()I", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ErrorAction<V> {
        private final Function0<V> action;
        private final int label;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function0<? extends V> */
        /* JADX WARN: Multi-variable type inference failed */
        public ErrorAction(int i, Function0<? extends V> function0) {
            Intrinsics.checkNotNullParameter(function0, "action");
            this.label = i;
            this.action = function0;
        }

        public final int getLabel() {
            return this.label;
        }

        public final Function0<V> getAction() {
            return this.action;
        }
    }

    /* compiled from: DonationErrorParams.kt */
    @Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J2\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0001\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00050\u000bJ2\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0001\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000e2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00050\u000bH\u0002J2\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0001\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u00112\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00050\u000bH\u0002J2\u0010\u0012\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0001\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00050\u000b2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J2\u0010\u0015\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0001\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00050\u000b2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J2\u0010\u0016\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0001\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u00182\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00050\u000bH\u0002¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$Companion;", "", "()V", "create", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams;", "V", "context", "Landroid/content/Context;", "throwable", "", "callback", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$Callback;", "getDeclinedErrorParams", "declinedError", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError$DeclinedError;", "getGenericRedemptionError", "genericError", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError$GenericError;", "getGoToGooglePayParams", "message", "", "getLearnMoreParams", "getVerificationErrorParams", "verificationError", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {

        /* compiled from: DonationErrorParams.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;
            public static final /* synthetic */ int[] $EnumSwitchMapping$1;

            static {
                int[] iArr = new int[DonationErrorSource.values().length];
                iArr[DonationErrorSource.GIFT.ordinal()] = 1;
                $EnumSwitchMapping$0 = iArr;
                int[] iArr2 = new int[StripeDeclineCode.Code.values().length];
                iArr2[StripeDeclineCode.Code.APPROVE_WITH_ID.ordinal()] = 1;
                iArr2[StripeDeclineCode.Code.CALL_ISSUER.ordinal()] = 2;
                iArr2[StripeDeclineCode.Code.CARD_NOT_SUPPORTED.ordinal()] = 3;
                iArr2[StripeDeclineCode.Code.EXPIRED_CARD.ordinal()] = 4;
                iArr2[StripeDeclineCode.Code.INCORRECT_NUMBER.ordinal()] = 5;
                iArr2[StripeDeclineCode.Code.INCORRECT_CVC.ordinal()] = 6;
                iArr2[StripeDeclineCode.Code.INSUFFICIENT_FUNDS.ordinal()] = 7;
                iArr2[StripeDeclineCode.Code.INVALID_CVC.ordinal()] = 8;
                iArr2[StripeDeclineCode.Code.INVALID_EXPIRY_MONTH.ordinal()] = 9;
                iArr2[StripeDeclineCode.Code.INVALID_EXPIRY_YEAR.ordinal()] = 10;
                iArr2[StripeDeclineCode.Code.INVALID_NUMBER.ordinal()] = 11;
                iArr2[StripeDeclineCode.Code.ISSUER_NOT_AVAILABLE.ordinal()] = 12;
                iArr2[StripeDeclineCode.Code.PROCESSING_ERROR.ordinal()] = 13;
                iArr2[StripeDeclineCode.Code.REENTER_TRANSACTION.ordinal()] = 14;
                $EnumSwitchMapping$1 = iArr2;
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final <V> DonationErrorParams<V> create(Context context, Throwable th, Callback<V> callback) {
            DonationErrorParams<V> donationErrorParams;
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(callback, "callback");
            if (th instanceof DonationError.GiftRecipientVerificationError) {
                return getVerificationErrorParams(context, (DonationError.GiftRecipientVerificationError) th, callback);
            }
            if (th instanceof DonationError.PaymentSetupError.DeclinedError) {
                return getDeclinedErrorParams(context, (DonationError.PaymentSetupError.DeclinedError) th, callback);
            }
            if (th instanceof DonationError.PaymentSetupError) {
                donationErrorParams = new DonationErrorParams<>(R.string.DonationsErrors__error_processing_payment, R.string.DonationsErrors__your_payment, callback.onOk(context), null, null);
            } else if (th instanceof DonationError.BadgeRedemptionError.TimeoutWaitingForTokenError) {
                donationErrorParams = new DonationErrorParams<>(R.string.DonationsErrors__still_processing, R.string.DonationsErrors__your_payment_is_still, callback.onOk(context), null, null);
            } else if (th instanceof DonationError.BadgeRedemptionError.FailedToValidateCredentialError) {
                donationErrorParams = new DonationErrorParams<>(R.string.DonationsErrors__failed_to_validate_badge, R.string.DonationsErrors__could_not_validate, callback.onContactSupport(context), null, null);
            } else if (th instanceof DonationError.BadgeRedemptionError.GenericError) {
                return getGenericRedemptionError(context, (DonationError.BadgeRedemptionError.GenericError) th, callback);
            } else {
                donationErrorParams = new DonationErrorParams<>(R.string.DonationsErrors__couldnt_add_badge, R.string.DonationsErrors__your_badge_could_not, callback.onContactSupport(context), null, null);
            }
            return donationErrorParams;
        }

        private final <V> DonationErrorParams<V> getGenericRedemptionError(Context context, DonationError.BadgeRedemptionError.GenericError genericError, Callback<V> callback) {
            if (WhenMappings.$EnumSwitchMapping$0[genericError.getSource().ordinal()] == 1) {
                return new DonationErrorParams<>(R.string.DonationsErrors__failed_to_send_gift_badge, R.string.DonationsErrors__could_not_send_gift_badge, callback.onContactSupport(context), null, null);
            }
            return new DonationErrorParams<>(R.string.DonationsErrors__couldnt_add_badge, R.string.DonationsErrors__your_badge_could_not, callback.onContactSupport(context), null, null);
        }

        private final <V> DonationErrorParams<V> getVerificationErrorParams(Context context, DonationError.GiftRecipientVerificationError giftRecipientVerificationError, Callback<V> callback) {
            if (giftRecipientVerificationError instanceof DonationError.GiftRecipientVerificationError.FailedToFetchProfile) {
                return new DonationErrorParams<>(R.string.DonationsErrors__could_not_verify_recipient, R.string.DonationsErrors__please_check_your_network_connection, callback.onOk(context), null, null);
            }
            return new DonationErrorParams<>(R.string.DonationsErrors__recipient_verification_failed, R.string.DonationsErrors__target_does_not_support_gifting, callback.onOk(context), null, null);
        }

        private final <V> DonationErrorParams<V> getDeclinedErrorParams(Context context, DonationError.PaymentSetupError.DeclinedError declinedError, Callback<V> callback) {
            if (!(declinedError.getDeclineCode() instanceof StripeDeclineCode.Known)) {
                return getLearnMoreParams(context, callback, R.string.DeclineCode__try_another_payment_method_or_contact_your_bank);
            }
            switch (WhenMappings.$EnumSwitchMapping$1[((StripeDeclineCode.Known) declinedError.getDeclineCode()).getCode().ordinal()]) {
                case 1:
                    return getGoToGooglePayParams(context, callback, R.string.DeclineCode__verify_your_payment_method_is_up_to_date_in_google_pay_and_try_again);
                case 2:
                    return getGoToGooglePayParams(context, callback, R.string.DeclineCode__verify_your_payment_method_is_up_to_date_in_google_pay_and_try_again_if_the_problem);
                case 3:
                    return getLearnMoreParams(context, callback, R.string.DeclineCode__your_card_does_not_support_this_type_of_purchase);
                case 4:
                    return getGoToGooglePayParams(context, callback, R.string.DeclineCode__your_card_has_expired);
                case 5:
                    return getGoToGooglePayParams(context, callback, R.string.DeclineCode__your_card_number_is_incorrect);
                case 6:
                    return getGoToGooglePayParams(context, callback, R.string.DeclineCode__your_cards_cvc_number_is_incorrect);
                case 7:
                    return getLearnMoreParams(context, callback, R.string.DeclineCode__your_card_does_not_have_sufficient_funds);
                case 8:
                    return getGoToGooglePayParams(context, callback, R.string.DeclineCode__your_cards_cvc_number_is_incorrect);
                case 9:
                    return getGoToGooglePayParams(context, callback, R.string.DeclineCode__the_expiration_month);
                case 10:
                    return getGoToGooglePayParams(context, callback, R.string.DeclineCode__the_expiration_year);
                case 11:
                    return getGoToGooglePayParams(context, callback, R.string.DeclineCode__your_card_number_is_incorrect);
                case 12:
                    return getLearnMoreParams(context, callback, R.string.DeclineCode__try_completing_the_payment_again);
                case 13:
                    return getLearnMoreParams(context, callback, R.string.DeclineCode__try_again);
                case 14:
                    return getLearnMoreParams(context, callback, R.string.DeclineCode__try_again);
                default:
                    return getLearnMoreParams(context, callback, R.string.DeclineCode__try_another_payment_method_or_contact_your_bank);
            }
        }

        private final <V> DonationErrorParams<V> getLearnMoreParams(Context context, Callback<V> callback, int i) {
            return new DonationErrorParams<>(R.string.DonationsErrors__error_processing_payment, i, callback.onOk(context), callback.onLearnMore(context), null);
        }

        private final <V> DonationErrorParams<V> getGoToGooglePayParams(Context context, Callback<V> callback, int i) {
            return new DonationErrorParams<>(R.string.DonationsErrors__error_processing_payment, i, callback.onGoToGooglePay(context), callback.onCancel(context), null);
        }
    }
}
