package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.util.livedata.ProcessState;

/* compiled from: ExpireTimerSettingsViewModel.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ExpireTimerSettingsViewModel$save$5 extends Lambda implements Function0<Unit> {
    final /* synthetic */ int $userSetTimer;
    final /* synthetic */ ExpireTimerSettingsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ExpireTimerSettingsViewModel$save$5(ExpireTimerSettingsViewModel expireTimerSettingsViewModel, int i) {
        super(0);
        this.this$0 = expireTimerSettingsViewModel;
        this.$userSetTimer = i;
    }

    /* renamed from: invoke$lambda-0 */
    public static final ExpireTimerSettingsState m881invoke$lambda0(int i, ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullExpressionValue(expireTimerSettingsState, "it");
        return ExpireTimerSettingsState.copy$default(expireTimerSettingsState, 0, null, new ProcessState.Success(Integer.valueOf(i)), false, false, 27, null);
    }

    @Override // kotlin.jvm.functions.Function0
    public final void invoke() {
        this.this$0.store.update(new Function(this.$userSetTimer) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel$save$5$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ExpireTimerSettingsViewModel$save$5.m881invoke$lambda0(this.f$0, (ExpireTimerSettingsState) obj);
            }
        });
    }
}
