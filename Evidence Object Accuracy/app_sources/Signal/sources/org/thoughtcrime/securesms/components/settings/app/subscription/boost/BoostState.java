package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import java.util.Currency;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.badges.models.Badge;

/* compiled from: BoostState.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u001c\n\u0002\u0010\b\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u00011Bm\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\n\u0012\b\b\u0002\u0010\f\u001a\u00020\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0007\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0010\u0012\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\t¢\u0006\u0002\u0010\u0013J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010#\u001a\u00020\u0005HÆ\u0003J\t\u0010$\u001a\u00020\u0007HÆ\u0003J\u000f\u0010%\u001a\b\u0012\u0004\u0012\u00020\n0\tHÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\nHÆ\u0003J\t\u0010'\u001a\u00020\rHÆ\u0003J\t\u0010(\u001a\u00020\u0007HÆ\u0003J\t\u0010)\u001a\u00020\u0010HÆ\u0003J\u000f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00120\tHÆ\u0003Js\u0010+\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\n2\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u00072\b\b\u0002\u0010\u000f\u001a\u00020\u00102\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\tHÆ\u0001J\u0013\u0010,\u001a\u00020\u00072\b\u0010-\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010.\u001a\u00020/HÖ\u0001J\t\u00100\u001a\u00020\u0012HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u000e\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u001cR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u001cR\u0013\u0010\u000b\u001a\u0004\u0018\u00010\n¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u000f\u001a\u00020\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0017\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\t¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0017¨\u00062"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostState;", "", "boostBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "currencySelection", "Ljava/util/Currency;", "isGooglePayAvailable", "", "boosts", "", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;", "selectedBoost", "customAmount", "Lorg/signal/core/util/money/FiatMoney;", "isCustomAmountFocused", "stage", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostState$Stage;", "supportedCurrencyCodes", "", "(Lorg/thoughtcrime/securesms/badges/models/Badge;Ljava/util/Currency;ZLjava/util/List;Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;Lorg/signal/core/util/money/FiatMoney;ZLorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostState$Stage;Ljava/util/List;)V", "getBoostBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getBoosts", "()Ljava/util/List;", "getCurrencySelection", "()Ljava/util/Currency;", "getCustomAmount", "()Lorg/signal/core/util/money/FiatMoney;", "()Z", "getSelectedBoost", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;", "getStage", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostState$Stage;", "getSupportedCurrencyCodes", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "Stage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BoostState {
    private final Badge boostBadge;
    private final List<Boost> boosts;
    private final Currency currencySelection;
    private final FiatMoney customAmount;
    private final boolean isCustomAmountFocused;
    private final boolean isGooglePayAvailable;
    private final Boost selectedBoost;
    private final Stage stage;
    private final List<String> supportedCurrencyCodes;

    /* compiled from: BoostState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostState$Stage;", "", "(Ljava/lang/String;I)V", "INIT", "READY", "TOKEN_REQUEST", "PAYMENT_PIPELINE", "FAILURE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Stage {
        INIT,
        READY,
        TOKEN_REQUEST,
        PAYMENT_PIPELINE,
        FAILURE
    }

    public final Badge component1() {
        return this.boostBadge;
    }

    public final Currency component2() {
        return this.currencySelection;
    }

    public final boolean component3() {
        return this.isGooglePayAvailable;
    }

    public final List<Boost> component4() {
        return this.boosts;
    }

    public final Boost component5() {
        return this.selectedBoost;
    }

    public final FiatMoney component6() {
        return this.customAmount;
    }

    public final boolean component7() {
        return this.isCustomAmountFocused;
    }

    public final Stage component8() {
        return this.stage;
    }

    public final List<String> component9() {
        return this.supportedCurrencyCodes;
    }

    public final BoostState copy(Badge badge, Currency currency, boolean z, List<Boost> list, Boost boost, FiatMoney fiatMoney, boolean z2, Stage stage, List<String> list2) {
        Intrinsics.checkNotNullParameter(currency, "currencySelection");
        Intrinsics.checkNotNullParameter(list, "boosts");
        Intrinsics.checkNotNullParameter(fiatMoney, "customAmount");
        Intrinsics.checkNotNullParameter(stage, "stage");
        Intrinsics.checkNotNullParameter(list2, "supportedCurrencyCodes");
        return new BoostState(badge, currency, z, list, boost, fiatMoney, z2, stage, list2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BoostState)) {
            return false;
        }
        BoostState boostState = (BoostState) obj;
        return Intrinsics.areEqual(this.boostBadge, boostState.boostBadge) && Intrinsics.areEqual(this.currencySelection, boostState.currencySelection) && this.isGooglePayAvailable == boostState.isGooglePayAvailable && Intrinsics.areEqual(this.boosts, boostState.boosts) && Intrinsics.areEqual(this.selectedBoost, boostState.selectedBoost) && Intrinsics.areEqual(this.customAmount, boostState.customAmount) && this.isCustomAmountFocused == boostState.isCustomAmountFocused && this.stage == boostState.stage && Intrinsics.areEqual(this.supportedCurrencyCodes, boostState.supportedCurrencyCodes);
    }

    public int hashCode() {
        Badge badge = this.boostBadge;
        int i = 0;
        int hashCode = (((badge == null ? 0 : badge.hashCode()) * 31) + this.currencySelection.hashCode()) * 31;
        boolean z = this.isGooglePayAvailable;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int hashCode2 = (((hashCode + i3) * 31) + this.boosts.hashCode()) * 31;
        Boost boost = this.selectedBoost;
        if (boost != null) {
            i = boost.hashCode();
        }
        int hashCode3 = (((hashCode2 + i) * 31) + this.customAmount.hashCode()) * 31;
        boolean z2 = this.isCustomAmountFocused;
        if (!z2) {
            i2 = z2 ? 1 : 0;
        }
        return ((((hashCode3 + i2) * 31) + this.stage.hashCode()) * 31) + this.supportedCurrencyCodes.hashCode();
    }

    public String toString() {
        return "BoostState(boostBadge=" + this.boostBadge + ", currencySelection=" + this.currencySelection + ", isGooglePayAvailable=" + this.isGooglePayAvailable + ", boosts=" + this.boosts + ", selectedBoost=" + this.selectedBoost + ", customAmount=" + this.customAmount + ", isCustomAmountFocused=" + this.isCustomAmountFocused + ", stage=" + this.stage + ", supportedCurrencyCodes=" + this.supportedCurrencyCodes + ')';
    }

    public BoostState(Badge badge, Currency currency, boolean z, List<Boost> list, Boost boost, FiatMoney fiatMoney, boolean z2, Stage stage, List<String> list2) {
        Intrinsics.checkNotNullParameter(currency, "currencySelection");
        Intrinsics.checkNotNullParameter(list, "boosts");
        Intrinsics.checkNotNullParameter(fiatMoney, "customAmount");
        Intrinsics.checkNotNullParameter(stage, "stage");
        Intrinsics.checkNotNullParameter(list2, "supportedCurrencyCodes");
        this.boostBadge = badge;
        this.currencySelection = currency;
        this.isGooglePayAvailable = z;
        this.boosts = list;
        this.selectedBoost = boost;
        this.customAmount = fiatMoney;
        this.isCustomAmountFocused = z2;
        this.stage = stage;
        this.supportedCurrencyCodes = list2;
    }

    public final Badge getBoostBadge() {
        return this.boostBadge;
    }

    public final Currency getCurrencySelection() {
        return this.currencySelection;
    }

    public final boolean isGooglePayAvailable() {
        return this.isGooglePayAvailable;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ BoostState(org.thoughtcrime.securesms.badges.models.Badge r14, java.util.Currency r15, boolean r16, java.util.List r17, org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost r18, org.signal.core.util.money.FiatMoney r19, boolean r20, org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState.Stage r21, java.util.List r22, int r23, kotlin.jvm.internal.DefaultConstructorMarker r24) {
        /*
            r13 = this;
            r0 = r23
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r4 = r2
            goto L_0x000a
        L_0x0009:
            r4 = r14
        L_0x000a:
            r1 = r0 & 4
            r3 = 0
            if (r1 == 0) goto L_0x0011
            r6 = 0
            goto L_0x0013
        L_0x0011:
            r6 = r16
        L_0x0013:
            r1 = r0 & 8
            if (r1 == 0) goto L_0x001d
            java.util.List r1 = kotlin.collections.CollectionsKt.emptyList()
            r7 = r1
            goto L_0x001f
        L_0x001d:
            r7 = r17
        L_0x001f:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0025
            r8 = r2
            goto L_0x0027
        L_0x0025:
            r8 = r18
        L_0x0027:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0035
            org.signal.core.util.money.FiatMoney r1 = new org.signal.core.util.money.FiatMoney
            java.math.BigDecimal r2 = java.math.BigDecimal.ZERO
            r5 = r15
            r1.<init>(r2, r15)
            r9 = r1
            goto L_0x0038
        L_0x0035:
            r5 = r15
            r9 = r19
        L_0x0038:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x003e
            r10 = 0
            goto L_0x0040
        L_0x003e:
            r10 = r20
        L_0x0040:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x0048
            org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState$Stage r1 = org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState.Stage.INIT
            r11 = r1
            goto L_0x004a
        L_0x0048:
            r11 = r21
        L_0x004a:
            r0 = r0 & 256(0x100, float:3.59E-43)
            if (r0 == 0) goto L_0x0054
            java.util.List r0 = kotlin.collections.CollectionsKt.emptyList()
            r12 = r0
            goto L_0x0056
        L_0x0054:
            r12 = r22
        L_0x0056:
            r3 = r13
            r5 = r15
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState.<init>(org.thoughtcrime.securesms.badges.models.Badge, java.util.Currency, boolean, java.util.List, org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost, org.signal.core.util.money.FiatMoney, boolean, org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState$Stage, java.util.List, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final List<Boost> getBoosts() {
        return this.boosts;
    }

    public final Boost getSelectedBoost() {
        return this.selectedBoost;
    }

    public final FiatMoney getCustomAmount() {
        return this.customAmount;
    }

    public final boolean isCustomAmountFocused() {
        return this.isCustomAmountFocused;
    }

    public final Stage getStage() {
        return this.stage;
    }

    public final List<String> getSupportedCurrencyCodes() {
        return this.supportedCurrencyCodes;
    }
}
