package org.thoughtcrime.securesms.components.registration;

import org.thoughtcrime.securesms.components.registration.PulsingFloatingActionButton;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PulsingFloatingActionButton$1$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ PulsingFloatingActionButton.AnonymousClass1.AnonymousClass1 f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ PulsingFloatingActionButton$1$1$$ExternalSyntheticLambda0(PulsingFloatingActionButton.AnonymousClass1.AnonymousClass1 r1, long j) {
        this.f$0 = r1;
        this.f$1 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onAnimationEnd$0(this.f$1);
    }
}
