package org.thoughtcrime.securesms.components.registration;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class VerificationCodeView extends FrameLayout {
    private final List<TextView> codes = new ArrayList(6);
    private final List<View> containers = new ArrayList(6);
    private int index;
    private OnCodeEnteredListener listener;

    /* loaded from: classes4.dex */
    public interface OnCodeEnteredListener {
        void onCodeComplete(String str);
    }

    public VerificationCodeView(Context context) {
        super(context);
        initialize(context);
    }

    public VerificationCodeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize(context);
    }

    public VerificationCodeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize(context);
    }

    public VerificationCodeView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        initialize(context);
    }

    private void initialize(Context context) {
        FrameLayout.inflate(context, R.layout.verification_code_view, this);
        this.codes.add((TextView) findViewById(R.id.code_zero));
        this.codes.add((TextView) findViewById(R.id.code_one));
        this.codes.add((TextView) findViewById(R.id.code_two));
        this.codes.add((TextView) findViewById(R.id.code_three));
        this.codes.add((TextView) findViewById(R.id.code_four));
        this.codes.add((TextView) findViewById(R.id.code_five));
        this.containers.add(findViewById(R.id.container_zero));
        this.containers.add(findViewById(R.id.container_one));
        this.containers.add(findViewById(R.id.container_two));
        this.containers.add(findViewById(R.id.container_three));
        this.containers.add(findViewById(R.id.container_four));
        this.containers.add(findViewById(R.id.container_five));
    }

    public void setOnCompleteListener(OnCodeEnteredListener onCodeEnteredListener) {
        this.listener = onCodeEnteredListener;
    }

    public void append(int i) {
        OnCodeEnteredListener onCodeEnteredListener;
        if (this.index < this.codes.size()) {
            setInactive(this.containers);
            setActive(this.containers.get(this.index));
            List<TextView> list = this.codes;
            int i2 = this.index;
            this.index = i2 + 1;
            TextView textView = list.get(i2);
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) textView.getHeight(), 0.0f);
            translateAnimation.setInterpolator(new OvershootInterpolator());
            translateAnimation.setDuration(500);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(200);
            AnimationSet animationSet = new AnimationSet(false);
            animationSet.addAnimation(alphaAnimation);
            animationSet.addAnimation(translateAnimation);
            animationSet.reset();
            animationSet.setStartTime(0);
            textView.setText(String.valueOf(i));
            textView.clearAnimation();
            textView.startAnimation(animationSet);
            if (this.index == this.codes.size() && (onCodeEnteredListener = this.listener) != null) {
                onCodeEnteredListener.onCodeComplete((String) Stream.of(this.codes).map(new Function() { // from class: org.thoughtcrime.securesms.components.registration.VerificationCodeView$$ExternalSyntheticLambda2
                    @Override // com.annimon.stream.function.Function
                    public final Object apply(Object obj) {
                        return ((TextView) obj).getText();
                    }
                }).collect(Collectors.joining()));
            }
        }
    }

    public void delete() {
        int i = this.index;
        if (i > 0) {
            List<TextView> list = this.codes;
            int i2 = i - 1;
            this.index = i2;
            list.get(i2).setText("");
            setInactive(this.containers);
            setActive(this.containers.get(this.index));
        }
    }

    public void clear() {
        if (this.index != 0) {
            Stream.of(this.codes).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.components.registration.VerificationCodeView$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    VerificationCodeView.m516$r8$lambda$TSBlyYWwTGzCICTwf4lMYgHrgM((TextView) obj);
                }
            });
            this.index = 0;
        }
        setInactive(this.containers);
    }

    private static void setInactive(List<View> list) {
        Stream.of(list).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.components.registration.VerificationCodeView$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                VerificationCodeView.$r8$lambda$PjzknNTz7xZ5vATXeYYtFQSU9Rs((View) obj);
            }
        });
    }

    private static void setActive(View view) {
        view.setBackgroundResource(R.drawable.labeled_edit_text_background_active);
    }
}
