package org.thoughtcrime.securesms.components.settings.conversation.permissions;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;

/* compiled from: PermissionsSettingsState.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0011\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B-\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J1\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00032\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsState;", "", "selfCanEditSettings", "", "nonAdminCanAddMembers", "nonAdminCanEditGroupInfo", "announcementGroup", "(ZZZZ)V", "getAnnouncementGroup", "()Z", "getNonAdminCanAddMembers", "getNonAdminCanEditGroupInfo", "getSelfCanEditSettings", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PermissionsSettingsState {
    private final boolean announcementGroup;
    private final boolean nonAdminCanAddMembers;
    private final boolean nonAdminCanEditGroupInfo;
    private final boolean selfCanEditSettings;

    public PermissionsSettingsState() {
        this(false, false, false, false, 15, null);
    }

    public static /* synthetic */ PermissionsSettingsState copy$default(PermissionsSettingsState permissionsSettingsState, boolean z, boolean z2, boolean z3, boolean z4, int i, Object obj) {
        if ((i & 1) != 0) {
            z = permissionsSettingsState.selfCanEditSettings;
        }
        if ((i & 2) != 0) {
            z2 = permissionsSettingsState.nonAdminCanAddMembers;
        }
        if ((i & 4) != 0) {
            z3 = permissionsSettingsState.nonAdminCanEditGroupInfo;
        }
        if ((i & 8) != 0) {
            z4 = permissionsSettingsState.announcementGroup;
        }
        return permissionsSettingsState.copy(z, z2, z3, z4);
    }

    public final boolean component1() {
        return this.selfCanEditSettings;
    }

    public final boolean component2() {
        return this.nonAdminCanAddMembers;
    }

    public final boolean component3() {
        return this.nonAdminCanEditGroupInfo;
    }

    public final boolean component4() {
        return this.announcementGroup;
    }

    public final PermissionsSettingsState copy(boolean z, boolean z2, boolean z3, boolean z4) {
        return new PermissionsSettingsState(z, z2, z3, z4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PermissionsSettingsState)) {
            return false;
        }
        PermissionsSettingsState permissionsSettingsState = (PermissionsSettingsState) obj;
        return this.selfCanEditSettings == permissionsSettingsState.selfCanEditSettings && this.nonAdminCanAddMembers == permissionsSettingsState.nonAdminCanAddMembers && this.nonAdminCanEditGroupInfo == permissionsSettingsState.nonAdminCanEditGroupInfo && this.announcementGroup == permissionsSettingsState.announcementGroup;
    }

    public int hashCode() {
        boolean z = this.selfCanEditSettings;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        boolean z2 = this.nonAdminCanAddMembers;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.nonAdminCanEditGroupInfo;
        if (z3) {
            z3 = true;
        }
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = (i9 + i10) * 31;
        boolean z4 = this.announcementGroup;
        if (!z4) {
            i = z4 ? 1 : 0;
        }
        return i13 + i;
    }

    public String toString() {
        return "PermissionsSettingsState(selfCanEditSettings=" + this.selfCanEditSettings + ", nonAdminCanAddMembers=" + this.nonAdminCanAddMembers + ", nonAdminCanEditGroupInfo=" + this.nonAdminCanEditGroupInfo + ", announcementGroup=" + this.announcementGroup + ')';
    }

    public PermissionsSettingsState(boolean z, boolean z2, boolean z3, boolean z4) {
        this.selfCanEditSettings = z;
        this.nonAdminCanAddMembers = z2;
        this.nonAdminCanEditGroupInfo = z3;
        this.announcementGroup = z4;
    }

    public /* synthetic */ PermissionsSettingsState(boolean z, boolean z2, boolean z3, boolean z4, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? false : z3, (i & 8) != 0 ? false : z4);
    }

    public final boolean getSelfCanEditSettings() {
        return this.selfCanEditSettings;
    }

    public final boolean getNonAdminCanAddMembers() {
        return this.nonAdminCanAddMembers;
    }

    public final boolean getNonAdminCanEditGroupInfo() {
        return this.nonAdminCanEditGroupInfo;
    }

    public final boolean getAnnouncementGroup() {
        return this.announcementGroup;
    }
}
