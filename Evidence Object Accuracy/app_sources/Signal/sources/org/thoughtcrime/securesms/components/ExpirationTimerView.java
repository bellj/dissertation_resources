package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ExpirationTimerView extends AppCompatImageView {
    private long expiresIn;
    private final int[] frames = {R.drawable.ic_timer_00_12, R.drawable.ic_timer_05_12, R.drawable.ic_timer_10_12, R.drawable.ic_timer_15_12, R.drawable.ic_timer_20_12, R.drawable.ic_timer_25_12, R.drawable.ic_timer_30_12, R.drawable.ic_timer_35_12, R.drawable.ic_timer_40_12, R.drawable.ic_timer_45_12, R.drawable.ic_timer_50_12, R.drawable.ic_timer_55_12, R.drawable.ic_timer_60_12};
    private long startedAt;
    private boolean stopped = true;
    private boolean visible = false;

    public ExpirationTimerView(Context context) {
        super(context);
    }

    public ExpirationTimerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ExpirationTimerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setExpirationTime(long j, long j2) {
        this.startedAt = j;
        this.expiresIn = j2;
        setPercentComplete(calculateProgress(j, j2));
    }

    public void setPercentComplete(float f) {
        setImageResource(this.frames[Math.max(0, Math.min((int) Math.ceil((double) ((1.0f - f) * ((float) (this.frames.length - 1)))), this.frames.length - 1))]);
    }

    public void startAnimation() {
        synchronized (this) {
            this.visible = true;
            if (this.stopped) {
                this.stopped = false;
                ThreadUtil.runOnMainDelayed(new AnimationUpdateRunnable(), calculateAnimationDelay(this.startedAt, this.expiresIn));
            }
        }
    }

    public void stopAnimation() {
        synchronized (this) {
            this.visible = false;
        }
    }

    private float calculateProgress(long j, long j2) {
        return Math.max(0.0f, Math.min(((float) (System.currentTimeMillis() - j)) / ((float) j2), 1.0f));
    }

    public long calculateAnimationDelay(long j, long j2) {
        return j2 - (System.currentTimeMillis() - j) < TimeUnit.SECONDS.toMillis(30) ? 50 : 1000;
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class AnimationUpdateRunnable implements Runnable {
        private final WeakReference<ExpirationTimerView> expirationTimerViewReference;

        private AnimationUpdateRunnable(ExpirationTimerView expirationTimerView) {
            this.expirationTimerViewReference = new WeakReference<>(expirationTimerView);
        }

        @Override // java.lang.Runnable
        public void run() {
            ExpirationTimerView expirationTimerView = this.expirationTimerViewReference.get();
            if (expirationTimerView != null) {
                expirationTimerView.setExpirationTime(expirationTimerView.startedAt, expirationTimerView.expiresIn);
                synchronized (expirationTimerView) {
                    if (!expirationTimerView.visible) {
                        expirationTimerView.stopped = true;
                    } else {
                        ThreadUtil.runOnMainDelayed(this, expirationTimerView.calculateAnimationDelay(expirationTimerView.startedAt, expirationTimerView.expiresIn));
                    }
                }
            }
        }
    }
}
