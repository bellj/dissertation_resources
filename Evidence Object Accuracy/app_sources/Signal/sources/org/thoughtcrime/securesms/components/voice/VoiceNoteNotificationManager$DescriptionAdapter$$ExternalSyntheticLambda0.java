package org.thoughtcrime.securesms.components.voice;

import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import org.thoughtcrime.securesms.components.voice.VoiceNoteNotificationManager;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class VoiceNoteNotificationManager$DescriptionAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ VoiceNoteNotificationManager.DescriptionAdapter f$0;
    public final /* synthetic */ PlayerNotificationManager.BitmapCallback f$1;

    public /* synthetic */ VoiceNoteNotificationManager$DescriptionAdapter$$ExternalSyntheticLambda0(VoiceNoteNotificationManager.DescriptionAdapter descriptionAdapter, PlayerNotificationManager.BitmapCallback bitmapCallback) {
        this.f$0 = descriptionAdapter;
        this.f$1 = bitmapCallback;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$getCurrentLargeIcon$0(this.f$1);
    }
}
