package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.fragment.FragmentKt;
import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.models.BadgePreview;
import org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostAnimation;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragmentDirections;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.CurrencySelection;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.GooglePayButton;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.NetworkFailure;
import org.thoughtcrime.securesms.components.settings.models.Progress;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.service.webrtc.SignalCallManager;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.whispersystems.signalservice.api.services.DonationsService;

/* compiled from: BoostFragment.kt */
@Metadata(d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\u0018\u0000 12\u00020\u0001:\u00011B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020#H\u0002J\u0010\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020'H\u0002J\b\u0010(\u001a\u00020\u001eH\u0016J\b\u0010)\u001a\u00020\u001eH\u0002J\u0010\u0010*\u001a\u00020\u001e2\u0006\u0010+\u001a\u00020,H\u0002J\u0012\u0010-\u001a\u00020\u001e2\b\u0010.\u001a\u0004\u0018\u00010/H\u0002J\u0010\u00100\u001a\u00020\u001e2\u0006\u0010\"\u001a\u00020#H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX.¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0012\u001a\u00020\u00138BX\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0018\u001a\u00020\u00198BX\u0002¢\u0006\f\n\u0004\b\u001c\u0010\u0017\u001a\u0004\b\u001a\u0010\u001b¨\u00062"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "boost1AnimationView", "Lcom/airbnb/lottie/LottieAnimationView;", "boost2AnimationView", "boost3AnimationView", "boost4AnimationView", "boost5AnimationView", "boost6AnimationView", "donationPaymentComponent", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent;", "errorDialog", "Landroid/content/DialogInterface;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "processingDonationPaymentDialog", "Landroidx/appcompat/app/AlertDialog;", "sayThanks", "", "getSayThanks", "()Ljava/lang/CharSequence;", "sayThanks$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostViewModel;", "viewModel$delegate", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getAnimationContainer", "view", "Landroid/view/View;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostState;", "onDestroyView", "onGooglePayButtonClicked", "onPaymentConfirmed", "boostBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "onPaymentError", "throwable", "", "startAnimationAboveSelectedBoost", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BoostFragment extends DSLSettingsBottomSheetFragment {
    public static final Companion Companion = new Companion(null);
    private static final int FETCH_BOOST_TOKEN_REQUEST_CODE;
    private static final String TAG = Log.tag(BoostFragment.class);
    private LottieAnimationView boost1AnimationView;
    private LottieAnimationView boost2AnimationView;
    private LottieAnimationView boost3AnimationView;
    private LottieAnimationView boost4AnimationView;
    private LottieAnimationView boost5AnimationView;
    private LottieAnimationView boost6AnimationView;
    private DonationPaymentComponent donationPaymentComponent;
    private DialogInterface errorDialog;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private AlertDialog processingDonationPaymentDialog;
    private final Lazy sayThanks$delegate = LazyKt__LazyJVMKt.lazy(new BoostFragment$sayThanks$2(this));
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(BoostViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$viewModel$2
        final /* synthetic */ BoostFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            DonationsService donationsService = ApplicationDependencies.getDonationsService();
            Intrinsics.checkNotNullExpressionValue(donationsService, "getDonationsService()");
            BoostRepository boostRepository = new BoostRepository(donationsService);
            DonationPaymentComponent access$getDonationPaymentComponent$p = BoostFragment.access$getDonationPaymentComponent$p(this.this$0);
            if (access$getDonationPaymentComponent$p == null) {
                Intrinsics.throwUninitializedPropertyAccessException("donationPaymentComponent");
                access$getDonationPaymentComponent$p = null;
            }
            return new BoostViewModel.Factory(boostRepository, access$getDonationPaymentComponent$p.getDonationPaymentRepository(), SignalCallManager.BUSY_TONE_LENGTH);
        }
    });

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        DonationPaymentComponent donationPaymentComponent;
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        donationPaymentComponent = (DonationPaymentComponent) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent");
                    }
                } else if (fragment instanceof DonationPaymentComponent) {
                    donationPaymentComponent = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.donationPaymentComponent = donationPaymentComponent;
            getViewModel().refresh();
            CurrencySelection.INSTANCE.register(dSLSettingsAdapter);
            BadgePreview.INSTANCE.register(dSLSettingsAdapter);
            Boost.Companion.register(dSLSettingsAdapter);
            GooglePayButton.INSTANCE.register(dSLSettingsAdapter);
            Progress.INSTANCE.register(dSLSettingsAdapter);
            NetworkFailure.INSTANCE.register(dSLSettingsAdapter);
            BoostAnimation.INSTANCE.register(dSLSettingsAdapter);
            AlertDialog create = new MaterialAlertDialogBuilder(requireContext()).setView(R.layout.processing_payment_dialog).setCancelable(false).create();
            Intrinsics.checkNotNullExpressionValue(create, "MaterialAlertDialogBuild…le(false)\n      .create()");
            this.processingDonationPaymentDialog = create;
            getRecyclerView().setOverScrollMode(1);
            View findViewById = requireView().findViewById(R.id.boost1_animation);
            Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewById(R.id.boost1_animation)");
            this.boost1AnimationView = (LottieAnimationView) findViewById;
            View findViewById2 = requireView().findViewById(R.id.boost2_animation);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "requireView().findViewById(R.id.boost2_animation)");
            this.boost2AnimationView = (LottieAnimationView) findViewById2;
            View findViewById3 = requireView().findViewById(R.id.boost3_animation);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "requireView().findViewById(R.id.boost3_animation)");
            this.boost3AnimationView = (LottieAnimationView) findViewById3;
            View findViewById4 = requireView().findViewById(R.id.boost4_animation);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "requireView().findViewById(R.id.boost4_animation)");
            this.boost4AnimationView = (LottieAnimationView) findViewById4;
            View findViewById5 = requireView().findViewById(R.id.boost5_animation);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "requireView().findViewById(R.id.boost5_animation)");
            this.boost5AnimationView = (LottieAnimationView) findViewById5;
            View findViewById6 = requireView().findViewById(R.id.boost6_animation);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "requireView().findViewById(R.id.boost6_animation)");
            this.boost6AnimationView = (LottieAnimationView) findViewById6;
            KeyboardAwareLinearLayout keyboardAwareLinearLayout = new KeyboardAwareLinearLayout(requireContext());
            keyboardAwareLinearLayout.addOnKeyboardHiddenListener(new KeyboardAwareLinearLayout.OnKeyboardHiddenListener() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$$ExternalSyntheticLambda1
                @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardHiddenListener
                public final void onKeyboardHidden() {
                    BoostFragment.$r8$lambda$_82HHu0O5dq4jtT806ZojInMy74(BoostFragment.this);
                }
            });
            keyboardAwareLinearLayout.addOnKeyboardShownListener(new KeyboardAwareLinearLayout.OnKeyboardShownListener(dSLSettingsAdapter) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$$ExternalSyntheticLambda2
                public final /* synthetic */ DSLSettingsAdapter f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardShownListener
                public final void onKeyboardShown() {
                    BoostFragment.m926$r8$lambda$kVliwt7NI8CcflruiPZEm2_2DY(BoostFragment.this, this.f$1);
                }
            });
            BottomSheetUtil.INSTANCE.requireCoordinatorLayout(this).addView(keyboardAwareLinearLayout);
            getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$$ExternalSyntheticLambda3
                public final /* synthetic */ BoostFragment f$1;

                {
                    this.f$1 = r2;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    BoostFragment.m925$r8$lambda$XLsPTUvDrpBQZUkI0e2mQcun_g(DSLSettingsAdapter.this, this.f$1, (BoostState) obj);
                }
            });
            LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
            Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
            Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
            lifecycleDisposable.bindTo(lifecycle);
            LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
            Disposable subscribe = getViewModel().getEvents().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$$ExternalSyntheticLambda4
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    BoostFragment.$r8$lambda$IMN8mzzV_nK7YdWspvlditzcGts(BoostFragment.this, (DonationEvent) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.events.subscri…led -> Unit\n      }\n    }");
            lifecycleDisposable2.plusAssign(subscribe);
            LifecycleDisposable lifecycleDisposable3 = this.lifecycleDisposable;
            DonationPaymentComponent donationPaymentComponent2 = this.donationPaymentComponent;
            if (donationPaymentComponent2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("donationPaymentComponent");
                donationPaymentComponent2 = null;
            }
            Disposable subscribe2 = donationPaymentComponent2.getGooglePayResultPublisher().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$$ExternalSyntheticLambda5
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    BoostFragment.$r8$lambda$rhM4ChcHEO4ASowEIpRmT5OaDFI(BoostFragment.this, (DonationPaymentComponent.GooglePayResult) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe2, "donationPaymentComponent…esultCode, it.data)\n    }");
            lifecycleDisposable3.plusAssign(subscribe2);
            LifecycleDisposable lifecycleDisposable4 = this.lifecycleDisposable;
            Disposable subscribe3 = DonationError.Companion.getErrorsForSource(DonationErrorSource.BOOST).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$$ExternalSyntheticLambda6
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    BoostFragment.m924$r8$lambda$WPwNyo7tSK2sowC5pRwaCyP4Mg(BoostFragment.this, (DonationError) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe3, "DonationError\n      .get…or(donationError)\n      }");
            lifecycleDisposable4.plusAssign(subscribe3);
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    public BoostFragment() {
        super(R.layout.boost_bottom_sheet, null, 0.0f, 6, null);
    }

    public final BoostViewModel getViewModel() {
        return (BoostViewModel) this.viewModel$delegate.getValue();
    }

    public final CharSequence getSayThanks() {
        Object value = this.sayThanks$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-sayThanks>(...)");
        return (CharSequence) value;
    }

    /* renamed from: bindAdapter$lambda-4$lambda-1 */
    public static final void m927bindAdapter$lambda4$lambda1(BoostFragment boostFragment) {
        Intrinsics.checkNotNullParameter(boostFragment, "this$0");
        boostFragment.getRecyclerView().post(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                BoostFragment.$r8$lambda$dwS7thKxPfRjY1mOmypfNIm13Ug(BoostFragment.this);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-4$lambda-1$lambda-0 */
    public static final void m928bindAdapter$lambda4$lambda1$lambda0(BoostFragment boostFragment) {
        Intrinsics.checkNotNullParameter(boostFragment, "this$0");
        boostFragment.getRecyclerView().requestLayout();
    }

    /* renamed from: bindAdapter$lambda-4$lambda-3 */
    public static final void m929bindAdapter$lambda4$lambda3(BoostFragment boostFragment, DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(boostFragment, "this$0");
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        boostFragment.getRecyclerView().post(new Runnable(dSLSettingsAdapter) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$$ExternalSyntheticLambda7
            public final /* synthetic */ DSLSettingsAdapter f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BoostFragment.m923$r8$lambda$SO6FdcOyjSaEdXKGF0vf9KxqIo(BoostFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-4$lambda-3$lambda-2 */
    public static final void m930bindAdapter$lambda4$lambda3$lambda2(BoostFragment boostFragment, DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(boostFragment, "this$0");
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        boostFragment.getRecyclerView().scrollToPosition(dSLSettingsAdapter.getItemCount() - 1);
    }

    /* renamed from: bindAdapter$lambda-5 */
    public static final void m931bindAdapter$lambda5(DSLSettingsAdapter dSLSettingsAdapter, BoostFragment boostFragment, BoostState boostState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(boostFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(boostState, "state");
        dSLSettingsAdapter.submitList(boostFragment.getConfiguration(boostState).toMappingModelList());
    }

    /* renamed from: bindAdapter$lambda-6 */
    public static final void m932bindAdapter$lambda6(BoostFragment boostFragment, DonationEvent donationEvent) {
        Intrinsics.checkNotNullParameter(boostFragment, "this$0");
        Intrinsics.checkNotNullParameter(donationEvent, "event");
        if (donationEvent instanceof DonationEvent.PaymentConfirmationSuccess) {
            boostFragment.onPaymentConfirmed(((DonationEvent.PaymentConfirmationSuccess) donationEvent).getBadge());
        } else if (Intrinsics.areEqual(donationEvent, DonationEvent.RequestTokenSuccess.INSTANCE)) {
            Log.i(TAG, "Successfully got request token from Google Pay");
        } else if (!Intrinsics.areEqual(donationEvent, DonationEvent.SubscriptionCancelled.INSTANCE)) {
            boolean z = donationEvent instanceof DonationEvent.SubscriptionCancellationFailed;
        }
    }

    /* renamed from: bindAdapter$lambda-7 */
    public static final void m933bindAdapter$lambda7(BoostFragment boostFragment, DonationPaymentComponent.GooglePayResult googlePayResult) {
        Intrinsics.checkNotNullParameter(boostFragment, "this$0");
        boostFragment.getViewModel().onActivityResult(googlePayResult.getRequestCode(), googlePayResult.getResultCode(), googlePayResult.getData());
    }

    /* renamed from: bindAdapter$lambda-8 */
    public static final void m934bindAdapter$lambda8(BoostFragment boostFragment, DonationError donationError) {
        Intrinsics.checkNotNullParameter(boostFragment, "this$0");
        boostFragment.onPaymentError(donationError);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        AlertDialog alertDialog = this.processingDonationPaymentDialog;
        if (alertDialog == null) {
            Intrinsics.throwUninitializedPropertyAccessException("processingDonationPaymentDialog");
            alertDialog = null;
        }
        alertDialog.hide();
    }

    private final DSLConfiguration getConfiguration(BoostState boostState) {
        AlertDialog alertDialog = null;
        if (boostState.getStage() == BoostState.Stage.PAYMENT_PIPELINE) {
            AlertDialog alertDialog2 = this.processingDonationPaymentDialog;
            if (alertDialog2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("processingDonationPaymentDialog");
            } else {
                alertDialog = alertDialog2;
            }
            alertDialog.show();
        } else {
            AlertDialog alertDialog3 = this.processingDonationPaymentDialog;
            if (alertDialog3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("processingDonationPaymentDialog");
            } else {
                alertDialog = alertDialog3;
            }
            alertDialog.hide();
        }
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this, boostState) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$getConfiguration$1
            final /* synthetic */ BoostState $state;
            final /* synthetic */ BoostFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$state = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.customPref(new BoostAnimation.Model());
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText.CenterModifier centerModifier = DSLSettingsText.CenterModifier.INSTANCE;
                boolean z = true;
                dSLConfiguration.sectionHeaderPref(companion.from(R.string.BoostFragment__give_signal_a_boost, centerModifier, DSLSettingsText.TitleLargeModifier.INSTANCE));
                dSLConfiguration.noPadTextPref(companion.from(BoostFragment.access$getSayThanks(this.this$0), centerModifier));
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                dSLConfiguration.space((int) dimensionUnit.toPixels(28.0f));
                Currency currencySelection = this.$state.getCurrencySelection();
                BoostState.Stage stage = this.$state.getStage();
                BoostState.Stage stage2 = BoostState.Stage.READY;
                boolean z2 = stage == stage2;
                final BoostFragment boostFragment = this.this$0;
                dSLConfiguration.customPref(new CurrencySelection.Model(currencySelection, z2, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = FragmentKt.findNavController(boostFragment);
                        Object[] array = BoostFragment.access$getViewModel(boostFragment).getSupportedCurrencyCodes().toArray(new String[0]);
                        if (array != null) {
                            BoostFragmentDirections.ActionBoostFragmentToSetDonationCurrencyFragment actionBoostFragmentToSetDonationCurrencyFragment = BoostFragmentDirections.actionBoostFragmentToSetDonationCurrencyFragment(true, (String[]) array);
                            Intrinsics.checkNotNullExpressionValue(actionBoostFragmentToSetDonationCurrencyFragment, "actionBoostFragmentToSet…cyCodes().toTypedArray())");
                            SafeNavigation.safeNavigate(findNavController, actionBoostFragmentToSetDonationCurrencyFragment);
                            return;
                        }
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                    }
                }));
                if (this.$state.getStage() == BoostState.Stage.INIT) {
                    dSLConfiguration.customPref(new Boost.LoadingModel());
                } else if (this.$state.getStage() == BoostState.Stage.FAILURE) {
                    dSLConfiguration.space((int) dimensionUnit.toPixels(20.0f));
                    final BoostFragment boostFragment2 = this.this$0;
                    dSLConfiguration.customPref(new NetworkFailure.Model(new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$getConfiguration$1.2
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            BoostFragment.access$getViewModel(boostFragment2).retry();
                        }
                    }));
                } else {
                    List<Boost> boosts = this.$state.getBoosts();
                    Boost selectedBoost = this.$state.getSelectedBoost();
                    Currency currency = this.$state.getCustomAmount().getCurrency();
                    boolean isCustomAmountFocused = this.$state.isCustomAmountFocused();
                    boolean z3 = this.$state.getStage() == stage2;
                    Intrinsics.checkNotNullExpressionValue(currency, "currency");
                    final BoostFragment boostFragment3 = this.this$0;
                    AnonymousClass3 r15 = new Function2<View, Boost, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$getConfiguration$1.3
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        @Override // kotlin.jvm.functions.Function2
                        public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                            invoke((View) obj, (Boost) obj2);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(View view, Boost boost) {
                            Intrinsics.checkNotNullParameter(view, "view");
                            Intrinsics.checkNotNullParameter(boost, "boost");
                            BoostFragment.access$startAnimationAboveSelectedBoost(boostFragment3, view);
                            BoostFragment.access$getViewModel(boostFragment3).setSelectedBoost(boost);
                        }
                    };
                    final BoostFragment boostFragment4 = this.this$0;
                    AnonymousClass4 r6 = new Function1<String, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$getConfiguration$1.4
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(String str) {
                            invoke(str);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(String str) {
                            Intrinsics.checkNotNullParameter(str, "it");
                            BoostFragment.access$getViewModel(boostFragment4).setCustomAmount(str);
                        }
                    };
                    final BoostFragment boostFragment5 = this.this$0;
                    dSLConfiguration.customPref(new Boost.SelectionModel(boosts, selectedBoost, currency, z3, r15, isCustomAmountFocused, r6, new Function1<Boolean, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$getConfiguration$1.5
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
                            invoke(bool.booleanValue());
                            return Unit.INSTANCE;
                        }

                        public final void invoke(boolean z4) {
                            if (z4) {
                                BoostFragment.access$getViewModel(boostFragment5).setCustomAmountFocused();
                            }
                        }
                    }));
                }
                dSLConfiguration.space((int) dimensionUnit.toPixels(16.0f));
                AnonymousClass6 r4 = new Function0<Unit>(this.this$0) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$getConfiguration$1.6
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        BoostFragment.access$onGooglePayButtonClicked((BoostFragment) this.receiver);
                    }
                };
                if (this.$state.getStage() != stage2) {
                    z = false;
                }
                dSLConfiguration.customPref(new GooglePayButton.Model(r4, z));
                DSLSettingsText from = companion.from(R.string.SubscribeFragment__more_payment_options, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from2 = DSLSettingsIcon.Companion.from(R.drawable.ic_open_20, R.color.signal_accent_primary);
                final BoostFragment boostFragment6 = this.this$0;
                DSLConfiguration.secondaryButtonNoOutline$default(dSLConfiguration, from, from2, false, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$getConfiguration$1.7
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        CommunicationActions.openBrowserLink(boostFragment6.requireContext(), boostFragment6.getString(R.string.donate_url));
                    }
                }, 4, null);
            }
        });
    }

    public final void onGooglePayButtonClicked() {
        BoostViewModel viewModel = getViewModel();
        String string = getString(R.string.preferences__one_time_donation);
        Intrinsics.checkNotNullExpressionValue(string, "getString(R.string.preferences__one_time_donation)");
        viewModel.requestTokenFromGooglePay(string);
    }

    private final void onPaymentConfirmed(Badge badge) {
        NavController findNavController = FragmentKt.findNavController(this);
        BoostFragmentDirections.ActionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog isBoost = BoostFragmentDirections.actionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog(badge).setIsBoost(true);
        Intrinsics.checkNotNullExpressionValue(isBoost, "actionBoostFragmentToBoo…stBadge).setIsBoost(true)");
        SafeNavigation.safeNavigate(findNavController, isBoost, new NavOptions.Builder().setPopUpTo(R.id.boostFragment, true).build());
    }

    private final void onPaymentError(Throwable th) {
        String str = TAG;
        Log.w(str, "onPaymentError", th, true);
        if (this.errorDialog != null) {
            Log.i(str, "Already displaying an error dialog. Skipping.");
            return;
        }
        DonationErrorDialogs donationErrorDialogs = DonationErrorDialogs.INSTANCE;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        this.errorDialog = donationErrorDialogs.show(requireContext, th, new DonationErrorDialogs.DialogCallback(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$onPaymentError$1
            final /* synthetic */ BoostFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs.DialogCallback
            public void onDialogDismissed() {
                FragmentKt.findNavController(this.this$0).popBackStack();
            }
        });
    }

    public final void startAnimationAboveSelectedBoost(View view) {
        LottieAnimationView animationContainer = getAnimationContainer(view);
        Projection relativeToViewRoot = Projection.relativeToViewRoot(view, null);
        Intrinsics.checkNotNullExpressionValue(relativeToViewRoot, "relativeToViewRoot(view, null)");
        Projection relativeToViewRoot2 = Projection.relativeToViewRoot(animationContainer, null);
        Intrinsics.checkNotNullExpressionValue(relativeToViewRoot2, "relativeToViewRoot(animationView, null)");
        float x = relativeToViewRoot.getX() + (((float) relativeToViewRoot.getWidth()) / 2.0f);
        float x2 = relativeToViewRoot2.getX() + (((float) relativeToViewRoot2.getWidth()) / 2.0f);
        animationContainer.setTranslationY((-((relativeToViewRoot2.getY() + ((float) relativeToViewRoot2.getHeight())) - relativeToViewRoot.getY())) + (((float) relativeToViewRoot.getHeight()) / 2.0f));
        animationContainer.setTranslationX(x - x2);
        animationContainer.playAnimation();
        relativeToViewRoot.release();
        relativeToViewRoot2.release();
    }

    private final LottieAnimationView getAnimationContainer(View view) {
        LottieAnimationView lottieAnimationView;
        switch (view.getId()) {
            case R.id.boost_1 /* 2131362244 */:
                lottieAnimationView = this.boost1AnimationView;
                if (lottieAnimationView == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("boost1AnimationView");
                    return null;
                }
                break;
            case R.id.boost_2 /* 2131362245 */:
                lottieAnimationView = this.boost2AnimationView;
                if (lottieAnimationView == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("boost2AnimationView");
                    return null;
                }
                break;
            case R.id.boost_3 /* 2131362246 */:
                lottieAnimationView = this.boost3AnimationView;
                if (lottieAnimationView == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("boost3AnimationView");
                    return null;
                }
                break;
            case R.id.boost_4 /* 2131362247 */:
                lottieAnimationView = this.boost4AnimationView;
                if (lottieAnimationView == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("boost4AnimationView");
                    return null;
                }
                break;
            case R.id.boost_5 /* 2131362248 */:
                lottieAnimationView = this.boost5AnimationView;
                if (lottieAnimationView == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("boost5AnimationView");
                    return null;
                }
                break;
            case R.id.boost_6 /* 2131362249 */:
                lottieAnimationView = this.boost6AnimationView;
                if (lottieAnimationView == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("boost6AnimationView");
                    return null;
                }
                break;
            default:
                throw new AssertionError();
        }
        return lottieAnimationView;
    }

    /* compiled from: BoostFragment.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostFragment$Companion;", "", "()V", "FETCH_BOOST_TOKEN_REQUEST_CODE", "", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
