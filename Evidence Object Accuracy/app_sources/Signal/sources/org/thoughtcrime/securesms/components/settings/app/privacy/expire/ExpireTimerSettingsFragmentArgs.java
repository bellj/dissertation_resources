package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class ExpireTimerSettingsFragmentArgs {
    private final HashMap arguments;

    private ExpireTimerSettingsFragmentArgs() {
        this.arguments = new HashMap();
    }

    private ExpireTimerSettingsFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static ExpireTimerSettingsFragmentArgs fromBundle(Bundle bundle) {
        ExpireTimerSettingsFragmentArgs expireTimerSettingsFragmentArgs = new ExpireTimerSettingsFragmentArgs();
        bundle.setClassLoader(ExpireTimerSettingsFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("recipient_id")) {
            expireTimerSettingsFragmentArgs.arguments.put("recipient_id", null);
        } else if (Parcelable.class.isAssignableFrom(RecipientId.class) || Serializable.class.isAssignableFrom(RecipientId.class)) {
            expireTimerSettingsFragmentArgs.arguments.put("recipient_id", (RecipientId) bundle.get("recipient_id"));
        } else {
            throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
        if (bundle.containsKey("for_result_mode")) {
            expireTimerSettingsFragmentArgs.arguments.put("for_result_mode", Boolean.valueOf(bundle.getBoolean("for_result_mode")));
        } else {
            expireTimerSettingsFragmentArgs.arguments.put("for_result_mode", Boolean.FALSE);
        }
        if (!bundle.containsKey("initial_value")) {
            expireTimerSettingsFragmentArgs.arguments.put("initial_value", null);
        } else if (Parcelable.class.isAssignableFrom(Integer.class) || Serializable.class.isAssignableFrom(Integer.class)) {
            expireTimerSettingsFragmentArgs.arguments.put("initial_value", (Integer) bundle.get("initial_value"));
        } else {
            throw new UnsupportedOperationException(Integer.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
        return expireTimerSettingsFragmentArgs;
    }

    public RecipientId getRecipientId() {
        return (RecipientId) this.arguments.get("recipient_id");
    }

    public boolean getForResultMode() {
        return ((Boolean) this.arguments.get("for_result_mode")).booleanValue();
    }

    public Integer getInitialValue() {
        return (Integer) this.arguments.get("initial_value");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("recipient_id")) {
            RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
            if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
            } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
            } else {
                throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        } else {
            bundle.putSerializable("recipient_id", null);
        }
        if (this.arguments.containsKey("for_result_mode")) {
            bundle.putBoolean("for_result_mode", ((Boolean) this.arguments.get("for_result_mode")).booleanValue());
        } else {
            bundle.putBoolean("for_result_mode", false);
        }
        if (this.arguments.containsKey("initial_value")) {
            Integer num = (Integer) this.arguments.get("initial_value");
            if (Parcelable.class.isAssignableFrom(Integer.class) || num == null) {
                bundle.putParcelable("initial_value", (Parcelable) Parcelable.class.cast(num));
            } else if (Serializable.class.isAssignableFrom(Integer.class)) {
                bundle.putSerializable("initial_value", (Serializable) Serializable.class.cast(num));
            } else {
                throw new UnsupportedOperationException(Integer.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        } else {
            bundle.putSerializable("initial_value", null);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ExpireTimerSettingsFragmentArgs expireTimerSettingsFragmentArgs = (ExpireTimerSettingsFragmentArgs) obj;
        if (this.arguments.containsKey("recipient_id") != expireTimerSettingsFragmentArgs.arguments.containsKey("recipient_id")) {
            return false;
        }
        if (getRecipientId() == null ? expireTimerSettingsFragmentArgs.getRecipientId() != null : !getRecipientId().equals(expireTimerSettingsFragmentArgs.getRecipientId())) {
            return false;
        }
        if (this.arguments.containsKey("for_result_mode") == expireTimerSettingsFragmentArgs.arguments.containsKey("for_result_mode") && getForResultMode() == expireTimerSettingsFragmentArgs.getForResultMode() && this.arguments.containsKey("initial_value") == expireTimerSettingsFragmentArgs.arguments.containsKey("initial_value")) {
            return getInitialValue() == null ? expireTimerSettingsFragmentArgs.getInitialValue() == null : getInitialValue().equals(expireTimerSettingsFragmentArgs.getInitialValue());
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((getRecipientId() != null ? getRecipientId().hashCode() : 0) + 31) * 31) + (getForResultMode() ? 1 : 0)) * 31;
        if (getInitialValue() != null) {
            i = getInitialValue().hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "ExpireTimerSettingsFragmentArgs{recipientId=" + getRecipientId() + ", forResultMode=" + getForResultMode() + ", initialValue=" + getInitialValue() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(ExpireTimerSettingsFragmentArgs expireTimerSettingsFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(expireTimerSettingsFragmentArgs.arguments);
        }

        public Builder() {
            this.arguments = new HashMap();
        }

        public ExpireTimerSettingsFragmentArgs build() {
            return new ExpireTimerSettingsFragmentArgs(this.arguments);
        }

        public Builder setRecipientId(RecipientId recipientId) {
            this.arguments.put("recipient_id", recipientId);
            return this;
        }

        public Builder setForResultMode(boolean z) {
            this.arguments.put("for_result_mode", Boolean.valueOf(z));
            return this;
        }

        public Builder setInitialValue(Integer num) {
            this.arguments.put("initial_value", num);
            return this;
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }

        public boolean getForResultMode() {
            return ((Boolean) this.arguments.get("for_result_mode")).booleanValue();
        }

        public Integer getInitialValue() {
            return (Integer) this.arguments.get("initial_value");
        }
    }
}
