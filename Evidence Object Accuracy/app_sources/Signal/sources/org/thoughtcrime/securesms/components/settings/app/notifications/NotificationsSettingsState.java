package org.thoughtcrime.securesms.components.settings.app.notifications;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: NotificationsSettingsState.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/NotificationsSettingsState;", "", "messageNotificationsState", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/MessageNotificationsState;", "callNotificationsState", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/CallNotificationsState;", "notifyWhenContactJoinsSignal", "", "(Lorg/thoughtcrime/securesms/components/settings/app/notifications/MessageNotificationsState;Lorg/thoughtcrime/securesms/components/settings/app/notifications/CallNotificationsState;Z)V", "getCallNotificationsState", "()Lorg/thoughtcrime/securesms/components/settings/app/notifications/CallNotificationsState;", "getMessageNotificationsState", "()Lorg/thoughtcrime/securesms/components/settings/app/notifications/MessageNotificationsState;", "getNotifyWhenContactJoinsSignal", "()Z", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationsSettingsState {
    private final CallNotificationsState callNotificationsState;
    private final MessageNotificationsState messageNotificationsState;
    private final boolean notifyWhenContactJoinsSignal;

    public static /* synthetic */ NotificationsSettingsState copy$default(NotificationsSettingsState notificationsSettingsState, MessageNotificationsState messageNotificationsState, CallNotificationsState callNotificationsState, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            messageNotificationsState = notificationsSettingsState.messageNotificationsState;
        }
        if ((i & 2) != 0) {
            callNotificationsState = notificationsSettingsState.callNotificationsState;
        }
        if ((i & 4) != 0) {
            z = notificationsSettingsState.notifyWhenContactJoinsSignal;
        }
        return notificationsSettingsState.copy(messageNotificationsState, callNotificationsState, z);
    }

    public final MessageNotificationsState component1() {
        return this.messageNotificationsState;
    }

    public final CallNotificationsState component2() {
        return this.callNotificationsState;
    }

    public final boolean component3() {
        return this.notifyWhenContactJoinsSignal;
    }

    public final NotificationsSettingsState copy(MessageNotificationsState messageNotificationsState, CallNotificationsState callNotificationsState, boolean z) {
        Intrinsics.checkNotNullParameter(messageNotificationsState, "messageNotificationsState");
        Intrinsics.checkNotNullParameter(callNotificationsState, "callNotificationsState");
        return new NotificationsSettingsState(messageNotificationsState, callNotificationsState, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NotificationsSettingsState)) {
            return false;
        }
        NotificationsSettingsState notificationsSettingsState = (NotificationsSettingsState) obj;
        return Intrinsics.areEqual(this.messageNotificationsState, notificationsSettingsState.messageNotificationsState) && Intrinsics.areEqual(this.callNotificationsState, notificationsSettingsState.callNotificationsState) && this.notifyWhenContactJoinsSignal == notificationsSettingsState.notifyWhenContactJoinsSignal;
    }

    public int hashCode() {
        int hashCode = ((this.messageNotificationsState.hashCode() * 31) + this.callNotificationsState.hashCode()) * 31;
        boolean z = this.notifyWhenContactJoinsSignal;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        return "NotificationsSettingsState(messageNotificationsState=" + this.messageNotificationsState + ", callNotificationsState=" + this.callNotificationsState + ", notifyWhenContactJoinsSignal=" + this.notifyWhenContactJoinsSignal + ')';
    }

    public NotificationsSettingsState(MessageNotificationsState messageNotificationsState, CallNotificationsState callNotificationsState, boolean z) {
        Intrinsics.checkNotNullParameter(messageNotificationsState, "messageNotificationsState");
        Intrinsics.checkNotNullParameter(callNotificationsState, "callNotificationsState");
        this.messageNotificationsState = messageNotificationsState;
        this.callNotificationsState = callNotificationsState;
        this.notifyWhenContactJoinsSignal = z;
    }

    public final MessageNotificationsState getMessageNotificationsState() {
        return this.messageNotificationsState;
    }

    public final CallNotificationsState getCallNotificationsState() {
        return this.callNotificationsState;
    }

    public final boolean getNotifyWhenContactJoinsSignal() {
        return this.notifyWhenContactJoinsSignal;
    }
}
