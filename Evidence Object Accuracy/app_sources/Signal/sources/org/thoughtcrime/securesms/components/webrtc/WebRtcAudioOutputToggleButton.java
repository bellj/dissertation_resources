package org.thoughtcrime.securesms.components.webrtc;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class WebRtcAudioOutputToggleButton extends AppCompatImageView {
    private static final int[][] OUTPUT_ENUM;
    private static final int[] OUTPUT_HANDSET;
    private static final int[] OUTPUT_HEADSET;
    private static final List<WebRtcAudioOutput> OUTPUT_MODES;
    private static final int[] OUTPUT_SPEAKER;
    private static final int[] SPEAKER_OFF;
    private static final int[] SPEAKER_ON;
    private static final String STATE_HANDSET_ENABLED;
    private static final String STATE_HEADSET_ENABLED;
    private static final String STATE_OUTPUT_INDEX;
    private static final String STATE_PARENT;
    private OnAudioOutputChangedListener audioOutputChangedListener;
    private boolean isHandsetAvailable;
    private boolean isHeadsetAvailable;
    private int outputIndex;
    private DialogInterface picker;

    static {
        int[] iArr = {R.attr.state_speaker_off};
        SPEAKER_OFF = iArr;
        int[] iArr2 = {R.attr.state_speaker_on};
        SPEAKER_ON = iArr2;
        int[] iArr3 = {R.attr.state_handset_selected};
        OUTPUT_HANDSET = iArr3;
        int[] iArr4 = {R.attr.state_speaker_selected};
        OUTPUT_SPEAKER = iArr4;
        int[] iArr5 = {R.attr.state_headset_selected};
        OUTPUT_HEADSET = iArr5;
        OUTPUT_ENUM = new int[][]{iArr, iArr2, iArr3, iArr4, iArr5};
        WebRtcAudioOutput webRtcAudioOutput = WebRtcAudioOutput.HANDSET;
        WebRtcAudioOutput webRtcAudioOutput2 = WebRtcAudioOutput.SPEAKER;
        OUTPUT_MODES = Arrays.asList(webRtcAudioOutput, webRtcAudioOutput2, webRtcAudioOutput, webRtcAudioOutput2, WebRtcAudioOutput.HEADSET);
    }

    public WebRtcAudioOutputToggleButton(Context context) {
        this(context, null);
    }

    public WebRtcAudioOutputToggleButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public WebRtcAudioOutputToggleButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        super.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcAudioOutputToggleButton$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WebRtcAudioOutputToggleButton.$r8$lambda$CflCUoDCbtrFHFrQDLa31M9KUUQ(WebRtcAudioOutputToggleButton.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(View view) {
        List<WebRtcAudioOutput> buildOutputModeList = buildOutputModeList(this.isHeadsetAvailable, this.isHandsetAvailable);
        if (buildOutputModeList.size() > 2 || !this.isHandsetAvailable) {
            showPicker(buildOutputModeList);
            return;
        }
        List<WebRtcAudioOutput> list = OUTPUT_MODES;
        setAudioOutput(list.get((this.outputIndex + 1) % list.size()), true);
    }

    @Override // android.widget.ImageView, android.view.View
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        hidePicker();
    }

    @Override // android.widget.ImageView, android.view.View
    public int[] onCreateDrawableState(int i) {
        int[] iArr = OUTPUT_ENUM[this.outputIndex];
        int[] onCreateDrawableState = super.onCreateDrawableState(i + iArr.length);
        ImageView.mergeDrawableStates(onCreateDrawableState, iArr);
        return onCreateDrawableState;
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        throw new UnsupportedOperationException("This View does not support custom click listeners.");
    }

    public void setControlAvailability(boolean z, boolean z2) {
        this.isHandsetAvailable = z;
        this.isHeadsetAvailable = z2;
    }

    public void setAudioOutput(WebRtcAudioOutput webRtcAudioOutput, boolean z) {
        int i = this.outputIndex;
        int resolveAudioOutputIndex = resolveAudioOutputIndex(OUTPUT_MODES.lastIndexOf(webRtcAudioOutput));
        this.outputIndex = resolveAudioOutputIndex;
        if (i != resolveAudioOutputIndex) {
            refreshDrawableState();
            if (z) {
                notifyListener();
            }
        }
    }

    public void setOnAudioOutputChangedListener(OnAudioOutputChangedListener onAudioOutputChangedListener) {
        this.audioOutputChangedListener = onAudioOutputChangedListener;
    }

    private void showPicker(List<WebRtcAudioOutput> list) {
        RecyclerView recyclerView = new RecyclerView(getContext());
        AudioOutputAdapter audioOutputAdapter = new AudioOutputAdapter(new OnAudioOutputChangedListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcAudioOutputToggleButton$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.components.webrtc.OnAudioOutputChangedListener
            public final void audioOutputChanged(WebRtcAudioOutput webRtcAudioOutput) {
                WebRtcAudioOutputToggleButton.$r8$lambda$UJfFlLYfKSrKi1YwTWtyn5zTRRY(WebRtcAudioOutputToggleButton.this, webRtcAudioOutput);
            }
        }, list);
        audioOutputAdapter.setSelectedOutput(OUTPUT_MODES.get(this.outputIndex));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
        recyclerView.setAdapter(audioOutputAdapter);
        this.picker = new MaterialAlertDialogBuilder(getContext()).setTitle(R.string.WebRtcAudioOutputToggle__audio_output).setView((View) recyclerView).setCancelable(true).show();
    }

    public /* synthetic */ void lambda$showPicker$1(WebRtcAudioOutput webRtcAudioOutput) {
        setAudioOutput(webRtcAudioOutput, true);
        hidePicker();
    }

    @Override // android.view.View
    protected Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable(STATE_PARENT, onSaveInstanceState);
        bundle.putInt(STATE_OUTPUT_INDEX, this.outputIndex);
        bundle.putBoolean(STATE_HEADSET_ENABLED, this.isHeadsetAvailable);
        bundle.putBoolean(STATE_HANDSET_ENABLED, this.isHandsetAvailable);
        return bundle;
    }

    @Override // android.view.View
    protected void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.isHeadsetAvailable = bundle.getBoolean(STATE_HEADSET_ENABLED);
            this.isHandsetAvailable = bundle.getBoolean(STATE_HANDSET_ENABLED);
            setAudioOutput(OUTPUT_MODES.get(resolveAudioOutputIndex(bundle.getInt(STATE_OUTPUT_INDEX))), false);
            super.onRestoreInstanceState(bundle.getParcelable(STATE_PARENT));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    private void hidePicker() {
        DialogInterface dialogInterface = this.picker;
        if (dialogInterface != null) {
            dialogInterface.dismiss();
            this.picker = null;
        }
    }

    private void notifyListener() {
        OnAudioOutputChangedListener onAudioOutputChangedListener = this.audioOutputChangedListener;
        if (onAudioOutputChangedListener != null) {
            onAudioOutputChangedListener.audioOutputChanged(OUTPUT_MODES.get(this.outputIndex));
        }
    }

    private static List<WebRtcAudioOutput> buildOutputModeList(boolean z, boolean z2) {
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(WebRtcAudioOutput.SPEAKER);
        if (z) {
            arrayList.add(WebRtcAudioOutput.HEADSET);
        }
        if (z2) {
            arrayList.add(WebRtcAudioOutput.HANDSET);
        }
        return arrayList;
    }

    private int resolveAudioOutputIndex(int i) {
        if (isIllegalAudioOutputIndex(i)) {
            throw new IllegalArgumentException("Unsupported index: " + i);
        } else if (isUnsupportedAudioOutput(i, this.isHeadsetAvailable, this.isHandsetAvailable)) {
            if (!this.isHandsetAvailable) {
                return OUTPUT_MODES.lastIndexOf(WebRtcAudioOutput.SPEAKER);
            }
            return OUTPUT_MODES.indexOf(WebRtcAudioOutput.HANDSET);
        } else if (!this.isHeadsetAvailable) {
            return i % 2;
        } else {
            return i;
        }
    }

    private static boolean isIllegalAudioOutputIndex(int i) {
        return i < 0 || i > OUTPUT_MODES.size();
    }

    private static boolean isUnsupportedAudioOutput(int i, boolean z, boolean z2) {
        List<WebRtcAudioOutput> list = OUTPUT_MODES;
        return (list.get(i) == WebRtcAudioOutput.HEADSET && !z) || (list.get(i) == WebRtcAudioOutput.HANDSET && !z2);
    }
}
