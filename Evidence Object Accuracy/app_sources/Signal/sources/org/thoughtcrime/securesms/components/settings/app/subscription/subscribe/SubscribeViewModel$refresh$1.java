package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: SubscribeViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SubscribeViewModel$refresh$1 extends Lambda implements Function1<Boolean, Unit> {
    final /* synthetic */ SubscribeViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SubscribeViewModel$refresh$1(SubscribeViewModel subscribeViewModel) {
        super(1);
        this.this$0 = subscribeViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
        invoke(bool.booleanValue());
        return Unit.INSTANCE;
    }

    public final void invoke(boolean z) {
        this.this$0.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$refresh$1$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel$refresh$1.m1072invoke$lambda0(this.f$0, (SubscribeState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final SubscribeState m1072invoke$lambda0(boolean z, SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "state");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, null, z, 63, null);
    }
}
