package org.thoughtcrime.securesms.components.viewpager;

import androidx.viewpager.widget.ViewPager;

/* loaded from: classes4.dex */
public abstract class ExtendedOnPageChangedListener implements ViewPager.OnPageChangeListener {
    private Integer currentPage = null;

    @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
    public void onPageScrollStateChanged(int i) {
    }

    @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
    public void onPageScrolled(int i, float f, int i2) {
    }

    public abstract void onPageUnselected(int i);

    @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
    public void onPageSelected(int i) {
        Integer num = this.currentPage;
        if (!(num == null || num.intValue() == i)) {
            onPageUnselected(this.currentPage.intValue());
        }
        this.currentPage = Integer.valueOf(i);
    }
}
