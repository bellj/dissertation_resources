package org.thoughtcrime.securesms.components.settings;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/SectionHeaderPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/SectionHeaderPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "sectionHeader", "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SectionHeaderPreferenceViewHolder extends MappingViewHolder<SectionHeaderPreference> {
    private final TextView sectionHeader;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SectionHeaderPreferenceViewHolder(View view) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
        View findViewById = view.findViewById(R.id.section_header);
        Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.section_header)");
        this.sectionHeader = (TextView) findViewById;
    }

    public void bind(SectionHeaderPreference sectionHeaderPreference) {
        Intrinsics.checkNotNullParameter(sectionHeaderPreference, "model");
        TextView textView = this.sectionHeader;
        DSLSettingsText title = sectionHeaderPreference.getTitle();
        Context context = this.context;
        Intrinsics.checkNotNullExpressionValue(context, "context");
        textView.setText(title.resolve(context));
    }
}
