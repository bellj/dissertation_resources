package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.constraintlayout.widget.ConstraintLayout;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: ButtonStripItemView.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0016\u0010\r\u001a\u00020\u000e2\u000e\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0010R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/ButtonStripItemView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "iconView", "Landroid/widget/ImageView;", "labelView", "Landroid/widget/TextView;", "setOnIconClickedListener", "", "onIconClickedListener", "Lkotlin/Function0;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ButtonStripItemView extends ConstraintLayout {
    private final ImageView iconView;
    private final TextView labelView;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ButtonStripItemView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ButtonStripItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ ButtonStripItemView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ButtonStripItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        ViewGroup.inflate(context, R.layout.button_strip_item_view, this);
        View findViewById = findViewById(R.id.icon);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.icon)");
        ImageView imageView = (ImageView) findViewById;
        this.iconView = imageView;
        View findViewById2 = findViewById(R.id.label);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.label)");
        TextView textView = (TextView) findViewById2;
        this.labelView = textView;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ButtonStripItemView);
        Intrinsics.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…able.ButtonStripItemView)");
        int resourceId = obtainStyledAttributes.getResourceId(0, -1);
        Drawable drawable = resourceId > 0 ? AppCompatResources.getDrawable(context, resourceId) : null;
        String string = obtainStyledAttributes.getString(1);
        String string2 = obtainStyledAttributes.getString(2);
        imageView.setImageDrawable(drawable);
        imageView.setContentDescription(string);
        textView.setText(string2);
        obtainStyledAttributes.recycle();
    }

    /* renamed from: setOnIconClickedListener$lambda-0 */
    public static final void m474setOnIconClickedListener$lambda0(Function0 function0, View view) {
        if (function0 != null) {
            function0.invoke();
        }
    }

    public final void setOnIconClickedListener(Function0<Unit> function0) {
        this.iconView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.ButtonStripItemView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ButtonStripItemView.m474setOnIconClickedListener$lambda0(Function0.this, view);
            }
        });
    }
}
