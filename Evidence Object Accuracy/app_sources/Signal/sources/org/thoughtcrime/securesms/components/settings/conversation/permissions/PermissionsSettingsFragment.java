package org.thoughtcrime.securesms.components.settings.conversation.permissions;

import android.content.Context;
import android.os.Parcelable;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsEvents;
import org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsViewModel;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.groups.ui.GroupErrors;

/* compiled from: PermissionsSettingsFragment.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0003J\u0010\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00020\u001dH\u0002R!\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048BX\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u001b\u0010\n\u001a\u00020\u000b8BX\u0002¢\u0006\f\n\u0004\b\u000e\u0010\t\u001a\u0004\b\f\u0010\r¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "permissionsOptions", "", "", "getPermissionsOptions", "()[Ljava/lang/String;", "permissionsOptions$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsViewModel;", "viewModel$delegate", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsState;", "getSelected", "", "isNonAdminAllowed", "", "handleGroupChangeError", "groupChangeError", "Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsEvents$GroupChangeError;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PermissionsSettingsFragment extends DSLSettingsFragment {
    private final Lazy permissionsOptions$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsFragment$permissionsOptions$2
        final /* synthetic */ PermissionsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.PermissionsSettingsFragment__editor_labels);
        }
    });
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(PermissionsSettingsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsFragment$viewModel$2
        final /* synthetic */ PermissionsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            PermissionsSettingsFragmentArgs fromBundle = PermissionsSettingsFragmentArgs.fromBundle(this.this$0.requireArguments());
            Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
            Parcelable groupId = fromBundle.getGroupId();
            if (groupId != null) {
                GroupId groupId2 = ParcelableGroupId.get((ParcelableGroupId) groupId);
                if (groupId2 != null) {
                    Intrinsics.checkNotNullExpressionValue(groupId2, "requireNotNull(Parcelabl…Id as ParcelableGroupId))");
                    Context requireContext = this.this$0.requireContext();
                    Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                    return new PermissionsSettingsViewModel.Factory(groupId2, new PermissionsSettingsRepository(requireContext));
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            }
            throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.groups.ParcelableGroupId");
        }
    });

    public final int getSelected(boolean z) {
        return z ? 1 : 0;
    }

    public PermissionsSettingsFragment() {
        super(R.string.ConversationSettingsFragment__permissions, 0, 0, null, 14, null);
    }

    public final String[] getPermissionsOptions() {
        Object value = this.permissionsOptions$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-permissionsOptions>(...)");
        return (String[]) value;
    }

    public final PermissionsSettingsViewModel getViewModel() {
        return (PermissionsSettingsViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ PermissionsSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PermissionsSettingsFragment.m1177$r8$lambda$38W98iiLEb6UZDkjqfLxIDIsN4(DSLSettingsAdapter.this, this.f$1, (PermissionsSettingsState) obj);
            }
        });
        getViewModel().getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PermissionsSettingsFragment.$r8$lambda$vm9bRIlm1TC6fePWACkIR1_QqjM(PermissionsSettingsFragment.this, (PermissionsSettingsEvents) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m1178bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, PermissionsSettingsFragment permissionsSettingsFragment, PermissionsSettingsState permissionsSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(permissionsSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(permissionsSettingsState, "state");
        dSLSettingsAdapter.submitList(permissionsSettingsFragment.getConfiguration(permissionsSettingsState).toMappingModelList());
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m1179bindAdapter$lambda1(PermissionsSettingsFragment permissionsSettingsFragment, PermissionsSettingsEvents permissionsSettingsEvents) {
        Intrinsics.checkNotNullParameter(permissionsSettingsFragment, "this$0");
        if (permissionsSettingsEvents instanceof PermissionsSettingsEvents.GroupChangeError) {
            Intrinsics.checkNotNullExpressionValue(permissionsSettingsEvents, "event");
            permissionsSettingsFragment.handleGroupChangeError((PermissionsSettingsEvents.GroupChangeError) permissionsSettingsEvents);
        }
    }

    private final void handleGroupChangeError(PermissionsSettingsEvents.GroupChangeError groupChangeError) {
        Toast.makeText(getContext(), GroupErrors.getUserDisplayMessage(groupChangeError.getReason()), 1).show();
    }

    private final DSLConfiguration getConfiguration(PermissionsSettingsState permissionsSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(permissionsSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsFragment$getConfiguration$1
            final /* synthetic */ PermissionsSettingsState $state;
            final /* synthetic */ PermissionsSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.PermissionsSettingsFragment__add_members, new DSLSettingsText.Modifier[0]);
                boolean selfCanEditSettings = this.$state.getSelfCanEditSettings();
                String[] access$getPermissionsOptions = PermissionsSettingsFragment.access$getPermissionsOptions(this.this$0);
                DSLSettingsText from2 = companion.from(R.string.PermissionsSettingsFragment__who_can_add_new_members, new DSLSettingsText.Modifier[0]);
                int access$getSelected = PermissionsSettingsFragment.access$getSelected(this.this$0, this.$state.getNonAdminCanAddMembers());
                final PermissionsSettingsFragment permissionsSettingsFragment = this.this$0;
                dSLConfiguration.radioListPref(from, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from : from2, (r19 & 8) != 0 ? true : selfCanEditSettings, access$getPermissionsOptions, access$getSelected, (r19 & 64) != 0 ? false : true, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsFragment$getConfiguration$1.1
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                        invoke(num.intValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(int i) {
                        PermissionsSettingsViewModel access$getViewModel = PermissionsSettingsFragment.access$getViewModel(permissionsSettingsFragment);
                        boolean z = true;
                        if (i != 1) {
                            z = false;
                        }
                        access$getViewModel.setNonAdminCanAddMembers(z);
                    }
                });
                DSLSettingsText from3 = companion.from(R.string.PermissionsSettingsFragment__edit_group_info, new DSLSettingsText.Modifier[0]);
                boolean selfCanEditSettings2 = this.$state.getSelfCanEditSettings();
                String[] access$getPermissionsOptions2 = PermissionsSettingsFragment.access$getPermissionsOptions(this.this$0);
                DSLSettingsText from4 = companion.from(R.string.PermissionsSettingsFragment__who_can_edit_this_groups_info, new DSLSettingsText.Modifier[0]);
                int access$getSelected2 = PermissionsSettingsFragment.access$getSelected(this.this$0, this.$state.getNonAdminCanEditGroupInfo());
                final PermissionsSettingsFragment permissionsSettingsFragment2 = this.this$0;
                dSLConfiguration.radioListPref(from3, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from3 : from4, (r19 & 8) != 0 ? true : selfCanEditSettings2, access$getPermissionsOptions2, access$getSelected2, (r19 & 64) != 0 ? false : true, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsFragment$getConfiguration$1.2
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                        invoke(num.intValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(int i) {
                        PermissionsSettingsViewModel access$getViewModel = PermissionsSettingsFragment.access$getViewModel(permissionsSettingsFragment2);
                        boolean z = true;
                        if (i != 1) {
                            z = false;
                        }
                        access$getViewModel.setNonAdminCanEditGroupInfo(z);
                    }
                });
                DSLSettingsText from5 = companion.from(R.string.PermissionsSettingsFragment__send_messages, new DSLSettingsText.Modifier[0]);
                boolean selfCanEditSettings3 = this.$state.getSelfCanEditSettings();
                String[] access$getPermissionsOptions3 = PermissionsSettingsFragment.access$getPermissionsOptions(this.this$0);
                DSLSettingsText from6 = companion.from(R.string.PermissionsSettingsFragment__who_can_send_messages, new DSLSettingsText.Modifier[0]);
                int access$getSelected3 = PermissionsSettingsFragment.access$getSelected(this.this$0, !this.$state.getAnnouncementGroup());
                final PermissionsSettingsFragment permissionsSettingsFragment3 = this.this$0;
                dSLConfiguration.radioListPref(from5, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from5 : from6, (r19 & 8) != 0 ? true : selfCanEditSettings3, access$getPermissionsOptions3, access$getSelected3, (r19 & 64) != 0 ? false : true, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsFragment$getConfiguration$1.3
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                        invoke(num.intValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(int i) {
                        PermissionsSettingsFragment.access$getViewModel(permissionsSettingsFragment3).setAnnouncementGroup(i == 0);
                    }
                });
            }
        });
    }
}
