package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ShapeScrim extends View {
    private final int canvasColor;
    private final Paint eraser;
    private final float radius;
    private Bitmap scrim;
    private Canvas scrimCanvas;
    private int scrimHeight;
    private int scrimWidth;
    private final ShapeType shape;

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public enum ShapeType {
        CIRCLE,
        SQUARE
    }

    public ShapeScrim(Context context) {
        this(context, null);
    }

    public ShapeScrim(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ShapeScrim(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.ShapeScrim, 0, 0);
            String string = obtainStyledAttributes.getString(1);
            if ("square".equalsIgnoreCase(string)) {
                this.shape = ShapeType.SQUARE;
            } else if ("circle".equalsIgnoreCase(string)) {
                this.shape = ShapeType.CIRCLE;
            } else {
                this.shape = ShapeType.SQUARE;
            }
            this.radius = obtainStyledAttributes.getFloat(0, 0.4f);
            obtainStyledAttributes.recycle();
        } else {
            this.shape = ShapeType.SQUARE;
            this.radius = 0.4f;
        }
        Paint paint = new Paint();
        this.eraser = paint;
        paint.setColor(-1);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        this.canvasColor = Color.parseColor("#55BDBDBD");
    }

    @Override // android.view.View
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        float min = ((float) Math.min(getWidth(), getHeight())) * this.radius;
        float measuredWidth = ((float) (getMeasuredWidth() / 2)) - min;
        float measuredHeight = ((float) (getMeasuredHeight() / 2)) - min;
        float f = min * 2.0f;
        this.scrimWidth = (int) ((measuredWidth + f) - measuredWidth);
        this.scrimHeight = (int) ((f + measuredHeight) - measuredHeight);
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float min = ((float) Math.min(getWidth(), getHeight())) * this.radius;
        if (this.scrimCanvas == null) {
            this.scrim = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            this.scrimCanvas = new Canvas(this.scrim);
        }
        this.scrim.eraseColor(0);
        this.scrimCanvas.drawColor(this.canvasColor);
        if (this.shape == ShapeType.CIRCLE) {
            drawCircle(this.scrimCanvas, min, this.eraser);
        } else {
            drawSquare(this.scrimCanvas, min, this.eraser);
        }
        canvas.drawBitmap(this.scrim, 0.0f, 0.0f, (Paint) null);
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i4, i4);
        if (i != i3 || i2 != i4) {
            this.scrim = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
            this.scrimCanvas = new Canvas(this.scrim);
        }
    }

    private void drawCircle(Canvas canvas, float f, Paint paint) {
        canvas.drawCircle((float) (getWidth() / 2), (float) (getHeight() / 2), f, paint);
    }

    private void drawSquare(Canvas canvas, float f, Paint paint) {
        float width = ((float) (getWidth() / 2)) - f;
        float height = ((float) (getHeight() / 2)) - f;
        float f2 = f * 2.0f;
        canvas.drawRoundRect(new RectF(width, height, width + f2, f2 + height), 25.0f, 25.0f, paint);
    }

    public int getScrimWidth() {
        return this.scrimWidth;
    }

    public int getScrimHeight() {
        return this.scrimHeight;
    }
}
