package org.thoughtcrime.securesms.components.settings.app.changenumber;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.AppSettingsChangeNumberDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ChangeNumberVerifyFragmentDirections {
    private ChangeNumberVerifyFragmentDirections() {
    }

    public static NavDirections actionChangePhoneNumberVerifyFragmentToCaptchaFragment() {
        return new ActionOnlyNavDirections(R.id.action_changePhoneNumberVerifyFragment_to_captchaFragment);
    }

    public static NavDirections actionChangePhoneNumberVerifyFragmentToChangeNumberEnterCodeFragment() {
        return new ActionOnlyNavDirections(R.id.action_changePhoneNumberVerifyFragment_to_changeNumberEnterCodeFragment);
    }

    public static NavDirections actionPopAppSettingsChangeNumber() {
        return AppSettingsChangeNumberDirections.actionPopAppSettingsChangeNumber();
    }
}
