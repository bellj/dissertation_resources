package org.thoughtcrime.securesms.maps;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Looper;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
class LocationRetriever implements DefaultLifecycleObserver, LocationListener {
    private static final String TAG = Log.tag(LocationRetriever.class);
    private final Context context;
    private final FailureListener failureListener;
    private final LocationManager locationManager;
    private final SuccessListener successListener;

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public interface FailureListener {
        void onFailure();
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public interface SuccessListener {
        void onSuccess(Location location);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onPause(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner);
    }

    public LocationRetriever(Context context, LifecycleOwner lifecycleOwner, SuccessListener successListener, FailureListener failureListener) {
        this.context = context;
        this.locationManager = ServiceUtil.getLocationManager(context);
        this.successListener = successListener;
        this.failureListener = failureListener;
        lifecycleOwner.getLifecycle().addObserver(this);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onStart(LifecycleOwner lifecycleOwner) {
        if (!(ContextCompat.checkSelfPermission(this.context, "android.permission.ACCESS_COARSE_LOCATION") == 0 || ContextCompat.checkSelfPermission(this.context, "android.permission.ACCESS_FINE_LOCATION") == 0)) {
            Log.w(TAG, "No location permission!");
            this.failureListener.onFailure();
        }
        LocationProvider provider = this.locationManager.getProvider("gps");
        if (provider == null) {
            Log.w(TAG, "GPS provider is null. Trying network provider.");
            provider = this.locationManager.getProvider("network");
        }
        if (provider == null) {
            Log.w(TAG, "Network provider is null. Unable to retrieve location.");
            this.failureListener.onFailure();
            return;
        }
        Location lastKnownLocation = this.locationManager.getLastKnownLocation(provider.getName());
        if (lastKnownLocation != null) {
            Log.i(TAG, "Using last known location.");
            this.successListener.onSuccess(lastKnownLocation);
            return;
        }
        Log.i(TAG, "No last known location. Requesting a single update.");
        this.locationManager.requestSingleUpdate(provider.getName(), this, (Looper) null);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onStop(LifecycleOwner lifecycleOwner) {
        Log.i(TAG, "Removing any possible location listeners.");
        this.locationManager.removeUpdates(this);
    }

    @Override // android.location.LocationListener
    public void onLocationChanged(Location location) {
        if (location != null) {
            Log.w(TAG, "[onLocationChanged] Successfully retrieved location.");
            this.successListener.onSuccess(location);
            return;
        }
        Log.w(TAG, "[onLocationChanged] Null location.");
        this.failureListener.onFailure();
    }

    @Override // android.location.LocationListener
    public void onStatusChanged(String str, int i, Bundle bundle) {
        String str2 = TAG;
        Log.i(str2, "[onStatusChanged] Provider: " + str + " Status: " + i);
    }

    @Override // android.location.LocationListener
    public void onProviderEnabled(String str) {
        String str2 = TAG;
        Log.i(str2, "[onProviderEnabled] Provider: " + str);
    }

    @Override // android.location.LocationListener
    public void onProviderDisabled(String str) {
        String str2 = TAG;
        Log.i(str2, "[onProviderDisabled] Provider: " + str);
    }
}
