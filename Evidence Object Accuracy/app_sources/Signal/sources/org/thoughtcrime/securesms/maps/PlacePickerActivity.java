package org.thoughtcrime.securesms.maps;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.maps.LocationRetriever;

/* loaded from: classes4.dex */
public final class PlacePickerActivity extends AppCompatActivity {
    private static final String ADDRESS_INTENT;
    private static final int ANIMATION_DURATION;
    private static final String KEY_CHAT_COLOR;
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator();
    private static final LatLng PRIME_MERIDIAN = new LatLng(51.4779d, -0.0015d);
    private static final String TAG = Log.tag(PlacePickerActivity.class);
    private static final float ZOOM;
    private AddressLookup addressLookup;
    private SingleAddressBottomSheet bottomSheet;
    private Address currentAddress;
    private LatLng currentLocation = new LatLng(0.0d, 0.0d);
    private GoogleMap googleMap;
    private LatLng initialLocation;

    public static void startActivityForResultAtCurrentLocation(Fragment fragment, int i, int i2) {
        fragment.startActivityForResult(new Intent(fragment.requireActivity(), PlacePickerActivity.class).putExtra(KEY_CHAT_COLOR, i2), i);
    }

    public static AddressData addressFromData(Intent intent) {
        return (AddressData) intent.getParcelableExtra(ADDRESS_INTENT);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_place_picker);
        this.bottomSheet = (SingleAddressBottomSheet) findViewById(R.id.bottom_sheet);
        View findViewById = findViewById(R.id.marker_image_view);
        View findViewById2 = findViewById(R.id.place_chosen_button);
        ViewCompat.setBackgroundTintList(findViewById2, ColorStateList.valueOf(getIntent().getIntExtra(KEY_CHAT_COLOR, -65536)));
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.maps.PlacePickerActivity$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PlacePickerActivity.this.lambda$onCreate$0(view);
            }
        });
        if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            new LocationRetriever(this, this, new LocationRetriever.SuccessListener() { // from class: org.thoughtcrime.securesms.maps.PlacePickerActivity$$ExternalSyntheticLambda3
                @Override // org.thoughtcrime.securesms.maps.LocationRetriever.SuccessListener
                public final void onSuccess(Location location) {
                    PlacePickerActivity.this.lambda$onCreate$1(location);
                }
            }, new LocationRetriever.FailureListener() { // from class: org.thoughtcrime.securesms.maps.PlacePickerActivity$$ExternalSyntheticLambda4
                @Override // org.thoughtcrime.securesms.maps.LocationRetriever.FailureListener
                public final void onFailure() {
                    PlacePickerActivity.this.lambda$onCreate$2();
                }
            });
        } else {
            Log.w(TAG, "No location permissions");
            setInitialLocation(PRIME_MERIDIAN);
        }
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (supportMapFragment != null) {
            supportMapFragment.getMapAsync(new OnMapReadyCallback(findViewById) { // from class: org.thoughtcrime.securesms.maps.PlacePickerActivity$$ExternalSyntheticLambda5
                public final /* synthetic */ View f$1;

                {
                    this.f$1 = r2;
                }

                @Override // com.google.android.gms.maps.OnMapReadyCallback
                public final void onMapReady(GoogleMap googleMap) {
                    PlacePickerActivity.this.lambda$onCreate$5(this.f$1, googleMap);
                }
            });
            return;
        }
        throw new AssertionError("No map fragment");
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        finishWithAddress();
    }

    public /* synthetic */ void lambda$onCreate$1(Location location) {
        setInitialLocation(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    public /* synthetic */ void lambda$onCreate$2() {
        Log.w(TAG, "Failed to get location.");
        setInitialLocation(PRIME_MERIDIAN);
    }

    public /* synthetic */ void lambda$onCreate$5(View view, GoogleMap googleMap) {
        setMap(googleMap);
        enableMyLocationButtonIfHaveThePermission(googleMap);
        googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener(view) { // from class: org.thoughtcrime.securesms.maps.PlacePickerActivity$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener
            public final void onCameraMoveStarted(int i) {
                PlacePickerActivity.this.lambda$onCreate$3(this.f$1, i);
            }
        });
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener(view, googleMap) { // from class: org.thoughtcrime.securesms.maps.PlacePickerActivity$$ExternalSyntheticLambda1
            public final /* synthetic */ View f$1;
            public final /* synthetic */ GoogleMap f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // com.google.android.gms.maps.GoogleMap.OnCameraIdleListener
            public final void onCameraIdle() {
                PlacePickerActivity.this.lambda$onCreate$4(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$3(View view, int i) {
        view.animate().translationY(-75.0f).setInterpolator(OVERSHOOT_INTERPOLATOR).setDuration(250).start();
        this.bottomSheet.hide();
    }

    public /* synthetic */ void lambda$onCreate$4(View view, GoogleMap googleMap) {
        view.animate().translationY(0.0f).setInterpolator(OVERSHOOT_INTERPOLATOR).setDuration(250).start();
        setCurrentLocation(googleMap.getCameraPosition().target);
    }

    private void setInitialLocation(LatLng latLng) {
        this.initialLocation = latLng;
        moveMapToInitialIfPossible();
    }

    private void setMap(GoogleMap googleMap) {
        this.googleMap = googleMap;
        moveMapToInitialIfPossible();
    }

    private void moveMapToInitialIfPossible() {
        if (this.initialLocation != null && this.googleMap != null) {
            Log.d(TAG, "Moving map to initial location");
            this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(this.initialLocation, ZOOM));
            setCurrentLocation(this.initialLocation);
        }
    }

    private void setCurrentLocation(LatLng latLng) {
        this.currentLocation = latLng;
        this.bottomSheet.showLoading();
        lookupAddress(latLng);
    }

    private void finishWithAddress() {
        Intent intent = new Intent();
        Address address = this.currentAddress;
        String addressLine = (address == null || address.getAddressLine(0) == null) ? "" : this.currentAddress.getAddressLine(0);
        LatLng latLng = this.currentLocation;
        intent.putExtra(ADDRESS_INTENT, new AddressData(latLng.latitude, latLng.longitude, addressLine));
        setResult(-1, intent);
        finish();
    }

    private void enableMyLocationButtonIfHaveThePermission(GoogleMap googleMap) {
        if (Build.VERSION.SDK_INT < 23 || checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 || checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            googleMap.setMyLocationEnabled(true);
        }
    }

    private void lookupAddress(LatLng latLng) {
        AddressLookup addressLookup = this.addressLookup;
        if (addressLookup != null) {
            addressLookup.cancel(true);
        }
        AddressLookup addressLookup2 = new AddressLookup();
        this.addressLookup = addressLookup2;
        addressLookup2.execute(latLng);
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        AddressLookup addressLookup = this.addressLookup;
        if (addressLookup != null) {
            addressLookup.cancel(true);
        }
    }

    /* loaded from: classes4.dex */
    public class AddressLookup extends AsyncTask<LatLng, Void, Address> {
        private final String TAG = Log.tag(AddressLookup.class);
        private final Geocoder geocoder;

        AddressLookup() {
            PlacePickerActivity.this = r3;
            this.geocoder = new Geocoder(r3.getApplicationContext(), Locale.getDefault());
        }

        public Address doInBackground(LatLng... latLngArr) {
            LatLng latLng;
            if (latLngArr.length == 0 || (latLng = latLngArr[0]) == null) {
                return null;
            }
            try {
                List<Address> fromLocation = this.geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                if (!fromLocation.isEmpty()) {
                    return fromLocation.get(0);
                }
                return null;
            } catch (IOException e) {
                Log.w(this.TAG, "Failed to get address from location", e);
                return null;
            }
        }

        public void onPostExecute(Address address) {
            PlacePickerActivity.this.currentAddress = address;
            if (address != null) {
                PlacePickerActivity.this.bottomSheet.showResult(address.getLatitude(), address.getLongitude(), PlacePickerActivity.addressToShortString(address), PlacePickerActivity.addressToString(address));
            } else {
                PlacePickerActivity.this.bottomSheet.hide();
            }
        }
    }

    public static String addressToString(Address address) {
        return address != null ? address.getAddressLine(0) : "";
    }

    public static String addressToShortString(Address address) {
        if (address == null) {
            return "";
        }
        String[] split = address.getAddressLine(0).split(",");
        if (split.length >= 3) {
            return split[1].trim() + ", " + split[2].trim();
        } else if (split.length == 2) {
            return split[1].trim();
        } else {
            return split[0].trim();
        }
    }
}
