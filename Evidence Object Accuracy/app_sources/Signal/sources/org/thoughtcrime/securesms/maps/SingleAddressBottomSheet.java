package org.thoughtcrime.securesms.maps;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import java.util.Locale;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class SingleAddressBottomSheet extends CoordinatorLayout {
    private BottomSheetBehavior<View> bottomSheetBehavior;
    private TextView placeAddressTextView;
    private TextView placeNameTextView;
    private ProgressBar placeProgressBar;

    public SingleAddressBottomSheet(Context context) {
        super(context);
        init();
    }

    public SingleAddressBottomSheet(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public SingleAddressBottomSheet(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void init() {
        BottomSheetBehavior<View> from = BottomSheetBehavior.from(((CoordinatorLayout) ViewGroup.inflate(getContext(), R.layout.activity_map_bottom_sheet_view, this)).findViewById(R.id.root_bottom_sheet));
        this.bottomSheetBehavior = from;
        from.setHideable(true);
        this.bottomSheetBehavior.setState(5);
        bindViews();
    }

    private void bindViews() {
        this.placeNameTextView = (TextView) findViewById(R.id.text_view_place_name);
        this.placeAddressTextView = (TextView) findViewById(R.id.text_view_place_address);
        this.placeProgressBar = (ProgressBar) findViewById(R.id.progress_bar_place);
    }

    public void showLoading() {
        this.bottomSheetBehavior.setState(3);
        this.placeNameTextView.setText("");
        this.placeAddressTextView.setText("");
        this.placeProgressBar.setVisibility(0);
    }

    public void showResult(double d, double d2, String str, String str2) {
        this.bottomSheetBehavior.setState(3);
        this.placeProgressBar.setVisibility(8);
        if (TextUtils.isEmpty(str2)) {
            String convert = Location.convert(d2, 0);
            this.placeNameTextView.setText(String.format(Locale.getDefault(), "%s %s", Location.convert(d, 0), convert));
            return;
        }
        this.placeNameTextView.setText(str);
        this.placeAddressTextView.setText(str2);
    }

    public void hide() {
        this.bottomSheetBehavior.setState(5);
    }
}
