package org.thoughtcrime.securesms.maps;

import android.os.Parcel;
import android.os.Parcelable;

/* loaded from: classes4.dex */
public final class AddressData implements Parcelable {
    public static final Parcelable.Creator<AddressData> CREATOR = new Parcelable.Creator<AddressData>() { // from class: org.thoughtcrime.securesms.maps.AddressData.1
        @Override // android.os.Parcelable.Creator
        public AddressData createFromParcel(Parcel parcel) {
            return new AddressData(parcel.readDouble(), parcel.readDouble(), parcel.readString());
        }

        @Override // android.os.Parcelable.Creator
        public AddressData[] newArray(int i) {
            return new AddressData[i];
        }
    };
    private final String address;
    private final double latitude;
    private final double longitude;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AddressData(double d, double d2, String str) {
        this.latitude = d;
        this.longitude = d2;
        this.address = str;
    }

    public String getAddress() {
        return this.address;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public double getLatitude() {
        return this.latitude;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(this.latitude);
        parcel.writeDouble(this.longitude);
        parcel.writeString(this.address);
    }
}
