package org.thoughtcrime.securesms.reactions.edit;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.WindowUtil;

/* compiled from: EditReactionsActivity.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\nH\u0014J\b\u0010\u000b\u001a\u00020\u0006H\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/reactions/edit/EditReactionsActivity;", "Lorg/thoughtcrime/securesms/PassphraseRequiredActivity;", "()V", "theme", "Lorg/thoughtcrime/securesms/util/DynamicTheme;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "ready", "", "onResume", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EditReactionsActivity extends PassphraseRequiredActivity {
    private final DynamicTheme theme = new DynamicNoActionBarTheme();

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        this.theme.onCreate(this);
        findViewById(16908290).setSystemUiVisibility(1280);
        WindowUtil.setStatusBarColor(getWindow(), ContextCompat.getColor(this, R.color.transparent));
        if (bundle == null) {
            getSupportFragmentManager().beginTransaction().replace(16908290, new EditReactionsFragment()).commit();
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.theme.onResume(this);
    }
}
