package org.thoughtcrime.securesms.reactions.edit;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: EditReactionsFragment.kt */
@Metadata(d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016J\u0012\u0010\u0006\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016J\u0012\u0010\u0007\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016¨\u0006\b"}, d2 = {"org/thoughtcrime/securesms/reactions/edit/EditReactionsFragment$Companion$startRockingAnimation$1", "Landroid/view/animation/Animation$AnimationListener;", "onAnimationEnd", "", "animation", "Landroid/view/animation/Animation;", "onAnimationRepeat", "onAnimationStart", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EditReactionsFragment$Companion$startRockingAnimation$1 implements Animation.AnimationListener {
    final /* synthetic */ View $target;

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationRepeat(Animation animation) {
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
    }

    public EditReactionsFragment$Companion$startRockingAnimation$1(View view) {
        this.$target = view;
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this.$target.getContext(), R.anim.rock);
        Intrinsics.checkNotNullExpressionValue(loadAnimation, "loadAnimation(target.context, R.anim.rock)");
        loadAnimation.setRepeatCount(-1);
        loadAnimation.setRepeatMode(2);
        this.$target.startAnimation(loadAnimation);
    }
}
