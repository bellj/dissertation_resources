package org.thoughtcrime.securesms.reactions;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.AlwaysChangedDiffUtil;

/* loaded from: classes4.dex */
public class ReactionViewPagerAdapter extends ListAdapter<EmojiCount, ViewHolder> {
    private int selectedPosition = 0;

    public /* bridge */ /* synthetic */ void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i, List list) {
        onBindViewHolder((ViewHolder) viewHolder, i, (List<Object>) list);
    }

    public ReactionViewPagerAdapter() {
        super(new AlwaysChangedDiffUtil());
    }

    public EmojiCount getEmojiCount(int i) {
        return getItem(i);
    }

    public void enableNestedScrollingForPosition(int i) {
        this.selectedPosition = i;
        notifyItemRangeChanged(0, getItemCount(), new Object());
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.reactions_bottom_sheet_dialog_fragment_recycler, viewGroup, false));
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i, List<Object> list) {
        if (list.isEmpty()) {
            onBindViewHolder(viewHolder, i);
        } else {
            viewHolder.setSelected(this.selectedPosition);
        }
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.onBind(getItem(i));
        viewHolder.setSelected(this.selectedPosition);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        recyclerView.setNestedScrollingEnabled(false);
        ViewGroup.LayoutParams layoutParams = recyclerView.getLayoutParams();
        double d = (double) recyclerView.getResources().getDisplayMetrics().heightPixels;
        Double.isNaN(d);
        layoutParams.height = (int) (d * 0.8d);
        recyclerView.setLayoutParams(layoutParams);
        recyclerView.setHasFixedSize(true);
    }

    /* loaded from: classes4.dex */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ReactionRecipientsAdapter adapter;
        private final RecyclerView recycler;

        public ViewHolder(View view) {
            super(view);
            ReactionRecipientsAdapter reactionRecipientsAdapter = new ReactionRecipientsAdapter();
            this.adapter = reactionRecipientsAdapter;
            RecyclerView recyclerView = (RecyclerView) view;
            this.recycler = recyclerView;
            recyclerView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            recyclerView.setAdapter(reactionRecipientsAdapter);
        }

        public void onBind(EmojiCount emojiCount) {
            this.adapter.updateData(emojiCount.getReactions());
        }

        public void setSelected(int i) {
            this.recycler.setNestedScrollingEnabled(getAdapterPosition() == i);
        }
    }
}
