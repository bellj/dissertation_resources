package org.thoughtcrime.securesms.reactions.edit;

import kotlin.Metadata;
import kotlin.jvm.internal.PropertyReference1Impl;
import org.thoughtcrime.securesms.reactions.edit.EditReactionsViewModel;

/* compiled from: EditReactionsViewModel.kt */
@Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
/* synthetic */ class EditReactionsViewModel$reactions$1 extends PropertyReference1Impl {
    public static final EditReactionsViewModel$reactions$1 INSTANCE = new EditReactionsViewModel$reactions$1();

    EditReactionsViewModel$reactions$1() {
        super(EditReactionsViewModel.State.class, "reactions", "getReactions()Ljava/util/List;", 0);
    }

    @Override // kotlin.jvm.internal.PropertyReference1Impl, kotlin.reflect.KProperty1
    public Object get(Object obj) {
        return ((EditReactionsViewModel.State) obj).getReactions();
    }
}
