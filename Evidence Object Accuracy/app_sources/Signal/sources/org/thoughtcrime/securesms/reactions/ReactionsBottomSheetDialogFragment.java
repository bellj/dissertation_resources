package org.thoughtcrime.securesms.reactions;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiImageView;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.reactions.ReactionsViewModel;
import org.thoughtcrime.securesms.util.FullscreenHelper;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.WindowUtil;

/* loaded from: classes4.dex */
public final class ReactionsBottomSheetDialogFragment extends BottomSheetDialogFragment {
    private static final String ARGS_IS_MMS;
    private static final String ARGS_MESSAGE_ID;
    private Callback callback;
    private final LifecycleDisposable disposables = new LifecycleDisposable();
    private ViewPager2 recipientPagerView;
    private ReactionViewPagerAdapter recipientsAdapter;
    private ReactionsViewModel viewModel;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onReactionsDialogDismissed();
    }

    public static DialogFragment create(long j, boolean z) {
        Bundle bundle = new Bundle();
        ReactionsBottomSheetDialogFragment reactionsBottomSheetDialogFragment = new ReactionsBottomSheetDialogFragment();
        bundle.putLong(ARGS_MESSAGE_ID, j);
        bundle.putBoolean(ARGS_IS_MMS, z);
        reactionsBottomSheetDialogFragment.setArguments(bundle);
        return reactionsBottomSheetDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Callback) {
            this.callback = (Callback) context;
        } else if (getParentFragment() instanceof Callback) {
            this.callback = (Callback) getParentFragment();
        } else {
            throw new IllegalStateException("Parent component does not implement Callback");
        }
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, R.style.Theme_Signal_BottomSheetDialog_Fixed_ReactWithAny);
        super.onCreate(bundle);
    }

    @Override // com.google.android.material.bottomsheet.BottomSheetDialogFragment, androidx.appcompat.app.AppCompatDialogFragment, androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        FullscreenHelper.showSystemUI(onCreateDialog.getWindow());
        WindowUtil.setNavigationBarColor(requireContext(), onCreateDialog.getWindow(), ContextCompat.getColor(requireContext(), R.color.signal_colorSurface1));
        return onCreateDialog;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.reactions_bottom_sheet_dialog_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.recipientPagerView = (ViewPager2) view.findViewById(R.id.reactions_bottom_view_recipient_pager);
        this.disposables.bindTo(getViewLifecycleOwner());
        setUpRecipientsRecyclerView();
        setUpTabMediator(view, bundle);
        setUpViewModel(new MessageId(requireArguments().getLong(ARGS_MESSAGE_ID), requireArguments().getBoolean(ARGS_IS_MMS)));
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        this.callback.onReactionsDialogDismissed();
    }

    private void setUpTabMediator(View view, Bundle bundle) {
        if (bundle == null) {
            new TabLayoutMediator((TabLayout) view.findViewById(R.id.emoji_tabs), this.recipientPagerView, new TabLayoutMediator.TabConfigurationStrategy() { // from class: org.thoughtcrime.securesms.reactions.ReactionsBottomSheetDialogFragment$$ExternalSyntheticLambda0
                @Override // com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
                public final void onConfigureTab(TabLayout.Tab tab, int i) {
                    ReactionsBottomSheetDialogFragment.this.lambda$setUpTabMediator$0(tab, i);
                }
            }).attach();
        }
    }

    public /* synthetic */ void lambda$setUpTabMediator$0(TabLayout.Tab tab, int i) {
        tab.setCustomView(R.layout.reactions_bottom_sheet_dialog_fragment_emoji_item);
        View customView = tab.getCustomView();
        Objects.requireNonNull(customView);
        EmojiImageView emojiImageView = (EmojiImageView) customView.findViewById(R.id.reactions_bottom_view_emoji_item_emoji);
        TextView textView = (TextView) customView.findViewById(R.id.reactions_bottom_view_emoji_item_text);
        EmojiCount emojiCount = this.recipientsAdapter.getEmojiCount(i);
        if (i != 0) {
            emojiImageView.setVisibility(0);
            emojiImageView.setImageEmoji(emojiCount.getDisplayEmoji());
            textView.setText(String.valueOf(emojiCount.getCount()));
            return;
        }
        emojiImageView.setVisibility(8);
        textView.setText(customView.getContext().getString(R.string.ReactionsBottomSheetDialogFragment_all, Integer.valueOf(emojiCount.getCount())));
    }

    private void setUpRecipientsRecyclerView() {
        this.recipientsAdapter = new ReactionViewPagerAdapter();
        this.recipientPagerView.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() { // from class: org.thoughtcrime.securesms.reactions.ReactionsBottomSheetDialogFragment.1
            @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
            public void onPageSelected(int i) {
                ReactionsBottomSheetDialogFragment.this.recipientPagerView.post(new ReactionsBottomSheetDialogFragment$1$$ExternalSyntheticLambda0(this, i));
            }

            public /* synthetic */ void lambda$onPageSelected$0(int i) {
                ReactionsBottomSheetDialogFragment.this.recipientsAdapter.enableNestedScrollingForPosition(i);
            }

            @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
            public void onPageScrollStateChanged(int i) {
                if (i == 0) {
                    ReactionsBottomSheetDialogFragment.this.recipientPagerView.requestLayout();
                }
            }
        });
        this.recipientPagerView.setAdapter(this.recipientsAdapter);
    }

    private void setUpViewModel(MessageId messageId) {
        ReactionsViewModel reactionsViewModel = (ReactionsViewModel) new ViewModelProvider(this, new ReactionsViewModel.Factory(messageId)).get(ReactionsViewModel.class);
        this.viewModel = reactionsViewModel;
        this.disposables.add(reactionsViewModel.getEmojiCounts().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.reactions.ReactionsBottomSheetDialogFragment$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ReactionsBottomSheetDialogFragment.this.lambda$setUpViewModel$1((List) obj);
            }
        }));
    }

    public /* synthetic */ void lambda$setUpViewModel$1(List list) throws Throwable {
        if (list.size() <= 1) {
            dismiss();
        }
        this.recipientsAdapter.submitList(list);
    }
}
