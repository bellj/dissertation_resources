package org.thoughtcrime.securesms.reactions.any;

import java.util.List;
import java.util.Objects;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public class ReactWithAnyEmojiPage {
    private final List<ReactWithAnyEmojiPageBlock> pageBlocks;

    public ReactWithAnyEmojiPage(List<ReactWithAnyEmojiPageBlock> list) {
        Preconditions.checkArgument(!list.isEmpty());
        this.pageBlocks = list;
    }

    public String getKey() {
        return this.pageBlocks.get(0).getPageModel().getKey();
    }

    public int getLabel() {
        return this.pageBlocks.get(0).getLabel();
    }

    public boolean hasEmoji() {
        return !this.pageBlocks.get(0).getPageModel().getEmoji().isEmpty();
    }

    public List<ReactWithAnyEmojiPageBlock> getPageBlocks() {
        return this.pageBlocks;
    }

    public int getIconAttr() {
        return this.pageBlocks.get(0).getPageModel().getIconAttr();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.pageBlocks.equals(((ReactWithAnyEmojiPage) obj).pageBlocks);
    }

    public int hashCode() {
        return Objects.hash(this.pageBlocks);
    }
}
