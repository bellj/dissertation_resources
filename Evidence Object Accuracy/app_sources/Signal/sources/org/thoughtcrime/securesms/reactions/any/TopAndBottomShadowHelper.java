package org.thoughtcrime.securesms.reactions.any;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: TopAndBottomShadowHelper.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u0015B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\nH\u0002J!\u0010\u000b\u001a\u00020\f2\u0012\u0010\r\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u000e\"\u00020\u0003H\u0002¢\u0006\u0002\u0010\u000fJ \u0010\u0010\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012H\u0016J!\u0010\u0014\u001a\u00020\f2\u0012\u0010\r\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u000e\"\u00020\u0003H\u0002¢\u0006\u0002\u0010\u000fR\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/reactions/any/TopAndBottomShadowHelper;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "toolbarShadow", "Landroid/view/View;", "bottomToolbarShadow", "(Landroid/view/View;Landroid/view/View;)V", "lastAnimationState", "Lorg/thoughtcrime/securesms/reactions/any/TopAndBottomShadowHelper$AnimationState;", "getAnimationState", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "hide", "", "views", "", "([Landroid/view/View;)V", "onScrolled", "dx", "", "dy", "show", "AnimationState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TopAndBottomShadowHelper extends RecyclerView.OnScrollListener {
    private final View bottomToolbarShadow;
    private AnimationState lastAnimationState = AnimationState.NONE;
    private final View toolbarShadow;

    /* compiled from: TopAndBottomShadowHelper.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/reactions/any/TopAndBottomShadowHelper$AnimationState;", "", "(Ljava/lang/String;I)V", "NONE", "HIDE_TOP_AND_HIDE_BOTTOM", "HIDE_TOP_AND_SHOW_BOTTOM", "SHOW_TOP_AND_HIDE_BOTTOM", "SHOW_TOP_AND_SHOW_BOTTOM", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum AnimationState {
        NONE,
        HIDE_TOP_AND_HIDE_BOTTOM,
        HIDE_TOP_AND_SHOW_BOTTOM,
        SHOW_TOP_AND_HIDE_BOTTOM,
        SHOW_TOP_AND_SHOW_BOTTOM
    }

    /* compiled from: TopAndBottomShadowHelper.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[AnimationState.values().length];
            iArr[AnimationState.NONE.ordinal()] = 1;
            iArr[AnimationState.HIDE_TOP_AND_HIDE_BOTTOM.ordinal()] = 2;
            iArr[AnimationState.HIDE_TOP_AND_SHOW_BOTTOM.ordinal()] = 3;
            iArr[AnimationState.SHOW_TOP_AND_HIDE_BOTTOM.ordinal()] = 4;
            iArr[AnimationState.SHOW_TOP_AND_SHOW_BOTTOM.ordinal()] = 5;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public TopAndBottomShadowHelper(View view, View view2) {
        Intrinsics.checkNotNullParameter(view, "toolbarShadow");
        Intrinsics.checkNotNullParameter(view2, "bottomToolbarShadow");
        this.toolbarShadow = view;
        this.bottomToolbarShadow = view2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        AnimationState animationState = getAnimationState(recyclerView);
        if (animationState != this.lastAnimationState) {
            int i3 = WhenMappings.$EnumSwitchMapping$0[animationState.ordinal()];
            if (i3 != 1) {
                if (i3 == 2) {
                    hide(this.toolbarShadow, this.bottomToolbarShadow);
                } else if (i3 == 3) {
                    hide(this.toolbarShadow);
                    show(this.bottomToolbarShadow);
                } else if (i3 == 4) {
                    show(this.toolbarShadow);
                    hide(this.bottomToolbarShadow);
                } else if (i3 == 5) {
                    show(this.toolbarShadow, this.bottomToolbarShadow);
                }
                this.lastAnimationState = animationState;
                return;
            }
            throw new AssertionError();
        }
    }

    private final AnimationState getAnimationState(RecyclerView recyclerView) {
        boolean canScrollVertically = recyclerView.canScrollVertically(1);
        boolean canScrollVertically2 = recyclerView.canScrollVertically(-1);
        if (!canScrollVertically && !canScrollVertically2) {
            return AnimationState.HIDE_TOP_AND_HIDE_BOTTOM;
        }
        if (canScrollVertically && !canScrollVertically2) {
            return AnimationState.HIDE_TOP_AND_SHOW_BOTTOM;
        }
        if (canScrollVertically || !canScrollVertically2) {
            return AnimationState.SHOW_TOP_AND_SHOW_BOTTOM;
        }
        return AnimationState.SHOW_TOP_AND_HIDE_BOTTOM;
    }

    private final void hide(View... viewArr) {
        for (View view : viewArr) {
            view.animate().setDuration(250).alpha(0.0f);
        }
    }

    private final void show(View... viewArr) {
        for (View view : viewArr) {
            view.animate().setDuration(250).alpha(1.0f);
        }
    }
}
