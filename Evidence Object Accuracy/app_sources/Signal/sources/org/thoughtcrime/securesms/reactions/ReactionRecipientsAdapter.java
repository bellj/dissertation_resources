package org.thoughtcrime.securesms.reactions;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.AvatarUtil;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class ReactionRecipientsAdapter extends RecyclerView.Adapter<ViewHolder> {
    private List<ReactionDetails> data = Collections.emptyList();

    public void updateData(List<ReactionDetails> list) {
        this.data = list;
        notifyDataSetChanged();
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.reactions_bottom_sheet_dialog_fragment_recipient_item, viewGroup, false));
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.bind(this.data.get(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.data.size();
    }

    /* loaded from: classes4.dex */
    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private final AvatarImageView avatar;
        private final BadgeImageView badge;
        private final TextView emoji;
        private final TextView recipient;

        public ViewHolder(View view) {
            super(view);
            this.avatar = (AvatarImageView) view.findViewById(R.id.reactions_bottom_view_recipient_avatar);
            this.badge = (BadgeImageView) view.findViewById(R.id.reactions_bottom_view_recipient_badge);
            this.recipient = (TextView) view.findViewById(R.id.reactions_bottom_view_recipient_name);
            this.emoji = (TextView) view.findViewById(R.id.reactions_bottom_view_recipient_emoji);
        }

        void bind(ReactionDetails reactionDetails) {
            this.emoji.setText(reactionDetails.getDisplayEmoji());
            if (reactionDetails.getSender().isSelf()) {
                this.recipient.setText(R.string.ReactionsRecipientAdapter_you);
                AvatarImageView avatarImageView = this.avatar;
                avatarImageView.setAvatar(GlideApp.with(avatarImageView), (Recipient) null, false);
                this.badge.setBadge(null);
                AvatarUtil.loadIconIntoImageView(reactionDetails.getSender(), this.avatar);
                return;
            }
            this.recipient.setText(reactionDetails.getSender().getDisplayName(this.itemView.getContext()));
            AvatarImageView avatarImageView2 = this.avatar;
            avatarImageView2.setAvatar(GlideApp.with(avatarImageView2), reactionDetails.getSender(), false);
            this.badge.setBadgeFromRecipient(reactionDetails.getSender());
        }
    }
}
