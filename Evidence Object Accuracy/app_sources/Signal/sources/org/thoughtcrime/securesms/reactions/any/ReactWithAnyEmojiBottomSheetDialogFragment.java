package org.thoughtcrime.securesms.reactions.any;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.loader.app.LoaderManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import j$.util.Optional;
import j$.util.function.Consumer;
import kotlin.jvm.functions.Function1;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;
import org.thoughtcrime.securesms.components.emoji.EmojiEventListener;
import org.thoughtcrime.securesms.components.emoji.EmojiPageView;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageCategoriesAdapter;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageCategoryMappingModel;
import org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView;
import org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiViewModel;
import org.thoughtcrime.securesms.reactions.edit.EditReactionsActivity;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;

/* loaded from: classes4.dex */
public final class ReactWithAnyEmojiBottomSheetDialogFragment extends FixedRoundedCornerBottomSheetDialogFragment implements EmojiEventListener, EmojiPageViewGridAdapter.VariationSelectorListener {
    private static final String ABOUT_STORAGE_KEY;
    private static final String ARG_EDIT;
    private static final String ARG_IS_MMS;
    private static final String ARG_MESSAGE_ID;
    private static final String ARG_RECENT_KEY;
    private static final String ARG_SHADOWS;
    private static final String ARG_START_PAGE;
    private static final String REACTION_STORAGE_KEY;
    private Callback callback;
    private final UpdateCategorySelectionOnScroll categoryUpdateOnScroll = new UpdateCategorySelectionOnScroll();
    private final LifecycleDisposable disposables = new LifecycleDisposable();
    private EmojiPageView emojiPageView;
    private KeyboardPageSearchView search;
    private View tabBar;
    private ReactWithAnyEmojiViewModel viewModel;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onReactWithAnyEmojiDialogDismissed();

        void onReactWithAnyEmojiSelected(String str);
    }

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    public int getThemeResId() {
        return R.style.Widget_Signal_ReactWithAny;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onKeyEvent(KeyEvent keyEvent) {
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter.VariationSelectorListener
    public void onVariationSelectorStateChanged(boolean z) {
    }

    public static DialogFragment createForStory() {
        ReactWithAnyEmojiBottomSheetDialogFragment reactWithAnyEmojiBottomSheetDialogFragment = new ReactWithAnyEmojiBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(ARG_MESSAGE_ID, -1);
        bundle.putBoolean(ARG_IS_MMS, false);
        bundle.putInt(ARG_START_PAGE, -1);
        bundle.putString(ARG_RECENT_KEY, REACTION_STORAGE_KEY);
        bundle.putBoolean(ARG_EDIT, false);
        reactWithAnyEmojiBottomSheetDialogFragment.setArguments(bundle);
        return reactWithAnyEmojiBottomSheetDialogFragment;
    }

    public static DialogFragment createForMessageRecord(MessageRecord messageRecord, int i) {
        ReactWithAnyEmojiBottomSheetDialogFragment reactWithAnyEmojiBottomSheetDialogFragment = new ReactWithAnyEmojiBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(ARG_MESSAGE_ID, messageRecord.getId());
        bundle.putBoolean(ARG_IS_MMS, messageRecord.isMms());
        bundle.putInt(ARG_START_PAGE, i);
        bundle.putString(ARG_RECENT_KEY, REACTION_STORAGE_KEY);
        bundle.putBoolean(ARG_EDIT, true);
        reactWithAnyEmojiBottomSheetDialogFragment.setArguments(bundle);
        return reactWithAnyEmojiBottomSheetDialogFragment;
    }

    public static DialogFragment createForAboutSelection() {
        ReactWithAnyEmojiBottomSheetDialogFragment reactWithAnyEmojiBottomSheetDialogFragment = new ReactWithAnyEmojiBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(ARG_MESSAGE_ID, -1);
        bundle.putBoolean(ARG_IS_MMS, false);
        bundle.putInt(ARG_START_PAGE, -1);
        bundle.putString(ARG_RECENT_KEY, "pref_recent_emoji2");
        reactWithAnyEmojiBottomSheetDialogFragment.setArguments(bundle);
        return reactWithAnyEmojiBottomSheetDialogFragment;
    }

    public static DialogFragment createForEditReactions() {
        ReactWithAnyEmojiBottomSheetDialogFragment reactWithAnyEmojiBottomSheetDialogFragment = new ReactWithAnyEmojiBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(ARG_MESSAGE_ID, -1);
        bundle.putBoolean(ARG_IS_MMS, false);
        bundle.putInt(ARG_START_PAGE, -1);
        bundle.putBoolean(ARG_SHADOWS, false);
        bundle.putString(ARG_RECENT_KEY, REACTION_STORAGE_KEY);
        reactWithAnyEmojiBottomSheetDialogFragment.setArguments(bundle);
        return reactWithAnyEmojiBottomSheetDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof Callback) {
            this.callback = (Callback) getParentFragment();
        } else {
            this.callback = (Callback) context;
        }
    }

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment, com.google.android.material.bottomsheet.BottomSheetDialogFragment, androidx.appcompat.app.AppCompatDialogFragment, androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        Window window;
        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(bundle);
        if (!requireArguments().getBoolean(ARG_SHADOWS, true) && (window = bottomSheetDialog.getWindow()) != null) {
            window.clearFlags(2);
        }
        return bottomSheetDialog;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.react_with_any_emoji_bottom_sheet_dialog_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.disposables.bindTo(getViewLifecycleOwner());
        EmojiPageView emojiPageView = (EmojiPageView) view.findViewById(R.id.react_with_any_emoji_page_view);
        this.emojiPageView = emojiPageView;
        emojiPageView.initialize(this, this, true);
        this.emojiPageView.addOnScrollListener(this.categoryUpdateOnScroll);
        this.search = (KeyboardPageSearchView) view.findViewById(R.id.react_with_any_emoji_search);
        initializeViewModel();
        EmojiKeyboardPageCategoriesAdapter emojiKeyboardPageCategoriesAdapter = new EmojiKeyboardPageCategoriesAdapter(new Consumer() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment$$ExternalSyntheticLambda2
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                ReactWithAnyEmojiBottomSheetDialogFragment.this.lambda$onViewCreated$0((String) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
        FrameLayout frameLayout = (FrameLayout) requireDialog().findViewById(R.id.container);
        View inflate = LayoutInflater.from(requireContext()).inflate(R.layout.react_with_any_emoji_tabs, (ViewGroup) frameLayout, false);
        this.tabBar = inflate;
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.emoji_categories_recycler);
        recyclerView.setAdapter(emojiKeyboardPageCategoriesAdapter);
        if (requireArguments().getBoolean(ARG_EDIT, false)) {
            View findViewById = this.tabBar.findViewById(R.id.customize_reactions_frame);
            findViewById.setVisibility(0);
            findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment$$ExternalSyntheticLambda3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    ReactWithAnyEmojiBottomSheetDialogFragment.this.lambda$onViewCreated$1(view2);
                }
            });
        }
        frameLayout.addView(this.tabBar);
        this.emojiPageView.addOnScrollListener(new TopAndBottomShadowHelper(requireView().findViewById(R.id.react_with_any_emoji_top_shadow), this.tabBar.findViewById(R.id.react_with_any_emoji_bottom_shadow)));
        this.disposables.add(this.viewModel.getEmojiList().subscribe(new io.reactivex.rxjava3.functions.Consumer() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ReactWithAnyEmojiBottomSheetDialogFragment.this.lambda$onViewCreated$2((MappingModelList) obj);
            }
        }));
        this.disposables.add(this.viewModel.getCategories().subscribe(new io.reactivex.rxjava3.functions.Consumer() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                EmojiKeyboardPageCategoriesAdapter.this.submitList((MappingModelList) obj);
            }
        }));
        this.disposables.add(this.viewModel.getSelectedKey().subscribe(new io.reactivex.rxjava3.functions.Consumer(emojiKeyboardPageCategoriesAdapter) { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment$$ExternalSyntheticLambda6
            public final /* synthetic */ EmojiKeyboardPageCategoriesAdapter f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ReactWithAnyEmojiBottomSheetDialogFragment.lambda$onViewCreated$5(RecyclerView.this, this.f$1, (String) obj);
            }
        }));
        this.search.setCallbacks(new SearchCallbacks());
    }

    public /* synthetic */ void lambda$onViewCreated$0(String str) {
        scrollTo(str);
        this.viewModel.selectPage(str);
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        startActivity(new Intent(requireContext(), EditReactionsActivity.class));
    }

    public /* synthetic */ void lambda$onViewCreated$2(MappingModelList mappingModelList) throws Throwable {
        this.emojiPageView.setList(mappingModelList, null);
    }

    public static /* synthetic */ void lambda$onViewCreated$5(RecyclerView recyclerView, EmojiKeyboardPageCategoriesAdapter emojiKeyboardPageCategoriesAdapter, String str) throws Throwable {
        recyclerView.post(new Runnable(str, recyclerView) { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;
            public final /* synthetic */ RecyclerView f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ReactWithAnyEmojiBottomSheetDialogFragment.lambda$onViewCreated$4(EmojiKeyboardPageCategoriesAdapter.this, this.f$1, this.f$2);
            }
        });
    }

    public static /* synthetic */ Boolean lambda$onViewCreated$3(String str, EmojiKeyboardPageCategoryMappingModel emojiKeyboardPageCategoryMappingModel) {
        return Boolean.valueOf(emojiKeyboardPageCategoryMappingModel.getKey().equals(str));
    }

    public static /* synthetic */ void lambda$onViewCreated$4(EmojiKeyboardPageCategoriesAdapter emojiKeyboardPageCategoriesAdapter, String str, RecyclerView recyclerView) {
        int indexOfFirst = emojiKeyboardPageCategoriesAdapter.indexOfFirst(EmojiKeyboardPageCategoryMappingModel.class, new Function1(str) { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public final Object invoke(Object obj) {
                return ReactWithAnyEmojiBottomSheetDialogFragment.lambda$onViewCreated$3(this.f$0, (EmojiKeyboardPageCategoryMappingModel) obj);
            }
        });
        if (indexOfFirst != -1) {
            recyclerView.smoothScrollToPosition(indexOfFirst);
        }
    }

    private void scrollTo(String str) {
        int indexOfFirst;
        if (this.emojiPageView.getAdapter() != null && (indexOfFirst = this.emojiPageView.getAdapter().indexOfFirst(EmojiPageViewGridAdapter.EmojiHeader.class, new Function1(str) { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment$$ExternalSyntheticLambda7
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public final Object invoke(Object obj) {
                return ReactWithAnyEmojiBottomSheetDialogFragment.lambda$scrollTo$6(this.f$0, (EmojiPageViewGridAdapter.EmojiHeader) obj);
            }
        })) != -1) {
            ((BottomSheetDialog) requireDialog()).getBehavior().setState(3);
            this.categoryUpdateOnScroll.startAutoScrolling();
            this.emojiPageView.smoothScrollToPositionTop(indexOfFirst);
        }
    }

    public static /* synthetic */ Boolean lambda$scrollTo$6(String str, EmojiPageViewGridAdapter.EmojiHeader emojiHeader) {
        return Boolean.valueOf(emojiHeader.getKey().equals(str));
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        LoaderManager.getInstance(requireActivity()).destroyLoader((int) requireArguments().getLong(ARG_MESSAGE_ID));
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        this.callback.onReactWithAnyEmojiDialogDismissed();
    }

    private void initializeViewModel() {
        Bundle requireArguments = requireArguments();
        this.viewModel = (ReactWithAnyEmojiViewModel) ViewModelProviders.of(this, new ReactWithAnyEmojiViewModel.Factory(new ReactWithAnyEmojiRepository(requireContext(), requireArguments.getString(ARG_RECENT_KEY, REACTION_STORAGE_KEY)), requireArguments.getLong(ARG_MESSAGE_ID), requireArguments.getBoolean(ARG_IS_MMS))).get(ReactWithAnyEmojiViewModel.class);
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onEmojiSelected(String str) {
        this.viewModel.onEmojiSelected(str);
        this.callback.onReactWithAnyEmojiSelected(str);
        dismiss();
    }

    /* loaded from: classes4.dex */
    public class UpdateCategorySelectionOnScroll extends RecyclerView.OnScrollListener {
        private boolean doneScrolling;

        private UpdateCategorySelectionOnScroll() {
            ReactWithAnyEmojiBottomSheetDialogFragment.this = r1;
            this.doneScrolling = true;
        }

        public void startAutoScrolling() {
            this.doneScrolling = false;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            if (i == 0 && !this.doneScrolling) {
                this.doneScrolling = true;
                onScrolled(recyclerView, 0, 0);
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            if (this.doneScrolling && recyclerView.getLayoutManager() != null && ReactWithAnyEmojiBottomSheetDialogFragment.this.emojiPageView.getAdapter() != null) {
                Optional<MappingModel<?>> model = ReactWithAnyEmojiBottomSheetDialogFragment.this.emojiPageView.getAdapter().getModel(((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition());
                if (model.isPresent() && (model.get() instanceof EmojiPageViewGridAdapter.HasKey)) {
                    ReactWithAnyEmojiBottomSheetDialogFragment.this.viewModel.selectPage(((EmojiPageViewGridAdapter.HasKey) model.get()).getKey());
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    private class SearchCallbacks implements KeyboardPageSearchView.Callbacks {
        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onClicked() {
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onFocusLost() {
        }

        private SearchCallbacks() {
            ReactWithAnyEmojiBottomSheetDialogFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onQueryChanged(String str) {
            boolean z = !TextUtils.isEmpty(str);
            ReactWithAnyEmojiBottomSheetDialogFragment.this.search.enableBackNavigation(z);
            if (z) {
                ViewUtil.fadeOut(ReactWithAnyEmojiBottomSheetDialogFragment.this.tabBar, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION, 4);
            } else {
                ViewUtil.fadeIn(ReactWithAnyEmojiBottomSheetDialogFragment.this.tabBar, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
            }
            ReactWithAnyEmojiBottomSheetDialogFragment.this.viewModel.onQueryChanged(str);
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onNavigationClicked() {
            ReactWithAnyEmojiBottomSheetDialogFragment.this.search.clearQuery();
            ReactWithAnyEmojiBottomSheetDialogFragment.this.search.clearFocus();
            ViewUtil.hideKeyboard(ReactWithAnyEmojiBottomSheetDialogFragment.this.requireContext(), ReactWithAnyEmojiBottomSheetDialogFragment.this.requireView());
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onFocusGained() {
            ((BottomSheetDialog) ReactWithAnyEmojiBottomSheetDialogFragment.this.requireDialog()).getBehavior().setState(3);
        }
    }
}
