package org.thoughtcrime.securesms.reactions;

import org.thoughtcrime.securesms.reactions.ReactionsBottomSheetDialogFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ReactionsBottomSheetDialogFragment$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ReactionsBottomSheetDialogFragment.AnonymousClass1 f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ ReactionsBottomSheetDialogFragment$1$$ExternalSyntheticLambda0(ReactionsBottomSheetDialogFragment.AnonymousClass1 r1, int i) {
        this.f$0 = r1;
        this.f$1 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onPageSelected$0(this.f$1);
    }
}
