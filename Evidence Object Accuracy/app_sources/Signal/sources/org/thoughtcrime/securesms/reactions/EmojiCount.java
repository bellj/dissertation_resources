package org.thoughtcrime.securesms.reactions;

import java.util.List;

/* loaded from: classes4.dex */
public final class EmojiCount {
    private final String baseEmoji;
    private final String displayEmoji;
    private final List<ReactionDetails> reactions;

    public static EmojiCount all(List<ReactionDetails> list) {
        return new EmojiCount("", "", list);
    }

    public EmojiCount(String str, String str2, List<ReactionDetails> list) {
        this.baseEmoji = str;
        this.displayEmoji = str2;
        this.reactions = list;
    }

    public String getBaseEmoji() {
        return this.baseEmoji;
    }

    public String getDisplayEmoji() {
        return this.displayEmoji;
    }

    public int getCount() {
        return this.reactions.size();
    }

    public List<ReactionDetails> getReactions() {
        return this.reactions;
    }
}
