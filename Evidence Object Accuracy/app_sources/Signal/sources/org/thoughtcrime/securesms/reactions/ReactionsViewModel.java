package org.thoughtcrime.securesms.reactions;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Function;
import j$.util.Comparator;
import j$.util.function.ToLongFunction;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.thoughtcrime.securesms.database.model.MessageId;

/* loaded from: classes4.dex */
public class ReactionsViewModel extends ViewModel {
    private final MessageId messageId;
    private final ReactionsRepository repository = new ReactionsRepository();

    public ReactionsViewModel(MessageId messageId) {
        this.messageId = messageId;
    }

    public Observable<List<EmojiCount>> getEmojiCounts() {
        return this.repository.getReactions(this.messageId).map(new Function() { // from class: org.thoughtcrime.securesms.reactions.ReactionsViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ReactionsViewModel.this.lambda$getEmojiCounts$1((List) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread());
    }

    public /* synthetic */ List lambda$getEmojiCounts$1(List list) throws Throwable {
        List list2 = Stream.of(list).groupBy(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.reactions.ReactionsViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((ReactionDetails) obj).getBaseEmoji();
            }
        }).sorted(new Comparator() { // from class: org.thoughtcrime.securesms.reactions.ReactionsViewModel$$ExternalSyntheticLambda2
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return ReactionsViewModel.this.compareReactions((Map.Entry) obj, (Map.Entry) obj2);
            }
        }).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.reactions.ReactionsViewModel$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ReactionsViewModel.this.lambda$getEmojiCounts$0((Map.Entry) obj);
            }
        }).toList();
        list2.add(0, EmojiCount.all(list));
        return list2;
    }

    public /* synthetic */ EmojiCount lambda$getEmojiCounts$0(Map.Entry entry) {
        return new EmojiCount((String) entry.getKey(), getCountDisplayEmoji((List) entry.getValue()), (List) entry.getValue());
    }

    public int compareReactions(Map.Entry<String, List<ReactionDetails>> entry, Map.Entry<String, List<ReactionDetails>> entry2) {
        int i = -Integer.compare(entry.getValue().size(), entry2.getValue().size());
        if (i != 0) {
            return i;
        }
        return -Long.compare(getLatestTimestamp(entry.getValue()), getLatestTimestamp(entry2.getValue()));
    }

    private long getLatestTimestamp(List<ReactionDetails> list) {
        return ((Long) Stream.of(list).max(Comparator.CC.comparingLong(new ToLongFunction() { // from class: org.thoughtcrime.securesms.reactions.ReactionsViewModel$$ExternalSyntheticLambda4
            @Override // j$.util.function.ToLongFunction
            public final long applyAsLong(Object obj) {
                return ((ReactionDetails) obj).getTimestamp();
            }
        })).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.reactions.ReactionsViewModel$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return Long.valueOf(((ReactionDetails) obj).getTimestamp());
            }
        }).orElse(-1L)).longValue();
    }

    private String getCountDisplayEmoji(List<ReactionDetails> list) {
        for (ReactionDetails reactionDetails : list) {
            if (reactionDetails.getSender().isSelf()) {
                return reactionDetails.getDisplayEmoji();
            }
        }
        return list.get(list.size() - 1).getDisplayEmoji();
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final MessageId messageId;

        public Factory(MessageId messageId) {
            this.messageId = messageId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ReactionsViewModel(this.messageId));
        }
    }
}
