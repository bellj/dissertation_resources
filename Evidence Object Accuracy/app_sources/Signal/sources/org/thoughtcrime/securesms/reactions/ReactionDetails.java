package org.thoughtcrime.securesms.reactions;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ReactionDetails.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0014\u001a\u00020\bHÆ\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001J\t\u0010\u001b\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/reactions/ReactionDetails;", "", "sender", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "baseEmoji", "", "displayEmoji", "timestamp", "", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Ljava/lang/String;Ljava/lang/String;J)V", "getBaseEmoji", "()Ljava/lang/String;", "getDisplayEmoji", "getSender", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getTimestamp", "()J", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ReactionDetails {
    private final String baseEmoji;
    private final String displayEmoji;
    private final Recipient sender;
    private final long timestamp;

    public static /* synthetic */ ReactionDetails copy$default(ReactionDetails reactionDetails, Recipient recipient, String str, String str2, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            recipient = reactionDetails.sender;
        }
        if ((i & 2) != 0) {
            str = reactionDetails.baseEmoji;
        }
        if ((i & 4) != 0) {
            str2 = reactionDetails.displayEmoji;
        }
        if ((i & 8) != 0) {
            j = reactionDetails.timestamp;
        }
        return reactionDetails.copy(recipient, str, str2, j);
    }

    public final Recipient component1() {
        return this.sender;
    }

    public final String component2() {
        return this.baseEmoji;
    }

    public final String component3() {
        return this.displayEmoji;
    }

    public final long component4() {
        return this.timestamp;
    }

    public final ReactionDetails copy(Recipient recipient, String str, String str2, long j) {
        Intrinsics.checkNotNullParameter(recipient, "sender");
        Intrinsics.checkNotNullParameter(str, "baseEmoji");
        Intrinsics.checkNotNullParameter(str2, "displayEmoji");
        return new ReactionDetails(recipient, str, str2, j);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReactionDetails)) {
            return false;
        }
        ReactionDetails reactionDetails = (ReactionDetails) obj;
        return Intrinsics.areEqual(this.sender, reactionDetails.sender) && Intrinsics.areEqual(this.baseEmoji, reactionDetails.baseEmoji) && Intrinsics.areEqual(this.displayEmoji, reactionDetails.displayEmoji) && this.timestamp == reactionDetails.timestamp;
    }

    public int hashCode() {
        return (((((this.sender.hashCode() * 31) + this.baseEmoji.hashCode()) * 31) + this.displayEmoji.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.timestamp);
    }

    public String toString() {
        return "ReactionDetails(sender=" + this.sender + ", baseEmoji=" + this.baseEmoji + ", displayEmoji=" + this.displayEmoji + ", timestamp=" + this.timestamp + ')';
    }

    public ReactionDetails(Recipient recipient, String str, String str2, long j) {
        Intrinsics.checkNotNullParameter(recipient, "sender");
        Intrinsics.checkNotNullParameter(str, "baseEmoji");
        Intrinsics.checkNotNullParameter(str2, "displayEmoji");
        this.sender = recipient;
        this.baseEmoji = str;
        this.displayEmoji = str2;
        this.timestamp = j;
    }

    public final Recipient getSender() {
        return this.sender;
    }

    public final String getBaseEmoji() {
        return this.baseEmoji;
    }

    public final String getDisplayEmoji() {
        return this.displayEmoji;
    }

    public final long getTimestamp() {
        return this.timestamp;
    }
}
