package org.thoughtcrime.securesms.reactions.any;

import android.net.Uri;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.Emoji;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;

/* loaded from: classes4.dex */
public class ThisMessageEmojiPageModel implements EmojiPageModel {
    private final List<String> emoji;

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public int getIconAttr() {
        return R.attr.emoji_category_recent;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public String getKey() {
        return RecentEmojiPageModel.KEY;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public Uri getSpriteUri() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public boolean isDynamic() {
        return true;
    }

    public ThisMessageEmojiPageModel(List<String> list) {
        this.emoji = list;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public List<String> getEmoji() {
        return this.emoji;
    }

    public static /* synthetic */ Emoji lambda$getDisplayEmoji$0(String str) {
        return new Emoji(str);
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public List<Emoji> getDisplayEmoji() {
        return Stream.of(getEmoji()).map(new Function() { // from class: org.thoughtcrime.securesms.reactions.any.ThisMessageEmojiPageModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ThisMessageEmojiPageModel.lambda$getDisplayEmoji$0((String) obj);
            }
        }).toList();
    }
}
