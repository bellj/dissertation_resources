package org.thoughtcrime.securesms.reactions.any;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.emoji.EmojiCategory;
import org.thoughtcrime.securesms.emoji.EmojiSource;
import org.thoughtcrime.securesms.reactions.ReactionDetails;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sms.MessageSender;

/* loaded from: classes4.dex */
public final class ReactWithAnyEmojiRepository {
    private static final String TAG = Log.tag(ReactWithAnyEmojiRepository.class);
    private final Context context;
    private final List<ReactWithAnyEmojiPage> emojiPages;
    private final RecentEmojiPageModel recentEmojiPageModel;

    public ReactWithAnyEmojiRepository(Context context, String str) {
        this.context = context;
        this.recentEmojiPageModel = new RecentEmojiPageModel(context, str);
        LinkedList linkedList = new LinkedList();
        this.emojiPages = linkedList;
        linkedList.addAll(Stream.of(EmojiSource.getLatest().getDisplayPages()).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiRepository$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ReactWithAnyEmojiRepository.lambda$new$0((EmojiPageModel) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiRepository$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ReactWithAnyEmojiRepository.lambda$new$1((EmojiPageModel) obj);
            }
        }).toList());
    }

    public static /* synthetic */ boolean lambda$new$0(EmojiPageModel emojiPageModel) {
        return emojiPageModel.getIconAttr() == EmojiCategory.EMOTICONS.getIcon();
    }

    public static /* synthetic */ ReactWithAnyEmojiPage lambda$new$1(EmojiPageModel emojiPageModel) {
        return new ReactWithAnyEmojiPage(Collections.singletonList(new ReactWithAnyEmojiPageBlock(EmojiCategory.getCategoryLabel(emojiPageModel.getIconAttr()), emojiPageModel)));
    }

    public List<ReactWithAnyEmojiPage> getEmojiPageModels(List<ReactionDetails> list) {
        LinkedList linkedList = new LinkedList();
        List list2 = Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiRepository$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((ReactionDetails) obj).getDisplayEmoji();
            }
        }).distinct().toList();
        if (list2.isEmpty()) {
            linkedList.add(new ReactWithAnyEmojiPage(Collections.singletonList(new ReactWithAnyEmojiPageBlock(R.string.ReactWithAnyEmojiBottomSheetDialogFragment__recently_used, this.recentEmojiPageModel))));
        } else {
            linkedList.add(new ReactWithAnyEmojiPage(Arrays.asList(new ReactWithAnyEmojiPageBlock(R.string.ReactWithAnyEmojiBottomSheetDialogFragment__this_message, new ThisMessageEmojiPageModel(list2)), new ReactWithAnyEmojiPageBlock(R.string.ReactWithAnyEmojiBottomSheetDialogFragment__recently_used, this.recentEmojiPageModel))));
        }
        linkedList.addAll(this.emojiPages);
        return linkedList;
    }

    public void addEmojiToMessage(String str, MessageId messageId) {
        SignalExecutors.BOUNDED.execute(new Runnable(messageId, str) { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ MessageId f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ReactWithAnyEmojiRepository.this.lambda$addEmojiToMessage$4(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$addEmojiToMessage$4(MessageId messageId, String str) {
        ReactionRecord reactionRecord = (ReactionRecord) Stream.of(SignalDatabase.reactions().getReactions(messageId)).filter(new Predicate() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiRepository$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ReactWithAnyEmojiRepository.lambda$addEmojiToMessage$2((ReactionRecord) obj);
            }
        }).findFirst().orElse(null);
        if (reactionRecord == null || !reactionRecord.getEmoji().equals(str)) {
            MessageSender.sendNewReaction(this.context, messageId, str);
            ThreadUtil.runOnMain(new Runnable(str) { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiRepository$$ExternalSyntheticLambda2
                public final /* synthetic */ String f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ReactWithAnyEmojiRepository.this.lambda$addEmojiToMessage$3(this.f$1);
                }
            });
            return;
        }
        MessageSender.sendReactionRemoval(this.context, messageId, reactionRecord);
    }

    public static /* synthetic */ boolean lambda$addEmojiToMessage$2(ReactionRecord reactionRecord) {
        return reactionRecord.getAuthor().equals(Recipient.self().getId());
    }

    public /* synthetic */ void lambda$addEmojiToMessage$3(String str) {
        this.recentEmojiPageModel.onCodePointSelected(str);
    }
}
