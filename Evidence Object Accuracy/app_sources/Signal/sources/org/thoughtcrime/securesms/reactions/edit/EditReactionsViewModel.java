package org.thoughtcrime.securesms.reactions.edit;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.KProperty1;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.EmojiValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.reactions.edit.EditReactionsViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: EditReactionsViewModel.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u0000 \u00172\u00020\u0001:\u0002\u0017\u0018B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\bJ\u0006\u0010\u0014\u001a\u00020\u0012J\u0006\u0010\u0015\u001a\u00020\u0012J\u000e\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\fR\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/reactions/edit/EditReactionsViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "emojiValues", "Lorg/thoughtcrime/securesms/keyvalue/EmojiValues;", "reactions", "Landroidx/lifecycle/LiveData;", "", "", "getReactions", "()Landroidx/lifecycle/LiveData;", "selection", "", "getSelection", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lorg/thoughtcrime/securesms/reactions/edit/EditReactionsViewModel$State;", "onEmojiSelected", "", "emoji", "resetToDefaults", "save", "setSelection", "Companion", "State", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EditReactionsViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    public static final int NO_SELECTION;
    private final EmojiValues emojiValues;
    private final LiveData<List<String>> reactions;
    private final LiveData<Integer> selection;
    private final Store<State> store;

    public EditReactionsViewModel() {
        EmojiValues emojiValues = SignalStore.emojiValues();
        Intrinsics.checkNotNullExpressionValue(emojiValues, "emojiValues()");
        this.emojiValues = emojiValues;
        List<String> reactions = emojiValues.getReactions();
        Intrinsics.checkNotNullExpressionValue(reactions, "emojiValues.reactions");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(reactions, 10));
        for (String str : reactions) {
            String preferredVariation = this.emojiValues.getPreferredVariation(str);
            Intrinsics.checkNotNullExpressionValue(preferredVariation, "emojiValues.getPreferredVariation(it)");
            arrayList.add(preferredVariation);
        }
        Store<State> store = new Store<>(new State(0, arrayList, 1, null));
        this.store = store;
        LiveData<List<String>> mapDistinct = LiveDataUtil.mapDistinct(store.getStateLiveData(), new Function() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsViewModel$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return EditReactionsViewModel.m2531reactions$lambda1(KProperty1.this, (EditReactionsViewModel.State) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(mapDistinct, "mapDistinct(store.stateLiveData, State::reactions)");
        this.reactions = mapDistinct;
        LiveData<Integer> mapDistinct2 = LiveDataUtil.mapDistinct(store.getStateLiveData(), new Function() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsViewModel$$ExternalSyntheticLambda3
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return EditReactionsViewModel.m2534selection$lambda2(KProperty1.this, (EditReactionsViewModel.State) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(mapDistinct2, "mapDistinct(store.stateLiveData, State::selection)");
        this.selection = mapDistinct2;
    }

    /* renamed from: reactions$lambda-1 */
    public static final List m2531reactions$lambda1(KProperty1 kProperty1, State state) {
        Intrinsics.checkNotNullParameter(kProperty1, "$tmp0");
        return (List) kProperty1.invoke(state);
    }

    public final LiveData<List<String>> getReactions() {
        return this.reactions;
    }

    /* renamed from: selection$lambda-2 */
    public static final Integer m2534selection$lambda2(KProperty1 kProperty1, State state) {
        Intrinsics.checkNotNullParameter(kProperty1, "$tmp0");
        return (Integer) kProperty1.invoke(state);
    }

    public final LiveData<Integer> getSelection() {
        return this.selection;
    }

    /* renamed from: setSelection$lambda-3 */
    public static final State m2535setSelection$lambda3(int i, State state) {
        Intrinsics.checkNotNullExpressionValue(state, "it");
        return State.copy$default(state, i, null, 2, null);
    }

    public final void setSelection(int i) {
        this.store.update(new com.annimon.stream.function.Function(i) { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return EditReactionsViewModel.m2535setSelection$lambda3(this.f$0, (EditReactionsViewModel.State) obj);
            }
        });
    }

    public final void onEmojiSelected(String str) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        this.store.update(new com.annimon.stream.function.Function(str, this) { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsViewModel$$ExternalSyntheticLambda4
            public final /* synthetic */ String f$0;
            public final /* synthetic */ EditReactionsViewModel f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return EditReactionsViewModel.m2530onEmojiSelected$lambda5(this.f$0, this.f$1, (EditReactionsViewModel.State) obj);
            }
        });
    }

    /* renamed from: onEmojiSelected$lambda-5 */
    public static final State m2530onEmojiSelected$lambda5(String str, EditReactionsViewModel editReactionsViewModel, State state) {
        Intrinsics.checkNotNullParameter(str, "$emoji");
        Intrinsics.checkNotNullParameter(editReactionsViewModel, "this$0");
        if (state.getSelection() == -1) {
            return state;
        }
        int size = state.getReactions().size();
        int selection = state.getSelection();
        if (!(selection >= 0 && selection < size)) {
            return state;
        }
        if (!Intrinsics.areEqual(str, EmojiUtil.getCanonicalRepresentation(str))) {
            editReactionsViewModel.emojiValues.setPreferredVariation(str);
        }
        String preferredVariation = editReactionsViewModel.emojiValues.getPreferredVariation(str);
        Intrinsics.checkNotNullExpressionValue(preferredVariation, "emojiValues.getPreferredVariation(emoji)");
        List list = CollectionsKt___CollectionsKt.toMutableList((Collection) state.getReactions());
        list.set(state.getSelection(), preferredVariation);
        Intrinsics.checkNotNullExpressionValue(state, "state");
        return State.copy$default(state, 0, list, 1, null);
    }

    /* renamed from: resetToDefaults$lambda-6 */
    public static final State m2532resetToDefaults$lambda6(State state) {
        Intrinsics.checkNotNullExpressionValue(state, "it");
        List<String> list = EmojiValues.DEFAULT_REACTIONS_LIST;
        Intrinsics.checkNotNullExpressionValue(list, "DEFAULT_REACTIONS_LIST");
        return State.copy$default(state, 0, list, 1, null);
    }

    public final void resetToDefaults() {
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return EditReactionsViewModel.m2532resetToDefaults$lambda6((EditReactionsViewModel.State) obj);
            }
        });
    }

    public final void save() {
        this.emojiValues.setReactions(this.store.getState().getReactions());
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsViewModel$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                EditReactionsViewModel.m2533save$lambda7();
            }
        });
    }

    /* renamed from: save$lambda-7 */
    public static final void m2533save$lambda7() {
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id, "self().id");
        recipients.markNeedsSync(id);
        StorageSyncHelper.scheduleSyncForDataChange();
    }

    /* compiled from: EditReactionsViewModel.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/reactions/edit/EditReactionsViewModel$Companion;", "", "()V", "NO_SELECTION", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* compiled from: EditReactionsViewModel.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0006HÖ\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/reactions/edit/EditReactionsViewModel$State;", "", "selection", "", "reactions", "", "", "(ILjava/util/List;)V", "getReactions", "()Ljava/util/List;", "getSelection", "()I", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class State {
        private final List<String> reactions;
        private final int selection;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.reactions.edit.EditReactionsViewModel$State */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ State copy$default(State state, int i, List list, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = state.selection;
            }
            if ((i2 & 2) != 0) {
                list = state.reactions;
            }
            return state.copy(i, list);
        }

        public final int component1() {
            return this.selection;
        }

        public final List<String> component2() {
            return this.reactions;
        }

        public final State copy(int i, List<String> list) {
            Intrinsics.checkNotNullParameter(list, "reactions");
            return new State(i, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof State)) {
                return false;
            }
            State state = (State) obj;
            return this.selection == state.selection && Intrinsics.areEqual(this.reactions, state.reactions);
        }

        public int hashCode() {
            return (this.selection * 31) + this.reactions.hashCode();
        }

        public String toString() {
            return "State(selection=" + this.selection + ", reactions=" + this.reactions + ')';
        }

        public State(int i, List<String> list) {
            Intrinsics.checkNotNullParameter(list, "reactions");
            this.selection = i;
            this.reactions = list;
        }

        public /* synthetic */ State(int i, List list, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this((i2 & 1) != 0 ? -1 : i, list);
        }

        public final List<String> getReactions() {
            return this.reactions;
        }

        public final int getSelection() {
            return this.selection;
        }
    }
}
