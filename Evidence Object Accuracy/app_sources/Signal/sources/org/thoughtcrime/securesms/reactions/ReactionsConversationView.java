package org.thoughtcrime.securesms.reactions;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiFunction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiImageView;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.reactions.ReactionsConversationView;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class ReactionsConversationView extends LinearLayout {
    private static final int OUTER_MARGIN = ViewUtil.dpToPx(5);
    private int bubbleWidth;
    private boolean outgoing;
    private List<ReactionRecord> records;

    public ReactionsConversationView(Context context) {
        super(context);
        init(null);
    }

    public ReactionsConversationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        this.records = new ArrayList();
        if (attributeSet != null) {
            this.outgoing = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.ReactionsConversationView, 0, 0).getBoolean(0, false);
        }
    }

    public void clear() {
        this.records.clear();
        this.bubbleWidth = 0;
        removeAllViews();
    }

    public void setReactions(List<ReactionRecord> list, int i) {
        if (!list.equals(this.records) || this.bubbleWidth != i) {
            this.records.clear();
            this.records.addAll(list);
            this.bubbleWidth = i;
            List<Reaction> buildSortedReactionsList = buildSortedReactionsList(list);
            removeAllViews();
            Iterator<Reaction> it = buildSortedReactionsList.iterator();
            while (true) {
                int i2 = 0;
                if (!it.hasNext()) {
                    break;
                }
                View buildPill = buildPill(getContext(), this, it.next());
                if (i == 0) {
                    i2 = 4;
                }
                buildPill.setVisibility(i2);
                addView(buildPill);
            }
            measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            int measuredWidth = getMeasuredWidth();
            int i3 = OUTER_MARGIN;
            if (measuredWidth < i - i3) {
                int i4 = (i - measuredWidth) - i3;
                if (this.outgoing) {
                    ViewUtil.setRightMargin(this, i4);
                } else {
                    ViewUtil.setLeftMargin(this, i4);
                }
            } else if (this.outgoing) {
                ViewUtil.setRightMargin(this, i3);
            } else {
                ViewUtil.setLeftMargin(this, i3);
            }
        }
    }

    private static List<Reaction> buildSortedReactionsList(List<ReactionRecord> list) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        RecipientId id = Recipient.self().getId();
        for (ReactionRecord reactionRecord : list) {
            String canonicalRepresentation = EmojiUtil.getCanonicalRepresentation(reactionRecord.getEmoji());
            Reaction reaction = (Reaction) linkedHashMap.get(canonicalRepresentation);
            if (reaction == null) {
                reaction = new Reaction(canonicalRepresentation, reactionRecord.getEmoji(), 1, reactionRecord.getDateReceived(), id.equals(reactionRecord.getAuthor()));
            } else {
                reaction.update(reactionRecord.getEmoji(), reactionRecord.getDateReceived(), id.equals(reactionRecord.getAuthor()));
            }
            linkedHashMap.put(canonicalRepresentation, reaction);
        }
        ArrayList arrayList = new ArrayList(linkedHashMap.values());
        Collections.sort(arrayList, Collections.reverseOrder());
        if (arrayList.size() <= 3) {
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList(3);
        arrayList2.add((Reaction) arrayList.get(0));
        arrayList2.add((Reaction) arrayList.get(1));
        arrayList2.add((Reaction) Stream.of(arrayList).skip(2).reduce(new Reaction(null, null, 0, 0, false), new BiFunction() { // from class: org.thoughtcrime.securesms.reactions.ReactionsConversationView$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return ((ReactionsConversationView.Reaction) obj).merge((ReactionsConversationView.Reaction) obj2);
            }
        }));
        return arrayList2;
    }

    private static View buildPill(Context context, ViewGroup viewGroup, Reaction reaction) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.reactions_pill, viewGroup, false);
        EmojiImageView emojiImageView = (EmojiImageView) inflate.findViewById(R.id.reactions_pill_emoji);
        TextView textView = (TextView) inflate.findViewById(R.id.reactions_pill_count);
        View findViewById = inflate.findViewById(R.id.reactions_pill_spacer);
        if (reaction.displayEmoji != null) {
            emojiImageView.setImageEmoji(reaction.displayEmoji);
            if (reaction.count > 1) {
                textView.setText(String.valueOf(reaction.count));
            } else {
                textView.setVisibility(8);
                findViewById.setVisibility(8);
            }
        } else {
            emojiImageView.setVisibility(8);
            findViewById.setVisibility(8);
            textView.setText(context.getString(R.string.ReactionsConversationView_plus, Integer.valueOf(reaction.count)));
        }
        if (reaction.userWasSender) {
            inflate.setBackground(ContextCompat.getDrawable(context, R.drawable.reaction_pill_background_selected));
            textView.setTextColor(ContextCompat.getColor(context, R.color.reactions_pill_selected_text_color));
        } else {
            inflate.setBackground(ContextCompat.getDrawable(context, R.drawable.reaction_pill_background));
        }
        return inflate;
    }

    /* loaded from: classes4.dex */
    public static class Reaction implements Comparable<Reaction> {
        private String baseEmoji;
        private int count;
        private String displayEmoji;
        private long lastSeen;
        private boolean userWasSender;

        Reaction(String str, String str2, int i, long j, boolean z) {
            this.baseEmoji = str;
            this.displayEmoji = str2;
            this.count = i;
            this.lastSeen = j;
            this.userWasSender = z;
        }

        void update(String str, long j, boolean z) {
            if (!this.userWasSender && (z || j > this.lastSeen)) {
                this.displayEmoji = str;
            }
            boolean z2 = true;
            this.count++;
            this.lastSeen = Math.max(this.lastSeen, j);
            if (!this.userWasSender && !z) {
                z2 = false;
            }
            this.userWasSender = z2;
        }

        public Reaction merge(Reaction reaction) {
            this.count += reaction.count;
            this.lastSeen = Math.max(this.lastSeen, reaction.lastSeen);
            this.userWasSender = this.userWasSender || reaction.userWasSender;
            return this;
        }

        public int compareTo(Reaction reaction) {
            int i = this.count;
            int i2 = reaction.count;
            if (i != i2) {
                return Integer.compare(i, i2);
            }
            return Long.compare(this.lastSeen, reaction.lastSeen);
        }
    }
}
