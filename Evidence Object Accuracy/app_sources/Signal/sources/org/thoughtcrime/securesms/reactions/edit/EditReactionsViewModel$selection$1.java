package org.thoughtcrime.securesms.reactions.edit;

import kotlin.Metadata;
import kotlin.jvm.internal.PropertyReference1Impl;
import org.thoughtcrime.securesms.reactions.edit.EditReactionsViewModel;

/* compiled from: EditReactionsViewModel.kt */
@Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
/* synthetic */ class EditReactionsViewModel$selection$1 extends PropertyReference1Impl {
    public static final EditReactionsViewModel$selection$1 INSTANCE = new EditReactionsViewModel$selection$1();

    EditReactionsViewModel$selection$1() {
        super(EditReactionsViewModel.State.class, "selection", "getSelection()I", 0);
    }

    @Override // kotlin.jvm.internal.PropertyReference1Impl, kotlin.reflect.KProperty1
    public Object get(Object obj) {
        return Integer.valueOf(((EditReactionsViewModel.State) obj).getSelection());
    }
}
