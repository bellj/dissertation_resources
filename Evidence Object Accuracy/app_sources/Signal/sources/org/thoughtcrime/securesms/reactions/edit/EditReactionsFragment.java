package org.thoughtcrime.securesms.reactions.edit;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.transition.ChangeBounds;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.transitions.AlphaTransition;
import org.thoughtcrime.securesms.components.emoji.EmojiImageView;
import org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: EditReactionsFragment.kt */
@Metadata(d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u0000 \"2\u00020\u00012\u00020\u0002:\u0001\"B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0011\u001a\u00020\u0012H\u0002J\b\u0010\u0013\u001a\u00020\u0012H\u0002J\u0010\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0007H\u0002J\b\u0010\u0016\u001a\u00020\u0012H\u0016J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u001a\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00072\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u001e\u001a\u00020\nH\u0002J\u0010\u0010\u001f\u001a\u00020\u00122\u0006\u0010 \u001a\u00020!H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X.¢\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X.¢\u0006\u0002\n\u0000¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/reactions/edit/EditReactionsFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "Lorg/thoughtcrime/securesms/reactions/any/ReactWithAnyEmojiBottomSheetDialogFragment$Callback;", "()V", "defaultSet", "Landroidx/constraintlayout/widget/ConstraintSet;", "mask", "Landroid/view/View;", "reactionViews", "", "Lorg/thoughtcrime/securesms/components/emoji/EmojiImageView;", "scrubber", "Landroidx/constraintlayout/widget/ConstraintLayout;", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "viewModel", "Lorg/thoughtcrime/securesms/reactions/edit/EditReactionsViewModel;", "configureToolbar", "", "deselectAll", "onEmojiClick", "view", "onReactWithAnyEmojiDialogDismissed", "onReactWithAnyEmojiSelected", "emoji", "", "onViewCreated", "savedInstanceState", "Landroid/os/Bundle;", "select", "emojiImageView", "updateToolbarTopMargin", "topMargin", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EditReactionsFragment extends LoggingFragment implements ReactWithAnyEmojiBottomSheetDialogFragment.Callback {
    public static final Companion Companion = new Companion(null);
    private static final String REACT_SHEET_TAG;
    private ConstraintSet defaultSet;
    private View mask;
    private List<? extends EmojiImageView> reactionViews;
    private ConstraintLayout scrubber;
    private Toolbar toolbar;
    private EditReactionsViewModel viewModel;

    public EditReactionsFragment() {
        super(R.layout.edit_reactions_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        Toolbar toolbar = (Toolbar) findViewById;
        this.toolbar = toolbar;
        EditReactionsViewModel editReactionsViewModel = null;
        if (toolbar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("toolbar");
            toolbar = null;
        }
        toolbar.setTitle(R.string.EditReactionsFragment__customize_reactions);
        Toolbar toolbar2 = this.toolbar;
        if (toolbar2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("toolbar");
            toolbar2 = null;
        }
        toolbar2.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditReactionsFragment.m2522onViewCreated$lambda0(EditReactionsFragment.this, view2);
            }
        });
        configureToolbar();
        List<? extends EmojiImageView> list = CollectionsKt__CollectionsKt.listOf((Object[]) new EmojiImageView[]{(EmojiImageView) view.findViewById(R.id.reaction_1), (EmojiImageView) view.findViewById(R.id.reaction_2), (EmojiImageView) view.findViewById(R.id.reaction_3), (EmojiImageView) view.findViewById(R.id.reaction_4), (EmojiImageView) view.findViewById(R.id.reaction_5), (EmojiImageView) view.findViewById(R.id.reaction_6)});
        this.reactionViews = list;
        if (list == null) {
            Intrinsics.throwUninitializedPropertyAccessException("reactionViews");
            list = null;
        }
        for (EmojiImageView emojiImageView : list) {
            emojiImageView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment$$ExternalSyntheticLambda2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    EditReactionsFragment.this.onEmojiClick(view2);
                }
            });
        }
        View findViewById2 = view.findViewById(R.id.edit_reactions_fragment_scrubber);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.e…ctions_fragment_scrubber)");
        this.scrubber = (ConstraintLayout) findViewById2;
        ConstraintSet constraintSet = new ConstraintSet();
        ConstraintLayout constraintLayout = this.scrubber;
        if (constraintLayout == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scrubber");
            constraintLayout = null;
        }
        constraintSet.clone(constraintLayout);
        this.defaultSet = constraintSet;
        View findViewById3 = view.findViewById(R.id.edit_reactions_fragment_reaction_mask);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.e…s_fragment_reaction_mask)");
        this.mask = findViewById3;
        view.findViewById(R.id.edit_reactions_reset_emoji).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditReactionsFragment.m2523onViewCreated$lambda3(EditReactionsFragment.this, view2);
            }
        });
        view.findViewById(R.id.edit_reactions_fragment_save).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditReactionsFragment.m2524onViewCreated$lambda4(EditReactionsFragment.this, view2);
            }
        });
        ViewModel viewModel = new ViewModelProvider(this).get(EditReactionsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this).…onsViewModel::class.java)");
        EditReactionsViewModel editReactionsViewModel2 = (EditReactionsViewModel) viewModel;
        this.viewModel = editReactionsViewModel2;
        if (editReactionsViewModel2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            editReactionsViewModel2 = null;
        }
        editReactionsViewModel2.getReactions().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditReactionsFragment.m2525onViewCreated$lambda6(EditReactionsFragment.this, (List) obj);
            }
        });
        EditReactionsViewModel editReactionsViewModel3 = this.viewModel;
        if (editReactionsViewModel3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        } else {
            editReactionsViewModel = editReactionsViewModel3;
        }
        editReactionsViewModel.getSelection().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditReactionsFragment.m2526onViewCreated$lambda7(EditReactionsFragment.this, (Integer) obj);
            }
        });
        view.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditReactionsFragment.m2527onViewCreated$lambda8(EditReactionsFragment.this, view2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2522onViewCreated$lambda0(EditReactionsFragment editReactionsFragment, View view) {
        Intrinsics.checkNotNullParameter(editReactionsFragment, "this$0");
        editReactionsFragment.requireActivity().onBackPressed();
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2523onViewCreated$lambda3(EditReactionsFragment editReactionsFragment, View view) {
        Intrinsics.checkNotNullParameter(editReactionsFragment, "this$0");
        EditReactionsViewModel editReactionsViewModel = editReactionsFragment.viewModel;
        if (editReactionsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            editReactionsViewModel = null;
        }
        editReactionsViewModel.resetToDefaults();
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m2524onViewCreated$lambda4(EditReactionsFragment editReactionsFragment, View view) {
        Intrinsics.checkNotNullParameter(editReactionsFragment, "this$0");
        EditReactionsViewModel editReactionsViewModel = editReactionsFragment.viewModel;
        if (editReactionsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            editReactionsViewModel = null;
        }
        editReactionsViewModel.save();
        editReactionsFragment.requireActivity().onBackPressed();
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m2525onViewCreated$lambda6(EditReactionsFragment editReactionsFragment, List list) {
        Intrinsics.checkNotNullParameter(editReactionsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(list, "emojis");
        int i = 0;
        for (Object obj : list) {
            i++;
            if (i < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            String str = (String) obj;
            List<? extends EmojiImageView> list2 = editReactionsFragment.reactionViews;
            if (list2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("reactionViews");
                list2 = null;
            }
            ((EmojiImageView) list2.get(i)).setImageEmoji(str);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v2, types: [android.view.View] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: onViewCreated$lambda-7 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m2526onViewCreated$lambda7(org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment r7, java.lang.Integer r8) {
        /*
            java.lang.String r0 = "this$0"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r7, r0)
            java.lang.String r0 = "mask"
            r1 = 0
            r2 = 1
            java.lang.String r3 = "alpha"
            r4 = 0
            if (r8 != 0) goto L_0x000f
            goto L_0x002f
        L_0x000f:
            int r5 = r8.intValue()
            r6 = -1
            if (r5 != r6) goto L_0x002f
            r7.deselectAll()
            android.view.View r7 = r7.mask
            if (r7 != 0) goto L_0x0021
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r0)
            goto L_0x0022
        L_0x0021:
            r4 = r7
        L_0x0022:
            float[] r7 = new float[r2]
            r8 = 0
            r7[r1] = r8
            android.animation.ObjectAnimator r7 = android.animation.ObjectAnimator.ofFloat(r4, r3, r7)
            r7.start()
            goto L_0x006e
        L_0x002f:
            android.view.View r5 = r7.mask
            if (r5 != 0) goto L_0x0037
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r0)
            r5 = r4
        L_0x0037:
            float[] r0 = new float[r2]
            r2 = 1065353216(0x3f800000, float:1.0)
            r0[r1] = r2
            android.animation.ObjectAnimator r0 = android.animation.ObjectAnimator.ofFloat(r5, r3, r0)
            r0.start()
            java.util.List<? extends org.thoughtcrime.securesms.components.emoji.EmojiImageView> r0 = r7.reactionViews
            if (r0 != 0) goto L_0x004e
            java.lang.String r0 = "reactionViews"
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r0)
            goto L_0x004f
        L_0x004e:
            r4 = r0
        L_0x004f:
            java.lang.String r0 = "selection"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r8, r0)
            int r8 = r8.intValue()
            java.lang.Object r8 = r4.get(r8)
            org.thoughtcrime.securesms.components.emoji.EmojiImageView r8 = (org.thoughtcrime.securesms.components.emoji.EmojiImageView) r8
            r7.select(r8)
            androidx.fragment.app.DialogFragment r8 = org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.createForEditReactions()
            androidx.fragment.app.FragmentManager r7 = r7.getChildFragmentManager()
            java.lang.String r0 = "REACT_SHEET_TAG"
            r8.show(r7, r0)
        L_0x006e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment.m2526onViewCreated$lambda7(org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment, java.lang.Integer):void");
    }

    /* renamed from: onViewCreated$lambda-8 */
    public static final void m2527onViewCreated$lambda8(EditReactionsFragment editReactionsFragment, View view) {
        Intrinsics.checkNotNullParameter(editReactionsFragment, "this$0");
        EditReactionsViewModel editReactionsViewModel = editReactionsFragment.viewModel;
        if (editReactionsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            editReactionsViewModel = null;
        }
        editReactionsViewModel.setSelection(-1);
    }

    private final void configureToolbar() {
        Toolbar toolbar = null;
        if (Build.VERSION.SDK_INT == 19) {
            Toolbar toolbar2 = this.toolbar;
            if (toolbar2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("toolbar");
            } else {
                toolbar = toolbar2;
            }
            updateToolbarTopMargin(ViewUtil.getStatusBarHeight(toolbar));
            return;
        }
        Toolbar toolbar3 = this.toolbar;
        if (toolbar3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("toolbar");
        } else {
            toolbar = toolbar3;
        }
        ViewCompat.setOnApplyWindowInsetsListener(toolbar, new OnApplyWindowInsetsListener() { // from class: org.thoughtcrime.securesms.reactions.edit.EditReactionsFragment$$ExternalSyntheticLambda0
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                return EditReactionsFragment.m2521configureToolbar$lambda9(EditReactionsFragment.this, view, windowInsetsCompat);
            }
        });
    }

    /* renamed from: configureToolbar$lambda-9 */
    public static final WindowInsetsCompat m2521configureToolbar$lambda9(EditReactionsFragment editReactionsFragment, View view, WindowInsetsCompat windowInsetsCompat) {
        Intrinsics.checkNotNullParameter(editReactionsFragment, "this$0");
        editReactionsFragment.updateToolbarTopMargin(windowInsetsCompat.getSystemWindowInsetTop());
        return windowInsetsCompat;
    }

    private final void updateToolbarTopMargin(int i) {
        Toolbar toolbar = this.toolbar;
        Toolbar toolbar2 = null;
        if (toolbar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("toolbar");
            toolbar = null;
        }
        ViewGroup.LayoutParams layoutParams = toolbar.getLayoutParams();
        if (layoutParams != null) {
            ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
            ((ViewGroup.MarginLayoutParams) layoutParams2).topMargin = i;
            Toolbar toolbar3 = this.toolbar;
            if (toolbar3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("toolbar");
            } else {
                toolbar2 = toolbar3;
            }
            toolbar2.setLayoutParams(layoutParams2);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
    }

    private final void select(EmojiImageView emojiImageView) {
        ConstraintSet constraintSet = new ConstraintSet();
        ConstraintLayout constraintLayout = this.scrubber;
        ConstraintLayout constraintLayout2 = null;
        if (constraintLayout == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scrubber");
            constraintLayout = null;
        }
        constraintSet.clone(constraintLayout);
        List<? extends EmojiImageView> list = this.reactionViews;
        if (list == null) {
            Intrinsics.throwUninitializedPropertyAccessException("reactionViews");
            list = null;
        }
        for (EmojiImageView emojiImageView2 : list) {
            emojiImageView2.clearAnimation();
            emojiImageView2.setRotation(0.0f);
            if (emojiImageView2.getId() == emojiImageView.getId()) {
                constraintSet.constrainWidth(emojiImageView2.getId(), EditReactionsFragmentKt.SELECTED_SIZE);
                constraintSet.constrainHeight(emojiImageView2.getId(), EditReactionsFragmentKt.SELECTED_SIZE);
                constraintSet.setAlpha(emojiImageView2.getId(), 1.0f);
            } else {
                constraintSet.constrainWidth(emojiImageView2.getId(), EditReactionsFragmentKt.UNSELECTED_SIZE);
                constraintSet.constrainHeight(emojiImageView2.getId(), EditReactionsFragmentKt.UNSELECTED_SIZE);
                constraintSet.setAlpha(emojiImageView2.getId(), 0.3f);
            }
        }
        ConstraintLayout constraintLayout3 = this.scrubber;
        if (constraintLayout3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scrubber");
            constraintLayout3 = null;
        }
        TransitionManager.beginDelayedTransition(constraintLayout3, Companion.createSelectTransitionSet(emojiImageView));
        ConstraintLayout constraintLayout4 = this.scrubber;
        if (constraintLayout4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scrubber");
        } else {
            constraintLayout2 = constraintLayout4;
        }
        constraintSet.applyTo(constraintLayout2);
    }

    private final void deselectAll() {
        List<? extends EmojiImageView> list = this.reactionViews;
        ConstraintLayout constraintLayout = null;
        if (list == null) {
            Intrinsics.throwUninitializedPropertyAccessException("reactionViews");
            list = null;
        }
        for (EmojiImageView emojiImageView : list) {
            emojiImageView.clearAnimation();
        }
        ConstraintLayout constraintLayout2 = this.scrubber;
        if (constraintLayout2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scrubber");
            constraintLayout2 = null;
        }
        TransitionManager.beginDelayedTransition(constraintLayout2, Companion.createTransitionSet());
        ConstraintSet constraintSet = this.defaultSet;
        if (constraintSet == null) {
            Intrinsics.throwUninitializedPropertyAccessException("defaultSet");
            constraintSet = null;
        }
        ConstraintLayout constraintLayout3 = this.scrubber;
        if (constraintLayout3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scrubber");
        } else {
            constraintLayout = constraintLayout3;
        }
        constraintSet.applyTo(constraintLayout);
    }

    public final void onEmojiClick(View view) {
        EditReactionsViewModel editReactionsViewModel = this.viewModel;
        List<? extends EmojiImageView> list = null;
        if (editReactionsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            editReactionsViewModel = null;
        }
        List<? extends EmojiImageView> list2 = this.reactionViews;
        if (list2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("reactionViews");
        } else {
            list = list2;
        }
        editReactionsViewModel.setSelection(CollectionsKt___CollectionsKt.indexOf((List<? extends View>) ((List<? extends Object>) list), view));
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiDialogDismissed() {
        EditReactionsViewModel editReactionsViewModel = this.viewModel;
        if (editReactionsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            editReactionsViewModel = null;
        }
        editReactionsViewModel.setSelection(-1);
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiSelected(String str) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        EditReactionsViewModel editReactionsViewModel = this.viewModel;
        if (editReactionsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            editReactionsViewModel = null;
        }
        editReactionsViewModel.onEmojiSelected(str);
    }

    /* compiled from: EditReactionsFragment.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\b\u0010\t\u001a\u00020\u0006H\u0002J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/reactions/edit/EditReactionsFragment$Companion;", "", "()V", EditReactionsFragment.REACT_SHEET_TAG, "", "createSelectTransitionSet", "Landroidx/transition/Transition;", "target", "Landroid/view/View;", "createTransitionSet", "startRockingAnimation", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Transition createTransitionSet() {
            TransitionSet transitionSet = new TransitionSet();
            transitionSet.setOrdering(0);
            transitionSet.setDuration(250L);
            transitionSet.addTransition(new AlphaTransition());
            transitionSet.addTransition(new ChangeBounds());
            return transitionSet;
        }

        public final Transition createSelectTransitionSet(View view) {
            Transition addListener = createTransitionSet().addListener(new EditReactionsFragment$Companion$createSelectTransitionSet$1(view));
            Intrinsics.checkNotNullExpressionValue(addListener, "target: View): Transitio…ansition) = Unit\n      })");
            return addListener;
        }

        public final void startRockingAnimation(View view) {
            Animation loadAnimation = AnimationUtils.loadAnimation(view.getContext(), R.anim.rock_start);
            Intrinsics.checkNotNullExpressionValue(loadAnimation, "loadAnimation(target.context, R.anim.rock_start)");
            loadAnimation.setAnimationListener(new EditReactionsFragment$Companion$startRockingAnimation$1(view));
            view.clearAnimation();
            view.startAnimation(loadAnimation);
        }
    }
}
