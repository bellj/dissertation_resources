package org.thoughtcrime.securesms.reactions.any;

import android.text.TextUtils;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import j$.util.Collection$EL;
import j$.util.function.Consumer;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.components.emoji.Emoji;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.emoji.EmojiCategory;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageCategoryMappingModel;
import org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchRepository;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.reactions.ReactionDetails;
import org.thoughtcrime.securesms.reactions.ReactionsRepository;
import org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiViewModel;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;

/* loaded from: classes4.dex */
public final class ReactWithAnyEmojiViewModel extends ViewModel {
    private static final int SEARCH_LIMIT;
    private final Observable<MappingModelList> categories;
    private final Observable<MappingModelList> emojiList;
    private final EmojiSearchRepository emojiSearchRepository;
    private final boolean isMms;
    private final long messageId;
    private final ReactWithAnyEmojiRepository repository;
    private final BehaviorSubject<EmojiSearchResult> searchResults;
    private final BehaviorSubject<String> selectedKey;

    private ReactWithAnyEmojiViewModel(ReactWithAnyEmojiRepository reactWithAnyEmojiRepository, long j, boolean z, EmojiSearchRepository emojiSearchRepository) {
        this.repository = reactWithAnyEmojiRepository;
        this.messageId = j;
        this.isMms = z;
        this.emojiSearchRepository = emojiSearchRepository;
        BehaviorSubject<EmojiSearchResult> createDefault = BehaviorSubject.createDefault(new EmojiSearchResult());
        this.searchResults = createDefault;
        BehaviorSubject<String> createDefault2 = BehaviorSubject.createDefault(getStartingKey());
        this.selectedKey = createDefault2;
        Observable<List<ReactionDetails>> reactions = new ReactionsRepository().getReactions(new MessageId(j, z));
        Objects.requireNonNull(reactWithAnyEmojiRepository);
        Observable<R> map = reactions.map(new Function() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ReactWithAnyEmojiRepository.this.getEmojiPageModels((List) obj);
            }
        });
        Observable map2 = map.map(new Function() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiViewModel$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ReactWithAnyEmojiViewModel.lambda$new$0((List) obj);
            }
        });
        this.categories = Observable.combineLatest(map, createDefault2.distinctUntilChanged(), new BiFunction() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiViewModel$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return ReactWithAnyEmojiViewModel.lambda$new$3((List) obj, (String) obj2);
            }
        });
        this.emojiList = Observable.combineLatest(map2, createDefault.distinctUntilChanged(), new BiFunction() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiViewModel$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return ReactWithAnyEmojiViewModel.lambda$new$4((MappingModelList) obj, (ReactWithAnyEmojiViewModel.EmojiSearchResult) obj2);
            }
        });
    }

    public static /* synthetic */ MappingModelList lambda$new$0(List list) throws Throwable {
        MappingModelList mappingModelList = new MappingModelList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ReactWithAnyEmojiPage reactWithAnyEmojiPage = (ReactWithAnyEmojiPage) it.next();
            String key = reactWithAnyEmojiPage.getKey();
            for (ReactWithAnyEmojiPageBlock reactWithAnyEmojiPageBlock : reactWithAnyEmojiPage.getPageBlocks()) {
                mappingModelList.add(new EmojiPageViewGridAdapter.EmojiHeader(key, reactWithAnyEmojiPageBlock.getLabel()));
                mappingModelList.addAll(toMappingModels(reactWithAnyEmojiPageBlock.getPageModel()));
            }
        }
        return mappingModelList;
    }

    public static /* synthetic */ MappingModelList lambda$new$3(List list, String str) throws Throwable {
        MappingModelList mappingModelList = new MappingModelList();
        mappingModelList.add(new EmojiKeyboardPageCategoryMappingModel.RecentsMappingModel(RecentEmojiPageModel.KEY.equals(str)));
        mappingModelList.addAll((Collection) Collection$EL.stream(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiViewModel$$ExternalSyntheticLambda1
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return ReactWithAnyEmojiViewModel.lambda$new$1((ReactWithAnyEmojiPage) obj);
            }
        }).map(new j$.util.function.Function(str) { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ReactWithAnyEmojiViewModel.lambda$new$2(this.f$0, (ReactWithAnyEmojiPage) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList()));
        return mappingModelList;
    }

    public static /* synthetic */ boolean lambda$new$1(ReactWithAnyEmojiPage reactWithAnyEmojiPage) {
        return !RecentEmojiPageModel.KEY.equals(reactWithAnyEmojiPage.getKey());
    }

    public static /* synthetic */ EmojiKeyboardPageCategoryMappingModel.EmojiCategoryMappingModel lambda$new$2(String str, ReactWithAnyEmojiPage reactWithAnyEmojiPage) {
        EmojiCategory forKey = EmojiCategory.forKey(reactWithAnyEmojiPage.getKey());
        return new EmojiKeyboardPageCategoryMappingModel.EmojiCategoryMappingModel(forKey, forKey.getKey().equals(str));
    }

    public static /* synthetic */ MappingModelList lambda$new$4(MappingModelList mappingModelList, EmojiSearchResult emojiSearchResult) throws Throwable {
        if (TextUtils.isEmpty(emojiSearchResult.query)) {
            return mappingModelList;
        }
        if (emojiSearchResult.model.getDisplayEmoji().isEmpty()) {
            return MappingModelList.singleton(new EmojiPageViewGridAdapter.EmojiNoResultsModel());
        }
        return toMappingModels(emojiSearchResult.model);
    }

    public Observable<MappingModelList> getCategories() {
        return this.categories.observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<String> getSelectedKey() {
        return this.selectedKey.observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<MappingModelList> getEmojiList() {
        return this.emojiList.observeOn(AndroidSchedulers.mainThread());
    }

    public void onEmojiSelected(String str) {
        if (this.messageId > 0) {
            SignalStore.emojiValues().setPreferredVariation(str);
            this.repository.addEmojiToMessage(str, new MessageId(this.messageId, this.isMms));
        }
    }

    public /* synthetic */ void lambda$onQueryChanged$5(String str, EmojiPageModel emojiPageModel) {
        this.searchResults.onNext(new EmojiSearchResult(str, emojiPageModel));
    }

    public void onQueryChanged(String str) {
        this.emojiSearchRepository.submitQuery(str, false, 40, new Consumer(str) { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                ReactWithAnyEmojiViewModel.this.lambda$onQueryChanged$5(this.f$1, (EmojiPageModel) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    public void selectPage(String str) {
        this.selectedKey.onNext(str);
    }

    private static MappingModelList toMappingModels(EmojiPageModel emojiPageModel) {
        return (MappingModelList) Collection$EL.stream(emojiPageModel.getDisplayEmoji()).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiViewModel$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ReactWithAnyEmojiViewModel.lambda$toMappingModels$6(EmojiPageModel.this, (Emoji) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(MappingModelList.collect());
    }

    public static /* synthetic */ EmojiPageViewGridAdapter.EmojiModel lambda$toMappingModels$6(EmojiPageModel emojiPageModel, Emoji emoji) {
        return new EmojiPageViewGridAdapter.EmojiModel(emojiPageModel.getKey(), emoji);
    }

    private static String getStartingKey() {
        if (RecentEmojiPageModel.hasRecents(ApplicationDependencies.getApplication(), TextSecurePreferences.RECENT_STORAGE_KEY)) {
            return RecentEmojiPageModel.KEY;
        }
        return EmojiCategory.PEOPLE.getKey();
    }

    /* loaded from: classes4.dex */
    public static class EmojiSearchResult {
        private final EmojiPageModel model;
        private final String query;

        private EmojiSearchResult(String str, EmojiPageModel emojiPageModel) {
            this.query = str;
            this.model = emojiPageModel;
        }

        public EmojiSearchResult() {
            this("", null);
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final boolean isMms;
        private final long messageId;
        private final ReactWithAnyEmojiRepository repository;

        public Factory(ReactWithAnyEmojiRepository reactWithAnyEmojiRepository, long j, boolean z) {
            this.repository = reactWithAnyEmojiRepository;
            this.messageId = j;
            this.isMms = z;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ReactWithAnyEmojiViewModel(this.repository, this.messageId, this.isMms, new EmojiSearchRepository(ApplicationDependencies.getApplication())));
        }
    }
}
