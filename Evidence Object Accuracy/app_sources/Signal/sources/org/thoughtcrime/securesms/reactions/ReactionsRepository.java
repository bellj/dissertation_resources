package org.thoughtcrime.securesms.reactions;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ReactionsRepository.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0002J\u001a\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\t2\u0006\u0010\u0006\u001a\u00020\u0007¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/reactions/ReactionsRepository;", "", "()V", "fetchReactionDetails", "", "Lorg/thoughtcrime/securesms/reactions/ReactionDetails;", "messageId", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "getReactions", "Lio/reactivex/rxjava3/core/Observable;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ReactionsRepository {
    public final Observable<List<ReactionDetails>> getReactions(MessageId messageId) {
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        Observable<List<ReactionDetails>> subscribeOn = Observable.create(new ObservableOnSubscribe(messageId) { // from class: org.thoughtcrime.securesms.reactions.ReactionsRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ MessageId f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                ReactionsRepository.$r8$lambda$fU6vkWysBis7SlfoL85V1xx10ks(ReactionsRepository.this, this.f$1, observableEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create { emitter: Observ…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getReactions$lambda-2 */
    public static final void m2507getReactions$lambda2(ReactionsRepository reactionsRepository, MessageId messageId, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(reactionsRepository, "this$0");
        Intrinsics.checkNotNullParameter(messageId, "$messageId");
        Intrinsics.checkNotNullParameter(observableEmitter, "emitter");
        DatabaseObserver databaseObserver = ApplicationDependencies.getDatabaseObserver();
        Intrinsics.checkNotNullExpressionValue(databaseObserver, "getDatabaseObserver()");
        ReactionsRepository$$ExternalSyntheticLambda0 reactionsRepository$$ExternalSyntheticLambda0 = new DatabaseObserver.MessageObserver(observableEmitter, reactionsRepository) { // from class: org.thoughtcrime.securesms.reactions.ReactionsRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ ObservableEmitter f$1;
            public final /* synthetic */ ReactionsRepository f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.MessageObserver
            public final void onMessageChanged(MessageId messageId2) {
                ReactionsRepository.m2506$r8$lambda$dl32yceYRqkppFwjmx5jMZZPzs(MessageId.this, this.f$1, this.f$2, messageId2);
            }
        };
        databaseObserver.registerMessageUpdateObserver(reactionsRepository$$ExternalSyntheticLambda0);
        observableEmitter.setCancellable(new Cancellable(reactionsRepository$$ExternalSyntheticLambda0) { // from class: org.thoughtcrime.securesms.reactions.ReactionsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ DatabaseObserver.MessageObserver f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                ReactionsRepository.$r8$lambda$ClP3O1UtKdnCp0UgbGfJQx0hL0I(DatabaseObserver.this, this.f$1);
            }
        });
        observableEmitter.onNext(reactionsRepository.fetchReactionDetails(messageId));
    }

    /* renamed from: getReactions$lambda-2$lambda-0 */
    public static final void m2508getReactions$lambda2$lambda0(MessageId messageId, ObservableEmitter observableEmitter, ReactionsRepository reactionsRepository, MessageId messageId2) {
        Intrinsics.checkNotNullParameter(messageId, "$messageId");
        Intrinsics.checkNotNullParameter(observableEmitter, "$emitter");
        Intrinsics.checkNotNullParameter(reactionsRepository, "this$0");
        Intrinsics.checkNotNullParameter(messageId2, "reactionMessageId");
        if (Intrinsics.areEqual(messageId2, messageId)) {
            observableEmitter.onNext(reactionsRepository.fetchReactionDetails(messageId2));
        }
    }

    /* renamed from: getReactions$lambda-2$lambda-1 */
    public static final void m2509getReactions$lambda2$lambda1(DatabaseObserver databaseObserver, DatabaseObserver.MessageObserver messageObserver) {
        Intrinsics.checkNotNullParameter(databaseObserver, "$databaseObserver");
        Intrinsics.checkNotNullParameter(messageObserver, "$messageObserver");
        databaseObserver.unregisterObserver(messageObserver);
    }

    private final List<ReactionDetails> fetchReactionDetails(MessageId messageId) {
        List<ReactionRecord> reactions = SignalDatabase.Companion.reactions().getReactions(messageId);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(reactions, 10));
        for (ReactionRecord reactionRecord : reactions) {
            Recipient resolved = Recipient.resolved(reactionRecord.getAuthor());
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(reaction.author)");
            String canonicalRepresentation = EmojiUtil.getCanonicalRepresentation(reactionRecord.getEmoji());
            Intrinsics.checkNotNullExpressionValue(canonicalRepresentation, "getCanonicalRepresentation(reaction.emoji)");
            arrayList.add(new ReactionDetails(resolved, canonicalRepresentation, reactionRecord.getEmoji(), reactionRecord.getDateReceived()));
        }
        return arrayList;
    }
}
