package org.thoughtcrime.securesms.reactions.any;

import java.util.Objects;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;

/* loaded from: classes4.dex */
public class ReactWithAnyEmojiPageBlock {
    private final int label;
    private final EmojiPageModel pageModel;

    public ReactWithAnyEmojiPageBlock(int i, EmojiPageModel emojiPageModel) {
        this.label = i;
        this.pageModel = emojiPageModel;
    }

    public int getLabel() {
        return this.label;
    }

    public EmojiPageModel getPageModel() {
        return this.pageModel;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ReactWithAnyEmojiPageBlock reactWithAnyEmojiPageBlock = (ReactWithAnyEmojiPageBlock) obj;
        if (this.label == reactWithAnyEmojiPageBlock.label && this.pageModel.getIconAttr() == reactWithAnyEmojiPageBlock.pageModel.getIconAttr() && Objects.equals(this.pageModel.getEmoji(), reactWithAnyEmojiPageBlock.pageModel.getEmoji())) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Objects.hash(Integer.valueOf(this.label), this.pageModel.getEmoji());
    }
}
