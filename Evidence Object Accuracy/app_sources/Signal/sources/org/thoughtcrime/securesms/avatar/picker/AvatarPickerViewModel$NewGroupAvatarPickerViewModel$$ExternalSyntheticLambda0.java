package org.thoughtcrime.securesms.avatar.picker;

import java.util.concurrent.Callable;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class AvatarPickerViewModel$NewGroupAvatarPickerViewModel$$ExternalSyntheticLambda0 implements Callable {
    public final /* synthetic */ AvatarPickerViewModel.NewGroupAvatarPickerViewModel f$0;

    public /* synthetic */ AvatarPickerViewModel$NewGroupAvatarPickerViewModel$$ExternalSyntheticLambda0(AvatarPickerViewModel.NewGroupAvatarPickerViewModel newGroupAvatarPickerViewModel) {
        this.f$0 = newGroupAvatarPickerViewModel;
    }

    @Override // java.util.concurrent.Callable
    public final Object call() {
        return AvatarPickerViewModel.NewGroupAvatarPickerViewModel.m341getAvatar$lambda1(this.f$0);
    }
}
