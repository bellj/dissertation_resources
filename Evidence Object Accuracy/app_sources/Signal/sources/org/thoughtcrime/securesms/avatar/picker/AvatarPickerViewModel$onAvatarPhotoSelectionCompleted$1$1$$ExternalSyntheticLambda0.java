package org.thoughtcrime.securesms.avatar.picker;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$onAvatarPhotoSelectionCompleted$1;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class AvatarPickerViewModel$onAvatarPhotoSelectionCompleted$1$1$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ Avatar f$0;
    public final /* synthetic */ AvatarPickerViewModel f$1;

    public /* synthetic */ AvatarPickerViewModel$onAvatarPhotoSelectionCompleted$1$1$$ExternalSyntheticLambda0(Avatar avatar, AvatarPickerViewModel avatarPickerViewModel) {
        this.f$0 = avatar;
        this.f$1 = avatarPickerViewModel;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return AvatarPickerViewModel$onAvatarPhotoSelectionCompleted$1.AnonymousClass1.m343invoke$lambda0(this.f$0, this.f$1, (AvatarPickerState) obj);
    }
}
