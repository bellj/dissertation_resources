package org.thoughtcrime.securesms.avatar.text;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentKt;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.random.Random;
import org.signal.core.util.EditTextUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.AvatarBundler;
import org.thoughtcrime.securesms.avatar.AvatarColorItem;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerItem;
import org.thoughtcrime.securesms.avatar.text.TextAvatarCreationViewModel;
import org.thoughtcrime.securesms.components.BoldSelectionTabItem;
import org.thoughtcrime.securesms.components.ControllableTabLayout;
import org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout;
import org.thoughtcrime.securesms.components.recyclerview.GridDividerDecoration;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;

/* compiled from: TextAvatarCreationFragment.kt */
@Metadata(d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u001c2\u00020\u0001:\u0002\u001c\u001dB\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\u001a\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationFragment;", "Landroidx/fragment/app/Fragment;", "()V", "content", "Landroidx/constraintlayout/widget/ConstraintLayout;", "hasBoundFromViewModel", "", "recycler", "Landroidx/recyclerview/widget/RecyclerView;", "textInput", "Landroid/widget/EditText;", "viewModel", "Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "withRecyclerSet", "Landroidx/constraintlayout/widget/ConstraintSet;", "withoutRecyclerSet", "createFactory", "Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationViewModel$Factory;", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "OnTabSelectedListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class TextAvatarCreationFragment extends Fragment {
    public static final Companion Companion = new Companion(null);
    public static final String REQUEST_KEY_TEXT;
    private ConstraintLayout content;
    private boolean hasBoundFromViewModel;
    private RecyclerView recycler;
    private EditText textInput;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(TextAvatarCreationViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<TextAvatarCreationViewModel.Factory>(this) { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment$viewModel$2
        @Override // kotlin.jvm.functions.Function0
        public final TextAvatarCreationViewModel.Factory invoke() {
            return TextAvatarCreationFragment.access$createFactory((TextAvatarCreationFragment) this.receiver);
        }
    });
    private final ConstraintSet withRecyclerSet = new ConstraintSet();
    private final ConstraintSet withoutRecyclerSet = new ConstraintSet();

    public TextAvatarCreationFragment() {
        super(R.layout.text_avatar_creation_fragment);
    }

    public final TextAvatarCreationViewModel getViewModel() {
        return (TextAvatarCreationViewModel) this.viewModel$delegate.getValue();
    }

    public final TextAvatarCreationViewModel.Factory createFactory() {
        Avatar.Text text;
        TextAvatarCreationFragmentArgs fromBundle = TextAvatarCreationFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        Bundle textAvatar = fromBundle.getTextAvatar();
        if (textAvatar != null) {
            text = AvatarBundler.INSTANCE.extractText(textAvatar);
        } else {
            text = new Avatar.Text("", (Avatars.ColorPair) CollectionsKt___CollectionsKt.random(Avatars.INSTANCE.getColors(), Random.Default), Avatar.DatabaseId.NotSet.INSTANCE);
        }
        return new TextAvatarCreationViewModel.Factory(text);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.text_avatar_creation_toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.t…_avatar_creation_toolbar)");
        View findViewById2 = view.findViewById(R.id.text_avatar_creation_tabs);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.text_avatar_creation_tabs)");
        ControllableTabLayout controllableTabLayout = (ControllableTabLayout) findViewById2;
        View findViewById3 = view.findViewById(R.id.text_avatar_creation_done);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.text_avatar_creation_done)");
        View findViewById4 = view.findViewById(R.id.keyboard_aware_layout);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.keyboard_aware_layout)");
        KeyboardAwareLinearLayout keyboardAwareLinearLayout = (KeyboardAwareLinearLayout) findViewById4;
        this.withRecyclerSet.load(requireContext(), R.layout.text_avatar_creation_fragment_content);
        this.withoutRecyclerSet.load(requireContext(), R.layout.text_avatar_creation_fragment_content_hidden_recycler);
        View findViewById5 = view.findViewById(R.id.content);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.content)");
        this.content = (ConstraintLayout) findViewById5;
        View findViewById6 = view.findViewById(R.id.text_avatar_creation_recycler);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.t…avatar_creation_recycler)");
        this.recycler = (RecyclerView) findViewById6;
        View findViewById7 = view.findViewById(R.id.avatar_picker_item_text);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.avatar_picker_item_text)");
        this.textInput = (EditText) findViewById7;
        ((Toolbar) findViewById).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TextAvatarCreationFragment.m346$r8$lambda$akWqbO8PSrc8BY4khNUY8alvTQ(view2);
            }
        });
        BoldSelectionTabItem.Companion.registerListeners(controllableTabLayout);
        OnTabSelectedListener onTabSelectedListener = new OnTabSelectedListener();
        controllableTabLayout.addOnTabSelectedListener((TabLayout.OnTabSelectedListener) onTabSelectedListener);
        TabLayout.Tab tabAt = controllableTabLayout.getTabAt(controllableTabLayout.getSelectedTabPosition());
        if (tabAt != null) {
            Intrinsics.checkNotNullExpressionValue(tabAt, "requireNotNull(tabLayout…out.selectedTabPosition))");
            onTabSelectedListener.onTabSelected(tabAt);
            MappingAdapter mappingAdapter = new MappingAdapter();
            RecyclerView recyclerView = this.recycler;
            EditText editText = null;
            if (recyclerView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("recycler");
                recyclerView = null;
            }
            recyclerView.addItemDecoration(new GridDividerDecoration(4, ViewUtil.dpToPx(16)));
            AvatarColorItem.Companion.registerViewHolder(mappingAdapter, new Function1<Avatars.ColorPair, Unit>(this) { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment$onViewCreated$2
                final /* synthetic */ TextAvatarCreationFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Avatars.ColorPair colorPair) {
                    invoke(colorPair);
                    return Unit.INSTANCE;
                }

                public final void invoke(Avatars.ColorPair colorPair) {
                    Intrinsics.checkNotNullParameter(colorPair, "it");
                    TextAvatarCreationFragment.access$getViewModel(this.this$0).setColor(colorPair);
                }
            });
            RecyclerView recyclerView2 = this.recycler;
            if (recyclerView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("recycler");
                recyclerView2 = null;
            }
            recyclerView2.setAdapter(mappingAdapter);
            getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(new AvatarPickerItem.ViewHolder(view, null, null, 6, null), mappingAdapter) { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment$$ExternalSyntheticLambda1
                public final /* synthetic */ AvatarPickerItem.ViewHolder f$1;
                public final /* synthetic */ MappingAdapter f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    TextAvatarCreationFragment.$r8$lambda$nFP64fDh4QbKEFkCCyprJkZPJTM(TextAvatarCreationFragment.this, this.f$1, this.f$2, (TextAvatarCreationState) obj);
                }
            });
            EditText editText2 = this.textInput;
            if (editText2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("textInput");
                editText2 = null;
            }
            EditTextUtil.addGraphemeClusterLimitFilter(editText2, 3);
            EditText editText3 = this.textInput;
            if (editText3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("textInput");
                editText3 = null;
            }
            editText3.addTextChangedListener(new TextWatcher(this) { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment$onViewCreated$$inlined$doAfterTextChanged$1
                final /* synthetic */ TextAvatarCreationFragment this$0;

                @Override // android.text.TextWatcher
                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                @Override // android.text.TextWatcher
                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                {
                    this.this$0 = r1;
                }

                @Override // android.text.TextWatcher
                public void afterTextChanged(Editable editable) {
                    if (editable != null && TextAvatarCreationFragment.access$getHasBoundFromViewModel$p(this.this$0)) {
                        TextAvatarCreationFragment.access$getViewModel(this.this$0).setText(editable.toString());
                    }
                }
            });
            findViewById3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment$$ExternalSyntheticLambda2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TextAvatarCreationFragment.m347$r8$lambda$tfY_oXSu0jAQ7y_j1tmRHCCFfw(TextAvatarCreationFragment.this, view2);
                }
            });
            EditText editText4 = this.textInput;
            if (editText4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("textInput");
            } else {
                editText = editText4;
            }
            editText.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment$$ExternalSyntheticLambda3
                @Override // android.widget.TextView.OnEditorActionListener
                public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    return TextAvatarCreationFragment.m345$r8$lambda$OSqTrJZA47UamoI42uxdWkyiKs(ControllableTabLayout.this, textView, i, keyEvent);
                }
            });
            keyboardAwareLinearLayout.addOnKeyboardHiddenListener(new KeyboardAwareLinearLayout.OnKeyboardHiddenListener(this) { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment$$ExternalSyntheticLambda4
                public final /* synthetic */ TextAvatarCreationFragment f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardHiddenListener
                public final void onKeyboardHidden() {
                    TextAvatarCreationFragment.m344$r8$lambda$NgHsLnGMmpKLA4XCRP5kRfGueE(ControllableTabLayout.this, this.f$1);
                }
            });
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m348onViewCreated$lambda0(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m349onViewCreated$lambda2(TextAvatarCreationFragment textAvatarCreationFragment, AvatarPickerItem.ViewHolder viewHolder, MappingAdapter mappingAdapter, TextAvatarCreationState textAvatarCreationState) {
        Intrinsics.checkNotNullParameter(textAvatarCreationFragment, "this$0");
        Intrinsics.checkNotNullParameter(viewHolder, "$viewHolder");
        Intrinsics.checkNotNullParameter(mappingAdapter, "$adapter");
        EditText editText = textAvatarCreationFragment.textInput;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("textInput");
            editText = null;
        }
        EditTextUtil.setCursorColor(editText, textAvatarCreationState.getCurrentAvatar().getColor().getForegroundColor());
        viewHolder.bind(new AvatarPickerItem.Model(textAvatarCreationState.getCurrentAvatar(), false));
        List<AvatarColorItem> colors = textAvatarCreationState.colors();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(colors, 10));
        for (AvatarColorItem avatarColorItem : colors) {
            arrayList.add(new AvatarColorItem.Model(avatarColorItem));
        }
        mappingAdapter.submitList(arrayList);
        textAvatarCreationFragment.hasBoundFromViewModel = true;
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m350onViewCreated$lambda4(TextAvatarCreationFragment textAvatarCreationFragment, View view) {
        Intrinsics.checkNotNullParameter(textAvatarCreationFragment, "this$0");
        FragmentKt.setFragmentResult(textAvatarCreationFragment, REQUEST_KEY_TEXT, AvatarBundler.INSTANCE.bundleText(textAvatarCreationFragment.getViewModel().getCurrentAvatar()));
        Navigation.findNavController(view).popBackStack();
    }

    /* renamed from: onViewCreated$lambda-5 */
    public static final boolean m351onViewCreated$lambda5(ControllableTabLayout controllableTabLayout, TextView textView, int i, KeyEvent keyEvent) {
        Intrinsics.checkNotNullParameter(controllableTabLayout, "$tabLayout");
        if (i != 5) {
            return false;
        }
        TabLayout.Tab tabAt = controllableTabLayout.getTabAt(1);
        if (tabAt == null) {
            return true;
        }
        tabAt.select();
        return true;
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m352onViewCreated$lambda6(ControllableTabLayout controllableTabLayout, TextAvatarCreationFragment textAvatarCreationFragment) {
        Intrinsics.checkNotNullParameter(controllableTabLayout, "$tabLayout");
        Intrinsics.checkNotNullParameter(textAvatarCreationFragment, "this$0");
        if (controllableTabLayout.getSelectedTabPosition() == 1) {
            TransitionSet startDelay = new AutoTransition().setStartDelay(250L);
            Intrinsics.checkNotNullExpressionValue(startDelay, "AutoTransition().setStartDelay(250L)");
            ConstraintLayout constraintLayout = textAvatarCreationFragment.content;
            ConstraintLayout constraintLayout2 = null;
            if (constraintLayout == null) {
                Intrinsics.throwUninitializedPropertyAccessException("content");
                constraintLayout = null;
            }
            TransitionManager.endTransitions(constraintLayout);
            ConstraintSet constraintSet = textAvatarCreationFragment.withRecyclerSet;
            ConstraintLayout constraintLayout3 = textAvatarCreationFragment.content;
            if (constraintLayout3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("content");
                constraintLayout3 = null;
            }
            constraintSet.applyTo(constraintLayout3);
            ConstraintLayout constraintLayout4 = textAvatarCreationFragment.content;
            if (constraintLayout4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("content");
            } else {
                constraintLayout2 = constraintLayout4;
            }
            TransitionManager.beginDelayedTransition(constraintLayout2, startDelay);
        }
    }

    /* compiled from: TextAvatarCreationFragment.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0012\u0010\b\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationFragment$OnTabSelectedListener;", "Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;", "(Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationFragment;)V", "onTabReselected", "", "tab", "Lcom/google/android/material/tabs/TabLayout$Tab;", "onTabSelected", "onTabUnselected", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    private final class OnTabSelectedListener implements TabLayout.OnTabSelectedListener {
        @Override // com.google.android.material.tabs.TabLayout.BaseOnTabSelectedListener
        public void onTabReselected(TabLayout.Tab tab) {
        }

        @Override // com.google.android.material.tabs.TabLayout.BaseOnTabSelectedListener
        public void onTabUnselected(TabLayout.Tab tab) {
        }

        public OnTabSelectedListener() {
            TextAvatarCreationFragment.this = r1;
        }

        @Override // com.google.android.material.tabs.TabLayout.BaseOnTabSelectedListener
        public void onTabSelected(TabLayout.Tab tab) {
            Intrinsics.checkNotNullParameter(tab, "tab");
            int position = tab.getPosition();
            EditText editText = null;
            if (position == 0) {
                EditText editText2 = TextAvatarCreationFragment.this.textInput;
                if (editText2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("textInput");
                    editText2 = null;
                }
                editText2.setEnabled(true);
                EditText editText3 = TextAvatarCreationFragment.this.textInput;
                if (editText3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("textInput");
                    editText3 = null;
                }
                ViewUtil.focusAndShowKeyboard(editText3);
                ConstraintSet constraintSet = TextAvatarCreationFragment.this.withoutRecyclerSet;
                ConstraintLayout constraintLayout = TextAvatarCreationFragment.this.content;
                if (constraintLayout == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("content");
                    constraintLayout = null;
                }
                constraintSet.applyTo(constraintLayout);
                EditText editText4 = TextAvatarCreationFragment.this.textInput;
                if (editText4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("textInput");
                    editText4 = null;
                }
                EditText editText5 = TextAvatarCreationFragment.this.textInput;
                if (editText5 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("textInput");
                } else {
                    editText = editText5;
                }
                editText4.setSelection(editText.length());
            } else if (position == 1) {
                EditText editText6 = TextAvatarCreationFragment.this.textInput;
                if (editText6 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("textInput");
                    editText6 = null;
                }
                editText6.setEnabled(false);
                Context requireContext = TextAvatarCreationFragment.this.requireContext();
                EditText editText7 = TextAvatarCreationFragment.this.textInput;
                if (editText7 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("textInput");
                } else {
                    editText = editText7;
                }
                ViewUtil.hideKeyboard(requireContext, editText);
            }
        }
    }

    /* compiled from: TextAvatarCreationFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationFragment$Companion;", "", "()V", "REQUEST_KEY_TEXT", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
