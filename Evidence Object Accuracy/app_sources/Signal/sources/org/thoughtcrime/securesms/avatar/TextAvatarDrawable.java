package org.thoughtcrime.securesms.avatar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.SpannableString;
import android.text.StaticLayout;
import android.text.TextPaint;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.components.emoji.EmojiProvider;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiParser;

/* compiled from: TextAvatarDrawable.kt */
@Metadata(d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u0007¢\u0006\u0002\u0010\u000bJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\b\u0010\u0012\u001a\u00020\tH\u0016J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0010\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\tH\u0016J\u0012\u0010\u0019\u001a\u00020\u000f2\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J$\u0010\u000e\u001a\u00020\u000f*\u00020\u001c2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u001e\u001a\u00020\u0014H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/TextAvatarDrawable;", "Landroid/graphics/drawable/Drawable;", "context", "Landroid/content/Context;", "avatar", "Lorg/thoughtcrime/securesms/avatar/Avatar$Text;", "inverted", "", MediaPreviewActivity.SIZE_EXTRA, "", "synchronous", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/avatar/Avatar$Text;ZIZ)V", "textPaint", "Landroid/text/TextPaint;", "draw", "", "canvas", "Landroid/graphics/Canvas;", "getOpacity", "getStartX", "", "layout", "Landroid/text/StaticLayout;", "setAlpha", "alpha", "setColorFilter", "colorFilter", "Landroid/graphics/ColorFilter;", "Landroid/text/Layout;", "x", "y", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class TextAvatarDrawable extends Drawable {
    private final Avatar.Text avatar;
    private final Context context;
    private final int size;
    private final boolean synchronous;
    private final TextPaint textPaint;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -1;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public /* synthetic */ TextAvatarDrawable(Context context, Avatar.Text text, boolean z, int i, boolean z2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, text, (i2 & 4) != 0 ? false : z, (i2 & 8) != 0 ? AvatarRenderer.INSTANCE.getDIMENSIONS() : i, (i2 & 16) != 0 ? false : z2);
    }

    public TextAvatarDrawable(Context context, Avatar.Text text, boolean z, int i, boolean z2) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(text, "avatar");
        this.context = context;
        this.avatar = text;
        this.size = i;
        this.synchronous = z2;
        TextPaint textPaint = new TextPaint(1);
        this.textPaint = textPaint;
        textPaint.setTypeface(AvatarRenderer.INSTANCE.getTypeface(context));
        Avatars.ColorPair color = text.getColor();
        textPaint.setColor(z ? color.getBackgroundColor() : color.getForegroundColor());
        textPaint.density = context.getResources().getDisplayMetrics().density;
        setBounds(0, 0, i, i);
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        CharSequence charSequence;
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        int width = getBounds().width();
        float f = (float) width;
        float textSizeForLength = Avatars.getTextSizeForLength(this.context, this.avatar.getText(), 0.8f * f, f * 0.45f);
        EmojiParser.CandidateList candidates = EmojiProvider.getCandidates(this.avatar.getText());
        this.textPaint.setTextSize(textSizeForLength);
        if (candidates == null || candidates.size() == 0) {
            charSequence = new SpannableString(this.avatar.getText());
        } else {
            charSequence = EmojiProvider.emojify(this.context, candidates, this.avatar.getText(), this.textPaint, this.synchronous, true);
        }
        if (charSequence != null) {
            StaticLayout staticLayout = new StaticLayout(new SpannableString(charSequence), this.textPaint, width, Layout.Alignment.ALIGN_NORMAL, 0.0f, 0.0f, true);
            draw(staticLayout, canvas, getStartX(staticLayout), (float) ((getBounds().height() / 2) - (staticLayout.getHeight() / 2)));
        }
    }

    private final void draw(Layout layout, Canvas canvas, float f, float f2) {
        int save = canvas.save();
        canvas.translate(f, f2);
        try {
            layout.draw(canvas);
        } finally {
            canvas.restoreToCount(save);
        }
    }

    private final float getStartX(StaticLayout staticLayout) {
        int paragraphDirection = staticLayout.getParagraphDirection(0);
        float width = (((float) getBounds().width()) - staticLayout.getLineWidth(0)) / ((float) 2);
        return paragraphDirection == 1 ? width : -width;
    }
}
