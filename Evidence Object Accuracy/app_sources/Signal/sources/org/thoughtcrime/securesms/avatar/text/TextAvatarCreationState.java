package org.thoughtcrime.securesms.avatar.text;

import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.AvatarColorItem;
import org.thoughtcrime.securesms.avatar.Avatars;

/* compiled from: TextAvatarCreationState.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bJ\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationState;", "", "currentAvatar", "Lorg/thoughtcrime/securesms/avatar/Avatar$Text;", "(Lorg/thoughtcrime/securesms/avatar/Avatar$Text;)V", "getCurrentAvatar", "()Lorg/thoughtcrime/securesms/avatar/Avatar$Text;", "colors", "", "Lorg/thoughtcrime/securesms/avatar/AvatarColorItem;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class TextAvatarCreationState {
    private final Avatar.Text currentAvatar;

    public static /* synthetic */ TextAvatarCreationState copy$default(TextAvatarCreationState textAvatarCreationState, Avatar.Text text, int i, Object obj) {
        if ((i & 1) != 0) {
            text = textAvatarCreationState.currentAvatar;
        }
        return textAvatarCreationState.copy(text);
    }

    public final Avatar.Text component1() {
        return this.currentAvatar;
    }

    public final TextAvatarCreationState copy(Avatar.Text text) {
        Intrinsics.checkNotNullParameter(text, "currentAvatar");
        return new TextAvatarCreationState(text);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof TextAvatarCreationState) && Intrinsics.areEqual(this.currentAvatar, ((TextAvatarCreationState) obj).currentAvatar);
    }

    public int hashCode() {
        return this.currentAvatar.hashCode();
    }

    public String toString() {
        return "TextAvatarCreationState(currentAvatar=" + this.currentAvatar + ')';
    }

    public TextAvatarCreationState(Avatar.Text text) {
        Intrinsics.checkNotNullParameter(text, "currentAvatar");
        this.currentAvatar = text;
    }

    public final Avatar.Text getCurrentAvatar() {
        return this.currentAvatar;
    }

    public final List<AvatarColorItem> colors() {
        List<Avatars.ColorPair> colors = Avatars.INSTANCE.getColors();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(colors, 10));
        for (Avatars.ColorPair colorPair : colors) {
            arrayList.add(new AvatarColorItem(colorPair, Intrinsics.areEqual(this.currentAvatar.getColor(), colorPair)));
        }
        return arrayList;
    }
}
