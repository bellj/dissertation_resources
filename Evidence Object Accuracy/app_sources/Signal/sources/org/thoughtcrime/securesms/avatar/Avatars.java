package org.thoughtcrime.securesms.avatar;

import android.content.Context;
import android.graphics.Paint;
import j$.util.Map;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: Avatars.kt */
@Metadata(d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001:\u0003,-.B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J@\u0010\u0015\u001a\u00020\u00162\b\b\u0001\u0010\u0017\u001a\u00020\u00162\b\b\u0001\u0010\u0018\u001a\u00020\u00162\b\b\u0001\u0010\u0019\u001a\u00020\u00162\b\b\u0001\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0005H\u0002J\u0017\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020\u0005H\u0007¢\u0006\u0002\u0010!J\u0010\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0007J,\u0010&\u001a\u00020\u00162\u0006\u0010'\u001a\u00020(2\u0006\u0010\u001d\u001a\u00020\u00052\b\b\u0001\u0010)\u001a\u00020\u00162\b\b\u0001\u0010*\u001a\u00020\u0016H\u0007J\u0010\u0010+\u001a\u00020\u001c2\u0006\u0010'\u001a\u00020(H\u0002R\u001d\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR-\u0010\r\u001a\u001e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000f0\u000ej\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000f`\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R-\u0010\u0013\u001a\u001e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000f0\u000ej\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000f`\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0012¨\u0006/"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatars;", "", "()V", "colorMap", "", "", "Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "getColorMap", "()Ljava/util/Map;", "colors", "", "getColors", "()Ljava/util/List;", "defaultAvatarsForGroup", "Ljava/util/LinkedHashMap;", "Lorg/thoughtcrime/securesms/avatar/Avatars$DefaultAvatar;", "Lkotlin/collections/LinkedHashMap;", "getDefaultAvatarsForGroup", "()Ljava/util/LinkedHashMap;", "defaultAvatarsForSelf", "getDefaultAvatarsForSelf", "branchSizes", "", "lastFontSize", "fontSize", "target", "maxFontSize", "paint", "Landroid/graphics/Paint;", DraftDatabase.Draft.TEXT, "getDrawableResource", "", "key", "(Ljava/lang/String;)Ljava/lang/Integer;", "getForegroundColor", "Lorg/thoughtcrime/securesms/avatar/Avatars$ForegroundColor;", "avatarColor", "Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "getTextSizeForLength", "context", "Landroid/content/Context;", "maxWidth", "maxSize", "textPaint", "ColorPair", "DefaultAvatar", "ForegroundColor", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class Avatars {
    public static final Avatars INSTANCE = new Avatars();
    private static final Map<String, ColorPair> colorMap;
    private static final List<ColorPair> colors;
    private static final LinkedHashMap<String, DefaultAvatar> defaultAvatarsForGroup = MapsKt__MapsKt.linkedMapOf(TuplesKt.to("avatar_heart", new DefaultAvatar(R.drawable.ic_avatar_heart, "A180")), TuplesKt.to("avatar_house", new DefaultAvatar(R.drawable.ic_avatar_house, "A120")), TuplesKt.to("avatar_melon", new DefaultAvatar(R.drawable.ic_avatar_melon, "A110")), TuplesKt.to("avatar_drink", new DefaultAvatar(R.drawable.ic_avatar_drink, "A170")), TuplesKt.to("avatar_celebration", new DefaultAvatar(R.drawable.ic_avatar_celebration, "A100")), TuplesKt.to("avatar_balloon", new DefaultAvatar(R.drawable.ic_avatar_balloon, "A220")), TuplesKt.to("avatar_book", new DefaultAvatar(R.drawable.ic_avatar_book, "A100")), TuplesKt.to("avatar_briefcase", new DefaultAvatar(R.drawable.ic_avatar_briefcase, "A180")), TuplesKt.to("avatar_sunset", new DefaultAvatar(R.drawable.ic_avatar_sunset, "A120")), TuplesKt.to("avatar_surfboard", new DefaultAvatar(R.drawable.ic_avatar_surfboard, "A110")), TuplesKt.to("avatar_soccerball", new DefaultAvatar(R.drawable.ic_avatar_soccerball, "A130")), TuplesKt.to("avatar_football", new DefaultAvatar(R.drawable.ic_avatar_football, "A220")));
    private static final LinkedHashMap<String, DefaultAvatar> defaultAvatarsForSelf = MapsKt__MapsKt.linkedMapOf(TuplesKt.to("avatar_abstract_01", new DefaultAvatar(R.drawable.ic_avatar_abstract_01, "A130")), TuplesKt.to("avatar_abstract_02", new DefaultAvatar(R.drawable.ic_avatar_abstract_02, "A120")), TuplesKt.to("avatar_abstract_03", new DefaultAvatar(R.drawable.ic_avatar_abstract_03, "A170")), TuplesKt.to("avatar_cat", new DefaultAvatar(R.drawable.ic_avatar_cat, "A190")), TuplesKt.to("avatar_dog", new DefaultAvatar(R.drawable.ic_avatar_dog, "A140")), TuplesKt.to("avatar_fox", new DefaultAvatar(R.drawable.ic_avatar_fox, "A190")), TuplesKt.to("avatar_tucan", new DefaultAvatar(R.drawable.ic_avatar_tucan, "A120")), TuplesKt.to("avatar_sloth", new DefaultAvatar(R.drawable.ic_avatar_sloth, "A160")), TuplesKt.to("avatar_dinosaur", new DefaultAvatar(R.drawable.ic_avatar_dinosour, "A130")), TuplesKt.to("avatar_pig", new DefaultAvatar(R.drawable.ic_avatar_pig, "A180")), TuplesKt.to("avatar_incognito", new DefaultAvatar(R.drawable.ic_avatar_incognito, "A220")), TuplesKt.to("avatar_ghost", new DefaultAvatar(R.drawable.ic_avatar_ghost, "A100")));

    private Avatars() {
    }

    /* compiled from: Avatars.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0012\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0019\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0003J\u0006\u0010\n\u001a\u00020\u0003R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatars$ForegroundColor;", "", "code", "", "colorInt", "", "(Ljava/lang/String;ILjava/lang/String;I)V", "getColorInt", "()I", "deserialize", "serialize", "A100", "A110", "A120", "A130", "A140", "A150", "A160", "A170", "A180", "A190", "A200", "A210", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public enum ForegroundColor {
        A100("A100", -13092619),
        A110("A110", -15576621),
        A120("A120", -16224864),
        A130("A130", -16353018),
        A140("A140", -10085633),
        A150("A150", -6356752),
        A160("A160", -4717188),
        A170("A170", -4324348),
        A180("A180", -8164607),
        A190("A190", -8556736),
        A200("A200", -11579539),
        A210("A210", -10724260);
        
        private final String code;
        private final int colorInt;

        ForegroundColor(String str, int i) {
            this.code = str;
            this.colorInt = i;
        }

        public final int getColorInt() {
            return this.colorInt;
        }

        public final ForegroundColor deserialize(String str) {
            ForegroundColor foregroundColor;
            Intrinsics.checkNotNullParameter(str, "code");
            ForegroundColor[] values = values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    foregroundColor = null;
                    break;
                }
                foregroundColor = values[i];
                if (Intrinsics.areEqual(foregroundColor.code, str)) {
                    break;
                }
                i++;
            }
            if (foregroundColor != null) {
                return foregroundColor;
            }
            throw new IllegalArgumentException();
        }

        public final String serialize() {
            return this.code;
        }
    }

    static {
        INSTANCE = new Avatars();
        ForegroundColor[] values = ForegroundColor.values();
        ArrayList arrayList = new ArrayList(values.length);
        for (ForegroundColor foregroundColor : values) {
            AvatarColor deserialize = AvatarColor.deserialize(foregroundColor.serialize());
            Intrinsics.checkNotNullExpressionValue(deserialize, "deserialize(it.serialize())");
            arrayList.add(new ColorPair(deserialize, foregroundColor));
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10)), 16));
        for (Object obj : arrayList) {
            linkedHashMap.put(((ColorPair) obj).getCode(), obj);
        }
        colorMap = linkedHashMap;
        colors = CollectionsKt___CollectionsKt.toList(linkedHashMap.values());
        defaultAvatarsForSelf = MapsKt__MapsKt.linkedMapOf(TuplesKt.to("avatar_abstract_01", new DefaultAvatar(R.drawable.ic_avatar_abstract_01, "A130")), TuplesKt.to("avatar_abstract_02", new DefaultAvatar(R.drawable.ic_avatar_abstract_02, "A120")), TuplesKt.to("avatar_abstract_03", new DefaultAvatar(R.drawable.ic_avatar_abstract_03, "A170")), TuplesKt.to("avatar_cat", new DefaultAvatar(R.drawable.ic_avatar_cat, "A190")), TuplesKt.to("avatar_dog", new DefaultAvatar(R.drawable.ic_avatar_dog, "A140")), TuplesKt.to("avatar_fox", new DefaultAvatar(R.drawable.ic_avatar_fox, "A190")), TuplesKt.to("avatar_tucan", new DefaultAvatar(R.drawable.ic_avatar_tucan, "A120")), TuplesKt.to("avatar_sloth", new DefaultAvatar(R.drawable.ic_avatar_sloth, "A160")), TuplesKt.to("avatar_dinosaur", new DefaultAvatar(R.drawable.ic_avatar_dinosour, "A130")), TuplesKt.to("avatar_pig", new DefaultAvatar(R.drawable.ic_avatar_pig, "A180")), TuplesKt.to("avatar_incognito", new DefaultAvatar(R.drawable.ic_avatar_incognito, "A220")), TuplesKt.to("avatar_ghost", new DefaultAvatar(R.drawable.ic_avatar_ghost, "A100")));
        defaultAvatarsForGroup = MapsKt__MapsKt.linkedMapOf(TuplesKt.to("avatar_heart", new DefaultAvatar(R.drawable.ic_avatar_heart, "A180")), TuplesKt.to("avatar_house", new DefaultAvatar(R.drawable.ic_avatar_house, "A120")), TuplesKt.to("avatar_melon", new DefaultAvatar(R.drawable.ic_avatar_melon, "A110")), TuplesKt.to("avatar_drink", new DefaultAvatar(R.drawable.ic_avatar_drink, "A170")), TuplesKt.to("avatar_celebration", new DefaultAvatar(R.drawable.ic_avatar_celebration, "A100")), TuplesKt.to("avatar_balloon", new DefaultAvatar(R.drawable.ic_avatar_balloon, "A220")), TuplesKt.to("avatar_book", new DefaultAvatar(R.drawable.ic_avatar_book, "A100")), TuplesKt.to("avatar_briefcase", new DefaultAvatar(R.drawable.ic_avatar_briefcase, "A180")), TuplesKt.to("avatar_sunset", new DefaultAvatar(R.drawable.ic_avatar_sunset, "A120")), TuplesKt.to("avatar_surfboard", new DefaultAvatar(R.drawable.ic_avatar_surfboard, "A110")), TuplesKt.to("avatar_soccerball", new DefaultAvatar(R.drawable.ic_avatar_soccerball, "A130")), TuplesKt.to("avatar_football", new DefaultAvatar(R.drawable.ic_avatar_football, "A220")));
    }

    public final Map<String, ColorPair> getColorMap() {
        return colorMap;
    }

    public final List<ColorPair> getColors() {
        return colors;
    }

    public final LinkedHashMap<String, DefaultAvatar> getDefaultAvatarsForSelf() {
        return defaultAvatarsForSelf;
    }

    public final LinkedHashMap<String, DefaultAvatar> getDefaultAvatarsForGroup() {
        return defaultAvatarsForGroup;
    }

    public final Integer getDrawableResource(String str) {
        Intrinsics.checkNotNullParameter(str, "key");
        DefaultAvatar defaultAvatar = (DefaultAvatar) Map.EL.getOrDefault(defaultAvatarsForSelf, str, defaultAvatarsForGroup.get(str));
        if (defaultAvatar != null) {
            return Integer.valueOf(defaultAvatar.getVectorDrawableId());
        }
        return null;
    }

    private final Paint textPaint(Context context) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTypeface(AvatarRenderer.INSTANCE.getTypeface(context));
        paint.setTextSize(1.0f);
        return paint;
    }

    @JvmStatic
    public static final float getTextSizeForLength(Context context, String str, float f, float f2) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(str, DraftDatabase.Draft.TEXT);
        Avatars avatars = INSTANCE;
        return avatars.branchSizes(0.0f, f / ((float) 2), f, f2, avatars.textPaint(context), str);
    }

    private final float branchSizes(float f, float f2, float f3, float f4, Paint paint, String str) {
        paint.setTextSize(f2);
        float measureText = paint.measureText(str);
        float abs = Math.abs(f - f2) / 2.0f;
        boolean z = true;
        boolean z2 = Math.abs(1.0f - (measureText / f3)) <= 0.01f;
        if (measureText == 0.0f) {
            return f4;
        }
        if (abs != 0.0f) {
            z = false;
        }
        if (z) {
            return Math.min(f4, f2);
        }
        if (f2 >= f4) {
            return f4;
        }
        if (z2) {
            return f2;
        }
        if (measureText > f3) {
            return branchSizes(f2, f2 - abs, f3, f4, paint, str);
        }
        return branchSizes(f2, f2 + abs, f3, f4, paint, str);
    }

    @JvmStatic
    public static final ForegroundColor getForegroundColor(AvatarColor avatarColor) {
        ForegroundColor foregroundColor;
        Intrinsics.checkNotNullParameter(avatarColor, "avatarColor");
        ForegroundColor[] values = ForegroundColor.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                foregroundColor = null;
                break;
            }
            foregroundColor = values[i];
            if (Intrinsics.areEqual(foregroundColor.serialize(), avatarColor.serialize())) {
                break;
            }
            i++;
        }
        return foregroundColor == null ? ForegroundColor.A210 : foregroundColor;
    }

    /* compiled from: Avatars.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B\u0017\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatars$DefaultAvatar;", "", "vectorDrawableId", "", "colorCode", "", "(ILjava/lang/String;)V", "getColorCode", "()Ljava/lang/String;", "getVectorDrawableId", "()I", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class DefaultAvatar {
        private final String colorCode;
        private final int vectorDrawableId;

        public static /* synthetic */ DefaultAvatar copy$default(DefaultAvatar defaultAvatar, int i, String str, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = defaultAvatar.vectorDrawableId;
            }
            if ((i2 & 2) != 0) {
                str = defaultAvatar.colorCode;
            }
            return defaultAvatar.copy(i, str);
        }

        public final int component1() {
            return this.vectorDrawableId;
        }

        public final String component2() {
            return this.colorCode;
        }

        public final DefaultAvatar copy(int i, String str) {
            Intrinsics.checkNotNullParameter(str, "colorCode");
            return new DefaultAvatar(i, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DefaultAvatar)) {
                return false;
            }
            DefaultAvatar defaultAvatar = (DefaultAvatar) obj;
            return this.vectorDrawableId == defaultAvatar.vectorDrawableId && Intrinsics.areEqual(this.colorCode, defaultAvatar.colorCode);
        }

        public int hashCode() {
            return (this.vectorDrawableId * 31) + this.colorCode.hashCode();
        }

        public String toString() {
            return "DefaultAvatar(vectorDrawableId=" + this.vectorDrawableId + ", colorCode=" + this.colorCode + ')';
        }

        public DefaultAvatar(int i, String str) {
            Intrinsics.checkNotNullParameter(str, "colorCode");
            this.vectorDrawableId = i;
            this.colorCode = str;
        }

        public final int getVectorDrawableId() {
            return this.vectorDrawableId;
        }

        public final String getColorCode() {
            return this.colorCode;
        }
    }

    /* compiled from: Avatars.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B!\u0012\b\b\u0001\u0010\u0007\u001a\u00020\b\u0012\b\b\u0001\u0010\t\u001a\u00020\b\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\t\u0010\u0012\u001a\u00020\bHÆ\u0003J\t\u0010\u0013\u001a\u00020\bHÆ\u0003J\t\u0010\u0014\u001a\u00020\u000bHÆ\u0003J'\u0010\u0015\u001a\u00020\u00002\b\b\u0003\u0010\u0007\u001a\u00020\b2\b\b\u0003\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\n\u001a\u00020\u000bHÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\bHÖ\u0001J\t\u0010\u001a\u001a\u00020\u000bHÖ\u0001R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000e¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "", "backgroundAvatarColor", "Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "foregroundAvatarColor", "Lorg/thoughtcrime/securesms/avatar/Avatars$ForegroundColor;", "(Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;Lorg/thoughtcrime/securesms/avatar/Avatars$ForegroundColor;)V", "backgroundColor", "", "foregroundColor", "code", "", "(IILjava/lang/String;)V", "getBackgroundColor", "()I", "getCode", "()Ljava/lang/String;", "getForegroundColor", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class ColorPair {
        private final int backgroundColor;
        private final String code;
        private final int foregroundColor;

        public static /* synthetic */ ColorPair copy$default(ColorPair colorPair, int i, int i2, String str, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = colorPair.backgroundColor;
            }
            if ((i3 & 2) != 0) {
                i2 = colorPair.foregroundColor;
            }
            if ((i3 & 4) != 0) {
                str = colorPair.code;
            }
            return colorPair.copy(i, i2, str);
        }

        public final int component1() {
            return this.backgroundColor;
        }

        public final int component2() {
            return this.foregroundColor;
        }

        public final String component3() {
            return this.code;
        }

        public final ColorPair copy(int i, int i2, String str) {
            Intrinsics.checkNotNullParameter(str, "code");
            return new ColorPair(i, i2, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ColorPair)) {
                return false;
            }
            ColorPair colorPair = (ColorPair) obj;
            return this.backgroundColor == colorPair.backgroundColor && this.foregroundColor == colorPair.foregroundColor && Intrinsics.areEqual(this.code, colorPair.code);
        }

        public int hashCode() {
            return (((this.backgroundColor * 31) + this.foregroundColor) * 31) + this.code.hashCode();
        }

        public String toString() {
            return "ColorPair(backgroundColor=" + this.backgroundColor + ", foregroundColor=" + this.foregroundColor + ", code=" + this.code + ')';
        }

        public ColorPair(int i, int i2, String str) {
            Intrinsics.checkNotNullParameter(str, "code");
            this.backgroundColor = i;
            this.foregroundColor = i2;
            this.code = str;
        }

        public final int getBackgroundColor() {
            return this.backgroundColor;
        }

        public final int getForegroundColor() {
            return this.foregroundColor;
        }

        public final String getCode() {
            return this.code;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public ColorPair(org.thoughtcrime.securesms.conversation.colors.AvatarColor r3, org.thoughtcrime.securesms.avatar.Avatars.ForegroundColor r4) {
            /*
                r2 = this;
                java.lang.String r0 = "backgroundAvatarColor"
                kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r0)
                java.lang.String r0 = "foregroundAvatarColor"
                kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r4, r0)
                int r0 = r3.colorInt()
                int r4 = r4.getColorInt()
                java.lang.String r3 = r3.serialize()
                java.lang.String r1 = "backgroundAvatarColor.serialize()"
                kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r3, r1)
                r2.<init>(r0, r4, r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.avatar.Avatars.ColorPair.<init>(org.thoughtcrime.securesms.conversation.colors.AvatarColor, org.thoughtcrime.securesms.avatar.Avatars$ForegroundColor):void");
        }
    }
}
