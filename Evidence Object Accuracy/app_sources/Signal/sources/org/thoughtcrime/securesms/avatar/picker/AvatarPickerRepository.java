package org.thoughtcrime.securesms.avatar.picker;

import android.content.Context;
import android.net.Uri;
import android.widget.Toast;
import io.reactivex.rxjava3.core.Single;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.AvatarPickerStorage;
import org.thoughtcrime.securesms.avatar.AvatarRenderer;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.AvatarPickerDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.NameUtil;
import org.whispersystems.signalservice.api.util.StreamDetails;

/* compiled from: AvatarPickerRepository.kt */
@Metadata(d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0010\u0003\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\"\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\b0\fJ\u001c\u0010\u000e\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0010J\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u0012J\u0010\u0010\u0016\u001a\u00020\n2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018J\u000e\u0010\u0016\u001a\u00020\n2\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0019\u001a\u00020\nJ\u0012\u0010\u001a\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u001b0\u0012J\u0012\u0010\u001c\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u001b0\u0012J\u001a\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u001b0\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u0012\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u001b0\u0012J\u0010\u0010\u001f\u001a\u00020\b2\b\u0010 \u001a\u0004\u0018\u00010!J*\u0010\"\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0013\u001a\u00020\u00142\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\b0\fJ\"\u0010#\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\b0\fJ*\u0010$\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0013\u001a\u00020\u00142\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\b0\fJ\"\u0010&\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\b0\fJ\"\u0010'\u001a\u00020\b2\u0006\u0010(\u001a\u00020\r2\u0012\u0010)\u001a\u000e\u0012\u0004\u0012\u00020*\u0012\u0004\u0012\u00020\b0\fR\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00030\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "applicationContext", "kotlin.jvm.PlatformType", "createMediaForNewGroup", "", "avatar", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "onSaved", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "delete", "onDelete", "Lkotlin/Function0;", "getAvatarForGroup", "Lio/reactivex/rxjava3/core/Single;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "getAvatarForSelf", "getDefaultAvatarForGroup", NotificationProfileDatabase.NotificationProfileTable.COLOR, "Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "getDefaultAvatarForSelf", "getDefaultAvatarsForGroup", "", "getDefaultAvatarsForSelf", "getPersistedAvatarsForGroup", "getPersistedAvatarsForSelf", "handleRenderFailure", "throwable", "", "persistAndCreateMediaForGroup", "persistAndCreateMediaForSelf", "persistAvatarForGroup", "onPersisted", "persistAvatarForSelf", "writeMediaToMultiSessionStorage", "media", "onMediaWrittenToMultiSessionStorage", "Landroid/net/Uri;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarPickerRepository {
    private final Context applicationContext;

    public AvatarPickerRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.applicationContext = context.getApplicationContext();
    }

    public final Single<Avatar> getAvatarForSelf() {
        Single<Avatar> fromCallable = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda7
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return AvatarPickerRepository.m320getAvatarForSelf$lambda0(AvatarPickerRepository.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n    val d…AvatarForSelf()\n    }\n  }");
        return fromCallable;
    }

    /* renamed from: getAvatarForSelf$lambda-0 */
    public static final Avatar m320getAvatarForSelf$lambda0(AvatarPickerRepository avatarPickerRepository) {
        Intrinsics.checkNotNullParameter(avatarPickerRepository, "this$0");
        StreamDetails selfProfileAvatarStream = AvatarHelper.getSelfProfileAvatarStream(avatarPickerRepository.applicationContext);
        if (selfProfileAvatarStream == null) {
            return avatarPickerRepository.getDefaultAvatarForSelf();
        }
        try {
            Uri createForSingleSessionInMemory = BlobProvider.getInstance().forData(StreamUtil.readFully(selfProfileAvatarStream.getStream())).createForSingleSessionInMemory();
            Intrinsics.checkNotNullExpressionValue(createForSingleSessionInMemory, "getInstance().forData(by…orSingleSessionInMemory()");
            return new Avatar.Photo(createForSingleSessionInMemory, selfProfileAvatarStream.getLength(), Avatar.DatabaseId.DoNotPersist.INSTANCE);
        } catch (IOException unused) {
            Log.w(AvatarPickerRepositoryKt.TAG, "Failed to read avatar!");
            return avatarPickerRepository.getDefaultAvatarForSelf();
        }
    }

    public final Single<Avatar> getAvatarForGroup(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Single<Avatar> fromCallable = Single.fromCallable(new Callable(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ AvatarPickerRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return AvatarPickerRepository.m319getAvatarForGroup$lambda1(GroupId.this, this.f$1);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n    val r…nt.avatarColor)\n    }\n  }");
        return fromCallable;
    }

    /* renamed from: getAvatarForGroup$lambda-1 */
    public static final Avatar m319getAvatarForGroup$lambda1(GroupId groupId, AvatarPickerRepository avatarPickerRepository) {
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Intrinsics.checkNotNullParameter(avatarPickerRepository, "this$0");
        Recipient externalGroupExact = Recipient.externalGroupExact(groupId);
        Intrinsics.checkNotNullExpressionValue(externalGroupExact, "externalGroupExact(groupId)");
        if (!AvatarHelper.hasAvatar(avatarPickerRepository.applicationContext, externalGroupExact.getId())) {
            return avatarPickerRepository.getDefaultAvatarForGroup(externalGroupExact.getAvatarColor());
        }
        try {
            Uri createForSingleSessionInMemory = BlobProvider.getInstance().forData(AvatarHelper.getAvatarBytes(avatarPickerRepository.applicationContext, externalGroupExact.getId())).createForSingleSessionInMemory();
            Intrinsics.checkNotNullExpressionValue(createForSingleSessionInMemory, "getInstance().forData(by…orSingleSessionInMemory()");
            return new Avatar.Photo(createForSingleSessionInMemory, AvatarHelper.getAvatarLength(avatarPickerRepository.applicationContext, externalGroupExact.getId()), Avatar.DatabaseId.DoNotPersist.INSTANCE);
        } catch (IOException unused) {
            Log.w(AvatarPickerRepositoryKt.TAG, "Failed to read group avatar!");
            return avatarPickerRepository.getDefaultAvatarForGroup(externalGroupExact.getAvatarColor());
        }
    }

    public final Single<List<Avatar>> getPersistedAvatarsForSelf() {
        Single<List<Avatar>> fromCallable = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda4
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return AvatarPickerRepository.m324getPersistedAvatarsForSelf$lambda2();
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n    Signa…r.getAvatarsForSelf()\n  }");
        return fromCallable;
    }

    /* renamed from: getPersistedAvatarsForSelf$lambda-2 */
    public static final List m324getPersistedAvatarsForSelf$lambda2() {
        return SignalDatabase.Companion.avatarPicker().getAvatarsForSelf();
    }

    public final Single<List<Avatar>> getPersistedAvatarsForGroup(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Single<List<Avatar>> fromCallable = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda0
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return AvatarPickerRepository.m323getPersistedAvatarsForGroup$lambda3(GroupId.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n    Signa…tarsForGroup(groupId)\n  }");
        return fromCallable;
    }

    /* renamed from: getPersistedAvatarsForGroup$lambda-3 */
    public static final List m323getPersistedAvatarsForGroup$lambda3(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        return SignalDatabase.Companion.avatarPicker().getAvatarsForGroup(groupId);
    }

    public final Single<List<Avatar>> getDefaultAvatarsForSelf() {
        Single<List<Avatar>> fromCallable = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda13
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return AvatarPickerRepository.m322getDefaultAvatarsForSelf$lambda5();
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n    Avata…abaseId.NotSet)\n    }\n  }");
        return fromCallable;
    }

    /* renamed from: getDefaultAvatarsForSelf$lambda-5 */
    public static final List m322getDefaultAvatarsForSelf$lambda5() {
        Set<Map.Entry<String, Avatars.DefaultAvatar>> entrySet = Avatars.INSTANCE.getDefaultAvatarsForSelf().entrySet();
        Intrinsics.checkNotNullExpressionValue(entrySet, "Avatars.defaultAvatarsForSelf.entries");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(entrySet, 10));
        int i = 0;
        for (Object obj : entrySet) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            Object key = ((Map.Entry) obj).getKey();
            Intrinsics.checkNotNullExpressionValue(key, "entry.key");
            Avatars avatars = Avatars.INSTANCE;
            arrayList.add(new Avatar.Vector((String) key, avatars.getColors().get(i % avatars.getColors().size()), Avatar.DatabaseId.NotSet.INSTANCE));
            i = i2;
        }
        return arrayList;
    }

    public final Single<List<Avatar>> getDefaultAvatarsForGroup() {
        Single<List<Avatar>> fromCallable = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda12
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return AvatarPickerRepository.m321getDefaultAvatarsForGroup$lambda7();
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n    Avata…abaseId.NotSet)\n    }\n  }");
        return fromCallable;
    }

    /* renamed from: getDefaultAvatarsForGroup$lambda-7 */
    public static final List m321getDefaultAvatarsForGroup$lambda7() {
        Set<Map.Entry<String, Avatars.DefaultAvatar>> entrySet = Avatars.INSTANCE.getDefaultAvatarsForGroup().entrySet();
        Intrinsics.checkNotNullExpressionValue(entrySet, "Avatars.defaultAvatarsForGroup.entries");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(entrySet, 10));
        int i = 0;
        for (Object obj : entrySet) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            Object key = ((Map.Entry) obj).getKey();
            Intrinsics.checkNotNullExpressionValue(key, "entry.key");
            Avatars avatars = Avatars.INSTANCE;
            arrayList.add(new Avatar.Vector((String) key, avatars.getColors().get(i % avatars.getColors().size()), Avatar.DatabaseId.NotSet.INSTANCE));
            i = i2;
        }
        return arrayList;
    }

    public final void writeMediaToMultiSessionStorage(Media media, Function1<? super Uri, Unit> function1) {
        Intrinsics.checkNotNullParameter(media, "media");
        Intrinsics.checkNotNullParameter(function1, "onMediaWrittenToMultiSessionStorage");
        SignalExecutors.BOUNDED.execute(new Runnable(this, media) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ AvatarPickerRepository f$1;
            public final /* synthetic */ Media f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerRepository.m330writeMediaToMultiSessionStorage$lambda8(Function1.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: writeMediaToMultiSessionStorage$lambda-8 */
    public static final void m330writeMediaToMultiSessionStorage$lambda8(Function1 function1, AvatarPickerRepository avatarPickerRepository, Media media) {
        Intrinsics.checkNotNullParameter(function1, "$onMediaWrittenToMultiSessionStorage");
        Intrinsics.checkNotNullParameter(avatarPickerRepository, "this$0");
        Intrinsics.checkNotNullParameter(media, "$media");
        AvatarPickerStorage avatarPickerStorage = AvatarPickerStorage.INSTANCE;
        Context context = avatarPickerRepository.applicationContext;
        Intrinsics.checkNotNullExpressionValue(context, "applicationContext");
        function1.invoke(avatarPickerStorage.save(context, media));
    }

    public final void persistAvatarForSelf(Avatar avatar, Function1<? super Avatar, Unit> function1) {
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        Intrinsics.checkNotNullParameter(function1, "onPersisted");
        SignalExecutors.BOUNDED.execute(new Runnable(function1) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda6
            public final /* synthetic */ Function1 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerRepository.m329persistAvatarForSelf$lambda9(Avatar.this, this.f$1);
            }
        });
    }

    /* renamed from: persistAvatarForSelf$lambda-9 */
    public static final void m329persistAvatarForSelf$lambda9(Avatar avatar, Function1 function1) {
        Intrinsics.checkNotNullParameter(avatar, "$avatar");
        Intrinsics.checkNotNullParameter(function1, "$onPersisted");
        AvatarPickerDatabase avatarPicker = SignalDatabase.Companion.avatarPicker();
        Avatar saveAvatarForSelf = avatarPicker.saveAvatarForSelf(avatar);
        avatarPicker.markUsage(saveAvatarForSelf);
        function1.invoke(saveAvatarForSelf);
    }

    public final void persistAvatarForGroup(Avatar avatar, GroupId groupId, Function1<? super Avatar, Unit> function1) {
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(function1, "onPersisted");
        SignalExecutors.BOUNDED.execute(new Runnable(groupId, function1) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ GroupId f$1;
            public final /* synthetic */ Function1 f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerRepository.m328persistAvatarForGroup$lambda10(Avatar.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: persistAvatarForGroup$lambda-10 */
    public static final void m328persistAvatarForGroup$lambda10(Avatar avatar, GroupId groupId, Function1 function1) {
        Intrinsics.checkNotNullParameter(avatar, "$avatar");
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Intrinsics.checkNotNullParameter(function1, "$onPersisted");
        AvatarPickerDatabase avatarPicker = SignalDatabase.Companion.avatarPicker();
        Avatar saveAvatarForGroup = avatarPicker.saveAvatarForGroup(avatar, groupId);
        avatarPicker.markUsage(saveAvatarForGroup);
        function1.invoke(saveAvatarForGroup);
    }

    public final void persistAndCreateMediaForSelf(Avatar avatar, Function1<? super Media, Unit> function1) {
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        Intrinsics.checkNotNullParameter(function1, "onSaved");
        SignalExecutors.BOUNDED.execute(new Runnable(this, function1) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda9
            public final /* synthetic */ AvatarPickerRepository f$1;
            public final /* synthetic */ Function1 f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerRepository.m327persistAndCreateMediaForSelf$lambda11(Avatar.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: persistAndCreateMediaForSelf$lambda-11 */
    public static final void m327persistAndCreateMediaForSelf$lambda11(Avatar avatar, AvatarPickerRepository avatarPickerRepository, Function1 function1) {
        Intrinsics.checkNotNullParameter(avatar, "$avatar");
        Intrinsics.checkNotNullParameter(avatarPickerRepository, "this$0");
        Intrinsics.checkNotNullParameter(function1, "$onSaved");
        if (!(avatar.getDatabaseId() instanceof Avatar.DatabaseId.DoNotPersist)) {
            avatarPickerRepository.persistAvatarForSelf(avatar, new Function1<Avatar, Unit>(avatarPickerRepository, avatar, function1) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$persistAndCreateMediaForSelf$1$1
                final /* synthetic */ Avatar $avatar;
                final /* synthetic */ Function1<Media, Unit> $onSaved;
                final /* synthetic */ AvatarPickerRepository this$0;

                /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.mediasend.Media, kotlin.Unit> */
                /* JADX WARN: Multi-variable type inference failed */
                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                    this.$avatar = r2;
                    this.$onSaved = r3;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Avatar avatar2) {
                    invoke(avatar2);
                    return Unit.INSTANCE;
                }

                public final void invoke(Avatar avatar2) {
                    Intrinsics.checkNotNullParameter(avatar2, "it");
                    AvatarRenderer avatarRenderer = AvatarRenderer.INSTANCE;
                    Context context = this.this$0.applicationContext;
                    Intrinsics.checkNotNullExpressionValue(context, "applicationContext");
                    avatarRenderer.renderAvatar(context, this.$avatar, this.$onSaved, new Function1<Throwable, Unit>(this.this$0) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$persistAndCreateMediaForSelf$1$1.1
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                            invoke(th);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(Throwable th) {
                            ((AvatarPickerRepository) this.receiver).handleRenderFailure(th);
                        }
                    });
                }
            });
            return;
        }
        AvatarRenderer avatarRenderer = AvatarRenderer.INSTANCE;
        Context context = avatarPickerRepository.applicationContext;
        Intrinsics.checkNotNullExpressionValue(context, "applicationContext");
        avatarRenderer.renderAvatar(context, avatar, function1, new Function1<Throwable, Unit>(avatarPickerRepository) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$persistAndCreateMediaForSelf$1$2
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                ((AvatarPickerRepository) this.receiver).handleRenderFailure(th);
            }
        });
    }

    public final void persistAndCreateMediaForGroup(Avatar avatar, GroupId groupId, Function1<? super Media, Unit> function1) {
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(function1, "onSaved");
        SignalExecutors.BOUNDED.execute(new Runnable(this, groupId, function1) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ AvatarPickerRepository f$1;
            public final /* synthetic */ GroupId f$2;
            public final /* synthetic */ Function1 f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerRepository.m326persistAndCreateMediaForGroup$lambda12(Avatar.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    /* renamed from: persistAndCreateMediaForGroup$lambda-12 */
    public static final void m326persistAndCreateMediaForGroup$lambda12(Avatar avatar, AvatarPickerRepository avatarPickerRepository, GroupId groupId, Function1 function1) {
        Intrinsics.checkNotNullParameter(avatar, "$avatar");
        Intrinsics.checkNotNullParameter(avatarPickerRepository, "this$0");
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Intrinsics.checkNotNullParameter(function1, "$onSaved");
        if (!(avatar.getDatabaseId() instanceof Avatar.DatabaseId.DoNotPersist)) {
            avatarPickerRepository.persistAvatarForGroup(avatar, groupId, new Function1<Avatar, Unit>(avatarPickerRepository, avatar, function1) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$persistAndCreateMediaForGroup$1$1
                final /* synthetic */ Avatar $avatar;
                final /* synthetic */ Function1<Media, Unit> $onSaved;
                final /* synthetic */ AvatarPickerRepository this$0;

                /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.mediasend.Media, kotlin.Unit> */
                /* JADX WARN: Multi-variable type inference failed */
                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                    this.$avatar = r2;
                    this.$onSaved = r3;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Avatar avatar2) {
                    invoke(avatar2);
                    return Unit.INSTANCE;
                }

                public final void invoke(Avatar avatar2) {
                    Intrinsics.checkNotNullParameter(avatar2, "it");
                    AvatarRenderer avatarRenderer = AvatarRenderer.INSTANCE;
                    Context context = this.this$0.applicationContext;
                    Intrinsics.checkNotNullExpressionValue(context, "applicationContext");
                    avatarRenderer.renderAvatar(context, this.$avatar, this.$onSaved, new Function1<Throwable, Unit>(this.this$0) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$persistAndCreateMediaForGroup$1$1.1
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                            invoke(th);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(Throwable th) {
                            ((AvatarPickerRepository) this.receiver).handleRenderFailure(th);
                        }
                    });
                }
            });
            return;
        }
        AvatarRenderer avatarRenderer = AvatarRenderer.INSTANCE;
        Context context = avatarPickerRepository.applicationContext;
        Intrinsics.checkNotNullExpressionValue(context, "applicationContext");
        avatarRenderer.renderAvatar(context, avatar, function1, new Function1<Throwable, Unit>(avatarPickerRepository) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$persistAndCreateMediaForGroup$1$2
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                ((AvatarPickerRepository) this.receiver).handleRenderFailure(th);
            }
        });
    }

    public final void createMediaForNewGroup(Avatar avatar, Function1<? super Media, Unit> function1) {
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        Intrinsics.checkNotNullParameter(function1, "onSaved");
        SignalExecutors.BOUNDED.execute(new Runnable(avatar, function1) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ Avatar f$1;
            public final /* synthetic */ Function1 f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerRepository.m317createMediaForNewGroup$lambda13(AvatarPickerRepository.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: createMediaForNewGroup$lambda-13 */
    public static final void m317createMediaForNewGroup$lambda13(AvatarPickerRepository avatarPickerRepository, Avatar avatar, Function1 function1) {
        Intrinsics.checkNotNullParameter(avatarPickerRepository, "this$0");
        Intrinsics.checkNotNullParameter(avatar, "$avatar");
        Intrinsics.checkNotNullParameter(function1, "$onSaved");
        AvatarRenderer avatarRenderer = AvatarRenderer.INSTANCE;
        Context context = avatarPickerRepository.applicationContext;
        Intrinsics.checkNotNullExpressionValue(context, "applicationContext");
        avatarRenderer.renderAvatar(context, avatar, function1, new Function1<Throwable, Unit>(avatarPickerRepository) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$createMediaForNewGroup$1$1
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                ((AvatarPickerRepository) this.receiver).handleRenderFailure(th);
            }
        });
    }

    public final void handleRenderFailure(Throwable th) {
        Log.w(AvatarPickerRepositoryKt.TAG, "Failed to render avatar.", th);
        ThreadUtil.postToMain(new Runnable() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda10
            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerRepository.m325handleRenderFailure$lambda14(AvatarPickerRepository.this);
            }
        });
    }

    /* renamed from: handleRenderFailure$lambda-14 */
    public static final void m325handleRenderFailure$lambda14(AvatarPickerRepository avatarPickerRepository) {
        Intrinsics.checkNotNullParameter(avatarPickerRepository, "this$0");
        Toast.makeText(avatarPickerRepository.applicationContext, (int) R.string.AvatarPickerRepository__failed_to_save_avatar, 0).show();
    }

    public final Avatar getDefaultAvatarForSelf() {
        String displayName = Recipient.self().getDisplayName(this.applicationContext);
        Intrinsics.checkNotNullExpressionValue(displayName, "self().getDisplayName(applicationContext)");
        String abbreviation = NameUtil.getAbbreviation(displayName);
        if (abbreviation == null || (StringsKt__StringsJVMKt.isBlank(abbreviation))) {
            return Avatar.Companion.getDefaultForSelf();
        }
        Avatars.ColorPair colorPair = Avatars.INSTANCE.getColorMap().get(Recipient.self().getAvatarColor().serialize());
        if (colorPair != null) {
            return new Avatar.Text(abbreviation, colorPair, Avatar.DatabaseId.DoNotPersist.INSTANCE);
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    public final Avatar getDefaultAvatarForGroup(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Recipient externalGroupExact = Recipient.externalGroupExact(groupId);
        Intrinsics.checkNotNullExpressionValue(externalGroupExact, "externalGroupExact(groupId)");
        return getDefaultAvatarForGroup(externalGroupExact.getAvatarColor());
    }

    public final Avatar getDefaultAvatarForGroup(AvatarColor avatarColor) {
        Avatars.ColorPair colorPair = Avatars.INSTANCE.getColorMap().get(avatarColor != null ? avatarColor.serialize() : null);
        Avatar.Resource defaultForGroup = Avatar.Companion.getDefaultForGroup();
        return colorPair != null ? Avatar.Resource.copy$default(defaultForGroup, 0, colorPair, 1, null) : defaultForGroup;
    }

    public final void delete(Avatar avatar, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        Intrinsics.checkNotNullParameter(function0, "onDelete");
        SignalExecutors.BOUNDED.execute(new Runnable(function0) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerRepository$$ExternalSyntheticLambda11
            public final /* synthetic */ Function0 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerRepository.m318delete$lambda15(Avatar.this, this.f$1);
            }
        });
    }

    /* renamed from: delete$lambda-15 */
    public static final void m318delete$lambda15(Avatar avatar, Function0 function0) {
        Intrinsics.checkNotNullParameter(avatar, "$avatar");
        Intrinsics.checkNotNullParameter(function0, "$onDelete");
        if (avatar.getDatabaseId() instanceof Avatar.DatabaseId.Saved) {
            SignalDatabase.Companion.avatarPicker().deleteAvatar(avatar);
        }
        function0.invoke();
    }
}
