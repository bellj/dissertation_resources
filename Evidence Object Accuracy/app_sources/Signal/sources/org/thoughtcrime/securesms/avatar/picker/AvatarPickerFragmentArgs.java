package org.thoughtcrime.securesms.avatar.picker;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.mediasend.Media;

/* loaded from: classes.dex */
public class AvatarPickerFragmentArgs {
    private final HashMap arguments;

    private AvatarPickerFragmentArgs() {
        this.arguments = new HashMap();
    }

    private AvatarPickerFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static AvatarPickerFragmentArgs fromBundle(Bundle bundle) {
        AvatarPickerFragmentArgs avatarPickerFragmentArgs = new AvatarPickerFragmentArgs();
        bundle.setClassLoader(AvatarPickerFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("group_id")) {
            throw new IllegalArgumentException("Required argument \"group_id\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(ParcelableGroupId.class) || Serializable.class.isAssignableFrom(ParcelableGroupId.class)) {
            avatarPickerFragmentArgs.arguments.put("group_id", (ParcelableGroupId) bundle.get("group_id"));
            if (bundle.containsKey("is_new_group")) {
                avatarPickerFragmentArgs.arguments.put("is_new_group", Boolean.valueOf(bundle.getBoolean("is_new_group")));
            } else {
                avatarPickerFragmentArgs.arguments.put("is_new_group", Boolean.FALSE);
            }
            if (!bundle.containsKey("group_avatar_media")) {
                throw new IllegalArgumentException("Required argument \"group_avatar_media\" is missing and does not have an android:defaultValue");
            } else if (Parcelable.class.isAssignableFrom(Media.class) || Serializable.class.isAssignableFrom(Media.class)) {
                avatarPickerFragmentArgs.arguments.put("group_avatar_media", (Media) bundle.get("group_avatar_media"));
                return avatarPickerFragmentArgs;
            } else {
                throw new UnsupportedOperationException(Media.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        } else {
            throw new UnsupportedOperationException(ParcelableGroupId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public ParcelableGroupId getGroupId() {
        return (ParcelableGroupId) this.arguments.get("group_id");
    }

    public boolean getIsNewGroup() {
        return ((Boolean) this.arguments.get("is_new_group")).booleanValue();
    }

    public Media getGroupAvatarMedia() {
        return (Media) this.arguments.get("group_avatar_media");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("group_id")) {
            ParcelableGroupId parcelableGroupId = (ParcelableGroupId) this.arguments.get("group_id");
            if (Parcelable.class.isAssignableFrom(ParcelableGroupId.class) || parcelableGroupId == null) {
                bundle.putParcelable("group_id", (Parcelable) Parcelable.class.cast(parcelableGroupId));
            } else if (Serializable.class.isAssignableFrom(ParcelableGroupId.class)) {
                bundle.putSerializable("group_id", (Serializable) Serializable.class.cast(parcelableGroupId));
            } else {
                throw new UnsupportedOperationException(ParcelableGroupId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        if (this.arguments.containsKey("is_new_group")) {
            bundle.putBoolean("is_new_group", ((Boolean) this.arguments.get("is_new_group")).booleanValue());
        } else {
            bundle.putBoolean("is_new_group", false);
        }
        if (this.arguments.containsKey("group_avatar_media")) {
            Media media = (Media) this.arguments.get("group_avatar_media");
            if (Parcelable.class.isAssignableFrom(Media.class) || media == null) {
                bundle.putParcelable("group_avatar_media", (Parcelable) Parcelable.class.cast(media));
            } else if (Serializable.class.isAssignableFrom(Media.class)) {
                bundle.putSerializable("group_avatar_media", (Serializable) Serializable.class.cast(media));
            } else {
                throw new UnsupportedOperationException(Media.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AvatarPickerFragmentArgs avatarPickerFragmentArgs = (AvatarPickerFragmentArgs) obj;
        if (this.arguments.containsKey("group_id") != avatarPickerFragmentArgs.arguments.containsKey("group_id")) {
            return false;
        }
        if (getGroupId() == null ? avatarPickerFragmentArgs.getGroupId() != null : !getGroupId().equals(avatarPickerFragmentArgs.getGroupId())) {
            return false;
        }
        if (this.arguments.containsKey("is_new_group") == avatarPickerFragmentArgs.arguments.containsKey("is_new_group") && getIsNewGroup() == avatarPickerFragmentArgs.getIsNewGroup() && this.arguments.containsKey("group_avatar_media") == avatarPickerFragmentArgs.arguments.containsKey("group_avatar_media")) {
            return getGroupAvatarMedia() == null ? avatarPickerFragmentArgs.getGroupAvatarMedia() == null : getGroupAvatarMedia().equals(avatarPickerFragmentArgs.getGroupAvatarMedia());
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((getGroupId() != null ? getGroupId().hashCode() : 0) + 31) * 31) + (getIsNewGroup() ? 1 : 0)) * 31;
        if (getGroupAvatarMedia() != null) {
            i = getGroupAvatarMedia().hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "AvatarPickerFragmentArgs{groupId=" + getGroupId() + ", isNewGroup=" + getIsNewGroup() + ", groupAvatarMedia=" + getGroupAvatarMedia() + "}";
    }

    /* loaded from: classes.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(AvatarPickerFragmentArgs avatarPickerFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(avatarPickerFragmentArgs.arguments);
        }

        public Builder(ParcelableGroupId parcelableGroupId, Media media) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("group_id", parcelableGroupId);
            hashMap.put("group_avatar_media", media);
        }

        public AvatarPickerFragmentArgs build() {
            return new AvatarPickerFragmentArgs(this.arguments);
        }

        public Builder setGroupId(ParcelableGroupId parcelableGroupId) {
            this.arguments.put("group_id", parcelableGroupId);
            return this;
        }

        public Builder setIsNewGroup(boolean z) {
            this.arguments.put("is_new_group", Boolean.valueOf(z));
            return this;
        }

        public Builder setGroupAvatarMedia(Media media) {
            this.arguments.put("group_avatar_media", media);
            return this;
        }

        public ParcelableGroupId getGroupId() {
            return (ParcelableGroupId) this.arguments.get("group_id");
        }

        public boolean getIsNewGroup() {
            return ((Boolean) this.arguments.get("is_new_group")).booleanValue();
        }

        public Media getGroupAvatarMedia() {
            return (Media) this.arguments.get("group_avatar_media");
        }
    }
}
