package org.thoughtcrime.securesms.avatar.picker;

import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: AvatarPickerViewModel.kt */
@Metadata(d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004*+,-B\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0014H$J\b\u0010\u0015\u001a\u00020\u0012H$J\u0014\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00170\u0014H$J\u0014\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00170\u0014H$J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u000e\u0010\u001b\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u001c\u001a\u00020\u000f2\u0006\u0010\u001d\u001a\u00020\u001eJ\u000e\u0010\u001f\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012J\b\u0010 \u001a\u00020\u000fH\u0014J$\u0010!\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u00122\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u000f0#H$J$\u0010$\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u00122\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u000f0#H$J\b\u0010&\u001a\u00020\u000fH\u0004J\b\u0010'\u001a\u00020\u000fH\u0004J(\u0010(\u001a\u00020\u000f2\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u000f0#2\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u000f0)R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000\u0001\u0003./0¨\u00061"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;", "(Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "clear", "", "delete", "avatar", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "getAvatar", "Lio/reactivex/rxjava3/core/Single;", "getDefaultAvatarFromRepository", "getDefaultAvatars", "", "getPersistedAvatars", "isSaveable", "", "onAvatarEditCompleted", "onAvatarPhotoSelectionCompleted", "media", "Lorg/thoughtcrime/securesms/mediasend/Media;", "onAvatarSelectedFromGrid", "onCleared", "persistAndCreateMedia", "onSaved", "Lkotlin/Function1;", "persistAvatar", "onPersisted", "refreshAvatar", "refreshSelectableAvatars", "save", "Lkotlin/Function0;", "Factory", "GroupAvatarPickerViewModel", "NewGroupAvatarPickerViewModel", "SelfAvatarPickerViewModel", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel$SelfAvatarPickerViewModel;", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel$GroupAvatarPickerViewModel;", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel$NewGroupAvatarPickerViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public abstract class AvatarPickerViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final AvatarPickerRepository repository;
    private final LiveData<AvatarPickerState> state;
    private final Store<AvatarPickerState> store;

    public /* synthetic */ AvatarPickerViewModel(AvatarPickerRepository avatarPickerRepository, DefaultConstructorMarker defaultConstructorMarker) {
        this(avatarPickerRepository);
    }

    protected abstract Single<Avatar> getAvatar();

    protected abstract Avatar getDefaultAvatarFromRepository();

    protected abstract Single<List<Avatar>> getDefaultAvatars();

    protected abstract Single<List<Avatar>> getPersistedAvatars();

    protected abstract void persistAndCreateMedia(Avatar avatar, Function1<? super Media, Unit> function1);

    /* access modifiers changed from: protected */
    public abstract void persistAvatar(Avatar avatar, Function1<? super Avatar, Unit> function1);

    private AvatarPickerViewModel(AvatarPickerRepository avatarPickerRepository) {
        this.repository = avatarPickerRepository;
        this.disposables = new CompositeDisposable();
        Store<AvatarPickerState> store = new Store<>(new AvatarPickerState(null, null, false, false, false, 31, null));
        this.store = store;
        LiveData<AvatarPickerState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<AvatarPickerState> getState() {
        return this.state;
    }

    public final void delete(Avatar avatar) {
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        this.repository.delete(avatar, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$delete$1
            final /* synthetic */ AvatarPickerViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                this.this$0.refreshAvatar();
                this.this$0.refreshSelectableAvatars();
            }
        });
    }

    public final void clear() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AvatarPickerViewModel.m334clear$lambda0(AvatarPickerViewModel.this, (AvatarPickerState) obj);
            }
        });
    }

    /* renamed from: clear$lambda-0 */
    public static final AvatarPickerState m334clear$lambda0(AvatarPickerViewModel avatarPickerViewModel, AvatarPickerState avatarPickerState) {
        Intrinsics.checkNotNullParameter(avatarPickerViewModel, "this$0");
        Avatar defaultAvatarFromRepository = avatarPickerViewModel.getDefaultAvatarFromRepository();
        Intrinsics.checkNotNullExpressionValue(avatarPickerState, "it");
        return AvatarPickerState.copy$default(avatarPickerState, defaultAvatarFromRepository, null, true, false, true, 2, null);
    }

    public final void save(Function1<? super Media, Unit> function1, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(function1, "onSaved");
        Intrinsics.checkNotNullParameter(function0, "onCleared");
        if (this.store.getState().isCleared()) {
            function0.invoke();
            return;
        }
        Avatar currentAvatar = this.store.getState().getCurrentAvatar();
        if (currentAvatar != null) {
            persistAndCreateMedia(currentAvatar, function1);
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: onAvatarSelectedFromGrid$lambda-1 */
    public static final AvatarPickerState m335onAvatarSelectedFromGrid$lambda1(Avatar avatar, AvatarPickerViewModel avatarPickerViewModel, AvatarPickerState avatarPickerState) {
        Intrinsics.checkNotNullParameter(avatar, "$avatar");
        Intrinsics.checkNotNullParameter(avatarPickerViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(avatarPickerState, "it");
        return AvatarPickerState.copy$default(avatarPickerState, avatar, null, avatarPickerViewModel.isSaveable(avatar), true, false, 2, null);
    }

    public final void onAvatarSelectedFromGrid(Avatar avatar) {
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        this.store.update(new Function(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ AvatarPickerViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AvatarPickerViewModel.m335onAvatarSelectedFromGrid$lambda1(Avatar.this, this.f$1, (AvatarPickerState) obj);
            }
        });
    }

    public final void onAvatarEditCompleted(Avatar avatar) {
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        persistAvatar(avatar, new AvatarPickerViewModel$onAvatarEditCompleted$1(this));
    }

    public final void onAvatarPhotoSelectionCompleted(Media media) {
        Intrinsics.checkNotNullParameter(media, "media");
        this.repository.writeMediaToMultiSessionStorage(media, new Function1<Uri, Unit>(this, media) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$onAvatarPhotoSelectionCompleted$1
            final /* synthetic */ Media $media;
            final /* synthetic */ AvatarPickerViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$media = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Uri uri) {
                invoke(uri);
                return Unit.INSTANCE;
            }

            public final void invoke(Uri uri) {
                Intrinsics.checkNotNullParameter(uri, "multiSessionUri");
                AvatarPickerViewModel avatarPickerViewModel = this.this$0;
                Avatar.Photo photo = new Avatar.Photo(uri, this.$media.getSize(), Avatar.DatabaseId.NotSet.INSTANCE);
                final AvatarPickerViewModel avatarPickerViewModel2 = this.this$0;
                avatarPickerViewModel.persistAvatar(photo, new Function1<Avatar, Unit>() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$onAvatarPhotoSelectionCompleted$1.1
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Avatar avatar) {
                        invoke(avatar);
                        return Unit.INSTANCE;
                    }

                    /* access modifiers changed from: private */
                    /* renamed from: invoke$lambda-0  reason: not valid java name */
                    public static final AvatarPickerState m343invoke$lambda0(Avatar avatar, AvatarPickerViewModel avatarPickerViewModel3, AvatarPickerState avatarPickerState) {
                        Intrinsics.checkNotNullParameter(avatar, "$avatar");
                        Intrinsics.checkNotNullParameter(avatarPickerViewModel3, "this$0");
                        Intrinsics.checkNotNullExpressionValue(avatarPickerState, "it");
                        return AvatarPickerState.copy$default(avatarPickerState, avatar, null, avatarPickerViewModel3.isSaveable(avatar), true, false, 2, null);
                    }

                    public final void invoke(Avatar avatar) {
                        Intrinsics.checkNotNullParameter(avatar, "avatar");
                        avatarPickerViewModel2.store.update(new AvatarPickerViewModel$onAvatarPhotoSelectionCompleted$1$1$$ExternalSyntheticLambda0(avatar, avatarPickerViewModel2));
                        avatarPickerViewModel2.refreshSelectableAvatars();
                    }
                });
            }
        });
    }

    public final void refreshAvatar() {
        this.disposables.add(getAvatar().subscribeOn(Schedulers.io()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                AvatarPickerViewModel.m336refreshAvatar$lambda3(AvatarPickerViewModel.this, (Avatar) obj);
            }
        }));
    }

    /* renamed from: refreshAvatar$lambda-3 */
    public static final void m336refreshAvatar$lambda3(AvatarPickerViewModel avatarPickerViewModel, Avatar avatar) {
        Intrinsics.checkNotNullParameter(avatarPickerViewModel, "this$0");
        avatarPickerViewModel.store.update(new Function(avatarPickerViewModel) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ AvatarPickerViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AvatarPickerViewModel.m337refreshAvatar$lambda3$lambda2(Avatar.this, this.f$1, (AvatarPickerState) obj);
            }
        });
    }

    /* renamed from: refreshAvatar$lambda-3$lambda-2 */
    public static final AvatarPickerState m337refreshAvatar$lambda3$lambda2(Avatar avatar, AvatarPickerViewModel avatarPickerViewModel, AvatarPickerState avatarPickerState) {
        Intrinsics.checkNotNullParameter(avatarPickerViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(avatarPickerState, "it");
        Intrinsics.checkNotNullExpressionValue(avatar, "avatar");
        return AvatarPickerState.copy$default(avatarPickerState, avatar, null, avatarPickerViewModel.isSaveable(avatar), (avatar instanceof Avatar.Photo) && !avatarPickerViewModel.isSaveable(avatar), false, 2, null);
    }

    public final void refreshSelectableAvatars() {
        this.disposables.add(Single.zip(getPersistedAvatars(), getDefaultAvatars(), new BiFunction() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return AvatarPickerViewModel.m338refreshSelectableAvatars$lambda6((List) obj, (List) obj2);
            }
        }).subscribeOn(Schedulers.io()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                AvatarPickerViewModel.m339refreshSelectableAvatars$lambda8(AvatarPickerViewModel.this, (List) obj);
            }
        }));
    }

    /* renamed from: refreshSelectableAvatars$lambda-6 */
    public static final List m338refreshSelectableAvatars$lambda6(List list, List list2) {
        Intrinsics.checkNotNullExpressionValue(list, "custom");
        List<Avatar.Vector> list3 = CollectionsKt___CollectionsJvmKt.filterIsInstance(list, Avatar.Vector.class);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list3, 10));
        for (Avatar.Vector vector : list3) {
            arrayList.add(vector.getKey());
        }
        Intrinsics.checkNotNullExpressionValue(list2, "def");
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : list2) {
            Avatar avatar = (Avatar) obj;
            if (!((avatar instanceof Avatar.Vector) && arrayList.contains(((Avatar.Vector) avatar).getKey()))) {
                arrayList2.add(obj);
            }
        }
        return CollectionsKt___CollectionsKt.plus((Collection) list, (Iterable) arrayList2);
    }

    /* renamed from: refreshSelectableAvatars$lambda-8 */
    public static final void m339refreshSelectableAvatars$lambda8(AvatarPickerViewModel avatarPickerViewModel, List list) {
        Intrinsics.checkNotNullParameter(avatarPickerViewModel, "this$0");
        avatarPickerViewModel.store.update(new Function(list) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AvatarPickerViewModel.m340refreshSelectableAvatars$lambda8$lambda7(this.f$0, (AvatarPickerState) obj);
            }
        });
    }

    /* renamed from: refreshSelectableAvatars$lambda-8$lambda-7 */
    public static final AvatarPickerState m340refreshSelectableAvatars$lambda8$lambda7(List list, AvatarPickerState avatarPickerState) {
        Intrinsics.checkNotNullExpressionValue(avatarPickerState, "it");
        Intrinsics.checkNotNullExpressionValue(list, "avatars");
        return AvatarPickerState.copy$default(avatarPickerState, null, list, false, false, false, 29, null);
    }

    public final boolean isSaveable(Avatar avatar) {
        return !Intrinsics.areEqual(avatar.getDatabaseId(), Avatar.DatabaseId.DoNotPersist.INSTANCE);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.dispose();
    }

    /* compiled from: AvatarPickerViewModel.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0014J\b\u0010\b\u001a\u00020\u0007H\u0014J\u0014\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\n0\u0006H\u0014J\u0014\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\n0\u0006H\u0014J$\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00072\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u0010H\u0014J$\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00072\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\r0\u0010H\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel$SelfAvatarPickerViewModel;", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel;", "repository", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;", "(Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;)V", "getAvatar", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "getDefaultAvatarFromRepository", "getDefaultAvatars", "", "getPersistedAvatars", "persistAndCreateMedia", "", "avatar", "onSaved", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "persistAvatar", "onPersisted", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    private static final class SelfAvatarPickerViewModel extends AvatarPickerViewModel {
        private final AvatarPickerRepository repository;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SelfAvatarPickerViewModel(AvatarPickerRepository avatarPickerRepository) {
            super(avatarPickerRepository, null);
            Intrinsics.checkNotNullParameter(avatarPickerRepository, "repository");
            this.repository = avatarPickerRepository;
            refreshAvatar();
            refreshSelectableAvatars();
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Single<Avatar> getAvatar() {
            return this.repository.getAvatarForSelf();
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Avatar getDefaultAvatarFromRepository() {
            return this.repository.getDefaultAvatarForSelf();
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Single<List<Avatar>> getPersistedAvatars() {
            return this.repository.getPersistedAvatarsForSelf();
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Single<List<Avatar>> getDefaultAvatars() {
            return this.repository.getDefaultAvatarsForSelf();
        }

        /* access modifiers changed from: protected */
        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        public void persistAvatar(Avatar avatar, Function1<? super Avatar, Unit> function1) {
            Intrinsics.checkNotNullParameter(avatar, "avatar");
            Intrinsics.checkNotNullParameter(function1, "onPersisted");
            this.repository.persistAvatarForSelf(avatar, function1);
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected void persistAndCreateMedia(Avatar avatar, Function1<? super Media, Unit> function1) {
            Intrinsics.checkNotNullParameter(avatar, "avatar");
            Intrinsics.checkNotNullParameter(function1, "onSaved");
            this.repository.persistAndCreateMediaForSelf(avatar, function1);
        }
    }

    /* compiled from: AvatarPickerViewModel.kt */
    @Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\fH\u0014J\b\u0010\r\u001a\u00020\nH\u0014J\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u000f0\fH\u0014J\u0014\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u000f0\fH\u0014J$\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\n2\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00120\u0015H\u0014J$\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\n2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00120\u0015H\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel$GroupAvatarPickerViewModel;", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "repository", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;", "groupAvatarMedia", "Lorg/thoughtcrime/securesms/mediasend/Media;", "(Lorg/thoughtcrime/securesms/groups/GroupId;Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;Lorg/thoughtcrime/securesms/mediasend/Media;)V", "initialAvatar", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "getAvatar", "Lio/reactivex/rxjava3/core/Single;", "getDefaultAvatarFromRepository", "getDefaultAvatars", "", "getPersistedAvatars", "persistAndCreateMedia", "", "avatar", "onSaved", "Lkotlin/Function1;", "persistAvatar", "onPersisted", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    private static final class GroupAvatarPickerViewModel extends AvatarPickerViewModel {
        private final GroupId groupId;
        private final Avatar initialAvatar;
        private final AvatarPickerRepository repository;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public GroupAvatarPickerViewModel(GroupId groupId, AvatarPickerRepository avatarPickerRepository, Media media) {
            super(avatarPickerRepository, null);
            Intrinsics.checkNotNullParameter(groupId, "groupId");
            Intrinsics.checkNotNullParameter(avatarPickerRepository, "repository");
            Avatar.Photo photo = null;
            this.groupId = groupId;
            this.repository = avatarPickerRepository;
            if (media != null) {
                Uri uri = media.getUri();
                Intrinsics.checkNotNullExpressionValue(uri, "it.uri");
                photo = new Avatar.Photo(uri, media.getSize(), Avatar.DatabaseId.DoNotPersist.INSTANCE);
            }
            this.initialAvatar = photo;
            refreshAvatar();
            refreshSelectableAvatars();
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Single<Avatar> getAvatar() {
            Avatar avatar = this.initialAvatar;
            if (avatar == null) {
                return this.repository.getAvatarForGroup(this.groupId);
            }
            Single<Avatar> just = Single.just(avatar);
            Intrinsics.checkNotNullExpressionValue(just, "{\n        Single.just(initialAvatar)\n      }");
            return just;
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Avatar getDefaultAvatarFromRepository() {
            return this.repository.getDefaultAvatarForGroup(this.groupId);
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Single<List<Avatar>> getPersistedAvatars() {
            return this.repository.getPersistedAvatarsForGroup(this.groupId);
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Single<List<Avatar>> getDefaultAvatars() {
            return this.repository.getDefaultAvatarsForGroup();
        }

        /* access modifiers changed from: protected */
        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        public void persistAvatar(Avatar avatar, Function1<? super Avatar, Unit> function1) {
            Intrinsics.checkNotNullParameter(avatar, "avatar");
            Intrinsics.checkNotNullParameter(function1, "onPersisted");
            this.repository.persistAvatarForGroup(avatar, this.groupId, function1);
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected void persistAndCreateMedia(Avatar avatar, Function1<? super Media, Unit> function1) {
            Intrinsics.checkNotNullParameter(avatar, "avatar");
            Intrinsics.checkNotNullParameter(function1, "onSaved");
            this.repository.persistAndCreateMediaForGroup(avatar, this.groupId, function1);
        }
    }

    /* compiled from: AvatarPickerViewModel.kt */
    @Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\nH\u0014J\b\u0010\u000b\u001a\u00020\bH\u0014J\u0014\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\r0\nH\u0014J\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\r0\nH\u0014J$\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\b2\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00100\u0013H\u0014J$\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\b2\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00100\u0013H\u0014R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel$NewGroupAvatarPickerViewModel;", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel;", "repository", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;", "initialMedia", "Lorg/thoughtcrime/securesms/mediasend/Media;", "(Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;Lorg/thoughtcrime/securesms/mediasend/Media;)V", "initialAvatar", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "getAvatar", "Lio/reactivex/rxjava3/core/Single;", "getDefaultAvatarFromRepository", "getDefaultAvatars", "", "getPersistedAvatars", "persistAndCreateMedia", "", "avatar", "onSaved", "Lkotlin/Function1;", "persistAvatar", "onPersisted", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class NewGroupAvatarPickerViewModel extends AvatarPickerViewModel {
        private final Avatar initialAvatar;
        private final AvatarPickerRepository repository;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public NewGroupAvatarPickerViewModel(AvatarPickerRepository avatarPickerRepository, Media media) {
            super(avatarPickerRepository, null);
            Intrinsics.checkNotNullParameter(avatarPickerRepository, "repository");
            Avatar.Photo photo = null;
            this.repository = avatarPickerRepository;
            if (media != null) {
                Uri uri = media.getUri();
                Intrinsics.checkNotNullExpressionValue(uri, "it.uri");
                photo = new Avatar.Photo(uri, media.getSize(), Avatar.DatabaseId.DoNotPersist.INSTANCE);
            }
            this.initialAvatar = photo;
            refreshAvatar();
            refreshSelectableAvatars();
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Single<Avatar> getAvatar() {
            Avatar avatar = this.initialAvatar;
            if (avatar != null) {
                Single<Avatar> just = Single.just(avatar);
                Intrinsics.checkNotNullExpressionValue(just, "{\n        Single.just(initialAvatar)\n      }");
                return just;
            }
            Single<Avatar> fromCallable = Single.fromCallable(new AvatarPickerViewModel$NewGroupAvatarPickerViewModel$$ExternalSyntheticLambda0(this));
            Intrinsics.checkNotNullExpressionValue(fromCallable, "{\n        Single.fromCal…romRepository() }\n      }");
            return fromCallable;
        }

        /* renamed from: getAvatar$lambda-1 */
        public static final Avatar m341getAvatar$lambda1(NewGroupAvatarPickerViewModel newGroupAvatarPickerViewModel) {
            Intrinsics.checkNotNullParameter(newGroupAvatarPickerViewModel, "this$0");
            return newGroupAvatarPickerViewModel.getDefaultAvatarFromRepository();
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Avatar getDefaultAvatarFromRepository() {
            return this.repository.getDefaultAvatarForGroup((AvatarColor) null);
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Single<List<Avatar>> getPersistedAvatars() {
            Single<List<Avatar>> just = Single.just(CollectionsKt__CollectionsKt.emptyList());
            Intrinsics.checkNotNullExpressionValue(just, "just(listOf())");
            return just;
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected Single<List<Avatar>> getDefaultAvatars() {
            return this.repository.getDefaultAvatarsForGroup();
        }

        /* access modifiers changed from: protected */
        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        public void persistAvatar(Avatar avatar, Function1<? super Avatar, Unit> function1) {
            Intrinsics.checkNotNullParameter(avatar, "avatar");
            Intrinsics.checkNotNullParameter(function1, "onPersisted");
            function1.invoke(avatar);
        }

        @Override // org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel
        protected void persistAndCreateMedia(Avatar avatar, Function1<? super Media, Unit> function1) {
            Intrinsics.checkNotNullParameter(avatar, "avatar");
            Intrinsics.checkNotNullParameter(function1, "onSaved");
            this.repository.createMediaForNewGroup(avatar, function1);
        }
    }

    /* compiled from: AvatarPickerViewModel.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\nJ%\u0010\u000b\u001a\u0002H\f\"\b\b\u0000\u0010\f*\u00020\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\f0\u000fH\u0016¢\u0006\u0002\u0010\u0010R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "isNewGroup", "", "groupAvatarMedia", "Lorg/thoughtcrime/securesms/mediasend/Media;", "(Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerRepository;Lorg/thoughtcrime/securesms/groups/GroupId;ZLorg/thoughtcrime/securesms/mediasend/Media;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Media groupAvatarMedia;
        private final GroupId groupId;
        private final boolean isNewGroup;
        private final AvatarPickerRepository repository;

        public Factory(AvatarPickerRepository avatarPickerRepository, GroupId groupId, boolean z, Media media) {
            Intrinsics.checkNotNullParameter(avatarPickerRepository, "repository");
            this.repository = avatarPickerRepository;
            this.groupId = groupId;
            this.isNewGroup = z;
            this.groupAvatarMedia = media;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Object obj;
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            GroupId groupId = this.groupId;
            if (groupId == null && !this.isNewGroup) {
                obj = new SelfAvatarPickerViewModel(this.repository);
            } else if (groupId == null) {
                obj = new NewGroupAvatarPickerViewModel(this.repository, this.groupAvatarMedia);
            } else {
                obj = new GroupAvatarPickerViewModel(groupId, this.repository, this.groupAvatarMedia);
            }
            T cast = cls.cast(obj);
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
