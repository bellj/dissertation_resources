package org.thoughtcrime.securesms.avatar.picker;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.avatar.Avatar;

/* compiled from: AvatarPickerState.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0013\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B?\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0007\u0012\b\b\u0002\u0010\t\u001a\u00020\u0007¢\u0006\u0002\u0010\nJ\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0007HÆ\u0003JC\u0010\u0017\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\t\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00072\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001R\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\t\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\fR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerState;", "", "currentAvatar", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "selectableAvatars", "", "canSave", "", "canClear", "isCleared", "(Lorg/thoughtcrime/securesms/avatar/Avatar;Ljava/util/List;ZZZ)V", "getCanClear", "()Z", "getCanSave", "getCurrentAvatar", "()Lorg/thoughtcrime/securesms/avatar/Avatar;", "getSelectableAvatars", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarPickerState {
    private final boolean canClear;
    private final boolean canSave;
    private final Avatar currentAvatar;
    private final boolean isCleared;
    private final List<Avatar> selectableAvatars;

    public AvatarPickerState() {
        this(null, null, false, false, false, 31, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: org.thoughtcrime.securesms.avatar.picker.AvatarPickerState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ AvatarPickerState copy$default(AvatarPickerState avatarPickerState, Avatar avatar, List list, boolean z, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            avatar = avatarPickerState.currentAvatar;
        }
        if ((i & 2) != 0) {
            list = avatarPickerState.selectableAvatars;
        }
        if ((i & 4) != 0) {
            z = avatarPickerState.canSave;
        }
        if ((i & 8) != 0) {
            z2 = avatarPickerState.canClear;
        }
        if ((i & 16) != 0) {
            z3 = avatarPickerState.isCleared;
        }
        return avatarPickerState.copy(avatar, list, z, z2, z3);
    }

    public final Avatar component1() {
        return this.currentAvatar;
    }

    public final List<Avatar> component2() {
        return this.selectableAvatars;
    }

    public final boolean component3() {
        return this.canSave;
    }

    public final boolean component4() {
        return this.canClear;
    }

    public final boolean component5() {
        return this.isCleared;
    }

    public final AvatarPickerState copy(Avatar avatar, List<? extends Avatar> list, boolean z, boolean z2, boolean z3) {
        Intrinsics.checkNotNullParameter(list, "selectableAvatars");
        return new AvatarPickerState(avatar, list, z, z2, z3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AvatarPickerState)) {
            return false;
        }
        AvatarPickerState avatarPickerState = (AvatarPickerState) obj;
        return Intrinsics.areEqual(this.currentAvatar, avatarPickerState.currentAvatar) && Intrinsics.areEqual(this.selectableAvatars, avatarPickerState.selectableAvatars) && this.canSave == avatarPickerState.canSave && this.canClear == avatarPickerState.canClear && this.isCleared == avatarPickerState.isCleared;
    }

    public int hashCode() {
        Avatar avatar = this.currentAvatar;
        int hashCode = (((avatar == null ? 0 : avatar.hashCode()) * 31) + this.selectableAvatars.hashCode()) * 31;
        boolean z = this.canSave;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode + i2) * 31;
        boolean z2 = this.canClear;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.isCleared;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return i9 + i;
    }

    public String toString() {
        return "AvatarPickerState(currentAvatar=" + this.currentAvatar + ", selectableAvatars=" + this.selectableAvatars + ", canSave=" + this.canSave + ", canClear=" + this.canClear + ", isCleared=" + this.isCleared + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.avatar.Avatar> */
    /* JADX WARN: Multi-variable type inference failed */
    public AvatarPickerState(Avatar avatar, List<? extends Avatar> list, boolean z, boolean z2, boolean z3) {
        Intrinsics.checkNotNullParameter(list, "selectableAvatars");
        this.currentAvatar = avatar;
        this.selectableAvatars = list;
        this.canSave = z;
        this.canClear = z2;
        this.isCleared = z3;
    }

    public final Avatar getCurrentAvatar() {
        return this.currentAvatar;
    }

    public /* synthetic */ AvatarPickerState(Avatar avatar, List list, boolean z, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : avatar, (i & 2) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i & 4) != 0 ? false : z, (i & 8) != 0 ? false : z2, (i & 16) == 0 ? z3 : false);
    }

    public final List<Avatar> getSelectableAvatars() {
        return this.selectableAvatars;
    }

    public final boolean getCanSave() {
        return this.canSave;
    }

    public final boolean getCanClear() {
        return this.canClear;
    }

    public final boolean isCleared() {
        return this.isCleared;
    }
}
