package org.thoughtcrime.securesms.avatar.vector;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: VectorAvatarCreationViewModel.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0011B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\f\u001a\u00020\u0003J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/vector/VectorAvatarCreationViewModel;", "Landroidx/lifecycle/ViewModel;", "initialAvatar", "Lorg/thoughtcrime/securesms/avatar/Avatar$Vector;", "(Lorg/thoughtcrime/securesms/avatar/Avatar$Vector;)V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/avatar/vector/VectorAvatarCreationState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "getCurrentAvatar", "setColor", "", "colorPair", "Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class VectorAvatarCreationViewModel extends ViewModel {
    private final LiveData<VectorAvatarCreationState> state;
    private final Store<VectorAvatarCreationState> store;

    public VectorAvatarCreationViewModel(Avatar.Vector vector) {
        Intrinsics.checkNotNullParameter(vector, "initialAvatar");
        Store<VectorAvatarCreationState> store = new Store<>(new VectorAvatarCreationState(vector));
        this.store = store;
        LiveData<VectorAvatarCreationState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<VectorAvatarCreationState> getState() {
        return this.state;
    }

    /* renamed from: setColor$lambda-0 */
    public static final VectorAvatarCreationState m360setColor$lambda0(Avatars.ColorPair colorPair, VectorAvatarCreationState vectorAvatarCreationState) {
        Intrinsics.checkNotNullParameter(colorPair, "$colorPair");
        return vectorAvatarCreationState.copy(Avatar.Vector.copy$default(vectorAvatarCreationState.getCurrentAvatar(), null, colorPair, null, 5, null));
    }

    public final void setColor(Avatars.ColorPair colorPair) {
        Intrinsics.checkNotNullParameter(colorPair, "colorPair");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.avatar.vector.VectorAvatarCreationViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return VectorAvatarCreationViewModel.m360setColor$lambda0(Avatars.ColorPair.this, (VectorAvatarCreationState) obj);
            }
        });
    }

    public final Avatar.Vector getCurrentAvatar() {
        return this.store.getState().getCurrentAvatar();
    }

    /* compiled from: VectorAvatarCreationViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/vector/VectorAvatarCreationViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "initialAvatar", "Lorg/thoughtcrime/securesms/avatar/Avatar$Vector;", "(Lorg/thoughtcrime/securesms/avatar/Avatar$Vector;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Avatar.Vector initialAvatar;

        public Factory(Avatar.Vector vector) {
            Intrinsics.checkNotNullParameter(vector, "initialAvatar");
            this.initialAvatar = vector;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new VectorAvatarCreationViewModel(this.initialAvatar));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
