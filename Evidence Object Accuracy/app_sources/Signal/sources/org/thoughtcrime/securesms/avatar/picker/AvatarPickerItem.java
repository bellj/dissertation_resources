package org.thoughtcrime.securesms.avatar.picker;

import android.content.Context;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.content.res.AppCompatResources;
import com.airbnb.lottie.SimpleColorFilter;
import j$.util.function.Function;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.AvatarRenderer;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: AvatarPickerItem.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0010\u0011B\u0007\b\u0002¢\u0006\u0002\u0010\u0002JJ\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u001c\u0010\b\u001a\u0018\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00050\tj\u0002`\f2\u001c\u0010\r\u001a\u0018\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tj\u0002`\u000fR\u000e\u0010\u0003\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerItem;", "", "()V", "SELECTION_CHANGED", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "onAvatarClickListener", "Lkotlin/Function2;", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "", "Lorg/thoughtcrime/securesms/avatar/picker/OnAvatarClickListener;", "onAvatarLongClickListener", "Landroid/view/View;", "Lorg/thoughtcrime/securesms/avatar/picker/OnAvatarLongClickListener;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarPickerItem {
    public static final AvatarPickerItem INSTANCE = new AvatarPickerItem();
    private static final Object SELECTION_CHANGED = new Object();

    private AvatarPickerItem() {
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m303register$lambda0(Function2 function2, Function2 function22, View view) {
        Intrinsics.checkNotNullParameter(function2, "$onAvatarClickListener");
        Intrinsics.checkNotNullParameter(function22, "$onAvatarLongClickListener");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view, function2, function22);
    }

    public final void register(MappingAdapter mappingAdapter, Function2<? super Avatar, ? super Boolean, Unit> function2, Function2<? super View, ? super Avatar, Boolean> function22) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        Intrinsics.checkNotNullParameter(function2, "onAvatarClickListener");
        Intrinsics.checkNotNullParameter(function22, "onAvatarLongClickListener");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function(function22) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerItem$$ExternalSyntheticLambda0
            public final /* synthetic */ Function2 f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return AvatarPickerItem.m303register$lambda0(Function2.this, this.f$1, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.avatar_picker_item));
    }

    /* compiled from: AvatarPickerItem.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u0000\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u0000H\u0016J\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u0000H\u0016J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000b\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\t¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerItem$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "avatar", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "isSelected", "", "(Lorg/thoughtcrime/securesms/avatar/Avatar;Z)V", "getAvatar", "()Lorg/thoughtcrime/securesms/avatar/Avatar;", "()Z", "areContentsTheSame", "newItem", "areItemsTheSame", "getChangePayload", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Model implements MappingModel<Model> {
        private final Avatar avatar;
        private final boolean isSelected;

        public Model(Avatar avatar, boolean z) {
            Intrinsics.checkNotNullParameter(avatar, "avatar");
            this.avatar = avatar;
            this.isSelected = z;
        }

        public final Avatar getAvatar() {
            return this.avatar;
        }

        public final boolean isSelected() {
            return this.isSelected;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return this.avatar.isSameAs(model.avatar);
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.avatar, model.avatar) && this.isSelected == model.isSelected;
        }

        public Object getChangePayload(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            if (!Intrinsics.areEqual(model.avatar, this.avatar) || this.isSelected == model.isSelected) {
                return null;
            }
            return AvatarPickerItem.SELECTION_CHANGED;
        }
    }

    /* compiled from: AvatarPickerItem.kt */
    @Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001BU\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\"\b\u0002\u0010\u0005\u001a\u001c\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0006j\u0004\u0018\u0001`\n\u0012\"\b\u0002\u0010\u000b\u001a\u001c\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b\u0018\u00010\u0006j\u0004\u0018\u0001`\f¢\u0006\u0002\u0010\rJ\u0010\u0010\u0014\u001a\u00020\t2\u0006\u0010\u0015\u001a\u00020\u0002H\u0016J\u0010\u0010\u0016\u001a\u00020\t2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R(\u0010\u0005\u001a\u001c\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0006j\u0004\u0018\u0001`\nX\u0004¢\u0006\u0002\n\u0000R(\u0010\u000b\u001a\u001c\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b\u0018\u00010\u0006j\u0004\u0018\u0001`\fX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerItem$Model;", "itemView", "Landroid/view/View;", "onAvatarClickListener", "Lkotlin/Function2;", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "", "", "Lorg/thoughtcrime/securesms/avatar/picker/OnAvatarClickListener;", "onAvatarLongClickListener", "Lorg/thoughtcrime/securesms/avatar/picker/OnAvatarLongClickListener;", "(Landroid/view/View;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;)V", "imageView", "Landroid/widget/ImageView;", "selectedFader", "selectedOverlay", "textView", "Landroid/widget/TextView;", "bind", "model", "updateFontSize", DraftDatabase.Draft.TEXT, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final ImageView imageView;
        private final Function2<Avatar, Boolean, Unit> onAvatarClickListener;
        private final Function2<View, Avatar, Boolean> onAvatarLongClickListener;
        private final View selectedFader;
        private final View selectedOverlay;
        private final TextView textView;

        public /* synthetic */ ViewHolder(View view, Function2 function2, Function2 function22, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(view, (i & 2) != 0 ? null : function2, (i & 4) != 0 ? null : function22);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function2<? super org.thoughtcrime.securesms.avatar.Avatar, ? super java.lang.Boolean, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function2<? super android.view.View, ? super org.thoughtcrime.securesms.avatar.Avatar, java.lang.Boolean> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view, Function2<? super Avatar, ? super Boolean, Unit> function2, Function2<? super View, ? super Avatar, Boolean> function22) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            this.onAvatarClickListener = function2;
            this.onAvatarLongClickListener = function22;
            View findViewById = view.findViewById(R.id.avatar_picker_item_image);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.…avatar_picker_item_image)");
            this.imageView = (ImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.avatar_picker_item_text);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.….avatar_picker_item_text)");
            TextView textView = (TextView) findViewById2;
            this.textView = textView;
            this.selectedFader = view.findViewById(R.id.avatar_picker_item_fader);
            this.selectedOverlay = view.findViewById(R.id.avatar_picker_item_selection_overlay);
            AvatarRenderer avatarRenderer = AvatarRenderer.INSTANCE;
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            textView.setTypeface(avatarRenderer.getTypeface(context));
            textView.addOnLayoutChangeListener(new AvatarPickerItem$ViewHolder$$ExternalSyntheticLambda0(this));
        }

        /* renamed from: _init_$lambda-0 */
        public static final void m307_init_$lambda0(ViewHolder viewHolder, View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            viewHolder.updateFontSize(viewHolder.textView.getText().toString());
        }

        private final void updateFontSize(String str) {
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            this.textView.setTextSize(0, Avatars.getTextSizeForLength(context, str, ((float) this.textView.getMeasuredWidth()) * 0.8f, ((float) this.textView.getMeasuredHeight()) * 0.45f));
            TextView textView = this.textView;
            if (!(textView instanceof EditText)) {
                textView.setText(str);
            }
        }

        public void bind(Model model) {
            ViewPropertyAnimator animate;
            ViewPropertyAnimator animate2;
            ViewPropertyAnimator animate3;
            ViewPropertyAnimator animate4;
            Intrinsics.checkNotNullParameter(model, "model");
            float f = 1.0f;
            float f2 = model.isSelected() ? 1.0f : 0.0f;
            if (model.isSelected()) {
                f = 0.9f;
            }
            this.imageView.animate().cancel();
            this.textView.animate().cancel();
            View view = this.selectedOverlay;
            if (!(view == null || (animate4 = view.animate()) == null)) {
                animate4.cancel();
            }
            View view2 = this.selectedFader;
            if (!(view2 == null || (animate3 = view2.animate()) == null)) {
                animate3.cancel();
            }
            this.itemView.setOnLongClickListener(new AvatarPickerItem$ViewHolder$$ExternalSyntheticLambda1(this, model));
            this.itemView.setOnClickListener(new AvatarPickerItem$ViewHolder$$ExternalSyntheticLambda2(this, model));
            List<Object> list = this.payload;
            Intrinsics.checkNotNullExpressionValue(list, "payload");
            if (!(!list.isEmpty()) || !this.payload.contains(AvatarPickerItem.SELECTION_CHANGED)) {
                this.imageView.setScaleX(f);
                this.imageView.setScaleY(f);
                this.textView.setScaleX(f);
                this.textView.setScaleY(f);
                View view3 = this.selectedFader;
                if (view3 != null) {
                    view3.setAlpha(f2);
                }
                View view4 = this.selectedOverlay;
                if (view4 != null) {
                    view4.setAlpha(f2);
                }
                this.imageView.clearColorFilter();
                this.imageView.setPadding(0, 0, 0, 0);
                Avatar avatar = model.getAvatar();
                if (avatar instanceof Avatar.Text) {
                    ViewExtensionsKt.setVisible(this.textView, true);
                    updateFontSize(((Avatar.Text) model.getAvatar()).getText());
                    if (!Intrinsics.areEqual(this.textView.getText().toString(), ((Avatar.Text) model.getAvatar()).getText())) {
                        this.textView.setText(((Avatar.Text) model.getAvatar()).getText());
                    }
                    this.imageView.setImageDrawable(null);
                    this.imageView.getBackground().setColorFilter(new SimpleColorFilter(((Avatar.Text) model.getAvatar()).getColor().getBackgroundColor()));
                    this.textView.setTextColor(((Avatar.Text) model.getAvatar()).getColor().getForegroundColor());
                } else if (avatar instanceof Avatar.Vector) {
                    ViewExtensionsKt.setVisible(this.textView, false);
                    Integer drawableResource = Avatars.INSTANCE.getDrawableResource(((Avatar.Vector) model.getAvatar()).getKey());
                    if (drawableResource == null) {
                        this.imageView.setImageDrawable(null);
                    } else {
                        this.imageView.setImageDrawable(AppCompatResources.getDrawable(this.context, drawableResource.intValue()));
                    }
                    this.imageView.getBackground().setColorFilter(new SimpleColorFilter(((Avatar.Vector) model.getAvatar()).getColor().getBackgroundColor()));
                } else if (avatar instanceof Avatar.Photo) {
                    ViewExtensionsKt.setVisible(this.textView, false);
                    GlideApp.with(this.imageView).load((Object) new DecryptableStreamUriLoader.DecryptableUri(((Avatar.Photo) model.getAvatar()).getUri())).into(this.imageView);
                } else if (avatar instanceof Avatar.Resource) {
                    ImageView imageView = this.imageView;
                    double width = (double) imageView.getWidth();
                    Double.isNaN(width);
                    int i = (int) (width * 0.2d);
                    imageView.setPadding(i, i, i, i);
                    ViewExtensionsKt.setVisible(this.textView, false);
                    GlideApp.with(this.imageView).clear(this.imageView);
                    this.imageView.setImageResource(((Avatar.Resource) model.getAvatar()).getResourceId());
                    this.imageView.setColorFilter(new SimpleColorFilter(((Avatar.Resource) model.getAvatar()).getColor().getForegroundColor()));
                    this.imageView.getBackground().setColorFilter(new SimpleColorFilter(((Avatar.Resource) model.getAvatar()).getColor().getBackgroundColor()));
                }
            } else {
                this.imageView.animate().scaleX(f).scaleY(f);
                this.textView.animate().scaleX(f).scaleY(f);
                View view5 = this.selectedOverlay;
                if (!(view5 == null || (animate2 = view5.animate()) == null)) {
                    animate2.alpha(f2);
                }
                View view6 = this.selectedFader;
                if (view6 != null && (animate = view6.animate()) != null) {
                    animate.alpha(f2);
                }
            }
        }

        /* renamed from: bind$lambda-1 */
        public static final boolean m308bind$lambda1(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            Function2<View, Avatar, Boolean> function2 = viewHolder.onAvatarLongClickListener;
            if (function2 == null) {
                return false;
            }
            View view2 = viewHolder.itemView;
            Intrinsics.checkNotNullExpressionValue(view2, "itemView");
            return function2.invoke(view2, model.getAvatar()).booleanValue();
        }

        /* renamed from: bind$lambda-2 */
        public static final void m309bind$lambda2(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            Function2<Avatar, Boolean, Unit> function2 = viewHolder.onAvatarClickListener;
            if (function2 != null) {
                function2.invoke(model.getAvatar(), Boolean.valueOf(model.isSelected()));
            }
        }
    }
}
