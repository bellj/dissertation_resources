package org.thoughtcrime.securesms.avatar.photo;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;

/* loaded from: classes.dex */
public class PhotoEditorFragmentArgs {
    private final HashMap arguments;

    private PhotoEditorFragmentArgs() {
        this.arguments = new HashMap();
    }

    private PhotoEditorFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static PhotoEditorFragmentArgs fromBundle(Bundle bundle) {
        PhotoEditorFragmentArgs photoEditorFragmentArgs = new PhotoEditorFragmentArgs();
        bundle.setClassLoader(PhotoEditorFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("photo_avatar")) {
            throw new IllegalArgumentException("Required argument \"photo_avatar\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(Bundle.class) || Serializable.class.isAssignableFrom(Bundle.class)) {
            Bundle bundle2 = (Bundle) bundle.get("photo_avatar");
            if (bundle2 != null) {
                photoEditorFragmentArgs.arguments.put("photo_avatar", bundle2);
                return photoEditorFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"photo_avatar\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(Bundle.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public Bundle getPhotoAvatar() {
        return (Bundle) this.arguments.get("photo_avatar");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("photo_avatar")) {
            Bundle bundle2 = (Bundle) this.arguments.get("photo_avatar");
            if (Parcelable.class.isAssignableFrom(Bundle.class) || bundle2 == null) {
                bundle.putParcelable("photo_avatar", (Parcelable) Parcelable.class.cast(bundle2));
            } else if (Serializable.class.isAssignableFrom(Bundle.class)) {
                bundle.putSerializable("photo_avatar", (Serializable) Serializable.class.cast(bundle2));
            } else {
                throw new UnsupportedOperationException(Bundle.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PhotoEditorFragmentArgs photoEditorFragmentArgs = (PhotoEditorFragmentArgs) obj;
        if (this.arguments.containsKey("photo_avatar") != photoEditorFragmentArgs.arguments.containsKey("photo_avatar")) {
            return false;
        }
        return getPhotoAvatar() == null ? photoEditorFragmentArgs.getPhotoAvatar() == null : getPhotoAvatar().equals(photoEditorFragmentArgs.getPhotoAvatar());
    }

    public int hashCode() {
        return 31 + (getPhotoAvatar() != null ? getPhotoAvatar().hashCode() : 0);
    }

    public String toString() {
        return "PhotoEditorFragmentArgs{photoAvatar=" + getPhotoAvatar() + "}";
    }

    /* loaded from: classes.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(PhotoEditorFragmentArgs photoEditorFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(photoEditorFragmentArgs.arguments);
        }

        public Builder(Bundle bundle) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (bundle != null) {
                hashMap.put("photo_avatar", bundle);
                return;
            }
            throw new IllegalArgumentException("Argument \"photo_avatar\" is marked as non-null but was passed a null value.");
        }

        public PhotoEditorFragmentArgs build() {
            return new PhotoEditorFragmentArgs(this.arguments);
        }

        public Builder setPhotoAvatar(Bundle bundle) {
            if (bundle != null) {
                this.arguments.put("photo_avatar", bundle);
                return this;
            }
            throw new IllegalArgumentException("Argument \"photo_avatar\" is marked as non-null but was passed a null value.");
        }

        public Bundle getPhotoAvatar() {
            return (Bundle) this.arguments.get("photo_avatar");
        }
    }
}
