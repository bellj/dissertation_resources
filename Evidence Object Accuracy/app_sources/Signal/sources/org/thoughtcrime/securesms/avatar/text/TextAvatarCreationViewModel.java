package org.thoughtcrime.securesms.avatar.text;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: TextAvatarCreationViewModel.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\f\u001a\u00020\u0003J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationViewModel;", "Landroidx/lifecycle/ViewModel;", "initialText", "Lorg/thoughtcrime/securesms/avatar/Avatar$Text;", "(Lorg/thoughtcrime/securesms/avatar/Avatar$Text;)V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "getCurrentAvatar", "setColor", "", "colorPair", "Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "setText", DraftDatabase.Draft.TEXT, "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class TextAvatarCreationViewModel extends ViewModel {
    private final LiveData<TextAvatarCreationState> state;
    private final Store<TextAvatarCreationState> store;

    public TextAvatarCreationViewModel(Avatar.Text text) {
        Intrinsics.checkNotNullParameter(text, "initialText");
        Store<TextAvatarCreationState> store = new Store<>(new TextAvatarCreationState(text));
        this.store = store;
        LiveData<TextAvatarCreationState> distinctUntilChanged = Transformations.distinctUntilChanged(store.getStateLiveData());
        Intrinsics.checkNotNullExpressionValue(distinctUntilChanged, "distinctUntilChanged(store.stateLiveData)");
        this.state = distinctUntilChanged;
    }

    public final LiveData<TextAvatarCreationState> getState() {
        return this.state;
    }

    /* renamed from: setColor$lambda-0 */
    public static final TextAvatarCreationState m353setColor$lambda0(Avatars.ColorPair colorPair, TextAvatarCreationState textAvatarCreationState) {
        Intrinsics.checkNotNullParameter(colorPair, "$colorPair");
        return textAvatarCreationState.copy(Avatar.Text.copy$default(textAvatarCreationState.getCurrentAvatar(), null, colorPair, null, 5, null));
    }

    public final void setColor(Avatars.ColorPair colorPair) {
        Intrinsics.checkNotNullParameter(colorPair, "colorPair");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextAvatarCreationViewModel.m353setColor$lambda0(Avatars.ColorPair.this, (TextAvatarCreationState) obj);
            }
        });
    }

    public final void setText(String str) {
        Intrinsics.checkNotNullParameter(str, DraftDatabase.Draft.TEXT);
        this.store.update(new Function(str) { // from class: org.thoughtcrime.securesms.avatar.text.TextAvatarCreationViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextAvatarCreationViewModel.m354setText$lambda1(this.f$0, (TextAvatarCreationState) obj);
            }
        });
    }

    /* renamed from: setText$lambda-1 */
    public static final TextAvatarCreationState m354setText$lambda1(String str, TextAvatarCreationState textAvatarCreationState) {
        Intrinsics.checkNotNullParameter(str, "$text");
        return Intrinsics.areEqual(textAvatarCreationState.getCurrentAvatar().getText(), str) ? textAvatarCreationState : textAvatarCreationState.copy(Avatar.Text.copy$default(textAvatarCreationState.getCurrentAvatar(), str, null, null, 6, null));
    }

    public final Avatar.Text getCurrentAvatar() {
        return this.store.getState().getCurrentAvatar();
    }

    /* compiled from: TextAvatarCreationViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/text/TextAvatarCreationViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "initialText", "Lorg/thoughtcrime/securesms/avatar/Avatar$Text;", "(Lorg/thoughtcrime/securesms/avatar/Avatar$Text;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Avatar.Text initialText;

        public Factory(Avatar.Text text) {
            Intrinsics.checkNotNullParameter(text, "initialText");
            this.initialText = text;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new TextAvatarCreationViewModel(this.initialText));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
