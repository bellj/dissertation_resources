package org.thoughtcrime.securesms.avatar.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: AvatarView.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u000b\u001a\u00020\fJ\u001e\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\r\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u0011J\u000e\u0010\u0014\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u0011J\u0006\u0010\u0015\u001a\u00020\u0013J\b\u0010\u0016\u001a\u00020\fH\u0002J\u000e\u0010\u0017\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\u0019J\u000e\u0010\u001a\u001a\u00020\f2\u0006\u0010\u001b\u001a\u00020\u001cJ\u0010\u0010\u001d\u001a\u00020\f2\u0006\u0010\u001e\u001a\u00020\u0013H\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/view/AvatarView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "avatar", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "storyRing", "Landroid/view/View;", "disableQuickContact", "", "displayChatAvatar", "requestManager", "Lorg/thoughtcrime/securesms/mms/GlideRequests;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "isQuickContactEnabled", "", "displayProfileAvatar", "hasStory", "hideStoryRing", "setFallbackPhotoProvider", "fallbackPhotoProvider", "Lorg/thoughtcrime/securesms/recipients/Recipient$FallbackPhotoProvider;", "setStoryRingFromState", "storyViewState", "Lorg/thoughtcrime/securesms/database/model/StoryViewState;", "showStoryRing", "hasUnreadStory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarView extends FrameLayout {
    private final AvatarImageView avatar;
    private final View storyRing;

    /* compiled from: AvatarView.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[StoryViewState.values().length];
            iArr[StoryViewState.NONE.ordinal()] = 1;
            iArr[StoryViewState.UNVIEWED.ordinal()] = 2;
            iArr[StoryViewState.VIEWED.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public AvatarView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ AvatarView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AvatarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        FrameLayout.inflate(context, R.layout.avatar_view, this);
        setClickable(false);
        View findViewById = findViewById(R.id.avatar_image_view);
        AvatarImageView avatarImageView = (AvatarImageView) findViewById;
        avatarImageView.initialize(context, attributeSet);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById<AvatarImage…alize(context, attrs)\n  }");
        this.avatar = avatarImageView;
        View findViewById2 = findViewById(R.id.avatar_story_ring);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.avatar_story_ring)");
        this.storyRing = findViewById2;
    }

    private final void showStoryRing(boolean z) {
        if (Stories.isFeatureEnabled()) {
            ViewExtensionsKt.setVisible(this.storyRing, true);
            this.storyRing.setActivated(z);
            this.avatar.setScaleX(0.8f);
            this.avatar.setScaleY(0.8f);
        }
    }

    private final void hideStoryRing() {
        ViewExtensionsKt.setVisible(this.storyRing, false);
        this.avatar.setScaleX(1.0f);
        this.avatar.setScaleY(1.0f);
    }

    public final boolean hasStory() {
        return ViewExtensionsKt.getVisible(this.storyRing);
    }

    public final void setStoryRingFromState(StoryViewState storyViewState) {
        Intrinsics.checkNotNullParameter(storyViewState, "storyViewState");
        int i = WhenMappings.$EnumSwitchMapping$0[storyViewState.ordinal()];
        if (i == 1) {
            hideStoryRing();
        } else if (i == 2) {
            showStoryRing(true);
        } else if (i == 3) {
            showStoryRing(false);
        }
    }

    public final void displayChatAvatar(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        this.avatar.setAvatar(recipient);
    }

    public final void displayChatAvatar(GlideRequests glideRequests, Recipient recipient, boolean z) {
        Intrinsics.checkNotNullParameter(glideRequests, "requestManager");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        this.avatar.setAvatar(glideRequests, recipient, z);
    }

    public final void displayProfileAvatar(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        this.avatar.setRecipient(recipient);
    }

    public final void setFallbackPhotoProvider(Recipient.FallbackPhotoProvider fallbackPhotoProvider) {
        Intrinsics.checkNotNullParameter(fallbackPhotoProvider, "fallbackPhotoProvider");
        this.avatar.setFallbackPhotoProvider(fallbackPhotoProvider);
    }

    public final void disableQuickContact() {
        this.avatar.disableQuickContact();
    }
}
