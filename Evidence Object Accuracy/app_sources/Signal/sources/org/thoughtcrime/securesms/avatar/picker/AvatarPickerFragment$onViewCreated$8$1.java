package org.thoughtcrime.securesms.avatar.picker;

import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.FragmentKt;
import androidx.navigation.Navigation;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.mediasend.Media;

/* compiled from: AvatarPickerFragment.kt */
/* access modifiers changed from: package-private */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/mediasend/Media;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarPickerFragment$onViewCreated$8$1 extends Lambda implements Function1<Media, Unit> {
    final /* synthetic */ View $v;
    final /* synthetic */ AvatarPickerFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AvatarPickerFragment$onViewCreated$8$1(AvatarPickerFragment avatarPickerFragment, View view) {
        super(1);
        this.this$0 = avatarPickerFragment;
        this.$v = view;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Media media) {
        invoke(media);
        return Unit.INSTANCE;
    }

    public final void invoke(Media media) {
        Intrinsics.checkNotNullParameter(media, "it");
        AvatarPickerFragment avatarPickerFragment = this.this$0;
        Bundle bundle = new Bundle();
        bundle.putParcelable(AvatarPickerFragment.SELECT_AVATAR_MEDIA, media);
        Unit unit = Unit.INSTANCE;
        FragmentKt.setFragmentResult(avatarPickerFragment, AvatarPickerFragment.REQUEST_KEY_SELECT_AVATAR, bundle);
        ThreadUtil.runOnMain(new Runnable(this.$v) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$onViewCreated$8$1$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerFragment$onViewCreated$8$1.m300invoke$lambda1(this.f$0);
            }
        });
    }

    /* renamed from: invoke$lambda-1 */
    public static final void m300invoke$lambda1(View view) {
        Navigation.findNavController(view).popBackStack();
    }
}
