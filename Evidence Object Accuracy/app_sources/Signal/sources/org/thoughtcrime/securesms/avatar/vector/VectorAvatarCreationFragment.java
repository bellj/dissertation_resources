package org.thoughtcrime.securesms.avatar.vector;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentKt;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import com.airbnb.lottie.SimpleColorFilter;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.AvatarBundler;
import org.thoughtcrime.securesms.avatar.AvatarColorItem;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.avatar.vector.VectorAvatarCreationViewModel;
import org.thoughtcrime.securesms.components.recyclerview.GridDividerDecoration;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;

/* compiled from: VectorAvatarCreationFragment.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u00020\nH\u0002J\u001a\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/vector/VectorAvatarCreationFragment;", "Landroidx/fragment/app/Fragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/avatar/vector/VectorAvatarCreationViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/avatar/vector/VectorAvatarCreationViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "createFactory", "Lorg/thoughtcrime/securesms/avatar/vector/VectorAvatarCreationViewModel$Factory;", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class VectorAvatarCreationFragment extends Fragment {
    public static final Companion Companion = new Companion(null);
    public static final String REQUEST_KEY_VECTOR;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(VectorAvatarCreationViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.avatar.vector.VectorAvatarCreationFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.avatar.vector.VectorAvatarCreationFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<VectorAvatarCreationViewModel.Factory>(this) { // from class: org.thoughtcrime.securesms.avatar.vector.VectorAvatarCreationFragment$viewModel$2
        @Override // kotlin.jvm.functions.Function0
        public final VectorAvatarCreationViewModel.Factory invoke() {
            return VectorAvatarCreationFragment.access$createFactory((VectorAvatarCreationFragment) this.receiver);
        }
    });

    public VectorAvatarCreationFragment() {
        super(R.layout.vector_avatar_creation_fragment);
    }

    public final VectorAvatarCreationViewModel getViewModel() {
        return (VectorAvatarCreationViewModel) this.viewModel$delegate.getValue();
    }

    public final VectorAvatarCreationViewModel.Factory createFactory() {
        VectorAvatarCreationFragmentArgs fromBundle = VectorAvatarCreationFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        Bundle vectorAvatar = fromBundle.getVectorAvatar();
        Intrinsics.checkNotNullExpressionValue(vectorAvatar, "args.vectorAvatar");
        return new VectorAvatarCreationViewModel.Factory(AvatarBundler.INSTANCE.extractVector(vectorAvatar));
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.vector_avatar_creation_toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.v…_avatar_creation_toolbar)");
        View findViewById2 = view.findViewById(R.id.vector_avatar_creation_recycler);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.v…avatar_creation_recycler)");
        RecyclerView recyclerView = (RecyclerView) findViewById2;
        View findViewById3 = view.findViewById(R.id.vector_avatar_creation_done);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.v…tor_avatar_creation_done)");
        View findViewById4 = view.findViewById(R.id.vector_avatar_creation_image);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.v…or_avatar_creation_image)");
        MappingAdapter mappingAdapter = new MappingAdapter();
        recyclerView.setAdapter(mappingAdapter);
        recyclerView.addItemDecoration(new GridDividerDecoration(4, ViewUtil.dpToPx(16)));
        AvatarColorItem.Companion.registerViewHolder(mappingAdapter, new Function1<Avatars.ColorPair, Unit>(this) { // from class: org.thoughtcrime.securesms.avatar.vector.VectorAvatarCreationFragment$onViewCreated$1
            final /* synthetic */ VectorAvatarCreationFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Avatars.ColorPair colorPair) {
                invoke(colorPair);
                return Unit.INSTANCE;
            }

            public final void invoke(Avatars.ColorPair colorPair) {
                Intrinsics.checkNotNullParameter(colorPair, "it");
                VectorAvatarCreationFragment.access$getViewModel(this.this$0).setColor(colorPair);
            }
        });
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer((ImageView) findViewById4, mappingAdapter) { // from class: org.thoughtcrime.securesms.avatar.vector.VectorAvatarCreationFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ ImageView f$0;
            public final /* synthetic */ MappingAdapter f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                VectorAvatarCreationFragment.m356$r8$lambda$XTcCalpJp0VGysjWEWo3DnIIJ0(this.f$0, this.f$1, (VectorAvatarCreationState) obj);
            }
        });
        ((Toolbar) findViewById).setNavigationOnClickListener(new View.OnClickListener(view) { // from class: org.thoughtcrime.securesms.avatar.vector.VectorAvatarCreationFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                VectorAvatarCreationFragment.m355$r8$lambda$NSjOiZdZu6mXNycU1LgxzOxhk4(this.f$0, view2);
            }
        });
        findViewById3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.avatar.vector.VectorAvatarCreationFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                VectorAvatarCreationFragment.$r8$lambda$X6ZkCx4EOLfrWbdRdmDHaXRWGek(VectorAvatarCreationFragment.this, view2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m357onViewCreated$lambda1(ImageView imageView, MappingAdapter mappingAdapter, VectorAvatarCreationState vectorAvatarCreationState) {
        Intrinsics.checkNotNullParameter(imageView, "$preview");
        Intrinsics.checkNotNullParameter(mappingAdapter, "$adapter");
        imageView.getBackground().setColorFilter(new SimpleColorFilter(vectorAvatarCreationState.getCurrentAvatar().getColor().getBackgroundColor()));
        Integer drawableResource = Avatars.INSTANCE.getDrawableResource(vectorAvatarCreationState.getCurrentAvatar().getKey());
        if (drawableResource != null) {
            imageView.setImageResource(drawableResource.intValue());
            List<AvatarColorItem> colors = vectorAvatarCreationState.colors();
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(colors, 10));
            for (AvatarColorItem avatarColorItem : colors) {
                arrayList.add(new AvatarColorItem.Model(avatarColorItem));
            }
            mappingAdapter.submitList(arrayList);
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m358onViewCreated$lambda2(View view, View view2) {
        Intrinsics.checkNotNullParameter(view, "$view");
        Navigation.findNavController(view).popBackStack();
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m359onViewCreated$lambda3(VectorAvatarCreationFragment vectorAvatarCreationFragment, View view) {
        Intrinsics.checkNotNullParameter(vectorAvatarCreationFragment, "this$0");
        FragmentKt.setFragmentResult(vectorAvatarCreationFragment, REQUEST_KEY_VECTOR, AvatarBundler.INSTANCE.bundleVector(vectorAvatarCreationFragment.getViewModel().getCurrentAvatar()));
        Navigation.findNavController(view).popBackStack();
    }

    /* compiled from: VectorAvatarCreationFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/vector/VectorAvatarCreationFragment$Companion;", "", "()V", "REQUEST_KEY_VECTOR", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
