package org.thoughtcrime.securesms.avatar;

import android.content.Context;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.AvatarPickerDatabase;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.storage.FileStorage;

/* compiled from: AvatarPickerStorage.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0007J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u0004H\u0007J\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u000bJ\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\u0011R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/AvatarPickerStorage;", "", "()V", "DIRECTORY", "", "FILENAME_BASE", "cleanOrphans", "", "context", "Landroid/content/Context;", "read", "Ljava/io/InputStream;", "fileName", "save", "Landroid/net/Uri;", "inputStream", "media", "Lorg/thoughtcrime/securesms/mediasend/Media;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarPickerStorage {
    private static final String DIRECTORY;
    private static final String FILENAME_BASE;
    public static final AvatarPickerStorage INSTANCE = new AvatarPickerStorage();

    private AvatarPickerStorage() {
    }

    @JvmStatic
    public static final InputStream read(Context context, String str) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(str, "fileName");
        InputStream read = FileStorage.read(context, "avatar_picker", str);
        Intrinsics.checkNotNullExpressionValue(read, "read(context, DIRECTORY, fileName)");
        return read;
    }

    public final Uri save(Context context, Media media) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(media, "media");
        InputStream attachmentStream = PartAuthority.getAttachmentStream(context, media.getUri());
        String extension = MediaUtil.getExtension(context, media.getUri());
        if (extension == null) {
            extension = "";
        }
        String save = FileStorage.save(context, attachmentStream, "avatar_picker", FILENAME_BASE, extension);
        Intrinsics.checkNotNullExpressionValue(save, "save(context, PartAuthor…ontext, media.uri) ?: \"\")");
        Uri avatarPickerUri = PartAuthority.getAvatarPickerUri(save);
        Intrinsics.checkNotNullExpressionValue(avatarPickerUri, "getAvatarPickerUri(fileName)");
        return avatarPickerUri;
    }

    public final Uri save(Context context, InputStream inputStream) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(inputStream, "inputStream");
        String extensionFromMimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(MediaUtil.IMAGE_JPEG);
        if (extensionFromMimeType == null) {
            extensionFromMimeType = "";
        }
        String save = FileStorage.save(context, inputStream, "avatar_picker", FILENAME_BASE, extensionFromMimeType);
        Intrinsics.checkNotNullExpressionValue(save, "save(context, inputStrea…iaUtil.IMAGE_JPEG) ?: \"\")");
        Uri avatarPickerUri = PartAuthority.getAvatarPickerUri(save);
        Intrinsics.checkNotNullExpressionValue(avatarPickerUri, "getAvatarPickerUri(fileName)");
        return avatarPickerUri;
    }

    @JvmStatic
    public static final void cleanOrphans(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        List<File> allFiles = FileStorage.getAllFiles(context, "avatar_picker", FILENAME_BASE);
        Intrinsics.checkNotNullExpressionValue(allFiles, "getAllFiles(context, DIRECTORY, FILENAME_BASE)");
        AvatarPickerDatabase avatarPicker = SignalDatabase.Companion.avatarPicker();
        List<Avatar> allAvatars = avatarPicker.getAllAvatars();
        ArrayList<Avatar.Photo> arrayList = new ArrayList();
        for (Object obj : allAvatars) {
            if (obj instanceof Avatar.Photo) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        for (Avatar.Photo photo : arrayList) {
            arrayList2.add(PartAuthority.getAvatarPickerFilename(photo.getUri()));
        }
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(allFiles, 10));
        for (File file : allFiles) {
            arrayList3.add(file.getName());
        }
        List list = CollectionsKt___CollectionsKt.minus((Iterable) arrayList2, (Iterable) arrayList3);
        List list2 = CollectionsKt___CollectionsKt.minus((Iterable) arrayList3, (Iterable) arrayList2);
        ArrayList<File> arrayList4 = new ArrayList();
        for (Object obj2 : allFiles) {
            if (list2.contains(((File) obj2).getName())) {
                arrayList4.add(obj2);
            }
        }
        for (File file2 : arrayList4) {
            file2.delete();
        }
        ArrayList<Avatar.Photo> arrayList5 = new ArrayList();
        for (Object obj3 : arrayList) {
            if (list.contains(PartAuthority.getAvatarPickerFilename(((Avatar.Photo) obj3).getUri()))) {
                arrayList5.add(obj3);
            }
        }
        for (Avatar.Photo photo2 : arrayList5) {
            avatarPicker.deleteAvatar(photo2);
        }
    }
}
