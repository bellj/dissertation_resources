package org.thoughtcrime.securesms.avatar.vector;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;

/* loaded from: classes.dex */
public class VectorAvatarCreationFragmentArgs {
    private final HashMap arguments;

    private VectorAvatarCreationFragmentArgs() {
        this.arguments = new HashMap();
    }

    private VectorAvatarCreationFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static VectorAvatarCreationFragmentArgs fromBundle(Bundle bundle) {
        VectorAvatarCreationFragmentArgs vectorAvatarCreationFragmentArgs = new VectorAvatarCreationFragmentArgs();
        bundle.setClassLoader(VectorAvatarCreationFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("vector_avatar")) {
            throw new IllegalArgumentException("Required argument \"vector_avatar\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(Bundle.class) || Serializable.class.isAssignableFrom(Bundle.class)) {
            Bundle bundle2 = (Bundle) bundle.get("vector_avatar");
            if (bundle2 != null) {
                vectorAvatarCreationFragmentArgs.arguments.put("vector_avatar", bundle2);
                return vectorAvatarCreationFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"vector_avatar\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(Bundle.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public Bundle getVectorAvatar() {
        return (Bundle) this.arguments.get("vector_avatar");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("vector_avatar")) {
            Bundle bundle2 = (Bundle) this.arguments.get("vector_avatar");
            if (Parcelable.class.isAssignableFrom(Bundle.class) || bundle2 == null) {
                bundle.putParcelable("vector_avatar", (Parcelable) Parcelable.class.cast(bundle2));
            } else if (Serializable.class.isAssignableFrom(Bundle.class)) {
                bundle.putSerializable("vector_avatar", (Serializable) Serializable.class.cast(bundle2));
            } else {
                throw new UnsupportedOperationException(Bundle.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        VectorAvatarCreationFragmentArgs vectorAvatarCreationFragmentArgs = (VectorAvatarCreationFragmentArgs) obj;
        if (this.arguments.containsKey("vector_avatar") != vectorAvatarCreationFragmentArgs.arguments.containsKey("vector_avatar")) {
            return false;
        }
        return getVectorAvatar() == null ? vectorAvatarCreationFragmentArgs.getVectorAvatar() == null : getVectorAvatar().equals(vectorAvatarCreationFragmentArgs.getVectorAvatar());
    }

    public int hashCode() {
        return 31 + (getVectorAvatar() != null ? getVectorAvatar().hashCode() : 0);
    }

    public String toString() {
        return "VectorAvatarCreationFragmentArgs{vectorAvatar=" + getVectorAvatar() + "}";
    }

    /* loaded from: classes.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(VectorAvatarCreationFragmentArgs vectorAvatarCreationFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(vectorAvatarCreationFragmentArgs.arguments);
        }

        public Builder(Bundle bundle) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (bundle != null) {
                hashMap.put("vector_avatar", bundle);
                return;
            }
            throw new IllegalArgumentException("Argument \"vector_avatar\" is marked as non-null but was passed a null value.");
        }

        public VectorAvatarCreationFragmentArgs build() {
            return new VectorAvatarCreationFragmentArgs(this.arguments);
        }

        public Builder setVectorAvatar(Bundle bundle) {
            if (bundle != null) {
                this.arguments.put("vector_avatar", bundle);
                return this;
            }
            throw new IllegalArgumentException("Argument \"vector_avatar\" is marked as non-null but was passed a null value.");
        }

        public Bundle getVectorAvatar() {
            return (Bundle) this.arguments.get("vector_avatar");
        }
    }
}
