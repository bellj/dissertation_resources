package org.thoughtcrime.securesms.avatar.picker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentKt;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.AvatarBundler;
import org.thoughtcrime.securesms.avatar.photo.PhotoEditorFragment;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragmentDirections;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerItem;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel;
import org.thoughtcrime.securesms.avatar.text.TextAvatarCreationFragment;
import org.thoughtcrime.securesms.avatar.vector.VectorAvatarCreationFragment;
import org.thoughtcrime.securesms.components.ButtonStripItemView;
import org.thoughtcrime.securesms.components.recyclerview.GridDividerDecoration;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.mediasend.AvatarSelectionActivity;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: AvatarPickerFragment.kt */
@Metadata(d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 42\u00020\u0001:\u00014B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u00020\fH\u0002J\"\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u0018\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0018\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J-\u0010\u001c\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u000e\u0010\u001d\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u001f0\u001e2\u0006\u0010 \u001a\u00020!H\u0016¢\u0006\u0002\u0010\"J\b\u0010#\u001a\u00020\u000eH\u0016J\u001a\u0010$\u001a\u00020\u000e2\u0006\u0010%\u001a\u00020\u001b2\b\u0010&\u001a\u0004\u0018\u00010'H\u0016J\b\u0010(\u001a\u00020\u000eH\u0002J\u000e\u0010)\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0016J\b\u0010*\u001a\u00020\u000eH\u0002J\u0010\u0010+\u001a\u00020\u000e2\u0006\u0010,\u001a\u00020-H\u0002J\u0012\u0010.\u001a\u00020\u000e2\b\u0010/\u001a\u0004\u0018\u000100H\u0002J\u0010\u00101\u001a\u00020\u000e2\u0006\u00102\u001a\u000203H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u00065"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerFragment;", "Landroidx/fragment/app/Fragment;", "()V", "recycler", "Landroidx/recyclerview/widget/RecyclerView;", "viewModel", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "createFactory", "Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerViewModel$Factory;", "onActivityResult", "", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onAvatarClick", "avatar", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "isSelected", "", "onAvatarLongClick", "anchorView", "Landroid/view/View;", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "onViewCreated", "view", "savedInstanceState", "Landroid/os/Bundle;", "openCameraCapture", "openEditor", "openGallery", "openPhotoEditor", "photo", "Lorg/thoughtcrime/securesms/avatar/Avatar$Photo;", "openTextEditor", DraftDatabase.Draft.TEXT, "Lorg/thoughtcrime/securesms/avatar/Avatar$Text;", "openVectorEditor", "vector", "Lorg/thoughtcrime/securesms/avatar/Avatar$Vector;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarPickerFragment extends Fragment {
    public static final Companion Companion = new Companion(null);
    private static final int REQUEST_CODE_SELECT_IMAGE;
    public static final String REQUEST_KEY_SELECT_AVATAR;
    public static final String SELECT_AVATAR_CLEAR;
    public static final String SELECT_AVATAR_MEDIA;
    private RecyclerView recycler;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(AvatarPickerViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<AvatarPickerViewModel.Factory>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$viewModel$2
        @Override // kotlin.jvm.functions.Function0
        public final AvatarPickerViewModel.Factory invoke() {
            return AvatarPickerFragment.access$createFactory((AvatarPickerFragment) this.receiver);
        }
    });

    public AvatarPickerFragment() {
        super(R.layout.avatar_picker_fragment);
    }

    /* compiled from: AvatarPickerFragment.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/picker/AvatarPickerFragment$Companion;", "", "()V", "REQUEST_CODE_SELECT_IMAGE", "", "REQUEST_KEY_SELECT_AVATAR", "", "SELECT_AVATAR_CLEAR", "SELECT_AVATAR_MEDIA", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final AvatarPickerViewModel getViewModel() {
        return (AvatarPickerViewModel) this.viewModel$delegate.getValue();
    }

    public final AvatarPickerViewModel.Factory createFactory() {
        AvatarPickerFragmentArgs fromBundle = AvatarPickerFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        GroupId groupId = ParcelableGroupId.get(fromBundle.getGroupId());
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        return new AvatarPickerViewModel.Factory(new AvatarPickerRepository(requireContext), groupId, fromBundle.getIsNewGroup(), fromBundle.getGroupAvatarMedia());
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.avatar_picker_toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.avatar_picker_toolbar)");
        Toolbar toolbar = (Toolbar) findViewById;
        View findViewById2 = view.findViewById(R.id.avatar_picker_camera);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.avatar_picker_camera)");
        ButtonStripItemView buttonStripItemView = (ButtonStripItemView) findViewById2;
        View findViewById3 = view.findViewById(R.id.avatar_picker_photo);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.avatar_picker_photo)");
        ButtonStripItemView buttonStripItemView2 = (ButtonStripItemView) findViewById3;
        View findViewById4 = view.findViewById(R.id.avatar_picker_text);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.avatar_picker_text)");
        ButtonStripItemView buttonStripItemView3 = (ButtonStripItemView) findViewById4;
        View findViewById5 = view.findViewById(R.id.avatar_picker_save);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.avatar_picker_save)");
        View findViewById6 = view.findViewById(R.id.avatar_picker_clear);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.avatar_picker_clear)");
        View findViewById7 = view.findViewById(R.id.avatar_picker_recycler);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.avatar_picker_recycler)");
        RecyclerView recyclerView = (RecyclerView) findViewById7;
        this.recycler = recyclerView;
        RecyclerView recyclerView2 = null;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("recycler");
            recyclerView = null;
        }
        recyclerView.addItemDecoration(new GridDividerDecoration(4, ViewUtil.dpToPx(16)));
        MappingAdapter mappingAdapter = new MappingAdapter();
        AvatarPickerItem.INSTANCE.register(mappingAdapter, new Function2<Avatar, Boolean, Unit>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$onViewCreated$1
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((Avatar) obj, ((Boolean) obj2).booleanValue());
                return Unit.INSTANCE;
            }

            public final void invoke(Avatar avatar, boolean z) {
                Intrinsics.checkNotNullParameter(avatar, "p0");
                AvatarPickerFragment.access$onAvatarClick((AvatarPickerFragment) this.receiver, avatar, z);
            }
        }, new Function2<View, Avatar, Boolean>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$onViewCreated$2
            public final Boolean invoke(View view2, Avatar avatar) {
                Intrinsics.checkNotNullParameter(view2, "p0");
                Intrinsics.checkNotNullParameter(avatar, "p1");
                return Boolean.valueOf(AvatarPickerFragment.access$onAvatarLongClick((AvatarPickerFragment) this.receiver, view2, avatar));
            }
        });
        RecyclerView recyclerView3 = this.recycler;
        if (recyclerView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("recycler");
        } else {
            recyclerView2 = recyclerView3;
        }
        recyclerView2.setAdapter(mappingAdapter);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(findViewById6, findViewById5, mappingAdapter, this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ View f$1;
            public final /* synthetic */ View f$2;
            public final /* synthetic */ MappingAdapter f$3;
            public final /* synthetic */ AvatarPickerFragment f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AvatarPickerFragment.$r8$lambda$DcdHNKtYocdrna3lwzk4_YU1adM(AvatarPickerItem.ViewHolder.this, this.f$1, this.f$2, this.f$3, this.f$4, (AvatarPickerState) obj);
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AvatarPickerFragment.m287$r8$lambda$NiO4Q9y1WVKu0aLkCzOqJsi3ZY(view2);
            }
        });
        buttonStripItemView.setOnIconClickedListener(new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$onViewCreated$5
            final /* synthetic */ AvatarPickerFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                AvatarPickerFragment.access$openCameraCapture(this.this$0);
            }
        });
        buttonStripItemView2.setOnIconClickedListener(new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$onViewCreated$6
            final /* synthetic */ AvatarPickerFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                AvatarPickerFragment.access$openGallery(this.this$0);
            }
        });
        buttonStripItemView3.setOnIconClickedListener(new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$onViewCreated$7
            final /* synthetic */ AvatarPickerFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                AvatarPickerFragment.access$openTextEditor(this.this$0, null);
            }
        });
        findViewById5.setOnClickListener(new View.OnClickListener(findViewById5, this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ View f$0;
            public final /* synthetic */ AvatarPickerFragment f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AvatarPickerFragment.m288$r8$lambda$mssL7soq5mNoro0mrQgpa7Q1rc(this.f$0, this.f$1, view2);
            }
        });
        findViewById6.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AvatarPickerFragment.$r8$lambda$UiKinFkGkzLJCuJL0F3VeFNACLc(AvatarPickerFragment.this, view2);
            }
        });
        FragmentKt.setFragmentResultListener(this, TextAvatarCreationFragment.REQUEST_KEY_TEXT, new Function2<String, Bundle, Unit>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$onViewCreated$10
            final /* synthetic */ AvatarPickerFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((String) obj, (Bundle) obj2);
                return Unit.INSTANCE;
            }

            public final void invoke(String str, Bundle bundle2) {
                Intrinsics.checkNotNullParameter(str, "<anonymous parameter 0>");
                Intrinsics.checkNotNullParameter(bundle2, "bundle");
                AvatarPickerFragment.access$getViewModel(this.this$0).onAvatarEditCompleted(AvatarBundler.INSTANCE.extractText(bundle2));
            }
        });
        FragmentKt.setFragmentResultListener(this, VectorAvatarCreationFragment.REQUEST_KEY_VECTOR, new Function2<String, Bundle, Unit>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$onViewCreated$11
            final /* synthetic */ AvatarPickerFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((String) obj, (Bundle) obj2);
                return Unit.INSTANCE;
            }

            public final void invoke(String str, Bundle bundle2) {
                Intrinsics.checkNotNullParameter(str, "<anonymous parameter 0>");
                Intrinsics.checkNotNullParameter(bundle2, "bundle");
                AvatarPickerFragment.access$getViewModel(this.this$0).onAvatarEditCompleted(AvatarBundler.INSTANCE.extractVector(bundle2));
            }
        });
        FragmentKt.setFragmentResultListener(this, PhotoEditorFragment.REQUEST_KEY_EDIT, new Function2<String, Bundle, Unit>(this) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$onViewCreated$12
            final /* synthetic */ AvatarPickerFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((String) obj, (Bundle) obj2);
                return Unit.INSTANCE;
            }

            public final void invoke(String str, Bundle bundle2) {
                Intrinsics.checkNotNullParameter(str, "<anonymous parameter 0>");
                Intrinsics.checkNotNullParameter(bundle2, "bundle");
                AvatarPickerFragment.access$getViewModel(this.this$0).onAvatarEditCompleted(AvatarBundler.INSTANCE.extractPhoto(bundle2));
            }
        });
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m290onViewCreated$lambda3(AvatarPickerItem.ViewHolder viewHolder, View view, View view2, MappingAdapter mappingAdapter, AvatarPickerFragment avatarPickerFragment, AvatarPickerState avatarPickerState) {
        Intrinsics.checkNotNullParameter(viewHolder, "$avatarViewHolder");
        Intrinsics.checkNotNullParameter(view, "$clearButton");
        Intrinsics.checkNotNullParameter(view2, "$saveButton");
        Intrinsics.checkNotNullParameter(mappingAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(avatarPickerFragment, "this$0");
        int i = 0;
        if (avatarPickerState.getCurrentAvatar() != null) {
            viewHolder.bind(new AvatarPickerItem.Model(avatarPickerState.getCurrentAvatar(), false));
        }
        ViewExtensionsKt.setVisible(view, avatarPickerState.getCanClear());
        view2.setClickable(avatarPickerState.getCanSave());
        List<Avatar> selectableAvatars = avatarPickerState.getSelectableAvatars();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(selectableAvatars, 10));
        for (Avatar avatar : selectableAvatars) {
            arrayList.add(new AvatarPickerItem.Model(avatar, Intrinsics.areEqual(avatar, avatarPickerState.getCurrentAvatar())));
        }
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (((AvatarPickerItem.Model) it.next()).isSelected()) {
                break;
            } else {
                i++;
            }
        }
        mappingAdapter.submitList(arrayList, new Runnable(i, avatarPickerFragment) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$$ExternalSyntheticLambda6
            public final /* synthetic */ int f$0;
            public final /* synthetic */ AvatarPickerFragment f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerFragment.$r8$lambda$Jw7lS8JbQWDChvIEXYUypCEuDXo(this.f$0, this.f$1);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-3$lambda-2 */
    public static final void m291onViewCreated$lambda3$lambda2(int i, AvatarPickerFragment avatarPickerFragment) {
        Intrinsics.checkNotNullParameter(avatarPickerFragment, "this$0");
        if (i > -1) {
            RecyclerView recyclerView = avatarPickerFragment.recycler;
            if (recyclerView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("recycler");
                recyclerView = null;
            }
            recyclerView.smoothScrollToPosition(i);
        }
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m292onViewCreated$lambda4(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    /* renamed from: onViewCreated$lambda-5 */
    public static final void m293onViewCreated$lambda5(View view, AvatarPickerFragment avatarPickerFragment, View view2) {
        Intrinsics.checkNotNullParameter(view, "$saveButton");
        Intrinsics.checkNotNullParameter(avatarPickerFragment, "this$0");
        if (view.isEnabled()) {
            view.setEnabled(false);
            avatarPickerFragment.getViewModel().save(new AvatarPickerFragment$onViewCreated$8$1(avatarPickerFragment, view2), new AvatarPickerFragment$onViewCreated$8$2(avatarPickerFragment, view2));
        }
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m294onViewCreated$lambda6(AvatarPickerFragment avatarPickerFragment, View view) {
        Intrinsics.checkNotNullParameter(avatarPickerFragment, "this$0");
        avatarPickerFragment.getViewModel().clear();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ViewUtil.hideKeyboard(requireContext(), requireView());
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1 && intent != null) {
            Parcelable parcelableExtra = intent.getParcelableExtra(AvatarSelectionActivity.EXTRA_MEDIA);
            if (parcelableExtra != null) {
                getViewModel().onAvatarPhotoSelectionCompleted((Media) parcelableExtra);
                return;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
        super.onActivityResult(i, i2, intent);
    }

    public final void onAvatarClick(Avatar avatar, boolean z) {
        if (z) {
            openEditor(avatar);
        } else {
            getViewModel().onAvatarSelectedFromGrid(avatar);
        }
    }

    public final boolean onAvatarLongClick(View view, Avatar avatar) {
        if ((avatar instanceof Avatar.Photo) || (avatar instanceof Avatar.Text)) {
            PopupMenu popupMenu = new PopupMenu(getContext(), view, 48);
            popupMenu.getMenuInflater().inflate(R.menu.avatar_picker_context, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener(avatar) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$$ExternalSyntheticLambda7
                public final /* synthetic */ Avatar f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.widget.PopupMenu.OnMenuItemClickListener
                public final boolean onMenuItemClick(MenuItem menuItem) {
                    return AvatarPickerFragment.m286$r8$lambda$FCmyOcJa0uGnArs9WbGpd6BXk(AvatarPickerFragment.this, this.f$1, menuItem);
                }
            });
            popupMenu.show();
            return true;
        } else if ((avatar instanceof Avatar.Vector) || (avatar instanceof Avatar.Resource)) {
            return true;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* renamed from: onAvatarLongClick$lambda-7 */
    public static final boolean m289onAvatarLongClick$lambda7(AvatarPickerFragment avatarPickerFragment, Avatar avatar, MenuItem menuItem) {
        Intrinsics.checkNotNullParameter(avatarPickerFragment, "this$0");
        Intrinsics.checkNotNullParameter(avatar, "$avatar");
        if (menuItem.getItemId() != R.id.action_delete) {
            return true;
        }
        avatarPickerFragment.getViewModel().delete(avatar);
        return true;
    }

    public final void openEditor(Avatar avatar) {
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        if (avatar instanceof Avatar.Photo) {
            openPhotoEditor((Avatar.Photo) avatar);
        } else if (avatar instanceof Avatar.Resource) {
            throw new UnsupportedOperationException();
        } else if (avatar instanceof Avatar.Text) {
            openTextEditor((Avatar.Text) avatar);
        } else if (avatar instanceof Avatar.Vector) {
            openVectorEditor((Avatar.Vector) avatar);
        }
    }

    private final void openPhotoEditor(Avatar.Photo photo) {
        NavController findNavController = Navigation.findNavController(requireView());
        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
        AvatarPickerFragmentDirections.ActionAvatarPickerFragmentToAvatarPhotoEditorFragment actionAvatarPickerFragmentToAvatarPhotoEditorFragment = AvatarPickerFragmentDirections.actionAvatarPickerFragmentToAvatarPhotoEditorFragment(AvatarBundler.INSTANCE.bundlePhoto(photo));
        Intrinsics.checkNotNullExpressionValue(actionAvatarPickerFragmentToAvatarPhotoEditorFragment, "actionAvatarPickerFragme…ndler.bundlePhoto(photo))");
        SafeNavigation.safeNavigate(findNavController, actionAvatarPickerFragmentToAvatarPhotoEditorFragment);
    }

    private final void openVectorEditor(Avatar.Vector vector) {
        NavController findNavController = Navigation.findNavController(requireView());
        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
        AvatarPickerFragmentDirections.ActionAvatarPickerFragmentToVectorAvatarCreationFragment actionAvatarPickerFragmentToVectorAvatarCreationFragment = AvatarPickerFragmentDirections.actionAvatarPickerFragmentToVectorAvatarCreationFragment(AvatarBundler.INSTANCE.bundleVector(vector));
        Intrinsics.checkNotNullExpressionValue(actionAvatarPickerFragmentToVectorAvatarCreationFragment, "actionAvatarPickerFragme…ler.bundleVector(vector))");
        SafeNavigation.safeNavigate(findNavController, actionAvatarPickerFragmentToVectorAvatarCreationFragment);
    }

    public final void openTextEditor(Avatar.Text text) {
        Bundle bundleText = text != null ? AvatarBundler.INSTANCE.bundleText(text) : null;
        NavController findNavController = Navigation.findNavController(requireView());
        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
        AvatarPickerFragmentDirections.ActionAvatarPickerFragmentToTextAvatarCreationFragment actionAvatarPickerFragmentToTextAvatarCreationFragment = AvatarPickerFragmentDirections.actionAvatarPickerFragmentToTextAvatarCreationFragment(bundleText);
        Intrinsics.checkNotNullExpressionValue(actionAvatarPickerFragmentToTextAvatarCreationFragment, "actionAvatarPickerFragme…rCreationFragment(bundle)");
        SafeNavigation.safeNavigate(findNavController, actionAvatarPickerFragmentToTextAvatarCreationFragment);
    }

    public final void openCameraCapture() {
        Permissions.with(this).request("android.permission.CAMERA").ifNecessary().onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$$ExternalSyntheticLambda8
            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerFragment.$r8$lambda$qXov4cunGO3MtKWgxV6IChSpsQQ(AvatarPickerFragment.this);
            }
        }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$$ExternalSyntheticLambda9
            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerFragment.$r8$lambda$CQ1DEQR06bXhnjyQ5lNomtXsdU8(AvatarPickerFragment.this);
            }
        }).execute();
    }

    /* renamed from: openCameraCapture$lambda-8 */
    public static final void m295openCameraCapture$lambda8(AvatarPickerFragment avatarPickerFragment) {
        Intrinsics.checkNotNullParameter(avatarPickerFragment, "this$0");
        avatarPickerFragment.startActivityForResult(AvatarSelectionActivity.getIntentForCameraCapture(avatarPickerFragment.requireContext()), 1);
    }

    /* renamed from: openCameraCapture$lambda-9 */
    public static final void m296openCameraCapture$lambda9(AvatarPickerFragment avatarPickerFragment) {
        Intrinsics.checkNotNullParameter(avatarPickerFragment, "this$0");
        Toast.makeText(avatarPickerFragment.requireContext(), (int) R.string.AvatarSelectionBottomSheetDialogFragment__taking_a_photo_requires_the_camera_permission, 0).show();
    }

    public final void openGallery() {
        Permissions.with(this).request("android.permission.READ_EXTERNAL_STORAGE").ifNecessary().onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerFragment.$r8$lambda$1JpkHptGrxsAh9wOfxIAfn0rFGs(AvatarPickerFragment.this);
            }
        }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerFragment.$r8$lambda$lki_XldacYsf4DIOTpWQLpr83QA(AvatarPickerFragment.this);
            }
        }).execute();
    }

    /* renamed from: openGallery$lambda-10 */
    public static final void m297openGallery$lambda10(AvatarPickerFragment avatarPickerFragment) {
        Intrinsics.checkNotNullParameter(avatarPickerFragment, "this$0");
        avatarPickerFragment.startActivityForResult(AvatarSelectionActivity.getIntentForGallery(avatarPickerFragment.requireContext()), 1);
    }

    /* renamed from: openGallery$lambda-11 */
    public static final void m298openGallery$lambda11(AvatarPickerFragment avatarPickerFragment) {
        Intrinsics.checkNotNullParameter(avatarPickerFragment, "this$0");
        Toast.makeText(avatarPickerFragment.requireContext(), (int) R.string.AvatarSelectionBottomSheetDialogFragment__viewing_your_gallery_requires_the_storage_permission, 0).show();
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Intrinsics.checkNotNullParameter(strArr, "permissions");
        Intrinsics.checkNotNullParameter(iArr, "grantResults");
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }
}
