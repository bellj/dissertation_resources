package org.thoughtcrime.securesms.avatar;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: AvatarBundler.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rJ\u000e\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u000bJ\u000e\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u000bJ\u000e\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u000bJ\f\u0010\u0018\u001a\u00020\u0019*\u00020\u000bH\u0002J\u001c\u0010\u001a\u001a\u00020\u001b*\u00020\u000b2\u0006\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u0019H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/AvatarBundler;", "", "()V", "COLOR", "", "DATABASE_ID", "KEY", "SIZE", "TEXT", "URI", "bundlePhoto", "Landroid/os/Bundle;", "photo", "Lorg/thoughtcrime/securesms/avatar/Avatar$Photo;", "bundleText", DraftDatabase.Draft.TEXT, "Lorg/thoughtcrime/securesms/avatar/Avatar$Text;", "bundleVector", "vector", "Lorg/thoughtcrime/securesms/avatar/Avatar$Vector;", "extractPhoto", "bundle", "extractText", "extractVector", "getDatabaseId", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "putDatabaseId", "", "key", "databaseId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarBundler {
    private static final String COLOR;
    private static final String DATABASE_ID;
    public static final AvatarBundler INSTANCE = new AvatarBundler();
    private static final String KEY;
    private static final String SIZE;
    private static final String TEXT;
    private static final String URI;

    private AvatarBundler() {
    }

    public final Bundle bundleText(Avatar.Text text) {
        Intrinsics.checkNotNullParameter(text, DraftDatabase.Draft.TEXT);
        Bundle bundle = new Bundle();
        bundle.putString(TEXT, text.getText());
        bundle.putString(COLOR, text.getColor().getCode());
        INSTANCE.putDatabaseId(bundle, DATABASE_ID, text.getDatabaseId());
        return bundle;
    }

    public final Avatar.Text extractText(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        String string = bundle.getString(TEXT);
        if (string != null) {
            Intrinsics.checkNotNullExpressionValue(string, "requireNotNull(bundle.getString(TEXT))");
            Avatars.ColorPair colorPair = Avatars.INSTANCE.getColorMap().get(bundle.getString(COLOR));
            if (colorPair != null) {
                return new Avatar.Text(string, colorPair, getDatabaseId(bundle));
            }
            throw new IllegalStateException();
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    public final Bundle bundlePhoto(Avatar.Photo photo) {
        Intrinsics.checkNotNullParameter(photo, "photo");
        Bundle bundle = new Bundle();
        bundle.putParcelable(URI, photo.getUri());
        bundle.putLong(SIZE, photo.getSize());
        INSTANCE.putDatabaseId(bundle, DATABASE_ID, photo.getDatabaseId());
        return bundle;
    }

    public final Avatar.Photo extractPhoto(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        Parcelable parcelable = bundle.getParcelable(URI);
        if (parcelable != null) {
            return new Avatar.Photo((Uri) parcelable, bundle.getLong(SIZE), getDatabaseId(bundle));
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    public final Bundle bundleVector(Avatar.Vector vector) {
        Intrinsics.checkNotNullParameter(vector, "vector");
        Bundle bundle = new Bundle();
        bundle.putString(KEY, vector.getKey());
        bundle.putString(COLOR, vector.getColor().getCode());
        INSTANCE.putDatabaseId(bundle, DATABASE_ID, vector.getDatabaseId());
        return bundle;
    }

    public final Avatar.Vector extractVector(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        String string = bundle.getString(KEY);
        if (string != null) {
            Intrinsics.checkNotNullExpressionValue(string, "requireNotNull(bundle.getString(KEY))");
            Avatars.ColorPair colorPair = Avatars.INSTANCE.getColorMap().get(bundle.getString(COLOR));
            if (colorPair != null) {
                return new Avatar.Vector(string, colorPair, getDatabaseId(bundle));
            }
            throw new IllegalStateException();
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    private final Avatar.DatabaseId getDatabaseId(Bundle bundle) {
        long j = bundle.getLong(DATABASE_ID, -1);
        if (j == -1) {
            return Avatar.DatabaseId.NotSet.INSTANCE;
        }
        return new Avatar.DatabaseId.Saved(j);
    }

    private final void putDatabaseId(Bundle bundle, String str, Avatar.DatabaseId databaseId) {
        if (databaseId instanceof Avatar.DatabaseId.Saved) {
            bundle.putLong(str, ((Avatar.DatabaseId.Saved) databaseId).getId());
        }
    }
}
