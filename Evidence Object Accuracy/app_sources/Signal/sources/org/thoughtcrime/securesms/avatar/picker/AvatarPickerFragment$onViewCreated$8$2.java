package org.thoughtcrime.securesms.avatar.picker;

import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.FragmentKt;
import androidx.navigation.Navigation;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;
import org.signal.core.util.ThreadUtil;

/* compiled from: AvatarPickerFragment.kt */
/* access modifiers changed from: package-private */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarPickerFragment$onViewCreated$8$2 extends Lambda implements Function0<Unit> {
    final /* synthetic */ View $v;
    final /* synthetic */ AvatarPickerFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AvatarPickerFragment$onViewCreated$8$2(AvatarPickerFragment avatarPickerFragment, View view) {
        super(0);
        this.this$0 = avatarPickerFragment;
        this.$v = view;
    }

    @Override // kotlin.jvm.functions.Function0
    public final void invoke() {
        AvatarPickerFragment avatarPickerFragment = this.this$0;
        Bundle bundle = new Bundle();
        bundle.putBoolean(AvatarPickerFragment.SELECT_AVATAR_CLEAR, true);
        Unit unit = Unit.INSTANCE;
        FragmentKt.setFragmentResult(avatarPickerFragment, AvatarPickerFragment.REQUEST_KEY_SELECT_AVATAR, bundle);
        ThreadUtil.runOnMain(new Runnable(this.$v) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment$onViewCreated$8$2$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarPickerFragment$onViewCreated$8$2.m301invoke$lambda1(this.f$0);
            }
        });
    }

    /* renamed from: invoke$lambda-1 */
    public static final void m301invoke$lambda1(View view) {
        Navigation.findNavController(view).popBackStack();
    }
}
