package org.thoughtcrime.securesms.avatar.picker;

import android.view.View;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class AvatarPickerItem$ViewHolder$$ExternalSyntheticLambda0 implements View.OnLayoutChangeListener {
    public final /* synthetic */ AvatarPickerItem.ViewHolder f$0;

    public /* synthetic */ AvatarPickerItem$ViewHolder$$ExternalSyntheticLambda0(AvatarPickerItem.ViewHolder viewHolder) {
        this.f$0 = viewHolder;
    }

    @Override // android.view.View.OnLayoutChangeListener
    public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        AvatarPickerItem.ViewHolder.m307_init_$lambda0(this.f$0, view, i, i2, i3, i4, i5, i6, i7, i8);
    }
}
