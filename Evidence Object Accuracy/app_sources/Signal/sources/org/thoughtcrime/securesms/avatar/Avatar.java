package org.thoughtcrime.securesms.avatar;

import android.net.Uri;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.random.Random;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* compiled from: Avatar.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u000b2\u00020\u0001:\u0006\u000b\f\r\u000e\u000f\u0010B\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H&J\u0010\u0010\n\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0003H\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\u0004\u0011\u0012\u0013\u0014¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatar;", "", "databaseId", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "(Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;)V", "getDatabaseId", "()Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "isSameAs", "", "other", "withDatabaseId", "Companion", "DatabaseId", "Photo", "Resource", "Text", "Vector", "Lorg/thoughtcrime/securesms/avatar/Avatar$Resource;", "Lorg/thoughtcrime/securesms/avatar/Avatar$Text;", "Lorg/thoughtcrime/securesms/avatar/Avatar$Vector;", "Lorg/thoughtcrime/securesms/avatar/Avatar$Photo;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public abstract class Avatar {
    public static final Companion Companion = new Companion(null);
    private final DatabaseId databaseId;

    public /* synthetic */ Avatar(DatabaseId databaseId, DefaultConstructorMarker defaultConstructorMarker) {
        this(databaseId);
    }

    public abstract boolean isSameAs(Avatar avatar);

    private Avatar(DatabaseId databaseId) {
        this.databaseId = databaseId;
    }

    public DatabaseId getDatabaseId() {
        return this.databaseId;
    }

    /* compiled from: Avatar.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÖ\u0001J\u0010\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0001H\u0016J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatar$Resource;", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "resourceId", "", NotificationProfileDatabase.NotificationProfileTable.COLOR, "Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "(ILorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;)V", "getColor", "()Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "getResourceId", "()I", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "isSameAs", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Resource extends Avatar {
        private final Avatars.ColorPair color;
        private final int resourceId;

        public static /* synthetic */ Resource copy$default(Resource resource, int i, Avatars.ColorPair colorPair, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = resource.resourceId;
            }
            if ((i2 & 2) != 0) {
                colorPair = resource.color;
            }
            return resource.copy(i, colorPair);
        }

        public final int component1() {
            return this.resourceId;
        }

        public final Avatars.ColorPair component2() {
            return this.color;
        }

        public final Resource copy(int i, Avatars.ColorPair colorPair) {
            Intrinsics.checkNotNullParameter(colorPair, NotificationProfileDatabase.NotificationProfileTable.COLOR);
            return new Resource(i, colorPair);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Resource)) {
                return false;
            }
            Resource resource = (Resource) obj;
            return this.resourceId == resource.resourceId && Intrinsics.areEqual(this.color, resource.color);
        }

        public int hashCode() {
            return (this.resourceId * 31) + this.color.hashCode();
        }

        public String toString() {
            return "Resource(resourceId=" + this.resourceId + ", color=" + this.color + ')';
        }

        public final int getResourceId() {
            return this.resourceId;
        }

        public final Avatars.ColorPair getColor() {
            return this.color;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Resource(int i, Avatars.ColorPair colorPair) {
            super(DatabaseId.DoNotPersist.INSTANCE, null);
            Intrinsics.checkNotNullParameter(colorPair, NotificationProfileDatabase.NotificationProfileTable.COLOR);
            this.resourceId = i;
            this.color = colorPair;
        }

        @Override // org.thoughtcrime.securesms.avatar.Avatar
        public boolean isSameAs(Avatar avatar) {
            Intrinsics.checkNotNullParameter(avatar, "other");
            return (avatar instanceof Resource) && ((Resource) avatar).resourceId == this.resourceId;
        }
    }

    /* compiled from: Avatar.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\u0010\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0001H\u0016J\t\u0010\u001a\u001a\u00020\u0003HÖ\u0001J\u0010\u0010\u001b\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0007H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatar$Text;", "Lorg/thoughtcrime/securesms/avatar/Avatar;", DraftDatabase.Draft.TEXT, "", NotificationProfileDatabase.NotificationProfileTable.COLOR, "Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "databaseId", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "(Ljava/lang/String;Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;)V", "getColor", "()Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "getDatabaseId", "()Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "getText", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "isSameAs", "toString", "withDatabaseId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Text extends Avatar {
        private final Avatars.ColorPair color;
        private final DatabaseId databaseId;
        private final String text;

        public static /* synthetic */ Text copy$default(Text text, String str, Avatars.ColorPair colorPair, DatabaseId databaseId, int i, Object obj) {
            if ((i & 1) != 0) {
                str = text.text;
            }
            if ((i & 2) != 0) {
                colorPair = text.color;
            }
            if ((i & 4) != 0) {
                databaseId = text.getDatabaseId();
            }
            return text.copy(str, colorPair, databaseId);
        }

        public final String component1() {
            return this.text;
        }

        public final Avatars.ColorPair component2() {
            return this.color;
        }

        public final DatabaseId component3() {
            return getDatabaseId();
        }

        public final Text copy(String str, Avatars.ColorPair colorPair, DatabaseId databaseId) {
            Intrinsics.checkNotNullParameter(str, DraftDatabase.Draft.TEXT);
            Intrinsics.checkNotNullParameter(colorPair, NotificationProfileDatabase.NotificationProfileTable.COLOR);
            Intrinsics.checkNotNullParameter(databaseId, "databaseId");
            return new Text(str, colorPair, databaseId);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Text)) {
                return false;
            }
            Text text = (Text) obj;
            return Intrinsics.areEqual(this.text, text.text) && Intrinsics.areEqual(this.color, text.color) && Intrinsics.areEqual(getDatabaseId(), text.getDatabaseId());
        }

        public int hashCode() {
            return (((this.text.hashCode() * 31) + this.color.hashCode()) * 31) + getDatabaseId().hashCode();
        }

        public String toString() {
            return "Text(text=" + this.text + ", color=" + this.color + ", databaseId=" + getDatabaseId() + ')';
        }

        public final String getText() {
            return this.text;
        }

        public final Avatars.ColorPair getColor() {
            return this.color;
        }

        @Override // org.thoughtcrime.securesms.avatar.Avatar
        public DatabaseId getDatabaseId() {
            return this.databaseId;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Text(String str, Avatars.ColorPair colorPair, DatabaseId databaseId) {
            super(databaseId, null);
            Intrinsics.checkNotNullParameter(str, DraftDatabase.Draft.TEXT);
            Intrinsics.checkNotNullParameter(colorPair, NotificationProfileDatabase.NotificationProfileTable.COLOR);
            Intrinsics.checkNotNullParameter(databaseId, "databaseId");
            this.text = str;
            this.color = colorPair;
            this.databaseId = databaseId;
        }

        @Override // org.thoughtcrime.securesms.avatar.Avatar
        public Avatar withDatabaseId(DatabaseId databaseId) {
            Intrinsics.checkNotNullParameter(databaseId, "databaseId");
            return copy$default(this, null, null, databaseId, 3, null);
        }

        @Override // org.thoughtcrime.securesms.avatar.Avatar
        public boolean isSameAs(Avatar avatar) {
            Intrinsics.checkNotNullParameter(avatar, "other");
            return (avatar instanceof Text) && Intrinsics.areEqual(avatar.getDatabaseId(), getDatabaseId());
        }
    }

    /* compiled from: Avatar.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\u0010\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0001H\u0016J\t\u0010\u001a\u001a\u00020\u0003HÖ\u0001J\u0010\u0010\u001b\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0007H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatar$Vector;", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "key", "", NotificationProfileDatabase.NotificationProfileTable.COLOR, "Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "databaseId", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "(Ljava/lang/String;Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;)V", "getColor", "()Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "getDatabaseId", "()Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "getKey", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "isSameAs", "toString", "withDatabaseId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Vector extends Avatar {
        private final Avatars.ColorPair color;
        private final DatabaseId databaseId;
        private final String key;

        public static /* synthetic */ Vector copy$default(Vector vector, String str, Avatars.ColorPair colorPair, DatabaseId databaseId, int i, Object obj) {
            if ((i & 1) != 0) {
                str = vector.key;
            }
            if ((i & 2) != 0) {
                colorPair = vector.color;
            }
            if ((i & 4) != 0) {
                databaseId = vector.getDatabaseId();
            }
            return vector.copy(str, colorPair, databaseId);
        }

        public final String component1() {
            return this.key;
        }

        public final Avatars.ColorPair component2() {
            return this.color;
        }

        public final DatabaseId component3() {
            return getDatabaseId();
        }

        public final Vector copy(String str, Avatars.ColorPair colorPair, DatabaseId databaseId) {
            Intrinsics.checkNotNullParameter(str, "key");
            Intrinsics.checkNotNullParameter(colorPair, NotificationProfileDatabase.NotificationProfileTable.COLOR);
            Intrinsics.checkNotNullParameter(databaseId, "databaseId");
            return new Vector(str, colorPair, databaseId);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Vector)) {
                return false;
            }
            Vector vector = (Vector) obj;
            return Intrinsics.areEqual(this.key, vector.key) && Intrinsics.areEqual(this.color, vector.color) && Intrinsics.areEqual(getDatabaseId(), vector.getDatabaseId());
        }

        public int hashCode() {
            return (((this.key.hashCode() * 31) + this.color.hashCode()) * 31) + getDatabaseId().hashCode();
        }

        public String toString() {
            return "Vector(key=" + this.key + ", color=" + this.color + ", databaseId=" + getDatabaseId() + ')';
        }

        public final String getKey() {
            return this.key;
        }

        public final Avatars.ColorPair getColor() {
            return this.color;
        }

        @Override // org.thoughtcrime.securesms.avatar.Avatar
        public DatabaseId getDatabaseId() {
            return this.databaseId;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Vector(String str, Avatars.ColorPair colorPair, DatabaseId databaseId) {
            super(databaseId, null);
            Intrinsics.checkNotNullParameter(str, "key");
            Intrinsics.checkNotNullParameter(colorPair, NotificationProfileDatabase.NotificationProfileTable.COLOR);
            Intrinsics.checkNotNullParameter(databaseId, "databaseId");
            this.key = str;
            this.color = colorPair;
            this.databaseId = databaseId;
        }

        @Override // org.thoughtcrime.securesms.avatar.Avatar
        public Avatar withDatabaseId(DatabaseId databaseId) {
            Intrinsics.checkNotNullParameter(databaseId, "databaseId");
            return copy$default(this, null, null, databaseId, 3, null);
        }

        @Override // org.thoughtcrime.securesms.avatar.Avatar
        public boolean isSameAs(Avatar avatar) {
            Intrinsics.checkNotNullParameter(avatar, "other");
            return (avatar instanceof Vector) && Intrinsics.areEqual(((Vector) avatar).key, this.key);
        }
    }

    /* compiled from: Avatar.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\u0010\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0001H\u0016J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\u0010\u0010\u001c\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0007H\u0016R\u0014\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatar$Photo;", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "uri", "Landroid/net/Uri;", MediaPreviewActivity.SIZE_EXTRA, "", "databaseId", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "(Landroid/net/Uri;JLorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;)V", "getDatabaseId", "()Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "getSize", "()J", "getUri", "()Landroid/net/Uri;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "isSameAs", "toString", "", "withDatabaseId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Photo extends Avatar {
        private final DatabaseId databaseId;
        private final long size;
        private final Uri uri;

        public static /* synthetic */ Photo copy$default(Photo photo, Uri uri, long j, DatabaseId databaseId, int i, Object obj) {
            if ((i & 1) != 0) {
                uri = photo.uri;
            }
            if ((i & 2) != 0) {
                j = photo.size;
            }
            if ((i & 4) != 0) {
                databaseId = photo.getDatabaseId();
            }
            return photo.copy(uri, j, databaseId);
        }

        public final Uri component1() {
            return this.uri;
        }

        public final long component2() {
            return this.size;
        }

        public final DatabaseId component3() {
            return getDatabaseId();
        }

        public final Photo copy(Uri uri, long j, DatabaseId databaseId) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            Intrinsics.checkNotNullParameter(databaseId, "databaseId");
            return new Photo(uri, j, databaseId);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Photo)) {
                return false;
            }
            Photo photo = (Photo) obj;
            return Intrinsics.areEqual(this.uri, photo.uri) && this.size == photo.size && Intrinsics.areEqual(getDatabaseId(), photo.getDatabaseId());
        }

        public int hashCode() {
            return (((this.uri.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.size)) * 31) + getDatabaseId().hashCode();
        }

        public String toString() {
            return "Photo(uri=" + this.uri + ", size=" + this.size + ", databaseId=" + getDatabaseId() + ')';
        }

        public final Uri getUri() {
            return this.uri;
        }

        public final long getSize() {
            return this.size;
        }

        @Override // org.thoughtcrime.securesms.avatar.Avatar
        public DatabaseId getDatabaseId() {
            return this.databaseId;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Photo(Uri uri, long j, DatabaseId databaseId) {
            super(databaseId, null);
            Intrinsics.checkNotNullParameter(uri, "uri");
            Intrinsics.checkNotNullParameter(databaseId, "databaseId");
            this.uri = uri;
            this.size = j;
            this.databaseId = databaseId;
        }

        @Override // org.thoughtcrime.securesms.avatar.Avatar
        public Avatar withDatabaseId(DatabaseId databaseId) {
            Intrinsics.checkNotNullParameter(databaseId, "databaseId");
            return copy$default(this, null, 0, databaseId, 3, null);
        }

        @Override // org.thoughtcrime.securesms.avatar.Avatar
        public boolean isSameAs(Avatar avatar) {
            Intrinsics.checkNotNullParameter(avatar, "other");
            return (avatar instanceof Photo) && Intrinsics.areEqual(getDatabaseId(), avatar.getDatabaseId());
        }
    }

    public Avatar withDatabaseId(DatabaseId databaseId) {
        Intrinsics.checkNotNullParameter(databaseId, "databaseId");
        throw new UnsupportedOperationException();
    }

    /* compiled from: Avatar.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u0006\u0010\u0005\u001a\u00020\u0004¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatar$Companion;", "", "()V", "getDefaultForGroup", "Lorg/thoughtcrime/securesms/avatar/Avatar$Resource;", "getDefaultForSelf", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Resource getDefaultForSelf() {
            return new Resource(R.drawable.ic_profile_outline_40, (Avatars.ColorPair) CollectionsKt___CollectionsKt.random(Avatars.INSTANCE.getColors(), Random.Default));
        }

        public final Resource getDefaultForGroup() {
            return new Resource(R.drawable.ic_group_outline_40, (Avatars.ColorPair) CollectionsKt___CollectionsKt.random(Avatars.INSTANCE.getColors(), Random.Default));
        }
    }

    /* compiled from: Avatar.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "", "()V", "DoNotPersist", "NotSet", "Saved", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId$DoNotPersist;", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId$NotSet;", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId$Saved;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static abstract class DatabaseId {
        public /* synthetic */ DatabaseId(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: Avatar.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId$DoNotPersist;", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes.dex */
        public static final class DoNotPersist extends DatabaseId {
            public static final DoNotPersist INSTANCE = new DoNotPersist();

            private DoNotPersist() {
                super(null);
            }
        }

        private DatabaseId() {
        }

        /* compiled from: Avatar.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId$NotSet;", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes.dex */
        public static final class NotSet extends DatabaseId {
            public static final NotSet INSTANCE = new NotSet();

            private NotSet() {
                super(null);
            }
        }

        /* compiled from: Avatar.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId$Saved;", "Lorg/thoughtcrime/securesms/avatar/Avatar$DatabaseId;", ContactRepository.ID_COLUMN, "", "(J)V", "getId", "()J", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes.dex */
        public static final class Saved extends DatabaseId {
            private final long id;

            public static /* synthetic */ Saved copy$default(Saved saved, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = saved.id;
                }
                return saved.copy(j);
            }

            public final long component1() {
                return this.id;
            }

            public final Saved copy(long j) {
                return new Saved(j);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Saved) && this.id == ((Saved) obj).id;
            }

            public int hashCode() {
                return SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id);
            }

            public String toString() {
                return "Saved(id=" + this.id + ')';
            }

            public Saved(long j) {
                super(null);
                this.id = j;
            }

            public final long getId() {
                return this.id;
            }
        }
    }
}
