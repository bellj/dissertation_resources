package org.thoughtcrime.securesms.avatar.picker;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public class AvatarPickerFragmentDirections {
    private AvatarPickerFragmentDirections() {
    }

    public static ActionAvatarPickerFragmentToTextAvatarCreationFragment actionAvatarPickerFragmentToTextAvatarCreationFragment(Bundle bundle) {
        return new ActionAvatarPickerFragmentToTextAvatarCreationFragment(bundle);
    }

    public static ActionAvatarPickerFragmentToVectorAvatarCreationFragment actionAvatarPickerFragmentToVectorAvatarCreationFragment(Bundle bundle) {
        return new ActionAvatarPickerFragmentToVectorAvatarCreationFragment(bundle);
    }

    public static ActionAvatarPickerFragmentToAvatarPhotoEditorFragment actionAvatarPickerFragmentToAvatarPhotoEditorFragment(Bundle bundle) {
        return new ActionAvatarPickerFragmentToAvatarPhotoEditorFragment(bundle);
    }

    /* loaded from: classes.dex */
    public static class ActionAvatarPickerFragmentToTextAvatarCreationFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_avatarPickerFragment_to_textAvatarCreationFragment;
        }

        private ActionAvatarPickerFragmentToTextAvatarCreationFragment(Bundle bundle) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("text_avatar", bundle);
        }

        public ActionAvatarPickerFragmentToTextAvatarCreationFragment setTextAvatar(Bundle bundle) {
            this.arguments.put("text_avatar", bundle);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("text_avatar")) {
                Bundle bundle2 = (Bundle) this.arguments.get("text_avatar");
                if (Parcelable.class.isAssignableFrom(Bundle.class) || bundle2 == null) {
                    bundle.putParcelable("text_avatar", (Parcelable) Parcelable.class.cast(bundle2));
                } else if (Serializable.class.isAssignableFrom(Bundle.class)) {
                    bundle.putSerializable("text_avatar", (Serializable) Serializable.class.cast(bundle2));
                } else {
                    throw new UnsupportedOperationException(Bundle.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public Bundle getTextAvatar() {
            return (Bundle) this.arguments.get("text_avatar");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionAvatarPickerFragmentToTextAvatarCreationFragment actionAvatarPickerFragmentToTextAvatarCreationFragment = (ActionAvatarPickerFragmentToTextAvatarCreationFragment) obj;
            if (this.arguments.containsKey("text_avatar") != actionAvatarPickerFragmentToTextAvatarCreationFragment.arguments.containsKey("text_avatar")) {
                return false;
            }
            if (getTextAvatar() == null ? actionAvatarPickerFragmentToTextAvatarCreationFragment.getTextAvatar() == null : getTextAvatar().equals(actionAvatarPickerFragmentToTextAvatarCreationFragment.getTextAvatar())) {
                return getActionId() == actionAvatarPickerFragmentToTextAvatarCreationFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getTextAvatar() != null ? getTextAvatar().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionAvatarPickerFragmentToTextAvatarCreationFragment(actionId=" + getActionId() + "){textAvatar=" + getTextAvatar() + "}";
        }
    }

    /* loaded from: classes.dex */
    public static class ActionAvatarPickerFragmentToVectorAvatarCreationFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_avatarPickerFragment_to_vectorAvatarCreationFragment;
        }

        private ActionAvatarPickerFragmentToVectorAvatarCreationFragment(Bundle bundle) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (bundle != null) {
                hashMap.put("vector_avatar", bundle);
                return;
            }
            throw new IllegalArgumentException("Argument \"vector_avatar\" is marked as non-null but was passed a null value.");
        }

        public ActionAvatarPickerFragmentToVectorAvatarCreationFragment setVectorAvatar(Bundle bundle) {
            if (bundle != null) {
                this.arguments.put("vector_avatar", bundle);
                return this;
            }
            throw new IllegalArgumentException("Argument \"vector_avatar\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("vector_avatar")) {
                Bundle bundle2 = (Bundle) this.arguments.get("vector_avatar");
                if (Parcelable.class.isAssignableFrom(Bundle.class) || bundle2 == null) {
                    bundle.putParcelable("vector_avatar", (Parcelable) Parcelable.class.cast(bundle2));
                } else if (Serializable.class.isAssignableFrom(Bundle.class)) {
                    bundle.putSerializable("vector_avatar", (Serializable) Serializable.class.cast(bundle2));
                } else {
                    throw new UnsupportedOperationException(Bundle.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public Bundle getVectorAvatar() {
            return (Bundle) this.arguments.get("vector_avatar");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionAvatarPickerFragmentToVectorAvatarCreationFragment actionAvatarPickerFragmentToVectorAvatarCreationFragment = (ActionAvatarPickerFragmentToVectorAvatarCreationFragment) obj;
            if (this.arguments.containsKey("vector_avatar") != actionAvatarPickerFragmentToVectorAvatarCreationFragment.arguments.containsKey("vector_avatar")) {
                return false;
            }
            if (getVectorAvatar() == null ? actionAvatarPickerFragmentToVectorAvatarCreationFragment.getVectorAvatar() == null : getVectorAvatar().equals(actionAvatarPickerFragmentToVectorAvatarCreationFragment.getVectorAvatar())) {
                return getActionId() == actionAvatarPickerFragmentToVectorAvatarCreationFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getVectorAvatar() != null ? getVectorAvatar().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionAvatarPickerFragmentToVectorAvatarCreationFragment(actionId=" + getActionId() + "){vectorAvatar=" + getVectorAvatar() + "}";
        }
    }

    /* loaded from: classes.dex */
    public static class ActionAvatarPickerFragmentToAvatarPhotoEditorFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_avatarPickerFragment_to_avatarPhotoEditorFragment;
        }

        private ActionAvatarPickerFragmentToAvatarPhotoEditorFragment(Bundle bundle) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (bundle != null) {
                hashMap.put("photo_avatar", bundle);
                return;
            }
            throw new IllegalArgumentException("Argument \"photo_avatar\" is marked as non-null but was passed a null value.");
        }

        public ActionAvatarPickerFragmentToAvatarPhotoEditorFragment setPhotoAvatar(Bundle bundle) {
            if (bundle != null) {
                this.arguments.put("photo_avatar", bundle);
                return this;
            }
            throw new IllegalArgumentException("Argument \"photo_avatar\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("photo_avatar")) {
                Bundle bundle2 = (Bundle) this.arguments.get("photo_avatar");
                if (Parcelable.class.isAssignableFrom(Bundle.class) || bundle2 == null) {
                    bundle.putParcelable("photo_avatar", (Parcelable) Parcelable.class.cast(bundle2));
                } else if (Serializable.class.isAssignableFrom(Bundle.class)) {
                    bundle.putSerializable("photo_avatar", (Serializable) Serializable.class.cast(bundle2));
                } else {
                    throw new UnsupportedOperationException(Bundle.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public Bundle getPhotoAvatar() {
            return (Bundle) this.arguments.get("photo_avatar");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionAvatarPickerFragmentToAvatarPhotoEditorFragment actionAvatarPickerFragmentToAvatarPhotoEditorFragment = (ActionAvatarPickerFragmentToAvatarPhotoEditorFragment) obj;
            if (this.arguments.containsKey("photo_avatar") != actionAvatarPickerFragmentToAvatarPhotoEditorFragment.arguments.containsKey("photo_avatar")) {
                return false;
            }
            if (getPhotoAvatar() == null ? actionAvatarPickerFragmentToAvatarPhotoEditorFragment.getPhotoAvatar() == null : getPhotoAvatar().equals(actionAvatarPickerFragmentToAvatarPhotoEditorFragment.getPhotoAvatar())) {
                return getActionId() == actionAvatarPickerFragmentToAvatarPhotoEditorFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getPhotoAvatar() != null ? getPhotoAvatar().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionAvatarPickerFragmentToAvatarPhotoEditorFragment(actionId=" + getActionId() + "){photoAvatar=" + getPhotoAvatar() + "}";
        }
    }
}
