package org.thoughtcrime.securesms.avatar;

import kotlin.Metadata;

/* compiled from: AvatarColorItem.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000*\"\u0010\u0000\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001¨\u0006\u0004"}, d2 = {"OnAvatarColorClickListener", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarColorItemKt {
}
