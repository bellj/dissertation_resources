package org.thoughtcrime.securesms.avatar.photo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentKt;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.Navigation;
import java.io.InputStream;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.AvatarBundler;
import org.thoughtcrime.securesms.avatar.AvatarPickerStorage;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.AvatarPickerDatabase;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.scribbles.ImageEditorFragment;

/* compiled from: PhotoEditorFragment.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00142\u00020\u00012\u00020\u0002:\u0001\u0014B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0004\u001a\u00020\u0005H\u0016J\b\u0010\u0006\u001a\u00020\u0005H\u0016J\b\u0010\u0007\u001a\u00020\u0005H\u0016J\b\u0010\b\u001a\u00020\u0005H\u0016J\u0018\u0010\t\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000bH\u0016J\u0010\u0010\r\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u000bH\u0016J\u001a\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/photo/PhotoEditorFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/scribbles/ImageEditorFragment$Controller;", "()V", "onCancelEditing", "", "onDoneEditing", "onMainImageFailedToLoad", "onMainImageLoaded", "onRequestFullScreen", "fullScreen", "", "hideKeyboard", "onTouchEventsNeeded", "needed", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class PhotoEditorFragment extends Fragment implements ImageEditorFragment.Controller {
    public static final Companion Companion = new Companion(null);
    private static final String IMAGE_EDITOR;
    public static final String REQUEST_KEY_EDIT;

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onMainImageFailedToLoad() {
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onMainImageLoaded() {
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onRequestFullScreen(boolean z, boolean z2) {
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onTouchEventsNeeded(boolean z) {
    }

    public PhotoEditorFragment() {
        super(R.layout.avatar_photo_editor_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        PhotoEditorFragmentArgs fromBundle = PhotoEditorFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        AvatarBundler avatarBundler = AvatarBundler.INSTANCE;
        Bundle photoAvatar = fromBundle.getPhotoAvatar();
        Intrinsics.checkNotNullExpressionValue(photoAvatar, "args.photoAvatar");
        ImageEditorFragment newInstanceForAvatarEdit = ImageEditorFragment.newInstanceForAvatarEdit(avatarBundler.extractPhoto(photoAvatar).getUri());
        FragmentManager childFragmentManager = getChildFragmentManager();
        Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        FragmentTransaction beginTransaction = childFragmentManager.beginTransaction();
        Intrinsics.checkNotNullExpressionValue(beginTransaction, "beginTransaction()");
        beginTransaction.add(R.id.fragment_container, newInstanceForAvatarEdit, IMAGE_EDITOR);
        beginTransaction.commit();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onDoneEditing() {
        PhotoEditorFragmentArgs fromBundle = PhotoEditorFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        Context applicationContext = requireContext().getApplicationContext();
        Fragment findFragmentByTag = getChildFragmentManager().findFragmentByTag(IMAGE_EDITOR);
        if (findFragmentByTag != null) {
            SignalExecutors.BOUNDED.execute(new Runnable(applicationContext, fromBundle, this) { // from class: org.thoughtcrime.securesms.avatar.photo.PhotoEditorFragment$$ExternalSyntheticLambda1
                public final /* synthetic */ Context f$1;
                public final /* synthetic */ PhotoEditorFragmentArgs f$2;
                public final /* synthetic */ PhotoEditorFragment f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    PhotoEditorFragment.$r8$lambda$0p6NKB3Rb3iK5Dmc1cxLCqEFnP0(ImageEditorFragment.this, this.f$1, this.f$2, this.f$3);
                }
            });
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.scribbles.ImageEditorFragment");
    }

    /* renamed from: onDoneEditing$lambda-2 */
    public static final void m284onDoneEditing$lambda2(ImageEditorFragment imageEditorFragment, Context context, PhotoEditorFragmentArgs photoEditorFragmentArgs, PhotoEditorFragment photoEditorFragment) {
        Intrinsics.checkNotNullParameter(imageEditorFragment, "$imageEditorFragment");
        Intrinsics.checkNotNullParameter(photoEditorFragmentArgs, "$args");
        Intrinsics.checkNotNullParameter(photoEditorFragment, "this$0");
        Uri renderToSingleUseBlob = imageEditorFragment.renderToSingleUseBlob();
        Intrinsics.checkNotNullExpressionValue(renderToSingleUseBlob, "imageEditorFragment.renderToSingleUseBlob()");
        Long fileSize = BlobProvider.getFileSize(renderToSingleUseBlob);
        if (fileSize == null) {
            fileSize = 0L;
        }
        long longValue = fileSize.longValue();
        InputStream stream = BlobProvider.getInstance().getStream(context, renderToSingleUseBlob);
        Intrinsics.checkNotNullExpressionValue(stream, "getInstance().getStream(…nContext, editedImageUri)");
        AvatarPickerStorage avatarPickerStorage = AvatarPickerStorage.INSTANCE;
        Intrinsics.checkNotNullExpressionValue(context, "applicationContext");
        Uri save = avatarPickerStorage.save(context, stream);
        AvatarBundler avatarBundler = AvatarBundler.INSTANCE;
        Bundle photoAvatar = photoEditorFragmentArgs.getPhotoAvatar();
        Intrinsics.checkNotNullExpressionValue(photoAvatar, "args.photoAvatar");
        Avatar.Photo extractPhoto = avatarBundler.extractPhoto(photoAvatar);
        AvatarPickerDatabase avatarPicker = SignalDatabase.Companion.avatarPicker();
        Avatar.Photo copy$default = Avatar.Photo.copy$default(extractPhoto, save, longValue, null, 4, null);
        avatarPicker.update(copy$default);
        BlobProvider.getInstance().delete(photoEditorFragment.requireContext(), extractPhoto.getUri());
        ThreadUtil.runOnMain(new Runnable(copy$default) { // from class: org.thoughtcrime.securesms.avatar.photo.PhotoEditorFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ Avatar.Photo f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PhotoEditorFragment.$r8$lambda$u2XFr1qUDdkY5FK_DFswuLdwhME(PhotoEditorFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: onDoneEditing$lambda-2$lambda-1 */
    public static final void m285onDoneEditing$lambda2$lambda1(PhotoEditorFragment photoEditorFragment, Avatar.Photo photo) {
        Intrinsics.checkNotNullParameter(photoEditorFragment, "this$0");
        Intrinsics.checkNotNullParameter(photo, "$newPhoto");
        FragmentKt.setFragmentResult(photoEditorFragment, REQUEST_KEY_EDIT, AvatarBundler.INSTANCE.bundlePhoto(photo));
        Navigation.findNavController(photoEditorFragment.requireView()).popBackStack();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onCancelEditing() {
        Navigation.findNavController(requireView()).popBackStack();
    }

    /* compiled from: PhotoEditorFragment.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/photo/PhotoEditorFragment$Companion;", "", "()V", "IMAGE_EDITOR", "", "REQUEST_KEY_EDIT", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
