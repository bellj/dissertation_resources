package org.thoughtcrime.securesms.avatar.text;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;

/* loaded from: classes.dex */
public class TextAvatarCreationFragmentArgs {
    private final HashMap arguments;

    private TextAvatarCreationFragmentArgs() {
        this.arguments = new HashMap();
    }

    private TextAvatarCreationFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static TextAvatarCreationFragmentArgs fromBundle(Bundle bundle) {
        TextAvatarCreationFragmentArgs textAvatarCreationFragmentArgs = new TextAvatarCreationFragmentArgs();
        bundle.setClassLoader(TextAvatarCreationFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("text_avatar")) {
            throw new IllegalArgumentException("Required argument \"text_avatar\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(Bundle.class) || Serializable.class.isAssignableFrom(Bundle.class)) {
            textAvatarCreationFragmentArgs.arguments.put("text_avatar", (Bundle) bundle.get("text_avatar"));
            return textAvatarCreationFragmentArgs;
        } else {
            throw new UnsupportedOperationException(Bundle.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public Bundle getTextAvatar() {
        return (Bundle) this.arguments.get("text_avatar");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("text_avatar")) {
            Bundle bundle2 = (Bundle) this.arguments.get("text_avatar");
            if (Parcelable.class.isAssignableFrom(Bundle.class) || bundle2 == null) {
                bundle.putParcelable("text_avatar", (Parcelable) Parcelable.class.cast(bundle2));
            } else if (Serializable.class.isAssignableFrom(Bundle.class)) {
                bundle.putSerializable("text_avatar", (Serializable) Serializable.class.cast(bundle2));
            } else {
                throw new UnsupportedOperationException(Bundle.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        TextAvatarCreationFragmentArgs textAvatarCreationFragmentArgs = (TextAvatarCreationFragmentArgs) obj;
        if (this.arguments.containsKey("text_avatar") != textAvatarCreationFragmentArgs.arguments.containsKey("text_avatar")) {
            return false;
        }
        return getTextAvatar() == null ? textAvatarCreationFragmentArgs.getTextAvatar() == null : getTextAvatar().equals(textAvatarCreationFragmentArgs.getTextAvatar());
    }

    public int hashCode() {
        return 31 + (getTextAvatar() != null ? getTextAvatar().hashCode() : 0);
    }

    public String toString() {
        return "TextAvatarCreationFragmentArgs{textAvatar=" + getTextAvatar() + "}";
    }

    /* loaded from: classes.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(TextAvatarCreationFragmentArgs textAvatarCreationFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(textAvatarCreationFragmentArgs.arguments);
        }

        public Builder(Bundle bundle) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("text_avatar", bundle);
        }

        public TextAvatarCreationFragmentArgs build() {
            return new TextAvatarCreationFragmentArgs(this.arguments);
        }

        public Builder setTextAvatar(Bundle bundle) {
            this.arguments.put("text_avatar", bundle);
            return this;
        }

        public Bundle getTextAvatar() {
            return (Bundle) this.arguments.get("text_avatar");
        }
    }
}
