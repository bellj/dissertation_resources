package org.thoughtcrime.securesms.avatar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.appcompat.content.res.AppCompatResources;
import com.airbnb.lottie.SimpleColorFilter;
import j$.util.Optional;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.util.MediaUtil;

/* compiled from: AvatarRenderer.kt */
@Metadata(d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0002J6\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u00142\b\b\u0002\u0010\u000b\u001a\u00020\u00042\b\b\u0002\u0010\u0015\u001a\u00020\u0014H\u0007J\u000e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u000f\u001a\u00020\u0010J@\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u001a2\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00190\u001c2\u0014\u0010\u001d\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u001e\u0012\u0004\u0012\u00020\u00190\u001cJW\u0010\u001f\u001a\u00020\u00192\u0006\u0010\u000f\u001a\u00020\u00102\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00190\u001c2\u0014\u0010\u001d\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u001e\u0012\u0004\u0012\u00020\u00190\u001c2\u0018\u0010 \u001a\u0014\u0012\u0004\u0012\u00020!\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\"0\u001cH\u0002ø\u0001\u0000J,\u0010#\u001a\u00020\u00192\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020$2\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00190\u001cH\u0002JB\u0010%\u001a\u00020\u00192\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020&2\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00190\u001c2\u0014\u0010\u001d\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u001e\u0012\u0004\u0012\u00020\u00190\u001cH\u0002JB\u0010'\u001a\u00020\u00192\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00190\u001c2\u0014\u0010\u001d\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u001e\u0012\u0004\u0012\u00020\u00190\u001cH\u0002JB\u0010(\u001a\u00020\u00192\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020)2\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00190\u001c2\u0014\u0010\u001d\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u001e\u0012\u0004\u0012\u00020\u00190\u001cH\u0002R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0002\u0004\n\u0002\b\u0019¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/AvatarRenderer;", "", "()V", "DIMENSIONS", "", "getDIMENSIONS", "()I", "createMedia", "Lorg/thoughtcrime/securesms/mediasend/Media;", "uri", "Landroid/net/Uri;", MediaPreviewActivity.SIZE_EXTRA, "", "createTextDrawable", "Landroid/graphics/drawable/Drawable;", "context", "Landroid/content/Context;", "avatar", "Lorg/thoughtcrime/securesms/avatar/Avatar$Text;", "inverted", "", "synchronous", "getTypeface", "Landroid/graphics/Typeface;", "renderAvatar", "", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "onAvatarRendered", "Lkotlin/Function1;", "onRenderFailed", "", "renderInBackground", "drawAvatar", "Landroid/graphics/Canvas;", "Lkotlin/Result;", "renderPhoto", "Lorg/thoughtcrime/securesms/avatar/Avatar$Photo;", "renderResource", "Lorg/thoughtcrime/securesms/avatar/Avatar$Resource;", "renderText", "renderVector", "Lorg/thoughtcrime/securesms/avatar/Avatar$Vector;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarRenderer {
    private static final int DIMENSIONS = AvatarHelper.AVATAR_DIMENSIONS;
    public static final AvatarRenderer INSTANCE = new AvatarRenderer();

    private AvatarRenderer() {
    }

    public final int getDIMENSIONS() {
        return DIMENSIONS;
    }

    public final Typeface getTypeface(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        Typeface createFromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Inter-Medium.otf");
        Intrinsics.checkNotNullExpressionValue(createFromAsset, "createFromAsset(context.…\"fonts/Inter-Medium.otf\")");
        return createFromAsset;
    }

    public final void renderAvatar(Context context, Avatar avatar, Function1<? super Media, Unit> function1, Function1<? super Throwable, Unit> function12) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(avatar, "avatar");
        Intrinsics.checkNotNullParameter(function1, "onAvatarRendered");
        Intrinsics.checkNotNullParameter(function12, "onRenderFailed");
        if (avatar instanceof Avatar.Resource) {
            renderResource(context, (Avatar.Resource) avatar, function1, function12);
        } else if (avatar instanceof Avatar.Vector) {
            renderVector(context, (Avatar.Vector) avatar, function1, function12);
        } else if (avatar instanceof Avatar.Photo) {
            renderPhoto(context, (Avatar.Photo) avatar, function1);
        } else if (avatar instanceof Avatar.Text) {
            renderText(context, (Avatar.Text) avatar, function1, function12);
        }
    }

    public static /* synthetic */ Drawable createTextDrawable$default(Context context, Avatar.Text text, boolean z, int i, boolean z2, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            z = false;
        }
        if ((i2 & 8) != 0) {
            i = DIMENSIONS;
        }
        if ((i2 & 16) != 0) {
            z2 = false;
        }
        return createTextDrawable(context, text, z, i, z2);
    }

    @JvmStatic
    public static final Drawable createTextDrawable(Context context, Avatar.Text text, boolean z, int i, boolean z2) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(text, "avatar");
        return new TextAvatarDrawable(context, text, z, i, z2);
    }

    private final void renderVector(Context context, Avatar.Vector vector, Function1<? super Media, Unit> function1, Function1<? super Throwable, Unit> function12) {
        renderInBackground(context, function1, function12, new Function1<Canvas, Result<? extends Unit>>(vector, context) { // from class: org.thoughtcrime.securesms.avatar.AvatarRenderer$renderVector$1
            final /* synthetic */ Avatar.Vector $avatar;
            final /* synthetic */ Context $context;

            /* access modifiers changed from: package-private */
            {
                this.$avatar = r1;
                this.$context = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Result<? extends Unit> invoke(Canvas canvas) {
                return Result.m152boximpl(m283invokeIoAF18A(canvas));
            }

            /* renamed from: invoke-IoAF18A  reason: not valid java name */
            public final Object m283invokeIoAF18A(Canvas canvas) {
                Intrinsics.checkNotNullParameter(canvas, "canvas");
                Integer drawableResource = Avatars.INSTANCE.getDrawableResource(this.$avatar.getKey());
                if (drawableResource != null) {
                    Drawable drawable = AppCompatResources.getDrawable(this.$context, drawableResource.intValue());
                    if (drawable != null) {
                        AvatarRenderer avatarRenderer = AvatarRenderer.INSTANCE;
                        drawable.setBounds(0, 0, avatarRenderer.getDIMENSIONS(), avatarRenderer.getDIMENSIONS());
                        canvas.drawColor(this.$avatar.getColor().getBackgroundColor());
                        drawable.draw(canvas);
                        Result.Companion companion = Result.Companion;
                        return Result.m153constructorimpl(Unit.INSTANCE);
                    }
                    throw new IllegalArgumentException("Required value was null.".toString());
                }
                Result.Companion companion2 = Result.Companion;
                return Result.m153constructorimpl(ResultKt.createFailure(new Exception("Drawable resource for key " + this.$avatar.getKey() + " does not exist.")));
            }
        });
    }

    private final void renderText(Context context, Avatar.Text text, Function1<? super Media, Unit> function1, Function1<? super Throwable, Unit> function12) {
        renderInBackground(context, function1, function12, new Function1<Canvas, Result<? extends Unit>>(context, text) { // from class: org.thoughtcrime.securesms.avatar.AvatarRenderer$renderText$1
            final /* synthetic */ Avatar.Text $avatar;
            final /* synthetic */ Context $context;

            /* access modifiers changed from: package-private */
            {
                this.$context = r1;
                this.$avatar = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Result<? extends Unit> invoke(Canvas canvas) {
                return Result.m152boximpl(m282invokeIoAF18A(canvas));
            }

            /* renamed from: invoke-IoAF18A  reason: not valid java name */
            public final Object m282invokeIoAF18A(Canvas canvas) {
                Intrinsics.checkNotNullParameter(canvas, "canvas");
                Drawable createTextDrawable$default = AvatarRenderer.createTextDrawable$default(this.$context, this.$avatar, false, 0, true, 12, null);
                canvas.drawColor(this.$avatar.getColor().getBackgroundColor());
                createTextDrawable$default.draw(canvas);
                Result.Companion companion = Result.Companion;
                return Result.m153constructorimpl(Unit.INSTANCE);
            }
        });
    }

    private final void renderPhoto(Context context, Avatar.Photo photo, Function1<? super Media, Unit> function1) {
        SignalExecutors.BOUNDED.execute(new Runnable(context, photo, function1) { // from class: org.thoughtcrime.securesms.avatar.AvatarRenderer$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Avatar.Photo f$1;
            public final /* synthetic */ Function1 f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarRenderer.$r8$lambda$JPB5bhf1yjG2tUuXnCQYqVFPjOI(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: renderPhoto$lambda-0 */
    public static final void m280renderPhoto$lambda0(Context context, Avatar.Photo photo, Function1 function1) {
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(photo, "$avatar");
        Intrinsics.checkNotNullParameter(function1, "$onAvatarRendered");
        BlobProvider instance = BlobProvider.getInstance();
        String avatarPickerFilename = PartAuthority.getAvatarPickerFilename(photo.getUri());
        Intrinsics.checkNotNullExpressionValue(avatarPickerFilename, "getAvatarPickerFilename(avatar.uri)");
        Uri createForSingleSessionOnDisk = instance.forData(AvatarPickerStorage.read(context, avatarPickerFilename), photo.getSize()).createForSingleSessionOnDisk(context);
        AvatarRenderer avatarRenderer = INSTANCE;
        Intrinsics.checkNotNullExpressionValue(createForSingleSessionOnDisk, "blob");
        function1.invoke(avatarRenderer.createMedia(createForSingleSessionOnDisk, photo.getSize()));
    }

    private final void renderResource(Context context, Avatar.Resource resource, Function1<? super Media, Unit> function1, Function1<? super Throwable, Unit> function12) {
        renderInBackground(context, function1, function12, new Function1<Canvas, Result<? extends Unit>>(context, resource) { // from class: org.thoughtcrime.securesms.avatar.AvatarRenderer$renderResource$1
            final /* synthetic */ Avatar.Resource $avatar;
            final /* synthetic */ Context $context;

            /* access modifiers changed from: package-private */
            {
                this.$context = r1;
                this.$avatar = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Result<? extends Unit> invoke(Canvas canvas) {
                return Result.m152boximpl(m281invokeIoAF18A(canvas));
            }

            /* renamed from: invoke-IoAF18A  reason: not valid java name */
            public final Object m281invokeIoAF18A(Canvas canvas) {
                Intrinsics.checkNotNullParameter(canvas, "canvas");
                Drawable drawable = AppCompatResources.getDrawable(this.$context, this.$avatar.getResourceId());
                if (drawable != null) {
                    drawable.setColorFilter(new SimpleColorFilter(this.$avatar.getColor().getForegroundColor()));
                    AvatarRenderer avatarRenderer = AvatarRenderer.INSTANCE;
                    double dimensions = (double) avatarRenderer.getDIMENSIONS();
                    Double.isNaN(dimensions);
                    int i = (int) (dimensions * 0.2d);
                    int i2 = i + 0;
                    drawable.setBounds(i2, i2, avatarRenderer.getDIMENSIONS() - i, avatarRenderer.getDIMENSIONS() - i);
                    canvas.drawColor(this.$avatar.getColor().getBackgroundColor());
                    drawable.draw(canvas);
                    Result.Companion companion = Result.Companion;
                    return Result.m153constructorimpl(Unit.INSTANCE);
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            }
        });
    }

    private final void renderInBackground(Context context, Function1<? super Media, Unit> function1, Function1<? super Throwable, Unit> function12, Function1<? super Canvas, Result<Unit>> function13) {
        SignalExecutors.BOUNDED.execute(new Runnable(function12, context, function1) { // from class: org.thoughtcrime.securesms.avatar.AvatarRenderer$$ExternalSyntheticLambda0
            public final /* synthetic */ Function1 f$1;
            public final /* synthetic */ Context f$2;
            public final /* synthetic */ Function1 f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AvatarRenderer.$r8$lambda$J3FnrMjzRnwxNJwgwlRrWXmfXjo(Function1.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    /* renamed from: renderInBackground$lambda-1 */
    public static final void m279renderInBackground$lambda1(Function1 function1, Function1 function12, Context context, Function1 function13) {
        Intrinsics.checkNotNullParameter(function1, "$drawAvatar");
        Intrinsics.checkNotNullParameter(function12, "$onRenderFailed");
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(function13, "$onAvatarRendered");
        int i = DIMENSIONS;
        Bitmap createBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
        Object r3 = ((Result) function1.invoke(new Canvas(createBitmap))).m160unboximpl();
        if (Result.m157isFailureimpl(r3)) {
            createBitmap.recycle();
            function12.invoke(Result.m155exceptionOrNullimpl(r3));
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        boolean compress = createBitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        createBitmap.recycle();
        if (!compress) {
            function12.invoke(new IOException("Failed to compress bitmap"));
            return;
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        Uri createForSingleSessionOnDisk = BlobProvider.getInstance().forData(new ByteArrayInputStream(byteArray), (long) byteArray.length).createForSingleSessionOnDisk(context);
        AvatarRenderer avatarRenderer = INSTANCE;
        Intrinsics.checkNotNullExpressionValue(createForSingleSessionOnDisk, "uri");
        function13.invoke(avatarRenderer.createMedia(createForSingleSessionOnDisk, (long) byteArray.length));
    }

    private final Media createMedia(Uri uri, long j) {
        long currentTimeMillis = System.currentTimeMillis();
        int i = DIMENSIONS;
        return new Media(uri, MediaUtil.IMAGE_JPEG, currentTimeMillis, i, i, j, 0, false, false, Optional.empty(), Optional.empty(), Optional.empty());
    }
}
