package org.thoughtcrime.securesms.avatar;

import android.view.View;
import org.thoughtcrime.securesms.avatar.AvatarColorItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class AvatarColorItem$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ AvatarColorItem.ViewHolder f$0;
    public final /* synthetic */ AvatarColorItem.Model f$1;

    public /* synthetic */ AvatarColorItem$ViewHolder$$ExternalSyntheticLambda0(AvatarColorItem.ViewHolder viewHolder, AvatarColorItem.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        AvatarColorItem.ViewHolder.m278bind$lambda0(this.f$0, this.f$1, view);
    }
}
