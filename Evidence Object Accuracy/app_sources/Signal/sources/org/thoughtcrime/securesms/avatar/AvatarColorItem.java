package org.thoughtcrime.securesms.avatar;

import android.view.View;
import android.widget.ImageView;
import com.airbnb.lottie.SimpleColorFilter;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: AvatarColorItem.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b\b\u0018\u0000 \u00142\u00020\u0001:\u0003\u0014\u0015\u0016B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u00052\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/AvatarColorItem;", "", "colors", "Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "selected", "", "(Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;Z)V", "getColors", "()Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "getSelected", "()Z", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "Companion", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarColorItem {
    public static final Companion Companion = new Companion(null);
    private final Avatars.ColorPair colors;
    private final boolean selected;

    public static /* synthetic */ AvatarColorItem copy$default(AvatarColorItem avatarColorItem, Avatars.ColorPair colorPair, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            colorPair = avatarColorItem.colors;
        }
        if ((i & 2) != 0) {
            z = avatarColorItem.selected;
        }
        return avatarColorItem.copy(colorPair, z);
    }

    public final Avatars.ColorPair component1() {
        return this.colors;
    }

    public final boolean component2() {
        return this.selected;
    }

    public final AvatarColorItem copy(Avatars.ColorPair colorPair, boolean z) {
        Intrinsics.checkNotNullParameter(colorPair, "colors");
        return new AvatarColorItem(colorPair, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AvatarColorItem)) {
            return false;
        }
        AvatarColorItem avatarColorItem = (AvatarColorItem) obj;
        return Intrinsics.areEqual(this.colors, avatarColorItem.colors) && this.selected == avatarColorItem.selected;
    }

    public int hashCode() {
        int hashCode = this.colors.hashCode() * 31;
        boolean z = this.selected;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        return "AvatarColorItem(colors=" + this.colors + ", selected=" + this.selected + ')';
    }

    public AvatarColorItem(Avatars.ColorPair colorPair, boolean z) {
        Intrinsics.checkNotNullParameter(colorPair, "colors");
        this.colors = colorPair;
        this.selected = z;
    }

    public final Avatars.ColorPair getColors() {
        return this.colors;
    }

    public final boolean getSelected() {
        return this.selected;
    }

    /* compiled from: AvatarColorItem.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00040\bj\u0002`\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/AvatarColorItem$Companion;", "", "()V", "registerViewHolder", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "onAvatarColorClickListener", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "Lorg/thoughtcrime/securesms/avatar/OnAvatarColorClickListener;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        /* renamed from: registerViewHolder$lambda-0 */
        public static final MappingViewHolder m277registerViewHolder$lambda0(Function1 function1, View view) {
            Intrinsics.checkNotNullParameter(function1, "$onAvatarColorClickListener");
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new ViewHolder(view, function1);
        }

        public final void registerViewHolder(MappingAdapter mappingAdapter, Function1<? super Avatars.ColorPair, Unit> function1) {
            Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
            Intrinsics.checkNotNullParameter(function1, "onAvatarColorClickListener");
            mappingAdapter.registerFactory(Model.class, new LayoutFactory(new AvatarColorItem$Companion$$ExternalSyntheticLambda0(function1), R.layout.avatar_color_item));
        }
    }

    /* compiled from: AvatarColorItem.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/AvatarColorItem$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "colorItem", "Lorg/thoughtcrime/securesms/avatar/AvatarColorItem;", "(Lorg/thoughtcrime/securesms/avatar/AvatarColorItem;)V", "getColorItem", "()Lorg/thoughtcrime/securesms/avatar/AvatarColorItem;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Model implements MappingModel<Model> {
        private final AvatarColorItem colorItem;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        public Model(AvatarColorItem avatarColorItem) {
            Intrinsics.checkNotNullParameter(avatarColorItem, "colorItem");
            this.colorItem = avatarColorItem;
        }

        public final AvatarColorItem getColorItem() {
            return this.colorItem;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(model.colorItem.getColors(), this.colorItem.getColors());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(model.colorItem, this.colorItem);
        }
    }

    /* compiled from: AvatarColorItem.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B%\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006j\u0002`\t¢\u0006\u0002\u0010\nJ\u0010\u0010\r\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\u0002H\u0016R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006j\u0002`\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/avatar/AvatarColorItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/avatar/AvatarColorItem$Model;", "itemView", "Landroid/view/View;", "onAvatarColorClickListener", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/avatar/Avatars$ColorPair;", "", "Lorg/thoughtcrime/securesms/avatar/OnAvatarColorClickListener;", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "imageView", "Landroid/widget/ImageView;", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final ImageView imageView;
        private final Function1<Avatars.ColorPair, Unit> onAvatarColorClickListener;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.avatar.Avatars$ColorPair, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view, Function1<? super Avatars.ColorPair, Unit> function1) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function1, "onAvatarColorClickListener");
            this.onAvatarColorClickListener = function1;
            View findViewById = findViewById(R.id.avatar_color_item);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.avatar_color_item)");
            this.imageView = (ImageView) findViewById;
        }

        /* renamed from: bind$lambda-0 */
        public static final void m278bind$lambda0(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            viewHolder.onAvatarColorClickListener.invoke(model.getColorItem().getColors());
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.itemView.setOnClickListener(new AvatarColorItem$ViewHolder$$ExternalSyntheticLambda0(this, model));
            this.imageView.getBackground().setColorFilter(new SimpleColorFilter(model.getColorItem().getColors().getBackgroundColor()));
            this.imageView.setSelected(model.getColorItem().getSelected());
        }
    }
}
