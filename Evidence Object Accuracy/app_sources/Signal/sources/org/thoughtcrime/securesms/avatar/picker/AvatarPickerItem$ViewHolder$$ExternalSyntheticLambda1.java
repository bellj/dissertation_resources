package org.thoughtcrime.securesms.avatar.picker;

import android.view.View;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class AvatarPickerItem$ViewHolder$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ AvatarPickerItem.ViewHolder f$0;
    public final /* synthetic */ AvatarPickerItem.Model f$1;

    public /* synthetic */ AvatarPickerItem$ViewHolder$$ExternalSyntheticLambda1(AvatarPickerItem.ViewHolder viewHolder, AvatarPickerItem.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return AvatarPickerItem.ViewHolder.m308bind$lambda1(this.f$0, this.f$1, view);
    }
}
