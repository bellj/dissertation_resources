package org.thoughtcrime.securesms.avatar.picker;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.avatar.Avatar;

/* compiled from: AvatarPickerViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "saved", "Lorg/thoughtcrime/securesms/avatar/Avatar;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AvatarPickerViewModel$onAvatarEditCompleted$1 extends Lambda implements Function1<Avatar, Unit> {
    final /* synthetic */ AvatarPickerViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AvatarPickerViewModel$onAvatarEditCompleted$1(AvatarPickerViewModel avatarPickerViewModel) {
        super(1);
        this.this$0 = avatarPickerViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Avatar avatar) {
        invoke(avatar);
        return Unit.INSTANCE;
    }

    /* renamed from: invoke$lambda-0 */
    public static final AvatarPickerState m342invoke$lambda0(Avatar avatar, AvatarPickerViewModel avatarPickerViewModel, AvatarPickerState avatarPickerState) {
        Intrinsics.checkNotNullParameter(avatar, "$saved");
        Intrinsics.checkNotNullParameter(avatarPickerViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(avatarPickerState, "it");
        return AvatarPickerState.copy$default(avatarPickerState, avatar, null, avatarPickerViewModel.isSaveable(avatar), true, false, 2, null);
    }

    public final void invoke(Avatar avatar) {
        Intrinsics.checkNotNullParameter(avatar, "saved");
        this.this$0.store.update(new Function(this.this$0) { // from class: org.thoughtcrime.securesms.avatar.picker.AvatarPickerViewModel$onAvatarEditCompleted$1$$ExternalSyntheticLambda0
            public final /* synthetic */ AvatarPickerViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AvatarPickerViewModel$onAvatarEditCompleted$1.m342invoke$lambda0(Avatar.this, this.f$1, (AvatarPickerState) obj);
            }
        });
        this.this$0.refreshSelectableAvatars();
    }
}
