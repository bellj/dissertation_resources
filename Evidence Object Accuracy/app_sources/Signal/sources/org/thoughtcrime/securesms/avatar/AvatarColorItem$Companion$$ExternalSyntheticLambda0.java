package org.thoughtcrime.securesms.avatar;

import android.view.View;
import j$.util.function.Function;
import kotlin.jvm.functions.Function1;
import org.thoughtcrime.securesms.avatar.AvatarColorItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class AvatarColorItem$Companion$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ Function1 f$0;

    public /* synthetic */ AvatarColorItem$Companion$$ExternalSyntheticLambda0(Function1 function1) {
        this.f$0 = function1;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return AvatarColorItem.Companion.m277registerViewHolder$lambda0(this.f$0, (View) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
