package org.thoughtcrime.securesms.payments;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.protobuf.InvalidProtocolBufferException;
import java.util.UUID;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;
import org.thoughtcrime.securesms.payments.proto.PaymentMetaData;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class PaymentParcelable implements Parcelable {
    public static final Parcelable.Creator<PaymentParcelable> CREATOR = new Parcelable.Creator<PaymentParcelable>() { // from class: org.thoughtcrime.securesms.payments.PaymentParcelable.1
        @Override // android.os.Parcelable.Creator
        public PaymentParcelable createFromParcel(Parcel parcel) {
            return new PaymentParcelable(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public PaymentParcelable[] newArray(int i) {
            return new PaymentParcelable[i];
        }
    };
    private final Payment payment;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public PaymentParcelable(Payment payment) {
        this.payment = payment;
    }

    protected PaymentParcelable(Parcel parcel) {
        this.payment = new ParcelPayment(parcel);
    }

    public Payment getPayment() {
        return this.payment;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.payment.getUuid().toString());
        parcel.writeParcelable(new PayeeParcelable(this.payment.getPayee()), i);
        parcel.writeLong(this.payment.getBlockIndex());
        parcel.writeLong(this.payment.getBlockTimestamp());
        parcel.writeLong(this.payment.getTimestamp());
        parcel.writeLong(this.payment.getDisplayTimestamp());
        parcel.writeInt(this.payment.getDirection().serialize());
        parcel.writeInt(this.payment.getState().serialize());
        if (this.payment.getFailureReason() == null) {
            parcel.writeInt(-1);
        } else {
            parcel.writeInt(this.payment.getFailureReason().serialize());
        }
        parcel.writeString(this.payment.getNote());
        parcel.writeString(this.payment.getAmount().serialize());
        parcel.writeString(this.payment.getFee().serialize());
        parcel.writeByteArray(this.payment.getPaymentMetaData().toByteArray());
        parcel.writeByte(this.payment.isSeen() ? (byte) 1 : 0);
        parcel.writeString(this.payment.getAmountWithDirection().serialize());
        parcel.writeString(this.payment.getAmountPlusFeeWithDirection().serialize());
        parcel.writeByte(this.payment.isDefrag() ? (byte) 1 : 0);
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static final class ParcelPayment implements Payment {
        private final Money amount;
        private final Money amountPlusFeeWithDirection;
        private final Money amountWithDirection;
        private final long blockIndex;
        private final long blockTimestamp;
        private final Direction direction;
        private final long displayTimestamp;
        private final FailureReason failureReason;
        private final Money fee;
        private final boolean isDefrag;
        private final boolean isSeen;
        private final String note;
        private final Payee payee;
        private final PaymentMetaData paymentMetaData;
        private final State state;
        private final long timestamp;
        private final UUID uuid;

        private ParcelPayment(Parcel parcel) {
            try {
                this.uuid = UUID.fromString(parcel.readString());
                this.payee = ((PayeeParcelable) parcel.readParcelable(PayeeParcelable.class.getClassLoader())).getPayee();
                this.blockIndex = parcel.readLong();
                this.blockTimestamp = parcel.readLong();
                this.timestamp = parcel.readLong();
                this.displayTimestamp = parcel.readLong();
                this.direction = Direction.deserialize(parcel.readInt());
                this.state = State.deserialize(parcel.readInt());
                int readInt = parcel.readInt();
                if (readInt == -1) {
                    this.failureReason = null;
                } else {
                    this.failureReason = FailureReason.deserialize(readInt);
                }
                this.note = parcel.readString();
                this.amount = Money.parse(parcel.readString());
                this.fee = Money.parse(parcel.readString());
                this.paymentMetaData = PaymentMetaData.parseFrom(parcel.createByteArray());
                boolean z = false;
                this.isSeen = parcel.readByte() == 1;
                this.amountWithDirection = Money.parse(parcel.readString());
                this.amountPlusFeeWithDirection = Money.parse(parcel.readString());
                this.isDefrag = parcel.readByte() == 1 ? true : z;
            } catch (InvalidProtocolBufferException | Money.ParseException unused) {
                throw new IllegalArgumentException();
            }
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public UUID getUuid() {
            return this.uuid;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public Payee getPayee() {
            return this.payee;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public long getBlockIndex() {
            return this.blockIndex;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public long getBlockTimestamp() {
            return this.blockTimestamp;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public long getTimestamp() {
            return this.timestamp;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public long getDisplayTimestamp() {
            return this.displayTimestamp;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public Direction getDirection() {
            return this.direction;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public State getState() {
            return this.state;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public FailureReason getFailureReason() {
            return this.failureReason;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public String getNote() {
            return this.note;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public Money getAmount() {
            return this.amount;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public Money getFee() {
            return this.fee;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public PaymentMetaData getPaymentMetaData() {
            return this.paymentMetaData;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public boolean isSeen() {
            return this.isSeen;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public Money getAmountWithDirection() {
            return this.amountWithDirection;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public Money getAmountPlusFeeWithDirection() {
            return this.amountPlusFeeWithDirection;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public boolean isDefrag() {
            return this.isDefrag;
        }
    }
}
