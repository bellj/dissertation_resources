package org.thoughtcrime.securesms.payments.preferences;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.payments.currency.CurrencyExchange;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PaymentsHomeViewModel$2$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ CurrencyExchange f$0;

    public /* synthetic */ PaymentsHomeViewModel$2$$ExternalSyntheticLambda0(CurrencyExchange currencyExchange) {
        this.f$0 = currencyExchange;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return PaymentsHomeViewModel.AnonymousClass2.lambda$onComplete$0(this.f$0, (PaymentsHomeState) obj);
    }
}
