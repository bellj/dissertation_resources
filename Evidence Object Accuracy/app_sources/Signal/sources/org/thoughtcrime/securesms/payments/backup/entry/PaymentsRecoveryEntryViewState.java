package org.thoughtcrime.securesms.payments.backup.entry;

/* loaded from: classes4.dex */
public class PaymentsRecoveryEntryViewState {
    private final boolean canMoveToNext;
    private final String currentEntry;
    private final int wordIndex;

    public PaymentsRecoveryEntryViewState() {
        this.wordIndex = 0;
        this.canMoveToNext = false;
        this.currentEntry = null;
    }

    public PaymentsRecoveryEntryViewState(int i, boolean z, String str) {
        this.wordIndex = i;
        this.canMoveToNext = z;
        this.currentEntry = str;
    }

    public int getWordIndex() {
        return this.wordIndex;
    }

    public boolean canMoveToNext() {
        return this.canMoveToNext;
    }

    public String getCurrentEntry() {
        return this.currentEntry;
    }
}
