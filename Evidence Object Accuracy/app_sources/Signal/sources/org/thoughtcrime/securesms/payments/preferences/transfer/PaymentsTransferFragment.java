package org.thoughtcrime.securesms.payments.preferences.transfer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddress;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;
import org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferViewModel;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class PaymentsTransferFragment extends LoggingFragment {
    private static final String TAG = Log.tag(PaymentsTransferFragment.class);
    private EditText address;

    public PaymentsTransferFragment() {
        super(R.layout.payments_transfer_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        PaymentsTransferViewModel paymentsTransferViewModel = (PaymentsTransferViewModel) new ViewModelProvider(Navigation.findNavController(view).getViewModelStoreOwner(R.id.payments_transfer), new PaymentsTransferViewModel.Factory()).get(PaymentsTransferViewModel.class);
        view.findViewById(R.id.payments_transfer_scan_qr).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsTransferFragment.$r8$lambda$GsvNiNfqztb2iAtHtGOhS0dATGQ(PaymentsTransferFragment.this, view2);
            }
        });
        view.findViewById(R.id.payments_transfer_next).setOnClickListener(new View.OnClickListener(paymentsTransferViewModel) { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferFragment$$ExternalSyntheticLambda5
            public final /* synthetic */ PaymentsTransferViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsTransferFragment.$r8$lambda$n5uiLbM9Klpl4xky_qTEwOUHAbc(PaymentsTransferFragment.this, this.f$1, view2);
            }
        });
        EditText editText = (EditText) view.findViewById(R.id.payments_transfer_to_address);
        this.address = editText;
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener(paymentsTransferViewModel) { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferFragment$$ExternalSyntheticLambda6
            public final /* synthetic */ PaymentsTransferViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return PaymentsTransferFragment.m2427$r8$lambda$s2_Xjh411bTYqUMOeJkO3YxKjI(PaymentsTransferFragment.this, this.f$1, textView, i, keyEvent);
            }
        });
        LiveData<String> address = paymentsTransferViewModel.getAddress();
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        EditText editText2 = this.address;
        Objects.requireNonNull(editText2);
        address.observe(viewLifecycleOwner, new Observer(editText2) { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferFragment$$ExternalSyntheticLambda7
            public final /* synthetic */ EditText f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                this.f$0.setText((String) obj);
            }
        });
        ((Toolbar) view.findViewById(R.id.payments_transfer_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferFragment$$ExternalSyntheticLambda8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsTransferFragment.$r8$lambda$3aDICFXj9l1_7urFU3_Co9rK0_k(PaymentsTransferFragment.this, view2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        scanQrCode();
    }

    public /* synthetic */ void lambda$onViewCreated$1(PaymentsTransferViewModel paymentsTransferViewModel, View view) {
        next(paymentsTransferViewModel.getOwnAddress());
    }

    public /* synthetic */ boolean lambda$onViewCreated$2(PaymentsTransferViewModel paymentsTransferViewModel, TextView textView, int i, KeyEvent keyEvent) {
        if (i == 6) {
            return next(paymentsTransferViewModel.getOwnAddress());
        }
        return false;
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view) {
        ViewUtil.hideKeyboard(requireContext(), view);
        Navigation.findNavController(view).popBackStack();
    }

    private boolean next(MobileCoinPublicAddress mobileCoinPublicAddress) {
        try {
            MobileCoinPublicAddress fromBase58 = MobileCoinPublicAddress.fromBase58(this.address.getText().toString());
            if (mobileCoinPublicAddress.equals(fromBase58)) {
                new AlertDialog.Builder(requireContext()).setTitle(R.string.PaymentsTransferFragment__invalid_address).setMessage(R.string.PaymentsTransferFragment__you_cant_transfer_to_your_own_signal_wallet_address).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
                return false;
            }
            SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), PaymentsTransferFragmentDirections.actionPaymentsTransferToCreatePayment(new PayeeParcelable(fromBase58)).setFinishOnConfirm(PaymentsTransferFragmentArgs.fromBundle(requireArguments()).getFinishOnConfirm()));
            return true;
        } catch (MobileCoinPublicAddress.AddressException e) {
            Log.w(TAG, "Address is not valid", e);
            new AlertDialog.Builder(requireContext()).setTitle(R.string.PaymentsTransferFragment__invalid_address).setMessage(R.string.PaymentsTransferFragment__check_the_wallet_address).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
            return false;
        }
    }

    private void scanQrCode() {
        Permissions.with(requireActivity()).request("android.permission.CAMERA").ifNecessary().withRationaleDialog(getString(R.string.PaymentsTransferFragment__to_scan_a_qr_code_signal_needs), R.drawable.ic_camera_24).onAnyPermanentlyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                PaymentsTransferFragment.$r8$lambda$Bgn6cP0YQ4lvMQ7dwHWKzeGpOMA(PaymentsTransferFragment.this);
            }
        }).onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferFragment$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                PaymentsTransferFragment.$r8$lambda$obaHlgeFgSDDxEhjO7FWoSJIzrE(PaymentsTransferFragment.this);
            }
        }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferFragment$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                PaymentsTransferFragment.m2426$r8$lambda$gFVkFg3WK7ew7KJLSkT3pRG84(PaymentsTransferFragment.this);
            }
        }).execute();
    }

    public /* synthetic */ void lambda$scanQrCode$4() {
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), (int) R.id.action_paymentsTransfer_to_paymentsScanQr);
    }

    public /* synthetic */ void lambda$scanQrCode$5() {
        Toast.makeText(requireContext(), (int) R.string.PaymentsTransferFragment__to_scan_a_qr_code_signal_needs_access_to_the_camera, 1).show();
    }

    public void onCameraPermissionPermanentlyDenied() {
        new AlertDialog.Builder(requireContext()).setTitle(R.string.Permissions_permission_required).setMessage(R.string.PaymentsTransferFragment__signal_needs_the_camera_permission_to_capture_qr_code_go_to_settings).setPositiveButton(R.string.PaymentsTransferFragment__settings, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferFragment$$ExternalSyntheticLambda3
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PaymentsTransferFragment.$r8$lambda$RG7kAemLVMQWxv55liri0DJV09U(PaymentsTransferFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }

    public /* synthetic */ void lambda$onCameraPermissionPermanentlyDenied$6(DialogInterface dialogInterface, int i) {
        requireActivity().startActivity(Permissions.getApplicationSettingsIntent(requireContext()));
    }
}
