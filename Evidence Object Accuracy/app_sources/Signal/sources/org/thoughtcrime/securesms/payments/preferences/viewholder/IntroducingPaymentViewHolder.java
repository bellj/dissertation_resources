package org.thoughtcrime.securesms.payments.preferences.viewholder;

import android.view.View;
import androidx.constraintlayout.widget.Group;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter;
import org.thoughtcrime.securesms.payments.preferences.model.IntroducingPayments;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;

/* loaded from: classes4.dex */
public class IntroducingPaymentViewHolder extends MappingViewHolder<IntroducingPayments> {
    private final View activateButton = findViewById(R.id.payment_preferences_splash_activate);
    private final Group activatingGroup = ((Group) findViewById(R.id.payment_preferences_splash_activating_group));
    private final PaymentsHomeAdapter.Callbacks callbacks;
    private final LearnMoreTextView learnMoreView = ((LearnMoreTextView) findViewById(R.id.payment_preferences_splash_text_1));
    private final View restoreButton = findViewById(R.id.payment_preferences_splash_restore);

    public IntroducingPaymentViewHolder(View view, PaymentsHomeAdapter.Callbacks callbacks) {
        super(view);
        this.callbacks = callbacks;
    }

    public void bind(IntroducingPayments introducingPayments) {
        if (introducingPayments.isActivating()) {
            this.activateButton.setVisibility(4);
            this.activatingGroup.setVisibility(0);
        } else {
            this.activateButton.setVisibility(0);
            this.activatingGroup.setVisibility(8);
        }
        this.learnMoreView.setLearnMoreVisible(true);
        this.learnMoreView.setLink(getContext().getString(R.string.PaymentsHomeFragment__learn_more__activate_payments));
        this.activateButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.viewholder.IntroducingPaymentViewHolder$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                IntroducingPaymentViewHolder.this.lambda$bind$0(view);
            }
        });
        if (SignalStore.paymentsValues().hasPaymentsEntropy()) {
            this.restoreButton.setVisibility(8);
            return;
        }
        this.restoreButton.setVisibility(0);
        this.restoreButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.viewholder.IntroducingPaymentViewHolder$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                IntroducingPaymentViewHolder.this.lambda$bind$1(view);
            }
        });
    }

    public /* synthetic */ void lambda$bind$0(View view) {
        this.callbacks.onActivatePayments();
    }

    public /* synthetic */ void lambda$bind$1(View view) {
        this.callbacks.onRestorePaymentsAccount();
    }
}
