package org.thoughtcrime.securesms.payments;

import java.util.Objects;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class Payee {
    public static final Payee UNKNOWN = new Payee(null, null);
    private final MobileCoinPublicAddress publicAddress;
    private final RecipientId recipientId;

    public static Payee fromRecipientAndAddress(RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress) {
        Objects.requireNonNull(recipientId);
        return new Payee(recipientId, mobileCoinPublicAddress);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public Payee(RecipientId recipientId) {
        this(recipientId, null);
        Objects.requireNonNull(recipientId);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public Payee(MobileCoinPublicAddress mobileCoinPublicAddress) {
        this(null, mobileCoinPublicAddress);
        Objects.requireNonNull(mobileCoinPublicAddress);
    }

    private Payee(RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress) {
        this.recipientId = recipientId;
        this.publicAddress = mobileCoinPublicAddress;
    }

    public boolean hasRecipientId() {
        RecipientId recipientId = this.recipientId;
        return recipientId != null && !recipientId.isUnknown();
    }

    public RecipientId requireRecipientId() {
        RecipientId recipientId = this.recipientId;
        Objects.requireNonNull(recipientId);
        return recipientId;
    }

    public boolean hasPublicAddress() {
        return this.publicAddress != null;
    }

    public MobileCoinPublicAddress requirePublicAddress() {
        MobileCoinPublicAddress mobileCoinPublicAddress = this.publicAddress;
        Objects.requireNonNull(mobileCoinPublicAddress);
        return mobileCoinPublicAddress;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Payee.class != obj.getClass()) {
            return false;
        }
        Payee payee = (Payee) obj;
        if (!Objects.equals(this.recipientId, payee.recipientId) || !Objects.equals(this.publicAddress, payee.publicAddress)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.recipientId, this.publicAddress);
    }
}
