package org.thoughtcrime.securesms.payments.reconciliation;

import java.util.Comparator;
import org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LedgerReconcile$BlockDetail$$ExternalSyntheticLambda0 implements Comparator {
    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return LedgerReconcile.BlockDetail.lambda$static$0((LedgerReconcile.BlockDetail) obj, (LedgerReconcile.BlockDetail) obj2);
    }
}
