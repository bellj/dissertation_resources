package org.thoughtcrime.securesms.payments;

import com.annimon.stream.ComparatorCompat;
import java.util.Comparator;
import java.util.UUID;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.proto.PaymentMetaData;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public interface Payment {
    public static final Comparator<Payment> ASCENDING_BLOCK_INDEX;
    public static final Comparator<Payment> DESCENDING_BLOCK_INDEX;
    public static final Comparator<Payment> DESCENDING_BLOCK_INDEX_UNKNOWN_FIRST;
    public static final Comparator<Payment> UNKNOWN_BLOCK_INDEX_FIRST;

    Money getAmount();

    Money getAmountPlusFeeWithDirection();

    Money getAmountWithDirection();

    long getBlockIndex();

    long getBlockTimestamp();

    Direction getDirection();

    long getDisplayTimestamp();

    FailureReason getFailureReason();

    Money getFee();

    String getNote();

    Payee getPayee();

    PaymentMetaData getPaymentMetaData();

    State getState();

    long getTimestamp();

    UUID getUuid();

    boolean isDefrag();

    boolean isSeen();

    /* renamed from: org.thoughtcrime.securesms.payments.Payment$-CC */
    /* loaded from: classes4.dex */
    public final /* synthetic */ class CC {
        static {
            Comparator<Payment> comparator = Payment.UNKNOWN_BLOCK_INDEX_FIRST;
        }

        public static /* synthetic */ int lambda$static$0(Payment payment, Payment payment2) {
            long blockIndex = payment2.getBlockIndex();
            boolean z = true;
            boolean z2 = blockIndex == 0;
            if (payment.getBlockIndex() != 0) {
                z = false;
            }
            return Boolean.compare(z2, z);
        }

        public static long $default$getDisplayTimestamp(Payment payment) {
            long blockTimestamp = payment.getBlockTimestamp();
            if (blockTimestamp > 0) {
                return blockTimestamp;
            }
            return payment.getTimestamp();
        }

        public static Money $default$getAmountWithDirection(Payment payment) {
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$Direction[payment.getDirection().ordinal()];
            if (i == 1) {
                return payment.getAmount().negate();
            }
            if (i == 2) {
                return payment.getAmount();
            }
            throw new AssertionError();
        }

        public static Money $default$getAmountPlusFeeWithDirection(Payment payment) {
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$Direction[payment.getDirection().ordinal()];
            if (i == 1) {
                return payment.getAmount().add(payment.getFee()).negate();
            }
            if (i == 2) {
                return payment.getAmount();
            }
            throw new AssertionError();
        }

        public static boolean $default$isDefrag(Payment payment) {
            return payment.getDirection() == Direction.SENT && payment.getPayee().hasRecipientId() && payment.getPayee().requireRecipientId().equals(Recipient.self().getId());
        }
    }

    static {
        Payment$$ExternalSyntheticLambda0 payment$$ExternalSyntheticLambda0 = new Comparator() { // from class: org.thoughtcrime.securesms.payments.Payment$$ExternalSyntheticLambda0
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return Payment.CC.lambda$static$0((Payment) obj, (Payment) obj2);
            }
        };
        UNKNOWN_BLOCK_INDEX_FIRST = payment$$ExternalSyntheticLambda0;
        Payment$$ExternalSyntheticLambda1 payment$$ExternalSyntheticLambda1 = new Comparator() { // from class: org.thoughtcrime.securesms.payments.Payment$$ExternalSyntheticLambda1
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return Long.compare(((Payment) obj).getBlockIndex(), ((Payment) obj2).getBlockIndex());
            }
        };
        ASCENDING_BLOCK_INDEX = payment$$ExternalSyntheticLambda1;
        Comparator<Payment> reversed = ComparatorCompat.reversed(payment$$ExternalSyntheticLambda1);
        DESCENDING_BLOCK_INDEX = reversed;
        DESCENDING_BLOCK_INDEX_UNKNOWN_FIRST = ComparatorCompat.chain(payment$$ExternalSyntheticLambda0).thenComparing((Comparator) reversed);
    }

    /* renamed from: org.thoughtcrime.securesms.payments.Payment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$Direction;

        static {
            int[] iArr = new int[Direction.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$Direction = iArr;
            try {
                iArr[Direction.SENT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$Direction[Direction.RECEIVED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }
}
