package org.thoughtcrime.securesms.payments.deactivate;

import androidx.lifecycle.Observer;
import org.thoughtcrime.securesms.payments.MoneyView;
import org.whispersystems.signalservice.api.payments.Money;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class DeactivateWalletFragment$$ExternalSyntheticLambda2 implements Observer {
    public final /* synthetic */ MoneyView f$0;

    public /* synthetic */ DeactivateWalletFragment$$ExternalSyntheticLambda2(MoneyView moneyView) {
        this.f$0 = moneyView;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.setMoney((Money) obj);
    }
}
