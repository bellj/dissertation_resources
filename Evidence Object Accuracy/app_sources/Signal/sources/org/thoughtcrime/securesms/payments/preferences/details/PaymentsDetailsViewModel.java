package org.thoughtcrime.securesms.payments.preferences.details;

import android.content.Context;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.Locale;
import java.util.UUID;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.payments.PaymentTransactionLiveData;
import org.thoughtcrime.securesms.payments.UnreadPaymentsRepository;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
final class PaymentsDetailsViewModel extends ViewModel {
    private final LiveData<Boolean> paymentExists;
    private final LiveData<ViewState> viewState;

    PaymentsDetailsViewModel(UUID uuid) {
        PaymentTransactionLiveData paymentTransactionLiveData = new PaymentTransactionLiveData(uuid);
        this.viewState = LiveDataUtil.combineLatest(paymentTransactionLiveData, Transformations.switchMap(paymentTransactionLiveData, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.details.PaymentsDetailsViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return PaymentsDetailsViewModel.lambda$new$0((PaymentDatabase.PaymentTransaction) obj);
            }
        }), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.payments.preferences.details.PaymentsDetailsViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return PaymentsDetailsViewModel.lambda$new$1((PaymentDatabase.PaymentTransaction) obj, (Recipient) obj2);
            }
        });
        this.paymentExists = Transformations.map(paymentTransactionLiveData, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.details.PaymentsDetailsViewModel$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return PaymentsDetailsViewModel.lambda$new$2((PaymentDatabase.PaymentTransaction) obj);
            }
        });
        new UnreadPaymentsRepository().markPaymentSeen(uuid);
    }

    public static /* synthetic */ LiveData lambda$new$0(PaymentDatabase.PaymentTransaction paymentTransaction) {
        if (paymentTransaction == null || !paymentTransaction.getPayee().hasRecipientId()) {
            return LiveDataUtil.just(Recipient.UNKNOWN);
        }
        return Recipient.live(paymentTransaction.getPayee().requireRecipientId()).getLiveData();
    }

    public static /* synthetic */ ViewState lambda$new$1(PaymentDatabase.PaymentTransaction paymentTransaction, Recipient recipient) {
        return new ViewState(paymentTransaction, recipient);
    }

    public static /* synthetic */ Boolean lambda$new$2(PaymentDatabase.PaymentTransaction paymentTransaction) {
        return Boolean.valueOf(paymentTransaction != null);
    }

    public LiveData<ViewState> getViewState() {
        return this.viewState;
    }

    public LiveData<Boolean> getPaymentExists() {
        return this.paymentExists;
    }

    /* loaded from: classes4.dex */
    public static class ViewState {
        private final PaymentDatabase.PaymentTransaction payment;
        private final Recipient recipient;

        private ViewState(PaymentDatabase.PaymentTransaction paymentTransaction, Recipient recipient) {
            this.payment = paymentTransaction;
            this.recipient = recipient;
        }

        public Recipient getRecipient() {
            return this.recipient;
        }

        public PaymentDatabase.PaymentTransaction getPayment() {
            return this.payment;
        }

        public String getDate() {
            return DateUtils.formatDate(Locale.getDefault(), this.payment.getDisplayTimestamp());
        }

        public String getTime(Context context) {
            return DateUtils.getTimeString(context, Locale.getDefault(), this.payment.getDisplayTimestamp());
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final UUID paymentId;

        public Factory(UUID uuid) {
            this.paymentId = uuid;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new PaymentsDetailsViewModel(this.paymentId));
        }
    }
}
