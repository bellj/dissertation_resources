package org.thoughtcrime.securesms.payments.preferences.viewholder;

import android.view.View;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter;
import org.thoughtcrime.securesms.payments.preferences.model.SeeAll;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class SeeAllViewHolder extends MappingViewHolder<SeeAll> {
    private final PaymentsHomeAdapter.Callbacks callbacks;
    private final View seeAllButton;

    public SeeAllViewHolder(View view, PaymentsHomeAdapter.Callbacks callbacks) {
        super(view);
        this.callbacks = callbacks;
        this.seeAllButton = view.findViewById(R.id.payments_home_see_all_item_button);
    }

    public /* synthetic */ void lambda$bind$0(SeeAll seeAll, View view) {
        this.callbacks.onSeeAll(seeAll.getPaymentType());
    }

    public void bind(SeeAll seeAll) {
        this.seeAllButton.setOnClickListener(new View.OnClickListener(seeAll) { // from class: org.thoughtcrime.securesms.payments.preferences.viewholder.SeeAllViewHolder$$ExternalSyntheticLambda0
            public final /* synthetic */ SeeAll f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SeeAllViewHolder.this.lambda$bind$0(this.f$1, view);
            }
        });
    }
}
