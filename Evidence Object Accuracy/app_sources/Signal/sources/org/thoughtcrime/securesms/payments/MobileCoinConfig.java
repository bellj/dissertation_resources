package org.thoughtcrime.securesms.payments;

import android.net.Uri;
import com.mobilecoin.lib.ClientConfig;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.internal.push.AuthCredentials;

/* loaded from: classes4.dex */
public abstract class MobileCoinConfig {
    /* access modifiers changed from: package-private */
    public abstract AuthCredentials getAuth() throws IOException;

    /* access modifiers changed from: package-private */
    public abstract ClientConfig getConfig();

    /* access modifiers changed from: package-private */
    public abstract Uri getConsensusUri();

    /* access modifiers changed from: package-private */
    public abstract byte[] getFogAuthoritySpki();

    /* access modifiers changed from: package-private */
    public abstract Uri getFogReportUri();

    /* access modifiers changed from: package-private */
    public abstract Uri getFogUri();

    public static MobileCoinConfig getTestNet(SignalServiceAccountManager signalServiceAccountManager) {
        return new MobileCoinTestNetConfig(signalServiceAccountManager);
    }

    public static MobileCoinConfig getMainNet(SignalServiceAccountManager signalServiceAccountManager) {
        return new MobileCoinMainNetConfig(signalServiceAccountManager);
    }

    public static Set<X509Certificate> getTrustRoots(int i) {
        try {
            InputStream openRawResource = ApplicationDependencies.getApplication().getResources().openRawResource(i);
            try {
                Collection<? extends Certificate> generateCertificates = CertificateFactory.getInstance("X.509").generateCertificates(openRawResource);
                HashSet hashSet = new HashSet(generateCertificates.size());
                for (Certificate certificate : generateCertificates) {
                    hashSet.add((X509Certificate) certificate);
                }
                if (openRawResource != null) {
                    openRawResource.close();
                }
                return hashSet;
            } catch (Throwable th) {
                if (openRawResource != null) {
                    try {
                        openRawResource.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        } catch (IOException | CertificateException e) {
            throw new AssertionError(e);
        }
    }
}
