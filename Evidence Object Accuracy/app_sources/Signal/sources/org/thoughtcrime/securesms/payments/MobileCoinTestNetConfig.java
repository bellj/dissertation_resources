package org.thoughtcrime.securesms.payments;

import android.net.Uri;
import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.Verifier;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.util.Hex;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.Set;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.internal.push.AuthCredentials;

/* loaded from: classes4.dex */
final class MobileCoinTestNetConfig extends MobileCoinConfig {
    private static final short CONSENSUS_PRODUCT_ID;
    private static final short FOG_LEDGER_PRODUCT_ID;
    private static final short FOG_REPORT_PRODUCT_ID;
    private static final short FOG_VIEW_PRODUCT_ID;
    private static final short SECURITY_VERSION;
    private final SignalServiceAccountManager signalServiceAccountManager;

    public MobileCoinTestNetConfig(SignalServiceAccountManager signalServiceAccountManager) {
        this.signalServiceAccountManager = signalServiceAccountManager;
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public Uri getConsensusUri() {
        return Uri.parse("mc://node1.test.mobilecoin.com");
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public Uri getFogUri() {
        return Uri.parse("fog://service.fog.mob.staging.namda.net");
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public Uri getFogReportUri() {
        return Uri.parse("fog://fog-rpt-stg.namda.net");
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public byte[] getFogAuthoritySpki() {
        return Base64.decodeOrThrow("MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAoCMq8nnjTq5EEQ4EI7yr\nABL9P4y4h1P/h0DepWgXx+w/fywcfRSZINxbaMpvcV3uSJayExrpV1KmaS2wfASe\nYhSj+rEzAm0XUOw3Q94NOx5A/dOQag/d1SS6/QpF3PQYZTULnRFetmM4yzEnXsXc\nWtzEu0hh02wYJbLeAq4CCcPTPe2qckrbUP9sD18/KOzzNeypF4p5dQ2m/ezfxtga\nLvdUMVDVIAs2v9a5iu6ce4bIcwTIUXgX0w3+UKRx8zqowc3HIqo9yeaGn4ZOwQHv\nAJZecPmb2pH1nK+BtDUvHpvf+Y3/NJxwh+IPp6Ef8aoUxs2g5oIBZ3Q31fjS2Bh2\ngmwoVooyytEysPAHvRPVBxXxLi36WpKfk1Vq8K7cgYh3IraOkH2/l2Pyi8EYYFkW\nsLYofYogaiPzVoq2ZdcizfoJWIYei5mgq+8m0ZKZYLebK1i2GdseBJNIbSt3wCNX\nZxyN6uqFHOCB29gmA5cbKvs/j9mDz64PJe9LCanqcDQV1U5l9dt9UdmUt7Ab1PjB\ntoIFaP+u473Z0hmZdCgAivuiBMMYMqt2V2EIw4IXLASE3roLOYp0p7h0IQHb+lVI\nuEl0ZmwAI30ZmzgcWc7RBeWD1/zNt55zzhfPRLx/DfDY5Kdp6oFHWMvI2r1/oZkd\nhjFp7pV6qrl7vOyR5QqmuRkCAwEAAQ==");
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public AuthCredentials getAuth() throws IOException {
        return this.signalServiceAccountManager.getPaymentsAuthorization();
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public ClientConfig getConfig() {
        try {
            byte[] byteArray = Hex.toByteArray("9659ea738275b3999bf1700398b60281be03af5cb399738a89b49ea2496595af");
            byte[] byteArray2 = Hex.toByteArray("a4764346f91979b4906d4ce26102228efe3aba39216dec1e7d22e6b06f919f11");
            byte[] byteArray3 = Hex.toByteArray("768f7bea6171fb83d775ee8485e4b5fcebf5f664ca7e8b9ceef9c7c21e9d9bf3");
            byte[] byteArray4 = Hex.toByteArray("e154f108c7758b5aa7161c3824c176f0c20f63012463bf3cc5651e678f02fb9e");
            byte[] byteArray5 = Hex.toByteArray("bf7fa957a6a94acb588851bc8767e0ca57706c79f4fc2aa6bcb993012c3c386c");
            Set<X509Certificate> trustRoots = MobileCoinConfig.getTrustRoots(R.raw.signal_mobilecoin_authority);
            ClientConfig clientConfig = new ClientConfig();
            String[] strArr = {"INTEL-SA-00334"};
            clientConfig.logAdapter = new MobileCoinLogAdapter();
            clientConfig.fogView = new ClientConfig.Service().withTrustRoots(trustRoots).withVerifier(new Verifier().withMrEnclave(byteArray4, null, strArr).withMrSigner(byteArray5, FOG_VIEW_PRODUCT_ID, 1, null, strArr));
            clientConfig.fogLedger = new ClientConfig.Service().withTrustRoots(trustRoots).withVerifier(new Verifier().withMrEnclave(byteArray3, null, strArr).withMrSigner(byteArray5, FOG_LEDGER_PRODUCT_ID, 1, null, strArr));
            clientConfig.consensus = new ClientConfig.Service().withTrustRoots(trustRoots).withVerifier(new Verifier().withMrEnclave(byteArray, null, strArr).withMrSigner(byteArray5, 1, 1, null, strArr));
            clientConfig.f12report = new ClientConfig.Service().withVerifier(new Verifier().withMrEnclave(byteArray2, null, strArr).withMrSigner(byteArray5, FOG_REPORT_PRODUCT_ID, 1, null, strArr));
            return clientConfig;
        } catch (AttestationException unused) {
            throw new IllegalStateException();
        }
    }
}
