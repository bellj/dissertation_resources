package org.thoughtcrime.securesms.payments.backup.phrase;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
public class ClearClipboardAlarmReceiver extends BroadcastReceiver {
    private static final String TAG = Log.tag(ClearClipboardAlarmReceiver.class);

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: clearing clipboard");
        ServiceUtil.getClipboardManager(context).setPrimaryClip(ClipData.newPlainText(context.getString(R.string.app_name), " "));
    }
}
