package org.thoughtcrime.securesms.payments.preferences.transfer;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class PaymentsTransferQrScanFragmentDirections {
    private PaymentsTransferQrScanFragmentDirections() {
    }

    public static NavDirections actionPaymentsScanQrPop() {
        return new ActionOnlyNavDirections(R.id.action_paymentsScanQr_pop);
    }
}
