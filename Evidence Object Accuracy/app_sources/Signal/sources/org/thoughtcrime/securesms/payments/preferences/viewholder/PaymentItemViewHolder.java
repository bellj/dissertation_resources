package org.thoughtcrime.securesms.payments.preferences.viewholder;

import android.view.View;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter;
import org.thoughtcrime.securesms.payments.preferences.model.PaymentItem;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel;
import org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder;

/* loaded from: classes4.dex */
public class PaymentItemViewHolder extends MappingViewHolder<PaymentItem> {
    private final TextView amount = ((TextView) findViewById(R.id.payments_home_payment_item_amount));
    private final AvatarImageView avatar = ((AvatarImageView) findViewById(R.id.recipient_view_avatar));
    private final PaymentsHomeAdapter.Callbacks callbacks;
    private final TextView date = ((TextView) findViewById(R.id.payments_home_payment_item_date));
    private final RecipientViewHolder<RecipientMappingModel.RecipientIdMappingModel> delegate;
    private final View inProgress = findViewById(R.id.payments_home_payment_item_avatar_progress_overlay);
    private final TextView name = ((TextView) findViewById(R.id.recipient_view_name));
    private final View unreadIndicator = findViewById(R.id.unread_indicator);

    public PaymentItemViewHolder(View view, PaymentsHomeAdapter.Callbacks callbacks) {
        super(view);
        this.delegate = new RecipientViewHolder<>(view, null, true);
        this.callbacks = callbacks;
    }

    public void bind(PaymentItem paymentItem) {
        if (!paymentItem.hasRecipient() || paymentItem.isDefrag()) {
            this.name.setText(paymentItem.getTransactionName(getContext()));
            this.avatar.disableQuickContact();
            this.avatar.setNonAvatarImageResource(paymentItem.getTransactionAvatar());
        } else {
            this.delegate.bind((RecipientViewHolder<RecipientMappingModel.RecipientIdMappingModel>) paymentItem.getRecipientIdModel());
        }
        int i = 0;
        this.inProgress.setVisibility(paymentItem.isInProgress() ? 0 : 8);
        this.date.setText(paymentItem.getDate(this.context));
        this.amount.setText(SpanUtil.color(ContextCompat.getColor(this.context, paymentItem.getAmountColor()), paymentItem.getAmount(getContext())));
        this.itemView.setOnClickListener(new View.OnClickListener(paymentItem) { // from class: org.thoughtcrime.securesms.payments.preferences.viewholder.PaymentItemViewHolder$$ExternalSyntheticLambda0
            public final /* synthetic */ PaymentItem f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PaymentItemViewHolder.this.lambda$bind$0(this.f$1, view);
            }
        });
        View view = this.unreadIndicator;
        if (!paymentItem.isUnread()) {
            i = 8;
        }
        view.setVisibility(i);
    }

    public /* synthetic */ void lambda$bind$0(PaymentItem paymentItem, View view) {
        this.callbacks.onPaymentItem(paymentItem);
    }
}
