package org.thoughtcrime.securesms.payments.preferences.transfer;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class PaymentsTransferFragmentArgs {
    private final HashMap arguments;

    private PaymentsTransferFragmentArgs() {
        this.arguments = new HashMap();
    }

    private PaymentsTransferFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static PaymentsTransferFragmentArgs fromBundle(Bundle bundle) {
        PaymentsTransferFragmentArgs paymentsTransferFragmentArgs = new PaymentsTransferFragmentArgs();
        bundle.setClassLoader(PaymentsTransferFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("finish_on_confirm")) {
            paymentsTransferFragmentArgs.arguments.put("finish_on_confirm", Boolean.valueOf(bundle.getBoolean("finish_on_confirm")));
        } else {
            paymentsTransferFragmentArgs.arguments.put("finish_on_confirm", Boolean.FALSE);
        }
        return paymentsTransferFragmentArgs;
    }

    public boolean getFinishOnConfirm() {
        return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("finish_on_confirm")) {
            bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
        } else {
            bundle.putBoolean("finish_on_confirm", false);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PaymentsTransferFragmentArgs paymentsTransferFragmentArgs = (PaymentsTransferFragmentArgs) obj;
        return this.arguments.containsKey("finish_on_confirm") == paymentsTransferFragmentArgs.arguments.containsKey("finish_on_confirm") && getFinishOnConfirm() == paymentsTransferFragmentArgs.getFinishOnConfirm();
    }

    public int hashCode() {
        return 31 + (getFinishOnConfirm() ? 1 : 0);
    }

    public String toString() {
        return "PaymentsTransferFragmentArgs{finishOnConfirm=" + getFinishOnConfirm() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(PaymentsTransferFragmentArgs paymentsTransferFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(paymentsTransferFragmentArgs.arguments);
        }

        public Builder() {
            this.arguments = new HashMap();
        }

        public PaymentsTransferFragmentArgs build() {
            return new PaymentsTransferFragmentArgs(this.arguments);
        }

        public Builder setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }
    }
}
