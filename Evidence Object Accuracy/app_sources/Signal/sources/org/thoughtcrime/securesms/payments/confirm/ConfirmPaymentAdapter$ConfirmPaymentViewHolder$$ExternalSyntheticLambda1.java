package org.thoughtcrime.securesms.payments.confirm;

import com.airbnb.lottie.LottieAnimationView;
import org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConfirmPaymentAdapter$ConfirmPaymentViewHolder$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ LottieAnimationView f$0;

    public /* synthetic */ ConfirmPaymentAdapter$ConfirmPaymentViewHolder$$ExternalSyntheticLambda1(LottieAnimationView lottieAnimationView) {
        this.f$0 = lottieAnimationView;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ConfirmPaymentAdapter.ConfirmPaymentViewHolder.lambda$playNextAnimation$1(this.f$0);
    }
}
