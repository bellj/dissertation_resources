package org.thoughtcrime.securesms.payments;

import java.io.IOException;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.currency.CurrencyExchange;
import org.whispersystems.signalservice.api.payments.CurrencyConversion;
import org.whispersystems.signalservice.api.payments.CurrencyConversions;

/* loaded from: classes4.dex */
public final class Payments {
    private static final long MINIMUM_ELAPSED_TIME_BETWEEN_REFRESH = TimeUnit.MINUTES.toMillis(1);
    private static final String TAG = Log.tag(Payments.class);
    private CurrencyConversions currencyConversions;
    private final MobileCoinConfig mobileCoinConfig;
    private Wallet wallet;

    public Payments(MobileCoinConfig mobileCoinConfig) {
        this.mobileCoinConfig = mobileCoinConfig;
    }

    public synchronized Wallet getWallet() {
        Wallet wallet = this.wallet;
        if (wallet != null) {
            return wallet;
        }
        Entropy paymentsEntropy = SignalStore.paymentsValues().getPaymentsEntropy();
        MobileCoinConfig mobileCoinConfig = this.mobileCoinConfig;
        Objects.requireNonNull(paymentsEntropy);
        Wallet wallet2 = new Wallet(mobileCoinConfig, paymentsEntropy);
        this.wallet = wallet2;
        return wallet2;
    }

    public synchronized void closeWallet() {
        this.wallet = null;
    }

    public synchronized CurrencyExchange getCurrencyExchange(boolean z) throws IOException {
        CurrencyConversion next;
        CurrencyConversions currencyConversions = this.currencyConversions;
        if (currencyConversions == null || shouldRefresh(z, currencyConversions.getTimestamp())) {
            Log.i(TAG, "Currency conversion data is unavailable or a refresh was requested and available");
            CurrencyConversions currencyConversions2 = ApplicationDependencies.getSignalServiceAccountManager().getCurrencyConversions();
            if (this.currencyConversions == null || (currencyConversions2 != null && currencyConversions2.getTimestamp() > this.currencyConversions.getTimestamp())) {
                this.currencyConversions = currencyConversions2;
            }
        }
        CurrencyConversions currencyConversions3 = this.currencyConversions;
        if (currencyConversions3 != null) {
            Iterator<CurrencyConversion> it = currencyConversions3.getCurrencies().iterator();
            while (it.hasNext()) {
                next = it.next();
                if ("MOB".equals(next.getBase())) {
                }
            }
        }
        throw new IOException("Unable to retrieve currency conversions");
        return new CurrencyExchange(next.getConversions(), this.currencyConversions.getTimestamp());
    }

    private boolean shouldRefresh(boolean z, long j) {
        return z && System.currentTimeMillis() - j >= MINIMUM_ELAPSED_TIME_BETWEEN_REFRESH;
    }
}
