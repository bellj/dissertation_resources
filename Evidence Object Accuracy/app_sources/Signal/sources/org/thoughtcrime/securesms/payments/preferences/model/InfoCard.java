package org.thoughtcrime.securesms.payments.preferences.model;

import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.PaymentsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes4.dex */
public class InfoCard implements MappingModel<InfoCard> {
    private final int actionId;
    private final Runnable dismiss;
    private final int iconId;
    private final int messageId;
    private final int titleId;
    private final Type type;

    /* loaded from: classes4.dex */
    public enum Type {
        RECORD_RECOVERY_PHASE,
        UPDATE_YOUR_PIN,
        ABOUT_MOBILECOIN,
        ADDING_TO_YOUR_WALLET,
        CASHING_OUT
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(InfoCard infoCard) {
        return MappingModel.CC.$default$getChangePayload(this, infoCard);
    }

    private InfoCard(int i, int i2, int i3, int i4, Type type, Runnable runnable) {
        this.titleId = i;
        this.messageId = i2;
        this.actionId = i3;
        this.iconId = i4;
        this.type = type;
        this.dismiss = runnable;
    }

    public int getTitleId() {
        return this.titleId;
    }

    public int getMessageId() {
        return this.messageId;
    }

    public int getActionId() {
        return this.actionId;
    }

    public Type getType() {
        return this.type;
    }

    public int getIconId() {
        return this.iconId;
    }

    public void dismiss() {
        this.dismiss.run();
    }

    public boolean areItemsTheSame(InfoCard infoCard) {
        return infoCard.type == this.type;
    }

    public boolean areContentsTheSame(InfoCard infoCard) {
        return infoCard.titleId == this.titleId && infoCard.messageId == this.messageId && infoCard.actionId == this.actionId && infoCard.iconId == this.iconId && infoCard.type == this.type;
    }

    public static List<InfoCard> getInfoCards() {
        ArrayList arrayList = new ArrayList(Type.values().length);
        PaymentsValues paymentsValues = SignalStore.paymentsValues();
        if (paymentsValues.showRecoveryPhraseInfoCard()) {
            arrayList.add(new InfoCard(R.string.payment_info_card_record_recovery_phrase, R.string.payment_info_card_your_recovery_phrase_gives_you, R.string.payment_info_card_record_your_phrase, R.drawable.ic_payments_info_card_restore_80, Type.RECORD_RECOVERY_PHASE, new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.model.InfoCard$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    PaymentsValues.this.dismissRecoveryPhraseInfoCard();
                }
            }));
        }
        if (paymentsValues.showUpdatePinInfoCard()) {
            arrayList.add(new InfoCard(R.string.payment_info_card_update_your_pin, R.string.payment_info_card_with_a_high_balance, R.string.payment_info_card_update_pin, R.drawable.ic_payments_info_card_pin_80, Type.UPDATE_YOUR_PIN, new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.model.InfoCard$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    PaymentsValues.this.dismissUpdatePinInfoCard();
                }
            }));
        }
        if (paymentsValues.showAboutMobileCoinInfoCard()) {
            arrayList.add(new InfoCard(R.string.payment_info_card_about_mobilecoin, R.string.payment_info_card_mobilecoin_is_a_new_privacy_focused_digital_currency, R.string.LearnMoreTextView_learn_more, R.drawable.ic_about_mc_80, Type.ABOUT_MOBILECOIN, new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.model.InfoCard$$ExternalSyntheticLambda2
                @Override // java.lang.Runnable
                public final void run() {
                    PaymentsValues.this.dismissAboutMobileCoinInfoCard();
                }
            }));
        }
        if (paymentsValues.showAddingToYourWalletInfoCard()) {
            arrayList.add(new InfoCard(R.string.payment_info_card_adding_funds, R.string.payment_info_card_you_can_add_funds_for_use_in, R.string.LearnMoreTextView_learn_more, R.drawable.ic_add_money_80, Type.ADDING_TO_YOUR_WALLET, new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.model.InfoCard$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    PaymentsValues.this.dismissAddingToYourWalletInfoCard();
                }
            }));
        }
        if (paymentsValues.showCashingOutInfoCard()) {
            arrayList.add(new InfoCard(R.string.payment_info_card_cashing_out, R.string.payment_info_card_you_can_cash_out_mobilecoin, R.string.LearnMoreTextView_learn_more, R.drawable.ic_cash_out_80, Type.CASHING_OUT, new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.model.InfoCard$$ExternalSyntheticLambda4
                @Override // java.lang.Runnable
                public final void run() {
                    PaymentsValues.this.dismissCashingOutInfoCard();
                }
            }));
        }
        return arrayList;
    }
}
