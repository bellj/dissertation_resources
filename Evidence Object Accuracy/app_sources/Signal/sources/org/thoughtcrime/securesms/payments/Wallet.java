package org.thoughtcrime.securesms.payments;

import com.google.protobuf.ByteString;
import com.mobilecoin.lib.AccountKey;
import com.mobilecoin.lib.AccountSnapshot;
import com.mobilecoin.lib.DefragmentationDelegate;
import com.mobilecoin.lib.MobileCoinClient;
import com.mobilecoin.lib.OwnedTxOut;
import com.mobilecoin.lib.PendingTransaction;
import com.mobilecoin.lib.Receipt;
import com.mobilecoin.lib.Transaction;
import com.mobilecoin.lib.UnsignedLong;
import com.mobilecoin.lib.exceptions.AmountDecoderException;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.BadEntropyException;
import com.mobilecoin.lib.exceptions.FogReportException;
import com.mobilecoin.lib.exceptions.FragmentedAccountException;
import com.mobilecoin.lib.exceptions.InsufficientFundsException;
import com.mobilecoin.lib.exceptions.InvalidFogResponse;
import com.mobilecoin.lib.exceptions.InvalidReceiptException;
import com.mobilecoin.lib.exceptions.InvalidTransactionException;
import com.mobilecoin.lib.exceptions.InvalidUriException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.exceptions.SerializationException;
import com.mobilecoin.lib.exceptions.TransactionBuilderException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeoutException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.keyvalue.PaymentsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.PaymentTransactionId;
import org.thoughtcrime.securesms.payments.TransactionSubmissionResult;
import org.thoughtcrime.securesms.payments.proto.MobileCoinLedger;
import org.whispersystems.signalservice.api.payments.Money;
import org.whispersystems.signalservice.api.util.Uint64RangeException;
import org.whispersystems.signalservice.api.util.Uint64Util;
import org.whispersystems.signalservice.internal.push.AuthCredentials;

/* loaded from: classes4.dex */
public final class Wallet {
    private static final String TAG = Log.tag(Wallet.class);
    private final AccountKey account;
    private final MobileCoinClient mobileCoinClient;
    private final MobileCoinConfig mobileCoinConfig;
    private final MobileCoinPublicAddress publicAddress;

    /* loaded from: classes4.dex */
    public enum TransactionStatus {
        COMPLETE,
        IN_PROGRESS,
        FAILED
    }

    public Wallet(MobileCoinConfig mobileCoinConfig, Entropy entropy) {
        this.mobileCoinConfig = mobileCoinConfig;
        try {
            AccountKey fromBip39Entropy = AccountKey.fromBip39Entropy(entropy.getBytes(), 0, mobileCoinConfig.getFogReportUri(), "", mobileCoinConfig.getFogAuthoritySpki());
            this.account = fromBip39Entropy;
            this.publicAddress = new MobileCoinPublicAddress(fromBip39Entropy.getPublicAddress());
            this.mobileCoinClient = new MobileCoinClient(fromBip39Entropy, mobileCoinConfig.getFogUri(), mobileCoinConfig.getConsensusUri(), mobileCoinConfig.getConfig());
            try {
                reauthorizeClient();
            } catch (IOException e) {
                Log.w(TAG, "Failed to authorize client", e);
            }
        } catch (BadEntropyException | InvalidUriException e2) {
            throw new AssertionError(e2);
        }
    }

    public MobileCoinPublicAddress getMobileCoinPublicAddress() {
        return this.publicAddress;
    }

    public Balance getCachedBalance() {
        return SignalStore.paymentsValues().mobileCoinLatestBalance();
    }

    public MobileCoinLedgerWrapper getCachedLedger() {
        return SignalStore.paymentsValues().mobileCoinLatestFullLedger();
    }

    public MobileCoinLedgerWrapper getFullLedger() {
        return getFullLedger(true);
    }

    private MobileCoinLedgerWrapper getFullLedger(boolean z) {
        PaymentsValues paymentsValues = SignalStore.paymentsValues();
        try {
            MobileCoinLedgerWrapper tryGetFullLedger = tryGetFullLedger(null);
            Objects.requireNonNull(tryGetFullLedger);
            paymentsValues.setMobileCoinFullLedger(tryGetFullLedger);
        } catch (IOException e) {
            if (!z || !(e.getCause() instanceof NetworkException) || ((NetworkException) e.getCause()).statusCode != 401) {
                Log.w(TAG, "Failed to get up to date ledger", e);
            } else {
                Log.w(TAG, "Failed to get up to date ledger, due to temp auth failure, retrying", e);
                return getFullLedger(false);
            }
        }
        return getCachedLedger();
    }

    public MobileCoinLedgerWrapper tryGetFullLedger(Long l) throws IOException {
        try {
            MobileCoinLedger.Builder newBuilder = MobileCoinLedger.newBuilder();
            BigInteger bigInteger = BigInteger.ZERO;
            long j = 0;
            UnsignedLong unsignedLong = UnsignedLong.ZERO;
            long currentTimeMillis = System.currentTimeMillis();
            AccountSnapshot accountSnapshot = this.mobileCoinClient.getAccountSnapshot();
            BigInteger orFetchMinimumTxFee = this.mobileCoinClient.getOrFetchMinimumTxFee();
            if (l == null || accountSnapshot.getBlockIndex().longValue() >= l.longValue()) {
                for (OwnedTxOut ownedTxOut : accountSnapshot.getAccountActivity().getAllTxOuts()) {
                    MobileCoinLedger.OwnedTXO.Builder publicKey = MobileCoinLedger.OwnedTXO.newBuilder().setAmount(Uint64Util.bigIntegerToUInt64(ownedTxOut.getValue())).setReceivedInBlock(getBlock(ownedTxOut.getReceivedBlockIndex(), ownedTxOut.getReceivedBlockTimestamp())).setKeyImage(ByteString.copyFrom(ownedTxOut.getKeyImage().getData())).setPublicKey(ByteString.copyFrom(ownedTxOut.getPublicKey().getKeyBytes()));
                    if (ownedTxOut.getSpentBlockIndex() == null || (l != null && !ownedTxOut.isSpent(UnsignedLong.valueOf(l.longValue())))) {
                        bigInteger = bigInteger.add(ownedTxOut.getValue());
                        newBuilder.addUnspentTxos(publicKey);
                    } else {
                        publicKey.setSpentInBlock(getBlock(ownedTxOut.getSpentBlockIndex(), ownedTxOut.getSpentBlockTimestamp()));
                        newBuilder.addSpentTxos(publicKey);
                    }
                    if (ownedTxOut.getSpentBlockIndex() != null && ownedTxOut.getSpentBlockIndex().compareTo(unsignedLong) > 0) {
                        unsignedLong = ownedTxOut.getSpentBlockIndex();
                    }
                    if (ownedTxOut.getReceivedBlockIndex().compareTo(unsignedLong) > 0) {
                        unsignedLong = ownedTxOut.getReceivedBlockIndex();
                    }
                    if (ownedTxOut.getSpentBlockTimestamp() != null && ownedTxOut.getSpentBlockTimestamp().getTime() > j) {
                        j = ownedTxOut.getSpentBlockTimestamp().getTime();
                    }
                    if (ownedTxOut.getReceivedBlockTimestamp() != null && ownedTxOut.getReceivedBlockTimestamp().getTime() > j) {
                        j = ownedTxOut.getReceivedBlockTimestamp().getTime();
                    }
                }
                newBuilder.setBalance(Uint64Util.bigIntegerToUInt64(bigInteger)).setTransferableBalance(Uint64Util.bigIntegerToUInt64(accountSnapshot.getTransferableAmount(orFetchMinimumTxFee))).setAsOfTimeStamp(currentTimeMillis).setHighestBlock(MobileCoinLedger.Block.newBuilder().setBlockNumber(unsignedLong.longValue()).setTimestamp(j));
                return new MobileCoinLedgerWrapper(newBuilder.build());
            }
            Log.d(TAG, "Waiting for block index");
            return null;
        } catch (AttestationException e) {
            Log.w(TAG, "Attestation problem getting ledger", e);
            throw new IOException(e);
        } catch (InvalidFogResponse e2) {
            Log.w(TAG, "Problem getting ledger", e2);
            throw new IOException(e2);
        } catch (NetworkException e3) {
            String str = TAG;
            Log.w(str, "Network problem getting ledger", e3);
            if (e3.statusCode == 401) {
                Log.d(str, "Reauthorizing client");
                reauthorizeClient();
            }
            throw new IOException(e3);
        } catch (Uint64RangeException e4) {
            throw new AssertionError(e4);
        }
    }

    private static MobileCoinLedger.Block getBlock(UnsignedLong unsignedLong, Date date) throws Uint64RangeException {
        MobileCoinLedger.Block.Builder newBuilder = MobileCoinLedger.Block.newBuilder();
        newBuilder.setBlockNumber(Uint64Util.bigIntegerToUInt64(unsignedLong.toBigInteger()));
        if (date != null) {
            newBuilder.setTimestamp(date.getTime());
        }
        return newBuilder.build();
    }

    public Money.MobileCoin getFee(Money.MobileCoin mobileCoin) throws IOException {
        Throwable e;
        try {
            return Money.picoMobileCoin(this.mobileCoinClient.estimateTotalFee(mobileCoin.requireMobileCoin().toPicoMobBigInteger()));
        } catch (AttestationException e2) {
            e = e2;
            Log.w(TAG, "Failed to get fee", e);
            return Money.MobileCoin.ZERO;
        } catch (InsufficientFundsException e3) {
            e = e3;
            Log.w(TAG, "Failed to get fee", e);
            return Money.MobileCoin.ZERO;
        } catch (InvalidFogResponse e4) {
            e = e4;
            Log.w(TAG, "Failed to get fee", e);
            return Money.MobileCoin.ZERO;
        } catch (NetworkException e5) {
            Log.w(TAG, "Failed to get fee", e5);
            throw new IOException(e5);
        }
    }

    public PaymentSubmissionResult sendPayment(MobileCoinPublicAddress mobileCoinPublicAddress, Money.MobileCoin mobileCoin, Money.MobileCoin mobileCoin2) {
        LinkedList linkedList = new LinkedList();
        sendPayment(mobileCoinPublicAddress, mobileCoin, mobileCoin2, false, linkedList);
        return new PaymentSubmissionResult(linkedList);
    }

    public TransactionStatusResult getSentTransactionStatus(PaymentTransactionId paymentTransactionId) throws IOException {
        Throwable e;
        Throwable e2;
        try {
            Transaction.Status transactionStatus = this.mobileCoinClient.getAccountSnapshot().getTransactionStatus(Transaction.fromBytes(((PaymentTransactionId.MobileCoin) paymentTransactionId).getTransaction()));
            int i = AnonymousClass1.$SwitchMap$com$mobilecoin$lib$Transaction$Status[transactionStatus.ordinal()];
            if (i == 1) {
                Log.w(TAG, "Unknown sent Transaction Status");
                return TransactionStatusResult.inProgress();
            } else if (i == 2) {
                return TransactionStatusResult.failed();
            } else {
                if (i == 3) {
                    return TransactionStatusResult.complete(transactionStatus.getBlockIndex().longValue());
                }
                throw new IllegalStateException("Unknown Transaction Status: " + transactionStatus);
            }
        } catch (AttestationException e3) {
            e2 = e3;
            Log.w(TAG, e2);
            throw new IOException(e2);
        } catch (InvalidFogResponse e4) {
            e = e4;
            Log.w(TAG, e);
            return TransactionStatusResult.failed();
        } catch (NetworkException e5) {
            e2 = e5;
            Log.w(TAG, e2);
            throw new IOException(e2);
        } catch (SerializationException e6) {
            e = e6;
            Log.w(TAG, e);
            return TransactionStatusResult.failed();
        }
    }

    public ReceivedTransactionStatus getReceivedTransactionStatus(byte[] bArr) throws IOException {
        Throwable e;
        Throwable e2;
        try {
            Receipt fromBytes = Receipt.fromBytes(bArr);
            Receipt.Status receiptStatus = this.mobileCoinClient.getReceiptStatus(fromBytes);
            int i = AnonymousClass1.$SwitchMap$com$mobilecoin$lib$Receipt$Status[receiptStatus.ordinal()];
            if (i == 1) {
                Log.w(TAG, "Unknown received Transaction Status");
                return ReceivedTransactionStatus.inProgress();
            } else if (i == 2) {
                return ReceivedTransactionStatus.failed();
            } else {
                if (i == 3) {
                    return ReceivedTransactionStatus.complete(Money.picoMobileCoin(fromBytes.getAmount(this.account)), receiptStatus.getBlockIndex().longValue());
                }
                throw new IllegalStateException("Unknown Transaction Status: " + receiptStatus);
            }
        } catch (AmountDecoderException e3) {
            Log.w(TAG, "Failed to decode amount", e3);
            return ReceivedTransactionStatus.failed();
        } catch (AttestationException e4) {
            e2 = e4;
            throw new IOException(e2);
        } catch (InvalidFogResponse e5) {
            e = e5;
            Log.w(TAG, e);
            return ReceivedTransactionStatus.failed();
        } catch (InvalidReceiptException e6) {
            e = e6;
            Log.w(TAG, e);
            return ReceivedTransactionStatus.failed();
        } catch (NetworkException e7) {
            e2 = e7;
            throw new IOException(e2);
        } catch (SerializationException e8) {
            e = e8;
            Log.w(TAG, e);
            return ReceivedTransactionStatus.failed();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.payments.Wallet$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$mobilecoin$lib$Receipt$Status;
        static final /* synthetic */ int[] $SwitchMap$com$mobilecoin$lib$Transaction$Status;

        static {
            int[] iArr = new int[Receipt.Status.values().length];
            $SwitchMap$com$mobilecoin$lib$Receipt$Status = iArr;
            try {
                iArr[Receipt.Status.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$Receipt$Status[Receipt.Status.FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$Receipt$Status[Receipt.Status.RECEIVED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[Transaction.Status.values().length];
            $SwitchMap$com$mobilecoin$lib$Transaction$Status = iArr2;
            try {
                iArr2[Transaction.Status.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$Transaction$Status[Transaction.Status.FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$Transaction$Status[Transaction.Status.ACCEPTED.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    private void sendPayment(MobileCoinPublicAddress mobileCoinPublicAddress, Money.MobileCoin mobileCoin, Money.MobileCoin mobileCoin2, boolean z, List<TransactionSubmissionResult> list) {
        Throwable e;
        Throwable e2;
        Money.MobileCoin mobileCoin3 = Money.MobileCoin.ZERO;
        if (z) {
            try {
                mobileCoin3 = defragment(mobileCoin, list);
            } catch (AttestationException e3) {
                e2 = e3;
                Log.w(TAG, "Defragment failed", e2);
                list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, true));
                return;
            } catch (FogReportException e4) {
                e2 = e4;
                Log.w(TAG, "Defragment failed", e2);
                list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, true));
                return;
            } catch (InsufficientFundsException e5) {
                Log.w(TAG, "Insufficient funds", e5);
                list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.INSUFFICIENT_FUNDS, true));
                return;
            } catch (InvalidFogResponse e6) {
                e2 = e6;
                Log.w(TAG, "Defragment failed", e2);
                list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, true));
                return;
            } catch (InvalidTransactionException e7) {
                e2 = e7;
                Log.w(TAG, "Defragment failed", e2);
                list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, true));
                return;
            } catch (NetworkException e8) {
                e2 = e8;
                Log.w(TAG, "Defragment failed", e2);
                list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, true));
                return;
            } catch (TransactionBuilderException e9) {
                e2 = e9;
                Log.w(TAG, "Defragment failed", e2);
                list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, true));
                return;
            } catch (TimeoutException e10) {
                e2 = e10;
                Log.w(TAG, "Defragment failed", e2);
                list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, true));
                return;
            }
        }
        Money.MobileCoin requireMobileCoin = mobileCoin2.subtract(mobileCoin3).requireMobileCoin();
        BigInteger picoMobBigInteger = mobileCoin.requireMobileCoin().toPicoMobBigInteger();
        PendingTransaction pendingTransaction = null;
        String str = TAG;
        Log.i(str, String.format("Total fee advised: %s\nDefrag fees: %s\nTransaction fee: %s", mobileCoin2, mobileCoin3, requireMobileCoin));
        if (!requireMobileCoin.isPositive()) {
            Log.i(str, "No fee left after defrag");
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, false));
            return;
        }
        try {
            pendingTransaction = this.mobileCoinClient.prepareTransaction(mobileCoinPublicAddress.getAddress(), picoMobBigInteger, requireMobileCoin.toPicoMobBigInteger());
        } catch (AttestationException e11) {
            Log.w(TAG, "Attestation problem", e11);
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, false));
        } catch (FogReportException e12) {
            e = e12;
            Log.w(TAG, "Invalid fog response", e);
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, false));
        } catch (FragmentedAccountException e13) {
            if (z) {
                Log.w(TAG, "Account is fragmented, but already tried to defragment", e13);
                list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, false));
            } else {
                Log.i(TAG, "Account is fragmented, defragmenting and retrying");
                sendPayment(mobileCoinPublicAddress, mobileCoin, mobileCoin2, true, list);
            }
        } catch (InsufficientFundsException e14) {
            Log.w(TAG, "Insufficient funds", e14);
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.INSUFFICIENT_FUNDS, false));
        } catch (InvalidFogResponse e15) {
            e = e15;
            Log.w(TAG, "Invalid fog response", e);
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, false));
        } catch (NetworkException e16) {
            Log.w(TAG, "Network problem", e16);
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, false));
        } catch (TransactionBuilderException e17) {
            Log.w(TAG, "Builder problem", e17);
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, false));
        }
        if (pendingTransaction == null) {
            Log.w(TAG, "Failed to create pending transaction");
            return;
        }
        try {
            String str2 = TAG;
            Log.i(str2, "Submitting transaction");
            this.mobileCoinClient.submitTransaction(pendingTransaction.getTransaction());
            Log.i(str2, "Transaction submitted");
            list.add(TransactionSubmissionResult.successfullySubmitted(new PaymentTransactionId.MobileCoin(pendingTransaction.getTransaction().toByteArray(), pendingTransaction.getReceipt().toByteArray(), requireMobileCoin)));
        } catch (AttestationException e18) {
            Log.w(TAG, "Attestation problem", e18);
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, false));
        } catch (InvalidTransactionException e19) {
            Log.w(TAG, "Invalid transaction", e19);
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, false));
        } catch (NetworkException e20) {
            Log.w(TAG, "Network problem", e20);
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.NETWORK_FAILURE, false));
        } catch (SerializationException e21) {
            Log.w(TAG, "Serialization problem", e21);
            list.add(TransactionSubmissionResult.failure(TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE, false));
        }
    }

    private Money.MobileCoin defragment(Money.MobileCoin mobileCoin, List<TransactionSubmissionResult> list) throws TransactionBuilderException, NetworkException, InvalidTransactionException, AttestationException, FogReportException, InvalidFogResponse, TimeoutException, InsufficientFundsException {
        String str = TAG;
        Log.i(str, "Defragmenting account");
        DefragDelegate defragDelegate = new DefragDelegate(this.mobileCoinClient, list);
        this.mobileCoinClient.defragmentAccount(mobileCoin.toPicoMobBigInteger(), defragDelegate);
        Log.i(str, "Account defragmented at a cost of " + defragDelegate.totalFeesSpent);
        return defragDelegate.totalFeesSpent;
    }

    private void reauthorizeClient() throws IOException {
        AuthCredentials auth = this.mobileCoinConfig.getAuth();
        this.mobileCoinClient.setFogBasicAuthorization(auth.username(), auth.password());
    }

    public void refresh() {
        getFullLedger();
    }

    /* loaded from: classes4.dex */
    public static final class TransactionStatusResult {
        private final long blockIndex;
        private final TransactionStatus transactionStatus;

        public TransactionStatusResult(TransactionStatus transactionStatus, long j) {
            this.transactionStatus = transactionStatus;
            this.blockIndex = j;
        }

        static TransactionStatusResult inProgress() {
            return new TransactionStatusResult(TransactionStatus.IN_PROGRESS, 0);
        }

        static TransactionStatusResult failed() {
            return new TransactionStatusResult(TransactionStatus.FAILED, 0);
        }

        static TransactionStatusResult complete(long j) {
            return new TransactionStatusResult(TransactionStatus.COMPLETE, j);
        }

        public TransactionStatus getTransactionStatus() {
            return this.transactionStatus;
        }

        public long getBlockIndex() {
            return this.blockIndex;
        }
    }

    /* loaded from: classes4.dex */
    public static final class ReceivedTransactionStatus {
        private final Money amount;
        private final long blockIndex;
        private final TransactionStatus status;

        public static ReceivedTransactionStatus failed() {
            return new ReceivedTransactionStatus(TransactionStatus.FAILED, null, 0);
        }

        public static ReceivedTransactionStatus inProgress() {
            return new ReceivedTransactionStatus(TransactionStatus.IN_PROGRESS, null, 0);
        }

        public static ReceivedTransactionStatus complete(Money money, long j) {
            return new ReceivedTransactionStatus(TransactionStatus.COMPLETE, money, j);
        }

        private ReceivedTransactionStatus(TransactionStatus transactionStatus, Money money, long j) {
            this.status = transactionStatus;
            this.amount = money;
            this.blockIndex = j;
        }

        public TransactionStatus getStatus() {
            return this.status;
        }

        public Money getAmount() {
            Money money;
            if (this.status == TransactionStatus.COMPLETE && (money = this.amount) != null) {
                return money;
            }
            throw new IllegalStateException();
        }

        public long getBlockIndex() {
            return this.blockIndex;
        }
    }

    /* loaded from: classes4.dex */
    public static class DefragDelegate implements DefragmentationDelegate {
        private final MobileCoinClient mobileCoinClient;
        private final List<TransactionSubmissionResult> results;
        private Money.MobileCoin totalFeesSpent = Money.MobileCoin.ZERO;

        DefragDelegate(MobileCoinClient mobileCoinClient, List<TransactionSubmissionResult> list) {
            this.mobileCoinClient = mobileCoinClient;
            this.results = list;
        }

        @Override // com.mobilecoin.lib.DefragmentationDelegate
        public void onStart() {
            Log.i(Wallet.TAG, "Defragmenting start");
        }

        @Override // com.mobilecoin.lib.DefragmentationDelegate
        public boolean onStepReady(PendingTransaction pendingTransaction, BigInteger bigInteger) throws NetworkException, InvalidTransactionException, AttestationException {
            Log.i(Wallet.TAG, "Submitting defrag transaction");
            this.mobileCoinClient.submitTransaction(pendingTransaction.getTransaction());
            Log.i(Wallet.TAG, "Defrag transaction submitted");
            try {
                Money.MobileCoin picoMobileCoin = Money.picoMobileCoin(bigInteger);
                this.results.add(TransactionSubmissionResult.successfullySubmittedDefrag(new PaymentTransactionId.MobileCoin(pendingTransaction.getTransaction().toByteArray(), pendingTransaction.getReceipt().toByteArray(), picoMobileCoin)));
                this.totalFeesSpent = this.totalFeesSpent.add(picoMobileCoin).requireMobileCoin();
                return true;
            } catch (SerializationException e) {
                throw new AssertionError(e);
            }
        }

        @Override // com.mobilecoin.lib.DefragmentationDelegate
        public void onComplete() {
            Log.i(Wallet.TAG, "Defragmenting complete");
        }

        @Override // com.mobilecoin.lib.DefragmentationDelegate
        public void onCancel() {
            Log.w(Wallet.TAG, "Defragmenting cancel");
        }
    }
}
