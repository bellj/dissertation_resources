package org.thoughtcrime.securesms.payments.preferences.model;

import android.os.Parcel;
import android.os.Parcelable;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddress;
import org.thoughtcrime.securesms.payments.Payee;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class PayeeParcelable implements Parcelable {
    private static final int CONTAINS_ADDRESS;
    private static final int CONTAINS_RECIPIENT_ID;
    private static final int CONTAINS_RECIPIENT_ID_AND_ADDRESS;
    public static final Parcelable.Creator<PayeeParcelable> CREATOR = new Parcelable.Creator<PayeeParcelable>() { // from class: org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable.1
        @Override // android.os.Parcelable.Creator
        public PayeeParcelable createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            if (readInt == 0) {
                return new PayeeParcelable(Payee.UNKNOWN);
            }
            if (readInt == 1) {
                return new PayeeParcelable(new Payee((RecipientId) parcel.readParcelable(RecipientId.class.getClassLoader())));
            }
            if (readInt == 2) {
                return new PayeeParcelable(new Payee(MobileCoinPublicAddress.fromBase58OrThrow(parcel.readString())));
            }
            if (readInt == 3) {
                return new PayeeParcelable(Payee.fromRecipientAndAddress((RecipientId) parcel.readParcelable(RecipientId.class.getClassLoader()), MobileCoinPublicAddress.fromBase58OrThrow(parcel.readString())));
            }
            throw new AssertionError();
        }

        @Override // android.os.Parcelable.Creator
        public PayeeParcelable[] newArray(int i) {
            return new PayeeParcelable[i];
        }
    };
    private static final int UNKNOWN;
    private final Payee payee;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public PayeeParcelable(Payee payee) {
        this.payee = payee;
    }

    public PayeeParcelable(RecipientId recipientId) {
        this(new Payee(recipientId));
    }

    public PayeeParcelable(RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress) {
        this(Payee.fromRecipientAndAddress(recipientId, mobileCoinPublicAddress));
    }

    public PayeeParcelable(MobileCoinPublicAddress mobileCoinPublicAddress) {
        this(new Payee(mobileCoinPublicAddress));
    }

    public Payee getPayee() {
        return this.payee;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PayeeParcelable)) {
            return false;
        }
        return this.payee.equals(((PayeeParcelable) obj).payee);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.payee.hashCode();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        if (this.payee.hasRecipientId()) {
            if (this.payee.hasPublicAddress()) {
                parcel.writeInt(3);
                parcel.writeParcelable(this.payee.requireRecipientId(), i);
                parcel.writeString(this.payee.requirePublicAddress().getPaymentAddressBase58());
                return;
            }
            parcel.writeInt(1);
            parcel.writeParcelable(this.payee.requireRecipientId(), i);
        } else if (this.payee.hasPublicAddress()) {
            parcel.writeInt(2);
            parcel.writeString(this.payee.requirePublicAddress().getPaymentAddressBase58());
        } else {
            parcel.writeInt(0);
        }
    }
}
