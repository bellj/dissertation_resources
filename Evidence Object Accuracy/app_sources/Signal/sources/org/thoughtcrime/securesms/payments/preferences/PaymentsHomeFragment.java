package org.thoughtcrime.securesms.payments.preferences;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.airbnb.lottie.LottieAnimationView;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.PaymentPreferencesDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinActivity;
import org.thoughtcrime.securesms.payments.FiatMoneyUtil;
import org.thoughtcrime.securesms.payments.MoneyView;
import org.thoughtcrime.securesms.payments.deactivate.DeactivateWalletFragment$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel;
import org.thoughtcrime.securesms.payments.preferences.model.PaymentItem;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class PaymentsHomeFragment extends LoggingFragment {
    private static final String TAG = Log.tag(PaymentsHomeFragment.class);
    private final OnBackPressed onBackPressed = new OnBackPressed();
    private PaymentsHomeViewModel viewModel;

    public PaymentsHomeFragment() {
        super(R.layout.payments_home_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.payments_home_fragment_toolbar);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.payments_home_fragment_recycler);
        View findViewById = view.findViewById(R.id.payments_home_fragment_header);
        MoneyView moneyView = (MoneyView) view.findViewById(R.id.payments_home_fragment_header_balance);
        TextView textView = (TextView) view.findViewById(R.id.payments_home_fragment_header_exchange);
        View findViewById2 = view.findViewById(R.id.button_start_frame);
        View findViewById3 = view.findViewById(R.id.button_end_frame);
        View findViewById4 = view.findViewById(R.id.payments_home_fragment_header_refresh);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsHomeFragment.m2403$r8$lambda$3tGgj7xRoKrb57X9r1HRFK8GPI(PaymentsHomeFragment.this, view2);
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda4
            @Override // androidx.appcompat.widget.Toolbar.OnMenuItemClickListener
            public final boolean onMenuItemClick(MenuItem menuItem) {
                return PaymentsHomeFragment.m2405$r8$lambda$VdgenSHwAz8N32T7V_qypyVmVo(PaymentsHomeFragment.this, menuItem);
            }
        });
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsHomeFragment.m2406$r8$lambda$d5ttGUN2kjJ3aH04k0zOa8JUSs(PaymentsHomeFragment.this, view2);
            }
        });
        findViewById3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsHomeFragment.$r8$lambda$75yQkLaTpaLjupyBVLCWavZg5R8(PaymentsHomeFragment.this, view2);
            }
        });
        PaymentsHomeAdapter paymentsHomeAdapter = new PaymentsHomeAdapter(new HomeCallbacks());
        recyclerView.setAdapter(paymentsHomeAdapter);
        PaymentsHomeViewModel paymentsHomeViewModel = (PaymentsHomeViewModel) ViewModelProviders.of(this, new PaymentsHomeViewModel.Factory()).get(PaymentsHomeViewModel.class);
        this.viewModel = paymentsHomeViewModel;
        paymentsHomeViewModel.getList().observe(getViewLifecycleOwner(), new Observer(recyclerView) { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda7
            public final /* synthetic */ RecyclerView f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsHomeFragment.$r8$lambda$onuxungbPVQCODCJr9tqNiUermI(PaymentsHomeAdapter.this, this.f$1, (MappingModelList) obj);
            }
        });
        this.viewModel.getPaymentsEnabled().observe(getViewLifecycleOwner(), new Observer(findViewById) { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda8
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsHomeFragment.m2404$r8$lambda$OLdoSJmIWsPHV3q8Topqd7zf4w(Toolbar.this, this.f$1, (Boolean) obj);
            }
        });
        LiveData<Money> balance = this.viewModel.getBalance();
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Objects.requireNonNull(moneyView);
        balance.observe(viewLifecycleOwner, new DeactivateWalletFragment$$ExternalSyntheticLambda2(moneyView));
        this.viewModel.getExchange().observe(getViewLifecycleOwner(), new Observer(textView) { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda9
            public final /* synthetic */ TextView f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsHomeFragment.$r8$lambda$KUTdDz5OvocyG_zkR7iTcYSSYhg(PaymentsHomeFragment.this, this.f$1, (FiatMoney) obj);
            }
        });
        findViewById4.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsHomeFragment.$r8$lambda$tvpcLkjMZ7nJhk07Twltq6KeyWk(PaymentsHomeFragment.this, view2);
            }
        });
        textView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsHomeFragment.$r8$lambda$ciYxtqffTdWRGbudhWkd4WVmlNM(PaymentsHomeFragment.this, view2);
            }
        });
        this.viewModel.getExchangeLoadState().observe(getViewLifecycleOwner(), new Observer(findViewById4, (LottieAnimationView) view.findViewById(R.id.payments_home_fragment_header_refresh_animation), textView, view) { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ View f$0;
            public final /* synthetic */ LottieAnimationView f$1;
            public final /* synthetic */ TextView f$2;
            public final /* synthetic */ View f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsHomeFragment.$r8$lambda$EjovG2u6Zzk_r7bbHx0B9UX88e4(this.f$0, this.f$1, this.f$2, this.f$3, (LoadState) obj);
            }
        });
        this.viewModel.getPaymentStateEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsHomeFragment.$r8$lambda$HTPA5Tmoc10mhuO0WaLvlmhnUR8(PaymentsHomeFragment.this, (PaymentStateEvent) obj);
            }
        });
        this.viewModel.getErrorEnablingPayments().observe(getViewLifecycleOwner(), new Observer(view) { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsHomeFragment.m2407$r8$lambda$jqmlaO31kJld4javoA4ofj5b5w(this.f$0, (PaymentsHomeViewModel.ErrorEnabling) obj);
            }
        });
        requireActivity().getOnBackPressedDispatcher().addCallback(this.onBackPressed);
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        this.viewModel.markAllPaymentsSeen();
        requireActivity().finish();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        if (SignalStore.paymentsValues().getPaymentsAvailability().isSendAllowed()) {
            SafeNavigation.safeNavigate(Navigation.findNavController(view), PaymentsHomeFragmentDirections.actionPaymentsHomeToPaymentsAddMoney());
        } else {
            showPaymentsDisabledDialog();
        }
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        if (SignalStore.paymentsValues().getPaymentsAvailability().isSendAllowed()) {
            SafeNavigation.safeNavigate(Navigation.findNavController(view), PaymentsHomeFragmentDirections.actionPaymentsHomeToPaymentRecipientSelectionFragment());
        } else {
            showPaymentsDisabledDialog();
        }
    }

    public static /* synthetic */ boolean lambda$onViewCreated$3(MappingModel mappingModel) {
        return mappingModel instanceof PaymentItem;
    }

    public static /* synthetic */ void lambda$onViewCreated$5(PaymentsHomeAdapter paymentsHomeAdapter, RecyclerView recyclerView, MappingModelList mappingModelList) {
        if (!Stream.of(paymentsHomeAdapter.getCurrentList()).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda12
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PaymentsHomeFragment.$r8$lambda$kBAZD9PRRya_cGIAF2t97A6oY8g((MappingModel) obj);
            }
        })) {
            paymentsHomeAdapter.submitList(mappingModelList, new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda13
                @Override // java.lang.Runnable
                public final void run() {
                    PaymentsHomeFragment.$r8$lambda$ystPIJsbPpseaYSArfPBPVaEqLE(RecyclerView.this);
                }
            });
        } else {
            paymentsHomeAdapter.submitList(mappingModelList);
        }
    }

    public static /* synthetic */ void lambda$onViewCreated$6(Toolbar toolbar, View view, Boolean bool) {
        if (bool.booleanValue()) {
            toolbar.inflateMenu(R.menu.payments_home_fragment_menu);
        } else {
            toolbar.getMenu().clear();
        }
        view.setVisibility(bool.booleanValue() ? 0 : 8);
    }

    public /* synthetic */ void lambda$onViewCreated$7(TextView textView, FiatMoney fiatMoney) {
        if (fiatMoney != null) {
            textView.setText(FiatMoneyUtil.format(getResources(), fiatMoney));
        } else {
            textView.setText(R.string.PaymentsHomeFragment__unknown_amount);
        }
    }

    public /* synthetic */ void lambda$onViewCreated$8(View view) {
        this.viewModel.refreshExchangeRates(true);
    }

    public /* synthetic */ void lambda$onViewCreated$9(View view) {
        this.viewModel.refreshExchangeRates(true);
    }

    /* renamed from: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$preferences$LoadState;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentStateEvent;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentsHomeViewModel$ErrorEnabling;

        static {
            int[] iArr = new int[LoadState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$preferences$LoadState = iArr;
            try {
                iArr[LoadState.INITIAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$LoadState[LoadState.LOADED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$LoadState[LoadState.LOADING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$LoadState[LoadState.ERROR.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[PaymentStateEvent.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentStateEvent = iArr2;
            try {
                iArr2[PaymentStateEvent.NO_BALANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentStateEvent[PaymentStateEvent.DEACTIVATED.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentStateEvent[PaymentStateEvent.DEACTIVATE_WITHOUT_BALANCE.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentStateEvent[PaymentStateEvent.DEACTIVATE_WITH_BALANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentStateEvent[PaymentStateEvent.ACTIVATED.ordinal()] = 5;
            } catch (NoSuchFieldError unused9) {
            }
            int[] iArr3 = new int[PaymentsHomeViewModel.ErrorEnabling.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentsHomeViewModel$ErrorEnabling = iArr3;
            try {
                iArr3[PaymentsHomeViewModel.ErrorEnabling.REGION.ordinal()] = 1;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentsHomeViewModel$ErrorEnabling[PaymentsHomeViewModel.ErrorEnabling.NETWORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused11) {
            }
        }
    }

    public static /* synthetic */ void lambda$onViewCreated$10(View view, LottieAnimationView lottieAnimationView, TextView textView, View view2, LoadState loadState) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$preferences$LoadState[loadState.ordinal()];
        if (i == 1 || i == 2) {
            view.setVisibility(0);
            lottieAnimationView.cancelAnimation();
            lottieAnimationView.setVisibility(8);
        } else if (i == 3) {
            view.setVisibility(4);
            lottieAnimationView.playAnimation();
            lottieAnimationView.setVisibility(0);
        } else if (i == 4) {
            view.setVisibility(0);
            lottieAnimationView.cancelAnimation();
            lottieAnimationView.setVisibility(8);
            textView.setText(R.string.PaymentsHomeFragment__currency_conversion_not_available);
            Toast.makeText(view2.getContext(), (int) R.string.PaymentsHomeFragment__cant_display_currency_conversion, 0).show();
        }
    }

    public /* synthetic */ void lambda$onViewCreated$14(PaymentStateEvent paymentStateEvent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setTitle(R.string.PaymentsHomeFragment__deactivate_payments_question);
        builder.setMessage(R.string.PaymentsHomeFragment__you_will_not_be_able_to_send);
        builder.setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda14
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PaymentsHomeFragment.m2408$r8$lambda$wPbxAKnbo_ZXNylbIwjlcrWgBk(dialogInterface, i);
            }
        });
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentStateEvent[paymentStateEvent.ordinal()];
        if (i == 1) {
            Toast.makeText(requireContext(), (int) R.string.PaymentsHomeFragment__balance_is_not_currently_available, 0).show();
        } else if (i != 2) {
            if (i == 3) {
                builder.setPositiveButton(SpanUtil.color(ContextCompat.getColor(requireContext(), R.color.signal_alert_primary), getString(R.string.PaymentsHomeFragment__deactivate)), new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda15
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i2) {
                        PaymentsHomeFragment.$r8$lambda$hs1ngARwl6ULzjXqBSXQ12CVANQ(PaymentsHomeFragment.this, dialogInterface, i2);
                    }
                });
            } else if (i == 4) {
                builder.setPositiveButton(getString(R.string.PaymentsHomeFragment__continue), new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment$$ExternalSyntheticLambda16
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i2) {
                        PaymentsHomeFragment.$r8$lambda$YTwejEGMiTizfbH9MekVBtxR0Jo(PaymentsHomeFragment.this, dialogInterface, i2);
                    }
                });
            } else if (i != 5) {
                throw new IllegalStateException("Unsupported event type: " + paymentStateEvent.name());
            } else {
                return;
            }
            builder.show();
        } else {
            Snackbar.make(requireView(), (int) R.string.PaymentsHomeFragment__payments_deactivated, -1).show();
        }
    }

    public /* synthetic */ void lambda$onViewCreated$12(DialogInterface dialogInterface, int i) {
        this.viewModel.confirmDeactivatePayments();
        dialogInterface.dismiss();
    }

    public /* synthetic */ void lambda$onViewCreated$13(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.deactivateWallet);
    }

    public static /* synthetic */ void lambda$onViewCreated$15(View view, PaymentsHomeViewModel.ErrorEnabling errorEnabling) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentsHomeViewModel$ErrorEnabling[errorEnabling.ordinal()];
        if (i == 1) {
            Toast.makeText(view.getContext(), (int) R.string.PaymentsHomeFragment__payments_is_not_available_in_your_region, 1).show();
        } else if (i == 2) {
            Toast.makeText(view.getContext(), (int) R.string.PaymentsHomeFragment__could_not_enable_payments, 0).show();
        } else {
            throw new AssertionError();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.viewModel.checkPaymentActivationState();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        this.onBackPressed.setEnabled(false);
    }

    public boolean onMenuItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.payments_home_fragment_menu_transfer_to_exchange) {
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_paymentsHome_to_paymentsTransfer);
            return true;
        } else if (menuItem.getItemId() == R.id.payments_home_fragment_menu_set_currency) {
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_paymentsHome_to_setCurrency);
            return true;
        } else if (menuItem.getItemId() == R.id.payments_home_fragment_menu_deactivate_wallet) {
            this.viewModel.deactivatePayments();
            return true;
        } else if (menuItem.getItemId() == R.id.payments_home_fragment_menu_view_recovery_phrase) {
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_paymentsHome_to_paymentsBackup);
            return true;
        } else if (menuItem.getItemId() != R.id.payments_home_fragment_menu_help) {
            return false;
        } else {
            startActivity(AppSettingsActivity.help(requireContext(), 6));
            return true;
        }
    }

    private void showPaymentsDisabledDialog() {
        new AlertDialog.Builder(requireActivity()).setMessage(R.string.PaymentsHomeFragment__payments_not_available).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
    }

    /* loaded from: classes4.dex */
    public class HomeCallbacks implements PaymentsHomeAdapter.Callbacks {
        HomeCallbacks() {
            PaymentsHomeFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public void onActivatePayments() {
            new MaterialAlertDialogBuilder(PaymentsHomeFragment.this.requireContext()).setMessage(R.string.PaymentsHomeFragment__you_can_use_signal_to_send).setPositiveButton(R.string.PaymentsHomeFragment__activate, (DialogInterface.OnClickListener) new PaymentsHomeFragment$HomeCallbacks$$ExternalSyntheticLambda0(this)).setNegativeButton(R.string.PaymentsHomeFragment__view_mobile_coin_terms, (DialogInterface.OnClickListener) new PaymentsHomeFragment$HomeCallbacks$$ExternalSyntheticLambda1(this)).setNeutralButton(17039360, (DialogInterface.OnClickListener) new PaymentsHomeFragment$HomeCallbacks$$ExternalSyntheticLambda2()).show();
        }

        public /* synthetic */ void lambda$onActivatePayments$0(DialogInterface dialogInterface, int i) {
            PaymentsHomeFragment.this.viewModel.activatePayments();
            dialogInterface.dismiss();
        }

        public /* synthetic */ void lambda$onActivatePayments$1(DialogInterface dialogInterface, int i) {
            CommunicationActions.openBrowserLink(PaymentsHomeFragment.this.requireContext(), PaymentsHomeFragment.this.getString(R.string.PaymentsHomeFragment__mobile_coin_terms_url));
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public void onRestorePaymentsAccount() {
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(PaymentsHomeFragment.this), PaymentsHomeFragmentDirections.actionPaymentsHomeToPaymentsBackup().setIsRestore(true));
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public void onSeeAll(PaymentType paymentType) {
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(PaymentsHomeFragment.this), PaymentsHomeFragmentDirections.actionPaymentsHomeToPaymentsAllActivity(paymentType));
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public void onPaymentItem(PaymentItem paymentItem) {
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(PaymentsHomeFragment.this), PaymentPreferencesDirections.actionDirectlyToPaymentDetails(paymentItem.getPaymentDetailsParcelable()));
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public void onInfoCardDismissed() {
            PaymentsHomeFragment.this.viewModel.onInfoCardDismissed();
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public void onUpdatePin() {
            PaymentsHomeFragment paymentsHomeFragment = PaymentsHomeFragment.this;
            paymentsHomeFragment.startActivityForResult(CreateKbsPinActivity.getIntentForPinChangeFromSettings(paymentsHomeFragment.requireContext()), 27698);
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public void onViewRecoveryPhrase() {
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(PaymentsHomeFragment.this), (int) R.id.action_paymentsHome_to_paymentsBackup);
        }
    }

    /* loaded from: classes4.dex */
    private class OnBackPressed extends OnBackPressedCallback {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OnBackPressed() {
            super(true);
            PaymentsHomeFragment.this = r1;
        }

        @Override // androidx.activity.OnBackPressedCallback
        public void handleOnBackPressed() {
            PaymentsHomeFragment.this.viewModel.markAllPaymentsSeen();
            PaymentsHomeFragment.this.requireActivity().finish();
        }
    }
}
