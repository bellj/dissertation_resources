package org.thoughtcrime.securesms.payments.confirm;

import android.view.View;
import org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConfirmPaymentAdapter$ConfirmPaymentViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ConfirmPaymentAdapter.ConfirmPaymentViewHolder f$0;

    public /* synthetic */ ConfirmPaymentAdapter$ConfirmPaymentViewHolder$$ExternalSyntheticLambda0(ConfirmPaymentAdapter.ConfirmPaymentViewHolder confirmPaymentViewHolder) {
        this.f$0 = confirmPaymentViewHolder;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$0(view);
    }
}
