package org.thoughtcrime.securesms.payments.preferences;

import android.util.Pair;
import androidx.lifecycle.LiveData;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import j$.util.function.Function;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.Direction;
import org.thoughtcrime.securesms.payments.MobileCoinLedgerWrapper;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public class PaymentsRepository {
    private static final String TAG = Log.tag(PaymentsRepository.class);
    private final PaymentDatabase paymentDatabase;
    private final LiveData<List<Payment>> recentPayments;
    private final LiveData<List<Payment>> recentReceivedPayments;
    private final LiveData<List<Payment>> recentSentPayments;

    public PaymentsRepository() {
        PaymentDatabase payments = SignalDatabase.payments();
        this.paymentDatabase = payments;
        LiveData<List<Payment>> mapAsync = LiveDataUtil.mapAsync(LiveDataUtil.combineLatest(payments.getAllLive(), SignalStore.paymentsValues().liveMobileCoinLedger(), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsRepository$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return Pair.create((List) obj, (MobileCoinLedgerWrapper) obj2);
            }
        }), new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsRepository$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PaymentsRepository.this.lambda$new$0((Pair) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        this.recentPayments = mapAsync;
        this.recentSentPayments = LiveDataUtil.mapAsync(mapAsync, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsRepository$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PaymentsRepository.this.lambda$new$1((List) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        this.recentReceivedPayments = LiveDataUtil.mapAsync(mapAsync, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsRepository$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PaymentsRepository.this.lambda$new$2((List) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public /* synthetic */ List lambda$new$0(Pair pair) {
        return reconcile((Collection) pair.first, (MobileCoinLedgerWrapper) pair.second);
    }

    public /* synthetic */ List lambda$new$1(List list) {
        return filterPayments(list, Direction.SENT);
    }

    public /* synthetic */ List lambda$new$2(List list) {
        return filterPayments(list, Direction.RECEIVED);
    }

    private List<Payment> reconcile(Collection<PaymentDatabase.PaymentTransaction> collection, MobileCoinLedgerWrapper mobileCoinLedgerWrapper) {
        List<Payment> reconcile = LedgerReconcile.reconcile(collection, mobileCoinLedgerWrapper);
        updateDatabaseWithNewBlockInformation(reconcile);
        return reconcile;
    }

    private void updateDatabaseWithNewBlockInformation(List<Payment> list) {
        List<LedgerReconcile.BlockOverridePayment> list2 = Stream.of(list).select(LedgerReconcile.BlockOverridePayment.class).toList();
        if (!list2.isEmpty()) {
            Log.i(TAG, String.format(Locale.US, "%d payments have new block index or timestamp information", Integer.valueOf(list2.size())));
            for (LedgerReconcile.BlockOverridePayment blockOverridePayment : list2) {
                Payment inner = blockOverridePayment.getInner();
                boolean z = inner.getBlockIndex() != blockOverridePayment.getBlockIndex();
                if (inner.getBlockTimestamp() != blockOverridePayment.getBlockTimestamp()) {
                    z = true;
                }
                if (!z) {
                    Log.w(TAG, "  Unnecessary");
                } else if (this.paymentDatabase.updateBlockDetails(inner.getUuid(), blockOverridePayment.getBlockIndex(), blockOverridePayment.getBlockTimestamp())) {
                    String str = TAG;
                    Log.d(str, "  Updated block details for " + inner.getUuid());
                } else {
                    String str2 = TAG;
                    Log.w(str2, "  Failed to update block details for " + inner.getUuid());
                }
            }
        }
    }

    public LiveData<List<Payment>> getRecentPayments() {
        return this.recentPayments;
    }

    public LiveData<List<Payment>> getRecentSentPayments() {
        return this.recentSentPayments;
    }

    public LiveData<List<Payment>> getRecentReceivedPayments() {
        return this.recentReceivedPayments;
    }

    private List<Payment> filterPayments(List<Payment> list, Direction direction) {
        return Stream.of(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsRepository$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PaymentsRepository.lambda$filterPayments$3(Direction.this, (Payment) obj);
            }
        }).toList();
    }

    public static /* synthetic */ boolean lambda$filterPayments$3(Direction direction, Payment payment) {
        return payment.getDirection() == direction;
    }
}
