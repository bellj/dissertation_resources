package org.thoughtcrime.securesms.payments.preferences.details;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.Group;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import java.util.Locale;
import java.util.Objects;
import org.signal.core.util.StringUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.payments.Direction;
import org.thoughtcrime.securesms.payments.MoneyView;
import org.thoughtcrime.securesms.payments.Payee;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.State;
import org.thoughtcrime.securesms.payments.preferences.details.PaymentsDetailsViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class PaymentDetailsFragment extends LoggingFragment {
    private static final String TAG = Log.tag(PaymentDetailsFragment.class);

    public PaymentDetailsFragment() {
        super(R.layout.payment_details_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ((Toolbar) view.findViewById(R.id.payments_details_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentDetailsFragment.$r8$lambda$kfCp1zFIsRPSlWOuG_VQ6gRMrC8(view2);
            }
        });
        PaymentDetailsParcelable paymentDetails = PaymentDetailsFragmentArgs.fromBundle(requireArguments()).getPaymentDetails();
        AvatarImageView avatarImageView = (AvatarImageView) view.findViewById(R.id.payments_details_avatar);
        BadgeImageView badgeImageView = (BadgeImageView) view.findViewById(R.id.payments_details_badge);
        TextView textView = (TextView) view.findViewById(R.id.payments_details_contact_to_from);
        MoneyView moneyView = (MoneyView) view.findViewById(R.id.payments_details_amount);
        TextView textView2 = (TextView) view.findViewById(R.id.payments_details_note);
        TextView textView3 = (TextView) view.findViewById(R.id.payments_details_status);
        View findViewById = view.findViewById(R.id.payments_details_sent_by_header);
        TextView textView4 = (TextView) view.findViewById(R.id.payments_details_sent_by);
        LearnMoreTextView learnMoreTextView = (LearnMoreTextView) view.findViewById(R.id.payments_details_info);
        TextView textView5 = (TextView) view.findViewById(R.id.payments_details_sent_to_header);
        MoneyView moneyView2 = (MoneyView) view.findViewById(R.id.payments_details_sent_to_amount);
        View findViewById2 = view.findViewById(R.id.payments_details_sent_fee_header);
        MoneyView moneyView3 = (MoneyView) view.findViewById(R.id.payments_details_sent_fee_amount);
        Group group = (Group) view.findViewById(R.id.payments_details_sent_views);
        View findViewById3 = view.findViewById(R.id.payments_details_block_header);
        TextView textView6 = (TextView) view.findViewById(R.id.payments_details_block);
        if (paymentDetails.hasPayment()) {
            Payment requirePayment = paymentDetails.requirePayment();
            avatarImageView.disableQuickContact();
            avatarImageView.setImageResource(R.drawable.ic_mobilecoin_avatar_24);
            textView.setText(getContactFromToTextFromDirection(requirePayment.getDirection()));
            moneyView.setMoney(requirePayment.getAmountPlusFeeWithDirection());
            textView2.setVisibility(8);
            textView3.setText(getStatusFromPayment(requirePayment));
            findViewById.setVisibility(8);
            textView4.setVisibility(8);
            learnMoreTextView.setLearnMoreVisible(true);
            learnMoreTextView.setText(R.string.PaymentsDetailsFragment__information);
            learnMoreTextView.setLink(getString(R.string.PaymentsDetailsFragment__learn_more__information));
            textView5.setVisibility(8);
            moneyView2.setVisibility(8);
            findViewById3.setVisibility(0);
            textView6.setVisibility(0);
            textView6.setText(String.valueOf(requirePayment.getBlockIndex()));
            if (requirePayment.getDirection() == Direction.SENT) {
                moneyView3.setMoney(requirePayment.getFee());
                findViewById2.setVisibility(0);
                moneyView3.setVisibility(0);
            }
            return;
        }
        PaymentsDetailsViewModel paymentsDetailsViewModel = (PaymentsDetailsViewModel) ViewModelProviders.of(this, new PaymentsDetailsViewModel.Factory(paymentDetails.requireUuid())).get(PaymentsDetailsViewModel.class);
        paymentsDetailsViewModel.getViewState().observe(getViewLifecycleOwner(), new Observer(avatarImageView, badgeImageView, textView, moneyView, learnMoreTextView, textView2, textView3, textView4, moneyView2, moneyView3, group, textView5) { // from class: org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ AvatarImageView f$1;
            public final /* synthetic */ MoneyView f$10;
            public final /* synthetic */ Group f$11;
            public final /* synthetic */ TextView f$12;
            public final /* synthetic */ BadgeImageView f$2;
            public final /* synthetic */ TextView f$3;
            public final /* synthetic */ MoneyView f$4;
            public final /* synthetic */ LearnMoreTextView f$5;
            public final /* synthetic */ TextView f$6;
            public final /* synthetic */ TextView f$7;
            public final /* synthetic */ TextView f$8;
            public final /* synthetic */ MoneyView f$9;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
                this.f$8 = r9;
                this.f$9 = r10;
                this.f$10 = r11;
                this.f$11 = r12;
                this.f$12 = r13;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentDetailsFragment.$r8$lambda$2kyzTQE2XJAQzpTaqN3TpsA7Iec(PaymentDetailsFragment.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9, this.f$10, this.f$11, this.f$12, (PaymentsDetailsViewModel.ViewState) obj);
            }
        });
        paymentsDetailsViewModel.getPaymentExists().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentDetailsFragment.$r8$lambda$ppgpubKAd0QF7X5H10XUpOb2A3Q(PaymentDetailsFragment.this, (Boolean) obj);
            }
        });
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    public /* synthetic */ void lambda$onViewCreated$1(AvatarImageView avatarImageView, BadgeImageView badgeImageView, TextView textView, MoneyView moneyView, LearnMoreTextView learnMoreTextView, TextView textView2, TextView textView3, TextView textView4, MoneyView moneyView2, MoneyView moneyView3, Group group, TextView textView5, PaymentsDetailsViewModel.ViewState viewState) {
        if (viewState.getRecipient().getId().isUnknown() || viewState.getPayment().isDefrag()) {
            avatarImageView.disableQuickContact();
            avatarImageView.setImageResource(R.drawable.ic_mobilecoin_avatar_24);
        } else {
            avatarImageView.setRecipient(viewState.getRecipient(), true);
            badgeImageView.setBadgeFromRecipient(viewState.getRecipient());
        }
        textView.setText(describeToOrFrom(viewState));
        if (viewState.getPayment().getState() == State.FAILED) {
            moneyView.setTextColor(ContextCompat.getColor(requireContext(), R.color.signal_text_primary_disabled));
            moneyView.setMoney(viewState.getPayment().getAmountPlusFeeWithDirection(), false);
            learnMoreTextView.setVisibility(8);
        } else {
            moneyView.setMoney(viewState.getPayment().getAmountPlusFeeWithDirection());
            if (viewState.getPayment().isDefrag()) {
                learnMoreTextView.setLearnMoreVisible(true);
                learnMoreTextView.setText(R.string.PaymentsDetailsFragment__coin_cleanup_information);
                learnMoreTextView.setLink(getString(R.string.PaymentsDetailsFragment__learn_more__cleanup_fee));
            } else {
                learnMoreTextView.setLearnMoreVisible(true);
                learnMoreTextView.setText(R.string.PaymentsDetailsFragment__information);
                learnMoreTextView.setLink(getString(R.string.PaymentsDetailsFragment__learn_more__information));
            }
            learnMoreTextView.setVisibility(0);
        }
        String trim = viewState.getPayment().getNote().trim();
        textView2.setText(trim);
        textView2.setVisibility(TextUtils.isEmpty(trim) ? 8 : 0);
        textView3.setText(describeStatus(viewState.getPayment()));
        textView4.setText(describeSentBy(viewState));
        if (viewState.getPayment().getDirection().isReceived()) {
            Money.MobileCoin mobileCoin = Money.MobileCoin.ZERO;
            moneyView2.setMoney(mobileCoin);
            moneyView3.setMoney(mobileCoin);
            group.setVisibility(8);
            return;
        }
        textView5.setText(describeSentTo(viewState, viewState.getPayment()));
        moneyView2.setMoney(viewState.getPayment().getAmount());
        moneyView3.setMoney(viewState.getPayment().getFee());
        group.setVisibility(0);
    }

    public /* synthetic */ void lambda$onViewCreated$2(Boolean bool) {
        if (!bool.booleanValue()) {
            Log.w(TAG, "Failed to find payment detail");
            FragmentActivity requireActivity = requireActivity();
            requireActivity.onBackPressed();
            Toast.makeText(requireActivity, (int) R.string.PaymentsDetailsFragment__no_details_available, 0).show();
        }
    }

    private CharSequence describeToOrFrom(PaymentsDetailsViewModel.ViewState viewState) {
        if (viewState.getPayment().isDefrag()) {
            return getString(R.string.PaymentsDetailsFragment__coin_cleanup_fee);
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$Direction[viewState.getPayment().getDirection().ordinal()];
        if (i == 1) {
            spannableStringBuilder.append((CharSequence) getString(R.string.PaymentsDetailsFragment__to));
        } else if (i == 2) {
            spannableStringBuilder.append((CharSequence) getString(R.string.PaymentsDetailsFragment__from));
        } else {
            throw new AssertionError();
        }
        spannableStringBuilder.append(' ').append(describe(viewState.getPayment().getPayee(), viewState.getRecipient()));
        return spannableStringBuilder;
    }

    private CharSequence describe(Payee payee, Recipient recipient) {
        if (payee.hasRecipientId()) {
            return recipient.getDisplayName(requireContext());
        }
        if (payee.hasPublicAddress()) {
            Context requireContext = requireContext();
            CharSequence abbreviateInMiddle = StringUtil.abbreviateInMiddle(payee.requirePublicAddress().getPaymentAddressBase58(), 17);
            Objects.requireNonNull(abbreviateInMiddle);
            return mono(requireContext, abbreviateInMiddle);
        }
        throw new AssertionError();
    }

    private static CharSequence mono(Context context, CharSequence charSequence) {
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new TextAppearanceSpan(context, R.style.TextAppearance_Signal_Mono), 0, charSequence.length(), 33);
        return spannableString;
    }

    private CharSequence describeSentBy(PaymentsDetailsViewModel.ViewState viewState) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$Direction[viewState.getPayment().getDirection().ordinal()];
        if (i == 1) {
            return getResources().getString(R.string.PaymentsDetailsFragment__you_on_s_at_s, viewState.getDate(), viewState.getTime(requireContext()));
        }
        if (i == 2) {
            return SpanUtil.replacePlaceHolder(getResources().getString(R.string.PaymentsDetailsFragment__s_on_s_at_s, SpanUtil.SPAN_PLACE_HOLDER, viewState.getDate(), viewState.getTime(requireContext())), describe(viewState.getPayment().getPayee(), viewState.getRecipient()));
        }
        throw new AssertionError();
    }

    private CharSequence describeSentTo(PaymentsDetailsViewModel.ViewState viewState, PaymentDatabase.PaymentTransaction paymentTransaction) {
        if (paymentTransaction.getDirection().isSent()) {
            return SpanUtil.insertSingleSpan(getResources(), R.string.PaymentsDetailsFragment__sent_to_s, describe(paymentTransaction.getPayee(), viewState.getRecipient()));
        }
        throw new AssertionError();
    }

    /* renamed from: org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$Direction;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$State;

        static {
            int[] iArr = new int[State.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$State = iArr;
            try {
                iArr[State.INITIAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$State[State.SUBMITTED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$State[State.SUCCESSFUL.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$State[State.FAILED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[Direction.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$Direction = iArr2;
            try {
                iArr2[Direction.SENT.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$Direction[Direction.RECEIVED.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    private CharSequence describeStatus(PaymentDatabase.PaymentTransaction paymentTransaction) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$State[paymentTransaction.getState().ordinal()];
        if (i == 1) {
            return getResources().getString(R.string.PaymentsDetailsFragment__submitting_payment);
        }
        if (i == 2) {
            return getResources().getString(R.string.PaymentsDetailsFragment__processing_payment);
        }
        if (i == 3) {
            return getResources().getString(R.string.PaymentsDetailsFragment__payment_complete);
        }
        if (i == 4) {
            return SpanUtil.color(ContextCompat.getColor(requireContext(), R.color.signal_alert_primary), getResources().getString(R.string.PaymentsDetailsFragment__payment_failed));
        }
        throw new AssertionError();
    }

    private CharSequence getContactFromToTextFromDirection(Direction direction) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$Direction[direction.ordinal()];
        if (i == 1) {
            return getResources().getString(R.string.PaymentsDetailsFragment__sent_payment);
        }
        if (i == 2) {
            return getResources().getString(R.string.PaymentsDetailsFragment__received_payment);
        }
        throw new AssertionError();
    }

    private CharSequence getStatusFromPayment(Payment payment) {
        return getResources().getString(R.string.PaymentsDeatilsFragment__payment_completed_s, DateUtils.getTimeString(requireContext(), Locale.getDefault(), payment.getDisplayTimestamp()));
    }
}
