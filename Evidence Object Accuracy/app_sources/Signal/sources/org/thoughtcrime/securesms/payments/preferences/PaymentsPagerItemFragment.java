package org.thoughtcrime.securesms.payments.preferences;

import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.PaymentPreferencesDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter;
import org.thoughtcrime.securesms.payments.preferences.PaymentsPagerItemViewModel;
import org.thoughtcrime.securesms.payments.preferences.model.PaymentItem;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public class PaymentsPagerItemFragment extends LoggingFragment {
    private static final String PAYMENT_CATEGORY;
    private PaymentsPagerItemViewModel viewModel;

    public static Fragment getFragmentForAllPayments() {
        return getFragment(PaymentCategory.ALL);
    }

    public static Fragment getFragmentForSentPayments() {
        return getFragment(PaymentCategory.SENT);
    }

    public static Fragment getFragmentForReceivedPayments() {
        return getFragment(PaymentCategory.RECEIVED);
    }

    private static Fragment getFragment(PaymentCategory paymentCategory) {
        Bundle bundle = new Bundle();
        bundle.putString(PAYMENT_CATEGORY, paymentCategory.getCode());
        PaymentsPagerItemFragment paymentsPagerItemFragment = new PaymentsPagerItemFragment();
        paymentsPagerItemFragment.setArguments(bundle);
        return paymentsPagerItemFragment;
    }

    public PaymentsPagerItemFragment() {
        super(R.layout.payment_preferences_all_pager_item_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.viewModel = (PaymentsPagerItemViewModel) ViewModelProviders.of(this, new PaymentsPagerItemViewModel.Factory(PaymentCategory.forCode(requireArguments().getString(PAYMENT_CATEGORY)))).get(PaymentsPagerItemViewModel.class);
        PaymentsHomeAdapter paymentsHomeAdapter = new PaymentsHomeAdapter(new Callbacks());
        ((RecyclerView) view.findViewById(R.id.payments_activity_pager_item_fragment_recycler)).setAdapter(paymentsHomeAdapter);
        this.viewModel.getList().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsPagerItemFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsHomeAdapter.this.submitList((MappingModelList) obj);
            }
        });
    }

    /* loaded from: classes4.dex */
    private class Callbacks implements PaymentsHomeAdapter.Callbacks {
        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public /* synthetic */ void onActivatePayments() {
            PaymentsHomeAdapter.Callbacks.CC.$default$onActivatePayments(this);
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public /* synthetic */ void onInfoCardDismissed() {
            PaymentsHomeAdapter.Callbacks.CC.$default$onInfoCardDismissed(this);
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public /* synthetic */ void onRestorePaymentsAccount() {
            PaymentsHomeAdapter.Callbacks.CC.$default$onRestorePaymentsAccount(this);
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public /* synthetic */ void onSeeAll(PaymentType paymentType) {
            PaymentsHomeAdapter.Callbacks.CC.$default$onSeeAll(this, paymentType);
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public /* synthetic */ void onUpdatePin() {
            PaymentsHomeAdapter.Callbacks.CC.$default$onUpdatePin(this);
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public /* synthetic */ void onViewRecoveryPhrase() {
            PaymentsHomeAdapter.Callbacks.CC.$default$onViewRecoveryPhrase(this);
        }

        private Callbacks() {
            PaymentsPagerItemFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter.Callbacks
        public void onPaymentItem(PaymentItem paymentItem) {
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(PaymentsPagerItemFragment.this), PaymentPreferencesDirections.actionDirectlyToPaymentDetails(paymentItem.getPaymentDetailsParcelable()));
        }
    }
}
