package org.thoughtcrime.securesms.payments.currency;

import j$.util.Optional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.signal.core.util.money.FiatMoney;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class CurrencyExchange {
    private final Map<String, BigDecimal> conversions;
    private final List<Currency> supportedCurrencies;
    private final long timestamp;

    public CurrencyExchange(Map<String, Double> map, long j) {
        this.conversions = new HashMap(map.size());
        this.supportedCurrencies = new ArrayList(map.size());
        this.timestamp = j;
        for (Map.Entry<String, Double> entry : map.entrySet()) {
            if (entry.getValue() != null) {
                this.conversions.put(entry.getKey(), BigDecimal.valueOf(entry.getValue().doubleValue()));
                Currency currencyByCurrencyCode = CurrencyUtil.getCurrencyByCurrencyCode(entry.getKey());
                if (currencyByCurrencyCode != null && SupportedCurrencies.ALL.contains(currencyByCurrencyCode.getCurrencyCode())) {
                    this.supportedCurrencies.add(currencyByCurrencyCode);
                }
            }
        }
    }

    public ExchangeRate getExchangeRate(Currency currency) {
        return new ExchangeRate(currency, this.conversions.get(currency.getCurrencyCode()), this.timestamp);
    }

    public List<Currency> getSupportedCurrencies() {
        return this.supportedCurrencies;
    }

    /* loaded from: classes4.dex */
    public static final class ExchangeRate {
        private final Currency currency;
        private final BigDecimal rate;
        private final long timestamp;

        ExchangeRate(Currency currency, BigDecimal bigDecimal, long j) {
            this.currency = currency;
            this.rate = bigDecimal;
            this.timestamp = j;
        }

        public long getTimestamp() {
            return this.timestamp;
        }

        public Optional<FiatMoney> exchange(Money money) {
            BigDecimal bigDecimal = money.requireMobileCoin().toBigDecimal();
            BigDecimal bigDecimal2 = this.rate;
            if (bigDecimal2 != null) {
                return Optional.of(new FiatMoney(bigDecimal.multiply(bigDecimal2).setScale(this.currency.getDefaultFractionDigits(), RoundingMode.HALF_EVEN), this.currency, this.timestamp));
            }
            return Optional.empty();
        }

        public Optional<Money> exchange(FiatMoney fiatMoney) {
            if (this.rate != null) {
                return Optional.of(Money.mobileCoin(fiatMoney.getAmount().setScale(12, RoundingMode.HALF_EVEN).divide(this.rate, RoundingMode.HALF_EVEN)));
            }
            return Optional.empty();
        }
    }
}
