package org.thoughtcrime.securesms.payments.create;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.CreatePaymentDetails;

/* loaded from: classes4.dex */
public class CreatePaymentFragmentDirections {
    private CreatePaymentFragmentDirections() {
    }

    public static NavDirections actionCreatePaymentFragmentToEditPaymentNoteFragment() {
        return new ActionOnlyNavDirections(R.id.action_createPaymentFragment_to_editPaymentNoteFragment);
    }

    public static ActionCreatePaymentFragmentToConfirmPaymentFragment actionCreatePaymentFragmentToConfirmPaymentFragment(CreatePaymentDetails createPaymentDetails) {
        return new ActionCreatePaymentFragmentToConfirmPaymentFragment(createPaymentDetails);
    }

    /* loaded from: classes4.dex */
    public static class ActionCreatePaymentFragmentToConfirmPaymentFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_createPaymentFragment_to_confirmPaymentFragment;
        }

        private ActionCreatePaymentFragmentToConfirmPaymentFragment(CreatePaymentDetails createPaymentDetails) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (createPaymentDetails != null) {
                hashMap.put("createPaymentDetails", createPaymentDetails);
                return;
            }
            throw new IllegalArgumentException("Argument \"createPaymentDetails\" is marked as non-null but was passed a null value.");
        }

        public ActionCreatePaymentFragmentToConfirmPaymentFragment setCreatePaymentDetails(CreatePaymentDetails createPaymentDetails) {
            if (createPaymentDetails != null) {
                this.arguments.put("createPaymentDetails", createPaymentDetails);
                return this;
            }
            throw new IllegalArgumentException("Argument \"createPaymentDetails\" is marked as non-null but was passed a null value.");
        }

        public ActionCreatePaymentFragmentToConfirmPaymentFragment setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("createPaymentDetails")) {
                CreatePaymentDetails createPaymentDetails = (CreatePaymentDetails) this.arguments.get("createPaymentDetails");
                if (Parcelable.class.isAssignableFrom(CreatePaymentDetails.class) || createPaymentDetails == null) {
                    bundle.putParcelable("createPaymentDetails", (Parcelable) Parcelable.class.cast(createPaymentDetails));
                } else if (Serializable.class.isAssignableFrom(CreatePaymentDetails.class)) {
                    bundle.putSerializable("createPaymentDetails", (Serializable) Serializable.class.cast(createPaymentDetails));
                } else {
                    throw new UnsupportedOperationException(CreatePaymentDetails.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("finish_on_confirm")) {
                bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
            } else {
                bundle.putBoolean("finish_on_confirm", false);
            }
            return bundle;
        }

        public CreatePaymentDetails getCreatePaymentDetails() {
            return (CreatePaymentDetails) this.arguments.get("createPaymentDetails");
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionCreatePaymentFragmentToConfirmPaymentFragment actionCreatePaymentFragmentToConfirmPaymentFragment = (ActionCreatePaymentFragmentToConfirmPaymentFragment) obj;
            if (this.arguments.containsKey("createPaymentDetails") != actionCreatePaymentFragmentToConfirmPaymentFragment.arguments.containsKey("createPaymentDetails")) {
                return false;
            }
            if (getCreatePaymentDetails() == null ? actionCreatePaymentFragmentToConfirmPaymentFragment.getCreatePaymentDetails() == null : getCreatePaymentDetails().equals(actionCreatePaymentFragmentToConfirmPaymentFragment.getCreatePaymentDetails())) {
                return this.arguments.containsKey("finish_on_confirm") == actionCreatePaymentFragmentToConfirmPaymentFragment.arguments.containsKey("finish_on_confirm") && getFinishOnConfirm() == actionCreatePaymentFragmentToConfirmPaymentFragment.getFinishOnConfirm() && getActionId() == actionCreatePaymentFragmentToConfirmPaymentFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((((getCreatePaymentDetails() != null ? getCreatePaymentDetails().hashCode() : 0) + 31) * 31) + (getFinishOnConfirm() ? 1 : 0)) * 31) + getActionId();
        }

        public String toString() {
            return "ActionCreatePaymentFragmentToConfirmPaymentFragment(actionId=" + getActionId() + "){createPaymentDetails=" + getCreatePaymentDetails() + ", finishOnConfirm=" + getFinishOnConfirm() + "}";
        }
    }
}
