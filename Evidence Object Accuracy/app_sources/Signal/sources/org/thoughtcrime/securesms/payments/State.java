package org.thoughtcrime.securesms.payments;

/* loaded from: classes4.dex */
public enum State {
    INITIAL(0),
    SUBMITTED(1),
    SUCCESSFUL(2),
    FAILED(3);
    
    private final int value;

    State(int i) {
        this.value = i;
    }

    public int serialize() {
        return this.value;
    }

    public static State deserialize(int i) {
        State state = INITIAL;
        if (i == state.value) {
            return state;
        }
        State state2 = SUBMITTED;
        if (i == state2.value) {
            return state2;
        }
        State state3 = SUCCESSFUL;
        if (i == state3.value) {
            return state3;
        }
        State state4 = FAILED;
        if (i == state4.value) {
            return state4;
        }
        throw new AssertionError("" + i);
    }

    public boolean isInProgress() {
        return this == INITIAL || this == SUBMITTED;
    }
}
