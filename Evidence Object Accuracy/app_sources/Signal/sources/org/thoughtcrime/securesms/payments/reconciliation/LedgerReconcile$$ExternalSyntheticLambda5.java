package org.thoughtcrime.securesms.payments.reconciliation;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.payments.MobileCoinLedgerWrapper;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LedgerReconcile$$ExternalSyntheticLambda5 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return ((MobileCoinLedgerWrapper.OwnedTxo) obj).isSpent();
    }
}
