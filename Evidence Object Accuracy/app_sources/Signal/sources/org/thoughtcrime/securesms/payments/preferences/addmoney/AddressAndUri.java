package org.thoughtcrime.securesms.payments.preferences.addmoney;

import android.net.Uri;

/* loaded from: classes4.dex */
public final class AddressAndUri {
    private final String addressB58;
    private final Uri uri;

    public AddressAndUri(String str, Uri uri) {
        this.addressB58 = str;
        this.uri = uri;
    }

    public String getAddressB58() {
        return this.addressB58;
    }

    public Uri getUri() {
        return this.uri;
    }
}
