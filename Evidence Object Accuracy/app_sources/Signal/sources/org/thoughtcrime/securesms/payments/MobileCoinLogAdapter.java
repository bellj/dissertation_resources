package org.thoughtcrime.securesms.payments;

import com.mobilecoin.lib.log.LogAdapter;
import com.mobilecoin.lib.log.Logger;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
final class MobileCoinLogAdapter implements LogAdapter {
    @Override // com.mobilecoin.lib.log.LogAdapter
    public boolean isLoggable(Logger.Level level, String str) {
        return level.ordinal() >= Logger.Level.WARNING.ordinal();
    }

    /* renamed from: org.thoughtcrime.securesms.payments.MobileCoinLogAdapter$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$mobilecoin$lib$log$Logger$Level;

        static {
            int[] iArr = new int[Logger.Level.values().length];
            $SwitchMap$com$mobilecoin$lib$log$Logger$Level = iArr;
            try {
                iArr[Logger.Level.INFO.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$log$Logger$Level[Logger.Level.VERBOSE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$log$Logger$Level[Logger.Level.DEBUG.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$log$Logger$Level[Logger.Level.WARNING.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$log$Logger$Level[Logger.Level.ERROR.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$log$Logger$Level[Logger.Level.WTF.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    @Override // com.mobilecoin.lib.log.LogAdapter
    public void log(Logger.Level level, String str, String str2, Throwable th, Object... objArr) {
        switch (AnonymousClass1.$SwitchMap$com$mobilecoin$lib$log$Logger$Level[level.ordinal()]) {
            case 1:
                Log.i(str, str2, th);
                return;
            case 2:
                Log.v(str, str2, th);
                return;
            case 3:
                Log.d(str, str2, th);
                return;
            case 4:
                Log.w(str, str2, th);
                return;
            case 5:
            case 6:
                Log.e(str, str2, th);
                return;
            default:
                return;
        }
    }
}
