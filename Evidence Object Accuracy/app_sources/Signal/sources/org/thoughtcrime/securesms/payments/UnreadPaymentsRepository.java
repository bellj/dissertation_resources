package org.thoughtcrime.securesms.payments;

import java.util.UUID;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* loaded from: classes4.dex */
public class UnreadPaymentsRepository {
    private static final Executor EXECUTOR = SignalExecutors.BOUNDED;

    public void markAllPaymentsSeen() {
        EXECUTOR.execute(new Runnable() { // from class: org.thoughtcrime.securesms.payments.UnreadPaymentsRepository$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                UnreadPaymentsRepository.this.markAllPaymentsSeenInternal();
            }
        });
    }

    public void markPaymentSeen(UUID uuid) {
        EXECUTOR.execute(new Runnable(uuid) { // from class: org.thoughtcrime.securesms.payments.UnreadPaymentsRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ UUID f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                UnreadPaymentsRepository.this.lambda$markPaymentSeen$0(this.f$1);
            }
        });
    }

    public void markAllPaymentsSeenInternal() {
        ApplicationDependencies.getApplication();
        SignalDatabase.payments().markAllSeen();
    }

    /* renamed from: markPaymentSeenInternal */
    public void lambda$markPaymentSeen$0(UUID uuid) {
        ApplicationDependencies.getApplication();
        SignalDatabase.payments().markPaymentSeen(uuid);
    }
}
