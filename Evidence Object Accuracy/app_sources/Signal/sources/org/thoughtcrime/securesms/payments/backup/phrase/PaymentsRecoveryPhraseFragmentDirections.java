package org.thoughtcrime.securesms.payments.backup.phrase;

import android.os.Bundle;
import androidx.navigation.NavDirections;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class PaymentsRecoveryPhraseFragmentDirections {
    private PaymentsRecoveryPhraseFragmentDirections() {
    }

    public static ActionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm actionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm(boolean z) {
        return new ActionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm(z);
    }

    /* loaded from: classes4.dex */
    public static class ActionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_paymentsRecoveryPhrase_to_paymentsRecoveryPhraseConfirm;
        }

        private ActionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm(boolean z) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("finish_on_confirm", Boolean.valueOf(z));
        }

        public ActionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("finish_on_confirm")) {
                bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
            }
            return bundle;
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm actionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm = (ActionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm) obj;
            return this.arguments.containsKey("finish_on_confirm") == actionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm.arguments.containsKey("finish_on_confirm") && getFinishOnConfirm() == actionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm.getFinishOnConfirm() && getActionId() == actionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm.getActionId();
        }

        public int hashCode() {
            return (((getFinishOnConfirm() ? 1 : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm(actionId=" + getActionId() + "){finishOnConfirm=" + getFinishOnConfirm() + "}";
        }
    }
}
