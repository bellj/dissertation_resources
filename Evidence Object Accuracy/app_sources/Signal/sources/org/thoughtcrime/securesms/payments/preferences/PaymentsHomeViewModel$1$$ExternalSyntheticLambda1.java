package org.thoughtcrime.securesms.payments.preferences;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PaymentsHomeViewModel$1$$ExternalSyntheticLambda1 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return PaymentsHomeViewModel.AnonymousClass1.lambda$onComplete$0((PaymentsHomeState) obj);
    }
}
