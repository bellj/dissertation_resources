package org.thoughtcrime.securesms.payments.proto;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.thoughtcrime.securesms.payments.proto.PaymentMetaData;

/* loaded from: classes4.dex */
public interface PaymentMetaDataOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    PaymentMetaData.MobileCoinTxoIdentification getMobileCoinTxoIdentification();

    boolean hasMobileCoinTxoIdentification();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
