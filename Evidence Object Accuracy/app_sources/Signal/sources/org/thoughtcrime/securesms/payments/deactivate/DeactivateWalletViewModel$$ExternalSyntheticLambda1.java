package org.thoughtcrime.securesms.payments.deactivate;

import androidx.arch.core.util.Function;
import org.thoughtcrime.securesms.payments.Balance;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class DeactivateWalletViewModel$$ExternalSyntheticLambda1 implements Function {
    @Override // androidx.arch.core.util.Function
    public final Object apply(Object obj) {
        return ((Balance) obj).getFullAmount();
    }
}
