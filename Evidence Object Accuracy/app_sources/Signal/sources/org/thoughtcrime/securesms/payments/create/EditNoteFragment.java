package org.thoughtcrime.securesms.payments.create;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiEditText;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class EditNoteFragment extends LoggingFragment {
    private EmojiEditText noteEditText;
    private CreatePaymentViewModel viewModel;

    public EditNoteFragment() {
        super(R.layout.edit_note_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.viewModel = (CreatePaymentViewModel) new ViewModelProvider(Navigation.findNavController(view).getViewModelStoreOwner(R.id.payments_create)).get(CreatePaymentViewModel.class);
        ((Toolbar) view.findViewById(R.id.edit_note_fragment_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.create.EditNoteFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditNoteFragment.$r8$lambda$5aJ4zDOrsWQpGhv2I5rWKlVQWV8(view2);
            }
        });
        this.noteEditText = (EmojiEditText) view.findViewById(R.id.edit_note_fragment_edit_text);
        this.viewModel.getNote().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.create.EditNoteFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditNoteFragment.m2391$r8$lambda$3z1iP0VZRtqgHMqmb2qSuM20nY(EditNoteFragment.this, (CharSequence) obj);
            }
        });
        this.noteEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.payments.create.EditNoteFragment$$ExternalSyntheticLambda2
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return EditNoteFragment.m2392$r8$lambda$XeTrY0jhAthbLLRHVd0eXyQEsg(EditNoteFragment.this, textView, i, keyEvent);
            }
        });
        view.findViewById(R.id.edit_note_fragment_fab).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.create.EditNoteFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditNoteFragment.$r8$lambda$slU58b7kZ28P_NTKmGZS9suqnRA(EditNoteFragment.this, view2);
            }
        });
        ViewUtil.focusAndMoveCursorToEndAndOpenKeyboard(this.noteEditText);
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    public /* synthetic */ void lambda$onViewCreated$1(CharSequence charSequence) {
        this.noteEditText.setText(charSequence);
        if (!TextUtils.isEmpty(charSequence)) {
            this.noteEditText.setSelection(charSequence.length());
        }
    }

    public /* synthetic */ boolean lambda$onViewCreated$2(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        saveNote();
        return true;
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view) {
        saveNote();
    }

    private void saveNote() {
        ViewUtil.hideKeyboard(requireView().getContext(), requireView());
        this.viewModel.setNote(this.noteEditText.getText() != null ? this.noteEditText.getText().toString() : null);
        Navigation.findNavController(requireView()).popBackStack();
    }
}
