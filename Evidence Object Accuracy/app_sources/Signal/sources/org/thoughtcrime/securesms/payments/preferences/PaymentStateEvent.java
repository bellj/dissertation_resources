package org.thoughtcrime.securesms.payments.preferences;

/* loaded from: classes4.dex */
public enum PaymentStateEvent {
    NO_BALANCE,
    DEACTIVATE_WITHOUT_BALANCE,
    DEACTIVATE_WITH_BALANCE,
    DEACTIVATED,
    ACTIVATED
}
