package org.thoughtcrime.securesms.payments.confirm;

import androidx.arch.core.util.Function;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.Optional;
import j$.util.function.Function;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.Balance;
import org.thoughtcrime.securesms.payments.CreatePaymentDetails;
import org.thoughtcrime.securesms.payments.FiatMoneyUtil;
import org.thoughtcrime.securesms.payments.PaymentTransactionLiveData;
import org.thoughtcrime.securesms.payments.PaymentsAddressException;
import org.thoughtcrime.securesms.payments.State;
import org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentRepository;
import org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentState;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class ConfirmPaymentViewModel extends ViewModel {
    private static final String TAG = Log.tag(ConfirmPaymentViewModel.class);
    private final ConfirmPaymentRepository confirmPaymentRepository;
    private final SingleLiveEvent<ErrorType> errorEvents = new SingleLiveEvent<>();
    private final MutableLiveData<Boolean> feeRetry;
    private final LiveData<Boolean> paymentDone;
    private final Store<ConfirmPaymentState> store;

    /* loaded from: classes4.dex */
    public enum ErrorType {
        NO_PROFILE_KEY,
        NO_ADDRESS,
        CAN_NOT_GET_FEE
    }

    public static /* synthetic */ Money lambda$new$2(Money money, Boolean bool) {
        return money;
    }

    /* JADX DEBUG: Type inference failed for r1v1. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.payments.Balance>, androidx.lifecycle.LiveData<Input> */
    /* JADX DEBUG: Type inference failed for r4v12. Raw type applied. Possible types: androidx.lifecycle.LiveData<j$.util.Optional<org.signal.core.util.money.FiatMoney>>, androidx.lifecycle.LiveData<Input> */
    ConfirmPaymentViewModel(ConfirmPaymentState confirmPaymentState, ConfirmPaymentRepository confirmPaymentRepository) {
        Store<ConfirmPaymentState> store = new Store<>(confirmPaymentState);
        this.store = store;
        this.confirmPaymentRepository = confirmPaymentRepository;
        Boolean bool = Boolean.TRUE;
        DefaultValueLiveData defaultValueLiveData = new DefaultValueLiveData(bool);
        this.feeRetry = defaultValueLiveData;
        store.update(SignalStore.paymentsValues().liveMobileCoinBalance(), new Store.Action() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ConfirmPaymentViewModel.lambda$new$0((Balance) obj, (ConfirmPaymentState) obj2);
            }
        });
        store.update(LiveDataUtil.delay(1000, bool), new Store.Action() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda7
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ConfirmPaymentViewModel.lambda$new$1((Boolean) obj, (ConfirmPaymentState) obj2);
            }
        });
        LiveData distinctUntilChanged = Transformations.distinctUntilChanged(Transformations.map(store.getStateLiveData(), new Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda8
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((ConfirmPaymentState) obj).getAmount();
            }
        }));
        store.update(LiveDataUtil.mapAsync(LiveDataUtil.combineLatest(distinctUntilChanged, defaultValueLiveData, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda9
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return ConfirmPaymentViewModel.lambda$new$2((Money) obj, (Boolean) obj2);
            }
        }), new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda10
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConfirmPaymentViewModel.this.getFee((Money) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }), new Store.Action() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda11
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ConfirmPaymentViewModel.lambda$new$3((ConfirmPaymentRepository.GetFeeResult) obj, (ConfirmPaymentState) obj2);
            }
        });
        store.update(Transformations.switchMap(Transformations.distinctUntilChanged(Transformations.map(store.getStateLiveData(), new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda12
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((ConfirmPaymentState) obj).getPaymentId();
            }
        })), new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda13
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ConfirmPaymentViewModel.lambda$new$4((UUID) obj);
            }
        }), new Store.Action() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda14
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ConfirmPaymentViewModel.this.handlePaymentTransactionChanged((PaymentDatabase.PaymentTransaction) obj, (ConfirmPaymentState) obj2);
            }
        });
        this.paymentDone = Transformations.distinctUntilChanged(Transformations.map(store.getStateLiveData(), new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda15
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ConfirmPaymentViewModel.lambda$new$5((ConfirmPaymentState) obj);
            }
        }));
        store.update(FiatMoneyUtil.getExchange(distinctUntilChanged), new Store.Action() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ConfirmPaymentViewModel.lambda$new$6((Optional) obj, (ConfirmPaymentState) obj2);
            }
        });
        store.update(Transformations.switchMap(Transformations.map(store.getStateLiveData(), new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda4
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((ConfirmPaymentState) obj).getStatus();
            }
        }), new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda5
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ConfirmPaymentViewModel.lambda$new$7((ConfirmPaymentState.Status) obj);
            }
        }), new Store.Action() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ConfirmPaymentViewModel.this.handleTimeout((ConfirmPaymentState.Status) obj, (ConfirmPaymentState) obj2);
            }
        });
    }

    public static /* synthetic */ ConfirmPaymentState lambda$new$0(Balance balance, ConfirmPaymentState confirmPaymentState) {
        return confirmPaymentState.updateBalance(balance.getFullAmount());
    }

    public static /* synthetic */ ConfirmPaymentState lambda$new$1(Boolean bool, ConfirmPaymentState confirmPaymentState) {
        return confirmPaymentState.getFeeStatus() == ConfirmPaymentState.FeeStatus.NOT_SET ? confirmPaymentState.updateFeeStillLoading() : confirmPaymentState;
    }

    public static /* synthetic */ ConfirmPaymentState lambda$new$3(ConfirmPaymentRepository.GetFeeResult getFeeResult, ConfirmPaymentState confirmPaymentState) {
        if (getFeeResult instanceof ConfirmPaymentRepository.GetFeeResult.Success) {
            return confirmPaymentState.updateFee(((ConfirmPaymentRepository.GetFeeResult.Success) getFeeResult).getFee());
        }
        if (getFeeResult instanceof ConfirmPaymentRepository.GetFeeResult.Error) {
            return confirmPaymentState.updateFeeError();
        }
        throw new AssertionError();
    }

    public static /* synthetic */ LiveData lambda$new$4(UUID uuid) {
        return uuid != null ? new PaymentTransactionLiveData(uuid) : new MutableLiveData();
    }

    public static /* synthetic */ Boolean lambda$new$5(ConfirmPaymentState confirmPaymentState) {
        return Boolean.valueOf(confirmPaymentState.getStatus().isTerminalStatus());
    }

    public static /* synthetic */ ConfirmPaymentState lambda$new$6(Optional optional, ConfirmPaymentState confirmPaymentState) {
        return confirmPaymentState.updateExchange((FiatMoney) optional.orElse(null));
    }

    public static /* synthetic */ LiveData lambda$new$7(ConfirmPaymentState.Status status) {
        if (status != ConfirmPaymentState.Status.PROCESSING) {
            return LiveDataUtil.never();
        }
        Log.i(TAG, "Beginning timeout timer");
        return LiveDataUtil.delay(TimeUnit.SECONDS.toMillis(20), status);
    }

    public LiveData<ConfirmPaymentState> getState() {
        return this.store.getStateLiveData();
    }

    public LiveData<Boolean> isPaymentDone() {
        return this.paymentDone;
    }

    public LiveData<ErrorType> getErrorTypeEvents() {
        return this.errorEvents;
    }

    public static /* synthetic */ ConfirmPaymentState lambda$confirmPayment$8(ConfirmPaymentState confirmPaymentState) {
        return confirmPaymentState.updateStatus(ConfirmPaymentState.Status.SUBMITTING);
    }

    public void confirmPayment() {
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConfirmPaymentViewModel.lambda$confirmPayment$8((ConfirmPaymentState) obj);
            }
        });
        this.confirmPaymentRepository.confirmPayment(this.store.getState(), new Consumer() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda1
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ConfirmPaymentViewModel.this.handleConfirmPaymentResult((ConfirmPaymentRepository.ConfirmPaymentResult) obj);
            }
        });
    }

    public void refreshFee() {
        this.feeRetry.setValue(Boolean.TRUE);
    }

    public ConfirmPaymentRepository.GetFeeResult getFee(Money money) {
        ConfirmPaymentRepository.GetFeeResult fee = this.confirmPaymentRepository.getFee(money);
        if (fee instanceof ConfirmPaymentRepository.GetFeeResult.Error) {
            this.errorEvents.postValue(ErrorType.CAN_NOT_GET_FEE);
        }
        return fee;
    }

    public void handleConfirmPaymentResult(ConfirmPaymentRepository.ConfirmPaymentResult confirmPaymentResult) {
        if (confirmPaymentResult instanceof ConfirmPaymentRepository.ConfirmPaymentResult.Success) {
            this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda16
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ConfirmPaymentViewModel.lambda$handleConfirmPaymentResult$9(ConfirmPaymentRepository.ConfirmPaymentResult.Success.this, (ConfirmPaymentState) obj);
                }
            });
        } else if (confirmPaymentResult instanceof ConfirmPaymentRepository.ConfirmPaymentResult.Error) {
            PaymentsAddressException.Code code = ((ConfirmPaymentRepository.ConfirmPaymentResult.Error) confirmPaymentResult).getCode();
            this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$$ExternalSyntheticLambda17
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ConfirmPaymentViewModel.lambda$handleConfirmPaymentResult$10((ConfirmPaymentState) obj);
                }
            });
            if (code != null) {
                this.errorEvents.postValue(getErrorType(code));
            }
        } else {
            throw new AssertionError();
        }
    }

    public static /* synthetic */ ConfirmPaymentState lambda$handleConfirmPaymentResult$9(ConfirmPaymentRepository.ConfirmPaymentResult.Success success, ConfirmPaymentState confirmPaymentState) {
        return confirmPaymentState.updatePaymentId(success.getPaymentId());
    }

    public static /* synthetic */ ConfirmPaymentState lambda$handleConfirmPaymentResult$10(ConfirmPaymentState confirmPaymentState) {
        return confirmPaymentState.updateStatus(ConfirmPaymentState.Status.ERROR);
    }

    private ErrorType getErrorType(PaymentsAddressException.Code code) {
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$PaymentsAddressException$Code[code.ordinal()]) {
            case 1:
                return ErrorType.NO_PROFILE_KEY;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return ErrorType.NO_ADDRESS;
            default:
                throw new AssertionError();
        }
    }

    public ConfirmPaymentState handlePaymentTransactionChanged(PaymentDatabase.PaymentTransaction paymentTransaction, ConfirmPaymentState confirmPaymentState) {
        if (paymentTransaction == null) {
            return confirmPaymentState;
        }
        if (confirmPaymentState.getStatus().isTerminalStatus()) {
            Log.w(TAG, "Payment already in a final state on transaction change");
            return confirmPaymentState;
        }
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$State[paymentTransaction.getState().ordinal()];
        if (i == 1) {
            return confirmPaymentState.updateStatus(ConfirmPaymentState.Status.SUBMITTING);
        }
        if (i == 2) {
            return confirmPaymentState.updateStatus(ConfirmPaymentState.Status.PROCESSING);
        }
        if (i == 3) {
            return confirmPaymentState.updateStatus(ConfirmPaymentState.Status.DONE);
        }
        if (i == 4) {
            return confirmPaymentState.updateStatus(ConfirmPaymentState.Status.ERROR);
        }
        throw new AssertionError();
    }

    /* renamed from: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$PaymentsAddressException$Code;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$State;

        static {
            int[] iArr = new int[State.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$State = iArr;
            try {
                iArr[State.INITIAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$State[State.SUBMITTED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$State[State.SUCCESSFUL.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$State[State.FAILED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[PaymentsAddressException.Code.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$PaymentsAddressException$Code = iArr2;
            try {
                iArr2[PaymentsAddressException.Code.NO_PROFILE_KEY.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$PaymentsAddressException$Code[PaymentsAddressException.Code.COULD_NOT_DECRYPT.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$PaymentsAddressException$Code[PaymentsAddressException.Code.NOT_ENABLED.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$PaymentsAddressException$Code[PaymentsAddressException.Code.INVALID_ADDRESS.ordinal()] = 4;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$PaymentsAddressException$Code[PaymentsAddressException.Code.INVALID_ADDRESS_SIGNATURE.ordinal()] = 5;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$PaymentsAddressException$Code[PaymentsAddressException.Code.NO_ADDRESS.ordinal()] = 6;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    public ConfirmPaymentState handleTimeout(ConfirmPaymentState.Status status, ConfirmPaymentState confirmPaymentState) {
        if (confirmPaymentState.getStatus().isTerminalStatus()) {
            Log.w(TAG, "Payment already in a final state on timeout");
            return confirmPaymentState;
        }
        String str = TAG;
        Log.w(str, "Timed out while in " + status);
        return confirmPaymentState.timeout();
    }

    /* loaded from: classes4.dex */
    static final class Factory implements ViewModelProvider.Factory {
        private final CreatePaymentDetails createPaymentDetails;

        public Factory(CreatePaymentDetails createPaymentDetails) {
            this.createPaymentDetails = createPaymentDetails;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ConfirmPaymentViewModel(new ConfirmPaymentState(this.createPaymentDetails.getPayee(), this.createPaymentDetails.getAmount(), this.createPaymentDetails.getNote()), new ConfirmPaymentRepository(ApplicationDependencies.getPayments().getWallet())));
        }
    }
}
