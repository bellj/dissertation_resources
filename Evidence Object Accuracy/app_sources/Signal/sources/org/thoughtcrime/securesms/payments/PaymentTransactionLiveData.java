package org.thoughtcrime.securesms.payments;

import androidx.lifecycle.LiveData;
import java.util.UUID;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.concurrent.SerialMonoLifoExecutor;

/* loaded from: classes4.dex */
public final class PaymentTransactionLiveData extends LiveData<PaymentDatabase.PaymentTransaction> {
    private final Executor executor = new SerialMonoLifoExecutor(SignalExecutors.BOUNDED);
    private final DatabaseObserver.Observer observer = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.payments.PaymentTransactionLiveData$$ExternalSyntheticLambda0
        @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
        public final void onChanged() {
            PaymentTransactionLiveData.$r8$lambda$jzUGfH6Fbl6GnFhag_qYkm76mgI(PaymentTransactionLiveData.this);
        }
    };
    private final PaymentDatabase paymentDatabase = SignalDatabase.payments();
    private final UUID paymentId;

    public PaymentTransactionLiveData(UUID uuid) {
        this.paymentId = uuid;
    }

    @Override // androidx.lifecycle.LiveData
    public void onActive() {
        getPaymentTransaction();
        ApplicationDependencies.getDatabaseObserver().registerPaymentObserver(this.paymentId, this.observer);
    }

    @Override // androidx.lifecycle.LiveData
    public void onInactive() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
    }

    public void getPaymentTransaction() {
        this.executor.execute(new Runnable() { // from class: org.thoughtcrime.securesms.payments.PaymentTransactionLiveData$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                PaymentTransactionLiveData.m2361$r8$lambda$midp9Wdu_WwXWoCWBQOzpf3MtY(PaymentTransactionLiveData.this);
            }
        });
    }

    public /* synthetic */ void lambda$getPaymentTransaction$0() {
        postValue(this.paymentDatabase.getPayment(this.paymentId));
    }
}
