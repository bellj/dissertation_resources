package org.thoughtcrime.securesms.payments.backup;

import android.os.Bundle;
import androidx.navigation.NavDirections;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class PaymentsRecoveryPasteFragmentDirections {
    private PaymentsRecoveryPasteFragmentDirections() {
    }

    public static ActionPaymentsRecoveryEntryToPaymentsRecoveryPhrase actionPaymentsRecoveryEntryToPaymentsRecoveryPhrase(boolean z) {
        return new ActionPaymentsRecoveryEntryToPaymentsRecoveryPhrase(z);
    }

    /* loaded from: classes4.dex */
    public static class ActionPaymentsRecoveryEntryToPaymentsRecoveryPhrase implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_paymentsRecoveryEntry_to_paymentsRecoveryPhrase;
        }

        private ActionPaymentsRecoveryEntryToPaymentsRecoveryPhrase(boolean z) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("finish_on_confirm", Boolean.valueOf(z));
        }

        public ActionPaymentsRecoveryEntryToPaymentsRecoveryPhrase setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        public ActionPaymentsRecoveryEntryToPaymentsRecoveryPhrase setWords(String[] strArr) {
            this.arguments.put("words", strArr);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("finish_on_confirm")) {
                bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
            }
            if (this.arguments.containsKey("words")) {
                bundle.putStringArray("words", (String[]) this.arguments.get("words"));
            } else {
                bundle.putStringArray("words", null);
            }
            return bundle;
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }

        public String[] getWords() {
            return (String[]) this.arguments.get("words");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionPaymentsRecoveryEntryToPaymentsRecoveryPhrase actionPaymentsRecoveryEntryToPaymentsRecoveryPhrase = (ActionPaymentsRecoveryEntryToPaymentsRecoveryPhrase) obj;
            if (this.arguments.containsKey("finish_on_confirm") != actionPaymentsRecoveryEntryToPaymentsRecoveryPhrase.arguments.containsKey("finish_on_confirm") || getFinishOnConfirm() != actionPaymentsRecoveryEntryToPaymentsRecoveryPhrase.getFinishOnConfirm() || this.arguments.containsKey("words") != actionPaymentsRecoveryEntryToPaymentsRecoveryPhrase.arguments.containsKey("words")) {
                return false;
            }
            if (getWords() == null ? actionPaymentsRecoveryEntryToPaymentsRecoveryPhrase.getWords() == null : getWords().equals(actionPaymentsRecoveryEntryToPaymentsRecoveryPhrase.getWords())) {
                return getActionId() == actionPaymentsRecoveryEntryToPaymentsRecoveryPhrase.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((((getFinishOnConfirm() ? 1 : 0) + 31) * 31) + Arrays.hashCode(getWords())) * 31) + getActionId();
        }

        public String toString() {
            return "ActionPaymentsRecoveryEntryToPaymentsRecoveryPhrase(actionId=" + getActionId() + "){finishOnConfirm=" + getFinishOnConfirm() + ", words=" + getWords() + "}";
        }
    }
}
