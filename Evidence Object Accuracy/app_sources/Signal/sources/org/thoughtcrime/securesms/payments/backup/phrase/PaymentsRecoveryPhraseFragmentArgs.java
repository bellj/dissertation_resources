package org.thoughtcrime.securesms.payments.backup.phrase;

import android.os.Bundle;
import java.util.Arrays;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class PaymentsRecoveryPhraseFragmentArgs {
    private final HashMap arguments;

    private PaymentsRecoveryPhraseFragmentArgs() {
        this.arguments = new HashMap();
    }

    private PaymentsRecoveryPhraseFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static PaymentsRecoveryPhraseFragmentArgs fromBundle(Bundle bundle) {
        PaymentsRecoveryPhraseFragmentArgs paymentsRecoveryPhraseFragmentArgs = new PaymentsRecoveryPhraseFragmentArgs();
        bundle.setClassLoader(PaymentsRecoveryPhraseFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("finish_on_confirm")) {
            paymentsRecoveryPhraseFragmentArgs.arguments.put("finish_on_confirm", Boolean.valueOf(bundle.getBoolean("finish_on_confirm")));
            if (bundle.containsKey("words")) {
                paymentsRecoveryPhraseFragmentArgs.arguments.put("words", bundle.getStringArray("words"));
            } else {
                paymentsRecoveryPhraseFragmentArgs.arguments.put("words", null);
            }
            return paymentsRecoveryPhraseFragmentArgs;
        }
        throw new IllegalArgumentException("Required argument \"finish_on_confirm\" is missing and does not have an android:defaultValue");
    }

    public boolean getFinishOnConfirm() {
        return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
    }

    public String[] getWords() {
        return (String[]) this.arguments.get("words");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("finish_on_confirm")) {
            bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
        }
        if (this.arguments.containsKey("words")) {
            bundle.putStringArray("words", (String[]) this.arguments.get("words"));
        } else {
            bundle.putStringArray("words", null);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PaymentsRecoveryPhraseFragmentArgs paymentsRecoveryPhraseFragmentArgs = (PaymentsRecoveryPhraseFragmentArgs) obj;
        if (this.arguments.containsKey("finish_on_confirm") == paymentsRecoveryPhraseFragmentArgs.arguments.containsKey("finish_on_confirm") && getFinishOnConfirm() == paymentsRecoveryPhraseFragmentArgs.getFinishOnConfirm() && this.arguments.containsKey("words") == paymentsRecoveryPhraseFragmentArgs.arguments.containsKey("words")) {
            return getWords() == null ? paymentsRecoveryPhraseFragmentArgs.getWords() == null : getWords().equals(paymentsRecoveryPhraseFragmentArgs.getWords());
        }
        return false;
    }

    public int hashCode() {
        return (((getFinishOnConfirm() ? 1 : 0) + 31) * 31) + Arrays.hashCode(getWords());
    }

    public String toString() {
        return "PaymentsRecoveryPhraseFragmentArgs{finishOnConfirm=" + getFinishOnConfirm() + ", words=" + getWords() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(PaymentsRecoveryPhraseFragmentArgs paymentsRecoveryPhraseFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(paymentsRecoveryPhraseFragmentArgs.arguments);
        }

        public Builder(boolean z) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("finish_on_confirm", Boolean.valueOf(z));
        }

        public PaymentsRecoveryPhraseFragmentArgs build() {
            return new PaymentsRecoveryPhraseFragmentArgs(this.arguments);
        }

        public Builder setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        public Builder setWords(String[] strArr) {
            this.arguments.put("words", strArr);
            return this;
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }

        public String[] getWords() {
            return (String[]) this.arguments.get("words");
        }
    }
}
