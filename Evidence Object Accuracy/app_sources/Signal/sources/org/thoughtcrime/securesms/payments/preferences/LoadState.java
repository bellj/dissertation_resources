package org.thoughtcrime.securesms.payments.preferences;

/* loaded from: classes4.dex */
public enum LoadState {
    INITIAL,
    LOADING,
    LOADED,
    ERROR
}
