package org.thoughtcrime.securesms.payments.preferences.viewholder;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter;
import org.thoughtcrime.securesms.payments.preferences.model.InfoCard;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class InfoCardViewHolder extends MappingViewHolder<InfoCard> {
    private final PaymentsHomeAdapter.Callbacks callbacks;
    private final ImageView icon;
    private final TextView learnMore;
    private final TextView message;
    private final Toolbar toolbar;

    public InfoCardViewHolder(View view, PaymentsHomeAdapter.Callbacks callbacks) {
        super(view);
        this.callbacks = callbacks;
        this.toolbar = (Toolbar) view.findViewById(R.id.payment_info_card_toolbar);
        this.message = (TextView) view.findViewById(R.id.payment_info_card_message);
        this.icon = (ImageView) view.findViewById(R.id.payment_info_card_icon);
        TextView textView = (TextView) view.findViewById(R.id.payment_info_card_learn_more);
        this.learnMore = textView;
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void bind(InfoCard infoCard) {
        this.toolbar.setTitle(infoCard.getTitleId());
        this.toolbar.getMenu().clear();
        this.toolbar.inflateMenu(R.menu.payment_info_card_overflow);
        this.toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener(infoCard) { // from class: org.thoughtcrime.securesms.payments.preferences.viewholder.InfoCardViewHolder$$ExternalSyntheticLambda0
            public final /* synthetic */ InfoCard f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.appcompat.widget.Toolbar.OnMenuItemClickListener
            public final boolean onMenuItemClick(MenuItem menuItem) {
                return InfoCardViewHolder.this.lambda$bind$2(this.f$1, menuItem);
            }
        });
        this.message.setText(infoCard.getMessageId());
        this.icon.setImageDrawable(AppCompatResources.getDrawable(getContext(), infoCard.getIconId()));
        SpannableString spannableString = new SpannableString(this.itemView.getContext().getString(infoCard.getActionId()));
        spannableString.setSpan(getSpan(infoCard.getType()), 0, spannableString.length(), 17);
        this.learnMore.setText(spannableString);
    }

    public /* synthetic */ boolean lambda$bind$2(InfoCard infoCard, MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.action_hide) {
            return false;
        }
        new AlertDialog.Builder(getContext()).setMessage(R.string.payment_info_card_hide_this_card).setPositiveButton(R.string.payment_info_card_hide, new DialogInterface.OnClickListener(infoCard) { // from class: org.thoughtcrime.securesms.payments.preferences.viewholder.InfoCardViewHolder$$ExternalSyntheticLambda3
            public final /* synthetic */ InfoCard f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                InfoCardViewHolder.this.lambda$bind$0(this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.viewholder.InfoCardViewHolder$$ExternalSyntheticLambda4
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
        return true;
    }

    public /* synthetic */ void lambda$bind$0(InfoCard infoCard, DialogInterface dialogInterface, int i) {
        infoCard.dismiss();
        dialogInterface.dismiss();
        this.callbacks.onInfoCardDismissed();
    }

    /* renamed from: org.thoughtcrime.securesms.payments.preferences.viewholder.InfoCardViewHolder$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$preferences$model$InfoCard$Type;

        static {
            int[] iArr = new int[InfoCard.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$preferences$model$InfoCard$Type = iArr;
            try {
                iArr[InfoCard.Type.RECORD_RECOVERY_PHASE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$model$InfoCard$Type[InfoCard.Type.UPDATE_YOUR_PIN.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$model$InfoCard$Type[InfoCard.Type.ABOUT_MOBILECOIN.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$model$InfoCard$Type[InfoCard.Type.ADDING_TO_YOUR_WALLET.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$model$InfoCard$Type[InfoCard.Type.CASHING_OUT.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    private ClickableSpan getSpan(InfoCard.Type type) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$preferences$model$InfoCard$Type[type.ordinal()];
        if (i == 1) {
            PaymentsHomeAdapter.Callbacks callbacks = this.callbacks;
            Objects.requireNonNull(callbacks);
            return new CallbackSpan(new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.viewholder.InfoCardViewHolder$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    PaymentsHomeAdapter.Callbacks.this.onViewRecoveryPhrase();
                }
            }, null);
        } else if (i == 2) {
            PaymentsHomeAdapter.Callbacks callbacks2 = this.callbacks;
            Objects.requireNonNull(callbacks2);
            return new CallbackSpan(new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.viewholder.InfoCardViewHolder$$ExternalSyntheticLambda2
                @Override // java.lang.Runnable
                public final void run() {
                    PaymentsHomeAdapter.Callbacks.this.onUpdatePin();
                }
            }, null);
        } else if (i == 3) {
            return new LearnMoreURLSpan(getContext().getString(R.string.payment_info_card__learn_more__about_mobilecoin));
        } else {
            if (i == 4) {
                return new LearnMoreURLSpan(getContext().getString(R.string.payment_info_card__learn_more__adding_to_your_wallet));
            }
            if (i == 5) {
                return new LearnMoreURLSpan(getContext().getString(R.string.payment_info_card__learn_more__cashing_out));
            }
            throw new IllegalArgumentException("Unexpected type " + type.name());
        }
    }

    /* loaded from: classes4.dex */
    public static final class CallbackSpan extends ClickableSpan {
        private final Runnable runnable;

        /* synthetic */ CallbackSpan(Runnable runnable, AnonymousClass1 r2) {
            this(runnable);
        }

        private CallbackSpan(Runnable runnable) {
            this.runnable = runnable;
        }

        @Override // android.text.style.ClickableSpan
        public void onClick(View view) {
            this.runnable.run();
        }

        @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
        public void updateDrawState(TextPaint textPaint) {
            super.updateDrawState(textPaint);
            textPaint.setUnderlineText(false);
        }
    }

    /* loaded from: classes4.dex */
    public static final class LearnMoreURLSpan extends URLSpan {
        public LearnMoreURLSpan(String str) {
            super(str);
        }

        @Override // android.text.style.CharacterStyle, android.text.style.ClickableSpan
        public void updateDrawState(TextPaint textPaint) {
            super.updateDrawState(textPaint);
            textPaint.setUnderlineText(false);
        }
    }
}
