package org.thoughtcrime.securesms.payments.preferences;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SetCurrencyViewModel$1$$ExternalSyntheticLambda0 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return SetCurrencyViewModel.AnonymousClass1.lambda$onError$1((SetCurrencyViewModel.SetCurrencyState) obj);
    }
}
