package org.thoughtcrime.securesms.payments.preferences;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class PaymentsAllActivityFragmentArgs {
    private final HashMap arguments;

    private PaymentsAllActivityFragmentArgs() {
        this.arguments = new HashMap();
    }

    private PaymentsAllActivityFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static PaymentsAllActivityFragmentArgs fromBundle(Bundle bundle) {
        PaymentsAllActivityFragmentArgs paymentsAllActivityFragmentArgs = new PaymentsAllActivityFragmentArgs();
        bundle.setClassLoader(PaymentsAllActivityFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("paymentType")) {
            throw new IllegalArgumentException("Required argument \"paymentType\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(PaymentType.class) || Serializable.class.isAssignableFrom(PaymentType.class)) {
            PaymentType paymentType = (PaymentType) bundle.get("paymentType");
            if (paymentType != null) {
                paymentsAllActivityFragmentArgs.arguments.put("paymentType", paymentType);
                return paymentsAllActivityFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"paymentType\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(PaymentType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public PaymentType getPaymentType() {
        return (PaymentType) this.arguments.get("paymentType");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("paymentType")) {
            PaymentType paymentType = (PaymentType) this.arguments.get("paymentType");
            if (Parcelable.class.isAssignableFrom(PaymentType.class) || paymentType == null) {
                bundle.putParcelable("paymentType", (Parcelable) Parcelable.class.cast(paymentType));
            } else if (Serializable.class.isAssignableFrom(PaymentType.class)) {
                bundle.putSerializable("paymentType", (Serializable) Serializable.class.cast(paymentType));
            } else {
                throw new UnsupportedOperationException(PaymentType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PaymentsAllActivityFragmentArgs paymentsAllActivityFragmentArgs = (PaymentsAllActivityFragmentArgs) obj;
        if (this.arguments.containsKey("paymentType") != paymentsAllActivityFragmentArgs.arguments.containsKey("paymentType")) {
            return false;
        }
        return getPaymentType() == null ? paymentsAllActivityFragmentArgs.getPaymentType() == null : getPaymentType().equals(paymentsAllActivityFragmentArgs.getPaymentType());
    }

    public int hashCode() {
        return 31 + (getPaymentType() != null ? getPaymentType().hashCode() : 0);
    }

    public String toString() {
        return "PaymentsAllActivityFragmentArgs{paymentType=" + getPaymentType() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(PaymentsAllActivityFragmentArgs paymentsAllActivityFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(paymentsAllActivityFragmentArgs.arguments);
        }

        public Builder(PaymentType paymentType) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (paymentType != null) {
                hashMap.put("paymentType", paymentType);
                return;
            }
            throw new IllegalArgumentException("Argument \"paymentType\" is marked as non-null but was passed a null value.");
        }

        public PaymentsAllActivityFragmentArgs build() {
            return new PaymentsAllActivityFragmentArgs(this.arguments);
        }

        public Builder setPaymentType(PaymentType paymentType) {
            if (paymentType != null) {
                this.arguments.put("paymentType", paymentType);
                return this;
            }
            throw new IllegalArgumentException("Argument \"paymentType\" is marked as non-null but was passed a null value.");
        }

        public PaymentType getPaymentType() {
            return (PaymentType) this.arguments.get("paymentType");
        }
    }
}
