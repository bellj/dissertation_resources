package org.thoughtcrime.securesms.payments;

import com.mobilecoin.lib.Mnemonics;
import com.mobilecoin.lib.exceptions.BadMnemonicException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public final class Mnemonic {
    public static final List<String> BIP39_WORDS_ENGLISH;
    private final String mnemonic;
    private final String[] words;

    static {
        try {
            BIP39_WORDS_ENGLISH = Collections.unmodifiableList(Arrays.asList(Mnemonics.wordsByPrefix("")));
        } catch (BadMnemonicException e) {
            throw new AssertionError(e);
        }
    }

    public Mnemonic(String str) {
        this.mnemonic = str;
        this.words = str.split(" ");
    }

    public List<String> getWords() {
        return Collections.unmodifiableList(Arrays.asList(this.words));
    }

    public int getWordCount() {
        return this.words.length;
    }

    public String getMnemonic() {
        return this.mnemonic;
    }
}
