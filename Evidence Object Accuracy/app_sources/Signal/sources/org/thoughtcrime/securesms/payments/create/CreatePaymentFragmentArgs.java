package org.thoughtcrime.securesms.payments.create;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;

/* loaded from: classes4.dex */
public class CreatePaymentFragmentArgs {
    private final HashMap arguments;

    private CreatePaymentFragmentArgs() {
        this.arguments = new HashMap();
    }

    private CreatePaymentFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static CreatePaymentFragmentArgs fromBundle(Bundle bundle) {
        CreatePaymentFragmentArgs createPaymentFragmentArgs = new CreatePaymentFragmentArgs();
        bundle.setClassLoader(CreatePaymentFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("payee")) {
            throw new IllegalArgumentException("Required argument \"payee\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(PayeeParcelable.class) || Serializable.class.isAssignableFrom(PayeeParcelable.class)) {
            PayeeParcelable payeeParcelable = (PayeeParcelable) bundle.get("payee");
            if (payeeParcelable != null) {
                createPaymentFragmentArgs.arguments.put("payee", payeeParcelable);
                if (bundle.containsKey("note")) {
                    createPaymentFragmentArgs.arguments.put("note", bundle.getString("note"));
                } else {
                    createPaymentFragmentArgs.arguments.put("note", null);
                }
                if (bundle.containsKey("finish_on_confirm")) {
                    createPaymentFragmentArgs.arguments.put("finish_on_confirm", Boolean.valueOf(bundle.getBoolean("finish_on_confirm")));
                } else {
                    createPaymentFragmentArgs.arguments.put("finish_on_confirm", Boolean.FALSE);
                }
                return createPaymentFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"payee\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(PayeeParcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public PayeeParcelable getPayee() {
        return (PayeeParcelable) this.arguments.get("payee");
    }

    public String getNote() {
        return (String) this.arguments.get("note");
    }

    public boolean getFinishOnConfirm() {
        return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("payee")) {
            PayeeParcelable payeeParcelable = (PayeeParcelable) this.arguments.get("payee");
            if (Parcelable.class.isAssignableFrom(PayeeParcelable.class) || payeeParcelable == null) {
                bundle.putParcelable("payee", (Parcelable) Parcelable.class.cast(payeeParcelable));
            } else if (Serializable.class.isAssignableFrom(PayeeParcelable.class)) {
                bundle.putSerializable("payee", (Serializable) Serializable.class.cast(payeeParcelable));
            } else {
                throw new UnsupportedOperationException(PayeeParcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        if (this.arguments.containsKey("note")) {
            bundle.putString("note", (String) this.arguments.get("note"));
        } else {
            bundle.putString("note", null);
        }
        if (this.arguments.containsKey("finish_on_confirm")) {
            bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
        } else {
            bundle.putBoolean("finish_on_confirm", false);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CreatePaymentFragmentArgs createPaymentFragmentArgs = (CreatePaymentFragmentArgs) obj;
        if (this.arguments.containsKey("payee") != createPaymentFragmentArgs.arguments.containsKey("payee")) {
            return false;
        }
        if (getPayee() == null ? createPaymentFragmentArgs.getPayee() != null : !getPayee().equals(createPaymentFragmentArgs.getPayee())) {
            return false;
        }
        if (this.arguments.containsKey("note") != createPaymentFragmentArgs.arguments.containsKey("note")) {
            return false;
        }
        if (getNote() == null ? createPaymentFragmentArgs.getNote() == null : getNote().equals(createPaymentFragmentArgs.getNote())) {
            return this.arguments.containsKey("finish_on_confirm") == createPaymentFragmentArgs.arguments.containsKey("finish_on_confirm") && getFinishOnConfirm() == createPaymentFragmentArgs.getFinishOnConfirm();
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((getPayee() != null ? getPayee().hashCode() : 0) + 31) * 31;
        if (getNote() != null) {
            i = getNote().hashCode();
        }
        return ((hashCode + i) * 31) + (getFinishOnConfirm() ? 1 : 0);
    }

    public String toString() {
        return "CreatePaymentFragmentArgs{payee=" + getPayee() + ", note=" + getNote() + ", finishOnConfirm=" + getFinishOnConfirm() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(CreatePaymentFragmentArgs createPaymentFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(createPaymentFragmentArgs.arguments);
        }

        public Builder(PayeeParcelable payeeParcelable) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (payeeParcelable != null) {
                hashMap.put("payee", payeeParcelable);
                return;
            }
            throw new IllegalArgumentException("Argument \"payee\" is marked as non-null but was passed a null value.");
        }

        public CreatePaymentFragmentArgs build() {
            return new CreatePaymentFragmentArgs(this.arguments);
        }

        public Builder setPayee(PayeeParcelable payeeParcelable) {
            if (payeeParcelable != null) {
                this.arguments.put("payee", payeeParcelable);
                return this;
            }
            throw new IllegalArgumentException("Argument \"payee\" is marked as non-null but was passed a null value.");
        }

        public Builder setNote(String str) {
            this.arguments.put("note", str);
            return this;
        }

        public Builder setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        public PayeeParcelable getPayee() {
            return (PayeeParcelable) this.arguments.get("payee");
        }

        public String getNote() {
            return (String) this.arguments.get("note");
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }
    }
}
