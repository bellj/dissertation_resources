package org.thoughtcrime.securesms.payments.create;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.transition.TransitionManager;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.payments.FiatMoneyUtil;
import org.thoughtcrime.securesms.payments.MoneyView;
import org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel;
import org.thoughtcrime.securesms.payments.preferences.RecipientHasNotEnabledPaymentsDialog;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.whispersystems.signalservice.api.payments.FormatterOptions;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class CreatePaymentFragment extends LoggingFragment {
    private static final Map<Integer, AmountKeyboardGlyph> ID_TO_GLYPH = new HashMap<Integer, AmountKeyboardGlyph>() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment.1
        {
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_decimal), AmountKeyboardGlyph.DECIMAL);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_lt), AmountKeyboardGlyph.BACK);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_0), AmountKeyboardGlyph.ZERO);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_1), AmountKeyboardGlyph.ONE);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_2), AmountKeyboardGlyph.TWO);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_3), AmountKeyboardGlyph.THREE);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_4), AmountKeyboardGlyph.FOUR);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_5), AmountKeyboardGlyph.FIVE);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_6), AmountKeyboardGlyph.SIX);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_7), AmountKeyboardGlyph.SEVEN);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_8), AmountKeyboardGlyph.EIGHT);
            put(Integer.valueOf((int) R.id.create_payment_fragment_keyboard_9), AmountKeyboardGlyph.NINE);
        }
    };
    private View addNote;
    private MoneyView amount;
    private TextView balance;
    private ConstraintLayout constraintLayout;
    private ConstraintSet cryptoConstraintSet;
    private TextView exchange;
    private ConstraintSet fiatConstraintSet;
    private Drawable infoIcon;
    private EmojiTextView note;
    private View pay;
    private View request;
    private Drawable spacer;
    private View toggle;

    public CreatePaymentFragment() {
        super(R.layout.create_payment_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ((Toolbar) view.findViewById(R.id.create_payment_fragment_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CreatePaymentFragment.$r8$lambda$8b7MGYsUJntje75lhcpTxyybZS8(CreatePaymentFragment.this, view2);
            }
        });
        CreatePaymentFragmentArgs fromBundle = CreatePaymentFragmentArgs.fromBundle(requireArguments());
        CreatePaymentViewModel createPaymentViewModel = (CreatePaymentViewModel) new ViewModelProvider(Navigation.findNavController(view).getViewModelStoreOwner(R.id.payments_create), new CreatePaymentViewModel.Factory(fromBundle.getPayee(), fromBundle.getNote())).get(CreatePaymentViewModel.class);
        this.constraintLayout = (ConstraintLayout) view.findViewById(R.id.create_payment_fragment_amount_header);
        this.request = view.findViewById(R.id.create_payment_fragment_request);
        this.amount = (MoneyView) view.findViewById(R.id.create_payment_fragment_amount);
        this.exchange = (TextView) view.findViewById(R.id.create_payment_fragment_exchange);
        this.pay = view.findViewById(R.id.create_payment_fragment_pay);
        this.balance = (TextView) view.findViewById(R.id.create_payment_fragment_balance);
        this.note = (EmojiTextView) view.findViewById(R.id.create_payment_fragment_note);
        this.addNote = view.findViewById(R.id.create_payment_fragment_add_note);
        this.toggle = view.findViewById(R.id.create_payment_fragment_toggle);
        ((TextView) view.findViewById(R.id.create_payment_fragment_keyboard_decimal)).setText(String.valueOf(DecimalFormatSymbols.getInstance().getDecimalSeparator()));
        view.findViewById(R.id.create_payment_fragment_info_tap_region).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CreatePaymentFragment.$r8$lambda$d19X1AWVywqgaZvYR6y1ozE6F8w(CreatePaymentFragment.this, view2);
            }
        });
        initializeInfoIcon();
        this.note.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CreatePaymentFragment.$r8$lambda$9B6fvLr9SVhEF5_EZiOGu4eXx8Q(view2);
            }
        });
        this.addNote.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CreatePaymentFragment.m2382$r8$lambda$37MQix5NHug4prE3vO9rJPWSvQ(view2);
            }
        });
        this.pay.setOnClickListener(new View.OnClickListener(fromBundle) { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda6
            public final /* synthetic */ CreatePaymentFragmentArgs f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CreatePaymentFragment.$r8$lambda$VaJ2j2YgOJ1C7LcjNmY5KgMF2tU(CreatePaymentViewModel.this, this.f$1, view2);
            }
        });
        this.toggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CreatePaymentFragment.$r8$lambda$A9NNHG7_522fGc0S3gwIcYBd7T8(CreatePaymentViewModel.this, view2);
            }
        });
        initializeConstraintSets();
        initializeKeyboardButtons(view, createPaymentViewModel);
        createPaymentViewModel.getInputState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda8
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CreatePaymentFragment.$r8$lambda$ekteubeKpRUnIP356wwIAcKSQpU(CreatePaymentFragment.this, (InputState) obj);
            }
        });
        createPaymentViewModel.getIsPaymentsSupportedByPayee().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda9
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CreatePaymentFragment.$r8$lambda$1utaOgo5KYT2DEMusn6Jif4yliU(CreatePaymentFragment.this, (Boolean) obj);
            }
        });
        createPaymentViewModel.isValidAmount().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda10
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CreatePaymentFragment.$r8$lambda$1Ai5kHit2COuDS2unSLvfFfQB8U(CreatePaymentFragment.this, ((Boolean) obj).booleanValue());
            }
        });
        createPaymentViewModel.getNote().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda11
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CreatePaymentFragment.$r8$lambda$y74nfzyCru3KiUATO6P3SWFbtKY(CreatePaymentFragment.this, (CharSequence) obj);
            }
        });
        createPaymentViewModel.getSpendableBalance().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CreatePaymentFragment.$r8$lambda$gmt7vC26i6mEL7wHRIRc3abd0qw(CreatePaymentFragment.this, (Money) obj);
            }
        });
        createPaymentViewModel.getCanSendPayment().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CreatePaymentFragment.$r8$lambda$flMIcif6Fl2RQQ3kDRPPjNfOXmk(CreatePaymentFragment.this, ((Boolean) obj).booleanValue());
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        new MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.CreatePaymentFragment__conversions_are_just_estimates).setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda12
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                CreatePaymentFragment.$r8$lambda$CjAvpDuVaYFG7cOEvaoKnHGSFW4(dialogInterface, i);
            }
        }).setNegativeButton(R.string.LearnMoreTextView_learn_more, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda13
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                CreatePaymentFragment.m2384$r8$lambda$aZdLcosU3lqDWSNzJEUfCWTZnE(CreatePaymentFragment.this, dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onViewCreated$1(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        CommunicationActions.openBrowserLink(requireContext(), getString(R.string.CreatePaymentFragment__learn_more__conversions));
    }

    public static /* synthetic */ void lambda$onViewCreated$3(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), (int) R.id.action_createPaymentFragment_to_editPaymentNoteFragment);
    }

    public static /* synthetic */ void lambda$onViewCreated$4(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), (int) R.id.action_createPaymentFragment_to_editPaymentNoteFragment);
    }

    public static /* synthetic */ void lambda$onViewCreated$5(CreatePaymentViewModel createPaymentViewModel, CreatePaymentFragmentArgs createPaymentFragmentArgs, View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), CreatePaymentFragmentDirections.actionCreatePaymentFragmentToConfirmPaymentFragment(createPaymentViewModel.getCreatePaymentDetails()).setFinishOnConfirm(createPaymentFragmentArgs.getFinishOnConfirm()));
    }

    public /* synthetic */ void lambda$onViewCreated$7(InputState inputState) {
        updateAmount(inputState);
        updateExchange(inputState);
        updateMoneyInputTarget(inputState.getInputTarget());
    }

    public /* synthetic */ void lambda$onViewCreated$8() {
        goBack(requireView());
    }

    public /* synthetic */ void lambda$onViewCreated$9(Boolean bool) {
        if (!bool.booleanValue()) {
            RecipientHasNotEnabledPaymentsDialog.show(requireContext(), new Runnable() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda14
                @Override // java.lang.Runnable
                public final void run() {
                    CreatePaymentFragment.$r8$lambda$boRt2sFRip4vnP4svFXzsTx9Uf0(CreatePaymentFragment.this);
                }
            });
        }
    }

    public void goBack(View view) {
        if (!Navigation.findNavController(view).popBackStack()) {
            requireActivity().finish();
        }
    }

    private void initializeInfoIcon() {
        Drawable drawable = AppCompatResources.getDrawable(requireContext(), R.drawable.payment_info_pad);
        Objects.requireNonNull(drawable);
        this.spacer = drawable;
        Drawable drawable2 = AppCompatResources.getDrawable(requireContext(), R.drawable.ic_update_info_16);
        Objects.requireNonNull(drawable2);
        this.infoIcon = drawable2;
        DrawableCompat.setTint(drawable2, this.exchange.getCurrentTextColor());
        this.spacer.setBounds(0, 0, ViewUtil.dpToPx(8), ViewUtil.dpToPx(16));
        this.infoIcon.setBounds(0, 0, ViewUtil.dpToPx(16), ViewUtil.dpToPx(16));
    }

    public void updateNote(CharSequence charSequence) {
        boolean z = !TextUtils.isEmpty(charSequence);
        int i = 8;
        this.addNote.setVisibility(z ? 8 : 0);
        EmojiTextView emojiTextView = this.note;
        if (z) {
            i = 0;
        }
        emojiTextView.setVisibility(i);
        this.note.setText(charSequence);
    }

    private void initializeKeyboardButtons(View view, CreatePaymentViewModel createPaymentViewModel) {
        for (Map.Entry<Integer, AmountKeyboardGlyph> entry : ID_TO_GLYPH.entrySet()) {
            view.findViewById(entry.getKey().intValue()).setOnClickListener(new View.OnClickListener(createPaymentViewModel, entry) { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda15
                public final /* synthetic */ CreatePaymentViewModel f$1;
                public final /* synthetic */ Map.Entry f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    CreatePaymentFragment.m2383$r8$lambda$PgtYrFTAztbkAruR_j0cmpos6Y(CreatePaymentFragment.this, this.f$1, this.f$2, view2);
                }
            });
        }
        view.findViewById(R.id.create_payment_fragment_keyboard_lt).setOnLongClickListener(new View.OnLongClickListener() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$$ExternalSyntheticLambda16
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view2) {
                return CreatePaymentFragment.m2385$r8$lambda$iOp5KA5b5SPk5_UzHgpywvkYM(CreatePaymentViewModel.this, view2);
            }
        });
    }

    public /* synthetic */ void lambda$initializeKeyboardButtons$10(CreatePaymentViewModel createPaymentViewModel, Map.Entry entry, View view) {
        createPaymentViewModel.updateAmount(requireContext(), (AmountKeyboardGlyph) entry.getValue());
    }

    /* renamed from: org.thoughtcrime.securesms.payments.create.CreatePaymentFragment$2 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$create$InputTarget;

        static {
            int[] iArr = new int[InputTarget.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$create$InputTarget = iArr;
            try {
                iArr[InputTarget.MONEY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$create$InputTarget[InputTarget.FIAT_MONEY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    private void updateAmount(InputState inputState) {
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$payments$create$InputTarget[inputState.getInputTarget().ordinal()];
        if (i == 1) {
            this.amount.setMoney(inputState.getMoneyAmount(), inputState.getMoney().getCurrency());
        } else if (i == 2) {
            this.amount.setMoney(inputState.getMoney(), false, inputState.getExchangeRate().get().getTimestamp());
            this.amount.append(SpanUtil.buildImageSpan(this.spacer));
            this.amount.append(SpanUtil.buildImageSpan(this.infoIcon));
        }
    }

    private void updateExchange(InputState inputState) {
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$payments$create$InputTarget[inputState.getInputTarget().ordinal()];
        if (i != 1) {
            if (i == 2) {
                this.exchange.setText(FiatMoneyUtil.manualFormat(inputState.getFiatMoney().get().getCurrency(), inputState.getFiatAmount()));
            }
        } else if (inputState.getFiatMoney().isPresent()) {
            this.exchange.setVisibility(0);
            this.exchange.setText(FiatMoneyUtil.format(getResources(), inputState.getFiatMoney().get(), FiatMoneyUtil.formatOptions().withDisplayTime(true)));
            this.exchange.append(SpanUtil.buildImageSpan(this.spacer));
            this.exchange.append(SpanUtil.buildImageSpan(this.infoIcon));
            this.toggle.setVisibility(0);
            this.toggle.setEnabled(true);
        } else {
            this.exchange.setVisibility(4);
            this.toggle.setVisibility(4);
            this.toggle.setEnabled(false);
        }
    }

    public void updateRequestAmountButtons(boolean z) {
        this.request.setEnabled(z);
    }

    public void updatePayAmountButtons(boolean z) {
        this.pay.setEnabled(z);
    }

    public void updateBalance(Money money) {
        this.balance.setText(getString(R.string.CreatePaymentFragment__available_balance_s, money.toString(FormatterOptions.defaults())));
    }

    private void initializeConstraintSets() {
        ConstraintSet constraintSet = new ConstraintSet();
        this.cryptoConstraintSet = constraintSet;
        constraintSet.clone(this.constraintLayout);
        ConstraintSet constraintSet2 = new ConstraintSet();
        this.fiatConstraintSet = constraintSet2;
        constraintSet2.clone(getContext(), R.layout.create_payment_fragment_amount_toggle);
    }

    private void updateMoneyInputTarget(InputTarget inputTarget) {
        TransitionManager.endTransitions(this.constraintLayout);
        TransitionManager.beginDelayedTransition(this.constraintLayout);
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$payments$create$InputTarget[inputTarget.ordinal()];
        if (i == 1) {
            this.cryptoConstraintSet.applyTo(this.constraintLayout);
            this.exchange.setTextColor(ContextCompat.getColor(requireContext(), R.color.signal_text_secondary));
            this.amount.setTextColor(ContextCompat.getColor(requireContext(), R.color.signal_text_primary));
        } else if (i == 2) {
            this.fiatConstraintSet.applyTo(this.constraintLayout);
            this.amount.setTextColor(ContextCompat.getColor(requireContext(), R.color.signal_text_secondary));
            this.exchange.setTextColor(ContextCompat.getColor(requireContext(), R.color.signal_text_primary));
        }
    }
}
