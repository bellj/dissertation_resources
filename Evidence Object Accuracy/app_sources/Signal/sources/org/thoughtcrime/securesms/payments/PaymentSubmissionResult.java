package org.thoughtcrime.securesms.payments;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.List;
import org.thoughtcrime.securesms.payments.TransactionSubmissionResult;

/* loaded from: classes4.dex */
public final class PaymentSubmissionResult {
    private final List<TransactionSubmissionResult> defrags;
    private final TransactionSubmissionResult erroredTransaction;
    private final TransactionSubmissionResult nonDefrag;

    public PaymentSubmissionResult(List<TransactionSubmissionResult> list) {
        if (!list.isEmpty()) {
            this.defrags = Stream.of(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.payments.PaymentSubmissionResult$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return ((TransactionSubmissionResult) obj).isDefrag();
                }
            }).toList();
            this.nonDefrag = (TransactionSubmissionResult) Stream.of(list).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.payments.PaymentSubmissionResult$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return ((TransactionSubmissionResult) obj).isDefrag();
                }
            }).findSingle().orElse(null);
            this.erroredTransaction = (TransactionSubmissionResult) Stream.of(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.payments.PaymentSubmissionResult$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return PaymentSubmissionResult.lambda$new$0((TransactionSubmissionResult) obj);
                }
            }).findSingle().orElse(null);
            return;
        }
        throw new IllegalStateException();
    }

    public static /* synthetic */ boolean lambda$new$0(TransactionSubmissionResult transactionSubmissionResult) {
        return transactionSubmissionResult.getErrorCode() != TransactionSubmissionResult.ErrorCode.NONE;
    }

    public List<TransactionSubmissionResult> defrags() {
        return this.defrags;
    }

    public boolean containsDefrags() {
        return this.defrags.size() > 0;
    }

    public TransactionSubmissionResult getNonDefrag() {
        return this.nonDefrag;
    }

    public TransactionSubmissionResult.ErrorCode getErrorCode() {
        TransactionSubmissionResult transactionSubmissionResult = this.erroredTransaction;
        if (transactionSubmissionResult != null) {
            return transactionSubmissionResult.getErrorCode();
        }
        return TransactionSubmissionResult.ErrorCode.NONE;
    }
}
