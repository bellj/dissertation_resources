package org.thoughtcrime.securesms.payments.preferences.viewholder;

import android.view.View;
import org.thoughtcrime.securesms.payments.preferences.model.InProgress;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class InProgressViewHolder extends MappingViewHolder<InProgress> {
    public void bind(InProgress inProgress) {
    }

    public InProgressViewHolder(View view) {
        super(view);
    }
}
