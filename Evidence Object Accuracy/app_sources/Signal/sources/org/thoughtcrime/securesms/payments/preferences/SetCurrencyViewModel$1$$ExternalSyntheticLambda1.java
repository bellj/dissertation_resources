package org.thoughtcrime.securesms.payments.preferences;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.payments.currency.CurrencyExchange;
import org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SetCurrencyViewModel$1$$ExternalSyntheticLambda1 implements Function {
    public final /* synthetic */ CurrencyExchange f$0;

    public /* synthetic */ SetCurrencyViewModel$1$$ExternalSyntheticLambda1(CurrencyExchange currencyExchange) {
        this.f$0 = currencyExchange;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return SetCurrencyViewModel.AnonymousClass1.lambda$onComplete$0(this.f$0, (SetCurrencyViewModel.SetCurrencyState) obj);
    }
}
