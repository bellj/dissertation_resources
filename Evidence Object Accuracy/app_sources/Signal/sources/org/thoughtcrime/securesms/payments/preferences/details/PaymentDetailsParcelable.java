package org.thoughtcrime.securesms.payments.preferences.details;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;
import java.util.UUID;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.PaymentParcelable;

/* loaded from: classes4.dex */
public class PaymentDetailsParcelable implements Parcelable {
    public static final Parcelable.Creator<PaymentDetailsParcelable> CREATOR = new Parcelable.Creator<PaymentDetailsParcelable>() { // from class: org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsParcelable.1
        @Override // android.os.Parcelable.Creator
        public PaymentDetailsParcelable createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            if (readInt == 0) {
                return PaymentDetailsParcelable.forPayment(((PaymentParcelable) parcel.readParcelable(PaymentParcelable.class.getClassLoader())).getPayment());
            }
            if (readInt == 1) {
                return PaymentDetailsParcelable.forUuid(UUID.fromString(parcel.readString()));
            }
            throw new IllegalStateException("Unexpected parcel type " + readInt);
        }

        @Override // android.os.Parcelable.Creator
        public PaymentDetailsParcelable[] newArray(int i) {
            return new PaymentDetailsParcelable[i];
        }
    };
    private static final int TYPE_PAYMENT;
    private static final int TYPE_UUID;
    private final Payment payment;
    private final UUID uuid;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    private PaymentDetailsParcelable(Payment payment, UUID uuid) {
        if ((uuid == null) != (payment != null ? false : true)) {
            this.payment = payment;
            this.uuid = uuid;
            return;
        }
        throw new IllegalStateException("Must have exactly one of uuid or payment.");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        if (this.payment != null) {
            parcel.writeInt(0);
            parcel.writeParcelable(new PaymentParcelable(this.payment), i);
            return;
        }
        parcel.writeInt(1);
        parcel.writeString(this.uuid.toString());
    }

    public boolean hasPayment() {
        return this.payment != null;
    }

    public Payment requirePayment() {
        Payment payment = this.payment;
        Objects.requireNonNull(payment);
        return payment;
    }

    public UUID requireUuid() {
        UUID uuid = this.uuid;
        if (uuid != null) {
            return uuid;
        }
        return requirePayment().getUuid();
    }

    public static PaymentDetailsParcelable forUuid(UUID uuid) {
        return new PaymentDetailsParcelable(null, uuid);
    }

    public static PaymentDetailsParcelable forPayment(Payment payment) {
        return new PaymentDetailsParcelable(payment, null);
    }
}
