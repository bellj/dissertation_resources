package org.thoughtcrime.securesms.payments;

import android.content.res.Resources;
import androidx.lifecycle.LiveData;
import j$.util.Optional;
import j$.util.function.Function;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class FiatMoneyUtil {
    private static final char CURRENCY_SYMBOL_PLACE_HOLDER;
    private static final char NON_BREAKING_WHITESPACE;
    private static final String TAG = Log.tag(FiatMoneyUtil.class);

    private FiatMoneyUtil() {
    }

    public static LiveData<Optional<FiatMoney>> getExchange(LiveData<Money> liveData) {
        return LiveDataUtil.mapAsync(liveData, new Function() { // from class: org.thoughtcrime.securesms.payments.FiatMoneyUtil$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return FiatMoneyUtil.lambda$getExchange$0((Money) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public static /* synthetic */ Optional lambda$getExchange$0(Money money) {
        try {
            return ApplicationDependencies.getPayments().getCurrencyExchange(false).getExchangeRate(SignalStore.paymentsValues().currentCurrency()).exchange(money);
        } catch (IOException e) {
            Log.w(TAG, e);
            return Optional.empty();
        }
    }

    public static String format(Resources resources, FiatMoney fiatMoney) {
        return format(resources, fiatMoney, new FormatOptions());
    }

    public static String format(Resources resources, FiatMoney fiatMoney, FormatOptions formatOptions) {
        NumberFormat numberFormat;
        if (formatOptions.withSymbol) {
            numberFormat = NumberFormat.getCurrencyInstance();
            numberFormat.setCurrency(fiatMoney.getCurrency());
        } else {
            numberFormat = NumberFormat.getNumberInstance();
            numberFormat.setMinimumFractionDigits(fiatMoney.getCurrency().getDefaultFractionDigits());
        }
        if (formatOptions.trimZerosAfterDecimal) {
            numberFormat.setMinimumFractionDigits(0);
            numberFormat.setMaximumFractionDigits(fiatMoney.getCurrency().getDefaultFractionDigits());
        }
        String format = numberFormat.format(fiatMoney.getAmount());
        return (fiatMoney.getTimestamp() <= 0 || !formatOptions.displayTime) ? format : resources.getString(R.string.CurrencyAmountFormatter_s_at_s, format, DateUtils.getTimeString(ApplicationDependencies.getApplication(), Locale.getDefault(), fiatMoney.getTimestamp()));
    }

    public static String manualFormat(Currency currency, String str) {
        NumberFormat currencyInstance = NumberFormat.getCurrencyInstance();
        currencyInstance.setCurrency(currency);
        DecimalFormat decimalFormat = (DecimalFormat) currencyInstance;
        String currencySymbol = decimalFormat.getDecimalFormatSymbols().getCurrencySymbol();
        String localizedPattern = decimalFormat.toLocalizedPattern();
        int indexOf = localizedPattern.indexOf(164);
        boolean z = indexOf <= 0;
        if (indexOf == 0) {
            char charAt = localizedPattern.charAt(indexOf + 1);
            if (Character.isWhitespace(charAt) || charAt == 160) {
                currencySymbol = currencySymbol + charAt;
            }
        } else if (indexOf > 0) {
            char charAt2 = localizedPattern.charAt(indexOf - 1);
            if (Character.isWhitespace(charAt2) || charAt2 == 160) {
                currencySymbol = charAt2 + currencySymbol;
            }
        }
        if (z) {
            return currencySymbol + str;
        }
        return str + currencySymbol;
    }

    public static FormatOptions formatOptions() {
        return new FormatOptions();
    }

    /* loaded from: classes4.dex */
    public static class FormatOptions {
        private boolean displayTime;
        private boolean trimZerosAfterDecimal;
        private boolean withSymbol;

        private FormatOptions() {
            this.displayTime = true;
            this.withSymbol = true;
            this.trimZerosAfterDecimal = false;
        }

        public FormatOptions withDisplayTime(boolean z) {
            this.displayTime = z;
            return this;
        }

        public FormatOptions numberOnly() {
            this.withSymbol = false;
            return this;
        }

        public FormatOptions trimZerosAfterDecimal() {
            this.trimZerosAfterDecimal = true;
            return this;
        }
    }
}
