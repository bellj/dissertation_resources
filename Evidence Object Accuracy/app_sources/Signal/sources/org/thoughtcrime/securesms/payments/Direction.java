package org.thoughtcrime.securesms.payments;

/* loaded from: classes4.dex */
public enum Direction {
    SENT(0),
    RECEIVED(1);
    
    private final int value;

    Direction(int i) {
        this.value = i;
    }

    public int serialize() {
        return this.value;
    }

    public static Direction deserialize(int i) {
        Direction direction = SENT;
        if (i == direction.value) {
            return direction;
        }
        Direction direction2 = RECEIVED;
        if (i == direction2.value) {
            return direction2;
        }
        throw new AssertionError("" + i);
    }

    public boolean isReceived() {
        return this == RECEIVED;
    }

    public boolean isSent() {
        return this == SENT;
    }
}
