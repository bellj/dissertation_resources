package org.thoughtcrime.securesms.payments.preferences.addmoney;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import java.util.Objects;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.qr.QrView;
import org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyRepository;
import org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyViewModel;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;

/* loaded from: classes4.dex */
public final class PaymentsAddMoneyFragment extends LoggingFragment {
    public PaymentsAddMoneyFragment() {
        super(R.layout.payments_add_money_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        PaymentsAddMoneyViewModel paymentsAddMoneyViewModel = (PaymentsAddMoneyViewModel) ViewModelProviders.of(this, new PaymentsAddMoneyViewModel.Factory()).get(PaymentsAddMoneyViewModel.class);
        QrView qrView = (QrView) view.findViewById(R.id.payments_add_money_qr_image);
        TextView textView = (TextView) view.findViewById(R.id.payments_add_money_abbreviated_wallet_address);
        View findViewById = view.findViewById(R.id.payments_add_money_copy_address_button);
        LearnMoreTextView learnMoreTextView = (LearnMoreTextView) view.findViewById(R.id.payments_add_money_info);
        learnMoreTextView.setLearnMoreVisible(true);
        learnMoreTextView.setLink(getString(R.string.PaymentsAddMoneyFragment__learn_more__information));
        ((Toolbar) view.findViewById(R.id.payments_add_money_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsAddMoneyFragment.m2424$r8$lambda$td1z9yAsFQa58v3xuJdTnXQSQ8(view2);
            }
        });
        LiveData<CharSequence> selfAddressAbbreviated = paymentsAddMoneyViewModel.getSelfAddressAbbreviated();
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Objects.requireNonNull(textView);
        selfAddressAbbreviated.observe(viewLifecycleOwner, new Observer(textView) { // from class: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ TextView f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                this.f$0.setText((CharSequence) obj);
            }
        });
        paymentsAddMoneyViewModel.getSelfAddressB58().observe(getViewLifecycleOwner(), new Observer(findViewById) { // from class: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsAddMoneyFragment.m2422$r8$lambda$SNDk_3ltIBIwZskkEHhGc4Z2E(PaymentsAddMoneyFragment.this, this.f$1, (String) obj);
            }
        });
        LiveData<String> selfAddressB58 = paymentsAddMoneyViewModel.getSelfAddressB58();
        LifecycleOwner viewLifecycleOwner2 = getViewLifecycleOwner();
        Objects.requireNonNull(qrView);
        selfAddressB58.observe(viewLifecycleOwner2, new Observer() { // from class: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                QrView.this.setQrText((String) obj);
            }
        });
        paymentsAddMoneyViewModel.getErrors().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyFragment$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsAddMoneyFragment.m2423$r8$lambda$qyKTmarNDTsb49J0b18zMkQnM8((PaymentsAddMoneyRepository.Error) obj);
            }
        });
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    public /* synthetic */ void lambda$onViewCreated$1(String str, View view) {
        copyAddressToClipboard(str);
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view, String str) {
        view.setOnClickListener(new View.OnClickListener(str) { // from class: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyFragment$$ExternalSyntheticLambda5
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsAddMoneyFragment.$r8$lambda$fa01b1OflfMWi5v5vSVfZsPyYgk(PaymentsAddMoneyFragment.this, this.f$1, view2);
            }
        });
    }

    /* renamed from: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$preferences$addmoney$PaymentsAddMoneyRepository$Error;

        static {
            int[] iArr = new int[PaymentsAddMoneyRepository.Error.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$preferences$addmoney$PaymentsAddMoneyRepository$Error = iArr;
            try {
                iArr[PaymentsAddMoneyRepository.Error.PAYMENTS_NOT_ENABLED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public static /* synthetic */ void lambda$onViewCreated$3(PaymentsAddMoneyRepository.Error error) {
        if (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$preferences$addmoney$PaymentsAddMoneyRepository$Error[error.ordinal()] != 1) {
            throw new AssertionError();
        }
        throw new AssertionError("Payments are not enabled");
    }

    private void copyAddressToClipboard(String str) {
        Context requireContext = requireContext();
        ((ClipboardManager) requireContext.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(requireContext.getString(R.string.app_name), str));
        Toast.makeText(requireContext, (int) R.string.PaymentsAddMoneyFragment__copied_to_clipboard, 0).show();
    }
}
