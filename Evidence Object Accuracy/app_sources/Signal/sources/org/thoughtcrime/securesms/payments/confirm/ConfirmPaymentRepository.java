package org.thoughtcrime.securesms.payments.confirm;

import androidx.core.util.Consumer;
import java.io.IOException;
import java.util.UUID;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobs.PaymentSendJob;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddress;
import org.thoughtcrime.securesms.payments.Payee;
import org.thoughtcrime.securesms.payments.PaymentsAddressException;
import org.thoughtcrime.securesms.payments.Wallet;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class ConfirmPaymentRepository {
    private static final String TAG = Log.tag(ConfirmPaymentRepository.class);
    private final Wallet wallet;

    public ConfirmPaymentRepository(Wallet wallet) {
        this.wallet = wallet;
    }

    public void confirmPayment(ConfirmPaymentState confirmPaymentState, Consumer<ConfirmPaymentResult> consumer) {
        Log.i(TAG, "confirmPayment");
        SignalExecutors.BOUNDED.execute(new Runnable(confirmPaymentState, consumer) { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ ConfirmPaymentState f$1;
            public final /* synthetic */ Consumer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConfirmPaymentRepository.$r8$lambda$atg40zFkYLTxhZ93g7KK3TSfrGs(ConfirmPaymentRepository.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$confirmPayment$0(ConfirmPaymentState confirmPaymentState, Consumer consumer) {
        RecipientId recipientId;
        MobileCoinPublicAddress mobileCoinPublicAddress;
        if (confirmPaymentState.getTotal().requireMobileCoin().greaterThan(this.wallet.getCachedBalance().getFullAmount().requireMobileCoin())) {
            Log.w(TAG, "The total was greater than the wallet's balance");
            consumer.accept(new ConfirmPaymentResult.Error());
            return;
        }
        Payee payee = confirmPaymentState.getPayee();
        if (payee.hasRecipientId()) {
            recipientId = payee.requireRecipientId();
            try {
                mobileCoinPublicAddress = ProfileUtil.getAddressForRecipient(Recipient.resolved(recipientId));
            } catch (IOException unused) {
                Log.w(TAG, "Failed to get address for recipient " + recipientId);
                consumer.accept(new ConfirmPaymentResult.Error());
                return;
            } catch (PaymentsAddressException e) {
                Log.w(TAG, "Failed to get address for recipient " + recipientId);
                consumer.accept(new ConfirmPaymentResult.Error(e.getCode()));
                return;
            }
        } else if (payee.hasPublicAddress()) {
            mobileCoinPublicAddress = payee.requirePublicAddress();
            recipientId = null;
        } else {
            throw new AssertionError();
        }
        UUID enqueuePayment = PaymentSendJob.enqueuePayment(recipientId, mobileCoinPublicAddress, Util.emptyIfNull(confirmPaymentState.getNote()), confirmPaymentState.getAmount().requireMobileCoin(), confirmPaymentState.getFee().requireMobileCoin());
        Log.i(TAG, "confirmPayment: PaymentSendJob enqueued");
        consumer.accept(new ConfirmPaymentResult.Success(enqueuePayment));
    }

    public GetFeeResult getFee(Money money) {
        try {
            return new GetFeeResult.Success(this.wallet.getFee(money.requireMobileCoin()));
        } catch (IOException unused) {
            return new GetFeeResult.Error();
        }
    }

    /* loaded from: classes4.dex */
    public static class ConfirmPaymentResult {
        ConfirmPaymentResult() {
        }

        /* loaded from: classes4.dex */
        public static class Success extends ConfirmPaymentResult {
            private final UUID paymentId;

            Success(UUID uuid) {
                this.paymentId = uuid;
            }

            public UUID getPaymentId() {
                return this.paymentId;
            }
        }

        /* loaded from: classes4.dex */
        public static class Error extends ConfirmPaymentResult {
            private final PaymentsAddressException.Code code;

            Error() {
                this(null);
            }

            Error(PaymentsAddressException.Code code) {
                this.code = code;
            }

            public PaymentsAddressException.Code getCode() {
                return this.code;
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class GetFeeResult {
        GetFeeResult() {
        }

        /* loaded from: classes4.dex */
        public static class Success extends GetFeeResult {
            private final Money fee;

            Success(Money money) {
                this.fee = money;
            }

            public Money getFee() {
                return this.fee;
            }
        }

        /* loaded from: classes4.dex */
        public static class Error extends GetFeeResult {
            Error() {
            }
        }
    }
}
