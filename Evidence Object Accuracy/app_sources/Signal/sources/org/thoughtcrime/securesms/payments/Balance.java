package org.thoughtcrime.securesms.payments;

import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class Balance {
    private final long checkedAt;
    private final Money fullAmount;
    private final Money transferableAmount;

    public Balance(Money money, Money money2, long j) {
        this.fullAmount = money;
        this.transferableAmount = money2;
        this.checkedAt = j;
    }

    public Money getFullAmount() {
        return this.fullAmount;
    }

    public Money getTransferableAmount() {
        return this.transferableAmount;
    }

    public long getCheckedAt() {
        return this.checkedAt;
    }

    public String toString() {
        return "Balance{fullAmount=" + this.fullAmount + ", transferableAmount=" + this.transferableAmount + ", checkedAt=" + this.checkedAt + '}';
    }
}
