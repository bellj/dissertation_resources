package org.thoughtcrime.securesms.payments.preferences.transfer;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddress;

/* loaded from: classes4.dex */
public final class PaymentsTransferViewModel extends ViewModel {
    private final MutableLiveData<String> address = new MutableLiveData<>();
    private final MobileCoinPublicAddress ownAddress = ApplicationDependencies.getPayments().getWallet().getMobileCoinPublicAddress();

    PaymentsTransferViewModel() {
    }

    public LiveData<String> getAddress() {
        return this.address;
    }

    public MobileCoinPublicAddress getOwnAddress() {
        return this.ownAddress;
    }

    public void postQrData(String str) {
        this.address.postValue(str);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new PaymentsTransferViewModel());
        }
    }
}
