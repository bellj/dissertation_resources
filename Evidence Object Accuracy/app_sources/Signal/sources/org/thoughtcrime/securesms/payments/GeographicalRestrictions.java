package org.thoughtcrime.securesms.payments;

import j$.util.Collection$EL;
import j$.util.DesugarArrays;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import j$.util.stream.Stream;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PagedDataSource$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class GeographicalRestrictions {
    private static final String TAG = Log.tag(GeographicalRestrictions.class);

    private GeographicalRestrictions() {
    }

    public static boolean e164Allowed(String str) {
        if (str == null) {
            return false;
        }
        if (str.startsWith("+")) {
            str = str.substring(1);
        }
        Stream stream = Collection$EL.stream(parsePrefixes(FeatureFlags.paymentsCountryBlocklist()));
        Objects.requireNonNull(str);
        return stream.noneMatch(new Predicate(str) { // from class: org.thoughtcrime.securesms.payments.GeographicalRestrictions$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return this.f$0.startsWith((String) obj);
            }
        });
    }

    private static List<String> parsePrefixes(String str) {
        return (List) DesugarArrays.stream(str.split(",")).map(new Function() { // from class: org.thoughtcrime.securesms.payments.GeographicalRestrictions$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((String) obj).replaceAll(" ", "");
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new GiphyMp4PagedDataSource$$ExternalSyntheticLambda0()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.payments.GeographicalRestrictions$$ExternalSyntheticLambda2
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return GeographicalRestrictions.lambda$parsePrefixes$1((String) obj);
            }
        }).collect(Collectors.toList());
    }

    public static /* synthetic */ boolean lambda$parsePrefixes$1(String str) {
        return !Util.isEmpty(str);
    }
}
