package org.thoughtcrime.securesms.payments.preferences;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class PaymentsAllActivityFragment extends LoggingFragment {
    public PaymentsAllActivityFragment() {
        super(R.layout.payments_activity_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.payments_all_activity_fragment_view_pager);
        ((Toolbar) view.findViewById(R.id.payments_all_activity_fragment_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsAllActivityFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsAllActivityFragment.m2401$r8$lambda$9w6hIhDTdkcIiL7AaQRkP7ZoI(view2);
            }
        });
        viewPager.setAdapter(new Adapter(getChildFragmentManager()));
        ((TabLayout) view.findViewById(R.id.payments_all_activity_fragment_tabs)).setupWithViewPager(viewPager);
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    /* loaded from: classes4.dex */
    private final class Adapter extends FragmentStatePagerAdapter {
        @Override // androidx.viewpager.widget.PagerAdapter
        public int getCount() {
            return 3;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        Adapter(FragmentManager fragmentManager) {
            super(fragmentManager, 1);
            PaymentsAllActivityFragment.this = r1;
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public CharSequence getPageTitle(int i) {
            if (i == 0) {
                return PaymentsAllActivityFragment.this.getString(R.string.PaymentsAllActivityFragment__all);
            }
            if (i == 1) {
                return PaymentsAllActivityFragment.this.getString(R.string.PaymentsAllActivityFragment__sent);
            }
            if (i == 2) {
                return PaymentsAllActivityFragment.this.getString(R.string.PaymentsAllActivityFragment__received);
            }
            throw new IllegalStateException("Unknown position: " + i);
        }

        @Override // androidx.fragment.app.FragmentStatePagerAdapter
        public Fragment getItem(int i) {
            if (i == 0) {
                return PaymentsPagerItemFragment.getFragmentForAllPayments();
            }
            if (i == 1) {
                return PaymentsPagerItemFragment.getFragmentForSentPayments();
            }
            if (i == 2) {
                return PaymentsPagerItemFragment.getFragmentForReceivedPayments();
            }
            throw new IllegalStateException("Unknown position: " + i);
        }
    }
}
