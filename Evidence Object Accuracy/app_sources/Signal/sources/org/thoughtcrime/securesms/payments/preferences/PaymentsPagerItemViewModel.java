package org.thoughtcrime.securesms.payments.preferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.function.Function;
import java.util.List;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.preferences.model.PaymentItem;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
final class PaymentsPagerItemViewModel extends ViewModel {
    private final LiveData<MappingModelList> list;

    PaymentsPagerItemViewModel(PaymentCategory paymentCategory, PaymentsRepository paymentsRepository) {
        LiveData<List<Payment>> liveData;
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentCategory[paymentCategory.ordinal()];
        if (i == 1) {
            liveData = paymentsRepository.getRecentPayments();
        } else if (i == 2) {
            liveData = paymentsRepository.getRecentSentPayments();
        } else if (i == 3) {
            liveData = paymentsRepository.getRecentReceivedPayments();
        } else {
            throw new IllegalArgumentException();
        }
        this.list = LiveDataUtil.mapAsync(liveData, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsPagerItemViewModel$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PaymentItem.fromPayment((List) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.payments.preferences.PaymentsPagerItemViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentCategory;

        static {
            int[] iArr = new int[PaymentCategory.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentCategory = iArr;
            try {
                iArr[PaymentCategory.ALL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentCategory[PaymentCategory.SENT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$preferences$PaymentCategory[PaymentCategory.RECEIVED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public LiveData<MappingModelList> getList() {
        return this.list;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final PaymentCategory paymentCategory;

        public Factory(PaymentCategory paymentCategory) {
            this.paymentCategory = paymentCategory;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new PaymentsPagerItemViewModel(this.paymentCategory, new PaymentsRepository()));
        }
    }
}
