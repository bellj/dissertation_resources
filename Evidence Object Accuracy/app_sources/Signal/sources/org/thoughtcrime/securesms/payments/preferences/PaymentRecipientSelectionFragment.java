package org.thoughtcrime.securesms.payments.preferences;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.Navigation;
import j$.util.Optional;
import j$.util.function.Consumer;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ContactFilterView;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.payments.CanNotSendPaymentDialog;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.whispersystems.signalservice.api.util.ExpiringProfileCredentialUtil;

/* loaded from: classes4.dex */
public class PaymentRecipientSelectionFragment extends LoggingFragment implements ContactSelectionListFragment.OnContactSelectedListener, ContactSelectionListFragment.ScrollCallback {
    private ContactFilterView contactFilterView;
    private ContactSelectionListFragment contactsFragment;
    private Toolbar toolbar;

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onContactDeselected(Optional<RecipientId> optional, String str) {
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onSelectionChanged() {
    }

    public PaymentRecipientSelectionFragment() {
        super(R.layout.payment_recipient_selection_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.payment_recipient_selection_fragment_toolbar);
        this.toolbar = toolbar;
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentRecipientSelectionFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentRecipientSelectionFragment.$r8$lambda$gqGt53E6mbuIOMHmw4pXhSal08A(view2);
            }
        });
        this.contactFilterView = (ContactFilterView) view.findViewById(R.id.contact_filter_edit_text);
        Bundle bundle2 = new Bundle();
        bundle2.putBoolean("refreshable", false);
        bundle2.putInt("display_mode", 65);
        bundle2.putBoolean("can_select_self", false);
        Fragment findFragmentById = getChildFragmentManager().findFragmentById(R.id.contact_selection_list_fragment_holder);
        if (findFragmentById == null) {
            FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
            ContactSelectionListFragment contactSelectionListFragment = new ContactSelectionListFragment();
            this.contactsFragment = contactSelectionListFragment;
            contactSelectionListFragment.setArguments(bundle2);
            beginTransaction.add(R.id.contact_selection_list_fragment_holder, this.contactsFragment);
            beginTransaction.commit();
        } else {
            this.contactsFragment = (ContactSelectionListFragment) findFragmentById;
        }
        initializeSearch();
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    private void initializeSearch() {
        this.contactFilterView.setOnFilterChangedListener(new ContactFilterView.OnFilterChangedListener() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentRecipientSelectionFragment$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.components.ContactFilterView.OnFilterChangedListener
            public final void onFilterChanged(String str) {
                PaymentRecipientSelectionFragment.m2400$r8$lambda$n0duTz2ATY5VH6CyE22uMFwNG0(PaymentRecipientSelectionFragment.this, str);
            }
        });
    }

    public /* synthetic */ void lambda$initializeSearch$1(String str) {
        this.contactsFragment.setQueryFilter(str);
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onBeforeContactSelected(Optional<RecipientId> optional, String str, Consumer<Boolean> consumer) {
        if (optional.isPresent()) {
            SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentRecipientSelectionFragment$$ExternalSyntheticLambda4
                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return PaymentRecipientSelectionFragment.$r8$lambda$qjOF4e1pHebjpTGwpjtAo5Y5W7I(Optional.this);
                }
            }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentRecipientSelectionFragment$$ExternalSyntheticLambda5
                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    PaymentRecipientSelectionFragment.m2397$r8$lambda$NkZ_gINpHSRxYMqx1N6GVw36wo(PaymentRecipientSelectionFragment.this, (Recipient) obj);
                }
            });
        }
        consumer.accept(Boolean.FALSE);
    }

    public static /* synthetic */ Recipient lambda$onBeforeContactSelected$2(Optional optional) {
        return Recipient.resolved((RecipientId) optional.get());
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.ScrollCallback
    public void onBeginScroll() {
        hideKeyboard();
    }

    private void hideKeyboard() {
        ViewUtil.hideKeyboard(requireContext(), this.toolbar);
        this.toolbar.clearFocus();
    }

    public void createPaymentOrShowWarningDialog(Recipient recipient) {
        if (ExpiringProfileCredentialUtil.isValid(recipient.getExpiringProfileKeyCredential())) {
            createPayment(recipient.getId());
        } else {
            showWarningDialog(recipient.getId());
        }
    }

    private void createPayment(RecipientId recipientId) {
        hideKeyboard();
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), PaymentRecipientSelectionFragmentDirections.actionPaymentRecipientSelectionToCreatePayment(new PayeeParcelable(recipientId)));
    }

    private void showWarningDialog(RecipientId recipientId) {
        CanNotSendPaymentDialog.show(requireContext(), new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentRecipientSelectionFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PaymentRecipientSelectionFragment.$r8$lambda$NgPIimm7p7n3_rjz0TWNWR9svew(PaymentRecipientSelectionFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: openConversation */
    public void lambda$showWarningDialog$3(RecipientId recipientId) {
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentRecipientSelectionFragment$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return PaymentRecipientSelectionFragment.m2398$r8$lambda$R3ghRXUXO1nrqLtz2P3bPtyLYc(RecipientId.this);
            }
        }, new SimpleTask.ForegroundTask(recipientId) { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentRecipientSelectionFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                PaymentRecipientSelectionFragment.m2399$r8$lambda$jufqIzH12FlHPlJFWnz3TFpnNU(PaymentRecipientSelectionFragment.this, this.f$1, (Long) obj);
            }
        });
    }

    public static /* synthetic */ Long lambda$openConversation$4(RecipientId recipientId) {
        return Long.valueOf(SignalDatabase.threads().getThreadIdIfExistsFor(recipientId));
    }

    public /* synthetic */ void lambda$openConversation$5(RecipientId recipientId, Long l) {
        startActivity(ConversationIntents.createBuilder(requireContext(), recipientId, l.longValue()).build());
    }
}
