package org.thoughtcrime.securesms.payments.create;

import j$.util.Optional;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.payments.currency.CurrencyExchange;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class InputState {
    private final Optional<CurrencyExchange.ExchangeRate> exchangeRate;
    private final String fiatAmount;
    private final Optional<FiatMoney> fiatMoney;
    private final InputTarget inputTarget;
    private final Money money;
    private final String moneyAmount;

    public InputState() {
        this(InputTarget.MONEY, "0", "0", Money.MobileCoin.ZERO, Optional.empty(), Optional.empty());
    }

    private InputState(InputTarget inputTarget, String str, String str2, Money money, Optional<FiatMoney> optional, Optional<CurrencyExchange.ExchangeRate> optional2) {
        this.inputTarget = inputTarget;
        this.moneyAmount = str;
        this.fiatAmount = str2;
        this.money = money;
        this.fiatMoney = optional;
        this.exchangeRate = optional2;
    }

    public String getFiatAmount() {
        return this.fiatAmount;
    }

    public Optional<FiatMoney> getFiatMoney() {
        return this.fiatMoney;
    }

    public String getMoneyAmount() {
        return this.moneyAmount;
    }

    public Money getMoney() {
        return this.money;
    }

    public InputTarget getInputTarget() {
        return this.inputTarget;
    }

    public Optional<CurrencyExchange.ExchangeRate> getExchangeRate() {
        return this.exchangeRate;
    }

    public InputState updateInputTarget(InputTarget inputTarget) {
        return new InputState(inputTarget, this.moneyAmount, this.fiatAmount, this.money, this.fiatMoney, this.exchangeRate);
    }

    public InputState updateAmount(String str, String str2, Money money, Optional<FiatMoney> optional) {
        return new InputState(this.inputTarget, str, str2, money, optional, this.exchangeRate);
    }

    public InputState updateExchangeRate(Optional<CurrencyExchange.ExchangeRate> optional) {
        return new InputState(this.inputTarget, this.moneyAmount, this.fiatAmount, this.money, this.fiatMoney, optional);
    }
}
