package org.thoughtcrime.securesms.payments.backup.entry;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputLayout;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.Mnemonic;
import org.thoughtcrime.securesms.payments.backup.entry.PaymentsRecoveryEntryViewModel;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;

/* loaded from: classes4.dex */
public class PaymentsRecoveryEntryFragment extends Fragment {
    public PaymentsRecoveryEntryFragment() {
        super(R.layout.payments_recovery_entry_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        TextView textView = (TextView) view.findViewById(R.id.payments_recovery_entry_fragment_message);
        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.payments_recovery_entry_fragment_word_wrapper);
        MaterialAutoCompleteTextView materialAutoCompleteTextView = (MaterialAutoCompleteTextView) view.findViewById(R.id.payments_recovery_entry_fragment_word);
        View findViewById = view.findViewById(R.id.payments_recovery_entry_fragment_next);
        PaymentsRecoveryEntryViewModel paymentsRecoveryEntryViewModel = (PaymentsRecoveryEntryViewModel) ViewModelProviders.of(this).get(PaymentsRecoveryEntryViewModel.class);
        ((Toolbar) view.findViewById(R.id.payments_recovery_entry_fragment_toolbar)).setNavigationOnClickListener(new View.OnClickListener(view) { // from class: org.thoughtcrime.securesms.payments.backup.entry.PaymentsRecoveryEntryFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsRecoveryEntryFragment.$r8$lambda$F9j3Zs57GbWHF6B685yVhi4KSQY(this.f$0, view2);
            }
        });
        paymentsRecoveryEntryViewModel.getState().observe(getViewLifecycleOwner(), new Observer(textView, materialAutoCompleteTextView, textInputLayout, findViewById) { // from class: org.thoughtcrime.securesms.payments.backup.entry.PaymentsRecoveryEntryFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ TextView f$1;
            public final /* synthetic */ MaterialAutoCompleteTextView f$2;
            public final /* synthetic */ TextInputLayout f$3;
            public final /* synthetic */ View f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsRecoveryEntryFragment.$r8$lambda$B8cgyw3yT3uIPhlJP8e9MxJxg4k(PaymentsRecoveryEntryFragment.this, this.f$1, this.f$2, this.f$3, this.f$4, (PaymentsRecoveryEntryViewState) obj);
            }
        });
        paymentsRecoveryEntryViewModel.getEvents().observe(getViewLifecycleOwner(), new Observer(view, paymentsRecoveryEntryViewModel) { // from class: org.thoughtcrime.securesms.payments.backup.entry.PaymentsRecoveryEntryFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ View f$0;
            public final /* synthetic */ PaymentsRecoveryEntryViewModel f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsRecoveryEntryFragment.$r8$lambda$kNPDF1k4XYutECN_2sTJvIeR9Kg(this.f$0, this.f$1, (PaymentsRecoveryEntryViewModel.Events) obj);
            }
        });
        materialAutoCompleteTextView.setAdapter(new ArrayAdapter(requireContext(), (int) R.layout.support_simple_spinner_dropdown_item, Mnemonic.BIP39_WORDS_ENGLISH));
        materialAutoCompleteTextView.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.payments.backup.entry.PaymentsRecoveryEntryFragment$$ExternalSyntheticLambda3
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                PaymentsRecoveryEntryFragment.m2367$r8$lambda$nRG69QuVorLVXWRCgQsBpk2V5c(PaymentsRecoveryEntryViewModel.this, (Editable) obj);
            }
        }));
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.entry.PaymentsRecoveryEntryFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsRecoveryEntryFragment.$r8$lambda$m9lx6Kz2xcF9w9lwnGZIUu1jMks(PaymentsRecoveryEntryViewModel.this, view2);
            }
        });
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view, View view2) {
        Navigation.findNavController(view).popBackStack(R.id.paymentsHome, false);
    }

    public /* synthetic */ void lambda$onViewCreated$1(TextView textView, MaterialAutoCompleteTextView materialAutoCompleteTextView, TextInputLayout textInputLayout, View view, PaymentsRecoveryEntryViewState paymentsRecoveryEntryViewState) {
        textView.setText(getString(R.string.PaymentsRecoveryEntryFragment__enter_word_d, Integer.valueOf(paymentsRecoveryEntryViewState.getWordIndex() + 1)));
        materialAutoCompleteTextView.setHint(getString(R.string.PaymentsRecoveryEntryFragment__word_d, Integer.valueOf(paymentsRecoveryEntryViewState.getWordIndex() + 1)));
        textInputLayout.setError((paymentsRecoveryEntryViewState.canMoveToNext() || TextUtils.isEmpty(paymentsRecoveryEntryViewState.getCurrentEntry())) ? null : getString(R.string.PaymentsRecoveryEntryFragment__invalid_word));
        view.setEnabled(paymentsRecoveryEntryViewState.canMoveToNext());
        String obj = materialAutoCompleteTextView.getText().toString();
        String str = (String) Util.firstNonNull(paymentsRecoveryEntryViewState.getCurrentEntry(), "");
        if (!obj.equals(str)) {
            materialAutoCompleteTextView.setText(str);
        }
    }

    public static /* synthetic */ void lambda$onViewCreated$2(View view, PaymentsRecoveryEntryViewModel paymentsRecoveryEntryViewModel, PaymentsRecoveryEntryViewModel.Events events) {
        if (events == PaymentsRecoveryEntryViewModel.Events.GO_TO_CONFIRM) {
            SafeNavigation.safeNavigate(Navigation.findNavController(view), PaymentsRecoveryEntryFragmentDirections.actionPaymentsRecoveryEntryToPaymentsRecoveryPhrase(false).setWords(paymentsRecoveryEntryViewModel.getWords()));
        }
    }

    public static /* synthetic */ void lambda$onViewCreated$3(PaymentsRecoveryEntryViewModel paymentsRecoveryEntryViewModel, Editable editable) {
        paymentsRecoveryEntryViewModel.onWordChanged(editable.toString());
    }
}
