package org.thoughtcrime.securesms.payments.preferences.viewholder;

import android.view.View;
import org.thoughtcrime.securesms.payments.preferences.model.NoRecentActivity;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class NoRecentActivityViewHolder extends MappingViewHolder<NoRecentActivity> {
    public void bind(NoRecentActivity noRecentActivity) {
    }

    public NoRecentActivityViewHolder(View view) {
        super(view);
    }
}
