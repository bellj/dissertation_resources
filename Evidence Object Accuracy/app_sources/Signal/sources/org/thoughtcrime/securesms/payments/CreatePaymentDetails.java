package org.thoughtcrime.securesms.payments;

import android.os.Parcel;
import android.os.Parcelable;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class CreatePaymentDetails implements Parcelable {
    public static final Parcelable.Creator<CreatePaymentDetails> CREATOR = new Parcelable.Creator<CreatePaymentDetails>() { // from class: org.thoughtcrime.securesms.payments.CreatePaymentDetails.1
        @Override // android.os.Parcelable.Creator
        public CreatePaymentDetails createFromParcel(Parcel parcel) {
            return new CreatePaymentDetails(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public CreatePaymentDetails[] newArray(int i) {
            return new CreatePaymentDetails[i];
        }
    };
    private final Money amount;
    private final String note;
    private final PayeeParcelable payee;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public CreatePaymentDetails(PayeeParcelable payeeParcelable, Money money, String str) {
        this.payee = payeeParcelable;
        this.amount = money;
        this.note = str;
    }

    protected CreatePaymentDetails(Parcel parcel) {
        this.payee = (PayeeParcelable) parcel.readParcelable(PayeeParcelable.class.getClassLoader());
        this.amount = Money.parseOrThrow(parcel.readString());
        this.note = parcel.readString();
    }

    public Payee getPayee() {
        return this.payee.getPayee();
    }

    public Money getAmount() {
        return this.amount;
    }

    public String getNote() {
        return this.note;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.payee, i);
        parcel.writeString(this.amount.serialize());
        parcel.writeString(this.note);
    }
}
