package org.thoughtcrime.securesms.payments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class CanNotSendPaymentDialog {
    private CanNotSendPaymentDialog() {
    }

    public static void show(Context context) {
        show(context, null);
    }

    public static void show(Context context, Runnable runnable) {
        AlertDialog.Builder message = new AlertDialog.Builder(context).setTitle(R.string.CanNotSendPaymentDialog__cant_send_payment).setMessage(R.string.CanNotSendPaymentDialog__to_send_a_payment_to_this_user);
        if (runnable != null) {
            message.setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.CanNotSendPaymentDialog$$ExternalSyntheticLambda0
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).setPositiveButton(R.string.CanNotSendPaymentDialog__send_a_message, new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.payments.CanNotSendPaymentDialog$$ExternalSyntheticLambda1
                public final /* synthetic */ Runnable f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    CanNotSendPaymentDialog.lambda$show$1(this.f$0, dialogInterface, i);
                }
            }).show();
        } else {
            message.setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.CanNotSendPaymentDialog$$ExternalSyntheticLambda2
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();
        }
    }

    public static /* synthetic */ void lambda$show$1(Runnable runnable, DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        runnable.run();
    }
}
