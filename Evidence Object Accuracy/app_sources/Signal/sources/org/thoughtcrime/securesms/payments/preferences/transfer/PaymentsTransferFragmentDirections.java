package org.thoughtcrime.securesms.payments.preferences.transfer;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;

/* loaded from: classes4.dex */
public class PaymentsTransferFragmentDirections {
    private PaymentsTransferFragmentDirections() {
    }

    public static NavDirections actionPaymentsTransferToPaymentsScanQr() {
        return new ActionOnlyNavDirections(R.id.action_paymentsTransfer_to_paymentsScanQr);
    }

    public static ActionPaymentsTransferToCreatePayment actionPaymentsTransferToCreatePayment(PayeeParcelable payeeParcelable) {
        return new ActionPaymentsTransferToCreatePayment(payeeParcelable);
    }

    /* loaded from: classes4.dex */
    public static class ActionPaymentsTransferToCreatePayment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_paymentsTransfer_to_createPayment;
        }

        private ActionPaymentsTransferToCreatePayment(PayeeParcelable payeeParcelable) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (payeeParcelable != null) {
                hashMap.put("payee", payeeParcelable);
                return;
            }
            throw new IllegalArgumentException("Argument \"payee\" is marked as non-null but was passed a null value.");
        }

        public ActionPaymentsTransferToCreatePayment setPayee(PayeeParcelable payeeParcelable) {
            if (payeeParcelable != null) {
                this.arguments.put("payee", payeeParcelable);
                return this;
            }
            throw new IllegalArgumentException("Argument \"payee\" is marked as non-null but was passed a null value.");
        }

        public ActionPaymentsTransferToCreatePayment setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("payee")) {
                PayeeParcelable payeeParcelable = (PayeeParcelable) this.arguments.get("payee");
                if (Parcelable.class.isAssignableFrom(PayeeParcelable.class) || payeeParcelable == null) {
                    bundle.putParcelable("payee", (Parcelable) Parcelable.class.cast(payeeParcelable));
                } else if (Serializable.class.isAssignableFrom(PayeeParcelable.class)) {
                    bundle.putSerializable("payee", (Serializable) Serializable.class.cast(payeeParcelable));
                } else {
                    throw new UnsupportedOperationException(PayeeParcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("finish_on_confirm")) {
                bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
            } else {
                bundle.putBoolean("finish_on_confirm", false);
            }
            return bundle;
        }

        public PayeeParcelable getPayee() {
            return (PayeeParcelable) this.arguments.get("payee");
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionPaymentsTransferToCreatePayment actionPaymentsTransferToCreatePayment = (ActionPaymentsTransferToCreatePayment) obj;
            if (this.arguments.containsKey("payee") != actionPaymentsTransferToCreatePayment.arguments.containsKey("payee")) {
                return false;
            }
            if (getPayee() == null ? actionPaymentsTransferToCreatePayment.getPayee() == null : getPayee().equals(actionPaymentsTransferToCreatePayment.getPayee())) {
                return this.arguments.containsKey("finish_on_confirm") == actionPaymentsTransferToCreatePayment.arguments.containsKey("finish_on_confirm") && getFinishOnConfirm() == actionPaymentsTransferToCreatePayment.getFinishOnConfirm() && getActionId() == actionPaymentsTransferToCreatePayment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((((getPayee() != null ? getPayee().hashCode() : 0) + 31) * 31) + (getFinishOnConfirm() ? 1 : 0)) * 31) + getActionId();
        }

        public String toString() {
            return "ActionPaymentsTransferToCreatePayment(actionId=" + getActionId() + "){payee=" + getPayee() + ", finishOnConfirm=" + getFinishOnConfirm() + "}";
        }
    }
}
