package org.thoughtcrime.securesms.payments.confirm;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.StringUtil;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.CanNotSendPaymentDialog;
import org.thoughtcrime.securesms.payments.FiatMoneyUtil;
import org.thoughtcrime.securesms.payments.Payee;
import org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentAdapter;
import org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentState;
import org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentViewModel;
import org.thoughtcrime.securesms.payments.preferences.RecipientHasNotEnabledPaymentsDialog;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.whispersystems.signalservice.api.payments.FormatterOptions;

/* loaded from: classes4.dex */
public class ConfirmPaymentFragment extends BottomSheetDialogFragment {
    private final Runnable dismiss = new Runnable() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentFragment$$ExternalSyntheticLambda3
        @Override // java.lang.Runnable
        public final void run() {
            ConfirmPaymentFragment.m2374$r8$lambda$QR09AVGAUtcGR0lFN4nbd48BYY(ConfirmPaymentFragment.this);
        }
    };
    private ConfirmPaymentViewModel viewModel;

    public /* synthetic */ void lambda$new$0() {
        dismissAllowingStateLoss();
        if (ConfirmPaymentFragmentArgs.fromBundle(requireArguments()).getFinishOnConfirm()) {
            requireActivity().setResult(-1);
            requireActivity().finish();
            return;
        }
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_directly_to_paymentsHome);
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, R.style.Signal_DayNight_BottomSheet_Rounded);
        super.onCreate(bundle);
    }

    @Override // com.google.android.material.bottomsheet.BottomSheetDialogFragment, androidx.appcompat.app.AppCompatDialogFragment, androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(bundle);
        bottomSheetDialog.getBehavior().setHideable(false);
        return bottomSheetDialog;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.confirm_payment_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.viewModel = (ConfirmPaymentViewModel) ViewModelProviders.of(this, new ConfirmPaymentViewModel.Factory(ConfirmPaymentFragmentArgs.fromBundle(requireArguments()).getCreatePaymentDetails())).get(ConfirmPaymentViewModel.class);
        ConfirmPaymentAdapter confirmPaymentAdapter = new ConfirmPaymentAdapter(new Callbacks(this, null));
        ((RecyclerView) view.findViewById(R.id.confirm_payment_fragment_list)).setAdapter(confirmPaymentAdapter);
        this.viewModel.getState().observe(getViewLifecycleOwner(), new Observer(confirmPaymentAdapter) { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ ConfirmPaymentAdapter f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConfirmPaymentFragment.$r8$lambda$V5DaXwt2s5y_hqr51U5qgbShy2c(ConfirmPaymentFragment.this, this.f$1, (ConfirmPaymentState) obj);
            }
        });
        this.viewModel.isPaymentDone().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConfirmPaymentFragment.$r8$lambda$8aOIEm2udypKmw5JtKAZkQ67_20(ConfirmPaymentFragment.this, (Boolean) obj);
            }
        });
        this.viewModel.getErrorTypeEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConfirmPaymentFragment.m2376$r8$lambda$snfaMqOs6TfpodjcgMhCMcRnZ0(ConfirmPaymentFragment.this, (ConfirmPaymentViewModel.ErrorType) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$1(ConfirmPaymentAdapter confirmPaymentAdapter, ConfirmPaymentState confirmPaymentState) {
        confirmPaymentAdapter.submitList(createList(confirmPaymentState));
    }

    public /* synthetic */ void lambda$onViewCreated$2(Boolean bool) {
        if (bool.booleanValue()) {
            ThreadUtil.runOnMainDelayed(this.dismiss, TimeUnit.SECONDS.toMillis(2));
        }
    }

    /* renamed from: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$FeeStatus;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentViewModel$ErrorType;

        static {
            int[] iArr = new int[ConfirmPaymentViewModel.ErrorType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentViewModel$ErrorType = iArr;
            try {
                iArr[ConfirmPaymentViewModel.ErrorType.NO_PROFILE_KEY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentViewModel$ErrorType[ConfirmPaymentViewModel.ErrorType.NO_ADDRESS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentViewModel$ErrorType[ConfirmPaymentViewModel.ErrorType.CAN_NOT_GET_FEE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[ConfirmPaymentState.FeeStatus.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$FeeStatus = iArr2;
            try {
                iArr2[ConfirmPaymentState.FeeStatus.STILL_LOADING.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$FeeStatus[ConfirmPaymentState.FeeStatus.ERROR.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$FeeStatus[ConfirmPaymentState.FeeStatus.NOT_SET.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$FeeStatus[ConfirmPaymentState.FeeStatus.SET.ordinal()] = 4;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    public /* synthetic */ void lambda$onViewCreated$5(ConfirmPaymentViewModel.ErrorType errorType) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentViewModel$ErrorType[errorType.ordinal()];
        if (i == 1) {
            CanNotSendPaymentDialog.show(requireContext());
        } else if (i == 2) {
            RecipientHasNotEnabledPaymentsDialog.show(requireContext());
        } else if (i == 3) {
            new AlertDialog.Builder(requireContext()).setMessage(R.string.ConfirmPaymentFragment__unable_to_request_a_network_fee).setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentFragment$$ExternalSyntheticLambda4
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    ConfirmPaymentFragment.m2375$r8$lambda$h05IGCoig3t9TBUziJ6CqkYcQ(ConfirmPaymentFragment.this, dialogInterface, i2);
                }
            }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentFragment$$ExternalSyntheticLambda5
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    ConfirmPaymentFragment.$r8$lambda$gKFV5lowJrY9zUcJXPcmpBQ59Mo(ConfirmPaymentFragment.this, dialogInterface, i2);
                }
            }).setCancelable(false).show();
        }
    }

    public /* synthetic */ void lambda$onViewCreated$3(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        this.viewModel.refreshFee();
    }

    public /* synthetic */ void lambda$onViewCreated$4(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        dismiss();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        ThreadUtil.cancelRunnableOnMain(this.dismiss);
    }

    private MappingModelList createList(ConfirmPaymentState confirmPaymentState) {
        MappingModelList mappingModelList = new MappingModelList();
        FormatterOptions defaults = FormatterOptions.defaults();
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$FeeStatus[confirmPaymentState.getFeeStatus().ordinal()];
        if (i == 1 || i == 2) {
            mappingModelList.add(new ConfirmPaymentAdapter.LoadingItem());
        } else if (i == 3 || i == 4) {
            mappingModelList.add(new ConfirmPaymentAdapter.LineItem(getToPayeeDescription(requireContext(), confirmPaymentState), confirmPaymentState.getAmount().toString(defaults)));
            if (confirmPaymentState.getExchange() != null) {
                mappingModelList.add(new ConfirmPaymentAdapter.LineItem(getString(R.string.ConfirmPayment__estimated_s, confirmPaymentState.getExchange().getCurrency().getCurrencyCode()), FiatMoneyUtil.format(getResources(), confirmPaymentState.getExchange(), FiatMoneyUtil.formatOptions().withDisplayTime(false))));
            }
            mappingModelList.add(new ConfirmPaymentAdapter.LineItem(getString(R.string.ConfirmPayment__network_fee), confirmPaymentState.getFee().toString(defaults)));
            mappingModelList.add(new ConfirmPaymentAdapter.Divider());
            mappingModelList.add(new ConfirmPaymentAdapter.TotalLineItem(getString(R.string.ConfirmPayment__total_amount), confirmPaymentState.getTotal().toString(defaults)));
        }
        mappingModelList.add(new ConfirmPaymentAdapter.ConfirmPaymentStatus(confirmPaymentState.getStatus(), confirmPaymentState.getFeeStatus(), confirmPaymentState.getBalance()));
        return mappingModelList;
    }

    private static CharSequence getToPayeeDescription(Context context, ConfirmPaymentState confirmPaymentState) {
        return new SpannableStringBuilder().append((CharSequence) context.getString(R.string.ConfirmPayment__to)).append(' ').append(getPayeeDescription(context, confirmPaymentState.getPayee()));
    }

    private static CharSequence getPayeeDescription(Context context, Payee payee) {
        if (payee.hasRecipientId()) {
            return Recipient.resolved(payee.requireRecipientId()).getDisplayName(context);
        }
        return mono(context, StringUtil.abbreviateInMiddle(payee.requirePublicAddress().getPaymentAddressBase58(), 17));
    }

    private static CharSequence mono(Context context, CharSequence charSequence) {
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new TextAppearanceSpan(context, R.style.TextAppearance_Signal_Mono), 0, charSequence.length(), 33);
        return spannableString;
    }

    /* loaded from: classes4.dex */
    private class Callbacks implements ConfirmPaymentAdapter.Callbacks {
        private Callbacks() {
            ConfirmPaymentFragment.this = r1;
        }

        /* synthetic */ Callbacks(ConfirmPaymentFragment confirmPaymentFragment, AnonymousClass1 r2) {
            this();
        }

        @Override // org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentAdapter.Callbacks
        public void onConfirmPayment() {
            ConfirmPaymentFragment.this.setCancelable(false);
            ConfirmPaymentFragment.this.viewModel.confirmPayment();
        }
    }
}
