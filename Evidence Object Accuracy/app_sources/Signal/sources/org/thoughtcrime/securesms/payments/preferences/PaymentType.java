package org.thoughtcrime.securesms.payments.preferences;

/* loaded from: classes4.dex */
public enum PaymentType {
    REQUEST("request"),
    PAYMENT("payment");
    
    private final String code;

    PaymentType(String str) {
        this.code = str;
    }

    String getCode() {
        return this.code;
    }
}
