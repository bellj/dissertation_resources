package org.thoughtcrime.securesms.payments.currency;

import java.io.IOException;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.payments.Payments;
import org.thoughtcrime.securesms.util.AsynchronousCallback;

/* loaded from: classes4.dex */
public final class CurrencyExchangeRepository {
    private static final String TAG = Log.tag(CurrencyExchangeRepository.class);
    private final Payments payments;

    public CurrencyExchangeRepository(Payments payments) {
        this.payments = payments;
    }

    public void getCurrencyExchange(AsynchronousCallback.WorkerThread<CurrencyExchange, Throwable> workerThread, boolean z) {
        SignalExecutors.BOUNDED.execute(new Runnable(workerThread, z) { // from class: org.thoughtcrime.securesms.payments.currency.CurrencyExchangeRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$1;
            public final /* synthetic */ boolean f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CurrencyExchangeRepository.m2393$r8$lambda$Z297QzUo_3JioLZXSrB_iSo9Gg(CurrencyExchangeRepository.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$getCurrencyExchange$0(AsynchronousCallback.WorkerThread workerThread, boolean z) {
        try {
            workerThread.onComplete(this.payments.getCurrencyExchange(z));
        } catch (IOException e) {
            Log.w(TAG, e);
            workerThread.onError(e);
        }
    }
}
