package org.thoughtcrime.securesms.payments;

/* loaded from: classes4.dex */
public enum FailureReason {
    UNKNOWN(0),
    INSUFFICIENT_FUNDS(1),
    NETWORK(2);
    
    private final int value;

    FailureReason(int i) {
        this.value = i;
    }

    public int serialize() {
        return this.value;
    }

    public static FailureReason deserialize(int i) {
        FailureReason failureReason = UNKNOWN;
        if (i == failureReason.value) {
            return failureReason;
        }
        FailureReason failureReason2 = INSUFFICIENT_FUNDS;
        if (i == failureReason2.value) {
            return failureReason2;
        }
        FailureReason failureReason3 = NETWORK;
        if (i == failureReason3.value) {
            return failureReason3;
        }
        throw new AssertionError("" + i);
    }
}
