package org.thoughtcrime.securesms.payments.backup;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;
import org.whispersystems.signalservice.api.payments.PaymentsConstants;

/* loaded from: classes4.dex */
public class PaymentsRecoveryStartFragment extends Fragment {
    private final OnBackPressed onBackPressed = new OnBackPressed();

    public PaymentsRecoveryStartFragment() {
        super(R.layout.payments_recovery_start_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.payments_recovery_start_fragment_toolbar);
        TextView textView = (TextView) view.findViewById(R.id.payments_recovery_start_fragment_title);
        LearnMoreTextView learnMoreTextView = (LearnMoreTextView) view.findViewById(R.id.payments_recovery_start_fragment_message);
        TextView textView2 = (TextView) view.findViewById(R.id.payments_recovery_start_fragment_start);
        TextView textView3 = (TextView) view.findViewById(R.id.payments_recovery_start_fragment_paste);
        PaymentsRecoveryStartFragmentArgs fromBundle = PaymentsRecoveryStartFragmentArgs.fromBundle(requireArguments());
        if (fromBundle.getIsRestore()) {
            textView.setText(R.string.PaymentsRecoveryStartFragment__enter_recovery_phrase);
            learnMoreTextView.setText(getString(R.string.PaymentsRecoveryStartFragment__your_recovery_phrase_is_a, Integer.valueOf(PaymentsConstants.MNEMONIC_LENGTH)));
            learnMoreTextView.setLink(getString(R.string.PaymentsRecoveryStartFragment__learn_more__restore));
            textView2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.PaymentsRecoveryStartFragment$$ExternalSyntheticLambda0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    PaymentsRecoveryStartFragment.m2364$r8$lambda$tMIOZYPcRzdrltUHyGF0ulJ9I(PaymentsRecoveryStartFragment.this, view2);
                }
            });
            textView2.setText(R.string.PaymentsRecoveryStartFragment__enter_manually);
            textView3.setVisibility(0);
            textView3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.PaymentsRecoveryStartFragment$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    PaymentsRecoveryStartFragment.m2363$r8$lambda$3lNSQV7369PRkofzqDMv2H7vZI(view2);
                }
            });
        } else {
            textView.setText(R.string.PaymentsRecoveryStartFragment__view_recovery_phrase);
            learnMoreTextView.setText(getString(R.string.PaymentsRecoveryStartFragment__your_balance_will_automatically_restore, Integer.valueOf(PaymentsConstants.MNEMONIC_LENGTH)));
            learnMoreTextView.setLink(getString(R.string.PaymentsRecoveryStartFragment__learn_more__view));
            textView2.setOnClickListener(new View.OnClickListener(fromBundle) { // from class: org.thoughtcrime.securesms.payments.backup.PaymentsRecoveryStartFragment$$ExternalSyntheticLambda2
                public final /* synthetic */ PaymentsRecoveryStartFragmentArgs f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    PaymentsRecoveryStartFragment.$r8$lambda$snkxWSRdeWxjxLZwcsuLb80OwBI(PaymentsRecoveryStartFragment.this, this.f$1, view2);
                }
            });
            textView2.setText(R.string.PaymentsRecoveryStartFragment__start);
            textView3.setVisibility(8);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener(fromBundle) { // from class: org.thoughtcrime.securesms.payments.backup.PaymentsRecoveryStartFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ PaymentsRecoveryStartFragmentArgs f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsRecoveryStartFragment.$r8$lambda$o2t_XsXGBLvE8q9ZrTDRc1dgX8s(PaymentsRecoveryStartFragment.this, this.f$1, view2);
            }
        });
        if (fromBundle.getFinishOnConfirm()) {
            requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), this.onBackPressed);
        }
        learnMoreTextView.setLearnMoreVisible(true);
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), PaymentsRecoveryStartFragmentDirections.actionPaymentsRecoveryStartToPaymentsRecoveryEntry());
    }

    public static /* synthetic */ void lambda$onViewCreated$1(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), PaymentsRecoveryStartFragmentDirections.actionPaymentsRecoveryStartToPaymentsRecoveryPaste());
    }

    public /* synthetic */ void lambda$onViewCreated$2(PaymentsRecoveryStartFragmentArgs paymentsRecoveryStartFragmentArgs, View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), PaymentsRecoveryStartFragmentDirections.actionPaymentsRecoveryStartToPaymentsRecoveryPhrase(paymentsRecoveryStartFragmentArgs.getFinishOnConfirm()));
    }

    public /* synthetic */ void lambda$onViewCreated$3(PaymentsRecoveryStartFragmentArgs paymentsRecoveryStartFragmentArgs, View view) {
        if (paymentsRecoveryStartFragmentArgs.getFinishOnConfirm()) {
            requireActivity().finish();
        } else {
            Navigation.findNavController(requireView()).popBackStack();
        }
    }

    /* loaded from: classes4.dex */
    private class OnBackPressed extends OnBackPressedCallback {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OnBackPressed() {
            super(true);
            PaymentsRecoveryStartFragment.this = r1;
        }

        @Override // androidx.activity.OnBackPressedCallback
        public void handleOnBackPressed() {
            PaymentsRecoveryStartFragment.this.requireActivity().finish();
        }
    }
}
