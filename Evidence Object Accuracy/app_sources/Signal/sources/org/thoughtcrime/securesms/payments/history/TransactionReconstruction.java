package org.thoughtcrime.securesms.payments.history;

import com.annimon.stream.ComparatorCompat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.thoughtcrime.securesms.payments.Direction;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class TransactionReconstruction {
    private final List<Transaction> allTransactions;
    private final List<Transaction> received;
    private final List<Transaction> sent;

    public static TransactionReconstruction estimateBlockLevelActivity(List<Money.MobileCoin> list, List<Money.MobileCoin> list2) {
        List list3;
        Money.MobileCoin sum = Money.MobileCoin.sum(list);
        ArrayList<Money.MobileCoin> arrayList = new ArrayList(list2);
        Collections.sort(arrayList, Money.MobileCoin.DESCENDING);
        ArrayList arrayList2 = new ArrayList(list2.size());
        for (Money.MobileCoin mobileCoin : arrayList) {
            if (mobileCoin.lessThan(sum)) {
                sum = sum.subtract(mobileCoin).requireMobileCoin();
            } else if (mobileCoin.isPositive()) {
                arrayList2.add(new Transaction(mobileCoin, Direction.RECEIVED));
            }
        }
        if (sum.isPositive()) {
            list3 = Collections.singletonList(new Transaction(sum, Direction.SENT));
        } else {
            list3 = Collections.emptyList();
        }
        Comparator<Transaction> comparator = Transaction.ORDER;
        Collections.sort(arrayList2, comparator);
        ArrayList arrayList3 = new ArrayList(list3.size() + arrayList2.size());
        arrayList3.addAll(list3);
        arrayList3.addAll(arrayList2);
        Collections.sort(arrayList3, comparator);
        return new TransactionReconstruction(list3, arrayList2, arrayList3);
    }

    private TransactionReconstruction(List<Transaction> list, List<Transaction> list2, List<Transaction> list3) {
        this.sent = list;
        this.received = list2;
        this.allTransactions = list3;
    }

    public List<Transaction> received() {
        return new ArrayList(this.received);
    }

    public List<Transaction> sent() {
        return new ArrayList(this.sent);
    }

    public List<Transaction> getAllTransactions() {
        return new ArrayList(this.allTransactions);
    }

    /* loaded from: classes4.dex */
    public static final class Transaction {
        private static final Comparator<Transaction> ABSOLUTE_SIZE;
        public static final Comparator<Transaction> ORDER;
        private static final Comparator<Transaction> RECEIVED_FIRST;
        private final Direction direction;
        private final Money.MobileCoin value;

        static {
            TransactionReconstruction$Transaction$$ExternalSyntheticLambda0 transactionReconstruction$Transaction$$ExternalSyntheticLambda0 = new TransactionReconstruction$Transaction$$ExternalSyntheticLambda0();
            RECEIVED_FIRST = transactionReconstruction$Transaction$$ExternalSyntheticLambda0;
            TransactionReconstruction$Transaction$$ExternalSyntheticLambda1 transactionReconstruction$Transaction$$ExternalSyntheticLambda1 = new TransactionReconstruction$Transaction$$ExternalSyntheticLambda1();
            ABSOLUTE_SIZE = transactionReconstruction$Transaction$$ExternalSyntheticLambda1;
            ORDER = ComparatorCompat.chain(transactionReconstruction$Transaction$$ExternalSyntheticLambda0).thenComparing((Comparator) transactionReconstruction$Transaction$$ExternalSyntheticLambda1);
        }

        public static /* synthetic */ int lambda$static$0(Transaction transaction, Transaction transaction2) {
            return transaction2.getDirection().compareTo(transaction.direction);
        }

        public static /* synthetic */ int lambda$static$1(Transaction transaction, Transaction transaction2) {
            return Money.MobileCoin.ASCENDING.compare(transaction.value, transaction2.value);
        }

        private Transaction(Money.MobileCoin mobileCoin, Direction direction) {
            this.value = mobileCoin;
            this.direction = direction;
        }

        public Money.MobileCoin getValue() {
            return this.value;
        }

        public Direction getDirection() {
            return this.direction;
        }

        public Money.MobileCoin getValueWithDirection() {
            return this.direction == Direction.SENT ? this.value.negate() : this.value;
        }

        public String toString() {
            return "Transaction{" + this.value + ", " + this.direction + '}';
        }
    }
}
