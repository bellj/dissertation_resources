package org.thoughtcrime.securesms.payments;

import android.net.Uri;
import com.mobilecoin.lib.PrintableWrapper;
import com.mobilecoin.lib.PublicAddress;
import com.mobilecoin.lib.exceptions.InvalidUriException;
import com.mobilecoin.lib.exceptions.SerializationException;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public final class MobileCoinPublicAddress {
    private static final String TAG = Log.tag(MobileCoinPublicAddress.class);
    private final String base58;
    private final PublicAddress publicAddress;
    private final Uri uri;

    static MobileCoinPublicAddress fromPublicAddress(PublicAddress publicAddress) throws AddressException {
        if (publicAddress != null) {
            return new MobileCoinPublicAddress(publicAddress);
        }
        throw new AddressException("Does not contain a public address");
    }

    public MobileCoinPublicAddress(PublicAddress publicAddress) {
        this.publicAddress = publicAddress;
        try {
            PrintableWrapper fromPublicAddress = PrintableWrapper.fromPublicAddress(publicAddress);
            this.base58 = fromPublicAddress.toB58String();
            this.uri = fromPublicAddress.toUri();
        } catch (SerializationException e) {
            throw new AssertionError(e);
        }
    }

    public static MobileCoinPublicAddress fromBytes(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        try {
            return new MobileCoinPublicAddress(PublicAddress.fromBytes(bArr));
        } catch (SerializationException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    public static MobileCoinPublicAddress fromBase58NullableOrThrow(String str) {
        if (str != null) {
            return fromBase58OrThrow(str);
        }
        return null;
    }

    public static MobileCoinPublicAddress fromBase58OrThrow(String str) {
        try {
            return fromBase58(str);
        } catch (AddressException e) {
            throw new AssertionError(e);
        }
    }

    public static MobileCoinPublicAddress fromBase58(String str) throws AddressException {
        try {
            return fromPublicAddress(PrintableWrapper.fromB58String(str).getPublicAddress());
        } catch (SerializationException e) {
            throw new AddressException(e);
        }
    }

    public static MobileCoinPublicAddress fromQr(String str) throws AddressException {
        try {
            return fromPublicAddress(PrintableWrapper.fromUri(Uri.parse(str)).getPublicAddress());
        } catch (InvalidUriException | SerializationException unused) {
            return fromBase58(str);
        }
    }

    public String getPaymentAddressBase58() {
        return this.base58;
    }

    public Uri getPaymentAddressUri() {
        return this.uri;
    }

    public byte[] serialize() {
        return this.publicAddress.toByteArray();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MobileCoinPublicAddress)) {
            return false;
        }
        return this.base58.equals(((MobileCoinPublicAddress) obj).base58);
    }

    public int hashCode() {
        return this.base58.hashCode();
    }

    public String toString() {
        return this.base58;
    }

    public PublicAddress getAddress() {
        return this.publicAddress;
    }

    /* loaded from: classes4.dex */
    public static final class AddressException extends Exception {
        private AddressException(Throwable th) {
            super(th);
        }

        private AddressException(String str) {
            super(str);
        }
    }
}
