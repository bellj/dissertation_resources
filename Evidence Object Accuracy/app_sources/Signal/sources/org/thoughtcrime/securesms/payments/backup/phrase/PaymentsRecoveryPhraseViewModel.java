package org.thoughtcrime.securesms.payments.backup.phrase;

import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import org.thoughtcrime.securesms.keyvalue.PaymentsValues;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* loaded from: classes4.dex */
public class PaymentsRecoveryPhraseViewModel extends ViewModel {
    private final PaymentsRecoveryPhraseRepository repository = new PaymentsRecoveryPhraseRepository();
    private final SingleLiveEvent<SubmitResult> submitResult = new SingleLiveEvent<>();

    /* loaded from: classes4.dex */
    public enum SubmitResult {
        SUCCESS,
        ERROR
    }

    public LiveData<SubmitResult> getSubmitResult() {
        return this.submitResult;
    }

    /* renamed from: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$keyvalue$PaymentsValues$WalletRestoreResult;

        static {
            int[] iArr = new int[PaymentsValues.WalletRestoreResult.values().length];
            $SwitchMap$org$thoughtcrime$securesms$keyvalue$PaymentsValues$WalletRestoreResult = iArr;
            try {
                iArr[PaymentsValues.WalletRestoreResult.ENTROPY_CHANGED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$PaymentsValues$WalletRestoreResult[PaymentsValues.WalletRestoreResult.ENTROPY_UNCHANGED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$PaymentsValues$WalletRestoreResult[PaymentsValues.WalletRestoreResult.MNEMONIC_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public void onSubmit(List<String> list) {
        this.repository.restoreMnemonic(list, new Consumer() { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseViewModel$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                PaymentsRecoveryPhraseViewModel.this.lambda$onSubmit$0((PaymentsValues.WalletRestoreResult) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onSubmit$0(PaymentsValues.WalletRestoreResult walletRestoreResult) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyvalue$PaymentsValues$WalletRestoreResult[walletRestoreResult.ordinal()];
        if (i == 1 || i == 2) {
            this.submitResult.postValue(SubmitResult.SUCCESS);
        } else if (i == 3) {
            this.submitResult.postValue(SubmitResult.ERROR);
        }
    }
}
