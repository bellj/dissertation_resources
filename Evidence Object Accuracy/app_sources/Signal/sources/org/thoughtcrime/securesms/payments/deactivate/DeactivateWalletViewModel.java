package org.thoughtcrime.securesms.payments.deactivate;

import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeRepository;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class DeactivateWalletViewModel extends ViewModel {
    private final LiveData<Money> balance = Transformations.map(SignalStore.paymentsValues().liveMobileCoinBalance(), new DeactivateWalletViewModel$$ExternalSyntheticLambda1());
    private final SingleLiveEvent<Result> deactivatePaymentResults = new SingleLiveEvent<>();
    private final PaymentsHomeRepository paymentsHomeRepository = new PaymentsHomeRepository();

    /* loaded from: classes4.dex */
    public enum Result {
        SUCCESS,
        FAILED
    }

    public /* synthetic */ void lambda$deactivateWallet$0(Boolean bool) {
        this.deactivatePaymentResults.postValue(bool.booleanValue() ? Result.SUCCESS : Result.FAILED);
    }

    public void deactivateWallet() {
        this.paymentsHomeRepository.deactivatePayments(new Consumer() { // from class: org.thoughtcrime.securesms.payments.deactivate.DeactivateWalletViewModel$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                DeactivateWalletViewModel.this.lambda$deactivateWallet$0((Boolean) obj);
            }
        });
    }

    public LiveData<Result> getDeactivationResults() {
        return this.deactivatePaymentResults;
    }

    public LiveData<Money> getBalance() {
        return this.balance;
    }
}
