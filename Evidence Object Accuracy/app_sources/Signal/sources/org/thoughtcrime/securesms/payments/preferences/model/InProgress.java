package org.thoughtcrime.securesms.payments.preferences.model;

import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes4.dex */
public class InProgress implements MappingModel<InProgress> {
    public boolean areContentsTheSame(InProgress inProgress) {
        return true;
    }

    public boolean areItemsTheSame(InProgress inProgress) {
        return true;
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(InProgress inProgress) {
        return MappingModel.CC.$default$getChangePayload(this, inProgress);
    }
}
