package org.thoughtcrime.securesms.payments.preferences;

import androidx.core.util.Consumer;
import java.io.IOException;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.PaymentLedgerUpdateJob;
import org.thoughtcrime.securesms.jobs.ProfileUploadJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.push.exceptions.PaymentsRegionException;

/* loaded from: classes4.dex */
public class PaymentsHomeRepository {
    private static final String TAG = Log.tag(PaymentsHomeRepository.class);

    /* loaded from: classes4.dex */
    public enum Error {
        NetworkError,
        RegionError
    }

    public void activatePayments(AsynchronousCallback.WorkerThread<Void, Error> workerThread) {
        SignalExecutors.BOUNDED.execute(new Runnable(workerThread) { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PaymentsHomeRepository.this.lambda$activatePayments$0(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$activatePayments$0(AsynchronousCallback.WorkerThread workerThread) {
        SignalStore.paymentsValues().setMobileCoinPaymentsEnabled(true);
        try {
            ProfileUtil.uploadProfile(ApplicationDependencies.getApplication());
            ApplicationDependencies.getJobManager().add(PaymentLedgerUpdateJob.updateLedger());
            workerThread.onComplete(null);
        } catch (PaymentsRegionException e) {
            SignalStore.paymentsValues().setMobileCoinPaymentsEnabled(false);
            Log.w(TAG, "Problem enabling payments in region", e);
            workerThread.onError(Error.RegionError);
        } catch (NonSuccessfulResponseCodeException e2) {
            SignalStore.paymentsValues().setMobileCoinPaymentsEnabled(false);
            Log.w(TAG, "Problem enabling payments", e2);
            workerThread.onError(Error.NetworkError);
        } catch (IOException e3) {
            SignalStore.paymentsValues().setMobileCoinPaymentsEnabled(false);
            Log.w(TAG, "Problem enabling payments", e3);
            tryToRestoreProfile();
            workerThread.onError(Error.NetworkError);
        }
    }

    private void tryToRestoreProfile() {
        try {
            ProfileUtil.uploadProfile(ApplicationDependencies.getApplication());
            Log.i(TAG, "Restored profile");
        } catch (IOException e) {
            Log.w(TAG, "Problem uploading profile", e);
        }
    }

    public void deactivatePayments(Consumer<Boolean> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeRepository$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                PaymentsHomeRepository.lambda$deactivatePayments$1(Consumer.this);
            }
        });
    }

    public static /* synthetic */ void lambda$deactivatePayments$1(Consumer consumer) {
        SignalStore.paymentsValues().setMobileCoinPaymentsEnabled(false);
        ApplicationDependencies.getJobManager().add(new ProfileUploadJob());
        consumer.accept(Boolean.valueOf(!SignalStore.paymentsValues().mobileCoinPaymentsEnabled()));
    }
}
