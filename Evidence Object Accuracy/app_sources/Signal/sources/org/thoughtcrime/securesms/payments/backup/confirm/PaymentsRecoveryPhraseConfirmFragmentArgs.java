package org.thoughtcrime.securesms.payments.backup.confirm;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class PaymentsRecoveryPhraseConfirmFragmentArgs {
    private final HashMap arguments;

    private PaymentsRecoveryPhraseConfirmFragmentArgs() {
        this.arguments = new HashMap();
    }

    private PaymentsRecoveryPhraseConfirmFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static PaymentsRecoveryPhraseConfirmFragmentArgs fromBundle(Bundle bundle) {
        PaymentsRecoveryPhraseConfirmFragmentArgs paymentsRecoveryPhraseConfirmFragmentArgs = new PaymentsRecoveryPhraseConfirmFragmentArgs();
        bundle.setClassLoader(PaymentsRecoveryPhraseConfirmFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("finish_on_confirm")) {
            paymentsRecoveryPhraseConfirmFragmentArgs.arguments.put("finish_on_confirm", Boolean.valueOf(bundle.getBoolean("finish_on_confirm")));
            return paymentsRecoveryPhraseConfirmFragmentArgs;
        }
        throw new IllegalArgumentException("Required argument \"finish_on_confirm\" is missing and does not have an android:defaultValue");
    }

    public boolean getFinishOnConfirm() {
        return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("finish_on_confirm")) {
            bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PaymentsRecoveryPhraseConfirmFragmentArgs paymentsRecoveryPhraseConfirmFragmentArgs = (PaymentsRecoveryPhraseConfirmFragmentArgs) obj;
        return this.arguments.containsKey("finish_on_confirm") == paymentsRecoveryPhraseConfirmFragmentArgs.arguments.containsKey("finish_on_confirm") && getFinishOnConfirm() == paymentsRecoveryPhraseConfirmFragmentArgs.getFinishOnConfirm();
    }

    public int hashCode() {
        return 31 + (getFinishOnConfirm() ? 1 : 0);
    }

    public String toString() {
        return "PaymentsRecoveryPhraseConfirmFragmentArgs{finishOnConfirm=" + getFinishOnConfirm() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(PaymentsRecoveryPhraseConfirmFragmentArgs paymentsRecoveryPhraseConfirmFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(paymentsRecoveryPhraseConfirmFragmentArgs.arguments);
        }

        public Builder(boolean z) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("finish_on_confirm", Boolean.valueOf(z));
        }

        public PaymentsRecoveryPhraseConfirmFragmentArgs build() {
            return new PaymentsRecoveryPhraseConfirmFragmentArgs(this.arguments);
        }

        public Builder setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }
    }
}
