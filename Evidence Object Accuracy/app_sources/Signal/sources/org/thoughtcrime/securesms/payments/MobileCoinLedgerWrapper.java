package org.thoughtcrime.securesms.payments;

import com.google.protobuf.ByteString;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.payments.proto.MobileCoinLedger;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class MobileCoinLedgerWrapper {
    private final Balance balance;
    private final MobileCoinLedger ledger;

    public MobileCoinLedgerWrapper(MobileCoinLedger mobileCoinLedger) {
        Money.MobileCoin picoMobileCoin = Money.picoMobileCoin(mobileCoinLedger.getBalance());
        Money.MobileCoin picoMobileCoin2 = Money.picoMobileCoin(mobileCoinLedger.getTransferableBalance());
        this.ledger = mobileCoinLedger;
        this.balance = new Balance(picoMobileCoin, picoMobileCoin2, mobileCoinLedger.getAsOfTimeStamp());
    }

    public Balance getBalance() {
        return this.balance;
    }

    public byte[] serialize() {
        return this.ledger.toByteArray();
    }

    public List<OwnedTxo> getAllTxos() {
        ArrayList arrayList = new ArrayList(this.ledger.getSpentTxosCount() + this.ledger.getUnspentTxosCount());
        addAllMapped(arrayList, this.ledger.getSpentTxosList());
        addAllMapped(arrayList, this.ledger.getUnspentTxosList());
        return arrayList;
    }

    private static void addAllMapped(List<OwnedTxo> list, List<MobileCoinLedger.OwnedTXO> list2) {
        for (MobileCoinLedger.OwnedTXO ownedTXO : list2) {
            list.add(new OwnedTxo(ownedTXO));
        }
    }

    /* loaded from: classes4.dex */
    public static class OwnedTxo {
        private final MobileCoinLedger.OwnedTXO ownedTXO;

        OwnedTxo(MobileCoinLedger.OwnedTXO ownedTXO) {
            this.ownedTXO = ownedTXO;
        }

        public Money.MobileCoin getValue() {
            return Money.picoMobileCoin(this.ownedTXO.getAmount());
        }

        public ByteString getKeyImage() {
            return this.ownedTXO.getKeyImage();
        }

        public ByteString getPublicKey() {
            return this.ownedTXO.getPublicKey();
        }

        public long getReceivedInBlock() {
            return this.ownedTXO.getReceivedInBlock().getBlockNumber();
        }

        public Long getSpentInBlock() {
            return nullIfZero(this.ownedTXO.getSpentInBlock().getBlockNumber());
        }

        public boolean isSpent() {
            return this.ownedTXO.getSpentInBlock().getBlockNumber() != 0;
        }

        public Long getReceivedInBlockTimestamp() {
            return nullIfZero(this.ownedTXO.getReceivedInBlock().getTimestamp());
        }

        public Long getSpentInBlockTimestamp() {
            return nullIfZero(this.ownedTXO.getSpentInBlock().getTimestamp());
        }

        private Long nullIfZero(long j) {
            if (j == 0) {
                return null;
            }
            return Long.valueOf(j);
        }
    }
}
