package org.thoughtcrime.securesms.payments.backup.phrase;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.Stream;
import com.annimon.stream.function.IndexedFunction;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseViewModel;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public class PaymentsRecoveryPhraseFragment extends Fragment {
    private static final int SPAN_COUNT;

    public PaymentsRecoveryPhraseFragment() {
        super(R.layout.payments_recovery_phrase_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        List<String> list;
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.payments_recovery_phrase_fragment_toolbar);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.payments_recovery_phrase_fragment_recycler);
        TextView textView = (TextView) view.findViewById(R.id.payments_recovery_phrase_fragment_message);
        View findViewById = view.findViewById(R.id.payments_recovery_phrase_fragment_next);
        View findViewById2 = view.findViewById(R.id.payments_recovery_phrase_fragment_edit);
        View findViewById3 = view.findViewById(R.id.payments_recovery_phrase_fragment_copy);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(requireContext(), 2);
        PaymentsRecoveryPhraseFragmentArgs fromBundle = PaymentsRecoveryPhraseFragmentArgs.fromBundle(requireArguments());
        if (fromBundle.getWords() != null) {
            list = Arrays.asList(fromBundle.getWords());
            setUpForConfirmation(textView, findViewById, findViewById2, findViewById3, list);
        } else {
            List<String> words = SignalStore.paymentsValues().getPaymentsMnemonic().getWords();
            setUpForDisplay(textView, findViewById, findViewById2, findViewById3, words, fromBundle);
            list = words;
        }
        List list2 = Stream.of(list).mapIndexed(new IndexedFunction() { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.IndexedFunction
            public final Object apply(int i, Object obj) {
                return new MnemonicPart(i, (String) obj);
            }
        }).sorted(new MnemonicPartComparator(list.size(), 2, null)).toList();
        MnemonicPartAdapter mnemonicPartAdapter = new MnemonicPartAdapter();
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(mnemonicPartAdapter);
        recyclerView.setOverScrollMode(2);
        toolbar.setNavigationOnClickListener(new View.OnClickListener(fromBundle, toolbar, view) { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ PaymentsRecoveryPhraseFragmentArgs f$1;
            public final /* synthetic */ Toolbar f$2;
            public final /* synthetic */ View f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsRecoveryPhraseFragment.$r8$lambda$iy4laF3eMGYUqvHNyACRxGDHg2g(PaymentsRecoveryPhraseFragment.this, this.f$1, this.f$2, this.f$3, view2);
            }
        });
        mnemonicPartAdapter.submitList(list2);
    }

    public /* synthetic */ void lambda$onViewCreated$1(PaymentsRecoveryPhraseFragmentArgs paymentsRecoveryPhraseFragmentArgs, Toolbar toolbar, View view, View view2) {
        if (paymentsRecoveryPhraseFragmentArgs.getFinishOnConfirm()) {
            requireActivity().finish();
        } else {
            toolbar.setNavigationOnClickListener(new View.OnClickListener(view) { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda4
                public final /* synthetic */ View f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    PaymentsRecoveryPhraseFragment.$r8$lambda$ENCvVaTUuN78G2Xl4detHSsEknU(this.f$0, view3);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view, View view2) {
        Navigation.findNavController(view).popBackStack(R.id.paymentsHome, false);
    }

    private void copyWordsToClipboard(List<String> list) {
        ServiceUtil.getClipboardManager(requireContext()).setPrimaryClip(ClipData.newPlainText(getString(R.string.app_name), Util.join(list, " ")));
        ServiceUtil.getAlarmManager(requireContext()).set(0, System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(30), PendingIntent.getBroadcast(requireContext(), 0, new Intent(requireContext(), ClearClipboardAlarmReceiver.class), 0));
    }

    private void setUpForConfirmation(TextView textView, View view, View view2, View view3, List<String> list) {
        textView.setText(R.string.PaymentsRecoveryPhraseFragment__make_sure_youve_entered);
        view2.setVisibility(0);
        view3.setVisibility(8);
        PaymentsRecoveryPhraseViewModel paymentsRecoveryPhraseViewModel = (PaymentsRecoveryPhraseViewModel) ViewModelProviders.of(this).get(PaymentsRecoveryPhraseViewModel.class);
        view.setOnClickListener(new View.OnClickListener(list) { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda8
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view4) {
                PaymentsRecoveryPhraseFragment.$r8$lambda$WTg43ClRcdu1ReOaorihBwKvdDQ(PaymentsRecoveryPhraseViewModel.this, this.f$1, view4);
            }
        });
        view2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view4) {
                PaymentsRecoveryPhraseFragment.$r8$lambda$PIUQZeAWLtqxjDdwwVrbT9DJypI(view4);
            }
        });
        paymentsRecoveryPhraseViewModel.getSubmitResult().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda10
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsRecoveryPhraseFragment.m2370$r8$lambda$nViO2voYgQiuuEbDglG_TtnTVA(PaymentsRecoveryPhraseFragment.this, (PaymentsRecoveryPhraseViewModel.SubmitResult) obj);
            }
        });
    }

    public static /* synthetic */ void lambda$setUpForConfirmation$3(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    private void setUpForDisplay(TextView textView, View view, View view2, View view3, List<String> list, PaymentsRecoveryPhraseFragmentArgs paymentsRecoveryPhraseFragmentArgs) {
        textView.setText(getString(R.string.PaymentsRecoveryPhraseFragment__write_down_the_following_d_words, Integer.valueOf(list.size())));
        view.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view4) {
                PaymentsRecoveryPhraseFragment.$r8$lambda$6XqfRTalpRGpqqHjnuPF0rtgF5o(PaymentsRecoveryPhraseFragmentArgs.this, view4);
            }
        });
        view2.setVisibility(8);
        view3.setVisibility(0);
        view3.setOnClickListener(new View.OnClickListener(list) { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda6
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view4) {
                PaymentsRecoveryPhraseFragment.$r8$lambda$bPz4UpA5U0nK7W59DsNhyCkN43k(PaymentsRecoveryPhraseFragment.this, this.f$1, view4);
            }
        });
    }

    public static /* synthetic */ void lambda$setUpForDisplay$4(PaymentsRecoveryPhraseFragmentArgs paymentsRecoveryPhraseFragmentArgs, View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), PaymentsRecoveryPhraseFragmentDirections.actionPaymentsRecoveryPhraseToPaymentsRecoveryPhraseConfirm(paymentsRecoveryPhraseFragmentArgs.getFinishOnConfirm()));
    }

    public /* synthetic */ void lambda$setUpForDisplay$5(List list, View view) {
        confirmCopy(list);
    }

    private void confirmCopy(List<String> list) {
        new AlertDialog.Builder(requireContext()).setTitle(R.string.PaymentsRecoveryPhraseFragment__copy_to_clipboard).setMessage(R.string.PaymentsRecoveryPhraseFragment__if_you_choose_to_store).setPositiveButton(R.string.PaymentsRecoveryPhraseFragment__copy, new DialogInterface.OnClickListener(list) { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PaymentsRecoveryPhraseFragment.$r8$lambda$yrRUmxlCfSA2_W03tlA123Z6aLA(PaymentsRecoveryPhraseFragment.this, this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PaymentsRecoveryPhraseFragment.$r8$lambda$g7Mgo95cXoXzZUAiwwbwyG1oPDc(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$confirmCopy$6(List list, DialogInterface dialogInterface, int i) {
        copyWordsToClipboard(list);
    }

    /* renamed from: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$backup$phrase$PaymentsRecoveryPhraseViewModel$SubmitResult;

        static {
            int[] iArr = new int[PaymentsRecoveryPhraseViewModel.SubmitResult.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$backup$phrase$PaymentsRecoveryPhraseViewModel$SubmitResult = iArr;
            try {
                iArr[PaymentsRecoveryPhraseViewModel.SubmitResult.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$backup$phrase$PaymentsRecoveryPhraseViewModel$SubmitResult[PaymentsRecoveryPhraseViewModel.SubmitResult.ERROR.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public void onSubmitResult(PaymentsRecoveryPhraseViewModel.SubmitResult submitResult) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$backup$phrase$PaymentsRecoveryPhraseViewModel$SubmitResult[submitResult.ordinal()];
        if (i == 1) {
            Toast.makeText(requireContext(), (int) R.string.PaymentsRecoveryPhraseFragment__payments_account_restored, 1).show();
            Navigation.findNavController(requireView()).popBackStack(R.id.paymentsHome, false);
        } else if (i == 2) {
            new AlertDialog.Builder(requireContext()).setTitle(R.string.PaymentsRecoveryPhraseFragment__invalid_recovery_phrase).setMessage(R.string.PaymentsRecoveryPhraseFragment__make_sure_youve_entered_your_phrase_correctly_and_try_again).setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseFragment$$ExternalSyntheticLambda7
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    PaymentsRecoveryPhraseFragment.m2369$r8$lambda$A9fEelOWRon2GuqwQk2U5JEjmo(dialogInterface, i2);
                }
            }).show();
        }
    }

    /* loaded from: classes4.dex */
    private static class MnemonicPartComparator implements Comparator<MnemonicPart> {
        private final int partsPerSpan;

        /* synthetic */ MnemonicPartComparator(int i, int i2, AnonymousClass1 r3) {
            this(i, i2);
        }

        private MnemonicPartComparator(int i, int i2) {
            this.partsPerSpan = i / i2;
        }

        public int compare(MnemonicPart mnemonicPart, MnemonicPart mnemonicPart2) {
            int index = mnemonicPart.getIndex() % this.partsPerSpan;
            int index2 = mnemonicPart2.getIndex() % this.partsPerSpan;
            if (index != index2) {
                return Integer.compare(index, index2);
            }
            return Integer.compare(mnemonicPart.getIndex(), mnemonicPart2.getIndex());
        }
    }
}
