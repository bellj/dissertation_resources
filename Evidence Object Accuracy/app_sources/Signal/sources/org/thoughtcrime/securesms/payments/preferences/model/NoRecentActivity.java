package org.thoughtcrime.securesms.payments.preferences.model;

import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes4.dex */
public class NoRecentActivity implements MappingModel<NoRecentActivity> {
    public boolean areContentsTheSame(NoRecentActivity noRecentActivity) {
        return true;
    }

    public boolean areItemsTheSame(NoRecentActivity noRecentActivity) {
        return true;
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(NoRecentActivity noRecentActivity) {
        return MappingModel.CC.$default$getChangePayload(this, noRecentActivity);
    }
}
