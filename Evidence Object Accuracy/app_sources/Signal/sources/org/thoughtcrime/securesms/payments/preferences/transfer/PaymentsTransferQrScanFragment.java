package org.thoughtcrime.securesms.payments.preferences.transfer;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.signal.qr.kitkat.ScanListener;
import org.signal.qr.kitkat.ScanningThread;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.camera.CameraView;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddress;
import org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferViewModel;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class PaymentsTransferQrScanFragment extends LoggingFragment {
    private static final String TAG = Log.tag(PaymentsTransferQrScanFragment.class);
    private LinearLayout overlay;
    private CameraView scannerView;
    private ScanningThread scanningThread;
    private PaymentsTransferViewModel viewModel;

    public PaymentsTransferQrScanFragment() {
        super(R.layout.payments_transfer_qr_scan_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.overlay = (LinearLayout) view.findViewById(R.id.overlay);
        this.scannerView = (CameraView) view.findViewById(R.id.scanner);
        if (getResources().getConfiguration().orientation == 2) {
            this.overlay.setOrientation(0);
        } else {
            this.overlay.setOrientation(1);
        }
        this.viewModel = (PaymentsTransferViewModel) new ViewModelProvider(Navigation.findNavController(view).getViewModelStoreOwner(R.id.payments_transfer), new PaymentsTransferViewModel.Factory()).get(PaymentsTransferViewModel.class);
        ((Toolbar) view.findViewById(R.id.payments_transfer_scan_qr)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferQrScanFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsTransferQrScanFragment.$r8$lambda$eX1IDNsbfoTbXathC_GER9iH8Ro(view2);
            }
        });
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ScanningThread scanningThread = new ScanningThread();
        this.scanningThread = scanningThread;
        scanningThread.setScanListener(new ScanListener() { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferQrScanFragment$$ExternalSyntheticLambda1
            @Override // org.signal.qr.kitkat.ScanListener
            public final void onQrDataFound(String str) {
                PaymentsTransferQrScanFragment.m2428$r8$lambda$9e_DZhGayYVs0LMkyYpk7gwoho(PaymentsTransferQrScanFragment.this, str);
            }
        });
        this.scannerView.onResume();
        this.scannerView.setPreviewCallback(this.scanningThread);
        this.scanningThread.start();
    }

    public /* synthetic */ void lambda$onResume$2(String str) {
        ThreadUtil.runOnMain(new Runnable(str) { // from class: org.thoughtcrime.securesms.payments.preferences.transfer.PaymentsTransferQrScanFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PaymentsTransferQrScanFragment.$r8$lambda$B2ANELgqs4cn7qONA56OKkxTiOA(PaymentsTransferQrScanFragment.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onResume$1(String str) {
        try {
            this.viewModel.postQrData(MobileCoinPublicAddress.fromQr(str).getPaymentAddressBase58());
            SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), (int) R.id.action_paymentsScanQr_pop);
        } catch (MobileCoinPublicAddress.AddressException unused) {
            Log.e(TAG, "Not a valid address");
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        this.scannerView.onPause();
        this.scanningThread.stopScanning();
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.scannerView.onPause();
        if (configuration.orientation == 2) {
            this.overlay.setOrientation(0);
        } else {
            this.overlay.setOrientation(1);
        }
        this.scannerView.onResume();
        this.scannerView.setPreviewCallback(this.scanningThread);
    }
}
