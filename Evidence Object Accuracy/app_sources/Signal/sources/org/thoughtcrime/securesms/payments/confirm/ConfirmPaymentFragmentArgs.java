package org.thoughtcrime.securesms.payments.confirm;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.payments.CreatePaymentDetails;

/* loaded from: classes4.dex */
public class ConfirmPaymentFragmentArgs {
    private final HashMap arguments;

    private ConfirmPaymentFragmentArgs() {
        this.arguments = new HashMap();
    }

    private ConfirmPaymentFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static ConfirmPaymentFragmentArgs fromBundle(Bundle bundle) {
        ConfirmPaymentFragmentArgs confirmPaymentFragmentArgs = new ConfirmPaymentFragmentArgs();
        bundle.setClassLoader(ConfirmPaymentFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("createPaymentDetails")) {
            throw new IllegalArgumentException("Required argument \"createPaymentDetails\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(CreatePaymentDetails.class) || Serializable.class.isAssignableFrom(CreatePaymentDetails.class)) {
            CreatePaymentDetails createPaymentDetails = (CreatePaymentDetails) bundle.get("createPaymentDetails");
            if (createPaymentDetails != null) {
                confirmPaymentFragmentArgs.arguments.put("createPaymentDetails", createPaymentDetails);
                if (bundle.containsKey("finish_on_confirm")) {
                    confirmPaymentFragmentArgs.arguments.put("finish_on_confirm", Boolean.valueOf(bundle.getBoolean("finish_on_confirm")));
                } else {
                    confirmPaymentFragmentArgs.arguments.put("finish_on_confirm", Boolean.FALSE);
                }
                return confirmPaymentFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"createPaymentDetails\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(CreatePaymentDetails.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public CreatePaymentDetails getCreatePaymentDetails() {
        return (CreatePaymentDetails) this.arguments.get("createPaymentDetails");
    }

    public boolean getFinishOnConfirm() {
        return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("createPaymentDetails")) {
            CreatePaymentDetails createPaymentDetails = (CreatePaymentDetails) this.arguments.get("createPaymentDetails");
            if (Parcelable.class.isAssignableFrom(CreatePaymentDetails.class) || createPaymentDetails == null) {
                bundle.putParcelable("createPaymentDetails", (Parcelable) Parcelable.class.cast(createPaymentDetails));
            } else if (Serializable.class.isAssignableFrom(CreatePaymentDetails.class)) {
                bundle.putSerializable("createPaymentDetails", (Serializable) Serializable.class.cast(createPaymentDetails));
            } else {
                throw new UnsupportedOperationException(CreatePaymentDetails.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        if (this.arguments.containsKey("finish_on_confirm")) {
            bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
        } else {
            bundle.putBoolean("finish_on_confirm", false);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ConfirmPaymentFragmentArgs confirmPaymentFragmentArgs = (ConfirmPaymentFragmentArgs) obj;
        if (this.arguments.containsKey("createPaymentDetails") != confirmPaymentFragmentArgs.arguments.containsKey("createPaymentDetails")) {
            return false;
        }
        if (getCreatePaymentDetails() == null ? confirmPaymentFragmentArgs.getCreatePaymentDetails() == null : getCreatePaymentDetails().equals(confirmPaymentFragmentArgs.getCreatePaymentDetails())) {
            return this.arguments.containsKey("finish_on_confirm") == confirmPaymentFragmentArgs.arguments.containsKey("finish_on_confirm") && getFinishOnConfirm() == confirmPaymentFragmentArgs.getFinishOnConfirm();
        }
        return false;
    }

    public int hashCode() {
        return (((getCreatePaymentDetails() != null ? getCreatePaymentDetails().hashCode() : 0) + 31) * 31) + (getFinishOnConfirm() ? 1 : 0);
    }

    public String toString() {
        return "ConfirmPaymentFragmentArgs{createPaymentDetails=" + getCreatePaymentDetails() + ", finishOnConfirm=" + getFinishOnConfirm() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(ConfirmPaymentFragmentArgs confirmPaymentFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(confirmPaymentFragmentArgs.arguments);
        }

        public Builder(CreatePaymentDetails createPaymentDetails) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (createPaymentDetails != null) {
                hashMap.put("createPaymentDetails", createPaymentDetails);
                return;
            }
            throw new IllegalArgumentException("Argument \"createPaymentDetails\" is marked as non-null but was passed a null value.");
        }

        public ConfirmPaymentFragmentArgs build() {
            return new ConfirmPaymentFragmentArgs(this.arguments);
        }

        public Builder setCreatePaymentDetails(CreatePaymentDetails createPaymentDetails) {
            if (createPaymentDetails != null) {
                this.arguments.put("createPaymentDetails", createPaymentDetails);
                return this;
            }
            throw new IllegalArgumentException("Argument \"createPaymentDetails\" is marked as non-null but was passed a null value.");
        }

        public Builder setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        public CreatePaymentDetails getCreatePaymentDetails() {
            return (CreatePaymentDetails) this.arguments.get("createPaymentDetails");
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }
    }
}
