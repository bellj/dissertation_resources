package org.thoughtcrime.securesms.payments.backup.phrase;

import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.adapter.AlwaysChangedDiffUtil;

/* loaded from: classes4.dex */
final class MnemonicPartAdapter extends ListAdapter<MnemonicPart, ViewHolder> {
    public MnemonicPartAdapter() {
        super(new AlwaysChangedDiffUtil());
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mnemonic_part_adapter_item, viewGroup, false));
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.bind(getItem(i));
    }

    /* loaded from: classes4.dex */
    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView view;

        ViewHolder(TextView textView) {
            super(textView);
            this.view = textView;
        }

        void bind(MnemonicPart mnemonicPart) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            spannableStringBuilder.append(SpanUtil.color(ContextCompat.getColor(this.view.getContext(), R.color.payment_currency_code_foreground_color), String.valueOf(mnemonicPart.getIndex() + 1))).append((CharSequence) " ").append(SpanUtil.bold(mnemonicPart.getWord()));
            this.view.setText(spannableStringBuilder);
        }
    }
}
