package org.thoughtcrime.securesms.payments;

/* loaded from: classes4.dex */
public final class TransactionSubmissionResult {
    private final ErrorCode code;
    private final boolean defrag;
    private final PaymentTransactionId transaction;

    /* loaded from: classes4.dex */
    public enum ErrorCode {
        INSUFFICIENT_FUNDS,
        GENERIC_FAILURE,
        NETWORK_FAILURE,
        NONE
    }

    private TransactionSubmissionResult(PaymentTransactionId paymentTransactionId, ErrorCode errorCode, boolean z) {
        this.transaction = paymentTransactionId;
        this.code = errorCode;
        this.defrag = z;
    }

    public static TransactionSubmissionResult successfullySubmittedDefrag(PaymentTransactionId paymentTransactionId) {
        return new TransactionSubmissionResult(paymentTransactionId, ErrorCode.NONE, true);
    }

    public static TransactionSubmissionResult successfullySubmitted(PaymentTransactionId paymentTransactionId) {
        return new TransactionSubmissionResult(paymentTransactionId, ErrorCode.NONE, false);
    }

    public static TransactionSubmissionResult failure(ErrorCode errorCode, boolean z) {
        return new TransactionSubmissionResult(null, errorCode, z);
    }

    public PaymentTransactionId getTransactionId() {
        PaymentTransactionId paymentTransactionId = this.transaction;
        if (paymentTransactionId != null) {
            return paymentTransactionId;
        }
        throw new IllegalStateException();
    }

    public ErrorCode getErrorCode() {
        return this.code;
    }

    public boolean isDefrag() {
        return this.defrag;
    }
}
