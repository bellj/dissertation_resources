package org.thoughtcrime.securesms.payments.preferences;

import android.view.View;
import j$.util.function.Function;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.BaseSettingsAdapter$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.components.settings.SettingHeader;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter;
import org.thoughtcrime.securesms.payments.preferences.model.InProgress;
import org.thoughtcrime.securesms.payments.preferences.model.InfoCard;
import org.thoughtcrime.securesms.payments.preferences.model.IntroducingPayments;
import org.thoughtcrime.securesms.payments.preferences.model.NoRecentActivity;
import org.thoughtcrime.securesms.payments.preferences.model.PaymentItem;
import org.thoughtcrime.securesms.payments.preferences.model.SeeAll;
import org.thoughtcrime.securesms.payments.preferences.viewholder.InProgressViewHolder;
import org.thoughtcrime.securesms.payments.preferences.viewholder.InfoCardViewHolder;
import org.thoughtcrime.securesms.payments.preferences.viewholder.IntroducingPaymentViewHolder;
import org.thoughtcrime.securesms.payments.preferences.viewholder.NoRecentActivityViewHolder;
import org.thoughtcrime.securesms.payments.preferences.viewholder.PaymentItemViewHolder;
import org.thoughtcrime.securesms.payments.preferences.viewholder.SeeAllViewHolder;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class PaymentsHomeAdapter extends MappingAdapter {

    /* loaded from: classes4.dex */
    public interface Callbacks {

        /* renamed from: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter$Callbacks$-CC */
        /* loaded from: classes4.dex */
        public final /* synthetic */ class CC {
            public static void $default$onActivatePayments(Callbacks callbacks) {
            }

            public static void $default$onInfoCardDismissed(Callbacks callbacks) {
            }

            public static void $default$onPaymentItem(Callbacks callbacks, PaymentItem paymentItem) {
            }

            public static void $default$onRestorePaymentsAccount(Callbacks callbacks) {
            }

            public static void $default$onSeeAll(Callbacks callbacks, PaymentType paymentType) {
            }

            public static void $default$onUpdatePin(Callbacks callbacks) {
            }

            public static void $default$onViewRecoveryPhrase(Callbacks callbacks) {
            }
        }

        void onActivatePayments();

        void onInfoCardDismissed();

        void onPaymentItem(PaymentItem paymentItem);

        void onRestorePaymentsAccount();

        void onSeeAll(PaymentType paymentType);

        void onUpdatePin();

        void onViewRecoveryPhrase();
    }

    public PaymentsHomeAdapter(Callbacks callbacks) {
        registerFactory(IntroducingPayments.class, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PaymentsHomeAdapter.$r8$lambda$vJFPRd8AKn0WBeFqnYFgMqL5EVY(PaymentsHomeAdapter.Callbacks.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.payments_home_introducing_payments_item);
        registerFactory(NoRecentActivity.class, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new NoRecentActivityViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.payments_home_no_recent_activity_item);
        registerFactory(InProgress.class, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new InProgressViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.payments_home_in_progress);
        registerFactory(PaymentItem.class, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PaymentsHomeAdapter.$r8$lambda$I1E_tx12tZ7gwKYakDZrV4Oafvo(PaymentsHomeAdapter.Callbacks.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.payments_home_payment_item);
        registerFactory(SettingHeader.Item.class, new BaseSettingsAdapter$$ExternalSyntheticLambda2(), R.layout.base_settings_header_item);
        registerFactory(SeeAll.class, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PaymentsHomeAdapter.m2402$r8$lambda$aMLlIQFAzXCzwejBLk3pUx_fYg(PaymentsHomeAdapter.Callbacks.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.payments_home_see_all_item);
        registerFactory(InfoCard.class, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeAdapter$$ExternalSyntheticLambda5
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PaymentsHomeAdapter.$r8$lambda$0U8bMLPCIvO57ScBJIYFmLAZU_c(PaymentsHomeAdapter.Callbacks.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.payment_info_card);
    }

    public static /* synthetic */ MappingViewHolder lambda$new$0(Callbacks callbacks, View view) {
        return new IntroducingPaymentViewHolder(view, callbacks);
    }

    public static /* synthetic */ MappingViewHolder lambda$new$1(Callbacks callbacks, View view) {
        return new PaymentItemViewHolder(view, callbacks);
    }

    public static /* synthetic */ MappingViewHolder lambda$new$2(Callbacks callbacks, View view) {
        return new SeeAllViewHolder(view, callbacks);
    }

    public static /* synthetic */ MappingViewHolder lambda$new$3(Callbacks callbacks, View view) {
        return new InfoCardViewHolder(view, callbacks);
    }
}
