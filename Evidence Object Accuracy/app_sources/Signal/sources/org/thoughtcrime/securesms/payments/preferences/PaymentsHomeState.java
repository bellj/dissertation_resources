package org.thoughtcrime.securesms.payments.preferences;

import java.util.Collections;
import java.util.List;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.payments.currency.CurrencyExchange;
import org.thoughtcrime.securesms.payments.preferences.model.PaymentItem;

/* loaded from: classes4.dex */
public class PaymentsHomeState {
    private final CurrencyExchange currencyExchange;
    private final FiatMoney exchangeAmount;
    private final LoadState exchangeRateLoadState;
    private final List<PaymentItem> payments;
    private final PaymentsState paymentsState;
    private final boolean recentPaymentsLoaded;
    private final List<PaymentItem> requests;
    private final int totalPayments;

    /* loaded from: classes4.dex */
    public enum PaymentsState {
        NOT_ACTIVATED,
        ACTIVATING,
        ACTIVATED,
        DEACTIVATING,
        ACTIVATE_NOT_ALLOWED
    }

    public PaymentsHomeState(PaymentsState paymentsState) {
        this(paymentsState, null, Collections.emptyList(), Collections.emptyList(), 0, new CurrencyExchange(Collections.emptyMap(), 0), LoadState.INITIAL, false);
    }

    public PaymentsHomeState(PaymentsState paymentsState, FiatMoney fiatMoney, List<PaymentItem> list, List<PaymentItem> list2, int i, CurrencyExchange currencyExchange, LoadState loadState, boolean z) {
        this.paymentsState = paymentsState;
        this.exchangeAmount = fiatMoney;
        this.requests = list;
        this.payments = list2;
        this.totalPayments = i;
        this.currencyExchange = currencyExchange;
        this.exchangeRateLoadState = loadState;
        this.recentPaymentsLoaded = z;
    }

    public PaymentsState getPaymentsState() {
        return this.paymentsState;
    }

    public FiatMoney getExchangeAmount() {
        return this.exchangeAmount;
    }

    public List<PaymentItem> getRequests() {
        return this.requests;
    }

    public List<PaymentItem> getPayments() {
        return this.payments;
    }

    public int getTotalPayments() {
        return this.totalPayments;
    }

    public CurrencyExchange getCurrencyExchange() {
        return this.currencyExchange;
    }

    public LoadState getExchangeRateLoadState() {
        return this.exchangeRateLoadState;
    }

    public boolean isRecentPaymentsLoaded() {
        return this.recentPaymentsLoaded;
    }

    public PaymentsHomeState updatePaymentsEnabled(PaymentsState paymentsState) {
        return new PaymentsHomeState(paymentsState, this.exchangeAmount, this.requests, this.payments, this.totalPayments, this.currencyExchange, this.exchangeRateLoadState, this.recentPaymentsLoaded);
    }

    public PaymentsHomeState updatePayments(List<PaymentItem> list, int i) {
        return new PaymentsHomeState(this.paymentsState, this.exchangeAmount, this.requests, list, i, this.currencyExchange, this.exchangeRateLoadState, true);
    }

    public PaymentsHomeState updateCurrencyAmount(FiatMoney fiatMoney) {
        return new PaymentsHomeState(this.paymentsState, fiatMoney, this.requests, this.payments, this.totalPayments, this.currencyExchange, this.exchangeRateLoadState, this.recentPaymentsLoaded);
    }

    public PaymentsHomeState updateExchangeRateLoadState(LoadState loadState) {
        return new PaymentsHomeState(this.paymentsState, this.exchangeAmount, this.requests, this.payments, this.totalPayments, this.currencyExchange, loadState, this.recentPaymentsLoaded);
    }

    public PaymentsHomeState updateCurrencyExchange(CurrencyExchange currencyExchange, LoadState loadState) {
        return new PaymentsHomeState(this.paymentsState, this.exchangeAmount, this.requests, this.payments, this.totalPayments, currencyExchange, loadState, this.recentPaymentsLoaded);
    }
}
