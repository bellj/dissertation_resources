package org.thoughtcrime.securesms.payments;

/* loaded from: classes4.dex */
public final class PaymentsAddressException extends Exception {
    private final Code code;

    public PaymentsAddressException(Code code) {
        super(code.message);
        this.code = code;
    }

    public Code getCode() {
        return this.code;
    }

    /* loaded from: classes4.dex */
    public enum Code {
        NO_PROFILE_KEY("No profile key available"),
        NOT_ENABLED("Payments not enabled"),
        COULD_NOT_DECRYPT("Payment address could not be decrypted"),
        INVALID_ADDRESS("Invalid MobileCoin address on payments address proto"),
        INVALID_ADDRESS_SIGNATURE("Invalid MobileCoin address signature on payments address proto"),
        NO_ADDRESS("No MobileCoin address on payments address proto");
        
        private final String message;

        Code(String str) {
            this.message = str;
        }
    }
}
