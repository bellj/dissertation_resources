package org.thoughtcrime.securesms.payments.preferences;

/* loaded from: classes4.dex */
public enum PaymentCategory {
    ALL("all"),
    SENT("sent"),
    RECEIVED("received");
    
    private final String code;

    PaymentCategory(String str) {
        this.code = str;
    }

    public String getCode() {
        return this.code;
    }

    public static PaymentCategory forCode(String str) {
        PaymentCategory[] values = values();
        for (PaymentCategory paymentCategory : values) {
            if (paymentCategory.code.equals(str)) {
                return paymentCategory;
            }
        }
        return ALL;
    }
}
