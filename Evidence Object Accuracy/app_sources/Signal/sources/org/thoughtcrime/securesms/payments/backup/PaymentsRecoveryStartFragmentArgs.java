package org.thoughtcrime.securesms.payments.backup;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class PaymentsRecoveryStartFragmentArgs {
    private final HashMap arguments;

    private PaymentsRecoveryStartFragmentArgs() {
        this.arguments = new HashMap();
    }

    private PaymentsRecoveryStartFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static PaymentsRecoveryStartFragmentArgs fromBundle(Bundle bundle) {
        PaymentsRecoveryStartFragmentArgs paymentsRecoveryStartFragmentArgs = new PaymentsRecoveryStartFragmentArgs();
        bundle.setClassLoader(PaymentsRecoveryStartFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("is_restore")) {
            paymentsRecoveryStartFragmentArgs.arguments.put("is_restore", Boolean.valueOf(bundle.getBoolean("is_restore")));
        } else {
            paymentsRecoveryStartFragmentArgs.arguments.put("is_restore", Boolean.FALSE);
        }
        if (bundle.containsKey("finish_on_confirm")) {
            paymentsRecoveryStartFragmentArgs.arguments.put("finish_on_confirm", Boolean.valueOf(bundle.getBoolean("finish_on_confirm")));
        } else {
            paymentsRecoveryStartFragmentArgs.arguments.put("finish_on_confirm", Boolean.FALSE);
        }
        return paymentsRecoveryStartFragmentArgs;
    }

    public boolean getIsRestore() {
        return ((Boolean) this.arguments.get("is_restore")).booleanValue();
    }

    public boolean getFinishOnConfirm() {
        return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("is_restore")) {
            bundle.putBoolean("is_restore", ((Boolean) this.arguments.get("is_restore")).booleanValue());
        } else {
            bundle.putBoolean("is_restore", false);
        }
        if (this.arguments.containsKey("finish_on_confirm")) {
            bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
        } else {
            bundle.putBoolean("finish_on_confirm", false);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PaymentsRecoveryStartFragmentArgs paymentsRecoveryStartFragmentArgs = (PaymentsRecoveryStartFragmentArgs) obj;
        return this.arguments.containsKey("is_restore") == paymentsRecoveryStartFragmentArgs.arguments.containsKey("is_restore") && getIsRestore() == paymentsRecoveryStartFragmentArgs.getIsRestore() && this.arguments.containsKey("finish_on_confirm") == paymentsRecoveryStartFragmentArgs.arguments.containsKey("finish_on_confirm") && getFinishOnConfirm() == paymentsRecoveryStartFragmentArgs.getFinishOnConfirm();
    }

    public int hashCode() {
        return (((getIsRestore() ? 1 : 0) + 31) * 31) + (getFinishOnConfirm() ? 1 : 0);
    }

    public String toString() {
        return "PaymentsRecoveryStartFragmentArgs{isRestore=" + getIsRestore() + ", finishOnConfirm=" + getFinishOnConfirm() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(PaymentsRecoveryStartFragmentArgs paymentsRecoveryStartFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(paymentsRecoveryStartFragmentArgs.arguments);
        }

        public Builder() {
            this.arguments = new HashMap();
        }

        public PaymentsRecoveryStartFragmentArgs build() {
            return new PaymentsRecoveryStartFragmentArgs(this.arguments);
        }

        public Builder setIsRestore(boolean z) {
            this.arguments.put("is_restore", Boolean.valueOf(z));
            return this;
        }

        public Builder setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        public boolean getIsRestore() {
            return ((Boolean) this.arguments.get("is_restore")).booleanValue();
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }
    }
}
