package org.thoughtcrime.securesms.payments;

import java.math.BigInteger;
import org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValue;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class CryptoValueUtil {
    private CryptoValueUtil() {
    }

    public static CryptoValue moneyToCryptoValue(Money money) {
        CryptoValue.Builder newBuilder = CryptoValue.newBuilder();
        if (money instanceof Money.MobileCoin) {
            newBuilder.setMobileCoinValue(CryptoValue.MobileCoinValue.newBuilder().setPicoMobileCoin(((Money.MobileCoin) money).serializeAmountString()));
        }
        return newBuilder.build();
    }

    public static Money cryptoValueToMoney(CryptoValue cryptoValue) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$model$databaseprotos$CryptoValue$ValueCase[cryptoValue.getValueCase().ordinal()];
        if (i == 1) {
            return Money.picoMobileCoin(new BigInteger(cryptoValue.getMobileCoinValue().getPicoMobileCoin()));
        }
        if (i != 2) {
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    /* renamed from: org.thoughtcrime.securesms.payments.CryptoValueUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$model$databaseprotos$CryptoValue$ValueCase;

        static {
            int[] iArr = new int[CryptoValue.ValueCase.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$model$databaseprotos$CryptoValue$ValueCase = iArr;
            try {
                iArr[CryptoValue.ValueCase.MOBILECOINVALUE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$model$databaseprotos$CryptoValue$ValueCase[CryptoValue.ValueCase.VALUE_NOT_SET.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }
}
