package org.thoughtcrime.securesms.payments.backup.confirm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.annimon.stream.function.Function;
import java.security.SecureRandom;
import java.util.Random;
import org.thoughtcrime.securesms.payments.Mnemonic;
import org.thoughtcrime.securesms.payments.backup.PaymentsRecoveryRepository;
import org.thoughtcrime.securesms.util.livedata.Store;

/* loaded from: classes4.dex */
public class PaymentsRecoveryPhraseConfirmViewModel extends ViewModel {
    private final Mnemonic mnemonic;
    private final Random random = new SecureRandom();
    private final Store<PaymentsRecoveryPhraseConfirmViewState> viewState;

    public PaymentsRecoveryPhraseConfirmViewModel() {
        PaymentsRecoveryRepository paymentsRecoveryRepository = new PaymentsRecoveryRepository();
        this.mnemonic = paymentsRecoveryRepository.getMnemonic();
        this.viewState = new Store<>(PaymentsRecoveryPhraseConfirmViewState.init(-1, -1));
    }

    public void updateRandomIndices() {
        this.viewState.update(new Function(getRandomIndices()) { // from class: org.thoughtcrime.securesms.payments.backup.confirm.PaymentsRecoveryPhraseConfirmViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ int[] f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PaymentsRecoveryPhraseConfirmViewModel.lambda$updateRandomIndices$0(this.f$0, (PaymentsRecoveryPhraseConfirmViewState) obj);
            }
        });
    }

    public static /* synthetic */ PaymentsRecoveryPhraseConfirmViewState lambda$updateRandomIndices$0(int[] iArr, PaymentsRecoveryPhraseConfirmViewState paymentsRecoveryPhraseConfirmViewState) {
        return PaymentsRecoveryPhraseConfirmViewState.init(iArr[0], iArr[1]);
    }

    private int[] getRandomIndices() {
        int nextInt = this.random.nextInt(this.mnemonic.getWordCount());
        int nextInt2 = this.random.nextInt(this.mnemonic.getWordCount());
        while (nextInt == nextInt2) {
            nextInt2 = this.random.nextInt(this.mnemonic.getWordCount());
        }
        return new int[]{nextInt, nextInt2};
    }

    public LiveData<PaymentsRecoveryPhraseConfirmViewState> getViewState() {
        return this.viewState.getStateLiveData();
    }

    public /* synthetic */ PaymentsRecoveryPhraseConfirmViewState lambda$validateWord1$1(String str, PaymentsRecoveryPhraseConfirmViewState paymentsRecoveryPhraseConfirmViewState) {
        return paymentsRecoveryPhraseConfirmViewState.buildUpon().withValidWord1(this.mnemonic.getWords().get(paymentsRecoveryPhraseConfirmViewState.getWord1Index()).equals(str.toLowerCase())).build();
    }

    public void validateWord1(String str) {
        this.viewState.update(new Function(str) { // from class: org.thoughtcrime.securesms.payments.backup.confirm.PaymentsRecoveryPhraseConfirmViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PaymentsRecoveryPhraseConfirmViewModel.this.lambda$validateWord1$1(this.f$1, (PaymentsRecoveryPhraseConfirmViewState) obj);
            }
        });
    }

    public /* synthetic */ PaymentsRecoveryPhraseConfirmViewState lambda$validateWord2$2(String str, PaymentsRecoveryPhraseConfirmViewState paymentsRecoveryPhraseConfirmViewState) {
        return paymentsRecoveryPhraseConfirmViewState.buildUpon().withValidWord2(this.mnemonic.getWords().get(paymentsRecoveryPhraseConfirmViewState.getWord2Index()).equals(str.toLowerCase())).build();
    }

    public void validateWord2(String str) {
        this.viewState.update(new Function(str) { // from class: org.thoughtcrime.securesms.payments.backup.confirm.PaymentsRecoveryPhraseConfirmViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PaymentsRecoveryPhraseConfirmViewModel.this.lambda$validateWord2$2(this.f$1, (PaymentsRecoveryPhraseConfirmViewState) obj);
            }
        });
    }
}
