package org.thoughtcrime.securesms.payments.preferences;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import com.annimon.stream.function.IndexedFunction;
import com.annimon.stream.function.Predicate;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.SetUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.SettingHeader;
import org.thoughtcrime.securesms.components.settings.SettingProgress;
import org.thoughtcrime.securesms.components.settings.SingleSelectSetting;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.currency.CurrencyExchange;
import org.thoughtcrime.securesms.payments.currency.CurrencyExchangeRepository;
import org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.livedata.Store;

/* loaded from: classes4.dex */
public final class SetCurrencyViewModel extends ViewModel {
    private static final String TAG = Log.tag(SetCurrencyViewModel.class);
    private final LiveData<CurrencyListState> list;
    private final Store<SetCurrencyState> store;

    public SetCurrencyViewModel(CurrencyExchangeRepository currencyExchangeRepository) {
        Store<SetCurrencyState> store = new Store<>(new SetCurrencyState(SignalStore.paymentsValues().currentCurrency()));
        this.store = store;
        this.list = Transformations.map(store.getStateLiveData(), new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return SetCurrencyViewModel.this.createListState((SetCurrencyViewModel.SetCurrencyState) obj);
            }
        });
        store.update(SignalStore.paymentsValues().liveCurrentCurrency(), new Store.Action() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ((SetCurrencyViewModel.SetCurrencyState) obj2).updateCurrentCurrency((Currency) obj);
            }
        });
        currencyExchangeRepository.getCurrencyExchange(new AsynchronousCallback.WorkerThread<CurrencyExchange, Throwable>() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel.1
            public static /* synthetic */ SetCurrencyState lambda$onComplete$0(CurrencyExchange currencyExchange, SetCurrencyState setCurrencyState) {
                Objects.requireNonNull(currencyExchange);
                return setCurrencyState.updateCurrencyExchange(currencyExchange);
            }

            public void onComplete(CurrencyExchange currencyExchange) {
                SetCurrencyViewModel.this.store.update(new SetCurrencyViewModel$1$$ExternalSyntheticLambda1(currencyExchange));
            }

            public void onError(Throwable th) {
                Log.w(SetCurrencyViewModel.TAG, th);
                SetCurrencyViewModel.this.store.update(new SetCurrencyViewModel$1$$ExternalSyntheticLambda0());
            }

            public static /* synthetic */ SetCurrencyState lambda$onError$1(SetCurrencyState setCurrencyState) {
                return setCurrencyState.updateExchangeRateLoadState(LoadState.ERROR);
            }
        }, false);
    }

    public void select(Currency currency) {
        SignalStore.paymentsValues().setCurrentCurrency(currency);
    }

    public LiveData<CurrencyListState> getCurrencyListState() {
        return this.list;
    }

    public CurrencyListState createListState(SetCurrencyState setCurrencyState) {
        MappingModelList mappingModelList = new MappingModelList();
        boolean z = setCurrencyState.getCurrencyExchangeLoadState() == LoadState.LOADED;
        mappingModelList.addAll(fromCurrencies(setCurrencyState.getDefaultCurrencies(), setCurrencyState.getCurrentCurrency()));
        mappingModelList.add(new SettingHeader.Item((int) R.string.SetCurrencyFragment__all_currencies));
        if (z) {
            mappingModelList.addAll(fromCurrencies(setCurrencyState.getOtherCurrencies(), setCurrencyState.getCurrentCurrency()));
        } else {
            mappingModelList.add(new SettingProgress.Item());
        }
        return new CurrencyListState(mappingModelList, findSelectedIndex(mappingModelList), z);
    }

    private MappingModelList fromCurrencies(Collection<Currency> collection, Currency currency) {
        return (MappingModelList) Stream.of(collection).map(new com.annimon.stream.function.Function(currency) { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ Currency f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SetCurrencyViewModel.lambda$fromCurrencies$1(this.f$0, (Currency) obj);
            }
        }).sortBy(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((SingleSelectSetting.Item) obj).getText();
            }
        }).collect(MappingModelList.toMappingModelList());
    }

    public static /* synthetic */ SingleSelectSetting.Item lambda$fromCurrencies$1(Currency currency, Currency currency2) {
        return new SingleSelectSetting.Item(currency2, currency2.getDisplayName(Locale.getDefault()), currency2.getCurrencyCode(), currency2.equals(currency));
    }

    private int findSelectedIndex(MappingModelList mappingModelList) {
        return ((Integer) Stream.of(mappingModelList).mapIndexed(new IndexedFunction() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.IndexedFunction
            public final Object apply(int i, Object obj) {
                return new Pair(Integer.valueOf(i), (MappingModel) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return SetCurrencyViewModel.lambda$findSelectedIndex$2((Pair) obj);
            }
        }).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SetCurrencyViewModel.lambda$findSelectedIndex$3((Pair) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return SetCurrencyViewModel.lambda$findSelectedIndex$4((Pair) obj);
            }
        }).findFirst().map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return (Integer) ((Pair) obj).first();
            }
        }).orElse(-1)).intValue();
    }

    public static /* synthetic */ boolean lambda$findSelectedIndex$2(Pair pair) {
        return pair.second() instanceof SingleSelectSetting.Item;
    }

    public static /* synthetic */ Pair lambda$findSelectedIndex$3(Pair pair) {
        return new Pair((Integer) pair.first(), (SingleSelectSetting.Item) pair.second());
    }

    public static /* synthetic */ boolean lambda$findSelectedIndex$4(Pair pair) {
        return ((SingleSelectSetting.Item) pair.second()).isSelected();
    }

    /* loaded from: classes4.dex */
    public static class CurrencyListState {
        private final boolean isLoaded;
        private final MappingModelList items;
        private final int selectedIndex;

        public CurrencyListState(MappingModelList mappingModelList, int i, boolean z) {
            this.items = mappingModelList;
            this.isLoaded = z;
            this.selectedIndex = i;
        }

        public boolean isLoaded() {
            return this.isLoaded;
        }

        public MappingModelList getItems() {
            return this.items;
        }

        public int getSelectedIndex() {
            return this.selectedIndex;
        }
    }

    /* loaded from: classes4.dex */
    public static class SetCurrencyState {
        private static final List<Currency> DEFAULT_CURRENCIES = Stream.of(BuildConfig.DEFAULT_CURRENCIES.split(",")).map(new SetCurrencyViewModel$SetCurrencyState$$ExternalSyntheticLambda0()).withoutNulls().toList();
        private final CurrencyExchange currencyExchange;
        private final LoadState currencyExchangeLoadState;
        private final Currency currentCurrency;
        private final Collection<Currency> defaultCurrencies;
        private final Collection<Currency> otherCurrencies;

        public SetCurrencyState(Currency currency) {
            this(currency, new CurrencyExchange(Collections.emptyMap(), 0), LoadState.LOADING, DEFAULT_CURRENCIES, Collections.emptyList());
        }

        public SetCurrencyState(Currency currency, CurrencyExchange currencyExchange, LoadState loadState, Collection<Currency> collection, Collection<Currency> collection2) {
            this.currentCurrency = currency;
            this.currencyExchange = currencyExchange;
            this.currencyExchangeLoadState = loadState;
            this.defaultCurrencies = collection;
            this.otherCurrencies = collection2;
        }

        public Currency getCurrentCurrency() {
            return this.currentCurrency;
        }

        public LoadState getCurrencyExchangeLoadState() {
            return this.currencyExchangeLoadState;
        }

        public Collection<Currency> getDefaultCurrencies() {
            return this.defaultCurrencies;
        }

        public Collection<Currency> getOtherCurrencies() {
            return this.otherCurrencies;
        }

        public SetCurrencyState updateExchangeRateLoadState(LoadState loadState) {
            return new SetCurrencyState(this.currentCurrency, this.currencyExchange, loadState, this.defaultCurrencies, this.otherCurrencies);
        }

        public SetCurrencyState updateCurrencyExchange(CurrencyExchange currencyExchange) {
            List<Currency> supportedCurrencies = currencyExchange.getSupportedCurrencies();
            Set intersection = SetUtil.intersection(supportedCurrencies, DEFAULT_CURRENCIES);
            return new SetCurrencyState(this.currentCurrency, currencyExchange, LoadState.LOADED, intersection, SetUtil.difference(supportedCurrencies, intersection));
        }

        public SetCurrencyState updateCurrentCurrency(Currency currency) {
            return new SetCurrencyState(currency, this.currencyExchange, this.currencyExchangeLoadState, this.defaultCurrencies, this.otherCurrencies);
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new SetCurrencyViewModel(new CurrencyExchangeRepository(ApplicationDependencies.getPayments())));
        }
    }
}
