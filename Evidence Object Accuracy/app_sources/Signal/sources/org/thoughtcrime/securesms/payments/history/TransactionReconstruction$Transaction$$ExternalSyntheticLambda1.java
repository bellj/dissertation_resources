package org.thoughtcrime.securesms.payments.history;

import java.util.Comparator;
import org.thoughtcrime.securesms.payments.history.TransactionReconstruction;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class TransactionReconstruction$Transaction$$ExternalSyntheticLambda1 implements Comparator {
    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return TransactionReconstruction.Transaction.lambda$static$1((TransactionReconstruction.Transaction) obj, (TransactionReconstruction.Transaction) obj2);
    }
}
