package org.thoughtcrime.securesms.payments.currency;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import java.util.Currency;
import java.util.Locale;

/* loaded from: classes4.dex */
public final class CurrencyUtil {
    public static Currency getCurrencyByCurrencyCode(String str) {
        try {
            return Currency.getInstance(str);
        } catch (IllegalArgumentException unused) {
            return null;
        }
    }

    public static Currency getCurrencyByE164(String str) {
        try {
            String regionCodeForNumber = PhoneNumberUtil.getInstance().getRegionCodeForNumber(PhoneNumberUtil.getInstance().parse(str, ""));
            Locale[] availableLocales = Locale.getAvailableLocales();
            for (Locale locale : availableLocales) {
                if (locale.getCountry().equals(regionCodeForNumber)) {
                    return getCurrencyByLocale(locale);
                }
            }
        } catch (NumberParseException unused) {
        }
        return null;
    }

    public static Currency getCurrencyByLocale(Locale locale) {
        if (locale == null) {
            return null;
        }
        try {
            return Currency.getInstance(locale);
        } catch (IllegalArgumentException unused) {
            return null;
        }
    }
}
