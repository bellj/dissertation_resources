package org.thoughtcrime.securesms.payments.proto;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public final class PaymentMetaData extends GeneratedMessageLite<PaymentMetaData, Builder> implements PaymentMetaDataOrBuilder {
    private static final PaymentMetaData DEFAULT_INSTANCE;
    public static final int MOBILECOINTXOIDENTIFICATION_FIELD_NUMBER;
    private static volatile Parser<PaymentMetaData> PARSER;
    private MobileCoinTxoIdentification mobileCoinTxoIdentification_;

    /* loaded from: classes4.dex */
    public interface MobileCoinTxoIdentificationOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        ByteString getKeyImages(int i);

        int getKeyImagesCount();

        List<ByteString> getKeyImagesList();

        ByteString getPublicKey(int i);

        int getPublicKeyCount();

        List<ByteString> getPublicKeyList();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private PaymentMetaData() {
    }

    /* loaded from: classes4.dex */
    public static final class MobileCoinTxoIdentification extends GeneratedMessageLite<MobileCoinTxoIdentification, Builder> implements MobileCoinTxoIdentificationOrBuilder {
        private static final MobileCoinTxoIdentification DEFAULT_INSTANCE;
        public static final int KEYIMAGES_FIELD_NUMBER;
        private static volatile Parser<MobileCoinTxoIdentification> PARSER;
        public static final int PUBLICKEY_FIELD_NUMBER;
        private Internal.ProtobufList<ByteString> keyImages_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<ByteString> publicKey_ = GeneratedMessageLite.emptyProtobufList();

        private MobileCoinTxoIdentification() {
        }

        @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
        public List<ByteString> getPublicKeyList() {
            return this.publicKey_;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
        public int getPublicKeyCount() {
            return this.publicKey_.size();
        }

        @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
        public ByteString getPublicKey(int i) {
            return this.publicKey_.get(i);
        }

        private void ensurePublicKeyIsMutable() {
            Internal.ProtobufList<ByteString> protobufList = this.publicKey_;
            if (!protobufList.isModifiable()) {
                this.publicKey_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setPublicKey(int i, ByteString byteString) {
            byteString.getClass();
            ensurePublicKeyIsMutable();
            this.publicKey_.set(i, byteString);
        }

        public void addPublicKey(ByteString byteString) {
            byteString.getClass();
            ensurePublicKeyIsMutable();
            this.publicKey_.add(byteString);
        }

        public void addAllPublicKey(Iterable<? extends ByteString> iterable) {
            ensurePublicKeyIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.publicKey_);
        }

        public void clearPublicKey() {
            this.publicKey_ = GeneratedMessageLite.emptyProtobufList();
        }

        @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
        public List<ByteString> getKeyImagesList() {
            return this.keyImages_;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
        public int getKeyImagesCount() {
            return this.keyImages_.size();
        }

        @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
        public ByteString getKeyImages(int i) {
            return this.keyImages_.get(i);
        }

        private void ensureKeyImagesIsMutable() {
            Internal.ProtobufList<ByteString> protobufList = this.keyImages_;
            if (!protobufList.isModifiable()) {
                this.keyImages_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setKeyImages(int i, ByteString byteString) {
            byteString.getClass();
            ensureKeyImagesIsMutable();
            this.keyImages_.set(i, byteString);
        }

        public void addKeyImages(ByteString byteString) {
            byteString.getClass();
            ensureKeyImagesIsMutable();
            this.keyImages_.add(byteString);
        }

        public void addAllKeyImages(Iterable<? extends ByteString> iterable) {
            ensureKeyImagesIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.keyImages_);
        }

        public void clearKeyImages() {
            this.keyImages_ = GeneratedMessageLite.emptyProtobufList();
        }

        public static MobileCoinTxoIdentification parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static MobileCoinTxoIdentification parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static MobileCoinTxoIdentification parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static MobileCoinTxoIdentification parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static MobileCoinTxoIdentification parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static MobileCoinTxoIdentification parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static MobileCoinTxoIdentification parseFrom(InputStream inputStream) throws IOException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static MobileCoinTxoIdentification parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static MobileCoinTxoIdentification parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static MobileCoinTxoIdentification parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static MobileCoinTxoIdentification parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static MobileCoinTxoIdentification parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (MobileCoinTxoIdentification) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(MobileCoinTxoIdentification mobileCoinTxoIdentification) {
            return DEFAULT_INSTANCE.createBuilder(mobileCoinTxoIdentification);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinTxoIdentification, Builder> implements MobileCoinTxoIdentificationOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(MobileCoinTxoIdentification.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
            public List<ByteString> getPublicKeyList() {
                return Collections.unmodifiableList(((MobileCoinTxoIdentification) this.instance).getPublicKeyList());
            }

            @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
            public int getPublicKeyCount() {
                return ((MobileCoinTxoIdentification) this.instance).getPublicKeyCount();
            }

            @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
            public ByteString getPublicKey(int i) {
                return ((MobileCoinTxoIdentification) this.instance).getPublicKey(i);
            }

            public Builder setPublicKey(int i, ByteString byteString) {
                copyOnWrite();
                ((MobileCoinTxoIdentification) this.instance).setPublicKey(i, byteString);
                return this;
            }

            public Builder addPublicKey(ByteString byteString) {
                copyOnWrite();
                ((MobileCoinTxoIdentification) this.instance).addPublicKey(byteString);
                return this;
            }

            public Builder addAllPublicKey(Iterable<? extends ByteString> iterable) {
                copyOnWrite();
                ((MobileCoinTxoIdentification) this.instance).addAllPublicKey(iterable);
                return this;
            }

            public Builder clearPublicKey() {
                copyOnWrite();
                ((MobileCoinTxoIdentification) this.instance).clearPublicKey();
                return this;
            }

            @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
            public List<ByteString> getKeyImagesList() {
                return Collections.unmodifiableList(((MobileCoinTxoIdentification) this.instance).getKeyImagesList());
            }

            @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
            public int getKeyImagesCount() {
                return ((MobileCoinTxoIdentification) this.instance).getKeyImagesCount();
            }

            @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaData.MobileCoinTxoIdentificationOrBuilder
            public ByteString getKeyImages(int i) {
                return ((MobileCoinTxoIdentification) this.instance).getKeyImages(i);
            }

            public Builder setKeyImages(int i, ByteString byteString) {
                copyOnWrite();
                ((MobileCoinTxoIdentification) this.instance).setKeyImages(i, byteString);
                return this;
            }

            public Builder addKeyImages(ByteString byteString) {
                copyOnWrite();
                ((MobileCoinTxoIdentification) this.instance).addKeyImages(byteString);
                return this;
            }

            public Builder addAllKeyImages(Iterable<? extends ByteString> iterable) {
                copyOnWrite();
                ((MobileCoinTxoIdentification) this.instance).addAllKeyImages(iterable);
                return this;
            }

            public Builder clearKeyImages() {
                copyOnWrite();
                ((MobileCoinTxoIdentification) this.instance).clearKeyImages();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new MobileCoinTxoIdentification();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0002\u0000\u0001\u001c\u0002\u001c", new Object[]{"publicKey_", "keyImages_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<MobileCoinTxoIdentification> parser = PARSER;
                    if (parser == null) {
                        synchronized (MobileCoinTxoIdentification.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            MobileCoinTxoIdentification mobileCoinTxoIdentification = new MobileCoinTxoIdentification();
            DEFAULT_INSTANCE = mobileCoinTxoIdentification;
            GeneratedMessageLite.registerDefaultInstance(MobileCoinTxoIdentification.class, mobileCoinTxoIdentification);
        }

        public static MobileCoinTxoIdentification getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<MobileCoinTxoIdentification> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.payments.proto.PaymentMetaData$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaDataOrBuilder
    public boolean hasMobileCoinTxoIdentification() {
        return this.mobileCoinTxoIdentification_ != null;
    }

    @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaDataOrBuilder
    public MobileCoinTxoIdentification getMobileCoinTxoIdentification() {
        MobileCoinTxoIdentification mobileCoinTxoIdentification = this.mobileCoinTxoIdentification_;
        return mobileCoinTxoIdentification == null ? MobileCoinTxoIdentification.getDefaultInstance() : mobileCoinTxoIdentification;
    }

    public void setMobileCoinTxoIdentification(MobileCoinTxoIdentification mobileCoinTxoIdentification) {
        mobileCoinTxoIdentification.getClass();
        this.mobileCoinTxoIdentification_ = mobileCoinTxoIdentification;
    }

    public void mergeMobileCoinTxoIdentification(MobileCoinTxoIdentification mobileCoinTxoIdentification) {
        mobileCoinTxoIdentification.getClass();
        MobileCoinTxoIdentification mobileCoinTxoIdentification2 = this.mobileCoinTxoIdentification_;
        if (mobileCoinTxoIdentification2 == null || mobileCoinTxoIdentification2 == MobileCoinTxoIdentification.getDefaultInstance()) {
            this.mobileCoinTxoIdentification_ = mobileCoinTxoIdentification;
        } else {
            this.mobileCoinTxoIdentification_ = MobileCoinTxoIdentification.newBuilder(this.mobileCoinTxoIdentification_).mergeFrom((MobileCoinTxoIdentification.Builder) mobileCoinTxoIdentification).buildPartial();
        }
    }

    public void clearMobileCoinTxoIdentification() {
        this.mobileCoinTxoIdentification_ = null;
    }

    public static PaymentMetaData parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (PaymentMetaData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static PaymentMetaData parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (PaymentMetaData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static PaymentMetaData parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (PaymentMetaData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static PaymentMetaData parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (PaymentMetaData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static PaymentMetaData parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (PaymentMetaData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static PaymentMetaData parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (PaymentMetaData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static PaymentMetaData parseFrom(InputStream inputStream) throws IOException {
        return (PaymentMetaData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static PaymentMetaData parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (PaymentMetaData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static PaymentMetaData parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (PaymentMetaData) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static PaymentMetaData parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (PaymentMetaData) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static PaymentMetaData parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (PaymentMetaData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static PaymentMetaData parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (PaymentMetaData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(PaymentMetaData paymentMetaData) {
        return DEFAULT_INSTANCE.createBuilder(paymentMetaData);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<PaymentMetaData, Builder> implements PaymentMetaDataOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(PaymentMetaData.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaDataOrBuilder
        public boolean hasMobileCoinTxoIdentification() {
            return ((PaymentMetaData) this.instance).hasMobileCoinTxoIdentification();
        }

        @Override // org.thoughtcrime.securesms.payments.proto.PaymentMetaDataOrBuilder
        public MobileCoinTxoIdentification getMobileCoinTxoIdentification() {
            return ((PaymentMetaData) this.instance).getMobileCoinTxoIdentification();
        }

        public Builder setMobileCoinTxoIdentification(MobileCoinTxoIdentification mobileCoinTxoIdentification) {
            copyOnWrite();
            ((PaymentMetaData) this.instance).setMobileCoinTxoIdentification(mobileCoinTxoIdentification);
            return this;
        }

        public Builder setMobileCoinTxoIdentification(MobileCoinTxoIdentification.Builder builder) {
            copyOnWrite();
            ((PaymentMetaData) this.instance).setMobileCoinTxoIdentification(builder.build());
            return this;
        }

        public Builder mergeMobileCoinTxoIdentification(MobileCoinTxoIdentification mobileCoinTxoIdentification) {
            copyOnWrite();
            ((PaymentMetaData) this.instance).mergeMobileCoinTxoIdentification(mobileCoinTxoIdentification);
            return this;
        }

        public Builder clearMobileCoinTxoIdentification() {
            copyOnWrite();
            ((PaymentMetaData) this.instance).clearMobileCoinTxoIdentification();
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new PaymentMetaData();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\t", new Object[]{"mobileCoinTxoIdentification_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<PaymentMetaData> parser = PARSER;
                if (parser == null) {
                    synchronized (PaymentMetaData.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        PaymentMetaData paymentMetaData = new PaymentMetaData();
        DEFAULT_INSTANCE = paymentMetaData;
        GeneratedMessageLite.registerDefaultInstance(PaymentMetaData.class, paymentMetaData);
    }

    public static PaymentMetaData getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<PaymentMetaData> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
