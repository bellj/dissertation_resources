package org.thoughtcrime.securesms.payments.reconciliation;

import com.annimon.stream.Collectors;
import com.annimon.stream.ComparatorCompat;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.google.protobuf.ByteString;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.signal.core.util.MapUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.payments.MobileCoinLedgerWrapper;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.PaymentDecorator;
import org.thoughtcrime.securesms.payments.ReconstructedPayment;
import org.thoughtcrime.securesms.payments.State;
import org.thoughtcrime.securesms.payments.history.TransactionReconstruction;
import org.thoughtcrime.securesms.payments.proto.PaymentMetaData;
import org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class LedgerReconcile {
    private static final String TAG = Log.tag(LedgerReconcile.class);

    public static /* synthetic */ Payment lambda$reconcile$3(Payment payment) {
        return payment;
    }

    public static List<Payment> reconcile(Collection<? extends Payment> collection, MobileCoinLedgerWrapper mobileCoinLedgerWrapper) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            List<Payment> reconcile = reconcile(collection, mobileCoinLedgerWrapper.getAllTxos());
            Log.d(TAG, String.format(Locale.US, "Took %d ms - Ledger %d, Local %d", Long.valueOf(System.currentTimeMillis() - currentTimeMillis), Integer.valueOf(mobileCoinLedgerWrapper.getAllTxos().size()), Integer.valueOf(collection.size())));
            return reconcile;
        } catch (Throwable th) {
            Log.d(TAG, String.format(Locale.US, "Took %d ms - Ledger %d, Local %d", Long.valueOf(System.currentTimeMillis() - currentTimeMillis), Integer.valueOf(mobileCoinLedgerWrapper.getAllTxos().size()), Integer.valueOf(collection.size())));
            throw th;
        }
    }

    public static /* synthetic */ boolean lambda$reconcile$0(Payment payment) {
        return payment.getState() != State.FAILED;
    }

    private static List<Payment> reconcile(Collection<? extends Payment> collection, List<MobileCoinLedgerWrapper.OwnedTxo> list) {
        List<Payment> list2 = Stream.of(collection).filter(new Predicate() { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return LedgerReconcile.lambda$reconcile$0((Payment) obj);
            }
        }).toList();
        HashSet hashSet = new HashSet(list2.size());
        HashSet hashSet2 = new HashSet(list2.size());
        for (Payment payment : list2) {
            PaymentMetaData.MobileCoinTxoIdentification mobileCoinTxoIdentification = payment.getPaymentMetaData().getMobileCoinTxoIdentification();
            hashSet.addAll(mobileCoinTxoIdentification.getPublicKeyList());
            hashSet2.addAll(mobileCoinTxoIdentification.getKeyImagesList());
        }
        HashSet hashSet3 = new HashSet(list);
        hashSet3.removeAll((Set) Stream.of(list).filter(new Predicate(hashSet) { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda4
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return LedgerReconcile.lambda$reconcile$2(this.f$0, (MobileCoinLedgerWrapper.OwnedTxo) obj);
            }
        }).collect(Collectors.toSet()));
        Set set = (Set) Stream.of(list).filter(new LedgerReconcile$$ExternalSyntheticLambda5()).collect(Collectors.toSet());
        set.removeAll((Set) Stream.of(list).filter(new Predicate(hashSet2) { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda3
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return LedgerReconcile.lambda$reconcile$1(this.f$0, (MobileCoinLedgerWrapper.OwnedTxo) obj);
            }
        }).collect(Collectors.toSet()));
        if (hashSet3.isEmpty() && set.isEmpty()) {
            return Stream.of(collection).map(new Function() { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda6
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return LedgerReconcile.lambda$reconcile$3((Payment) obj);
                }
            }).toList();
        }
        List<DetailedTransaction> reconstructAllTransactions = reconstructAllTransactions(hashSet3, set);
        ArrayList arrayList = new ArrayList(reconstructAllTransactions.size());
        List<Payment> decoratePaymentsWithBlockIndexes = decoratePaymentsWithBlockIndexes(collection, list);
        for (DetailedTransaction detailedTransaction : reconstructAllTransactions) {
            arrayList.add(new ReconstructedPayment(detailedTransaction.blockDetail.getBlockIndex(), detailedTransaction.blockDetail.getBlockTimestampOrZero(), detailedTransaction.transaction.getDirection(), detailedTransaction.transaction.getValue()));
        }
        Collections.sort(arrayList, Payment.DESCENDING_BLOCK_INDEX);
        return ZipList.zipList(decoratePaymentsWithBlockIndexes, arrayList, Payment.DESCENDING_BLOCK_INDEX_UNKNOWN_FIRST);
    }

    public static /* synthetic */ boolean lambda$reconcile$1(Set set, MobileCoinLedgerWrapper.OwnedTxo ownedTxo) {
        return set.contains(ownedTxo.getKeyImage());
    }

    public static /* synthetic */ boolean lambda$reconcile$2(Set set, MobileCoinLedgerWrapper.OwnedTxo ownedTxo) {
        return set.contains(ownedTxo.getPublicKey());
    }

    private static List<Payment> decoratePaymentsWithBlockIndexes(Collection<? extends Payment> collection, List<MobileCoinLedgerWrapper.OwnedTxo> list) {
        ArrayList arrayList = new ArrayList(collection.size());
        HashMap hashMap = new HashMap(list.size() * 2);
        for (MobileCoinLedgerWrapper.OwnedTxo ownedTxo : list) {
            hashMap.put(ownedTxo.getPublicKey(), ownedTxo);
            hashMap.put(ownedTxo.getKeyImage(), ownedTxo);
        }
        for (Payment payment : collection) {
            arrayList.add(findBlock(payment, hashMap));
        }
        return arrayList;
    }

    private static Payment findBlock(Payment payment, Map<ByteString, MobileCoinLedgerWrapper.OwnedTxo> map) {
        long j = 0;
        if (payment.getDirection().isReceived()) {
            for (ByteString byteString : payment.getPaymentMetaData().getMobileCoinTxoIdentification().getPublicKeyList()) {
                MobileCoinLedgerWrapper.OwnedTxo ownedTxo = map.get(byteString);
                if (ownedTxo != null) {
                    long receivedInBlock = ownedTxo.getReceivedInBlock();
                    if (ownedTxo.getReceivedInBlockTimestamp() != null) {
                        j = ownedTxo.getReceivedInBlockTimestamp().longValue();
                    }
                    return BlockOverridePayment.override(payment, receivedInBlock, j);
                }
            }
            return payment;
        }
        for (ByteString byteString2 : payment.getPaymentMetaData().getMobileCoinTxoIdentification().getKeyImagesList()) {
            MobileCoinLedgerWrapper.OwnedTxo ownedTxo2 = map.get(byteString2);
            if (!(ownedTxo2 == null || ownedTxo2.getSpentInBlock() == null)) {
                long longValue = ownedTxo2.getSpentInBlock().longValue();
                if (ownedTxo2.getSpentInBlockTimestamp() != null) {
                    j = ownedTxo2.getSpentInBlockTimestamp().longValue();
                }
                return BlockOverridePayment.override(payment, longValue, j);
            }
        }
        return payment;
    }

    /* loaded from: classes4.dex */
    public static class BlockDetail {
        public static final Comparator<BlockDetail> BLOCK_INDEX = new LedgerReconcile$BlockDetail$$ExternalSyntheticLambda0();
        private final long blockIndex;
        private final Long blockTimestamp;

        public static /* synthetic */ int lambda$static$0(BlockDetail blockDetail, BlockDetail blockDetail2) {
            return Long.compare(blockDetail.blockIndex, blockDetail2.blockIndex);
        }

        public BlockDetail(long j, Long l) {
            this.blockIndex = j;
            this.blockTimestamp = l;
        }

        public long getBlockTimestampOrZero() {
            Long l = this.blockTimestamp;
            if (l == null) {
                return 0;
            }
            return l.longValue();
        }

        public long getBlockIndex() {
            return this.blockIndex;
        }
    }

    /* loaded from: classes4.dex */
    public static class DetailedTransaction {
        public static final Comparator<DetailedTransaction> ASCENDING;
        private static final Comparator<DetailedTransaction> BLOCK_INDEX;
        public static final Comparator<DetailedTransaction> DESCENDING;
        private static final Comparator<DetailedTransaction> TRANSACTION;
        private final BlockDetail blockDetail;
        private final TransactionReconstruction.Transaction transaction;

        static {
            LedgerReconcile$DetailedTransaction$$ExternalSyntheticLambda0 ledgerReconcile$DetailedTransaction$$ExternalSyntheticLambda0 = new LedgerReconcile$DetailedTransaction$$ExternalSyntheticLambda0();
            BLOCK_INDEX = ledgerReconcile$DetailedTransaction$$ExternalSyntheticLambda0;
            LedgerReconcile$DetailedTransaction$$ExternalSyntheticLambda1 ledgerReconcile$DetailedTransaction$$ExternalSyntheticLambda1 = new LedgerReconcile$DetailedTransaction$$ExternalSyntheticLambda1();
            TRANSACTION = ledgerReconcile$DetailedTransaction$$ExternalSyntheticLambda1;
            ComparatorCompat thenComparing = ComparatorCompat.chain(ledgerReconcile$DetailedTransaction$$ExternalSyntheticLambda0).thenComparing((Comparator) ledgerReconcile$DetailedTransaction$$ExternalSyntheticLambda1);
            ASCENDING = thenComparing;
            DESCENDING = ComparatorCompat.reversed(thenComparing);
        }

        public static /* synthetic */ int lambda$static$0(DetailedTransaction detailedTransaction, DetailedTransaction detailedTransaction2) {
            return BlockDetail.BLOCK_INDEX.compare(detailedTransaction.blockDetail, detailedTransaction2.blockDetail);
        }

        public static /* synthetic */ int lambda$static$1(DetailedTransaction detailedTransaction, DetailedTransaction detailedTransaction2) {
            return TransactionReconstruction.Transaction.ORDER.compare(detailedTransaction.transaction, detailedTransaction2.transaction);
        }

        public DetailedTransaction(BlockDetail blockDetail, TransactionReconstruction.Transaction transaction) {
            this.blockDetail = blockDetail;
            this.transaction = transaction;
        }
    }

    private static List<DetailedTransaction> reconstructAllTransactions(Set<MobileCoinLedgerWrapper.OwnedTxo> set, Set<MobileCoinLedgerWrapper.OwnedTxo> set2) {
        Set set3 = (Set) Stream.of(set).map(new Function() { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return Long.valueOf(((MobileCoinLedgerWrapper.OwnedTxo) obj).getReceivedInBlock());
            }
        }).collect(Collectors.toSet());
        set3.addAll((Collection) Stream.of(set2).map(new Function() { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((MobileCoinLedgerWrapper.OwnedTxo) obj).getSpentInBlock();
            }
        }).collect(Collectors.toSet()));
        return Stream.of(set3).sorted(new Comparator() { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda9
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return ((Long) obj2).compareTo((Long) obj);
            }
        }).flatMap(new Function((Map) Stream.of(set).collect(Collectors.groupingBy(new Function() { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return Long.valueOf(((MobileCoinLedgerWrapper.OwnedTxo) obj).getReceivedInBlock());
            }
        })), (Map) Stream.of(set2).filter(new LedgerReconcile$$ExternalSyntheticLambda5()).collect(Collectors.groupingBy(new Function() { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((MobileCoinLedgerWrapper.OwnedTxo) obj).getSpentInBlock();
            }
        }))) { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda10
            public final /* synthetic */ Map f$0;
            public final /* synthetic */ Map f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return LedgerReconcile.lambda$reconstructAllTransactions$6(this.f$0, this.f$1, (Long) obj);
            }
        }).sorted(DetailedTransaction.DESCENDING).toList();
    }

    public static /* synthetic */ Stream lambda$reconstructAllTransactions$6(Map map, Map map2, Long l) {
        List list = (List) MapUtil.getOrDefault(map, l, Collections.emptyList());
        List list2 = (List) MapUtil.getOrDefault(map2, l, Collections.emptyList());
        if (list2.size() + list.size() != 0) {
            Long l2 = null;
            if (list2.size() > 0) {
                l2 = ((MobileCoinLedgerWrapper.OwnedTxo) list2.get(0)).getSpentInBlockTimestamp();
            }
            if (l2 == null && list.size() > 0) {
                l2 = ((MobileCoinLedgerWrapper.OwnedTxo) list.get(0)).getReceivedInBlockTimestamp();
            }
            return Stream.of(TransactionReconstruction.estimateBlockLevelActivity(toMobileCoinList(list2), toMobileCoinList(list)).getAllTransactions()).map(new Function() { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return LedgerReconcile.lambda$reconstructAllTransactions$5(LedgerReconcile.BlockDetail.this, (TransactionReconstruction.Transaction) obj);
                }
            });
        }
        throw new AssertionError();
    }

    public static /* synthetic */ DetailedTransaction lambda$reconstructAllTransactions$5(BlockDetail blockDetail, TransactionReconstruction.Transaction transaction) {
        return new DetailedTransaction(blockDetail, transaction);
    }

    private static List<Money.MobileCoin> toMobileCoinList(List<MobileCoinLedgerWrapper.OwnedTxo> list) {
        return Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((MobileCoinLedgerWrapper.OwnedTxo) obj).getValue();
            }
        }).toList();
    }

    /* loaded from: classes4.dex */
    public static class BlockOverridePayment extends PaymentDecorator {
        private final long blockIndex;
        private final long blockTimestamp;

        static Payment override(Payment payment, long j, long j2) {
            if (payment.getBlockTimestamp() == j2 && payment.getBlockIndex() == j) {
                return payment;
            }
            return new BlockOverridePayment(payment, j, j2);
        }

        private BlockOverridePayment(Payment payment, long j, long j2) {
            super(payment);
            this.blockIndex = j;
            this.blockTimestamp = j2;
        }

        @Override // org.thoughtcrime.securesms.payments.PaymentDecorator, org.thoughtcrime.securesms.payments.Payment
        public long getBlockIndex() {
            return this.blockIndex;
        }

        @Override // org.thoughtcrime.securesms.payments.PaymentDecorator, org.thoughtcrime.securesms.payments.Payment
        public long getBlockTimestamp() {
            long j = this.blockTimestamp;
            return j != 0 ? j : super.getBlockTimestamp();
        }
    }
}
