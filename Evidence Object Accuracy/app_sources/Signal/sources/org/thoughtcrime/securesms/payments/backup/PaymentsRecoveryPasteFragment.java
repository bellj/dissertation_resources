package org.thoughtcrime.securesms.payments.backup;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;
import org.whispersystems.signalservice.api.payments.PaymentsConstants;

/* loaded from: classes4.dex */
public class PaymentsRecoveryPasteFragment extends Fragment {
    public PaymentsRecoveryPasteFragment() {
        super(R.layout.payments_recovery_paste_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        EditText editText = (EditText) view.findViewById(R.id.payments_recovery_paste_fragment_phrase);
        View findViewById = view.findViewById(R.id.payments_recovery_paste_fragment_next);
        ((Toolbar) view.findViewById(R.id.payments_recovery_paste_fragment_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.PaymentsRecoveryPasteFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsRecoveryPasteFragment.$r8$lambda$PeNocQscRiOwoTaeyK8DKmp1xhs(view2);
            }
        });
        if (bundle == null) {
            findViewById.setEnabled(false);
        }
        editText.addTextChangedListener(new AfterTextChanged(new Consumer(findViewById) { // from class: org.thoughtcrime.securesms.payments.backup.PaymentsRecoveryPasteFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                PaymentsRecoveryPasteFragment.$r8$lambda$okin_Poj5FH_VaQH2evkLxJzxxk(this.f$0, (Editable) obj);
            }
        }));
        findViewById.setOnClickListener(new View.OnClickListener(editText) { // from class: org.thoughtcrime.securesms.payments.backup.PaymentsRecoveryPasteFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ EditText f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsRecoveryPasteFragment.$r8$lambda$lD4cAOmO5E91hEKGohtGFGcYmp4(PaymentsRecoveryPasteFragment.this, this.f$1, view2);
            }
        });
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    public static /* synthetic */ void lambda$onViewCreated$1(View view, Editable editable) {
        view.setEnabled(!editable.toString().isEmpty());
        view.setAlpha(!editable.toString().isEmpty() ? 1.0f : 0.5f);
    }

    public /* synthetic */ void lambda$onViewCreated$2(EditText editText, View view) {
        String[] split = editText.getText().toString().split("\\s+");
        if (split.length != PaymentsConstants.MNEMONIC_LENGTH) {
            showErrorDialog();
        } else {
            SafeNavigation.safeNavigate(Navigation.findNavController(view), PaymentsRecoveryPasteFragmentDirections.actionPaymentsRecoveryEntryToPaymentsRecoveryPhrase(false).setWords(split));
        }
    }

    private void showErrorDialog() {
        new AlertDialog.Builder(requireContext()).setTitle(R.string.PaymentsRecoveryPasteFragment__invalid_recovery_phrase).setMessage(getString(R.string.PaymentsRecoveryPasteFragment__make_sure, Integer.valueOf(PaymentsConstants.MNEMONIC_LENGTH))).setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.PaymentsRecoveryPasteFragment$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PaymentsRecoveryPasteFragment.$r8$lambda$ofIDP23d5InfFcclwFbP2_kqFuI(dialogInterface, i);
            }
        }).show();
    }
}
