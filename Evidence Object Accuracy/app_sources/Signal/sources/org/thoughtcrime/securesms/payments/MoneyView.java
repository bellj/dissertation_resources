package org.thoughtcrime.securesms.payments;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.DateUtils;
import org.whispersystems.signalservice.api.payments.Currency;
import org.whispersystems.signalservice.api.payments.FormatterOptions;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class MoneyView extends AppCompatTextView {
    private FormatterOptions formatterOptions;

    public MoneyView(Context context) {
        super(context);
        init(context, null);
    }

    public MoneyView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context, attributeSet);
    }

    public MoneyView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context, attributeSet);
    }

    public void init(Context context, AttributeSet attributeSet) {
        FormatterOptions.Builder builder = FormatterOptions.builder(Locale.getDefault());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.MoneyView, 0, 0);
        if (obtainStyledAttributes.getBoolean(0, false)) {
            builder.alwaysPrefixWithSign();
        }
        this.formatterOptions = builder.withoutSpaceBeforeUnit().build();
        String string = obtainStyledAttributes.getString(1);
        if (string != null) {
            try {
                setMoney(Money.parse(string));
            } catch (Money.ParseException e) {
                throw new AssertionError("Invalid money format", e);
            }
        }
        obtainStyledAttributes.recycle();
    }

    public String localizeAmountString(String str) {
        String valueOf = String.valueOf(DecimalFormatSymbols.getInstance().getDecimalSeparator());
        return str.replace(".", "__D__").replace(",", "__G__").replace("__D__", valueOf).replace("__G__", String.valueOf(DecimalFormatSymbols.getInstance().getGroupingSeparator()));
    }

    public void setMoney(String str, Currency currency) {
        SpannableString spannableString = new SpannableString(localizeAmountString(str) + currency.getCurrencyCode());
        int length = spannableString.length() - currency.getCurrencyCode().length();
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.payment_currency_code_foreground_color)), length, currency.getCurrencyCode().length() + length, 33);
        setText(spannableString);
    }

    public void setMoney(Money money) {
        setMoney(money, true, 0);
    }

    public void setMoney(Money money, boolean z) {
        setMoney(money, z, 0);
    }

    public void setMoney(Money money, boolean z, long j) {
        SpannableString spannableString;
        String money2 = money.toString(this.formatterOptions);
        int indexOf = money2.indexOf(money.getCurrency().getCurrencyCode());
        if (j > 0) {
            spannableString = new SpannableString(getResources().getString(R.string.CurrencyAmountFormatter_s_at_s, money2, DateUtils.getTimeString(ApplicationDependencies.getApplication(), Locale.getDefault(), j)));
        } else {
            spannableString = new SpannableString(money2);
        }
        if (z) {
            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.payment_currency_code_foreground_color)), indexOf, money.getCurrency().getCurrencyCode().length() + indexOf, 33);
        }
        setText(spannableString);
    }

    private static NumberFormat getMoneyFormat(int i) {
        NumberFormat numberInstance = NumberFormat.getNumberInstance(Locale.getDefault());
        numberInstance.setGroupingUsed(true);
        numberInstance.setMaximumFractionDigits(i);
        return numberInstance;
    }
}
