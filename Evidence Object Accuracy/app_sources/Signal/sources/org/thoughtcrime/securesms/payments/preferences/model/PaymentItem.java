package org.thoughtcrime.securesms.payments.preferences.model;

import android.content.Context;
import androidx.core.content.ContextCompat;
import com.annimon.stream.Stream;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.ReconstructedPayment;
import org.thoughtcrime.securesms.payments.State;
import org.thoughtcrime.securesms.payments.preferences.PaymentType;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda14;
import org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsParcelable;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel;
import org.whispersystems.signalservice.api.payments.FormatterOptions;

/* loaded from: classes4.dex */
public final class PaymentItem implements MappingModel<PaymentItem> {
    private final Payment payment;
    private final PaymentType paymentType;

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(PaymentItem paymentItem) {
        return MappingModel.CC.$default$getChangePayload(this, paymentItem);
    }

    public int getTransactionAvatar() {
        return R.drawable.ic_mobilecoin_avatar_24;
    }

    public static MappingModelList fromPayment(List<Payment> list) {
        return (MappingModelList) Stream.of(list).map(new PaymentsHomeViewModel$$ExternalSyntheticLambda14()).collect(MappingModelList.toMappingModelList());
    }

    public static PaymentItem fromPayment(Payment payment) {
        return new PaymentItem(payment, PaymentType.PAYMENT);
    }

    private PaymentItem(Payment payment, PaymentType paymentType) {
        this.payment = payment;
        this.paymentType = paymentType;
    }

    public PaymentDetailsParcelable getPaymentDetailsParcelable() {
        Payment payment = this.payment;
        if (payment instanceof ReconstructedPayment) {
            return PaymentDetailsParcelable.forPayment(payment);
        }
        return PaymentDetailsParcelable.forUuid(payment.getUuid());
    }

    public boolean isInProgress() {
        return this.payment.getState().isInProgress();
    }

    public boolean isUnread() {
        return !this.payment.isSeen();
    }

    public CharSequence getDate(Context context) {
        if (isInProgress()) {
            return context.getString(R.string.PaymentsHomeFragment__processing_payment);
        }
        if (this.payment.getState() == State.FAILED) {
            return SpanUtil.color(ContextCompat.getColor(context, R.color.signal_alert_primary), context.getString(R.string.PaymentsHomeFragment__payment_failed));
        }
        return context.getString(this.payment.getDirection().isReceived() ? R.string.PaymentsHomeFragment__received_s : R.string.PaymentsHomeFragment__sent_s, DateUtils.formatDateWithoutDayOfWeek(Locale.getDefault(), this.payment.getDisplayTimestamp()));
    }

    public String getAmount(Context context) {
        if (isInProgress() && this.payment.getDirection().isReceived()) {
            return context.getString(R.string.PaymentsHomeFragment__unknown_amount);
        }
        if (this.payment.getState() == State.FAILED) {
            return context.getString(R.string.PaymentsHomeFragment__details);
        }
        return this.payment.getAmountPlusFeeWithDirection().toString(FormatterOptions.builder(Locale.getDefault()).alwaysPrefixWithSign().withMaximumFractionDigits(4).build());
    }

    public int getAmountColor() {
        if (isInProgress()) {
            return R.color.signal_text_primary_disabled;
        }
        if (this.payment.getState() == State.FAILED) {
            return R.color.signal_text_secondary;
        }
        if (this.paymentType == PaymentType.REQUEST) {
            return R.color.core_grey_45;
        }
        return this.payment.getDirection().isReceived() ? R.color.core_green : R.color.signal_text_primary;
    }

    public boolean isDefrag() {
        return this.payment.isDefrag();
    }

    public boolean hasRecipient() {
        return this.payment.getPayee().hasRecipientId();
    }

    public String getTransactionName(Context context) {
        int i;
        if (this.payment.isDefrag()) {
            i = R.string.PaymentsHomeFragment__coin_cleanup_fee;
        } else {
            i = this.payment.getDirection().isSent() ? R.string.PaymentsHomeFragment__sent_payment : R.string.PaymentsHomeFragment__received_payment;
        }
        return context.getString(i);
    }

    public RecipientMappingModel.RecipientIdMappingModel getRecipientIdModel() {
        return new RecipientMappingModel.RecipientIdMappingModel(this.payment.getPayee().requireRecipientId());
    }

    public boolean areItemsTheSame(PaymentItem paymentItem) {
        return this.payment.getUuid().equals(paymentItem.payment.getUuid());
    }

    public boolean areContentsTheSame(PaymentItem paymentItem) {
        return this.payment.getDisplayTimestamp() == paymentItem.payment.getDisplayTimestamp() && this.payment.getAmount().equals(paymentItem.payment.getAmount()) && this.paymentType == paymentItem.paymentType && this.payment.getDirection() == paymentItem.payment.getDirection() && this.payment.getState() == paymentItem.payment.getState() && Objects.equals(this.payment.getPayee(), paymentItem.payment.getPayee()) && this.payment.isSeen() == paymentItem.payment.isSeen() && this.payment.isDefrag() == paymentItem.payment.isDefrag();
    }
}
