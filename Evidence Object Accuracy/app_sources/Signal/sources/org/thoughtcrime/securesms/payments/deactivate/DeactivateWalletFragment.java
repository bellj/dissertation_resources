package org.thoughtcrime.securesms.payments.deactivate;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.MoneyView;
import org.thoughtcrime.securesms.payments.deactivate.DeactivateWalletViewModel;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class DeactivateWalletFragment extends Fragment {
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.deactivate_wallet_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        MoneyView moneyView = (MoneyView) view.findViewById(R.id.deactivate_wallet_fragment_balance);
        View findViewById = view.findViewById(R.id.deactivate_wallet_fragment_transfer);
        View findViewById2 = view.findViewById(R.id.deactivate_wallet_fragment_deactivate);
        LearnMoreTextView learnMoreTextView = (LearnMoreTextView) view.findViewById(R.id.deactivate_wallet_fragment_notice);
        learnMoreTextView.setLearnMoreVisible(true);
        learnMoreTextView.setLink(getString(R.string.DeactivateWalletFragment__learn_more__we_recommend_transferring_your_funds));
        DeactivateWalletViewModel deactivateWalletViewModel = (DeactivateWalletViewModel) ViewModelProviders.of(this).get(DeactivateWalletViewModel.class);
        LiveData<Money> balance = deactivateWalletViewModel.getBalance();
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Objects.requireNonNull(moneyView);
        balance.observe(viewLifecycleOwner, new DeactivateWalletFragment$$ExternalSyntheticLambda2(moneyView));
        deactivateWalletViewModel.getDeactivationResults().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.payments.deactivate.DeactivateWalletFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                DeactivateWalletFragment.m2396$r8$lambda$CD4IuI7ckDJKUre5pKqfU8DwE4(DeactivateWalletFragment.this, (DeactivateWalletViewModel.Result) obj);
            }
        });
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.deactivate.DeactivateWalletFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DeactivateWalletFragment.$r8$lambda$6Pa8EAsoZ_M_9dM_QI9NLCrMrN0(DeactivateWalletFragment.this, view2);
            }
        });
        ((Toolbar) view.findViewById(R.id.deactivate_wallet_fragment_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.deactivate.DeactivateWalletFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DeactivateWalletFragment.m2394$r8$lambda$1VVP3_FC_36f1FXh46mET4Uyfo(DeactivateWalletFragment.this, view2);
            }
        });
        findViewById2.setOnClickListener(new View.OnClickListener(deactivateWalletViewModel) { // from class: org.thoughtcrime.securesms.payments.deactivate.DeactivateWalletFragment$$ExternalSyntheticLambda6
            public final /* synthetic */ DeactivateWalletViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DeactivateWalletFragment.$r8$lambda$B2mbAOjrj6f4z_JkZuOuIMJVhFw(DeactivateWalletFragment.this, this.f$1, view2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(DeactivateWalletViewModel.Result result) {
        if (result == DeactivateWalletViewModel.Result.SUCCESS) {
            Navigation.findNavController(requireView()).popBackStack();
        } else {
            Toast.makeText(requireContext(), (int) R.string.DeactivateWalletFragment__error_deactivating_wallet, 0).show();
        }
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), (int) R.id.action_deactivateWallet_to_paymentsTransfer);
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        Navigation.findNavController(requireView()).popBackStack();
    }

    public /* synthetic */ void lambda$onViewCreated$5(DeactivateWalletViewModel deactivateWalletViewModel, View view) {
        new AlertDialog.Builder(requireContext()).setTitle(R.string.DeactivateWalletFragment__deactivate_without_transferring_question).setMessage(R.string.DeactivateWalletFragment__your_balance_will_remain).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.deactivate.DeactivateWalletFragment$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeactivateWalletFragment.m2395$r8$lambda$96sMLtr6rr1nHtSq0o2cMH876I(dialogInterface, i);
            }
        }).setPositiveButton(SpanUtil.color(ContextCompat.getColor(requireContext(), R.color.signal_alert_primary), getString(R.string.DeactivateWalletFragment__deactivate)), new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.deactivate.DeactivateWalletFragment$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeactivateWalletFragment.$r8$lambda$UFLtgxcGTuldyrA6er4bcPWv5Ug(DeactivateWalletViewModel.this, dialogInterface, i);
            }
        }).show();
    }

    public static /* synthetic */ void lambda$onViewCreated$4(DeactivateWalletViewModel deactivateWalletViewModel, DialogInterface dialogInterface, int i) {
        deactivateWalletViewModel.deactivateWallet();
        dialogInterface.dismiss();
    }
}
