package org.thoughtcrime.securesms.payments.backup.phrase;

import androidx.core.util.Consumer;
import java.util.Collection;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.PaymentLedgerUpdateJob;
import org.thoughtcrime.securesms.jobs.ProfileUploadJob;
import org.thoughtcrime.securesms.keyvalue.PaymentsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class PaymentsRecoveryPhraseRepository {
    private static final String TAG = Log.tag(PaymentsRecoveryPhraseRepository.class);

    public void restoreMnemonic(List<String> list, Consumer<PaymentsValues.WalletRestoreResult> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable(list, consumer) { // from class: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$1;
            public final /* synthetic */ Consumer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PaymentsRecoveryPhraseRepository.m2371$r8$lambda$UqIIcgIs_DEma9e5DDhgopSeQ(PaymentsRecoveryPhraseRepository.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$restoreMnemonic$0(List list, Consumer consumer) {
        PaymentsValues.WalletRestoreResult restoreWallet = SignalStore.paymentsValues().restoreWallet(Util.join((Collection) list, " "));
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyvalue$PaymentsValues$WalletRestoreResult[restoreWallet.ordinal()];
        if (i == 1) {
            Log.i(TAG, "restoreMnemonic: mnemonic resulted in entropy mismatch, flushing cached values");
            SignalDatabase.payments().deleteAll();
            ApplicationDependencies.getPayments().closeWallet();
            updateProfileAndFetchLedger();
        } else if (i == 2) {
            Log.i(TAG, "restoreMnemonic: mnemonic resulted in entropy match, no flush needed.");
            updateProfileAndFetchLedger();
        } else if (i == 3) {
            Log.w(TAG, "restoreMnemonic: failed to restore wallet from given mnemonic.");
        }
        consumer.accept(restoreWallet);
    }

    /* renamed from: org.thoughtcrime.securesms.payments.backup.phrase.PaymentsRecoveryPhraseRepository$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$keyvalue$PaymentsValues$WalletRestoreResult;

        static {
            int[] iArr = new int[PaymentsValues.WalletRestoreResult.values().length];
            $SwitchMap$org$thoughtcrime$securesms$keyvalue$PaymentsValues$WalletRestoreResult = iArr;
            try {
                iArr[PaymentsValues.WalletRestoreResult.ENTROPY_CHANGED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$PaymentsValues$WalletRestoreResult[PaymentsValues.WalletRestoreResult.ENTROPY_UNCHANGED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$PaymentsValues$WalletRestoreResult[PaymentsValues.WalletRestoreResult.MNEMONIC_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private void updateProfileAndFetchLedger() {
        ApplicationDependencies.getJobManager().startChain(new ProfileUploadJob()).then(PaymentLedgerUpdateJob.updateLedger()).enqueue();
    }
}
