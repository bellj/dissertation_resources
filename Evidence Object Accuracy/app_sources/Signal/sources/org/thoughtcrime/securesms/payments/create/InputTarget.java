package org.thoughtcrime.securesms.payments.create;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* loaded from: classes4.dex */
public abstract class InputTarget extends Enum<InputTarget> {
    /* access modifiers changed from: package-private */
    public abstract InputTarget next();
}
