package org.thoughtcrime.securesms.payments.proto;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;
import org.thoughtcrime.securesms.payments.proto.MobileCoinLedger;

/* loaded from: classes4.dex */
public interface MobileCoinLedgerOrBuilder extends MessageLiteOrBuilder {
    long getAsOfTimeStamp();

    long getBalance();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    MobileCoinLedger.Block getHighestBlock();

    MobileCoinLedger.OwnedTXO getSpentTxos(int i);

    int getSpentTxosCount();

    List<MobileCoinLedger.OwnedTXO> getSpentTxosList();

    long getTransferableBalance();

    MobileCoinLedger.OwnedTXO getUnspentTxos(int i);

    int getUnspentTxosCount();

    List<MobileCoinLedger.OwnedTXO> getUnspentTxosList();

    boolean hasHighestBlock();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
