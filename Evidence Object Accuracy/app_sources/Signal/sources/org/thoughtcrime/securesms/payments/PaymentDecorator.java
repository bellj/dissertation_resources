package org.thoughtcrime.securesms.payments;

import java.util.UUID;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.proto.PaymentMetaData;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public abstract class PaymentDecorator implements Payment {
    private final Payment inner;

    @Override // org.thoughtcrime.securesms.payments.Payment
    public /* synthetic */ Money getAmountPlusFeeWithDirection() {
        return Payment.CC.$default$getAmountPlusFeeWithDirection(this);
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public /* synthetic */ Money getAmountWithDirection() {
        return Payment.CC.$default$getAmountWithDirection(this);
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public /* synthetic */ long getDisplayTimestamp() {
        return Payment.CC.$default$getDisplayTimestamp(this);
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public /* synthetic */ boolean isDefrag() {
        return Payment.CC.$default$isDefrag(this);
    }

    public PaymentDecorator(Payment payment) {
        this.inner = payment;
    }

    public Payment getInner() {
        return this.inner;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public UUID getUuid() {
        return this.inner.getUuid();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public Payee getPayee() {
        return this.inner.getPayee();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public long getBlockIndex() {
        return this.inner.getBlockIndex();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public long getBlockTimestamp() {
        return this.inner.getBlockTimestamp();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public long getTimestamp() {
        return this.inner.getTimestamp();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public Direction getDirection() {
        return this.inner.getDirection();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public State getState() {
        return this.inner.getState();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public FailureReason getFailureReason() {
        return this.inner.getFailureReason();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public String getNote() {
        return this.inner.getNote();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public Money getAmount() {
        return this.inner.getAmount();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public Money getFee() {
        return this.inner.getFee();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public PaymentMetaData getPaymentMetaData() {
        return this.inner.getPaymentMetaData();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public boolean isSeen() {
        return this.inner.isSeen();
    }
}
