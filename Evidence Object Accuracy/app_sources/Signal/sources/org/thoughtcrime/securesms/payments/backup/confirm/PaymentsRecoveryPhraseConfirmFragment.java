package org.thoughtcrime.securesms.payments.backup.confirm;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import com.google.android.material.textfield.TextInputLayout;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;

/* loaded from: classes4.dex */
public class PaymentsRecoveryPhraseConfirmFragment extends Fragment {
    private static final int ERROR_THRESHOLD;
    private Drawable invalidWordX;
    private Drawable validWordCheckMark;

    public PaymentsRecoveryPhraseConfirmFragment() {
        super(R.layout.payments_recovery_phrase_confirm_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        EditText editText = (EditText) view.findViewById(R.id.payments_recovery_phrase_confirm_fragment_word_1);
        EditText editText2 = (EditText) view.findViewById(R.id.payments_recovery_phrase_confirm_fragment_word_2);
        View findViewById = view.findViewById(R.id.payments_recovery_phrase_confirm_fragment_see_again);
        View findViewById2 = view.findViewById(R.id.payments_recovery_phrase_confirm_fragment_done);
        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.payments_recovery_phrase_confirm_fragment_word1_wrapper);
        TextInputLayout textInputLayout2 = (TextInputLayout) view.findViewById(R.id.payments_recovery_phrase_confirm_fragment_word2_wrapper);
        PaymentsRecoveryPhraseConfirmFragmentArgs fromBundle = PaymentsRecoveryPhraseConfirmFragmentArgs.fromBundle(requireArguments());
        this.validWordCheckMark = AppCompatResources.getDrawable(requireContext(), R.drawable.ic_check_circle_24);
        this.invalidWordX = AppCompatResources.getDrawable(requireContext(), R.drawable.ic_circle_x_24);
        DrawableCompat.setTint(this.validWordCheckMark, ContextCompat.getColor(requireContext(), R.color.signal_accent_green));
        DrawableCompat.setTint(this.invalidWordX, ContextCompat.getColor(requireContext(), R.color.signal_alert_primary));
        PaymentsRecoveryPhraseConfirmViewModel paymentsRecoveryPhraseConfirmViewModel = (PaymentsRecoveryPhraseConfirmViewModel) ViewModelProviders.of(requireActivity()).get(PaymentsRecoveryPhraseConfirmViewModel.class);
        ((Toolbar) view.findViewById(R.id.payments_recovery_phrase_confirm_fragment_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.confirm.PaymentsRecoveryPhraseConfirmFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsRecoveryPhraseConfirmFragment.m2365$r8$lambda$rfxcxyGhkOTONRFDgrd_5UXTqg(PaymentsRecoveryPhraseConfirmFragment.this, view2);
            }
        });
        editText.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.payments.backup.confirm.PaymentsRecoveryPhraseConfirmFragment$$ExternalSyntheticLambda1
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                PaymentsRecoveryPhraseConfirmFragment.$r8$lambda$vGrp56m13JJ7GVq0IN14XNNx8io(PaymentsRecoveryPhraseConfirmViewModel.this, (Editable) obj);
            }
        }));
        editText2.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.payments.backup.confirm.PaymentsRecoveryPhraseConfirmFragment$$ExternalSyntheticLambda2
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                PaymentsRecoveryPhraseConfirmFragment.$r8$lambda$f4sy9deRIlXGDyt209pEzvNuOOk(PaymentsRecoveryPhraseConfirmViewModel.this, (Editable) obj);
            }
        }));
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.backup.confirm.PaymentsRecoveryPhraseConfirmFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsRecoveryPhraseConfirmFragment.$r8$lambda$n3TajNSZOBqcfam0FWUKElDtDMI(PaymentsRecoveryPhraseConfirmFragment.this, view2);
            }
        });
        findViewById2.setOnClickListener(new View.OnClickListener(view, fromBundle) { // from class: org.thoughtcrime.securesms.payments.backup.confirm.PaymentsRecoveryPhraseConfirmFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ View f$1;
            public final /* synthetic */ PaymentsRecoveryPhraseConfirmFragmentArgs f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PaymentsRecoveryPhraseConfirmFragment.$r8$lambda$V7coKwXIYaSJ7mqRUs74rCTTtV8(PaymentsRecoveryPhraseConfirmFragment.this, this.f$1, this.f$2, view2);
            }
        });
        paymentsRecoveryPhraseConfirmViewModel.getViewState().observe(getViewLifecycleOwner(), new Observer(editText, editText2, findViewById2, textInputLayout, textInputLayout2) { // from class: org.thoughtcrime.securesms.payments.backup.confirm.PaymentsRecoveryPhraseConfirmFragment$$ExternalSyntheticLambda5
            public final /* synthetic */ EditText f$1;
            public final /* synthetic */ EditText f$2;
            public final /* synthetic */ View f$3;
            public final /* synthetic */ TextInputLayout f$4;
            public final /* synthetic */ TextInputLayout f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PaymentsRecoveryPhraseConfirmFragment.$r8$lambda$6cnBiy7FCwo3Ld4t12dGoTBNGzc(PaymentsRecoveryPhraseConfirmFragment.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, (PaymentsRecoveryPhraseConfirmViewState) obj);
            }
        });
        paymentsRecoveryPhraseConfirmViewModel.updateRandomIndices();
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        Navigation.findNavController(requireView()).popBackStack();
    }

    public static /* synthetic */ void lambda$onViewCreated$1(PaymentsRecoveryPhraseConfirmViewModel paymentsRecoveryPhraseConfirmViewModel, Editable editable) {
        paymentsRecoveryPhraseConfirmViewModel.validateWord1(editable.toString());
    }

    public static /* synthetic */ void lambda$onViewCreated$2(PaymentsRecoveryPhraseConfirmViewModel paymentsRecoveryPhraseConfirmViewModel, Editable editable) {
        paymentsRecoveryPhraseConfirmViewModel.validateWord2(editable.toString());
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view) {
        Navigation.findNavController(requireView()).popBackStack();
    }

    public /* synthetic */ void lambda$onViewCreated$4(View view, PaymentsRecoveryPhraseConfirmFragmentArgs paymentsRecoveryPhraseConfirmFragmentArgs, View view2) {
        SignalStore.paymentsValues().setUserConfirmedMnemonic(true);
        ViewUtil.hideKeyboard(requireContext(), view);
        Toast.makeText(requireContext(), (int) R.string.PaymentRecoveryPhraseConfirmFragment__recovery_phrase_confirmed, 0).show();
        if (paymentsRecoveryPhraseConfirmFragmentArgs.getFinishOnConfirm()) {
            requireActivity().setResult(-1);
            requireActivity().finish();
            return;
        }
        Navigation.findNavController(view).popBackStack(R.id.paymentsHome, false);
    }

    public /* synthetic */ void lambda$onViewCreated$5(EditText editText, EditText editText2, View view, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, PaymentsRecoveryPhraseConfirmViewState paymentsRecoveryPhraseConfirmViewState) {
        updateValidity(editText, paymentsRecoveryPhraseConfirmViewState.isWord1Valid());
        updateValidity(editText2, paymentsRecoveryPhraseConfirmViewState.isWord2Valid());
        view.setEnabled(paymentsRecoveryPhraseConfirmViewState.areAllWordsValid());
        String string = getString(R.string.PaymentRecoveryPhraseConfirmFragment__word_d, Integer.valueOf(paymentsRecoveryPhraseConfirmViewState.getWord1Index() + 1));
        String string2 = getString(R.string.PaymentRecoveryPhraseConfirmFragment__word_d, Integer.valueOf(paymentsRecoveryPhraseConfirmViewState.getWord2Index() + 1));
        textInputLayout.setHint(string);
        textInputLayout2.setHint(string2);
    }

    private void updateValidity(TextView textView, boolean z) {
        if (z) {
            setEndDrawable(textView, this.validWordCheckMark);
        } else if (textView.getText().length() >= 1) {
            setEndDrawable(textView, this.invalidWordX);
        } else {
            textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
        }
    }

    private void setEndDrawable(TextView textView, Drawable drawable) {
        if (textView.getLayoutDirection() == 0) {
            textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, drawable, (Drawable) null);
        } else {
            textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
        }
    }
}
