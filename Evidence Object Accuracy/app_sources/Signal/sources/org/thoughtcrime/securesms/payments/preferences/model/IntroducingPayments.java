package org.thoughtcrime.securesms.payments.preferences.model;

import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeState;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes4.dex */
public class IntroducingPayments implements MappingModel<IntroducingPayments> {
    private PaymentsHomeState.PaymentsState paymentsState;

    public boolean areItemsTheSame(IntroducingPayments introducingPayments) {
        return true;
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(IntroducingPayments introducingPayments) {
        return MappingModel.CC.$default$getChangePayload(this, introducingPayments);
    }

    public IntroducingPayments(PaymentsHomeState.PaymentsState paymentsState) {
        this.paymentsState = paymentsState;
    }

    public boolean areContentsTheSame(IntroducingPayments introducingPayments) {
        return this.paymentsState == introducingPayments.paymentsState;
    }

    public boolean isActivating() {
        return this.paymentsState == PaymentsHomeState.PaymentsState.ACTIVATING;
    }
}
