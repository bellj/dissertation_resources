package org.thoughtcrime.securesms.payments.preferences;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class RecipientHasNotEnabledPaymentsDialog {
    private RecipientHasNotEnabledPaymentsDialog() {
    }

    public static void show(Context context) {
        show(context, null);
    }

    public static void show(Context context, Runnable runnable) {
        new AlertDialog.Builder(context).setTitle(R.string.ConfirmPaymentFragment__invalid_recipient).setMessage(R.string.ConfirmPaymentFragment__this_person_has_not_activated_payments).setPositiveButton(17039370, new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.payments.preferences.RecipientHasNotEnabledPaymentsDialog$$ExternalSyntheticLambda0
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RecipientHasNotEnabledPaymentsDialog.lambda$show$0(this.f$0, dialogInterface, i);
            }
        }).setCancelable(false).show();
    }

    public static /* synthetic */ void lambda$show$0(Runnable runnable, DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        if (runnable != null) {
            runnable.run();
        }
    }
}
