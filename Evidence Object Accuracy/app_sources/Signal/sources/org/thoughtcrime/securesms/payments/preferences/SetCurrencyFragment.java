package org.thoughtcrime.securesms.payments.preferences;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Currency;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.BaseSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.SingleSelectSetting;
import org.thoughtcrime.securesms.payments.preferences.SetCurrencyViewModel;

/* loaded from: classes4.dex */
public final class SetCurrencyFragment extends LoggingFragment {
    private boolean handledInitialScroll = false;

    public SetCurrencyFragment() {
        super(R.layout.set_currency_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.set_currency_fragment_list);
        ((Toolbar) view.findViewById(R.id.set_currency_fragment_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SetCurrencyFragment.$r8$lambda$Bv_TP0rUSLzyMgOUcm_pW4CK4Kg(view2);
            }
        });
        SetCurrencyViewModel setCurrencyViewModel = (SetCurrencyViewModel) ViewModelProviders.of(this, new SetCurrencyViewModel.Factory()).get(SetCurrencyViewModel.class);
        BaseSettingsAdapter baseSettingsAdapter = new BaseSettingsAdapter();
        baseSettingsAdapter.configureSingleSelect(new SingleSelectSetting.SingleSelectSelectionChangedListener() { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyFragment$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.components.settings.SingleSelectSetting.SingleSelectSelectionChangedListener
            public final void onSelectionChanged(Object obj) {
                SetCurrencyFragment.m2420$r8$lambda$LlXEIO7kh66loRUynPReBj9pQw(SetCurrencyViewModel.this, obj);
            }
        });
        recyclerView.setAdapter(baseSettingsAdapter);
        setCurrencyViewModel.getCurrencyListState().observe(getViewLifecycleOwner(), new Observer(baseSettingsAdapter, bundle, recyclerView) { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ BaseSettingsAdapter f$1;
            public final /* synthetic */ Bundle f$2;
            public final /* synthetic */ RecyclerView f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                SetCurrencyFragment.m2419$r8$lambda$10CGMQGdhkjrtmQ5YryW4_HxEM(SetCurrencyFragment.this, this.f$1, this.f$2, this.f$3, (SetCurrencyViewModel.CurrencyListState) obj);
            }
        });
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    public static /* synthetic */ void lambda$onViewCreated$1(SetCurrencyViewModel setCurrencyViewModel, Object obj) {
        setCurrencyViewModel.select((Currency) obj);
    }

    public /* synthetic */ void lambda$onViewCreated$4(BaseSettingsAdapter baseSettingsAdapter, Bundle bundle, RecyclerView recyclerView, SetCurrencyViewModel.CurrencyListState currencyListState) {
        baseSettingsAdapter.submitList(currencyListState.getItems(), new Runnable(currencyListState, bundle, recyclerView) { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ SetCurrencyViewModel.CurrencyListState f$1;
            public final /* synthetic */ Bundle f$2;
            public final /* synthetic */ RecyclerView f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SetCurrencyFragment.$r8$lambda$89PxTU0n8UlJoZ4c9nfykdf5yFE(SetCurrencyFragment.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$3(SetCurrencyViewModel.CurrencyListState currencyListState, Bundle bundle, RecyclerView recyclerView) {
        if (currencyListState.isLoaded() && currencyListState.getSelectedIndex() != -1 && bundle == null && !this.handledInitialScroll) {
            this.handledInitialScroll = true;
            recyclerView.post(new Runnable(currencyListState) { // from class: org.thoughtcrime.securesms.payments.preferences.SetCurrencyFragment$$ExternalSyntheticLambda4
                public final /* synthetic */ SetCurrencyViewModel.CurrencyListState f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SetCurrencyFragment.$r8$lambda$AasmwlUKSaNiLNoBmlexNbvRfjM(RecyclerView.this, this.f$1);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$onViewCreated$2(RecyclerView recyclerView, SetCurrencyViewModel.CurrencyListState currencyListState) {
        recyclerView.scrollToPosition(currencyListState.getSelectedIndex());
    }
}
