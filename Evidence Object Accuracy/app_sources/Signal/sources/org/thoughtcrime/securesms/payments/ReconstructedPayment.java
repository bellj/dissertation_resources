package org.thoughtcrime.securesms.payments;

import java.util.UUID;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.proto.PaymentMetaData;
import org.whispersystems.signalservice.api.payments.Money;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes4.dex */
public final class ReconstructedPayment implements Payment {
    private final Money amount;
    private final long blockIndex;
    private final long blockTimestamp;
    private final Direction direction;

    @Override // org.thoughtcrime.securesms.payments.Payment
    public /* synthetic */ Money getAmountPlusFeeWithDirection() {
        return Payment.CC.$default$getAmountPlusFeeWithDirection(this);
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public /* synthetic */ Money getAmountWithDirection() {
        return Payment.CC.$default$getAmountWithDirection(this);
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public /* synthetic */ long getDisplayTimestamp() {
        return Payment.CC.$default$getDisplayTimestamp(this);
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public FailureReason getFailureReason() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public String getNote() {
        return "";
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public /* synthetic */ boolean isDefrag() {
        return Payment.CC.$default$isDefrag(this);
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public boolean isSeen() {
        return true;
    }

    public ReconstructedPayment(long j, long j2, Direction direction, Money money) {
        this.blockIndex = j;
        this.blockTimestamp = j2;
        this.direction = direction;
        this.amount = money;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public UUID getUuid() {
        return UuidUtil.UNKNOWN_UUID;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public Payee getPayee() {
        return Payee.UNKNOWN;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public long getBlockIndex() {
        return this.blockIndex;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public long getTimestamp() {
        return this.blockTimestamp;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public long getBlockTimestamp() {
        return this.blockTimestamp;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public Direction getDirection() {
        return this.direction;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public State getState() {
        return State.SUCCESSFUL;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public Money getAmount() {
        return this.amount;
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public Money getFee() {
        return this.amount.toZero();
    }

    @Override // org.thoughtcrime.securesms.payments.Payment
    public PaymentMetaData getPaymentMetaData() {
        return PaymentMetaData.getDefaultInstance();
    }
}
