package org.thoughtcrime.securesms.payments;

import com.mobilecoin.lib.Mnemonics;
import com.mobilecoin.lib.exceptions.BadEntropyException;
import com.mobilecoin.lib.exceptions.BadMnemonicException;
import java.util.Arrays;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class Entropy {
    private static final String TAG = Log.tag(Entropy.class);
    private final byte[] bytes;

    Entropy(byte[] bArr) {
        this.bytes = bArr;
    }

    public static Entropy generateNew() {
        return new Entropy(Util.getSecretBytes(32));
    }

    public static Entropy fromBytes(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        if (bArr.length == 32) {
            return new Entropy(bArr);
        }
        Log.w(TAG, String.format(Locale.US, "Entropy was supplied of length %d and ignored", Integer.valueOf(bArr.length)), new Throwable());
        return null;
    }

    public byte[] getBytes() {
        return this.bytes;
    }

    public Mnemonic asMnemonic() {
        try {
            String bip39EntropyToMnemonic = Mnemonics.bip39EntropyToMnemonic(this.bytes);
            if (Arrays.equals(this.bytes, Mnemonics.bip39EntropyFromMnemonic(bip39EntropyToMnemonic))) {
                return new Mnemonic(bip39EntropyToMnemonic);
            }
            throw new AssertionError("Round trip mnemonic failure");
        } catch (BadEntropyException | BadMnemonicException e) {
            throw new AssertionError(e);
        }
    }
}
