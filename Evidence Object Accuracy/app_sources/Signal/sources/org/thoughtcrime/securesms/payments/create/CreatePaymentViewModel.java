package org.thoughtcrime.securesms.payments.create;

import android.content.Context;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.Optional;
import j$.util.function.Function;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.Balance;
import org.thoughtcrime.securesms.payments.CreatePaymentDetails;
import org.thoughtcrime.securesms.payments.FiatMoneyUtil;
import org.thoughtcrime.securesms.payments.currency.CurrencyExchange;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.payments.FormatterOptions;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class CreatePaymentViewModel extends ViewModel {
    private static final Money.MobileCoin AMOUNT_LOWER_BOUND_EXCLUSIVE = Money.MobileCoin.ZERO;
    private static final Money.MobileCoin AMOUNT_UPPER_BOUND_EXCLUSIVE = Money.MobileCoin.MAX_VALUE;
    private static final String TAG = Log.tag(CreatePaymentViewModel.class);
    private final Store<InputState> inputState;
    private final LiveData<Boolean> isPaymentsSupportedByPayee;
    private final LiveData<Boolean> isValidAmount;
    private final MutableLiveData<CharSequence> note;
    private final PayeeParcelable payee;
    private final LiveData<Money> spendableBalance;

    /* synthetic */ CreatePaymentViewModel(PayeeParcelable payeeParcelable, CharSequence charSequence, AnonymousClass1 r3) {
        this(payeeParcelable, charSequence);
    }

    private CreatePaymentViewModel(PayeeParcelable payeeParcelable, CharSequence charSequence) {
        this.payee = payeeParcelable;
        LiveData<Money> map = Transformations.map(SignalStore.paymentsValues().liveMobileCoinBalance(), new Function() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda1
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((Balance) obj).getTransferableAmount();
            }
        });
        this.spendableBalance = map;
        this.note = new MutableLiveData<>(charSequence);
        Store<InputState> store = new Store<>(new InputState());
        this.inputState = store;
        this.isValidAmount = LiveDataUtil.combineLatest(map, store.getStateLiveData(), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return CreatePaymentViewModel.this.lambda$new$0((Money) obj, (InputState) obj2);
            }
        });
        if (payeeParcelable.getPayee().hasRecipientId()) {
            this.isPaymentsSupportedByPayee = LiveDataUtil.mapAsync(new DefaultValueLiveData(payeeParcelable.getPayee().requireRecipientId()), new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda3
                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return CreatePaymentViewModel.lambda$new$1((RecipientId) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            });
        } else {
            this.isPaymentsSupportedByPayee = new DefaultValueLiveData(Boolean.TRUE);
        }
        store.update(LiveDataUtil.mapAsync(SignalStore.paymentsValues().liveCurrentCurrency(), new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return CreatePaymentViewModel.lambda$new$2((Currency) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }), new Store.Action() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return CreatePaymentViewModel.this.lambda$new$3((Optional) obj, (InputState) obj2);
            }
        });
    }

    public /* synthetic */ Boolean lambda$new$0(Money money, InputState inputState) {
        return Boolean.valueOf(validateAmount(money.requireMobileCoin(), inputState.getMoney().requireMobileCoin()));
    }

    public static /* synthetic */ Boolean lambda$new$1(RecipientId recipientId) {
        try {
            ProfileUtil.getAddressForRecipient(Recipient.resolved(recipientId));
            return Boolean.TRUE;
        } catch (Exception e) {
            Log.w(TAG, "Could not get address for recipient: ", e);
            return Boolean.FALSE;
        }
    }

    public static /* synthetic */ Optional lambda$new$2(Currency currency) {
        try {
            return Optional.ofNullable(ApplicationDependencies.getPayments().getCurrencyExchange(true).getExchangeRate(currency));
        } catch (IOException e) {
            Log.w(TAG, "Unable to get fresh exchange data, falling back to cached", e);
            try {
                return Optional.ofNullable(ApplicationDependencies.getPayments().getCurrencyExchange(false).getExchangeRate(currency));
            } catch (IOException e2) {
                Log.w(TAG, "Unable to get any exchange data", e2);
                return Optional.empty();
            }
        }
    }

    public /* synthetic */ InputState lambda$new$3(Optional optional, InputState inputState) {
        return lambda$updateAmount$7(ApplicationDependencies.getApplication(), inputState.updateExchangeRate(optional), AmountKeyboardGlyph.NONE);
    }

    public LiveData<InputState> getInputState() {
        return this.inputState.getStateLiveData();
    }

    public LiveData<Boolean> getIsPaymentsSupportedByPayee() {
        return this.isPaymentsSupportedByPayee;
    }

    public LiveData<CharSequence> getNote() {
        return Transformations.distinctUntilChanged(this.note);
    }

    public LiveData<Boolean> isValidAmount() {
        return this.isValidAmount;
    }

    public LiveData<Boolean> getCanSendPayment() {
        return this.isValidAmount;
    }

    public LiveData<Money> getSpendableBalance() {
        return this.spendableBalance;
    }

    public void clearAmount() {
        this.inputState.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CreatePaymentViewModel.lambda$clearAmount$5((InputState) obj);
            }
        });
    }

    public static /* synthetic */ InputState lambda$clearAmount$5(InputState inputState) {
        Money money = Money.MobileCoin.ZERO;
        return inputState.updateAmount("0", "0", money, inputState.getExchangeRate().flatMap(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda7
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((CurrencyExchange.ExchangeRate) obj).exchange(Money.this);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }));
    }

    public static /* synthetic */ InputState lambda$toggleMoneyInputTarget$6(InputState inputState) {
        return inputState.updateInputTarget(inputState.getInputTarget().next());
    }

    public void toggleMoneyInputTarget() {
        this.inputState.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CreatePaymentViewModel.lambda$toggleMoneyInputTarget$6((InputState) obj);
            }
        });
    }

    public void setNote(CharSequence charSequence) {
        this.note.setValue(charSequence);
    }

    public void updateAmount(Context context, AmountKeyboardGlyph amountKeyboardGlyph) {
        this.inputState.update(new com.annimon.stream.function.Function(context, amountKeyboardGlyph) { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda9
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ AmountKeyboardGlyph f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CreatePaymentViewModel.this.lambda$updateAmount$7(this.f$1, this.f$2, (InputState) obj);
            }
        });
    }

    /* renamed from: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$create$InputTarget;

        static {
            int[] iArr = new int[InputTarget.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$create$InputTarget = iArr;
            try {
                iArr[InputTarget.FIAT_MONEY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$create$InputTarget[InputTarget.MONEY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    /* renamed from: updateAmount */
    public InputState lambda$updateAmount$7(Context context, InputState inputState, AmountKeyboardGlyph amountKeyboardGlyph) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$create$InputTarget[inputState.getInputTarget().ordinal()];
        if (i == 1) {
            return updateFiatAmount(context, inputState, amountKeyboardGlyph, SignalStore.paymentsValues().currentCurrency());
        }
        if (i == 2) {
            return updateMoneyAmount(context, inputState, amountKeyboardGlyph);
        }
        throw new IllegalStateException("Unexpected input target " + inputState.getInputTarget().name());
    }

    private InputState updateFiatAmount(Context context, InputState inputState, AmountKeyboardGlyph amountKeyboardGlyph, Currency currency) {
        String updateAmountString = updateAmountString(context, inputState.getFiatAmount(), amountKeyboardGlyph, currency.getDefaultFractionDigits());
        FiatMoney stringToFiatValueOrZero = stringToFiatValueOrZero(updateAmountString, currency);
        Money money = (Money) inputState.getExchangeRate().flatMap(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda6
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((CurrencyExchange.ExchangeRate) obj).exchange(FiatMoney.this);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).get();
        String str = "0";
        if (!updateAmountString.equals(str)) {
            str = money.toString(FormatterOptions.builder().withoutUnit().build());
        }
        if (!withinMobileCoinBounds(money.requireMobileCoin())) {
            return inputState;
        }
        return inputState.updateAmount(str, updateAmountString, money, Optional.of(stringToFiatValueOrZero));
    }

    private InputState updateMoneyAmount(Context context, InputState inputState, AmountKeyboardGlyph amountKeyboardGlyph) {
        String updateAmountString = updateAmountString(context, inputState.getMoneyAmount(), amountKeyboardGlyph, inputState.getMoney().getCurrency().getDecimalPrecision());
        Money.MobileCoin stringToMobileCoinValueOrZero = stringToMobileCoinValueOrZero(updateAmountString);
        Optional<U> flatMap = inputState.getExchangeRate().flatMap(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda10
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((CurrencyExchange.ExchangeRate) obj).exchange(Money.MobileCoin.this);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        if (!withinMobileCoinBounds(stringToMobileCoinValueOrZero)) {
            return inputState;
        }
        String str = "0";
        if (!updateAmountString.equals(str)) {
            str = (String) flatMap.map(new j$.util.function.Function(context) { // from class: org.thoughtcrime.securesms.payments.create.CreatePaymentViewModel$$ExternalSyntheticLambda11
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return CreatePaymentViewModel.lambda$updateMoneyAmount$10(this.f$0, (FiatMoney) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).orElse(str);
        }
        return inputState.updateAmount(updateAmountString, str, stringToMobileCoinValueOrZero, flatMap);
    }

    public static /* synthetic */ String lambda$updateMoneyAmount$10(Context context, FiatMoney fiatMoney) {
        return FiatMoneyUtil.format(context.getResources(), fiatMoney, FiatMoneyUtil.formatOptions().withDisplayTime(false).numberOnly());
    }

    private boolean validateAmount(Money.MobileCoin mobileCoin, Money.MobileCoin mobileCoin2) {
        try {
            if (!mobileCoin2.greaterThan(AMOUNT_LOWER_BOUND_EXCLUSIVE)) {
                return false;
            }
            if (!mobileCoin2.greaterThan(mobileCoin)) {
                return true;
            }
            return false;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    private static boolean withinMobileCoinBounds(Money.MobileCoin mobileCoin) {
        return !mobileCoin.lessThan(AMOUNT_LOWER_BOUND_EXCLUSIVE) && !mobileCoin.greaterThan(AMOUNT_UPPER_BOUND_EXCLUSIVE);
    }

    private FiatMoney stringToFiatValueOrZero(String str, Currency currency) {
        if (str != null) {
            try {
                return new FiatMoney(new BigDecimal(str), currency);
            } catch (NumberFormatException unused) {
            }
        }
        return new FiatMoney(BigDecimal.ZERO, currency);
    }

    private Money.MobileCoin stringToMobileCoinValueOrZero(String str) {
        if (str != null) {
            try {
                return Money.mobileCoin(new BigDecimal(str));
            } catch (NumberFormatException unused) {
            }
        }
        return Money.MobileCoin.ZERO;
    }

    public CreatePaymentDetails getCreatePaymentDetails() {
        CharSequence value = this.note.getValue();
        String charSequence = value != null ? value.toString() : null;
        PayeeParcelable payeeParcelable = this.payee;
        Money money = this.inputState.getState().getMoney();
        Objects.requireNonNull(money);
        return new CreatePaymentDetails(payeeParcelable, money, charSequence);
    }

    private static String updateAmountString(Context context, String str, AmountKeyboardGlyph amountKeyboardGlyph, int i) {
        if (amountKeyboardGlyph == AmountKeyboardGlyph.NONE) {
            return str;
        }
        if (amountKeyboardGlyph == AmountKeyboardGlyph.BACK) {
            if (!str.isEmpty()) {
                str = str.substring(0, str.length() - 1);
                if (str.isEmpty()) {
                    return AmountKeyboardGlyph.ZERO.getGlyph(context);
                }
            }
            return str;
        }
        AmountKeyboardGlyph amountKeyboardGlyph2 = AmountKeyboardGlyph.ZERO;
        boolean equals = amountKeyboardGlyph2.getGlyph(context).equals(str);
        AmountKeyboardGlyph amountKeyboardGlyph3 = AmountKeyboardGlyph.DECIMAL;
        int indexOf = str.indexOf(amountKeyboardGlyph3.getGlyph(context));
        if (amountKeyboardGlyph == amountKeyboardGlyph3) {
            if (equals) {
                return amountKeyboardGlyph2.getGlyph(context) + amountKeyboardGlyph.getGlyph(context);
            } else if (indexOf > -1) {
                return str;
            }
        }
        if (indexOf > -1 && (str.length() - 1) - indexOf >= i) {
            return str;
        }
        if (equals) {
            return amountKeyboardGlyph.getGlyph(context);
        }
        return str + amountKeyboardGlyph.getGlyph(context);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final CharSequence note;
        private final PayeeParcelable payee;

        public Factory(PayeeParcelable payeeParcelable, CharSequence charSequence) {
            this.payee = payeeParcelable;
            this.note = charSequence;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new CreatePaymentViewModel(this.payee, this.note, null);
        }
    }
}
