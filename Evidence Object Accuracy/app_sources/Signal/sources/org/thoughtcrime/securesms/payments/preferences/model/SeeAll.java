package org.thoughtcrime.securesms.payments.preferences.model;

import org.thoughtcrime.securesms.payments.preferences.PaymentType;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes4.dex */
public class SeeAll implements MappingModel<SeeAll> {
    private final PaymentType paymentType;

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(SeeAll seeAll) {
        return MappingModel.CC.$default$getChangePayload(this, seeAll);
    }

    public SeeAll(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public PaymentType getPaymentType() {
        return this.paymentType;
    }

    public boolean areItemsTheSame(SeeAll seeAll) {
        return this.paymentType == seeAll.paymentType;
    }

    public boolean areContentsTheSame(SeeAll seeAll) {
        return areItemsTheSame(seeAll);
    }
}
