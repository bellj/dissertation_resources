package org.thoughtcrime.securesms.payments.preferences;

import androidx.arch.core.util.Function;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import j$.util.Optional;
import java.util.Currency;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.SettingHeader;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.PaymentsAvailability;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.UnreadPaymentsRepository;
import org.thoughtcrime.securesms.payments.currency.CurrencyExchange;
import org.thoughtcrime.securesms.payments.currency.CurrencyExchangeRepository;
import org.thoughtcrime.securesms.payments.deactivate.DeactivateWalletViewModel$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeRepository;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeState;
import org.thoughtcrime.securesms.payments.preferences.model.InProgress;
import org.thoughtcrime.securesms.payments.preferences.model.InfoCard;
import org.thoughtcrime.securesms.payments.preferences.model.IntroducingPayments;
import org.thoughtcrime.securesms.payments.preferences.model.NoRecentActivity;
import org.thoughtcrime.securesms.payments.preferences.model.SeeAll;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class PaymentsHomeViewModel extends ViewModel {
    private static final int MAX_PAYMENT_ITEMS;
    private static final String TAG = Log.tag(PaymentsHomeViewModel.class);
    private final LiveData<Money> balance;
    private final CurrencyExchangeRepository currencyExchangeRepository;
    private final SingleLiveEvent<ErrorEnabling> errorEnablingPayments;
    private final LiveData<FiatMoney> exchange;
    private final LiveData<LoadState> exchangeLoadState;
    private final LiveData<MappingModelList> list;
    private final SingleLiveEvent<PaymentStateEvent> paymentStateEvents;
    private final LiveData<Boolean> paymentsEnabled;
    private final PaymentsHomeRepository paymentsHomeRepository;
    private final Store<PaymentsHomeState> store;
    private final UnreadPaymentsRepository unreadPaymentsRepository = new UnreadPaymentsRepository();

    /* loaded from: classes4.dex */
    public enum ErrorEnabling {
        REGION,
        NETWORK
    }

    public static /* synthetic */ PaymentsHomeState lambda$onInfoCardDismissed$6(PaymentsHomeState paymentsHomeState) {
        return paymentsHomeState;
    }

    PaymentsHomeViewModel(PaymentsHomeRepository paymentsHomeRepository, PaymentsRepository paymentsRepository, CurrencyExchangeRepository currencyExchangeRepository) {
        this.paymentsHomeRepository = paymentsHomeRepository;
        this.currencyExchangeRepository = currencyExchangeRepository;
        Store<PaymentsHomeState> store = new Store<>(new PaymentsHomeState(getPaymentsState()));
        this.store = store;
        LiveData<Money> mapDistinct = LiveDataUtil.mapDistinct(SignalStore.paymentsValues().liveMobileCoinBalance(), new DeactivateWalletViewModel$$ExternalSyntheticLambda1());
        this.balance = mapDistinct;
        this.list = Transformations.map(store.getStateLiveData(), new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return PaymentsHomeViewModel.this.createList((PaymentsHomeState) obj);
            }
        });
        this.paymentsEnabled = LiveDataUtil.mapDistinct(store.getStateLiveData(), new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda1
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return PaymentsHomeViewModel.lambda$new$0((PaymentsHomeState) obj);
            }
        });
        this.exchange = LiveDataUtil.mapDistinct(store.getStateLiveData(), new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((PaymentsHomeState) obj).getExchangeAmount();
            }
        });
        this.exchangeLoadState = LiveDataUtil.mapDistinct(store.getStateLiveData(), new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda3
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((PaymentsHomeState) obj).getExchangeRateLoadState();
            }
        });
        this.paymentStateEvents = new SingleLiveEvent<>();
        this.errorEnablingPayments = new SingleLiveEvent<>();
        store.update(paymentsRepository.getRecentPayments(), new Store.Action() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda4
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return PaymentsHomeViewModel.this.updateRecentPayments((List) obj, (PaymentsHomeState) obj2);
            }
        });
        store.update(LiveDataUtil.combineLatest(mapDistinct, LiveDataUtil.combineLatest(SignalStore.paymentsValues().liveCurrentCurrency(), LiveDataUtil.mapDistinct(store.getStateLiveData(), new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda5
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((PaymentsHomeState) obj).getCurrencyExchange();
            }
        }), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return ((CurrencyExchange) obj2).getExchangeRate((Currency) obj);
            }
        }), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda7
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return ((CurrencyExchange.ExchangeRate) obj2).exchange((Money) obj);
            }
        }), new Store.Action() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda8
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return PaymentsHomeViewModel.lambda$new$3((Optional) obj, (PaymentsHomeState) obj2);
            }
        });
        refreshExchangeRates(true);
    }

    public static /* synthetic */ Boolean lambda$new$0(PaymentsHomeState paymentsHomeState) {
        return Boolean.valueOf(paymentsHomeState.getPaymentsState() == PaymentsHomeState.PaymentsState.ACTIVATED);
    }

    public static /* synthetic */ PaymentsHomeState lambda$new$3(Optional optional, PaymentsHomeState paymentsHomeState) {
        return paymentsHomeState.updateCurrencyAmount((FiatMoney) optional.orElse(null));
    }

    private static PaymentsHomeState.PaymentsState getPaymentsState() {
        PaymentsAvailability paymentsAvailability = SignalStore.paymentsValues().getPaymentsAvailability();
        if (paymentsAvailability.canRegister()) {
            return PaymentsHomeState.PaymentsState.NOT_ACTIVATED;
        }
        if (paymentsAvailability.isEnabled()) {
            return PaymentsHomeState.PaymentsState.ACTIVATED;
        }
        return PaymentsHomeState.PaymentsState.ACTIVATE_NOT_ALLOWED;
    }

    public LiveData<PaymentStateEvent> getPaymentStateEvents() {
        return this.paymentStateEvents;
    }

    public LiveData<ErrorEnabling> getErrorEnablingPayments() {
        return this.errorEnablingPayments;
    }

    public LiveData<MappingModelList> getList() {
        return this.list;
    }

    public LiveData<Boolean> getPaymentsEnabled() {
        return this.paymentsEnabled;
    }

    public LiveData<Money> getBalance() {
        return this.balance;
    }

    public LiveData<FiatMoney> getExchange() {
        return this.exchange;
    }

    public LiveData<LoadState> getExchangeLoadState() {
        return this.exchangeLoadState;
    }

    public void markAllPaymentsSeen() {
        this.unreadPaymentsRepository.markAllPaymentsSeen();
    }

    public void checkPaymentActivationState() {
        PaymentsHomeState.PaymentsState paymentsState = this.store.getState().getPaymentsState();
        boolean mobileCoinPaymentsEnabled = SignalStore.paymentsValues().mobileCoinPaymentsEnabled();
        if (paymentsState.equals(PaymentsHomeState.PaymentsState.ACTIVATED) && !mobileCoinPaymentsEnabled) {
            this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda11
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return PaymentsHomeViewModel.lambda$checkPaymentActivationState$4((PaymentsHomeState) obj);
                }
            });
            this.paymentStateEvents.setValue(PaymentStateEvent.DEACTIVATED);
        } else if (paymentsState.equals(PaymentsHomeState.PaymentsState.NOT_ACTIVATED) && mobileCoinPaymentsEnabled) {
            this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda12
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return PaymentsHomeViewModel.lambda$checkPaymentActivationState$5((PaymentsHomeState) obj);
                }
            });
            this.paymentStateEvents.setValue(PaymentStateEvent.ACTIVATED);
        }
    }

    public static /* synthetic */ PaymentsHomeState lambda$checkPaymentActivationState$4(PaymentsHomeState paymentsHomeState) {
        return paymentsHomeState.updatePaymentsEnabled(PaymentsHomeState.PaymentsState.NOT_ACTIVATED);
    }

    public static /* synthetic */ PaymentsHomeState lambda$checkPaymentActivationState$5(PaymentsHomeState paymentsHomeState) {
        return paymentsHomeState.updatePaymentsEnabled(PaymentsHomeState.PaymentsState.ACTIVATED);
    }

    public MappingModelList createList(PaymentsHomeState paymentsHomeState) {
        MappingModelList mappingModelList = new MappingModelList();
        if (paymentsHomeState.getPaymentsState() == PaymentsHomeState.PaymentsState.ACTIVATED) {
            if (paymentsHomeState.getTotalPayments() > 0) {
                mappingModelList.add(new SettingHeader.Item((int) R.string.PaymentsHomeFragment__recent_activity));
                mappingModelList.addAll(paymentsHomeState.getPayments());
                if (paymentsHomeState.getTotalPayments() > 4) {
                    mappingModelList.add(new SeeAll(PaymentType.PAYMENT));
                }
            }
            if (!paymentsHomeState.isRecentPaymentsLoaded()) {
                mappingModelList.add(new InProgress());
            } else if (paymentsHomeState.getRequests().isEmpty() && paymentsHomeState.getPayments().isEmpty() && paymentsHomeState.isRecentPaymentsLoaded()) {
                mappingModelList.add(new NoRecentActivity());
            }
        } else if (paymentsHomeState.getPaymentsState() == PaymentsHomeState.PaymentsState.ACTIVATE_NOT_ALLOWED) {
            Log.w(TAG, "Payments remotely disabled or not in region");
        } else {
            mappingModelList.add(new IntroducingPayments(paymentsHomeState.getPaymentsState()));
        }
        mappingModelList.addAll(InfoCard.getInfoCards());
        return mappingModelList;
    }

    public PaymentsHomeState updateRecentPayments(List<Payment> list, PaymentsHomeState paymentsHomeState) {
        return paymentsHomeState.updatePayments(Stream.of(list).limit(4).map(new PaymentsHomeViewModel$$ExternalSyntheticLambda14()).toList(), list.size());
    }

    public void onInfoCardDismissed() {
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda13
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PaymentsHomeViewModel.lambda$onInfoCardDismissed$6((PaymentsHomeState) obj);
            }
        });
    }

    public void activatePayments() {
        if (this.store.getState().getPaymentsState() == PaymentsHomeState.PaymentsState.NOT_ACTIVATED) {
            this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda16
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return PaymentsHomeViewModel.lambda$activatePayments$7((PaymentsHomeState) obj);
                }
            });
            this.paymentsHomeRepository.activatePayments(new AsynchronousCallback.WorkerThread<Void, PaymentsHomeRepository.Error>() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel.1
                public static /* synthetic */ PaymentsHomeState lambda$onComplete$0(PaymentsHomeState paymentsHomeState) {
                    return paymentsHomeState.updatePaymentsEnabled(PaymentsHomeState.PaymentsState.ACTIVATED);
                }

                public void onComplete(Void r2) {
                    PaymentsHomeViewModel.this.store.update(new PaymentsHomeViewModel$1$$ExternalSyntheticLambda1());
                }

                public static /* synthetic */ PaymentsHomeState lambda$onError$1(PaymentsHomeState paymentsHomeState) {
                    return paymentsHomeState.updatePaymentsEnabled(PaymentsHomeState.PaymentsState.NOT_ACTIVATED);
                }

                public void onError(PaymentsHomeRepository.Error error) {
                    PaymentsHomeViewModel.this.store.update(new PaymentsHomeViewModel$1$$ExternalSyntheticLambda0());
                    if (error == PaymentsHomeRepository.Error.NetworkError) {
                        PaymentsHomeViewModel.this.errorEnablingPayments.postValue(ErrorEnabling.NETWORK);
                    } else if (error == PaymentsHomeRepository.Error.RegionError) {
                        PaymentsHomeViewModel.this.errorEnablingPayments.postValue(ErrorEnabling.REGION);
                    } else {
                        throw new AssertionError();
                    }
                }
            });
        }
    }

    public static /* synthetic */ PaymentsHomeState lambda$activatePayments$7(PaymentsHomeState paymentsHomeState) {
        return paymentsHomeState.updatePaymentsEnabled(PaymentsHomeState.PaymentsState.ACTIVATING);
    }

    public void deactivatePayments() {
        Money value = this.balance.getValue();
        if (value == null) {
            this.paymentStateEvents.setValue(PaymentStateEvent.NO_BALANCE);
        } else if (value.isPositive()) {
            this.paymentStateEvents.setValue(PaymentStateEvent.DEACTIVATE_WITH_BALANCE);
        } else {
            this.paymentStateEvents.setValue(PaymentStateEvent.DEACTIVATE_WITHOUT_BALANCE);
        }
    }

    public void confirmDeactivatePayments() {
        if (this.store.getState().getPaymentsState() == PaymentsHomeState.PaymentsState.ACTIVATED) {
            this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda9
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return PaymentsHomeViewModel.lambda$confirmDeactivatePayments$8((PaymentsHomeState) obj);
                }
            });
            this.paymentsHomeRepository.deactivatePayments(new Consumer() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda10
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    PaymentsHomeViewModel.this.lambda$confirmDeactivatePayments$10((Boolean) obj);
                }
            });
        }
    }

    public static /* synthetic */ PaymentsHomeState lambda$confirmDeactivatePayments$8(PaymentsHomeState paymentsHomeState) {
        return paymentsHomeState.updatePaymentsEnabled(PaymentsHomeState.PaymentsState.DEACTIVATING);
    }

    public /* synthetic */ void lambda$confirmDeactivatePayments$10(Boolean bool) {
        this.store.update(new com.annimon.stream.function.Function(bool) { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda17
            public final /* synthetic */ Boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PaymentsHomeViewModel.lambda$confirmDeactivatePayments$9(this.f$0, (PaymentsHomeState) obj);
            }
        });
        if (bool.booleanValue()) {
            this.paymentStateEvents.postValue(PaymentStateEvent.DEACTIVATED);
        }
    }

    public static /* synthetic */ PaymentsHomeState lambda$confirmDeactivatePayments$9(Boolean bool, PaymentsHomeState paymentsHomeState) {
        return paymentsHomeState.updatePaymentsEnabled(bool.booleanValue() ? PaymentsHomeState.PaymentsState.NOT_ACTIVATED : PaymentsHomeState.PaymentsState.ACTIVATED);
    }

    public static /* synthetic */ PaymentsHomeState lambda$refreshExchangeRates$11(PaymentsHomeState paymentsHomeState) {
        return paymentsHomeState.updateExchangeRateLoadState(LoadState.LOADING);
    }

    public void refreshExchangeRates(boolean z) {
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel$$ExternalSyntheticLambda15
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PaymentsHomeViewModel.lambda$refreshExchangeRates$11((PaymentsHomeState) obj);
            }
        });
        this.currencyExchangeRepository.getCurrencyExchange(new AsynchronousCallback.WorkerThread<CurrencyExchange, Throwable>() { // from class: org.thoughtcrime.securesms.payments.preferences.PaymentsHomeViewModel.2
            public static /* synthetic */ PaymentsHomeState lambda$onComplete$0(CurrencyExchange currencyExchange, PaymentsHomeState paymentsHomeState) {
                return paymentsHomeState.updateCurrencyExchange(currencyExchange, LoadState.LOADED);
            }

            public void onComplete(CurrencyExchange currencyExchange) {
                PaymentsHomeViewModel.this.store.update(new PaymentsHomeViewModel$2$$ExternalSyntheticLambda0(currencyExchange));
            }

            public void onError(Throwable th) {
                Log.w(PaymentsHomeViewModel.TAG, th);
                PaymentsHomeViewModel.this.store.update(new PaymentsHomeViewModel$2$$ExternalSyntheticLambda1());
            }

            public static /* synthetic */ PaymentsHomeState lambda$onError$1(PaymentsHomeState paymentsHomeState) {
                return paymentsHomeState.updateExchangeRateLoadState(LoadState.ERROR);
            }
        }, z);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new PaymentsHomeViewModel(new PaymentsHomeRepository(), new PaymentsRepository(), new CurrencyExchangeRepository(ApplicationDependencies.getPayments())));
        }
    }
}
