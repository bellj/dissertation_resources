package org.thoughtcrime.securesms.payments.preferences;

import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.PaymentPreferencesDirections;
import org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsParcelable;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;

/* loaded from: classes4.dex */
public class SetCurrencyFragmentDirections {
    private SetCurrencyFragmentDirections() {
    }

    public static PaymentPreferencesDirections.ActionDirectlyToCreatePayment actionDirectlyToCreatePayment(PayeeParcelable payeeParcelable) {
        return PaymentPreferencesDirections.actionDirectlyToCreatePayment(payeeParcelable);
    }

    public static NavDirections actionDirectlyToPaymentsHome() {
        return PaymentPreferencesDirections.actionDirectlyToPaymentsHome();
    }

    public static PaymentPreferencesDirections.ActionDirectlyToPaymentDetails actionDirectlyToPaymentDetails(PaymentDetailsParcelable paymentDetailsParcelable) {
        return PaymentPreferencesDirections.actionDirectlyToPaymentDetails(paymentDetailsParcelable);
    }

    public static NavDirections actionDirectlyToPaymentsTransfer() {
        return PaymentPreferencesDirections.actionDirectlyToPaymentsTransfer();
    }

    public static NavDirections actionDirectlyToPaymentsBackup() {
        return PaymentPreferencesDirections.actionDirectlyToPaymentsBackup();
    }
}
