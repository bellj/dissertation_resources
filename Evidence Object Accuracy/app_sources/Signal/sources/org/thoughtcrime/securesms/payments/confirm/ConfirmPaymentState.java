package org.thoughtcrime.securesms.payments.confirm;

import java.util.UUID;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.payments.Payee;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class ConfirmPaymentState {
    private final Money amount;
    private final Money balance;
    private final FiatMoney exchange;
    private final Money fee;
    private final FeeStatus feeStatus;
    private final String note;
    private final Payee payee;
    private final UUID paymentId;
    private final Status status;
    private final Money total;

    /* loaded from: classes4.dex */
    public enum FeeStatus {
        NOT_SET,
        STILL_LOADING,
        SET,
        ERROR
    }

    public ConfirmPaymentState(Payee payee, Money money, String str) {
        this(payee, money.toZero(), money, str, money.toZero(), FeeStatus.NOT_SET, null, Status.CONFIRM, null);
    }

    private ConfirmPaymentState(Payee payee, Money money, Money money2, String str, Money money3, FeeStatus feeStatus, FiatMoney fiatMoney, Status status, UUID uuid) {
        this.payee = payee;
        this.balance = money;
        this.amount = money2;
        this.note = str;
        this.fee = money3;
        this.feeStatus = feeStatus;
        this.exchange = fiatMoney;
        this.status = status;
        this.paymentId = uuid;
        this.total = money2.add(money3);
    }

    public Payee getPayee() {
        return this.payee;
    }

    public Money getBalance() {
        return this.balance;
    }

    public Money getAmount() {
        return this.amount;
    }

    public String getNote() {
        return this.note;
    }

    public Money getFee() {
        return this.fee;
    }

    public FeeStatus getFeeStatus() {
        return this.feeStatus;
    }

    public FiatMoney getExchange() {
        return this.exchange;
    }

    public Status getStatus() {
        return this.status;
    }

    public Money getTotal() {
        return this.total;
    }

    public UUID getPaymentId() {
        return this.paymentId;
    }

    public ConfirmPaymentState updateStatus(Status status) {
        return new ConfirmPaymentState(this.payee, this.balance, this.amount, this.note, this.fee, this.feeStatus, this.exchange, status, this.paymentId);
    }

    public ConfirmPaymentState updateBalance(Money money) {
        return new ConfirmPaymentState(this.payee, money, this.amount, this.note, this.fee, this.feeStatus, this.exchange, this.status, this.paymentId);
    }

    public ConfirmPaymentState updateFee(Money money) {
        return new ConfirmPaymentState(this.payee, this.balance, this.amount, this.note, money, FeeStatus.SET, this.exchange, this.status, this.paymentId);
    }

    public ConfirmPaymentState updateFeeStillLoading() {
        Payee payee = this.payee;
        Money money = this.balance;
        Money money2 = this.amount;
        return new ConfirmPaymentState(payee, money, money2, this.note, money2.toZero(), FeeStatus.STILL_LOADING, this.exchange, this.status, this.paymentId);
    }

    public ConfirmPaymentState updateFeeError() {
        Payee payee = this.payee;
        Money money = this.balance;
        Money money2 = this.amount;
        return new ConfirmPaymentState(payee, money, money2, this.note, money2.toZero(), FeeStatus.ERROR, this.exchange, this.status, this.paymentId);
    }

    public ConfirmPaymentState updatePaymentId(UUID uuid) {
        return new ConfirmPaymentState(this.payee, this.balance, this.amount, this.note, this.fee, this.feeStatus, this.exchange, this.status, uuid);
    }

    public ConfirmPaymentState updateExchange(FiatMoney fiatMoney) {
        return new ConfirmPaymentState(this.payee, this.balance, this.amount, this.note, this.fee, this.feeStatus, fiatMoney, this.status, this.paymentId);
    }

    public ConfirmPaymentState timeout() {
        return new ConfirmPaymentState(this.payee, this.balance, this.amount, this.note, this.fee, this.feeStatus, this.exchange, Status.TIMEOUT, this.paymentId);
    }

    /* loaded from: classes4.dex */
    public enum Status {
        CONFIRM,
        SUBMITTING,
        PROCESSING,
        DONE,
        ERROR,
        TIMEOUT;

        public boolean isTerminalStatus() {
            return this == DONE || this == ERROR || this == TIMEOUT;
        }
    }
}
