package org.thoughtcrime.securesms.payments.backup.confirm;

/* loaded from: classes4.dex */
public final class PaymentsRecoveryPhraseConfirmViewState {
    private final boolean isWord1Valid;
    private final boolean isWord2Valid;
    private final int word1Index;
    private final int word2Index;

    private PaymentsRecoveryPhraseConfirmViewState(Builder builder) {
        this.word1Index = builder.word1Index;
        this.word2Index = builder.word2Index;
        this.isWord1Valid = builder.isWord1Valid;
        this.isWord2Valid = builder.isWord2Valid;
    }

    public int getWord1Index() {
        return this.word1Index;
    }

    public int getWord2Index() {
        return this.word2Index;
    }

    public boolean isWord1Valid() {
        return this.isWord1Valid;
    }

    public boolean isWord2Valid() {
        return this.isWord2Valid;
    }

    public boolean areAllWordsValid() {
        return isWord1Valid() && isWord2Valid();
    }

    public Builder buildUpon() {
        return new Builder(this.word1Index, this.word2Index).withValidWord1(isWord1Valid()).withValidWord2(isWord2Valid());
    }

    public static PaymentsRecoveryPhraseConfirmViewState init(int i, int i2) {
        return new Builder(i, i2).build();
    }

    /* loaded from: classes4.dex */
    public static final class Builder {
        private boolean isWord1Valid;
        private boolean isWord2Valid;
        private final int word1Index;
        private final int word2Index;

        private Builder(int i, int i2) {
            this.word1Index = i;
            this.word2Index = i2;
        }

        public Builder withValidWord1(boolean z) {
            this.isWord1Valid = z;
            return this;
        }

        public Builder withValidWord2(boolean z) {
            this.isWord2Valid = z;
            return this;
        }

        public PaymentsRecoveryPhraseConfirmViewState build() {
            return new PaymentsRecoveryPhraseConfirmViewState(this);
        }
    }
}
