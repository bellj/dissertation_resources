package org.thoughtcrime.securesms.payments;

import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public abstract class PaymentTransactionId {
    private PaymentTransactionId() {
    }

    /* loaded from: classes4.dex */
    public static final class MobileCoin extends PaymentTransactionId {
        private final Money.MobileCoin fee;
        private final byte[] receipt;
        private final byte[] transaction;

        public MobileCoin(byte[] bArr, byte[] bArr2, Money.MobileCoin mobileCoin) {
            super();
            this.transaction = bArr;
            this.receipt = bArr2;
            this.fee = mobileCoin;
            if (bArr.length == 0 || bArr2.length == 0) {
                throw new AssertionError("Both transaction and receipt must be specified");
            }
        }

        public byte[] getTransaction() {
            return this.transaction;
        }

        public byte[] getReceipt() {
            return this.receipt;
        }

        public Money.MobileCoin getFee() {
            return this.fee;
        }
    }
}
