package org.thoughtcrime.securesms.payments.backup;

import android.os.Bundle;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class PaymentsRecoveryStartFragmentDirections {
    private PaymentsRecoveryStartFragmentDirections() {
    }

    public static ActionPaymentsRecoveryStartToPaymentsRecoveryPhrase actionPaymentsRecoveryStartToPaymentsRecoveryPhrase(boolean z) {
        return new ActionPaymentsRecoveryStartToPaymentsRecoveryPhrase(z);
    }

    public static NavDirections actionPaymentsRecoveryStartToPaymentsRecoveryEntry() {
        return new ActionOnlyNavDirections(R.id.action_paymentsRecoveryStart_to_paymentsRecoveryEntry);
    }

    public static NavDirections actionPaymentsRecoveryStartToPaymentsRecoveryPaste() {
        return new ActionOnlyNavDirections(R.id.action_paymentsRecoveryStart_to_paymentsRecoveryPaste);
    }

    /* loaded from: classes4.dex */
    public static class ActionPaymentsRecoveryStartToPaymentsRecoveryPhrase implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_paymentsRecoveryStart_to_paymentsRecoveryPhrase;
        }

        private ActionPaymentsRecoveryStartToPaymentsRecoveryPhrase(boolean z) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("finish_on_confirm", Boolean.valueOf(z));
        }

        public ActionPaymentsRecoveryStartToPaymentsRecoveryPhrase setFinishOnConfirm(boolean z) {
            this.arguments.put("finish_on_confirm", Boolean.valueOf(z));
            return this;
        }

        public ActionPaymentsRecoveryStartToPaymentsRecoveryPhrase setWords(String[] strArr) {
            this.arguments.put("words", strArr);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("finish_on_confirm")) {
                bundle.putBoolean("finish_on_confirm", ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue());
            }
            if (this.arguments.containsKey("words")) {
                bundle.putStringArray("words", (String[]) this.arguments.get("words"));
            } else {
                bundle.putStringArray("words", null);
            }
            return bundle;
        }

        public boolean getFinishOnConfirm() {
            return ((Boolean) this.arguments.get("finish_on_confirm")).booleanValue();
        }

        public String[] getWords() {
            return (String[]) this.arguments.get("words");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionPaymentsRecoveryStartToPaymentsRecoveryPhrase actionPaymentsRecoveryStartToPaymentsRecoveryPhrase = (ActionPaymentsRecoveryStartToPaymentsRecoveryPhrase) obj;
            if (this.arguments.containsKey("finish_on_confirm") != actionPaymentsRecoveryStartToPaymentsRecoveryPhrase.arguments.containsKey("finish_on_confirm") || getFinishOnConfirm() != actionPaymentsRecoveryStartToPaymentsRecoveryPhrase.getFinishOnConfirm() || this.arguments.containsKey("words") != actionPaymentsRecoveryStartToPaymentsRecoveryPhrase.arguments.containsKey("words")) {
                return false;
            }
            if (getWords() == null ? actionPaymentsRecoveryStartToPaymentsRecoveryPhrase.getWords() == null : getWords().equals(actionPaymentsRecoveryStartToPaymentsRecoveryPhrase.getWords())) {
                return getActionId() == actionPaymentsRecoveryStartToPaymentsRecoveryPhrase.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((((getFinishOnConfirm() ? 1 : 0) + 31) * 31) + Arrays.hashCode(getWords())) * 31) + getActionId();
        }

        public String toString() {
            return "ActionPaymentsRecoveryStartToPaymentsRecoveryPhrase(actionId=" + getActionId() + "){finishOnConfirm=" + getFinishOnConfirm() + ", words=" + getWords() + "}";
        }
    }
}
