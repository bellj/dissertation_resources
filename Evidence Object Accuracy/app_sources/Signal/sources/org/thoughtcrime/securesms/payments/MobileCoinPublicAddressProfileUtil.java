package org.thoughtcrime.securesms.payments;

import com.google.protobuf.ByteString;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.thoughtcrime.securesms.payments.PaymentsAddressException;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class MobileCoinPublicAddressProfileUtil {
    private MobileCoinPublicAddressProfileUtil() {
    }

    public static SignalServiceProtos.PaymentAddress signPaymentsAddress(byte[] bArr, IdentityKeyPair identityKeyPair) {
        return SignalServiceProtos.PaymentAddress.newBuilder().setMobileCoinAddress(SignalServiceProtos.PaymentAddress.MobileCoinAddress.newBuilder().setAddress(ByteString.copyFrom(bArr)).setSignature(ByteString.copyFrom(identityKeyPair.getPrivateKey().calculateSignature(bArr)))).build();
    }

    public static byte[] verifyPaymentsAddress(SignalServiceProtos.PaymentAddress paymentAddress, IdentityKey identityKey) throws PaymentsAddressException {
        if (paymentAddress.hasMobileCoinAddress()) {
            byte[] byteArray = paymentAddress.getMobileCoinAddress().getAddress().toByteArray();
            byte[] byteArray2 = paymentAddress.getMobileCoinAddress().getSignature().toByteArray();
            if (byteArray2.length == 64 && identityKey.getPublicKey().verifySignature(byteArray, byteArray2)) {
                return byteArray;
            }
            throw new PaymentsAddressException(PaymentsAddressException.Code.INVALID_ADDRESS_SIGNATURE);
        }
        throw new PaymentsAddressException(PaymentsAddressException.Code.NO_ADDRESS);
    }
}
