package org.thoughtcrime.securesms.payments.preferences.addmoney;

import android.net.Uri;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import org.signal.core.util.StringUtil;
import org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyRepository;
import org.thoughtcrime.securesms.util.AsynchronousCallback;

/* loaded from: classes4.dex */
public final class PaymentsAddMoneyViewModel extends ViewModel {
    private final MutableLiveData<PaymentsAddMoneyRepository.Error> errors = new MutableLiveData<>();
    private final LiveData<CharSequence> selfAddressAbbreviated;
    private final MutableLiveData<AddressAndUri> selfAddressAndUri;
    private final LiveData<String> selfAddressB58;
    private final LiveData<Uri> selfAddressUri;

    PaymentsAddMoneyViewModel(PaymentsAddMoneyRepository paymentsAddMoneyRepository) {
        MutableLiveData<AddressAndUri> mutableLiveData = new MutableLiveData<>();
        this.selfAddressAndUri = mutableLiveData;
        paymentsAddMoneyRepository.getWalletAddress(new AsynchronousCallback.MainThread<AddressAndUri, PaymentsAddMoneyRepository.Error>() { // from class: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyViewModel.1
            @Override // org.thoughtcrime.securesms.util.AsynchronousCallback.MainThread
            public /* synthetic */ AsynchronousCallback.WorkerThread<AddressAndUri, PaymentsAddMoneyRepository.Error> toWorkerCallback() {
                return AsynchronousCallback.MainThread.CC.$default$toWorkerCallback(this);
            }

            public void onComplete(AddressAndUri addressAndUri) {
                PaymentsAddMoneyViewModel.this.selfAddressAndUri.setValue(addressAndUri);
            }

            public void onError(PaymentsAddMoneyRepository.Error error) {
                PaymentsAddMoneyViewModel.this.errors.setValue(error);
            }
        });
        LiveData<String> map = Transformations.map(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((AddressAndUri) obj).getAddressB58();
            }
        });
        this.selfAddressB58 = map;
        this.selfAddressUri = Transformations.map(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyViewModel$$ExternalSyntheticLambda1
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((AddressAndUri) obj).getUri();
            }
        });
        this.selfAddressAbbreviated = Transformations.map(map, new Function() { // from class: org.thoughtcrime.securesms.payments.preferences.addmoney.PaymentsAddMoneyViewModel$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return StringUtil.abbreviateInMiddle((String) obj, 17);
            }
        });
    }

    public LiveData<String> getSelfAddressB58() {
        return this.selfAddressB58;
    }

    public LiveData<CharSequence> getSelfAddressAbbreviated() {
        return this.selfAddressAbbreviated;
    }

    public LiveData<PaymentsAddMoneyRepository.Error> getErrors() {
        return this.errors;
    }

    LiveData<Uri> getSelfAddressUriForQr() {
        return this.selfAddressUri;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new PaymentsAddMoneyViewModel(new PaymentsAddMoneyRepository()));
        }
    }
}
