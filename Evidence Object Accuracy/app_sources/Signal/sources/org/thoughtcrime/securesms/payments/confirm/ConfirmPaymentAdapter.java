package org.thoughtcrime.securesms.payments.confirm;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import com.airbnb.lottie.LottieAnimationView;
import j$.util.function.Function;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentAdapter;
import org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentState;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.whispersystems.signalservice.api.payments.FormatterOptions;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class ConfirmPaymentAdapter extends MappingAdapter {

    /* loaded from: classes4.dex */
    public interface Callbacks {
        void onConfirmPayment();
    }

    /* loaded from: classes4.dex */
    public static final class Divider implements MappingModel<Divider> {
        public boolean areContentsTheSame(Divider divider) {
            return true;
        }

        public boolean areItemsTheSame(Divider divider) {
            return true;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Divider divider) {
            return MappingModel.CC.$default$getChangePayload(this, divider);
        }
    }

    /* loaded from: classes4.dex */
    public static class LoadingItem implements MappingModel<LoadingItem> {
        public boolean areContentsTheSame(LoadingItem loadingItem) {
            return true;
        }

        public boolean areItemsTheSame(LoadingItem loadingItem) {
            return true;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(LoadingItem loadingItem) {
            return MappingModel.CC.$default$getChangePayload(this, loadingItem);
        }
    }

    public ConfirmPaymentAdapter(Callbacks callbacks) {
        registerFactory(LoadingItem.class, new Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new ConfirmPaymentAdapter.LoadingItemViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.confirm_payment_loading_item);
        registerFactory(LineItem.class, new Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentAdapter$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new ConfirmPaymentAdapter.LineItemViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.confirm_payment_line_item);
        registerFactory(Divider.class, new EmojiPageViewGridAdapter$$ExternalSyntheticLambda3(), R.layout.confirm_payment_divider);
        registerFactory(TotalLineItem.class, new Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentAdapter$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new ConfirmPaymentAdapter.TotalLineItemViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.confirm_payment_total_line_item);
        registerFactory(ConfirmPaymentStatus.class, new Function() { // from class: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentAdapter$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConfirmPaymentAdapter.$r8$lambda$ec3t0fjhtqNMZZ6l3J9nXX66iyk(ConfirmPaymentAdapter.Callbacks.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.confirm_payment_status);
    }

    public static /* synthetic */ MappingViewHolder lambda$new$0(Callbacks callbacks, View view) {
        return new ConfirmPaymentViewHolder(view, callbacks);
    }

    /* loaded from: classes4.dex */
    public static class LineItem implements MappingModel<LineItem> {
        private final CharSequence description;
        private final String value;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(LineItem lineItem) {
            return MappingModel.CC.$default$getChangePayload(this, lineItem);
        }

        public LineItem(CharSequence charSequence, String str) {
            this.description = charSequence;
            this.value = str;
        }

        public CharSequence getDescription() {
            return this.description;
        }

        public String getValue() {
            return this.value;
        }

        public boolean areItemsTheSame(LineItem lineItem) {
            return this.description.toString().equals(lineItem.description.toString());
        }

        public boolean areContentsTheSame(LineItem lineItem) {
            return this.value.equals(lineItem.value);
        }
    }

    /* loaded from: classes4.dex */
    public static class TotalLineItem implements MappingModel<TotalLineItem> {
        private final LineItem lineItem;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(TotalLineItem totalLineItem) {
            return MappingModel.CC.$default$getChangePayload(this, totalLineItem);
        }

        public TotalLineItem(String str, String str2) {
            this.lineItem = new LineItem(str, str2);
        }

        public LineItem getLineItem() {
            return this.lineItem;
        }

        public boolean areItemsTheSame(TotalLineItem totalLineItem) {
            return this.lineItem.areItemsTheSame(totalLineItem.lineItem);
        }

        public boolean areContentsTheSame(TotalLineItem totalLineItem) {
            return this.lineItem.areContentsTheSame(totalLineItem.lineItem);
        }
    }

    /* loaded from: classes4.dex */
    public static class ConfirmPaymentStatus implements MappingModel<ConfirmPaymentStatus> {
        private final Money balance;
        private final ConfirmPaymentState.FeeStatus feeStatus;
        private final ConfirmPaymentState.Status status;

        public boolean areItemsTheSame(ConfirmPaymentStatus confirmPaymentStatus) {
            return true;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(ConfirmPaymentStatus confirmPaymentStatus) {
            return MappingModel.CC.$default$getChangePayload(this, confirmPaymentStatus);
        }

        public ConfirmPaymentStatus(ConfirmPaymentState.Status status, ConfirmPaymentState.FeeStatus feeStatus, Money money) {
            this.status = status;
            this.feeStatus = feeStatus;
            this.balance = money;
        }

        public int getConfirmPaymentVisibility() {
            return this.status == ConfirmPaymentState.Status.CONFIRM ? 0 : 4;
        }

        public boolean getConfirmPaymentEnabled() {
            return this.status == ConfirmPaymentState.Status.CONFIRM && this.feeStatus == ConfirmPaymentState.FeeStatus.SET;
        }

        public ConfirmPaymentState.Status getStatus() {
            return this.status;
        }

        public CharSequence getInfoText(Context context) {
            switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$Status[this.status.ordinal()]) {
                case 1:
                    return context.getString(R.string.ConfirmPayment__balance_s, this.balance.toString(FormatterOptions.defaults()));
                case 2:
                    return context.getString(R.string.ConfirmPayment__submitting_payment);
                case 3:
                    return context.getString(R.string.ConfirmPayment__processing_payment);
                case 4:
                    return context.getString(R.string.ConfirmPayment__payment_complete);
                case 5:
                    return SpanUtil.color(ContextCompat.getColor(context, R.color.signal_alert_primary), context.getString(R.string.ConfirmPayment__payment_failed));
                case 6:
                    return context.getString(R.string.ConfirmPayment__payment_will_continue_processing);
                default:
                    throw new AssertionError();
            }
        }

        public boolean areContentsTheSame(ConfirmPaymentStatus confirmPaymentStatus) {
            return this.status == confirmPaymentStatus.status && this.feeStatus == confirmPaymentStatus.feeStatus && this.balance.equals(confirmPaymentStatus.balance);
        }
    }

    /* renamed from: org.thoughtcrime.securesms.payments.confirm.ConfirmPaymentAdapter$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$Status;

        static {
            int[] iArr = new int[ConfirmPaymentState.Status.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$Status = iArr;
            try {
                iArr[ConfirmPaymentState.Status.CONFIRM.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$Status[ConfirmPaymentState.Status.SUBMITTING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$Status[ConfirmPaymentState.Status.PROCESSING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$Status[ConfirmPaymentState.Status.DONE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$Status[ConfirmPaymentState.Status.ERROR.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$Status[ConfirmPaymentState.Status.TIMEOUT.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class LoadingItemViewHolder extends MappingViewHolder<LoadingItem> {
        public void bind(LoadingItem loadingItem) {
        }

        public LoadingItemViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes4.dex */
    public static final class LineItemViewHolder extends MappingViewHolder<LineItem> {
        private final TextView description = ((TextView) findViewById(R.id.confirm_payment_line_item_description));
        private final TextView value = ((TextView) findViewById(R.id.confirm_payment_line_item_value));

        public LineItemViewHolder(View view) {
            super(view);
        }

        public void bind(LineItem lineItem) {
            this.description.setText(lineItem.getDescription());
            this.value.setText(lineItem.getValue());
        }
    }

    /* loaded from: classes4.dex */
    public static class TotalLineItemViewHolder extends MappingViewHolder<TotalLineItem> {
        private final LineItemViewHolder delegate;

        public TotalLineItemViewHolder(View view) {
            super(view);
            this.delegate = new LineItemViewHolder(view);
        }

        public void bind(TotalLineItem totalLineItem) {
            this.delegate.bind(totalLineItem.getLineItem());
        }
    }

    /* loaded from: classes4.dex */
    public static class ConfirmPaymentViewHolder extends MappingViewHolder<ConfirmPaymentStatus> {
        private final Callbacks callbacks;
        private final LottieAnimationView completed = ((LottieAnimationView) findViewById(R.id.confirm_payment_success_lottie));
        private final View confirmPayment = findViewById(R.id.confirm_payment_status_confirm);
        private final LottieAnimationView failed = ((LottieAnimationView) findViewById(R.id.confirm_payment_error_lottie));
        private final LottieAnimationView inProgress = ((LottieAnimationView) findViewById(R.id.confirm_payment_spinner_lottie));
        private final TextView infoText = ((TextView) findViewById(R.id.confirm_payment_status_info));
        private final LottieAnimationView timeout = ((LottieAnimationView) findViewById(R.id.confirm_payment_timeout_lottie));

        public ConfirmPaymentViewHolder(View view, Callbacks callbacks) {
            super(view);
            this.callbacks = callbacks;
        }

        public /* synthetic */ void lambda$bind$0(View view) {
            this.callbacks.onConfirmPayment();
        }

        public void bind(ConfirmPaymentStatus confirmPaymentStatus) {
            this.confirmPayment.setOnClickListener(new ConfirmPaymentAdapter$ConfirmPaymentViewHolder$$ExternalSyntheticLambda0(this));
            this.confirmPayment.setVisibility(confirmPaymentStatus.getConfirmPaymentVisibility());
            this.confirmPayment.setEnabled(confirmPaymentStatus.getConfirmPaymentEnabled());
            this.infoText.setText(confirmPaymentStatus.getInfoText(getContext()));
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$confirm$ConfirmPaymentState$Status[confirmPaymentStatus.getStatus().ordinal()];
            if (i == 2 || i == 3) {
                playNextAnimation(this.inProgress, this.completed, this.failed, this.timeout);
            } else if (i == 4) {
                playNextAnimation(this.completed, this.inProgress, this.failed, this.timeout);
            } else if (i == 5) {
                playNextAnimation(this.failed, this.inProgress, this.completed, this.timeout);
            } else if (i == 6) {
                playNextAnimation(this.timeout, this.inProgress, this.completed, this.failed);
            }
        }

        private static void playNextAnimation(LottieAnimationView lottieAnimationView, LottieAnimationView... lottieAnimationViewArr) {
            for (LottieAnimationView lottieAnimationView2 : lottieAnimationViewArr) {
                lottieAnimationView2.setVisibility(4);
            }
            lottieAnimationView.setVisibility(0);
            lottieAnimationView.post(new ConfirmPaymentAdapter$ConfirmPaymentViewHolder$$ExternalSyntheticLambda1(lottieAnimationView));
        }

        public static /* synthetic */ void lambda$playNextAnimation$1(LottieAnimationView lottieAnimationView) {
            if (!lottieAnimationView.isAnimating()) {
                lottieAnimationView.playAnimation();
            }
        }
    }
}
