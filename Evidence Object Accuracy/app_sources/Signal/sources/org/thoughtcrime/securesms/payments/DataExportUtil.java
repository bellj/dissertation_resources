package org.thoughtcrime.securesms.payments;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import j$.time.Instant;
import j$.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* loaded from: classes4.dex */
public final class DataExportUtil {
    private DataExportUtil() {
    }

    public static String createTsv() {
        if (!FeatureFlags.internalUser()) {
            throw new AssertionError("This is intended for internal use only");
        } else if (Build.VERSION.SDK_INT >= 26) {
            List<PaymentDatabase.PaymentTransaction> all = SignalDatabase.payments().getAll();
            MobileCoinLedgerWrapper value = SignalStore.paymentsValues().liveMobileCoinLedger().getValue();
            Objects.requireNonNull(value);
            return createTsv(LedgerReconcile.reconcile(all, value));
        } else {
            throw new AssertionError();
        }
    }

    private static String createTsv(List<Payment> list) {
        Application application = ApplicationDependencies.getApplication();
        StringBuilder sb = new StringBuilder();
        int i = 5;
        sb.append(String.format(Locale.US, "%s\t%s\t%s\t%s\t%s%n", "Date Time", "From", "To", "Amount", "Fee"));
        for (Payment payment : list) {
            if (payment.getState() == State.SUCCESSFUL) {
                String displayName = Recipient.self().getDisplayName(application);
                String describePayee = describePayee(application, payment.getPayee());
                int i2 = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$Direction[payment.getDirection().ordinal()];
                if (i2 != 1) {
                    if (i2 == 2) {
                        describePayee = displayName;
                        displayName = describePayee;
                    } else {
                        throw new AssertionError();
                    }
                }
                Locale locale = Locale.US;
                Object[] objArr = new Object[i];
                objArr[0] = DateTimeFormatter.ISO_INSTANT.format(Instant.ofEpochMilli(payment.getDisplayTimestamp()));
                objArr[1] = displayName;
                objArr[2] = describePayee;
                objArr[3] = payment.getAmountWithDirection().requireMobileCoin().toBigDecimal();
                objArr[4] = payment.getFee().requireMobileCoin().toBigDecimal();
                sb.append(String.format(locale, "%s\t%s\t%s\t%s\t%s%n", objArr));
                i = 5;
            }
        }
        return sb.toString();
    }

    /* renamed from: org.thoughtcrime.securesms.payments.DataExportUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$Direction;

        static {
            int[] iArr = new int[Direction.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$Direction = iArr;
            try {
                iArr[Direction.SENT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$Direction[Direction.RECEIVED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    private static String describePayee(Context context, Payee payee) {
        if (payee.hasRecipientId()) {
            return Recipient.resolved(payee.requireRecipientId()).getDisplayName(context);
        }
        return payee.hasPublicAddress() ? payee.requirePublicAddress().getPaymentAddressBase58() : "Unknown";
    }
}
