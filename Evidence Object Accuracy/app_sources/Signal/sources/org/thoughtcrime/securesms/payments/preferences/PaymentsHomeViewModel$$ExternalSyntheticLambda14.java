package org.thoughtcrime.securesms.payments.preferences;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.preferences.model.PaymentItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PaymentsHomeViewModel$$ExternalSyntheticLambda14 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return PaymentItem.fromPayment((Payment) obj);
    }
}
