package org.thoughtcrime.securesms.payments;

import android.net.Uri;
import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.Verifier;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.util.Hex;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.Set;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.internal.push.AuthCredentials;

/* loaded from: classes4.dex */
public final class MobileCoinMainNetConfig extends MobileCoinConfig {
    private final SignalServiceAccountManager signalServiceAccountManager;

    public MobileCoinMainNetConfig(SignalServiceAccountManager signalServiceAccountManager) {
        this.signalServiceAccountManager = signalServiceAccountManager;
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public Uri getConsensusUri() {
        return Uri.parse("mc://node1.prod.mobilecoinww.com");
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public Uri getFogUri() {
        return Uri.parse("fog://service.fog.mob.production.namda.net");
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public Uri getFogReportUri() {
        return Uri.parse("fog://fog-rpt-prd.namda.net");
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public byte[] getFogAuthoritySpki() {
        return Base64.decodeOrThrow("MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAxaNIOgcoQtq0S64dFVha\n6rn0hDv/ec+W0cKRdFKygiyp5xuWdW3YKVAkK1PPgSDD2dwmMN/1xcGWrPMqezx1\nh1xCzbr7HL7XvLyFyoiMB2JYd7aoIuGIbHpCOlpm8ulVnkOX7BNuo0Hi2F0AAHyT\nPwmtVMt6RZmae1Z/Pl2I06+GgWN6vufV7jcjiLT3yQPsn1kVSj+DYCf3zq+1sCkn\nKIvoRPMdQh9Vi3I/fqNXz00DSB7lt3v5/FQ6sPbjljqdGD/qUl4xKRW+EoDLlAUf\nzahomQOLXVAlxcws3Ua5cZUhaJi6U5jVfw5Ng2N7FwX/D5oX82r9o3xcFqhWpGnf\nSxSrAudv1X7WskXomKhUzMl/0exWpcJbdrQWB/qshzi9Et7HEDNY+xEDiwGiikj5\nf0Lb+QA4mBMlAhY/cmWec8NKi1gf3Dmubh6c3sNteb9OpZ/irA3AfE8jI37K1rve\nzDI8kbNtmYgvyhfz0lZzRT2WAfffiTe565rJglvKa8rh8eszKk2HC9DyxUb/TcyL\n/OjGhe2fDYO2t6brAXCqjPZAEkVJq3I30NmnPdE19SQeP7wuaUIb3U7MGxoZC/Nu\nJoxZh8svvZ8cyqVjG+dOQ6/UfrFY0jiswT8AsrfqBis/ZV5EFukZr+zbPtg2MH0H\n3tSJ14BCLduvc7FY6lAZmOcCAwEAAQ==");
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public AuthCredentials getAuth() throws IOException {
        return this.signalServiceAccountManager.getPaymentsAuthorization();
    }

    @Override // org.thoughtcrime.securesms.payments.MobileCoinConfig
    public ClientConfig getConfig() {
        try {
            byte[] byteArray = Hex.toByteArray("e66db38b8a43a33f6c1610d335a361963bb2b31e056af0dc0a895ac6c857cab9");
            byte[] byteArray2 = Hex.toByteArray("653228afd2b02a6c28f1dc3b108b1dfa457d170b32ae8ec2978f941bd1655c83");
            byte[] byteArray3 = Hex.toByteArray("709ab90621e3a8d9eb26ed9e2830e091beceebd55fb01c5d7c31d27e83b9b0d1");
            byte[] byteArray4 = Hex.toByteArray("f3f7e9a674c55fb2af543513527b6a7872de305bac171783f6716a0bf6919499");
            byte[] byteArray5 = Hex.toByteArray("511eab36de691ded50eb08b173304194da8b9d86bfdd7102001fe6bb279c3666");
            byte[] byteArray6 = Hex.toByteArray("89db0d1684fcc98258295c39f4ab68f7de5917ef30f0004d9a86f29930cebbbd");
            byte[] byteArray7 = Hex.toByteArray("ddd59da874fdf3239d5edb1ef251df07a8728c9ef63057dd0b50ade5a9ddb041");
            byte[] byteArray8 = Hex.toByteArray("dd84abda7f05116e21fcd1ee6361b0ec29445fff0472131eaf37bf06255b567a");
            Set<X509Certificate> trustRoots = MobileCoinConfig.getTrustRoots(R.raw.signal_mobilecoin_authority);
            ClientConfig clientConfig = new ClientConfig();
            String[] strArr = {"INTEL-SA-00334"};
            clientConfig.logAdapter = new MobileCoinLogAdapter();
            clientConfig.fogView = new ClientConfig.Service().withTrustRoots(trustRoots).withVerifier(new Verifier().withMrEnclave(byteArray7, null, strArr).withMrEnclave(byteArray8, null, strArr));
            clientConfig.fogLedger = new ClientConfig.Service().withTrustRoots(trustRoots).withVerifier(new Verifier().withMrEnclave(byteArray5, null, strArr).withMrEnclave(byteArray6, null, strArr));
            clientConfig.consensus = new ClientConfig.Service().withTrustRoots(trustRoots).withVerifier(new Verifier().withMrEnclave(byteArray, null, strArr).withMrEnclave(byteArray2, null, strArr));
            clientConfig.f12report = new ClientConfig.Service().withVerifier(new Verifier().withMrEnclave(byteArray3, null, strArr).withMrEnclave(byteArray4, null, strArr));
            return clientConfig;
        } catch (AttestationException unused) {
            throw new IllegalStateException();
        }
    }
}
