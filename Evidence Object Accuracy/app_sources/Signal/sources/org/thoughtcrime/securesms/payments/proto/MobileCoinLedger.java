package org.thoughtcrime.securesms.payments.proto;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public final class MobileCoinLedger extends GeneratedMessageLite<MobileCoinLedger, Builder> implements MobileCoinLedgerOrBuilder {
    public static final int ASOFTIMESTAMP_FIELD_NUMBER;
    public static final int BALANCE_FIELD_NUMBER;
    private static final MobileCoinLedger DEFAULT_INSTANCE;
    public static final int HIGHESTBLOCK_FIELD_NUMBER;
    private static volatile Parser<MobileCoinLedger> PARSER;
    public static final int SPENTTXOS_FIELD_NUMBER;
    public static final int TRANSFERABLEBALANCE_FIELD_NUMBER;
    public static final int UNSPENTTXOS_FIELD_NUMBER;
    private long asOfTimeStamp_;
    private long balance_;
    private Block highestBlock_;
    private Internal.ProtobufList<OwnedTXO> spentTxos_ = GeneratedMessageLite.emptyProtobufList();
    private long transferableBalance_;
    private Internal.ProtobufList<OwnedTXO> unspentTxos_ = GeneratedMessageLite.emptyProtobufList();

    /* loaded from: classes4.dex */
    public interface BlockOrBuilder extends MessageLiteOrBuilder {
        long getBlockNumber();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        long getTimestamp();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes4.dex */
    public interface OwnedTXOOrBuilder extends MessageLiteOrBuilder {
        long getAmount();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        ByteString getKeyImage();

        ByteString getPublicKey();

        Block getReceivedInBlock();

        Block getSpentInBlock();

        boolean hasReceivedInBlock();

        boolean hasSpentInBlock();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private MobileCoinLedger() {
    }

    /* loaded from: classes4.dex */
    public static final class OwnedTXO extends GeneratedMessageLite<OwnedTXO, Builder> implements OwnedTXOOrBuilder {
        public static final int AMOUNT_FIELD_NUMBER;
        private static final OwnedTXO DEFAULT_INSTANCE;
        public static final int KEYIMAGE_FIELD_NUMBER;
        private static volatile Parser<OwnedTXO> PARSER;
        public static final int PUBLICKEY_FIELD_NUMBER;
        public static final int RECEIVEDINBLOCK_FIELD_NUMBER;
        public static final int SPENTINBLOCK_FIELD_NUMBER;
        private long amount_;
        private ByteString keyImage_;
        private ByteString publicKey_;
        private Block receivedInBlock_;
        private Block spentInBlock_;

        private OwnedTXO() {
            ByteString byteString = ByteString.EMPTY;
            this.keyImage_ = byteString;
            this.publicKey_ = byteString;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
        public long getAmount() {
            return this.amount_;
        }

        public void setAmount(long j) {
            this.amount_ = j;
        }

        public void clearAmount() {
            this.amount_ = 0;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
        public ByteString getKeyImage() {
            return this.keyImage_;
        }

        public void setKeyImage(ByteString byteString) {
            byteString.getClass();
            this.keyImage_ = byteString;
        }

        public void clearKeyImage() {
            this.keyImage_ = getDefaultInstance().getKeyImage();
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
        public ByteString getPublicKey() {
            return this.publicKey_;
        }

        public void setPublicKey(ByteString byteString) {
            byteString.getClass();
            this.publicKey_ = byteString;
        }

        public void clearPublicKey() {
            this.publicKey_ = getDefaultInstance().getPublicKey();
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
        public boolean hasReceivedInBlock() {
            return this.receivedInBlock_ != null;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
        public Block getReceivedInBlock() {
            Block block = this.receivedInBlock_;
            return block == null ? Block.getDefaultInstance() : block;
        }

        public void setReceivedInBlock(Block block) {
            block.getClass();
            this.receivedInBlock_ = block;
        }

        public void mergeReceivedInBlock(Block block) {
            block.getClass();
            Block block2 = this.receivedInBlock_;
            if (block2 == null || block2 == Block.getDefaultInstance()) {
                this.receivedInBlock_ = block;
            } else {
                this.receivedInBlock_ = Block.newBuilder(this.receivedInBlock_).mergeFrom((Block.Builder) block).buildPartial();
            }
        }

        public void clearReceivedInBlock() {
            this.receivedInBlock_ = null;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
        public boolean hasSpentInBlock() {
            return this.spentInBlock_ != null;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
        public Block getSpentInBlock() {
            Block block = this.spentInBlock_;
            return block == null ? Block.getDefaultInstance() : block;
        }

        public void setSpentInBlock(Block block) {
            block.getClass();
            this.spentInBlock_ = block;
        }

        public void mergeSpentInBlock(Block block) {
            block.getClass();
            Block block2 = this.spentInBlock_;
            if (block2 == null || block2 == Block.getDefaultInstance()) {
                this.spentInBlock_ = block;
            } else {
                this.spentInBlock_ = Block.newBuilder(this.spentInBlock_).mergeFrom((Block.Builder) block).buildPartial();
            }
        }

        public void clearSpentInBlock() {
            this.spentInBlock_ = null;
        }

        public static OwnedTXO parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (OwnedTXO) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static OwnedTXO parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (OwnedTXO) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static OwnedTXO parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (OwnedTXO) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static OwnedTXO parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (OwnedTXO) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static OwnedTXO parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (OwnedTXO) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static OwnedTXO parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (OwnedTXO) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static OwnedTXO parseFrom(InputStream inputStream) throws IOException {
            return (OwnedTXO) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static OwnedTXO parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (OwnedTXO) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static OwnedTXO parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (OwnedTXO) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static OwnedTXO parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (OwnedTXO) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static OwnedTXO parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (OwnedTXO) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static OwnedTXO parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (OwnedTXO) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(OwnedTXO ownedTXO) {
            return DEFAULT_INSTANCE.createBuilder(ownedTXO);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<OwnedTXO, Builder> implements OwnedTXOOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(OwnedTXO.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
            public long getAmount() {
                return ((OwnedTXO) this.instance).getAmount();
            }

            public Builder setAmount(long j) {
                copyOnWrite();
                ((OwnedTXO) this.instance).setAmount(j);
                return this;
            }

            public Builder clearAmount() {
                copyOnWrite();
                ((OwnedTXO) this.instance).clearAmount();
                return this;
            }

            @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
            public ByteString getKeyImage() {
                return ((OwnedTXO) this.instance).getKeyImage();
            }

            public Builder setKeyImage(ByteString byteString) {
                copyOnWrite();
                ((OwnedTXO) this.instance).setKeyImage(byteString);
                return this;
            }

            public Builder clearKeyImage() {
                copyOnWrite();
                ((OwnedTXO) this.instance).clearKeyImage();
                return this;
            }

            @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
            public ByteString getPublicKey() {
                return ((OwnedTXO) this.instance).getPublicKey();
            }

            public Builder setPublicKey(ByteString byteString) {
                copyOnWrite();
                ((OwnedTXO) this.instance).setPublicKey(byteString);
                return this;
            }

            public Builder clearPublicKey() {
                copyOnWrite();
                ((OwnedTXO) this.instance).clearPublicKey();
                return this;
            }

            @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
            public boolean hasReceivedInBlock() {
                return ((OwnedTXO) this.instance).hasReceivedInBlock();
            }

            @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
            public Block getReceivedInBlock() {
                return ((OwnedTXO) this.instance).getReceivedInBlock();
            }

            public Builder setReceivedInBlock(Block block) {
                copyOnWrite();
                ((OwnedTXO) this.instance).setReceivedInBlock(block);
                return this;
            }

            public Builder setReceivedInBlock(Block.Builder builder) {
                copyOnWrite();
                ((OwnedTXO) this.instance).setReceivedInBlock(builder.build());
                return this;
            }

            public Builder mergeReceivedInBlock(Block block) {
                copyOnWrite();
                ((OwnedTXO) this.instance).mergeReceivedInBlock(block);
                return this;
            }

            public Builder clearReceivedInBlock() {
                copyOnWrite();
                ((OwnedTXO) this.instance).clearReceivedInBlock();
                return this;
            }

            @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
            public boolean hasSpentInBlock() {
                return ((OwnedTXO) this.instance).hasSpentInBlock();
            }

            @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.OwnedTXOOrBuilder
            public Block getSpentInBlock() {
                return ((OwnedTXO) this.instance).getSpentInBlock();
            }

            public Builder setSpentInBlock(Block block) {
                copyOnWrite();
                ((OwnedTXO) this.instance).setSpentInBlock(block);
                return this;
            }

            public Builder setSpentInBlock(Block.Builder builder) {
                copyOnWrite();
                ((OwnedTXO) this.instance).setSpentInBlock(builder.build());
                return this;
            }

            public Builder mergeSpentInBlock(Block block) {
                copyOnWrite();
                ((OwnedTXO) this.instance).mergeSpentInBlock(block);
                return this;
            }

            public Builder clearSpentInBlock() {
                copyOnWrite();
                ((OwnedTXO) this.instance).clearSpentInBlock();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new OwnedTXO();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0000\u0000\u0000\u0001\u0003\u0002\n\u0003\n\u0004\t\u0005\t", new Object[]{"amount_", "keyImage_", "publicKey_", "receivedInBlock_", "spentInBlock_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<OwnedTXO> parser = PARSER;
                    if (parser == null) {
                        synchronized (OwnedTXO.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            OwnedTXO ownedTXO = new OwnedTXO();
            DEFAULT_INSTANCE = ownedTXO;
            GeneratedMessageLite.registerDefaultInstance(OwnedTXO.class, ownedTXO);
        }

        public static OwnedTXO getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<OwnedTXO> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.payments.proto.MobileCoinLedger$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class Block extends GeneratedMessageLite<Block, Builder> implements BlockOrBuilder {
        public static final int BLOCKNUMBER_FIELD_NUMBER;
        private static final Block DEFAULT_INSTANCE;
        private static volatile Parser<Block> PARSER;
        public static final int TIMESTAMP_FIELD_NUMBER;
        private long blockNumber_;
        private long timestamp_;

        private Block() {
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.BlockOrBuilder
        public long getBlockNumber() {
            return this.blockNumber_;
        }

        public void setBlockNumber(long j) {
            this.blockNumber_ = j;
        }

        public void clearBlockNumber() {
            this.blockNumber_ = 0;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.BlockOrBuilder
        public long getTimestamp() {
            return this.timestamp_;
        }

        public void setTimestamp(long j) {
            this.timestamp_ = j;
        }

        public void clearTimestamp() {
            this.timestamp_ = 0;
        }

        public static Block parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Block parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Block parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Block parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Block parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Block parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Block parseFrom(InputStream inputStream) throws IOException {
            return (Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Block parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Block parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Block) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Block parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Block) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Block parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Block parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Block block) {
            return DEFAULT_INSTANCE.createBuilder(block);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Block, Builder> implements BlockOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Block.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.BlockOrBuilder
            public long getBlockNumber() {
                return ((Block) this.instance).getBlockNumber();
            }

            public Builder setBlockNumber(long j) {
                copyOnWrite();
                ((Block) this.instance).setBlockNumber(j);
                return this;
            }

            public Builder clearBlockNumber() {
                copyOnWrite();
                ((Block) this.instance).clearBlockNumber();
                return this;
            }

            @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedger.BlockOrBuilder
            public long getTimestamp() {
                return ((Block) this.instance).getTimestamp();
            }

            public Builder setTimestamp(long j) {
                copyOnWrite();
                ((Block) this.instance).setTimestamp(j);
                return this;
            }

            public Builder clearTimestamp() {
                copyOnWrite();
                ((Block) this.instance).clearTimestamp();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Block();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0003\u0002\u0003", new Object[]{"blockNumber_", "timestamp_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Block> parser = PARSER;
                    if (parser == null) {
                        synchronized (Block.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Block block = new Block();
            DEFAULT_INSTANCE = block;
            GeneratedMessageLite.registerDefaultInstance(Block.class, block);
        }

        public static Block getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Block> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public long getBalance() {
        return this.balance_;
    }

    public void setBalance(long j) {
        this.balance_ = j;
    }

    public void clearBalance() {
        this.balance_ = 0;
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public long getTransferableBalance() {
        return this.transferableBalance_;
    }

    public void setTransferableBalance(long j) {
        this.transferableBalance_ = j;
    }

    public void clearTransferableBalance() {
        this.transferableBalance_ = 0;
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public boolean hasHighestBlock() {
        return this.highestBlock_ != null;
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public Block getHighestBlock() {
        Block block = this.highestBlock_;
        return block == null ? Block.getDefaultInstance() : block;
    }

    public void setHighestBlock(Block block) {
        block.getClass();
        this.highestBlock_ = block;
    }

    public void mergeHighestBlock(Block block) {
        block.getClass();
        Block block2 = this.highestBlock_;
        if (block2 == null || block2 == Block.getDefaultInstance()) {
            this.highestBlock_ = block;
        } else {
            this.highestBlock_ = Block.newBuilder(this.highestBlock_).mergeFrom((Block.Builder) block).buildPartial();
        }
    }

    public void clearHighestBlock() {
        this.highestBlock_ = null;
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public long getAsOfTimeStamp() {
        return this.asOfTimeStamp_;
    }

    public void setAsOfTimeStamp(long j) {
        this.asOfTimeStamp_ = j;
    }

    public void clearAsOfTimeStamp() {
        this.asOfTimeStamp_ = 0;
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public List<OwnedTXO> getSpentTxosList() {
        return this.spentTxos_;
    }

    public List<? extends OwnedTXOOrBuilder> getSpentTxosOrBuilderList() {
        return this.spentTxos_;
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public int getSpentTxosCount() {
        return this.spentTxos_.size();
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public OwnedTXO getSpentTxos(int i) {
        return this.spentTxos_.get(i);
    }

    public OwnedTXOOrBuilder getSpentTxosOrBuilder(int i) {
        return this.spentTxos_.get(i);
    }

    private void ensureSpentTxosIsMutable() {
        Internal.ProtobufList<OwnedTXO> protobufList = this.spentTxos_;
        if (!protobufList.isModifiable()) {
            this.spentTxos_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setSpentTxos(int i, OwnedTXO ownedTXO) {
        ownedTXO.getClass();
        ensureSpentTxosIsMutable();
        this.spentTxos_.set(i, ownedTXO);
    }

    public void addSpentTxos(OwnedTXO ownedTXO) {
        ownedTXO.getClass();
        ensureSpentTxosIsMutable();
        this.spentTxos_.add(ownedTXO);
    }

    public void addSpentTxos(int i, OwnedTXO ownedTXO) {
        ownedTXO.getClass();
        ensureSpentTxosIsMutable();
        this.spentTxos_.add(i, ownedTXO);
    }

    public void addAllSpentTxos(Iterable<? extends OwnedTXO> iterable) {
        ensureSpentTxosIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.spentTxos_);
    }

    public void clearSpentTxos() {
        this.spentTxos_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeSpentTxos(int i) {
        ensureSpentTxosIsMutable();
        this.spentTxos_.remove(i);
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public List<OwnedTXO> getUnspentTxosList() {
        return this.unspentTxos_;
    }

    public List<? extends OwnedTXOOrBuilder> getUnspentTxosOrBuilderList() {
        return this.unspentTxos_;
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public int getUnspentTxosCount() {
        return this.unspentTxos_.size();
    }

    @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
    public OwnedTXO getUnspentTxos(int i) {
        return this.unspentTxos_.get(i);
    }

    public OwnedTXOOrBuilder getUnspentTxosOrBuilder(int i) {
        return this.unspentTxos_.get(i);
    }

    private void ensureUnspentTxosIsMutable() {
        Internal.ProtobufList<OwnedTXO> protobufList = this.unspentTxos_;
        if (!protobufList.isModifiable()) {
            this.unspentTxos_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setUnspentTxos(int i, OwnedTXO ownedTXO) {
        ownedTXO.getClass();
        ensureUnspentTxosIsMutable();
        this.unspentTxos_.set(i, ownedTXO);
    }

    public void addUnspentTxos(OwnedTXO ownedTXO) {
        ownedTXO.getClass();
        ensureUnspentTxosIsMutable();
        this.unspentTxos_.add(ownedTXO);
    }

    public void addUnspentTxos(int i, OwnedTXO ownedTXO) {
        ownedTXO.getClass();
        ensureUnspentTxosIsMutable();
        this.unspentTxos_.add(i, ownedTXO);
    }

    public void addAllUnspentTxos(Iterable<? extends OwnedTXO> iterable) {
        ensureUnspentTxosIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.unspentTxos_);
    }

    public void clearUnspentTxos() {
        this.unspentTxos_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeUnspentTxos(int i) {
        ensureUnspentTxosIsMutable();
        this.unspentTxos_.remove(i);
    }

    public static MobileCoinLedger parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinLedger) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinLedger parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinLedger) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinLedger parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinLedger) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinLedger parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinLedger) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinLedger parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinLedger) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinLedger parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinLedger) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinLedger parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinLedger) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinLedger parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinLedger) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinLedger parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinLedger) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinLedger parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinLedger) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinLedger parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinLedger) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinLedger parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinLedger) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinLedger mobileCoinLedger) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinLedger);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinLedger, Builder> implements MobileCoinLedgerOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(MobileCoinLedger.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public long getBalance() {
            return ((MobileCoinLedger) this.instance).getBalance();
        }

        public Builder setBalance(long j) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).setBalance(j);
            return this;
        }

        public Builder clearBalance() {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).clearBalance();
            return this;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public long getTransferableBalance() {
            return ((MobileCoinLedger) this.instance).getTransferableBalance();
        }

        public Builder setTransferableBalance(long j) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).setTransferableBalance(j);
            return this;
        }

        public Builder clearTransferableBalance() {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).clearTransferableBalance();
            return this;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public boolean hasHighestBlock() {
            return ((MobileCoinLedger) this.instance).hasHighestBlock();
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public Block getHighestBlock() {
            return ((MobileCoinLedger) this.instance).getHighestBlock();
        }

        public Builder setHighestBlock(Block block) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).setHighestBlock(block);
            return this;
        }

        public Builder setHighestBlock(Block.Builder builder) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).setHighestBlock(builder.build());
            return this;
        }

        public Builder mergeHighestBlock(Block block) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).mergeHighestBlock(block);
            return this;
        }

        public Builder clearHighestBlock() {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).clearHighestBlock();
            return this;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public long getAsOfTimeStamp() {
            return ((MobileCoinLedger) this.instance).getAsOfTimeStamp();
        }

        public Builder setAsOfTimeStamp(long j) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).setAsOfTimeStamp(j);
            return this;
        }

        public Builder clearAsOfTimeStamp() {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).clearAsOfTimeStamp();
            return this;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public List<OwnedTXO> getSpentTxosList() {
            return Collections.unmodifiableList(((MobileCoinLedger) this.instance).getSpentTxosList());
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public int getSpentTxosCount() {
            return ((MobileCoinLedger) this.instance).getSpentTxosCount();
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public OwnedTXO getSpentTxos(int i) {
            return ((MobileCoinLedger) this.instance).getSpentTxos(i);
        }

        public Builder setSpentTxos(int i, OwnedTXO ownedTXO) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).setSpentTxos(i, ownedTXO);
            return this;
        }

        public Builder setSpentTxos(int i, OwnedTXO.Builder builder) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).setSpentTxos(i, builder.build());
            return this;
        }

        public Builder addSpentTxos(OwnedTXO ownedTXO) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).addSpentTxos(ownedTXO);
            return this;
        }

        public Builder addSpentTxos(int i, OwnedTXO ownedTXO) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).addSpentTxos(i, ownedTXO);
            return this;
        }

        public Builder addSpentTxos(OwnedTXO.Builder builder) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).addSpentTxos(builder.build());
            return this;
        }

        public Builder addSpentTxos(int i, OwnedTXO.Builder builder) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).addSpentTxos(i, builder.build());
            return this;
        }

        public Builder addAllSpentTxos(Iterable<? extends OwnedTXO> iterable) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).addAllSpentTxos(iterable);
            return this;
        }

        public Builder clearSpentTxos() {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).clearSpentTxos();
            return this;
        }

        public Builder removeSpentTxos(int i) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).removeSpentTxos(i);
            return this;
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public List<OwnedTXO> getUnspentTxosList() {
            return Collections.unmodifiableList(((MobileCoinLedger) this.instance).getUnspentTxosList());
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public int getUnspentTxosCount() {
            return ((MobileCoinLedger) this.instance).getUnspentTxosCount();
        }

        @Override // org.thoughtcrime.securesms.payments.proto.MobileCoinLedgerOrBuilder
        public OwnedTXO getUnspentTxos(int i) {
            return ((MobileCoinLedger) this.instance).getUnspentTxos(i);
        }

        public Builder setUnspentTxos(int i, OwnedTXO ownedTXO) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).setUnspentTxos(i, ownedTXO);
            return this;
        }

        public Builder setUnspentTxos(int i, OwnedTXO.Builder builder) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).setUnspentTxos(i, builder.build());
            return this;
        }

        public Builder addUnspentTxos(OwnedTXO ownedTXO) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).addUnspentTxos(ownedTXO);
            return this;
        }

        public Builder addUnspentTxos(int i, OwnedTXO ownedTXO) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).addUnspentTxos(i, ownedTXO);
            return this;
        }

        public Builder addUnspentTxos(OwnedTXO.Builder builder) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).addUnspentTxos(builder.build());
            return this;
        }

        public Builder addUnspentTxos(int i, OwnedTXO.Builder builder) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).addUnspentTxos(i, builder.build());
            return this;
        }

        public Builder addAllUnspentTxos(Iterable<? extends OwnedTXO> iterable) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).addAllUnspentTxos(iterable);
            return this;
        }

        public Builder clearUnspentTxos() {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).clearUnspentTxos();
            return this;
        }

        public Builder removeUnspentTxos(int i) {
            copyOnWrite();
            ((MobileCoinLedger) this.instance).removeUnspentTxos(i);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinLedger();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0006\u0000\u0000\u0001\u0006\u0006\u0000\u0002\u0000\u0001\u0003\u0002\u0003\u0003\t\u0004\u0003\u0005\u001b\u0006\u001b", new Object[]{"balance_", "transferableBalance_", "highestBlock_", "asOfTimeStamp_", "spentTxos_", OwnedTXO.class, "unspentTxos_", OwnedTXO.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinLedger> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinLedger.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinLedger mobileCoinLedger = new MobileCoinLedger();
        DEFAULT_INSTANCE = mobileCoinLedger;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinLedger.class, mobileCoinLedger);
    }

    public static MobileCoinLedger getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinLedger> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
