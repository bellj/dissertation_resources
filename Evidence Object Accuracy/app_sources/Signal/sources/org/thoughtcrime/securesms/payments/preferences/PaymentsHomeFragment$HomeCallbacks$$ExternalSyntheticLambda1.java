package org.thoughtcrime.securesms.payments.preferences;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.payments.preferences.PaymentsHomeFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PaymentsHomeFragment$HomeCallbacks$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PaymentsHomeFragment.HomeCallbacks f$0;

    public /* synthetic */ PaymentsHomeFragment$HomeCallbacks$$ExternalSyntheticLambda1(PaymentsHomeFragment.HomeCallbacks homeCallbacks) {
        this.f$0 = homeCallbacks;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onActivatePayments$1(dialogInterface, i);
    }
}
