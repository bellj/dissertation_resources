package org.thoughtcrime.securesms.payments.reconciliation;

import java.util.Comparator;
import org.thoughtcrime.securesms.payments.reconciliation.LedgerReconcile;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LedgerReconcile$DetailedTransaction$$ExternalSyntheticLambda1 implements Comparator {
    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return LedgerReconcile.DetailedTransaction.lambda$static$1((LedgerReconcile.DetailedTransaction) obj, (LedgerReconcile.DetailedTransaction) obj2);
    }
}
