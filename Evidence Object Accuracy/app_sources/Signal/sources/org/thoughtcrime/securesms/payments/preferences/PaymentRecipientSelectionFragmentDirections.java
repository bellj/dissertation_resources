package org.thoughtcrime.securesms.payments.preferences;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.PaymentPreferencesDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsParcelable;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;

/* loaded from: classes4.dex */
public class PaymentRecipientSelectionFragmentDirections {
    private PaymentRecipientSelectionFragmentDirections() {
    }

    public static ActionPaymentRecipientSelectionToCreatePayment actionPaymentRecipientSelectionToCreatePayment(PayeeParcelable payeeParcelable) {
        return new ActionPaymentRecipientSelectionToCreatePayment(payeeParcelable);
    }

    public static PaymentPreferencesDirections.ActionDirectlyToCreatePayment actionDirectlyToCreatePayment(PayeeParcelable payeeParcelable) {
        return PaymentPreferencesDirections.actionDirectlyToCreatePayment(payeeParcelable);
    }

    public static NavDirections actionDirectlyToPaymentsHome() {
        return PaymentPreferencesDirections.actionDirectlyToPaymentsHome();
    }

    public static PaymentPreferencesDirections.ActionDirectlyToPaymentDetails actionDirectlyToPaymentDetails(PaymentDetailsParcelable paymentDetailsParcelable) {
        return PaymentPreferencesDirections.actionDirectlyToPaymentDetails(paymentDetailsParcelable);
    }

    public static NavDirections actionDirectlyToPaymentsTransfer() {
        return PaymentPreferencesDirections.actionDirectlyToPaymentsTransfer();
    }

    public static NavDirections actionDirectlyToPaymentsBackup() {
        return PaymentPreferencesDirections.actionDirectlyToPaymentsBackup();
    }

    /* loaded from: classes4.dex */
    public static class ActionPaymentRecipientSelectionToCreatePayment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_paymentRecipientSelection_to_createPayment;
        }

        private ActionPaymentRecipientSelectionToCreatePayment(PayeeParcelable payeeParcelable) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (payeeParcelable != null) {
                hashMap.put("payee", payeeParcelable);
                return;
            }
            throw new IllegalArgumentException("Argument \"payee\" is marked as non-null but was passed a null value.");
        }

        public ActionPaymentRecipientSelectionToCreatePayment setPayee(PayeeParcelable payeeParcelable) {
            if (payeeParcelable != null) {
                this.arguments.put("payee", payeeParcelable);
                return this;
            }
            throw new IllegalArgumentException("Argument \"payee\" is marked as non-null but was passed a null value.");
        }

        public ActionPaymentRecipientSelectionToCreatePayment setNote(String str) {
            this.arguments.put("note", str);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("payee")) {
                PayeeParcelable payeeParcelable = (PayeeParcelable) this.arguments.get("payee");
                if (Parcelable.class.isAssignableFrom(PayeeParcelable.class) || payeeParcelable == null) {
                    bundle.putParcelable("payee", (Parcelable) Parcelable.class.cast(payeeParcelable));
                } else if (Serializable.class.isAssignableFrom(PayeeParcelable.class)) {
                    bundle.putSerializable("payee", (Serializable) Serializable.class.cast(payeeParcelable));
                } else {
                    throw new UnsupportedOperationException(PayeeParcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("note")) {
                bundle.putString("note", (String) this.arguments.get("note"));
            } else {
                bundle.putString("note", null);
            }
            return bundle;
        }

        public PayeeParcelable getPayee() {
            return (PayeeParcelable) this.arguments.get("payee");
        }

        public String getNote() {
            return (String) this.arguments.get("note");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionPaymentRecipientSelectionToCreatePayment actionPaymentRecipientSelectionToCreatePayment = (ActionPaymentRecipientSelectionToCreatePayment) obj;
            if (this.arguments.containsKey("payee") != actionPaymentRecipientSelectionToCreatePayment.arguments.containsKey("payee")) {
                return false;
            }
            if (getPayee() == null ? actionPaymentRecipientSelectionToCreatePayment.getPayee() != null : !getPayee().equals(actionPaymentRecipientSelectionToCreatePayment.getPayee())) {
                return false;
            }
            if (this.arguments.containsKey("note") != actionPaymentRecipientSelectionToCreatePayment.arguments.containsKey("note")) {
                return false;
            }
            if (getNote() == null ? actionPaymentRecipientSelectionToCreatePayment.getNote() == null : getNote().equals(actionPaymentRecipientSelectionToCreatePayment.getNote())) {
                return getActionId() == actionPaymentRecipientSelectionToCreatePayment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((getPayee() != null ? getPayee().hashCode() : 0) + 31) * 31;
            if (getNote() != null) {
                i = getNote().hashCode();
            }
            return ((hashCode + i) * 31) + getActionId();
        }

        public String toString() {
            return "ActionPaymentRecipientSelectionToCreatePayment(actionId=" + getActionId() + "){payee=" + getPayee() + ", note=" + getNote() + "}";
        }
    }
}
