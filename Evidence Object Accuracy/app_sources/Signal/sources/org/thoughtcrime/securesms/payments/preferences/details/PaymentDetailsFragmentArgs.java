package org.thoughtcrime.securesms.payments.preferences.details;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class PaymentDetailsFragmentArgs {
    private final HashMap arguments;

    private PaymentDetailsFragmentArgs() {
        this.arguments = new HashMap();
    }

    private PaymentDetailsFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static PaymentDetailsFragmentArgs fromBundle(Bundle bundle) {
        PaymentDetailsFragmentArgs paymentDetailsFragmentArgs = new PaymentDetailsFragmentArgs();
        bundle.setClassLoader(PaymentDetailsFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("paymentDetails")) {
            throw new IllegalArgumentException("Required argument \"paymentDetails\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(PaymentDetailsParcelable.class) || Serializable.class.isAssignableFrom(PaymentDetailsParcelable.class)) {
            PaymentDetailsParcelable paymentDetailsParcelable = (PaymentDetailsParcelable) bundle.get("paymentDetails");
            if (paymentDetailsParcelable != null) {
                paymentDetailsFragmentArgs.arguments.put("paymentDetails", paymentDetailsParcelable);
                return paymentDetailsFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"paymentDetails\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(PaymentDetailsParcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public PaymentDetailsParcelable getPaymentDetails() {
        return (PaymentDetailsParcelable) this.arguments.get("paymentDetails");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("paymentDetails")) {
            PaymentDetailsParcelable paymentDetailsParcelable = (PaymentDetailsParcelable) this.arguments.get("paymentDetails");
            if (Parcelable.class.isAssignableFrom(PaymentDetailsParcelable.class) || paymentDetailsParcelable == null) {
                bundle.putParcelable("paymentDetails", (Parcelable) Parcelable.class.cast(paymentDetailsParcelable));
            } else if (Serializable.class.isAssignableFrom(PaymentDetailsParcelable.class)) {
                bundle.putSerializable("paymentDetails", (Serializable) Serializable.class.cast(paymentDetailsParcelable));
            } else {
                throw new UnsupportedOperationException(PaymentDetailsParcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PaymentDetailsFragmentArgs paymentDetailsFragmentArgs = (PaymentDetailsFragmentArgs) obj;
        if (this.arguments.containsKey("paymentDetails") != paymentDetailsFragmentArgs.arguments.containsKey("paymentDetails")) {
            return false;
        }
        return getPaymentDetails() == null ? paymentDetailsFragmentArgs.getPaymentDetails() == null : getPaymentDetails().equals(paymentDetailsFragmentArgs.getPaymentDetails());
    }

    public int hashCode() {
        return 31 + (getPaymentDetails() != null ? getPaymentDetails().hashCode() : 0);
    }

    public String toString() {
        return "PaymentDetailsFragmentArgs{paymentDetails=" + getPaymentDetails() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(PaymentDetailsFragmentArgs paymentDetailsFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(paymentDetailsFragmentArgs.arguments);
        }

        public Builder(PaymentDetailsParcelable paymentDetailsParcelable) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (paymentDetailsParcelable != null) {
                hashMap.put("paymentDetails", paymentDetailsParcelable);
                return;
            }
            throw new IllegalArgumentException("Argument \"paymentDetails\" is marked as non-null but was passed a null value.");
        }

        public PaymentDetailsFragmentArgs build() {
            return new PaymentDetailsFragmentArgs(this.arguments);
        }

        public Builder setPaymentDetails(PaymentDetailsParcelable paymentDetailsParcelable) {
            if (paymentDetailsParcelable != null) {
                this.arguments.put("paymentDetails", paymentDetailsParcelable);
                return this;
            }
            throw new IllegalArgumentException("Argument \"paymentDetails\" is marked as non-null but was passed a null value.");
        }

        public PaymentDetailsParcelable getPaymentDetails() {
            return (PaymentDetailsParcelable) this.arguments.get("paymentDetails");
        }
    }
}
