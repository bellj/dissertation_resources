package org.thoughtcrime.securesms.payments.backup.entry;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.payments.Mnemonic;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.payments.PaymentsConstants;

/* loaded from: classes4.dex */
public class PaymentsRecoveryEntryViewModel extends ViewModel {
    private SingleLiveEvent<Events> events = new SingleLiveEvent<>();
    private Store<PaymentsRecoveryEntryViewState> state = new Store<>(new PaymentsRecoveryEntryViewState());
    private String[] words = new String[PaymentsConstants.MNEMONIC_LENGTH];

    /* loaded from: classes4.dex */
    public enum Events {
        GO_TO_CONFIRM
    }

    public LiveData<PaymentsRecoveryEntryViewState> getState() {
        return this.state.getStateLiveData();
    }

    public LiveData<Events> getEvents() {
        return this.events;
    }

    public String[] getWords() {
        return this.words;
    }

    public /* synthetic */ PaymentsRecoveryEntryViewState lambda$onWordChanged$0(String str, PaymentsRecoveryEntryViewState paymentsRecoveryEntryViewState) {
        return new PaymentsRecoveryEntryViewState(paymentsRecoveryEntryViewState.getWordIndex(), isValid(str), str);
    }

    public void onWordChanged(String str) {
        this.state.update(new Function(str) { // from class: org.thoughtcrime.securesms.payments.backup.entry.PaymentsRecoveryEntryViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PaymentsRecoveryEntryViewModel.this.lambda$onWordChanged$0(this.f$1, (PaymentsRecoveryEntryViewState) obj);
            }
        });
    }

    public void onNextClicked() {
        this.state.update(new Function() { // from class: org.thoughtcrime.securesms.payments.backup.entry.PaymentsRecoveryEntryViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PaymentsRecoveryEntryViewModel.this.lambda$onNextClicked$1((PaymentsRecoveryEntryViewState) obj);
            }
        });
    }

    public /* synthetic */ PaymentsRecoveryEntryViewState lambda$onNextClicked$1(PaymentsRecoveryEntryViewState paymentsRecoveryEntryViewState) {
        this.words[paymentsRecoveryEntryViewState.getWordIndex()] = paymentsRecoveryEntryViewState.getCurrentEntry();
        if (paymentsRecoveryEntryViewState.getWordIndex() == PaymentsConstants.MNEMONIC_LENGTH - 1) {
            this.events.postValue(Events.GO_TO_CONFIRM);
            return new PaymentsRecoveryEntryViewState(0, isValid(this.words[0]), this.words[0]);
        }
        int wordIndex = paymentsRecoveryEntryViewState.getWordIndex() + 1;
        return new PaymentsRecoveryEntryViewState(wordIndex, isValid(this.words[wordIndex]), this.words[wordIndex]);
    }

    private boolean isValid(String str) {
        if (str == null) {
            return false;
        }
        return Mnemonic.BIP39_WORDS_ENGLISH.contains(str.toLowerCase());
    }
}
