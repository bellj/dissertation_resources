package org.thoughtcrime.securesms.payments.backup;

import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.Mnemonic;

/* loaded from: classes4.dex */
public final class PaymentsRecoveryRepository {
    public Mnemonic getMnemonic() {
        return SignalStore.paymentsValues().getPaymentsMnemonic();
    }
}
