package org.thoughtcrime.securesms.payments.backup.phrase;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class MnemonicPart {
    private final int index;
    private final String word;

    public MnemonicPart(int i, String str) {
        this.word = str;
        this.index = i;
    }

    public String getWord() {
        return this.word;
    }

    public int getIndex() {
        return this.index;
    }
}
