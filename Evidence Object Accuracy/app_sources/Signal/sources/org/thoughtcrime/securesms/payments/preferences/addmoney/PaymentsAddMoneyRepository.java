package org.thoughtcrime.securesms.payments.preferences.addmoney;

import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddress;
import org.thoughtcrime.securesms.util.AsynchronousCallback;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class PaymentsAddMoneyRepository {

    /* loaded from: classes4.dex */
    public enum Error {
        PAYMENTS_NOT_ENABLED
    }

    public void getWalletAddress(AsynchronousCallback.MainThread<AddressAndUri, Error> mainThread) {
        if (!SignalStore.paymentsValues().mobileCoinPaymentsEnabled()) {
            mainThread.onError(Error.PAYMENTS_NOT_ENABLED);
        }
        MobileCoinPublicAddress mobileCoinPublicAddress = ApplicationDependencies.getPayments().getWallet().getMobileCoinPublicAddress();
        mainThread.onComplete(new AddressAndUri(mobileCoinPublicAddress.getPaymentAddressBase58(), mobileCoinPublicAddress.getPaymentAddressUri()));
    }
}
