package org.thoughtcrime.securesms.payments.reconciliation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/* loaded from: classes4.dex */
public final class ZipList {
    ZipList() {
    }

    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.util.Comparator<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static <T> List<T> zipList(List<? extends T> list, List<? extends T> list2, Comparator<T> comparator) {
        ArrayList arrayList = new ArrayList(list.size() + list2.size());
        if (list.isEmpty()) {
            return new ArrayList(list2);
        }
        if (list2.isEmpty()) {
            return new ArrayList(list);
        }
        int i = 0;
        int i2 = 0;
        do {
            Object obj = list.get(i);
            Object obj2 = list2.get(i2);
            if (comparator.compare(obj, obj2) > 0) {
                arrayList.add(obj2);
                i2++;
            } else {
                arrayList.add(obj);
                i++;
            }
            if (i >= list.size()) {
                break;
            }
        } while (i2 < list2.size());
        while (i < list.size()) {
            arrayList.add(list.get(i));
            i++;
        }
        while (i2 < list2.size()) {
            arrayList.add(list2.get(i2));
            i2++;
        }
        return arrayList;
    }
}
