package org.thoughtcrime.securesms.payments.preferences;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.PaymentPreferencesDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsParcelable;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;

/* loaded from: classes4.dex */
public class PaymentsHomeFragmentDirections {
    private PaymentsHomeFragmentDirections() {
    }

    public static ActionPaymentsHomeToPaymentsAllActivity actionPaymentsHomeToPaymentsAllActivity(PaymentType paymentType) {
        return new ActionPaymentsHomeToPaymentsAllActivity(paymentType);
    }

    public static NavDirections actionPaymentsHomeToPaymentsAddMoney() {
        return new ActionOnlyNavDirections(R.id.action_paymentsHome_to_paymentsAddMoney);
    }

    public static NavDirections actionPaymentsHomeToPaymentRecipientSelectionFragment() {
        return new ActionOnlyNavDirections(R.id.action_paymentsHome_to_paymentRecipientSelectionFragment);
    }

    public static NavDirections actionPaymentsHomeToPaymentsTransfer() {
        return new ActionOnlyNavDirections(R.id.action_paymentsHome_to_paymentsTransfer);
    }

    public static NavDirections actionPaymentsHomeToSetCurrency() {
        return new ActionOnlyNavDirections(R.id.action_paymentsHome_to_setCurrency);
    }

    public static NavDirections actionPaymentsHomeToDeactivateWallet() {
        return new ActionOnlyNavDirections(R.id.action_paymentsHome_to_deactivateWallet);
    }

    public static ActionPaymentsHomeToPaymentsBackup actionPaymentsHomeToPaymentsBackup() {
        return new ActionPaymentsHomeToPaymentsBackup();
    }

    public static PaymentPreferencesDirections.ActionDirectlyToCreatePayment actionDirectlyToCreatePayment(PayeeParcelable payeeParcelable) {
        return PaymentPreferencesDirections.actionDirectlyToCreatePayment(payeeParcelable);
    }

    public static NavDirections actionDirectlyToPaymentsHome() {
        return PaymentPreferencesDirections.actionDirectlyToPaymentsHome();
    }

    public static PaymentPreferencesDirections.ActionDirectlyToPaymentDetails actionDirectlyToPaymentDetails(PaymentDetailsParcelable paymentDetailsParcelable) {
        return PaymentPreferencesDirections.actionDirectlyToPaymentDetails(paymentDetailsParcelable);
    }

    public static NavDirections actionDirectlyToPaymentsTransfer() {
        return PaymentPreferencesDirections.actionDirectlyToPaymentsTransfer();
    }

    public static NavDirections actionDirectlyToPaymentsBackup() {
        return PaymentPreferencesDirections.actionDirectlyToPaymentsBackup();
    }

    /* loaded from: classes4.dex */
    public static class ActionPaymentsHomeToPaymentsAllActivity implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_paymentsHome_to_paymentsAllActivity;
        }

        private ActionPaymentsHomeToPaymentsAllActivity(PaymentType paymentType) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (paymentType != null) {
                hashMap.put("paymentType", paymentType);
                return;
            }
            throw new IllegalArgumentException("Argument \"paymentType\" is marked as non-null but was passed a null value.");
        }

        public ActionPaymentsHomeToPaymentsAllActivity setPaymentType(PaymentType paymentType) {
            if (paymentType != null) {
                this.arguments.put("paymentType", paymentType);
                return this;
            }
            throw new IllegalArgumentException("Argument \"paymentType\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("paymentType")) {
                PaymentType paymentType = (PaymentType) this.arguments.get("paymentType");
                if (Parcelable.class.isAssignableFrom(PaymentType.class) || paymentType == null) {
                    bundle.putParcelable("paymentType", (Parcelable) Parcelable.class.cast(paymentType));
                } else if (Serializable.class.isAssignableFrom(PaymentType.class)) {
                    bundle.putSerializable("paymentType", (Serializable) Serializable.class.cast(paymentType));
                } else {
                    throw new UnsupportedOperationException(PaymentType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public PaymentType getPaymentType() {
            return (PaymentType) this.arguments.get("paymentType");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionPaymentsHomeToPaymentsAllActivity actionPaymentsHomeToPaymentsAllActivity = (ActionPaymentsHomeToPaymentsAllActivity) obj;
            if (this.arguments.containsKey("paymentType") != actionPaymentsHomeToPaymentsAllActivity.arguments.containsKey("paymentType")) {
                return false;
            }
            if (getPaymentType() == null ? actionPaymentsHomeToPaymentsAllActivity.getPaymentType() == null : getPaymentType().equals(actionPaymentsHomeToPaymentsAllActivity.getPaymentType())) {
                return getActionId() == actionPaymentsHomeToPaymentsAllActivity.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getPaymentType() != null ? getPaymentType().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionPaymentsHomeToPaymentsAllActivity(actionId=" + getActionId() + "){paymentType=" + getPaymentType() + "}";
        }
    }

    /* loaded from: classes4.dex */
    public static class ActionPaymentsHomeToPaymentsBackup implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_paymentsHome_to_paymentsBackup;
        }

        private ActionPaymentsHomeToPaymentsBackup() {
            this.arguments = new HashMap();
        }

        public ActionPaymentsHomeToPaymentsBackup setIsRestore(boolean z) {
            this.arguments.put("is_restore", Boolean.valueOf(z));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("is_restore")) {
                bundle.putBoolean("is_restore", ((Boolean) this.arguments.get("is_restore")).booleanValue());
            } else {
                bundle.putBoolean("is_restore", false);
            }
            return bundle;
        }

        public boolean getIsRestore() {
            return ((Boolean) this.arguments.get("is_restore")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionPaymentsHomeToPaymentsBackup actionPaymentsHomeToPaymentsBackup = (ActionPaymentsHomeToPaymentsBackup) obj;
            return this.arguments.containsKey("is_restore") == actionPaymentsHomeToPaymentsBackup.arguments.containsKey("is_restore") && getIsRestore() == actionPaymentsHomeToPaymentsBackup.getIsRestore() && getActionId() == actionPaymentsHomeToPaymentsBackup.getActionId();
        }

        public int hashCode() {
            return (((getIsRestore() ? 1 : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionPaymentsHomeToPaymentsBackup(actionId=" + getActionId() + "){isRestore=" + getIsRestore() + "}";
        }
    }
}
