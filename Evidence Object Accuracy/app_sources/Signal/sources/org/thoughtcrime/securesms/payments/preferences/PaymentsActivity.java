package org.thoughtcrime.securesms.payments.preferences;

import android.os.Bundle;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.PaymentLedgerUpdateJob;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public class PaymentsActivity extends PassphraseRequiredActivity {
    public static final String EXTRA_PAYMENTS_STARTING_ACTION;
    public static final String EXTRA_STARTING_ARGUMENTS;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.payments_activity);
        NavController findNavController = Navigation.findNavController(this, R.id.nav_host_fragment);
        findNavController.setGraph(R.navigation.payments_preferences);
        int intExtra = getIntent().getIntExtra(EXTRA_PAYMENTS_STARTING_ACTION, R.id.paymentsHome);
        if (intExtra != R.id.paymentsHome) {
            SafeNavigation.safeNavigate(findNavController, intExtra, getIntent().getBundleExtra(EXTRA_STARTING_ARGUMENTS));
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
        ApplicationDependencies.getJobManager().add(PaymentLedgerUpdateJob.updateLedger());
    }
}
