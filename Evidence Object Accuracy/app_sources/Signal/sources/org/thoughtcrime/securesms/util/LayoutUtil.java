package org.thoughtcrime.securesms.util;

import android.text.Layout;

/* loaded from: classes4.dex */
public class LayoutUtil {
    private static final float DEFAULT_LINE_SPACING_EXTRA;
    private static final float DEFAULT_LINE_SPACING_MULTIPLIER;

    public static int getLineHeight(Layout layout, int i) {
        return layout.getLineTop(i + 1) - layout.getLineTop(i);
    }

    public static int getLineTopWithoutPadding(Layout layout, int i) {
        int lineTop = layout.getLineTop(i);
        return i == 0 ? lineTop - layout.getTopPadding() : lineTop;
    }

    public static int getLineBottomWithoutPadding(Layout layout, int i) {
        int lineBottomWithoutSpacing = getLineBottomWithoutSpacing(layout, i);
        return i == layout.getLineCount() + -1 ? lineBottomWithoutSpacing - layout.getBottomPadding() : lineBottomWithoutSpacing;
    }

    public static int getLineBottomWithoutSpacing(Layout layout, int i) {
        int lineBottom = layout.getLineBottom(i);
        boolean z = true;
        boolean z2 = i == layout.getLineCount() - 1;
        float spacingAdd = layout.getSpacingAdd();
        float spacingMultiplier = layout.getSpacingMultiplier();
        if (spacingAdd == DEFAULT_LINE_SPACING_EXTRA && spacingMultiplier == DEFAULT_LINE_SPACING_MULTIPLIER) {
            z = false;
        }
        if (!z || z2) {
            return lineBottom;
        }
        if (Float.compare(spacingMultiplier, DEFAULT_LINE_SPACING_MULTIPLIER) != 0) {
            float lineHeight = (float) getLineHeight(layout, i);
            spacingAdd = lineHeight - ((lineHeight - spacingAdd) / spacingMultiplier);
        }
        return (int) (((float) lineBottom) - spacingAdd);
    }
}
