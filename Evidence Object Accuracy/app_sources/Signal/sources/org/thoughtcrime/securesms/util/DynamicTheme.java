package org.thoughtcrime.securesms.util;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import androidx.appcompat.app.AppCompatDelegate;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes.dex */
public class DynamicTheme {
    private static final String TAG = Log.tag(DynamicTheme.class);
    private static int globalNightModeConfiguration;
    private int onCreateNightModeConfiguration;

    protected int getTheme() {
        return R.style.Signal_DayNight;
    }

    public void onCreate(Activity activity) {
        int i = globalNightModeConfiguration;
        int nightModeConfiguration = ConfigurationUtil.getNightModeConfiguration(activity);
        this.onCreateNightModeConfiguration = nightModeConfiguration;
        globalNightModeConfiguration = nightModeConfiguration;
        activity.setTheme(getTheme());
        if (i != globalNightModeConfiguration) {
            String str = TAG;
            Log.d(str, "Previous night mode has changed previous: " + i + " now: " + globalNightModeConfiguration);
            CachedInflater.from(activity).clear();
        }
    }

    public void onResume(Activity activity) {
        if (this.onCreateNightModeConfiguration != ConfigurationUtil.getNightModeConfiguration(activity)) {
            String str = TAG;
            Log.d(str, "Create configuration different from current previous: " + this.onCreateNightModeConfiguration + " now: " + ConfigurationUtil.getNightModeConfiguration(activity));
            CachedInflater.from(activity).clear();
        }
    }

    public static boolean systemThemeAvailable() {
        return Build.VERSION.SDK_INT >= 29;
    }

    public static void setDefaultDayNightMode(Context context) {
        if (SignalStore.settings().getTheme() == SettingsValues.Theme.SYSTEM) {
            String str = TAG;
            Log.d(str, "Setting to follow system expecting: " + ConfigurationUtil.getNightModeConfiguration(context.getApplicationContext()));
            AppCompatDelegate.setDefaultNightMode(-1);
        } else if (isDarkTheme(context)) {
            Log.d(TAG, "Setting to always night");
            AppCompatDelegate.setDefaultNightMode(2);
        } else {
            Log.d(TAG, "Setting to always day");
            AppCompatDelegate.setDefaultNightMode(1);
        }
        CachedInflater.from(context).clear();
    }

    public static boolean isDarkTheme(Context context) {
        SettingsValues.Theme theme = SignalStore.settings().getTheme();
        if (theme != SettingsValues.Theme.SYSTEM || !systemThemeAvailable()) {
            return theme == SettingsValues.Theme.DARK;
        }
        return isSystemInDarkTheme(context);
    }

    private static boolean isSystemInDarkTheme(Context context) {
        return (context.getResources().getConfiguration().uiMode & 48) == 32;
    }
}
