package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.text.style.ClickableSpan;
import android.view.View;
import org.signal.libsignal.protocol.IdentityKey;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.verify.VerifyIdentityActivity;

/* loaded from: classes4.dex */
public class VerifySpan extends ClickableSpan {
    private final Context context;
    private final IdentityKey identityKey;
    private final RecipientId recipientId;

    public VerifySpan(Context context, IdentityKeyMismatch identityKeyMismatch) {
        this.context = context;
        this.recipientId = identityKeyMismatch.getRecipientId(context);
        this.identityKey = identityKeyMismatch.getIdentityKey();
    }

    public VerifySpan(Context context, RecipientId recipientId, IdentityKey identityKey) {
        this.context = context;
        this.recipientId = recipientId;
        this.identityKey = identityKey;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        Context context = this.context;
        context.startActivity(VerifyIdentityActivity.newIntent(context, this.recipientId, this.identityKey, false));
    }
}
