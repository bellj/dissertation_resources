package org.thoughtcrime.securesms.util;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.UUID;

/* loaded from: classes4.dex */
public final class BucketingUtil {
    private BucketingUtil() {
    }

    public static long bucket(String str, UUID uuid, long j) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes());
            instance.update(".".getBytes());
            ByteBuffer wrap = ByteBuffer.wrap(new byte[16]);
            wrap.order(ByteOrder.BIG_ENDIAN);
            wrap.putLong(uuid.getMostSignificantBits());
            wrap.putLong(uuid.getLeastSignificantBits());
            instance.update(wrap.array());
            return new BigInteger(Arrays.copyOfRange(instance.digest(), 0, 8)).mod(BigInteger.valueOf(j)).longValue();
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
