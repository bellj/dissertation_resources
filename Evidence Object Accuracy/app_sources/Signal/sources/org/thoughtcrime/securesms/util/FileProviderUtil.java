package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import androidx.core.content.FileProvider;
import java.io.File;

/* loaded from: classes4.dex */
public class FileProviderUtil {
    private static final String AUTHORITY;

    public static Uri getUriFor(Context context, File file) {
        if (Build.VERSION.SDK_INT >= 24) {
            return FileProvider.getUriForFile(context, AUTHORITY, file);
        }
        return Uri.fromFile(file);
    }

    public static boolean isAuthority(Uri uri) {
        return AUTHORITY.equals(uri.getAuthority());
    }

    public static boolean delete(Context context, Uri uri) {
        if (AUTHORITY.equals(uri.getAuthority())) {
            return context.getContentResolver().delete(uri, null, null) > 0;
        }
        return new File(uri.getPath()).delete();
    }
}
