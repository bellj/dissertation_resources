package org.thoughtcrime.securesms.util;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.functions.Predicate;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.conscrypt.Conscrypt;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;
import org.whispersystems.signalservice.internal.configuration.SignalProxy;

/* loaded from: classes4.dex */
public final class SignalProxyUtil {
    private static final Pattern HOST_PATTERN = Pattern.compile("^([^:]+).*$");
    private static final String PROXY_LINK_HOST;
    private static final Pattern PROXY_LINK_PATTERN = Pattern.compile("^(https|sgnl)://signal.tube/#([^:]+).*$");
    private static final String TAG = Log.tag(SignalProxyUtil.class);

    private SignalProxyUtil() {
    }

    public static void startListeningToWebsocket() {
        if (SignalStore.proxy().isProxyEnabled() && ApplicationDependencies.getSignalWebSocket().getWebSocketState().firstOrError().blockingGet().isFailure()) {
            Log.w(TAG, "Proxy is in a failed state. Restarting.");
            ApplicationDependencies.closeConnections();
        }
        ApplicationDependencies.getIncomingMessageObserver();
    }

    public static void enableProxy(SignalProxy signalProxy) {
        SignalStore.proxy().enableProxy(signalProxy);
        Conscrypt.setUseEngineSocketByDefault(true);
        ApplicationDependencies.resetNetworkConnectionsAfterProxyChange();
        startListeningToWebsocket();
    }

    public static void disableProxy() {
        SignalStore.proxy().disableProxy();
        Conscrypt.setUseEngineSocketByDefault(false);
        ApplicationDependencies.resetNetworkConnectionsAfterProxyChange();
        startListeningToWebsocket();
    }

    public static boolean testWebsocketConnection(long j) {
        startListeningToWebsocket();
        if (SignalStore.account().getE164() != null) {
            return ((Boolean) ApplicationDependencies.getSignalWebSocket().getWebSocketState().subscribeOn(Schedulers.trampoline()).observeOn(Schedulers.trampoline()).timeout(j, TimeUnit.MILLISECONDS).skipWhile(new Predicate() { // from class: org.thoughtcrime.securesms.util.SignalProxyUtil$$ExternalSyntheticLambda0
                @Override // io.reactivex.rxjava3.functions.Predicate
                public final boolean test(Object obj) {
                    return SignalProxyUtil.lambda$testWebsocketConnection$0((WebSocketConnectionState) obj);
                }
            }).firstOrError().flatMap(new Function() { // from class: org.thoughtcrime.securesms.util.SignalProxyUtil$$ExternalSyntheticLambda1
                @Override // io.reactivex.rxjava3.functions.Function
                public final Object apply(Object obj) {
                    return SignalProxyUtil.lambda$testWebsocketConnection$1((WebSocketConnectionState) obj);
                }
            }).onErrorReturn(new Function() { // from class: org.thoughtcrime.securesms.util.SignalProxyUtil$$ExternalSyntheticLambda2
                @Override // io.reactivex.rxjava3.functions.Function
                public final Object apply(Object obj) {
                    Throwable th = (Throwable) obj;
                    return Boolean.FALSE;
                }
            }).blockingGet()).booleanValue();
        }
        Log.i(TAG, "User is unregistered! Doing simple check.");
        return testWebsocketConnectionUnregistered(j);
    }

    public static /* synthetic */ boolean lambda$testWebsocketConnection$0(WebSocketConnectionState webSocketConnectionState) throws Throwable {
        return webSocketConnectionState != WebSocketConnectionState.CONNECTED && !webSocketConnectionState.isFailure();
    }

    public static /* synthetic */ SingleSource lambda$testWebsocketConnection$1(WebSocketConnectionState webSocketConnectionState) throws Throwable {
        return Single.just(Boolean.valueOf(webSocketConnectionState == WebSocketConnectionState.CONNECTED));
    }

    public static String parseHostFromProxyDeepLink(String str) {
        if (str == null) {
            return null;
        }
        Matcher matcher = PROXY_LINK_PATTERN.matcher(str);
        if (matcher.matches()) {
            return matcher.group(2);
        }
        return null;
    }

    public static String convertUserEnteredAddressToHost(String str) {
        String parseHostFromProxyDeepLink = parseHostFromProxyDeepLink(str);
        if (parseHostFromProxyDeepLink != null) {
            return parseHostFromProxyDeepLink;
        }
        Matcher matcher = HOST_PATTERN.matcher(str);
        if (!matcher.matches()) {
            return str;
        }
        String group = matcher.group(1);
        return group != null ? group : "";
    }

    public static String generateProxyUrl(String str) {
        String parseHostFromProxyDeepLink = parseHostFromProxyDeepLink(str);
        if (parseHostFromProxyDeepLink != null) {
            str = parseHostFromProxyDeepLink;
        }
        Matcher matcher = HOST_PATTERN.matcher(str);
        if (matcher.matches()) {
            str = matcher.group(1);
        }
        return "https://signal.tube/#" + str;
    }

    private static boolean testWebsocketConnectionUnregistered(long j) {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        SignalExecutors.UNBOUNDED.execute(new Runnable(atomicBoolean, countDownLatch) { // from class: org.thoughtcrime.securesms.util.SignalProxyUtil$$ExternalSyntheticLambda3
            public final /* synthetic */ AtomicBoolean f$1;
            public final /* synthetic */ CountDownLatch f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalProxyUtil.lambda$testWebsocketConnectionUnregistered$3(SignalServiceAccountManager.this, this.f$1, this.f$2);
            }
        });
        try {
            countDownLatch.await(j, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Log.w(TAG, "Interrupted!", e);
        }
        return atomicBoolean.get();
    }

    public static /* synthetic */ void lambda$testWebsocketConnectionUnregistered$3(SignalServiceAccountManager signalServiceAccountManager, AtomicBoolean atomicBoolean, CountDownLatch countDownLatch) {
        try {
            signalServiceAccountManager.checkNetworkConnection();
            atomicBoolean.set(true);
            countDownLatch.countDown();
        } catch (IOException unused) {
            countDownLatch.countDown();
        }
    }
}
