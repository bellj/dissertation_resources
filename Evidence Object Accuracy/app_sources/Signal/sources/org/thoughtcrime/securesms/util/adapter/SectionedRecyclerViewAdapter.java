package org.thoughtcrime.securesms.util.adapter;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiFunction;
import java.util.List;
import org.thoughtcrime.securesms.util.adapter.SectionedRecyclerViewAdapter;
import org.thoughtcrime.securesms.util.adapter.SectionedRecyclerViewAdapter.Section;

/* loaded from: classes4.dex */
public abstract class SectionedRecyclerViewAdapter<IdType, SectionImpl extends Section<IdType>> extends RecyclerView.Adapter {
    private static final int TYPE_CONTENT;
    private static final int TYPE_EMPTY;
    private static final int TYPE_HEADER;
    private final StableIdGenerator<IdType> stableIdGenerator = new StableIdGenerator<>();

    protected abstract void bindViewHolder(RecyclerView.ViewHolder viewHolder, SectionImpl sectionimpl, int i);

    protected abstract RecyclerView.ViewHolder createContentViewHolder(ViewGroup viewGroup);

    protected abstract RecyclerView.ViewHolder createEmptyViewHolder(ViewGroup viewGroup);

    protected abstract RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup viewGroup);

    protected abstract List<SectionImpl> getSections();

    public SectionedRecyclerViewAdapter() {
        setHasStableIds(true);
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 1) {
            return createHeaderViewHolder(viewGroup);
        }
        if (i == 2) {
            return createContentViewHolder(viewGroup);
        }
        if (i == 3) {
            RecyclerView.ViewHolder createEmptyViewHolder = createEmptyViewHolder(viewGroup);
            if (createEmptyViewHolder != null) {
                return createEmptyViewHolder;
            }
            throw new IllegalStateException("Expected an empty view holder, but got none!");
        }
        throw new AssertionError("Unexpected viewType! " + i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        for (SectionImpl sectionimpl : getSections()) {
            if (sectionimpl.handles(i)) {
                return sectionimpl.getItemId(this.stableIdGenerator, i);
            }
        }
        throw new NoSectionException();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        for (SectionImpl sectionimpl : getSections()) {
            if (sectionimpl.handles(i)) {
                return sectionimpl.getViewType(i);
            }
        }
        throw new NoSectionException();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        for (SectionImpl sectionimpl : getSections()) {
            if (sectionimpl.handles(i)) {
                bindViewHolder(viewHolder, sectionimpl, sectionimpl.getLocalPosition(i));
                return;
            }
        }
        throw new NoSectionException();
    }

    public static /* synthetic */ Integer lambda$getItemCount$0(Integer num, Section section) {
        return Integer.valueOf(num.intValue() + section.size());
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return ((Integer) Stream.of(getSections()).reduce(0, new BiFunction() { // from class: org.thoughtcrime.securesms.util.adapter.SectionedRecyclerViewAdapter$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return SectionedRecyclerViewAdapter.lambda$getItemCount$0((Integer) obj, (SectionedRecyclerViewAdapter.Section) obj2);
            }
        })).intValue();
    }

    /* loaded from: classes4.dex */
    public static abstract class Section<E> {
        private final int offset;

        public abstract int getContentSize();

        public abstract long getItemId(StableIdGenerator<E> stableIdGenerator, int i);

        public abstract boolean hasEmptyState();

        public Section(int i) {
            this.offset = i;
        }

        public final int getLocalPosition(int i) {
            return i - this.offset;
        }

        final int getViewType(int i) {
            if (getLocalPosition(i) == 0) {
                return 1;
            }
            return getContentSize() == 0 ? 3 : 2;
        }

        final boolean handles(int i) {
            int localPosition = getLocalPosition(i);
            return localPosition >= 0 && localPosition < size();
        }

        public boolean isContent(int i) {
            return handles(i) && getViewType(i) == 2;
        }

        public final int size() {
            if (getContentSize() == 0 && hasEmptyState()) {
                return 2;
            }
            if (getContentSize() == 0) {
                return 0;
            }
            return getContentSize() + 1;
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class NoSectionException extends IllegalStateException {
        private NoSectionException() {
        }
    }
}
