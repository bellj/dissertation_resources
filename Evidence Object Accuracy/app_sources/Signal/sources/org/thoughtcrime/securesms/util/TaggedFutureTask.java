package org.thoughtcrime.securesms.util;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* loaded from: classes4.dex */
public class TaggedFutureTask<V> extends FutureTask<V> {
    private final Object tag;

    public TaggedFutureTask(Runnable runnable, V v, Object obj) {
        super(runnable, v);
        this.tag = obj;
    }

    public TaggedFutureTask(Callable<V> callable, Object obj) {
        super(callable);
        this.tag = obj;
    }

    public Object getTag() {
        return this.tag;
    }
}
