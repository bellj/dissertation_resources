package org.thoughtcrime.securesms.util.paging;

/* loaded from: classes4.dex */
public class Invalidator {
    private Runnable callback;

    public synchronized void invalidate() {
        Runnable runnable = this.callback;
        if (runnable != null) {
            runnable.run();
        }
    }

    public synchronized void observe(Runnable runnable) {
        this.callback = runnable;
    }
}
