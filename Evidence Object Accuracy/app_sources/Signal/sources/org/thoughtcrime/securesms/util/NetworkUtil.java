package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.webrtc.CallBandwidthMode;
import org.webrtc.PeerConnection;

/* loaded from: classes4.dex */
public final class NetworkUtil {
    private NetworkUtil() {
    }

    public static boolean isConnectedWifi(Context context) {
        NetworkInfo networkInfo = getNetworkInfo(context);
        if (networkInfo == null || !networkInfo.isConnected() || networkInfo.getType() != 1) {
            return false;
        }
        return true;
    }

    public static boolean isConnectedMobile(Context context) {
        NetworkInfo networkInfo = getNetworkInfo(context);
        return networkInfo != null && networkInfo.isConnected() && networkInfo.getType() == 0;
    }

    public static boolean isConnectedRoaming(Context context) {
        NetworkInfo networkInfo = getNetworkInfo(context);
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isRoaming() && networkInfo.getType() == 0;
    }

    public static CallManager.BandwidthMode getCallingBandwidthMode(Context context) {
        return getCallingBandwidthMode(context, PeerConnection.AdapterType.UNKNOWN);
    }

    public static CallManager.BandwidthMode getCallingBandwidthMode(Context context, PeerConnection.AdapterType adapterType) {
        CallManager.BandwidthMode callingBandwidthMode = SignalStore.internalValues().callingBandwidthMode();
        CallManager.BandwidthMode bandwidthMode = CallManager.BandwidthMode.NORMAL;
        if (callingBandwidthMode != bandwidthMode) {
            return SignalStore.internalValues().callingBandwidthMode();
        }
        return useLowBandwidthCalling(context, adapterType) ? CallManager.BandwidthMode.LOW : bandwidthMode;
    }

    public static String getNetworkTypeDescriptor(Context context) {
        NetworkInfo networkInfo = getNetworkInfo(context);
        if (networkInfo == null || !networkInfo.isConnected()) {
            return "NOT CONNECTED";
        }
        if (networkInfo.getType() == 1) {
            return "WIFI";
        }
        if (networkInfo.getType() != 0) {
            return "UNKNOWN";
        }
        switch (networkInfo.getSubtype()) {
            case 1:
                return "MOBILE - GPRS";
            case 2:
                return "MOBILE - EDGE";
            case 3:
                return "MOBILE - UMTS";
            case 4:
                return "MOBILE - CDMA";
            case 5:
                return "MOBILE - EVDO_0";
            case 6:
                return "MOBILE - EVDO_A";
            case 7:
                return "MOBILE - 1xRTT";
            case 8:
                return "MOBILE - HSDPA";
            case 9:
                return "MOBILE - HSUPA";
            case 10:
                return "MOBILE - HSPA";
            case 11:
                return "MOBILE - IDEN";
            case 12:
                return "MOBILE - EVDO_B";
            case 13:
                return "MOBILE - LTE";
            case 14:
                return "MOBILE - EHRDP";
            case 15:
                return "MOBILE - HSPAP";
            case 16:
                return "MOBILE - GSM";
            case 17:
                return "MOBILE - TD_SCDMA";
            case 18:
                return "MOBILE - IWLAN";
            case 19:
                return "MOBILE - LTE_CA";
            case 20:
                return "MOBILE - NR";
            default:
                return "MOBILE - OTHER";
        }
    }

    public static NetworkStatus getNetworkStatus(Context context) {
        ConnectivityManager connectivityManager = ServiceUtil.getConnectivityManager(context);
        boolean z = false;
        if (Build.VERSION.SDK_INT < 23) {
            return new NetworkStatus(false, false);
        }
        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
        boolean z2 = networkCapabilities != null && networkCapabilities.hasTransport(4);
        if (networkCapabilities == null || networkCapabilities.hasCapability(11)) {
            z = true;
        }
        return new NetworkStatus(z2, true ^ z);
    }

    /* renamed from: org.thoughtcrime.securesms.util.NetworkUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$webrtc$CallBandwidthMode;
        static final /* synthetic */ int[] $SwitchMap$org$webrtc$PeerConnection$AdapterType;

        static {
            int[] iArr = new int[CallBandwidthMode.values().length];
            $SwitchMap$org$thoughtcrime$securesms$webrtc$CallBandwidthMode = iArr;
            try {
                iArr[CallBandwidthMode.HIGH_ON_WIFI.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$webrtc$CallBandwidthMode[CallBandwidthMode.HIGH_ALWAYS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[PeerConnection.AdapterType.values().length];
            $SwitchMap$org$webrtc$PeerConnection$AdapterType = iArr2;
            try {
                iArr2[PeerConnection.AdapterType.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$webrtc$PeerConnection$AdapterType[PeerConnection.AdapterType.VPN.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$webrtc$PeerConnection$AdapterType[PeerConnection.AdapterType.ADAPTER_TYPE_ANY.ordinal()] = 3;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$webrtc$PeerConnection$AdapterType[PeerConnection.AdapterType.ETHERNET.ordinal()] = 4;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$webrtc$PeerConnection$AdapterType[PeerConnection.AdapterType.WIFI.ordinal()] = 5;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$webrtc$PeerConnection$AdapterType[PeerConnection.AdapterType.LOOPBACK.ordinal()] = 6;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$webrtc$PeerConnection$AdapterType[PeerConnection.AdapterType.CELLULAR.ordinal()] = 7;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$webrtc$PeerConnection$AdapterType[PeerConnection.AdapterType.CELLULAR_2G.ordinal()] = 8;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$webrtc$PeerConnection$AdapterType[PeerConnection.AdapterType.CELLULAR_3G.ordinal()] = 9;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$webrtc$PeerConnection$AdapterType[PeerConnection.AdapterType.CELLULAR_4G.ordinal()] = 10;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$webrtc$PeerConnection$AdapterType[PeerConnection.AdapterType.CELLULAR_5G.ordinal()] = 11;
            } catch (NoSuchFieldError unused13) {
            }
        }
    }

    private static boolean useLowBandwidthCalling(Context context, PeerConnection.AdapterType adapterType) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$webrtc$CallBandwidthMode[SignalStore.settings().getCallBandwidthMode().ordinal()];
        if (i == 1) {
            int i2 = AnonymousClass1.$SwitchMap$org$webrtc$PeerConnection$AdapterType[adapterType.ordinal()];
            if (i2 == 1 || i2 == 2 || i2 == 3) {
                return !isConnectedWifi(context);
            }
            switch (i2) {
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                    return true;
                default:
                    return false;
            }
        } else if (i != 2) {
            return true;
        } else {
            return false;
        }
    }

    private static NetworkInfo getNetworkInfo(Context context) {
        return ServiceUtil.getConnectivityManager(context).getActiveNetworkInfo();
    }
}
