package org.thoughtcrime.securesms.util;

import android.os.Parcel;
import android.os.Parcelable;
import org.thoughtcrime.securesms.util.CharacterCalculator;

/* loaded from: classes4.dex */
public class MmsCharacterCalculator extends CharacterCalculator {
    public static final Parcelable.Creator<MmsCharacterCalculator> CREATOR = new Parcelable.Creator<MmsCharacterCalculator>() { // from class: org.thoughtcrime.securesms.util.MmsCharacterCalculator.1
        @Override // android.os.Parcelable.Creator
        public MmsCharacterCalculator createFromParcel(Parcel parcel) {
            return new MmsCharacterCalculator();
        }

        @Override // android.os.Parcelable.Creator
        public MmsCharacterCalculator[] newArray(int i) {
            return new MmsCharacterCalculator[i];
        }
    };
    private static final int MAX_SIZE;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
    }

    @Override // org.thoughtcrime.securesms.util.CharacterCalculator
    public CharacterCalculator.CharacterState calculateCharacters(String str) {
        return new CharacterCalculator.CharacterState(1, 5000 - str.length(), 5000, 5000);
    }
}
