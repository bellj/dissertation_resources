package org.thoughtcrime.securesms.util.adapter;

import androidx.recyclerview.widget.DiffUtil;

/* loaded from: classes4.dex */
public final class AlwaysChangedDiffUtil<T> extends DiffUtil.ItemCallback<T> {
    @Override // androidx.recyclerview.widget.DiffUtil.ItemCallback
    public boolean areContentsTheSame(T t, T t2) {
        return false;
    }

    @Override // androidx.recyclerview.widget.DiffUtil.ItemCallback
    public boolean areItemsTheSame(T t, T t2) {
        return false;
    }
}
