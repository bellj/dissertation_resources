package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.appcompat.content.res.AppCompatResources;
import java.util.Objects;

/* loaded from: classes4.dex */
public final class ContextUtil {
    private ContextUtil() {
    }

    public static Drawable requireDrawable(Context context, int i) {
        Drawable drawable = AppCompatResources.getDrawable(context, i);
        Objects.requireNonNull(drawable);
        return drawable;
    }
}
