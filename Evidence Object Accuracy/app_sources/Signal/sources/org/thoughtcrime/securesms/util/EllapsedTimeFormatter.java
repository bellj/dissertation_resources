package org.thoughtcrime.securesms.util;

import java.util.Locale;

/* loaded from: classes4.dex */
public class EllapsedTimeFormatter {
    private final long hours;
    private final long minutes;
    private final long seconds;

    private EllapsedTimeFormatter(long j) {
        this.hours = j / 3600;
        long j2 = j % 3600;
        this.minutes = j2 / 60;
        this.seconds = j2 % 60;
    }

    public String toString() {
        long j = this.hours;
        if (j > 0) {
            return String.format(Locale.US, "%02d:%02d:%02d", Long.valueOf(j), Long.valueOf(this.minutes), Long.valueOf(this.seconds));
        }
        return String.format(Locale.US, "%02d:%02d", Long.valueOf(this.minutes), Long.valueOf(this.seconds));
    }

    public static EllapsedTimeFormatter fromDurationMillis(long j) {
        if (j == -1) {
            return null;
        }
        return new EllapsedTimeFormatter(j);
    }
}
