package org.thoughtcrime.securesms.util.dualsim;

import android.content.Context;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import j$.util.Optional;
import j$.util.function.Function;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
public final class SubscriptionManagerCompat {
    private static final String TAG = Log.tag(SubscriptionManagerCompat.class);
    private final Context context;

    public SubscriptionManagerCompat(Context context) {
        this.context = context.getApplicationContext();
    }

    public Optional<Integer> getPreferredSubscriptionId() {
        if (Build.VERSION.SDK_INT < 24) {
            return Optional.empty();
        }
        return Optional.of(Integer.valueOf(SubscriptionManager.getDefaultSmsSubscriptionId()));
    }

    public Optional<SubscriptionInfoCompat> getActiveSubscriptionInfo(int i) {
        if (Build.VERSION.SDK_INT < 22) {
            return Optional.empty();
        }
        return Optional.ofNullable(getActiveSubscriptionInfoMap(false).get(Integer.valueOf(i)));
    }

    public Collection<SubscriptionInfoCompat> getActiveAndReadySubscriptionInfos() {
        if (Build.VERSION.SDK_INT < 22) {
            return Collections.emptyList();
        }
        return getActiveSubscriptionInfoMap(true).values();
    }

    private Map<Integer, SubscriptionInfoCompat> getActiveSubscriptionInfoMap(boolean z) {
        List<SubscriptionInfo> activeSubscriptionInfoList = getActiveSubscriptionInfoList();
        if (activeSubscriptionInfoList.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<SubscriptionInfo, CharSequence> descriptionsFor = getDescriptionsFor(activeSubscriptionInfoList);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (SubscriptionInfo subscriptionInfo : activeSubscriptionInfoList) {
            if (!z || isReady(subscriptionInfo)) {
                linkedHashMap.put(Integer.valueOf(subscriptionInfo.getSubscriptionId()), new SubscriptionInfoCompat(subscriptionInfo.getSubscriptionId(), descriptionsFor.get(subscriptionInfo), subscriptionInfo.getMcc(), subscriptionInfo.getMnc()));
            }
        }
        return linkedHashMap;
    }

    public boolean isMultiSim() {
        if (Build.VERSION.SDK_INT >= 22 && getActiveSubscriptionInfoList().size() >= 2) {
            return true;
        }
        return false;
    }

    private List<SubscriptionInfo> getActiveSubscriptionInfoList() {
        SubscriptionManager subscriptionManager = ServiceUtil.getSubscriptionManager(this.context);
        if (subscriptionManager == null) {
            Log.w(TAG, "Missing SubscriptionManager.");
            return Collections.emptyList();
        }
        List<SubscriptionInfo> activeSubscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();
        return activeSubscriptionInfoList != null ? activeSubscriptionInfoList : Collections.emptyList();
    }

    private Map<SubscriptionInfo, CharSequence> getDescriptionsFor(Collection<SubscriptionInfo> collection) {
        Map<SubscriptionInfo, CharSequence> createDescriptionMap = createDescriptionMap(collection, new Function() { // from class: org.thoughtcrime.securesms.util.dualsim.SubscriptionManagerCompat$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((SubscriptionInfo) obj).getDisplayName();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        if (hasNoDuplicates(createDescriptionMap.values())) {
            return createDescriptionMap;
        }
        return createDescriptionMap(collection, new Function() { // from class: org.thoughtcrime.securesms.util.dualsim.SubscriptionManagerCompat$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SubscriptionManagerCompat.this.describeSimIndex((SubscriptionInfo) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public String describeSimIndex(SubscriptionInfo subscriptionInfo) {
        return this.context.getString(R.string.conversation_activity__sim_n, Integer.valueOf(subscriptionInfo.getSimSlotIndex() + 1));
    }

    private static Map<SubscriptionInfo, CharSequence> createDescriptionMap(Collection<SubscriptionInfo> collection, Function<SubscriptionInfo, CharSequence> function) {
        HashMap hashMap = new HashMap();
        for (SubscriptionInfo subscriptionInfo : collection) {
            hashMap.put(subscriptionInfo, function.apply(subscriptionInfo));
        }
        return hashMap;
    }

    private static <T> boolean hasNoDuplicates(Collection<T> collection) {
        HashSet hashSet = new HashSet();
        for (T t : collection) {
            if (!hashSet.add(t)) {
                return false;
            }
        }
        return true;
    }

    private boolean isReady(SubscriptionInfo subscriptionInfo) {
        if (Build.VERSION.SDK_INT >= 24 && ServiceUtil.getTelephonyManager(this.context).createForSubscriptionId(subscriptionInfo.getSubscriptionId()).getSimState() != 5) {
            return false;
        }
        return true;
    }
}
