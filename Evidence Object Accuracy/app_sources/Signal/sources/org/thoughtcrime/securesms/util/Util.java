package org.thoughtcrime.securesms.util;

import android.app.ActivityManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiFunction;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber$PhoneNumber;
import j$.util.Optional;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ComposeText;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.OutgoingLegacyMmsConnection;

/* loaded from: classes.dex */
public class Util {
    private static final long BUILD_LIFESPAN = TimeUnit.DAYS.toMillis(90);
    private static final String TAG = Log.tag(Util.class);

    public static CharSequence emptyIfNull(CharSequence charSequence) {
        return charSequence != null ? charSequence : "";
    }

    public static String emptyIfNull(String str) {
        return str != null ? str : "";
    }

    public static <T> T firstNonNull(T t, T t2) {
        return t != null ? t : t2;
    }

    public static int getCanonicalVersionCode() {
        return BuildConfig.CANONICAL_VERSION_CODE;
    }

    public static float halfOffsetFromScale(int i, float f) {
        float f2 = (float) i;
        return (f2 - (f * f2)) / 2.0f;
    }

    public static <T> List<T> asList(T... tArr) {
        LinkedList linkedList = new LinkedList();
        Collections.addAll(linkedList, tArr);
        return linkedList;
    }

    public static String join(String[] strArr, String str) {
        return join((Collection) Arrays.asList(strArr), str);
    }

    public static <T> String join(Collection<T> collection, String str) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (T t : collection) {
            sb.append(t);
            i++;
            if (i < collection.size()) {
                sb.append(str);
            }
        }
        return sb.toString();
    }

    public static String join(long[] jArr, String str) {
        ArrayList arrayList = new ArrayList(jArr.length);
        for (long j : jArr) {
            arrayList.add(Long.valueOf(j));
        }
        return join((List<Long>) arrayList, str);
    }

    @SafeVarargs
    public static <E> List<E> join(List<E>... listArr) {
        ArrayList arrayList = new ArrayList(((Integer) Stream.of(listArr).reduce(0, new BiFunction() { // from class: org.thoughtcrime.securesms.util.Util$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return Util.lambda$join$0((Integer) obj, (List) obj2);
            }
        })).intValue());
        for (List<E> list : listArr) {
            arrayList.addAll(list);
        }
        return arrayList;
    }

    public static /* synthetic */ Integer lambda$join$0(Integer num, List list) {
        return Integer.valueOf(num.intValue() + list.size());
    }

    public static String join(List<Long> list, String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0) {
                sb.append(str);
            }
            sb.append(list.get(i));
        }
        return sb.toString();
    }

    public static String rightPad(String str, int i) {
        if (str.length() >= i) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str);
        while (sb.length() < i) {
            sb.append(" ");
        }
        return sb.toString();
    }

    public static boolean isEmpty(EncodedStringValue[] encodedStringValueArr) {
        return encodedStringValueArr == null || encodedStringValueArr.length == 0;
    }

    public static boolean isEmpty(ComposeText composeText) {
        return composeText == null || composeText.getText() == null || TextUtils.isEmpty(composeText.getTextTrimmed());
    }

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isEmpty(CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0;
    }

    public static boolean hasItems(Collection<?> collection) {
        return collection != null && !collection.isEmpty();
    }

    public static <K, V> V getOrDefault(Map<K, V> map, K k, V v) {
        return map.containsKey(k) ? map.get(k) : v;
    }

    public static String getFirstNonEmpty(String... strArr) {
        for (String str : strArr) {
            if (!isEmpty(str)) {
                return str;
            }
        }
        return "";
    }

    public static CharSequence getBoldedString(String str) {
        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(new StyleSpan(1), 0, spannableString.length(), 33);
        return spannableString;
    }

    public static String toIsoString(byte[] bArr) {
        try {
            return new String(bArr, "iso-8859-1");
        } catch (UnsupportedEncodingException unused) {
            throw new AssertionError("ISO_8859_1 must be supported!");
        }
    }

    public static byte[] toIsoBytes(String str) {
        try {
            return str.getBytes("iso-8859-1");
        } catch (UnsupportedEncodingException unused) {
            throw new AssertionError("ISO_8859_1 must be supported!");
        }
    }

    public static byte[] toUtf8Bytes(String str) {
        try {
            return str.getBytes("utf-8");
        } catch (UnsupportedEncodingException unused) {
            throw new AssertionError("UTF_8 must be supported!");
        }
    }

    public static void wait(Object obj, long j) {
        try {
            obj.wait(j);
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        }
    }

    public static Optional<Phonenumber$PhoneNumber> getDeviceNumber(Context context) {
        try {
            String line1Number = ((TelephonyManager) context.getSystemService(RecipientDatabase.PHONE)).getLine1Number();
            Optional<String> simCountryIso = getSimCountryIso(context);
            if (TextUtils.isEmpty(line1Number)) {
                return Optional.empty();
            }
            if (!simCountryIso.isPresent()) {
                return Optional.empty();
            }
            return Optional.ofNullable(PhoneNumberUtil.getInstance().parse(line1Number, simCountryIso.get()));
        } catch (NumberParseException e) {
            Log.w(TAG, e);
            return Optional.empty();
        }
    }

    public static Optional<String> getSimCountryIso(Context context) {
        String simCountryIso = ((TelephonyManager) context.getSystemService(RecipientDatabase.PHONE)).getSimCountryIso();
        return Optional.ofNullable(simCountryIso != null ? simCountryIso.toUpperCase() : null);
    }

    @SafeVarargs
    public static <T> T firstNonNull(T... tArr) {
        for (T t : tArr) {
            if (t != null) {
                return t;
            }
        }
        throw new IllegalStateException("All choices were null.");
    }

    public static <T> List<List<T>> partition(List<T> list, int i) {
        LinkedList linkedList = new LinkedList();
        int i2 = 0;
        while (i2 < list.size()) {
            linkedList.add(list.subList(i2, Math.min(i, list.size() - i2) + i2));
            i2 += i;
        }
        return linkedList;
    }

    public static List<String> split(String str, String str2) {
        LinkedList linkedList = new LinkedList();
        if (TextUtils.isEmpty(str)) {
            return linkedList;
        }
        Collections.addAll(linkedList, str.split(str2));
        return linkedList;
    }

    public static byte[][] split(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, i);
        byte[] bArr3 = new byte[i2];
        byte[][] bArr4 = {bArr2, bArr3};
        System.arraycopy(bArr, i, bArr3, 0, i2);
        return bArr4;
    }

    public static byte[] combine(byte[]... bArr) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (byte[] bArr2 : bArr) {
                byteArrayOutputStream.write(bArr2);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] trim(byte[] bArr, int i) {
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, i);
        return bArr2;
    }

    public static boolean isDefaultSmsProvider(Context context) {
        return context.getPackageName().equals(Telephony.Sms.getDefaultSmsPackage(context));
    }

    public static int getManifestApkVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new AssertionError(e);
        }
    }

    public static String getSecret(int i) {
        return Base64.encodeBytes(getSecretBytes(i));
    }

    public static byte[] getSecretBytes(int i) {
        return getSecretBytes(new SecureRandom(), i);
    }

    public static byte[] getSecretBytes(SecureRandom secureRandom, int i) {
        byte[] bArr = new byte[i];
        secureRandom.nextBytes(bArr);
        return bArr;
    }

    public static long getTimeUntilBuildExpiry() {
        if (SignalStore.misc().isClientDeprecated()) {
            return 0;
        }
        long currentTimeMillis = BUILD_LIFESPAN - (System.currentTimeMillis() - BuildConfig.BUILD_TIMESTAMP);
        long timeUntilDeprecation = RemoteDeprecation.getTimeUntilDeprecation();
        if (timeUntilDeprecation != -1) {
            return Math.max(Math.min(currentTimeMillis, timeUntilDeprecation), 0L);
        }
        return Math.max(currentTimeMillis, 0L);
    }

    public static boolean isMmsCapable(Context context) {
        return Build.VERSION.SDK_INT >= 21 || OutgoingLegacyMmsConnection.isConnectionPossible(context);
    }

    public static <T> T getRandomElement(T[] tArr) {
        return tArr[new SecureRandom().nextInt(tArr.length)];
    }

    public static <T> T getRandomElement(List<T> list) {
        return list.get(new SecureRandom().nextInt(list.size()));
    }

    public static boolean equals(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static int hashCode(Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    public static Uri uri(String str) {
        if (str == null) {
            return null;
        }
        return Uri.parse(str);
    }

    public static boolean isLowMemory(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        return activityManager.isLowRamDevice() || activityManager.getLargeMemoryClass() <= 64;
    }

    public static int clamp(int i, int i2, int i3) {
        return Math.min(Math.max(i, i2), i3);
    }

    public static long clamp(long j, long j2, long j3) {
        return Math.min(Math.max(j, j2), j3);
    }

    public static float clamp(float f, float f2, float f3) {
        return Math.min(Math.max(f, f2), f3);
    }

    public static String readTextFromClipboard(Context context) {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService("clipboard");
        if (!clipboardManager.hasPrimaryClip() || clipboardManager.getPrimaryClip().getItemCount() <= 0) {
            return null;
        }
        return clipboardManager.getPrimaryClip().getItemAt(0).getText().toString();
    }

    public static void writeTextToClipboard(Context context, String str) {
        writeTextToClipboard(context, context.getString(R.string.app_name), str);
    }

    public static void writeTextToClipboard(Context context, String str, String str2) {
        ((ClipboardManager) context.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(str, str2));
    }

    public static int toIntExact(long j) {
        int i = (int) j;
        if (((long) i) == j) {
            return i;
        }
        throw new ArithmeticException("integer overflow");
    }

    public static boolean isEquals(Long l, long j) {
        return l != null && l.longValue() == j;
    }

    public static String getPrettyFileSize(long j) {
        return MemoryUnitFormat.formatBytes(j);
    }

    public static void copyToClipboard(Context context, CharSequence charSequence) {
        ServiceUtil.getClipboardManager(context).setPrimaryClip(ClipData.newPlainText(DraftDatabase.Draft.TEXT, charSequence));
    }

    @SafeVarargs
    public static <T> List<T> concatenatedList(Collection<T>... collectionArr) {
        ArrayList arrayList = new ArrayList(((Integer) Stream.of(collectionArr).reduce(0, new BiFunction() { // from class: org.thoughtcrime.securesms.util.Util$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return Util.lambda$concatenatedList$1((Integer) obj, (Collection) obj2);
            }
        })).intValue());
        for (Collection<T> collection : collectionArr) {
            arrayList.addAll(collection);
        }
        return arrayList;
    }

    public static /* synthetic */ Integer lambda$concatenatedList$1(Integer num, Collection collection) {
        return Integer.valueOf(num.intValue() + collection.size());
    }

    public static boolean isLong(String str) {
        try {
            Long.parseLong(str);
            return true;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    public static int parseInt(String str, int i) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException unused) {
            return i;
        }
    }
}
