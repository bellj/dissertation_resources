package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

/* loaded from: classes4.dex */
public final class VibrateUtil {
    private static final int TICK_LENGTH;

    private VibrateUtil() {
    }

    public static void vibrateTick(Context context) {
        vibrate(context, 30);
    }

    public static void vibrate(Context context, int i) {
        Vibrator vibrator = ServiceUtil.getVibrator(context);
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot((long) i, 64));
        } else {
            vibrator.vibrate((long) i);
        }
    }
}
