package org.thoughtcrime.securesms.util;

import android.os.Parcelable;

/* loaded from: classes4.dex */
public abstract class CharacterCalculator implements Parcelable {
    public abstract CharacterState calculateCharacters(String str);

    /* loaded from: classes4.dex */
    public static class CharacterState {
        public final int charactersRemaining;
        public final int maxPrimaryMessageSize;
        public final int maxTotalMessageSize;
        public final int messagesSpent;

        public CharacterState(int i, int i2, int i3, int i4) {
            this.messagesSpent = i;
            this.charactersRemaining = i2;
            this.maxTotalMessageSize = i3;
            this.maxPrimaryMessageSize = i4;
        }
    }
}
