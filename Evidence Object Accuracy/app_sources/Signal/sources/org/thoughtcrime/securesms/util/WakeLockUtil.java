package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.os.PowerManager;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class WakeLockUtil {
    private static final String TAG = Log.tag(WakeLockUtil.class);

    public static void runWithLock(Context context, int i, long j, String str, Runnable runnable) {
        Throwable th;
        PowerManager.WakeLock wakeLock;
        try {
            wakeLock = acquire(context, i, j, str);
            try {
                runnable.run();
                if (wakeLock != null) {
                    release(wakeLock, str);
                }
            } catch (Throwable th2) {
                th = th2;
                if (wakeLock != null) {
                    release(wakeLock, str);
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            wakeLock = null;
        }
    }

    public static PowerManager.WakeLock acquire(Context context, int i, long j, String str) {
        String prefixTag = prefixTag(str);
        try {
            PowerManager.WakeLock newWakeLock = ServiceUtil.getPowerManager(context).newWakeLock(i, prefixTag);
            newWakeLock.acquire(j);
            return newWakeLock;
        } catch (Exception e) {
            String str2 = TAG;
            Log.w(str2, "Failed to acquire wakelock with tag: " + prefixTag, e);
            return null;
        }
    }

    public static void release(PowerManager.WakeLock wakeLock, String str) {
        String prefixTag = prefixTag(str);
        try {
            if (wakeLock == null) {
                Log.d(TAG, "Wakelock was null. Skipping. Tag: " + prefixTag);
            } else if (wakeLock.isHeld()) {
                wakeLock.release();
            } else {
                Log.d(TAG, "Wakelock wasn't held at time of release: " + prefixTag);
            }
        } catch (Exception e) {
            while (true) {
                prefixTag = "Failed to release wakelock with tag: " + prefixTag;
                Log.w(TAG, prefixTag, e);
                return;
            }
        }
    }

    private static String prefixTag(String str) {
        if (str.startsWith("signal:")) {
            return str;
        }
        return "signal:" + str;
    }
}
