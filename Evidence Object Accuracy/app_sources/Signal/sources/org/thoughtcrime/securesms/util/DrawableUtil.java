package org.thoughtcrime.securesms.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import androidx.core.graphics.drawable.DrawableCompat;

/* loaded from: classes4.dex */
public final class DrawableUtil {
    private static final int SHORTCUT_INFO_BITMAP_SIZE;
    private static final int SHORTCUT_INFO_PADDING;
    public static final int SHORTCUT_INFO_WRAPPED_SIZE;

    static {
        int dpToPx = ViewUtil.dpToPx(108);
        SHORTCUT_INFO_BITMAP_SIZE = dpToPx;
        int dpToPx2 = ViewUtil.dpToPx(72);
        SHORTCUT_INFO_WRAPPED_SIZE = dpToPx2;
        SHORTCUT_INFO_PADDING = (dpToPx - dpToPx2) / 2;
    }

    private DrawableUtil() {
    }

    public static Bitmap toBitmap(Drawable drawable, int i, int i2) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return createBitmap;
    }

    public static Bitmap wrapBitmapForShortcutInfo(Bitmap bitmap) {
        int i = SHORTCUT_INFO_BITMAP_SIZE;
        Bitmap createBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
        int i2 = SHORTCUT_INFO_WRAPPED_SIZE;
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, i2, i2, true);
        Canvas canvas = new Canvas(createBitmap);
        int i3 = SHORTCUT_INFO_PADDING;
        canvas.drawBitmap(createScaledBitmap, (float) i3, (float) i3, (Paint) null);
        return createBitmap;
    }

    public static Drawable tint(Drawable drawable, int i) {
        Drawable mutate = DrawableCompat.wrap(drawable).mutate();
        DrawableCompat.setTint(mutate, i);
        return mutate;
    }
}
