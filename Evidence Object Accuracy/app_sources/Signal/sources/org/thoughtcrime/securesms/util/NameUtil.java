package org.thoughtcrime.securesms.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsKt;
import org.signal.core.util.CharacterIterable;

/* compiled from: NameUtil.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\b\u001a\u00020\u0007H\u0007J\f\u0010\t\u001a\u00020\u0007*\u00020\u0007H\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/util/NameUtil;", "", "()V", "PATTERN", "Ljava/util/regex/Pattern;", "kotlin.jvm.PlatformType", "getAbbreviation", "", "name", "firstGrapheme", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NameUtil {
    public static final NameUtil INSTANCE = new NameUtil();
    private static final Pattern PATTERN = Pattern.compile("[^\\p{L}\\p{Nd}\\p{S}]+");

    private NameUtil() {
    }

    @JvmStatic
    public static final String getAbbreviation(String str) {
        Intrinsics.checkNotNullParameter(str, "name");
        List<String> list = StringsKt__StringsKt.split$default((CharSequence) str, new String[]{" "}, false, 0, 6, (Object) null);
        ArrayList<String> arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (String str2 : list) {
            arrayList.add(StringsKt__StringsKt.trim(str2).toString());
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        for (String str3 : arrayList) {
            arrayList2.add(PATTERN.matcher(str3).replaceFirst(""));
        }
        ArrayList arrayList3 = new ArrayList();
        Iterator it = arrayList2.iterator();
        while (true) {
            boolean z = false;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            String str4 = (String) next;
            Intrinsics.checkNotNullExpressionValue(str4, "it");
            if (str4.length() > 0) {
                z = true;
            }
            if (z) {
                arrayList3.add(next);
            }
        }
        if (arrayList3.isEmpty()) {
            return null;
        }
        if (arrayList3.size() == 1) {
            NameUtil nameUtil = INSTANCE;
            Object obj = arrayList3.get(0);
            Intrinsics.checkNotNullExpressionValue(obj, "parts[0]");
            return nameUtil.firstGrapheme((String) obj);
        }
        StringBuilder sb = new StringBuilder();
        NameUtil nameUtil2 = INSTANCE;
        Object obj2 = arrayList3.get(0);
        Intrinsics.checkNotNullExpressionValue(obj2, "parts[0]");
        sb.append(nameUtil2.firstGrapheme((String) obj2));
        Object obj3 = arrayList3.get(1);
        Intrinsics.checkNotNullExpressionValue(obj3, "parts[1]");
        sb.append(nameUtil2.firstGrapheme((String) obj3));
        return sb.toString();
    }

    private final String firstGrapheme(String str) {
        Object obj = CollectionsKt___CollectionsKt.first(new CharacterIterable(str));
        Intrinsics.checkNotNullExpressionValue(obj, "CharacterIterable(this).first()");
        return (String) obj;
    }
}
