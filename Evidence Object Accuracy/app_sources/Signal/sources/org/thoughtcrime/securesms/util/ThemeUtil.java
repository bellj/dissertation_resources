package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.ContextThemeWrapper;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ThemeUtil {
    public static boolean isDarkNotificationTheme(Context context) {
        return (context.getResources().getConfiguration().uiMode & 48) == 32;
    }

    public static boolean isDarkTheme(Context context) {
        return getAttribute(context, R.attr.theme_type, "light").equals("dark");
    }

    public static int getThemedResourceId(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return typedValue.resourceId;
        }
        return -1;
    }

    public static boolean getThemedBoolean(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(i, typedValue, true) || typedValue.data == 0) {
            return false;
        }
        return true;
    }

    public static int getThemedColor(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return typedValue.data;
        }
        return -65536;
    }

    public static Drawable getThemedDrawable(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return AppCompatResources.getDrawable(context, typedValue.resourceId);
        }
        return null;
    }

    public static LayoutInflater getThemedInflater(Context context, LayoutInflater layoutInflater, int i) {
        return layoutInflater.cloneInContext(new ContextThemeWrapper(context, i));
    }

    public static float getThemedDimen(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return typedValue.getDimension(context.getResources().getDisplayMetrics());
        }
        return 0.0f;
    }

    private static String getAttribute(Context context, int i, String str) {
        CharSequence coerceToString;
        TypedValue typedValue = new TypedValue();
        return (!context.getTheme().resolveAttribute(i, typedValue, true) || (coerceToString = typedValue.coerceToString()) == null) ? str : coerceToString.toString();
    }
}
