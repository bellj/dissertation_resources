package org.thoughtcrime.securesms.util;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: EnumUtils.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\b\u0002\n\u0002\u0010\u0010\n\u0002\b\u0002\u001a$\u0010\u0000\u001a\u0002H\u0001\"\u0010\b\u0000\u0010\u0001\u0018\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u0001H\b¢\u0006\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"next", "T", "", "(Ljava/lang/Enum;)Ljava/lang/Enum;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EnumUtilsKt {
    public static final /* synthetic */ <T extends Enum<T>> T next(T t) {
        Intrinsics.checkNotNullParameter(t, "<this>");
        Intrinsics.reifiedOperationMarker(5, "T");
        return (T) new Enum[0][(t.ordinal() + 1) % 0];
    }
}
