package org.thoughtcrime.securesms.util.spans;

import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

/* loaded from: classes4.dex */
public class CenterAlignedRelativeSizeSpan extends MetricAffectingSpan {
    private final float relativeSize;

    public CenterAlignedRelativeSizeSpan(float f) {
        this.relativeSize = f;
    }

    @Override // android.text.style.MetricAffectingSpan
    public void updateMeasureState(TextPaint textPaint) {
        updateDrawState(textPaint);
    }

    @Override // android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        textPaint.setTextSize(textPaint.getTextSize() * this.relativeSize);
        textPaint.baselineShift += ((int) (textPaint.ascent() * this.relativeSize)) / 4;
    }
}
