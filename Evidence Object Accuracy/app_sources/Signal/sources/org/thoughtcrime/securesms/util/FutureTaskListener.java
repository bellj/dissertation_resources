package org.thoughtcrime.securesms.util;

import java.util.concurrent.ExecutionException;

/* loaded from: classes4.dex */
public interface FutureTaskListener<V> {
    void onFailure(ExecutionException executionException);

    void onSuccess(V v);
}
