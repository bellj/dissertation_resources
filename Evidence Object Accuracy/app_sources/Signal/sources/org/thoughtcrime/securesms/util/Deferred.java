package org.thoughtcrime.securesms.util;

/* loaded from: classes4.dex */
public class Deferred {
    private Runnable deferred;
    private boolean isDeferred = true;

    public void defer(Runnable runnable) {
        this.deferred = runnable;
        executeIfNecessary();
    }

    public void setDeferred(boolean z) {
        this.isDeferred = z;
        executeIfNecessary();
    }

    public boolean isDeferred() {
        return this.isDeferred;
    }

    private void executeIfNecessary() {
        Runnable runnable = this.deferred;
        if (runnable != null && !this.isDeferred) {
            this.deferred = null;
            runnable.run();
        }
    }
}
