package org.thoughtcrime.securesms.util.fragments;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ListenerExtensions.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\u0018\u0000 \t2\u00060\u0001j\u0002`\u0002:\u0001\tB\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\b¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/util/fragments/ListenerNotFoundException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "hierarchy", "", "", "cause", "", "(Ljava/util/List;Ljava/lang/Throwable;)V", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ListenerNotFoundException extends Exception {
    public static final Companion Companion = new Companion(null);

    /* compiled from: ListenerExtensions.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/util/fragments/ListenerNotFoundException$Companion;", "", "()V", "formatMessage", "", "hierarchy", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final String formatMessage(List<String> list) {
            Intrinsics.checkNotNullParameter(list, "hierarchy");
            return "Hierarchy Searched: \n" + CollectionsKt___CollectionsKt.joinToString$default(list, "\n", null, null, 0, null, null, 62, null);
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ListenerNotFoundException(List<String> list, Throwable th) {
        super(Companion.formatMessage(list), th);
        Intrinsics.checkNotNullParameter(list, "hierarchy");
        Intrinsics.checkNotNullParameter(th, "cause");
    }
}
