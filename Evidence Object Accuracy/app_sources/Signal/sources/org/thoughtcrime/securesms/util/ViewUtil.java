package org.thoughtcrime.securesms.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.lifecycle.Lifecycle;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;
import org.thoughtcrime.securesms.util.views.Stub;

/* loaded from: classes4.dex */
public final class ViewUtil {
    private ViewUtil() {
    }

    public static void focusAndMoveCursorToEndAndOpenKeyboard(EditText editText) {
        int length = editText.getText().length();
        editText.setSelection(length, length);
        focusAndShowKeyboard(editText);
    }

    public static void focusAndShowKeyboard(final View view) {
        view.requestFocus();
        if (view.hasWindowFocus()) {
            showTheKeyboardNow(view);
        } else {
            view.getViewTreeObserver().addOnWindowFocusChangeListener(new ViewTreeObserver.OnWindowFocusChangeListener() { // from class: org.thoughtcrime.securesms.util.ViewUtil.1
                @Override // android.view.ViewTreeObserver.OnWindowFocusChangeListener
                public void onWindowFocusChanged(boolean z) {
                    if (z) {
                        ViewUtil.showTheKeyboardNow(view);
                        view.getViewTreeObserver().removeOnWindowFocusChangeListener(this);
                    }
                }
            });
        }
    }

    public static void showTheKeyboardNow(View view) {
        if (view.isFocused()) {
            view.post(new Runnable(view) { // from class: org.thoughtcrime.securesms.util.ViewUtil$$ExternalSyntheticLambda0
                public final /* synthetic */ View f$0;

                {
                    this.f$0 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ViewUtil.lambda$showTheKeyboardNow$0(this.f$0);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$showTheKeyboardNow$0(View view) {
        ServiceUtil.getInputMethodManager(view.getContext()).showSoftInput(view, 1);
    }

    public static <T extends View> T inflateStub(View view, int i) {
        return (T) ((ViewStub) view.findViewById(i)).inflate();
    }

    public static <T extends View> Stub<T> findStubById(Activity activity, int i) {
        return new Stub<>((ViewStub) activity.findViewById(i));
    }

    public static <T extends View> Stub<T> findStubById(View view, int i) {
        return new Stub<>((ViewStub) view.findViewById(i));
    }

    private static Animation getAlphaAnimation(float f, float f2, int i) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f, f2);
        alphaAnimation.setInterpolator(new FastOutSlowInInterpolator());
        alphaAnimation.setDuration((long) i);
        return alphaAnimation;
    }

    public static void fadeIn(View view, int i) {
        animateIn(view, getAlphaAnimation(0.0f, 1.0f, i));
    }

    public static ListenableFuture<Boolean> fadeOut(View view, int i) {
        return fadeOut(view, i, 8);
    }

    public static ListenableFuture<Boolean> fadeOut(View view, int i, int i2) {
        return animateOut(view, getAlphaAnimation(1.0f, 0.0f, i), i2);
    }

    public static ListenableFuture<Boolean> animateOut(View view, Animation animation) {
        return animateOut(view, animation, 8);
    }

    public static ListenableFuture<Boolean> animateOut(final View view, Animation animation, final int i) {
        final SettableFuture settableFuture = new SettableFuture();
        if (view.getVisibility() == i) {
            settableFuture.set(Boolean.TRUE);
        } else {
            view.clearAnimation();
            animation.reset();
            animation.setStartTime(0);
            animation.setAnimationListener(new Animation.AnimationListener() { // from class: org.thoughtcrime.securesms.util.ViewUtil.2
                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationRepeat(Animation animation2) {
                }

                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationStart(Animation animation2) {
                }

                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationEnd(Animation animation2) {
                    view.setVisibility(i);
                    settableFuture.set(Boolean.TRUE);
                }
            });
            view.startAnimation(animation);
        }
        return settableFuture;
    }

    public static void animateIn(View view, Animation animation) {
        if (view.getVisibility() != 0) {
            view.clearAnimation();
            animation.reset();
            animation.setStartTime(0);
            view.setVisibility(0);
            view.startAnimation(animation);
        }
    }

    public static <T extends View> T inflate(LayoutInflater layoutInflater, ViewGroup viewGroup, int i) {
        return (T) layoutInflater.inflate(i, viewGroup, false);
    }

    public static void setTextViewGravityStart(TextView textView, Context context) {
        if (isRtl(context)) {
            textView.setGravity(5);
        } else {
            textView.setGravity(3);
        }
    }

    public static void mirrorIfRtl(View view, Context context) {
        if (isRtl(context)) {
            view.setScaleX(-1.0f);
        }
    }

    public static boolean isLtr(View view) {
        return isLtr(view.getContext());
    }

    public static boolean isLtr(Context context) {
        return context.getResources().getConfiguration().getLayoutDirection() == 0;
    }

    public static boolean isRtl(View view) {
        return isRtl(view.getContext());
    }

    public static boolean isRtl(Context context) {
        return context.getResources().getConfiguration().getLayoutDirection() == 1;
    }

    public static float pxToDp(float f) {
        return f / Resources.getSystem().getDisplayMetrics().density;
    }

    public static int dpToPx(Context context, int i) {
        double d = (double) (((float) i) * context.getResources().getDisplayMetrics().density);
        Double.isNaN(d);
        return (int) (d + 0.5d);
    }

    public static int dpToPx(int i) {
        return Math.round(((float) i) * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToSp(int i) {
        return (int) (((float) dpToPx(i)) / Resources.getSystem().getDisplayMetrics().scaledDensity);
    }

    public static int spToPx(float f) {
        return (int) TypedValue.applyDimension(2, f, Resources.getSystem().getDisplayMetrics());
    }

    public static void updateLayoutParams(View view, int i, int i2) {
        view.getLayoutParams().width = i;
        view.getLayoutParams().height = i2;
        view.requestLayout();
    }

    public static void updateLayoutParamsIfNonNull(View view, int i, int i2) {
        if (view != null) {
            updateLayoutParams(view, i, i2);
        }
    }

    public static void setVisibilityIfNonNull(View view, int i) {
        if (view != null) {
            view.setVisibility(i);
        }
    }

    public static int getLeftMargin(View view) {
        if (isLtr(view)) {
            return ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin;
        }
        return ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin;
    }

    public static int getRightMargin(View view) {
        if (isLtr(view)) {
            return ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin;
        }
        return ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin;
    }

    public static int getTopMargin(View view) {
        return ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).topMargin;
    }

    public static void setLeftMargin(View view, int i) {
        if (isLtr(view)) {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin = i;
        } else {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin = i;
        }
        view.forceLayout();
        view.requestLayout();
    }

    public static void setRightMargin(View view, int i) {
        if (isLtr(view)) {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin = i;
        } else {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin = i;
        }
        view.forceLayout();
        view.requestLayout();
    }

    public static void setTopMargin(View view, int i) {
        ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).topMargin = i;
        view.requestLayout();
    }

    public static void setBottomMargin(View view, int i) {
        ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).bottomMargin = i;
        view.requestLayout();
    }

    public static int getWidth(View view) {
        return view.getLayoutParams().width;
    }

    public static void setPaddingTop(View view, int i) {
        view.setPadding(view.getPaddingLeft(), i, view.getPaddingRight(), view.getPaddingBottom());
    }

    public static void setPaddingBottom(View view, int i) {
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), i);
    }

    public static void setPadding(View view, int i) {
        view.setPadding(i, i, i, i);
    }

    public static void setPaddingStart(View view, int i) {
        if (isLtr(view)) {
            view.setPadding(i, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
        } else {
            view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), i, view.getPaddingBottom());
        }
    }

    public static void setPaddingEnd(View view, int i) {
        if (isLtr(view)) {
            view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), i, view.getPaddingBottom());
        } else {
            view.setPadding(i, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
        }
    }

    public static boolean isPointInsideView(View view, float f, float f2) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        if (f <= ((float) i) || f >= ((float) (i + view.getWidth())) || f2 <= ((float) i2) || f2 >= ((float) (i2 + view.getHeight()))) {
            return false;
        }
        return true;
    }

    public static int getStatusBarHeight(View view) {
        int identifier = view.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            return view.getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    public static int getNavigationBarHeight(View view) {
        int identifier = view.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (identifier > 0) {
            return view.getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    public static void hideKeyboard(Context context, View view) {
        ((InputMethodManager) context.getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void setEnabledRecursive(View view, boolean z) {
        view.setEnabled(z);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                setEnabledRecursive(viewGroup.getChildAt(i), z);
            }
        }
    }

    public static Lifecycle getActivityLifecycle(View view) {
        return getActivityLifecycle(view.getContext());
    }

    private static Lifecycle getActivityLifecycle(Context context) {
        if (context instanceof ContextThemeWrapper) {
            return getActivityLifecycle(((ContextThemeWrapper) context).getBaseContext());
        }
        if (context instanceof AppCompatActivity) {
            return ((AppCompatActivity) context).getLifecycle();
        }
        return null;
    }
}
