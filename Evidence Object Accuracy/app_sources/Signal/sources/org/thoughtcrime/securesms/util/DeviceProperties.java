package org.thoughtcrime.securesms.util;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;

/* loaded from: classes4.dex */
public final class DeviceProperties {
    public static boolean shouldAllowApngStickerAnimation(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return false;
        }
        ActivityManager.MemoryInfo memoryInfo = getMemoryInfo(context);
        int megabytes = (int) ByteUnit.BYTES.toMegabytes(memoryInfo.totalMem);
        if (isLowMemoryDevice(context) || memoryInfo.lowMemory) {
            return false;
        }
        if (megabytes >= FeatureFlags.animatedStickerMinimumTotalMemoryMb() || getMemoryClass(context) >= FeatureFlags.animatedStickerMinimumMemoryClass()) {
            return true;
        }
        return false;
    }

    public static boolean isLowMemoryDevice(Context context) {
        return ServiceUtil.getActivityManager(context).isLowRamDevice();
    }

    public static int getMemoryClass(Context context) {
        return ServiceUtil.getActivityManager(context).getMemoryClass();
    }

    public static ActivityManager.MemoryInfo getMemoryInfo(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ServiceUtil.getActivityManager(context).getMemoryInfo(memoryInfo);
        return memoryInfo;
    }

    public static boolean isBackgroundRestricted(Context context) {
        return ServiceUtil.getActivityManager(context).isBackgroundRestricted();
    }
}
