package org.thoughtcrime.securesms.util;

import android.graphics.PointF;

/* loaded from: classes4.dex */
public class MathUtils {
    private static float crossProduct(float f, float f2, float f3, float f4, float f5, float f6) {
        return ((f - f5) * (f4 - f6)) - ((f3 - f5) * (f2 - f6));
    }

    public static boolean pointInTriangle(PointF pointF, PointF pointF2, PointF pointF3, PointF pointF4) {
        boolean z = crossProduct(pointF, pointF2, pointF3) < 0.0f;
        boolean z2 = crossProduct(pointF, pointF3, pointF4) < 0.0f;
        boolean z3 = crossProduct(pointF, pointF4, pointF2) < 0.0f;
        if (z == z2 && z2 == z3) {
            return true;
        }
        return false;
    }

    private static float crossProduct(PointF pointF, PointF pointF2, PointF pointF3) {
        return crossProduct(pointF.x, pointF.y, pointF2.x, pointF2.y, pointF3.x, pointF3.y);
    }
}
