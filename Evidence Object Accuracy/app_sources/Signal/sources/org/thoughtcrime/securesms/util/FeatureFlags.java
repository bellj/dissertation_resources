package org.thoughtcrime.securesms.util;

import android.text.TextUtils;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Predicate;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;
import org.signal.core.util.SetUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda4;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.jobs.RefreshAttributesJob;
import org.thoughtcrime.securesms.jobs.RemoteConfigRefreshJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.messageprocessingalarm.MessageProcessReceiver;

/* loaded from: classes.dex */
public final class FeatureFlags {
    private static final String ANIMATED_STICKER_MIN_MEMORY;
    private static final String ANIMATED_STICKER_MIN_TOTAL_MEMORY;
    private static final String AUTOMATIC_SESSION_INTERVAL;
    private static final String AUTOMATIC_SESSION_RESET;
    private static final String CDS_REFRESH_INTERVAL;
    private static final String CLIENT_EXPIRATION;
    private static final String CUSTOM_VIDEO_MUXER;
    private static final String DEFAULT_MAX_BACKOFF;
    public static final String DONATE_MEGAPHONE;
    private static final String DONOR_BADGES;
    private static final String DONOR_BADGES_DISPLAY;
    private static final long FETCH_INTERVAL = TimeUnit.HOURS.toMillis(2);
    private static final Map<String, OnFlagChange> FLAG_CHANGE_LISTENERS = new HashMap<String, OnFlagChange>() { // from class: org.thoughtcrime.securesms.util.FeatureFlags.2
        {
            put(FeatureFlags.MESSAGE_PROCESSOR_ALARM_INTERVAL, new FeatureFlags$2$$ExternalSyntheticLambda0());
            put(FeatureFlags.SENDER_KEY, new FeatureFlags$2$$ExternalSyntheticLambda1());
            put(FeatureFlags.STORIES, new FeatureFlags$2$$ExternalSyntheticLambda2());
            put(FeatureFlags.GIFT_BADGE_RECEIVE_SUPPORT, new FeatureFlags$2$$ExternalSyntheticLambda3());
        }

        public static /* synthetic */ void lambda$new$0(Change change) {
            MessageProcessReceiver.startOrUpdateAlarm(ApplicationDependencies.getApplication());
        }

        public static /* synthetic */ void lambda$new$1(Change change) {
            ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
        }

        public static /* synthetic */ void lambda$new$2(Change change) {
            ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
        }

        public static /* synthetic */ void lambda$new$3(Change change) {
            ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
        }
    };
    static final Map<String, Object> FORCED_VALUES = new HashMap<String, Object>() { // from class: org.thoughtcrime.securesms.util.FeatureFlags.1
    };
    private static final String GIFT_BADGE_RECEIVE_SUPPORT;
    private static final String GIFT_BADGE_SEND_SUPPORT;
    private static final String GROUPS_V2_HARD_LIMIT;
    private static final String GROUPS_V2_RECOMMENDED_LIMIT;
    private static final String GROUP_CALL_RINGING;
    private static final String GROUP_NAME_MAX_LENGTH;
    private static final String HARDWARE_AEC_BLOCKLIST_MODELS;
    static final Set<String> HOT_SWAPPABLE = SetUtil.newHashSet(VERIFY_V2, CLIENT_EXPIRATION, CUSTOM_VIDEO_MUXER, CDS_REFRESH_INTERVAL, GROUP_NAME_MAX_LENGTH, AUTOMATIC_SESSION_RESET, AUTOMATIC_SESSION_INTERVAL, DEFAULT_MAX_BACKOFF, SERVER_ERROR_MAX_BACKOFF, OKHTTP_AUTOMATIC_RETRY, SHARE_SELECTION_LIMIT, ANIMATED_STICKER_MIN_MEMORY, ANIMATED_STICKER_MIN_TOTAL_MEMORY, MESSAGE_PROCESSOR_ALARM_INTERVAL, MESSAGE_PROCESSOR_DELAY, MEDIA_QUALITY_LEVELS, RETRY_RECEIPT_LIFESPAN, RETRY_RESPOND_MAX_AGE, SUGGEST_SMS_BLACKLIST, RETRY_RECEIPTS, SENDER_KEY, MAX_GROUP_CALL_RING_SIZE, GROUP_CALL_RINGING, SENDER_KEY_MAX_AGE, DONOR_BADGES_DISPLAY, DONATE_MEGAPHONE, HARDWARE_AEC_BLOCKLIST_MODELS, SOFTWARE_AEC_BLOCKLIST_MODELS, USE_HARDWARE_AEC_IF_OLD, USE_AEC3, PAYMENTS_COUNTRY_BLOCKLIST, USE_FCM_FOREGROUND_SERVICE, USE_QR_LEGACY_SCAN, TELECOM_MANUFACTURER_ALLOWLIST, TELECOM_MODEL_BLOCKLIST);
    private static final String INTERNAL_USER;
    private static final String MAX_GROUP_CALL_RING_SIZE;
    private static final String MEDIA_QUALITY_LEVELS;
    private static final String MESSAGE_PROCESSOR_ALARM_INTERVAL;
    private static final String MESSAGE_PROCESSOR_DELAY;
    static final Set<String> NOT_REMOTE_CAPABLE = SetUtil.newHashSet(PHONE_NUMBER_PRIVACY);
    private static final String OKHTTP_AUTOMATIC_RETRY;
    private static final String PAYMENTS_COUNTRY_BLOCKLIST;
    private static final String PAYMENTS_KILL_SWITCH;
    private static final String PHONE_NUMBER_PRIVACY;
    static final Set<String> REMOTE_CAPABLE = SetUtil.newHashSet(PAYMENTS_KILL_SWITCH, GROUPS_V2_RECOMMENDED_LIMIT, GROUPS_V2_HARD_LIMIT, INTERNAL_USER, USERNAMES, VERIFY_V2, CLIENT_EXPIRATION, DONATE_MEGAPHONE, CUSTOM_VIDEO_MUXER, CDS_REFRESH_INTERVAL, GROUP_NAME_MAX_LENGTH, AUTOMATIC_SESSION_RESET, AUTOMATIC_SESSION_INTERVAL, DEFAULT_MAX_BACKOFF, SERVER_ERROR_MAX_BACKOFF, OKHTTP_AUTOMATIC_RETRY, SHARE_SELECTION_LIMIT, ANIMATED_STICKER_MIN_MEMORY, ANIMATED_STICKER_MIN_TOTAL_MEMORY, MESSAGE_PROCESSOR_ALARM_INTERVAL, MESSAGE_PROCESSOR_DELAY, MEDIA_QUALITY_LEVELS, RETRY_RECEIPT_LIFESPAN, RETRY_RESPOND_MAX_AGE, SENDER_KEY, RETRY_RECEIPTS, SUGGEST_SMS_BLACKLIST, MAX_GROUP_CALL_RING_SIZE, GROUP_CALL_RINGING, SENDER_KEY_MAX_AGE, DONOR_BADGES, DONOR_BADGES_DISPLAY, STORIES, STORIES_TEXT_FUNCTIONS, HARDWARE_AEC_BLOCKLIST_MODELS, SOFTWARE_AEC_BLOCKLIST_MODELS, USE_HARDWARE_AEC_IF_OLD, USE_AEC3, PAYMENTS_COUNTRY_BLOCKLIST, USE_FCM_FOREGROUND_SERVICE, STORIES_AUTO_DOWNLOAD_MAXIMUM, GIFT_BADGE_RECEIVE_SUPPORT, GIFT_BADGE_SEND_SUPPORT, USE_QR_LEGACY_SCAN, TELECOM_MANUFACTURER_ALLOWLIST, TELECOM_MODEL_BLOCKLIST);
    private static final Map<String, Object> REMOTE_VALUES = new TreeMap();
    private static final String RETRY_RECEIPTS;
    private static final String RETRY_RECEIPT_LIFESPAN;
    private static final String RETRY_RESPOND_MAX_AGE;
    private static final String SENDER_KEY;
    private static final String SENDER_KEY_MAX_AGE;
    private static final String SERVER_ERROR_MAX_BACKOFF;
    private static final String SHARE_SELECTION_LIMIT;
    private static final String SOFTWARE_AEC_BLOCKLIST_MODELS;
    static final Set<String> STICKY = SetUtil.newHashSet(VERIFY_V2);
    private static final String STORIES;
    private static final String STORIES_AUTO_DOWNLOAD_MAXIMUM;
    private static final String STORIES_TEXT_FUNCTIONS;
    private static final String SUGGEST_SMS_BLACKLIST;
    private static final String TAG = Log.tag(FeatureFlags.class);
    private static final String TELECOM_MANUFACTURER_ALLOWLIST;
    private static final String TELECOM_MODEL_BLOCKLIST;
    private static final String USERNAMES;
    private static final String USE_AEC3;
    private static final String USE_FCM_FOREGROUND_SERVICE;
    private static final String USE_HARDWARE_AEC_IF_OLD;
    private static final String USE_QR_LEGACY_SCAN;
    private static final String VERIFY_V2;

    /* loaded from: classes4.dex */
    public enum Change {
        ENABLED,
        DISABLED,
        CHANGED,
        REMOVED
    }

    /* loaded from: classes4.dex */
    public interface OnFlagChange {
        void onFlagChange(Change change);
    }

    /* loaded from: classes4.dex */
    private enum VersionFlag {
        OFF,
        ON_IN_FUTURE_VERSION,
        ON
    }

    private FeatureFlags() {
    }

    public static synchronized void init() {
        synchronized (FeatureFlags.class) {
            Map<String, Object> parseStoredConfig = parseStoredConfig(SignalStore.remoteConfigValues().getCurrentConfig());
            Map<String, Object> parseStoredConfig2 = parseStoredConfig(SignalStore.remoteConfigValues().getPendingConfig());
            Map<String, Change> computeChanges = computeChanges(parseStoredConfig, parseStoredConfig2);
            SignalStore.remoteConfigValues().setCurrentConfig(mapToJson(parseStoredConfig2));
            Map<String, Object> map = REMOTE_VALUES;
            map.putAll(parseStoredConfig2);
            triggerFlagChangeListeners(computeChanges);
            String str = TAG;
            Log.i(str, "init() " + map.toString());
        }
    }

    public static void refreshIfNecessary() {
        long currentTimeMillis = System.currentTimeMillis() - SignalStore.remoteConfigValues().getLastFetchTime();
        if (currentTimeMillis < 0 || currentTimeMillis > FETCH_INTERVAL) {
            Log.i(TAG, "Scheduling remote config refresh.");
            ApplicationDependencies.getJobManager().add(new RemoteConfigRefreshJob());
            return;
        }
        String str = TAG;
        Log.i(str, "Skipping remote config refresh. Refreshed " + currentTimeMillis + " ms ago.");
    }

    public static void refreshSync() throws IOException {
        update(ApplicationDependencies.getSignalServiceAccountManager().getRemoteConfig());
    }

    public static synchronized void update(Map<String, Object> map) {
        synchronized (FeatureFlags.class) {
            Map<String, Object> map2 = REMOTE_VALUES;
            Map<String, Object> parseStoredConfig = parseStoredConfig(SignalStore.remoteConfigValues().getPendingConfig());
            UpdateResult updateInternal = updateInternal(map, map2, parseStoredConfig, REMOTE_CAPABLE, HOT_SWAPPABLE, STICKY);
            SignalStore.remoteConfigValues().setPendingConfig(mapToJson(updateInternal.getDisk()));
            map2.clear();
            map2.putAll(updateInternal.getMemory());
            triggerFlagChangeListeners(updateInternal.getMemoryChanges());
            SignalStore.remoteConfigValues().setLastFetchTime(System.currentTimeMillis());
            String str = TAG;
            Log.i(str, "[Memory] Before: " + map2.toString());
            Log.i(str, "[Memory] After : " + updateInternal.getMemory().toString());
            Log.i(str, "[Disk]   Before: " + parseStoredConfig.toString());
            Log.i(str, "[Disk]   After : " + updateInternal.getDisk().toString());
        }
    }

    public static synchronized boolean usernames() {
        boolean z;
        synchronized (FeatureFlags.class) {
            z = getBoolean(USERNAMES, false);
        }
        return z;
    }

    public static SelectionLimits groupLimits() {
        return new SelectionLimits(getInteger(GROUPS_V2_RECOMMENDED_LIMIT, 151), getInteger(GROUPS_V2_HARD_LIMIT, 1001));
    }

    public static boolean payments() {
        return !getBoolean(PAYMENTS_KILL_SWITCH, false);
    }

    public static boolean internalUser() {
        return getBoolean(INTERNAL_USER, false);
    }

    public static boolean verifyV2() {
        return getBoolean(VERIFY_V2, false);
    }

    public static String clientExpiration() {
        return getString(CLIENT_EXPIRATION, null);
    }

    public static String donateMegaphone() {
        return getString(DONATE_MEGAPHONE, "");
    }

    public static boolean phoneNumberPrivacy() {
        getBoolean(PHONE_NUMBER_PRIVACY, false);
        return false;
    }

    public static boolean useStreamingVideoMuxer() {
        return getBoolean(CUSTOM_VIDEO_MUXER, false);
    }

    public static int cdsRefreshIntervalSeconds() {
        return getInteger(CDS_REFRESH_INTERVAL, (int) TimeUnit.HOURS.toSeconds(48));
    }

    public static SelectionLimits shareSelectionLimit() {
        int integer = getInteger(SHARE_SELECTION_LIMIT, 5);
        return new SelectionLimits(integer, integer);
    }

    public static int getMaxGroupNameGraphemeLength() {
        return Math.max(32, getInteger(GROUP_NAME_MAX_LENGTH, -1));
    }

    public static boolean automaticSessionReset() {
        return getBoolean(AUTOMATIC_SESSION_RESET, true);
    }

    public static int automaticSessionResetIntervalSeconds() {
        return getInteger(AUTOMATIC_SESSION_RESET, (int) TimeUnit.HOURS.toSeconds(1));
    }

    public static long getDefaultMaxBackoff() {
        return TimeUnit.SECONDS.toMillis((long) getInteger(DEFAULT_MAX_BACKOFF, 60));
    }

    public static long getServerErrorMaxBackoff() {
        return TimeUnit.SECONDS.toMillis((long) getInteger(SERVER_ERROR_MAX_BACKOFF, (int) TimeUnit.HOURS.toSeconds(6)));
    }

    public static boolean okHttpAutomaticRetry() {
        return getBoolean(OKHTTP_AUTOMATIC_RETRY, true);
    }

    public static int animatedStickerMinimumMemoryClass() {
        return getInteger(ANIMATED_STICKER_MIN_MEMORY, 193);
    }

    public static int animatedStickerMinimumTotalMemoryMb() {
        return getInteger(ANIMATED_STICKER_MIN_TOTAL_MEMORY, (int) ByteUnit.GIGABYTES.toMegabytes(3));
    }

    public static String getMediaQualityLevels() {
        return getString(MEDIA_QUALITY_LEVELS, "");
    }

    public static boolean retryReceipts() {
        return getBoolean(RETRY_RECEIPTS, true);
    }

    public static long retryReceiptLifespan() {
        return getLong(RETRY_RECEIPT_LIFESPAN, TimeUnit.HOURS.toMillis(1));
    }

    public static long retryRespondMaxAge() {
        return getLong(RETRY_RESPOND_MAX_AGE, TimeUnit.DAYS.toMillis(14));
    }

    public static long senderKeyMaxAge() {
        TimeUnit timeUnit = TimeUnit.DAYS;
        return Math.min(getLong(SENDER_KEY_MAX_AGE, timeUnit.toMillis(14)), timeUnit.toMillis(90));
    }

    public static String suggestSmsBlacklist() {
        return getString(SUGGEST_SMS_BLACKLIST, "");
    }

    public static long maxGroupCallRingSize() {
        return getLong(MAX_GROUP_CALL_RING_SIZE, 16);
    }

    public static boolean groupCallRinging() {
        return getBoolean(GROUP_CALL_RINGING, false);
    }

    public static String paymentsCountryBlocklist() {
        return getString(PAYMENTS_COUNTRY_BLOCKLIST, "98,963,53,850,7");
    }

    public static boolean donorBadges() {
        return getBoolean(DONOR_BADGES, true) || SignalStore.donationsValues().getSubscriber() != null;
    }

    public static boolean stories() {
        return getBoolean(STORIES, false);
    }

    public static boolean storiesTextFunctions() {
        return getBoolean(STORIES_TEXT_FUNCTIONS, false);
    }

    public static boolean displayDonorBadges() {
        return getBoolean(DONOR_BADGES_DISPLAY, true);
    }

    public static String hardwareAecBlocklistModels() {
        return getString(HARDWARE_AEC_BLOCKLIST_MODELS, "");
    }

    public static String softwareAecBlocklistModels() {
        return getString(SOFTWARE_AEC_BLOCKLIST_MODELS, "");
    }

    public static String telecomManufacturerAllowList() {
        return getString(TELECOM_MANUFACTURER_ALLOWLIST, "");
    }

    public static String telecomModelBlockList() {
        return getString(TELECOM_MODEL_BLOCKLIST, "");
    }

    public static boolean useHardwareAecIfOlderThanApi29() {
        return getBoolean(USE_HARDWARE_AEC_IF_OLD, false);
    }

    public static boolean useAec3() {
        return getBoolean(USE_AEC3, true);
    }

    public static boolean useFcmForegroundService() {
        return getBoolean(USE_FCM_FOREGROUND_SERVICE, false);
    }

    public static int storiesAutoDownloadMaximum() {
        return getInteger(STORIES_AUTO_DOWNLOAD_MAXIMUM, 2);
    }

    public static boolean giftBadgeReceiveSupport() {
        return getBoolean(GIFT_BADGE_RECEIVE_SUPPORT, false);
    }

    public static boolean giftBadgeSendSupport() {
        return giftBadgeReceiveSupport() && getBoolean(GIFT_BADGE_SEND_SUPPORT, false);
    }

    public static boolean useQrLegacyScan() {
        return getBoolean(USE_QR_LEGACY_SCAN, false);
    }

    public static synchronized Map<String, Object> getMemoryValues() {
        TreeMap treeMap;
        synchronized (FeatureFlags.class) {
            treeMap = new TreeMap(REMOTE_VALUES);
        }
        return treeMap;
    }

    public static synchronized Map<String, Object> getDiskValues() {
        TreeMap treeMap;
        synchronized (FeatureFlags.class) {
            treeMap = new TreeMap(parseStoredConfig(SignalStore.remoteConfigValues().getCurrentConfig()));
        }
        return treeMap;
    }

    public static synchronized Map<String, Object> getPendingDiskValues() {
        TreeMap treeMap;
        synchronized (FeatureFlags.class) {
            treeMap = new TreeMap(parseStoredConfig(SignalStore.remoteConfigValues().getPendingConfig()));
        }
        return treeMap;
    }

    public static synchronized Map<String, Object> getForcedValues() {
        TreeMap treeMap;
        synchronized (FeatureFlags.class) {
            treeMap = new TreeMap(FORCED_VALUES);
        }
        return treeMap;
    }

    static UpdateResult updateInternal(Map<String, Object> map, Map<String, Object> map2, Map<String, Object> map3, Set<String> set, Set<String> set2, Set<String> set3) {
        TreeMap treeMap = new TreeMap(map2);
        TreeMap treeMap2 = new TreeMap(map3);
        HashSet hashSet = new HashSet();
        hashSet.addAll(map.keySet());
        hashSet.addAll(map3.keySet());
        hashSet.addAll(map2.keySet());
        Stream of = Stream.of(hashSet);
        Objects.requireNonNull(set);
        of.filter(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda4(set)).forEach(new Consumer(map, map3, treeMap2, set2, treeMap, set3) { // from class: org.thoughtcrime.securesms.util.FeatureFlags$$ExternalSyntheticLambda0
            public final /* synthetic */ Map f$0;
            public final /* synthetic */ Map f$1;
            public final /* synthetic */ Map f$2;
            public final /* synthetic */ Set f$3;
            public final /* synthetic */ Map f$4;
            public final /* synthetic */ Set f$5;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                FeatureFlags.lambda$updateInternal$0(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, (String) obj);
            }
        });
        Stream.of(hashSet).filterNot(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda4(set)).filterNot(new Predicate(set3, map3) { // from class: org.thoughtcrime.securesms.util.FeatureFlags$$ExternalSyntheticLambda1
            public final /* synthetic */ Set f$0;
            public final /* synthetic */ Map f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return FeatureFlags.lambda$updateInternal$1(this.f$0, this.f$1, (String) obj);
            }
        }).forEach(new Consumer(treeMap2, set2, treeMap) { // from class: org.thoughtcrime.securesms.util.FeatureFlags$$ExternalSyntheticLambda2
            public final /* synthetic */ Map f$0;
            public final /* synthetic */ Set f$1;
            public final /* synthetic */ Map f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                FeatureFlags.lambda$updateInternal$2(this.f$0, this.f$1, this.f$2, (String) obj);
            }
        });
        return new UpdateResult(treeMap, treeMap2, computeChanges(map2, treeMap));
    }

    public static /* synthetic */ void lambda$updateInternal$0(Map map, Map map2, Map map3, Set set, Map map4, Set set2, String str) {
        Object obj = map.get(str);
        Object obj2 = map2.get(str);
        if (obj == null || obj2 == null || obj.getClass() == obj2.getClass()) {
            if (set2.contains(str) && ((obj instanceof Boolean) || (obj2 instanceof Boolean))) {
                Boolean bool = Boolean.TRUE;
                if (obj2 == bool) {
                    obj = bool;
                }
            } else if (set2.contains(str)) {
                String str2 = TAG;
                Log.w(str2, "Tried to make a non-boolean sticky! Ignoring. (key: " + str + ")");
            }
            if (obj != null) {
                map3.put(str, obj);
            } else {
                map3.remove(str);
            }
            if (!set.contains(str)) {
                return;
            }
            if (obj != null) {
                map4.put(str, obj);
            } else {
                map4.remove(str);
            }
        } else {
            String str3 = TAG;
            Log.w(str3, "Type mismatch! key: " + str);
            map3.remove(str);
            if (set.contains(str)) {
                map4.remove(str);
            }
        }
    }

    public static /* synthetic */ boolean lambda$updateInternal$1(Set set, Map map, String str) {
        return set.contains(str) && map.get(str) == Boolean.TRUE;
    }

    public static /* synthetic */ void lambda$updateInternal$2(Map map, Set set, Map map2, String str) {
        map.remove(str);
        if (set.contains(str)) {
            map2.remove(str);
        }
    }

    static Map<String, Change> computeChanges(Map<String, Object> map, Map<String, Object> map2) {
        HashMap hashMap = new HashMap();
        HashSet<String> hashSet = new HashSet();
        hashSet.addAll(map.keySet());
        hashSet.addAll(map2.keySet());
        for (String str : hashSet) {
            Object obj = map.get(str);
            Object obj2 = map2.get(str);
            if (obj == null && obj2 == null) {
                throw new AssertionError("Should not be possible.");
            } else if (obj != null && obj2 == null) {
                hashMap.put(str, Change.REMOVED);
            } else if (obj2 != obj && (obj2 instanceof Boolean)) {
                hashMap.put(str, ((Boolean) obj2).booleanValue() ? Change.ENABLED : Change.DISABLED);
            } else if (!Objects.equals(obj, obj2)) {
                hashMap.put(str, Change.CHANGED);
            }
        }
        return hashMap;
    }

    private static VersionFlag getVersionFlag(String str) {
        int integer = getInteger(str, 0);
        if (integer == 0) {
            return VersionFlag.OFF;
        }
        if (1097 >= integer) {
            return VersionFlag.ON;
        }
        return VersionFlag.ON_IN_FUTURE_VERSION;
    }

    public static long getBackgroundMessageProcessInterval() {
        return TimeUnit.MINUTES.toMillis((long) getInteger(MESSAGE_PROCESSOR_ALARM_INTERVAL, (int) TimeUnit.HOURS.toMinutes(6)));
    }

    public static long getBackgroundMessageProcessForegroundDelay() {
        return (long) getInteger(MESSAGE_PROCESSOR_DELAY, WebRtcCallView.PIP_RESIZE_DURATION);
    }

    private static boolean getBoolean(String str, boolean z) {
        Boolean bool = (Boolean) FORCED_VALUES.get(str);
        if (bool != null) {
            return bool.booleanValue();
        }
        Object obj = REMOTE_VALUES.get(str);
        if (obj instanceof Boolean) {
            return ((Boolean) obj).booleanValue();
        }
        if (obj != null) {
            String str2 = TAG;
            Log.w(str2, "Expected a boolean for key '" + str + "', but got something else! Falling back to the default.");
        }
        return z;
    }

    private static int getInteger(String str, int i) {
        Integer num = (Integer) FORCED_VALUES.get(str);
        if (num != null) {
            return num.intValue();
        }
        Object obj = REMOTE_VALUES.get(str);
        if (obj instanceof String) {
            try {
                return Integer.parseInt((String) obj);
            } catch (NumberFormatException unused) {
                String str2 = TAG;
                Log.w(str2, "Expected an int for key '" + str + "', but got something else! Falling back to the default.");
            }
        }
        return i;
    }

    private static long getLong(String str, long j) {
        Long l = (Long) FORCED_VALUES.get(str);
        if (l != null) {
            return l.longValue();
        }
        Object obj = REMOTE_VALUES.get(str);
        if (obj instanceof String) {
            try {
                return Long.parseLong((String) obj);
            } catch (NumberFormatException unused) {
                String str2 = TAG;
                Log.w(str2, "Expected a long for key '" + str + "', but got something else! Falling back to the default.");
            }
        }
        return j;
    }

    private static String getString(String str, String str2) {
        String str3 = (String) FORCED_VALUES.get(str);
        if (str3 != null) {
            return str3;
        }
        Object obj = REMOTE_VALUES.get(str);
        return obj instanceof String ? (String) obj : str2;
    }

    private static Map<String, Object> parseStoredConfig(String str) {
        HashMap hashMap = new HashMap();
        if (TextUtils.isEmpty(str)) {
            Log.i(TAG, "No remote config stored. Skipping.");
            return hashMap;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.get(next));
            }
            return hashMap;
        } catch (JSONException unused) {
            throw new AssertionError("Failed to parse! Cleared storage.");
        }
    }

    private static String mapToJson(Map<String, Object> map) {
        try {
            JSONObject jSONObject = new JSONObject();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                jSONObject.put(entry.getKey(), entry.getValue());
            }
            return jSONObject.toString();
        } catch (JSONException e) {
            throw new AssertionError(e);
        }
    }

    private static void triggerFlagChangeListeners(Map<String, Change> map) {
        for (Map.Entry<String, Change> entry : map.entrySet()) {
            OnFlagChange onFlagChange = FLAG_CHANGE_LISTENERS.get(entry.getKey());
            if (onFlagChange != null) {
                String str = TAG;
                Log.i(str, "Triggering change listener for: " + entry.getKey());
                onFlagChange.onFlagChange(entry.getValue());
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class UpdateResult {
        private final Map<String, Object> disk;
        private final Map<String, Object> memory;
        private final Map<String, Change> memoryChanges;

        UpdateResult(Map<String, Object> map, Map<String, Object> map2, Map<String, Change> map3) {
            this.memory = map;
            this.disk = map2;
            this.memoryChanges = map3;
        }

        public Map<String, Object> getMemory() {
            return this.memory;
        }

        public Map<String, Object> getDisk() {
            return this.disk;
        }

        public Map<String, Change> getMemoryChanges() {
            return this.memoryChanges;
        }
    }
}
