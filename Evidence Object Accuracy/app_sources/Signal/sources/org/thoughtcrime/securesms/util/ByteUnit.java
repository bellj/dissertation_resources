package org.thoughtcrime.securesms.util;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* loaded from: classes4.dex */
public class ByteUnit extends Enum<ByteUnit> {
    public long toBytes(long j) {
        throw new AbstractMethodError();
    }

    public long toKilobytes(long j) {
        throw new AbstractMethodError();
    }

    public long toMegabytes(long j) {
        throw new AbstractMethodError();
    }

    public long toGigabytes(long j) {
        throw new AbstractMethodError();
    }
}
