package org.thoughtcrime.securesms.util;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.thoughtcrime.securesms.MediaPreviewActivity;

/* compiled from: ProjectionList.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\b\u0012\u0004\u0012\u00020\u0002`\u00032\u00020\u0004B\u000f\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\b\u0010\b\u001a\u00020\tH\u0016¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/util/ProjectionList;", "Ljava/util/ArrayList;", "Lorg/thoughtcrime/securesms/util/Projection;", "Lkotlin/collections/ArrayList;", "Ljava/io/Closeable;", MediaPreviewActivity.SIZE_EXTRA, "", "(I)V", "close", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ProjectionList extends ArrayList<Projection> implements Closeable {
    public ProjectionList() {
        this(0, 1, null);
    }

    public ProjectionList(int i) {
        super(i);
    }

    public /* synthetic */ ProjectionList(int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? 0 : i);
    }

    @Override // java.util.ArrayList, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final /* bridge */ boolean contains(Object obj) {
        if (!(obj instanceof Projection)) {
            return false;
        }
        return contains((Projection) obj);
    }

    public /* bridge */ boolean contains(Projection projection) {
        return super.contains((Object) projection);
    }

    public /* bridge */ int getSize() {
        return super.size();
    }

    @Override // java.util.ArrayList, java.util.List, java.util.AbstractList
    public final /* bridge */ int indexOf(Object obj) {
        if (!(obj instanceof Projection)) {
            return -1;
        }
        return indexOf((Projection) obj);
    }

    public /* bridge */ int indexOf(Projection projection) {
        return super.indexOf((Object) projection);
    }

    @Override // java.util.ArrayList, java.util.List, java.util.AbstractList
    public final /* bridge */ int lastIndexOf(Object obj) {
        if (!(obj instanceof Projection)) {
            return -1;
        }
        return lastIndexOf((Projection) obj);
    }

    public /* bridge */ int lastIndexOf(Projection projection) {
        return super.lastIndexOf((Object) projection);
    }

    @Override // java.util.ArrayList, java.util.List, java.util.AbstractList
    public final /* bridge */ Projection remove(int i) {
        return removeAt(i);
    }

    @Override // java.util.ArrayList, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final /* bridge */ boolean remove(Object obj) {
        if (!(obj instanceof Projection)) {
            return false;
        }
        return remove((Projection) obj);
    }

    public /* bridge */ boolean remove(Projection projection) {
        return super.remove((Object) projection);
    }

    public /* bridge */ Projection removeAt(int i) {
        return (Projection) super.remove(i);
    }

    @Override // java.util.ArrayList, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final /* bridge */ int size() {
        return getSize();
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        Iterator<Projection> it = iterator();
        while (it.hasNext()) {
            it.next().release();
        }
    }
}
