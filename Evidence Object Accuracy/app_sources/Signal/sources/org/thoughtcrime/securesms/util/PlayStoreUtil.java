package org.thoughtcrime.securesms.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/* loaded from: classes4.dex */
public final class PlayStoreUtil {
    private PlayStoreUtil() {
    }

    public static void openPlayStoreOrOurApkDownloadPage(Context context) {
        CommunicationActions.openBrowserLink(context, "https://signal.org/android/apk");
    }

    private static void openPlayStore(Context context) {
        String packageName = context.getPackageName();
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + packageName)));
        } catch (ActivityNotFoundException unused) {
            CommunicationActions.openBrowserLink(context, "https://play.google.com/store/apps/details?id=" + packageName);
        }
    }
}
