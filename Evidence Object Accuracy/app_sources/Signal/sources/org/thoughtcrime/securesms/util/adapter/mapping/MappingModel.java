package org.thoughtcrime.securesms.util.adapter.mapping;

/* loaded from: classes4.dex */
public interface MappingModel<T> {

    /* renamed from: org.thoughtcrime.securesms.util.adapter.mapping.MappingModel$-CC */
    /* loaded from: classes4.dex */
    public final /* synthetic */ class CC<T> {
        public static Object $default$getChangePayload(MappingModel mappingModel, Object obj) {
            return null;
        }
    }

    boolean areContentsTheSame(T t);

    boolean areItemsTheSame(T t);

    Object getChangePayload(T t);
}
