package org.thoughtcrime.securesms.util;

import java.util.HashMap;
import java.util.Map;

/* loaded from: classes4.dex */
public final class SignalLocalMetrics {
    private SignalLocalMetrics() {
    }

    /* loaded from: classes.dex */
    public static final class ColdStart {
        private static final String NAME_CONVERSATION_LIST;
        private static final String NAME_OTHER;
        private static final String SPLIT_ACTIVITY_CREATE;
        private static final String SPLIT_APPLICATION_CREATE;
        private static final String SPLIT_DATA_LOADED;
        private static final String SPLIT_RENDER;
        private static String conversationListId;
        private static boolean isConversationList;
        private static String otherId;

        public static void start() {
            conversationListId = "cold-start-conversation-list-" + System.currentTimeMillis();
            otherId = "cold-start-other-" + System.currentTimeMillis();
            LocalMetrics.getInstance().start(conversationListId, NAME_CONVERSATION_LIST);
            LocalMetrics.getInstance().start(otherId, NAME_OTHER);
        }

        public static void onApplicationCreateFinished() {
            LocalMetrics.getInstance().split(conversationListId, SPLIT_APPLICATION_CREATE);
            LocalMetrics.getInstance().split(otherId, SPLIT_APPLICATION_CREATE);
        }

        public static void onRenderStart() {
            LocalMetrics.getInstance().split(conversationListId, SPLIT_ACTIVITY_CREATE);
            LocalMetrics.getInstance().split(otherId, SPLIT_ACTIVITY_CREATE);
        }

        public static void onConversationListDataLoaded() {
            isConversationList = true;
            LocalMetrics.getInstance().split(conversationListId, SPLIT_DATA_LOADED);
        }

        public static void onRenderFinished() {
            if (isConversationList) {
                LocalMetrics.getInstance().split(conversationListId, SPLIT_RENDER);
                LocalMetrics.getInstance().end(conversationListId);
                LocalMetrics.getInstance().cancel(otherId);
                return;
            }
            LocalMetrics.getInstance().split(otherId, SPLIT_RENDER);
            LocalMetrics.getInstance().end(otherId);
            LocalMetrics.getInstance().cancel(conversationListId);
        }
    }

    /* loaded from: classes4.dex */
    public static final class ConversationOpen {
        private static final String NAME;
        private static final String SPLIT_DATA_LOADED;
        private static final String SPLIT_DATA_POSTED;
        private static final String SPLIT_METADATA_LOADED;
        private static final String SPLIT_RENDER;
        private static final String SPLIT_VIEWMODEL_INIT;
        private static String id;

        public static void start() {
            id = "conversation-open-" + System.currentTimeMillis();
            LocalMetrics.getInstance().start(id, NAME);
        }

        public static void onMetadataLoadStarted() {
            LocalMetrics.getInstance().split(id, SPLIT_VIEWMODEL_INIT);
        }

        public static void onMetadataLoaded() {
            LocalMetrics.getInstance().split(id, SPLIT_METADATA_LOADED);
        }

        public static void onDataLoaded() {
            LocalMetrics.getInstance().split(id, SPLIT_DATA_LOADED);
        }

        public static void onDataPostedToMain() {
            LocalMetrics.getInstance().split(id, SPLIT_DATA_POSTED);
        }

        public static void onRenderFinished() {
            LocalMetrics.getInstance().split(id, SPLIT_RENDER);
            LocalMetrics.getInstance().end(id);
        }
    }

    /* loaded from: classes4.dex */
    public static final class IndividualMessageSend {
        private static final Map<Long, String> ID_MAP = new HashMap();
        private static final String NAME;
        private static final String SPLIT_DB_INSERT;
        private static final String SPLIT_ENCRYPT;
        private static final String SPLIT_JOB_ENQUEUE;
        private static final String SPLIT_JOB_POST_NETWORK;
        private static final String SPLIT_JOB_PRE_NETWORK;
        private static final String SPLIT_NETWORK_MAIN;
        private static final String SPLIT_NETWORK_SYNC;
        private static final String SPLIT_UI_UPDATE;

        public static String start() {
            String str = NAME + System.currentTimeMillis();
            LocalMetrics.getInstance().start(str, NAME);
            return str;
        }

        public static void onInsertedIntoDatabase(long j, String str) {
            if (str != null) {
                ID_MAP.put(Long.valueOf(j), str);
                split(j, SPLIT_DB_INSERT);
            }
        }

        public static void onJobStarted(long j) {
            split(j, SPLIT_JOB_ENQUEUE);
        }

        public static void onDeliveryStarted(long j) {
            split(j, SPLIT_JOB_PRE_NETWORK);
        }

        public static void onMessageEncrypted(long j) {
            split(j, SPLIT_ENCRYPT);
        }

        public static void onMessageSent(long j) {
            split(j, SPLIT_NETWORK_MAIN);
        }

        public static void onSyncMessageSent(long j) {
            split(j, SPLIT_NETWORK_SYNC);
        }

        public static void onJobFinished(long j) {
            split(j, SPLIT_JOB_POST_NETWORK);
        }

        public static void onUiUpdated(long j) {
            split(j, SPLIT_UI_UPDATE);
            end(j);
            ID_MAP.remove(Long.valueOf(j));
        }

        public static void cancel(long j) {
            Map<Long, String> map = ID_MAP;
            String str = map.get(Long.valueOf(j));
            if (str != null) {
                LocalMetrics.getInstance().cancel(str);
            }
            map.remove(Long.valueOf(j));
        }

        private static void split(long j, String str) {
            String str2 = ID_MAP.get(Long.valueOf(j));
            if (str2 != null) {
                LocalMetrics.getInstance().split(str2, str);
            }
        }

        private static void end(long j) {
            String str = ID_MAP.get(Long.valueOf(j));
            if (str != null) {
                LocalMetrics.getInstance().end(str);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class GroupMessageSend {
        private static final Map<Long, String> ID_MAP = new HashMap();
        private static final String NAME;
        private static final String SPLIT_DB_INSERT;
        private static final String SPLIT_ENCRYPTION;
        private static final String SPLIT_JOB_ENQUEUE;
        private static final String SPLIT_JOB_POST_NETWORK;
        private static final String SPLIT_JOB_PRE_NETWORK;
        private static final String SPLIT_MSL_SENDER_KEY;
        private static final String SPLIT_NETWORK_LEGACY;
        private static final String SPLIT_NETWORK_LEGACY_SYNC;
        private static final String SPLIT_NETWORK_SENDER_KEY;
        private static final String SPLIT_NETWORK_SENDER_KEY_SYNC;
        private static final String SPLIT_SENDER_KEY_SHARED;
        private static final String SPLIT_UI_UPDATE;

        public static String start() {
            String str = NAME + System.currentTimeMillis();
            LocalMetrics.getInstance().start(str, NAME);
            return str;
        }

        public static void onInsertedIntoDatabase(long j, String str) {
            if (str != null) {
                ID_MAP.put(Long.valueOf(j), str);
                split(j, SPLIT_DB_INSERT);
            }
        }

        public static void onJobStarted(long j) {
            split(j, SPLIT_JOB_ENQUEUE);
        }

        public static void onSenderKeyStarted(long j) {
            split(j, SPLIT_JOB_PRE_NETWORK);
        }

        public static void onSenderKeyShared(long j) {
            split(j, SPLIT_SENDER_KEY_SHARED);
        }

        public static void onSenderKeyEncrypted(long j) {
            split(j, SPLIT_ENCRYPTION);
        }

        public static void onSenderKeyMessageSent(long j) {
            split(j, SPLIT_NETWORK_SENDER_KEY);
        }

        public static void onSenderKeySyncSent(long j) {
            split(j, SPLIT_NETWORK_SENDER_KEY_SYNC);
        }

        public static void onSenderKeyMslInserted(long j) {
            split(j, SPLIT_MSL_SENDER_KEY);
        }

        public static void onLegacyMessageSent(long j) {
            split(j, SPLIT_NETWORK_LEGACY);
        }

        public static void onLegacySyncFinished(long j) {
            split(j, SPLIT_NETWORK_LEGACY_SYNC);
        }

        public static void onJobFinished(long j) {
            split(j, SPLIT_JOB_POST_NETWORK);
        }

        public static void onUiUpdated(long j) {
            split(j, SPLIT_UI_UPDATE);
            end(j);
            ID_MAP.remove(Long.valueOf(j));
        }

        public static void cancel(String str) {
            if (str != null) {
                LocalMetrics.getInstance().cancel(str);
            }
        }

        public static void cancel(long j) {
            Map<Long, String> map = ID_MAP;
            String str = map.get(Long.valueOf(j));
            if (str != null) {
                LocalMetrics.getInstance().cancel(str);
            }
            map.remove(Long.valueOf(j));
        }

        private static void split(long j, String str) {
            String str2 = ID_MAP.get(Long.valueOf(j));
            if (str2 != null) {
                LocalMetrics.getInstance().split(str2, str);
            }
        }

        private static void end(long j) {
            String str = ID_MAP.get(Long.valueOf(j));
            if (str != null) {
                LocalMetrics.getInstance().end(str);
            }
        }
    }
}
