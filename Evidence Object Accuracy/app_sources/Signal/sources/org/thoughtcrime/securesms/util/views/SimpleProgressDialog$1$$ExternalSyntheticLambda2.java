package org.thoughtcrime.securesms.util.views;

import androidx.appcompat.app.AlertDialog;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SimpleProgressDialog$1$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ AlertDialog f$0;

    public /* synthetic */ SimpleProgressDialog$1$$ExternalSyntheticLambda2(AlertDialog alertDialog) {
        this.f$0 = alertDialog;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.dismiss();
    }
}
