package org.thoughtcrime.securesms.util.adapter;

import android.util.LongSparseArray;
import android.util.SparseIntArray;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes4.dex */
public class RecyclerViewConcatenateAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<ChildAdapter> adapters = new LinkedList();
    private long nextUnassignedItemId;
    private final List<ChildAdapter> viewTypes = new LinkedList();

    /* loaded from: classes4.dex */
    public static class AdapterDataObserver extends RecyclerView.AdapterDataObserver {
        private final RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter;
        private final RecyclerViewConcatenateAdapter mergeAdapter;

        AdapterDataObserver(RecyclerViewConcatenateAdapter recyclerViewConcatenateAdapter, RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter) {
            this.mergeAdapter = recyclerViewConcatenateAdapter;
            this.adapter = adapter;
        }

        public void onChanged() {
            this.mergeAdapter.notifyDataSetChanged();
        }

        public void onItemRangeChanged(int i, int i2, Object obj) {
            this.mergeAdapter.notifyItemRangeChanged(this.mergeAdapter.getSubAdapterFirstGlobalPosition(this.adapter) + i, i2, obj);
        }

        public void onItemRangeChanged(int i, int i2) {
            this.mergeAdapter.notifyItemRangeChanged(this.mergeAdapter.getSubAdapterFirstGlobalPosition(this.adapter) + i, i2);
        }

        public void onItemRangeInserted(int i, int i2) {
            this.mergeAdapter.notifyItemRangeInserted(this.mergeAdapter.getSubAdapterFirstGlobalPosition(this.adapter) + i, i2);
        }

        public void onItemRangeRemoved(int i, int i2) {
            this.mergeAdapter.notifyItemRangeRemoved(this.mergeAdapter.getSubAdapterFirstGlobalPosition(this.adapter) + i, i2);
        }
    }

    /* loaded from: classes4.dex */
    public static class ChildAdapter {
        final RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter;
        private final AdapterDataObserver adapterDataObserver;
        private final SparseIntArray globalViewTypesMap = new SparseIntArray();
        private final LongSparseArray<Long> localItemIdMap = new LongSparseArray<>();
        private final SparseIntArray localViewTypesMap = new SparseIntArray();

        ChildAdapter(RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter, AdapterDataObserver adapterDataObserver) {
            this.adapter = adapter;
            this.adapterDataObserver = adapterDataObserver;
            adapter.registerAdapterDataObserver(adapterDataObserver);
        }

        int getGlobalItemViewType(int i, int i2) {
            int itemViewType = this.adapter.getItemViewType(i);
            int i3 = this.localViewTypesMap.get(itemViewType, i2);
            if (i3 == i2) {
                this.globalViewTypesMap.append(i3, itemViewType);
                this.localViewTypesMap.append(itemViewType, i3);
            }
            return i3;
        }

        long getGlobalItemId(int i, long j) {
            long itemId = this.adapter.getItemId(i);
            if (-1 == itemId) {
                return -1;
            }
            Long l = this.localItemIdMap.get(itemId);
            if (l != null) {
                return l.longValue();
            }
            this.localItemIdMap.put(itemId, Long.valueOf(j));
            return j;
        }

        void unregister() {
            this.adapter.unregisterAdapterDataObserver(this.adapterDataObserver);
        }

        RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return this.adapter.onCreateViewHolder(viewGroup, this.globalViewTypesMap.get(i));
        }
    }

    /* loaded from: classes4.dex */
    public static class ChildAdapterPositionPair {
        final ChildAdapter childAdapter;
        final int localPosition;

        ChildAdapterPositionPair(ChildAdapter childAdapter, int i) {
            this.childAdapter = childAdapter;
            this.localPosition = i;
        }

        public RecyclerView.Adapter<? extends RecyclerView.ViewHolder> getAdapter() {
            return this.childAdapter.adapter;
        }

        public int getLocalPosition() {
            return this.localPosition;
        }
    }

    public void addAdapter(RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter) {
        addAdapter(this.adapters.size(), adapter);
    }

    public void addAdapter(int i, RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter) {
        this.adapters.add(i, new ChildAdapter(adapter, new AdapterDataObserver(this, adapter)));
        notifyDataSetChanged();
    }

    public void clearAdapters() {
        for (ChildAdapter childAdapter : this.adapters) {
            childAdapter.unregister();
        }
        this.adapters.clear();
        notifyDataSetChanged();
    }

    public ChildAdapterPositionPair getLocalPosition(int i) {
        int i2 = 0;
        for (ChildAdapter childAdapter : this.adapters) {
            int itemCount = childAdapter.adapter.getItemCount() + i2;
            if (i < itemCount) {
                return new ChildAdapterPositionPair(childAdapter, i - i2);
            }
            i2 = itemCount;
        }
        throw new AssertionError("Position out of range");
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ChildAdapter childAdapter = this.viewTypes.get(i);
        if (childAdapter != null) {
            return childAdapter.onCreateViewHolder(viewGroup, i);
        }
        throw new AssertionError("Unknown view type");
    }

    public int getSubAdapterFirstGlobalPosition(RecyclerView.Adapter adapter) {
        int i = 0;
        for (ChildAdapter childAdapter : this.adapters) {
            RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter2 = childAdapter.adapter;
            if (adapter2 == adapter) {
                return i;
            }
            i += adapter2.getItemCount();
        }
        throw new AssertionError("Adapter not found in list of adapters");
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i, List<Object> list) {
        ChildAdapterPositionPair localPosition = getLocalPosition(i);
        localPosition.getAdapter().onBindViewHolder(viewHolder, localPosition.localPosition, list);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ChildAdapterPositionPair localPosition = getLocalPosition(i);
        localPosition.getAdapter().onBindViewHolder(viewHolder, localPosition.localPosition);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        int size = this.viewTypes.size();
        ChildAdapterPositionPair localPosition = getLocalPosition(i);
        int globalItemViewType = localPosition.childAdapter.getGlobalItemViewType(localPosition.localPosition, size);
        if (globalItemViewType == size) {
            this.viewTypes.add(globalItemViewType, localPosition.childAdapter);
        }
        return globalItemViewType;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        ChildAdapterPositionPair localPosition = getLocalPosition(i);
        long globalItemId = localPosition.childAdapter.getGlobalItemId(localPosition.localPosition, this.nextUnassignedItemId);
        long j = this.nextUnassignedItemId;
        if (globalItemId == j) {
            this.nextUnassignedItemId = j + 1;
        }
        return globalItemId;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        int i = 0;
        for (ChildAdapter childAdapter : this.adapters) {
            i += childAdapter.adapter.getItemCount();
        }
        return i;
    }
}
