package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public final class AppSignatureUtil {
    private static final int HASH_LENGTH_BYTES;
    private static final int HASH_LENGTH_CHARS;
    private static final String HASH_TYPE;
    private static final String TAG = Log.tag(AppSignatureUtil.class);

    private AppSignatureUtil() {
    }

    public static String getAppSignature(Context context) {
        String str = null;
        try {
            String packageName = context.getPackageName();
            Signature[] signatureArr = context.getPackageManager().getPackageInfo(packageName, 64).signatures;
            if (signatureArr.length > 0) {
                str = hash(packageName, signatureArr[0].toCharsString());
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(TAG, e);
        }
        return str != null ? str : "Unknown";
    }

    private static String hash(String str, String str2) {
        String str3 = str + " " + str2;
        try {
            MessageDigest instance = MessageDigest.getInstance(HASH_TYPE);
            instance.update(str3.getBytes(StandardCharsets.UTF_8));
            return Base64.encodeBytes(Arrays.copyOfRange(instance.digest(), 0, 9)).substring(0, 11);
        } catch (NoSuchAlgorithmException e) {
            Log.w(TAG, e);
            return null;
        }
    }
}
