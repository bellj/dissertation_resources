package org.thoughtcrime.securesms.util;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: InsetItemDecoration.kt */
@Metadata(d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"<anonymous>", "", "<anonymous parameter 0>", "Landroid/view/View;", "<anonymous parameter 1>", "Landroidx/recyclerview/widget/RecyclerView;", "invoke", "(Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;)Ljava/lang/Boolean;"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class InsetItemDecorationKt$ALWAYS_TRUE$1 extends Lambda implements Function2<View, RecyclerView, Boolean> {
    public static final InsetItemDecorationKt$ALWAYS_TRUE$1 INSTANCE = new InsetItemDecorationKt$ALWAYS_TRUE$1();

    InsetItemDecorationKt$ALWAYS_TRUE$1() {
        super(2);
    }

    public final Boolean invoke(View view, RecyclerView recyclerView) {
        Intrinsics.checkNotNullParameter(view, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(recyclerView, "<anonymous parameter 1>");
        return Boolean.TRUE;
    }
}
