package org.thoughtcrime.securesms.util;

import android.content.Context;
import j$.util.Optional;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.state.SessionRecord;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.crypto.ReentrantSessionLock;
import org.thoughtcrime.securesms.crypto.storage.SignalIdentityKeyStore;
import org.thoughtcrime.securesms.crypto.storage.SignalServiceAccountDataStoreImpl;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.IncomingIdentityDefaultMessage;
import org.thoughtcrime.securesms.sms.IncomingIdentityUpdateMessage;
import org.thoughtcrime.securesms.sms.IncomingIdentityVerifiedMessage;
import org.thoughtcrime.securesms.sms.IncomingTextMessage;
import org.thoughtcrime.securesms.sms.OutgoingIdentityDefaultMessage;
import org.thoughtcrime.securesms.sms.OutgoingIdentityVerifiedMessage;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;
import org.whispersystems.signalservice.api.SignalSessionLock;
import org.whispersystems.signalservice.api.messages.multidevice.VerifiedMessage;

/* loaded from: classes4.dex */
public final class IdentityUtil {
    private static final String TAG = Log.tag(IdentityUtil.class);

    private IdentityUtil() {
    }

    public static ListenableFuture<Optional<IdentityRecord>> getRemoteIdentityKey(Context context, Recipient recipient) {
        SettableFuture settableFuture = new SettableFuture();
        SimpleTask.run(SignalExecutors.BOUNDED, new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.util.IdentityUtil$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return IdentityUtil.lambda$getRemoteIdentityKey$0(RecipientId.this);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.util.IdentityUtil$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                SettableFuture.this.set((Optional) obj);
            }
        });
        return settableFuture;
    }

    public static /* synthetic */ Optional lambda$getRemoteIdentityKey$0(RecipientId recipientId) {
        return ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecord(recipientId);
    }

    public static void markIdentityVerified(Context context, Recipient recipient, boolean z, boolean z2) {
        OutgoingTextMessage outgoingTextMessage;
        IncomingTextMessage incomingTextMessage;
        IncomingTextMessage incomingTextMessage2;
        OutgoingTextMessage outgoingTextMessage2;
        Recipient recipient2 = recipient;
        long currentTimeMillis = System.currentTimeMillis();
        SmsDatabase sms = SignalDatabase.sms();
        GroupDatabase.Reader groups = SignalDatabase.groups().getGroups();
        while (true) {
            try {
                GroupDatabase.GroupRecord next = groups.getNext();
                if (next == null) {
                    break;
                } else if (next.getMembers().contains(recipient.getId()) && next.isActive() && !next.isMms()) {
                    if (z2) {
                        IncomingTextMessage incomingTextMessage3 = new IncomingTextMessage(recipient.getId(), 1, currentTimeMillis, -1, currentTimeMillis, null, Optional.of(next.getId()), 0, false, null);
                        if (z) {
                            incomingTextMessage2 = new IncomingIdentityVerifiedMessage(incomingTextMessage3);
                        } else {
                            incomingTextMessage2 = new IncomingIdentityDefaultMessage(incomingTextMessage3);
                        }
                        sms.insertMessageInbox(incomingTextMessage2);
                        sms = sms;
                        recipient2 = recipient;
                    } else {
                        long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(Recipient.resolved(SignalDatabase.recipients().getOrInsertFromGroupId(next.getId())));
                        if (z) {
                            sms = sms;
                            recipient2 = recipient;
                            outgoingTextMessage2 = new OutgoingIdentityVerifiedMessage(recipient2);
                        } else {
                            sms = sms;
                            recipient2 = recipient;
                            outgoingTextMessage2 = new OutgoingIdentityDefaultMessage(recipient2);
                        }
                        SignalDatabase.sms().insertMessageOutbox(orCreateThreadIdFor, outgoingTextMessage2, false, currentTimeMillis, (MessageDatabase.InsertListener) null);
                        SignalDatabase.threads().update(orCreateThreadIdFor, true);
                    }
                }
            } catch (Throwable th) {
                if (groups != null) {
                    try {
                        groups.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        groups.close();
        if (z2) {
            IncomingTextMessage incomingTextMessage4 = new IncomingTextMessage(recipient.getId(), 1, currentTimeMillis, -1, currentTimeMillis, null, Optional.empty(), 0, false, null);
            if (z) {
                incomingTextMessage = new IncomingIdentityVerifiedMessage(incomingTextMessage4);
            } else {
                incomingTextMessage = new IncomingIdentityDefaultMessage(incomingTextMessage4);
            }
            sms.insertMessageInbox(incomingTextMessage);
            return;
        }
        if (z) {
            outgoingTextMessage = new OutgoingIdentityVerifiedMessage(recipient2);
        } else {
            outgoingTextMessage = new OutgoingIdentityDefaultMessage(recipient2);
        }
        long orCreateThreadIdFor2 = SignalDatabase.threads().getOrCreateThreadIdFor(recipient2);
        Log.i(TAG, "Inserting verified outbox...");
        SignalDatabase.sms().insertMessageOutbox(orCreateThreadIdFor2, outgoingTextMessage, false, currentTimeMillis, (MessageDatabase.InsertListener) null);
        SignalDatabase.threads().update(orCreateThreadIdFor2, true);
    }

    public static void markIdentityUpdate(Context context, RecipientId recipientId) {
        RecipientId recipientId2 = recipientId;
        String str = TAG;
        Log.w(str, "Inserting safety number change event(s) for " + recipientId2, new Throwable());
        long currentTimeMillis = System.currentTimeMillis();
        SmsDatabase sms = SignalDatabase.sms();
        GroupDatabase.Reader groups = SignalDatabase.groups().getGroups();
        while (true) {
            try {
                GroupDatabase.GroupRecord next = groups.getNext();
                if (next == null) {
                    break;
                }
                if (next.getMembers().contains(recipientId2) && next.isActive()) {
                    sms.insertMessageInbox(new IncomingIdentityUpdateMessage(new IncomingTextMessage(recipientId, 1, currentTimeMillis, currentTimeMillis, currentTimeMillis, null, Optional.of(next.getId()), 0, false, null)));
                    sms = sms;
                }
                recipientId2 = recipientId;
            } catch (Throwable th) {
                if (groups != null) {
                    try {
                        groups.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        groups.close();
        Optional<MessageDatabase.InsertResult> insertMessageInbox = sms.insertMessageInbox(new IncomingIdentityUpdateMessage(new IncomingTextMessage(recipientId, 1, currentTimeMillis, -1, currentTimeMillis, null, Optional.empty(), 0, false, null)));
        if (insertMessageInbox.isPresent()) {
            ApplicationDependencies.getMessageNotifier().updateNotification(context, ConversationId.forConversation(insertMessageInbox.get().getThreadId()));
        }
    }

    public static void saveIdentity(String str, IdentityKey identityKey) {
        SignalSessionLock.Lock acquire = ReentrantSessionLock.INSTANCE.acquire();
        try {
            SignalServiceAccountDataStoreImpl aci = ApplicationDependencies.getProtocolStore().aci();
            SignalProtocolAddress signalProtocolAddress = new SignalProtocolAddress(str, 1);
            if (ApplicationDependencies.getProtocolStore().aci().identities().saveIdentity(signalProtocolAddress, identityKey) && aci.containsSession(signalProtocolAddress)) {
                SessionRecord loadSession = aci.loadSession(signalProtocolAddress);
                loadSession.archiveCurrentState();
                aci.storeSession(signalProtocolAddress, loadSession);
            }
            if (acquire != null) {
                acquire.close();
            }
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void processVerifiedMessage(Context context, VerifiedMessage verifiedMessage) {
        IdentityDatabase.VerifiedStatus verifiedStatus;
        SignalSessionLock.Lock acquire = ReentrantSessionLock.INSTANCE.acquire();
        try {
            SignalIdentityKeyStore identities = ApplicationDependencies.getProtocolStore().aci().identities();
            Recipient externalPush = Recipient.externalPush(verifiedMessage.getDestination());
            if (externalPush.isSelf()) {
                String str = TAG;
                Log.w(str, "Attempting to change verified status of self to " + verifiedMessage.getVerified() + ", skipping.");
                if (acquire != null) {
                    acquire.close();
                    return;
                }
                return;
            }
            Optional<IdentityRecord> identityRecord = identities.getIdentityRecord(externalPush.getId());
            if (identityRecord.isPresent() || verifiedMessage.getVerified() != VerifiedMessage.VerifiedState.DEFAULT) {
                if (verifiedMessage.getVerified() == VerifiedMessage.VerifiedState.DEFAULT && identityRecord.isPresent() && identityRecord.get().getIdentityKey().equals(verifiedMessage.getIdentityKey()) && identityRecord.get().getVerifiedStatus() != (verifiedStatus = IdentityDatabase.VerifiedStatus.DEFAULT)) {
                    String str2 = TAG;
                    Log.i(str2, "Setting " + externalPush.getId() + " verified status to " + verifiedStatus);
                    identities.setVerified(externalPush.getId(), identityRecord.get().getIdentityKey(), verifiedStatus);
                    markIdentityVerified(context, externalPush, false, true);
                }
                if (verifiedMessage.getVerified() == VerifiedMessage.VerifiedState.VERIFIED && (!identityRecord.isPresent() || ((identityRecord.isPresent() && !identityRecord.get().getIdentityKey().equals(verifiedMessage.getIdentityKey())) || (identityRecord.isPresent() && identityRecord.get().getVerifiedStatus() != IdentityDatabase.VerifiedStatus.VERIFIED)))) {
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Setting ");
                    sb.append(externalPush.getId());
                    sb.append(" verified status to ");
                    IdentityDatabase.VerifiedStatus verifiedStatus2 = IdentityDatabase.VerifiedStatus.VERIFIED;
                    sb.append(verifiedStatus2);
                    Log.i(str3, sb.toString());
                    saveIdentity(verifiedMessage.getDestination().getIdentifier(), verifiedMessage.getIdentityKey());
                    identities.setVerified(externalPush.getId(), verifiedMessage.getIdentityKey(), verifiedStatus2);
                    markIdentityVerified(context, externalPush, true, true);
                }
                if (acquire != null) {
                    acquire.close();
                    return;
                }
                return;
            }
            Log.w(TAG, "No existing record for default status");
            if (acquire != null) {
                acquire.close();
            }
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static String getUnverifiedBannerDescription(Context context, List<Recipient> list) {
        return getPluralizedIdentityDescription(context, list, R.string.IdentityUtil_unverified_banner_one, R.string.IdentityUtil_unverified_banner_two, R.string.IdentityUtil_unverified_banner_many);
    }

    public static String getUnverifiedSendDialogDescription(Context context, List<Recipient> list) {
        return getPluralizedIdentityDescription(context, list, R.string.IdentityUtil_unverified_dialog_one, R.string.IdentityUtil_unverified_dialog_two, R.string.IdentityUtil_unverified_dialog_many);
    }

    public static String getUntrustedSendDialogDescription(Context context, List<Recipient> list) {
        return getPluralizedIdentityDescription(context, list, R.string.IdentityUtil_untrusted_dialog_one, R.string.IdentityUtil_untrusted_dialog_two, R.string.IdentityUtil_untrusted_dialog_many);
    }

    private static String getPluralizedIdentityDescription(Context context, List<Recipient> list, int i, int i2, int i3) {
        if (list.isEmpty()) {
            return null;
        }
        if (list.size() == 1) {
            return context.getString(i, list.get(0).getDisplayName(context));
        }
        String displayName = list.get(0).getDisplayName(context);
        String displayName2 = list.get(1).getDisplayName(context);
        if (list.size() == 2) {
            return context.getString(i2, displayName, displayName2);
        }
        int size = list.size() - 2;
        return context.getString(i3, displayName, displayName2, context.getResources().getQuantityString(R.plurals.identity_others, size, Integer.valueOf(size)));
    }
}
