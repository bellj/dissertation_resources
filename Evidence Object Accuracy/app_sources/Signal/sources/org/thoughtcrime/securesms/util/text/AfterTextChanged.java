package org.thoughtcrime.securesms.util.text;

import android.text.Editable;
import android.text.TextWatcher;
import androidx.core.util.Consumer;

/* loaded from: classes4.dex */
public final class AfterTextChanged implements TextWatcher {
    private final Consumer<Editable> afterTextChangedConsumer;

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public AfterTextChanged(Consumer<Editable> consumer) {
        this.afterTextChangedConsumer = consumer;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        this.afterTextChangedConsumer.accept(editable);
    }
}
