package org.thoughtcrime.securesms.util.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/* loaded from: classes4.dex */
public interface ListenableFuture<T> extends Future<T> {

    /* loaded from: classes4.dex */
    public interface Listener<T> {
        void onFailure(ExecutionException executionException);

        void onSuccess(T t);
    }

    void addListener(Listener<T> listener);
}
