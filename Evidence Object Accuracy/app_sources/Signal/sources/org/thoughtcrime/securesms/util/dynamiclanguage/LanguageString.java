package org.thoughtcrime.securesms.util.dynamiclanguage;

import java.util.Locale;

/* loaded from: classes.dex */
public final class LanguageString {
    private LanguageString() {
    }

    public static Locale parseLocale(String str) {
        if (str == null || str.isEmpty()) {
            return null;
        }
        Locale createLocale = createLocale(str);
        if (!isValid(createLocale)) {
            return null;
        }
        return createLocale;
    }

    private static Locale createLocale(String str) {
        String[] split = str.split("_");
        if (split.length == 2) {
            return new Locale(split[0], split[1]);
        }
        return new Locale(split[0]);
    }

    private static boolean isValid(Locale locale) {
        try {
            if (locale.getISO3Language() != null) {
                return locale.getISO3Country() != null;
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }
}
