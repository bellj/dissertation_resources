package org.thoughtcrime.securesms.util.adapter;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes4.dex */
public final class FixedViewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private boolean hidden;
    private final List<View> viewList;

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
    }

    public FixedViewsAdapter(View... viewArr) {
        this.viewList = Arrays.asList(viewArr);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        if (this.hidden) {
            return 0;
        }
        return this.viewList.size();
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new RecyclerView.ViewHolder(this.viewList.get(i)) { // from class: org.thoughtcrime.securesms.util.adapter.FixedViewsAdapter.1
        };
    }

    public void hide() {
        setHidden(true);
    }

    public void show() {
        setHidden(false);
    }

    private void setHidden(boolean z) {
        if (this.hidden != z) {
            this.hidden = z;
            if (z) {
                notifyItemRangeRemoved(0, this.viewList.size());
            } else {
                notifyItemRangeInserted(0, this.viewList.size());
            }
        }
    }
}
