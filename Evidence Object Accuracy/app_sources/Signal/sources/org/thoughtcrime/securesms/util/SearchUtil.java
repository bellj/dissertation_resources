package org.thoughtcrime.securesms.util;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.CharacterStyle;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import org.signal.libsignal.protocol.util.Pair;

/* loaded from: classes4.dex */
public class SearchUtil {
    public static final int MATCH_ALL;
    public static final int STRICT;

    /* loaded from: classes4.dex */
    public interface StyleFactory {
        CharacterStyle create();
    }

    public static Spannable getHighlightedSpan(Locale locale, StyleFactory styleFactory, String str, String str2, int i) {
        if (TextUtils.isEmpty(str)) {
            return new SpannableString("");
        }
        return getHighlightedSpan(locale, styleFactory, new SpannableString(str.replaceAll("\n", " ")), str2, i);
    }

    public static Spannable getHighlightedSpan(Locale locale, StyleFactory styleFactory, Spannable spannable, String str, int i) {
        List<Pair<Integer, Integer>> list;
        if (TextUtils.isEmpty(spannable)) {
            return new SpannableString("");
        }
        if (TextUtils.isEmpty(str)) {
            return spannable;
        }
        SpannableString spannableString = new SpannableString(spannable);
        if (i == 0) {
            list = getStrictHighlightRanges(locale, spannable.toString(), str);
        } else if (i == 1) {
            list = getHighlightRanges(locale, spannable.toString(), str);
        } else {
            throw new InvalidParameterException("match mode must be STRICT or MATCH_ALL: " + i);
        }
        for (Pair<Integer, Integer> pair : list) {
            spannableString.setSpan(styleFactory.create(), pair.first().intValue(), pair.second().intValue(), 17);
        }
        return spannableString;
    }

    static List<Pair<Integer, Integer>> getStrictHighlightRanges(Locale locale, String str, String str2) {
        int indexOf;
        int length;
        if (str.length() == 0) {
            return Collections.emptyList();
        }
        String lowerCase = str.toLowerCase(locale);
        List<String> list = Stream.of(str2.toLowerCase(locale).split("\\s")).filter(new Predicate() { // from class: org.thoughtcrime.securesms.util.SearchUtil$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return SearchUtil.lambda$getStrictHighlightRanges$0((String) obj);
            }
        }).toList();
        LinkedList linkedList = new LinkedList();
        int i = 0;
        for (String str3 : list) {
            while (true) {
                indexOf = lowerCase.indexOf(str3, i);
                length = str3.length() + indexOf;
                if (indexOf <= 0 || Character.isWhitespace(lowerCase.charAt(indexOf - 1))) {
                    break;
                }
                i = length;
            }
            if (indexOf >= 0) {
                linkedList.add(new Pair(Integer.valueOf(indexOf), Integer.valueOf(length)));
            }
            if (indexOf < 0 || length >= lowerCase.length()) {
                break;
            }
            i = length;
        }
        return linkedList.size() != list.size() ? Collections.emptyList() : linkedList;
    }

    public static /* synthetic */ boolean lambda$getStrictHighlightRanges$0(String str) {
        return str.trim().length() > 0;
    }

    static List<Pair<Integer, Integer>> getHighlightRanges(Locale locale, String str, String str2) {
        if (str.length() == 0) {
            return Collections.emptyList();
        }
        String lowerCase = str.toLowerCase(locale);
        List<String> list = Stream.of(str2.toLowerCase(locale).split("\\s")).filter(new Predicate() { // from class: org.thoughtcrime.securesms.util.SearchUtil$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return SearchUtil.lambda$getHighlightRanges$1((String) obj);
            }
        }).toList();
        LinkedList linkedList = new LinkedList();
        for (String str3 : list) {
            int i = 0;
            int i2 = 0;
            while (i != -1) {
                i = lowerCase.indexOf(str3, i2);
                if (i != -1) {
                    i2 = str3.length() + i;
                    linkedList.add(new Pair(Integer.valueOf(i), Integer.valueOf(i2)));
                    i = i2;
                }
            }
        }
        return linkedList;
    }

    public static /* synthetic */ boolean lambda$getHighlightRanges$1(String str) {
        return str.trim().length() > 0;
    }
}
