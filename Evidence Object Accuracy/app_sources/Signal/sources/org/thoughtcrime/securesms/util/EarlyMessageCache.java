package org.thoughtcrime.securesms.util;

import j$.util.Optional;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.thoughtcrime.securesms.database.model.ServiceMessageId;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.messages.SignalServiceContent;

/* loaded from: classes4.dex */
public final class EarlyMessageCache {
    private final LRUCache<ServiceMessageId, List<SignalServiceContent>> cache = new LRUCache<>(100);

    public synchronized void store(RecipientId recipientId, long j, SignalServiceContent signalServiceContent) {
        ServiceMessageId serviceMessageId = new ServiceMessageId(recipientId, j);
        List<SignalServiceContent> list = this.cache.get(serviceMessageId);
        if (list == null) {
            list = new LinkedList<>();
        }
        list.add(signalServiceContent);
        this.cache.put(serviceMessageId, list);
    }

    public synchronized Optional<List<SignalServiceContent>> retrieve(RecipientId recipientId, long j) {
        return Optional.ofNullable(this.cache.remove(new ServiceMessageId(recipientId, j)));
    }

    public synchronized Collection<ServiceMessageId> getAllReferencedIds() {
        return new HashSet(this.cache.keySet());
    }
}
