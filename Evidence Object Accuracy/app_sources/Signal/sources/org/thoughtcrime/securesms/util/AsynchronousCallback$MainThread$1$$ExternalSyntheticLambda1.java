package org.thoughtcrime.securesms.util;

import org.thoughtcrime.securesms.util.AsynchronousCallback;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AsynchronousCallback$MainThread$1$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ AsynchronousCallback.MainThread.AnonymousClass1 f$0;
    public final /* synthetic */ Object f$1;

    public /* synthetic */ AsynchronousCallback$MainThread$1$$ExternalSyntheticLambda1(AsynchronousCallback.MainThread.AnonymousClass1 r1, Object obj) {
        this.f$0 = r1;
        this.f$1 = obj;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onComplete$0(this.f$1);
    }
}
