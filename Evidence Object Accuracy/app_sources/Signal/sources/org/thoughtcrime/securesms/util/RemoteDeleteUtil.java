package org.thoughtcrime.securesms.util;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class RemoteDeleteUtil {
    private static final long RECEIVE_THRESHOLD = TimeUnit.DAYS.toMillis(1);
    private static final long SEND_THRESHOLD = TimeUnit.HOURS.toMillis(3);

    private RemoteDeleteUtil() {
    }

    public static boolean isValidReceive(MessageRecord messageRecord, Recipient recipient, long j) {
        long j2;
        boolean z = (recipient.isSelf() && messageRecord.isOutgoing()) || (!recipient.isSelf() && !messageRecord.isOutgoing());
        boolean z2 = messageRecord.getIndividualRecipient().equals(recipient) || (recipient.isSelf() && messageRecord.isOutgoing());
        if (!recipient.isSelf() || !messageRecord.isOutgoing()) {
            j2 = messageRecord.getServerTimestamp();
        } else {
            j2 = messageRecord.getDateSent();
        }
        if (!z || !z2 || j - j2 >= RECEIVE_THRESHOLD) {
            return false;
        }
        return true;
    }

    public static boolean isValidSend(Collection<MessageRecord> collection, long j) {
        return Stream.of(collection).allMatch(new Predicate(j) { // from class: org.thoughtcrime.securesms.util.RemoteDeleteUtil$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return RemoteDeleteUtil.isValidSend((MessageRecord) obj, this.f$0);
            }
        });
    }

    public static boolean isValidSend(MessageRecord messageRecord, long j) {
        return !messageRecord.isUpdate() && messageRecord.isOutgoing() && messageRecord.isPush() && (!messageRecord.getRecipient().isGroup() || messageRecord.getRecipient().isActiveGroup()) && !messageRecord.getRecipient().isSelf() && !messageRecord.isRemoteDelete() && !MessageRecordUtil.hasGiftBadge(messageRecord) && j - messageRecord.getDateSent() < SEND_THRESHOLD;
    }
}
