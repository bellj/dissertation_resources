package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import androidx.preference.PreferenceManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.backup.BackupProtos;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.impl.SqlCipherMigrationConstraintObserver;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.RegistrationLockReminders;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.preferences.widgets.NotificationPrivacyPreference;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes.dex */
public class TextSecurePreferences {
    public static final String ALL_MMS_PREF;
    public static final String ALL_SMS_PREF;
    public static final String ALWAYS_RELAY_CALLS_PREF;
    private static final String APP_MIGRATION_VERSION;
    private static final String ARGON2_TESTED;
    private static final String ATTACHMENT_ENCRYPTED_SECRET;
    private static final String ATTACHMENT_UNENCRYPTED_SECRET;
    public static final String BACKUP;
    public static final String BACKUP_ENABLED;
    private static final String BACKUP_PASSPHRASE;
    private static final String BACKUP_TIME;
    public static final String CALL_BANDWIDTH_PREF;
    public static final String CALL_NOTIFICATIONS_PREF;
    public static final String CALL_RINGTONE_PREF;
    public static final String CALL_VIBRATE_PREF;
    public static final String CHANGE_PASSPHRASE_PREF;
    private static final String DATABASE_ENCRYPTED_SECRET;
    private static final String DATABASE_UNENCRYPTED_SECRET;
    private static final String DIRECTORY_FRESH_TIME_PREF;
    public static final String DIRECT_CAPTURE_CAMERA_ID;
    public static final String DISABLE_PASSPHRASE_PREF;
    public static final String ENABLE_MANUAL_MMS_PREF;
    private static final String ENCRYPTED_BACKUP_PASSPHRASE;
    private static final String ENTER_PRESENT_PREF;
    private static final String ENTER_SENDS_PREF;
    private static final String FIRST_INSTALL_VERSION;
    private static final String HAS_SEEN_SWIPE_TO_REPLY;
    private static final String HAS_SEEN_VIDEO_RECORDING_TOOLTIP;
    public static final String INCOGNITO_KEYBORAD_PREF;
    private static final String IN_THREAD_NOTIFICATION_PREF;
    private static final String JOB_MANAGER_VERSION;
    public static final String LANGUAGE_PREF;
    private static final String LAST_FULL_CONTACT_SYNC_TIME;
    private static final String LAST_OUTAGE_CHECK_TIME;
    private static final String LAST_VERSION_CODE_PREF;
    public static final String LED_BLINK_PREF;
    private static final String LED_BLINK_PREF_CUSTOM;
    public static final String LED_COLOR_PREF;
    public static final String LINK_PREVIEWS;
    private static final String LOG_ENCRYPTED_SECRET;
    private static final String LOG_UNENCRYPTED_SECRET;
    public static final String MEDIA_DOWNLOAD_MOBILE_PREF;
    public static final String MEDIA_DOWNLOAD_ROAMING_PREF;
    public static final String MEDIA_DOWNLOAD_WIFI_PREF;
    private static final String MEDIA_KEYBOARD_MODE;
    public static final String MESSAGE_BODY_TEXT_SIZE_PREF;
    private static final String MMSC_CUSTOM_HOST_PREF;
    private static final String MMSC_CUSTOM_PASSWORD_PREF;
    private static final String MMSC_CUSTOM_PROXY_PORT_PREF;
    private static final String MMSC_CUSTOM_PROXY_PREF;
    private static final String MMSC_CUSTOM_USERNAME_PREF;
    public static final String MMSC_HOST_PREF;
    public static final String MMSC_PASSWORD_PREF;
    public static final String MMSC_PROXY_HOST_PREF;
    public static final String MMSC_PROXY_PORT_PREF;
    public static final String MMSC_USERNAME_PREF;
    private static final String MMS_CUSTOM_USER_AGENT;
    public static final String MMS_USER_AGENT;
    private static final String MULTI_DEVICE_PROVISIONED_PREF;
    private static final String NEEDS_FULL_CONTACT_SYNC;
    private static final String NEEDS_MESSAGE_PULL;
    private static final String NEEDS_SQLCIPHER_MIGRATION;
    public static final String NEW_CONTACTS_NOTIFICATIONS;
    private static final String NOTIFICATION_CHANNEL_VERSION;
    private static final String NOTIFICATION_MESSAGES_CHANNEL_VERSION;
    private static final String NOTIFICATION_PREF;
    public static final String NOTIFICATION_PRIORITY_PREF;
    public static final String NOTIFICATION_PRIVACY_PREF;
    public static final String PASSPHRASE_TIMEOUT_INTERVAL_PREF;
    public static final String PASSPHRASE_TIMEOUT_PREF;
    private static final String PROMPTED_OPTIMIZE_DOZE_PREF;
    private static final String PROMPTED_PUSH_REGISTRATION_PREF;
    private static final String RATING_ENABLED_PREF;
    private static final String RATING_LATER_PREF;
    public static final String READ_RECEIPTS_PREF;
    public static final String RECENT_STORAGE_KEY;
    private static final String REGISTRATION_LOCK_LAST_REMINDER_TIME_POST_KBS;
    private static final String REGISTRATION_LOCK_NEXT_REMINDER_INTERVAL;
    @Deprecated
    private static final String REGISTRATION_LOCK_PIN_PREF_V1;
    @Deprecated
    public static final String REGISTRATION_LOCK_PREF_V1;
    public static final String REGISTRATION_LOCK_PREF_V2;
    public static final String REPEAT_ALERTS_PREF;
    public static final String RINGTONE_PREF;
    public static final String SCREEN_LOCK;
    public static final String SCREEN_LOCK_TIMEOUT;
    public static final String SCREEN_SECURITY_PREF;
    private static final String SEEN_CAMERA_FIRST_TOOLTIP;
    private static final String SEEN_STICKER_INTRO_TOOLTIP;
    private static final String SEEN_WELCOME_SCREEN_PREF;
    private static final String SERVICE_OUTAGE;
    private static final String SHOW_INVITE_REMINDER_PREF;
    public static final String SHOW_UNIDENTIFIED_DELIVERY_INDICATORS;
    public static final String SIGNAL_PIN_CHANGE;
    private static final String SIGNED_PREKEY_ROTATION_TIME_PREF;
    private static final String SMS_DELIVERY_REPORT_PREF;
    private static final String STORAGE_MANIFEST_VERSION;
    private static final String SUCCESSFUL_DIRECTORY_PREF;
    public static final String SYSTEM_EMOJI_PREF;
    private static final String TAG = Log.tag(TextSecurePreferences.class);
    public static final String THEME_PREF;
    public static final String TRANSFER;
    public static final String TYPING_INDICATORS;
    private static final String UNAUTHORIZED_RECEIVED;
    private static final String UNIDENTIFIED_ACCESS_CERTIFICATE_ROTATION_TIME_PREF;
    private static final String UNIDENTIFIED_DELIVERY_ENABLED;
    public static final String UNIVERSAL_UNIDENTIFIED_ACCESS;
    private static final String UPDATE_APK_DIGEST;
    private static final String UPDATE_APK_DOWNLOAD_ID;
    private static final String UPDATE_APK_REFRESH_TIME_PREF;
    public static final String VIBRATE_PREF;
    private static final String VIEW_ONCE_TOOLTIP_SEEN;
    public static final String WEBRTC_CALLING_PREF;
    private static final String WIFI_SMS_PREF;
    private static final String[] booleanPreferencesToBackup = {SCREEN_SECURITY_PREF, INCOGNITO_KEYBORAD_PREF, ALWAYS_RELAY_CALLS_PREF, READ_RECEIPTS_PREF, TYPING_INDICATORS, SHOW_UNIDENTIFIED_DELIVERY_INDICATORS, UNIVERSAL_UNIDENTIFIED_ACCESS, NOTIFICATION_PREF, VIBRATE_PREF, IN_THREAD_NOTIFICATION_PREF, CALL_NOTIFICATIONS_PREF, CALL_VIBRATE_PREF, NEW_CONTACTS_NOTIFICATIONS, SHOW_INVITE_REMINDER_PREF, SYSTEM_EMOJI_PREF, ENTER_SENDS_PREF};
    private static volatile SharedPreferences preferences = null;
    private static final String[] stringPreferencesToBackup = {LED_COLOR_PREF, LED_BLINK_PREF, REPEAT_ALERTS_PREF, NOTIFICATION_PRIVACY_PREF, THEME_PREF, LANGUAGE_PREF, MESSAGE_BODY_TEXT_SIZE_PREF};
    private static final String[] stringSetPreferencesToBackup = {MEDIA_DOWNLOAD_MOBILE_PREF, MEDIA_DOWNLOAD_WIFI_PREF, MEDIA_DOWNLOAD_ROAMING_PREF};

    /* loaded from: classes.dex */
    public enum MediaKeyboardMode {
        EMOJI,
        STICKER,
        GIF
    }

    public static long getPreferencesToSaveToBackupCount(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        long j = 0;
        for (String str : booleanPreferencesToBackup) {
            if (sharedPreferences.contains(str)) {
                j++;
            }
        }
        for (String str2 : stringPreferencesToBackup) {
            if (sharedPreferences.contains(str2)) {
                j++;
            }
        }
        for (String str3 : stringSetPreferencesToBackup) {
            if (sharedPreferences.contains(str3)) {
                j++;
            }
        }
        return j;
    }

    public static List<BackupProtos.SharedPreference> getPreferencesToSaveToBackup(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        ArrayList arrayList = new ArrayList();
        String str = context.getPackageName() + "_preferences";
        String[] strArr = booleanPreferencesToBackup;
        for (String str2 : strArr) {
            if (sharedPreferences.contains(str2)) {
                arrayList.add(BackupProtos.SharedPreference.newBuilder().setFile(str).setKey(str2).setBooleanValue(sharedPreferences.getBoolean(str2, false)).build());
            }
        }
        String[] strArr2 = stringPreferencesToBackup;
        for (String str3 : strArr2) {
            if (sharedPreferences.contains(str3)) {
                arrayList.add(BackupProtos.SharedPreference.newBuilder().setFile(str).setKey(str3).setValue(sharedPreferences.getString(str3, null)).build());
            }
        }
        String[] strArr3 = stringSetPreferencesToBackup;
        for (String str4 : strArr3) {
            if (sharedPreferences.contains(str4)) {
                arrayList.add(BackupProtos.SharedPreference.newBuilder().setFile(str).setKey(str4).setIsStringSetValue(true).addAllStringSetValue(sharedPreferences.getStringSet(str4, Collections.emptySet())).build());
            }
        }
        return arrayList;
    }

    public static void onPostBackupRestore(Context context) {
        if (NotificationChannels.supported()) {
            NotificationChannels.updateMessageVibrate(context, SignalStore.settings().isMessageVibrateEnabled());
        }
    }

    public static boolean isScreenLockEnabled(Context context) {
        return getBooleanPreference(context, SCREEN_LOCK, false);
    }

    public static void setScreenLockEnabled(Context context, boolean z) {
        setBooleanPreference(context, SCREEN_LOCK, z);
    }

    public static long getScreenLockTimeout(Context context) {
        return getLongPreference(context, SCREEN_LOCK_TIMEOUT, 0);
    }

    public static void setScreenLockTimeout(Context context, long j) {
        setLongPreference(context, SCREEN_LOCK_TIMEOUT, j);
    }

    public static boolean isV1RegistrationLockEnabled(Context context) {
        return getBooleanPreference(context, REGISTRATION_LOCK_PREF_V1, false);
    }

    @Deprecated
    public static void setV1RegistrationLockEnabled(Context context, boolean z) {
        setBooleanPreference(context, REGISTRATION_LOCK_PREF_V1, z);
    }

    @Deprecated
    public static String getDeprecatedV1RegistrationLockPin(Context context) {
        return getStringPreference(context, REGISTRATION_LOCK_PIN_PREF_V1, null);
    }

    public static void clearRegistrationLockV1(Context context) {
        getSharedPreferences(context).edit().remove(REGISTRATION_LOCK_PIN_PREF_V1).apply();
    }

    @Deprecated
    public static void setV1RegistrationLockPin(Context context, String str) {
        setStringPreference(context, REGISTRATION_LOCK_PIN_PREF_V1, str);
    }

    public static long getRegistrationLockLastReminderTime(Context context) {
        return getLongPreference(context, REGISTRATION_LOCK_LAST_REMINDER_TIME_POST_KBS, 0);
    }

    public static void setRegistrationLockLastReminderTime(Context context, long j) {
        setLongPreference(context, REGISTRATION_LOCK_LAST_REMINDER_TIME_POST_KBS, j);
    }

    public static long getRegistrationLockNextReminderInterval(Context context) {
        return getLongPreference(context, REGISTRATION_LOCK_NEXT_REMINDER_INTERVAL, RegistrationLockReminders.INITIAL_INTERVAL);
    }

    public static void setRegistrationLockNextReminderInterval(Context context, long j) {
        setLongPreference(context, REGISTRATION_LOCK_NEXT_REMINDER_INTERVAL, j);
    }

    public static void setBackupPassphrase(Context context, String str) {
        setStringPreference(context, BACKUP_PASSPHRASE, str);
    }

    public static String getBackupPassphrase(Context context) {
        return getStringPreference(context, BACKUP_PASSPHRASE, null);
    }

    public static void setEncryptedBackupPassphrase(Context context, String str) {
        setStringPreference(context, ENCRYPTED_BACKUP_PASSPHRASE, str);
    }

    public static String getEncryptedBackupPassphrase(Context context) {
        return getStringPreference(context, ENCRYPTED_BACKUP_PASSPHRASE, null);
    }

    @Deprecated
    public static boolean isBackupEnabled(Context context) {
        return getBooleanPreference(context, BACKUP_ENABLED, false);
    }

    public static void setNextBackupTime(Context context, long j) {
        setLongPreference(context, BACKUP_TIME, j);
    }

    public static long getNextBackupTime(Context context) {
        return getLongPreference(context, BACKUP_TIME, -1);
    }

    public static void setNeedsSqlCipherMigration(Context context, boolean z) {
        setBooleanPreference(context, NEEDS_SQLCIPHER_MIGRATION, z);
        EventBus.getDefault().post(new SqlCipherMigrationConstraintObserver.SqlCipherNeedsMigrationEvent());
    }

    public static boolean getNeedsSqlCipherMigration(Context context) {
        return getBooleanPreference(context, NEEDS_SQLCIPHER_MIGRATION, false);
    }

    public static void setAttachmentEncryptedSecret(Context context, String str) {
        setStringPreference(context, ATTACHMENT_ENCRYPTED_SECRET, str);
    }

    public static void setAttachmentUnencryptedSecret(Context context, String str) {
        setStringPreference(context, ATTACHMENT_UNENCRYPTED_SECRET, str);
    }

    public static String getAttachmentEncryptedSecret(Context context) {
        return getStringPreference(context, ATTACHMENT_ENCRYPTED_SECRET, null);
    }

    public static String getAttachmentUnencryptedSecret(Context context) {
        return getStringPreference(context, ATTACHMENT_UNENCRYPTED_SECRET, null);
    }

    public static void setDatabaseEncryptedSecret(Context context, String str) {
        setStringPreference(context, DATABASE_ENCRYPTED_SECRET, str);
    }

    public static void setDatabaseUnencryptedSecret(Context context, String str) {
        setStringPreference(context, DATABASE_UNENCRYPTED_SECRET, str);
    }

    public static String getDatabaseUnencryptedSecret(Context context) {
        return getStringPreference(context, DATABASE_UNENCRYPTED_SECRET, null);
    }

    public static String getDatabaseEncryptedSecret(Context context) {
        return getStringPreference(context, DATABASE_ENCRYPTED_SECRET, null);
    }

    public static void setHasSuccessfullyRetrievedDirectory(Context context, boolean z) {
        setBooleanPreference(context, SUCCESSFUL_DIRECTORY_PREF, z);
    }

    public static boolean hasSuccessfullyRetrievedDirectory(Context context) {
        return getBooleanPreference(context, SUCCESSFUL_DIRECTORY_PREF, false);
    }

    public static void setUnauthorizedReceived(Context context, boolean z) {
        boolean isUnauthorizedRecieved = isUnauthorizedRecieved(context);
        setBooleanPreference(context, UNAUTHORIZED_RECEIVED, z);
        if (isUnauthorizedRecieved != z) {
            Recipient.self().live().refresh();
        }
        if (z) {
            clearLocalCredentials(context);
        }
    }

    public static boolean isUnauthorizedRecieved(Context context) {
        return getBooleanPreference(context, UNAUTHORIZED_RECEIVED, false);
    }

    public static boolean isIncognitoKeyboardEnabled(Context context) {
        return getBooleanPreference(context, INCOGNITO_KEYBORAD_PREF, false);
    }

    public static boolean isReadReceiptsEnabled(Context context) {
        return getBooleanPreference(context, READ_RECEIPTS_PREF, false);
    }

    public static void setReadReceiptsEnabled(Context context, boolean z) {
        setBooleanPreference(context, READ_RECEIPTS_PREF, z);
    }

    public static boolean isTypingIndicatorsEnabled(Context context) {
        return getBooleanPreference(context, TYPING_INDICATORS, false);
    }

    public static void setTypingIndicatorsEnabled(Context context, boolean z) {
        setBooleanPreference(context, TYPING_INDICATORS, z);
    }

    public static boolean wereLinkPreviewsEnabled(Context context) {
        return getBooleanPreference(context, LINK_PREVIEWS, true);
    }

    public static int getNotificationPriority(Context context) {
        try {
            return Integer.parseInt(getStringPreference(context, NOTIFICATION_PRIORITY_PREF, String.valueOf(1)));
        } catch (ClassCastException unused) {
            return getIntegerPreference(context, NOTIFICATION_PRIORITY_PREF, 1);
        }
    }

    public static int getMessageBodyTextSize(Context context) {
        return Integer.parseInt(getStringPreference(context, MESSAGE_BODY_TEXT_SIZE_PREF, "16"));
    }

    public static boolean isTurnOnly(Context context) {
        return getBooleanPreference(context, ALWAYS_RELAY_CALLS_PREF, false);
    }

    public static boolean isWebrtcCallingEnabled(Context context) {
        return getBooleanPreference(context, WEBRTC_CALLING_PREF, false);
    }

    public static void setWebrtcCallingEnabled(Context context, boolean z) {
        setBooleanPreference(context, WEBRTC_CALLING_PREF, z);
    }

    public static void setDirectCaptureCameraId(Context context, int i) {
        setIntegerPrefrence(context, DIRECT_CAPTURE_CAMERA_ID, i);
    }

    public static int getDirectCaptureCameraId(Context context) {
        return getIntegerPreference(context, DIRECT_CAPTURE_CAMERA_ID, 1);
    }

    public static void setMultiDevice(Context context, boolean z) {
        setBooleanPreference(context, MULTI_DEVICE_PROVISIONED_PREF, z);
    }

    public static boolean isMultiDevice(Context context) {
        return getBooleanPreference(context, MULTI_DEVICE_PROVISIONED_PREF, false);
    }

    @Deprecated
    public static NotificationPrivacyPreference getNotificationPrivacy(Context context) {
        return new NotificationPrivacyPreference(getStringPreference(context, NOTIFICATION_PRIVACY_PREF, "all"));
    }

    public static boolean isNewContactsNotificationEnabled(Context context) {
        return getBooleanPreference(context, NEW_CONTACTS_NOTIFICATIONS, true);
    }

    public static long getRatingLaterTimestamp(Context context) {
        return getLongPreference(context, RATING_LATER_PREF, 0);
    }

    public static void setRatingLaterTimestamp(Context context, long j) {
        setLongPreference(context, RATING_LATER_PREF, j);
    }

    public static boolean isRatingEnabled(Context context) {
        return getBooleanPreference(context, RATING_ENABLED_PREF, true);
    }

    public static void setRatingEnabled(Context context, boolean z) {
        setBooleanPreference(context, RATING_ENABLED_PREF, z);
    }

    @Deprecated
    public static boolean isWifiSmsEnabled(Context context) {
        return getBooleanPreference(context, WIFI_SMS_PREF, false);
    }

    @Deprecated
    public static int getRepeatAlertsCount(Context context) {
        try {
            return Integer.parseInt(getStringPreference(context, REPEAT_ALERTS_PREF, "0"));
        } catch (NumberFormatException e) {
            Log.w(TAG, e);
            return 0;
        }
    }

    @Deprecated
    public static boolean isInThreadNotifications(Context context) {
        return getBooleanPreference(context, IN_THREAD_NOTIFICATION_PREF, true);
    }

    public static long getUnidentifiedAccessCertificateRotationTime(Context context) {
        return getLongPreference(context, UNIDENTIFIED_ACCESS_CERTIFICATE_ROTATION_TIME_PREF, 0);
    }

    public static void setUnidentifiedAccessCertificateRotationTime(Context context, long j) {
        setLongPreference(context, UNIDENTIFIED_ACCESS_CERTIFICATE_ROTATION_TIME_PREF, j);
    }

    public static boolean isUniversalUnidentifiedAccess(Context context) {
        return getBooleanPreference(context, UNIVERSAL_UNIDENTIFIED_ACCESS, false);
    }

    public static void setShowUnidentifiedDeliveryIndicatorsEnabled(Context context, boolean z) {
        setBooleanPreference(context, SHOW_UNIDENTIFIED_DELIVERY_INDICATORS, z);
    }

    public static boolean isShowUnidentifiedDeliveryIndicatorsEnabled(Context context) {
        return getBooleanPreference(context, SHOW_UNIDENTIFIED_DELIVERY_INDICATORS, false);
    }

    public static void setIsUnidentifiedDeliveryEnabled(Context context, boolean z) {
        setBooleanPreference(context, UNIDENTIFIED_DELIVERY_ENABLED, z);
    }

    public static long getSignedPreKeyRotationTime(Context context) {
        return getLongPreference(context, SIGNED_PREKEY_ROTATION_TIME_PREF, 0);
    }

    public static void setSignedPreKeyRotationTime(Context context, long j) {
        setLongPreference(context, SIGNED_PREKEY_ROTATION_TIME_PREF, j);
    }

    public static long getDirectoryRefreshTime(Context context) {
        return getLongPreference(context, DIRECTORY_FRESH_TIME_PREF, 0);
    }

    public static void setDirectoryRefreshTime(Context context, long j) {
        setLongPreference(context, DIRECTORY_FRESH_TIME_PREF, j);
    }

    public static long getUpdateApkRefreshTime(Context context) {
        return getLongPreference(context, UPDATE_APK_REFRESH_TIME_PREF, 0);
    }

    public static void setUpdateApkRefreshTime(Context context, long j) {
        setLongPreference(context, UPDATE_APK_REFRESH_TIME_PREF, j);
    }

    public static void setUpdateApkDownloadId(Context context, long j) {
        setLongPreference(context, UPDATE_APK_DOWNLOAD_ID, j);
    }

    public static long getUpdateApkDownloadId(Context context) {
        return getLongPreference(context, UPDATE_APK_DOWNLOAD_ID, -1);
    }

    public static void setUpdateApkDigest(Context context, String str) {
        setStringPreference(context, UPDATE_APK_DIGEST, str);
    }

    public static String getUpdateApkDigest(Context context) {
        return getStringPreference(context, UPDATE_APK_DIGEST, null);
    }

    public static boolean isEnterImeKeyEnabled(Context context) {
        return getBooleanPreference(context, ENTER_PRESENT_PREF, false);
    }

    @Deprecated
    public static boolean isEnterSendsEnabled(Context context) {
        return getBooleanPreference(context, ENTER_SENDS_PREF, false);
    }

    public static boolean isPasswordDisabled(Context context) {
        return getBooleanPreference(context, DISABLE_PASSPHRASE_PREF, true);
    }

    public static void setPasswordDisabled(Context context, boolean z) {
        setBooleanPreference(context, DISABLE_PASSPHRASE_PREF, z);
    }

    public static boolean getUseCustomMmsc(Context context) {
        return getBooleanPreference(context, MMSC_CUSTOM_HOST_PREF, isLegacyUseLocalApnsEnabled(context));
    }

    public static void setUseCustomMmsc(Context context, boolean z) {
        setBooleanPreference(context, MMSC_CUSTOM_HOST_PREF, z);
    }

    public static String getMmscUrl(Context context) {
        return getStringPreference(context, MMSC_HOST_PREF, "");
    }

    public static void setMmscUrl(Context context, String str) {
        setStringPreference(context, MMSC_HOST_PREF, str);
    }

    public static boolean getUseCustomMmscProxy(Context context) {
        return getBooleanPreference(context, MMSC_CUSTOM_PROXY_PREF, isLegacyUseLocalApnsEnabled(context));
    }

    public static void setUseCustomMmscProxy(Context context, boolean z) {
        setBooleanPreference(context, MMSC_CUSTOM_PROXY_PREF, z);
    }

    public static String getMmscProxy(Context context) {
        return getStringPreference(context, MMSC_PROXY_HOST_PREF, "");
    }

    public static void setMmscProxy(Context context, String str) {
        setStringPreference(context, MMSC_PROXY_HOST_PREF, str);
    }

    public static boolean getUseCustomMmscProxyPort(Context context) {
        return getBooleanPreference(context, MMSC_CUSTOM_PROXY_PORT_PREF, isLegacyUseLocalApnsEnabled(context));
    }

    public static void setUseCustomMmscProxyPort(Context context, boolean z) {
        setBooleanPreference(context, MMSC_CUSTOM_PROXY_PORT_PREF, z);
    }

    public static String getMmscProxyPort(Context context) {
        return getStringPreference(context, MMSC_PROXY_PORT_PREF, "");
    }

    public static void setMmscProxyPort(Context context, String str) {
        setStringPreference(context, MMSC_PROXY_PORT_PREF, str);
    }

    public static boolean getUseCustomMmscUsername(Context context) {
        return getBooleanPreference(context, MMSC_CUSTOM_USERNAME_PREF, isLegacyUseLocalApnsEnabled(context));
    }

    public static void setUseCustomMmscUsername(Context context, boolean z) {
        setBooleanPreference(context, MMSC_CUSTOM_USERNAME_PREF, z);
    }

    public static String getMmscUsername(Context context) {
        return getStringPreference(context, MMSC_USERNAME_PREF, "");
    }

    public static void setMmscUsername(Context context, String str) {
        setStringPreference(context, MMSC_USERNAME_PREF, str);
    }

    public static boolean getUseCustomMmscPassword(Context context) {
        return getBooleanPreference(context, MMSC_CUSTOM_PASSWORD_PREF, isLegacyUseLocalApnsEnabled(context));
    }

    public static void setUseCustomMmscPassword(Context context, boolean z) {
        setBooleanPreference(context, MMSC_CUSTOM_PASSWORD_PREF, z);
    }

    public static String getMmscPassword(Context context) {
        return getStringPreference(context, MMSC_PASSWORD_PREF, "");
    }

    public static void setMmscPassword(Context context, String str) {
        setStringPreference(context, MMSC_PASSWORD_PREF, str);
    }

    public static String getMmsUserAgent(Context context, String str) {
        return getBooleanPreference(context, MMS_CUSTOM_USER_AGENT, false) ? getStringPreference(context, MMS_USER_AGENT, str) : str;
    }

    public static void setScreenSecurityEnabled(Context context, boolean z) {
        setBooleanPreference(context, SCREEN_SECURITY_PREF, z);
    }

    public static boolean isScreenSecurityEnabled(Context context) {
        return getBooleanPreference(context, SCREEN_SECURITY_PREF, false);
    }

    public static boolean isLegacyUseLocalApnsEnabled(Context context) {
        return getBooleanPreference(context, ENABLE_MANUAL_MMS_PREF, false);
    }

    public static int getLastVersionCode(Context context) {
        return getIntegerPreference(context, LAST_VERSION_CODE_PREF, Util.getCanonicalVersionCode());
    }

    public static void setLastVersionCode(Context context, int i) {
        if (!setIntegerPrefrenceBlocking(context, LAST_VERSION_CODE_PREF, i)) {
            throw new AssertionError("couldn't write version code to sharedpreferences");
        }
    }

    public static String getTheme(Context context) {
        return getStringPreference(context, THEME_PREF, DynamicTheme.systemThemeAvailable() ? "system" : "light");
    }

    public static boolean isShowInviteReminders(Context context) {
        return getBooleanPreference(context, SHOW_INVITE_REMINDER_PREF, true);
    }

    public static boolean isPassphraseTimeoutEnabled(Context context) {
        return getBooleanPreference(context, PASSPHRASE_TIMEOUT_PREF, false);
    }

    public static int getPassphraseTimeoutInterval(Context context) {
        return getIntegerPreference(context, PASSPHRASE_TIMEOUT_INTERVAL_PREF, WebRtcCallView.PIP_RESIZE_DURATION);
    }

    public static void setPassphraseTimeoutInterval(Context context, int i) {
        setIntegerPrefrence(context, PASSPHRASE_TIMEOUT_INTERVAL_PREF, i);
    }

    public static String getLanguage(Context context) {
        return getStringPreference(context, LANGUAGE_PREF, "zz");
    }

    public static void setLanguage(Context context, String str) {
        setStringPreference(context, LANGUAGE_PREF, str);
    }

    @Deprecated
    public static boolean isSmsDeliveryReportsEnabled(Context context) {
        return getBooleanPreference(context, SMS_DELIVERY_REPORT_PREF, false);
    }

    public static boolean hasSeenWelcomeScreen(Context context) {
        return getBooleanPreference(context, SEEN_WELCOME_SCREEN_PREF, true);
    }

    public static void setHasSeenWelcomeScreen(Context context, boolean z) {
        setBooleanPreference(context, SEEN_WELCOME_SCREEN_PREF, z);
    }

    public static boolean hasPromptedPushRegistration(Context context) {
        return getBooleanPreference(context, PROMPTED_PUSH_REGISTRATION_PREF, false);
    }

    public static void setPromptedPushRegistration(Context context, boolean z) {
        setBooleanPreference(context, PROMPTED_PUSH_REGISTRATION_PREF, z);
    }

    public static void setPromptedOptimizeDoze(Context context, boolean z) {
        setBooleanPreference(context, PROMPTED_OPTIMIZE_DOZE_PREF, z);
    }

    public static boolean hasPromptedOptimizeDoze(Context context) {
        return getBooleanPreference(context, PROMPTED_OPTIMIZE_DOZE_PREF, false);
    }

    public static boolean isInterceptAllMmsEnabled(Context context) {
        return getBooleanPreference(context, ALL_MMS_PREF, true);
    }

    public static boolean isInterceptAllSmsEnabled(Context context) {
        return getBooleanPreference(context, ALL_SMS_PREF, true);
    }

    @Deprecated
    public static boolean isNotificationsEnabled(Context context) {
        return getBooleanPreference(context, NOTIFICATION_PREF, true);
    }

    @Deprecated
    public static boolean isCallNotificationsEnabled(Context context) {
        return getBooleanPreference(context, CALL_NOTIFICATIONS_PREF, true);
    }

    @Deprecated
    public static Uri getNotificationRingtone(Context context) {
        String stringPreference = getStringPreference(context, RINGTONE_PREF, Settings.System.DEFAULT_NOTIFICATION_URI.toString());
        if (stringPreference != null && stringPreference.startsWith("file:")) {
            stringPreference = Settings.System.DEFAULT_NOTIFICATION_URI.toString();
        }
        return Uri.parse(stringPreference);
    }

    @Deprecated
    public static Uri getCallNotificationRingtone(Context context) {
        String stringPreference = getStringPreference(context, CALL_RINGTONE_PREF, Settings.System.DEFAULT_RINGTONE_URI.toString());
        if (stringPreference != null && stringPreference.startsWith("file:")) {
            stringPreference = Settings.System.DEFAULT_RINGTONE_URI.toString();
        }
        return Uri.parse(stringPreference);
    }

    @Deprecated
    public static boolean isNotificationVibrateEnabled(Context context) {
        return getBooleanPreference(context, VIBRATE_PREF, true);
    }

    @Deprecated
    public static boolean isCallNotificationVibrateEnabled(Context context) {
        boolean z = true;
        if (Build.VERSION.SDK_INT >= 23 && Settings.System.getInt(context.getContentResolver(), "vibrate_when_ringing", 1) != 1) {
            z = false;
        }
        return getBooleanPreference(context, CALL_VIBRATE_PREF, z);
    }

    @Deprecated
    public static String getNotificationLedColor(Context context) {
        return getStringPreference(context, LED_COLOR_PREF, "blue");
    }

    @Deprecated
    public static String getNotificationLedPattern(Context context) {
        return getStringPreference(context, LED_BLINK_PREF, "500,2000");
    }

    public static String getNotificationLedPatternCustom(Context context) {
        return getStringPreference(context, LED_BLINK_PREF_CUSTOM, "500,2000");
    }

    public static void setNotificationLedPatternCustom(Context context, String str) {
        setStringPreference(context, LED_BLINK_PREF_CUSTOM, str);
    }

    @Deprecated
    public static boolean isSystemEmojiPreferred(Context context) {
        return getBooleanPreference(context, SYSTEM_EMOJI_PREF, false);
    }

    public static Set<String> getMobileMediaDownloadAllowed(Context context) {
        return getMediaDownloadAllowed(context, MEDIA_DOWNLOAD_MOBILE_PREF, R.array.pref_media_download_mobile_data_default);
    }

    public static Set<String> getWifiMediaDownloadAllowed(Context context) {
        return getMediaDownloadAllowed(context, MEDIA_DOWNLOAD_WIFI_PREF, R.array.pref_media_download_wifi_default);
    }

    public static Set<String> getRoamingMediaDownloadAllowed(Context context) {
        return getMediaDownloadAllowed(context, MEDIA_DOWNLOAD_ROAMING_PREF, R.array.pref_media_download_roaming_default);
    }

    private static Set<String> getMediaDownloadAllowed(Context context, String str, int i) {
        return getStringSetPreference(context, str, new HashSet(Arrays.asList(context.getResources().getStringArray(i))));
    }

    public static void setLastOutageCheckTime(Context context, long j) {
        setLongPreference(context, LAST_OUTAGE_CHECK_TIME, j);
    }

    public static long getLastOutageCheckTime(Context context) {
        return getLongPreference(context, LAST_OUTAGE_CHECK_TIME, 0);
    }

    public static void setServiceOutage(Context context, boolean z) {
        setBooleanPreference(context, SERVICE_OUTAGE, z);
    }

    public static boolean getServiceOutage(Context context) {
        return getBooleanPreference(context, SERVICE_OUTAGE, false);
    }

    public static long getLastFullContactSyncTime(Context context) {
        return getLongPreference(context, LAST_FULL_CONTACT_SYNC_TIME, 0);
    }

    public static void setLastFullContactSyncTime(Context context, long j) {
        setLongPreference(context, LAST_FULL_CONTACT_SYNC_TIME, j);
    }

    public static boolean needsFullContactSync(Context context) {
        return getBooleanPreference(context, NEEDS_FULL_CONTACT_SYNC, false);
    }

    public static void setNeedsFullContactSync(Context context, boolean z) {
        setBooleanPreference(context, NEEDS_FULL_CONTACT_SYNC, z);
    }

    public static void setLogEncryptedSecret(Context context, String str) {
        setStringPreference(context, LOG_ENCRYPTED_SECRET, str);
    }

    public static String getLogEncryptedSecret(Context context) {
        return getStringPreference(context, LOG_ENCRYPTED_SECRET, null);
    }

    public static void setLogUnencryptedSecret(Context context, String str) {
        setStringPreference(context, LOG_UNENCRYPTED_SECRET, str);
    }

    public static String getLogUnencryptedSecret(Context context) {
        return getStringPreference(context, LOG_UNENCRYPTED_SECRET, null);
    }

    public static int getNotificationChannelVersion(Context context) {
        return getIntegerPreference(context, NOTIFICATION_CHANNEL_VERSION, 1);
    }

    public static void setNotificationChannelVersion(Context context, int i) {
        setIntegerPrefrence(context, NOTIFICATION_CHANNEL_VERSION, i);
    }

    public static int getNotificationMessagesChannelVersion(Context context) {
        return getIntegerPreference(context, NOTIFICATION_MESSAGES_CHANNEL_VERSION, 1);
    }

    public static void setNotificationMessagesChannelVersion(Context context, int i) {
        setIntegerPrefrence(context, NOTIFICATION_MESSAGES_CHANNEL_VERSION, i);
    }

    public static boolean getNeedsMessagePull(Context context) {
        return getBooleanPreference(context, NEEDS_MESSAGE_PULL, false);
    }

    public static void setNeedsMessagePull(Context context, boolean z) {
        setBooleanPreference(context, NEEDS_MESSAGE_PULL, z);
    }

    public static boolean hasSeenStickerIntroTooltip(Context context) {
        return getBooleanPreference(context, SEEN_STICKER_INTRO_TOOLTIP, false);
    }

    public static void setHasSeenStickerIntroTooltip(Context context, boolean z) {
        setBooleanPreference(context, SEEN_STICKER_INTRO_TOOLTIP, z);
    }

    public static void setMediaKeyboardMode(Context context, MediaKeyboardMode mediaKeyboardMode) {
        setStringPreference(context, MEDIA_KEYBOARD_MODE, mediaKeyboardMode.name());
    }

    public static MediaKeyboardMode getMediaKeyboardMode(Context context) {
        return MediaKeyboardMode.valueOf(getStringPreference(context, MEDIA_KEYBOARD_MODE, MediaKeyboardMode.EMOJI.name()));
    }

    public static void setHasSeenViewOnceTooltip(Context context, boolean z) {
        setBooleanPreference(context, VIEW_ONCE_TOOLTIP_SEEN, z);
    }

    public static boolean hasSeenViewOnceTooltip(Context context) {
        return getBooleanPreference(context, VIEW_ONCE_TOOLTIP_SEEN, false);
    }

    public static void setHasSeenCameraFirstTooltip(Context context, boolean z) {
        setBooleanPreference(context, SEEN_CAMERA_FIRST_TOOLTIP, z);
    }

    public static boolean hasSeenCameraFirstTooltip(Context context) {
        return getBooleanPreference(context, SEEN_CAMERA_FIRST_TOOLTIP, false);
    }

    public static void setJobManagerVersion(Context context, int i) {
        setIntegerPrefrence(context, JOB_MANAGER_VERSION, i);
    }

    public static int getJobManagerVersion(Context context) {
        return getIntegerPreference(context, JOB_MANAGER_VERSION, 1);
    }

    public static void setAppMigrationVersion(Context context, int i) {
        setIntegerPrefrence(context, APP_MIGRATION_VERSION, i);
    }

    public static int getAppMigrationVersion(Context context) {
        return getIntegerPreference(context, APP_MIGRATION_VERSION, 1);
    }

    public static void setFirstInstallVersion(Context context, int i) {
        setIntegerPrefrence(context, FIRST_INSTALL_VERSION, i);
    }

    public static int getFirstInstallVersion(Context context) {
        return getIntegerPreference(context, FIRST_INSTALL_VERSION, -1);
    }

    public static boolean hasSeenSwipeToReplyTooltip(Context context) {
        return getBooleanPreference(context, HAS_SEEN_SWIPE_TO_REPLY, false);
    }

    public static void setHasSeenSwipeToReplyTooltip(Context context, boolean z) {
        setBooleanPreference(context, HAS_SEEN_SWIPE_TO_REPLY, z);
    }

    public static boolean hasSeenVideoRecordingTooltip(Context context) {
        return getBooleanPreference(context, HAS_SEEN_VIDEO_RECORDING_TOOLTIP, false);
    }

    public static void setHasSeenVideoRecordingTooltip(Context context, boolean z) {
        setBooleanPreference(context, HAS_SEEN_VIDEO_RECORDING_TOOLTIP, z);
    }

    public static void setStorageManifestVersion(Context context, long j) {
        setLongPreference(context, STORAGE_MANIFEST_VERSION, j);
    }

    public static boolean isArgon2Tested(Context context) {
        return getBooleanPreference(context, ARGON2_TESTED, false);
    }

    public static void setArgon2Tested(Context context, boolean z) {
        setBooleanPreference(context, ARGON2_TESTED, z);
    }

    public static void setBooleanPreference(Context context, String str, boolean z) {
        getSharedPreferences(context).edit().putBoolean(str, z).apply();
    }

    public static boolean getBooleanPreference(Context context, String str, boolean z) {
        return getSharedPreferences(context).getBoolean(str, z);
    }

    public static void setStringPreference(Context context, String str, String str2) {
        getSharedPreferences(context).edit().putString(str, str2).apply();
    }

    public static String getStringPreference(Context context, String str, String str2) {
        return getSharedPreferences(context).getString(str, str2);
    }

    public static int getIntegerPreference(Context context, String str, int i) {
        return getSharedPreferences(context).getInt(str, i);
    }

    private static void setIntegerPrefrence(Context context, String str, int i) {
        getSharedPreferences(context).edit().putInt(str, i).apply();
    }

    private static boolean setIntegerPrefrenceBlocking(Context context, String str, int i) {
        return getSharedPreferences(context).edit().putInt(str, i).commit();
    }

    public static long getLongPreference(Context context, String str, long j) {
        return getSharedPreferences(context).getLong(str, j);
    }

    private static void setLongPreference(Context context, String str, long j) {
        getSharedPreferences(context).edit().putLong(str, j).apply();
    }

    private static void removePreference(Context context, String str) {
        getSharedPreferences(context).edit().remove(str).apply();
    }

    private static Set<String> getStringSetPreference(Context context, String str, Set<String> set) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.contains(str) ? sharedPreferences.getStringSet(str, Collections.emptySet()) : set;
    }

    private static void clearLocalCredentials(Context context) {
        SignalDatabase.recipients().setProfileKey(Recipient.self().getId(), ProfileKeyUtil.createNew());
        ApplicationDependencies.getGroupsV2Authorization().clear();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        if (preferences == null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return preferences;
    }
}
