package org.thoughtcrime.securesms.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationCancellationHelper$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.notifications.v2.NotificationFactory;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class BubbleUtil {
    private static final String TAG = Log.tag(BubbleUtil.class);

    /* loaded from: classes4.dex */
    public enum BubbleState {
        SHOWN,
        HIDDEN
    }

    private BubbleUtil() {
    }

    public static boolean canBubble(Context context, RecipientId recipientId, Long l) {
        if (l == null) {
            Log.i(TAG, "Cannot bubble recipient without thread");
            return false;
        } else if (!SignalStore.settings().getMessageNotificationsPrivacy().isDisplayContact()) {
            Log.i(TAG, "Bubbles are not available when notification privacy settings are enabled.");
            return false;
        } else {
            Recipient resolved = Recipient.resolved(recipientId);
            if (resolved.isBlocked()) {
                Log.i(TAG, "Cannot bubble blocked recipient");
                return false;
            }
            NotificationManager notificationManager = ServiceUtil.getNotificationManager(context);
            NotificationChannel notificationChannel = notificationManager.getNotificationChannel(ConversationUtil.getChannelId(context, resolved), ConversationUtil.getShortcutId(recipientId));
            if (notificationManager.areBubblesAllowed() || (notificationChannel != null && notificationChannel.canBubble())) {
                return true;
            }
            return false;
        }
    }

    public static void displayAsBubble(Context context, RecipientId recipientId, long j) {
        if (Build.VERSION.SDK_INT >= 30) {
            SignalExecutors.BOUNDED.execute(new Runnable(context, recipientId, j, ConversationId.forConversation(j)) { // from class: org.thoughtcrime.securesms.util.BubbleUtil$$ExternalSyntheticLambda1
                public final /* synthetic */ Context f$0;
                public final /* synthetic */ RecipientId f$1;
                public final /* synthetic */ long f$2;
                public final /* synthetic */ ConversationId f$3;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r5;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    BubbleUtil.lambda$displayAsBubble$1(this.f$0, this.f$1, this.f$2, this.f$3);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$displayAsBubble$1(Context context, RecipientId recipientId, long j, ConversationId conversationId) {
        if (canBubble(context, recipientId, Long.valueOf(j))) {
            Notification notification = (Notification) Stream.of(ServiceUtil.getNotificationManager(context).getActiveNotifications()).filter(new Predicate(NotificationIds.getNotificationIdForThread(conversationId)) { // from class: org.thoughtcrime.securesms.util.BubbleUtil$$ExternalSyntheticLambda0
                public final /* synthetic */ int f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return BubbleUtil.lambda$displayAsBubble$0(this.f$0, (StatusBarNotification) obj);
                }
            }).findFirst().map(new NotificationCancellationHelper$$ExternalSyntheticLambda1()).orElse(null);
            if (notification == null || notification.deleteIntent == null) {
                NotificationFactory.notifyToBubbleConversation(context, Recipient.resolved(recipientId), j);
            } else {
                ApplicationDependencies.getMessageNotifier().updateNotification(context, conversationId, BubbleState.SHOWN);
            }
        }
    }

    public static /* synthetic */ boolean lambda$displayAsBubble$0(int i, StatusBarNotification statusBarNotification) {
        return statusBarNotification.getId() == i;
    }
}
