package org.thoughtcrime.securesms.util;

import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ProcessLifecycleOwner;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import org.signal.core.util.ThreadUtil;

/* loaded from: classes.dex */
public class AppForegroundObserver {
    private volatile Boolean isForegrounded = null;
    private final Set<Listener> listeners = new CopyOnWriteArraySet();

    /* loaded from: classes.dex */
    public interface Listener {

        /* renamed from: org.thoughtcrime.securesms.util.AppForegroundObserver$Listener$-CC */
        /* loaded from: classes4.dex */
        public final /* synthetic */ class CC {
            public static void $default$onBackground(Listener listener) {
            }

            public static void $default$onForeground(Listener listener) {
            }
        }

        void onBackground();

        void onForeground();
    }

    public void begin() {
        ThreadUtil.assertMainThread();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new DefaultLifecycleObserver() { // from class: org.thoughtcrime.securesms.util.AppForegroundObserver.1
            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
                DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
                DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* bridge */ /* synthetic */ void onPause(LifecycleOwner lifecycleOwner) {
                DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* bridge */ /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
                DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public void onStart(LifecycleOwner lifecycleOwner) {
                AppForegroundObserver.this.onForeground();
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public void onStop(LifecycleOwner lifecycleOwner) {
                AppForegroundObserver.this.onBackground();
            }
        });
    }

    public void addListener(Listener listener) {
        this.listeners.add(listener);
        if (this.isForegrounded == null) {
            return;
        }
        if (this.isForegrounded.booleanValue()) {
            listener.onForeground();
        } else {
            listener.onBackground();
        }
    }

    public void removeListener(Listener listener) {
        this.listeners.remove(listener);
    }

    public boolean isForegrounded() {
        return this.isForegrounded != null && this.isForegrounded.booleanValue();
    }

    public void onForeground() {
        this.isForegrounded = Boolean.TRUE;
        for (Listener listener : this.listeners) {
            listener.onForeground();
        }
    }

    public void onBackground() {
        this.isForegrounded = Boolean.FALSE;
        for (Listener listener : this.listeners) {
            listener.onBackground();
        }
    }
}
