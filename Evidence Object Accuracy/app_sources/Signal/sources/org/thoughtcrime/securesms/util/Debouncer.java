package org.thoughtcrime.securesms.util;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.TimeUnit;

/* loaded from: classes4.dex */
public class Debouncer {
    private final Handler handler;
    private final long threshold;

    public Debouncer(long j, TimeUnit timeUnit) {
        this(timeUnit.toMillis(j));
    }

    public Debouncer(long j) {
        this.handler = new Handler(Looper.getMainLooper());
        this.threshold = j;
    }

    public void publish(Runnable runnable) {
        this.handler.removeCallbacksAndMessages(null);
        this.handler.postDelayed(runnable, this.threshold);
    }

    public void clear() {
        this.handler.removeCallbacksAndMessages(null);
    }
}
