package org.thoughtcrime.securesms.util.livedata;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Store$LiveDataStore$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Store.LiveDataStore f$0;
    public final /* synthetic */ Function f$1;

    public /* synthetic */ Store$LiveDataStore$$ExternalSyntheticLambda0(Store.LiveDataStore liveDataStore, Function function) {
        this.f$0 = liveDataStore;
        this.f$1 = function;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$update$2(this.f$1);
    }
}
