package org.thoughtcrime.securesms.util;

import kotlin.Metadata;
import org.signal.donations.GooglePayApi;
import org.signal.donations.StripeApi;
import org.thoughtcrime.securesms.BuildConfig;

/* compiled from: Environment.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0005B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/util/Environment;", "", "()V", "IS_STAGING", "", "Donations", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class Environment {
    public static final Environment INSTANCE = new Environment();
    public static final boolean IS_STAGING;

    private Environment() {
    }

    /* compiled from: Environment.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/util/Environment$Donations;", "", "()V", "GOOGLE_PAY_CONFIGURATION", "Lorg/signal/donations/GooglePayApi$Configuration;", "getGOOGLE_PAY_CONFIGURATION", "()Lorg/signal/donations/GooglePayApi$Configuration;", "STRIPE_CONFIGURATION", "Lorg/signal/donations/StripeApi$Configuration;", "getSTRIPE_CONFIGURATION", "()Lorg/signal/donations/StripeApi$Configuration;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Donations {
        private static final GooglePayApi.Configuration GOOGLE_PAY_CONFIGURATION = new GooglePayApi.Configuration(1);
        public static final Donations INSTANCE = new Donations();
        private static final StripeApi.Configuration STRIPE_CONFIGURATION = new StripeApi.Configuration(BuildConfig.STRIPE_PUBLISHABLE_KEY, null, null, 6, null);

        private Donations() {
        }

        public final GooglePayApi.Configuration getGOOGLE_PAY_CONFIGURATION() {
            return GOOGLE_PAY_CONFIGURATION;
        }

        public final StripeApi.Configuration getSTRIPE_CONFIGURATION() {
            return STRIPE_CONFIGURATION;
        }
    }
}
