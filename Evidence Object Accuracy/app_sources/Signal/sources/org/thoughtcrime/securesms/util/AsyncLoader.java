package org.thoughtcrime.securesms.util;

import android.content.Context;
import androidx.loader.content.AsyncTaskLoader;

/* loaded from: classes4.dex */
public abstract class AsyncLoader<D> extends AsyncTaskLoader<D> {
    private D data;

    public AsyncLoader(Context context) {
        super(context);
    }

    @Override // androidx.loader.content.Loader
    public void deliverResult(D d) {
        if (!isReset()) {
            this.data = d;
            super.deliverResult(d);
        }
    }

    @Override // androidx.loader.content.Loader
    protected void onStartLoading() {
        D d = this.data;
        if (d != null) {
            deliverResult(d);
        }
        if (takeContentChanged() || this.data == null) {
            forceLoad();
        }
    }

    @Override // androidx.loader.content.Loader
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override // androidx.loader.content.Loader
    public void onReset() {
        super.onReset();
        onStopLoading();
        this.data = null;
    }
}
