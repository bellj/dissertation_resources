package org.thoughtcrime.securesms.util;

import java.io.IOException;

/* loaded from: classes4.dex */
public interface IOFunction<I, O> {
    O apply(I i) throws IOException;
}
