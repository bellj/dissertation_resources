package org.thoughtcrime.securesms.util;

import androidx.recyclerview.widget.LinearLayoutManager;
import org.thoughtcrime.securesms.util.SnapToTopDataObserver;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SnapToTopDataObserver$ScrollRequestBuilder$$ExternalSyntheticLambda0 implements SnapToTopDataObserver.OnPerformScroll {
    @Override // org.thoughtcrime.securesms.util.SnapToTopDataObserver.OnPerformScroll
    public final void onPerformScroll(LinearLayoutManager linearLayoutManager, int i) {
        linearLayoutManager.scrollToPosition(i);
    }
}
