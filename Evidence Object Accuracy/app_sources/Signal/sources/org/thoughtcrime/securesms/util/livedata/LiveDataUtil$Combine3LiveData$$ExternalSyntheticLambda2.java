package org.thoughtcrime.securesms.util.livedata;

import androidx.lifecycle.Observer;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LiveDataUtil$Combine3LiveData$$ExternalSyntheticLambda2 implements Observer {
    public final /* synthetic */ LiveDataUtil.Combine3LiveData f$0;
    public final /* synthetic */ LiveDataUtil.Combine3 f$1;

    public /* synthetic */ LiveDataUtil$Combine3LiveData$$ExternalSyntheticLambda2(LiveDataUtil.Combine3LiveData combine3LiveData, LiveDataUtil.Combine3 combine3) {
        this.f$0 = combine3LiveData;
        this.f$1 = combine3;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.lambda$new$2(this.f$1, obj);
    }
}
