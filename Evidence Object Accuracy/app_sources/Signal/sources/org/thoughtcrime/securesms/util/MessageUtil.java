package org.thoughtcrime.securesms.util;

import android.content.Context;
import j$.util.Optional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.thoughtcrime.securesms.mms.TextSlide;
import org.thoughtcrime.securesms.providers.BlobProvider;

/* loaded from: classes4.dex */
public final class MessageUtil {
    private MessageUtil() {
    }

    public static SplitResult getSplitMessage(Context context, String str, int i) {
        Optional empty = Optional.empty();
        if (str.length() > i) {
            String substring = str.substring(0, i);
            byte[] bytes = str.getBytes();
            String format = String.format("signal-%s.txt", new SimpleDateFormat("yyyy-MM-dd-HHmmss", Locale.US).format(new Date()));
            empty = Optional.of(new TextSlide(context, BlobProvider.getInstance().forData(bytes).withMimeType(MediaUtil.LONG_TEXT).withFileName(format).createForSingleSessionInMemory(), format, (long) bytes.length));
            str = substring;
        }
        return new SplitResult(str, empty);
    }

    /* loaded from: classes4.dex */
    public static class SplitResult {
        private final String body;
        private final Optional<TextSlide> textSlide;

        private SplitResult(String str, Optional<TextSlide> optional) {
            this.body = str;
            this.textSlide = optional;
        }

        public String getBody() {
            return this.body;
        }

        public Optional<TextSlide> getTextSlide() {
            return this.textSlide;
        }
    }
}
