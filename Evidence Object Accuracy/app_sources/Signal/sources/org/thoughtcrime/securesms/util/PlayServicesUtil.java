package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import com.google.android.gms.common.GoogleApiAvailability;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class PlayServicesUtil {
    private static final String TAG = Log.tag(PlayServicesUtil.class);

    /* loaded from: classes4.dex */
    public enum PlayServicesStatus {
        SUCCESS,
        MISSING,
        NEEDS_UPDATE,
        TRANSIENT_ERROR
    }

    public static PlayServicesStatus getPlayServicesStatus(Context context) {
        try {
            int isGooglePlayServicesAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
            String str = TAG;
            Log.i(str, "Play Services: " + isGooglePlayServicesAvailable);
            if (isGooglePlayServicesAvailable == 0) {
                return PlayServicesStatus.SUCCESS;
            }
            if (isGooglePlayServicesAvailable != 1) {
                if (isGooglePlayServicesAvailable == 2) {
                    try {
                        ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo("com.google.android.gms", 0);
                        if (applicationInfo != null && !applicationInfo.enabled) {
                            return PlayServicesStatus.MISSING;
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        Log.w(TAG, e);
                    }
                    return PlayServicesStatus.NEEDS_UPDATE;
                } else if (!(isGooglePlayServicesAvailable == 3 || isGooglePlayServicesAvailable == 9 || isGooglePlayServicesAvailable == 16 || isGooglePlayServicesAvailable == 19)) {
                    return PlayServicesStatus.TRANSIENT_ERROR;
                }
            }
            return PlayServicesStatus.MISSING;
        } catch (Throwable th) {
            Log.w(TAG, th);
            return PlayServicesStatus.MISSING;
        }
    }
}
