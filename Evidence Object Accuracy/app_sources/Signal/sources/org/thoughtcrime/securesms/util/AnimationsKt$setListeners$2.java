package org.thoughtcrime.securesms.util;

import android.view.animation.Animation;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Lambda;

/* compiled from: Animations.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Landroid/view/animation/Animation;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AnimationsKt$setListeners$2 extends Lambda implements Function1<Animation, Unit> {
    public static final AnimationsKt$setListeners$2 INSTANCE = new AnimationsKt$setListeners$2();

    AnimationsKt$setListeners$2() {
        super(1);
    }

    public final void invoke(Animation animation) {
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Animation animation) {
        invoke(animation);
        return Unit.INSTANCE;
    }
}
