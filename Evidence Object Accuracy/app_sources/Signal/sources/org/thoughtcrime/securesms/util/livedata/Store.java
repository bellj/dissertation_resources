package org.thoughtcrime.securesms.util.livedata;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Observable;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.util.concurrent.SerialExecutor;

/* loaded from: classes4.dex */
public class Store<State> {
    private final Store<State>.LiveDataStore liveStore;

    /* loaded from: classes4.dex */
    public interface Action<Input, State> {
        State apply(Input input, State state);
    }

    public Store(State state) {
        this.liveStore = new LiveDataStore(state);
    }

    public LiveData<State> getStateLiveData() {
        return this.liveStore;
    }

    public State getState() {
        return this.liveStore.getState();
    }

    public void update(Function<State, State> function) {
        this.liveStore.update(function);
    }

    public <Input> void update(LiveData<Input> liveData, Action<Input, State> action) {
        this.liveStore.update(liveData, action);
    }

    public <Input> void update(Observable<Input> observable, Action<Input, State> action) {
        this.liveStore.update(LiveDataReactiveStreams.fromPublisher(observable.toFlowable(BackpressureStrategy.LATEST)), action);
    }

    public void clear() {
        this.liveStore.clear();
    }

    /* loaded from: classes4.dex */
    public final class LiveDataStore extends MediatorLiveData<State> {
        private final Set<LiveData> sources = new HashSet();
        private State state;
        private final Executor stateUpdater = new SerialExecutor(SignalExecutors.BOUNDED);

        LiveDataStore(State state) {
            Store.this = r2;
            setState(state);
        }

        synchronized State getState() {
            return this.state;
        }

        private synchronized void setState(State state) {
            this.state = state;
            postValue(state);
        }

        <Input> void update(LiveData<Input> liveData, Action<Input, State> action) {
            this.sources.add(liveData);
            addSource(liveData, new Store$LiveDataStore$$ExternalSyntheticLambda2(this, action));
        }

        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: org.thoughtcrime.securesms.util.livedata.Store$LiveDataStore */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: org.thoughtcrime.securesms.util.livedata.Store$Action */
        /* JADX WARN: Multi-variable type inference failed */
        public /* synthetic */ void lambda$update$0(Action action, Object obj) {
            setState(action.apply(obj, getState()));
        }

        public /* synthetic */ void lambda$update$1(Action action, Object obj) {
            this.stateUpdater.execute(new Store$LiveDataStore$$ExternalSyntheticLambda1(this, action, obj));
        }

        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: org.thoughtcrime.securesms.util.livedata.Store$LiveDataStore */
        /* JADX WARN: Multi-variable type inference failed */
        public /* synthetic */ void lambda$update$2(Function function) {
            setState(function.apply(getState()));
        }

        void update(Function<State, State> function) {
            this.stateUpdater.execute(new Store$LiveDataStore$$ExternalSyntheticLambda0(this, function));
        }

        void clear() {
            for (LiveData liveData : this.sources) {
                removeSource(liveData);
            }
            this.sources.clear();
        }
    }
}
