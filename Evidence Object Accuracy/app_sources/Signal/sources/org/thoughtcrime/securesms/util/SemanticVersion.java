package org.thoughtcrime.securesms.util;

import com.annimon.stream.ComparatorCompat;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* loaded from: classes4.dex */
public final class SemanticVersion implements Comparable<SemanticVersion> {
    private static final Comparator<SemanticVersion> COMPARATOR;
    private static final Comparator<SemanticVersion> MAJOR_COMPARATOR;
    private static final Comparator<SemanticVersion> MINOR_COMPARATOR;
    private static final Comparator<SemanticVersion> PATCH_COMPARATOR;
    private static final Pattern VERSION_PATTERN = Pattern.compile("^([0-9]+)\\.([0-9]+)\\.([0-9]+)$");
    private final int major;
    private final int minor;
    private final int patch;

    static {
        VERSION_PATTERN = Pattern.compile("^([0-9]+)\\.([0-9]+)\\.([0-9]+)$");
        SemanticVersion$$ExternalSyntheticLambda0 semanticVersion$$ExternalSyntheticLambda0 = new Comparator() { // from class: org.thoughtcrime.securesms.util.SemanticVersion$$ExternalSyntheticLambda0
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return SemanticVersion.lambda$static$0((SemanticVersion) obj, (SemanticVersion) obj2);
            }
        };
        MAJOR_COMPARATOR = semanticVersion$$ExternalSyntheticLambda0;
        SemanticVersion$$ExternalSyntheticLambda1 semanticVersion$$ExternalSyntheticLambda1 = new Comparator() { // from class: org.thoughtcrime.securesms.util.SemanticVersion$$ExternalSyntheticLambda1
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return SemanticVersion.lambda$static$1((SemanticVersion) obj, (SemanticVersion) obj2);
            }
        };
        MINOR_COMPARATOR = semanticVersion$$ExternalSyntheticLambda1;
        SemanticVersion$$ExternalSyntheticLambda2 semanticVersion$$ExternalSyntheticLambda2 = new Comparator() { // from class: org.thoughtcrime.securesms.util.SemanticVersion$$ExternalSyntheticLambda2
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return SemanticVersion.lambda$static$2((SemanticVersion) obj, (SemanticVersion) obj2);
            }
        };
        PATCH_COMPARATOR = semanticVersion$$ExternalSyntheticLambda2;
        COMPARATOR = ComparatorCompat.chain(semanticVersion$$ExternalSyntheticLambda0).thenComparing((Comparator) semanticVersion$$ExternalSyntheticLambda1).thenComparing((Comparator) semanticVersion$$ExternalSyntheticLambda2);
    }

    public static /* synthetic */ int lambda$static$0(SemanticVersion semanticVersion, SemanticVersion semanticVersion2) {
        return Integer.compare(semanticVersion.major, semanticVersion2.major);
    }

    public static /* synthetic */ int lambda$static$1(SemanticVersion semanticVersion, SemanticVersion semanticVersion2) {
        return Integer.compare(semanticVersion.minor, semanticVersion2.minor);
    }

    public static /* synthetic */ int lambda$static$2(SemanticVersion semanticVersion, SemanticVersion semanticVersion2) {
        return Integer.compare(semanticVersion.patch, semanticVersion2.patch);
    }

    public SemanticVersion(int i, int i2, int i3) {
        this.major = i;
        this.minor = i2;
        this.patch = i3;
    }

    public static SemanticVersion parse(String str) {
        if (str == null) {
            return null;
        }
        Matcher matcher = VERSION_PATTERN.matcher(str);
        if (Util.isEmpty(str) || !matcher.matches()) {
            return null;
        }
        return new SemanticVersion(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(3)));
    }

    public int compareTo(SemanticVersion semanticVersion) {
        return COMPARATOR.compare(this, semanticVersion);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SemanticVersion.class != obj.getClass()) {
            return false;
        }
        SemanticVersion semanticVersion = (SemanticVersion) obj;
        if (this.major == semanticVersion.major && this.minor == semanticVersion.minor && this.patch == semanticVersion.patch) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Objects.hash(Integer.valueOf(this.major), Integer.valueOf(this.minor), Integer.valueOf(this.patch));
    }
}
