package org.thoughtcrime.securesms.util;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: InsetItemDecoration.kt */
@Metadata(bv = {}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\t\b\u0016\u0018\u00002\u00020\u0001:\u0001\u001fB\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000f\u0010\u0010B\u001d\b\u0016\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0011¢\u0006\u0004\b\u000f\u0010\u0014B;\b\u0016\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0011\u0012\u001c\u0010\u0018\u001a\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00160\u0015j\u0002`\u0017¢\u0006\u0004\b\u000f\u0010\u0019BQ\b\u0016\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u001c\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u0011\u0012\u001e\b\u0002\u0010\u0018\u001a\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00160\u0015j\u0002`\u0017¢\u0006\u0004\b\u000f\u0010\u001eJ(\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\bH\u0016R\u0014\u0010\r\u001a\u00020\f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/util/InsetItemDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "Landroid/graphics/Rect;", "outRect", "Landroid/view/View;", "view", "Landroidx/recyclerview/widget/RecyclerView;", "parent", "Landroidx/recyclerview/widget/RecyclerView$State;", "state", "", "getItemOffsets", "Lorg/thoughtcrime/securesms/util/InsetItemDecoration$SetInset;", "setInset", "Lorg/thoughtcrime/securesms/util/InsetItemDecoration$SetInset;", "<init>", "(Lorg/thoughtcrime/securesms/util/InsetItemDecoration$SetInset;)V", "", "horizontalInset", "verticalInset", "(II)V", "Lkotlin/Function2;", "", "Lorg/thoughtcrime/securesms/util/Predicate;", "predicate", "(IILkotlin/jvm/functions/Function2;)V", "leftInset", "rightInset", "topInset", "bottomInset", "(IIIILkotlin/jvm/functions/Function2;)V", "SetInset", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public class InsetItemDecoration extends RecyclerView.ItemDecoration {
    private final SetInset setInset;

    public InsetItemDecoration(SetInset setInset) {
        Intrinsics.checkNotNullParameter(setInset, "setInset");
        this.setInset = setInset;
    }

    public InsetItemDecoration(int i, int i2) {
        this(i, i2, InsetItemDecorationKt.ALWAYS_TRUE);
    }

    public /* synthetic */ InsetItemDecoration(int i, int i2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this((i3 & 1) != 0 ? 0 : i, (i3 & 2) != 0 ? 0 : i2);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public InsetItemDecoration(int i, int i2, Function2<? super View, ? super RecyclerView, Boolean> function2) {
        this(i, i, i2, i2, function2);
        Intrinsics.checkNotNullParameter(function2, "predicate");
    }

    public /* synthetic */ InsetItemDecoration(int i, int i2, Function2 function2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this((i3 & 1) != 0 ? 0 : i, (i3 & 2) != 0 ? 0 : i2, function2);
    }

    public /* synthetic */ InsetItemDecoration(int i, int i2, int i3, int i4, Function2 function2, int i5, DefaultConstructorMarker defaultConstructorMarker) {
        this((i5 & 1) != 0 ? 0 : i, (i5 & 2) != 0 ? 0 : i2, (i5 & 4) != 0 ? 0 : i3, (i5 & 8) == 0 ? i4 : 0, (i5 & 16) != 0 ? InsetItemDecorationKt.ALWAYS_TRUE : function2);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public InsetItemDecoration(final int i, final int i2, final int i3, final int i4, final Function2<? super View, ? super RecyclerView, Boolean> function2) {
        this(new SetInset() { // from class: org.thoughtcrime.securesms.util.InsetItemDecoration.1
            @Override // org.thoughtcrime.securesms.util.InsetItemDecoration.SetInset
            public void setInset(Rect rect, View view, RecyclerView recyclerView) {
                Intrinsics.checkNotNullParameter(rect, "outRect");
                Intrinsics.checkNotNullParameter(view, "view");
                Intrinsics.checkNotNullParameter(recyclerView, "parent");
                if (Intrinsics.areEqual(function2, InsetItemDecorationKt.ALWAYS_TRUE) || function2.invoke(view, recyclerView).booleanValue()) {
                    rect.left = i;
                    rect.right = i2;
                    rect.top = i3;
                    rect.bottom = i4;
                }
            }
        });
        Intrinsics.checkNotNullParameter(function2, "predicate");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        Intrinsics.checkNotNullParameter(rect, "outRect");
        Intrinsics.checkNotNullParameter(view, "view");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        super.getItemOffsets(rect, view, recyclerView, state);
        this.setInset.setInset(rect, view, recyclerView);
    }

    /* compiled from: InsetItemDecoration.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b&\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ \u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH&¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/util/InsetItemDecoration$SetInset;", "", "()V", "getPosition", "", "view", "Landroid/view/View;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "setInset", "", "outRect", "Landroid/graphics/Rect;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class SetInset {
        public abstract void setInset(Rect rect, View view, RecyclerView recyclerView);

        public final int getPosition(View view, RecyclerView recyclerView) {
            Intrinsics.checkNotNullParameter(view, "view");
            Intrinsics.checkNotNullParameter(recyclerView, "parent");
            return recyclerView.getChildAdapterPosition(view);
        }
    }
}
