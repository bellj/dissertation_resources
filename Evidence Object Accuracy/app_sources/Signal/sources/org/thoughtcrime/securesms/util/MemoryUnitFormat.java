package org.thoughtcrime.securesms.util;

import java.text.DecimalFormat;

/* loaded from: classes4.dex */
public enum MemoryUnitFormat {
    BYTES(" B"),
    KILO_BYTES(" kB"),
    MEGA_BYTES(" MB"),
    GIGA_BYTES(" GB"),
    TERA_BYTES(" TB");
    
    private static final DecimalFormat ONE_DP = new DecimalFormat("#,##0.0");
    private static final DecimalFormat OPTIONAL_ONE_DP = new DecimalFormat("#,##0.#");
    private final String unitString;

    MemoryUnitFormat(String str) {
        this.unitString = str;
    }

    public double fromBytes(long j) {
        double d = (double) j;
        double pow = Math.pow(1000.0d, (double) ordinal());
        Double.isNaN(d);
        return d / pow;
    }

    public static String formatBytes(long j, MemoryUnitFormat memoryUnitFormat, boolean z) {
        if (j <= 0) {
            j = 0;
        }
        int log10 = j != 0 ? (int) (Math.log10((double) j) / 3.0d) : 0;
        if (log10 >= values().length) {
            log10 = values().length - 1;
        }
        MemoryUnitFormat memoryUnitFormat2 = values()[log10];
        if (memoryUnitFormat2.ordinal() >= memoryUnitFormat.ordinal()) {
            memoryUnitFormat = memoryUnitFormat2;
        }
        StringBuilder sb = new StringBuilder();
        sb.append((z ? ONE_DP : OPTIONAL_ONE_DP).format(memoryUnitFormat.fromBytes(j)));
        sb.append(memoryUnitFormat.unitString);
        return sb.toString();
    }

    public static String formatBytes(long j) {
        return formatBytes(j, BYTES, false);
    }
}
