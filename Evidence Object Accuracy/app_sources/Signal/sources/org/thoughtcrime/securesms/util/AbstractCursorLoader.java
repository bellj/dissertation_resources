package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.database.Cursor;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public abstract class AbstractCursorLoader extends AsyncTaskLoader<Cursor> {
    private static final String TAG = Log.tag(AbstractCursorLoader.class);
    protected final Context context;
    protected Cursor cursor;
    private final Loader<Cursor>.ForceLoadContentObserver observer = new Loader.ForceLoadContentObserver();

    public abstract Cursor getCursor();

    public AbstractCursorLoader(Context context) {
        super(context);
        this.context = context.getApplicationContext();
    }

    public void deliverResult(Cursor cursor) {
        if (!isReset()) {
            Cursor cursor2 = this.cursor;
            this.cursor = cursor;
            if (isStarted()) {
                super.deliverResult((AbstractCursorLoader) cursor);
            }
            if (cursor2 != null && cursor2 != this.cursor && !cursor2.isClosed()) {
                cursor2.close();
            }
        } else if (cursor != null) {
            cursor.close();
        }
    }

    @Override // androidx.loader.content.Loader
    protected void onStartLoading() {
        Cursor cursor = this.cursor;
        if (cursor != null) {
            deliverResult(cursor);
        }
        if (takeContentChanged() || this.cursor == null) {
            forceLoad();
        }
    }

    @Override // androidx.loader.content.Loader
    protected void onStopLoading() {
        cancelLoad();
    }

    public void onCanceled(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    @Override // androidx.loader.content.AsyncTaskLoader
    public Cursor loadInBackground() {
        long currentTimeMillis = System.currentTimeMillis();
        Cursor cursor = getCursor();
        if (cursor != null) {
            cursor.getCount();
            cursor.registerContentObserver(this.observer);
        }
        String str = TAG;
        Log.d(str, "[" + getClass().getSimpleName() + "] Cursor load time: " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        return cursor;
    }

    @Override // androidx.loader.content.Loader
    public void onReset() {
        super.onReset();
        onStopLoading();
        Cursor cursor = this.cursor;
        if (cursor != null && !cursor.isClosed()) {
            this.cursor.close();
        }
        this.cursor = null;
    }
}
