package org.thoughtcrime.securesms.util;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.widget.Toast;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.MapUtil;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.util.task.ProgressDialogAsyncTask;

/* loaded from: classes4.dex */
public class SaveAttachmentTask extends ProgressDialogAsyncTask<Attachment, Void, Pair<Integer, String>> {
    private static final int FAILURE;
    static final int SUCCESS;
    private static final String TAG = Log.tag(SaveAttachmentTask.class);
    private static final int WRITE_ACCESS_FAILURE;
    private final int attachmentCount;
    private final Map<Uri, Set<String>> batchOperationNameCache;
    private final WeakReference<Context> contextReference;

    public SaveAttachmentTask(Context context) {
        this(context, 1);
    }

    public SaveAttachmentTask(Context context, int i) {
        super(context, context.getResources().getQuantityString(R.plurals.ConversationFragment_saving_n_attachments, i, Integer.valueOf(i)), context.getResources().getQuantityString(R.plurals.ConversationFragment_saving_n_attachments_to_sd_card, i, Integer.valueOf(i)));
        this.batchOperationNameCache = new HashMap();
        this.contextReference = new WeakReference<>(context);
        this.attachmentCount = i;
    }

    public Pair<Integer, String> doInBackground(Attachment... attachmentArr) {
        if (attachmentArr == null || attachmentArr.length == 0) {
            throw new AssertionError("must pass in at least one attachment");
        }
        try {
            Context context = this.contextReference.get();
            if (!StorageUtil.canWriteToMediaStore()) {
                return new Pair<>(2, null);
            }
            if (context == null) {
                return new Pair<>(1, null);
            }
            String str = null;
            for (Attachment attachment : attachmentArr) {
                if (attachment != null && (str = saveAttachment(context, attachment)) == null) {
                    return new Pair<>(1, null);
                }
            }
            if (attachmentArr.length > 1) {
                return new Pair<>(0, null);
            }
            return new Pair<>(0, str);
        } catch (IOException e) {
            Log.w(TAG, e);
            return new Pair<>(1, null);
        }
    }

    private String saveAttachment(Context context, Attachment attachment) throws IOException {
        String correctedMimeType = MediaUtil.getCorrectedMimeType(attachment.contentType);
        Objects.requireNonNull(correctedMimeType);
        String str = attachment.fileName;
        if (str == null) {
            str = generateOutputFileName(correctedMimeType, attachment.date);
        }
        CreateMediaUriResult createMediaUri = createMediaUri(getMediaStoreContentUriForType(correctedMimeType), correctedMimeType, sanitizeOutputFileName(str));
        ContentValues contentValues = new ContentValues();
        InputStream attachmentStream = PartAuthority.getAttachmentStream(context, attachment.uri);
        if (attachmentStream == null) {
            if (attachmentStream != null) {
                attachmentStream.close();
            }
            return null;
        }
        try {
            if (Objects.equals(createMediaUri.outputUri.getScheme(), "file")) {
                FileOutputStream fileOutputStream = new FileOutputStream(createMediaUri.mediaUri.getPath());
                StreamUtil.copy(attachmentStream, fileOutputStream);
                MediaScannerConnection.scanFile(context, new String[]{createMediaUri.mediaUri.getPath()}, new String[]{correctedMimeType}, null);
                fileOutputStream.close();
            } else {
                OutputStream openOutputStream = context.getContentResolver().openOutputStream(createMediaUri.mediaUri, "w");
                long copy = StreamUtil.copy(attachmentStream, openOutputStream);
                if (copy > 0) {
                    contentValues.put("_size", Long.valueOf(copy));
                }
                if (openOutputStream != null) {
                    openOutputStream.close();
                }
            }
            attachmentStream.close();
            if (Build.VERSION.SDK_INT > 28) {
                contentValues.put("is_pending", (Integer) 0);
            }
            if (contentValues.size() > 0) {
                getContext().getContentResolver().update(createMediaUri.mediaUri, contentValues, null, null);
            }
            return createMediaUri.outputUri.getLastPathSegment();
        } catch (Throwable th) {
            try {
                attachmentStream.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    private Uri getMediaStoreContentUriForType(String str) {
        if (str.startsWith("video/")) {
            return StorageUtil.getVideoUri();
        }
        if (str.startsWith("audio/")) {
            return StorageUtil.getAudioUri();
        }
        if (str.startsWith("image/")) {
            return StorageUtil.getImageUri();
        }
        return StorageUtil.getDownloadUri();
    }

    private File ensureExternalPath(File file) {
        if (file != null && file.exists()) {
            return file;
        }
        if (file == null) {
            File externalStoragePublicDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
            if (externalStoragePublicDirectory.exists() || externalStoragePublicDirectory.mkdirs()) {
                return externalStoragePublicDirectory;
            }
            return null;
        } else if (file.mkdirs()) {
            return file;
        } else {
            return null;
        }
    }

    private String getExternalPathForType(String str) {
        File file;
        if (str.startsWith("video/")) {
            file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        } else if (str.startsWith("audio/")) {
            file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        } else {
            file = str.startsWith("image/") ? Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) : null;
        }
        File ensureExternalPath = ensureExternalPath(file);
        if (ensureExternalPath == null) {
            return null;
        }
        return ensureExternalPath.getAbsolutePath();
    }

    private String generateOutputFileName(String str, long j) {
        String extensionFromMimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(str);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
        String str2 = "signal-" + simpleDateFormat.format(Long.valueOf(j));
        if (extensionFromMimeType == null) {
            extensionFromMimeType = "attach";
        }
        return str2 + "." + extensionFromMimeType;
    }

    private String sanitizeOutputFileName(String str) {
        return new File(str).getName();
    }

    private CreateMediaUriResult createMediaUri(Uri uri, String str, String str2) throws IOException {
        String str3 = str2;
        String[] fileNameParts = getFileNameParts(str3);
        int i = 0;
        String str4 = fileNameParts[0];
        String str5 = fileNameParts[1];
        String mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(str5);
        if (MediaUtil.isOctetStream(mimeTypeFromExtension) && MediaUtil.isImageVideoOrAudioType(str)) {
            String str6 = TAG;
            Log.d(str6, "MimeTypeMap returned octet stream for media, changing to provided content type [" + str + "] instead.");
            mimeTypeFromExtension = str;
        }
        if (MediaUtil.isOctetStream(mimeTypeFromExtension)) {
            if (uri.equals(StorageUtil.getAudioUri())) {
                mimeTypeFromExtension = MediaUtil.AUDIO_UNSPECIFIED;
            } else if (uri.equals(StorageUtil.getVideoUri())) {
                mimeTypeFromExtension = MediaUtil.VIDEO_UNSPECIFIED;
            } else if (uri.equals(StorageUtil.getImageUri())) {
                mimeTypeFromExtension = "image/*";
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("_display_name", str3);
        contentValues.put("mime_type", mimeTypeFromExtension);
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        contentValues.put("date_added", Long.valueOf(timeUnit.toSeconds(System.currentTimeMillis())));
        contentValues.put("date_modified", Long.valueOf(timeUnit.toSeconds(System.currentTimeMillis())));
        AnonymousClass1 r13 = null;
        if (Build.VERSION.SDK_INT > 28) {
            while (true) {
                if (!pathInCache(uri, str3) && !displayNameTaken(uri, str3)) {
                    break;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(str4);
                sb.append("-");
                i++;
                sb.append(i);
                sb.append(".");
                sb.append(str5);
                str3 = sb.toString();
            }
            contentValues.put("_display_name", str3);
            contentValues.put("is_pending", (Integer) 1);
            putInCache(uri, str3);
        } else if (Objects.equals(uri.getScheme(), "file")) {
            File file = new File(uri.getPath());
            File file2 = new File(file, str4 + "." + str5);
            while (true) {
                if (!pathInCache(uri, file2.getPath()) && !file2.exists()) {
                    break;
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str4);
                sb2.append("-");
                i++;
                sb2.append(i);
                sb2.append(".");
                sb2.append(str5);
                file2 = new File(file, sb2.toString());
            }
            if (!file2.isHidden()) {
                putInCache(uri, file2.getPath());
                return new CreateMediaUriResult(uri, Uri.fromFile(file2));
            }
            throw new IOException("Specified name would not be visible");
        } else {
            String externalPathForType = getExternalPathForType(str);
            if (externalPathForType != null) {
                String format = String.format("%s/%s", externalPathForType, str3);
                int i2 = 0;
                while (true) {
                    if (!pathInCache(uri, format) && !pathTaken(uri, format)) {
                        break;
                    }
                    Log.d(TAG, "The content exists. Rename and check again.");
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str4);
                    sb3.append("-");
                    int i3 = i2 + 1;
                    sb3.append(i3);
                    sb3.append(".");
                    sb3.append(str5);
                    String sb4 = sb3.toString();
                    i2 = i3;
                    str3 = sb4;
                    format = String.format("%s/%s", externalPathForType, sb4);
                    r13 = null;
                }
                putInCache(uri, str3);
                contentValues.put(AttachmentDatabase.DATA, format);
            } else {
                throw new IOException(String.format(Locale.US, "Path for type: %s was not available", str));
            }
        }
        try {
            return new CreateMediaUriResult(uri, getContext().getContentResolver().insert(uri, contentValues));
        } catch (RuntimeException e) {
            if ((e instanceof IllegalArgumentException) || (e.getCause() instanceof IllegalArgumentException)) {
                String str7 = TAG;
                Log.w(str7, "Unable to create uri in " + uri + " with mimeType [" + mimeTypeFromExtension + "]");
                return new CreateMediaUriResult(StorageUtil.getDownloadUri(), getContext().getContentResolver().insert(StorageUtil.getDownloadUri(), contentValues));
            }
            throw e;
        }
    }

    private void putInCache(Uri uri, String str) {
        Set<String> set = (Set) MapUtil.getOrDefault(this.batchOperationNameCache, uri, new HashSet());
        if (set.add(str)) {
            this.batchOperationNameCache.put(uri, set);
            return;
        }
        throw new IllegalStateException("Path already used in data set.");
    }

    private boolean pathInCache(Uri uri, String str) {
        Set<String> set = this.batchOperationNameCache.get(uri);
        if (set == null) {
            return false;
        }
        return set.contains(str);
    }

    private boolean pathTaken(Uri uri, String str) throws IOException {
        Cursor query = getContext().getContentResolver().query(uri, new String[]{AttachmentDatabase.DATA}, "_data = ?", new String[]{str}, null);
        try {
            if (query != null) {
                boolean moveToFirst = query.moveToFirst();
                query.close();
                return moveToFirst;
            }
            throw new IOException("Something is wrong with the filename to save");
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private boolean displayNameTaken(Uri uri, String str) throws IOException {
        Cursor query = getContext().getContentResolver().query(uri, new String[]{"_display_name"}, "_display_name = ?", new String[]{str}, null);
        try {
            if (query != null) {
                boolean moveToFirst = query.moveToFirst();
                query.close();
                return moveToFirst;
            }
            throw new IOException("Something is wrong with the displayName to save");
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private String[] getFileNameParts(String str) {
        String[] strArr = new String[2];
        String[] split = str.split("\\.(?=[^\\.]+$)");
        strArr[0] = split[0];
        if (split.length > 1) {
            strArr[1] = split[1];
        } else {
            strArr[1] = "";
        }
        return strArr;
    }

    public void onPostExecute(Pair<Integer, String> pair) {
        super.onPostExecute((SaveAttachmentTask) pair);
        Context context = this.contextReference.get();
        if (context != null) {
            int intValue = pair.first().intValue();
            if (intValue == 0) {
                Toast.makeText(context, !TextUtils.isEmpty(pair.second()) ? context.getResources().getString(R.string.SaveAttachmentTask_saved_to, pair.second()) : context.getResources().getString(R.string.SaveAttachmentTask_saved), 1).show();
            } else if (intValue == 1) {
                Toast.makeText(context, context.getResources().getQuantityText(R.plurals.ConversationFragment_error_while_saving_attachments_to_sd_card, this.attachmentCount), 1).show();
            } else if (intValue == 2) {
                Toast.makeText(context, (int) R.string.ConversationFragment_unable_to_write_to_sd_card_exclamation, 1).show();
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class Attachment {
        public String contentType;
        public long date;
        public String fileName;
        public Uri uri;

        public Attachment(Uri uri, String str, long j, String str2) {
            if (uri == null || str == null || j < 0) {
                throw new AssertionError("uri, content type, and date must all be specified");
            }
            this.uri = uri;
            this.fileName = str2;
            this.contentType = str;
            this.date = j;
        }
    }

    /* loaded from: classes4.dex */
    public static class CreateMediaUriResult {
        final Uri mediaUri;
        final Uri outputUri;

        private CreateMediaUriResult(Uri uri, Uri uri2) {
            this.outputUri = uri;
            this.mediaUri = uri2;
        }
    }

    public static void showWarningDialog(Context context, DialogInterface.OnClickListener onClickListener) {
        showWarningDialog(context, onClickListener, 1);
    }

    public static void showWarningDialog(Context context, DialogInterface.OnClickListener onClickListener, int i) {
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context);
        materialAlertDialogBuilder.setTitle(R.string.ConversationFragment_save_to_sd_card);
        materialAlertDialogBuilder.setIcon(R.drawable.ic_warning);
        materialAlertDialogBuilder.setCancelable(true);
        materialAlertDialogBuilder.setMessage((CharSequence) context.getResources().getQuantityString(R.plurals.ConversationFragment_saving_n_media_to_storage_warning, i, Integer.valueOf(i)));
        materialAlertDialogBuilder.setPositiveButton(R.string.yes, onClickListener);
        materialAlertDialogBuilder.setNegativeButton(R.string.no, (DialogInterface.OnClickListener) null);
        materialAlertDialogBuilder.show();
    }
}
