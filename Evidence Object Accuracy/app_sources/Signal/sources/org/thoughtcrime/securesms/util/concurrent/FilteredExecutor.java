package org.thoughtcrime.securesms.util.concurrent;

import java.util.concurrent.Executor;

/* loaded from: classes4.dex */
public final class FilteredExecutor implements Executor {
    private final Executor backgroundExecutor;
    private final Filter filter;

    /* loaded from: classes4.dex */
    public interface Filter {
        boolean shouldRunOnExecutor();
    }

    public FilteredExecutor(Executor executor, Filter filter) {
        this.backgroundExecutor = executor;
        this.filter = filter;
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        if (this.filter.shouldRunOnExecutor()) {
            this.backgroundExecutor.execute(runnable);
        } else {
            runnable.run();
        }
    }
}
