package org.thoughtcrime.securesms.util.livedata;

import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LiveDataUtil$2$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ LiveDataUtil.AnonymousClass2 f$0;
    public final /* synthetic */ Object f$1;

    public /* synthetic */ LiveDataUtil$2$$ExternalSyntheticLambda0(LiveDataUtil.AnonymousClass2 r1, Object obj) {
        this.f$0 = r1;
        this.f$1 = obj;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onActive$0(this.f$1);
    }
}
