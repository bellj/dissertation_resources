package org.thoughtcrime.securesms.util.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import java.lang.ref.WeakReference;

/* loaded from: classes4.dex */
public abstract class ProgressDialogAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    private final WeakReference<Context> contextReference;
    private final String message;
    private ProgressDialog progress;
    private final String title;

    public ProgressDialogAsyncTask(Context context, String str, String str2) {
        this.contextReference = new WeakReference<>(context);
        this.title = str;
        this.message = str2;
    }

    public ProgressDialogAsyncTask(Context context, int i, int i2) {
        this(context, context.getString(i), context.getString(i2));
    }

    @Override // android.os.AsyncTask
    protected void onPreExecute() {
        Context context = this.contextReference.get();
        if (context != null) {
            this.progress = ProgressDialog.show(context, this.title, this.message, true);
        }
    }

    @Override // android.os.AsyncTask
    public void onPostExecute(Result result) {
        ProgressDialog progressDialog = this.progress;
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public Context getContext() {
        return this.contextReference.get();
    }
}
