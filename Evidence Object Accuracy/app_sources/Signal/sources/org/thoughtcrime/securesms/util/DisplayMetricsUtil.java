package org.thoughtcrime.securesms.util;

import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

/* loaded from: classes4.dex */
public final class DisplayMetricsUtil {
    private DisplayMetricsUtil() {
    }

    public static void forceAspectRatioToScreenByAdjustingHeight(DisplayMetrics displayMetrics, View view) {
        int i = displayMetrics.heightPixels;
        int i2 = displayMetrics.widthPixels;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = (layoutParams.width * i) / i2;
        view.setLayoutParams(layoutParams);
    }
}
