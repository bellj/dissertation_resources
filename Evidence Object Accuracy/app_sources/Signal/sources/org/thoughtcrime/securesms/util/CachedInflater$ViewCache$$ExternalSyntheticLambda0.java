package org.thoughtcrime.securesms.util;

import android.view.ViewGroup;
import androidx.asynclayoutinflater.view.AsyncLayoutInflater;
import org.thoughtcrime.securesms.util.CachedInflater;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CachedInflater$ViewCache$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ CachedInflater.ViewCache f$0;
    public final /* synthetic */ long f$1;
    public final /* synthetic */ int f$2;
    public final /* synthetic */ AsyncLayoutInflater f$3;
    public final /* synthetic */ int f$4;
    public final /* synthetic */ ViewGroup f$5;

    public /* synthetic */ CachedInflater$ViewCache$$ExternalSyntheticLambda0(CachedInflater.ViewCache viewCache, long j, int i, AsyncLayoutInflater asyncLayoutInflater, int i2, ViewGroup viewGroup) {
        this.f$0 = viewCache;
        this.f$1 = j;
        this.f$2 = i;
        this.f$3 = asyncLayoutInflater;
        this.f$4 = i2;
        this.f$5 = viewGroup;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$cacheUntilLimit$1(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
    }
}
