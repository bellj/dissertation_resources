package org.thoughtcrime.securesms.util;

import android.app.Activity;
import android.os.Build;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;

/* loaded from: classes4.dex */
public final class SplashScreenUtil {
    private SplashScreenUtil() {
    }

    public static void setSplashScreenThemeIfNecessary(Activity activity, SettingsValues.Theme theme) {
        if (Build.VERSION.SDK_INT >= 31 && activity != null) {
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyvalue$SettingsValues$Theme[theme.ordinal()];
            if (i == 1) {
                activity.getSplashScreen().setSplashScreenTheme((int) R.style.Theme_Signal_DayNight_NoActionBar_LightSplash);
            } else if (i == 2) {
                activity.getSplashScreen().setSplashScreenTheme((int) R.style.Theme_Signal_DayNight_NoActionBar_DarkSplash);
            } else if (i == 3) {
                activity.getSplashScreen().setSplashScreenTheme(0);
            }
        }
    }

    /* renamed from: org.thoughtcrime.securesms.util.SplashScreenUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$keyvalue$SettingsValues$Theme;

        static {
            int[] iArr = new int[SettingsValues.Theme.values().length];
            $SwitchMap$org$thoughtcrime$securesms$keyvalue$SettingsValues$Theme = iArr;
            try {
                iArr[SettingsValues.Theme.LIGHT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$SettingsValues$Theme[SettingsValues.Theme.DARK.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$SettingsValues$Theme[SettingsValues.Theme.SYSTEM.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }
}
