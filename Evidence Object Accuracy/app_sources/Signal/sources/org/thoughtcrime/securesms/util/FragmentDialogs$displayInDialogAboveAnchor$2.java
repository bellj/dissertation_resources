package org.thoughtcrime.securesms.util;

import android.content.DialogInterface;
import android.view.View;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: FragmentDialogs.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "<anonymous parameter 0>", "Landroid/content/DialogInterface;", "<anonymous parameter 1>", "Landroid/view/View;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class FragmentDialogs$displayInDialogAboveAnchor$2 extends Lambda implements Function2<DialogInterface, View, Unit> {
    public static final FragmentDialogs$displayInDialogAboveAnchor$2 INSTANCE = new FragmentDialogs$displayInDialogAboveAnchor$2();

    FragmentDialogs$displayInDialogAboveAnchor$2() {
        super(2);
    }

    public final void invoke(DialogInterface dialogInterface, View view) {
        Intrinsics.checkNotNullParameter(dialogInterface, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(view, "<anonymous parameter 1>");
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
        invoke((DialogInterface) obj, (View) obj2);
        return Unit.INSTANCE;
    }
}
