package org.thoughtcrime.securesms.util.views;

import android.view.ViewStub;
import java.util.Objects;

/* loaded from: classes4.dex */
public class NullableStub<T> {
    private T view;
    private ViewStub viewStub;

    public NullableStub(ViewStub viewStub) {
        this.viewStub = viewStub;
    }

    private T get() {
        ViewStub viewStub = this.viewStub;
        if (viewStub != null && this.view == null) {
            this.view = (T) viewStub.inflate();
            this.viewStub = null;
        }
        return this.view;
    }

    public T require() {
        T t = get();
        Objects.requireNonNull(t);
        return t;
    }

    public boolean isResolvable() {
        return this.viewStub != null || resolved();
    }

    public boolean resolved() {
        return this.view != null;
    }
}
