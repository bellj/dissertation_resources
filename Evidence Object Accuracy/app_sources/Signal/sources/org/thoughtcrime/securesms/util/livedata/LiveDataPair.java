package org.thoughtcrime.securesms.util.livedata;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import org.signal.libsignal.protocol.util.Pair;

/* loaded from: classes4.dex */
public final class LiveDataPair<A, B> extends MediatorLiveData<Pair<A, B>> {
    private A a;
    private B b;

    public LiveDataPair(LiveData<A> liveData, LiveData<B> liveData2) {
        this(liveData, liveData2, null, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: androidx.lifecycle.LiveData<A> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: androidx.lifecycle.LiveData<B> */
    /* JADX WARN: Multi-variable type inference failed */
    public LiveDataPair(LiveData<A> liveData, LiveData<B> liveData2, A a, B b) {
        this.a = a;
        this.b = b;
        setValue(new Pair(a, b));
        if (liveData == liveData2) {
            addSource(liveData, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataPair$$ExternalSyntheticLambda0
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataPair.this.lambda$new$0(obj);
                }
            });
            return;
        }
        addSource(liveData, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataPair$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                LiveDataPair.this.lambda$new$1(obj);
            }
        });
        addSource(liveData2, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataPair$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                LiveDataPair.this.lambda$new$2(obj);
            }
        });
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$0(Object obj) {
        if (obj != 0) {
            this.a = obj;
            this.b = obj;
        }
        setValue(new Pair(obj, this.b));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$1(Object obj) {
        if (obj != 0) {
            this.a = obj;
        }
        setValue(new Pair(obj, this.b));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$2(Object obj) {
        if (obj != 0) {
            this.b = obj;
        }
        setValue(new Pair(this.a, obj));
    }
}
