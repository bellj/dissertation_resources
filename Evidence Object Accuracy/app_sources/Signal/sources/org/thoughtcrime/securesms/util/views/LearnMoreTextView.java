package org.thoughtcrime.securesms.util.views;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.CommunicationActions;

/* loaded from: classes4.dex */
public class LearnMoreTextView extends AppCompatTextView {
    private CharSequence baseText;
    private Spannable link;
    private int linkColor;
    private View.OnClickListener linkListener;
    private boolean visible;

    public LearnMoreTextView(Context context) {
        super(context);
        init();
    }

    public LearnMoreTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        setMovementMethod(LinkMovementMethod.getInstance());
        setLinkTextInternal(R.string.LearnMoreTextView_learn_more);
        setLinkColor(ContextCompat.getColor(getContext(), R.color.signal_colorOnSurface));
        this.visible = true;
    }

    @Override // android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        this.baseText = charSequence;
        setTextInternal(charSequence, bufferType);
    }

    @Override // android.widget.TextView
    public void setTextColor(int i) {
        super.setTextColor(i);
    }

    public void setOnLinkClickListener(View.OnClickListener onClickListener) {
        this.linkListener = onClickListener;
    }

    public void setLearnMoreVisible(boolean z) {
        this.visible = z;
        setTextInternal(this.baseText, z ? TextView.BufferType.SPANNABLE : TextView.BufferType.NORMAL);
    }

    public void setLearnMoreVisible(boolean z, int i) {
        setLinkTextInternal(i);
        this.visible = z;
        setTextInternal(this.baseText, z ? TextView.BufferType.SPANNABLE : TextView.BufferType.NORMAL);
    }

    public void setLink(String str) {
        setOnLinkClickListener(new OpenUrlOnClickListener(str));
    }

    public void setLinkColor(int i) {
        this.linkColor = i;
    }

    private void setLinkTextInternal(int i) {
        AnonymousClass1 r0 = new ClickableSpan() { // from class: org.thoughtcrime.securesms.util.views.LearnMoreTextView.1
            @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
            public void updateDrawState(TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setUnderlineText(false);
                textPaint.setColor(LearnMoreTextView.this.linkColor);
            }

            @Override // android.text.style.ClickableSpan
            public void onClick(View view) {
                if (LearnMoreTextView.this.linkListener != null) {
                    LearnMoreTextView.this.linkListener.onClick(view);
                }
            }
        };
        SpannableString spannableString = new SpannableString(getContext().getString(i));
        this.link = spannableString;
        spannableString.setSpan(r0, 0, spannableString.length(), 17);
    }

    private void setTextInternal(CharSequence charSequence, TextView.BufferType bufferType) {
        if (this.visible) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            spannableStringBuilder.append(charSequence).append(' ').append((CharSequence) this.link);
            super.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);
            return;
        }
        super.setText(charSequence, bufferType);
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class OpenUrlOnClickListener implements View.OnClickListener {
        private final String url;

        public OpenUrlOnClickListener(String str) {
            this.url = str;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            CommunicationActions.openBrowserLink(view.getContext(), this.url);
        }
    }
}
