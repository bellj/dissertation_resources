package org.thoughtcrime.securesms.util.livedata;

import android.os.Handler;
import android.os.Looper;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import com.annimon.stream.function.Predicate;
import j$.util.function.Function;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.util.concurrent.SerialMonoLifoExecutor;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public final class LiveDataUtil {

    /* loaded from: classes4.dex */
    public interface Combine<A, B, R> {
        R apply(A a, B b);
    }

    /* loaded from: classes4.dex */
    public interface Combine3<A, B, C, R> {
        R apply(A a, B b, C c);
    }

    /* loaded from: classes4.dex */
    public interface EqualityChecker<T> {
        boolean contentsMatch(T t, T t2);
    }

    public static /* synthetic */ boolean lambda$filterNotNull$0(Object obj) {
        return obj != null;
    }

    private LiveDataUtil() {
    }

    public static <A> LiveData<A> filterNotNull(LiveData<A> liveData) {
        return filter(liveData, new Predicate() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataUtil$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return LiveDataUtil.lambda$filterNotNull$0(obj);
            }
        });
    }

    public static <A> LiveData<A> filter(LiveData<A> liveData, Predicate<A> predicate) {
        MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, new Observer(mediatorLiveData) { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataUtil$$ExternalSyntheticLambda5
            public final /* synthetic */ MediatorLiveData f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                LiveDataUtil.lambda$filter$1(Predicate.this, this.f$1, obj);
            }
        });
        return mediatorLiveData;
    }

    public static /* synthetic */ void lambda$filter$1(Predicate predicate, MediatorLiveData mediatorLiveData, Object obj) {
        if (predicate.test(obj)) {
            mediatorLiveData.setValue(obj);
        }
    }

    public static <A, B> LiveData<B> mapAsync(LiveData<A> liveData, Function<A, B> function) {
        return mapAsync(SignalExecutors.BOUNDED, liveData, function);
    }

    public static <A, B> LiveData<B> mapAsync(Executor executor, LiveData<A> liveData, Function<A, B> function) {
        MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, new Observer(new SerialMonoLifoExecutor(executor), mediatorLiveData, function) { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataUtil$$ExternalSyntheticLambda7
            public final /* synthetic */ Executor f$0;
            public final /* synthetic */ MediatorLiveData f$1;
            public final /* synthetic */ Function f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                LiveDataUtil.lambda$mapAsync$3(this.f$0, this.f$1, this.f$2, obj);
            }
        });
        return mediatorLiveData;
    }

    public static /* synthetic */ void lambda$mapAsync$3(Executor executor, MediatorLiveData mediatorLiveData, Function function, Object obj) {
        executor.execute(new Runnable(function, obj) { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataUtil$$ExternalSyntheticLambda4
            public final /* synthetic */ Function f$1;
            public final /* synthetic */ Object f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LiveDataUtil.lambda$mapAsync$2(MediatorLiveData.this, this.f$1, this.f$2);
            }
        });
    }

    public static /* synthetic */ void lambda$mapAsync$2(MediatorLiveData mediatorLiveData, Function function, Object obj) {
        mediatorLiveData.postValue(function.apply(obj));
    }

    public static <A, B> LiveData<B> mapDistinct(LiveData<A> liveData, androidx.arch.core.util.Function<A, B> function) {
        return Transformations.distinctUntilChanged(Transformations.map(liveData, function));
    }

    public static <A, B, R> LiveData<R> combineLatest(LiveData<A> liveData, LiveData<B> liveData2, Combine<A, B, R> combine) {
        return new CombineLiveData(liveData, liveData2, combine);
    }

    public static <A, B, C, R> LiveData<R> combineLatest(LiveData<A> liveData, LiveData<B> liveData2, LiveData<C> liveData3, Combine3<A, B, C, R> combine3) {
        return new Combine3LiveData(liveData, liveData2, liveData3, combine3);
    }

    public static <T> LiveData<T> merge(List<LiveData<T>> list) {
        LinkedHashSet<LiveData> linkedHashSet = new LinkedHashSet(list.size());
        linkedHashSet.addAll(list);
        if (linkedHashSet.size() == 1) {
            return list.get(0);
        }
        MediatorLiveData mediatorLiveData = new MediatorLiveData();
        for (LiveData liveData : linkedHashSet) {
            mediatorLiveData.addSource(liveData, new LiveDataUtil$$ExternalSyntheticLambda1(mediatorLiveData));
        }
        return mediatorLiveData;
    }

    public static <T> LiveData<T> just(T t) {
        return new MutableLiveData(t);
    }

    public static <A> LiveData<A> empty() {
        return new MutableLiveData();
    }

    public static <T> LiveData<T> until(LiveData<T> liveData, LiveData<T> liveData2) {
        MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData2, new LiveDataUtil$$ExternalSyntheticLambda1(mediatorLiveData));
        mediatorLiveData.addSource(liveData, new Observer(liveData2) { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataUtil$$ExternalSyntheticLambda3
            public final /* synthetic */ LiveData f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                LiveDataUtil.lambda$until$4(MediatorLiveData.this, this.f$1, obj);
            }
        });
        return mediatorLiveData;
    }

    public static /* synthetic */ void lambda$until$4(MediatorLiveData mediatorLiveData, LiveData liveData, Object obj) {
        mediatorLiveData.removeSource(liveData);
        mediatorLiveData.setValue(obj);
    }

    public static <T> LiveData<T> skip(LiveData<T> liveData, int i) {
        return new MediatorLiveData<T>(i, liveData) { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataUtil.1
            int skipsRemaining;
            final /* synthetic */ int val$skip;
            final /* synthetic */ LiveData val$source;

            {
                this.val$skip = r1;
                this.val$source = r2;
                this.skipsRemaining = r1;
                addSource(r2, new LiveDataUtil$1$$ExternalSyntheticLambda0(this));
            }

            public /* synthetic */ void lambda$new$0(Object obj) {
                int i2 = this.skipsRemaining;
                if (i2 <= 0) {
                    setValue(obj);
                } else {
                    this.skipsRemaining = i2 - 1;
                }
            }
        };
    }

    public static <T> LiveData<T> delay(final long j, final T t) {
        return new MutableLiveData<T>() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataUtil.2
            boolean emittedValue;

            @Override // androidx.lifecycle.LiveData
            public void onActive() {
                if (!this.emittedValue) {
                    new Handler(Looper.getMainLooper()).postDelayed(new LiveDataUtil$2$$ExternalSyntheticLambda0(this, t), j);
                    this.emittedValue = true;
                }
            }

            public /* synthetic */ void lambda$onActive$0(Object obj) {
                setValue(obj);
            }
        };
    }

    public static <T> LiveData<T> never() {
        return new MutableLiveData();
    }

    public static <T, R> LiveData<T> distinctUntilChanged(LiveData<T> liveData, Function<T, R> function) {
        return distinctUntilChanged(liveData, new EqualityChecker() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataUtil$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.EqualityChecker
            public final boolean contentsMatch(Object obj, Object obj2) {
                return LiveDataUtil.lambda$distinctUntilChanged$5(Function.this, obj, obj2);
            }
        });
    }

    public static /* synthetic */ boolean lambda$distinctUntilChanged$5(Function function, Object obj, Object obj2) {
        return Objects.equals(function.apply(obj), function.apply(obj2));
    }

    public static <T> LiveData<T> distinctUntilChanged(LiveData<T> liveData, final EqualityChecker<T> equalityChecker) {
        final MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, new Observer<T>() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataUtil.3
            boolean firstChange = true;

            @Override // androidx.lifecycle.Observer
            public void onChanged(T t) {
                T value = mediatorLiveData.getValue();
                if (value != 0 || t != 0) {
                    if (this.firstChange || value == 0 || t == 0 || !equalityChecker.contentsMatch(value, t)) {
                        this.firstChange = false;
                        mediatorLiveData.setValue(t);
                    }
                }
            }
        });
        return mediatorLiveData;
    }

    public static <A> LiveData<A> until(LiveData<A> liveData, Predicate<A> predicate) {
        MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, new Observer(predicate, liveData) { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataUtil$$ExternalSyntheticLambda2
            public final /* synthetic */ Predicate f$1;
            public final /* synthetic */ LiveData f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                LiveDataUtil.lambda$until$6(MediatorLiveData.this, this.f$1, this.f$2, obj);
            }
        });
        return mediatorLiveData;
    }

    public static /* synthetic */ void lambda$until$6(MediatorLiveData mediatorLiveData, Predicate predicate, LiveData liveData, Object obj) {
        mediatorLiveData.setValue(obj);
        if (predicate.test(obj)) {
            mediatorLiveData.removeSource(liveData);
        }
    }

    /* loaded from: classes4.dex */
    public static final class CombineLiveData<A, B, R> extends MediatorLiveData<R> {
        private A a;
        private B b;

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: androidx.lifecycle.LiveData<A> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: androidx.lifecycle.LiveData<B> */
        /* JADX WARN: Multi-variable type inference failed */
        CombineLiveData(LiveData<A> liveData, LiveData<B> liveData2, Combine<A, B, R> combine) {
            if (liveData == liveData2) {
                addSource(liveData, new LiveDataUtil$CombineLiveData$$ExternalSyntheticLambda0(this, combine));
                return;
            }
            addSource(liveData, new LiveDataUtil$CombineLiveData$$ExternalSyntheticLambda1(this, combine));
            addSource(liveData2, new LiveDataUtil$CombineLiveData$$ExternalSyntheticLambda2(this, combine));
        }

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        public /* synthetic */ void lambda$new$0(Combine combine, Object obj) {
            if (obj != 0) {
                this.a = obj;
                this.b = obj;
                setValue(combine.apply(obj, obj));
            }
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        public /* synthetic */ void lambda$new$1(Combine combine, Object obj) {
            if (obj != 0) {
                this.a = obj;
                B b = this.b;
                if (b != null) {
                    setValue(combine.apply(obj, b));
                }
            }
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        public /* synthetic */ void lambda$new$2(Combine combine, Object obj) {
            if (obj != 0) {
                this.b = obj;
                A a = this.a;
                if (a != null) {
                    setValue(combine.apply(a, obj));
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class Combine3LiveData<A, B, C, R> extends MediatorLiveData<R> {
        private A a;
        private B b;
        private C c;

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: androidx.lifecycle.LiveData<A> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: androidx.lifecycle.LiveData<B> */
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: androidx.lifecycle.LiveData<C> */
        /* JADX WARN: Multi-variable type inference failed */
        Combine3LiveData(LiveData<A> liveData, LiveData<B> liveData2, LiveData<C> liveData3, Combine3<A, B, C, R> combine3) {
            Preconditions.checkArgument((liveData == liveData2 || liveData2 == liveData3 || liveData == liveData3) ? false : true);
            addSource(liveData, new LiveDataUtil$Combine3LiveData$$ExternalSyntheticLambda0(this, combine3));
            addSource(liveData2, new LiveDataUtil$Combine3LiveData$$ExternalSyntheticLambda1(this, combine3));
            addSource(liveData3, new LiveDataUtil$Combine3LiveData$$ExternalSyntheticLambda2(this, combine3));
        }

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        public /* synthetic */ void lambda$new$0(Combine3 combine3, Object obj) {
            C c;
            if (obj != 0) {
                this.a = obj;
                B b = this.b;
                if (b != null && (c = this.c) != null) {
                    setValue(combine3.apply(obj, b, c));
                }
            }
        }

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        public /* synthetic */ void lambda$new$1(Combine3 combine3, Object obj) {
            C c;
            if (obj != 0) {
                this.b = obj;
                A a = this.a;
                if (a != null && (c = this.c) != null) {
                    setValue(combine3.apply(a, obj, c));
                }
            }
        }

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        public /* synthetic */ void lambda$new$2(Combine3 combine3, Object obj) {
            B b;
            if (obj != 0) {
                this.c = obj;
                A a = this.a;
                if (a != null && (b = this.b) != null) {
                    setValue(combine3.apply(a, b, obj));
                }
            }
        }
    }
}
