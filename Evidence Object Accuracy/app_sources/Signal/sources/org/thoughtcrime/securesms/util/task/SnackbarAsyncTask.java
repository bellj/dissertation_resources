package org.thoughtcrime.securesms.util.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import androidx.lifecycle.Lifecycle;
import com.google.android.material.snackbar.Snackbar;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public abstract class SnackbarAsyncTask<Params> extends AsyncTask<Params, Void, Void> implements View.OnClickListener {
    private static final String TAG = Log.tag(SnackbarAsyncTask.class);
    private final Lifecycle lifecycle;
    private ProgressDialog progressDialog;
    private Params reversibleParameter;
    private final boolean showProgress;
    private final int snackbarActionColor;
    private final String snackbarActionText;
    private final int snackbarDuration;
    private final String snackbarText;
    private final View view;

    protected abstract void executeAction(Params params);

    protected abstract void reverseAction(Params params);

    public SnackbarAsyncTask(Lifecycle lifecycle, View view, String str, String str2, int i, int i2, boolean z) {
        this.lifecycle = lifecycle;
        this.view = view;
        this.snackbarText = str;
        this.snackbarActionText = str2;
        this.snackbarActionColor = i;
        this.snackbarDuration = i2;
        this.showProgress = z;
    }

    @Override // android.os.AsyncTask
    protected void onPreExecute() {
        if (this.showProgress) {
            this.progressDialog = ProgressDialog.show(this.view.getContext(), "", "", true);
        } else {
            this.progressDialog = null;
        }
    }

    @Override // android.os.AsyncTask
    @SafeVarargs
    public final Void doInBackground(Params... paramsArr) {
        Params params = (paramsArr == null || paramsArr.length <= 0) ? null : paramsArr[0];
        this.reversibleParameter = params;
        executeAction(params);
        return null;
    }

    public void onPostExecute(Void r3) {
        ProgressDialog progressDialog;
        if (this.showProgress && (progressDialog = this.progressDialog) != null) {
            progressDialog.dismiss();
            this.progressDialog = null;
        }
        if (!this.lifecycle.getCurrentState().isAtLeast(Lifecycle.State.CREATED)) {
            Log.w(TAG, "Not in at least created state. Refusing to show snack bar.");
        } else {
            Snackbar.make(this.view, this.snackbarText, this.snackbarDuration).setAction(this.snackbarActionText, this).show();
        }
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        new AsyncTask<Void, Void, Void>() { // from class: org.thoughtcrime.securesms.util.task.SnackbarAsyncTask.1
            @Override // android.os.AsyncTask
            protected void onPreExecute() {
                if (SnackbarAsyncTask.this.showProgress) {
                    SnackbarAsyncTask snackbarAsyncTask = SnackbarAsyncTask.this;
                    snackbarAsyncTask.progressDialog = ProgressDialog.show(snackbarAsyncTask.view.getContext(), "", "", true);
                    return;
                }
                SnackbarAsyncTask.this.progressDialog = null;
            }

            /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: org.thoughtcrime.securesms.util.task.SnackbarAsyncTask */
            /* JADX WARN: Multi-variable type inference failed */
            public Void doInBackground(Void... voidArr) {
                SnackbarAsyncTask snackbarAsyncTask = SnackbarAsyncTask.this;
                snackbarAsyncTask.reverseAction(snackbarAsyncTask.reversibleParameter);
                return null;
            }

            public void onPostExecute(Void r2) {
                if (SnackbarAsyncTask.this.showProgress && SnackbarAsyncTask.this.progressDialog != null) {
                    SnackbarAsyncTask.this.progressDialog.dismiss();
                    SnackbarAsyncTask.this.progressDialog = null;
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }
}
