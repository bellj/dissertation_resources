package org.thoughtcrime.securesms.util.adapter.mapping;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import org.signal.paging.PagingController;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class PagingMappingAdapter<Key> extends MappingAdapter {
    private PagingController<Key> pagingController;

    public PagingMappingAdapter() {
        this(1, ViewUtil.dpToPx(100));
    }

    public PagingMappingAdapter(int i, int i2) {
        registerFactory(Placeholder.class, new Factory(i, i2) { // from class: org.thoughtcrime.securesms.util.adapter.mapping.PagingMappingAdapter$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$0;
            public final /* synthetic */ int f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.util.adapter.mapping.Factory
            public final MappingViewHolder createViewHolder(ViewGroup viewGroup) {
                return PagingMappingAdapter.lambda$new$0(this.f$0, this.f$1, viewGroup);
            }
        });
    }

    public static /* synthetic */ MappingViewHolder lambda$new$0(int i, int i2, ViewGroup viewGroup) {
        FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(i, i2));
        return new MappingViewHolder.SimpleViewHolder(frameLayout);
    }

    public void setPagingController(PagingController<Key> pagingController) {
        this.pagingController = pagingController;
    }

    @Override // androidx.recyclerview.widget.ListAdapter
    public MappingModel<?> getItem(int i) {
        PagingController<Key> pagingController = this.pagingController;
        if (pagingController != null) {
            pagingController.onDataNeededAroundIndex(i);
        }
        if (i < 0 || i >= super.getCurrentList().size()) {
            return null;
        }
        return (MappingModel) super.getItem(i);
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        MappingModel<?> item = getItem(i);
        if (item == null) {
            return this.itemTypes.get(Placeholder.class).intValue();
        }
        Integer num = this.itemTypes.get(item.getClass());
        if (num != null) {
            return num.intValue();
        }
        throw new AssertionError("No view holder factory for type: " + item.getClass());
    }

    public boolean hasItem(int i) {
        return getItem(i) != null;
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class Placeholder implements MappingModel<Placeholder> {
        public boolean areContentsTheSame(Placeholder placeholder) {
            return false;
        }

        public boolean areItemsTheSame(Placeholder placeholder) {
            return false;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Placeholder placeholder) {
            return MappingModel.CC.$default$getChangePayload(this, placeholder);
        }

        private Placeholder() {
        }
    }
}
