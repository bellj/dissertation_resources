package org.thoughtcrime.securesms.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.Window;

/* loaded from: classes4.dex */
public final class WindowUtil {
    private WindowUtil() {
    }

    public static void setLightNavigationBarFromTheme(Activity activity) {
        if (Build.VERSION.SDK_INT >= 27) {
            if (ThemeUtil.getThemedBoolean(activity, 16844140)) {
                setLightNavigationBar(activity.getWindow());
            } else {
                clearLightNavigationBar(activity.getWindow());
            }
        }
    }

    public static void clearLightNavigationBar(Window window) {
        if (Build.VERSION.SDK_INT >= 27) {
            clearSystemUiFlags(window, 16);
        }
    }

    public static void setLightNavigationBar(Window window) {
        if (Build.VERSION.SDK_INT >= 27) {
            setSystemUiFlags(window, 16);
        }
    }

    public static void setNavigationBarColor(Activity activity, int i) {
        setNavigationBarColor(activity, activity.getWindow(), i);
    }

    public static void setNavigationBarColor(Context context, Window window, int i) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21) {
            if (i2 < 27) {
                window.setNavigationBarColor(ThemeUtil.getThemedColor(context, 16843858));
            } else {
                window.setNavigationBarColor(i);
            }
        }
    }

    public static void setLightStatusBarFromTheme(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ThemeUtil.getThemedBoolean(activity, 16844000)) {
                setLightStatusBar(activity.getWindow());
            } else {
                clearLightStatusBar(activity.getWindow());
            }
        }
    }

    public static void clearLightStatusBar(Window window) {
        if (Build.VERSION.SDK_INT >= 23) {
            clearSystemUiFlags(window, 8192);
        }
    }

    public static void setLightStatusBar(Window window) {
        if (Build.VERSION.SDK_INT >= 23) {
            setSystemUiFlags(window, 8192);
        }
    }

    public static void setStatusBarColor(Window window, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            window.setStatusBarColor(i);
        }
    }

    public static boolean isStatusBarPresent(Window window) {
        Rect rect = new Rect();
        window.getDecorView().getWindowVisibleDisplayFrame(rect);
        return rect.top > 0;
    }

    private static void clearSystemUiFlags(Window window, int i) {
        View decorView = window.getDecorView();
        decorView.setSystemUiVisibility((i ^ -1) & decorView.getSystemUiVisibility());
    }

    private static void setSystemUiFlags(Window window, int i) {
        View decorView = window.getDecorView();
        decorView.setSystemUiVisibility(i | decorView.getSystemUiVisibility());
    }
}
