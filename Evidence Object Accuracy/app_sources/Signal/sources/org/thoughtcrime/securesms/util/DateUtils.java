package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.os.Build;
import android.text.format.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class DateUtils extends android.text.format.DateUtils {
    private static final ThreadLocal<SimpleDateFormat> BRIEF_EXACT_FORMAT = new ThreadLocal<>();
    private static final ThreadLocal<SimpleDateFormat> DATE_FORMAT = new ThreadLocal<>();
    private static final long MAX_RELATIVE_TIMESTAMP = TimeUnit.MINUTES.toMillis(3);
    private static final String TAG = Log.tag(DateUtils.class);

    private static boolean isWithin(long j, long j2, TimeUnit timeUnit) {
        return System.currentTimeMillis() - j <= timeUnit.toMillis(j2);
    }

    private static boolean isWithinAbs(long j, long j2, TimeUnit timeUnit) {
        return Math.abs(System.currentTimeMillis() - j) <= timeUnit.toMillis(j2);
    }

    private static boolean isYesterday(long j) {
        return android.text.format.DateUtils.isToday(j + TimeUnit.DAYS.toMillis(1));
    }

    private static int convertDelta(long j, TimeUnit timeUnit) {
        return (int) timeUnit.convert(System.currentTimeMillis() - j, TimeUnit.MILLISECONDS);
    }

    private static String getFormattedDateTime(long j, String str, Locale locale) {
        return setLowercaseAmPmStrings(new SimpleDateFormat(getLocalizedPattern(str, locale), locale), locale).format(new Date(j));
    }

    public static String getBriefRelativeTimeSpanString(Context context, Locale locale, long j) {
        TimeUnit timeUnit = TimeUnit.MINUTES;
        if (isWithin(j, 1, timeUnit)) {
            return context.getString(R.string.DateUtils_just_now);
        }
        TimeUnit timeUnit2 = TimeUnit.HOURS;
        if (isWithin(j, 1, timeUnit2)) {
            return context.getResources().getString(R.string.DateUtils_minutes_ago, Integer.valueOf(convertDelta(j, timeUnit)));
        }
        TimeUnit timeUnit3 = TimeUnit.DAYS;
        if (isWithin(j, 1, timeUnit3)) {
            int convertDelta = convertDelta(j, timeUnit2);
            return context.getResources().getQuantityString(R.plurals.hours_ago, convertDelta, Integer.valueOf(convertDelta));
        } else if (isWithin(j, 6, timeUnit3)) {
            return getFormattedDateTime(j, "EEE", locale);
        } else {
            if (isWithin(j, 365, timeUnit3)) {
                return getFormattedDateTime(j, "MMM d", locale);
            }
            return getFormattedDateTime(j, "MMM d, yyyy", locale);
        }
    }

    public static String getExtendedRelativeTimeSpanString(Context context, Locale locale, long j) {
        TimeUnit timeUnit = TimeUnit.MINUTES;
        if (isWithin(j, 1, timeUnit)) {
            return context.getString(R.string.DateUtils_just_now);
        }
        if (isWithin(j, 1, TimeUnit.HOURS)) {
            return context.getResources().getString(R.string.DateUtils_minutes_ago, Integer.valueOf((int) timeUnit.convert(System.currentTimeMillis() - j, TimeUnit.MILLISECONDS)));
        }
        StringBuilder sb = new StringBuilder();
        TimeUnit timeUnit2 = TimeUnit.DAYS;
        if (isWithin(j, 6, timeUnit2)) {
            sb.append("EEE ");
        } else if (isWithin(j, 365, timeUnit2)) {
            sb.append("MMM d, ");
        } else {
            sb.append("MMM d, yyyy, ");
        }
        if (DateFormat.is24HourFormat(context)) {
            sb.append("HH:mm");
        } else {
            sb.append("hh:mm a");
        }
        return getFormattedDateTime(j, sb.toString(), locale);
    }

    public static String getSimpleRelativeTimeSpanString(Context context, Locale locale, long j) {
        TimeUnit timeUnit = TimeUnit.MINUTES;
        if (isWithin(j, 1, timeUnit)) {
            return context.getString(R.string.DateUtils_just_now);
        }
        if (isWithin(j, 1, TimeUnit.HOURS)) {
            return context.getResources().getString(R.string.DateUtils_minutes_ago, Integer.valueOf((int) timeUnit.convert(System.currentTimeMillis() - j, TimeUnit.MILLISECONDS)));
        }
        return getFormattedDateTime(j, DateFormat.is24HourFormat(context) ? "HH:mm" : "hh:mm a", locale);
    }

    public static String getTimeString(Context context, Locale locale, long j) {
        StringBuilder sb = new StringBuilder();
        if (isSameDay(System.currentTimeMillis(), j)) {
            sb.append("");
        } else {
            TimeUnit timeUnit = TimeUnit.DAYS;
            if (isWithinAbs(j, 6, timeUnit)) {
                sb.append("EEE ");
            } else if (isWithinAbs(j, 364, timeUnit)) {
                sb.append("MMM d, ");
            } else {
                sb.append("MMM d, yyyy, ");
            }
        }
        if (DateFormat.is24HourFormat(context)) {
            sb.append("HH:mm");
        } else {
            sb.append("hh:mm a");
        }
        return getFormattedDateTime(j, sb.toString(), locale);
    }

    public static String getDayPrecisionTimeSpanString(Context context, Locale locale, long j) {
        String str;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        if (simpleDateFormat.format(Long.valueOf(System.currentTimeMillis())).equals(simpleDateFormat.format(Long.valueOf(j)))) {
            return context.getString(R.string.DeviceListItem_today);
        }
        TimeUnit timeUnit = TimeUnit.DAYS;
        if (isWithin(j, 6, timeUnit)) {
            str = "EEE ";
        } else {
            str = isWithin(j, 365, timeUnit) ? "MMM d" : "MMM d, yyy";
        }
        return getFormattedDateTime(j, str, locale);
    }

    public static SimpleDateFormat getDetailedDateFormatter(Context context, Locale locale) {
        String str;
        if (DateFormat.is24HourFormat(context)) {
            str = getLocalizedPattern("MMM d, yyyy HH:mm:ss zzz", locale);
        } else {
            str = getLocalizedPattern("MMM d, yyyy hh:mm:ss a zzz", locale);
        }
        return new SimpleDateFormat(str, locale);
    }

    public static String getConversationDateHeaderString(Context context, Locale locale, long j) {
        if (android.text.format.DateUtils.isToday(j)) {
            return context.getString(R.string.DateUtils_today);
        }
        if (isYesterday(j)) {
            return context.getString(R.string.DateUtils_yesterday);
        }
        if (isWithin(j, 182, TimeUnit.DAYS)) {
            return formatDateWithDayOfWeek(locale, j);
        }
        return formatDateWithYear(locale, j);
    }

    public static String formatDateWithDayOfWeek(Locale locale, long j) {
        return getFormattedDateTime(j, "EEE, MMM d", locale);
    }

    public static String formatDateWithYear(Locale locale, long j) {
        return getFormattedDateTime(j, "MMM d, yyyy", locale);
    }

    public static String formatDate(Locale locale, long j) {
        return getFormattedDateTime(j, "EEE, MMM d, yyyy", locale);
    }

    public static String formatDateWithoutDayOfWeek(Locale locale, long j) {
        return getFormattedDateTime(j, "MMM d yyyy", locale);
    }

    public static boolean isSameDay(long j, long j2) {
        return getDateFormat().format(new Date(j)).equals(getDateFormat().format(new Date(j2)));
    }

    public static boolean isSameExtendedRelativeTimestamp(long j, long j2) {
        return j - j2 < MAX_RELATIVE_TIMESTAMP;
    }

    private static String getLocalizedPattern(String str, Locale locale) {
        return DateFormat.getBestDateTimePattern(locale, str);
    }

    private static SimpleDateFormat setLowercaseAmPmStrings(SimpleDateFormat simpleDateFormat, Locale locale) {
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(locale);
        dateFormatSymbols.setAmPmStrings(new String[]{"am", "pm"});
        simpleDateFormat.setDateFormatSymbols(dateFormatSymbols);
        return simpleDateFormat;
    }

    public static long parseIso8601(String str) {
        SimpleDateFormat simpleDateFormat;
        if (Build.VERSION.SDK_INT >= 24) {
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX", Locale.getDefault());
        } else {
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
        }
        if (Util.isEmpty(str)) {
            return -1;
        }
        try {
            return simpleDateFormat.parse(str).getTime();
        } catch (ParseException e) {
            Log.w(TAG, "Failed to parse date.", e);
            return -1;
        }
    }

    private static SimpleDateFormat getDateFormat() {
        ThreadLocal<SimpleDateFormat> threadLocal = DATE_FORMAT;
        SimpleDateFormat simpleDateFormat = threadLocal.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyyMMdd");
        threadLocal.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    private static SimpleDateFormat getBriefExactFormat() {
        ThreadLocal<SimpleDateFormat> threadLocal = BRIEF_EXACT_FORMAT;
        SimpleDateFormat simpleDateFormat = threadLocal.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat();
        threadLocal.set(simpleDateFormat2);
        return simpleDateFormat2;
    }
}
