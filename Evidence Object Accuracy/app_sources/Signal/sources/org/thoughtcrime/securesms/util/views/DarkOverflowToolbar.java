package org.thoughtcrime.securesms.util.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class DarkOverflowToolbar extends Toolbar {
    public DarkOverflowToolbar(Context context) {
        super(context);
        init();
    }

    public DarkOverflowToolbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public DarkOverflowToolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void init() {
        if (getOverflowIcon() != null) {
            getOverflowIcon().setColorFilter(ContextCompat.getColor(getContext(), R.color.signal_icon_tint_primary), PorterDuff.Mode.SRC_ATOP);
        }
    }
}
