package org.thoughtcrime.securesms.util.fragments;

import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ListenerExtensions.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u001c\u0010\u0000\u001a\u0004\u0018\u0001H\u0001\"\u0006\b\u0000\u0010\u0001\u0018\u0001*\u00020\u0002H\b¢\u0006\u0002\u0010\u0003\u001a\u001a\u0010\u0004\u001a\u0002H\u0001\"\u0006\b\u0000\u0010\u0001\u0018\u0001*\u00020\u0002H\b¢\u0006\u0002\u0010\u0003¨\u0006\u0005"}, d2 = {"findListener", "T", "Landroidx/fragment/app/Fragment;", "(Landroidx/fragment/app/Fragment;)Ljava/lang/Object;", "requireListener", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ListenerExtensionsKt {
    public static final /* synthetic */ <T> T findListener(Fragment fragment) {
        Intrinsics.checkNotNullParameter(fragment, "<this>");
        T t = (T) fragment.getParentFragment();
        if (t != null) {
            Intrinsics.reifiedOperationMarker(3, "T");
            return t;
        }
        T t2 = (T) fragment.requireActivity();
        Intrinsics.reifiedOperationMarker(2, "T");
        return t2;
    }

    public static final /* synthetic */ <T> T requireListener(Fragment fragment) {
        Intrinsics.checkNotNullParameter(fragment, "<this>");
        ArrayList arrayList = new ArrayList();
        try {
            T t = (T) fragment.getParentFragment();
            if (t != null) {
                Intrinsics.reifiedOperationMarker(3, "T");
                return t;
            }
            T t2 = (T) fragment.requireActivity();
            Intrinsics.reifiedOperationMarker(1, "T");
            return t2;
        } catch (ClassCastException e) {
            String name = fragment.requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name, "requireActivity()::class.java.name");
            arrayList.add(name);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }
}
