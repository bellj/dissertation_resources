package org.thoughtcrime.securesms.util;

import java.io.IOException;

/* loaded from: classes4.dex */
public final class Base64 {
    private Base64() {
    }

    public static byte[] decode(String str) throws IOException {
        return org.whispersystems.util.Base64.decode(str);
    }

    public static String encodeBytes(byte[] bArr) {
        return org.whispersystems.util.Base64.encodeBytes(bArr);
    }

    public static byte[] decodeOrThrow(String str) {
        try {
            return org.whispersystems.util.Base64.decode(str);
        } catch (IOException unused) {
            throw new AssertionError();
        }
    }

    public static byte[] decodeNullableOrThrow(String str) {
        if (str == null) {
            return null;
        }
        try {
            return org.whispersystems.util.Base64.decode(str);
        } catch (IOException unused) {
            throw new AssertionError();
        }
    }
}
