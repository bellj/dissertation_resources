package org.thoughtcrime.securesms.util.viewholders;

import android.view.View;
import android.widget.TextView;
import j$.util.function.Function;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.adapter.mapping.Factory;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel;
import org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder;

/* loaded from: classes4.dex */
public class RecipientViewHolder<T extends RecipientMappingModel<T>> extends MappingViewHolder<T> {
    protected final AvatarImageView avatar;
    protected final BadgeImageView badge;
    protected final EventListener<T> eventListener;
    protected final TextView name;
    private final boolean quickContactEnabled;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder<T extends org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel<T>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
    public /* bridge */ /* synthetic */ void bind(Object obj) {
        bind((RecipientViewHolder<T>) ((RecipientMappingModel) obj));
    }

    public RecipientViewHolder(View view, EventListener<T> eventListener) {
        this(view, eventListener, false);
    }

    public RecipientViewHolder(View view, EventListener<T> eventListener, boolean z) {
        super(view);
        this.eventListener = eventListener;
        this.quickContactEnabled = z;
        this.avatar = (AvatarImageView) findViewById(R.id.recipient_view_avatar);
        this.badge = (BadgeImageView) findViewById(R.id.recipient_view_badge);
        this.name = (TextView) findViewById(R.id.recipient_view_name);
    }

    public void bind(T t) {
        AvatarImageView avatarImageView = this.avatar;
        if (avatarImageView != null) {
            avatarImageView.setRecipient(t.getRecipient(), this.quickContactEnabled);
        }
        BadgeImageView badgeImageView = this.badge;
        if (badgeImageView != null) {
            badgeImageView.setBadgeFromRecipient(t.getRecipient());
        }
        TextView textView = this.name;
        if (textView != null) {
            textView.setText(t.getName(this.context));
        }
        if (this.eventListener != null) {
            this.itemView.setOnClickListener(new View.OnClickListener(t) { // from class: org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder$$ExternalSyntheticLambda0
                public final /* synthetic */ RecipientMappingModel f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    RecipientViewHolder.this.lambda$bind$0(this.f$1, view);
                }
            });
        } else {
            this.itemView.setOnClickListener(null);
        }
    }

    public /* synthetic */ void lambda$bind$0(RecipientMappingModel recipientMappingModel, View view) {
        this.eventListener.onModelClick(recipientMappingModel);
    }

    public static <T extends RecipientMappingModel<T>> Factory<T> createFactory(int i, EventListener<T> eventListener) {
        return new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientViewHolder.lambda$createFactory$1(RecipientViewHolder.EventListener.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, i);
    }

    public static /* synthetic */ MappingViewHolder lambda$createFactory$1(EventListener eventListener, View view) {
        return new RecipientViewHolder(view, eventListener);
    }

    /* loaded from: classes4.dex */
    public interface EventListener<T extends RecipientMappingModel<T>> {
        void onClick(Recipient recipient);

        void onModelClick(T t);

        /* renamed from: org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder$EventListener$-CC */
        /* loaded from: classes4.dex */
        public final /* synthetic */ class CC<T extends RecipientMappingModel<T>> {
            public static void $default$onModelClick(EventListener eventListener, RecipientMappingModel recipientMappingModel) {
                eventListener.onClick(recipientMappingModel.getRecipient());
            }
        }
    }
}
