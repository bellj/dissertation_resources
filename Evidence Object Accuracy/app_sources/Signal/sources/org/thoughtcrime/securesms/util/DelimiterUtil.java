package org.thoughtcrime.securesms.util;

import java.util.regex.Pattern;

/* loaded from: classes4.dex */
public class DelimiterUtil {
    public static String escape(String str, char c) {
        return str.replace("" + c, "\\" + c);
    }

    public static String unescape(String str, char c) {
        return str.replace("\\" + c, "" + c);
    }

    public static String[] split(String str, char c) {
        if (str == null || str.length() == 0) {
            return new String[0];
        }
        return str.split("(?<!\\\\)" + Pattern.quote(c + ""));
    }
}
