package org.thoughtcrime.securesms.util;

import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class Stopwatch {
    private final List<Split> splits;
    private final long startTime = System.currentTimeMillis();
    private final String title;

    public Stopwatch(String str) {
        this.title = str;
        this.splits = new LinkedList();
    }

    public void split(String str) {
        this.splits.add(new Split(System.currentTimeMillis(), str));
    }

    public void stop(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.title);
        sb.append("] ");
        if (this.splits.size() > 0) {
            sb.append(this.splits.get(0).label);
            sb.append(": ");
            sb.append(this.splits.get(0).time - this.startTime);
            sb.append("  ");
        }
        if (this.splits.size() > 1) {
            for (int i = 1; i < this.splits.size(); i++) {
                sb.append(this.splits.get(i).label);
                sb.append(": ");
                sb.append(this.splits.get(i).time - this.splits.get(i - 1).time);
                sb.append("  ");
            }
            sb.append("total: ");
            List<Split> list = this.splits;
            sb.append(list.get(list.size() - 1).time - this.startTime);
        }
        Log.d(str, sb.toString());
    }

    /* loaded from: classes4.dex */
    public static class Split {
        final String label;
        final long time;

        Split(long j, String str) {
            this.time = j;
            this.label = str;
        }
    }
}
