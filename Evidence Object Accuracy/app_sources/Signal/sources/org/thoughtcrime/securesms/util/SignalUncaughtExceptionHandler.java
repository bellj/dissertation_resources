package org.thoughtcrime.securesms.util;

import io.reactivex.rxjava3.exceptions.OnErrorNotImplementedException;
import java.lang.Thread;
import org.signal.core.util.ExceptionUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes.dex */
public class SignalUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    private static final String TAG = Log.tag(SignalUncaughtExceptionHandler.class);
    private final Thread.UncaughtExceptionHandler originalHandler;

    public SignalUncaughtExceptionHandler(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.originalHandler = uncaughtExceptionHandler;
    }

    @Override // java.lang.Thread.UncaughtExceptionHandler
    public void uncaughtException(Thread thread, Throwable th) {
        if ((th instanceof OnErrorNotImplementedException) && th.getCause() != null) {
            th = th.getCause();
        }
        Log.e(TAG, "", th, true);
        SignalStore.blockUntilAllWritesFinished();
        Log.blockUntilAllWritesFinished();
        ApplicationDependencies.getJobManager().flush();
        this.originalHandler.uncaughtException(thread, ExceptionUtil.joinStackTraceAndMessage(th));
    }
}
