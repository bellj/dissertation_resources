package org.thoughtcrime.securesms.util;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LeakyBucketLimiter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ LeakyBucketLimiter f$0;

    public /* synthetic */ LeakyBucketLimiter$$ExternalSyntheticLambda0(LeakyBucketLimiter leakyBucketLimiter) {
        this.f$0 = leakyBucketLimiter;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.drip();
    }
}
