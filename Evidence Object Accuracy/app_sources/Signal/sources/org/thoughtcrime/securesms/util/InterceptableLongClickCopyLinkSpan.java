package org.thoughtcrime.securesms.util;

import android.view.View;

/* loaded from: classes4.dex */
public final class InterceptableLongClickCopyLinkSpan extends LongClickCopySpan {
    private final UrlClickHandler onClickListener;

    public InterceptableLongClickCopyLinkSpan(String str, UrlClickHandler urlClickHandler) {
        this(str, urlClickHandler, null, true);
    }

    public InterceptableLongClickCopyLinkSpan(String str, UrlClickHandler urlClickHandler, Integer num, boolean z) {
        super(str, num, z);
        this.onClickListener = urlClickHandler;
    }

    @Override // android.text.style.URLSpan, android.text.style.ClickableSpan
    public void onClick(View view) {
        if (!this.onClickListener.handleOnClick(getURL())) {
            super.onClick(view);
        }
    }
}
