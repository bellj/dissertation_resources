package org.thoughtcrime.securesms.util;

import org.signal.core.util.ThreadUtil;

/* loaded from: classes4.dex */
public final class AsynchronousCallback {

    /* loaded from: classes4.dex */
    public interface WorkerThread<R, E> {
        void onComplete(R r);

        void onError(E e);
    }

    /* loaded from: classes4.dex */
    public interface MainThread<R, E> {
        void onComplete(R r);

        void onError(E e);

        WorkerThread<R, E> toWorkerCallback();

        /* renamed from: org.thoughtcrime.securesms.util.AsynchronousCallback$MainThread$-CC */
        /* loaded from: classes4.dex */
        public final /* synthetic */ class CC<R, E> {
            public static WorkerThread $default$toWorkerCallback(MainThread mainThread) {
                return new WorkerThread<R, E>() { // from class: org.thoughtcrime.securesms.util.AsynchronousCallback.MainThread.1
                    public /* synthetic */ void lambda$onComplete$0(Object obj) {
                        MainThread.this.onComplete(obj);
                    }

                    @Override // org.thoughtcrime.securesms.util.AsynchronousCallback.WorkerThread
                    public void onComplete(R r) {
                        ThreadUtil.runOnMain(new AsynchronousCallback$MainThread$1$$ExternalSyntheticLambda1(this, r));
                    }

                    public /* synthetic */ void lambda$onError$1(Object obj) {
                        MainThread.this.onError(obj);
                    }

                    @Override // org.thoughtcrime.securesms.util.AsynchronousCallback.WorkerThread
                    public void onError(E e) {
                        ThreadUtil.runOnMain(new AsynchronousCallback$MainThread$1$$ExternalSyntheticLambda0(this, e));
                    }
                };
            }
        }
    }
}
