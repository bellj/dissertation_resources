package org.thoughtcrime.securesms.util;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

/* loaded from: classes4.dex */
public class NoCrossfadeChangeDefaultAnimator extends DefaultItemAnimator {
    @Override // androidx.recyclerview.widget.DefaultItemAnimator, androidx.recyclerview.widget.RecyclerView.ItemAnimator
    public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder, List<Object> list) {
        return true;
    }

    @Override // androidx.recyclerview.widget.DefaultItemAnimator, androidx.recyclerview.widget.SimpleItemAnimator
    public boolean animateChange(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4) {
        if (viewHolder != viewHolder2) {
            if (viewHolder != null) {
                dispatchChangeFinished(viewHolder, true);
            }
            if (viewHolder2 != null) {
                dispatchChangeFinished(viewHolder2, false);
            }
        } else if (viewHolder != null) {
            dispatchChangeFinished(viewHolder, true);
        }
        return false;
    }
}
