package org.thoughtcrime.securesms.util;

import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: FragmentDialogs.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J@\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\t\u001a\u00020\n2\u001a\b\u0002\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\r0\fJB\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\t\u001a\u00020\n2\u001a\b\u0002\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\r0\f¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/util/FragmentDialogs;", "", "()V", "displayInDialogAboveAnchor", "Landroid/content/DialogInterface;", "Landroidx/fragment/app/Fragment;", "anchorView", "Landroid/view/View;", "contentView", "windowDim", "", "onShow", "Lkotlin/Function2;", "", "contentLayoutId", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class FragmentDialogs {
    public static final FragmentDialogs INSTANCE = new FragmentDialogs();

    private FragmentDialogs() {
    }

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: org.thoughtcrime.securesms.util.FragmentDialogs */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ DialogInterface displayInDialogAboveAnchor$default(FragmentDialogs fragmentDialogs, Fragment fragment, View view, int i, float f, Function2 function2, int i2, Object obj) {
        float f2 = (i2 & 4) != 0 ? -1.0f : f;
        if ((i2 & 8) != 0) {
            function2 = FragmentDialogs$displayInDialogAboveAnchor$1.INSTANCE;
        }
        return fragmentDialogs.displayInDialogAboveAnchor(fragment, view, i, f2, function2);
    }

    public final DialogInterface displayInDialogAboveAnchor(Fragment fragment, View view, int i, float f, Function2<? super DialogInterface, ? super View, Unit> function2) {
        Intrinsics.checkNotNullParameter(fragment, "<this>");
        Intrinsics.checkNotNullParameter(view, "anchorView");
        Intrinsics.checkNotNullParameter(function2, "onShow");
        View inflate = LayoutInflater.from(view.getContext()).inflate(i, (ViewGroup) fragment.requireView(), false);
        inflate.measure(View.MeasureSpec.makeMeasureSpec(inflate.getLayoutParams().width, 1073741824), View.MeasureSpec.makeMeasureSpec(inflate.getLayoutParams().height, 1073741824));
        inflate.layout(0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
        Intrinsics.checkNotNullExpressionValue(inflate, "contentView");
        return displayInDialogAboveAnchor(fragment, view, inflate, f, function2);
    }

    /* renamed from: displayInDialogAboveAnchor$lambda-1 */
    public static final void m3248displayInDialogAboveAnchor$lambda1(Fragment fragment, DialogInterface dialogInterface) {
        StoryTextPostPreviewFragment.Callback callback;
        Intrinsics.checkNotNullParameter(fragment, "$this_displayInDialogAboveAnchor");
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment2 = fragment.getParentFragment();
            while (true) {
                if (fragment2 == null) {
                    FragmentActivity requireActivity = fragment.requireActivity();
                    if (requireActivity != null) {
                        callback = (StoryTextPostPreviewFragment.Callback) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment.Callback");
                    }
                } else if (fragment2 instanceof StoryTextPostPreviewFragment.Callback) {
                    callback = fragment2;
                    break;
                } else {
                    String name = fragment2.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment2 = fragment2.getParentFragment();
                }
            }
            callback.setIsDisplayingLinkPreviewTooltip(false);
        } catch (ClassCastException e) {
            String name2 = fragment.requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: org.thoughtcrime.securesms.util.FragmentDialogs */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ DialogInterface displayInDialogAboveAnchor$default(FragmentDialogs fragmentDialogs, Fragment fragment, View view, View view2, float f, Function2 function2, int i, Object obj) {
        float f2 = (i & 4) != 0 ? -1.0f : f;
        if ((i & 8) != 0) {
            function2 = FragmentDialogs$displayInDialogAboveAnchor$2.INSTANCE;
        }
        return fragmentDialogs.displayInDialogAboveAnchor(fragment, view, view2, f2, function2);
    }

    public final DialogInterface displayInDialogAboveAnchor(Fragment fragment, View view, View view2, float f, Function2<? super DialogInterface, ? super View, Unit> function2) {
        Intrinsics.checkNotNullParameter(fragment, "<this>");
        Intrinsics.checkNotNullParameter(view, "anchorView");
        Intrinsics.checkNotNullParameter(view2, "contentView");
        Intrinsics.checkNotNullParameter(function2, "onShow");
        AlertDialog create = new AlertDialog.Builder(fragment.requireContext()).setView(view2).create();
        Intrinsics.checkNotNullExpressionValue(create, "Builder(requireContext()…tentView)\n      .create()");
        Window window = create.getWindow();
        Intrinsics.checkNotNull(window);
        Window window2 = create.getWindow();
        Intrinsics.checkNotNull(window2);
        WindowManager.LayoutParams attributes = window2.getAttributes();
        Projection translateY = Projection.relativeToViewRoot(view, null).translateY(view.getTranslationY());
        Intrinsics.checkNotNullExpressionValue(translateY, "relativeToViewRoot(ancho…(anchorView.translationY)");
        attributes.y = (int) (translateY.getY() - ((float) view2.getHeight()));
        attributes.gravity = 48;
        translateY.release();
        window.setAttributes(attributes);
        if (f >= 0.0f) {
            Window window3 = create.getWindow();
            Intrinsics.checkNotNull(window3);
            window3.setDimAmount(f);
        }
        Window window4 = create.getWindow();
        Intrinsics.checkNotNull(window4);
        window4.setBackgroundDrawable(new ColorDrawable(0));
        create.setOnDismissListener(new DialogInterface.OnDismissListener() { // from class: org.thoughtcrime.securesms.util.FragmentDialogs$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                FragmentDialogs.m3248displayInDialogAboveAnchor$lambda1(Fragment.this, dialogInterface);
            }
        });
        create.setOnShowListener(new DialogInterface.OnShowListener(create, view2) { // from class: org.thoughtcrime.securesms.util.FragmentDialogs$$ExternalSyntheticLambda1
            public final /* synthetic */ AlertDialog f$1;
            public final /* synthetic */ View f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                FragmentDialogs.m3249displayInDialogAboveAnchor$lambda2(Function2.this, this.f$1, this.f$2, dialogInterface);
            }
        });
        create.show();
        return create;
    }

    /* renamed from: displayInDialogAboveAnchor$lambda-2 */
    public static final void m3249displayInDialogAboveAnchor$lambda2(Function2 function2, AlertDialog alertDialog, View view, DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(function2, "$onShow");
        Intrinsics.checkNotNullParameter(alertDialog, "$alertDialog");
        Intrinsics.checkNotNullParameter(view, "$contentView");
        function2.invoke(alertDialog, view);
    }
}
