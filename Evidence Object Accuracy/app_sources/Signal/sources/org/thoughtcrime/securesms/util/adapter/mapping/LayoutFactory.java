package org.thoughtcrime.securesms.util.adapter.mapping;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import j$.util.function.Function;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes4.dex */
public class LayoutFactory<T extends MappingModel<T>> implements Factory<T> {
    private Function<View, MappingViewHolder<T>> creator;
    private final int layout;

    public LayoutFactory(Function<View, MappingViewHolder<T>> function, int i) {
        this.creator = function;
        this.layout = i;
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.Factory
    public MappingViewHolder<T> createViewHolder(ViewGroup viewGroup) {
        return this.creator.apply(LayoutInflater.from(viewGroup.getContext()).inflate(this.layout, viewGroup, false));
    }
}
