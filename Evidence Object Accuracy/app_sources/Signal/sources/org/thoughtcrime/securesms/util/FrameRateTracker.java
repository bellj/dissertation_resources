package org.thoughtcrime.securesms.util;

import android.app.Application;
import android.content.Context;
import android.view.Choreographer;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;

/* loaded from: classes.dex */
public class FrameRateTracker {
    private static final int MAX_CONSECUTIVE_FRAME_LOGS;
    private static final String TAG = Log.tag(FrameRateTracker.class);
    private long badFrameThresholdNanos;
    private final Choreographer.FrameCallback calculator = new Choreographer.FrameCallback() { // from class: org.thoughtcrime.securesms.util.FrameRateTracker.1
        @Override // android.view.Choreographer.FrameCallback
        public void doFrame(long j) {
            long j2 = j - FrameRateTracker.this.lastFrameTimeNanos;
            double nanos = (double) TimeUnit.SECONDS.toNanos(1);
            double d = (double) j2;
            Double.isNaN(nanos);
            Double.isNaN(d);
            double d2 = nanos / d;
            if (j2 <= FrameRateTracker.this.badFrameThresholdNanos) {
                FrameRateTracker.this.consecutiveFrameWarnings = 0;
            } else if (FrameRateTracker.this.consecutiveFrameWarnings < 10) {
                Log.w(FrameRateTracker.TAG, String.format(Locale.ENGLISH, "Bad frame! Took %d ms (%d dropped frames, or %.2f FPS)", Long.valueOf(TimeUnit.NANOSECONDS.toMillis(j2)), Long.valueOf(j2 / FrameRateTracker.this.idealTimePerFrameNanos), Double.valueOf(d2)));
                FrameRateTracker.access$208(FrameRateTracker.this);
            }
            FrameRateTracker.this.lastFrameTimeNanos = j;
            Choreographer.getInstance().postFrameCallback(this);
        }
    };
    private long consecutiveFrameWarnings;
    private final Application context;
    private long idealTimePerFrameNanos;
    private long lastFrameTimeNanos;
    private double refreshRate;

    static /* synthetic */ long access$208(FrameRateTracker frameRateTracker) {
        long j = frameRateTracker.consecutiveFrameWarnings;
        frameRateTracker.consecutiveFrameWarnings = 1 + j;
        return j;
    }

    public FrameRateTracker(Application application) {
        this.context = application;
        updateRefreshRate();
    }

    public void start() {
        Log.d(TAG, String.format(Locale.ENGLISH, "Beginning frame rate tracking. Screen refresh rate: %.2f hz, or %.2f ms per frame.", Double.valueOf(this.refreshRate), Float.valueOf(((float) this.idealTimePerFrameNanos) / 1000000.0f)));
        this.lastFrameTimeNanos = System.nanoTime();
        Choreographer.getInstance().postFrameCallback(this.calculator);
    }

    public void stop() {
        Choreographer.getInstance().removeFrameCallback(this.calculator);
    }

    public static float getDisplayRefreshRate(Context context) {
        return ServiceUtil.getWindowManager(context).getDefaultDisplay().getRefreshRate();
    }

    private void updateRefreshRate() {
        double displayRefreshRate = (double) getDisplayRefreshRate(this.context);
        double d = this.refreshRate;
        if (d != displayRefreshRate) {
            if (d > 0.0d) {
                Log.d(TAG, String.format(Locale.ENGLISH, "Refresh rate changed from %.2f hz to %.2f hz", Double.valueOf(d), Double.valueOf(displayRefreshRate)));
            }
            this.refreshRate = (double) getDisplayRefreshRate(this.context);
            double nanos = (double) TimeUnit.SECONDS.toNanos(1);
            double d2 = this.refreshRate;
            Double.isNaN(nanos);
            long j = (long) (nanos / d2);
            this.idealTimePerFrameNanos = j;
            this.badFrameThresholdNanos = j * ((long) ((int) (d2 / 4.0d)));
        }
    }
}
