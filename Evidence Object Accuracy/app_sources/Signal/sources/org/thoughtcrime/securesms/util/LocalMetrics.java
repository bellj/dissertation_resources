package org.thoughtcrime.securesms.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.LocalMetricsDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.model.LocalMetricsEvent;
import org.thoughtcrime.securesms.database.model.LocalMetricsSplit;

/* compiled from: LocalMetrics.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004J\u0006\u0010\u0015\u001a\u00020\u0013J\u000e\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004J\b\u0010\u0017\u001a\u00020\u0000H\u0007J\u0016\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u0004J\u0016\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\r0\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00110\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/util/LocalMetrics;", "", "()V", "TAG", "", "db", "Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase;", "getDb", "()Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase;", "db$delegate", "Lkotlin/Lazy;", "eventsById", "", "Lorg/thoughtcrime/securesms/database/model/LocalMetricsEvent;", "executor", "Ljava/util/concurrent/Executor;", "lastSplitTimeById", "", "cancel", "", ContactRepository.ID_COLUMN, "clear", NotificationProfileDatabase.NotificationProfileScheduleTable.END, "getInstance", "split", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "name", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LocalMetrics {
    public static final LocalMetrics INSTANCE = new LocalMetrics();
    private static final String TAG;
    private static final Lazy db$delegate = LazyKt__LazyJVMKt.lazy(LocalMetrics$db$2.INSTANCE);
    private static final Map<String, LocalMetricsEvent> eventsById = new LRUCache(200);
    private static final Executor executor;
    private static final Map<String, Long> lastSplitTimeById = new LRUCache(200);

    private LocalMetrics() {
    }

    static {
        INSTANCE = new LocalMetrics();
        String tag = Log.tag(LocalMetrics.class);
        Intrinsics.checkNotNullExpressionValue(tag, "tag(LocalMetrics::class.java)");
        TAG = tag;
        eventsById = new LRUCache(200);
        lastSplitTimeById = new LRUCache(200);
        ExecutorService newCachedSingleThreadExecutor = SignalExecutors.newCachedSingleThreadExecutor("signal-LocalMetrics");
        Intrinsics.checkNotNullExpressionValue(newCachedSingleThreadExecutor, "newCachedSingleThreadExe…or(\"signal-LocalMetrics\")");
        executor = newCachedSingleThreadExecutor;
        db$delegate = LazyKt__LazyJVMKt.lazy(LocalMetrics$db$2.INSTANCE);
    }

    private final LocalMetricsDatabase getDb() {
        return (LocalMetricsDatabase) db$delegate.getValue();
    }

    @JvmStatic
    public static final LocalMetrics getInstance() {
        return INSTANCE;
    }

    public final void start(String str, String str2) {
        Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(str2, "name");
        executor.execute(new Runnable(str, str2, System.currentTimeMillis()) { // from class: org.thoughtcrime.securesms.util.LocalMetrics$$ExternalSyntheticLambda4
            public final /* synthetic */ String f$0;
            public final /* synthetic */ String f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LocalMetrics.m3255$r8$lambda$Rfr_D_xfNaxSo6Es1sARsNqZcw(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: start$lambda-0 */
    public static final void m3261start$lambda0(String str, String str2, long j) {
        Intrinsics.checkNotNullParameter(str, "$id");
        Intrinsics.checkNotNullParameter(str2, "$name");
        eventsById.put(str, new LocalMetricsEvent(System.currentTimeMillis(), str, str2, new ArrayList()));
        lastSplitTimeById.put(str, Long.valueOf(j));
    }

    public final void split(String str, String str2) {
        Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(str2, "split");
        executor.execute(new Runnable(str, str2, System.currentTimeMillis()) { // from class: org.thoughtcrime.securesms.util.LocalMetrics$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$0;
            public final /* synthetic */ String f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LocalMetrics.m3256$r8$lambda$ju42KaujCGvhT3iBiNFP3ONSsU(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: split$lambda-2 */
    public static final void m3260split$lambda2(String str, String str2, long j) {
        List<LocalMetricsSplit> splits;
        List<LocalMetricsSplit> splits2;
        Intrinsics.checkNotNullParameter(str, "$id");
        Intrinsics.checkNotNullParameter(str2, "$split");
        Long l = lastSplitTimeById.get(str);
        LocalMetricsEvent localMetricsEvent = eventsById.get(str);
        boolean z = true;
        if (localMetricsEvent != null && (splits2 = localMetricsEvent.getSplits()) != null && !splits2.isEmpty()) {
            Iterator<T> it = splits2.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (Intrinsics.areEqual(((LocalMetricsSplit) it.next()).getName(), str2)) {
                        z = false;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        if (l != null && z) {
            LocalMetricsEvent localMetricsEvent2 = eventsById.get(str);
            if (!(localMetricsEvent2 == null || (splits = localMetricsEvent2.getSplits()) == null)) {
                splits.add(new LocalMetricsSplit(str2, j - l.longValue()));
            }
            lastSplitTimeById.put(str, Long.valueOf(j));
        }
    }

    public final void cancel(String str) {
        Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
        executor.execute(new Runnable(str) { // from class: org.thoughtcrime.securesms.util.LocalMetrics$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LocalMetrics.$r8$lambda$yH_L4mDUgdwZh6f21iR4msldsac(this.f$0);
            }
        });
    }

    /* renamed from: cancel$lambda-3 */
    public static final void m3257cancel$lambda3(String str) {
        Intrinsics.checkNotNullParameter(str, "$id");
        eventsById.remove(str);
    }

    public final void end(String str) {
        Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
        executor.execute(new Runnable(str) { // from class: org.thoughtcrime.securesms.util.LocalMetrics$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LocalMetrics.$r8$lambda$lApOF2uxaBKT4R84DZBvqM4Lg3k(this.f$0);
            }
        });
    }

    /* renamed from: end$lambda-4 */
    public static final void m3259end$lambda4(String str) {
        Intrinsics.checkNotNullParameter(str, "$id");
        LocalMetricsEvent localMetricsEvent = eventsById.get(str);
        if (localMetricsEvent != null) {
            INSTANCE.getDb().insert(System.currentTimeMillis(), localMetricsEvent);
            Log.d(TAG, localMetricsEvent.toString());
        }
    }

    public final void clear() {
        executor.execute(new Runnable() { // from class: org.thoughtcrime.securesms.util.LocalMetrics$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                LocalMetrics.$r8$lambda$YtbbSNMPZD1KrB60NSEnk5VeMQU();
            }
        });
    }

    /* renamed from: clear$lambda-5 */
    public static final void m3258clear$lambda5() {
        Log.w(TAG, "Clearing local metrics store.");
        INSTANCE.getDb().clear();
    }
}
