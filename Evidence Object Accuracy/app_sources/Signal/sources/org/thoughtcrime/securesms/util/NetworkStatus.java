package org.thoughtcrime.securesms.util;

import kotlin.Metadata;

/* compiled from: NetworkStatus.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\t\u0010\b\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u00032\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\b\u0010\u000e\u001a\u00020\u000fH\u0016R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0006¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/util/NetworkStatus;", "", "isOnVpn", "", "isMetered", "(ZZ)V", "()Z", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NetworkStatus {
    private final boolean isMetered;
    private final boolean isOnVpn;

    public static /* synthetic */ NetworkStatus copy$default(NetworkStatus networkStatus, boolean z, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z = networkStatus.isOnVpn;
        }
        if ((i & 2) != 0) {
            z2 = networkStatus.isMetered;
        }
        return networkStatus.copy(z, z2);
    }

    public final boolean component1() {
        return this.isOnVpn;
    }

    public final boolean component2() {
        return this.isMetered;
    }

    public final NetworkStatus copy(boolean z, boolean z2) {
        return new NetworkStatus(z, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NetworkStatus)) {
            return false;
        }
        NetworkStatus networkStatus = (NetworkStatus) obj;
        return this.isOnVpn == networkStatus.isOnVpn && this.isMetered == networkStatus.isMetered;
    }

    public int hashCode() {
        boolean z = this.isOnVpn;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        boolean z2 = this.isMetered;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        return i5 + i;
    }

    public NetworkStatus(boolean z, boolean z2) {
        this.isOnVpn = z;
        this.isMetered = z2;
    }

    public final boolean isMetered() {
        return this.isMetered;
    }

    public final boolean isOnVpn() {
        return this.isOnVpn;
    }

    public String toString() {
        return "[isOnVpn: " + this.isOnVpn + ", isMetered: " + this.isMetered + ']';
    }
}
