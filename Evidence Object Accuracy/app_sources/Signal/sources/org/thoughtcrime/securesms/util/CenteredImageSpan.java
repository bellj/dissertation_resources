package org.thoughtcrime.securesms.util;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.style.ReplacementSpan;

/* loaded from: classes4.dex */
public class CenteredImageSpan extends ReplacementSpan {
    private final Drawable drawable;

    public CenteredImageSpan(Drawable drawable) {
        this.drawable = drawable;
    }

    @Override // android.text.style.ReplacementSpan
    public int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
        return this.drawable.getBounds().right;
    }

    @Override // android.text.style.ReplacementSpan
    public void draw(Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, Paint paint) {
        canvas.save();
        canvas.translate(f, (float) ((i3 + ((i5 - i3) / 2)) - (this.drawable.getBounds().height() / 2)));
        this.drawable.draw(canvas);
        canvas.restore();
    }
}
