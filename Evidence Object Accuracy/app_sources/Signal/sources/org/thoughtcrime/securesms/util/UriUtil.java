package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.net.Uri;
import java.io.File;
import java.io.IOException;

/* loaded from: classes4.dex */
public final class UriUtil {
    public static boolean isValidExternalUri(Context context, Uri uri) {
        if (!"file".equals(uri.getScheme())) {
            return true;
        }
        try {
            File file = new File(uri.getPath());
            if (file.getCanonicalPath().equals(file.getPath()) && !file.getCanonicalPath().startsWith("/data")) {
                if (!file.getCanonicalPath().contains(context.getPackageName())) {
                    return true;
                }
            }
            return false;
        } catch (IOException unused) {
            return false;
        }
    }
}
