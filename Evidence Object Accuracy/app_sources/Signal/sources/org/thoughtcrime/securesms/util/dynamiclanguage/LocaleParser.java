package org.thoughtcrime.securesms.util.dynamiclanguage;

import android.content.res.Resources;
import androidx.core.os.ConfigurationCompat;
import java.util.Arrays;
import java.util.Locale;
import org.thoughtcrime.securesms.BuildConfig;

/* loaded from: classes.dex */
public final class LocaleParser {
    private LocaleParser() {
    }

    public static Locale findBestMatchingLocaleForLanguage(String str) {
        Locale parseLocale = LanguageString.parseLocale(str);
        if (appSupportsTheExactLocale(parseLocale)) {
            return parseLocale;
        }
        return findBestSystemLocale();
    }

    private static boolean appSupportsTheExactLocale(Locale locale) {
        if (locale == null) {
            return false;
        }
        return Arrays.asList(BuildConfig.LANGUAGES).contains(locale.toString());
    }

    private static Locale findBestSystemLocale() {
        Locale firstMatch = ConfigurationCompat.getLocales(Resources.getSystem().getConfiguration()).getFirstMatch(BuildConfig.LANGUAGES);
        if (firstMatch != null) {
            return firstMatch;
        }
        return Locale.ENGLISH;
    }
}
