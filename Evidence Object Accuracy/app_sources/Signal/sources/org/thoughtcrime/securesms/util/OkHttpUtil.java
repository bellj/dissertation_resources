package org.thoughtcrime.securesms.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Objects;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okhttp3.internal.Util;

/* loaded from: classes4.dex */
public final class OkHttpUtil {
    private OkHttpUtil() {
    }

    public static byte[] readAsBytes(InputStream inputStream, long j) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[(int) ByteUnit.KILOBYTES.toBytes(32)];
        int i = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read < 0) {
                return byteArrayOutputStream.toByteArray();
            }
            i += read;
            if (((long) i) <= j) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                throw new IOException("Exceeded maximum size during read!");
            }
        }
    }

    public static String readAsString(ResponseBody responseBody, long j) throws IOException {
        if (responseBody.contentLength() <= j) {
            byte[] readAsBytes = readAsBytes(responseBody.byteStream(), j);
            MediaType contentType = responseBody.contentType();
            Charset charset = contentType != null ? contentType.charset(Util.UTF_8) : Util.UTF_8;
            Objects.requireNonNull(charset);
            return new String(readAsBytes, charset);
        }
        throw new IOException("Content-Length exceeded maximum size!");
    }
}
