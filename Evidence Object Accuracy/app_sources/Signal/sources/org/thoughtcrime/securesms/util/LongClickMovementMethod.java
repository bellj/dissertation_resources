package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class LongClickMovementMethod extends LinkMovementMethod {
    private static LongClickMovementMethod sInstance;
    private LongClickCopySpan currentSpan;
    private final GestureDetector gestureDetector;
    private View widget;

    private LongClickMovementMethod(Context context) {
        this.gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() { // from class: org.thoughtcrime.securesms.util.LongClickMovementMethod.1
            @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
            public void onLongPress(MotionEvent motionEvent) {
                if (LongClickMovementMethod.this.currentSpan != null && LongClickMovementMethod.this.widget != null) {
                    LongClickMovementMethod.this.currentSpan.onLongClick(LongClickMovementMethod.this.widget);
                    LongClickMovementMethod.this.widget = null;
                    LongClickMovementMethod.this.currentSpan = null;
                }
            }

            @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
            public boolean onSingleTapUp(MotionEvent motionEvent) {
                if (LongClickMovementMethod.this.currentSpan == null || LongClickMovementMethod.this.widget == null) {
                    return true;
                }
                LongClickMovementMethod.this.currentSpan.onClick(LongClickMovementMethod.this.widget);
                LongClickMovementMethod.this.widget = null;
                LongClickMovementMethod.this.currentSpan = null;
                return true;
            }
        });
    }

    @Override // android.text.method.LinkMovementMethod, android.text.method.ScrollingMovementMethod, android.text.method.BaseMovementMethod, android.text.method.MovementMethod
    public boolean onTouchEvent(TextView textView, Spannable spannable, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 1 || action == 0) {
            int x = ((int) motionEvent.getX()) - textView.getTotalPaddingLeft();
            int y = ((int) motionEvent.getY()) - textView.getTotalPaddingTop();
            int scrollX = x + textView.getScrollX();
            int scrollY = y + textView.getScrollY();
            Layout layout = textView.getLayout();
            int offsetForHorizontal = layout.getOffsetForHorizontal(layout.getLineForVertical(scrollY), (float) scrollX);
            LongClickCopySpan[] longClickCopySpanArr = (LongClickCopySpan[]) spannable.getSpans(offsetForHorizontal, offsetForHorizontal, LongClickCopySpan.class);
            if (longClickCopySpanArr.length != 0) {
                LongClickCopySpan longClickCopySpan = longClickCopySpanArr[0];
                if (action == 0) {
                    Selection.setSelection(spannable, spannable.getSpanStart(longClickCopySpan), spannable.getSpanEnd(longClickCopySpan));
                    longClickCopySpan.setHighlighted(true, ContextCompat.getColor(textView.getContext(), R.color.touch_highlight));
                } else {
                    Selection.removeSelection(spannable);
                    longClickCopySpan.setHighlighted(false, 0);
                }
                this.currentSpan = longClickCopySpan;
                this.widget = textView;
                return this.gestureDetector.onTouchEvent(motionEvent);
            }
        } else if (action == 3) {
            for (LongClickCopySpan longClickCopySpan2 : (LongClickCopySpan[]) spannable.getSpans(Selection.getSelectionStart(spannable), Selection.getSelectionEnd(spannable), LongClickCopySpan.class)) {
                longClickCopySpan2.setHighlighted(false, 0);
            }
            Selection.removeSelection(spannable);
            return this.gestureDetector.onTouchEvent(motionEvent);
        }
        return super.onTouchEvent(textView, spannable, motionEvent);
    }

    public static LongClickMovementMethod getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new LongClickMovementMethod(context.getApplicationContext());
        }
        return sInstance;
    }
}
