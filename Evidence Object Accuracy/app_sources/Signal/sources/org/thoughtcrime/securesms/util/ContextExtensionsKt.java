package org.thoughtcrime.securesms.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ContextExtensions.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004¨\u0006\u0005"}, d2 = {"safeUnregisterReceiver", "", "Landroid/content/Context;", "receiver", "Landroid/content/BroadcastReceiver;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContextExtensionsKt {
    public static final void safeUnregisterReceiver(Context context, BroadcastReceiver broadcastReceiver) {
        Intrinsics.checkNotNullParameter(context, "<this>");
        if (broadcastReceiver != null) {
            try {
                context.unregisterReceiver(broadcastReceiver);
            } catch (IllegalArgumentException unused) {
            }
        }
    }
}
