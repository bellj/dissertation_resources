package org.thoughtcrime.securesms.util;

import androidx.core.util.ObjectsCompat;

/* loaded from: classes4.dex */
public class Triple<A, B, C> {
    private final A a;
    private final B b;
    private final C c;

    public Triple(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public A first() {
        return this.a;
    }

    public B second() {
        return this.b;
    }

    public C third() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Triple)) {
            return false;
        }
        Triple triple = (Triple) obj;
        if (!ObjectsCompat.equals(triple.a, this.a) || !ObjectsCompat.equals(triple.b, this.b) || !ObjectsCompat.equals(triple.c, this.c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        A a = this.a;
        int i = 0;
        int hashCode = a == null ? 0 : a.hashCode();
        B b = this.b;
        int hashCode2 = hashCode ^ (b == null ? 0 : b.hashCode());
        C c = this.c;
        if (c != null) {
            i = c.hashCode();
        }
        return hashCode2 ^ i;
    }
}
