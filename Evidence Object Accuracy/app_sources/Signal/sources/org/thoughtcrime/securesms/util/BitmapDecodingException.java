package org.thoughtcrime.securesms.util;

/* loaded from: classes4.dex */
public class BitmapDecodingException extends Exception {
    public BitmapDecodingException(String str) {
        super(str);
    }

    public BitmapDecodingException(Exception exc) {
        super(exc);
    }
}
