package org.thoughtcrime.securesms.util.viewholders;

import android.app.Application;
import android.content.Context;
import java.util.Objects;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel;

/* loaded from: classes4.dex */
public abstract class RecipientMappingModel<T extends RecipientMappingModel<T>> implements MappingModel<T> {
    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(Object obj) {
        return MappingModel.CC.$default$getChangePayload(this, obj);
    }

    public abstract Recipient getRecipient();

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel<T extends org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel<T>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* bridge */ /* synthetic */ boolean areContentsTheSame(Object obj) {
        return areContentsTheSame((RecipientMappingModel<T>) ((RecipientMappingModel) obj));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel<T extends org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel<T>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* bridge */ /* synthetic */ boolean areItemsTheSame(Object obj) {
        return areItemsTheSame((RecipientMappingModel<T>) ((RecipientMappingModel) obj));
    }

    public String getName(Context context) {
        return getRecipient().getDisplayName(context);
    }

    public boolean areItemsTheSame(T t) {
        return getRecipient().getId().equals(t.getRecipient().getId());
    }

    public boolean areContentsTheSame(T t) {
        Application application = ApplicationDependencies.getApplication();
        return getName(application).equals(t.getName(application)) && Objects.equals(getRecipient().getContactPhoto(), t.getRecipient().getContactPhoto());
    }

    /* loaded from: classes4.dex */
    public static class RecipientIdMappingModel extends RecipientMappingModel<RecipientIdMappingModel> {
        private final RecipientId recipientId;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel$RecipientIdMappingModel */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel, org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* bridge */ /* synthetic */ boolean areContentsTheSame(Object obj) {
            return RecipientMappingModel.super.areContentsTheSame((RecipientIdMappingModel) ((RecipientMappingModel) obj));
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel$RecipientIdMappingModel */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel, org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* bridge */ /* synthetic */ boolean areItemsTheSame(Object obj) {
            return RecipientMappingModel.super.areItemsTheSame((RecipientIdMappingModel) ((RecipientMappingModel) obj));
        }

        public RecipientIdMappingModel(RecipientId recipientId) {
            this.recipientId = recipientId;
        }

        @Override // org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel
        public Recipient getRecipient() {
            return Recipient.resolved(this.recipientId);
        }
    }
}
