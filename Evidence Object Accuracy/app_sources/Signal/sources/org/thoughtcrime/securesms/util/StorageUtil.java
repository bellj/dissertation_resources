package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.storage.StorageVolume;
import android.provider.MediaStore;
import java.io.File;
import java.util.Iterator;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.NoExternalStorageException;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.permissions.Permissions;

/* loaded from: classes4.dex */
public class StorageUtil {
    private static final String PRODUCTION_PACKAGE_ID;

    public static File getOrCreateBackupDirectory() throws NoExternalStorageException {
        if (Environment.getExternalStorageDirectory().canWrite()) {
            File backupDirectory = getBackupDirectory();
            if (backupDirectory.exists() || backupDirectory.mkdirs()) {
                return backupDirectory;
            }
            throw new NoExternalStorageException("Unable to create backup directory...");
        }
        throw new NoExternalStorageException();
    }

    public static File getBackupDirectory() throws NoExternalStorageException {
        return new File(new File(Environment.getExternalStorageDirectory(), "Signal"), "Backups");
    }

    public static String getDisplayPath(Context context, Uri uri) {
        StorageVolume storageVolume;
        String lastPathSegment = uri.getLastPathSegment();
        Objects.requireNonNull(lastPathSegment);
        String replaceFirst = lastPathSegment.replaceFirst(":.*", "");
        String replaceFirst2 = lastPathSegment.replaceFirst(".*:", "");
        Iterator<StorageVolume> it = ServiceUtil.getStorageManager(context).getStorageVolumes().iterator();
        while (true) {
            if (!it.hasNext()) {
                storageVolume = null;
                break;
            }
            storageVolume = it.next();
            if (Objects.equals(storageVolume.getUuid(), replaceFirst)) {
                break;
            }
        }
        if (storageVolume == null) {
            return replaceFirst2;
        }
        return context.getString(R.string.StorageUtil__s_s, storageVolume.getDescription(context), replaceFirst2);
    }

    public static File getBackupCacheDirectory(Context context) {
        return context.getExternalCacheDir();
    }

    private static File getSignalStorageDir() throws NoExternalStorageException {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (externalStorageDirectory.canWrite()) {
            return externalStorageDirectory;
        }
        throw new NoExternalStorageException();
    }

    public static boolean canWriteInSignalStorageDir() {
        try {
            return getSignalStorageDir().canWrite();
        } catch (NoExternalStorageException unused) {
            return false;
        }
    }

    public static File getLegacyBackupDirectory() throws NoExternalStorageException {
        return getSignalStorageDir();
    }

    public static boolean canWriteToMediaStore() {
        return Build.VERSION.SDK_INT > 28 || Permissions.hasAll(ApplicationDependencies.getApplication(), "android.permission.WRITE_EXTERNAL_STORAGE");
    }

    public static boolean canReadFromMediaStore() {
        return Permissions.hasAll(ApplicationDependencies.getApplication(), "android.permission.READ_EXTERNAL_STORAGE");
    }

    public static Uri getVideoUri() {
        if (Build.VERSION.SDK_INT < 21) {
            return getLegacyUri(Environment.DIRECTORY_MOVIES);
        }
        return MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
    }

    public static Uri getAudioUri() {
        if (Build.VERSION.SDK_INT < 21) {
            return getLegacyUri(Environment.DIRECTORY_MUSIC);
        }
        return MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
    }

    public static Uri getImageUri() {
        if (Build.VERSION.SDK_INT < 21) {
            return getLegacyUri(Environment.DIRECTORY_PICTURES);
        }
        return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    }

    public static Uri getDownloadUri() {
        if (Build.VERSION.SDK_INT < 29) {
            return getLegacyUri(Environment.DIRECTORY_DOWNLOADS);
        }
        return MediaStore.Downloads.EXTERNAL_CONTENT_URI;
    }

    public static Uri getLegacyUri(String str) {
        return Uri.fromFile(Environment.getExternalStoragePublicDirectory(str));
    }

    public static String getCleanFileName(String str) {
        if (str == null) {
            return null;
        }
        return str.replace((char) 8237, (char) 65533).replace((char) 8238, (char) 65533);
    }
}
