package org.thoughtcrime.securesms.util;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import io.reactivex.rxjava3.core.ObservableEmitter;
import kotlin.Metadata;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;

/* compiled from: InternetConnectionObserver.kt */
@Metadata(d1 = {"\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u001c\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016¨\u0006\b"}, d2 = {"org/thoughtcrime/securesms/util/InternetConnectionObserver$observe$1$observer$1", "Landroid/content/BroadcastReceiver;", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class InternetConnectionObserver$observe$1$observer$1 extends BroadcastReceiver {
    final /* synthetic */ Application $application;
    final /* synthetic */ ObservableEmitter<Boolean> $it;

    public InternetConnectionObserver$observe$1$observer$1(ObservableEmitter<Boolean> observableEmitter, Application application) {
        this.$it = observableEmitter;
        this.$application = application;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.$it.isDisposed()) {
            this.$it.onNext(Boolean.valueOf(NetworkConstraint.isMet(this.$application)));
        }
    }
}
