package org.thoughtcrime.securesms.util;

import android.view.View;
import android.view.ViewGroup;
import androidx.asynclayoutinflater.view.AsyncLayoutInflater;
import org.thoughtcrime.securesms.util.CachedInflater;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CachedInflater$ViewCache$$ExternalSyntheticLambda1 implements AsyncLayoutInflater.OnInflateFinishedListener {
    public final /* synthetic */ CachedInflater.ViewCache f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ CachedInflater$ViewCache$$ExternalSyntheticLambda1(CachedInflater.ViewCache viewCache, long j) {
        this.f$0 = viewCache;
        this.f$1 = j;
    }

    @Override // androidx.asynclayoutinflater.view.AsyncLayoutInflater.OnInflateFinishedListener
    public final void onInflateFinished(View view, int i, ViewGroup viewGroup) {
        this.f$0.lambda$cacheUntilLimit$0(this.f$1, view, i, viewGroup);
    }
}
