package org.thoughtcrime.securesms.util;

import android.content.Context;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* loaded from: classes4.dex */
public final class ScreenDensity {
    private static final LinkedHashMap<Integer, String> LEVELS = new LinkedHashMap<Integer, String>() { // from class: org.thoughtcrime.securesms.util.ScreenDensity.1
        {
            put(120, "ldpi");
            put(160, "mdpi");
            put(240, "hdpi");
            put(320, "xhdpi");
            put(480, "xxhdpi");
            put(640, "xxxhdpi");
        }
    };
    private static final String UNKNOWN;
    private static final float XHDPI_TO_HDPI;
    private static final float XHDPI_TO_LDPI;
    private static final float XHDPI_TO_MDPI;
    private final String bucket;
    private final int density;

    public ScreenDensity(String str, int i) {
        this.bucket = str;
        this.density = i;
    }

    public static ScreenDensity get(Context context) {
        int i = context.getResources().getDisplayMetrics().densityDpi;
        Iterator<Map.Entry<Integer, String>> it = LEVELS.entrySet().iterator();
        String str = UNKNOWN;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Map.Entry<Integer, String> next = it.next();
            String value = next.getValue();
            if (next.getKey().intValue() > i) {
                str = value;
                break;
            }
            str = value;
        }
        return new ScreenDensity(str, i);
    }

    public static String getBestDensityBucketForDevice() {
        ScreenDensity screenDensity = get(ApplicationDependencies.getApplication());
        return screenDensity.isKnownDensity() ? screenDensity.bucket : "xhdpi";
    }

    public String getBucket() {
        return this.bucket;
    }

    public boolean isKnownDensity() {
        return !this.bucket.equals(UNKNOWN);
    }

    public String toString() {
        return this.bucket + " (" + this.density + ")";
    }

    public static float xhdpiRelativeDensityScaleFactor(String str) {
        str.hashCode();
        char c = 65535;
        switch (str.hashCode()) {
            case 3197941:
                if (str.equals("hdpi")) {
                    c = 0;
                    break;
                }
                break;
            case 3317105:
                if (str.equals("ldpi")) {
                    c = 1;
                    break;
                }
                break;
            case 3346896:
                if (str.equals("mdpi")) {
                    c = 2;
                    break;
                }
                break;
            case 114020461:
                if (str.equals("xhdpi")) {
                    c = 3;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return XHDPI_TO_HDPI;
            case 1:
                return XHDPI_TO_LDPI;
            case 2:
                return XHDPI_TO_MDPI;
            case 3:
                return 1.0f;
            default:
                throw new IllegalStateException("Unsupported density: " + str);
        }
    }
}
