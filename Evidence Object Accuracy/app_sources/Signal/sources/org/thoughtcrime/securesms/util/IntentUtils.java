package org.thoughtcrime.securesms.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.ResolveInfo;
import java.util.List;

/* loaded from: classes4.dex */
public class IntentUtils {
    public static boolean isResolvable(Context context, Intent intent) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        if (queryIntentActivities == null || queryIntentActivities.size() <= 1) {
            return false;
        }
        return true;
    }

    public static LabeledIntent getLabelintent(Context context, Intent intent, int i, int i2) {
        ComponentName resolveActivity = intent.resolveActivity(context.getPackageManager());
        if (resolveActivity == null) {
            return null;
        }
        Intent intent2 = new Intent();
        intent2.setComponent(resolveActivity);
        intent2.setData(intent.getData());
        return new LabeledIntent(intent2, context.getPackageName(), i, i2);
    }
}
