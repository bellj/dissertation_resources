package org.thoughtcrime.securesms.util;

import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import java.io.IOException;

/* loaded from: classes4.dex */
public final class MediaMetadataRetrieverUtil {
    private MediaMetadataRetrieverUtil() {
    }

    public static void setDataSource(MediaMetadataRetriever mediaMetadataRetriever, MediaDataSource mediaDataSource) throws IOException {
        try {
            mediaMetadataRetriever.setDataSource(mediaDataSource);
        } catch (Exception e) {
            throw new IOException(e);
        }
    }
}
