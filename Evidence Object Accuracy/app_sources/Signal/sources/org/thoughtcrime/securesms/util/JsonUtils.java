package org.thoughtcrime.securesms.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.kotlin.ExtensionsKt;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes4.dex */
public class JsonUtils {
    private static final ObjectMapper objectMapper;

    static {
        ObjectMapper objectMapper2 = new ObjectMapper();
        objectMapper = objectMapper2;
        objectMapper2.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper2.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        objectMapper2.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        ExtensionsKt.registerKotlinModule(objectMapper2);
    }

    public static <T> T fromJson(byte[] bArr, Class<T> cls) throws IOException {
        return (T) fromJson(new String(bArr), cls);
    }

    public static <T> T fromJson(String str, Class<T> cls) throws IOException {
        return (T) objectMapper.readValue(str, cls);
    }

    public static <T> T fromJson(InputStream inputStream, Class<T> cls) throws IOException {
        return (T) objectMapper.readValue(inputStream, cls);
    }

    public static <T> T fromJson(Reader reader, Class<T> cls) throws IOException {
        return (T) objectMapper.readValue(reader, cls);
    }

    public static String toJson(Object obj) throws IOException {
        return objectMapper.writeValueAsString(obj);
    }

    public static ObjectMapper getMapper() {
        return objectMapper;
    }

    /* loaded from: classes4.dex */
    public static class SaneJSONObject {
        private final JSONObject delegate;

        public SaneJSONObject(JSONObject jSONObject) {
            this.delegate = jSONObject;
        }

        public String getString(String str) throws JSONException {
            if (this.delegate.isNull(str)) {
                return null;
            }
            return this.delegate.getString(str);
        }

        public long getLong(String str) throws JSONException {
            return this.delegate.getLong(str);
        }

        public boolean isNull(String str) {
            return this.delegate.isNull(str);
        }

        public int getInt(String str) throws JSONException {
            return this.delegate.getInt(str);
        }
    }
}
