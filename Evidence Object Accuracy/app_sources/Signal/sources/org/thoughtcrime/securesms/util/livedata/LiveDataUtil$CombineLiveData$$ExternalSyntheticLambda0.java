package org.thoughtcrime.securesms.util.livedata;

import androidx.lifecycle.Observer;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LiveDataUtil$CombineLiveData$$ExternalSyntheticLambda0 implements Observer {
    public final /* synthetic */ LiveDataUtil.CombineLiveData f$0;
    public final /* synthetic */ LiveDataUtil.Combine f$1;

    public /* synthetic */ LiveDataUtil$CombineLiveData$$ExternalSyntheticLambda0(LiveDataUtil.CombineLiveData combineLiveData, LiveDataUtil.Combine combine) {
        this.f$0 = combineLiveData;
        this.f$1 = combine;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.lambda$new$0(this.f$1, obj);
    }
}
