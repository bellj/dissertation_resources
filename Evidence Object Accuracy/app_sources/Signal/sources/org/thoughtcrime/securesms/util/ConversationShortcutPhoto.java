package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import java.security.MessageDigest;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.FallbackPhoto80dp;
import org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.SystemContactPhoto;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.database.model.ProfileAvatarFileDetails;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class ConversationShortcutPhoto implements Key {
    private static final long VERSION;
    private final String avatarObject;
    private final ProfileAvatarFileDetails profileAvatarFileDetails;
    private final Recipient recipient;

    public ConversationShortcutPhoto(Recipient recipient) {
        this.recipient = recipient.resolve();
        this.avatarObject = (String) Util.firstNonNull(recipient.getProfileAvatar(), "");
        this.profileAvatarFileDetails = recipient.getProfileAvatarFileDetails();
    }

    @Override // com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(this.recipient.getDisplayName(ApplicationDependencies.getApplication()).getBytes());
        messageDigest.update(this.avatarObject.getBytes());
        messageDigest.update(isSystemContactPhoto() ? (byte) 1 : 0);
        messageDigest.update(this.profileAvatarFileDetails.getDiskCacheKeyBytes());
        messageDigest.update(ByteUtil.longToByteArray(1));
    }

    @Override // com.bumptech.glide.load.Key
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ConversationShortcutPhoto.class != obj.getClass()) {
            return false;
        }
        ConversationShortcutPhoto conversationShortcutPhoto = (ConversationShortcutPhoto) obj;
        if (!Objects.equals(this.recipient, conversationShortcutPhoto.recipient) || !Objects.equals(this.avatarObject, conversationShortcutPhoto.avatarObject) || isSystemContactPhoto() != conversationShortcutPhoto.isSystemContactPhoto() || !Objects.equals(this.profileAvatarFileDetails, conversationShortcutPhoto.profileAvatarFileDetails)) {
            return false;
        }
        return true;
    }

    @Override // com.bumptech.glide.load.Key
    public int hashCode() {
        return Objects.hash(this.recipient, this.avatarObject, Boolean.valueOf(isSystemContactPhoto()), this.profileAvatarFileDetails);
    }

    private boolean isSystemContactPhoto() {
        return this.recipient.getContactPhoto() instanceof SystemContactPhoto;
    }

    /* loaded from: classes4.dex */
    public static final class Loader implements ModelLoader<ConversationShortcutPhoto, Bitmap> {
        private final Context context;

        public boolean handles(ConversationShortcutPhoto conversationShortcutPhoto) {
            return true;
        }

        private Loader(Context context) {
            this.context = context;
        }

        public ModelLoader.LoadData<Bitmap> buildLoadData(ConversationShortcutPhoto conversationShortcutPhoto, int i, int i2, Options options) {
            return new ModelLoader.LoadData<>(conversationShortcutPhoto, new Fetcher(this.context, conversationShortcutPhoto));
        }

        /* loaded from: classes4.dex */
        public static class Factory implements ModelLoaderFactory<ConversationShortcutPhoto, Bitmap> {
            private final Context context;

            public void teardown() {
            }

            public Factory(Context context) {
                this.context = context;
            }

            public ModelLoader<ConversationShortcutPhoto, Bitmap> build(MultiModelLoaderFactory multiModelLoaderFactory) {
                return new Loader(this.context);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class Fetcher implements DataFetcher<Bitmap> {
        private final Context context;
        private final ConversationShortcutPhoto photo;

        @Override // com.bumptech.glide.load.data.DataFetcher
        public void cancel() {
        }

        @Override // com.bumptech.glide.load.data.DataFetcher
        public void cleanup() {
        }

        private Fetcher(Context context, ConversationShortcutPhoto conversationShortcutPhoto) {
            this.context = context;
            this.photo = conversationShortcutPhoto;
        }

        @Override // com.bumptech.glide.load.data.DataFetcher
        public void loadData(Priority priority, DataFetcher.DataCallback<? super Bitmap> dataCallback) {
            Bitmap bitmap;
            try {
                bitmap = getShortcutInfoBitmap(this.context);
            } catch (InterruptedException | ExecutionException unused) {
                bitmap = getFallbackForShortcut(this.context);
            }
            dataCallback.onDataReady(bitmap);
        }

        @Override // com.bumptech.glide.load.data.DataFetcher
        public Class<Bitmap> getDataClass() {
            return Bitmap.class;
        }

        @Override // com.bumptech.glide.load.data.DataFetcher
        public DataSource getDataSource() {
            return DataSource.LOCAL;
        }

        private Bitmap getShortcutInfoBitmap(Context context) throws ExecutionException, InterruptedException {
            Recipient recipient = this.photo.recipient;
            int i = DrawableUtil.SHORTCUT_INFO_WRAPPED_SIZE;
            return DrawableUtil.wrapBitmapForShortcutInfo(AvatarUtil.loadIconBitmapSquareNoCache(context, recipient, i, i));
        }

        private Bitmap getFallbackForShortcut(Context context) {
            int i;
            FallbackContactPhoto fallbackContactPhoto;
            Recipient recipient = this.photo.recipient;
            if (recipient.isSelf()) {
                i = R.drawable.ic_note_80;
            } else {
                i = recipient.isGroup() ? R.drawable.ic_group_80 : R.drawable.ic_profile_80;
            }
            if (recipient.isSelf() || recipient.isGroup()) {
                fallbackContactPhoto = new FallbackPhoto80dp(i, recipient.getAvatarColor());
            } else {
                fallbackContactPhoto = new ShortcutGeneratedContactPhoto(recipient.getDisplayName(context), i, ViewUtil.dpToPx(80), recipient.getAvatarColor());
            }
            Bitmap bitmap = DrawableUtil.toBitmap(fallbackContactPhoto.asCallCard(context), ViewUtil.dpToPx(80), ViewUtil.dpToPx(80));
            Bitmap wrapBitmapForShortcutInfo = DrawableUtil.wrapBitmapForShortcutInfo(bitmap);
            bitmap.recycle();
            return wrapBitmapForShortcutInfo;
        }
    }

    /* loaded from: classes4.dex */
    public static final class ShortcutGeneratedContactPhoto extends GeneratedContactPhoto {
        private final AvatarColor color;

        public ShortcutGeneratedContactPhoto(String str, int i, int i2, AvatarColor avatarColor) {
            super(str, i, i2);
            this.color = avatarColor;
        }

        @Override // org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto
        protected Drawable newFallbackDrawable(Context context, AvatarColor avatarColor, boolean z) {
            return new FallbackPhoto80dp(getFallbackResId(), avatarColor).asDrawable(context, AvatarColor.UNKNOWN);
        }

        @Override // org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto, org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
        public Drawable asCallCard(Context context) {
            return new FallbackPhoto80dp(getFallbackResId(), this.color).asCallCard(context);
        }
    }
}
