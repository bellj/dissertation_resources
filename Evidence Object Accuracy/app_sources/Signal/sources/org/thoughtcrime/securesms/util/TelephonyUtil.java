package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* loaded from: classes4.dex */
public class TelephonyUtil {
    private static final String TAG = Log.tag(TelephonyUtil.class);

    public static TelephonyManager getManager(Context context) {
        return (TelephonyManager) context.getSystemService(RecipientDatabase.PHONE);
    }

    public static String getMccMnc(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(RecipientDatabase.PHONE);
        int i = context.getResources().getConfiguration().mcc;
        int i2 = context.getResources().getConfiguration().mnc;
        if (telephonyManager.getSimState() == 5) {
            Log.i(TAG, "Choosing MCC+MNC info from TelephonyManager.getSimOperator()");
            return telephonyManager.getSimOperator();
        } else if (telephonyManager.getPhoneType() != 2) {
            Log.i(TAG, "Choosing MCC+MNC info from TelephonyManager.getNetworkOperator()");
            return telephonyManager.getNetworkOperator();
        } else if (i == 0 || i2 == 0) {
            return null;
        } else {
            Log.i(TAG, "Choosing MCC+MNC info from current context's Configuration");
            Locale locale = Locale.ROOT;
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(i);
            if (i2 == 65535) {
                i2 = 0;
            }
            objArr[1] = Integer.valueOf(i2);
            return String.format(locale, "%03d%d", objArr);
        }
    }

    public static String getApn(Context context) {
        return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(2).getExtraInfo();
    }

    public static boolean isAnyPstnLineBusy(Context context) {
        return getManager(context).getCallState() != 0;
    }
}
