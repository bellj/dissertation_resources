package org.thoughtcrime.securesms.util.concurrent;

import java.util.concurrent.ExecutionException;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;

/* loaded from: classes4.dex */
public abstract class AssertedSuccessListener<T> implements ListenableFuture.Listener<T> {
    @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
    public void onFailure(ExecutionException executionException) {
        throw new AssertionError(executionException);
    }
}
