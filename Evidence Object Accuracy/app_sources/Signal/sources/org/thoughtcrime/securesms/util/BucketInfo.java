package org.thoughtcrime.securesms.util;

import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import java.util.Date;

/* loaded from: classes4.dex */
public final class BucketInfo {
    public static final int STANDBY_BUCKET_EXEMPTED;
    private final int bestBucket;
    private final int currentBucket;
    private final CharSequence history;
    private final int worstBucket;

    private BucketInfo(int i, int i2, int i3, CharSequence charSequence) {
        this.currentBucket = i;
        this.worstBucket = i2;
        this.bestBucket = i3;
        this.history = charSequence;
    }

    public static BucketInfo getInfo(UsageStatsManager usageStatsManager, long j) {
        StringBuilder sb = new StringBuilder();
        int appStandbyBucket = usageStatsManager.getAppStandbyBucket();
        long currentTimeMillis = System.currentTimeMillis();
        UsageEvents.Event event = new UsageEvents.Event();
        UsageEvents queryEventsForSelf = usageStatsManager.queryEventsForSelf(currentTimeMillis - j, currentTimeMillis);
        int i = appStandbyBucket;
        int i2 = i;
        while (queryEventsForSelf.hasNextEvent()) {
            queryEventsForSelf.getNextEvent(event);
            if (event.getEventType() == 11) {
                int appStandbyBucket2 = event.getAppStandbyBucket();
                sb.append(new Date(event.getTimeStamp()));
                sb.append(": ");
                sb.append("Bucket Change: ");
                sb.append(bucketToString(appStandbyBucket2));
                sb.append("\n");
                if (appStandbyBucket2 > i) {
                    i = appStandbyBucket2;
                }
                if (appStandbyBucket2 < i2) {
                    i2 = appStandbyBucket2;
                }
            }
        }
        return new BucketInfo(appStandbyBucket, i, i2, sb);
    }

    public static String bucketToString(int i) {
        if (i == 5) {
            return "Exempted";
        }
        if (i == 10) {
            return "Active";
        }
        if (i == 20) {
            return "Working Set";
        }
        if (i == 30) {
            return "Frequent";
        }
        if (i == 40) {
            return "Rare";
        }
        return "Unknown " + i;
    }

    public int getBestBucket() {
        return this.bestBucket;
    }

    public int getWorstBucket() {
        return this.worstBucket;
    }

    public int getCurrentBucket() {
        return this.currentBucket;
    }

    public CharSequence getHistory() {
        return this.history;
    }
}
