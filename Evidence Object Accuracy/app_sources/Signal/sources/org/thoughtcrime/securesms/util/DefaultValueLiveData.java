package org.thoughtcrime.securesms.util;

import androidx.lifecycle.MutableLiveData;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public class DefaultValueLiveData<T> extends MutableLiveData<T> {
    private final T defaultValue;

    public DefaultValueLiveData(T t) {
        super(t);
        this.defaultValue = t;
    }

    @Override // androidx.lifecycle.MutableLiveData, androidx.lifecycle.LiveData
    public void postValue(T t) {
        Preconditions.checkNotNull(t);
        super.postValue(t);
    }

    @Override // androidx.lifecycle.MutableLiveData, androidx.lifecycle.LiveData
    public void setValue(T t) {
        Preconditions.checkNotNull(t);
        super.setValue(t);
    }

    @Override // androidx.lifecycle.LiveData
    public T getValue() {
        T t = (T) super.getValue();
        return t != null ? t : this.defaultValue;
    }
}
