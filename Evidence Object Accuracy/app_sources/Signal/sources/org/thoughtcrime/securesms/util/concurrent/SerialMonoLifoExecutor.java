package org.thoughtcrime.securesms.util.concurrent;

import java.util.concurrent.Executor;

/* loaded from: classes4.dex */
public final class SerialMonoLifoExecutor implements Executor {
    private Runnable active;
    private final Executor executor;
    private Runnable next;

    public SerialMonoLifoExecutor(Executor executor) {
        this.executor = executor;
    }

    @Override // java.util.concurrent.Executor
    public synchronized void execute(Runnable runnable) {
        enqueue(runnable);
    }

    public synchronized boolean enqueue(Runnable runnable) {
        boolean z;
        z = this.next != null;
        this.next = new Runnable(runnable) { // from class: org.thoughtcrime.securesms.util.concurrent.SerialMonoLifoExecutor$$ExternalSyntheticLambda0
            public final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SerialMonoLifoExecutor.this.lambda$enqueue$0(this.f$1);
            }
        };
        if (this.active == null) {
            scheduleNext();
        }
        return z;
    }

    public /* synthetic */ void lambda$enqueue$0(Runnable runnable) {
        try {
            runnable.run();
        } finally {
            scheduleNext();
        }
    }

    private synchronized void scheduleNext() {
        Runnable runnable = this.next;
        this.active = runnable;
        this.next = null;
        if (runnable != null) {
            this.executor.execute(runnable);
        }
    }
}
