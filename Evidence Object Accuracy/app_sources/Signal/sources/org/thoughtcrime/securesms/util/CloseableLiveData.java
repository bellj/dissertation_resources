package org.thoughtcrime.securesms.util;

import androidx.lifecycle.MutableLiveData;
import java.io.Closeable;
import org.signal.core.util.StreamUtil;

/* loaded from: classes4.dex */
public class CloseableLiveData<E extends Closeable> extends MutableLiveData<E> {
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.util.CloseableLiveData<E extends java.io.Closeable> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // androidx.lifecycle.MutableLiveData, androidx.lifecycle.LiveData
    public /* bridge */ /* synthetic */ void setValue(Object obj) {
        setValue((CloseableLiveData<E>) ((Closeable) obj));
    }

    public void setValue(E e) {
        setValue(e, true);
    }

    public void setValue(E e, boolean z) {
        Closeable closeable = (Closeable) getValue();
        if (closeable != null && z) {
            StreamUtil.close(closeable);
        }
        super.setValue((CloseableLiveData<E>) e);
    }

    public void close() {
        Closeable closeable = (Closeable) getValue();
        if (closeable != null) {
            StreamUtil.close(closeable);
        }
    }
}
