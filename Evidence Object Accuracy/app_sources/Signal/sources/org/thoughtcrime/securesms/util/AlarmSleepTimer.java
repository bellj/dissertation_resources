package org.thoughtcrime.securesms.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.SystemClock;
import java.util.concurrent.ConcurrentSkipListSet;
import org.signal.core.util.logging.Log;
import org.whispersystems.signalservice.api.util.SleepTimer;

/* loaded from: classes4.dex */
public class AlarmSleepTimer implements SleepTimer {
    private static final String TAG = Log.tag(AlarmSleepTimer.class);
    private static ConcurrentSkipListSet<Integer> actionIdList = new ConcurrentSkipListSet<>();
    private final Context context;

    public AlarmSleepTimer(Context context) {
        this.context = context;
    }

    @Override // org.whispersystems.signalservice.api.util.SleepTimer
    public void sleep(long j) {
        AlarmReceiver alarmReceiver = new AlarmReceiver();
        int i = 0;
        while (!actionIdList.add(Integer.valueOf(i))) {
            try {
                i++;
            } finally {
                actionIdList.remove(Integer.valueOf(i));
            }
        }
        try {
            Context context = this.context;
            context.registerReceiver(alarmReceiver, new IntentFilter("org.thoughtcrime.securesms.util.AlarmSleepTimer.AlarmReceiver.WAKE_UP_THREAD." + i));
            long currentTimeMillis = System.currentTimeMillis();
            alarmReceiver.setAlarm(j, "org.thoughtcrime.securesms.util.AlarmSleepTimer.AlarmReceiver.WAKE_UP_THREAD." + i);
            while (System.currentTimeMillis() - currentTimeMillis < j) {
                try {
                    synchronized (this) {
                        wait((j - System.currentTimeMillis()) + currentTimeMillis);
                    }
                } catch (InterruptedException e) {
                    Log.w(TAG, e);
                }
            }
            this.context.unregisterReceiver(alarmReceiver);
        } catch (Exception e2) {
            Log.w(TAG, "Exception during sleep ...", e2);
        }
    }

    /* loaded from: classes4.dex */
    private class AlarmReceiver extends BroadcastReceiver {
        private static final String WAKE_UP_THREAD_ACTION;

        private AlarmReceiver() {
            AlarmSleepTimer.this = r1;
        }

        public void setAlarm(long j, String str) {
            PendingIntent broadcast = PendingIntent.getBroadcast(AlarmSleepTimer.this.context, 0, new Intent(str), 0);
            AlarmManager alarmManager = (AlarmManager) AlarmSleepTimer.this.context.getSystemService("alarm");
            String str2 = AlarmSleepTimer.TAG;
            Log.w(str2, "Setting alarm to wake up in " + j + "ms.");
            if (Build.VERSION.SDK_INT >= 23) {
                alarmManager.setExactAndAllowWhileIdle(2, SystemClock.elapsedRealtime() + j, broadcast);
            } else {
                alarmManager.setExact(2, SystemClock.elapsedRealtime() + j, broadcast);
            }
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Log.w(AlarmSleepTimer.TAG, "Waking up.");
            synchronized (AlarmSleepTimer.this) {
                AlarmSleepTimer.this.notifyAll();
            }
        }
    }
}
