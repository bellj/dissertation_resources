package org.thoughtcrime.securesms.util;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.SmsMessage;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.CharacterCalculator;

/* loaded from: classes4.dex */
public class SmsCharacterCalculator extends CharacterCalculator {
    public static final Parcelable.Creator<SmsCharacterCalculator> CREATOR = new Parcelable.Creator<SmsCharacterCalculator>() { // from class: org.thoughtcrime.securesms.util.SmsCharacterCalculator.1
        @Override // android.os.Parcelable.Creator
        public SmsCharacterCalculator createFromParcel(Parcel parcel) {
            return new SmsCharacterCalculator();
        }

        @Override // android.os.Parcelable.Creator
        public SmsCharacterCalculator[] newArray(int i) {
            return new SmsCharacterCalculator[i];
        }
    };
    private static final String TAG = Log.tag(SmsCharacterCalculator.class);

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
    }

    @Override // org.thoughtcrime.securesms.util.CharacterCalculator
    public CharacterCalculator.CharacterState calculateCharacters(String str) {
        int i;
        int i2 = MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH;
        int i3 = 1;
        try {
            int[] calculateLength = SmsMessage.calculateLength(str, false);
            int i4 = calculateLength[0];
            i = calculateLength[1];
            i2 = calculateLength[2];
            i3 = i4;
        } catch (NullPointerException e) {
            Log.w(TAG, e);
            i = str.length();
        } catch (RuntimeException e2) {
            if ((e2 instanceof SecurityException) || (e2.getCause() instanceof SecurityException)) {
                Log.e(TAG, "Security Exception", e2);
                i = str.length();
            } else {
                throw e2;
            }
        }
        int i5 = i3 > 0 ? (i + i2) / i3 : i + i2;
        return new CharacterCalculator.CharacterState(i3, i2, i5, i5);
    }
}
