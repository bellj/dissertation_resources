package org.thoughtcrime.securesms.util;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: PlatformCurrencyUtil.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bR\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/util/PlatformCurrencyUtil;", "", "()V", "USD", "Ljava/util/Currency;", "getUSD", "()Ljava/util/Currency;", "getAvailableCurrencyCodes", "", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PlatformCurrencyUtil {
    public static final PlatformCurrencyUtil INSTANCE = new PlatformCurrencyUtil();
    private static final Currency USD;

    private PlatformCurrencyUtil() {
    }

    static {
        INSTANCE = new PlatformCurrencyUtil();
        Currency instance = Currency.getInstance("USD");
        Intrinsics.checkNotNullExpressionValue(instance, "getInstance(\"USD\")");
        USD = instance;
    }

    public final Currency getUSD() {
        return USD;
    }

    public final Set<String> getAvailableCurrencyCodes() {
        Set<Currency> availableCurrencies = Currency.getAvailableCurrencies();
        Intrinsics.checkNotNullExpressionValue(availableCurrencies, "getAvailableCurrencies()");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(availableCurrencies, 10));
        for (Currency currency : availableCurrencies) {
            arrayList.add(currency.getCurrencyCode());
        }
        return CollectionsKt___CollectionsKt.toSet(arrayList);
    }
}
