package org.thoughtcrime.securesms.util;

import android.os.Parcel;
import android.os.Parcelable;
import org.thoughtcrime.securesms.util.CharacterCalculator;

/* loaded from: classes4.dex */
public class PushCharacterCalculator extends CharacterCalculator {
    public static final Parcelable.Creator<PushCharacterCalculator> CREATOR = new Parcelable.Creator<PushCharacterCalculator>() { // from class: org.thoughtcrime.securesms.util.PushCharacterCalculator.1
        @Override // android.os.Parcelable.Creator
        public PushCharacterCalculator createFromParcel(Parcel parcel) {
            return new PushCharacterCalculator();
        }

        @Override // android.os.Parcelable.Creator
        public PushCharacterCalculator[] newArray(int i) {
            return new PushCharacterCalculator[i];
        }
    };
    private static final int MAX_PRIMARY_SIZE;
    private static final int MAX_TOTAL_SIZE;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
    }

    @Override // org.thoughtcrime.securesms.util.CharacterCalculator
    public CharacterCalculator.CharacterState calculateCharacters(String str) {
        return new CharacterCalculator.CharacterState(1, MAX_TOTAL_SIZE - str.length(), MAX_TOTAL_SIZE, 2000);
    }
}
