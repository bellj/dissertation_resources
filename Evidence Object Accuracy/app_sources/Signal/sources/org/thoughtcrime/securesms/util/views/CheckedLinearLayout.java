package org.thoughtcrime.securesms.util.views;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.LinearLayout;
import java.util.Objects;

/* loaded from: classes4.dex */
public final class CheckedLinearLayout extends LinearLayout implements Checkable {
    private static final int[] CHECKED_STATE = {16842912};
    private boolean checked = false;

    public CheckedLinearLayout(Context context) {
        super(context);
    }

    public CheckedLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CheckedLinearLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // android.view.View
    protected Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Objects.requireNonNull(onSaveInstanceState);
        return new InstanceState(onSaveInstanceState, this.checked);
    }

    @Override // android.view.View
    protected void onRestoreInstanceState(Parcelable parcelable) {
        InstanceState instanceState = (InstanceState) parcelable;
        super.onRestoreInstanceState(instanceState.getSuperState());
        setChecked(instanceState.checked);
    }

    @Override // android.widget.Checkable
    public void setChecked(boolean z) {
        if (this.checked != z) {
            toggle();
        }
    }

    @Override // android.widget.Checkable
    public boolean isChecked() {
        return this.checked;
    }

    @Override // android.widget.Checkable
    public void toggle() {
        this.checked = !this.checked;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof Checkable) {
                ((Checkable) childAt).setChecked(this.checked);
            }
        }
        refreshDrawableState();
    }

    @Override // android.view.View, android.view.ViewGroup
    protected int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (isChecked()) {
            LinearLayout.mergeDrawableStates(onCreateDrawableState, CHECKED_STATE);
        }
        return onCreateDrawableState;
    }

    /* loaded from: classes4.dex */
    public static class InstanceState extends View.BaseSavedState {
        public static final Parcelable.Creator<InstanceState> CREATOR = new Parcelable.Creator<InstanceState>() { // from class: org.thoughtcrime.securesms.util.views.CheckedLinearLayout.InstanceState.1
            @Override // android.os.Parcelable.Creator
            public InstanceState createFromParcel(Parcel parcel) {
                return new InstanceState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public InstanceState[] newArray(int i) {
                return new InstanceState[i];
            }
        };
        private final boolean checked;

        InstanceState(Parcelable parcelable, boolean z) {
            super(parcelable);
            this.checked = z;
        }

        private InstanceState(Parcel parcel) {
            super(parcel);
            this.checked = parcel.readInt() > 0;
        }

        @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.checked ? 1 : 0);
        }
    }
}
