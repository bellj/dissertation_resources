package org.thoughtcrime.securesms.util.adapter.mapping;

import android.view.ViewGroup;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes4.dex */
public interface Factory<T extends MappingModel<T>> {
    MappingViewHolder<T> createViewHolder(ViewGroup viewGroup);
}
