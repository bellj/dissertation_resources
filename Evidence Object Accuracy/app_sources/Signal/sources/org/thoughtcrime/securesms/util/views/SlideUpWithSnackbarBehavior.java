package org.thoughtcrime.securesms.util.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import org.signal.core.util.DimensionUnit;

/* loaded from: classes4.dex */
public class SlideUpWithSnackbarBehavior extends CoordinatorLayout.Behavior<View> {
    private static final float PAD_TOP_OF_SNACKBAR_DP;
    private final float padTopOfSnackbar = DimensionUnit.DP.toPixels(PAD_TOP_OF_SNACKBAR_DP);

    public SlideUpWithSnackbarBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, View view, View view2) {
        view.setTranslationY(Math.min(0.0f, view2.getTranslationY() - (((float) view2.getHeight()) + this.padTopOfSnackbar)));
        return true;
    }

    public void onDependentViewRemoved(CoordinatorLayout coordinatorLayout, View view, View view2) {
        view.setTranslationY(0.0f);
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean layoutDependsOn(CoordinatorLayout coordinatorLayout, View view, View view2) {
        return view2 instanceof Snackbar.SnackbarLayout;
    }
}
