package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.io.ByteArrayOutputStream;
import java.util.concurrent.ExecutionException;
import org.thoughtcrime.securesms.mms.GlideApp;

/* loaded from: classes4.dex */
public final class ImageCompressionUtil {
    private ImageCompressionUtil() {
    }

    public static Result compressWithinConstraints(Context context, String str, Object obj, int i, int i2, int i3) throws BitmapDecodingException {
        Result compress = compress(context, str, obj, i, i3);
        if (compress.getData().length <= i2) {
            return compress;
        }
        return null;
    }

    public static Result compress(Context context, String str, Object obj, int i, int i2) throws BitmapDecodingException {
        try {
            Bitmap bitmap = GlideApp.with(context.getApplicationContext()).asBitmap().load(obj).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).centerInside().submit(i, i).get();
            if (bitmap != null) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                Bitmap.CompressFormat mimeTypeToCompressFormat = mimeTypeToCompressFormat(str);
                bitmap.compress(mimeTypeToCompressFormat, i2, byteArrayOutputStream);
                return new Result(byteArrayOutputStream.toByteArray(), compressFormatToMimeType(mimeTypeToCompressFormat), bitmap.getWidth(), bitmap.getHeight());
            }
            throw new BitmapDecodingException("Unable to decode image");
        } catch (InterruptedException | ExecutionException e) {
            throw new BitmapDecodingException(e);
        }
    }

    private static Bitmap.CompressFormat mimeTypeToCompressFormat(String str) {
        if (MediaUtil.isJpegType(str) || MediaUtil.isHeicType(str) || MediaUtil.isHeifType(str)) {
            return Bitmap.CompressFormat.JPEG;
        }
        return Bitmap.CompressFormat.PNG;
    }

    /* renamed from: org.thoughtcrime.securesms.util.ImageCompressionUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$android$graphics$Bitmap$CompressFormat;

        static {
            int[] iArr = new int[Bitmap.CompressFormat.values().length];
            $SwitchMap$android$graphics$Bitmap$CompressFormat = iArr;
            try {
                iArr[Bitmap.CompressFormat.JPEG.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$android$graphics$Bitmap$CompressFormat[Bitmap.CompressFormat.PNG.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    private static String compressFormatToMimeType(Bitmap.CompressFormat compressFormat) {
        int i = AnonymousClass1.$SwitchMap$android$graphics$Bitmap$CompressFormat[compressFormat.ordinal()];
        if (i == 1) {
            return MediaUtil.IMAGE_JPEG;
        }
        if (i == 2) {
            return MediaUtil.IMAGE_PNG;
        }
        throw new AssertionError("Unsupported format!");
    }

    /* loaded from: classes4.dex */
    public static final class Result {
        private final byte[] data;
        private final int height;
        private final String mimeType;
        private final int width;

        public Result(byte[] bArr, String str, int i, int i2) {
            this.data = bArr;
            this.mimeType = str;
            this.width = i;
            this.height = i2;
        }

        public byte[] getData() {
            return this.data;
        }

        public String getMimeType() {
            return this.mimeType;
        }

        public int getWidth() {
            return this.width;
        }

        public int getHeight() {
            return this.height;
        }
    }
}
