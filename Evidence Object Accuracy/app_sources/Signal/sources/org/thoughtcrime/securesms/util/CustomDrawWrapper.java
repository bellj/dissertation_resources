package org.thoughtcrime.securesms.util;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: CustomDrawWrapper.kt */
@Metadata(bv = {}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0018\u0010\n\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\t¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016R\u0014\u0010\u0007\u001a\u00020\u00068\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\bR&\u0010\n\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\t8\u0002X\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/util/CustomDrawWrapper;", "Landroid/graphics/drawable/LayerDrawable;", "Landroid/graphics/Canvas;", "canvas", "", "draw", "Landroid/graphics/drawable/Drawable;", "wrapped", "Landroid/graphics/drawable/Drawable;", "Lkotlin/Function2;", "drawFn", "Lkotlin/jvm/functions/Function2;", "<init>", "(Landroid/graphics/drawable/Drawable;Lkotlin/jvm/functions/Function2;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class CustomDrawWrapper extends LayerDrawable {
    private final Function2<Drawable, Canvas, Unit> drawFn;
    private final Drawable wrapped;

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function2<? super android.graphics.drawable.Drawable, ? super android.graphics.Canvas, kotlin.Unit> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CustomDrawWrapper(Drawable drawable, Function2<? super Drawable, ? super Canvas, Unit> function2) {
        super(new Drawable[]{drawable});
        Intrinsics.checkNotNullParameter(drawable, "wrapped");
        Intrinsics.checkNotNullParameter(function2, "drawFn");
        this.wrapped = drawable;
        this.drawFn = function2;
    }

    @Override // android.graphics.drawable.LayerDrawable, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        this.drawFn.invoke(this.wrapped, canvas);
    }
}
