package org.thoughtcrime.securesms.util.adapter.mapping;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MappingModelList$2$$ExternalSyntheticLambda2 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return MappingModelList.AnonymousClass2.lambda$finisher$0((MappingModelList) obj);
    }
}
