package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import androidx.documentfile.provider.DocumentFile;
import java.io.File;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.backup.BackupPassphrase;
import org.thoughtcrime.securesms.database.NoExternalStorageException;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.BackupUtil;

/* loaded from: classes4.dex */
public class BackupUtil {
    public static final int PASSPHRASE_LENGTH;
    private static final String TAG = Log.tag(BackupUtil.class);

    public static String getLastBackupTime(Context context, Locale locale) {
        try {
            BackupInfo latestBackup = getLatestBackup();
            if (latestBackup == null) {
                return context.getString(R.string.BackupUtil_never);
            }
            return DateUtils.getExtendedRelativeTimeSpanString(context, locale, latestBackup.getTimestamp());
        } catch (NoExternalStorageException e) {
            Log.w(TAG, e);
            return context.getString(R.string.BackupUtil_unknown);
        }
    }

    public static boolean isUserSelectionRequired(Context context) {
        return Build.VERSION.SDK_INT >= 29 && !Permissions.hasAll(context, "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE");
    }

    public static boolean canUserAccessBackupDirectory(Context context) {
        DocumentFile fromTreeUri;
        if (!isUserSelectionRequired(context)) {
            return Permissions.hasAll(context, "android.permission.WRITE_EXTERNAL_STORAGE");
        }
        Uri signalBackupDirectory = SignalStore.settings().getSignalBackupDirectory();
        if (signalBackupDirectory != null && (fromTreeUri = DocumentFile.fromTreeUri(context, signalBackupDirectory)) != null && fromTreeUri.exists() && fromTreeUri.canRead() && fromTreeUri.canWrite()) {
            return true;
        }
        return false;
    }

    public static BackupInfo getLatestBackup() throws NoExternalStorageException {
        List<BackupInfo> allBackupsNewestFirst = getAllBackupsNewestFirst();
        if (allBackupsNewestFirst.isEmpty()) {
            return null;
        }
        return allBackupsNewestFirst.get(0);
    }

    public static void deleteAllBackups() {
        Log.i(TAG, "Deleting all backups");
        try {
            for (BackupInfo backupInfo : getAllBackupsNewestFirst()) {
                backupInfo.delete();
            }
        } catch (NoExternalStorageException e) {
            Log.w(TAG, e);
        }
    }

    public static void deleteOldBackups() {
        Log.i(TAG, "Deleting older backups");
        try {
            List<BackupInfo> allBackupsNewestFirst = getAllBackupsNewestFirst();
            for (int i = 2; i < allBackupsNewestFirst.size(); i++) {
                allBackupsNewestFirst.get(i).delete();
            }
        } catch (NoExternalStorageException e) {
            Log.w(TAG, e);
        }
    }

    public static void disableBackups(Context context) {
        Uri signalBackupDirectory;
        BackupPassphrase.set(context, null);
        SignalStore.settings().setBackupEnabled(false);
        deleteAllBackups();
        if (isUserSelectionRequired(context) && (signalBackupDirectory = SignalStore.settings().getSignalBackupDirectory()) != null) {
            SignalStore.settings().clearSignalBackupDirectory();
            try {
                context.getContentResolver().releasePersistableUriPermission(signalBackupDirectory, 3);
            } catch (SecurityException e) {
                Log.w(TAG, "Could not release permissions", e);
            }
        }
    }

    private static List<BackupInfo> getAllBackupsNewestFirst() throws NoExternalStorageException {
        if (isUserSelectionRequired(ApplicationDependencies.getApplication())) {
            return getAllBackupsNewestFirstApi29();
        }
        return getAllBackupsNewestFirstLegacy();
    }

    private static List<BackupInfo> getAllBackupsNewestFirstApi29() {
        Uri signalBackupDirectory = SignalStore.settings().getSignalBackupDirectory();
        if (signalBackupDirectory == null) {
            Log.i(TAG, "Backup directory is not set. Returning an empty list.");
            return Collections.emptyList();
        }
        DocumentFile fromTreeUri = DocumentFile.fromTreeUri(ApplicationDependencies.getApplication(), signalBackupDirectory);
        if (fromTreeUri == null || !fromTreeUri.exists() || !fromTreeUri.canRead()) {
            Log.w(TAG, "Backup directory is inaccessible. Returning an empty list.");
            return Collections.emptyList();
        }
        DocumentFile[] listFiles = fromTreeUri.listFiles();
        ArrayList arrayList = new ArrayList(listFiles.length);
        for (DocumentFile documentFile : listFiles) {
            if (documentFile.isFile() && documentFile.getName() != null && documentFile.getName().endsWith(".backup")) {
                long backupTimestamp = getBackupTimestamp(documentFile.getName());
                if (backupTimestamp != -1) {
                    arrayList.add(new BackupInfo(backupTimestamp, documentFile.length(), documentFile.getUri()));
                }
            }
        }
        Collections.sort(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.util.BackupUtil$$ExternalSyntheticLambda1
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return BackupUtil.lambda$getAllBackupsNewestFirstApi29$0((BackupUtil.BackupInfo) obj, (BackupUtil.BackupInfo) obj2);
            }
        });
        return arrayList;
    }

    public static /* synthetic */ int lambda$getAllBackupsNewestFirstApi29$0(BackupInfo backupInfo, BackupInfo backupInfo2) {
        return Long.compare(backupInfo2.timestamp, backupInfo.timestamp);
    }

    public static BackupInfo getBackupInfoFromSingleUri(Context context, Uri uri) throws BackupFileException {
        DocumentFile fromSingleUri = DocumentFile.fromSingleUri(context, uri);
        Objects.requireNonNull(fromSingleUri);
        return getBackupInfoFromSingleDocumentFile(fromSingleUri);
    }

    static BackupInfo getBackupInfoFromSingleDocumentFile(DocumentFile documentFile) throws BackupFileException {
        BackupFileState backupFileState = getBackupFileState(documentFile);
        if (backupFileState.isSuccess()) {
            String name = documentFile.getName();
            Objects.requireNonNull(name);
            return new BackupInfo(getBackupTimestamp(name), documentFile.length(), documentFile.getUri());
        }
        Log.w(TAG, "Could not load backup info.");
        backupFileState.throwIfError();
        return null;
    }

    private static List<BackupInfo> getAllBackupsNewestFirstLegacy() throws NoExternalStorageException {
        File[] listFiles = StorageUtil.getOrCreateBackupDirectory().listFiles();
        ArrayList arrayList = new ArrayList(listFiles.length);
        for (File file : listFiles) {
            if (file.isFile() && file.getAbsolutePath().endsWith(".backup")) {
                long backupTimestamp = getBackupTimestamp(file.getName());
                if (backupTimestamp != -1) {
                    arrayList.add(new BackupInfo(backupTimestamp, file.length(), Uri.fromFile(file)));
                }
            }
        }
        Collections.sort(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.util.BackupUtil$$ExternalSyntheticLambda0
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return BackupUtil.lambda$getAllBackupsNewestFirstLegacy$1((BackupUtil.BackupInfo) obj, (BackupUtil.BackupInfo) obj2);
            }
        });
        return arrayList;
    }

    public static /* synthetic */ int lambda$getAllBackupsNewestFirstLegacy$1(BackupInfo backupInfo, BackupInfo backupInfo2) {
        return Long.compare(backupInfo2.timestamp, backupInfo.timestamp);
    }

    public static String[] generateBackupPassphrase() {
        String[] strArr = new String[6];
        byte[] bArr = new byte[30];
        new SecureRandom().nextBytes(bArr);
        for (int i = 0; i < 30; i += 5) {
            strArr[i / 5] = String.format(Locale.ENGLISH, "%05d", Long.valueOf(ByteUtil.byteArray5ToLong(bArr, i) % 100000));
        }
        return strArr;
    }

    public static boolean hasBackupFiles(Context context) {
        File[] listFiles;
        if (Permissions.hasAll(context, "android.permission.READ_EXTERNAL_STORAGE")) {
            try {
                File backupDirectory = StorageUtil.getBackupDirectory();
                if (!backupDirectory.exists() || !backupDirectory.isDirectory() || (listFiles = backupDirectory.listFiles()) == null) {
                    return false;
                }
                if (listFiles.length > 0) {
                    return true;
                }
                return false;
            } catch (NoExternalStorageException e) {
                Log.w(TAG, "Failed to read storage!", e);
            }
        }
        return false;
    }

    private static long getBackupTimestamp(String str) {
        String[] split = str.split("[.]");
        if (split.length != 2) {
            return -1;
        }
        String[] split2 = split[0].split("\\-");
        if (split2.length != 7) {
            return -1;
        }
        try {
            Calendar instance = Calendar.getInstance();
            instance.set(1, Integer.parseInt(split2[1]));
            instance.set(2, Integer.parseInt(split2[2]) - 1);
            instance.set(5, Integer.parseInt(split2[3]));
            instance.set(11, Integer.parseInt(split2[4]));
            instance.set(12, Integer.parseInt(split2[5]));
            instance.set(13, Integer.parseInt(split2[6]));
            instance.set(14, 0);
            return instance.getTimeInMillis();
        } catch (NumberFormatException e) {
            Log.w(TAG, e);
            return -1;
        }
    }

    private static BackupFileState getBackupFileState(DocumentFile documentFile) {
        if (!documentFile.exists()) {
            return BackupFileState.NOT_FOUND;
        }
        if (!documentFile.canRead()) {
            return BackupFileState.NOT_READABLE;
        }
        if (Util.isEmpty(documentFile.getName()) || !documentFile.getName().endsWith(".backup")) {
            return BackupFileState.UNSUPPORTED_FILE_EXTENSION;
        }
        return BackupFileState.READABLE;
    }

    /* loaded from: classes4.dex */
    public enum BackupFileState {
        READABLE("The document at the specified Uri looks like a readable backup."),
        NOT_FOUND("The document at the specified Uri cannot be found."),
        NOT_READABLE("The document at the specified Uri cannot be read."),
        UNSUPPORTED_FILE_EXTENSION("The document at the specified Uri has an unsupported file extension.");
        
        private final String message;

        BackupFileState(String str) {
            this.message = str;
        }

        public boolean isSuccess() {
            return this == READABLE;
        }

        public void throwIfError() throws BackupFileException {
            if (!isSuccess()) {
                throw new BackupFileException(this);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class BackupFileException extends Exception {
        private final BackupFileState state;

        BackupFileException(BackupFileState backupFileState) {
            super(backupFileState.message);
            this.state = backupFileState;
        }

        public BackupFileState getState() {
            return this.state;
        }
    }

    /* loaded from: classes4.dex */
    public static class BackupInfo {
        private final long size;
        private final long timestamp;
        private final Uri uri;

        BackupInfo(long j, long j2, Uri uri) {
            this.timestamp = j;
            this.size = j2;
            this.uri = uri;
        }

        public long getTimestamp() {
            return this.timestamp;
        }

        public long getSize() {
            return this.size;
        }

        public Uri getUri() {
            return this.uri;
        }

        public void delete() {
            String path = this.uri.getPath();
            Objects.requireNonNull(path);
            File file = new File(path);
            if (file.exists()) {
                String str = BackupUtil.TAG;
                Log.i(str, "Deleting File: " + file.getAbsolutePath());
                if (!file.delete()) {
                    String str2 = BackupUtil.TAG;
                    Log.w(str2, "Delete failed: " + file.getAbsolutePath());
                    return;
                }
                return;
            }
            DocumentFile fromSingleUri = DocumentFile.fromSingleUri(ApplicationDependencies.getApplication(), this.uri);
            if (fromSingleUri != null && fromSingleUri.exists()) {
                String str3 = BackupUtil.TAG;
                Log.i(str3, "Deleting DocumentFile: " + this.uri);
                if (!fromSingleUri.delete()) {
                    String str4 = BackupUtil.TAG;
                    Log.w(str4, "Delete failed: " + this.uri);
                }
            }
        }
    }
}
