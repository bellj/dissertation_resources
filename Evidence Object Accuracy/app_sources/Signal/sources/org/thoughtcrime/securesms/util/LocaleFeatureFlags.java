package org.thoughtcrime.securesms.util;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import j$.util.Optional;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.mms.PushMediaConstraints;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class LocaleFeatureFlags {
    private static final String COUNTRY_WILDCARD;
    private static final int NOT_FOUND;
    private static final String TAG = Log.tag(LocaleFeatureFlags.class);

    public static boolean isInDonateMegaphone() {
        return isEnabled(FeatureFlags.DONATE_MEGAPHONE, FeatureFlags.donateMegaphone());
    }

    public static Optional<PushMediaConstraints.MediaConfig> getMediaQualityLevel() {
        return Optional.ofNullable(PushMediaConstraints.MediaConfig.forLevel(getCountryValue(parseCountryValues(FeatureFlags.getMediaQualityLevels(), -1), Recipient.self().getE164().orElse(""), -1)));
    }

    public static boolean shouldSuggestSms() {
        return !new HashSet(Arrays.asList(FeatureFlags.suggestSmsBlacklist().split(","))).contains(String.valueOf(PhoneNumberFormatter.getLocalCountryCode()));
    }

    public static boolean shouldShowReleaseNote(String str, String str2) {
        return isEnabled(str, str2);
    }

    private static boolean isEnabled(String str, String str2) {
        Map<String, Integer> parseCountryValues = parseCountryValues(str2, 0);
        Recipient self = Recipient.self();
        if (parseCountryValues.isEmpty() || !self.getE164().isPresent() || !self.getServiceId().isPresent() || ((long) getCountryValue(parseCountryValues, self.getE164().orElse(""), 0)) <= BucketingUtil.bucket(str, self.requireServiceId().uuid(), 1000000)) {
            return false;
        }
        return true;
    }

    static Map<String, Integer> parseCountryValues(String str, int i) {
        HashMap hashMap = new HashMap();
        for (String str2 : str.split(",")) {
            String[] split = str2.split(":");
            if (split.length == 2 && !split[0].isEmpty()) {
                hashMap.put(split[0], Integer.valueOf(Util.parseInt(split[1], i)));
            }
        }
        return hashMap;
    }

    static int getCountryValue(Map<String, Integer> map, String str, int i) {
        Integer num = map.get(COUNTRY_WILDCARD);
        try {
            String valueOf = String.valueOf(PhoneNumberUtil.getInstance().parse(str, "").getCountryCode());
            if (map.containsKey(valueOf)) {
                num = map.get(valueOf);
            }
            return num != null ? num.intValue() : i;
        } catch (NumberParseException unused) {
            Log.d(TAG, "Unable to determine country code for bucketing.");
            return i;
        }
    }
}
