package org.thoughtcrime.securesms.util;

import android.content.Context;
import com.google.protobuf.ByteString;
import j$.util.Optional;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.StringUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.MessageGroupContext;
import org.thoughtcrime.securesms.mms.OutgoingGroupUpdateMessage;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.messages.SignalServiceContent;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceGroup;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupContext;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupV2;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class GroupUtil {
    private static final String TAG = Log.tag(GroupUtil.class);

    private GroupUtil() {
    }

    public static SignalServiceGroupContext getGroupContextIfPresent(SignalServiceContent signalServiceContent) {
        if (signalServiceContent == null) {
            return null;
        }
        if (signalServiceContent.getDataMessage().isPresent() && signalServiceContent.getDataMessage().get().getGroupContext().isPresent()) {
            return signalServiceContent.getDataMessage().get().getGroupContext().get();
        }
        if (signalServiceContent.getSyncMessage().isPresent() && signalServiceContent.getSyncMessage().get().getSent().isPresent() && signalServiceContent.getSyncMessage().get().getSent().get().getDataMessage().isPresent() && signalServiceContent.getSyncMessage().get().getSent().get().getDataMessage().get().getGroupContext().isPresent()) {
            return signalServiceContent.getSyncMessage().get().getSent().get().getDataMessage().get().getGroupContext().get();
        }
        if (!signalServiceContent.getStoryMessage().isPresent() || !signalServiceContent.getStoryMessage().get().getGroupContext().isPresent()) {
            return null;
        }
        try {
            return SignalServiceGroupContext.create(null, signalServiceContent.getStoryMessage().get().getGroupContext().get());
        } catch (InvalidMessageException e) {
            throw new AssertionError(e);
        }
    }

    public static GroupId idFromGroupContext(SignalServiceGroupContext signalServiceGroupContext) throws BadGroupIdException {
        if (signalServiceGroupContext.getGroupV1().isPresent()) {
            return GroupId.v1(signalServiceGroupContext.getGroupV1().get().getGroupId());
        }
        if (signalServiceGroupContext.getGroupV2().isPresent()) {
            return GroupId.v2(signalServiceGroupContext.getGroupV2().get().getMasterKey());
        }
        throw new AssertionError();
    }

    public static GroupId idFromGroupContextOrThrow(SignalServiceGroupContext signalServiceGroupContext) {
        try {
            return idFromGroupContext(signalServiceGroupContext);
        } catch (BadGroupIdException e) {
            throw new AssertionError(e);
        }
    }

    public static Optional<GroupId> idFromGroupContext(Optional<SignalServiceGroupContext> optional) throws BadGroupIdException {
        if (optional.isPresent()) {
            return Optional.of(idFromGroupContext(optional.get()));
        }
        return Optional.empty();
    }

    public static GroupMasterKey requireMasterKey(byte[] bArr) {
        try {
            return new GroupMasterKey(bArr);
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public static GroupDescription getNonV2GroupDescription(Context context, String str) {
        if (str == null) {
            return new GroupDescription(context, null);
        }
        try {
            return new GroupDescription(context, new MessageGroupContext(str, false));
        } catch (IOException e) {
            Log.w(TAG, e);
            return new GroupDescription(context, null);
        }
    }

    public static void setDataMessageGroupContext(Context context, SignalServiceDataMessage.Builder builder, GroupId.Push push) {
        if (push.isV2()) {
            GroupDatabase.V2GroupProperties requireV2GroupProperties = SignalDatabase.groups().requireGroup(push).requireV2GroupProperties();
            builder.asGroupMessage(SignalServiceGroupV2.newBuilder(requireV2GroupProperties.getGroupMasterKey()).withRevision(requireV2GroupProperties.getGroupRevision()).build());
            return;
        }
        builder.asGroupMessage(new SignalServiceGroup(push.getDecodedId()));
    }

    public static OutgoingGroupUpdateMessage createGroupV1LeaveMessage(GroupId.V1 v1, Recipient recipient) {
        return new OutgoingGroupUpdateMessage(recipient, SignalServiceProtos.GroupContext.newBuilder().setId(ByteString.copyFrom(v1.getDecodedId())).setType(SignalServiceProtos.GroupContext.Type.QUIT).build(), (Attachment) null, System.currentTimeMillis(), 0L, false, (QuoteModel) null, (List<Contact>) Collections.emptyList(), (List<LinkPreview>) Collections.emptyList(), (List<Mention>) Collections.emptyList());
    }

    /* loaded from: classes4.dex */
    public static class GroupDescription {
        private final Context context;
        private final MessageGroupContext groupContext;
        private final List<RecipientId> members;

        GroupDescription(Context context, MessageGroupContext messageGroupContext) {
            this.context = context.getApplicationContext();
            this.groupContext = messageGroupContext;
            List<RecipientId> list = null;
            if (messageGroupContext == null) {
                this.members = null;
                return;
            }
            List<RecipientId> membersListExcludingSelf = messageGroupContext.getMembersListExcludingSelf();
            this.members = !membersListExcludingSelf.isEmpty() ? membersListExcludingSelf : list;
        }

        public String toString(Recipient recipient) {
            StringBuilder sb = new StringBuilder();
            Context context = this.context;
            sb.append(context.getString(R.string.MessageRecord_s_updated_group, recipient.getDisplayName(context)));
            MessageGroupContext messageGroupContext = this.groupContext;
            if (messageGroupContext == null) {
                return sb.toString();
            }
            String isolateBidi = StringUtil.isolateBidi(messageGroupContext.getName());
            List<RecipientId> list = this.members;
            if (list != null && list.size() > 0) {
                sb.append("\n");
                sb.append(this.context.getResources().getQuantityString(R.plurals.GroupUtil_joined_the_group, this.members.size(), toString(this.members)));
            }
            if (!isolateBidi.trim().isEmpty()) {
                if (this.members != null) {
                    sb.append(" ");
                } else {
                    sb.append("\n");
                }
                sb.append(this.context.getString(R.string.GroupUtil_group_name_is_now, isolateBidi));
            }
            return sb.toString();
        }

        private String toString(List<RecipientId> list) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                sb.append(Recipient.live(list.get(i)).get().getDisplayName(this.context));
                if (i != list.size() - 1) {
                    sb.append(", ");
                }
            }
            return sb.toString();
        }
    }
}
