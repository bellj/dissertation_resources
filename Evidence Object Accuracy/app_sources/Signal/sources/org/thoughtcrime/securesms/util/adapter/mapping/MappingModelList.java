package org.thoughtcrime.securesms.util.adapter.mapping;

import com.google.common.collect.Sets;
import j$.util.function.BiConsumer;
import j$.util.function.BinaryOperator;
import j$.util.function.Function;
import j$.util.function.Supplier;
import j$.util.stream.Collector;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/* loaded from: classes4.dex */
public class MappingModelList extends ArrayList<MappingModel<?>> {
    public MappingModelList() {
    }

    public MappingModelList(Collection<? extends MappingModel<?>> collection) {
        super(collection);
    }

    public static MappingModelList singleton(MappingModel<?> mappingModel) {
        MappingModelList mappingModelList = new MappingModelList();
        mappingModelList.add(mappingModel);
        return mappingModelList;
    }

    public static Collector<MappingModel<?>, MappingModelList, MappingModelList> collect() {
        return new Collector<MappingModel<?>, MappingModelList, MappingModelList>() { // from class: org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList.1
            @Override // j$.util.stream.Collector
            public Supplier<MappingModelList> supplier() {
                return new MappingModelList$1$$ExternalSyntheticLambda1();
            }

            @Override // j$.util.stream.Collector
            public BiConsumer<MappingModelList, MappingModel<?>> accumulator() {
                return new MappingModelList$1$$ExternalSyntheticLambda0();
            }

            @Override // j$.util.stream.Collector
            public BinaryOperator<MappingModelList> combiner() {
                return new MappingModelList$1$$ExternalSyntheticLambda2();
            }

            @Override // j$.util.stream.Collector
            public Function<MappingModelList, MappingModelList> finisher() {
                return Function.CC.identity();
            }

            @Override // j$.util.stream.Collector
            public Set<Collector.Characteristics> characteristics() {
                return Sets.immutableEnumSet(Collector.Characteristics.IDENTITY_FINISH, new Collector.Characteristics[0]);
            }
        };
    }

    public static com.annimon.stream.Collector<MappingModel<?>, MappingModelList, MappingModelList> toMappingModelList() {
        return new com.annimon.stream.Collector<MappingModel<?>, MappingModelList, MappingModelList>() { // from class: org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList.2
            public static /* synthetic */ MappingModelList lambda$finisher$0(MappingModelList mappingModelList) {
                return mappingModelList;
            }

            @Override // com.annimon.stream.Collector
            public com.annimon.stream.function.Supplier<MappingModelList> supplier() {
                return new MappingModelList$2$$ExternalSyntheticLambda0();
            }

            @Override // com.annimon.stream.Collector
            public com.annimon.stream.function.BiConsumer<MappingModelList, MappingModel<?>> accumulator() {
                return new MappingModelList$2$$ExternalSyntheticLambda1();
            }

            @Override // com.annimon.stream.Collector
            public com.annimon.stream.function.Function<MappingModelList, MappingModelList> finisher() {
                return new MappingModelList$2$$ExternalSyntheticLambda2();
            }
        };
    }
}
