package org.thoughtcrime.securesms.util;

import j$.lang.Iterable;
import j$.util.Collection$CC;
import j$.util.function.Consumer;
import j$.util.function.Predicate;
import j$.util.function.UnaryOperator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.CollectionToArray;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.markers.KMappedMarker;
import kotlin.ranges.RangesKt___RangesKt;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* compiled from: RecipientAccessList.kt */
@Metadata(d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u001e\n\u0002\b\u0005\n\u0002\u0010(\n\u0002\b\u0002\n\u0002\u0010*\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001¢\u0006\u0002\u0010\u0004J\u0011\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0003J\u0017\u0010\u0017\u001a\u00020\u00152\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u0019H\u0001J\u0011\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0011H\u0003J\u0011\u0010\u001c\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0002H\u0001J\t\u0010\u001d\u001a\u00020\u0015H\u0001J\u000f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00020\u001fH\u0003J\u0011\u0010 \u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0002H\u0001J\u000f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00020\"H\u0001J\u0017\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00020\"2\u0006\u0010\u001b\u001a\u00020\u0011H\u0001J\u000e\u0010#\u001a\u00020\u00022\u0006\u0010$\u001a\u00020%J\u000e\u0010&\u001a\u00020'2\u0006\u0010$\u001a\u00020%J\u001f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010)\u001a\u00020\u00112\u0006\u0010*\u001a\u00020\u0011H\u0001R'\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\b\u0010\tR'\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u000b\u001a\u0004\b\u000e\u0010\tR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0010\u001a\u00020\u0011X\u0005¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/util/RecipientAccessList;", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "(Ljava/util/List;)V", "byE164", "", "", "getByE164", "()Ljava/util/Map;", "byE164$delegate", "Lkotlin/Lazy;", "byServiceId", "Lorg/whispersystems/signalservice/api/push/ServiceId;", "getByServiceId", "byServiceId$delegate", MediaPreviewActivity.SIZE_EXTRA, "", "getSize", "()I", "contains", "", "element", "containsAll", "elements", "", "get", "index", "indexOf", "isEmpty", "iterator", "", "lastIndexOf", "listIterator", "", "requireByAddress", "address", "Lorg/whispersystems/signalservice/api/push/SignalServiceAddress;", "requireIdByAddress", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "subList", "fromIndex", "toIndex", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RecipientAccessList implements List<Recipient>, KMappedMarker, j$.util.List {
    private final Lazy byE164$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Map<String, ? extends Recipient>>(this) { // from class: org.thoughtcrime.securesms.util.RecipientAccessList$byE164$2
        final /* synthetic */ RecipientAccessList this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        /* Return type fixed from 'java.util.Map<java.lang.String, org.thoughtcrime.securesms.recipients.Recipient>' to match base method */
        @Override // kotlin.jvm.functions.Function0
        public final Map<String, ? extends Recipient> invoke() {
            List list = this.this$0.recipients;
            ArrayList arrayList = new ArrayList();
            for (Object obj : list) {
                if (((Recipient) obj).hasE164()) {
                    arrayList.add(obj);
                }
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10)), 16));
            for (Object obj2 : arrayList) {
                String requireE164 = ((Recipient) obj2).requireE164();
                Intrinsics.checkNotNullExpressionValue(requireE164, "it.requireE164()");
                linkedHashMap.put(requireE164, obj2);
            }
            return linkedHashMap;
        }
    });
    private final Lazy byServiceId$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Map<ServiceId, ? extends Recipient>>(this) { // from class: org.thoughtcrime.securesms.util.RecipientAccessList$byServiceId$2
        final /* synthetic */ RecipientAccessList this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        /* Return type fixed from 'java.util.Map<org.whispersystems.signalservice.api.push.ServiceId, org.thoughtcrime.securesms.recipients.Recipient>' to match base method */
        @Override // kotlin.jvm.functions.Function0
        public final Map<ServiceId, ? extends Recipient> invoke() {
            List list = this.this$0.recipients;
            ArrayList arrayList = new ArrayList();
            for (Object obj : list) {
                if (((Recipient) obj).hasServiceId()) {
                    arrayList.add(obj);
                }
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10)), 16));
            for (Object obj2 : arrayList) {
                ServiceId requireServiceId = ((Recipient) obj2).requireServiceId();
                Intrinsics.checkNotNullExpressionValue(requireServiceId, "it.requireServiceId()");
                linkedHashMap.put(requireServiceId, obj2);
            }
            return linkedHashMap;
        }
    });
    private final List<Recipient> recipients;

    @Override // java.util.List, j$.util.List
    public /* bridge */ /* synthetic */ void add(int i, Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void add(int i, Recipient recipient) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public /* bridge */ /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean add(Recipient recipient) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(int i, Collection<? extends Recipient> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(Collection<? extends Recipient> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean contains(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, "element");
        return this.recipients.contains(recipient);
    }

    public boolean containsAll(Collection<? extends Object> collection) {
        Intrinsics.checkNotNullParameter(collection, "elements");
        return this.recipients.containsAll(collection);
    }

    public /* synthetic */ void forEach(Consumer consumer) {
        Iterable.CC.$default$forEach(this, consumer);
    }

    public /* synthetic */ void forEach(java.util.function.Consumer consumer) {
        forEach(Consumer.VivifiedWrapper.convert(consumer));
    }

    public Recipient get(int i) {
        return this.recipients.get(i);
    }

    public int getSize() {
        return this.recipients.size();
    }

    public int indexOf(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, "element");
        return this.recipients.indexOf(recipient);
    }

    public boolean isEmpty() {
        return this.recipients.isEmpty();
    }

    public Iterator<Recipient> iterator() {
        return this.recipients.iterator();
    }

    public int lastIndexOf(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, "element");
        return this.recipients.lastIndexOf(recipient);
    }

    public ListIterator<Recipient> listIterator() {
        return this.recipients.listIterator();
    }

    public ListIterator<Recipient> listIterator(int i) {
        return this.recipients.listIterator(i);
    }

    public Recipient remove(int i) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public /* synthetic */ boolean removeIf(Predicate predicate) {
        return Collection$CC.$default$removeIf(this, predicate);
    }

    public /* synthetic */ boolean removeIf(java.util.function.Predicate predicate) {
        return removeIf(Predicate.VivifiedWrapper.convert(predicate));
    }

    public void replaceAll(UnaryOperator<Recipient> unaryOperator) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public /* synthetic */ void replaceAll(java.util.function.UnaryOperator unaryOperator) {
        replaceAll(UnaryOperator.VivifiedWrapper.convert(unaryOperator));
    }

    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public /* bridge */ /* synthetic */ Object set(int i, Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public Recipient set(int i, Recipient recipient) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void sort(Comparator<? super Recipient> comparator) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public List<Recipient> subList(int i, int i2) {
        return this.recipients.subList(i, i2);
    }

    public Object[] toArray() {
        return CollectionToArray.toArray(this);
    }

    public <T> T[] toArray(T[] tArr) {
        Intrinsics.checkNotNullParameter(tArr, "array");
        return (T[]) CollectionToArray.toArray(this, tArr);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
    /* JADX WARN: Multi-variable type inference failed */
    public RecipientAccessList(List<? extends Recipient> list) {
        Intrinsics.checkNotNullParameter(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        this.recipients = list;
    }

    public final /* bridge */ boolean contains(Object obj) {
        if (!(obj instanceof Recipient)) {
            return false;
        }
        return contains((Recipient) obj);
    }

    public final /* bridge */ int indexOf(Object obj) {
        if (!(obj instanceof Recipient)) {
            return -1;
        }
        return indexOf((Recipient) obj);
    }

    public final /* bridge */ int lastIndexOf(Object obj) {
        if (!(obj instanceof Recipient)) {
            return -1;
        }
        return lastIndexOf((Recipient) obj);
    }

    public final /* bridge */ int size() {
        return getSize();
    }

    private final Map<ServiceId, Recipient> getByServiceId() {
        return (Map) this.byServiceId$delegate.getValue();
    }

    private final Map<String, Recipient> getByE164() {
        return (Map) this.byE164$delegate.getValue();
    }

    public final Recipient requireByAddress(SignalServiceAddress signalServiceAddress) {
        Intrinsics.checkNotNullParameter(signalServiceAddress, "address");
        if (getByServiceId().containsKey(signalServiceAddress.getServiceId())) {
            Recipient recipient = getByServiceId().get(signalServiceAddress.getServiceId());
            Intrinsics.checkNotNull(recipient);
            return recipient;
        } else if (!signalServiceAddress.getNumber().isPresent() || !getByE164().containsKey(signalServiceAddress.getNumber().get())) {
            throw new IllegalArgumentException("Could not find a matching recipient!");
        } else {
            Recipient recipient2 = getByE164().get(signalServiceAddress.getNumber().get());
            Intrinsics.checkNotNull(recipient2);
            return recipient2;
        }
    }

    public final RecipientId requireIdByAddress(SignalServiceAddress signalServiceAddress) {
        Intrinsics.checkNotNullParameter(signalServiceAddress, "address");
        RecipientId id = requireByAddress(signalServiceAddress).getId();
        Intrinsics.checkNotNullExpressionValue(id, "requireByAddress(address).id");
        return id;
    }
}
