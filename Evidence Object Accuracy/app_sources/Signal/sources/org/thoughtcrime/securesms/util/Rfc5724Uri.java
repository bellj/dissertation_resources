package org.thoughtcrime.securesms.util;

import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes4.dex */
public class Rfc5724Uri {
    private final String path = parsePath();
    private final Map<String, String> queryParams = parseQueryParams();
    private final String schema = parseSchema();
    private final String uri;

    public Rfc5724Uri(String str) throws URISyntaxException {
        this.uri = str;
    }

    private String parseSchema() throws URISyntaxException {
        String[] split = this.uri.split(":");
        if (split.length >= 1 && !split[0].isEmpty()) {
            return split[0];
        }
        throw new URISyntaxException(this.uri, "invalid schema");
    }

    private String parsePath() throws URISyntaxException {
        String[] split = this.uri.split("\\?")[0].split(":", 2);
        if (split.length >= 2 && !split[1].isEmpty()) {
            return split[1];
        }
        throw new URISyntaxException(this.uri, "invalid path");
    }

    private Map<String, String> parseQueryParams() throws URISyntaxException {
        HashMap hashMap = new HashMap();
        if (this.uri.split("\\?").length < 2) {
            return hashMap;
        }
        for (String str : this.uri.split("\\?")[1].split("&")) {
            String[] split = str.split("=");
            if (split.length == 1) {
                hashMap.put(split[0], "");
            } else {
                hashMap.put(split[0], URLDecoder.decode(split[1]));
            }
        }
        return hashMap;
    }

    public String getSchema() {
        return this.schema;
    }

    public String getPath() {
        return this.path;
    }

    public Map<String, String> getQueryParams() {
        return this.queryParams;
    }
}
