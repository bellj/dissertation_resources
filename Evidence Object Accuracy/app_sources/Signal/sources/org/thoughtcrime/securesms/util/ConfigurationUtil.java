package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.content.res.Configuration;

/* loaded from: classes4.dex */
public final class ConfigurationUtil {
    private ConfigurationUtil() {
    }

    public static int getNightModeConfiguration(Context context) {
        return getNightModeConfiguration(context.getResources().getConfiguration());
    }

    public static int getNightModeConfiguration(Configuration configuration) {
        return configuration.uiMode & 48;
    }

    public static float getFontScale(Configuration configuration) {
        return configuration.fontScale;
    }
}
