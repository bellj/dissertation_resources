package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.content.DialogInterface;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleEmitter;
import io.reactivex.rxjava3.core.SingleOnSubscribe;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.util.task.ProgressDialogAsyncTask;

/* compiled from: DeleteDialog.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J$\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J,\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\r2\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002JB\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\n0\u000f2\u0006\u0010\f\u001a\u00020\r2\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\b\b\u0002\u0010\u0010\u001a\u00020\u00112\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00112\b\b\u0002\u0010\u0013\u001a\u00020\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/util/DeleteDialog;", "", "()V", "deleteForEveryone", "", "messageRecords", "", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "emitter", "Lio/reactivex/rxjava3/core/SingleEmitter;", "", "handleDeleteForEveryone", "context", "Landroid/content/Context;", "show", "Lio/reactivex/rxjava3/core/Single;", MultiselectForwardFragment.DIALOG_TITLE, "", "message", "forceRemoteDelete", "DeleteProgressDialogAsyncTask", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DeleteDialog {
    public static final DeleteDialog INSTANCE = new DeleteDialog();

    private DeleteDialog() {
    }

    public static /* synthetic */ Single show$default(DeleteDialog deleteDialog, Context context, Set set, CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            charSequence = context.getResources().getQuantityString(R.plurals.ConversationFragment_delete_selected_messages, set.size(), Integer.valueOf(set.size()));
            Intrinsics.checkNotNullExpressionValue(charSequence, "context.resources.getQua…ize, messageRecords.size)");
        }
        if ((i & 8) != 0) {
            charSequence2 = null;
        }
        return deleteDialog.show(context, set, charSequence, charSequence2, (i & 16) != 0 ? false : z);
    }

    public final Single<Boolean> show(Context context, Set<? extends MessageRecord> set, CharSequence charSequence, CharSequence charSequence2, boolean z) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(set, "messageRecords");
        Intrinsics.checkNotNullParameter(charSequence, MultiselectForwardFragment.DIALOG_TITLE);
        Single<Boolean> create = Single.create(new SingleOnSubscribe(context, charSequence, charSequence2, z, set) { // from class: org.thoughtcrime.securesms.util.DeleteDialog$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ CharSequence f$1;
            public final /* synthetic */ CharSequence f$2;
            public final /* synthetic */ boolean f$3;
            public final /* synthetic */ Set f$4;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // io.reactivex.rxjava3.core.SingleOnSubscribe
            public final void subscribe(SingleEmitter singleEmitter) {
                DeleteDialog.m3237show$lambda5(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, singleEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create { emitter ->\n    … }\n    builder.show()\n  }");
        return create;
    }

    /* renamed from: show$lambda-5 */
    public static final void m3237show$lambda5(Context context, CharSequence charSequence, CharSequence charSequence2, boolean z, Set set, SingleEmitter singleEmitter) {
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(charSequence, "$title");
        Intrinsics.checkNotNullParameter(set, "$messageRecords");
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context);
        materialAlertDialogBuilder.setTitle(charSequence);
        materialAlertDialogBuilder.setMessage(charSequence2);
        materialAlertDialogBuilder.setCancelable(true);
        if (z) {
            materialAlertDialogBuilder.setPositiveButton(R.string.ConversationFragment_delete_for_everyone, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(set, singleEmitter) { // from class: org.thoughtcrime.securesms.util.DeleteDialog$$ExternalSyntheticLambda1
                public final /* synthetic */ Set f$0;
                public final /* synthetic */ SingleEmitter f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    DeleteDialog.m3238show$lambda5$lambda0(this.f$0, this.f$1, dialogInterface, i);
                }
            });
        } else {
            materialAlertDialogBuilder.setPositiveButton(R.string.ConversationFragment_delete_for_me, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(context, set, singleEmitter) { // from class: org.thoughtcrime.securesms.util.DeleteDialog$$ExternalSyntheticLambda2
                public final /* synthetic */ Context f$0;
                public final /* synthetic */ Set f$1;
                public final /* synthetic */ SingleEmitter f$2;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    DeleteDialog.m3239show$lambda5$lambda1(this.f$0, this.f$1, this.f$2, dialogInterface, i);
                }
            });
            if (RemoteDeleteUtil.isValidSend(set, System.currentTimeMillis())) {
                materialAlertDialogBuilder.setNeutralButton(R.string.ConversationFragment_delete_for_everyone, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(context, set, singleEmitter) { // from class: org.thoughtcrime.securesms.util.DeleteDialog$$ExternalSyntheticLambda3
                    public final /* synthetic */ Context f$0;
                    public final /* synthetic */ Set f$1;
                    public final /* synthetic */ SingleEmitter f$2;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        DeleteDialog.m3240show$lambda5$lambda2(this.f$0, this.f$1, this.f$2, dialogInterface, i);
                    }
                });
            }
        }
        materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.util.DeleteDialog$$ExternalSyntheticLambda4
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeleteDialog.m3241show$lambda5$lambda3(SingleEmitter.this, dialogInterface, i);
            }
        });
        materialAlertDialogBuilder.setOnCancelListener((DialogInterface.OnCancelListener) new DialogInterface.OnCancelListener() { // from class: org.thoughtcrime.securesms.util.DeleteDialog$$ExternalSyntheticLambda5
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                DeleteDialog.m3242show$lambda5$lambda4(SingleEmitter.this, dialogInterface);
            }
        });
        materialAlertDialogBuilder.show();
    }

    /* renamed from: show$lambda-5$lambda-0 */
    public static final void m3238show$lambda5$lambda0(Set set, SingleEmitter singleEmitter, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(set, "$messageRecords");
        DeleteDialog deleteDialog = INSTANCE;
        Intrinsics.checkNotNullExpressionValue(singleEmitter, "emitter");
        deleteDialog.deleteForEveryone(set, singleEmitter);
    }

    /* renamed from: show$lambda-5$lambda-1 */
    public static final void m3239show$lambda5$lambda1(Context context, Set set, SingleEmitter singleEmitter, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(set, "$messageRecords");
        Intrinsics.checkNotNullExpressionValue(singleEmitter, "emitter");
        new DeleteProgressDialogAsyncTask(context, set, new Function1<Boolean, Unit>(singleEmitter) { // from class: org.thoughtcrime.securesms.util.DeleteDialog$show$1$2$1
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
                invoke(bool);
                return Unit.INSTANCE;
            }

            public final void invoke(Boolean bool) {
                ((SingleEmitter) this.receiver).onSuccess(bool);
            }
        }).executeOnExecutor(SignalExecutors.BOUNDED, new Void[0]);
    }

    /* renamed from: show$lambda-5$lambda-2 */
    public static final void m3240show$lambda5$lambda2(Context context, Set set, SingleEmitter singleEmitter, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(set, "$messageRecords");
        DeleteDialog deleteDialog = INSTANCE;
        Intrinsics.checkNotNullExpressionValue(singleEmitter, "emitter");
        deleteDialog.handleDeleteForEveryone(context, set, singleEmitter);
    }

    /* renamed from: show$lambda-5$lambda-3 */
    public static final void m3241show$lambda5$lambda3(SingleEmitter singleEmitter, DialogInterface dialogInterface, int i) {
        singleEmitter.onSuccess(Boolean.FALSE);
    }

    /* renamed from: show$lambda-5$lambda-4 */
    public static final void m3242show$lambda5$lambda4(SingleEmitter singleEmitter, DialogInterface dialogInterface) {
        singleEmitter.onSuccess(Boolean.FALSE);
    }

    private final void handleDeleteForEveryone(Context context, Set<? extends MessageRecord> set, SingleEmitter<Boolean> singleEmitter) {
        if (SignalStore.uiHints().hasConfirmedDeleteForEveryoneOnce()) {
            deleteForEveryone(set, singleEmitter);
        } else {
            new MaterialAlertDialogBuilder(context).setMessage(R.string.ConversationFragment_this_message_will_be_deleted_for_everyone_in_the_conversation).setPositiveButton(R.string.ConversationFragment_delete_for_everyone, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(set, singleEmitter) { // from class: org.thoughtcrime.securesms.util.DeleteDialog$$ExternalSyntheticLambda7
                public final /* synthetic */ Set f$0;
                public final /* synthetic */ SingleEmitter f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    DeleteDialog.m3234handleDeleteForEveryone$lambda6(this.f$0, this.f$1, dialogInterface, i);
                }
            }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.util.DeleteDialog$$ExternalSyntheticLambda8
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    DeleteDialog.m3235handleDeleteForEveryone$lambda7(SingleEmitter.this, dialogInterface, i);
                }
            }).setOnCancelListener((DialogInterface.OnCancelListener) new DialogInterface.OnCancelListener() { // from class: org.thoughtcrime.securesms.util.DeleteDialog$$ExternalSyntheticLambda9
                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    DeleteDialog.m3236handleDeleteForEveryone$lambda8(SingleEmitter.this, dialogInterface);
                }
            }).show();
        }
    }

    /* renamed from: handleDeleteForEveryone$lambda-6 */
    public static final void m3234handleDeleteForEveryone$lambda6(Set set, SingleEmitter singleEmitter, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(set, "$messageRecords");
        Intrinsics.checkNotNullParameter(singleEmitter, "$emitter");
        SignalStore.uiHints().markHasConfirmedDeleteForEveryoneOnce();
        INSTANCE.deleteForEveryone(set, singleEmitter);
    }

    /* renamed from: handleDeleteForEveryone$lambda-7 */
    public static final void m3235handleDeleteForEveryone$lambda7(SingleEmitter singleEmitter, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(singleEmitter, "$emitter");
        singleEmitter.onSuccess(Boolean.FALSE);
    }

    /* renamed from: handleDeleteForEveryone$lambda-8 */
    public static final void m3236handleDeleteForEveryone$lambda8(SingleEmitter singleEmitter, DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(singleEmitter, "$emitter");
        singleEmitter.onSuccess(Boolean.FALSE);
    }

    private final void deleteForEveryone(Set<? extends MessageRecord> set, SingleEmitter<Boolean> singleEmitter) {
        SignalExecutors.BOUNDED.execute(new Runnable(set, singleEmitter) { // from class: org.thoughtcrime.securesms.util.DeleteDialog$$ExternalSyntheticLambda6
            public final /* synthetic */ Set f$0;
            public final /* synthetic */ SingleEmitter f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DeleteDialog.m3233deleteForEveryone$lambda10(this.f$0, this.f$1);
            }
        });
    }

    /* compiled from: DeleteDialog.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\u0005\b\u0002\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B/\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0002\u0010\fJ%\u0010\r\u001a\u00020\u00032\u0016\u0010\u000e\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00020\u000f\"\u0004\u0018\u00010\u0002H\u0014¢\u0006\u0002\u0010\u0010J\u0017\u0010\u0011\u001a\u00020\u000b2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0003H\u0014¢\u0006\u0002\u0010\u0013R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000b0\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/util/DeleteDialog$DeleteProgressDialogAsyncTask;", "Lorg/thoughtcrime/securesms/util/task/ProgressDialogAsyncTask;", "Ljava/lang/Void;", "", "context", "Landroid/content/Context;", "messageRecords", "", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "onDeletionCompleted", "Lkotlin/Function1;", "", "(Landroid/content/Context;Ljava/util/Set;Lkotlin/jvm/functions/Function1;)V", "doInBackground", "params", "", "([Ljava/lang/Void;)Ljava/lang/Boolean;", "onPostExecute", MediaSendActivityResult.EXTRA_RESULT, "(Ljava/lang/Boolean;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class DeleteProgressDialogAsyncTask extends ProgressDialogAsyncTask<Void, Void, Boolean> {
        private final Set<MessageRecord> messageRecords;
        private final Function1<Boolean, Unit> onDeletionCompleted;

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.database.model.MessageRecord> */
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public DeleteProgressDialogAsyncTask(Context context, Set<? extends MessageRecord> set, Function1<? super Boolean, Unit> function1) {
            super(context, (int) R.string.ConversationFragment_deleting, (int) R.string.ConversationFragment_deleting_messages);
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(set, "messageRecords");
            Intrinsics.checkNotNullParameter(function1, "onDeletionCompleted");
            this.messageRecords = set;
            this.onDeletionCompleted = function1;
        }

        public Boolean doInBackground(Void... voidArr) {
            boolean z;
            Intrinsics.checkNotNullParameter(voidArr, "params");
            Set<MessageRecord> set = this.messageRecords;
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
            for (MessageRecord messageRecord : set) {
                if (messageRecord.isMms()) {
                    z = SignalDatabase.Companion.mms().deleteMessage(messageRecord.getId());
                } else {
                    z = SignalDatabase.Companion.sms().deleteMessage(messageRecord.getId());
                }
                arrayList.add(Boolean.valueOf(z));
            }
            boolean z2 = false;
            if (!arrayList.isEmpty()) {
                Iterator it = arrayList.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((Boolean) it.next()).booleanValue()) {
                            z2 = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            return Boolean.valueOf(z2);
        }

        public void onPostExecute(Boolean bool) {
            super.onPostExecute((DeleteProgressDialogAsyncTask) bool);
            this.onDeletionCompleted.invoke(Boolean.valueOf(Intrinsics.areEqual(bool, Boolean.TRUE)));
        }
    }

    /* renamed from: deleteForEveryone$lambda-10 */
    public static final void m3233deleteForEveryone$lambda10(Set set, SingleEmitter singleEmitter) {
        Intrinsics.checkNotNullParameter(set, "$messageRecords");
        Intrinsics.checkNotNullParameter(singleEmitter, "$emitter");
        Iterator it = set.iterator();
        while (it.hasNext()) {
            MessageRecord messageRecord = (MessageRecord) it.next();
            MessageSender.sendRemoteDelete(messageRecord.getId(), messageRecord.isMms());
        }
        singleEmitter.onSuccess(Boolean.FALSE);
    }
}
