package org.thoughtcrime.securesms.util;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;

/* compiled from: InsetItemDecoration.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\"*\u0010\u0005\u001a\u0018\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0000j\u0002`\u00048\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0006*0\b\u0002\u0010\u0007\"\u0014\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00002\u0014\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0000¨\u0006\b"}, d2 = {"Lkotlin/Function2;", "Landroid/view/View;", "Landroidx/recyclerview/widget/RecyclerView;", "", "Lorg/thoughtcrime/securesms/util/Predicate;", "ALWAYS_TRUE", "Lkotlin/jvm/functions/Function2;", "Predicate", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class InsetItemDecorationKt {
    private static final Function2<View, RecyclerView, Boolean> ALWAYS_TRUE = InsetItemDecorationKt$ALWAYS_TRUE$1.INSTANCE;
}
