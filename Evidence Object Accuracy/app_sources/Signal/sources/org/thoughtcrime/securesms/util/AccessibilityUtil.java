package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.provider.Settings;

/* loaded from: classes4.dex */
public final class AccessibilityUtil {
    private AccessibilityUtil() {
    }

    public static boolean areAnimationsDisabled(Context context) {
        return Settings.Global.getFloat(context.getContentResolver(), "animator_duration_scale", 1.0f) == 0.0f;
    }
}
