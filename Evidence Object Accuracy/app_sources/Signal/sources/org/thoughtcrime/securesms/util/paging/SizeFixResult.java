package org.thoughtcrime.securesms.util.paging;

import java.util.List;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class SizeFixResult<T> {
    private static final String TAG = Log.tag(SizeFixResult.class);
    final List<T> items;
    final int total;

    private SizeFixResult(List<T> list, int i) {
        this.items = list;
        this.total = i;
    }

    public List<T> getItems() {
        return this.items;
    }

    public int getTotal() {
        return this.total;
    }

    public static <T> SizeFixResult<T> ensureMultipleOfPageSize(List<T> list, int i, int i2, int i3) {
        if (list.size() + i == i3 || (list.size() != 0 && list.size() % i2 == 0)) {
            return new SizeFixResult<>(list, i3);
        }
        if (list.size() < i2) {
            String str = TAG;
            Log.w(str, "Hit a miscalculation where we don't have the full dataset, but it's smaller than a page size. records: " + list.size() + ", startPosition: " + i + ", pageSize: " + i2 + ", total: " + i3);
            return new SizeFixResult<>(list, list.size() + i);
        }
        String str2 = TAG;
        Log.w(str2, "Hit a miscalculation where our data size isn't a multiple of the page size. records: " + list.size() + ", startPosition: " + i + ", pageSize: " + i2 + ", total: " + i3);
        return new SizeFixResult<>(list.subList(0, list.size() - (list.size() % i2)), i3);
    }
}
