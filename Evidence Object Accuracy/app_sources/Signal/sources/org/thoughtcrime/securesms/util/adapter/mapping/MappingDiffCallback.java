package org.thoughtcrime.securesms.util.adapter.mapping;

import androidx.recyclerview.widget.DiffUtil;

/* loaded from: classes4.dex */
public class MappingDiffCallback extends DiffUtil.ItemCallback<MappingModel<?>> {
    public boolean areItemsTheSame(MappingModel mappingModel, MappingModel mappingModel2) {
        if (mappingModel.getClass() == mappingModel2.getClass()) {
            return mappingModel.areItemsTheSame(mappingModel2);
        }
        return false;
    }

    public boolean areContentsTheSame(MappingModel mappingModel, MappingModel mappingModel2) {
        if (mappingModel.getClass() == mappingModel2.getClass()) {
            return mappingModel.areContentsTheSame(mappingModel2);
        }
        return false;
    }

    public Object getChangePayload(MappingModel mappingModel, MappingModel mappingModel2) {
        if (mappingModel.getClass() == mappingModel2.getClass()) {
            return mappingModel.getChangePayload(mappingModel2);
        }
        return null;
    }
}
