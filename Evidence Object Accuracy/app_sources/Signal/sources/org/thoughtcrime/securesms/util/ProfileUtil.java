package org.thoughtcrime.securesms.util;

import android.content.Context;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.util.Pair;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobs.GroupV2UpdateSelfProfileKeyJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceProfileKeyUpdateJob;
import org.thoughtcrime.securesms.jobs.ProfileUploadJob;
import org.thoughtcrime.securesms.jobs.RefreshAttributesJob;
import org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob;
import org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda9;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddress;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddressProfileUtil;
import org.thoughtcrime.securesms.payments.PaymentsAddressException;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;
import org.whispersystems.signalservice.api.crypto.ProfileCipher;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccessPair;
import org.whispersystems.signalservice.api.profiles.AvatarUploadParams;
import org.whispersystems.signalservice.api.profiles.ProfileAndCredential;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.services.ProfileService;
import org.whispersystems.signalservice.api.util.StreamDetails;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class ProfileUtil {
    private static final String TAG = Log.tag(ProfileUtil.class);

    private ProfileUtil() {
    }

    public static void handleSelfProfileKeyChange() {
        List<? extends Job> list = (List) Collection$EL.stream(SignalDatabase.groups().getAllGroupV2Ids()).map(new Function() { // from class: org.thoughtcrime.securesms.util.ProfileUtil$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return GroupV2UpdateSelfProfileKeyJob.withoutLimits((GroupId.V2) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
        String str = TAG;
        Log.w(str, "[handleSelfProfileKeyChange] Scheduling jobs, including " + list.size() + " group update jobs.");
        ApplicationDependencies.getJobManager().startChain(new RefreshAttributesJob()).then(new ProfileUploadJob()).then(new MultiDeviceProfileKeyUpdateJob()).then(list).enqueue();
    }

    public static ProfileAndCredential retrieveProfileSync(Context context, Recipient recipient, SignalServiceProfile.RequestType requestType) throws IOException {
        return retrieveProfileSync(context, recipient, requestType, true);
    }

    public static ProfileAndCredential retrieveProfileSync(Context context, Recipient recipient, SignalServiceProfile.RequestType requestType, boolean z) throws IOException {
        return new ProfileService.ProfileResponseProcessor(retrieveProfile(context, recipient, requestType, z).blockingGet().second()).getResultOrThrow();
    }

    public static Single<Pair<Recipient, ServiceResponse<ProfileAndCredential>>> retrieveProfile(Context context, Recipient recipient, SignalServiceProfile.RequestType requestType) {
        return retrieveProfile(context, recipient, requestType, true);
    }

    private static Single<Pair<Recipient, ServiceResponse<ProfileAndCredential>>> retrieveProfile(Context context, Recipient recipient, SignalServiceProfile.RequestType requestType, boolean z) {
        return Single.fromCallable(new Callable(context, recipient) { // from class: org.thoughtcrime.securesms.util.ProfileUtil$$ExternalSyntheticLambda3
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Recipient f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ProfileUtil.toSignalServiceAddress(this.f$0, this.f$1);
            }
        }).flatMap(new io.reactivex.rxjava3.functions.Function(ProfileKeyUtil.profileKeyOptional(recipient.getProfileKey()), z ? getUnidentifiedAccess(context, recipient) : Optional.empty(), requestType, recipient) { // from class: org.thoughtcrime.securesms.util.ProfileUtil$$ExternalSyntheticLambda4
            public final /* synthetic */ Optional f$1;
            public final /* synthetic */ Optional f$2;
            public final /* synthetic */ SignalServiceProfile.RequestType f$3;
            public final /* synthetic */ Recipient f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ProfileUtil.lambda$retrieveProfile$2(ProfileService.this, this.f$1, this.f$2, this.f$3, this.f$4, (SignalServiceAddress) obj);
            }
        }).onErrorReturn(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.util.ProfileUtil$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ProfileUtil.lambda$retrieveProfile$3(Recipient.this, (Throwable) obj);
            }
        });
    }

    public static /* synthetic */ Pair lambda$retrieveProfile$1(Recipient recipient, ServiceResponse serviceResponse) throws Throwable {
        return new Pair(recipient, serviceResponse);
    }

    public static /* synthetic */ SingleSource lambda$retrieveProfile$2(ProfileService profileService, Optional optional, Optional optional2, SignalServiceProfile.RequestType requestType, Recipient recipient, SignalServiceAddress signalServiceAddress) throws Throwable {
        return profileService.getProfile(signalServiceAddress, optional, optional2, requestType, Locale.getDefault()).map(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.util.ProfileUtil$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ProfileUtil.lambda$retrieveProfile$1(Recipient.this, (ServiceResponse) obj);
            }
        });
    }

    public static /* synthetic */ Pair lambda$retrieveProfile$3(Recipient recipient, Throwable th) throws Throwable {
        return new Pair(recipient, ServiceResponse.forUnknownError(th));
    }

    public static String decryptString(ProfileKey profileKey, byte[] bArr) throws InvalidCiphertextException, IOException {
        if (bArr == null) {
            return null;
        }
        return new ProfileCipher(profileKey).decryptString(bArr);
    }

    public static String decryptString(ProfileKey profileKey, String str) throws InvalidCiphertextException, IOException {
        if (str == null) {
            return null;
        }
        return decryptString(profileKey, Base64.decode(str));
    }

    public static MobileCoinPublicAddress getAddressForRecipient(Recipient recipient) throws IOException, PaymentsAddressException {
        Throwable e;
        try {
            ProfileKey profileKey = getProfileKey(recipient);
            ProfileAndCredential retrieveProfileSync = retrieveProfileSync(ApplicationDependencies.getApplication(), recipient, SignalServiceProfile.RequestType.PROFILE);
            byte[] paymentAddress = retrieveProfileSync.getProfile().getPaymentAddress();
            if (paymentAddress != null) {
                try {
                    MobileCoinPublicAddress fromBytes = MobileCoinPublicAddress.fromBytes(MobileCoinPublicAddressProfileUtil.verifyPaymentsAddress(SignalServiceProtos.PaymentAddress.parseFrom(new ProfileCipher(profileKey).decryptWithLength(paymentAddress)), new IdentityKey(Base64.decode(retrieveProfileSync.getProfile().getIdentityKey()), 0)));
                    if (fromBytes != null) {
                        return fromBytes;
                    }
                    throw new PaymentsAddressException(PaymentsAddressException.Code.INVALID_ADDRESS);
                } catch (IOException e2) {
                    e = e2;
                    String str = TAG;
                    Log.w(str, "Could not decrypt payments address, ProfileKey may be outdated for " + recipient.getId(), e);
                    throw new PaymentsAddressException(PaymentsAddressException.Code.COULD_NOT_DECRYPT);
                } catch (InvalidKeyException e3) {
                    String str2 = TAG;
                    Log.w(str2, "Could not verify payments address due to bad identity key " + recipient.getId(), e3);
                    throw new PaymentsAddressException(PaymentsAddressException.Code.INVALID_ADDRESS_SIGNATURE);
                } catch (InvalidCiphertextException e4) {
                    e = e4;
                    String str = TAG;
                    Log.w(str, "Could not decrypt payments address, ProfileKey may be outdated for " + recipient.getId(), e);
                    throw new PaymentsAddressException(PaymentsAddressException.Code.COULD_NOT_DECRYPT);
                }
            } else {
                String str3 = TAG;
                Log.w(str3, "Payments not enabled for " + recipient.getId());
                throw new PaymentsAddressException(PaymentsAddressException.Code.NOT_ENABLED);
            }
        } catch (IOException unused) {
            String str4 = TAG;
            Log.w(str4, "Profile key not available for " + recipient.getId());
            throw new PaymentsAddressException(PaymentsAddressException.Code.NO_PROFILE_KEY);
        }
    }

    private static ProfileKey getProfileKey(Recipient recipient) throws IOException {
        byte[] profileKey = recipient.getProfileKey();
        if (profileKey != null) {
            try {
                return new ProfileKey(profileKey);
            } catch (InvalidInputException unused) {
                String str = TAG;
                Log.w(str, "Profile key invalid for " + recipient.getId());
                throw new IOException("Invalid profile key");
            }
        } else {
            String str2 = TAG;
            Log.w(str2, "Profile key unknown for " + recipient.getId());
            throw new IOException("No profile key");
        }
    }

    public static void uploadProfileWithBadges(Context context, List<Badge> list) throws IOException {
        Log.d(TAG, "uploadProfileWithBadges()");
        uploadProfile(Recipient.self().getProfileName(), (String) Optional.ofNullable(Recipient.self().getAbout()).orElse(""), (String) Optional.ofNullable(Recipient.self().getAboutEmoji()).orElse(""), getSelfPaymentsAddressProtobuf(), AvatarUploadParams.unchanged(AvatarHelper.hasAvatar(context, Recipient.self().getId())), list);
    }

    public static void uploadProfileWithName(Context context, ProfileName profileName) throws IOException {
        Log.d(TAG, "uploadProfileWithName()");
        StreamDetails selfProfileAvatarStream = AvatarHelper.getSelfProfileAvatarStream(context);
        try {
            uploadProfile(profileName, (String) Optional.ofNullable(Recipient.self().getAbout()).orElse(""), (String) Optional.ofNullable(Recipient.self().getAboutEmoji()).orElse(""), getSelfPaymentsAddressProtobuf(), AvatarUploadParams.unchanged(AvatarHelper.hasAvatar(context, Recipient.self().getId())), Recipient.self().getBadges());
            if (selfProfileAvatarStream != null) {
                selfProfileAvatarStream.close();
            }
        } catch (Throwable th) {
            if (selfProfileAvatarStream != null) {
                try {
                    selfProfileAvatarStream.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void uploadProfileWithAbout(Context context, String str, String str2) throws IOException {
        Log.d(TAG, "uploadProfileWithAbout()");
        StreamDetails selfProfileAvatarStream = AvatarHelper.getSelfProfileAvatarStream(context);
        try {
            uploadProfile(Recipient.self().getProfileName(), str, str2, getSelfPaymentsAddressProtobuf(), AvatarUploadParams.unchanged(AvatarHelper.hasAvatar(context, Recipient.self().getId())), Recipient.self().getBadges());
            if (selfProfileAvatarStream != null) {
                selfProfileAvatarStream.close();
            }
        } catch (Throwable th) {
            if (selfProfileAvatarStream != null) {
                try {
                    selfProfileAvatarStream.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void uploadProfile(Context context) throws IOException {
        Log.d(TAG, "uploadProfile()");
        StreamDetails selfProfileAvatarStream = AvatarHelper.getSelfProfileAvatarStream(context);
        try {
            uploadProfileWithAvatar(selfProfileAvatarStream);
            if (selfProfileAvatarStream != null) {
                selfProfileAvatarStream.close();
            }
        } catch (Throwable th) {
            if (selfProfileAvatarStream != null) {
                try {
                    selfProfileAvatarStream.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void uploadProfileWithAvatar(StreamDetails streamDetails) throws IOException {
        Log.d(TAG, "uploadProfileWithAvatar()");
        uploadProfile(Recipient.self().getProfileName(), (String) Optional.ofNullable(Recipient.self().getAbout()).orElse(""), (String) Optional.ofNullable(Recipient.self().getAboutEmoji()).orElse(""), getSelfPaymentsAddressProtobuf(), AvatarUploadParams.forAvatar(streamDetails), Recipient.self().getBadges());
    }

    public static Optional<ExpiringProfileKeyCredential> updateExpiringProfileKeyCredential(Recipient recipient) throws IOException {
        ProfileKey profileKeyOrNull = ProfileKeyUtil.profileKeyOrNull(recipient.getProfileKey());
        if (profileKeyOrNull != null) {
            String str = TAG;
            Log.i(str, String.format("Updating profile key credential on recipient %s, fetching", recipient.getId()));
            Optional<ExpiringProfileKeyCredential> resolveProfileKeyCredential = ApplicationDependencies.getSignalServiceAccountManager().resolveProfileKeyCredential(recipient.requireServiceId(), profileKeyOrNull, Locale.getDefault());
            if (resolveProfileKeyCredential.isPresent()) {
                if (!SignalDatabase.recipients().setProfileKeyCredential(recipient.getId(), profileKeyOrNull, resolveProfileKeyCredential.get())) {
                    Log.w(str, String.format("Failed to update the profile key credential on recipient %s", recipient.getId()));
                } else {
                    Log.i(str, String.format("Got new profile key credential for recipient %s", recipient.getId()));
                    return resolveProfileKeyCredential;
                }
            }
        }
        return Optional.empty();
    }

    private static void uploadProfile(ProfileName profileName, String str, String str2, SignalServiceProtos.PaymentAddress paymentAddress, AvatarUploadParams avatarUploadParams, List<Badge> list) throws IOException {
        List<String> list2 = (List) Collection$EL.stream(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.util.ProfileUtil$$ExternalSyntheticLambda1
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return ((Badge) obj).getVisible();
            }
        }).map(new RefreshOwnProfileJob$$ExternalSyntheticLambda9()).collect(Collectors.toList());
        String str3 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Uploading ");
        String str4 = "non-";
        String str5 = "";
        sb.append(!profileName.isEmpty() ? str4 : str5);
        sb.append("empty profile name.");
        Log.d(str3, sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Uploading ");
        sb2.append(!Util.isEmpty(str) ? str4 : str5);
        sb2.append("empty about.");
        Log.d(str3, sb2.toString());
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Uploading ");
        sb3.append(!Util.isEmpty(str2) ? str4 : str5);
        sb3.append("empty emoji.");
        Log.d(str3, sb3.toString());
        StringBuilder sb4 = new StringBuilder();
        sb4.append("Uploading ");
        sb4.append(paymentAddress != null ? str4 : str5);
        sb4.append("empty payments address.");
        Log.d(str3, sb4.toString());
        StringBuilder sb5 = new StringBuilder();
        sb5.append("Uploading ");
        sb5.append(!list2.isEmpty() ? str4 : str5);
        sb5.append("empty badge list.");
        Log.d(str3, sb5.toString());
        if (avatarUploadParams.keepTheSame) {
            StringBuilder sb6 = new StringBuilder();
            sb6.append("Leaving avatar unchanged. We think we ");
            if (!avatarUploadParams.hasAvatar) {
                str5 = "do not ";
            }
            sb6.append(str5);
            sb6.append("have one.");
            Log.d(str3, sb6.toString());
        } else {
            StringBuilder sb7 = new StringBuilder();
            sb7.append("Uploading ");
            StreamDetails streamDetails = avatarUploadParams.stream;
            if (streamDetails == null || streamDetails.getLength() == 0) {
                str4 = str5;
            }
            sb7.append(str4);
            sb7.append("empty avatar.");
            Log.d(str3, sb7.toString());
        }
        String orElse = ApplicationDependencies.getSignalServiceAccountManager().setVersionedProfile(SignalStore.account().requireAci(), ProfileKeyUtil.getSelfProfileKey(), profileName.serialize(), str, str2, Optional.ofNullable(paymentAddress), avatarUploadParams, list2).orElse(null);
        SignalStore.registrationValues().markHasUploadedProfile();
        if (!avatarUploadParams.keepTheSame) {
            SignalDatabase.recipients().setProfileAvatar(Recipient.self().getId(), orElse);
        }
        ApplicationDependencies.getJobManager().add(new RefreshOwnProfileJob());
    }

    private static SignalServiceProtos.PaymentAddress getSelfPaymentsAddressProtobuf() {
        if (!SignalStore.paymentsValues().mobileCoinPaymentsEnabled()) {
            return null;
        }
        return MobileCoinPublicAddressProfileUtil.signPaymentsAddress(ApplicationDependencies.getPayments().getWallet().getMobileCoinPublicAddress().serialize(), SignalStore.account().getAciIdentityKey());
    }

    private static Optional<UnidentifiedAccess> getUnidentifiedAccess(Context context, Recipient recipient) {
        Optional<UnidentifiedAccessPair> accessFor = UnidentifiedAccessUtil.getAccessFor(context, recipient, false);
        if (accessFor.isPresent()) {
            return accessFor.get().getTargetUnidentifiedAccess();
        }
        return Optional.empty();
    }

    public static SignalServiceAddress toSignalServiceAddress(Context context, Recipient recipient) throws IOException {
        if (recipient.getRegistered() != RecipientDatabase.RegisteredState.NOT_REGISTERED) {
            return RecipientUtil.toSignalServiceAddress(context, recipient);
        }
        if (recipient.hasServiceId()) {
            return new SignalServiceAddress(recipient.requireServiceId(), recipient.getE164().orElse(null));
        }
        throw new IOException(recipient.getId() + " not registered!");
    }
}
