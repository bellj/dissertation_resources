package org.thoughtcrime.securesms.util;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: TopToastPopup.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u001f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0002\u001a\u00020\u0003H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/util/TopToastPopup;", "Landroid/widget/PopupWindow;", "parent", "Landroid/view/ViewGroup;", "iconResource", "", "descriptionText", "", "(Landroid/view/ViewGroup;ILjava/lang/String;)V", "description", "Landroid/widget/TextView;", "icon", "Landroid/widget/ImageView;", "measureChild", "", "show", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TopToastPopup extends PopupWindow {
    public static final Companion Companion = new Companion(null);
    private static final long DURATION = TimeUnit.SECONDS.toMillis(2);
    private final TextView description;
    private final ImageView icon;

    public /* synthetic */ TopToastPopup(ViewGroup viewGroup, int i, String str, DefaultConstructorMarker defaultConstructorMarker) {
        this(viewGroup, i, str);
    }

    @JvmStatic
    public static final TopToastPopup show(ViewGroup viewGroup, int i, String str) {
        return Companion.show(viewGroup, i, str);
    }

    private TopToastPopup(ViewGroup viewGroup, int i, String str) {
        super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.top_toast_popup, viewGroup, false), -1, ViewUtil.dpToPx(86));
        View findViewById = getContentView().findViewById(R.id.top_toast_popup_icon);
        Intrinsics.checkNotNullExpressionValue(findViewById, "contentView.findViewById….id.top_toast_popup_icon)");
        ImageView imageView = (ImageView) findViewById;
        this.icon = imageView;
        View findViewById2 = getContentView().findViewById(R.id.top_toast_popup_description);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "contentView.findViewById…_toast_popup_description)");
        TextView textView = (TextView) findViewById2;
        this.description = textView;
        if (Build.VERSION.SDK_INT >= 21) {
            setElevation((float) ViewUtil.dpToPx(8));
        }
        setAnimationStyle(R.style.PopupAnimation);
        imageView.setImageResource(i);
        textView.setText(str);
    }

    public final void show(ViewGroup viewGroup) {
        showAtLocation(viewGroup, 8388659, 0, 0);
        measureChild();
        update();
        getContentView().postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.util.TopToastPopup$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                TopToastPopup.m3271show$lambda0(TopToastPopup.this);
            }
        }, DURATION);
    }

    /* renamed from: show$lambda-0 */
    public static final void m3271show$lambda0(TopToastPopup topToastPopup) {
        Intrinsics.checkNotNullParameter(topToastPopup, "this$0");
        topToastPopup.dismiss();
    }

    private final void measureChild() {
        getContentView().measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    /* compiled from: TopToastPopup.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/util/TopToastPopup$Companion;", "", "()V", "DURATION", "", "show", "Lorg/thoughtcrime/securesms/util/TopToastPopup;", "parent", "Landroid/view/ViewGroup;", "icon", "", "description", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final TopToastPopup show(ViewGroup viewGroup, int i, String str) {
            Intrinsics.checkNotNullParameter(viewGroup, "parent");
            Intrinsics.checkNotNullParameter(str, "description");
            TopToastPopup topToastPopup = new TopToastPopup(viewGroup, i, str, null);
            topToastPopup.show(viewGroup);
            return topToastPopup;
        }
    }
}
