package org.thoughtcrime.securesms.util;

import android.content.Context;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.Quote;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.TextSlide;
import org.thoughtcrime.securesms.stickers.StickerUrl;

/* compiled from: MessageRecordUtil.kt */
@Metadata(d1 = {"\u00004\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\f\u0010\u0002\u001a\u0004\u0018\u00010\u0003*\u00020\u0004\u001a\n\u0010\u0005\u001a\u00020\u0006*\u00020\u0004\u001a\n\u0010\u0007\u001a\u00020\b*\u00020\u0004\u001a\u0012\u0010\t\u001a\u00020\b*\u00020\u00042\u0006\u0010\n\u001a\u00020\u000b\u001a\n\u0010\f\u001a\u00020\b*\u00020\u0004\u001a\n\u0010\r\u001a\u00020\b*\u00020\u0004\u001a\n\u0010\u000e\u001a\u00020\b*\u00020\u0004\u001a\n\u0010\u000f\u001a\u00020\b*\u00020\u0004\u001a\n\u0010\u0010\u001a\u00020\b*\u00020\u0004\u001a\u0012\u0010\u0011\u001a\u00020\b*\u00020\u00042\u0006\u0010\n\u001a\u00020\u000b\u001a\u0012\u0010\u0012\u001a\u00020\b*\u00020\u00042\u0006\u0010\n\u001a\u00020\u000b\u001a\n\u0010\u0013\u001a\u00020\b*\u00020\u0004\u001a\n\u0010\u0014\u001a\u00020\b*\u00020\u0004\u001a\n\u0010\u0015\u001a\u00020\b*\u00020\u0004\u001a\n\u0010\u0016\u001a\u00020\b*\u00020\u0004\u001a\n\u0010\u0017\u001a\u00020\b*\u00020\u0004\u001a\u0012\u0010\u0018\u001a\u00020\b*\u00020\u00042\u0006\u0010\n\u001a\u00020\u000b\u001a\u0012\u0010\u0019\u001a\u00020\b*\u00020\u00042\u0006\u0010\n\u001a\u00020\u000b\u001a\n\u0010\u001a\u001a\u00020\b*\u00020\u0004\u001a\n\u0010\u001b\u001a\u00020\b*\u00020\u0004\u001a\u0012\u0010\u001c\u001a\u00020\b*\u00020\u00042\u0006\u0010\n\u001a\u00020\u000b\u001a\n\u0010\u001d\u001a\u00020\b*\u00020\u0004\u001a\n\u0010\u001e\u001a\u00020\u001f*\u00020\u0004\u001a\n\u0010 \u001a\u00020!*\u00020\u0004\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\""}, d2 = {"MAX_BODY_DISPLAY_LENGTH", "", "getQuote", "Lorg/thoughtcrime/securesms/database/model/Quote;", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "getRecordQuoteType", "Lorg/thoughtcrime/securesms/mms/QuoteModel$Type;", "hasAudio", "", "hasBigImageLinkPreview", "context", "Landroid/content/Context;", "hasDocument", "hasExtraText", "hasGiftBadge", "hasLinkPreview", "hasLocation", "hasNoBubble", "hasOnlyThumbnail", "hasQuote", "hasSharedContact", "hasSticker", "hasTextSlide", "hasThumbnail", "isBorderless", "isCaptionlessMms", "isMediaMessage", "isStoryReaction", "isTextOnly", "isViewOnceMessage", "requireGiftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "requireTextSlide", "Lorg/thoughtcrime/securesms/mms/TextSlide;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageRecordUtil {
    public static final int MAX_BODY_DISPLAY_LENGTH;

    public static final boolean isMediaMessage(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        if (messageRecord.isMms() && !messageRecord.isMmsNotification()) {
            MediaMmsMessageRecord mediaMmsMessageRecord = (MediaMmsMessageRecord) messageRecord;
            if (mediaMmsMessageRecord.containsMediaSlide() && mediaMmsMessageRecord.getSlideDeck().getStickerSlide() == null) {
                return true;
            }
        }
        return false;
    }

    public static final boolean hasSticker(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        return messageRecord.isMms() && ((MmsMessageRecord) messageRecord).getSlideDeck().getStickerSlide() != null;
    }

    public static final boolean hasSharedContact(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        if (messageRecord.isMms()) {
            List<Contact> sharedContacts = ((MmsMessageRecord) messageRecord).getSharedContacts();
            Intrinsics.checkNotNullExpressionValue(sharedContacts, "this as MmsMessageRecord).sharedContacts");
            if (!sharedContacts.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public static final boolean hasLocation(MessageRecord messageRecord) {
        boolean z;
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        if (messageRecord.isMms()) {
            List<Slide> slides = ((MmsMessageRecord) messageRecord).getSlideDeck().getSlides();
            Intrinsics.checkNotNullExpressionValue(slides, "this as MmsMessageRecord).slideDeck.slides");
            if (!(slides instanceof Collection) || !slides.isEmpty()) {
                for (Slide slide : slides) {
                    if (slide.hasLocation()) {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
            if (z) {
                return true;
            }
        }
        return false;
    }

    public static final boolean hasAudio(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        return messageRecord.isMms() && ((MmsMessageRecord) messageRecord).getSlideDeck().getAudioSlide() != null;
    }

    public static final boolean isCaptionlessMms(MessageRecord messageRecord, Context context) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        return messageRecord.isMms() && messageRecord.isDisplayBodyEmpty(context) && ((MmsMessageRecord) messageRecord).getSlideDeck().getTextSlide() == null;
    }

    public static final boolean hasThumbnail(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        return messageRecord.isMms() && ((MmsMessageRecord) messageRecord).getSlideDeck().getThumbnailSlide() != null;
    }

    public static final boolean isStoryReaction(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        return messageRecord.isMms() && MmsSmsColumns.Types.isStoryReaction(((MmsMessageRecord) messageRecord).getType());
    }

    public static final boolean isBorderless(MessageRecord messageRecord, Context context) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        if (isCaptionlessMms(messageRecord, context) && hasThumbnail(messageRecord)) {
            Slide thumbnailSlide = ((MmsMessageRecord) messageRecord).getSlideDeck().getThumbnailSlide();
            if (thumbnailSlide != null && thumbnailSlide.isBorderless()) {
                return true;
            }
        }
        return false;
    }

    public static final boolean hasNoBubble(MessageRecord messageRecord, Context context) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        return hasSticker(messageRecord) || isBorderless(messageRecord, context) || (isTextOnly(messageRecord, context) && messageRecord.isJumbomoji(context));
    }

    public static final boolean hasOnlyThumbnail(MessageRecord messageRecord, Context context) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        return hasThumbnail(messageRecord) && !hasAudio(messageRecord) && !hasDocument(messageRecord) && !hasSharedContact(messageRecord) && !hasSticker(messageRecord) && !isBorderless(messageRecord, context) && !isViewOnceMessage(messageRecord);
    }

    public static final boolean hasDocument(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        return messageRecord.isMms() && ((MmsMessageRecord) messageRecord).getSlideDeck().getDocumentSlide() != null;
    }

    public static final boolean isViewOnceMessage(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        return messageRecord.isMms() && ((MmsMessageRecord) messageRecord).isViewOnce();
    }

    public static final boolean hasExtraText(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        boolean z = messageRecord.isMms() && ((MmsMessageRecord) messageRecord).getSlideDeck().getTextSlide() != null;
        boolean z2 = messageRecord.getBody().length() > 1000;
        if (z || z2) {
            return true;
        }
        return false;
    }

    public static final boolean hasQuote(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        return messageRecord.isMms() && ((MmsMessageRecord) messageRecord).getQuote() != null;
    }

    public static final Quote getQuote(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        if (messageRecord.isMms()) {
            return ((MmsMessageRecord) messageRecord).getQuote();
        }
        return null;
    }

    public static final boolean hasLinkPreview(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        if (messageRecord.isMms()) {
            List<LinkPreview> linkPreviews = ((MmsMessageRecord) messageRecord).getLinkPreviews();
            Intrinsics.checkNotNullExpressionValue(linkPreviews, "this as MmsMessageRecord).linkPreviews");
            if (!linkPreviews.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public static final boolean hasTextSlide(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        if (messageRecord.isMms()) {
            MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) messageRecord;
            if (mmsMessageRecord.getSlideDeck().getTextSlide() != null) {
                TextSlide textSlide = mmsMessageRecord.getSlideDeck().getTextSlide();
                if ((textSlide != null ? textSlide.getUri() : null) != null) {
                    return true;
                }
            }
        }
        return false;
    }

    public static final TextSlide requireTextSlide(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        TextSlide textSlide = ((MmsMessageRecord) messageRecord).getSlideDeck().getTextSlide();
        if (textSlide != null) {
            return textSlide;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    public static final boolean hasBigImageLinkPreview(MessageRecord messageRecord, Context context) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        if (!hasLinkPreview(messageRecord)) {
            return false;
        }
        LinkPreview linkPreview = ((MmsMessageRecord) messageRecord).getLinkPreviews().get(0);
        if (linkPreview.getThumbnail().isPresent() && !Util.isEmpty(linkPreview.getDescription())) {
            return true;
        }
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.media_bubble_min_width_solo);
        if (!linkPreview.getThumbnail().isPresent() || linkPreview.getThumbnail().get().getWidth() < dimensionPixelSize || StickerUrl.isValidShareLink(linkPreview.getUrl())) {
            return false;
        }
        return true;
    }

    public static final boolean hasGiftBadge(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        GiftBadge giftBadge = null;
        MmsMessageRecord mmsMessageRecord = messageRecord instanceof MmsMessageRecord ? (MmsMessageRecord) messageRecord : null;
        if (mmsMessageRecord != null) {
            giftBadge = mmsMessageRecord.getGiftBadge();
        }
        return giftBadge != null;
    }

    public static final GiftBadge requireGiftBadge(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        GiftBadge giftBadge = ((MmsMessageRecord) messageRecord).getGiftBadge();
        Intrinsics.checkNotNull(giftBadge);
        return giftBadge;
    }

    public static final boolean isTextOnly(MessageRecord messageRecord, Context context) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        return !messageRecord.isMms() || (!isViewOnceMessage(messageRecord) && !hasLinkPreview(messageRecord) && !hasQuote(messageRecord) && !hasExtraText(messageRecord) && !hasDocument(messageRecord) && !hasThumbnail(messageRecord) && !hasAudio(messageRecord) && !hasLocation(messageRecord) && !hasSharedContact(messageRecord) && !hasSticker(messageRecord) && !isCaptionlessMms(messageRecord, context) && !hasGiftBadge(messageRecord));
    }

    public static final QuoteModel.Type getRecordQuoteType(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "<this>");
        return hasGiftBadge(messageRecord) ? QuoteModel.Type.GIFT_BADGE : QuoteModel.Type.NORMAL;
    }
}
