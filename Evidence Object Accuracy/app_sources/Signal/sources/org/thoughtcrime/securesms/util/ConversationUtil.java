package org.thoughtcrime.securesms.util;

import android.content.ComponentName;
import android.content.Context;
import androidx.core.app.Person;
import androidx.core.content.LocusIdCompat;
import androidx.core.content.pm.ShortcutInfoCompat;
import androidx.core.content.pm.ShortcutManagerCompat;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.IntFunction;
import com.annimon.stream.function.Predicate;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobs.ConversationShortcutUpdateJob;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class ConversationUtil {
    private static final String CATEGORY_SHARE_TARGET;
    public static final int CONVERSATION_SUPPORT_VERSION;
    private static final String TAG = Log.tag(ConversationUtil.class);

    private ConversationUtil() {
    }

    public static String getChannelId(Context context, Recipient recipient) {
        Recipient resolve = recipient.resolve();
        return resolve.getNotificationChannel() != null ? resolve.getNotificationChannel() : NotificationChannels.getMessagesChannel(context);
    }

    public static void refreshRecipientShortcuts() {
        ConversationShortcutUpdateJob.enqueue();
    }

    public static void pushShortcutForRecipientIfNeededSync(Context context, Recipient recipient) {
        String shortcutId = getShortcutId(recipient);
        List<ShortcutInfoCompat> dynamicShortcuts = ShortcutManagerCompat.getDynamicShortcuts(context);
        if (!Stream.of(dynamicShortcuts).filter(new Predicate(shortcutId) { // from class: org.thoughtcrime.securesms.util.ConversationUtil$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationUtil.lambda$pushShortcutForRecipientIfNeededSync$0(this.f$0, (ShortcutInfoCompat) obj);
            }
        }).findFirst().isPresent()) {
            pushShortcutForRecipientInternal(context, recipient, dynamicShortcuts.size());
        }
    }

    public static /* synthetic */ boolean lambda$pushShortcutForRecipientIfNeededSync$0(String str, ShortcutInfoCompat shortcutInfoCompat) {
        return Objects.equals(str, shortcutInfoCompat.getId());
    }

    public static void clearAllShortcuts(Context context) {
        ShortcutManagerCompat.removeLongLivedShortcuts(context, Stream.of(ShortcutManagerCompat.getDynamicShortcuts(context)).map(new Function() { // from class: org.thoughtcrime.securesms.util.ConversationUtil$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((ShortcutInfoCompat) obj).getId();
            }
        }).toList());
    }

    public static void clearShortcuts(Context context, Collection<RecipientId> collection) {
        SignalExecutors.BOUNDED.execute(new Runnable(context, collection) { // from class: org.thoughtcrime.securesms.util.ConversationUtil$$ExternalSyntheticLambda2
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Collection f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationUtil.lambda$clearShortcuts$1(this.f$0, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$clearShortcuts$1(Context context, Collection collection) {
        ShortcutManagerCompat.removeLongLivedShortcuts(context, Stream.of(collection).withoutNulls().map(new Function() { // from class: org.thoughtcrime.securesms.util.ConversationUtil$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationUtil.getShortcutId((RecipientId) obj);
            }
        }).toList());
    }

    public static String getShortcutId(RecipientId recipientId) {
        return recipientId.serialize();
    }

    public static String getShortcutId(Recipient recipient) {
        return getShortcutId(recipient.getId());
    }

    public static RecipientId getRecipientId(String str) {
        if (str == null) {
            return null;
        }
        try {
            return RecipientId.from(str);
        } catch (Throwable th) {
            Log.d(TAG, "Unable to parse recipientId from shortcutId", th);
            return null;
        }
    }

    public static int getMaxShortcuts(Context context) {
        return Math.min(ShortcutManagerCompat.getMaxShortcutCountPerActivity(context), 150);
    }

    public static void removeLongLivedShortcuts(Context context, Collection<RecipientId> collection) {
        ShortcutManagerCompat.removeLongLivedShortcuts(context, (List) Collection$EL.stream(collection).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.util.ConversationUtil$$ExternalSyntheticLambda5
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationUtil.getShortcutId((RecipientId) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList()));
    }

    public static boolean setActiveShortcuts(Context context, List<Recipient> list) {
        if (ShortcutManagerCompat.isRateLimitingActive(context)) {
            return false;
        }
        int maxShortcutCountPerActivity = ShortcutManagerCompat.getMaxShortcutCountPerActivity(context);
        if (list.size() > maxShortcutCountPerActivity) {
            String str = TAG;
            Log.w(str, "Too many recipients provided! Provided: " + list.size() + ", Max: " + maxShortcutCountPerActivity);
            list = list.subList(0, maxShortcutCountPerActivity);
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (int i = 0; i < list.size(); i++) {
            arrayList.add(buildShortcutInfo(context, list.get(i), i));
        }
        return ShortcutManagerCompat.setDynamicShortcuts(context, arrayList);
    }

    private static void pushShortcutForRecipientInternal(Context context, Recipient recipient, int i) {
        ShortcutManagerCompat.pushDynamicShortcut(context, buildShortcutInfo(context, recipient, i));
    }

    private static ShortcutInfoCompat buildShortcutInfo(Context context, Recipient recipient, int i) {
        Recipient resolve = recipient.resolve();
        Person[] buildPersons = buildPersons(context, resolve);
        Long threadIdFor = SignalDatabase.threads().getThreadIdFor(resolve.getId());
        String string = resolve.isSelf() ? context.getString(R.string.note_to_self) : resolve.getShortDisplayName(context);
        String string2 = resolve.isSelf() ? context.getString(R.string.note_to_self) : resolve.getDisplayName(context);
        String shortcutId = getShortcutId(resolve);
        return new ShortcutInfoCompat.Builder(context, shortcutId).setLongLived(true).setIntent(ConversationIntents.createBuilder(context, resolve.getId(), threadIdFor != null ? threadIdFor.longValue() : -1).build()).setShortLabel(string).setLongLabel(string2).setIcon(AvatarUtil.getIconCompatForShortcut(context, resolve)).setPersons(buildPersons).setCategories(Collections.singleton(CATEGORY_SHARE_TARGET)).setActivity(new ComponentName(context, "org.thoughtcrime.securesms.RoutingActivity")).setRank(i).setLocusId(new LocusIdCompat(shortcutId)).build();
    }

    private static Person[] buildPersons(Context context, Recipient recipient) {
        return recipient.isGroup() ? buildPersonsForGroup(context, recipient.getGroupId().get()) : new Person[]{buildPerson(context, recipient)};
    }

    private static Person[] buildPersonsForGroup(Context context, GroupId groupId) {
        return (Person[]) Stream.of(SignalDatabase.groups().getGroupMembers(groupId, GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF)).map(new com.annimon.stream.function.Function(context) { // from class: org.thoughtcrime.securesms.util.ConversationUtil$$ExternalSyntheticLambda3
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationUtil.lambda$buildPersonsForGroup$2(this.f$0, (Recipient) obj);
            }
        }).toArray(new IntFunction() { // from class: org.thoughtcrime.securesms.util.ConversationUtil$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.IntFunction
            public final Object apply(int i) {
                return ConversationUtil.lambda$buildPersonsForGroup$3(i);
            }
        });
    }

    public static /* synthetic */ Person lambda$buildPersonsForGroup$2(Context context, Recipient recipient) {
        return buildPersonWithoutIcon(context, recipient.resolve());
    }

    public static /* synthetic */ Person[] lambda$buildPersonsForGroup$3(int i) {
        return new Person[i];
    }

    public static Person buildPersonWithoutIcon(Context context, Recipient recipient) {
        return new Person.Builder().setKey(getShortcutId(recipient.getId())).setName(recipient.getDisplayName(context)).setUri(recipient.isSystemContact() ? recipient.getContactUri().toString() : null).build();
    }

    public static Person buildPerson(Context context, Recipient recipient) {
        return new Person.Builder().setKey(getShortcutId(recipient.getId())).setName(recipient.getDisplayName(context)).setIcon(AvatarUtil.getIconForNotification(context, recipient)).setUri(recipient.isSystemContact() ? recipient.getContactUri().toString() : null).build();
    }
}
