package org.thoughtcrime.securesms.util;

import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class DynamicDarkToolbarTheme extends DynamicTheme {
    @Override // org.thoughtcrime.securesms.util.DynamicTheme
    protected int getTheme() {
        return R.style.Signal_DayNight_DarkNoActionBar;
    }
}
