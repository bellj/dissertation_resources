package org.thoughtcrime.securesms.util;

import android.os.Handler;
import android.os.Looper;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;

/* loaded from: classes.dex */
public final class AppStartup {
    private static final AppStartup INSTANCE = new AppStartup();
    private static final String TAG = Log.tag(AppStartup.class);
    private final long FAILSAFE_RENDER_TIME = 2500;
    private final long UI_WAIT_TIME = 500;
    private long applicationStartTime;
    private final List<Task> blocking = new LinkedList();
    private final List<Task> nonBlocking = new LinkedList();
    private int outstandingCriticalRenderEvents;
    private final List<Task> postRender = new LinkedList();
    private final Handler postRenderHandler = new Handler(Looper.getMainLooper());
    private long renderEndTime;
    private long renderStartTime;

    public static AppStartup getInstance() {
        return INSTANCE;
    }

    private AppStartup() {
    }

    public void onApplicationCreate() {
        this.applicationStartTime = System.currentTimeMillis();
    }

    public AppStartup addBlocking(String str, Runnable runnable) {
        this.blocking.add(new Task(str, runnable));
        return this;
    }

    public AppStartup addNonBlocking(Runnable runnable) {
        this.nonBlocking.add(new Task("", runnable));
        return this;
    }

    public AppStartup addPostRender(Runnable runnable) {
        this.postRender.add(new Task("", runnable));
        return this;
    }

    public void onCriticalRenderEventStart() {
        if (this.outstandingCriticalRenderEvents == 0 && this.postRender.size() > 0) {
            Log.i(TAG, "Received first critical render event.");
            this.renderStartTime = System.currentTimeMillis();
            SignalLocalMetrics.ColdStart.onRenderStart();
            this.postRenderHandler.removeCallbacksAndMessages(null);
            this.postRenderHandler.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.util.AppStartup$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    AppStartup.this.lambda$onCriticalRenderEventStart$0();
                }
            }, 2500);
        }
        this.outstandingCriticalRenderEvents++;
    }

    public /* synthetic */ void lambda$onCriticalRenderEventStart$0() {
        Log.w(TAG, "Reached the failsafe event for post-render! Either someone forgot to call #onRenderEnd(), the activity was started while the phone was locked, or app start is taking a very long time.");
        executePostRender();
    }

    public void onCriticalRenderEventEnd() {
        if (this.outstandingCriticalRenderEvents <= 0) {
            Log.w(TAG, "Too many end events! onCriticalRenderEventStart/End was mismanaged.");
        }
        int max = Math.max(this.outstandingCriticalRenderEvents - 1, 0);
        this.outstandingCriticalRenderEvents = max;
        if (max == 0 && this.postRender.size() > 0) {
            this.renderEndTime = System.currentTimeMillis();
            SignalLocalMetrics.ColdStart.onRenderFinished();
            String str = TAG;
            Log.i(str, "First render has finished. Cold Start: " + (this.renderEndTime - this.applicationStartTime) + " ms, Render Time: " + (this.renderEndTime - this.renderStartTime) + " ms");
            this.postRenderHandler.removeCallbacksAndMessages(null);
            executePostRender();
        }
    }

    public void execute() {
        Stopwatch stopwatch = new Stopwatch("init");
        for (Task task : this.blocking) {
            task.getRunnable().run();
            stopwatch.split(task.getName());
        }
        this.blocking.clear();
        for (Task task2 : this.nonBlocking) {
            SignalExecutors.BOUNDED.execute(task2.getRunnable());
        }
        this.nonBlocking.clear();
        stopwatch.split("schedule-non-blocking");
        stopwatch.stop(TAG);
        this.postRenderHandler.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.util.AppStartup$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                AppStartup.this.lambda$execute$1();
            }
        }, 500);
    }

    public /* synthetic */ void lambda$execute$1() {
        Log.i(TAG, "Assuming the application has started in the background. Running post-render tasks.");
        executePostRender();
    }

    private void executePostRender() {
        for (Task task : this.postRender) {
            SignalExecutors.BOUNDED.execute(task.getRunnable());
        }
        this.postRender.clear();
    }

    /* loaded from: classes4.dex */
    public class Task {
        private final String name;
        private final Runnable runnable;

        protected Task(String str, Runnable runnable) {
            AppStartup.this = r1;
            this.name = str;
            this.runnable = runnable;
        }

        String getName() {
            return this.name;
        }

        public Runnable getRunnable() {
            return this.runnable;
        }
    }
}
