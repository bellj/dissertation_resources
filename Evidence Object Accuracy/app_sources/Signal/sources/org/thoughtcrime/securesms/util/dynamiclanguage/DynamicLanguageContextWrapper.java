package org.thoughtcrime.securesms.util.dynamiclanguage;

import android.content.Context;
import android.content.res.Configuration;
import java.util.Locale;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public final class DynamicLanguageContextWrapper {
    private DynamicLanguageContextWrapper() {
    }

    public static void prepareOverrideConfiguration(Context context, Configuration configuration) {
        Locale usersSelectedLocale = getUsersSelectedLocale(context);
        Locale.setDefault(usersSelectedLocale);
        configuration.setLocale(usersSelectedLocale);
    }

    public static Locale getUsersSelectedLocale(Context context) {
        return LocaleParser.findBestMatchingLocaleForLanguage(TextSecurePreferences.getLanguage(context));
    }

    public static void updateContext(Context context) {
        prepareOverrideConfiguration(context, context.getResources().getConfiguration());
    }
}
