package org.thoughtcrime.securesms.util.livedata;

import androidx.lifecycle.Observer;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LiveDataUtil$1$$ExternalSyntheticLambda0 implements Observer {
    public final /* synthetic */ LiveDataUtil.AnonymousClass1 f$0;

    public /* synthetic */ LiveDataUtil$1$$ExternalSyntheticLambda0(LiveDataUtil.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.lambda$new$0(obj);
    }
}
