package org.thoughtcrime.securesms.util;

import android.text.Html;

/* loaded from: classes4.dex */
public class HtmlUtil {
    public static String bold(String str) {
        return "<b>" + Html.escapeHtml(str) + "</b>";
    }
}
