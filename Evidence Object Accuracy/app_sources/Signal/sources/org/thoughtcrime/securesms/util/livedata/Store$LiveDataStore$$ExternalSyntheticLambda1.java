package org.thoughtcrime.securesms.util.livedata;

import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Store$LiveDataStore$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ Store.LiveDataStore f$0;
    public final /* synthetic */ Store.Action f$1;
    public final /* synthetic */ Object f$2;

    public /* synthetic */ Store$LiveDataStore$$ExternalSyntheticLambda1(Store.LiveDataStore liveDataStore, Store.Action action, Object obj) {
        this.f$0 = liveDataStore;
        this.f$1 = action;
        this.f$2 = obj;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$update$0(this.f$1, this.f$2);
    }
}
