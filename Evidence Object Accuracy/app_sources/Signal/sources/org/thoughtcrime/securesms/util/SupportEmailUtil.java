package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import java.util.Locale;
import org.signal.core.util.ResourceUtil;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public final class SupportEmailUtil {
    private static CharSequence getSignalVersion() {
        return BuildConfig.VERSION_NAME;
    }

    private SupportEmailUtil() {
    }

    public static String getSupportEmailAddress(Context context) {
        return context.getString(R.string.SupportEmailUtil_support_email);
    }

    public static String generateSupportEmailBody(Context context, int i, String str, String str2) {
        return generateSupportEmailBody(context, i, null, str, str2);
    }

    public static String generateSupportEmailBody(Context context, int i, String str, String str2, String str3) {
        return String.format("%s\n%s\n%s", Util.emptyIfNull(str2), buildSystemInfo(context, i, Util.emptyIfNull(str)), Util.emptyIfNull(str3));
    }

    private static String buildSystemInfo(Context context, int i, String str) {
        Resources englishResources = ResourceUtil.getEnglishResources(context);
        return "--- " + context.getString(R.string.HelpFragment__support_info) + " ---\n" + context.getString(R.string.SupportEmailUtil_filter) + " " + englishResources.getString(i) + str + "\n" + context.getString(R.string.SupportEmailUtil_device_info) + " " + ((Object) getDeviceInfo()) + "\n" + context.getString(R.string.SupportEmailUtil_android_version) + " " + ((Object) getAndroidVersion()) + "\n" + context.getString(R.string.SupportEmailUtil_signal_version) + " " + ((Object) getSignalVersion()) + "\n" + context.getString(R.string.SupportEmailUtil_signal_package) + " " + ((Object) getSignalPackage(context)) + "\n" + context.getString(R.string.SupportEmailUtil_registration_lock) + " " + ((Object) getRegistrationLockEnabled(context)) + "\n" + context.getString(R.string.SupportEmailUtil_locale) + " " + Locale.getDefault().toString();
    }

    private static CharSequence getDeviceInfo() {
        return String.format("%s %s (%s)", Build.MANUFACTURER, Build.MODEL, Build.PRODUCT);
    }

    private static CharSequence getAndroidVersion() {
        return String.format("%s (%s, %s)", Build.VERSION.RELEASE, Build.VERSION.INCREMENTAL, Build.DISPLAY);
    }

    private static CharSequence getSignalPackage(Context context) {
        return String.format("%s (%s)", "org.thoughtcrime.securesms", AppSignatureUtil.getAppSignature(context));
    }

    private static CharSequence getRegistrationLockEnabled(Context context) {
        return String.valueOf(TextSecurePreferences.isV1RegistrationLockEnabled(context) || SignalStore.kbsValues().isV2RegistrationLockEnabled());
    }
}
