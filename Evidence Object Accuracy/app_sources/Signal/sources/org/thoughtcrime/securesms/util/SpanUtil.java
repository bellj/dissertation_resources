package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.AlignmentSpan;
import android.text.style.BulletSpan;
import android.text.style.CharacterStyle;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.TypefaceSpan;
import android.view.View;
import androidx.core.content.ContextCompat;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class SpanUtil {
    private static final Typeface BOLD_TYPEFACE = Typeface.create("sans-serif-medium", 0);
    private static final Typeface LIGHT_TYPEFACE = Typeface.create("sans-serif", 0);
    private static final Typeface MEDIUM_BOLD_TYPEFACE = Typeface.create("sans-serif-medium", 1);
    public static final String SPAN_PLACE_HOLDER;

    private SpanUtil() {
    }

    public static CharSequence center(CharSequence charSequence) {
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, charSequence.length(), 33);
        return spannableString;
    }

    public static CharSequence textAppearance(Context context, int i, CharSequence charSequence) {
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new TextAppearanceSpan(context, i), 0, charSequence.length(), 33);
        return spannableString;
    }

    public static CharSequence italic(CharSequence charSequence) {
        return italic(charSequence, charSequence.length());
    }

    public static CharSequence italic(CharSequence charSequence, int i) {
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new StyleSpan(2), 0, i, 33);
        return spannableString;
    }

    public static CharSequence small(CharSequence charSequence) {
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new RelativeSizeSpan(0.9f), 0, charSequence.length(), 33);
        return spannableString;
    }

    public static CharSequence ofSize(CharSequence charSequence, int i) {
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new AbsoluteSizeSpan(i, true), 0, charSequence.length(), 33);
        return spannableString;
    }

    public static CharSequence bold(CharSequence charSequence) {
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new StyleSpan(1), 0, charSequence.length(), 33);
        return spannableString;
    }

    public static CharSequence boldSubstring(CharSequence charSequence, CharSequence charSequence2) {
        SpannableString spannableString = new SpannableString(charSequence);
        int indexOf = TextUtils.indexOf(charSequence, charSequence2);
        int length = charSequence2.length() + indexOf;
        if (indexOf >= 0 && length <= charSequence.length()) {
            spannableString.setSpan(new StyleSpan(1), indexOf, length, 33);
        }
        return spannableString;
    }

    public static CharSequence color(int i, CharSequence charSequence) {
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new ForegroundColorSpan(i), 0, charSequence.length(), 33);
        return spannableString;
    }

    public static CharSequence bullet(CharSequence charSequence) {
        return bullet(charSequence, 2);
    }

    public static CharSequence bullet(CharSequence charSequence, int i) {
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new BulletSpan(i), 0, charSequence.length(), 33);
        return spannableString;
    }

    public static CharSequence buildImageSpan(Drawable drawable) {
        SpannableString spannableString = new SpannableString(" ");
        spannableString.setSpan(new ImageSpan(drawable, Build.VERSION.SDK_INT >= 29 ? 2 : 1), 0, spannableString.length(), 0);
        return spannableString;
    }

    public static CharSequence buildCenteredImageSpan(Drawable drawable) {
        SpannableString spannableString = new SpannableString(" ");
        spannableString.setSpan(new CenteredImageSpan(drawable), 0, spannableString.length(), 0);
        return spannableString;
    }

    public static void appendCenteredImageSpan(SpannableStringBuilder spannableStringBuilder, Drawable drawable, int i, int i2) {
        drawable.setBounds(0, 0, ViewUtil.dpToPx(i), ViewUtil.dpToPx(i2));
        spannableStringBuilder.append(" ").append(buildCenteredImageSpan(drawable));
    }

    public static CharSequence learnMore(Context context, int i, View.OnClickListener onClickListener) {
        String string = context.getString(R.string.LearnMoreTextView_learn_more);
        return clickSubstring(string, string, onClickListener, i);
    }

    public static CharSequence readMore(Context context, int i, View.OnClickListener onClickListener) {
        String string = context.getString(R.string.SpanUtil__read_more);
        return clickSubstring(string, string, onClickListener, i);
    }

    public static CharSequence clickable(CharSequence charSequence, int i, View.OnClickListener onClickListener) {
        return clickSubstring(charSequence, charSequence, onClickListener, i);
    }

    public static Spannable clickSubstring(Context context, int i, int i2, final View.OnClickListener onClickListener) {
        String string = context.getString(i, SPAN_PLACE_HOLDER);
        String string2 = context.getString(i2);
        int indexOf = string.indexOf(SPAN_PLACE_HOLDER);
        SpannableString spannableString = new SpannableString(string.substring(0, indexOf) + string2 + string.substring(indexOf + 10));
        spannableString.setSpan(new ClickableSpan() { // from class: org.thoughtcrime.securesms.util.SpanUtil.1
            @Override // android.text.style.ClickableSpan
            public void onClick(View view) {
                onClickListener.onClick(view);
            }

            @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
            public void updateDrawState(TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setUnderlineText(false);
            }
        }, indexOf, string2.length() + indexOf, 17);
        return spannableString;
    }

    public static CharSequence clickSubstring(Context context, CharSequence charSequence, CharSequence charSequence2, View.OnClickListener onClickListener) {
        return clickSubstring(charSequence, charSequence2, onClickListener, ContextCompat.getColor(context, R.color.signal_accent_primary));
    }

    public static CharSequence clickSubstring(CharSequence charSequence, CharSequence charSequence2, final View.OnClickListener onClickListener, final int i) {
        AnonymousClass2 r0 = new ClickableSpan() { // from class: org.thoughtcrime.securesms.util.SpanUtil.2
            @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
            public void updateDrawState(TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setUnderlineText(false);
                textPaint.setColor(i);
            }

            @Override // android.text.style.ClickableSpan
            public void onClick(View view) {
                onClickListener.onClick(view);
            }
        };
        SpannableString spannableString = new SpannableString(charSequence);
        int indexOf = TextUtils.indexOf(charSequence, charSequence2);
        int length = charSequence2.length() + indexOf;
        if (indexOf >= 0 && length <= charSequence.length()) {
            spannableString.setSpan(r0, indexOf, length, 33);
        }
        return spannableString;
    }

    public static CharSequence insertSingleSpan(Resources resources, int i, CharSequence charSequence) {
        return replacePlaceHolder(resources.getString(i, SPAN_PLACE_HOLDER), charSequence);
    }

    public static CharSequence replacePlaceHolder(String str, CharSequence charSequence) {
        int indexOf = str.indexOf(SPAN_PLACE_HOLDER);
        if (indexOf == -1) {
            return str;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        spannableStringBuilder.replace(indexOf, indexOf + 10, charSequence);
        return spannableStringBuilder;
    }

    public static CharacterStyle getMediumBoldSpan() {
        if (Build.VERSION.SDK_INT >= 28) {
            return new TypefaceSpan(MEDIUM_BOLD_TYPEFACE);
        }
        return new StyleSpan(1);
    }

    public static CharacterStyle getBoldSpan() {
        if (Build.VERSION.SDK_INT >= 28) {
            return new TypefaceSpan(BOLD_TYPEFACE);
        }
        return new StyleSpan(1);
    }

    public static CharacterStyle getNormalSpan() {
        if (Build.VERSION.SDK_INT >= 28) {
            return new TypefaceSpan(LIGHT_TYPEFACE);
        }
        return new StyleSpan(0);
    }
}
