package org.thoughtcrime.securesms.util.adapter.mapping;

import j$.util.function.BiFunction;
import j$.util.function.BinaryOperator;
import j$.util.function.Function;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MappingModelList$1$$ExternalSyntheticLambda2 implements BinaryOperator {
    @Override // j$.util.function.BinaryOperator, j$.util.function.BiFunction
    public /* synthetic */ BiFunction andThen(Function function) {
        return BiFunction.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.BiFunction
    public final Object apply(Object obj, Object obj2) {
        return ((MappingModelList) obj).addAll((MappingModelList) obj2);
    }
}
