package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import androidx.recyclerview.widget.RecyclerView;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.AudioSlide;
import org.thoughtcrime.securesms.mms.DocumentSlide;
import org.thoughtcrime.securesms.mms.GifSlide;
import org.thoughtcrime.securesms.mms.ImageSlide;
import org.thoughtcrime.securesms.mms.MmsSlide;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.StickerSlide;
import org.thoughtcrime.securesms.mms.TextSlide;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.mms.ViewOnceSlide;
import org.thoughtcrime.securesms.providers.BlobProvider;

/* loaded from: classes4.dex */
public class MediaUtil {
    public static final String AUDIO_AAC;
    public static final String AUDIO_UNSPECIFIED;
    public static final String IMAGE_GIF;
    public static final String IMAGE_HEIC;
    public static final String IMAGE_HEIF;
    public static final String IMAGE_JPEG;
    public static final String IMAGE_PNG;
    public static final String IMAGE_WEBP;
    public static final String LONG_TEXT;
    public static final String OCTET;
    private static final String TAG = Log.tag(MediaUtil.class);
    public static final String UNKNOWN;
    public static final String VCARD;
    public static final String VIDEO_MP4;
    public static final String VIDEO_UNSPECIFIED;
    public static final String VIEW_ONCE;

    /* loaded from: classes4.dex */
    public enum SlideType {
        GIF,
        IMAGE,
        VIDEO,
        AUDIO,
        MMS,
        LONG_TEXT,
        VIEW_ONCE,
        DOCUMENT
    }

    public static SlideType getSlideTypeFromContentType(String str) {
        if (isGif(str)) {
            return SlideType.GIF;
        }
        if (isImageType(str)) {
            return SlideType.IMAGE;
        }
        if (isVideoType(str)) {
            return SlideType.VIDEO;
        }
        if (isAudioType(str)) {
            return SlideType.AUDIO;
        }
        if (isMms(str)) {
            return SlideType.MMS;
        }
        if (isLongTextType(str)) {
            return SlideType.LONG_TEXT;
        }
        if (isViewOnceType(str)) {
            return SlideType.VIEW_ONCE;
        }
        return SlideType.DOCUMENT;
    }

    public static Slide getSlideForAttachment(Context context, Attachment attachment) {
        if (attachment.isSticker()) {
            return new StickerSlide(context, attachment);
        }
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[getSlideTypeFromContentType(attachment.getContentType()).ordinal()]) {
            case 1:
                return new GifSlide(context, attachment);
            case 2:
                return new ImageSlide(context, attachment);
            case 3:
                return new VideoSlide(context, attachment);
            case 4:
                return new AudioSlide(context, attachment);
            case 5:
                return new MmsSlide(context, attachment);
            case 6:
                return new TextSlide(context, attachment);
            case 7:
                return new ViewOnceSlide(context, attachment);
            case 8:
                return new DocumentSlide(context, attachment);
            default:
                throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.util.MediaUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType;

        static {
            int[] iArr = new int[SlideType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType = iArr;
            try {
                iArr[SlideType.GIF.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[SlideType.IMAGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[SlideType.VIDEO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[SlideType.AUDIO.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[SlideType.MMS.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[SlideType.LONG_TEXT.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[SlideType.VIEW_ONCE.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[SlideType.DOCUMENT.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
        }
    }

    public static String getMimeType(Context context, Uri uri) {
        if (uri == null) {
            return null;
        }
        if (PartAuthority.isLocalUri(uri)) {
            return PartAuthority.getAttachmentContentType(context, uri);
        }
        String type = context.getContentResolver().getType(uri);
        if (type == null || isOctetStream(type)) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(uri.toString()).toLowerCase());
        }
        return getCorrectedMimeType(type);
    }

    public static String getExtension(Context context, Uri uri) {
        return MimeTypeMap.getSingleton().getExtensionFromMimeType(getMimeType(context, uri));
    }

    public static String getCorrectedMimeType(String str) {
        if (str == null) {
            return null;
        }
        if (!str.equals("image/jpg")) {
            return str;
        }
        return MimeTypeMap.getSingleton().hasMimeType(IMAGE_JPEG) ? IMAGE_JPEG : str;
    }

    public static long getMediaSize(Context context, Uri uri) throws IOException {
        InputStream attachmentStream = PartAuthority.getAttachmentStream(context, uri);
        if (attachmentStream != null) {
            long j = 0;
            byte[] bArr = new byte[RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT];
            while (true) {
                int read = attachmentStream.read(bArr);
                if (read != -1) {
                    j += (long) read;
                } else {
                    attachmentStream.close();
                    return j;
                }
            }
        } else {
            throw new IOException("Couldn't obtain input stream.");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:74:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0113 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x00f1 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0102 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.util.Pair<java.lang.Integer, java.lang.Integer> getDimensions(android.content.Context r6, java.lang.String r7, android.net.Uri r8) {
        /*
        // Method dump skipped, instructions count: 355
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.util.MediaUtil.getDimensions(android.content.Context, java.lang.String, android.net.Uri):android.util.Pair");
    }

    public static boolean isMms(String str) {
        return !TextUtils.isEmpty(str) && str.trim().equals("application/mms");
    }

    public static boolean isGif(Attachment attachment) {
        return isGif(attachment.getContentType());
    }

    public static boolean isJpeg(Attachment attachment) {
        return isJpegType(attachment.getContentType());
    }

    public static boolean isHeic(Attachment attachment) {
        return isHeicType(attachment.getContentType());
    }

    public static boolean isHeif(Attachment attachment) {
        return isHeifType(attachment.getContentType());
    }

    public static boolean isImage(Attachment attachment) {
        return isImageType(attachment.getContentType());
    }

    public static boolean isAudio(Attachment attachment) {
        return isAudioType(attachment.getContentType());
    }

    public static boolean isVideo(Attachment attachment) {
        return isVideoType(attachment.getContentType());
    }

    public static boolean isVideo(String str) {
        return !TextUtils.isEmpty(str) && str.trim().startsWith("video/");
    }

    public static boolean isVcard(String str) {
        return !TextUtils.isEmpty(str) && str.trim().equals(VCARD);
    }

    public static boolean isGif(String str) {
        return !TextUtils.isEmpty(str) && str.trim().equals(IMAGE_GIF);
    }

    public static boolean isJpegType(String str) {
        return !TextUtils.isEmpty(str) && str.trim().equals(IMAGE_JPEG);
    }

    public static boolean isHeicType(String str) {
        return !TextUtils.isEmpty(str) && str.trim().equals(IMAGE_HEIC);
    }

    public static boolean isHeifType(String str) {
        return !TextUtils.isEmpty(str) && str.trim().equals(IMAGE_HEIF);
    }

    public static boolean isFile(Attachment attachment) {
        return !isGif(attachment) && !isImage(attachment) && !isAudio(attachment) && !isVideo(attachment);
    }

    public static boolean isTextType(String str) {
        return str != null && str.startsWith("text/");
    }

    public static boolean isNonGifVideo(Media media) {
        return isVideo(media.getMimeType()) && !media.isVideoGif();
    }

    public static boolean isImageType(String str) {
        if (str == null) {
            return false;
        }
        if ((!str.startsWith("image/") || str.equals("image/svg+xml")) && !str.equals("vnd.android.cursor.dir/image")) {
            return false;
        }
        return true;
    }

    public static boolean isAudioType(String str) {
        if (str == null) {
            return false;
        }
        if (str.startsWith("audio/") || str.equals("vnd.android.cursor.dir/audio")) {
            return true;
        }
        return false;
    }

    public static boolean isVideoType(String str) {
        if (str == null) {
            return false;
        }
        if (str.startsWith("video/") || str.equals("vnd.android.cursor.dir/video")) {
            return true;
        }
        return false;
    }

    public static boolean isImageOrVideoType(String str) {
        return isImageType(str) || isVideoType(str);
    }

    public static boolean isStorySupportedType(String str) {
        return isImageOrVideoType(str) && !isGif(str);
    }

    public static boolean isImageVideoOrAudioType(String str) {
        return isImageOrVideoType(str) || isAudioType(str);
    }

    public static boolean isImageAndNotGif(String str) {
        return isImageType(str) && !isGif(str);
    }

    public static boolean isLongTextType(String str) {
        return str != null && str.equals(LONG_TEXT);
    }

    public static boolean isViewOnceType(String str) {
        return str != null && str.equals(VIEW_ONCE);
    }

    public static boolean isOctetStream(String str) {
        return OCTET.equals(str);
    }

    public static boolean hasVideoThumbnail(Context context, Uri uri) {
        if (uri == null) {
            return false;
        }
        if (BlobProvider.isAuthority(uri) && isVideo(BlobProvider.getMimeType(uri)) && Build.VERSION.SDK_INT >= 23) {
            return true;
        }
        if (!isSupportedVideoUriScheme(uri.getScheme())) {
            return false;
        }
        if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
            return uri.getLastPathSegment().contains("video");
        }
        if (uri.toString().startsWith(MediaStore.Video.Media.EXTERNAL_CONTENT_URI.toString())) {
            return true;
        }
        if (!uri.toString().startsWith("file://") || !isVideo(URLConnection.guessContentTypeFromName(uri.toString()))) {
            return PartAuthority.isAttachmentUri(uri) && isVideoType(PartAuthority.getAttachmentContentType(context, uri));
        }
        return true;
    }

    public static Bitmap getVideoThumbnail(Context context, Uri uri, long j) {
        if (uri == null) {
            return null;
        }
        if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
            return MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(), Long.parseLong(uri.getLastPathSegment().split(":")[1]), 1, null);
        } else if (uri.toString().startsWith(MediaStore.Video.Media.EXTERNAL_CONTENT_URI.toString())) {
            return MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(), Long.parseLong(uri.getLastPathSegment()), 1, null);
        } else if (uri.toString().startsWith("file://") && isVideo(URLConnection.guessContentTypeFromName(uri.toString()))) {
            return ThumbnailUtils.createVideoThumbnail(uri.toString().replace("file://", ""), 1);
        } else {
            int i = Build.VERSION.SDK_INT;
            if (i >= 23 && BlobProvider.isAuthority(uri) && isVideo(BlobProvider.getMimeType(uri))) {
                try {
                    return extractFrame(BlobProvider.getInstance().getMediaDataSource(context, uri), j);
                } catch (IOException e) {
                    String str = TAG;
                    Log.w(str, "Failed to extract frame for URI: " + uri, e);
                }
            } else if (i >= 23 && PartAuthority.isAttachmentUri(uri) && isVideoType(PartAuthority.getAttachmentContentType(context, uri))) {
                try {
                    return extractFrame(SignalDatabase.attachments().mediaDataSourceFor(PartAuthority.requireAttachmentId(uri)), j);
                } catch (IOException e2) {
                    String str2 = TAG;
                    Log.w(str2, "Failed to extract frame for URI: " + uri, e2);
                }
            }
            return null;
        }
    }

    private static Bitmap extractFrame(MediaDataSource mediaDataSource, long j) throws IOException {
        if (mediaDataSource == null) {
            return null;
        }
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        MediaMetadataRetrieverUtil.setDataSource(mediaMetadataRetriever, mediaDataSource);
        return mediaMetadataRetriever.getFrameAtTime(j);
    }

    public static String getDiscreteMimeType(String str) {
        String[] split = str.split("/", 2);
        if (split.length > 1) {
            return split[0];
        }
        return null;
    }

    /* loaded from: classes4.dex */
    public static class ThumbnailData implements AutoCloseable {
        private final float aspectRatio;
        private final Bitmap bitmap;

        public ThumbnailData(Bitmap bitmap) {
            this.bitmap = bitmap;
            this.aspectRatio = ((float) bitmap.getWidth()) / ((float) bitmap.getHeight());
        }

        public Bitmap getBitmap() {
            return this.bitmap;
        }

        public float getAspectRatio() {
            return this.aspectRatio;
        }

        public InputStream toDataStream() {
            return BitmapUtil.toCompressedJpeg(this.bitmap);
        }

        @Override // java.lang.AutoCloseable
        public void close() {
            this.bitmap.recycle();
        }
    }

    private static boolean isSupportedVideoUriScheme(String str) {
        return "content".equals(str) || "file".equals(str);
    }
}
