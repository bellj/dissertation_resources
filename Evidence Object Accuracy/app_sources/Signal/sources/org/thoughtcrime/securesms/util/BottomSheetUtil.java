package org.thoughtcrime.securesms.util;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: BottomSheetUtil.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\"\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\u00042\u0006\u0010\n\u001a\u00020\u000bH\u0007J\n\u0010\f\u001a\u00020\r*\u00020\u000bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/util/BottomSheetUtil;", "", "()V", "STANDARD_BOTTOM_SHEET_FRAGMENT_TAG", "", "show", "", "manager", "Landroidx/fragment/app/FragmentManager;", "tag", "dialog", "Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;", "requireCoordinatorLayout", "Landroidx/coordinatorlayout/widget/CoordinatorLayout;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BottomSheetUtil {
    public static final BottomSheetUtil INSTANCE = new BottomSheetUtil();
    public static final String STANDARD_BOTTOM_SHEET_FRAGMENT_TAG;

    private BottomSheetUtil() {
    }

    @JvmStatic
    public static final void show(FragmentManager fragmentManager, String str, BottomSheetDialogFragment bottomSheetDialogFragment) {
        Intrinsics.checkNotNullParameter(fragmentManager, "manager");
        Intrinsics.checkNotNullParameter(bottomSheetDialogFragment, "dialog");
        FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        beginTransaction.add(bottomSheetDialogFragment, str);
        beginTransaction.commitAllowingStateLoss();
    }

    public final CoordinatorLayout requireCoordinatorLayout(BottomSheetDialogFragment bottomSheetDialogFragment) {
        Intrinsics.checkNotNullParameter(bottomSheetDialogFragment, "<this>");
        View findViewById = bottomSheetDialogFragment.requireDialog().findViewById(R.id.coordinator);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireDialog().findViewById(R.id.coordinator)");
        return (CoordinatorLayout) findViewById;
    }
}
