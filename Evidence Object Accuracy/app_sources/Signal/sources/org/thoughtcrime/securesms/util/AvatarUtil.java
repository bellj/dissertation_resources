package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.IconCompat;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomViewTarget;
import com.bumptech.glide.request.transition.Transition;
import j$.util.Optional;
import java.util.concurrent.ExecutionException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.avatars.ContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ProfileContactPhoto;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class AvatarUtil {
    private static final String TAG = Log.tag(AvatarUtil.class);
    public static final int UNDEFINED_SIZE;

    private AvatarUtil() {
    }

    public static void loadBlurredIconIntoImageView(Recipient recipient, final AppCompatImageView appCompatImageView) {
        ContactPhoto contactPhoto;
        Context context = appCompatImageView.getContext();
        if (recipient.isSelf()) {
            contactPhoto = new ProfileContactPhoto(Recipient.self());
        } else if (recipient.getContactPhoto() == null) {
            appCompatImageView.setImageDrawable(null);
            appCompatImageView.setBackgroundColor(ContextCompat.getColor(appCompatImageView.getContext(), R.color.black));
            return;
        } else {
            contactPhoto = recipient.getContactPhoto();
        }
        GlideApp.with(appCompatImageView).load((Object) contactPhoto).transform((Transformation<Bitmap>) new BlurTransformation(context, 0.25f, 25.0f)).into((GlideRequest<Drawable>) new CustomViewTarget<View, Drawable>(appCompatImageView) { // from class: org.thoughtcrime.securesms.util.AvatarUtil.1
            @Override // com.bumptech.glide.request.target.Target
            public /* bridge */ /* synthetic */ void onResourceReady(Object obj, Transition transition) {
                onResourceReady((Drawable) obj, (Transition<? super Drawable>) transition);
            }

            @Override // com.bumptech.glide.request.target.Target
            public void onLoadFailed(Drawable drawable) {
                appCompatImageView.setImageDrawable(null);
                AppCompatImageView appCompatImageView2 = appCompatImageView;
                appCompatImageView2.setBackgroundColor(ContextCompat.getColor(appCompatImageView2.getContext(), R.color.black));
            }

            public void onResourceReady(Drawable drawable, Transition<? super Drawable> transition) {
                appCompatImageView.setImageDrawable(drawable);
            }

            @Override // com.bumptech.glide.request.target.CustomViewTarget
            protected void onResourceCleared(Drawable drawable) {
                appCompatImageView.setImageDrawable(drawable);
            }
        });
    }

    public static void loadIconIntoImageView(Recipient recipient, ImageView imageView) {
        loadIconIntoImageView(recipient, imageView, -1);
    }

    public static void loadIconIntoImageView(Recipient recipient, ImageView imageView, int i) {
        Context context = imageView.getContext();
        requestCircle(GlideApp.with(context).asDrawable(), context, recipient, i).into(imageView);
    }

    public static Bitmap loadIconBitmapSquareNoCache(Context context, Recipient recipient, int i, int i2) throws ExecutionException, InterruptedException {
        return (Bitmap) requestSquare(GlideApp.with(context).asBitmap(), context, recipient).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).submit(i, i2).get();
    }

    public static IconCompat getIconForNotification(Context context, Recipient recipient) {
        try {
            return IconCompat.createWithBitmap((Bitmap) requestCircle(GlideApp.with(context).asBitmap(), context, recipient, -1).submit().get());
        } catch (InterruptedException | ExecutionException unused) {
            return null;
        }
    }

    public static IconCompat getIconCompatForShortcut(Context context, Recipient recipient) {
        try {
            GlideRequest<Bitmap> load = GlideApp.with(context).asBitmap().load((Object) new ConversationShortcutPhoto(recipient));
            if (recipient.shouldBlurAvatar()) {
                load = load.transform((Transformation<Bitmap>) new BlurTransformation(context, 0.25f, 25.0f));
            }
            return IconCompat.createWithAdaptiveBitmap(load.submit().get());
        } catch (InterruptedException | ExecutionException e) {
            String str = TAG;
            Log.w(str, "Failed to generate shortcut icon for recipient " + recipient.getId() + ". Generating fallback.", e);
            int i = DrawableUtil.SHORTCUT_INFO_WRAPPED_SIZE;
            return IconCompat.createWithAdaptiveBitmap(DrawableUtil.wrapBitmapForShortcutInfo(DrawableUtil.toBitmap(getFallback(context, recipient, i), i, i)));
        }
    }

    public static Bitmap getBitmapForNotification(Context context, Recipient recipient) {
        try {
            return (Bitmap) requestCircle(GlideApp.with(context).asBitmap(), context, recipient, -1).submit().get();
        } catch (InterruptedException | ExecutionException unused) {
            return null;
        }
    }

    private static <T> GlideRequest<T> requestCircle(GlideRequest<T> glideRequest, Context context, Recipient recipient, int i) {
        return request(glideRequest, context, recipient, i).circleCrop();
    }

    private static <T> GlideRequest<T> requestSquare(GlideRequest<T> glideRequest, Context context, Recipient recipient) {
        return request(glideRequest, context, recipient, -1).centerCrop();
    }

    private static <T> GlideRequest<T> request(GlideRequest<T> glideRequest, Context context, Recipient recipient, int i) {
        return request(glideRequest, context, recipient, true, i);
    }

    private static <T> GlideRequest<T> request(GlideRequest<T> glideRequest, Context context, Recipient recipient, boolean z, int i) {
        ContactPhoto contactPhoto;
        if (!Recipient.self().equals(recipient) || !z) {
            contactPhoto = recipient.getContactPhoto();
        } else {
            contactPhoto = new ProfileContactPhoto(recipient);
        }
        GlideRequest<T> diskCacheStrategy = glideRequest.load((Object) contactPhoto).error(getFallback(context, recipient, i)).diskCacheStrategy(DiskCacheStrategy.ALL);
        return recipient.shouldBlurAvatar() ? diskCacheStrategy.transform((Transformation<Bitmap>) new BlurTransformation(context, 0.25f, 25.0f)) : diskCacheStrategy;
    }

    private static Drawable getFallback(Context context, Recipient recipient, int i) {
        return new GeneratedContactPhoto((String) Optional.ofNullable(recipient.getDisplayName(context)).orElse(""), R.drawable.ic_profile_outline_40, i).asDrawable(context, recipient.getAvatarColor());
    }
}
