package org.thoughtcrime.securesms.util;

import android.content.ContentResolver;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Settings;
import java.lang.reflect.InvocationTargetException;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public final class RingtoneUtil {
    private static final String TAG = Log.tag(RingtoneUtil.class);

    private RingtoneUtil() {
    }

    public static Ringtone getRingtone(Context context, Uri uri) {
        try {
            return RingtoneManager.getRingtone(context, uri);
        } catch (SecurityException e) {
            Log.w(TAG, "Unable to get default ringtone due to permission", e);
            return RingtoneManager.getRingtone(context, getActualDefaultRingtoneUri(context));
        }
    }

    public static Uri getActualDefaultRingtoneUri(Context context) {
        Log.i(TAG, "Attempting to get default ringtone directly via normal way");
        try {
            return RingtoneManager.getActualDefaultRingtoneUri(context, 1);
        } catch (SecurityException e) {
            Log.w(TAG, "Failed to get ringtone with first fallback approach", e);
            Log.i(TAG, "Attempting to get default ringtone directly via reflection");
            String stringForUser = getStringForUser(context.getContentResolver(), getUserId(context));
            Uri parse = stringForUser != null ? Uri.parse(stringForUser) : null;
            return (parse == null || getUserIdFromAuthority(parse.getAuthority(), getUserId(context)) != getUserId(context)) ? parse : getUriWithoutUserId(parse);
        }
    }

    private static String getStringForUser(ContentResolver contentResolver, int i) {
        try {
            return (String) Settings.System.class.getMethod("getStringForUser", ContentResolver.class, String.class, Integer.TYPE).invoke(Settings.System.class, contentResolver, "ringtone", Integer.valueOf(i));
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            Log.w(TAG, "Unable to getStringForUser via reflection", e);
            return null;
        }
    }

    private static int getUserId(Context context) {
        Object invoke;
        try {
            invoke = Context.class.getMethod("getUserId", new Class[0]).invoke(context, new Object[0]);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            Log.w(TAG, "Unable to getUserId via reflection", e);
        }
        if (invoke instanceof Integer) {
            return ((Integer) invoke).intValue();
        }
        Log.w(TAG, "getUserId did not return an integer");
        return 0;
    }

    private static Uri getUriWithoutUserId(Uri uri) {
        if (uri == null) {
            return null;
        }
        Uri.Builder buildUpon = uri.buildUpon();
        buildUpon.authority(getAuthorityWithoutUserId(uri.getAuthority()));
        return buildUpon.build();
    }

    private static String getAuthorityWithoutUserId(String str) {
        if (str == null) {
            return null;
        }
        return str.substring(str.lastIndexOf(64) + 1);
    }

    private static int getUserIdFromAuthority(String str, int i) {
        int lastIndexOf;
        if (str == null || (lastIndexOf = str.lastIndexOf(64)) == -1) {
            return i;
        }
        try {
            return Integer.parseInt(str.substring(0, lastIndexOf));
        } catch (NumberFormatException unused) {
            return i;
        }
    }
}
