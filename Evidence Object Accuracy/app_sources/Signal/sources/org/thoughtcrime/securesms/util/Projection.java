package org.thoughtcrime.securesms.util;

import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.util.Pools$SimplePool;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/* loaded from: classes4.dex */
public final class Projection {
    private static final Pools$SimplePool<Projection> projectionPool = new Pools$SimplePool<>(125);
    private Corners corners = null;
    private int height = 0;
    private Path path = new Path();
    private RectF rect = new RectF();
    private int width = 0;
    private float x = 0.0f;
    private float y = 0.0f;

    private Projection() {
    }

    private Projection set(float f, float f2, int i, int i2, Corners corners) {
        this.x = f;
        this.y = f2;
        this.width = i;
        this.height = i2;
        this.corners = corners;
        this.rect.set(f, f2, ((float) i) + f, ((float) i2) + f2);
        this.path.reset();
        if (corners != null) {
            this.path.addRoundRect(this.rect, corners.toRadii(), Path.Direction.CW);
        } else {
            this.path.addRect(this.rect, Path.Direction.CW);
        }
        return this;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public Corners getCorners() {
        return this.corners;
    }

    public Path getPath() {
        return this.path;
    }

    public void applyToPath(Path path) {
        Corners corners = this.corners;
        if (corners == null) {
            path.addRect(this.rect, Path.Direction.CW);
        } else if (Build.VERSION.SDK_INT >= 21) {
            path.addRoundRect(this.rect, corners.toRadii(), Path.Direction.CW);
        } else {
            path.op(path, Path.Op.UNION);
        }
    }

    public String toString() {
        return "Projection{x=" + this.x + ", y=" + this.y + ", width=" + this.width + ", height=" + this.height + ", corners=" + this.corners + ", path=" + this.path + ", rect=" + this.rect + '}';
    }

    public Projection translateX(float f) {
        return set(this.x + f, this.y, this.width, this.height, this.corners);
    }

    public Projection translateY(float f) {
        return set(this.x, this.y + f, this.width, this.height, this.corners);
    }

    public Projection scale(float f) {
        Corners corners;
        Corners corners2 = this.corners;
        if (corners2 == null) {
            corners = null;
        } else {
            corners = new Corners(corners2.topLeft * f, this.corners.topRight * f, this.corners.bottomRight * f, this.corners.bottomLeft * f);
        }
        return set(this.x, this.y, (int) (((float) this.width) * f), (int) (((float) this.height) * f), corners);
    }

    public Projection insetTop(int i) {
        Corners corners;
        Corners corners2 = this.corners;
        if (corners2 == null) {
            corners = null;
        } else {
            corners = new Corners(0.0f, 0.0f, corners2.bottomRight, this.corners.bottomLeft);
        }
        return set(this.x, this.y + ((float) i), this.width, this.height - i, corners);
    }

    public Projection insetBottom(int i) {
        Corners corners;
        Corners corners2 = this.corners;
        if (corners2 == null) {
            corners = null;
        } else {
            corners = new Corners(corners2.topLeft, this.corners.topRight, 0.0f, 0.0f);
        }
        return set(this.x, this.y, this.width, this.height - i, corners);
    }

    public static Projection relativeToParent(ViewGroup viewGroup, View view, Corners corners) {
        Rect rect = new Rect();
        view.getDrawingRect(rect);
        viewGroup.offsetDescendantRectToMyCoords(view, rect);
        return acquireAndSet((float) rect.left, (float) rect.top, view.getWidth(), view.getHeight(), corners);
    }

    public static Projection relativeToViewRoot(View view, Corners corners) {
        Rect rect = new Rect();
        view.getDrawingRect(rect);
        ((ViewGroup) view.getRootView()).offsetDescendantRectToMyCoords(view, rect);
        return acquireAndSet((float) rect.left, (float) rect.top, view.getWidth(), view.getHeight(), corners);
    }

    public static Projection relativeToViewWithCommonRoot(View view, View view2, Corners corners) {
        Rect rect = new Rect();
        ViewGroup viewGroup = (ViewGroup) view.getRootView();
        view.getDrawingRect(rect);
        viewGroup.offsetDescendantRectToMyCoords(view, rect);
        viewGroup.offsetRectIntoDescendantCoords(view2, rect);
        return acquireAndSet((float) rect.left, (float) rect.top, view.getWidth(), view.getHeight(), corners);
    }

    public static Projection translateFromDescendantToParentCoords(Projection projection, View view, ViewGroup viewGroup) {
        Rect rect = new Rect();
        float f = projection.x;
        float f2 = projection.y;
        rect.set((int) f, (int) f2, ((int) f) + projection.width, ((int) f2) + projection.height);
        viewGroup.offsetDescendantRectToMyCoords(view, rect);
        return acquireAndSet((float) rect.left, (float) rect.top, projection.width, projection.height, projection.corners);
    }

    public static List<Projection> getCapAndTail(Projection projection, Projection projection2) {
        if (projection.equals(projection2)) {
            return Collections.emptyList();
        }
        float f = projection.x;
        float f2 = projection.y;
        int width = projection.getWidth();
        int i = (int) (projection2.y - projection.y);
        Corners corners = projection.getCorners();
        Corners corners2 = null;
        Corners corners3 = corners != null ? new Corners(corners.topLeft, corners.topRight, 0.0f, 0.0f) : null;
        float f3 = projection.x;
        float height = projection.y + ((float) i) + ((float) projection2.getHeight());
        int width2 = projection.getWidth();
        int height2 = (int) ((projection.y + ((float) projection.getHeight())) - height);
        if (corners != null) {
            corners2 = new Corners(0.0f, 0.0f, corners.bottomRight, corners.bottomLeft);
        }
        return Arrays.asList(acquireAndSet(f, f2, width, i, corners3), acquireAndSet(f3, height, width2, height2, corners2));
    }

    private static Projection acquire() {
        Projection acquire = projectionPool.acquire();
        if (acquire != null) {
            return acquire;
        }
        return new Projection();
    }

    private static Projection acquireAndSet(float f, float f2, int i, int i2, Corners corners) {
        Projection acquire = acquire();
        acquire.set(f, f2, i, i2, corners);
        return acquire;
    }

    public void release() {
        projectionPool.release(this);
    }

    /* loaded from: classes4.dex */
    public static final class Corners {
        private final float bottomLeft;
        private final float bottomRight;
        private final float topLeft;
        private final float topRight;

        public Corners(float f, float f2, float f3, float f4) {
            this.topLeft = f;
            this.topRight = f2;
            this.bottomRight = f3;
            this.bottomLeft = f4;
        }

        public Corners(float[] fArr) {
            this.topLeft = fArr[0];
            this.topRight = fArr[2];
            this.bottomRight = fArr[4];
            this.bottomLeft = fArr[6];
        }

        public Corners(float f) {
            this(f, f, f, f);
        }

        public float getTopLeft() {
            return this.topLeft;
        }

        public float getTopRight() {
            return this.topRight;
        }

        public float getBottomLeft() {
            return this.bottomLeft;
        }

        public float getBottomRight() {
            return this.bottomRight;
        }

        public float[] toRadii() {
            float f = this.topLeft;
            float f2 = this.topRight;
            float f3 = this.bottomRight;
            float f4 = this.bottomLeft;
            return new float[]{f, f, f2, f2, f3, f3, f4, f4};
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Corners.class != obj.getClass()) {
                return false;
            }
            Corners corners = (Corners) obj;
            if (Float.compare(corners.topLeft, this.topLeft) == 0 && Float.compare(corners.topRight, this.topRight) == 0 && Float.compare(corners.bottomRight, this.bottomRight) == 0 && Float.compare(corners.bottomLeft, this.bottomLeft) == 0) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return Objects.hash(Float.valueOf(this.topLeft), Float.valueOf(this.topRight), Float.valueOf(this.bottomRight), Float.valueOf(this.bottomLeft));
        }

        public String toString() {
            return "Corners{topLeft=" + this.topLeft + ", topRight=" + this.topRight + ", bottomRight=" + this.bottomRight + ", bottomLeft=" + this.bottomLeft + '}';
        }
    }
}
