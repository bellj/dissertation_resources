package org.thoughtcrime.securesms.util;

import androidx.recyclerview.widget.RecyclerView;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* loaded from: classes4.dex */
public final class FileUtils {
    /* access modifiers changed from: package-private */
    public static native int createMemoryFileDescriptor(String str);

    public static native int getFileDescriptorOwner(FileDescriptor fileDescriptor);

    static {
        System.loadLibrary("native-utils");
    }

    public static byte[] getFileDigest(FileInputStream fileInputStream) throws IOException {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA256");
            byte[] bArr = new byte[RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT];
            while (true) {
                int read = fileInputStream.read(bArr, 0, RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT);
                if (read == -1) {
                    return instance.digest();
                }
                instance.update(bArr, 0, read);
            }
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public static void deleteDirectoryContents(File file) {
        File[] listFiles;
        if (file != null && file.exists() && file.isDirectory() && (listFiles = file.listFiles()) != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    deleteDirectory(file2);
                } else {
                    file2.delete();
                }
            }
        }
    }

    public static boolean deleteDirectory(File file) {
        if (file == null || !file.exists() || !file.isDirectory()) {
            return false;
        }
        deleteDirectoryContents(file);
        return file.delete();
    }
}
