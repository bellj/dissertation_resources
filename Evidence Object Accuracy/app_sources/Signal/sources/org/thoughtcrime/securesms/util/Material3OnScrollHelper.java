package org.thoughtcrime.securesms.util;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.view.View;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.animation.ArgbEvaluatorCompat;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.util.views.Stub;

/* compiled from: Material3OnScrollHelper.kt */
@Metadata(d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\b\u0016\u0018\u00002\u00020\u0001:\u0002$%B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b\u0012\u0016\b\u0002\u0010\t\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00050\n0\b¢\u0006\u0002\u0010\u000bJ\u000e\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cJ\u0006\u0010\u001d\u001a\u00020\u001aJ\u0012\u0010\u001e\u001a\u00020\u001a2\b\b\u0001\u0010\u001f\u001a\u00020 H\u0002J\u0012\u0010!\u001a\u00020\u001a2\b\b\u0001\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010\"\u001a\u00020\u001a2\u0006\u0010#\u001a\u00020\rH\u0002R\u0012\u0010\f\u001a\u0004\u0018\u00010\rX\u000e¢\u0006\u0004\n\u0002\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\u0010X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0012R\u0012\u0010\u0017\u001a\u00060\u0018R\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00050\n0\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\bX\u0004¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/util/Material3OnScrollHelper;", "", "activity", "Landroid/app/Activity;", "view", "Landroid/view/View;", "(Landroid/app/Activity;Landroid/view/View;)V", "views", "", "viewStubs", "Lorg/thoughtcrime/securesms/util/views/Stub;", "(Landroid/app/Activity;Ljava/util/List;Ljava/util/List;)V", "active", "", "Ljava/lang/Boolean;", "activeColorSet", "Lorg/thoughtcrime/securesms/util/Material3OnScrollHelper$ColorSet;", "getActiveColorSet", "()Lorg/thoughtcrime/securesms/util/Material3OnScrollHelper$ColorSet;", "animator", "Landroid/animation/ValueAnimator;", "inactiveColorSet", "getInactiveColorSet", "scrollListener", "Lorg/thoughtcrime/securesms/util/Material3OnScrollHelper$OnScrollListener;", "attach", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "setColorImmediate", "setStatusBarColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "", "setToolbarColor", "updateActiveState", "isActive", "ColorSet", "OnScrollListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public class Material3OnScrollHelper {
    private Boolean active;
    private final ColorSet activeColorSet;
    private final Activity activity;
    private ValueAnimator animator;
    private final ColorSet inactiveColorSet;
    private final OnScrollListener scrollListener;
    private final List<Stub<? extends View>> viewStubs;
    private final List<View> views;

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends android.view.View> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.util.views.Stub<? extends android.view.View>> */
    /* JADX WARN: Multi-variable type inference failed */
    public Material3OnScrollHelper(Activity activity, List<? extends View> list, List<? extends Stub<? extends View>> list2) {
        Intrinsics.checkNotNullParameter(activity, "activity");
        Intrinsics.checkNotNullParameter(list, "views");
        Intrinsics.checkNotNullParameter(list2, "viewStubs");
        this.activity = activity;
        this.views = list;
        this.viewStubs = list2;
        this.activeColorSet = new ColorSet(R.color.signal_colorSurface2);
        this.inactiveColorSet = new ColorSet(R.color.signal_colorBackground);
        this.scrollListener = new OnScrollListener();
    }

    public /* synthetic */ Material3OnScrollHelper(Activity activity, List list, List list2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(activity, list, (i & 4) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list2);
    }

    /* compiled from: Material3OnScrollHelper.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0019\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\t¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/util/Material3OnScrollHelper$ColorSet;", "", NotificationProfileDatabase.NotificationProfileTable.COLOR, "", "(I)V", "toolbarColorRes", "statusBarColorRes", "(II)V", "getStatusBarColorRes", "()I", "getToolbarColorRes", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ColorSet {
        private final int statusBarColorRes;
        private final int toolbarColorRes;

        public static /* synthetic */ ColorSet copy$default(ColorSet colorSet, int i, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = colorSet.toolbarColorRes;
            }
            if ((i3 & 2) != 0) {
                i2 = colorSet.statusBarColorRes;
            }
            return colorSet.copy(i, i2);
        }

        public final int component1() {
            return this.toolbarColorRes;
        }

        public final int component2() {
            return this.statusBarColorRes;
        }

        public final ColorSet copy(int i, int i2) {
            return new ColorSet(i, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ColorSet)) {
                return false;
            }
            ColorSet colorSet = (ColorSet) obj;
            return this.toolbarColorRes == colorSet.toolbarColorRes && this.statusBarColorRes == colorSet.statusBarColorRes;
        }

        public int hashCode() {
            return (this.toolbarColorRes * 31) + this.statusBarColorRes;
        }

        public String toString() {
            return "ColorSet(toolbarColorRes=" + this.toolbarColorRes + ", statusBarColorRes=" + this.statusBarColorRes + ')';
        }

        public ColorSet(int i, int i2) {
            this.toolbarColorRes = i;
            this.statusBarColorRes = i2;
        }

        public final int getToolbarColorRes() {
            return this.toolbarColorRes;
        }

        public final int getStatusBarColorRes() {
            return this.statusBarColorRes;
        }

        public ColorSet(int i) {
            this(i, i);
        }
    }

    public ColorSet getActiveColorSet() {
        return this.activeColorSet;
    }

    public ColorSet getInactiveColorSet() {
        return this.inactiveColorSet;
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public Material3OnScrollHelper(Activity activity, View view) {
        this(activity, CollectionsKt__CollectionsJVMKt.listOf(view), CollectionsKt__CollectionsKt.emptyList());
        Intrinsics.checkNotNullParameter(activity, "activity");
        Intrinsics.checkNotNullParameter(view, "view");
    }

    public final void attach(RecyclerView recyclerView) {
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        recyclerView.addOnScrollListener(this.scrollListener);
        this.scrollListener.onScrolled(recyclerView, 0, 0);
    }

    public final void setColorImmediate() {
        if (this.active != null) {
            ValueAnimator valueAnimator = this.animator;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            ColorSet activeColorSet = Intrinsics.areEqual(this.active, Boolean.TRUE) ? getActiveColorSet() : getInactiveColorSet();
            setToolbarColor(ContextCompat.getColor(this.activity, activeColorSet.getToolbarColorRes()));
            setStatusBarColor(ContextCompat.getColor(this.activity, activeColorSet.getStatusBarColorRes()));
        }
    }

    public final void updateActiveState(boolean z) {
        if (!Intrinsics.areEqual(this.active, Boolean.valueOf(z))) {
            boolean z2 = true;
            boolean z3 = this.active != null;
            this.active = Boolean.valueOf(z);
            for (View view : this.views) {
                view.setActivated(z);
            }
            List<Stub<? extends View>> list = this.viewStubs;
            ArrayList<Stub> arrayList = new ArrayList();
            for (Object obj : list) {
                if (((Stub) obj).resolved()) {
                    arrayList.add(obj);
                }
            }
            for (Stub stub : arrayList) {
                ((View) stub.get()).setActivated(z);
            }
            ValueAnimator valueAnimator = this.animator;
            if (valueAnimator == null || !valueAnimator.isRunning()) {
                z2 = false;
            }
            if (z2) {
                ValueAnimator valueAnimator2 = this.animator;
                if (valueAnimator2 != null) {
                    valueAnimator2.reverse();
                    return;
                }
                return;
            }
            ColorSet inactiveColorSet = z ? getInactiveColorSet() : getActiveColorSet();
            ColorSet activeColorSet = z ? getActiveColorSet() : getInactiveColorSet();
            if (z3) {
                int color = ContextCompat.getColor(this.activity, inactiveColorSet.getToolbarColorRes());
                int color2 = ContextCompat.getColor(this.activity, activeColorSet.getToolbarColorRes());
                int color3 = ContextCompat.getColor(this.activity, inactiveColorSet.getStatusBarColorRes());
                int color4 = ContextCompat.getColor(this.activity, activeColorSet.getStatusBarColorRes());
                ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
                ofFloat.setDuration(200L);
                ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(color, color2, color3, color4) { // from class: org.thoughtcrime.securesms.util.Material3OnScrollHelper$$ExternalSyntheticLambda0
                    public final /* synthetic */ int f$1;
                    public final /* synthetic */ int f$2;
                    public final /* synthetic */ int f$3;
                    public final /* synthetic */ int f$4;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                        this.f$3 = r4;
                        this.f$4 = r5;
                    }

                    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                    public final void onAnimationUpdate(ValueAnimator valueAnimator3) {
                        Material3OnScrollHelper.m3262updateActiveState$lambda4$lambda3(Material3OnScrollHelper.this, this.f$1, this.f$2, this.f$3, this.f$4, valueAnimator3);
                    }
                });
                ofFloat.start();
                this.animator = ofFloat;
                return;
            }
            setColorImmediate();
        }
    }

    /* renamed from: updateActiveState$lambda-4$lambda-3 */
    public static final void m3262updateActiveState$lambda4$lambda3(Material3OnScrollHelper material3OnScrollHelper, int i, int i2, int i3, int i4, ValueAnimator valueAnimator) {
        Intrinsics.checkNotNullParameter(material3OnScrollHelper, "this$0");
        Integer evaluate = ArgbEvaluatorCompat.getInstance().evaluate(valueAnimator.getAnimatedFraction(), Integer.valueOf(i), Integer.valueOf(i2));
        Intrinsics.checkNotNullExpressionValue(evaluate, "getInstance().evaluate(i…arColor, endToolbarColor)");
        material3OnScrollHelper.setToolbarColor(evaluate.intValue());
        Integer evaluate2 = ArgbEvaluatorCompat.getInstance().evaluate(valueAnimator.getAnimatedFraction(), Integer.valueOf(i3), Integer.valueOf(i4));
        Intrinsics.checkNotNullExpressionValue(evaluate2, "getInstance().evaluate(i…Color, endStatusBarColor)");
        material3OnScrollHelper.setStatusBarColor(evaluate2.intValue());
    }

    private final void setToolbarColor(int i) {
        for (View view : this.views) {
            view.setBackgroundColor(i);
        }
        List<Stub<? extends View>> list = this.viewStubs;
        ArrayList<Stub> arrayList = new ArrayList();
        for (Object obj : list) {
            if (((Stub) obj).resolved()) {
                arrayList.add(obj);
            }
        }
        for (Stub stub : arrayList) {
            ((View) stub.get()).setBackgroundColor(i);
        }
    }

    private final void setStatusBarColor(int i) {
        WindowUtil.setStatusBarColor(this.activity.getWindow(), i);
    }

    /* compiled from: Material3OnScrollHelper.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0016¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/util/Material3OnScrollHelper$OnScrollListener;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "(Lorg/thoughtcrime/securesms/util/Material3OnScrollHelper;)V", "onScrolled", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "dx", "", "dy", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class OnScrollListener extends RecyclerView.OnScrollListener {
        public OnScrollListener() {
            Material3OnScrollHelper.this = r1;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
            Material3OnScrollHelper.this.updateActiveState(recyclerView.canScrollVertically(-1));
        }
    }
}
