package org.thoughtcrime.securesms.util;

import android.content.Context;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;

/* loaded from: classes4.dex */
public class SignalMeUtil {
    private static final String HOST;
    private static final Pattern HOST_PATTERN = Pattern.compile("^(https|sgnl)://signal.me/#p/(\\+[0-9]+)$");

    SignalMeUtil() {
    }

    public static String parseE164FromLink(Context context, String str) {
        if (Util.isEmpty(str)) {
            return null;
        }
        Matcher matcher = HOST_PATTERN.matcher(str);
        if (matcher.matches()) {
            String group = matcher.group(2);
            if (PhoneNumberUtil.getInstance().isPossibleNumber(group, Locale.getDefault().getCountry())) {
                return PhoneNumberFormatter.get(context).format(group);
            }
        }
        return null;
    }
}
