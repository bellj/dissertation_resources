package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.content.DialogInterface;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class Dialogs {
    public static void showAlertDialog(Context context, String str, String str2) {
        new MaterialAlertDialogBuilder(context).setTitle((CharSequence) str).setMessage((CharSequence) str2).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
    }

    public static void showInfoDialog(Context context, String str, String str2) {
        new MaterialAlertDialogBuilder(context).setTitle((CharSequence) str).setMessage((CharSequence) str2).setIcon(R.drawable.ic_info_outline).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
    }
}
