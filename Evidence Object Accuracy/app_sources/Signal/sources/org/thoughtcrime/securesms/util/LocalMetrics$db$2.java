package org.thoughtcrime.securesms.util;

import android.app.Application;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.database.LocalMetricsDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* compiled from: LocalMetrics.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class LocalMetrics$db$2 extends Lambda implements Function0<LocalMetricsDatabase> {
    public static final LocalMetrics$db$2 INSTANCE = new LocalMetrics$db$2();

    LocalMetrics$db$2() {
        super(0);
    }

    @Override // kotlin.jvm.functions.Function0
    public final LocalMetricsDatabase invoke() {
        LocalMetricsDatabase.Companion companion = LocalMetricsDatabase.Companion;
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        return companion.getInstance(application);
    }
}
