package org.thoughtcrime.securesms.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.TaskStackBuilder;
import androidx.fragment.app.FragmentActivity;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.io.IOException;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.WebRtcCallActivity;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinUpdateRequiredBottomSheetDialogFragment;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.proxy.ProxyBottomSheetFragment;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public class CommunicationActions {
    private static final String TAG = Log.tag(CommunicationActions.class);

    public static void startVoiceCall(final FragmentActivity fragmentActivity, final Recipient recipient) {
        if (TelephonyUtil.isAnyPstnLineBusy(fragmentActivity)) {
            Toast.makeText(fragmentActivity, (int) R.string.CommunicationActions_a_cellular_call_is_already_in_progress, 0).show();
        } else if (recipient.isRegistered()) {
            ApplicationDependencies.getSignalCallManager().isCallActive(new ResultReceiver(new Handler(Looper.getMainLooper())) { // from class: org.thoughtcrime.securesms.util.CommunicationActions.1
                @Override // android.os.ResultReceiver
                protected void onReceiveResult(int i, Bundle bundle) {
                    if (i == 1) {
                        CommunicationActions.startCallInternal(fragmentActivity, recipient, false);
                    } else {
                        new MaterialAlertDialogBuilder(fragmentActivity).setMessage(R.string.CommunicationActions_start_voice_call).setPositiveButton(R.string.CommunicationActions_call, (DialogInterface.OnClickListener) new CommunicationActions$1$$ExternalSyntheticLambda0(fragmentActivity, recipient)).setNegativeButton(R.string.CommunicationActions_cancel, (DialogInterface.OnClickListener) new CommunicationActions$1$$ExternalSyntheticLambda1()).setCancelable(true).show();
                    }
                }
            });
        } else {
            startInsecureCall(fragmentActivity, recipient);
        }
    }

    public static void startVideoCall(final FragmentActivity fragmentActivity, final Recipient recipient) {
        if (TelephonyUtil.isAnyPstnLineBusy(fragmentActivity)) {
            Toast.makeText(fragmentActivity, (int) R.string.CommunicationActions_a_cellular_call_is_already_in_progress, 0).show();
        } else {
            ApplicationDependencies.getSignalCallManager().isCallActive(new ResultReceiver(new Handler(Looper.getMainLooper())) { // from class: org.thoughtcrime.securesms.util.CommunicationActions.2
                @Override // android.os.ResultReceiver
                protected void onReceiveResult(int i, Bundle bundle) {
                    FragmentActivity fragmentActivity2 = fragmentActivity;
                    Recipient recipient2 = recipient;
                    boolean z = true;
                    if (i == 1) {
                        z = false;
                    }
                    CommunicationActions.startCallInternal(fragmentActivity2, recipient2, z);
                }
            });
        }
    }

    public static void startConversation(Context context, Recipient recipient, String str) {
        startConversation(context, recipient, str, null);
    }

    public static void startConversation(final Context context, final Recipient recipient, final String str, final TaskStackBuilder taskStackBuilder) {
        new AsyncTask<Void, Void, Long>() { // from class: org.thoughtcrime.securesms.util.CommunicationActions.3
            public Long doInBackground(Void... voidArr) {
                return SignalDatabase.threads().getThreadIdFor(recipient.getId());
            }

            public void onPostExecute(Long l) {
                ConversationIntents.Builder createBuilder = ConversationIntents.createBuilder(context, recipient.getId(), l != null ? l.longValue() : -1);
                if (!TextUtils.isEmpty(str)) {
                    createBuilder.withDraftText(str);
                }
                Intent build = createBuilder.build();
                TaskStackBuilder taskStackBuilder2 = taskStackBuilder;
                if (taskStackBuilder2 != null) {
                    taskStackBuilder2.addNextIntent(build);
                    taskStackBuilder.startActivities();
                    return;
                }
                context.startActivity(build);
            }
        }.execute(new Void[0]);
    }

    public static void startInsecureCall(Activity activity, Recipient recipient) {
        new AlertDialog.Builder(activity).setTitle(R.string.CommunicationActions_insecure_call).setMessage(R.string.CommunicationActions_carrier_charges_may_apply).setPositiveButton(R.string.CommunicationActions_call, new DialogInterface.OnClickListener(activity, recipient) { // from class: org.thoughtcrime.securesms.util.CommunicationActions$$ExternalSyntheticLambda1
            public final /* synthetic */ Activity f$0;
            public final /* synthetic */ Recipient f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                CommunicationActions.lambda$startInsecureCall$0(this.f$0, this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(R.string.CommunicationActions_cancel, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.util.CommunicationActions$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

    public static /* synthetic */ void lambda$startInsecureCall$0(Activity activity, Recipient recipient, DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        startInsecureCallInternal(activity, recipient);
    }

    public static void composeSmsThroughDefaultApp(Context context, Recipient recipient, String str) {
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + recipient.requireSmsAddress()));
        if (str != null) {
            intent.putExtra("sms_body", str);
        }
        context.startActivity(intent);
    }

    public static void openBrowserLink(Context context, String str) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (ActivityNotFoundException unused) {
            Toast.makeText(context, (int) R.string.CommunicationActions_no_browser_found, 0).show();
        }
    }

    public static void openEmail(Context context, String str, String str2, String str3) {
        Intent intent = new Intent("android.intent.action.SENDTO");
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra("android.intent.extra.EMAIL", new String[]{str});
        intent.putExtra("android.intent.extra.SUBJECT", Util.emptyIfNull(str2));
        intent.putExtra("android.intent.extra.TEXT", Util.emptyIfNull(str3));
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.CommunicationActions_send_email)));
    }

    public static boolean handlePotentialGroupLinkUrl(FragmentActivity fragmentActivity, String str) {
        try {
            GroupInviteLinkUrl fromUri = GroupInviteLinkUrl.fromUri(str);
            if (fromUri == null) {
                return false;
            }
            handleGroupLinkUrl(fragmentActivity, fromUri);
            return true;
        } catch (GroupInviteLinkUrl.InvalidGroupLinkException e) {
            Log.w(TAG, "Could not parse group URL", e);
            Toast.makeText(fragmentActivity, (int) R.string.GroupJoinUpdateRequiredBottomSheetDialogFragment_group_link_is_not_valid, 0).show();
            return true;
        } catch (GroupInviteLinkUrl.UnknownGroupLinkVersionException e2) {
            Log.w(TAG, "Group link is for an advanced version", e2);
            GroupJoinUpdateRequiredBottomSheetDialogFragment.show(fragmentActivity.getSupportFragmentManager());
            return true;
        }
    }

    public static void handleGroupLinkUrl(FragmentActivity fragmentActivity, GroupInviteLinkUrl groupInviteLinkUrl) {
        SimpleTask.run(SignalExecutors.BOUNDED, new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.util.CommunicationActions$$ExternalSyntheticLambda6
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return CommunicationActions.lambda$handleGroupLinkUrl$2(GroupId.V2.this);
            }
        }, new SimpleTask.ForegroundTask(groupInviteLinkUrl) { // from class: org.thoughtcrime.securesms.util.CommunicationActions$$ExternalSyntheticLambda7
            public final /* synthetic */ GroupInviteLinkUrl f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                CommunicationActions.lambda$handleGroupLinkUrl$3(FragmentActivity.this, this.f$1, (Recipient) obj);
            }
        });
    }

    public static /* synthetic */ Recipient lambda$handleGroupLinkUrl$2(GroupId.V2 v2) {
        GroupDatabase.GroupRecord orElse = SignalDatabase.groups().getGroup(v2).orElse(null);
        if (orElse == null || !orElse.isActive()) {
            return null;
        }
        return Recipient.resolved(orElse.getRecipientId());
    }

    public static /* synthetic */ void lambda$handleGroupLinkUrl$3(FragmentActivity fragmentActivity, GroupInviteLinkUrl groupInviteLinkUrl, Recipient recipient) {
        if (recipient != null) {
            startConversation(fragmentActivity, recipient, null);
            Toast.makeText(fragmentActivity, (int) R.string.GroupJoinBottomSheetDialogFragment_you_are_already_a_member, 0).show();
            return;
        }
        GroupJoinBottomSheetDialogFragment.show(fragmentActivity.getSupportFragmentManager(), groupInviteLinkUrl);
    }

    public static boolean handlePotentialProxyLinkUrl(FragmentActivity fragmentActivity, String str) {
        String parseHostFromProxyDeepLink = SignalProxyUtil.parseHostFromProxyDeepLink(str);
        if (parseHostFromProxyDeepLink == null) {
            return false;
        }
        ProxyBottomSheetFragment.showForProxy(fragmentActivity.getSupportFragmentManager(), parseHostFromProxyDeepLink);
        return true;
    }

    public static boolean handlePotentialSignalMeUrl(FragmentActivity fragmentActivity, String str) {
        String parseE164FromLink = SignalMeUtil.parseE164FromLink(fragmentActivity, str);
        if (parseE164FromLink == null) {
            return false;
        }
        SimpleTask.run(new SimpleTask.BackgroundTask(parseE164FromLink) { // from class: org.thoughtcrime.securesms.util.CommunicationActions$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return CommunicationActions.lambda$handlePotentialSignalMeUrl$4(FragmentActivity.this, this.f$1);
            }
        }, new SimpleTask.ForegroundTask(fragmentActivity) { // from class: org.thoughtcrime.securesms.util.CommunicationActions$$ExternalSyntheticLambda4
            public final /* synthetic */ FragmentActivity f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                CommunicationActions.lambda$handlePotentialSignalMeUrl$5(SimpleProgressDialog.DismissibleDialog.this, this.f$1, (Recipient) obj);
            }
        });
        return true;
    }

    public static /* synthetic */ Recipient lambda$handlePotentialSignalMeUrl$4(FragmentActivity fragmentActivity, String str) {
        Recipient external = Recipient.external(fragmentActivity, str);
        if (external.isRegistered() && external.hasServiceId()) {
            return external;
        }
        try {
            ContactDiscovery.refresh((Context) fragmentActivity, external, false);
            return Recipient.resolved(external.getId());
        } catch (IOException unused) {
            Log.w(TAG, "[handlePotentialMeUrl] Failed to refresh directory for new contact.");
            return external;
        }
    }

    public static /* synthetic */ void lambda$handlePotentialSignalMeUrl$5(SimpleProgressDialog.DismissibleDialog dismissibleDialog, FragmentActivity fragmentActivity, Recipient recipient) {
        dismissibleDialog.dismiss();
        startConversation(fragmentActivity, recipient, null);
    }

    private static void startInsecureCallInternal(Activity activity, Recipient recipient) {
        try {
            activity.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + recipient.requireSmsAddress())));
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, e);
            Dialogs.showAlertDialog(activity, activity.getString(R.string.ConversationActivity_calls_not_supported), activity.getString(R.string.ConversationActivity_this_device_does_not_appear_to_support_dial_actions));
        }
    }

    public static void startCallInternal(FragmentActivity fragmentActivity, Recipient recipient, boolean z) {
        if (z) {
            startVideoCallInternal(fragmentActivity, recipient);
        } else {
            startAudioCallInternal(fragmentActivity, recipient);
        }
    }

    private static void startAudioCallInternal(FragmentActivity fragmentActivity, Recipient recipient) {
        Permissions.with(fragmentActivity).request("android.permission.RECORD_AUDIO").ifNecessary().withRationaleDialog(fragmentActivity.getString(R.string.ConversationActivity__to_call_s_signal_needs_access_to_your_microphone, new Object[]{recipient.getDisplayName(fragmentActivity)}), R.drawable.ic_mic_solid_24).withPermanentDenialDialog(fragmentActivity.getString(R.string.ConversationActivity__to_call_s_signal_needs_access_to_your_microphone, new Object[]{recipient.getDisplayName(fragmentActivity)})).onAllGranted(new Runnable(fragmentActivity) { // from class: org.thoughtcrime.securesms.util.CommunicationActions$$ExternalSyntheticLambda0
            public final /* synthetic */ FragmentActivity f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CommunicationActions.lambda$startAudioCallInternal$6(Recipient.this, this.f$1);
            }
        }).execute();
    }

    public static /* synthetic */ void lambda$startAudioCallInternal$6(Recipient recipient, FragmentActivity fragmentActivity) {
        ApplicationDependencies.getSignalCallManager().startOutgoingAudioCall(recipient);
        MessageSender.onMessageSent();
        Intent intent = new Intent(fragmentActivity, WebRtcCallActivity.class);
        intent.setFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
        fragmentActivity.startActivity(intent);
    }

    private static void startVideoCallInternal(FragmentActivity fragmentActivity, Recipient recipient) {
        Permissions.with(fragmentActivity).request("android.permission.RECORD_AUDIO", "android.permission.CAMERA").ifNecessary().withRationaleDialog(fragmentActivity.getString(R.string.ConversationActivity_signal_needs_the_microphone_and_camera_permissions_in_order_to_call_s, new Object[]{recipient.getDisplayName(fragmentActivity)}), R.drawable.ic_mic_solid_24, R.drawable.ic_video_solid_24_tinted).withPermanentDenialDialog(fragmentActivity.getString(R.string.ConversationActivity_signal_needs_the_microphone_and_camera_permissions_in_order_to_call_s, new Object[]{recipient.getDisplayName(fragmentActivity)})).onAllGranted(new Runnable(fragmentActivity) { // from class: org.thoughtcrime.securesms.util.CommunicationActions$$ExternalSyntheticLambda5
            public final /* synthetic */ FragmentActivity f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CommunicationActions.lambda$startVideoCallInternal$7(Recipient.this, this.f$1);
            }
        }).execute();
    }

    public static /* synthetic */ void lambda$startVideoCallInternal$7(Recipient recipient, FragmentActivity fragmentActivity) {
        ApplicationDependencies.getSignalCallManager().startPreJoinCall(recipient);
        Intent intent = new Intent(fragmentActivity, WebRtcCallActivity.class);
        intent.setFlags(SQLiteDatabase.CREATE_IF_NECESSARY).putExtra(WebRtcCallActivity.EXTRA_ENABLE_VIDEO_IF_AVAILABLE, true);
        fragmentActivity.startActivity(intent);
    }
}
