package org.thoughtcrime.securesms.util.livedata;

import androidx.lifecycle.Observer;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Store$LiveDataStore$$ExternalSyntheticLambda2 implements Observer {
    public final /* synthetic */ Store.LiveDataStore f$0;
    public final /* synthetic */ Store.Action f$1;

    public /* synthetic */ Store$LiveDataStore$$ExternalSyntheticLambda2(Store.LiveDataStore liveDataStore, Store.Action action) {
        this.f$0 = liveDataStore;
        this.f$1 = action;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.lambda$update$1(this.f$1, obj);
    }
}
