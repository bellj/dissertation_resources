package org.thoughtcrime.securesms.util.adapter.mapping;

import j$.util.function.BiConsumer;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MappingModelList$1$$ExternalSyntheticLambda0 implements BiConsumer {
    @Override // j$.util.function.BiConsumer
    public final void accept(Object obj, Object obj2) {
        ((MappingModelList) obj).add((MappingModel) obj2);
    }

    public /* synthetic */ BiConsumer andThen(BiConsumer biConsumer) {
        return BiConsumer.CC.$default$andThen(this, biConsumer);
    }
}
