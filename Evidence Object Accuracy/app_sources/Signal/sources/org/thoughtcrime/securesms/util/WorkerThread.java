package org.thoughtcrime.securesms.util;

import java.util.List;

/* loaded from: classes4.dex */
public class WorkerThread extends Thread {
    private final List<Runnable> workQueue;

    public WorkerThread(List<Runnable> list, String str) {
        super(str);
        this.workQueue = list;
    }

    private Runnable getWork() {
        Runnable remove;
        synchronized (this.workQueue) {
            while (this.workQueue.isEmpty()) {
                try {
                    try {
                        this.workQueue.wait();
                    } catch (InterruptedException e) {
                        throw new AssertionError(e);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
            remove = this.workQueue.remove(0);
        }
        return remove;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        while (true) {
            getWork().run();
        }
    }
}
