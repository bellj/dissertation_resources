package org.thoughtcrime.securesms.util;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.IOException;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.util.RemoteDeprecation;

/* loaded from: classes4.dex */
public final class RemoteDeprecation {
    private static final String TAG = Log.tag(RemoteDeprecation.class);

    private RemoteDeprecation() {
    }

    public static long getTimeUntilDeprecation() {
        return getTimeUntilDeprecation(FeatureFlags.clientExpiration(), System.currentTimeMillis(), BuildConfig.VERSION_NAME);
    }

    static long getTimeUntilDeprecation(String str, long j, String str2) {
        if (Util.isEmpty(str)) {
            return -1;
        }
        try {
            SemanticVersion parse = SemanticVersion.parse(str2);
            Objects.requireNonNull(parse);
            SemanticVersion semanticVersion = parse;
            ClientExpiration clientExpiration = (ClientExpiration) Stream.of((ClientExpiration[]) JsonUtils.fromJson(str, ClientExpiration[].class)).filter(new Predicate() { // from class: org.thoughtcrime.securesms.util.RemoteDeprecation$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return RemoteDeprecation.$r8$lambda$G0kG3DMuuebXslys_1hJHdJgSJ0((RemoteDeprecation.ClientExpiration) obj);
                }
            }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.util.RemoteDeprecation$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return RemoteDeprecation.m3264$r8$lambda$fOEzn2XasHxFF5BgD5CJXGkAzc(SemanticVersion.this, (RemoteDeprecation.ClientExpiration) obj);
                }
            }).sortBy(new Function() { // from class: org.thoughtcrime.securesms.util.RemoteDeprecation$$ExternalSyntheticLambda2
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return Long.valueOf(((RemoteDeprecation.ClientExpiration) obj).getExpiration());
                }
            }).findFirst().orElse(null);
            if (clientExpiration != null) {
                return Math.max(clientExpiration.getExpiration() - j, 0L);
            }
        } catch (IOException e) {
            Log.w(TAG, e);
        }
        return -1;
    }

    public static /* synthetic */ boolean lambda$getTimeUntilDeprecation$0(ClientExpiration clientExpiration) {
        return (clientExpiration.getVersion() == null || clientExpiration.getExpiration() == -1) ? false : true;
    }

    public static /* synthetic */ boolean lambda$getTimeUntilDeprecation$1(SemanticVersion semanticVersion, ClientExpiration clientExpiration) {
        return clientExpiration.requireVersion().compareTo(semanticVersion) > 0;
    }

    /* loaded from: classes.dex */
    public static final class ClientExpiration {
        @JsonProperty
        private final String iso8601;
        @JsonProperty
        private final String minVersion;

        ClientExpiration(@JsonProperty("minVersion") String str, @JsonProperty("iso8601") String str2) {
            this.minVersion = str;
            this.iso8601 = str2;
        }

        public SemanticVersion getVersion() {
            return SemanticVersion.parse(this.minVersion);
        }

        public SemanticVersion requireVersion() {
            SemanticVersion version = getVersion();
            Objects.requireNonNull(version);
            return version;
        }

        public long getExpiration() {
            return DateUtils.parseIso8601(this.iso8601);
        }
    }
}
