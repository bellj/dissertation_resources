package org.thoughtcrime.securesms.util;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: FixedSizeDrawable.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\b\u0010\b\u001a\u00020\u0005H\u0016J\b\u0010\t\u001a\u00020\u0005H\u0016R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/util/FixedSizeDrawable;", "Landroid/graphics/drawable/LayerDrawable;", "drawable", "Landroid/graphics/drawable/Drawable;", "width", "", "height", "(Landroid/graphics/drawable/Drawable;II)V", "getIntrinsicHeight", "getIntrinsicWidth", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class FixedSizeDrawable extends LayerDrawable {
    private final int height;
    private final int width;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public FixedSizeDrawable(Drawable drawable, int i, int i2) {
        super(new Drawable[]{drawable});
        Intrinsics.checkNotNullParameter(drawable, "drawable");
        this.width = i;
        this.height = i2;
    }

    @Override // android.graphics.drawable.LayerDrawable, android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.width;
    }

    @Override // android.graphics.drawable.LayerDrawable, android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.height;
    }
}
