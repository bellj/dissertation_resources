package org.thoughtcrime.securesms.util.concurrent;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;

/* loaded from: classes4.dex */
public final class SerialExecutor implements Executor {
    private Runnable active;
    private final Executor executor;
    private final Queue<Runnable> tasks = new ArrayDeque();

    public SerialExecutor(Executor executor) {
        this.executor = executor;
    }

    @Override // java.util.concurrent.Executor
    public synchronized void execute(Runnable runnable) {
        this.tasks.offer(new Runnable(runnable) { // from class: org.thoughtcrime.securesms.util.concurrent.SerialExecutor$$ExternalSyntheticLambda0
            public final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SerialExecutor.this.lambda$execute$0(this.f$1);
            }
        });
        if (this.active == null) {
            scheduleNext();
        }
    }

    public /* synthetic */ void lambda$execute$0(Runnable runnable) {
        try {
            runnable.run();
        } finally {
            scheduleNext();
        }
    }

    private synchronized void scheduleNext() {
        Runnable poll = this.tasks.poll();
        this.active = poll;
        if (poll != null) {
            this.executor.execute(poll);
        }
    }
}
