package org.thoughtcrime.securesms.util;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.ShortNumberInfo;
import java.util.HashSet;
import java.util.Set;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class ShortCodeUtil {
    private static final Set<String> SHORT_COUNTRIES = new HashSet<String>() { // from class: org.thoughtcrime.securesms.util.ShortCodeUtil.1
        {
            add("NU");
            add("TK");
            add("NC");
            add("AC");
        }
    };
    private static final String TAG = Log.tag(ShortCodeUtil.class);

    public static boolean isShortCode(String str, String str2) {
        try {
            PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
            String regionCodeForNumber = instance.getRegionCodeForNumber(instance.parse(str, null));
            if (str2.replaceAll("[^0-9+]", "").length() <= 4 && !SHORT_COUNTRIES.contains(regionCodeForNumber)) {
                return true;
            }
            return ShortNumberInfo.getInstance().isPossibleShortNumberForRegion(instance.parse(str2, regionCodeForNumber), regionCodeForNumber);
        } catch (NumberParseException e) {
            Log.w(TAG, e);
            return false;
        }
    }
}
