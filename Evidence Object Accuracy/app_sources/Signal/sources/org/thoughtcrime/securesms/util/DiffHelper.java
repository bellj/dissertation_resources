package org.thoughtcrime.securesms.util;

import java.util.Collection;
import org.signal.core.util.SetUtil;

/* loaded from: classes4.dex */
public class DiffHelper {
    public static <E> Result<E> calculate(Collection<E> collection, Collection<E> collection2) {
        return new Result<>(SetUtil.difference(collection2, collection), SetUtil.difference(collection, collection2));
    }

    /* loaded from: classes4.dex */
    public static class Result<E> {
        private final Collection<E> inserted;
        private final Collection<E> removed;

        public Result(Collection<E> collection, Collection<E> collection2) {
            this.removed = collection2;
            this.inserted = collection;
        }

        public Collection<E> getInserted() {
            return this.inserted;
        }

        public Collection<E> getRemoved() {
            return this.removed;
        }
    }
}
