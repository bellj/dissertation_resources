package org.thoughtcrime.securesms.util;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;

/* loaded from: classes4.dex */
public class ListenableFutureTask<V> extends FutureTask<V> {
    private final Executor callbackExecutor;
    private final Object identifier;
    private final List<FutureTaskListener<V>> listeners;

    public ListenableFutureTask(Callable<V> callable) {
        this((Callable) callable, (Object) null);
    }

    public ListenableFutureTask(Callable<V> callable, Object obj) {
        this(callable, obj, null);
    }

    public ListenableFutureTask(Callable<V> callable, Object obj, Executor executor) {
        super(callable);
        this.listeners = new LinkedList();
        this.identifier = obj;
        this.callbackExecutor = executor;
    }

    public ListenableFutureTask(V v) {
        this(v, (Object) null);
    }

    public ListenableFutureTask(final V v, Object obj) {
        super(new Callable<V>() { // from class: org.thoughtcrime.securesms.util.ListenableFutureTask.1
            @Override // java.util.concurrent.Callable
            public V call() throws Exception {
                return (V) v;
            }
        });
        this.listeners = new LinkedList();
        this.identifier = obj;
        this.callbackExecutor = null;
        run();
    }

    public synchronized void addListener(FutureTaskListener<V> futureTaskListener) {
        if (isDone()) {
            callback(futureTaskListener);
        } else {
            this.listeners.add(futureTaskListener);
        }
    }

    public synchronized void removeListener(FutureTaskListener<V> futureTaskListener) {
        this.listeners.remove(futureTaskListener);
    }

    @Override // java.util.concurrent.FutureTask
    protected synchronized void done() {
        callback();
    }

    private void callback() {
        AnonymousClass2 r0 = new Runnable() { // from class: org.thoughtcrime.securesms.util.ListenableFutureTask.2
            @Override // java.lang.Runnable
            public void run() {
                for (FutureTaskListener futureTaskListener : ListenableFutureTask.this.listeners) {
                    ListenableFutureTask.this.callback(futureTaskListener);
                }
            }
        };
        Executor executor = this.callbackExecutor;
        if (executor == null) {
            r0.run();
        } else {
            executor.execute(r0);
        }
    }

    public void callback(FutureTaskListener<V> futureTaskListener) {
        if (futureTaskListener != null) {
            try {
                futureTaskListener.onSuccess(get());
            } catch (InterruptedException e) {
                throw new AssertionError(e);
            } catch (ExecutionException e2) {
                futureTaskListener.onFailure(e2);
            }
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        Object obj2;
        if (obj == null || !(obj instanceof ListenableFutureTask) || (obj2 = this.identifier) == null) {
            return super.equals(obj);
        }
        return obj2.equals(obj);
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object obj = this.identifier;
        if (obj != null) {
            return obj.hashCode();
        }
        return super.hashCode();
    }
}
