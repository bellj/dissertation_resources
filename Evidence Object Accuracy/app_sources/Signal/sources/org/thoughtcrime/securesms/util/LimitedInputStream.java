package org.thoughtcrime.securesms.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes4.dex */
public class LimitedInputStream extends FilterInputStream {
    private boolean closed;
    private long count;
    private long sizeMax;

    public LimitedInputStream(InputStream inputStream, long j) {
        super(inputStream);
        this.sizeMax = j;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() throws IOException {
        if (this.count >= this.sizeMax) {
            return -1;
        }
        int read = super.read();
        if (read != -1) {
            this.count++;
        }
        return read;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        long j = this.count;
        long j2 = this.sizeMax;
        if (j >= j2) {
            return -1;
        }
        int read = super.read(bArr, i, Util.toIntExact(Math.min((long) i2, j2 - j)));
        if (read > 0) {
            this.count += (long) read;
        }
        return read;
    }
}
