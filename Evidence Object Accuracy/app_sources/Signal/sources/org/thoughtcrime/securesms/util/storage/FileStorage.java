package org.thoughtcrime.securesms.util.storage;

import android.content.Context;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.StreamUtil;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.ModernEncryptingPartOutputStream;

/* loaded from: classes4.dex */
public final class FileStorage {
    public static String save(Context context, InputStream inputStream, String str, String str2, String str3) throws IOException {
        File dir = context.getDir(str, 0);
        File createTempFile = File.createTempFile(str2, "." + str3, dir);
        StreamUtil.copy(inputStream, getOutputStream(context, createTempFile));
        return createTempFile.getName();
    }

    public static InputStream read(Context context, String str, String str2) throws IOException {
        return getInputStream(context, new File(context.getDir(str, 0), str2));
    }

    public static List<String> getAll(Context context, String str, String str2) {
        return (List) Collection$EL.stream(getAllFiles(context, str, str2)).map(new Function() { // from class: org.thoughtcrime.securesms.util.storage.FileStorage$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((File) obj).getName();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
    }

    public static List<File> getAllFiles(Context context, String str, String str2) {
        File[] listFiles = context.getDir(str, 0).listFiles(new FileFilter(str2) { // from class: org.thoughtcrime.securesms.util.storage.FileStorage$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.io.FileFilter
            public final boolean accept(File file) {
                return FileStorage.lambda$getAllFiles$0(this.f$0, file);
            }
        });
        if (listFiles != null) {
            return Arrays.asList(listFiles);
        }
        return Collections.emptyList();
    }

    public static /* synthetic */ boolean lambda$getAllFiles$0(String str, File file) {
        return file.getName().contains(str);
    }

    public static File getFile(Context context, String str, String str2) {
        return new File(context.getDir(str, 0), str2);
    }

    private static OutputStream getOutputStream(Context context, File file) throws IOException {
        return (OutputStream) ModernEncryptingPartOutputStream.createFor(AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), file, true).second;
    }

    private static InputStream getInputStream(Context context, File file) throws IOException {
        return ModernDecryptingPartInputStream.createFor(AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), file, 0);
    }
}
