package org.thoughtcrime.securesms.util;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* loaded from: classes4.dex */
public class ThrottledDebouncer {
    private static final int WHAT;
    private final OverflowHandler handler = new OverflowHandler();
    private final long threshold;

    public ThrottledDebouncer(long j) {
        this.threshold = j;
    }

    public void publish(Runnable runnable) {
        this.handler.setRunnable(runnable);
        if (!this.handler.hasMessages(WHAT)) {
            long max = Math.max(0L, this.threshold - (System.currentTimeMillis() - this.handler.lastRun));
            OverflowHandler overflowHandler = this.handler;
            overflowHandler.sendMessageDelayed(overflowHandler.obtainMessage(WHAT), max);
        }
    }

    public void clear() {
        this.handler.removeCallbacksAndMessages(null);
    }

    /* loaded from: classes4.dex */
    public static class OverflowHandler extends Handler {
        private long lastRun = 0;
        private Runnable runnable;

        public OverflowHandler() {
            super(Looper.getMainLooper());
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            if (message.what == ThrottledDebouncer.WHAT && this.runnable != null) {
                this.lastRun = System.currentTimeMillis();
                this.runnable.run();
                this.runnable = null;
            }
        }

        public void setRunnable(Runnable runnable) {
            this.runnable = runnable;
        }
    }
}
