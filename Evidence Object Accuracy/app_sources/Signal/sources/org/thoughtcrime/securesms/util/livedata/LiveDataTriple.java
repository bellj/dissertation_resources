package org.thoughtcrime.securesms.util.livedata;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import org.thoughtcrime.securesms.util.Triple;

/* loaded from: classes4.dex */
public final class LiveDataTriple<A, B, C> extends MediatorLiveData<Triple<A, B, C>> {
    private A a;
    private B b;
    private C c;

    public LiveDataTriple(LiveData<A> liveData, LiveData<B> liveData2, LiveData<C> liveData3) {
        this(liveData, liveData2, liveData3, null, null, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: androidx.lifecycle.LiveData<A> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: androidx.lifecycle.LiveData<B> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: androidx.lifecycle.LiveData<C> */
    /* JADX WARN: Multi-variable type inference failed */
    public LiveDataTriple(LiveData<A> liveData, LiveData<B> liveData2, LiveData<C> liveData3, A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
        setValue(new Triple(a, b, c));
        if (liveData == liveData2 && liveData == liveData3) {
            addSource(liveData, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataTriple$$ExternalSyntheticLambda0
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataTriple.m3278$r8$lambda$NR_G6ONNmQKQw2eNWS0VP7dCIw(LiveDataTriple.this, obj);
                }
            });
        } else if (liveData == liveData2) {
            addSource(liveData, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataTriple$$ExternalSyntheticLambda1
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataTriple.$r8$lambda$fZ5yxYnxdbIzOuCRhHlflvc1BZg(LiveDataTriple.this, obj);
                }
            });
            addSource(liveData3, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataTriple$$ExternalSyntheticLambda2
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataTriple.$r8$lambda$sLMZG72SZZNeXAkQwKit3R2Fh1o(LiveDataTriple.this, obj);
                }
            });
        } else if (liveData == liveData3) {
            addSource(liveData, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataTriple$$ExternalSyntheticLambda3
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataTriple.$r8$lambda$ni07JFAuHnZ9SLmDy9Rhx7FPgZY(LiveDataTriple.this, obj);
                }
            });
            addSource(liveData2, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataTriple$$ExternalSyntheticLambda4
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataTriple.m3279$r8$lambda$uvjEpXZSvaXGeSsfpeTD4yKU0(LiveDataTriple.this, obj);
                }
            });
        } else if (liveData2 == liveData3) {
            addSource(liveData2, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataTriple$$ExternalSyntheticLambda5
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataTriple.m3280$r8$lambda$xZ9piTupr2ye4JvUDtoOjPCQlI(LiveDataTriple.this, obj);
                }
            });
            addSource(liveData, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataTriple$$ExternalSyntheticLambda6
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataTriple.m3282$r8$lambda$zbVcO_5Omg7p0LFnnt59noKg(LiveDataTriple.this, obj);
                }
            });
        } else {
            addSource(liveData, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataTriple$$ExternalSyntheticLambda7
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataTriple.$r8$lambda$E2iFPnLiQNGXIWAS0Eq2v7PbB0E(LiveDataTriple.this, obj);
                }
            });
            addSource(liveData2, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataTriple$$ExternalSyntheticLambda8
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataTriple.m3281$r8$lambda$z5GBqp_JNKPI5qd0MlNjIMTDw0(LiveDataTriple.this, obj);
                }
            });
            addSource(liveData3, new Observer() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataTriple$$ExternalSyntheticLambda9
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    LiveDataTriple.m3277$r8$lambda$N18If4CJC5gT8qRFlN3PEDE4(LiveDataTriple.this, obj);
                }
            });
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$0(Object obj) {
        if (obj != 0) {
            this.a = obj;
            this.b = obj;
            this.c = obj;
        }
        setValue(new Triple(obj, this.b, this.c));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$1(Object obj) {
        if (obj != 0) {
            this.a = obj;
            this.b = obj;
        }
        setValue(new Triple(obj, this.b, this.c));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$2(Object obj) {
        if (obj != 0) {
            this.c = obj;
        }
        setValue(new Triple(this.a, this.b, obj));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$3(Object obj) {
        if (obj != 0) {
            this.a = obj;
            this.c = obj;
        }
        setValue(new Triple(obj, this.b, this.c));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$4(Object obj) {
        if (obj != 0) {
            this.b = obj;
        }
        setValue(new Triple(this.a, obj, this.c));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$5(Object obj) {
        if (obj != 0) {
            this.b = obj;
            this.c = obj;
        }
        setValue(new Triple(this.a, obj, this.c));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$6(Object obj) {
        if (obj != 0) {
            this.a = obj;
        }
        setValue(new Triple(obj, this.b, this.c));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$7(Object obj) {
        if (obj != 0) {
            this.a = obj;
        }
        setValue(new Triple(obj, this.b, this.c));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$8(Object obj) {
        if (obj != 0) {
            this.b = obj;
        }
        setValue(new Triple(this.a, obj, this.c));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$9(Object obj) {
        if (obj != 0) {
            this.c = obj;
        }
        setValue(new Triple(this.a, this.b, obj));
    }
}
