package org.thoughtcrime.securesms.util;

import android.text.Annotation;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: PlaceholderURLSpan.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/util/PlaceholderURLSpan;", "Landroid/text/Annotation;", "url", "", "(Ljava/lang/String;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PlaceholderURLSpan extends Annotation {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public PlaceholderURLSpan(String str) {
        super("placeholderUrl", str);
        Intrinsics.checkNotNullParameter(str, "url");
    }
}
