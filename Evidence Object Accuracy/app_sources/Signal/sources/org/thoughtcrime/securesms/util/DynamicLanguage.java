package org.thoughtcrime.securesms.util;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.res.Configuration;
import java.util.Locale;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.dynamiclanguage.LanguageString;

@Deprecated
/* loaded from: classes4.dex */
public class DynamicLanguage {
    public void onCreate(Activity activity) {
    }

    public void onResume(Activity activity) {
    }

    public void updateServiceLocale(Service service) {
        setContextLocale(service, getSelectedLocale(service));
    }

    public Locale getCurrentLocale() {
        return Locale.getDefault();
    }

    static int getLayoutDirection(Context context) {
        return context.getResources().getConfiguration().getLayoutDirection();
    }

    private static void setContextLocale(Context context, Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        if (!configuration.locale.equals(locale)) {
            configuration.setLocale(locale);
            context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());
        }
    }

    private static Locale getSelectedLocale(Context context) {
        Locale parseLocale = LanguageString.parseLocale(SignalStore.settings().getLanguage());
        return parseLocale == null ? Locale.getDefault() : parseLocale;
    }
}
