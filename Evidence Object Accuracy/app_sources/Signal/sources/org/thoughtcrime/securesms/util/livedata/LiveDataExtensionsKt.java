package org.thoughtcrime.securesms.util.livedata;

import androidx.lifecycle.LiveData;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: LiveDataExtensions.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u001a6\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0005¨\u0006\u0006"}, d2 = {"distinctUntilChanged", "Landroidx/lifecycle/LiveData;", "T", "R", "selector", "Lkotlin/Function1;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LiveDataExtensionsKt {
    public static final <T, R> LiveData<T> distinctUntilChanged(LiveData<T> liveData, Function1<? super T, ? extends R> function1) {
        Intrinsics.checkNotNullParameter(liveData, "<this>");
        Intrinsics.checkNotNullParameter(function1, "selector");
        LiveData<T> distinctUntilChanged = LiveDataUtil.distinctUntilChanged(liveData, new Function() { // from class: org.thoughtcrime.securesms.util.livedata.LiveDataExtensionsKt$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return LiveDataExtensionsKt.m3276distinctUntilChanged$lambda0(Function1.this, obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        Intrinsics.checkNotNullExpressionValue(distinctUntilChanged, "distinctUntilChanged(this, selector)");
        return distinctUntilChanged;
    }

    /* renamed from: distinctUntilChanged$lambda-0 */
    public static final Object m3276distinctUntilChanged$lambda0(Function1 function1, Object obj) {
        Intrinsics.checkNotNullParameter(function1, "$tmp0");
        return function1.invoke(obj);
    }
}
