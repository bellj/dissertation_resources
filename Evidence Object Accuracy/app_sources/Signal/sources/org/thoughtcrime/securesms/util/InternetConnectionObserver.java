package org.thoughtcrime.securesms.util;

import android.app.Application;
import android.content.IntentFilter;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Cancellable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* compiled from: InternetConnectionObserver.kt */
@Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/util/InternetConnectionObserver;", "", "()V", "observe", "Lio/reactivex/rxjava3/core/Observable;", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class InternetConnectionObserver {
    public static final InternetConnectionObserver INSTANCE = new InternetConnectionObserver();

    private InternetConnectionObserver() {
    }

    public final Observable<Boolean> observe() {
        Observable<Boolean> create = Observable.create(new ObservableOnSubscribe() { // from class: org.thoughtcrime.securesms.util.InternetConnectionObserver$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                InternetConnectionObserver.m3252observe$lambda1(observableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create {\n    val applica…CONNECTIVITY_ACTION))\n  }");
        return create;
    }

    /* renamed from: observe$lambda-1 */
    public static final void m3252observe$lambda1(ObservableEmitter observableEmitter) {
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        InternetConnectionObserver$observe$1$observer$1 internetConnectionObserver$observe$1$observer$1 = new InternetConnectionObserver$observe$1$observer$1(observableEmitter, application);
        observableEmitter.setCancellable(new Cancellable(application, internetConnectionObserver$observe$1$observer$1) { // from class: org.thoughtcrime.securesms.util.InternetConnectionObserver$$ExternalSyntheticLambda1
            public final /* synthetic */ Application f$0;
            public final /* synthetic */ InternetConnectionObserver$observe$1$observer$1 f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                InternetConnectionObserver.m3253observe$lambda1$lambda0(this.f$0, this.f$1);
            }
        });
        application.registerReceiver(internetConnectionObserver$observe$1$observer$1, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    /* renamed from: observe$lambda-1$lambda-0 */
    public static final void m3253observe$lambda1$lambda0(Application application, InternetConnectionObserver$observe$1$observer$1 internetConnectionObserver$observe$1$observer$1) {
        Intrinsics.checkNotNullParameter(application, "$application");
        Intrinsics.checkNotNullParameter(internetConnectionObserver$observe$1$observer$1, "$observer");
        application.unregisterReceiver(internetConnectionObserver$observe$1$observer$1);
    }
}
