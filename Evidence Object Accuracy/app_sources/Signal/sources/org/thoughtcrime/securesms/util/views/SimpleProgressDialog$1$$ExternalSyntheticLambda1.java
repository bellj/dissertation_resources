package org.thoughtcrime.securesms.util.views;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SimpleProgressDialog$1$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ AtomicReference f$0;
    public final /* synthetic */ AtomicLong f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ SimpleProgressDialog$1$$ExternalSyntheticLambda1(AtomicReference atomicReference, AtomicLong atomicLong, int i) {
        this.f$0 = atomicReference;
        this.f$1 = atomicLong;
        this.f$2 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        SimpleProgressDialog.AnonymousClass1.lambda$dismiss$0(this.f$0, this.f$1, this.f$2);
    }
}
