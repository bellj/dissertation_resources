package org.thoughtcrime.securesms.util;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ActionRequestListener.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u0013*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002:\u0001\u0013B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0002\u0010\u0006J4\u0010\u0005\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\u000e\u0010\f\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u0007H\u0016JA\u0010\u0003\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00028\u00002\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\u000e\u0010\f\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\r2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u000e\u001a\u00020\u0007H\u0016¢\u0006\u0002\u0010\u0012R\u000e\u0010\u0005\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/util/ActionRequestListener;", "T", "Lcom/bumptech/glide/request/RequestListener;", "onResourceReady", "Ljava/lang/Runnable;", "onLoadFailed", "(Ljava/lang/Runnable;Ljava/lang/Runnable;)V", "", "e", "Lcom/bumptech/glide/load/engine/GlideException;", "model", "", "target", "Lcom/bumptech/glide/request/target/Target;", "isFirstResource", "resource", "dataSource", "Lcom/bumptech/glide/load/DataSource;", "(Ljava/lang/Object;Ljava/lang/Object;Lcom/bumptech/glide/request/target/Target;Lcom/bumptech/glide/load/DataSource;Z)Z", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ActionRequestListener<T> implements RequestListener<T> {
    public static final Companion Companion = new Companion(null);
    private final Runnable onLoadFailed;
    private final Runnable onResourceReady;

    @JvmStatic
    public static final <T> ActionRequestListener<T> onEither(Runnable runnable) {
        return Companion.onEither(runnable);
    }

    public ActionRequestListener(Runnable runnable, Runnable runnable2) {
        Intrinsics.checkNotNullParameter(runnable, "onResourceReady");
        Intrinsics.checkNotNullParameter(runnable2, "onLoadFailed");
        this.onResourceReady = runnable;
        this.onLoadFailed = runnable2;
    }

    /* compiled from: ActionRequestListener.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0001\u0010\u00052\u0006\u0010\u0003\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/util/ActionRequestListener$Companion;", "", "()V", "onEither", "Lorg/thoughtcrime/securesms/util/ActionRequestListener;", "T", "Ljava/lang/Runnable;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final <T> ActionRequestListener<T> onEither(Runnable runnable) {
            Intrinsics.checkNotNullParameter(runnable, "onEither");
            return new ActionRequestListener<>(runnable, runnable);
        }
    }

    @Override // com.bumptech.glide.request.RequestListener
    public boolean onLoadFailed(GlideException glideException, Object obj, Target<T> target, boolean z) {
        this.onLoadFailed.run();
        return false;
    }

    @Override // com.bumptech.glide.request.RequestListener
    public boolean onResourceReady(T t, Object obj, Target<T> target, DataSource dataSource, boolean z) {
        this.onResourceReady.run();
        return false;
    }
}
