package org.thoughtcrime.securesms.util.navigation;

import android.content.res.Resources;
import android.os.Bundle;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavDirections;
import androidx.navigation.NavOptions;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* compiled from: SafeNavigation.kt */
@Metadata(d1 = {"\u0000.\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0002\u001a\u0012\u0010\u0005\u001a\u00020\u0006*\u00020\u00072\u0006\u0010\b\u001a\u00020\t\u001a\u001c\u0010\u0005\u001a\u00020\u0006*\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u001a\u0014\u0010\u0005\u001a\u00020\u0006*\u00020\u00072\b\b\u0001\u0010\f\u001a\u00020\u0004\u001a\u001e\u0010\u0005\u001a\u00020\u0006*\u00020\u00072\b\b\u0001\u0010\f\u001a\u00020\u00042\b\u0010\r\u001a\u0004\u0018\u00010\u000e\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"TAG", "", "getDisplayName", ContactRepository.ID_COLUMN, "", "safeNavigate", "", "Landroidx/navigation/NavController;", "directions", "Landroidx/navigation/NavDirections;", "navOptions", "Landroidx/navigation/NavOptions;", "resId", "arguments", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafeNavigation {
    private static final String TAG;

    public static final void safeNavigate(NavController navController, int i) {
        Intrinsics.checkNotNullParameter(navController, "<this>");
        NavDestination currentDestination = navController.getCurrentDestination();
        if ((currentDestination != null ? currentDestination.getAction(i) : null) != null) {
            navController.navigate(i);
            return;
        }
        Log.w(TAG, "Unable to find action " + getDisplayName(i) + " for " + navController.getCurrentDestination());
    }

    public static final void safeNavigate(NavController navController, int i, Bundle bundle) {
        Intrinsics.checkNotNullParameter(navController, "<this>");
        NavDestination currentDestination = navController.getCurrentDestination();
        if ((currentDestination != null ? currentDestination.getAction(i) : null) != null) {
            navController.navigate(i, bundle);
            return;
        }
        Log.w(TAG, "Unable to find action " + getDisplayName(i) + " for " + navController.getCurrentDestination());
    }

    public static final void safeNavigate(NavController navController, NavDirections navDirections) {
        Intrinsics.checkNotNullParameter(navController, "<this>");
        Intrinsics.checkNotNullParameter(navDirections, "directions");
        NavDestination currentDestination = navController.getCurrentDestination();
        if ((currentDestination != null ? currentDestination.getAction(navDirections.getActionId()) : null) != null) {
            navController.navigate(navDirections);
            return;
        }
        Log.w(TAG, "Unable to find " + getDisplayName(navDirections.getActionId()) + " for " + navController.getCurrentDestination());
    }

    public static final void safeNavigate(NavController navController, NavDirections navDirections, NavOptions navOptions) {
        Intrinsics.checkNotNullParameter(navController, "<this>");
        Intrinsics.checkNotNullParameter(navDirections, "directions");
        NavDestination currentDestination = navController.getCurrentDestination();
        if ((currentDestination != null ? currentDestination.getAction(navDirections.getActionId()) : null) != null) {
            navController.navigate(navDirections, navOptions);
            return;
        }
        Log.w(TAG, "Unable to find " + getDisplayName(navDirections.getActionId()) + " for " + navController.getCurrentDestination());
    }

    private static final String getDisplayName(int i) {
        if (i <= 16777215) {
            return String.valueOf(i);
        }
        try {
            return ApplicationDependencies.getApplication().getResources().getResourceName(i);
        } catch (Resources.NotFoundException unused) {
            return String.valueOf(i);
        }
    }
}
