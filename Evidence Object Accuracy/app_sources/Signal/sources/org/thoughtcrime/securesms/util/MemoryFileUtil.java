package org.thoughtcrime.securesms.util;

import android.os.Build;
import android.os.MemoryFile;
import android.os.ParcelFileDescriptor;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* loaded from: classes4.dex */
public final class MemoryFileUtil {
    private MemoryFileUtil() {
    }

    public static ParcelFileDescriptor getParcelFileDescriptor(MemoryFile memoryFile) throws IOException {
        if (Build.VERSION.SDK_INT >= 26) {
            return MemoryFileDescriptorProxy.create(ApplicationDependencies.getApplication(), memoryFile);
        }
        return getParcelFileDescriptorLegacy(memoryFile);
    }

    public static ParcelFileDescriptor getParcelFileDescriptorLegacy(MemoryFile memoryFile) throws IOException {
        try {
            FileDescriptor fileDescriptor = (FileDescriptor) MemoryFile.class.getDeclaredMethod("getFileDescriptor", new Class[0]).invoke(memoryFile, new Object[0]);
            Field declaredField = fileDescriptor.getClass().getDeclaredField("descriptor");
            declaredField.setAccessible(true);
            return ParcelFileDescriptor.adoptFd(declaredField.getInt(fileDescriptor));
        } catch (IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
            throw new IOException(e);
        }
    }
}
