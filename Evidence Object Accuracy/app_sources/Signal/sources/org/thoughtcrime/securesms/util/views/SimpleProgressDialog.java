package org.thoughtcrime.securesms.util.views;

import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes4.dex */
public final class SimpleProgressDialog {
    private static final String TAG = Log.tag(SimpleProgressDialog.class);

    /* loaded from: classes4.dex */
    public interface DismissibleDialog {
        void dismiss();

        void dismissNow();
    }

    private SimpleProgressDialog() {
    }

    public static AlertDialog show(Context context) {
        AlertDialog create = new AlertDialog.Builder(context).setView(R.layout.progress_dialog).setCancelable(false).create();
        create.show();
        create.getWindow().setLayout(context.getResources().getDimensionPixelSize(R.dimen.progress_dialog_size), context.getResources().getDimensionPixelSize(R.dimen.progress_dialog_size));
        return create;
    }

    public static DismissibleDialog showDelayed(Context context) {
        return showDelayed(context, WebRtcCallView.PIP_RESIZE_DURATION, MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH);
    }

    public static DismissibleDialog showDelayed(Context context, int i, final int i2) {
        final AtomicReference atomicReference = new AtomicReference();
        final AtomicLong atomicLong = new AtomicLong();
        final SimpleProgressDialog$$ExternalSyntheticLambda0 simpleProgressDialog$$ExternalSyntheticLambda0 = new Runnable(atomicLong, atomicReference, context) { // from class: org.thoughtcrime.securesms.util.views.SimpleProgressDialog$$ExternalSyntheticLambda0
            public final /* synthetic */ AtomicLong f$0;
            public final /* synthetic */ AtomicReference f$1;
            public final /* synthetic */ Context f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SimpleProgressDialog.lambda$showDelayed$0(this.f$0, this.f$1, this.f$2);
            }
        };
        ThreadUtil.runOnMainDelayed(simpleProgressDialog$$ExternalSyntheticLambda0, (long) i);
        return new DismissibleDialog() { // from class: org.thoughtcrime.securesms.util.views.SimpleProgressDialog.1
            @Override // org.thoughtcrime.securesms.util.views.SimpleProgressDialog.DismissibleDialog
            public void dismiss() {
                ThreadUtil.cancelRunnableOnMain(simpleProgressDialog$$ExternalSyntheticLambda0);
                ThreadUtil.runOnMain(new SimpleProgressDialog$1$$ExternalSyntheticLambda1(atomicReference, atomicLong, i2));
            }

            public static /* synthetic */ void lambda$dismiss$0(AtomicReference atomicReference2, AtomicLong atomicLong2, int i3) {
                AlertDialog alertDialog = (AlertDialog) atomicReference2.getAndSet(null);
                if (alertDialog != null) {
                    long currentTimeMillis = ((long) i3) - (System.currentTimeMillis() - atomicLong2.get());
                    if (currentTimeMillis > 0) {
                        ThreadUtil.runOnMainDelayed(new SimpleProgressDialog$1$$ExternalSyntheticLambda2(alertDialog), currentTimeMillis);
                    } else {
                        alertDialog.dismiss();
                    }
                }
            }

            @Override // org.thoughtcrime.securesms.util.views.SimpleProgressDialog.DismissibleDialog
            public void dismissNow() {
                ThreadUtil.cancelRunnableOnMain(simpleProgressDialog$$ExternalSyntheticLambda0);
                ThreadUtil.runOnMain(new SimpleProgressDialog$1$$ExternalSyntheticLambda0(atomicReference));
            }

            public static /* synthetic */ void lambda$dismissNow$1(AtomicReference atomicReference2) {
                AlertDialog alertDialog = (AlertDialog) atomicReference2.getAndSet(null);
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
            }
        };
    }

    public static /* synthetic */ void lambda$showDelayed$0(AtomicLong atomicLong, AtomicReference atomicReference, Context context) {
        Log.i(TAG, "Taking some time. Showing a progress dialog.");
        atomicLong.set(System.currentTimeMillis());
        atomicReference.set(show(context));
    }
}
