package org.thoughtcrime.securesms.util.views;

import android.animation.Animator;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.FrameLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.theme.overlay.MaterialThemeOverlay;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* compiled from: CircularProgressMaterialButton.kt */
@Metadata(d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u0000 +2\u00020\u0001:\u0002+,B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0017\u001a\u00020\u0018J\u0010\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\u001eH\u0014J\b\u0010\u001f\u001a\u00020\u001eH\u0014J\u0010\u0010 \u001a\u00020\u00182\u0006\u0010!\u001a\u00020\u001bH\u0016J\u0010\u0010\"\u001a\u00020\u00182\u0006\u0010#\u001a\u00020\u001bH\u0016J\u0012\u0010$\u001a\u00020\u00182\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J\u0006\u0010'\u001a\u00020\u0018J\u0010\u0010\u0015\u001a\u00020\u00182\b\b\u0001\u0010(\u001a\u00020)J\u0018\u0010*\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R(\u0010\u0012\u001a\u0004\u0018\u00010\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u00118F@FX\u000e¢\u0006\f\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016¨\u0006-"}, d2 = {"Lorg/thoughtcrime/securesms/util/views/CircularProgressMaterialButton;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "animator", "Landroid/animation/Animator;", "currentState", "Lorg/thoughtcrime/securesms/util/views/CircularProgressMaterialButton$State;", "materialButton", "Lcom/google/android/material/button/MaterialButton;", "progressIndicator", "Lcom/google/android/material/progressindicator/CircularProgressIndicator;", "requestedState", DraftDatabase.DRAFT_VALUE, "", DraftDatabase.Draft.TEXT, "getText", "()Ljava/lang/CharSequence;", "setText", "(Ljava/lang/CharSequence;)V", "cancelSpinning", "", "ensureRequestedState", "animate", "", "onRestoreInstanceState", "state", "Landroid/os/Parcelable;", "onSaveInstanceState", "setClickable", "clickable", "setEnabled", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "setOnClickListener", "onClickListener", "Landroid/view/View$OnClickListener;", "setSpinning", "resId", "", "transformTo", "Companion", "State", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CircularProgressMaterialButton extends FrameLayout {
    private static final long ANIMATION_DURATION = 300;
    public static final Companion Companion = new Companion(null);
    private static final String STATE = "state";
    private static final String SUPER_STATE = "super_state";
    private Animator animator;
    private State currentState;
    private final MaterialButton materialButton;
    private final CircularProgressIndicator progressIndicator;
    private State requestedState;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public CircularProgressMaterialButton(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ CircularProgressMaterialButton(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CircularProgressMaterialButton(Context context, AttributeSet attributeSet) {
        super(MaterialThemeOverlay.wrap(context, attributeSet, 0, 0), attributeSet, 0);
        Intrinsics.checkNotNullParameter(context, "context");
        FrameLayout.inflate(getContext(), R.layout.circular_progress_material_button, this);
        State state = State.BUTTON;
        this.currentState = state;
        this.requestedState = state;
        View findViewById = findViewById(R.id.button);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.button)");
        MaterialButton materialButton = (MaterialButton) findViewById;
        this.materialButton = materialButton;
        View findViewById2 = findViewById(R.id.progress_indicator);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.progress_indicator)");
        this.progressIndicator = (CircularProgressIndicator) findViewById2;
        Context context2 = getContext();
        Intrinsics.checkNotNullExpressionValue(context2, "getContext()");
        int[] iArr = R.styleable.CircularProgressMaterialButton;
        Intrinsics.checkNotNullExpressionValue(iArr, "CircularProgressMaterialButton");
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet, iArr, 0, 0);
        Intrinsics.checkNotNullExpressionValue(obtainStyledAttributes, "obtainStyledAttributes(s…efStyleAttr, defStyleRes)");
        materialButton.setText(obtainStyledAttributes.getString(0));
        obtainStyledAttributes.recycle();
    }

    public final CharSequence getText() {
        return this.materialButton.getText();
    }

    public final void setText(CharSequence charSequence) {
        this.materialButton.setText(charSequence);
    }

    public final void setText(int i) {
        this.materialButton.setText(i);
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.materialButton.setEnabled(z);
        ViewExtensionsKt.setVisible(this.progressIndicator, z);
    }

    @Override // android.view.View
    public void setClickable(boolean z) {
        super.setClickable(z);
        this.materialButton.setClickable(z);
        ViewExtensionsKt.setVisible(this.progressIndicator, z);
    }

    @Override // android.view.View
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SUPER_STATE, super.onSaveInstanceState());
        String str = STATE;
        State state = this.requestedState;
        State state2 = this.currentState;
        bundle.putInt(str, state != state2 ? state.getCode() : state2.getCode());
        return bundle;
    }

    @Override // android.view.View
    protected void onRestoreInstanceState(Parcelable parcelable) {
        Intrinsics.checkNotNullParameter(parcelable, "state");
        Bundle bundle = (Bundle) parcelable;
        super.onRestoreInstanceState(bundle.getParcelable(SUPER_STATE));
        this.currentState = this.materialButton.getVisibility() == 4 ? State.PROGRESS : State.BUTTON;
        this.requestedState = State.Companion.fromCode(bundle.getInt(STATE));
        ensureRequestedState(false);
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.materialButton.setOnClickListener(onClickListener);
    }

    public final void setSpinning() {
        transformTo(State.PROGRESS, true);
    }

    public final void cancelSpinning() {
        transformTo(State.BUTTON, true);
    }

    private final void transformTo(State state, boolean z) {
        if (isAttachedToWindow()) {
            if (state != this.currentState || state != this.requestedState) {
                this.requestedState = state;
                Animator animator = this.animator;
                boolean z2 = true;
                if (animator == null || !animator.isRunning()) {
                    z2 = false;
                }
                if (!z2) {
                    if (!z || Build.VERSION.SDK_INT < 21) {
                        this.materialButton.setVisibility(state.getMaterialButtonVisibility());
                        this.currentState = state;
                        return;
                    }
                    this.currentState = state;
                    State state2 = State.BUTTON;
                    if (state == state2) {
                        this.materialButton.setVisibility(0);
                    }
                    float f = 0.0f;
                    float max = (float) Math.max(getMeasuredWidth(), getMeasuredHeight());
                    MaterialButton materialButton = this.materialButton;
                    int measuredWidth = materialButton.getMeasuredWidth() / 2;
                    int measuredHeight = this.materialButton.getMeasuredHeight() / 2;
                    float f2 = state == state2 ? 0.0f : max;
                    if (state != State.PROGRESS) {
                        f = max;
                    }
                    Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(materialButton, measuredWidth, measuredHeight, f2, f);
                    createCircularReveal.setDuration(ANIMATION_DURATION);
                    Intrinsics.checkNotNullExpressionValue(createCircularReveal, "");
                    createCircularReveal.addListener(new Animator.AnimatorListener(this, state) { // from class: org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton$transformTo$lambda-3$$inlined$doOnEnd$1
                        final /* synthetic */ CircularProgressMaterialButton.State $state$inlined;
                        final /* synthetic */ CircularProgressMaterialButton this$0;

                        @Override // android.animation.Animator.AnimatorListener
                        public void onAnimationCancel(Animator animator2) {
                            Intrinsics.checkNotNullParameter(animator2, "animator");
                        }

                        @Override // android.animation.Animator.AnimatorListener
                        public void onAnimationRepeat(Animator animator2) {
                            Intrinsics.checkNotNullParameter(animator2, "animator");
                        }

                        @Override // android.animation.Animator.AnimatorListener
                        public void onAnimationStart(Animator animator2) {
                            Intrinsics.checkNotNullParameter(animator2, "animator");
                        }

                        {
                            this.this$0 = r1;
                            this.$state$inlined = r2;
                        }

                        @Override // android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator2) {
                            Intrinsics.checkNotNullParameter(animator2, "animator");
                            this.this$0.materialButton.setVisibility(this.$state$inlined.getMaterialButtonVisibility());
                            this.this$0.ensureRequestedState(true);
                        }
                    });
                    createCircularReveal.start();
                    this.animator = createCircularReveal;
                }
            }
        }
    }

    public final void ensureRequestedState(boolean z) {
        if (this.requestedState != this.currentState && isAttachedToWindow()) {
            transformTo(this.requestedState, z);
        }
    }

    /* compiled from: CircularProgressMaterialButton.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\t\b\u0001\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000bB\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007j\u0002\b\tj\u0002\b\n¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/util/views/CircularProgressMaterialButton$State;", "", "code", "", "materialButtonVisibility", "(Ljava/lang/String;III)V", "getCode", "()I", "getMaterialButtonVisibility", "BUTTON", "PROGRESS", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum State {
        BUTTON(0, 0),
        PROGRESS(1, 4);
        
        public static final Companion Companion = new Companion(null);
        private final int code;
        private final int materialButtonVisibility;

        State(int i, int i2) {
            this.code = i;
            this.materialButtonVisibility = i2;
        }

        public final int getCode() {
            return this.code;
        }

        public final int getMaterialButtonVisibility() {
            return this.materialButtonVisibility;
        }

        /* compiled from: CircularProgressMaterialButton.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/util/views/CircularProgressMaterialButton$State$Companion;", "", "()V", "fromCode", "Lorg/thoughtcrime/securesms/util/views/CircularProgressMaterialButton$State;", "code", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final State fromCode(int i) {
                if (i == 0) {
                    return State.BUTTON;
                }
                if (i == 1) {
                    return State.PROGRESS;
                }
                throw new IllegalStateException(("Unexpected code " + i).toString());
            }
        }
    }

    /* compiled from: CircularProgressMaterialButton.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006XD¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/util/views/CircularProgressMaterialButton$Companion;", "", "()V", "ANIMATION_DURATION", "", "STATE", "", "SUPER_STATE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
