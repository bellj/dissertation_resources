package org.thoughtcrime.securesms.util.adapter;

import java.util.HashMap;
import java.util.Map;

/* loaded from: classes4.dex */
public class StableIdGenerator<E> {
    private long index = 1;
    private final Map<E, Long> keys = new HashMap();

    public long getId(E e) {
        if (this.keys.containsKey(e)) {
            return this.keys.get(e).longValue();
        }
        long j = this.index;
        this.index = 1 + j;
        this.keys.put(e, Long.valueOf(j));
        return j;
    }
}
