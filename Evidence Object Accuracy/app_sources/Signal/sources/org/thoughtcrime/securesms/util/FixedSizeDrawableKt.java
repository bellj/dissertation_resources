package org.thoughtcrime.securesms.util;

import android.graphics.drawable.Drawable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;

/* compiled from: FixedSizeDrawable.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u001a\u001e\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0001\u0010\u0005\u001a\u00020\u00042\b\b\u0001\u0010\u0006\u001a\u00020\u0004¨\u0006\u0007"}, d2 = {"withFixedSize", "Lorg/thoughtcrime/securesms/util/FixedSizeDrawable;", "Landroid/graphics/drawable/Drawable;", MediaPreviewActivity.SIZE_EXTRA, "", "width", "height", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class FixedSizeDrawableKt {
    public static final FixedSizeDrawable withFixedSize(Drawable drawable, int i, int i2) {
        Intrinsics.checkNotNullParameter(drawable, "<this>");
        return new FixedSizeDrawable(drawable, i, i2);
    }

    public static final FixedSizeDrawable withFixedSize(Drawable drawable, int i) {
        Intrinsics.checkNotNullParameter(drawable, "<this>");
        return withFixedSize(drawable, i, i);
    }
}
