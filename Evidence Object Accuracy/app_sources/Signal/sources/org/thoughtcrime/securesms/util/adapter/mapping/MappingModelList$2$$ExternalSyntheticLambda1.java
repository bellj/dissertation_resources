package org.thoughtcrime.securesms.util.adapter.mapping;

import com.annimon.stream.function.BiConsumer;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MappingModelList$2$$ExternalSyntheticLambda1 implements BiConsumer {
    @Override // com.annimon.stream.function.BiConsumer
    public final void accept(Object obj, Object obj2) {
        ((MappingModelList) obj).add((MappingModel) obj2);
    }
}
