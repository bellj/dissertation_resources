package org.thoughtcrime.securesms.util;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: Views.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u001a\u0014\u0010\u0005\u001a\u00020\u0001*\u00020\u00022\b\b\u0001\u0010\u0003\u001a\u00020\u0004¨\u0006\u0006"}, d2 = {"runHideAnimation", "", "Landroid/view/View;", "anim", "", "runRevealAnimation", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ViewsKt {
    public static final void runRevealAnimation(View view, int i) {
        Intrinsics.checkNotNullParameter(view, "<this>");
        view.setAnimation(AnimationUtils.loadAnimation(view.getContext(), i));
        ViewExtensionsKt.setVisible(view, true);
    }

    public static final void runHideAnimation(View view, int i) {
        Intrinsics.checkNotNullParameter(view, "<this>");
        Animation loadAnimation = AnimationUtils.loadAnimation(view.getContext(), i);
        Intrinsics.checkNotNullExpressionValue(loadAnimation, "");
        AnimationsKt.setListeners$default(loadAnimation, null, new Function1<Animation, Unit>(view) { // from class: org.thoughtcrime.securesms.util.ViewsKt$runHideAnimation$1$1
            final /* synthetic */ View $this_runHideAnimation;

            /* access modifiers changed from: package-private */
            {
                this.$this_runHideAnimation = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Animation animation) {
                invoke(animation);
                return Unit.INSTANCE;
            }

            public final void invoke(Animation animation) {
                ViewExtensionsKt.setVisible(this.$this_runHideAnimation, false);
            }
        }, null, 5, null);
        view.startAnimation(loadAnimation);
    }
}
