package org.thoughtcrime.securesms.util;

import android.view.animation.Animation;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: Animations.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u001aR\u0010\u0006\u001a\u00020\u0002*\u00020\u00002\u0016\b\u0002\u0010\u0003\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0000\u0012\u0004\u0012\u00020\u00020\u00012\u0016\b\u0002\u0010\u0004\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0000\u0012\u0004\u0012\u00020\u00020\u00012\u0016\b\u0002\u0010\u0005\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0000\u0012\u0004\u0012\u00020\u00020\u0001¨\u0006\u0007"}, d2 = {"Landroid/view/animation/Animation;", "Lkotlin/Function1;", "", "onAnimationStart", "onAnimationEnd", "onAnimationRepeat", "setListeners", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class AnimationsKt {
    public static /* synthetic */ void setListeners$default(Animation animation, Function1 function1, Function1 function12, Function1 function13, int i, Object obj) {
        if ((i & 1) != 0) {
            function1 = AnimationsKt$setListeners$1.INSTANCE;
        }
        if ((i & 2) != 0) {
            function12 = AnimationsKt$setListeners$2.INSTANCE;
        }
        if ((i & 4) != 0) {
            function13 = AnimationsKt$setListeners$3.INSTANCE;
        }
        setListeners(animation, function1, function12, function13);
    }

    public static final void setListeners(Animation animation, Function1<? super Animation, Unit> function1, Function1<? super Animation, Unit> function12, Function1<? super Animation, Unit> function13) {
        Intrinsics.checkNotNullParameter(animation, "<this>");
        Intrinsics.checkNotNullParameter(function1, "onAnimationStart");
        Intrinsics.checkNotNullParameter(function12, "onAnimationEnd");
        Intrinsics.checkNotNullParameter(function13, "onAnimationRepeat");
        animation.setAnimationListener(new Animation.AnimationListener(function1, function12, function13) { // from class: org.thoughtcrime.securesms.util.AnimationsKt$setListeners$4
            final /* synthetic */ Function1<Animation, Unit> $onAnimationEnd;
            final /* synthetic */ Function1<Animation, Unit> $onAnimationRepeat;
            final /* synthetic */ Function1<Animation, Unit> $onAnimationStart;

            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: kotlin.jvm.functions.Function1<? super android.view.animation.Animation, kotlin.Unit> */
            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: kotlin.jvm.functions.Function1<? super android.view.animation.Animation, kotlin.Unit> */
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super android.view.animation.Animation, kotlin.Unit> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$onAnimationStart = r1;
                this.$onAnimationEnd = r2;
                this.$onAnimationRepeat = r3;
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationStart(Animation animation2) {
                this.$onAnimationStart.invoke(animation2);
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationEnd(Animation animation2) {
                this.$onAnimationEnd.invoke(animation2);
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationRepeat(Animation animation2) {
                this.$onAnimationRepeat.invoke(animation2);
            }
        });
    }
}
