package org.thoughtcrime.securesms.util;

import android.os.Build;
import android.os.PowerManager;

/* loaded from: classes4.dex */
public class PowerManagerCompat {
    public static boolean isDeviceIdleMode(PowerManager powerManager) {
        if (Build.VERSION.SDK_INT >= 23) {
            return powerManager.isDeviceIdleMode();
        }
        return false;
    }
}
