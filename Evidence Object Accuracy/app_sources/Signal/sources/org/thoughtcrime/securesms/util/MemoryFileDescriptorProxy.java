package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.MemoryFile;
import android.os.ParcelFileDescriptor;
import android.os.ProxyFileDescriptorCallback;
import android.os.storage.StorageManager;
import android.system.ErrnoException;
import android.system.OsConstants;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public final class MemoryFileDescriptorProxy {
    private static final String TAG = Log.tag(MemoryFileDescriptorProxy.class);

    MemoryFileDescriptorProxy() {
    }

    public static ParcelFileDescriptor create(Context context, MemoryFile memoryFile) throws IOException {
        StorageManager storageManager = (StorageManager) context.getSystemService(StorageManager.class);
        Objects.requireNonNull(storageManager);
        HandlerThread handlerThread = new HandlerThread("MemoryFile");
        handlerThread.start();
        String str = TAG;
        Log.i(str, "Thread started");
        ParcelFileDescriptor openProxyFileDescriptor = storageManager.openProxyFileDescriptor(SQLiteDatabase.CREATE_IF_NECESSARY, new ProxyCallback(memoryFile, new Runnable(handlerThread) { // from class: org.thoughtcrime.securesms.util.MemoryFileDescriptorProxy$$ExternalSyntheticLambda0
            public final /* synthetic */ HandlerThread f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MemoryFileDescriptorProxy.$r8$lambda$UNlreC_rHVoNFDMLEoUTfUJpJAA(this.f$0);
            }
        }), new Handler(handlerThread.getLooper()));
        Log.i(str, "Created");
        return openProxyFileDescriptor;
    }

    public static /* synthetic */ void lambda$create$0(HandlerThread handlerThread) {
        if (handlerThread.quitSafely()) {
            Log.i(TAG, "Thread quitSafely true");
        } else {
            Log.w(TAG, "Thread quitSafely false");
        }
    }

    /* loaded from: classes4.dex */
    public static final class ProxyCallback extends ProxyFileDescriptorCallback {
        private final MemoryFile memoryFile;
        private final Runnable onClose;

        ProxyCallback(MemoryFile memoryFile, Runnable runnable) {
            this.memoryFile = memoryFile;
            this.onClose = runnable;
        }

        @Override // android.os.ProxyFileDescriptorCallback
        public long onGetSize() {
            return (long) this.memoryFile.length();
        }

        @Override // android.os.ProxyFileDescriptorCallback
        public int onRead(long j, int i, byte[] bArr) throws ErrnoException {
            try {
                InputStream inputStream = this.memoryFile.getInputStream();
                if (inputStream.skip(j) == j) {
                    return inputStream.read(bArr, 0, i);
                }
                if (j > ((long) this.memoryFile.length())) {
                    throw new ErrnoException("onRead", OsConstants.EIO);
                }
                throw new AssertionError();
            } catch (IOException unused) {
                throw new ErrnoException("onRead", OsConstants.EBADF);
            }
        }

        @Override // android.os.ProxyFileDescriptorCallback
        public void onRelease() {
            Log.i(MemoryFileDescriptorProxy.TAG, "onRelease()");
            this.memoryFile.close();
            this.onClose.run();
        }
    }
}
