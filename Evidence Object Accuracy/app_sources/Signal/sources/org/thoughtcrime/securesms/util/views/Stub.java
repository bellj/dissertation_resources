package org.thoughtcrime.securesms.util.views;

import android.view.ViewStub;

/* loaded from: classes4.dex */
public class Stub<T> {
    private T view;
    private ViewStub viewStub;

    public Stub(ViewStub viewStub) {
        this.viewStub = viewStub;
    }

    public T get() {
        if (this.view == null) {
            this.view = (T) this.viewStub.inflate();
            this.viewStub = null;
        }
        return this.view;
    }

    public boolean resolved() {
        return this.view != null;
    }
}
