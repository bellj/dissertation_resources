package org.thoughtcrime.securesms.util;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.HashMap;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class StickyHeaderDecoration extends RecyclerView.ItemDecoration {
    private static final String TAG = Log.tag(StickyHeaderDecoration.class);
    private final StickyHeaderAdapter adapter;
    private final Map<Long, RecyclerView.ViewHolder> headerCache = new HashMap();
    private final boolean renderInline;
    private final boolean sticky;
    private final int type;

    /* loaded from: classes4.dex */
    public interface StickyHeaderAdapter<T extends RecyclerView.ViewHolder> {
        public static final long NO_HEADER_ID;

        long getHeaderId(int i);

        int getItemCount();

        void onBindHeaderViewHolder(T t, int i, int i2);

        T onCreateHeaderViewHolder(ViewGroup viewGroup, int i, int i2);
    }

    public StickyHeaderDecoration(StickyHeaderAdapter stickyHeaderAdapter, boolean z, boolean z2, int i) {
        this.adapter = stickyHeaderAdapter;
        this.renderInline = z;
        this.sticky = z2;
        this.type = i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
        rect.set(0, (childAdapterPosition == -1 || !hasHeader(recyclerView, this.adapter, childAdapterPosition)) ? 0 : getHeaderHeightForLayout(getHeader(recyclerView, this.adapter, childAdapterPosition).itemView), 0, 0);
    }

    protected boolean hasHeader(RecyclerView recyclerView, StickyHeaderAdapter<?> stickyHeaderAdapter, int i) {
        long headerId = stickyHeaderAdapter.getHeaderId(i);
        if (headerId == -1) {
            return false;
        }
        boolean isReverseLayout = isReverseLayout(recyclerView);
        int itemCount = stickyHeaderAdapter.getItemCount();
        if ((isReverseLayout && i == itemCount - 1 && stickyHeaderAdapter.getHeaderId(i) != -1) || (!isReverseLayout && i == 0)) {
            return true;
        }
        long headerId2 = stickyHeaderAdapter.getHeaderId(i + (isReverseLayout ? 1 : -1));
        if (headerId2 == -1 || headerId != headerId2) {
            return true;
        }
        return false;
    }

    protected RecyclerView.ViewHolder getHeader(RecyclerView recyclerView, StickyHeaderAdapter stickyHeaderAdapter, int i) {
        long headerId = stickyHeaderAdapter.getHeaderId(i);
        RecyclerView.ViewHolder viewHolder = this.headerCache.get(Long.valueOf(headerId));
        if (viewHolder == null) {
            if (headerId != -1) {
                viewHolder = stickyHeaderAdapter.onCreateHeaderViewHolder(recyclerView, i, this.type);
                stickyHeaderAdapter.onBindHeaderViewHolder(viewHolder, i, this.type);
            }
            if (viewHolder == null) {
                viewHolder = new RecyclerView.ViewHolder(LayoutInflater.from(recyclerView.getContext()).inflate(R.layout.null_recyclerview_header, (ViewGroup) recyclerView, false)) { // from class: org.thoughtcrime.securesms.util.StickyHeaderDecoration.1
                };
            }
            this.headerCache.put(Long.valueOf(headerId), viewHolder);
        }
        View view = viewHolder.itemView;
        view.measure(ViewGroup.getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), 1073741824), recyclerView.getPaddingLeft() + recyclerView.getPaddingRight(), view.getLayoutParams().width), ViewGroup.getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), 0), recyclerView.getPaddingTop() + recyclerView.getPaddingBottom(), view.getLayoutParams().height));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        return viewHolder;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        int childCount = recyclerView.getChildCount();
        int i = 0;
        int i2 = 0;
        while (i < childCount) {
            View childAt = recyclerView.getChildAt(translatedChildPosition(recyclerView, i));
            int childAdapterPosition = recyclerView.getChildAdapterPosition(childAt);
            if (this.adapter.getHeaderId(childAdapterPosition) == -1) {
                i2 = i + 1;
            }
            if (childAdapterPosition != -1 && ((i == i2 && this.sticky) || hasHeader(recyclerView, this.adapter, childAdapterPosition))) {
                View view = getHeader(recyclerView, this.adapter, childAdapterPosition).itemView;
                canvas.save();
                canvas.translate((float) recyclerView.getLeft(), (float) getHeaderTop(recyclerView, childAt, view, childAdapterPosition, i));
                view.draw(canvas);
                canvas.restore();
            }
            i++;
            i2 = i2;
        }
    }

    protected int getHeaderTop(RecyclerView recyclerView, View view, View view2, int i, int i2) {
        int headerHeightForLayout = getHeaderHeightForLayout(view2);
        int childY = getChildY(view) - headerHeightForLayout;
        if (!this.sticky || i2 != 0) {
            return childY;
        }
        int childCount = recyclerView.getChildCount();
        long headerId = this.adapter.getHeaderId(i);
        int i3 = 1;
        while (true) {
            if (i3 >= childCount) {
                break;
            }
            int childAdapterPosition = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(translatedChildPosition(recyclerView, i3)));
            if (childAdapterPosition == -1 || this.adapter.getHeaderId(childAdapterPosition) == headerId) {
                i3++;
            } else {
                int childY2 = getChildY(recyclerView.getChildAt(translatedChildPosition(recyclerView, i3))) - (headerHeightForLayout + getHeader(recyclerView, this.adapter, childAdapterPosition).itemView.getHeight());
                if (childY2 < 0) {
                    return childY2;
                }
            }
        }
        return this.sticky ? Math.max(0, childY) : childY;
    }

    private static int translatedChildPosition(RecyclerView recyclerView, int i) {
        return isReverseLayout(recyclerView) ? (recyclerView.getChildCount() - 1) - i : i;
    }

    private static int getChildY(View view) {
        return (int) view.getY();
    }

    private int getHeaderHeightForLayout(View view) {
        if (this.renderInline) {
            return 0;
        }
        return view.getHeight();
    }

    private static boolean isReverseLayout(RecyclerView recyclerView) {
        return (recyclerView.getLayoutManager() instanceof LinearLayoutManager) && ((LinearLayoutManager) recyclerView.getLayoutManager()).getReverseLayout();
    }
}
