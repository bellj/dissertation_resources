package org.thoughtcrime.securesms.util;

import android.text.TextUtils;
import j$.util.Optional;
import java.io.IOException;
import java.util.Locale;
import java.util.regex.Pattern;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public class UsernameUtil {
    private static final Pattern DIGIT_START_PATTERN = Pattern.compile("^[0-9].*$");
    private static final Pattern FULL_PATTERN = Pattern.compile("^[a-z_][a-z0-9_]{3,25}$", 2);
    public static final int MAX_LENGTH;
    public static final int MIN_LENGTH;
    private static final String TAG = Log.tag(UsernameUtil.class);

    /* loaded from: classes4.dex */
    public enum InvalidReason {
        TOO_SHORT,
        TOO_LONG,
        INVALID_CHARACTERS,
        STARTS_WITH_NUMBER
    }

    public static boolean isValidUsernameForSearch(String str) {
        return !TextUtils.isEmpty(str) && !DIGIT_START_PATTERN.matcher(str).matches();
    }

    public static Optional<InvalidReason> checkUsername(String str) {
        if (str == null) {
            return Optional.of(InvalidReason.TOO_SHORT);
        }
        if (str.length() < 4) {
            return Optional.of(InvalidReason.TOO_SHORT);
        }
        if (str.length() > 26) {
            return Optional.of(InvalidReason.TOO_LONG);
        }
        if (DIGIT_START_PATTERN.matcher(str).matches()) {
            return Optional.of(InvalidReason.STARTS_WITH_NUMBER);
        }
        if (!FULL_PATTERN.matcher(str).matches()) {
            return Optional.of(InvalidReason.INVALID_CHARACTERS);
        }
        return Optional.empty();
    }

    public static Optional<ServiceId> fetchAciForUsername(String str) {
        Optional<RecipientId> byUsername = SignalDatabase.recipients().getByUsername(str);
        if (byUsername.isPresent()) {
            Recipient resolved = Recipient.resolved(byUsername.get());
            if (resolved.getServiceId().isPresent()) {
                Log.i(TAG, "Found username locally -- using associated UUID.");
                return resolved.getServiceId();
            }
            Log.w(TAG, "Found username locally, but it had no associated UUID! Clearing it.");
            SignalDatabase.recipients().clearUsernameIfExists(str);
        }
        try {
            Log.d(TAG, "No local user with this username. Searching remotely.");
            return Optional.ofNullable(ApplicationDependencies.getSignalServiceMessageReceiver().retrieveProfileByUsername(str, Optional.empty(), Locale.getDefault()).getServiceId());
        } catch (IOException unused) {
            return Optional.empty();
        }
    }
}
