package org.thoughtcrime.securesms.util.adapter;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import j$.util.Optional;
import j$.util.function.Function;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.components.RecyclerViewFastScroller;
import org.thoughtcrime.securesms.util.StickyHeaderDecoration;
import org.thoughtcrime.securesms.util.adapter.RecyclerViewConcatenateAdapter;

/* loaded from: classes4.dex */
public final class RecyclerViewConcatenateAdapterStickyHeader extends RecyclerViewConcatenateAdapter implements StickyHeaderDecoration.StickyHeaderAdapter, RecyclerViewFastScroller.FastScrollAdapter {
    public static /* synthetic */ Long lambda$getHeaderId$0(Pair pair) {
        return Long.valueOf(((StickyHeaderDecoration.StickyHeaderAdapter) pair.first()).getHeaderId(((Integer) pair.second()).intValue()));
    }

    @Override // org.thoughtcrime.securesms.util.StickyHeaderDecoration.StickyHeaderAdapter
    public long getHeaderId(int i) {
        return ((Long) getForPosition(i).map(new Function() { // from class: org.thoughtcrime.securesms.util.adapter.RecyclerViewConcatenateAdapterStickyHeader$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecyclerViewConcatenateAdapterStickyHeader.lambda$getHeaderId$0((Pair) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(-1L)).longValue();
    }

    public static /* synthetic */ RecyclerView.ViewHolder lambda$onCreateHeaderViewHolder$1(ViewGroup viewGroup, int i, Pair pair) {
        return ((StickyHeaderDecoration.StickyHeaderAdapter) pair.first()).onCreateHeaderViewHolder(viewGroup, ((Integer) pair.second()).intValue(), i);
    }

    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup, int i, int i2) {
        return (RecyclerView.ViewHolder) getForPosition(i).map(new Function(viewGroup, i2) { // from class: org.thoughtcrime.securesms.util.adapter.RecyclerViewConcatenateAdapterStickyHeader$$ExternalSyntheticLambda0
            public final /* synthetic */ ViewGroup f$0;
            public final /* synthetic */ int f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecyclerViewConcatenateAdapterStickyHeader.lambda$onCreateHeaderViewHolder$1(this.f$0, this.f$1, (Pair) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null);
    }

    @Override // org.thoughtcrime.securesms.util.StickyHeaderDecoration.StickyHeaderAdapter
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i, int i2) {
        Optional<Pair<StickyHeaderDecoration.StickyHeaderAdapter, Integer>> forPosition = getForPosition(i);
        if (forPosition.isPresent()) {
            Pair<StickyHeaderDecoration.StickyHeaderAdapter, Integer> pair = forPosition.get();
            pair.first().onBindHeaderViewHolder(viewHolder, pair.second().intValue(), i2);
        }
    }

    @Override // org.thoughtcrime.securesms.components.RecyclerViewFastScroller.FastScrollAdapter
    public CharSequence getBubbleText(int i) {
        return (CharSequence) getForPosition(i).map(new Function() { // from class: org.thoughtcrime.securesms.util.adapter.RecyclerViewConcatenateAdapterStickyHeader$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecyclerViewConcatenateAdapterStickyHeader.lambda$getBubbleText$2((Pair) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse("");
    }

    public static /* synthetic */ CharSequence lambda$getBubbleText$2(Pair pair) {
        return pair.first() instanceof RecyclerViewFastScroller.FastScrollAdapter ? ((RecyclerViewFastScroller.FastScrollAdapter) pair.first()).getBubbleText(((Integer) pair.second()).intValue()) : "";
    }

    private Optional<Pair<StickyHeaderDecoration.StickyHeaderAdapter, Integer>> getForPosition(int i) {
        RecyclerViewConcatenateAdapter.ChildAdapterPositionPair localPosition = getLocalPosition(i);
        RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter = localPosition.getAdapter();
        if (adapter instanceof StickyHeaderDecoration.StickyHeaderAdapter) {
            return Optional.of(new Pair((StickyHeaderDecoration.StickyHeaderAdapter) adapter, Integer.valueOf(localPosition.localPosition)));
        }
        return Optional.empty();
    }
}
