package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.Toast;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class LongClickCopySpan extends URLSpan {
    private static final String PREFIX_MAILTO;
    private static final String PREFIX_TEL;
    private int highlightColor;
    private boolean isHighlighted;
    private final Integer textColor;
    private final boolean underline;

    public LongClickCopySpan(String str) {
        this(str, null, true);
    }

    public LongClickCopySpan(String str, Integer num, boolean z) {
        super(str);
        this.textColor = num;
        this.underline = z;
    }

    public void onLongClick(View view) {
        Context context = view.getContext();
        String prepareUrl = prepareUrl(getURL());
        copyUrl(context, prepareUrl);
        Toast.makeText(context, context.getString(R.string.ConversationItem_copied_text, prepareUrl), 0).show();
    }

    @Override // android.text.style.CharacterStyle, android.text.style.ClickableSpan
    public void updateDrawState(TextPaint textPaint) {
        super.updateDrawState(textPaint);
        Integer num = this.textColor;
        if (num != null) {
            textPaint.setColor(num.intValue());
        }
        textPaint.bgColor = this.highlightColor;
        textPaint.setUnderlineText(!this.isHighlighted && this.underline);
    }

    public void setHighlighted(boolean z, int i) {
        this.isHighlighted = z;
        this.highlightColor = i;
    }

    private void copyUrl(Context context, String str) {
        Util.writeTextToClipboard(context, str);
    }

    private String prepareUrl(String str) {
        if (str.startsWith(PREFIX_MAILTO)) {
            return str.substring(7);
        }
        return str.startsWith(PREFIX_TEL) ? str.substring(4) : str;
    }
}
