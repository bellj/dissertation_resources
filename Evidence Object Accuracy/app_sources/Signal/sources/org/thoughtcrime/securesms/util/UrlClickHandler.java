package org.thoughtcrime.securesms.util;

/* loaded from: classes4.dex */
public interface UrlClickHandler {
    boolean handleOnClick(String str);
}
