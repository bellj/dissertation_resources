package org.thoughtcrime.securesms.util;

import android.content.DialogInterface;
import androidx.fragment.app.FragmentActivity;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CommunicationActions$1$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ FragmentActivity f$0;
    public final /* synthetic */ Recipient f$1;

    public /* synthetic */ CommunicationActions$1$$ExternalSyntheticLambda0(FragmentActivity fragmentActivity, Recipient recipient) {
        this.f$0 = fragmentActivity;
        this.f$1 = recipient;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        CommunicationActions.access$000(this.f$0, this.f$1, false);
    }
}
