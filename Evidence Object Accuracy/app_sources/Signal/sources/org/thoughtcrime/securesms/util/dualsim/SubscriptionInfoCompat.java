package org.thoughtcrime.securesms.util.dualsim;

/* loaded from: classes4.dex */
public class SubscriptionInfoCompat {
    private final CharSequence displayName;
    private final int mcc;
    private final int mnc;
    private final int subscriptionId;

    public SubscriptionInfoCompat(int i, CharSequence charSequence, int i2, int i3) {
        this.subscriptionId = i;
        this.displayName = charSequence;
        this.mcc = i2;
        this.mnc = i3;
    }

    public CharSequence getDisplayName() {
        CharSequence charSequence = this.displayName;
        return charSequence != null ? charSequence : "";
    }

    public int getSubscriptionId() {
        return this.subscriptionId;
    }

    public int getMnc() {
        return this.mnc;
    }

    public int getMcc() {
        return this.mcc;
    }
}
