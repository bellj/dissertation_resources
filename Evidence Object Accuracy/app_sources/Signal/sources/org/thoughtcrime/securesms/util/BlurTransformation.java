package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import java.security.MessageDigest;
import java.util.Locale;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public final class BlurTransformation extends BitmapTransformation {
    public static final float MAX_RADIUS;
    private final float bitmapScaleFactor;
    private final float blurRadius;
    private final RenderScript rs;

    public BlurTransformation(Context context, float f, float f2) {
        this.rs = RenderScript.create(context);
        boolean z = true;
        Preconditions.checkArgument(f2 >= 0.0f && f2 <= 25.0f, "Blur radius must be a non-negative value less than or equal to 25.");
        Preconditions.checkArgument(f <= 0.0f ? false : z, "Bitmap scale factor must be a non-negative value");
        this.bitmapScaleFactor = f;
        this.blurRadius = f2;
    }

    @Override // com.bumptech.glide.load.resource.bitmap.BitmapTransformation
    protected Bitmap transform(BitmapPool bitmapPool, Bitmap bitmap, int i, int i2) {
        Matrix matrix = new Matrix();
        float f = this.bitmapScaleFactor;
        matrix.setScale(f, f);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, Math.min(i, bitmap.getWidth()), Math.min(i2, bitmap.getHeight()), matrix, true);
        Allocation createFromBitmap = Allocation.createFromBitmap(this.rs, createBitmap, Allocation.MipmapControl.MIPMAP_FULL, 128);
        Allocation createTyped = Allocation.createTyped(this.rs, createFromBitmap.getType());
        RenderScript renderScript = this.rs;
        ScriptIntrinsicBlur create = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        create.setInput(createFromBitmap);
        create.setRadius(this.blurRadius);
        create.forEach(createTyped);
        createTyped.copyTo(createBitmap);
        return createBitmap;
    }

    @Override // com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(String.format(Locale.US, "blur-%f-%f", Float.valueOf(this.bitmapScaleFactor), Float.valueOf(this.blurRadius)).getBytes());
    }
}
