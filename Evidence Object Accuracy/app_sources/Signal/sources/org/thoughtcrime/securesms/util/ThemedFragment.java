package org.thoughtcrime.securesms.util;

import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ThemedFragment.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J*\u0010\r\u001a\u0004\u0018\u00010\u000e*\u00020\b2\b\b\u0001\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0007J\u0016\u0010\u0014\u001a\u00020\b*\u00020\b2\b\b\u0001\u0010\u0015\u001a\u00020\u0006H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u001e\u0010\u0007\u001a\u00020\u0006*\u00020\b8FX\u0004¢\u0006\f\u0012\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\f¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/util/ThemedFragment;", "", "()V", "THEME_RES_ID", "", "UNSET", "", "themeResId", "Landroidx/fragment/app/Fragment;", "getThemeResId$annotations", "(Landroidx/fragment/app/Fragment;)V", "getThemeResId", "(Landroidx/fragment/app/Fragment;)I", "themedInflate", "Landroid/view/View;", "layoutId", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "withTheme", "themeId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ThemedFragment {
    public static final ThemedFragment INSTANCE = new ThemedFragment();
    private static final String THEME_RES_ID;
    private static final int UNSET;

    @JvmStatic
    public static /* synthetic */ void getThemeResId$annotations(Fragment fragment) {
    }

    private ThemedFragment() {
    }

    public static final int getThemeResId(Fragment fragment) {
        Intrinsics.checkNotNullParameter(fragment, "<this>");
        Bundle arguments = fragment.getArguments();
        if (arguments != null) {
            return arguments.getInt(THEME_RES_ID);
        }
        return -1;
    }

    @JvmStatic
    public static final View themedInflate(Fragment fragment, int i, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter(fragment, "<this>");
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        if (getThemeResId(fragment) != -1) {
            return layoutInflater.cloneInContext(new ContextThemeWrapper(layoutInflater.getContext(), getThemeResId(fragment))).inflate(i, viewGroup, false);
        }
        return layoutInflater.inflate(i, viewGroup, false);
    }

    @JvmStatic
    public static final Fragment withTheme(Fragment fragment, int i) {
        Intrinsics.checkNotNullParameter(fragment, "<this>");
        Bundle arguments = fragment.getArguments();
        if (arguments == null) {
            arguments = new Bundle();
        }
        arguments.putInt(THEME_RES_ID, i);
        fragment.setArguments(arguments);
        return fragment;
    }
}
