package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Pair;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.MediaConstraints;

/* loaded from: classes4.dex */
public class BitmapUtil {
    private static final int MAX_COMPRESSION_ATTEMPTS;
    private static final int MAX_COMPRESSION_QUALITY;
    private static final int MAX_IMAGE_HALF_SCALES;
    private static final int MIN_COMPRESSION_QUALITY;
    private static final int MIN_COMPRESSION_QUALITY_DECREASE;
    private static final String TAG = Log.tag(BitmapUtil.class);

    @Deprecated
    public static <T> ScaleResult createScaledBytes(Context context, T t, MediaConstraints mediaConstraints) throws BitmapDecodingException {
        return createScaledBytes(context, t, mediaConstraints.getImageMaxWidth(context), mediaConstraints.getImageMaxHeight(context), mediaConstraints.getImageMaxSize(context));
    }

    public static <T> ScaleResult createScaledBytes(Context context, T t, int i, int i2, int i3) throws BitmapDecodingException {
        return createScaledBytes(context, t, i, i2, i3, Bitmap.CompressFormat.JPEG);
    }

    public static <T> ScaleResult createScaledBytes(Context context, T t, int i, int i2, int i3, Bitmap.CompressFormat compressFormat) throws BitmapDecodingException {
        return createScaledBytes(context, t, i, i2, i3, compressFormat, 1, 0);
    }

    private static <T> ScaleResult createScaledBytes(Context context, T t, int i, int i2, int i3, Bitmap.CompressFormat compressFormat, int i4, int i5) throws BitmapDecodingException {
        int i6;
        Throwable th;
        byte[] byteArray;
        String str;
        try {
            Bitmap bitmap = GlideApp.with(context.getApplicationContext()).asBitmap().load((Object) t).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).centerInside().submit(i, i2).get();
            if (bitmap != null) {
                String str2 = TAG;
                Locale locale = Locale.US;
                Log.i(str2, String.format(locale, "Initial scaled bitmap has size of %d bytes.", Integer.valueOf(bitmap.getByteCount())));
                Log.i(str2, String.format(locale, "Max dimensions %d x %d, %d bytes", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3)));
                int i7 = i5;
                int i8 = 90;
                int i9 = 0;
                while (true) {
                    i6 = i7 + 1;
                    try {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(compressFormat, i8, byteArrayOutputStream);
                        byteArray = byteArrayOutputStream.toByteArray();
                        String str3 = TAG;
                        Log.d(str3, "iteration with quality " + i8 + " size " + byteArray.length + " bytes.");
                        if (i8 != 45) {
                            double d = (double) i8;
                            str = str3;
                            double d2 = (double) i3;
                            double length = (double) byteArray.length;
                            Double.isNaN(d2);
                            Double.isNaN(length);
                            double sqrt = Math.sqrt(d2 / length);
                            Double.isNaN(d);
                            int floor = (int) Math.floor(d * sqrt);
                            if (i8 - floor < 5) {
                                floor = i8 - 5;
                            }
                            i8 = Math.max(floor, 45);
                            if (byteArray.length <= i3) {
                                break;
                            }
                            i9++;
                            if (i9 >= 5) {
                                break;
                            }
                            i7 = i6;
                        } else {
                            str = str3;
                            break;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                    }
                }
                if (byteArray.length > i3) {
                    if (i4 <= 3) {
                        bitmap.recycle();
                        try {
                            Log.i(str, "Halving dimensions and retrying.");
                            return createScaledBytes(context, t, i / 2, i2 / 2, i3, compressFormat, i4 + 1, i6);
                        } catch (Throwable th3) {
                            th = th3;
                            bitmap = null;
                            if (bitmap != null) {
                                bitmap.recycle();
                            }
                            throw th;
                        }
                    } else {
                        throw new BitmapDecodingException("Unable to scale image below " + byteArray.length + " bytes.");
                    }
                } else if (byteArray.length > 0) {
                    Log.i(str, String.format(Locale.US, "createScaledBytes(%s) -> quality %d, %d attempt(s) over %d sizes.", t.getClass().getName(), Integer.valueOf(i8), Integer.valueOf(i6), Integer.valueOf(i4)));
                    ScaleResult scaleResult = new ScaleResult(byteArray, bitmap.getWidth(), bitmap.getHeight());
                    bitmap.recycle();
                    return scaleResult;
                } else {
                    throw new BitmapDecodingException("Decoding failed. Bitmap has a length of " + byteArray.length + " bytes.");
                }
            } else {
                throw new BitmapDecodingException("Unable to decode image");
            }
        } catch (InterruptedException | ExecutionException e) {
            throw new BitmapDecodingException(e);
        }
    }

    public static <T> Bitmap createScaledBitmap(Context context, T t, int i, int i2) throws BitmapDecodingException {
        try {
            return GlideApp.with(context.getApplicationContext()).asBitmap().load((Object) t).centerInside().submit(i, i2).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new BitmapDecodingException(e);
        }
    }

    public static Bitmap createScaledBitmap(Bitmap bitmap, int i, int i2) {
        if (bitmap.getWidth() <= i && bitmap.getHeight() <= i2) {
            return bitmap;
        }
        if (i <= 0 || i2 <= 0) {
            return bitmap;
        }
        float width = ((float) bitmap.getWidth()) / ((float) i);
        float height = ((float) bitmap.getHeight()) / ((float) i2);
        if (width > height) {
            i2 = (int) (((float) bitmap.getHeight()) / width);
        } else {
            i = (int) (((float) bitmap.getWidth()) / height);
        }
        return Bitmap.createScaledBitmap(bitmap, i, i2, true);
    }

    public static Bitmap.CompressFormat getCompressFormatForContentType(String str) {
        if (str == null) {
            return Bitmap.CompressFormat.JPEG;
        }
        char c = 65535;
        switch (str.hashCode()) {
            case -1487394660:
                if (str.equals(MediaUtil.IMAGE_JPEG)) {
                    c = 0;
                    break;
                }
                break;
            case -1487018032:
                if (str.equals(MediaUtil.IMAGE_WEBP)) {
                    c = 1;
                    break;
                }
                break;
            case -879258763:
                if (str.equals(MediaUtil.IMAGE_PNG)) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return Bitmap.CompressFormat.JPEG;
            case 1:
                return Bitmap.CompressFormat.WEBP;
            case 2:
                return Bitmap.CompressFormat.PNG;
            default:
                return Bitmap.CompressFormat.JPEG;
        }
    }

    private static BitmapFactory.Options getImageDimensions(InputStream inputStream) throws BitmapDecodingException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        BitmapFactory.decodeStream(bufferedInputStream, null, options);
        try {
            bufferedInputStream.close();
        } catch (IOException unused) {
            Log.w(TAG, "failed to close the InputStream after reading image dimensions");
        }
        if (options.outWidth != -1 && options.outHeight != -1) {
            return options;
        }
        throw new BitmapDecodingException("Failed to decode image dimensions: " + options.outWidth + ", " + options.outHeight);
    }

    public static int getExifOrientation(ExifInterface exifInterface) {
        return exifInterface.getAttributeInt("Orientation", 0);
    }

    public static Pair<Integer, Integer> getExifDimensions(ExifInterface exifInterface) {
        int attributeInt = exifInterface.getAttributeInt("ImageWidth", 0);
        int attributeInt2 = exifInterface.getAttributeInt("ImageLength", 0);
        if (attributeInt == 0 || attributeInt2 == 0) {
            return null;
        }
        int exifOrientation = getExifOrientation(exifInterface);
        if (exifOrientation == 6 || exifOrientation == 8 || exifOrientation == 7 || exifOrientation == 5) {
            return new Pair<>(Integer.valueOf(attributeInt2), Integer.valueOf(attributeInt));
        }
        return new Pair<>(Integer.valueOf(attributeInt), Integer.valueOf(attributeInt2));
    }

    public static Pair<Integer, Integer> getDimensions(InputStream inputStream) throws BitmapDecodingException {
        BitmapFactory.Options imageDimensions = getImageDimensions(inputStream);
        return new Pair<>(Integer.valueOf(imageDimensions.outWidth), Integer.valueOf(imageDimensions.outHeight));
    }

    public static InputStream toCompressedJpeg(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, byteArrayOutputStream);
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

    public static byte[] toByteArray(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] toWebPByteArray(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (Build.VERSION.SDK_INT >= 30) {
            bitmap.compress(Bitmap.CompressFormat.WEBP_LOSSLESS, 100, byteArrayOutputStream);
        } else {
            bitmap.compress(Bitmap.CompressFormat.WEBP, 100, byteArrayOutputStream);
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static Bitmap fromByteArray(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        return BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
    }

    public static byte[] createFromNV21(byte[] bArr, int i, int i2, int i3, Rect rect, boolean z) throws IOException {
        byte[] rotateNV21 = rotateNV21(bArr, i, i2, i3, z);
        int i4 = i3 % SubsamplingScaleImageView.ORIENTATION_180;
        YuvImage yuvImage = new YuvImage(rotateNV21, 17, i4 > 0 ? i2 : i, i4 > 0 ? i : i2, null);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(rect, 80, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return byteArray;
    }

    public static byte[] rotateNV21(byte[] bArr, int i, int i2, int i3, boolean z) throws IOException {
        if (i3 == 0) {
            return bArr;
        }
        if (i3 % 90 != 0 || i3 < 0 || i3 > 270) {
            throw new IllegalArgumentException("0 <= rotation < 360, rotation % 90 == 0");
        }
        int i4 = i * i2;
        int i5 = (i4 * 3) / 2;
        if (i5 == bArr.length) {
            byte[] bArr2 = new byte[bArr.length];
            boolean z2 = i3 % SubsamplingScaleImageView.ORIENTATION_180 != 0;
            int i6 = i3 % SubsamplingScaleImageView.ORIENTATION_270;
            boolean z3 = !z ? i6 != 0 : i6 == 0;
            boolean z4 = i3 >= 180;
            for (int i7 = 0; i7 < i2; i7++) {
                for (int i8 = 0; i8 < i; i8++) {
                    int i9 = (i7 * i) + i8;
                    int i10 = ((i7 >> 1) * i) + i4 + (i8 & -2);
                    int i11 = i10 + 1;
                    int i12 = z2 ? i2 : i;
                    int i13 = z2 ? i : i2;
                    int i14 = z2 ? i7 : i8;
                    int i15 = z2 ? i8 : i7;
                    if (z3) {
                        i14 = (i12 - i14) - 1;
                    }
                    if (z4) {
                        i15 = (i13 - i15) - 1;
                    }
                    int i16 = i4 + ((i15 >> 1) * i12) + (i14 & -2);
                    bArr2[(i15 * i12) + i14] = (byte) (bArr[i9] & 255);
                    bArr2[i16] = (byte) (bArr[i10] & 255);
                    bArr2[i16 + 1] = (byte) (bArr[i11] & 255);
                }
            }
            return bArr2;
        }
        throw new IOException("provided width and height don't jive with the data length (" + bArr.length + "). Width: " + i + " height: " + i2 + " = data length: " + i5);
    }

    public static Bitmap createFromDrawable(final Drawable drawable, final int i, final int i2) {
        Bitmap bitmap;
        final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        final Bitmap[] bitmapArr = new Bitmap[1];
        ThreadUtil.runOnMain(new Runnable() { // from class: org.thoughtcrime.securesms.util.BitmapUtil.1
            @Override // java.lang.Runnable
            public void run() {
                Bitmap bitmap2;
                Drawable drawable2 = drawable;
                if (drawable2 instanceof BitmapDrawable) {
                    bitmapArr[0] = ((BitmapDrawable) drawable2).getBitmap();
                } else {
                    int intrinsicWidth = drawable2.getIntrinsicWidth();
                    if (intrinsicWidth <= 0) {
                        intrinsicWidth = i;
                    }
                    int intrinsicHeight = drawable.getIntrinsicHeight();
                    if (intrinsicHeight <= 0) {
                        intrinsicHeight = i2;
                    }
                    try {
                        bitmap2 = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888);
                        Canvas canvas = new Canvas(bitmap2);
                        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                        drawable.draw(canvas);
                    } catch (Exception e) {
                        Log.w(BitmapUtil.TAG, e);
                        bitmap2 = null;
                    }
                    bitmapArr[0] = bitmap2;
                }
                synchronized (bitmapArr) {
                    atomicBoolean.set(true);
                    bitmapArr.notifyAll();
                }
            }
        });
        synchronized (bitmapArr) {
            while (!atomicBoolean.get()) {
                Util.wait(bitmapArr, 0);
            }
            bitmap = bitmapArr[0];
        }
        return bitmap;
    }

    public static int getMaxTextureSize() {
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        egl10.eglInitialize(eglGetDisplay, new int[2]);
        int[] iArr = new int[1];
        egl10.eglGetConfigs(eglGetDisplay, null, 0, iArr);
        int i = iArr[0];
        EGLConfig[] eGLConfigArr = new EGLConfig[i];
        egl10.eglGetConfigs(eglGetDisplay, eGLConfigArr, i, iArr);
        int[] iArr2 = new int[1];
        int i2 = 0;
        for (int i3 = 0; i3 < iArr[0]; i3++) {
            egl10.eglGetConfigAttrib(eglGetDisplay, eGLConfigArr[i3], 12332, iArr2);
            int i4 = iArr2[0];
            if (i2 < i4) {
                i2 = i4;
            }
        }
        egl10.eglTerminate(eglGetDisplay);
        return Math.min(i2, (int) RecyclerView.ItemAnimator.FLAG_MOVED);
    }

    /* loaded from: classes4.dex */
    public static class ScaleResult {
        private final byte[] bitmap;
        private final int height;
        private final int width;

        public ScaleResult(byte[] bArr, int i, int i2) {
            this.bitmap = bArr;
            this.width = i;
            this.height = i2;
        }

        public byte[] getBitmap() {
            return this.bitmap;
        }

        public int getWidth() {
            return this.width;
        }

        public int getHeight() {
            return this.height;
        }
    }
}
