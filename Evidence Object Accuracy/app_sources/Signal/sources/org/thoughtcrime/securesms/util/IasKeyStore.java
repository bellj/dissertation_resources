package org.thoughtcrime.securesms.util;

import android.content.Context;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import org.thoughtcrime.securesms.push.IasTrustStore;

/* loaded from: classes4.dex */
public final class IasKeyStore {
    private IasKeyStore() {
    }

    public static KeyStore getIasKeyStore(Context context) {
        try {
            IasTrustStore iasTrustStore = new IasTrustStore(context);
            KeyStore instance = KeyStore.getInstance("BKS");
            instance.load(iasTrustStore.getKeyStoreInputStream(), iasTrustStore.getKeyStorePassword().toCharArray());
            return instance;
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            throw new AssertionError(e);
        }
    }
}
