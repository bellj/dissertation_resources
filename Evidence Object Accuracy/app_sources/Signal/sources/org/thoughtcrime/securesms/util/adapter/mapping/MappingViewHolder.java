package org.thoughtcrime.securesms.util.adapter.mapping;

import android.content.Context;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes4.dex */
public abstract class MappingViewHolder<Model> extends RecyclerView.ViewHolder {
    protected final Context context;
    protected final List<Object> payload = new LinkedList();

    public abstract void bind(Model model);

    public void onAttachedToWindow() {
    }

    public void onDetachedFromWindow() {
    }

    public MappingViewHolder(View view) {
        super(view);
        this.context = view.getContext();
    }

    public final <T extends View> T findViewById(int i) {
        return (T) this.itemView.findViewById(i);
    }

    public Context getContext() {
        return this.itemView.getContext();
    }

    public void setPayload(List<Object> list) {
        this.payload.clear();
        this.payload.addAll(list);
    }

    /* loaded from: classes4.dex */
    public static final class SimpleViewHolder<Model> extends MappingViewHolder<Model> {
        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public void bind(Model model) {
        }

        public SimpleViewHolder(View view) {
            super(view);
        }
    }
}
