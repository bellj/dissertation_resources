package org.thoughtcrime.securesms.util;

import org.thoughtcrime.securesms.util.FeatureFlags;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class FeatureFlags$2$$ExternalSyntheticLambda2 implements FeatureFlags.OnFlagChange {
    @Override // org.thoughtcrime.securesms.util.FeatureFlags.OnFlagChange
    public final void onFlagChange(FeatureFlags.Change change) {
        FeatureFlags.AnonymousClass2.lambda$new$2(change);
    }
}
