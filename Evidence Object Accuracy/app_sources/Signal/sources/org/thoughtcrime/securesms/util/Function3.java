package org.thoughtcrime.securesms.util;

/* loaded from: classes4.dex */
public interface Function3<A, B, C, D> {
    D apply(A a, B b, C c);
}
