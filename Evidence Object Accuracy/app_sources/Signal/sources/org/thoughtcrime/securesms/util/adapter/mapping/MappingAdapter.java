package org.thoughtcrime.securesms.util.adapter.mapping;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import j$.util.Optional;
import j$.util.function.Function;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import org.thoughtcrime.securesms.util.NoCrossfadeChangeDefaultAnimator;

/* loaded from: classes4.dex */
public class MappingAdapter extends ListAdapter<MappingModel<?>, MappingViewHolder<?>> {
    final Map<Integer, Factory<?>> factories = new HashMap();
    final Map<Class<?>, Integer> itemTypes = new HashMap();
    int typeCount = 0;

    public /* bridge */ /* synthetic */ void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i, List list) {
        onBindViewHolder((MappingViewHolder) viewHolder, i, (List<Object>) list);
    }

    public MappingAdapter() {
        super(new MappingDiffCallback());
    }

    public void onViewAttachedToWindow(MappingViewHolder<?> mappingViewHolder) {
        super.onViewAttachedToWindow((MappingAdapter) mappingViewHolder);
        mappingViewHolder.onAttachedToWindow();
    }

    public void onViewDetachedFromWindow(MappingViewHolder<?> mappingViewHolder) {
        super.onViewDetachedFromWindow((MappingAdapter) mappingViewHolder);
        mappingViewHolder.onDetachedFromWindow();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (recyclerView.getItemAnimator() != null && recyclerView.getItemAnimator().getClass() == DefaultItemAnimator.class) {
            recyclerView.setItemAnimator(new NoCrossfadeChangeDefaultAnimator());
        }
    }

    public <T extends MappingModel<T>> void registerFactory(Class<T> cls, Factory<T> factory) {
        int i = this.typeCount;
        this.typeCount = i + 1;
        this.factories.put(Integer.valueOf(i), factory);
        this.itemTypes.put(cls, Integer.valueOf(i));
    }

    public <T extends MappingModel<T>> void registerFactory(Class<T> cls, Function<View, MappingViewHolder<T>> function, int i) {
        registerFactory(cls, new LayoutFactory(function, i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        Integer num = this.itemTypes.get(getItem(i).getClass());
        if (num != null) {
            return num.intValue();
        }
        throw new AssertionError("No view holder factory for type: " + getItem(i).getClass());
    }

    public MappingViewHolder<?> onCreateViewHolder(ViewGroup viewGroup, int i) {
        Factory<?> factory = this.factories.get(Integer.valueOf(i));
        Objects.requireNonNull(factory);
        return factory.createViewHolder(viewGroup);
    }

    public void onBindViewHolder(MappingViewHolder<?> mappingViewHolder, int i, List<Object> list) {
        mappingViewHolder.setPayload(list);
        onBindViewHolder((MappingViewHolder) mappingViewHolder, i);
    }

    public void onBindViewHolder(MappingViewHolder mappingViewHolder, int i) {
        mappingViewHolder.bind(getItem(i));
    }

    public <T extends MappingModel<T>> int indexOfFirst(Class<T> cls, Function1<T, Boolean> function1) {
        return CollectionsKt___CollectionsKt.indexOfFirst(getCurrentList(), new Function1(cls, function1) { // from class: org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter$$ExternalSyntheticLambda0
            public final /* synthetic */ Class f$0;
            public final /* synthetic */ Function1 f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // kotlin.jvm.functions.Function1
            public final Object invoke(Object obj) {
                return MappingAdapter.lambda$indexOfFirst$0(this.f$0, this.f$1, (MappingModel) obj);
            }
        });
    }

    public static /* synthetic */ Boolean lambda$indexOfFirst$0(Class cls, Function1 function1, MappingModel mappingModel) {
        return Boolean.valueOf(cls.isAssignableFrom(mappingModel.getClass()) && ((Boolean) function1.invoke(mappingModel)).booleanValue());
    }

    public Optional<MappingModel<?>> getModel(int i) {
        List<MappingModel<?>> currentList = getCurrentList();
        if (i < 0 || i >= currentList.size()) {
            return Optional.empty();
        }
        return Optional.ofNullable(currentList.get(i));
    }
}
