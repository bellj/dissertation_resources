package org.thoughtcrime.securesms.util.livedata;

import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LiveDataUtil$$ExternalSyntheticLambda1 implements Observer {
    public final /* synthetic */ MediatorLiveData f$0;

    public /* synthetic */ LiveDataUtil$$ExternalSyntheticLambda1(MediatorLiveData mediatorLiveData) {
        this.f$0 = mediatorLiveData;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.setValue(obj);
    }
}
