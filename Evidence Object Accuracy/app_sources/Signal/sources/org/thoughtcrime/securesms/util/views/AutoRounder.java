package org.thoughtcrime.securesms.util.views;

import android.view.View;
import android.view.ViewTreeObserver;
import androidx.core.util.Consumer;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: AutoRounder.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u0000 \f*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003:\u0001\fB\u001d\b\u0002\u0012\u0006\u0010\u0004\u001a\u00028\u0000\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\b\u0010\n\u001a\u00020\u000bH\u0016R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00028\u0000X\u0004¢\u0006\u0004\n\u0002\u0010\t¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/util/views/AutoRounder;", "T", "Landroid/view/View;", "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;", "view", "setRadius", "Landroidx/core/util/Consumer;", "", "(Landroid/view/View;Landroidx/core/util/Consumer;)V", "Landroid/view/View;", "onGlobalLayout", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AutoRounder<T extends View> implements ViewTreeObserver.OnGlobalLayoutListener {
    public static final Companion Companion = new Companion(null);
    private final Consumer<Float> setRadius;
    private final T view;

    public /* synthetic */ AutoRounder(View view, Consumer consumer, DefaultConstructorMarker defaultConstructorMarker) {
        this(view, consumer);
    }

    @JvmStatic
    public static final <VIEW extends View> void autoSetCorners(VIEW view, Consumer<Float> consumer) {
        Companion.autoSetCorners(view, consumer);
    }

    private AutoRounder(T t, Consumer<Float> consumer) {
        this.view = t;
        this.setRadius = consumer;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        if (this.view.getHeight() > 0) {
            this.setRadius.accept(Float.valueOf(((float) this.view.getHeight()) / 2.0f));
            this.view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    /* compiled from: AutoRounder.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J-\u0010\u0003\u001a\u00020\u0004\"\b\b\u0001\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u0002H\u00052\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0007¢\u0006\u0002\u0010\u000b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/util/views/AutoRounder$Companion;", "", "()V", "autoSetCorners", "", "VIEW", "Landroid/view/View;", "view", "setRadius", "Landroidx/core/util/Consumer;", "", "(Landroid/view/View;Landroidx/core/util/Consumer;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final <VIEW extends View> void autoSetCorners(VIEW view, Consumer<Float> consumer) {
            Intrinsics.checkNotNullParameter(view, "view");
            Intrinsics.checkNotNullParameter(consumer, "setRadius");
            view.getViewTreeObserver().addOnGlobalLayoutListener(new AutoRounder(view, consumer, null));
        }
    }
}
