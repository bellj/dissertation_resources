package org.thoughtcrime.securesms.util;

import java.util.Calendar;

/* loaded from: classes4.dex */
public final class CalendarDateOnly {
    public static Calendar getInstance() {
        Calendar instance = Calendar.getInstance();
        removeTime(instance);
        return instance;
    }

    public static void removeTime(Calendar calendar) {
        calendar.set(11, calendar.getActualMinimum(11));
        calendar.set(12, calendar.getActualMinimum(12));
        calendar.set(13, calendar.getActualMinimum(13));
        calendar.set(14, calendar.getActualMinimum(14));
    }
}
