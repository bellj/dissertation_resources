package org.thoughtcrime.securesms.util;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: CustomDrawWrapper.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a$\u0010\u0005\u001a\u00020\u0000*\u00020\u00002\u0018\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001¨\u0006\u0006"}, d2 = {"Landroid/graphics/drawable/Drawable;", "Lkotlin/Function2;", "Landroid/graphics/Canvas;", "", "customDrawFn", "customizeOnDraw", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class CustomDrawWrapperKt {
    public static final Drawable customizeOnDraw(Drawable drawable, Function2<? super Drawable, ? super Canvas, Unit> function2) {
        Intrinsics.checkNotNullParameter(drawable, "<this>");
        Intrinsics.checkNotNullParameter(function2, "customDrawFn");
        return new CustomDrawWrapper(drawable, function2);
    }
}
