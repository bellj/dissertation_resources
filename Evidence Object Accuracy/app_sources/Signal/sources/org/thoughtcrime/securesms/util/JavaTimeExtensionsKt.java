package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.os.Build;
import android.text.format.DateFormat;
import j$.time.DayOfWeek;
import j$.time.Instant;
import j$.time.LocalDateTime;
import j$.time.LocalTime;
import j$.time.OffsetDateTime;
import j$.time.ZoneId;
import j$.time.ZoneOffset;
import j$.time.format.DateTimeFormatter;
import j$.time.format.FormatStyle;
import j$.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* compiled from: JavaTimeExtensions.kt */
@Metadata(bv = {}, d1 = {"\u00008\n\u0000\n\u0002\b\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\u001a\n\u0010\u0002\u001a\u00020\u0001*\u00020\u0000\u001a\u0014\u0010\u0006\u001a\u00020\u0005*\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0001\u001a\u001a\u0010\n\u001a\u00020\t*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0003\u001a\u0014\u0010\f\u001a\u00020\u0003*\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0000\u001a\u0014\u0010\f\u001a\u00020\u0003*\u00020\r2\b\b\u0002\u0010\u000b\u001a\u00020\u0000\u001a\u0014\u0010\u000f\u001a\u00020\u000e*\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0000\u001a\u0012\u0010\u0013\u001a\u00020\u0012*\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0010\u001a\u0010\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015*\u00020\u0014¨\u0006\u0018"}, d2 = {"j$/time/ZoneId", "j$/time/ZoneOffset", "toOffset", "j$/time/LocalDateTime", "zoneOffset", "", "toMillis", NotificationProfileDatabase.NotificationProfileScheduleTable.START, NotificationProfileDatabase.NotificationProfileScheduleTable.END, "", "isBetween", "zoneId", "toLocalDateTime", "j$/time/Instant", "j$/time/LocalTime", "toLocalTime", "Landroid/content/Context;", "context", "", "formatHours", "Ljava/util/Locale;", "", "j$/time/DayOfWeek", "orderOfDaysInWeek", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class JavaTimeExtensionsKt {
    public static final ZoneOffset toOffset(ZoneId zoneId) {
        Intrinsics.checkNotNullParameter(zoneId, "<this>");
        ZoneOffset offset = OffsetDateTime.now(zoneId).getOffset();
        Intrinsics.checkNotNullExpressionValue(offset, "now(this).offset");
        return offset;
    }

    public static /* synthetic */ long toMillis$default(LocalDateTime localDateTime, ZoneOffset zoneOffset, int i, Object obj) {
        if ((i & 1) != 0) {
            ZoneId systemDefault = ZoneId.systemDefault();
            Intrinsics.checkNotNullExpressionValue(systemDefault, "systemDefault()");
            zoneOffset = toOffset(systemDefault);
        }
        return toMillis(localDateTime, zoneOffset);
    }

    public static final long toMillis(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
        Intrinsics.checkNotNullParameter(localDateTime, "<this>");
        Intrinsics.checkNotNullParameter(zoneOffset, "zoneOffset");
        return TimeUnit.SECONDS.toMillis(localDateTime.toEpochSecond(zoneOffset));
    }

    public static final boolean isBetween(LocalDateTime localDateTime, LocalDateTime localDateTime2, LocalDateTime localDateTime3) {
        Intrinsics.checkNotNullParameter(localDateTime, "<this>");
        Intrinsics.checkNotNullParameter(localDateTime2, NotificationProfileDatabase.NotificationProfileScheduleTable.START);
        Intrinsics.checkNotNullParameter(localDateTime3, NotificationProfileDatabase.NotificationProfileScheduleTable.END);
        return (localDateTime.isEqual(localDateTime2) || localDateTime.isAfter(localDateTime2)) && (localDateTime.isEqual(localDateTime3) || localDateTime.isBefore(localDateTime3));
    }

    public static /* synthetic */ LocalDateTime toLocalDateTime$default(long j, ZoneId zoneId, int i, Object obj) {
        if ((i & 1) != 0) {
            zoneId = ZoneId.systemDefault();
            Intrinsics.checkNotNullExpressionValue(zoneId, "systemDefault()");
        }
        return toLocalDateTime(j, zoneId);
    }

    public static final LocalDateTime toLocalDateTime(long j, ZoneId zoneId) {
        Intrinsics.checkNotNullParameter(zoneId, "zoneId");
        LocalDateTime ofInstant = LocalDateTime.ofInstant(Instant.ofEpochMilli(j), zoneId);
        Intrinsics.checkNotNullExpressionValue(ofInstant, "ofInstant(Instant.ofEpochMilli(this), zoneId)");
        return ofInstant;
    }

    public static /* synthetic */ LocalDateTime toLocalDateTime$default(Instant instant, ZoneId zoneId, int i, Object obj) {
        if ((i & 1) != 0) {
            zoneId = ZoneId.systemDefault();
            Intrinsics.checkNotNullExpressionValue(zoneId, "systemDefault()");
        }
        return toLocalDateTime(instant, zoneId);
    }

    public static final LocalDateTime toLocalDateTime(Instant instant, ZoneId zoneId) {
        Intrinsics.checkNotNullParameter(instant, "<this>");
        Intrinsics.checkNotNullParameter(zoneId, "zoneId");
        LocalDateTime ofInstant = LocalDateTime.ofInstant(instant, zoneId);
        Intrinsics.checkNotNullExpressionValue(ofInstant, "ofInstant(this, zoneId)");
        return ofInstant;
    }

    public static /* synthetic */ LocalTime toLocalTime$default(long j, ZoneId zoneId, int i, Object obj) {
        if ((i & 1) != 0) {
            zoneId = ZoneId.systemDefault();
            Intrinsics.checkNotNullExpressionValue(zoneId, "systemDefault()");
        }
        return toLocalTime(j, zoneId);
    }

    public static final LocalTime toLocalTime(long j, ZoneId zoneId) {
        Intrinsics.checkNotNullParameter(zoneId, "zoneId");
        LocalTime localTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(j), zoneId).toLocalTime();
        Intrinsics.checkNotNullExpressionValue(localTime, "ofInstant(Instant.ofEpoc…s), zoneId).toLocalTime()");
        return localTime;
    }

    public static final String formatHours(LocalTime localTime, Context context) {
        Intrinsics.checkNotNullParameter(localTime, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        if (Build.VERSION.SDK_INT >= 26 || !DateFormat.is24HourFormat(context)) {
            String format = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(localTime);
            Intrinsics.checkNotNullExpressionValue(format, "{\n    DateTimeFormatter.…e.SHORT).format(this)\n  }");
            return format;
        }
        String format2 = DateTimeFormatter.ofPattern("HH:mm", Locale.getDefault()).format(localTime);
        Intrinsics.checkNotNullExpressionValue(format2, "{\n    DateTimeFormatter.…fault()).format(this)\n  }");
        return format2;
    }

    public static final List<DayOfWeek> orderOfDaysInWeek(Locale locale) {
        Intrinsics.checkNotNullParameter(locale, "<this>");
        DayOfWeek firstDayOfWeek = WeekFields.of(locale).getFirstDayOfWeek();
        Intrinsics.checkNotNullExpressionValue(firstDayOfWeek, "of(this).firstDayOfWeek");
        DayOfWeek plus = firstDayOfWeek.plus(1);
        Intrinsics.checkNotNullExpressionValue(plus, "firstDayOfWeek.plus(1)");
        DayOfWeek plus2 = firstDayOfWeek.plus(2);
        Intrinsics.checkNotNullExpressionValue(plus2, "firstDayOfWeek.plus(2)");
        DayOfWeek plus3 = firstDayOfWeek.plus(3);
        Intrinsics.checkNotNullExpressionValue(plus3, "firstDayOfWeek.plus(3)");
        DayOfWeek plus4 = firstDayOfWeek.plus(4);
        Intrinsics.checkNotNullExpressionValue(plus4, "firstDayOfWeek.plus(4)");
        DayOfWeek plus5 = firstDayOfWeek.plus(5);
        Intrinsics.checkNotNullExpressionValue(plus5, "firstDayOfWeek.plus(5)");
        DayOfWeek plus6 = firstDayOfWeek.plus(6);
        Intrinsics.checkNotNullExpressionValue(plus6, "firstDayOfWeek.plus(6)");
        return CollectionsKt__CollectionsKt.listOf((Object[]) new DayOfWeek[]{firstDayOfWeek, plus, plus2, plus3, plus4, plus5, plus6});
    }
}
