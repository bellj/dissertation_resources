package org.thoughtcrime.securesms.util;

import androidx.core.os.LocaleListCompat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.dynamiclanguage.LanguageString;

/* compiled from: LocaleUtil.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/util/LocaleUtil;", "", "()V", "getFirstLocale", "Ljava/util/Locale;", "getLocaleDefaults", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LocaleUtil {
    public static final LocaleUtil INSTANCE = new LocaleUtil();

    private LocaleUtil() {
    }

    public final Locale getFirstLocale() {
        Locale locale = (Locale) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) getLocaleDefaults()));
        if (locale != null) {
            return locale;
        }
        Locale locale2 = Locale.getDefault();
        Intrinsics.checkNotNullExpressionValue(locale2, "getDefault()");
        return locale2;
    }

    public final List<Locale> getLocaleDefaults() {
        ArrayList arrayList = new ArrayList();
        Locale parseLocale = LanguageString.parseLocale(SignalStore.settings().getLanguage());
        LocaleListCompat localeListCompat = LocaleListCompat.getDefault();
        Intrinsics.checkNotNullExpressionValue(localeListCompat, "getDefault()");
        if (parseLocale != null) {
            arrayList.add(parseLocale);
        }
        int size = localeListCompat.size();
        for (int i = 0; i < size; i++) {
            arrayList.add(localeListCompat.get(i));
        }
        return arrayList;
    }
}
