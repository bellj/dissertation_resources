package org.thoughtcrime.securesms.util.rx;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.processors.BehaviorProcessor;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: RxStore.kt */
@Metadata(bv = {}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000*\b\b\u0000\u0010\u0002*\u00020\u00012\u00020\u0001B\u0019\u0012\u0006\u0010\u001d\u001a\u00028\u0000\u0012\b\b\u0002\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u001e\u0010\u001fJ\u001a\u0010\u0006\u001a\u00020\u00052\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00000\u0003J4\u0010\u0006\u001a\u00020\u000b\"\u0004\b\u0001\u0010\u00072\f\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00010\b2\u0018\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00000\nR\u0014\u0010\r\u001a\u00020\f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR8\u0010\u0011\u001a&\u0012\f\u0012\n \u0010*\u0004\u0018\u00018\u00008\u0000 \u0010*\u0012\u0012\f\u0012\n \u0010*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\u000f0\u000f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012Rh\u0010\u0014\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0000 \u0010*\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00030\u0003 \u0010**\u0012$\u0012\"\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0000 \u0010*\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00030\u0003\u0018\u00010\u00130\u00138\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001d\u0010\u0016\u001a\b\u0012\u0004\u0012\u00028\u00000\b8\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u001c\u001a\u00028\u00008F¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001b¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/util/rx/RxStore;", "", "T", "Lkotlin/Function1;", "transformer", "", "update", "U", "Lio/reactivex/rxjava3/core/Flowable;", "flowable", "Lkotlin/Function2;", "Lio/reactivex/rxjava3/disposables/Disposable;", "Lio/reactivex/rxjava3/core/Scheduler;", "scheduler", "Lio/reactivex/rxjava3/core/Scheduler;", "Lio/reactivex/rxjava3/processors/BehaviorProcessor;", "kotlin.jvm.PlatformType", "behaviorProcessor", "Lio/reactivex/rxjava3/processors/BehaviorProcessor;", "Lio/reactivex/rxjava3/subjects/Subject;", "actionSubject", "Lio/reactivex/rxjava3/subjects/Subject;", "stateFlowable", "Lio/reactivex/rxjava3/core/Flowable;", "getStateFlowable", "()Lio/reactivex/rxjava3/core/Flowable;", "getState", "()Ljava/lang/Object;", "state", "defaultValue", "<init>", "(Ljava/lang/Object;Lio/reactivex/rxjava3/core/Scheduler;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class RxStore<T> {
    private final Subject<Function1<T, T>> actionSubject;
    private final BehaviorProcessor<T> behaviorProcessor;
    private final Scheduler scheduler;
    private final Flowable<T> stateFlowable;

    public RxStore(T t, Scheduler scheduler) {
        Intrinsics.checkNotNullParameter(t, "defaultValue");
        Intrinsics.checkNotNullParameter(scheduler, "scheduler");
        this.scheduler = scheduler;
        BehaviorProcessor<T> createDefault = BehaviorProcessor.createDefault(t);
        this.behaviorProcessor = createDefault;
        Subject subject = (Subject<T>) PublishSubject.create().toSerialized();
        this.actionSubject = subject;
        Flowable<T> onBackpressureLatest = createDefault.onBackpressureLatest();
        Intrinsics.checkNotNullExpressionValue(onBackpressureLatest, "behaviorProcessor.onBackpressureLatest()");
        this.stateFlowable = onBackpressureLatest;
        subject.observeOn(scheduler).scan(t, new BiFunction() { // from class: org.thoughtcrime.securesms.util.rx.RxStore$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return RxStore.m3288_init_$lambda0(obj, (Function1) obj2);
            }
        }).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.util.rx.RxStore$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                RxStore.m3289_init_$lambda1(RxStore.this, obj);
            }
        });
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ RxStore(java.lang.Object r1, io.reactivex.rxjava3.core.Scheduler r2, int r3, kotlin.jvm.internal.DefaultConstructorMarker r4) {
        /*
            r0 = this;
            r3 = r3 & 2
            if (r3 == 0) goto L_0x000d
            io.reactivex.rxjava3.core.Scheduler r2 = io.reactivex.rxjava3.schedulers.Schedulers.computation()
            java.lang.String r3 = "computation()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r3)
        L_0x000d:
            r0.<init>(r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.util.rx.RxStore.<init>(java.lang.Object, io.reactivex.rxjava3.core.Scheduler, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final T getState() {
        T value = this.behaviorProcessor.getValue();
        Intrinsics.checkNotNull(value);
        return value;
    }

    public final Flowable<T> getStateFlowable() {
        return this.stateFlowable;
    }

    /* renamed from: _init_$lambda-0 */
    public static final Object m3288_init_$lambda0(Object obj, Function1 function1) {
        Intrinsics.checkNotNullExpressionValue(obj, "v");
        return function1.invoke(obj);
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m3289_init_$lambda1(RxStore rxStore, Object obj) {
        Intrinsics.checkNotNullParameter(rxStore, "this$0");
        rxStore.behaviorProcessor.onNext(obj);
    }

    public final void update(Function1<? super T, ? extends T> function1) {
        Intrinsics.checkNotNullParameter(function1, "transformer");
        this.actionSubject.onNext(function1);
    }

    public final <U> Disposable update(Flowable<U> flowable, Function2<? super U, ? super T, ? extends T> function2) {
        Intrinsics.checkNotNullParameter(flowable, "flowable");
        Intrinsics.checkNotNullParameter(function2, "transformer");
        Disposable subscribe = flowable.subscribe(new Consumer(function2) { // from class: org.thoughtcrime.securesms.util.rx.RxStore$$ExternalSyntheticLambda2
            public final /* synthetic */ Function2 f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                RxStore.m3290update$lambda2(RxStore.this, this.f$1, obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "flowable.subscribe {\n   …ransformer(it, t) }\n    }");
        return subscribe;
    }

    /* renamed from: update$lambda-2 */
    public static final void m3290update$lambda2(RxStore rxStore, Function2 function2, Object obj) {
        Intrinsics.checkNotNullParameter(rxStore, "this$0");
        Intrinsics.checkNotNullParameter(function2, "$transformer");
        rxStore.actionSubject.onNext(new Function1<T, T>(function2, obj) { // from class: org.thoughtcrime.securesms.util.rx.RxStore$update$1$1
            final /* synthetic */ Object $it;
            final /* synthetic */ Function2 $transformer;

            /* access modifiers changed from: package-private */
            {
                this.$transformer = r1;
                this.$it = r2;
            }

            /* JADX WARN: Type inference failed for: r3v1, types: [T, java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public final T invoke(T t) {
                Intrinsics.checkNotNullParameter(t, "t");
                return this.$transformer.invoke(this.$it, t);
            }
        });
    }
}
