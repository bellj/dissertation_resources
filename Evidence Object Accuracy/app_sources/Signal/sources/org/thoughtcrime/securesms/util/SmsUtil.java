package org.thoughtcrime.securesms.util;

import android.app.role.RoleManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.content.ContextCompat;

/* loaded from: classes4.dex */
public final class SmsUtil {
    private SmsUtil() {
    }

    public static Intent getSmsRoleIntent(Context context) {
        if (Build.VERSION.SDK_INT >= 29) {
            return ((RoleManager) ContextCompat.getSystemService(context, RoleManager.class)).createRequestRoleIntent("android.app.role.SMS");
        }
        Intent intent = new Intent("android.provider.Telephony.ACTION_CHANGE_DEFAULT");
        intent.putExtra("package", context.getPackageName());
        return intent;
    }
}
