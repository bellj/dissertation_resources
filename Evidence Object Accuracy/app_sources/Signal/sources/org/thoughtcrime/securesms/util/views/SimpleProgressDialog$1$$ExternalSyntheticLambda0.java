package org.thoughtcrime.securesms.util.views;

import java.util.concurrent.atomic.AtomicReference;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SimpleProgressDialog$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ AtomicReference f$0;

    public /* synthetic */ SimpleProgressDialog$1$$ExternalSyntheticLambda0(AtomicReference atomicReference) {
        this.f$0 = atomicReference;
    }

    @Override // java.lang.Runnable
    public final void run() {
        SimpleProgressDialog.AnonymousClass1.lambda$dismissNow$1(this.f$0);
    }
}
