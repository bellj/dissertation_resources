package org.thoughtcrime.securesms.util;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.SnapToTopDataObserver;

/* loaded from: classes4.dex */
public class SnapToTopDataObserver extends RecyclerView.AdapterDataObserver {
    private static final String TAG = Log.tag(SnapToTopDataObserver.class);
    private final Deferred deferred;
    private final LinearLayoutManager layoutManager;
    private final RecyclerView recyclerView;
    private final ScrollRequestValidator scrollRequestValidator;
    private final ScrollToTop scrollToTop;

    /* loaded from: classes4.dex */
    public interface OnPerformScroll {
        void onPerformScroll(LinearLayoutManager linearLayoutManager, int i);
    }

    /* loaded from: classes4.dex */
    public interface ScrollRequestValidator {
        boolean isItemAtPositionLoaded(int i);

        boolean isPositionStillValid(int i);
    }

    /* loaded from: classes4.dex */
    public interface ScrollToTop {
        void scrollToTop();
    }

    public SnapToTopDataObserver(RecyclerView recyclerView) {
        this(recyclerView, null, null);
    }

    public SnapToTopDataObserver(RecyclerView recyclerView, ScrollRequestValidator scrollRequestValidator, ScrollToTop scrollToTop) {
        this.recyclerView = recyclerView;
        this.layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        this.deferred = new Deferred();
        this.scrollRequestValidator = scrollRequestValidator;
        this.scrollToTop = scrollToTop == null ? new ScrollToTop() { // from class: org.thoughtcrime.securesms.util.SnapToTopDataObserver$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.SnapToTopDataObserver.ScrollToTop
            public final void scrollToTop() {
                SnapToTopDataObserver.this.lambda$new$0();
            }
        } : scrollToTop;
    }

    public /* synthetic */ void lambda$new$0() {
        this.layoutManager.scrollToPosition(0);
    }

    public void requestScrollPosition(int i) {
        buildScrollPosition(i).submit();
    }

    public ScrollRequestBuilder buildScrollPosition(int i) {
        return new ScrollRequestBuilder(i);
    }

    public void requestScrollPositionInternal(int i, OnPerformScroll onPerformScroll, Runnable runnable, Runnable runnable2) {
        Objects.requireNonNull(this.scrollRequestValidator, "Cannot request positions when SnapToTopObserver was initialized without a validator.");
        if (!this.scrollRequestValidator.isPositionStillValid(i)) {
            String str = TAG;
            Log.d(str, "requestScrollPositionInternal(" + i + ") Invalid");
            runnable2.run();
        } else if (this.scrollRequestValidator.isItemAtPositionLoaded(i)) {
            String str2 = TAG;
            Log.d(str2, "requestScrollPositionInternal(" + i + ") Scrolling");
            onPerformScroll.onPerformScroll(this.layoutManager, i);
            runnable.run();
        } else {
            String str3 = TAG;
            Log.d(str3, "requestScrollPositionInternal(" + i + ") Deferring");
            this.deferred.setDeferred(true);
            this.deferred.defer(new Runnable(i, onPerformScroll, runnable, runnable2) { // from class: org.thoughtcrime.securesms.util.SnapToTopDataObserver$$ExternalSyntheticLambda1
                public final /* synthetic */ int f$1;
                public final /* synthetic */ SnapToTopDataObserver.OnPerformScroll f$2;
                public final /* synthetic */ Runnable f$3;
                public final /* synthetic */ Runnable f$4;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SnapToTopDataObserver.this.lambda$requestScrollPositionInternal$1(this.f$1, this.f$2, this.f$3, this.f$4);
                }
            });
        }
    }

    public /* synthetic */ void lambda$requestScrollPositionInternal$1(int i, OnPerformScroll onPerformScroll, Runnable runnable, Runnable runnable2) {
        String str = TAG;
        Log.d(str, "requestScrollPositionInternal(" + i + ") Executing deferred");
        requestScrollPositionInternal(i, onPerformScroll, runnable, runnable2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
    public void onItemRangeMoved(int i, int i2, int i3) {
        snapToTopIfNecessary(i2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
    public void onItemRangeInserted(int i, int i2) {
        snapToTopIfNecessary(i);
    }

    private void snapToTopIfNecessary(int i) {
        if (this.deferred.isDeferred()) {
            this.deferred.setDeferred(false);
        } else if (i == 0 && this.recyclerView.getScrollState() == 0) {
            if (!this.recyclerView.canScrollVertically(this.layoutManager.getReverseLayout() ? 1 : -1) && this.layoutManager.findFirstVisibleItemPosition() == 0) {
                Log.d(TAG, "Scrolling to top.");
                this.scrollToTop.scrollToTop();
            }
        }
    }

    /* loaded from: classes4.dex */
    public final class ScrollRequestBuilder {
        private Runnable onInvalidPosition = new SnapToTopDataObserver$ScrollRequestBuilder$$ExternalSyntheticLambda2();
        private OnPerformScroll onPerformScroll = new SnapToTopDataObserver$ScrollRequestBuilder$$ExternalSyntheticLambda0();
        private Runnable onScrollRequestComplete = new SnapToTopDataObserver$ScrollRequestBuilder$$ExternalSyntheticLambda1();
        private final int position;

        public static /* synthetic */ void lambda$new$0() {
        }

        public static /* synthetic */ void lambda$new$1() {
        }

        public ScrollRequestBuilder(int i) {
            SnapToTopDataObserver.this = r1;
            this.position = i;
        }

        public ScrollRequestBuilder withOnPerformScroll(OnPerformScroll onPerformScroll) {
            this.onPerformScroll = onPerformScroll;
            return this;
        }

        public ScrollRequestBuilder withOnScrollRequestComplete(Runnable runnable) {
            this.onScrollRequestComplete = runnable;
            return this;
        }

        public ScrollRequestBuilder withOnInvalidPosition(Runnable runnable) {
            this.onInvalidPosition = runnable;
            return this;
        }

        public void submit() {
            SnapToTopDataObserver.this.requestScrollPositionInternal(this.position, this.onPerformScroll, this.onScrollRequestComplete, this.onInvalidPosition);
        }
    }
}
