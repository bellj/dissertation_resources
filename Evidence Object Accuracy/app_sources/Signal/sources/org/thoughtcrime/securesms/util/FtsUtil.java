package org.thoughtcrime.securesms.util;

import com.annimon.stream.Stream;
import com.annimon.stream.function.BiConsumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.util.HashSet;
import java.util.Set;
import org.thoughtcrime.securesms.database.SearchDatabase$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.database.SearchDatabase$$ExternalSyntheticLambda3;

/* loaded from: classes4.dex */
public final class FtsUtil {
    private static final Set<Character> BANNED_CHARACTERS = new HashSet();

    static {
        BANNED_CHARACTERS = new HashSet();
        for (int i = 33; i <= 47; i++) {
            BANNED_CHARACTERS.add(Character.valueOf((char) i));
        }
        for (int i2 = 58; i2 <= 64; i2++) {
            BANNED_CHARACTERS.add(Character.valueOf((char) i2));
        }
        for (int i3 = 91; i3 <= 96; i3++) {
            BANNED_CHARACTERS.add(Character.valueOf((char) i3));
        }
        for (int i4 = 123; i4 <= 126; i4++) {
            BANNED_CHARACTERS.add(Character.valueOf((char) i4));
        }
    }

    private FtsUtil() {
    }

    public static String sanitize(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (!BANNED_CHARACTERS.contains(Character.valueOf(charAt))) {
                sb.append(charAt);
            } else if (charAt == '\'') {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    public static String createPrefixMatchString(String str) {
        return ((StringBuilder) Stream.of(sanitize(str).split(" ")).map(new SearchDatabase$$ExternalSyntheticLambda0()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.util.FtsUtil$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return FtsUtil.lambda$createPrefixMatchString$0((String) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.util.FtsUtil$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return FtsUtil.fixQuotes((String) obj);
            }
        }).collect(new SearchDatabase$$ExternalSyntheticLambda3(), new BiConsumer() { // from class: org.thoughtcrime.securesms.util.FtsUtil$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.BiConsumer
            public final void accept(Object obj, Object obj2) {
                FtsUtil.lambda$createPrefixMatchString$1((StringBuilder) obj, (String) obj2);
            }
        })).toString();
    }

    public static /* synthetic */ boolean lambda$createPrefixMatchString$0(String str) {
        return str.length() > 0;
    }

    public static /* synthetic */ void lambda$createPrefixMatchString$1(StringBuilder sb, String str) {
        sb.append(str);
        sb.append("* ");
    }

    public static String fixQuotes(String str) {
        return "\"" + str.replace("\"", "\"\"") + "\"";
    }
}
