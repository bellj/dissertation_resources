package org.thoughtcrime.securesms.util.cjkv;

import java.lang.Character;

/* loaded from: classes4.dex */
public final class CJKVUtil {
    private CJKVUtil() {
    }

    public static boolean isCJKV(String str) {
        if (!(str == null || str.length() == 0)) {
            int i = 0;
            while (i < str.length()) {
                int codePointAt = Character.codePointAt(str, i);
                if (!isCodepointCJKV(codePointAt)) {
                    return false;
                }
                i += Character.charCount(codePointAt);
            }
        }
        return true;
    }

    private static boolean isCodepointCJKV(int i) {
        if (i == 32) {
            return true;
        }
        Character.UnicodeBlock of = Character.UnicodeBlock.of(i);
        if (Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS.equals(of) || Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A.equals(of) || Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B.equals(of) || Character.UnicodeBlock.CJK_COMPATIBILITY.equals(of) || Character.UnicodeBlock.CJK_COMPATIBILITY_FORMS.equals(of) || Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS.equals(of) || Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT.equals(of) || Character.UnicodeBlock.CJK_RADICALS_SUPPLEMENT.equals(of) || Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION.equals(of) || Character.UnicodeBlock.ENCLOSED_CJK_LETTERS_AND_MONTHS.equals(of) || Character.UnicodeBlock.KANGXI_RADICALS.equals(of) || Character.UnicodeBlock.IDEOGRAPHIC_DESCRIPTION_CHARACTERS.equals(of) || Character.UnicodeBlock.HIRAGANA.equals(of) || Character.UnicodeBlock.KATAKANA.equals(of) || Character.UnicodeBlock.KATAKANA_PHONETIC_EXTENSIONS.equals(of) || Character.UnicodeBlock.HANGUL_JAMO.equals(of) || Character.UnicodeBlock.HANGUL_COMPATIBILITY_JAMO.equals(of) || Character.UnicodeBlock.HANGUL_SYLLABLES.equals(of) || Character.isIdeographic(i)) {
            return true;
        }
        return false;
    }
}
