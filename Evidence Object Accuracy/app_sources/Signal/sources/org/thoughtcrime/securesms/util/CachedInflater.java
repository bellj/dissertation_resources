package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.asynclayoutinflater.view.AsyncLayoutInflater;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.concurrent.SerialExecutor;

/* loaded from: classes4.dex */
public class CachedInflater {
    private static final String TAG = Log.tag(CachedInflater.class);
    private final Context context;

    public static CachedInflater from(Context context) {
        return new CachedInflater(context);
    }

    private CachedInflater(Context context) {
        this.context = context;
    }

    public <V extends View> V inflate(int i, ViewGroup viewGroup, boolean z) {
        V v = (V) ViewCache.getInstance().pull(i, this.context.getResources().getConfiguration());
        if (v == null) {
            return (V) LayoutInflater.from(this.context).inflate(i, viewGroup, z);
        }
        if (viewGroup != null && z) {
            viewGroup.addView(v);
        }
        return v;
    }

    public void cacheUntilLimit(int i, ViewGroup viewGroup, int i2) {
        ViewCache.getInstance().cacheUntilLimit(this.context, i, viewGroup, i2);
    }

    public void clear() {
        Log.d(TAG, "Clearing view cache.");
        ViewCache.getInstance().clear();
    }

    /* loaded from: classes4.dex */
    public static class ViewCache {
        private static final Executor ENQUEUING_EXECUTOR = new SerialExecutor(SignalExecutors.BOUNDED);
        private static final ViewCache INSTANCE = new ViewCache();
        private final Map<Integer, List<View>> cache = new HashMap();
        private float fontScale;
        private long lastClearTime;
        private int layoutDirection;
        private int nightModeConfiguration;

        private ViewCache() {
        }

        static ViewCache getInstance() {
            return INSTANCE;
        }

        void cacheUntilLimit(Context context, int i, ViewGroup viewGroup, int i2) {
            Configuration configuration = context.getResources().getConfiguration();
            int nightModeConfiguration = ConfigurationUtil.getNightModeConfiguration(configuration);
            float fontScale = ConfigurationUtil.getFontScale(configuration);
            int layoutDirection = configuration.getLayoutDirection();
            if (!(this.nightModeConfiguration == nightModeConfiguration && this.fontScale == fontScale && this.layoutDirection == layoutDirection)) {
                clear();
                this.nightModeConfiguration = nightModeConfiguration;
                this.fontScale = fontScale;
                this.layoutDirection = layoutDirection;
            }
            AsyncLayoutInflater asyncLayoutInflater = new AsyncLayoutInflater(context);
            int max = Math.max(i2 - ((List) Util.getOrDefault(this.cache, Integer.valueOf(i), Collections.emptyList())).size(), 0);
            ENQUEUING_EXECUTOR.execute(new CachedInflater$ViewCache$$ExternalSyntheticLambda0(this, System.currentTimeMillis(), max, asyncLayoutInflater, i, viewGroup));
        }

        public /* synthetic */ void lambda$cacheUntilLimit$1(long j, int i, AsyncLayoutInflater asyncLayoutInflater, int i2, ViewGroup viewGroup) {
            if (j < this.lastClearTime) {
                String str = CachedInflater.TAG;
                Log.d(str, "Prefetch is no longer valid. Ignoring " + i + " inflates.");
                return;
            }
            CachedInflater$ViewCache$$ExternalSyntheticLambda1 cachedInflater$ViewCache$$ExternalSyntheticLambda1 = new CachedInflater$ViewCache$$ExternalSyntheticLambda1(this, j);
            for (int i3 = 0; i3 < i; i3++) {
                asyncLayoutInflater.inflate(i2, viewGroup, cachedInflater$ViewCache$$ExternalSyntheticLambda1);
            }
        }

        public /* synthetic */ void lambda$cacheUntilLimit$0(long j, View view, int i, ViewGroup viewGroup) {
            ThreadUtil.assertMainThread();
            if (j < this.lastClearTime) {
                Log.d(CachedInflater.TAG, "Prefetch is no longer valid. Ignoring.");
                return;
            }
            List<View> list = this.cache.get(Integer.valueOf(i));
            if (list == null) {
                list = new LinkedList<>();
            }
            list.add(view);
            this.cache.put(Integer.valueOf(i), list);
        }

        View pull(int i, Configuration configuration) {
            if (this.nightModeConfiguration == ConfigurationUtil.getNightModeConfiguration(configuration) && this.fontScale == ConfigurationUtil.getFontScale(configuration) && this.layoutDirection == configuration.getLayoutDirection()) {
                List<View> list = this.cache.get(Integer.valueOf(i));
                if (list == null || list.isEmpty()) {
                    return null;
                }
                return list.remove(0);
            }
            clear();
            return null;
        }

        void clear() {
            this.lastClearTime = System.currentTimeMillis();
            this.cache.clear();
        }
    }
}
