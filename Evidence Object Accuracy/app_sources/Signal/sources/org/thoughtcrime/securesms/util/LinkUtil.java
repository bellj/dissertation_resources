package org.thoughtcrime.securesms.util;

import android.text.TextUtils;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.HttpUrl;
import org.signal.core.util.SetUtil;
import org.thoughtcrime.securesms.stickers.StickerUrl;

/* loaded from: classes4.dex */
public final class LinkUtil {
    private static final Pattern ALL_ASCII_PATTERN = Pattern.compile("^[\\x00-\\x7F]*$");
    private static final Pattern ALL_NON_ASCII_PATTERN = Pattern.compile("^[^\\x00-\\x7F]*$");
    private static final Pattern DOMAIN_PATTERN = Pattern.compile("^(https?://)?([^/]+).*$");
    private static final Pattern ILLEGAL_CHARACTERS_PATTERN = Pattern.compile("[‬‭‮─-◿]");
    private static final Set<String> INVALID_TOP_LEVEL_DOMAINS = SetUtil.newHashSet("onion", "i2p");

    private LinkUtil() {
    }

    public static boolean isValidPreviewUrl(String str) {
        if (str == null) {
            return false;
        }
        if (StickerUrl.isValidShareLink(str)) {
            return true;
        }
        HttpUrl parse = HttpUrl.parse(str);
        if (parse == null || TextUtils.isEmpty(parse.scheme()) || !"https".equals(parse.scheme()) || !isLegalUrl(str, false)) {
            return false;
        }
        return true;
    }

    public static boolean isLegalUrl(String str) {
        return isLegalUrl(str, true);
    }

    private static boolean isLegalUrl(String str, boolean z) {
        if (ILLEGAL_CHARACTERS_PATTERN.matcher(str).find()) {
            return false;
        }
        Matcher matcher = DOMAIN_PATTERN.matcher(str);
        if (!matcher.matches()) {
            return false;
        }
        String group = matcher.group(2);
        Objects.requireNonNull(group);
        String replaceAll = group.replaceAll("\\.", "");
        String parseTopLevelDomain = parseTopLevelDomain(group);
        boolean z2 = ALL_ASCII_PATTERN.matcher(replaceAll).matches() || ALL_NON_ASCII_PATTERN.matcher(replaceAll).matches();
        boolean z3 = z || !INVALID_TOP_LEVEL_DOMAINS.contains(parseTopLevelDomain);
        if (!z2 || !z3) {
            return false;
        }
        return true;
    }

    private static String parseTopLevelDomain(String str) {
        int lastIndexOf = str.lastIndexOf(".");
        if (lastIndexOf < 0 || lastIndexOf >= str.length() - 1) {
            return null;
        }
        return str.substring(lastIndexOf + 1);
    }
}
