package org.thoughtcrime.securesms.util;

import android.os.Handler;
import androidx.core.os.HandlerCompat;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public final class LeakyBucketLimiter {
    private static final String TAG = Log.tag(LeakyBucketLimiter.class);
    private final Object RUNNABLE_TOKEN = new Object();
    private final int bucketCapacity;
    private int bucketLevel;
    private final long dripInterval;
    private final Handler handler;
    private Runnable lastOverflowedRunnable;

    public LeakyBucketLimiter(int i, long j, Handler handler) {
        this.bucketCapacity = i;
        this.dripInterval = j;
        this.handler = handler;
    }

    public void run(Runnable runnable) {
        boolean z;
        boolean z2;
        synchronized (this) {
            int i = this.bucketLevel;
            z = false;
            if (i < this.bucketCapacity) {
                int i2 = i + 1;
                this.bucketLevel = i2;
                if (i2 == 1) {
                    z = true;
                }
                z2 = z;
                z = true;
            } else {
                this.lastOverflowedRunnable = runnable;
                z2 = false;
            }
        }
        if (z) {
            this.handler.removeCallbacksAndMessages(this.RUNNABLE_TOKEN);
            HandlerCompat.postDelayed(this.handler, runnable, this.RUNNABLE_TOKEN, 0);
        } else {
            Log.d(TAG, "Overflowed!");
        }
        if (z2) {
            this.handler.postDelayed(new LeakyBucketLimiter$$ExternalSyntheticLambda0(this), this.dripInterval);
        }
    }

    public void drip() {
        boolean z;
        Runnable runnable;
        synchronized (this) {
            z = false;
            runnable = null;
            if (this.lastOverflowedRunnable == null) {
                this.bucketLevel = Math.max(this.bucketLevel - 1, 0);
            } else {
                Log.d(TAG, "Running most-recently-overflowed task.");
                Runnable runnable2 = this.lastOverflowedRunnable;
                this.lastOverflowedRunnable = null;
                runnable = runnable2;
            }
            if (this.bucketLevel > 0) {
                z = true;
            }
        }
        if (runnable != null) {
            runnable.run();
        }
        if (z) {
            this.handler.postDelayed(new LeakyBucketLimiter$$ExternalSyntheticLambda0(this), this.dripInterval);
        }
    }
}
