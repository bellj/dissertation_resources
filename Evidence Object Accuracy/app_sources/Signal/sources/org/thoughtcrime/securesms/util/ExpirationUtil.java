package org.thoughtcrime.securesms.util;

import android.content.Context;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class ExpirationUtil {
    private static final int SECONDS_IN_DAY;
    private static final int SECONDS_IN_HOUR = ((int) TimeUnit.HOURS.toSeconds(1));
    private static final int SECONDS_IN_MINUTE = ((int) TimeUnit.MINUTES.toSeconds(1));
    private static final int SECONDS_IN_WEEK;

    static {
        TimeUnit timeUnit = TimeUnit.DAYS;
        SECONDS_IN_WEEK = (int) timeUnit.toSeconds(7);
        SECONDS_IN_DAY = (int) timeUnit.toSeconds(1);
        SECONDS_IN_HOUR = (int) TimeUnit.HOURS.toSeconds(1);
        SECONDS_IN_MINUTE = (int) TimeUnit.MINUTES.toSeconds(1);
    }

    public static String getExpirationDisplayValue(Context context, int i) {
        if (i <= 0) {
            return context.getString(R.string.expiration_off);
        }
        int i2 = SECONDS_IN_WEEK;
        int i3 = i / i2;
        String displayValue = getDisplayValue(context, "", R.plurals.expiration_weeks, i3);
        int i4 = i - (i2 * i3);
        int i5 = SECONDS_IN_DAY;
        int i6 = i4 / i5;
        String displayValue2 = getDisplayValue(context, displayValue, R.plurals.expiration_days, i6);
        if (i3 > 0) {
            return displayValue2;
        }
        int i7 = i4 - (i5 * i6);
        int i8 = SECONDS_IN_HOUR;
        int i9 = i7 / i8;
        String displayValue3 = getDisplayValue(context, displayValue2, R.plurals.expiration_hours, i9);
        if (i6 > 0) {
            return displayValue3;
        }
        int i10 = i7 - (i8 * i9);
        int i11 = SECONDS_IN_MINUTE;
        int i12 = i10 / i11;
        String displayValue4 = getDisplayValue(context, displayValue3, R.plurals.expiration_minutes, i12);
        if (i9 > 0) {
            return displayValue4;
        }
        return getDisplayValue(context, displayValue4, R.plurals.expiration_seconds, i10 - (i12 * i11));
    }

    private static String getDisplayValue(Context context, String str, int i, int i2) {
        if (i2 <= 0) {
            return str;
        }
        String quantityString = context.getResources().getQuantityString(i, i2, Integer.valueOf(i2));
        if (str.isEmpty()) {
            return quantityString;
        }
        return context.getString(R.string.expiration_combined, str, quantityString);
    }

    public static String getExpirationAbbreviatedDisplayValue(Context context, int i) {
        if (i <= 0) {
            return context.getString(R.string.expiration_off);
        }
        int i2 = SECONDS_IN_WEEK;
        int i3 = i / i2;
        String abbreviatedDisplayValue = getAbbreviatedDisplayValue(context, "", R.string.expiration_weeks_abbreviated, i3);
        int i4 = i - (i3 * i2);
        int i5 = SECONDS_IN_DAY;
        int i6 = i4 / i5;
        String abbreviatedDisplayValue2 = getAbbreviatedDisplayValue(context, abbreviatedDisplayValue, R.string.expiration_days_abbreviated, i6);
        int i7 = i4 - (i6 * i5);
        int i8 = SECONDS_IN_HOUR;
        int i9 = i7 / i8;
        String abbreviatedDisplayValue3 = getAbbreviatedDisplayValue(context, abbreviatedDisplayValue2, R.string.expiration_hours_abbreviated, i9);
        int i10 = i7 - (i9 * i8);
        int i11 = SECONDS_IN_MINUTE;
        int i12 = i10 / i11;
        return getAbbreviatedDisplayValue(context, getAbbreviatedDisplayValue(context, abbreviatedDisplayValue3, R.string.expiration_minutes_abbreviated, i12), R.string.expiration_seconds_abbreviated, i10 - (i12 * i11));
    }

    private static String getAbbreviatedDisplayValue(Context context, String str, int i, int i2) {
        if (i2 <= 0) {
            return str;
        }
        String string = context.getString(i, Integer.valueOf(i2));
        if (str.isEmpty()) {
            return string;
        }
        return context.getString(R.string.expiration_combined, str, string);
    }
}
