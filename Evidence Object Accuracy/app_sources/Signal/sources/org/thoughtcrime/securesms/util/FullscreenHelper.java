package org.thoughtcrime.securesms.util;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.core.graphics.Insets;
import androidx.core.view.DisplayCutoutCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

/* loaded from: classes4.dex */
public final class FullscreenHelper {
    private final Activity activity;

    public FullscreenHelper(Activity activity) {
        this.activity = activity;
        if (Build.VERSION.SDK_INT >= 28) {
            activity.getWindow().getAttributes().layoutInDisplayCutoutMode = 1;
        }
        showSystemUI();
    }

    public void configureToolbarLayout(View view, View view2) {
        if (Build.VERSION.SDK_INT == 19) {
            setSpacerHeight(view, ViewUtil.getStatusBarHeight(view));
            int[] makePaddingValuesForAPI19 = makePaddingValuesForAPI19(this.activity);
            view2.setPadding(makePaddingValuesForAPI19[0], 0, makePaddingValuesForAPI19[1], 0);
            return;
        }
        ViewCompat.setOnApplyWindowInsetsListener(view, new OnApplyWindowInsetsListener() { // from class: org.thoughtcrime.securesms.util.FullscreenHelper$$ExternalSyntheticLambda0
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view3, WindowInsetsCompat windowInsetsCompat) {
                return FullscreenHelper.lambda$configureToolbarLayout$0(view3, windowInsetsCompat);
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(view2, new OnApplyWindowInsetsListener(view2) { // from class: org.thoughtcrime.securesms.util.FullscreenHelper$$ExternalSyntheticLambda1
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view3, WindowInsetsCompat windowInsetsCompat) {
                return FullscreenHelper.lambda$configureToolbarLayout$1(this.f$0, view3, windowInsetsCompat);
            }
        });
    }

    public static /* synthetic */ WindowInsetsCompat lambda$configureToolbarLayout$0(View view, WindowInsetsCompat windowInsetsCompat) {
        setSpacerHeight(view, windowInsetsCompat.getSystemWindowInsetTop());
        return windowInsetsCompat;
    }

    public static /* synthetic */ WindowInsetsCompat lambda$configureToolbarLayout$1(View view, View view2, WindowInsetsCompat windowInsetsCompat) {
        int[] makePaddingValues = makePaddingValues(windowInsetsCompat);
        view.setPadding(makePaddingValues[0], 0, makePaddingValues[1], 0);
        return windowInsetsCompat;
    }

    public static void configureBottomBarLayout(Activity activity, View view, View view2) {
        if (Build.VERSION.SDK_INT == 19) {
            setSpacerHeight(view, ViewUtil.getNavigationBarHeight(view));
            int[] makePaddingValuesForAPI19 = makePaddingValuesForAPI19(activity);
            view2.setPadding(makePaddingValuesForAPI19[0], 0, makePaddingValuesForAPI19[1], 0);
            return;
        }
        ViewCompat.setOnApplyWindowInsetsListener(view, new OnApplyWindowInsetsListener() { // from class: org.thoughtcrime.securesms.util.FullscreenHelper$$ExternalSyntheticLambda4
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view3, WindowInsetsCompat windowInsetsCompat) {
                return FullscreenHelper.lambda$configureBottomBarLayout$2(view3, windowInsetsCompat);
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(view2, new OnApplyWindowInsetsListener(view2) { // from class: org.thoughtcrime.securesms.util.FullscreenHelper$$ExternalSyntheticLambda5
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view3, WindowInsetsCompat windowInsetsCompat) {
                return FullscreenHelper.lambda$configureBottomBarLayout$3(this.f$0, view3, windowInsetsCompat);
            }
        });
    }

    public static /* synthetic */ WindowInsetsCompat lambda$configureBottomBarLayout$2(View view, WindowInsetsCompat windowInsetsCompat) {
        setSpacerHeight(view, windowInsetsCompat.getSystemWindowInsetBottom());
        return windowInsetsCompat;
    }

    public static /* synthetic */ WindowInsetsCompat lambda$configureBottomBarLayout$3(View view, View view2, WindowInsetsCompat windowInsetsCompat) {
        int[] makePaddingValues = makePaddingValues(windowInsetsCompat);
        view.setPadding(makePaddingValues[0], 0, makePaddingValues[1], 0);
        return windowInsetsCompat;
    }

    private static void setSpacerHeight(View view, int i) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = i;
        view.setLayoutParams(layoutParams);
        view.setVisibility(0);
    }

    private static int[] makePaddingValuesForAPI19(Activity activity) {
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        if (rotation == 0 || rotation == 2) {
            return new int[]{0, 0};
        }
        Resources resources = activity.getResources();
        int identifier = resources.getIdentifier("status_bar_height", "dimen", "android");
        int identifier2 = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        int dimensionPixelSize = resources.getDimensionPixelSize(identifier);
        int dimensionPixelSize2 = resources.getDimensionPixelSize(identifier2);
        return rotation != 1 ? rotation != 3 ? new int[]{0, 0} : new int[]{dimensionPixelSize2, dimensionPixelSize} : new int[]{dimensionPixelSize, dimensionPixelSize2};
    }

    private static int[] makePaddingValues(WindowInsetsCompat windowInsetsCompat) {
        int i;
        int i2;
        Insets tappableElementInsets = windowInsetsCompat.getTappableElementInsets();
        DisplayCutoutCompat displayCutout = windowInsetsCompat.getDisplayCutout();
        if (displayCutout == null) {
            i = tappableElementInsets.left;
        } else {
            i = Math.max(tappableElementInsets.left, displayCutout.getSafeInsetLeft());
        }
        if (displayCutout == null) {
            i2 = tappableElementInsets.right;
        } else {
            i2 = Math.max(tappableElementInsets.right, displayCutout.getSafeInsetRight());
        }
        return new int[]{i, i2};
    }

    public void showAndHideWithSystemUI(Window window, View... viewArr) {
        window.getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener(viewArr) { // from class: org.thoughtcrime.securesms.util.FullscreenHelper$$ExternalSyntheticLambda6
            public final /* synthetic */ View[] f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnSystemUiVisibilityChangeListener
            public final void onSystemUiVisibilityChange(int i) {
                FullscreenHelper.lambda$showAndHideWithSystemUI$6(this.f$0, i);
            }
        });
    }

    public static /* synthetic */ void lambda$showAndHideWithSystemUI$6(View[] viewArr, int i) {
        boolean z = (i & 4) != 0;
        for (View view : viewArr) {
            view.animate().alpha(z ? 0.0f : 1.0f).withStartAction(new Runnable(z, view) { // from class: org.thoughtcrime.securesms.util.FullscreenHelper$$ExternalSyntheticLambda2
                public final /* synthetic */ boolean f$0;
                public final /* synthetic */ View f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    FullscreenHelper.lambda$showAndHideWithSystemUI$4(this.f$0, this.f$1);
                }
            }).withEndAction(new Runnable(z, view) { // from class: org.thoughtcrime.securesms.util.FullscreenHelper$$ExternalSyntheticLambda3
                public final /* synthetic */ boolean f$0;
                public final /* synthetic */ View f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    FullscreenHelper.lambda$showAndHideWithSystemUI$5(this.f$0, this.f$1);
                }
            }).start();
        }
    }

    public static /* synthetic */ void lambda$showAndHideWithSystemUI$4(boolean z, View view) {
        if (!z) {
            view.setVisibility(0);
        }
    }

    public static /* synthetic */ void lambda$showAndHideWithSystemUI$5(boolean z, View view) {
        if (z) {
            view.setVisibility(4);
        }
    }

    public void toggleUiVisibility() {
        if ((this.activity.getWindow().getDecorView().getSystemUiVisibility() & 4) != 0) {
            showSystemUI();
        } else {
            hideSystemUI();
        }
    }

    public void hideSystemUI() {
        this.activity.getWindow().getDecorView().setSystemUiVisibility(3846);
    }

    public void showSystemUI() {
        showSystemUI(this.activity.getWindow());
    }

    public static void showSystemUI(Window window) {
        window.getDecorView().setSystemUiVisibility(1792);
    }
}
