package org.thoughtcrime.securesms.util;

import android.app.ActivityManager;
import android.content.Context;
import android.os.ParcelFileDescriptor;
import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public final class MemoryFileDescriptor implements Closeable {
    private static final String TAG = Log.tag(MemoryFileDescriptor.class);
    private static long sizeOfAllMemoryFileDescriptors;
    private static Boolean supported;
    private final ParcelFileDescriptor parcelFileDescriptor;
    private final AtomicLong sizeEstimate;

    /* loaded from: classes4.dex */
    public static class MemoryFileException extends IOException {
    }

    public static synchronized boolean supported() {
        boolean booleanValue;
        synchronized (MemoryFileDescriptor.class) {
            if (supported == null) {
                try {
                    int createMemoryFileDescriptor = FileUtils.createMemoryFileDescriptor("CHECK");
                    if (createMemoryFileDescriptor < 0) {
                        supported = Boolean.FALSE;
                        Log.w(TAG, "MemoryFileDescriptor is not available.");
                    } else {
                        supported = Boolean.TRUE;
                        ParcelFileDescriptor.adoptFd(createMemoryFileDescriptor).close();
                    }
                } catch (IOException e) {
                    Log.w(TAG, e);
                }
            }
            booleanValue = supported.booleanValue();
        }
        return booleanValue;
    }

    private MemoryFileDescriptor(ParcelFileDescriptor parcelFileDescriptor, long j) {
        this.parcelFileDescriptor = parcelFileDescriptor;
        this.sizeEstimate = new AtomicLong(j);
    }

    public static MemoryFileDescriptor newMemoryFileDescriptor(Context context, String str, long j) throws MemoryFileException {
        if (j >= 0) {
            if (j > 0) {
                ActivityManager activityManager = ServiceUtil.getActivityManager(context);
                ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                synchronized (MemoryFileDescriptor.class) {
                    activityManager.getMemoryInfo(memoryInfo);
                    long j2 = sizeOfAllMemoryFileDescriptors;
                    long j3 = ((memoryInfo.availMem - memoryInfo.threshold) - j) - j2;
                    if (j3 > 0) {
                        sizeOfAllMemoryFileDescriptors = j2 + j;
                    } else {
                        NumberFormat instance = NumberFormat.getInstance(Locale.US);
                        Log.w(TAG, String.format("Not enough RAM available without taking the system into a low memory state.%nAvailable: %s%nLow memory threshold: %s%nRequested: %s%nTotal MemoryFileDescriptor limit: %s%nShortfall: %s", instance.format(memoryInfo.availMem), instance.format(memoryInfo.threshold), instance.format(j), instance.format(sizeOfAllMemoryFileDescriptors), instance.format(j3)));
                        throw new MemoryLimitException();
                    }
                }
            }
            int createMemoryFileDescriptor = FileUtils.createMemoryFileDescriptor(str);
            if (createMemoryFileDescriptor >= 0) {
                return new MemoryFileDescriptor(ParcelFileDescriptor.adoptFd(createMemoryFileDescriptor), j);
            }
            String str2 = TAG;
            Log.w(str2, "Failed to create file descriptor: " + createMemoryFileDescriptor);
            throw new MemoryFileCreationException();
        }
        throw new IllegalArgumentException();
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        try {
            try {
                clearAndRemoveAllocation();
            } catch (Exception e) {
                Log.w(TAG, "Failed to clear data in MemoryFileDescriptor", e);
            }
        } finally {
            this.parcelFileDescriptor.close();
        }
    }

    private void clearAndRemoveAllocation() throws IOException {
        clear();
        long andSet = this.sizeEstimate.getAndSet(0);
        synchronized (MemoryFileDescriptor.class) {
            sizeOfAllMemoryFileDescriptors -= andSet;
        }
    }

    private void clear() throws IOException {
        FileInputStream fileInputStream = new FileInputStream(getFileDescriptor());
        try {
            FileChannel channel = fileInputStream.getChannel();
            long size = channel.size();
            if (size == 0) {
                fileInputStream.close();
                return;
            }
            channel.position(0L);
            fileInputStream.close();
            byte[] bArr = new byte[16384];
            FileOutputStream fileOutputStream = new FileOutputStream(getFileDescriptor());
            while (size > 0) {
                try {
                    int min = (int) Math.min(size, (long) 16384);
                    fileOutputStream.write(bArr, 0, min);
                    size -= (long) min;
                } catch (Throwable th) {
                    try {
                        fileOutputStream.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                    throw th;
                }
            }
            fileOutputStream.close();
        } catch (Throwable th3) {
            try {
                fileInputStream.close();
            } catch (Throwable th4) {
                th3.addSuppressed(th4);
            }
            throw th3;
        }
    }

    public FileDescriptor getFileDescriptor() {
        return this.parcelFileDescriptor.getFileDescriptor();
    }

    public void seek(long j) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(getFileDescriptor());
        try {
            fileInputStream.getChannel().position(j);
            fileInputStream.close();
        } catch (Throwable th) {
            try {
                fileInputStream.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public long size() throws IOException {
        FileInputStream fileInputStream = new FileInputStream(getFileDescriptor());
        try {
            long size = fileInputStream.getChannel().size();
            fileInputStream.close();
            return size;
        } catch (Throwable th) {
            try {
                fileInputStream.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static final class MemoryLimitException extends MemoryFileException {
        private MemoryLimitException() {
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static final class MemoryFileCreationException extends MemoryFileException {
        private MemoryFileCreationException() {
        }
    }
}
