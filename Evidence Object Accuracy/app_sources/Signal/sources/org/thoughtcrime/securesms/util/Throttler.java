package org.thoughtcrime.securesms.util;

import android.os.Handler;
import android.os.Looper;

/* loaded from: classes4.dex */
public class Throttler {
    private static final int WHAT;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private final long threshold;

    public Throttler(long j) {
        this.threshold = j;
    }

    public void publish(Runnable runnable) {
        if (!this.handler.hasMessages(WHAT)) {
            runnable.run();
            Handler handler = this.handler;
            handler.sendMessageDelayed(handler.obtainMessage(WHAT), this.threshold);
        }
    }

    public void clear() {
        this.handler.removeCallbacksAndMessages(null);
    }
}
