package org.thoughtcrime.securesms.util;

import android.content.Context;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.ModernEncryptingPartOutputStream;

/* compiled from: EncryptedStreamUtils.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\bH\u0007¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/util/EncryptedStreamUtils;", "", "()V", "getInputStream", "Ljava/io/InputStream;", "context", "Landroid/content/Context;", "inputFile", "Ljava/io/File;", "getOutputStream", "Ljava/io/OutputStream;", "outputFile", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EncryptedStreamUtils {
    public static final EncryptedStreamUtils INSTANCE = new EncryptedStreamUtils();

    private EncryptedStreamUtils() {
    }

    public final OutputStream getOutputStream(Context context, File file) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(file, "outputFile");
        Object obj = ModernEncryptingPartOutputStream.createFor(AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), file, true).second;
        Intrinsics.checkNotNullExpressionValue(obj, "createFor(attachmentSecr… outputFile, true).second");
        return (OutputStream) obj;
    }

    public final InputStream getInputStream(Context context, File file) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(file, "inputFile");
        InputStream createFor = ModernDecryptingPartInputStream.createFor(AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), file, 0);
        Intrinsics.checkNotNullExpressionValue(createFor, "createFor(attachmentSecret, inputFile, 0)");
        return createFor;
    }
}
