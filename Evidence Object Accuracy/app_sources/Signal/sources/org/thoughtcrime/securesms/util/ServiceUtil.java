package org.thoughtcrime.securesms.util;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.app.job.JobScheduler;
import android.content.ClipboardManager;
import android.content.Context;
import android.hardware.SensorManager;
import android.hardware.display.DisplayManager;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.os.PowerManager;
import android.os.Vibrator;
import android.os.storage.StorageManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodManager;
import androidx.core.content.ContextCompat;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* loaded from: classes4.dex */
public class ServiceUtil {
    public static InputMethodManager getInputMethodManager(Context context) {
        return (InputMethodManager) context.getSystemService("input_method");
    }

    public static WindowManager getWindowManager(Context context) {
        return (WindowManager) context.getSystemService("window");
    }

    public static StorageManager getStorageManager(Context context) {
        return (StorageManager) ContextCompat.getSystemService(context, StorageManager.class);
    }

    public static ConnectivityManager getConnectivityManager(Context context) {
        return (ConnectivityManager) context.getSystemService("connectivity");
    }

    public static NotificationManager getNotificationManager(Context context) {
        return (NotificationManager) context.getSystemService("notification");
    }

    public static TelephonyManager getTelephonyManager(Context context) {
        return (TelephonyManager) context.getSystemService(RecipientDatabase.PHONE);
    }

    public static AudioManager getAudioManager(Context context) {
        return (AudioManager) context.getSystemService("audio");
    }

    public static SensorManager getSensorManager(Context context) {
        return (SensorManager) context.getSystemService("sensor");
    }

    public static PowerManager getPowerManager(Context context) {
        return (PowerManager) context.getSystemService("power");
    }

    public static AlarmManager getAlarmManager(Context context) {
        return (AlarmManager) context.getSystemService("alarm");
    }

    public static Vibrator getVibrator(Context context) {
        return (Vibrator) context.getSystemService("vibrator");
    }

    public static DisplayManager getDisplayManager(Context context) {
        return (DisplayManager) context.getSystemService("display");
    }

    public static AccessibilityManager getAccessibilityManager(Context context) {
        return (AccessibilityManager) context.getSystemService("accessibility");
    }

    public static ClipboardManager getClipboardManager(Context context) {
        return (ClipboardManager) context.getSystemService("clipboard");
    }

    public static JobScheduler getJobScheduler(Context context) {
        return (JobScheduler) context.getSystemService(JobScheduler.class);
    }

    public static SubscriptionManager getSubscriptionManager(Context context) {
        return (SubscriptionManager) context.getSystemService("telephony_subscription_service");
    }

    public static ActivityManager getActivityManager(Context context) {
        return (ActivityManager) context.getSystemService("activity");
    }

    public static LocationManager getLocationManager(Context context) {
        return (LocationManager) ContextCompat.getSystemService(context, LocationManager.class);
    }

    public static KeyguardManager getKeyguardManager(Context context) {
        return (KeyguardManager) ContextCompat.getSystemService(context, KeyguardManager.class);
    }
}
