package org.thoughtcrime.securesms.util;

import android.content.Context;
import android.text.TextUtils;
import java.util.Collections;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.jobmanager.impl.NotInCallConstraint;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class AttachmentUtil {
    private static final String TAG = Log.tag(AttachmentUtil.class);

    public static boolean isAutoDownloadPermitted(Context context, DatabaseAttachment databaseAttachment) {
        if (databaseAttachment == null) {
            Log.w(TAG, "attachment was null, returning vacuous true");
            return true;
        } else if (!isFromTrustedConversation(context, databaseAttachment)) {
            return false;
        } else {
            Set<String> allowedAutoDownloadTypes = getAllowedAutoDownloadTypes(context);
            String contentType = databaseAttachment.getContentType();
            if (databaseAttachment.isVoiceNote()) {
                return true;
            }
            if ((MediaUtil.isAudio(databaseAttachment) && TextUtils.isEmpty(databaseAttachment.getFileName())) || MediaUtil.isLongTextType(databaseAttachment.getContentType()) || databaseAttachment.isSticker()) {
                return true;
            }
            if (databaseAttachment.isVideoGif()) {
                if (!NotInCallConstraint.isNotInConnectedCall() || !allowedAutoDownloadTypes.contains(DraftDatabase.Draft.IMAGE)) {
                    return false;
                }
                return true;
            } else if (isNonDocumentType(contentType)) {
                if (!NotInCallConstraint.isNotInConnectedCall() || !allowedAutoDownloadTypes.contains(MediaUtil.getDiscreteMimeType(contentType))) {
                    return false;
                }
                return true;
            } else if (!NotInCallConstraint.isNotInConnectedCall() || !allowedAutoDownloadTypes.contains("documents")) {
                return false;
            } else {
                return true;
            }
        }
    }

    public static void deleteAttachment(Context context, DatabaseAttachment databaseAttachment) {
        AttachmentId attachmentId = databaseAttachment.getAttachmentId();
        long mmsId = databaseAttachment.getMmsId();
        if (SignalDatabase.attachments().getAttachmentsForMessage(mmsId).size() <= 1) {
            SignalDatabase.mms().deleteMessage(mmsId);
        } else {
            SignalDatabase.attachments().deleteAttachment(attachmentId);
        }
    }

    private static boolean isNonDocumentType(String str) {
        return MediaUtil.isImageType(str) || MediaUtil.isVideoType(str) || MediaUtil.isAudioType(str);
    }

    private static Set<String> getAllowedAutoDownloadTypes(Context context) {
        if (NetworkUtil.isConnectedWifi(context)) {
            return TextSecurePreferences.getWifiMediaDownloadAllowed(context);
        }
        if (NetworkUtil.isConnectedRoaming(context)) {
            return TextSecurePreferences.getRoamingMediaDownloadAllowed(context);
        }
        if (NetworkUtil.isConnectedMobile(context)) {
            return TextSecurePreferences.getMobileMediaDownloadAllowed(context);
        }
        return Collections.emptySet();
    }

    private static boolean isFromTrustedConversation(Context context, DatabaseAttachment databaseAttachment) {
        try {
            MessageRecord messageRecord = SignalDatabase.mms().getMessageRecord(databaseAttachment.getMmsId());
            Recipient recipient = messageRecord.getRecipient();
            Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(messageRecord.getThreadId());
            if (recipientForThreadId == null || !recipientForThreadId.isGroup()) {
                return isTrustedIndividual(recipient, messageRecord);
            }
            if (recipientForThreadId.isProfileSharing() || isTrustedIndividual(recipient, messageRecord)) {
                return true;
            }
            return false;
        } catch (NoSuchMessageException unused) {
            Log.w(TAG, "Message could not be found! Assuming not a trusted contact.");
            return false;
        }
    }

    private static boolean isTrustedIndividual(Recipient recipient, MessageRecord messageRecord) {
        return recipient.isSystemContact() || recipient.isProfileSharing() || messageRecord.isOutgoing() || recipient.isSelf() || recipient.isReleaseNotes();
    }
}
