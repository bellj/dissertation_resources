package org.thoughtcrime.securesms.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/* loaded from: classes4.dex */
public class ParcelUtil {
    public static byte[] serialize(Parcelable parcelable) {
        Parcel obtain = Parcel.obtain();
        parcelable.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        return marshall;
    }

    public static Parcel deserialize(byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        return obtain;
    }

    public static <T> T deserialize(byte[] bArr, Parcelable.Creator<T> creator) {
        return creator.createFromParcel(deserialize(bArr));
    }

    public static void writeStringCollection(Parcel parcel, Collection<String> collection) {
        parcel.writeStringList(new ArrayList(collection));
    }

    public static Collection<String> readStringCollection(Parcel parcel) {
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        return arrayList;
    }

    public static void writeParcelableCollection(Parcel parcel, Collection<? extends Parcelable> collection) {
        parcel.writeParcelableArray((Parcelable[]) collection.toArray(new Parcelable[0]), 0);
    }

    public static <E> Collection<E> readParcelableCollection(Parcel parcel, Class<E> cls) {
        return Arrays.asList(parcel.readParcelableArray(cls.getClassLoader()));
    }

    public static void writeBoolean(Parcel parcel, boolean z) {
        parcel.writeByte(z ? (byte) 1 : 0);
    }

    public static boolean readBoolean(Parcel parcel) {
        return parcel.readByte() != 0;
    }

    public static void writeByteArray(Parcel parcel, byte[] bArr) {
        if (bArr == null) {
            parcel.writeInt(-1);
            return;
        }
        parcel.writeInt(bArr.length);
        parcel.writeByteArray(bArr);
    }

    public static byte[] readByteArray(Parcel parcel) {
        int readInt = parcel.readInt();
        if (readInt == -1) {
            return null;
        }
        byte[] bArr = new byte[readInt];
        parcel.readByteArray(bArr);
        return bArr;
    }
}
