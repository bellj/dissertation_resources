package org.thoughtcrime.securesms.database.helpers;

import android.content.ContentValues;
import android.content.Context;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.Conversions;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.state.PreKeyRecord;
import org.signal.libsignal.protocol.state.SignedPreKeyRecord;
import org.thoughtcrime.securesms.database.OneTimePreKeyDatabase;
import org.thoughtcrime.securesms.database.SignedPreKeyDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.JsonUtils;

/* loaded from: classes4.dex */
public final class PreKeyMigrationHelper {
    private static final int CURRENT_VERSION_MARKER;
    private static final int PLAINTEXT_VERSION;
    private static final String PREKEY_DIRECTORY;
    private static final String SIGNED_PREKEY_DIRECTORY;
    private static final String TAG = Log.tag(PreKeyMigrationHelper.class);

    /* loaded from: classes.dex */
    public static class PreKeyIndex {
        static final String FILE_NAME;
        @JsonProperty
        private int nextPreKeyId;
    }

    /* loaded from: classes.dex */
    public static class SignedPreKeyIndex {
        static final String FILE_NAME;
        @JsonProperty
        private int activeSignedPreKeyId = -1;
        @JsonProperty
        private int nextSignedPreKeyId;
    }

    public static boolean migratePreKeys(Context context, SQLiteDatabase sQLiteDatabase) {
        File[] listFiles = getPreKeyDirectory(context).listFiles();
        boolean z = true;
        if (listFiles != null) {
            for (File file : listFiles) {
                if (!"index.dat".equals(file.getName())) {
                    try {
                        PreKeyRecord preKeyRecord = new PreKeyRecord(loadSerializedRecord(file));
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("key_id", Integer.valueOf(preKeyRecord.getId()));
                        contentValues.put("public_key", Base64.encodeBytes(preKeyRecord.getKeyPair().getPublicKey().serialize()));
                        contentValues.put("private_key", Base64.encodeBytes(preKeyRecord.getKeyPair().getPrivateKey().serialize()));
                        sQLiteDatabase.insert(OneTimePreKeyDatabase.TABLE_NAME, (String) null, contentValues);
                        Log.i(TAG, "Migrated one-time prekey: " + preKeyRecord.getId());
                    } catch (IOException | InvalidKeyException | InvalidMessageException e) {
                        Log.w(TAG, e);
                        z = false;
                    }
                }
            }
        }
        File[] listFiles2 = getSignedPreKeyDirectory(context).listFiles();
        if (listFiles2 != null) {
            for (File file2 : listFiles2) {
                if (!"index.dat".equals(file2.getName())) {
                    try {
                        SignedPreKeyRecord signedPreKeyRecord = new SignedPreKeyRecord(loadSerializedRecord(file2));
                        ContentValues contentValues2 = new ContentValues();
                        contentValues2.put("key_id", Integer.valueOf(signedPreKeyRecord.getId()));
                        contentValues2.put("public_key", Base64.encodeBytes(signedPreKeyRecord.getKeyPair().getPublicKey().serialize()));
                        contentValues2.put("private_key", Base64.encodeBytes(signedPreKeyRecord.getKeyPair().getPrivateKey().serialize()));
                        contentValues2.put(SignedPreKeyDatabase.SIGNATURE, Base64.encodeBytes(signedPreKeyRecord.getSignature()));
                        contentValues2.put("timestamp", Long.valueOf(signedPreKeyRecord.getTimestamp()));
                        sQLiteDatabase.insert("signed_prekeys", (String) null, contentValues2);
                        Log.i(TAG, "Migrated signed prekey: " + signedPreKeyRecord.getId());
                    } catch (IOException | InvalidMessageException e2) {
                        Log.w(TAG, e2);
                        z = false;
                    }
                }
            }
        }
        File file3 = new File(getPreKeyDirectory(context), "index.dat");
        File file4 = new File(getSignedPreKeyDirectory(context), "index.dat");
        if (file3.exists()) {
            try {
                InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file3));
                PreKeyIndex preKeyIndex = (PreKeyIndex) JsonUtils.fromJson(inputStreamReader, PreKeyIndex.class);
                inputStreamReader.close();
                Log.i(TAG, "Setting next prekey id: " + preKeyIndex.nextPreKeyId);
                SignalStore.account().aciPreKeys().setNextOneTimePreKeyId(preKeyIndex.nextPreKeyId);
            } catch (IOException e3) {
                Log.w(TAG, e3);
            }
        }
        if (file4.exists()) {
            try {
                InputStreamReader inputStreamReader2 = new InputStreamReader(new FileInputStream(file4));
                SignedPreKeyIndex signedPreKeyIndex = (SignedPreKeyIndex) JsonUtils.fromJson(inputStreamReader2, SignedPreKeyIndex.class);
                inputStreamReader2.close();
                String str = TAG;
                Log.i(str, "Setting next signed prekey id: " + signedPreKeyIndex.nextSignedPreKeyId);
                Log.i(str, "Setting active signed prekey id: " + signedPreKeyIndex.activeSignedPreKeyId);
                SignalStore.account().aciPreKeys().setNextSignedPreKeyId(signedPreKeyIndex.nextSignedPreKeyId);
                SignalStore.account().aciPreKeys().setActiveSignedPreKeyId(signedPreKeyIndex.activeSignedPreKeyId);
            } catch (IOException e4) {
                Log.w(TAG, e4);
            }
        }
        return z;
    }

    public static void cleanUpPreKeys(Context context) {
        File preKeyDirectory = getPreKeyDirectory(context);
        File[] listFiles = preKeyDirectory.listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                Log.i(TAG, "Deleting: " + file.getAbsolutePath());
                file.delete();
            }
            Log.i(TAG, "Deleting: " + preKeyDirectory.getAbsolutePath());
            preKeyDirectory.delete();
        }
        File signedPreKeyDirectory = getSignedPreKeyDirectory(context);
        File[] listFiles2 = signedPreKeyDirectory.listFiles();
        if (listFiles2 != null) {
            for (File file2 : listFiles2) {
                Log.i(TAG, "Deleting: " + file2.getAbsolutePath());
                file2.delete();
            }
            Log.i(TAG, "Deleting: " + signedPreKeyDirectory.getAbsolutePath());
            signedPreKeyDirectory.delete();
        }
    }

    private static byte[] loadSerializedRecord(File file) throws IOException, InvalidMessageException {
        FileInputStream fileInputStream = new FileInputStream(file);
        int readInteger = readInteger(fileInputStream);
        if (readInteger <= 2) {
            byte[] readBlob = readBlob(fileInputStream);
            if (readInteger >= 2) {
                fileInputStream.close();
                return readBlob;
            }
            throw new IOException("Migration didn't happen! " + file.getAbsolutePath() + ", " + readInteger);
        }
        throw new IOException("Invalid version: " + readInteger);
    }

    private static File getPreKeyDirectory(Context context) {
        return getRecordsDirectory(context, PREKEY_DIRECTORY);
    }

    private static File getSignedPreKeyDirectory(Context context) {
        return getRecordsDirectory(context, "signed_prekeys");
    }

    private static File getRecordsDirectory(Context context, String str) {
        File file = new File(context.getFilesDir(), str);
        if (!file.exists() && !file.mkdirs()) {
            Log.w(TAG, "PreKey directory creation failed!");
        }
        return file;
    }

    private static byte[] readBlob(FileInputStream fileInputStream) throws IOException {
        int readInteger = readInteger(fileInputStream);
        byte[] bArr = new byte[readInteger];
        fileInputStream.read(bArr, 0, readInteger);
        return bArr;
    }

    private static int readInteger(FileInputStream fileInputStream) throws IOException {
        byte[] bArr = new byte[4];
        fileInputStream.read(bArr, 0, 4);
        return Conversions.byteArrayToInt(bArr);
    }
}
