package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ReactionDatabase.kt */
@Metadata(d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0010\u001e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0006\u0010\r\u001a\u00020\bJ\u0016\u0010\u000e\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\f0\u00132\u0006\u0010\t\u001a\u00020\nJ&\u0010\u0014\u001a\u0014\u0012\u0004\u0012\u00020\n\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u00130\u00152\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\n0\u0017J\u0016\u0010\u0018\u001a\u00020\u00192\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0010\u0010\u001a\u001a\u00020\u00192\u0006\u0010\t\u001a\u00020\nH\u0002J\u0016\u0010\u001b\u001a\u00020\b2\u0006\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u001d\u001a\u00020\u0010¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/database/ReactionDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "addReaction", "", "messageId", "Lorg/thoughtcrime/securesms/database/model/MessageId;", ReactionDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/database/model/ReactionRecord;", "deleteAbandonedReactions", "deleteReaction", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "deleteReactions", "getReactions", "", "getReactionsForMessages", "", "messageIds", "", "hasReaction", "", "hasReactions", "remapRecipient", "oldAuthorId", "newAuthorId", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ReactionDatabase extends Database {
    private static final String AUTHOR_ID;
    public static final String CREATE_TABLE = "CREATE TABLE reaction (\n  _id INTEGER PRIMARY KEY,\n  message_id INTEGER NOT NULL,\n  is_mms INTEGER NOT NULL,\n  author_id INTEGER NOT NULL REFERENCES recipient (_id) ON DELETE CASCADE,\n  emoji TEXT NOT NULL,\n  date_sent INTEGER NOT NULL,\n  date_received INTEGER NOT NULL,\n  UNIQUE(message_id, is_mms, author_id) ON CONFLICT REPLACE\n)";
    public static final String[] CREATE_TRIGGERS = {"\n        CREATE TRIGGER reactions_sms_delete AFTER DELETE ON sms \n        BEGIN \n        \tDELETE FROM reaction WHERE message_id = old._id AND is_mms = 0;\n        END\n      ", "\n        CREATE TRIGGER reactions_mms_delete AFTER DELETE ON mms \n        BEGIN \n        \tDELETE FROM reaction WHERE message_id = old._id AND is_mms = 1;\n        END\n      "};
    public static final Companion Companion = new Companion(null);
    private static final String DATE_RECEIVED;
    private static final String DATE_SENT;
    private static final String EMOJI;
    private static final String ID;
    public static final String IS_MMS;
    public static final String MESSAGE_ID;
    public static final String TABLE_NAME;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ReactionDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    /* compiled from: ReactionDatabase.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0018\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u00078\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\bR\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/database/ReactionDatabase$Companion;", "", "()V", "AUTHOR_ID", "", "CREATE_TABLE", "CREATE_TRIGGERS", "", "[Ljava/lang/String;", "DATE_RECEIVED", "DATE_SENT", "EMOJI", "ID", "IS_MMS", "MESSAGE_ID", "TABLE_NAME", "readReaction", "Lorg/thoughtcrime/securesms/database/model/ReactionRecord;", "cursor", "Landroid/database/Cursor;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final ReactionRecord readReaction(Cursor cursor) {
            String requireString = CursorUtil.requireString(cursor, "emoji");
            Intrinsics.checkNotNullExpressionValue(requireString, "requireString(cursor, EMOJI)");
            RecipientId from = RecipientId.from(CursorUtil.requireLong(cursor, ReactionDatabase.AUTHOR_ID));
            Intrinsics.checkNotNullExpressionValue(from, "from(CursorUtil.requireLong(cursor, AUTHOR_ID))");
            return new ReactionRecord(requireString, from, CursorUtil.requireLong(cursor, "date_sent"), CursorUtil.requireLong(cursor, "date_received"));
        }
    }

    public final List<ReactionRecord> getReactions(MessageId messageId) {
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        String[] buildArgs = SqlUtil.buildArgs(Long.valueOf(messageId.getId()), Integer.valueOf(messageId.isMms() ? 1 : 0));
        ArrayList arrayList = new ArrayList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, null, "message_id = ? AND is_mms = ?", buildArgs, null, null, null);
        Throwable th = null;
        while (query.moveToNext()) {
            try {
                Companion companion = Companion;
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                arrayList.add(companion.readReaction(query));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        return arrayList;
    }

    public final Map<MessageId, List<ReactionRecord>> getReactionsForMessages(Collection<MessageId> collection) {
        Intrinsics.checkNotNullParameter(collection, "messageIds");
        if (collection.isEmpty()) {
            return MapsKt__MapsKt.emptyMap();
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(collection, 10));
        for (MessageId messageId : collection) {
            arrayList.add(SqlUtil.buildArgs(Long.valueOf(messageId.getId()), Integer.valueOf(messageId.isMms() ? 1 : 0)));
        }
        for (SqlUtil.Query query : SqlUtil.buildCustomCollectionQuery("message_id = ? AND is_mms = ?", arrayList)) {
            Cursor query2 = getReadableDatabase().query(TABLE_NAME, null, query.getWhere(), query.getWhereArgs(), null, null, null);
            Throwable th = null;
            while (query2.moveToNext()) {
                try {
                    Companion companion = Companion;
                    Intrinsics.checkNotNullExpressionValue(query2, "cursor");
                    ReactionRecord readReaction = companion.readReaction(query2);
                    MessageId messageId2 = new MessageId(CursorUtil.requireLong(query2, "message_id"), CursorUtil.requireBoolean(query2, "is_mms"));
                    List list = (List) linkedHashMap.get(messageId2);
                    if (list == null) {
                        list = new ArrayList();
                        linkedHashMap.put(messageId2, list);
                    }
                    list.add(readReaction);
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        return linkedHashMap;
    }

    /* JADX INFO: finally extract failed */
    public final void addReaction(MessageId messageId, ReactionRecord reactionRecord) {
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        Intrinsics.checkNotNullParameter(reactionRecord, TABLE_NAME);
        getWritableDatabase().beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_id", Long.valueOf(messageId.getId()));
            contentValues.put("is_mms", Integer.valueOf(messageId.isMms() ? 1 : 0));
            contentValues.put("emoji", reactionRecord.getEmoji());
            contentValues.put(AUTHOR_ID, reactionRecord.getAuthor().serialize());
            contentValues.put("date_sent", Long.valueOf(reactionRecord.getDateSent()));
            contentValues.put("date_received", Long.valueOf(reactionRecord.getDateReceived()));
            getWritableDatabase().insert(TABLE_NAME, (String) null, contentValues);
            if (messageId.isMms()) {
                SignalDatabase.Companion.mms().updateReactionsUnread(getWritableDatabase(), messageId.getId(), hasReactions(messageId), false);
            } else {
                SignalDatabase.Companion.sms().updateReactionsUnread(getWritableDatabase(), messageId.getId(), hasReactions(messageId), false);
            }
            getWritableDatabase().setTransactionSuccessful();
            getWritableDatabase().endTransaction();
            ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(messageId);
        } catch (Throwable th) {
            getWritableDatabase().endTransaction();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public final void deleteReaction(MessageId messageId, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        getWritableDatabase().beginTransaction();
        try {
            Object[] objArr = new Object[3];
            int i = 0;
            objArr[0] = Long.valueOf(messageId.getId());
            if (messageId.isMms()) {
                i = 1;
            }
            objArr[1] = Integer.valueOf(i);
            objArr[2] = recipientId;
            getWritableDatabase().delete(TABLE_NAME, "message_id = ? AND is_mms = ? AND author_id = ?", SqlUtil.buildArgs(objArr));
            if (messageId.isMms()) {
                SignalDatabase.Companion.mms().updateReactionsUnread(getWritableDatabase(), messageId.getId(), hasReactions(messageId), true);
            } else {
                SignalDatabase.Companion.sms().updateReactionsUnread(getWritableDatabase(), messageId.getId(), hasReactions(messageId), true);
            }
            getWritableDatabase().setTransactionSuccessful();
            getWritableDatabase().endTransaction();
            ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(messageId);
        } catch (Throwable th) {
            getWritableDatabase().endTransaction();
            throw th;
        }
    }

    public final void deleteReactions(MessageId messageId) {
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        getWritableDatabase().delete(TABLE_NAME, "message_id = ? AND is_mms = ?", SqlUtil.buildArgs(Long.valueOf(messageId.getId()), Integer.valueOf(messageId.isMms() ? 1 : 0)));
    }

    public final boolean hasReaction(MessageId messageId, ReactionRecord reactionRecord) {
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        Intrinsics.checkNotNullParameter(reactionRecord, TABLE_NAME);
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"message_id"}, "message_id = ? AND is_mms = ? AND author_id = ? AND emoji = ?", SqlUtil.buildArgs(Long.valueOf(messageId.getId()), Integer.valueOf(messageId.isMms() ? 1 : 0), reactionRecord.getAuthor(), reactionRecord.getEmoji()), null, null, null);
        Throwable th = null;
        try {
            return query.moveToFirst();
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    private final boolean hasReactions(MessageId messageId) {
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"message_id"}, "message_id = ? AND is_mms = ?", SqlUtil.buildArgs(Long.valueOf(messageId.getId()), Integer.valueOf(messageId.isMms() ? 1 : 0)), null, null, null);
        Throwable th = null;
        try {
            return query.moveToFirst();
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final void remapRecipient(RecipientId recipientId, RecipientId recipientId2) {
        Intrinsics.checkNotNullParameter(recipientId, "oldAuthorId");
        Intrinsics.checkNotNullParameter(recipientId2, "newAuthorId");
        String[] buildArgs = SqlUtil.buildArgs(recipientId);
        ContentValues contentValues = new ContentValues();
        contentValues.put(AUTHOR_ID, recipientId2.serialize());
        getReadableDatabase().update(TABLE_NAME, contentValues, "author_id = ?", buildArgs);
    }

    public final void deleteAbandonedReactions() {
        getWritableDatabase().delete(TABLE_NAME, "(is_mms = 0 AND message_id NOT IN (SELECT _id FROM sms))\nOR\n(is_mms = 1 AND message_id NOT IN (SELECT _id FROM mms))", (String[]) null);
    }
}
