package org.thoughtcrime.securesms.database;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;

/* compiled from: PnpOperations.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\t\u0003\u0004\u0005\u0006\u0007\b\t\n\u000bB\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\t\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpOperation;", "", "()V", "ChangeNumberInsert", "Merge", "RemoveE164", "RemovePni", "SessionSwitchoverInsert", "SetAci", "SetE164", "SetPni", "Update", "Lorg/thoughtcrime/securesms/database/PnpOperation$Update;", "Lorg/thoughtcrime/securesms/database/PnpOperation$RemoveE164;", "Lorg/thoughtcrime/securesms/database/PnpOperation$RemovePni;", "Lorg/thoughtcrime/securesms/database/PnpOperation$SetE164;", "Lorg/thoughtcrime/securesms/database/PnpOperation$SetPni;", "Lorg/thoughtcrime/securesms/database/PnpOperation$SetAci;", "Lorg/thoughtcrime/securesms/database/PnpOperation$Merge;", "Lorg/thoughtcrime/securesms/database/PnpOperation$SessionSwitchoverInsert;", "Lorg/thoughtcrime/securesms/database/PnpOperation$ChangeNumberInsert;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class PnpOperation {
    public /* synthetic */ PnpOperation(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    private PnpOperation() {
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\tHÆ\u0003J7\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\tHÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bHÖ\u0003J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u0005HÖ\u0001R\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpOperation$Update;", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", CdsDatabase.E164, "", RecipientDatabase.PNI_COLUMN, "Lorg/whispersystems/signalservice/api/push/PNI;", "aci", "Lorg/whispersystems/signalservice/api/push/ACI;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Ljava/lang/String;Lorg/whispersystems/signalservice/api/push/PNI;Lorg/whispersystems/signalservice/api/push/ACI;)V", "getAci", "()Lorg/whispersystems/signalservice/api/push/ACI;", "getE164", "()Ljava/lang/String;", "getPni", "()Lorg/whispersystems/signalservice/api/push/PNI;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Update extends PnpOperation {
        private final ACI aci;
        private final String e164;
        private final PNI pni;
        private final RecipientId recipientId;

        public static /* synthetic */ Update copy$default(Update update, RecipientId recipientId, String str, PNI pni, ACI aci, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = update.recipientId;
            }
            if ((i & 2) != 0) {
                str = update.e164;
            }
            if ((i & 4) != 0) {
                pni = update.pni;
            }
            if ((i & 8) != 0) {
                aci = update.aci;
            }
            return update.copy(recipientId, str, pni, aci);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final String component2() {
            return this.e164;
        }

        public final PNI component3() {
            return this.pni;
        }

        public final ACI component4() {
            return this.aci;
        }

        public final Update copy(RecipientId recipientId, String str, PNI pni, ACI aci) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return new Update(recipientId, str, pni, aci);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Update)) {
                return false;
            }
            Update update = (Update) obj;
            return Intrinsics.areEqual(this.recipientId, update.recipientId) && Intrinsics.areEqual(this.e164, update.e164) && Intrinsics.areEqual(this.pni, update.pni) && Intrinsics.areEqual(this.aci, update.aci);
        }

        public int hashCode() {
            int hashCode = this.recipientId.hashCode() * 31;
            String str = this.e164;
            int i = 0;
            int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
            PNI pni = this.pni;
            int hashCode3 = (hashCode2 + (pni == null ? 0 : pni.hashCode())) * 31;
            ACI aci = this.aci;
            if (aci != null) {
                i = aci.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            return "Update(recipientId=" + this.recipientId + ", e164=" + this.e164 + ", pni=" + this.pni + ", aci=" + this.aci + ')';
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final String getE164() {
            return this.e164;
        }

        public final PNI getPni() {
            return this.pni;
        }

        public final ACI getAci() {
            return this.aci;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Update(RecipientId recipientId, String str, PNI pni, ACI aci) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
            this.e164 = str;
            this.pni = pni;
            this.aci = aci;
        }
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpOperation$RemoveE164;", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RemoveE164 extends PnpOperation {
        private final RecipientId recipientId;

        public static /* synthetic */ RemoveE164 copy$default(RemoveE164 removeE164, RecipientId recipientId, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = removeE164.recipientId;
            }
            return removeE164.copy(recipientId);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final RemoveE164 copy(RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return new RemoveE164(recipientId);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof RemoveE164) && Intrinsics.areEqual(this.recipientId, ((RemoveE164) obj).recipientId);
        }

        public int hashCode() {
            return this.recipientId.hashCode();
        }

        public String toString() {
            return "RemoveE164(recipientId=" + this.recipientId + ')';
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public RemoveE164(RecipientId recipientId) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
        }
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpOperation$RemovePni;", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RemovePni extends PnpOperation {
        private final RecipientId recipientId;

        public static /* synthetic */ RemovePni copy$default(RemovePni removePni, RecipientId recipientId, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = removePni.recipientId;
            }
            return removePni.copy(recipientId);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final RemovePni copy(RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return new RemovePni(recipientId);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof RemovePni) && Intrinsics.areEqual(this.recipientId, ((RemovePni) obj).recipientId);
        }

        public int hashCode() {
            return this.recipientId.hashCode();
        }

        public String toString() {
            return "RemovePni(recipientId=" + this.recipientId + ')';
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public RemovePni(RecipientId recipientId) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
        }
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpOperation$SetE164;", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", CdsDatabase.E164, "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Ljava/lang/String;)V", "getE164", "()Ljava/lang/String;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SetE164 extends PnpOperation {
        private final String e164;
        private final RecipientId recipientId;

        public static /* synthetic */ SetE164 copy$default(SetE164 setE164, RecipientId recipientId, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = setE164.recipientId;
            }
            if ((i & 2) != 0) {
                str = setE164.e164;
            }
            return setE164.copy(recipientId, str);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final String component2() {
            return this.e164;
        }

        public final SetE164 copy(RecipientId recipientId, String str) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
            return new SetE164(recipientId, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SetE164)) {
                return false;
            }
            SetE164 setE164 = (SetE164) obj;
            return Intrinsics.areEqual(this.recipientId, setE164.recipientId) && Intrinsics.areEqual(this.e164, setE164.e164);
        }

        public int hashCode() {
            return (this.recipientId.hashCode() * 31) + this.e164.hashCode();
        }

        public String toString() {
            return "SetE164(recipientId=" + this.recipientId + ", e164=" + this.e164 + ')';
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final String getE164() {
            return this.e164;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SetE164(RecipientId recipientId, String str) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
            this.recipientId = recipientId;
            this.e164 = str;
        }
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpOperation$SetPni;", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", RecipientDatabase.PNI_COLUMN, "Lorg/whispersystems/signalservice/api/push/PNI;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/whispersystems/signalservice/api/push/PNI;)V", "getPni", "()Lorg/whispersystems/signalservice/api/push/PNI;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SetPni extends PnpOperation {
        private final PNI pni;
        private final RecipientId recipientId;

        public static /* synthetic */ SetPni copy$default(SetPni setPni, RecipientId recipientId, PNI pni, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = setPni.recipientId;
            }
            if ((i & 2) != 0) {
                pni = setPni.pni;
            }
            return setPni.copy(recipientId, pni);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final PNI component2() {
            return this.pni;
        }

        public final SetPni copy(RecipientId recipientId, PNI pni) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(pni, RecipientDatabase.PNI_COLUMN);
            return new SetPni(recipientId, pni);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SetPni)) {
                return false;
            }
            SetPni setPni = (SetPni) obj;
            return Intrinsics.areEqual(this.recipientId, setPni.recipientId) && Intrinsics.areEqual(this.pni, setPni.pni);
        }

        public int hashCode() {
            return (this.recipientId.hashCode() * 31) + this.pni.hashCode();
        }

        public String toString() {
            return "SetPni(recipientId=" + this.recipientId + ", pni=" + this.pni + ')';
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final PNI getPni() {
            return this.pni;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SetPni(RecipientId recipientId, PNI pni) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(pni, RecipientDatabase.PNI_COLUMN);
            this.recipientId = recipientId;
            this.pni = pni;
        }
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpOperation$SetAci;", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "aci", "Lorg/whispersystems/signalservice/api/push/ACI;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/whispersystems/signalservice/api/push/ACI;)V", "getAci", "()Lorg/whispersystems/signalservice/api/push/ACI;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SetAci extends PnpOperation {
        private final ACI aci;
        private final RecipientId recipientId;

        public static /* synthetic */ SetAci copy$default(SetAci setAci, RecipientId recipientId, ACI aci, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = setAci.recipientId;
            }
            if ((i & 2) != 0) {
                aci = setAci.aci;
            }
            return setAci.copy(recipientId, aci);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final ACI component2() {
            return this.aci;
        }

        public final SetAci copy(RecipientId recipientId, ACI aci) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(aci, "aci");
            return new SetAci(recipientId, aci);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SetAci)) {
                return false;
            }
            SetAci setAci = (SetAci) obj;
            return Intrinsics.areEqual(this.recipientId, setAci.recipientId) && Intrinsics.areEqual(this.aci, setAci.aci);
        }

        public int hashCode() {
            return (this.recipientId.hashCode() * 31) + this.aci.hashCode();
        }

        public String toString() {
            return "SetAci(recipientId=" + this.recipientId + ", aci=" + this.aci + ')';
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final ACI getAci() {
            return this.aci;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SetAci(RecipientId recipientId, ACI aci) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(aci, "aci");
            this.recipientId = recipientId;
            this.aci = aci;
        }
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fHÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpOperation$Merge;", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "primaryId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "secondaryId", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getPrimaryId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getSecondaryId", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Merge extends PnpOperation {
        private final RecipientId primaryId;
        private final RecipientId secondaryId;

        public static /* synthetic */ Merge copy$default(Merge merge, RecipientId recipientId, RecipientId recipientId2, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = merge.primaryId;
            }
            if ((i & 2) != 0) {
                recipientId2 = merge.secondaryId;
            }
            return merge.copy(recipientId, recipientId2);
        }

        public final RecipientId component1() {
            return this.primaryId;
        }

        public final RecipientId component2() {
            return this.secondaryId;
        }

        public final Merge copy(RecipientId recipientId, RecipientId recipientId2) {
            Intrinsics.checkNotNullParameter(recipientId, "primaryId");
            Intrinsics.checkNotNullParameter(recipientId2, "secondaryId");
            return new Merge(recipientId, recipientId2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Merge)) {
                return false;
            }
            Merge merge = (Merge) obj;
            return Intrinsics.areEqual(this.primaryId, merge.primaryId) && Intrinsics.areEqual(this.secondaryId, merge.secondaryId);
        }

        public int hashCode() {
            return (this.primaryId.hashCode() * 31) + this.secondaryId.hashCode();
        }

        public String toString() {
            return "Merge(primaryId=" + this.primaryId + ", secondaryId=" + this.secondaryId + ')';
        }

        public final RecipientId getPrimaryId() {
            return this.primaryId;
        }

        public final RecipientId getSecondaryId() {
            return this.secondaryId;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Merge(RecipientId recipientId, RecipientId recipientId2) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "primaryId");
            Intrinsics.checkNotNullParameter(recipientId2, "secondaryId");
            this.primaryId = recipientId;
            this.secondaryId = recipientId2;
        }
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpOperation$SessionSwitchoverInsert;", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SessionSwitchoverInsert extends PnpOperation {
        private final RecipientId recipientId;

        public static /* synthetic */ SessionSwitchoverInsert copy$default(SessionSwitchoverInsert sessionSwitchoverInsert, RecipientId recipientId, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = sessionSwitchoverInsert.recipientId;
            }
            return sessionSwitchoverInsert.copy(recipientId);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final SessionSwitchoverInsert copy(RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return new SessionSwitchoverInsert(recipientId);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof SessionSwitchoverInsert) && Intrinsics.areEqual(this.recipientId, ((SessionSwitchoverInsert) obj).recipientId);
        }

        public int hashCode() {
            return this.recipientId.hashCode();
        }

        public String toString() {
            return "SessionSwitchoverInsert(recipientId=" + this.recipientId + ')';
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SessionSwitchoverInsert(RecipientId recipientId) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
        }
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpOperation$ChangeNumberInsert;", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "oldE164", "", "newE164", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Ljava/lang/String;Ljava/lang/String;)V", "getNewE164", "()Ljava/lang/String;", "getOldE164", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ChangeNumberInsert extends PnpOperation {
        private final String newE164;
        private final String oldE164;
        private final RecipientId recipientId;

        public static /* synthetic */ ChangeNumberInsert copy$default(ChangeNumberInsert changeNumberInsert, RecipientId recipientId, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = changeNumberInsert.recipientId;
            }
            if ((i & 2) != 0) {
                str = changeNumberInsert.oldE164;
            }
            if ((i & 4) != 0) {
                str2 = changeNumberInsert.newE164;
            }
            return changeNumberInsert.copy(recipientId, str, str2);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final String component2() {
            return this.oldE164;
        }

        public final String component3() {
            return this.newE164;
        }

        public final ChangeNumberInsert copy(RecipientId recipientId, String str, String str2) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(str, "oldE164");
            Intrinsics.checkNotNullParameter(str2, "newE164");
            return new ChangeNumberInsert(recipientId, str, str2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ChangeNumberInsert)) {
                return false;
            }
            ChangeNumberInsert changeNumberInsert = (ChangeNumberInsert) obj;
            return Intrinsics.areEqual(this.recipientId, changeNumberInsert.recipientId) && Intrinsics.areEqual(this.oldE164, changeNumberInsert.oldE164) && Intrinsics.areEqual(this.newE164, changeNumberInsert.newE164);
        }

        public int hashCode() {
            return (((this.recipientId.hashCode() * 31) + this.oldE164.hashCode()) * 31) + this.newE164.hashCode();
        }

        public String toString() {
            return "ChangeNumberInsert(recipientId=" + this.recipientId + ", oldE164=" + this.oldE164 + ", newE164=" + this.newE164 + ')';
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final String getOldE164() {
            return this.oldE164;
        }

        public final String getNewE164() {
            return this.newE164;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ChangeNumberInsert(RecipientId recipientId, String str, String str2) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(str, "oldE164");
            Intrinsics.checkNotNullParameter(str2, "newE164");
            this.recipientId = recipientId;
            this.oldE164 = str;
            this.newE164 = str2;
        }
    }
}
