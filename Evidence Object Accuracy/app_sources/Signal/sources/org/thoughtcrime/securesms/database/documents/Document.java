package org.thoughtcrime.securesms.database.documents;

import java.util.Set;

/* loaded from: classes4.dex */
public interface Document<T> {
    Set<T> getItems();

    int size();
}
