package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes4.dex */
public final class ExpiringProfileKeyCredentialColumnData extends GeneratedMessageLite<ExpiringProfileKeyCredentialColumnData, Builder> implements ExpiringProfileKeyCredentialColumnDataOrBuilder {
    private static final ExpiringProfileKeyCredentialColumnData DEFAULT_INSTANCE;
    public static final int EXPIRINGPROFILEKEYCREDENTIAL_FIELD_NUMBER;
    private static volatile Parser<ExpiringProfileKeyCredentialColumnData> PARSER;
    public static final int PROFILEKEY_FIELD_NUMBER;
    private ByteString expiringProfileKeyCredential_;
    private ByteString profileKey_;

    private ExpiringProfileKeyCredentialColumnData() {
        ByteString byteString = ByteString.EMPTY;
        this.profileKey_ = byteString;
        this.expiringProfileKeyCredential_ = byteString;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ExpiringProfileKeyCredentialColumnDataOrBuilder
    public ByteString getProfileKey() {
        return this.profileKey_;
    }

    public void setProfileKey(ByteString byteString) {
        byteString.getClass();
        this.profileKey_ = byteString;
    }

    public void clearProfileKey() {
        this.profileKey_ = getDefaultInstance().getProfileKey();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ExpiringProfileKeyCredentialColumnDataOrBuilder
    public ByteString getExpiringProfileKeyCredential() {
        return this.expiringProfileKeyCredential_;
    }

    public void setExpiringProfileKeyCredential(ByteString byteString) {
        byteString.getClass();
        this.expiringProfileKeyCredential_ = byteString;
    }

    public void clearExpiringProfileKeyCredential() {
        this.expiringProfileKeyCredential_ = getDefaultInstance().getExpiringProfileKeyCredential();
    }

    public static ExpiringProfileKeyCredentialColumnData parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ExpiringProfileKeyCredentialColumnData parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ExpiringProfileKeyCredentialColumnData parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ExpiringProfileKeyCredentialColumnData parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ExpiringProfileKeyCredentialColumnData parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ExpiringProfileKeyCredentialColumnData parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ExpiringProfileKeyCredentialColumnData parseFrom(InputStream inputStream) throws IOException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ExpiringProfileKeyCredentialColumnData parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ExpiringProfileKeyCredentialColumnData parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ExpiringProfileKeyCredentialColumnData parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ExpiringProfileKeyCredentialColumnData parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ExpiringProfileKeyCredentialColumnData parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ExpiringProfileKeyCredentialColumnData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ExpiringProfileKeyCredentialColumnData expiringProfileKeyCredentialColumnData) {
        return DEFAULT_INSTANCE.createBuilder(expiringProfileKeyCredentialColumnData);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ExpiringProfileKeyCredentialColumnData, Builder> implements ExpiringProfileKeyCredentialColumnDataOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(ExpiringProfileKeyCredentialColumnData.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ExpiringProfileKeyCredentialColumnDataOrBuilder
        public ByteString getProfileKey() {
            return ((ExpiringProfileKeyCredentialColumnData) this.instance).getProfileKey();
        }

        public Builder setProfileKey(ByteString byteString) {
            copyOnWrite();
            ((ExpiringProfileKeyCredentialColumnData) this.instance).setProfileKey(byteString);
            return this;
        }

        public Builder clearProfileKey() {
            copyOnWrite();
            ((ExpiringProfileKeyCredentialColumnData) this.instance).clearProfileKey();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ExpiringProfileKeyCredentialColumnDataOrBuilder
        public ByteString getExpiringProfileKeyCredential() {
            return ((ExpiringProfileKeyCredentialColumnData) this.instance).getExpiringProfileKeyCredential();
        }

        public Builder setExpiringProfileKeyCredential(ByteString byteString) {
            copyOnWrite();
            ((ExpiringProfileKeyCredentialColumnData) this.instance).setExpiringProfileKeyCredential(byteString);
            return this;
        }

        public Builder clearExpiringProfileKeyCredential() {
            copyOnWrite();
            ((ExpiringProfileKeyCredentialColumnData) this.instance).clearExpiringProfileKeyCredential();
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.ExpiringProfileKeyCredentialColumnData$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ExpiringProfileKeyCredentialColumnData();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\n", new Object[]{"profileKey_", "expiringProfileKeyCredential_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ExpiringProfileKeyCredentialColumnData> parser = PARSER;
                if (parser == null) {
                    synchronized (ExpiringProfileKeyCredentialColumnData.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ExpiringProfileKeyCredentialColumnData expiringProfileKeyCredentialColumnData = new ExpiringProfileKeyCredentialColumnData();
        DEFAULT_INSTANCE = expiringProfileKeyCredentialColumnData;
        GeneratedMessageLite.registerDefaultInstance(ExpiringProfileKeyCredentialColumnData.class, expiringProfileKeyCredentialColumnData);
    }

    public static ExpiringProfileKeyCredentialColumnData getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ExpiringProfileKeyCredentialColumnData> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
