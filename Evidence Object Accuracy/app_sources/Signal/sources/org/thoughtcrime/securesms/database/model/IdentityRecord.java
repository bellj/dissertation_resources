package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.libsignal.protocol.IdentityKey;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: IdentityRecord.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0017\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\t¢\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001c\u001a\u00020\tHÆ\u0003J\t\u0010\u001d\u001a\u00020\u000bHÆ\u0003J\t\u0010\u001e\u001a\u00020\tHÆ\u0003JE\u0010\u001f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\tHÆ\u0001J\u0013\u0010 \u001a\u00020\t2\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\"\u001a\u00020#HÖ\u0001J\t\u0010$\u001a\u00020%HÖ\u0001R\u0013\u0010\b\u001a\u00020\t8\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0013\u0010\f\u001a\u00020\t8\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "identityKey", "Lorg/signal/libsignal/protocol/IdentityKey;", "verifiedStatus", "Lorg/thoughtcrime/securesms/database/IdentityDatabase$VerifiedStatus;", "firstUse", "", "timestamp", "", "nonblockingApproval", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/signal/libsignal/protocol/IdentityKey;Lorg/thoughtcrime/securesms/database/IdentityDatabase$VerifiedStatus;ZJZ)V", "isFirstUse", "()Z", "getIdentityKey", "()Lorg/signal/libsignal/protocol/IdentityKey;", "isApprovedNonBlocking", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getTimestamp", "()J", "getVerifiedStatus", "()Lorg/thoughtcrime/securesms/database/IdentityDatabase$VerifiedStatus;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class IdentityRecord {
    private final boolean firstUse;
    private final IdentityKey identityKey;
    private final boolean nonblockingApproval;
    private final RecipientId recipientId;
    private final long timestamp;
    private final IdentityDatabase.VerifiedStatus verifiedStatus;

    public static /* synthetic */ IdentityRecord copy$default(IdentityRecord identityRecord, RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, long j, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            recipientId = identityRecord.recipientId;
        }
        if ((i & 2) != 0) {
            identityKey = identityRecord.identityKey;
        }
        if ((i & 4) != 0) {
            verifiedStatus = identityRecord.verifiedStatus;
        }
        if ((i & 8) != 0) {
            z = identityRecord.firstUse;
        }
        if ((i & 16) != 0) {
            j = identityRecord.timestamp;
        }
        if ((i & 32) != 0) {
            z2 = identityRecord.nonblockingApproval;
        }
        return identityRecord.copy(recipientId, identityKey, verifiedStatus, z, j, z2);
    }

    public final RecipientId component1() {
        return this.recipientId;
    }

    public final IdentityKey component2() {
        return this.identityKey;
    }

    public final IdentityDatabase.VerifiedStatus component3() {
        return this.verifiedStatus;
    }

    public final boolean component4() {
        return this.firstUse;
    }

    public final long component5() {
        return this.timestamp;
    }

    public final boolean component6() {
        return this.nonblockingApproval;
    }

    public final IdentityRecord copy(RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, long j, boolean z2) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(identityKey, "identityKey");
        Intrinsics.checkNotNullParameter(verifiedStatus, "verifiedStatus");
        return new IdentityRecord(recipientId, identityKey, verifiedStatus, z, j, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IdentityRecord)) {
            return false;
        }
        IdentityRecord identityRecord = (IdentityRecord) obj;
        return Intrinsics.areEqual(this.recipientId, identityRecord.recipientId) && Intrinsics.areEqual(this.identityKey, identityRecord.identityKey) && this.verifiedStatus == identityRecord.verifiedStatus && this.firstUse == identityRecord.firstUse && this.timestamp == identityRecord.timestamp && this.nonblockingApproval == identityRecord.nonblockingApproval;
    }

    public int hashCode() {
        int hashCode = ((((this.recipientId.hashCode() * 31) + this.identityKey.hashCode()) * 31) + this.verifiedStatus.hashCode()) * 31;
        boolean z = this.firstUse;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int m = (((hashCode + i2) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.timestamp)) * 31;
        boolean z2 = this.nonblockingApproval;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        return m + i;
    }

    public String toString() {
        return "IdentityRecord(recipientId=" + this.recipientId + ", identityKey=" + this.identityKey + ", verifiedStatus=" + this.verifiedStatus + ", firstUse=" + this.firstUse + ", timestamp=" + this.timestamp + ", nonblockingApproval=" + this.nonblockingApproval + ')';
    }

    public IdentityRecord(RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, long j, boolean z2) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(identityKey, "identityKey");
        Intrinsics.checkNotNullParameter(verifiedStatus, "verifiedStatus");
        this.recipientId = recipientId;
        this.identityKey = identityKey;
        this.verifiedStatus = verifiedStatus;
        this.firstUse = z;
        this.timestamp = j;
        this.nonblockingApproval = z2;
    }

    public final RecipientId getRecipientId() {
        return this.recipientId;
    }

    public final IdentityKey getIdentityKey() {
        return this.identityKey;
    }

    public final IdentityDatabase.VerifiedStatus getVerifiedStatus() {
        return this.verifiedStatus;
    }

    public final boolean isFirstUse() {
        return this.firstUse;
    }

    public final long getTimestamp() {
        return this.timestamp;
    }

    public final boolean isApprovedNonBlocking() {
        return this.nonblockingApproval;
    }
}
