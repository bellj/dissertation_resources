package org.thoughtcrime.securesms.database;

import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.thoughtcrime.securesms.database.model.RecipientRecord;

/* compiled from: PnpOperations.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/database/model/RecipientRecord;", "it", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PnpDataSet$perform$2 extends Lambda implements Function1<RecipientRecord, RecipientRecord> {
    public static final PnpDataSet$perform$2 INSTANCE = new PnpDataSet$perform$2();

    PnpDataSet$perform$2() {
        super(1);
    }

    public final RecipientRecord invoke(RecipientRecord recipientRecord) {
        Intrinsics.checkNotNullParameter(recipientRecord, "it");
        return recipientRecord.copy((r78 & 1) != 0 ? recipientRecord.id : null, (r78 & 2) != 0 ? recipientRecord.serviceId : null, (r78 & 4) != 0 ? recipientRecord.pni : null, (r78 & 8) != 0 ? recipientRecord.username : null, (r78 & 16) != 0 ? recipientRecord.e164 : null, (r78 & 32) != 0 ? recipientRecord.email : null, (r78 & 64) != 0 ? recipientRecord.groupId : null, (r78 & 128) != 0 ? recipientRecord.distributionListId : null, (r78 & 256) != 0 ? recipientRecord.groupType : null, (r78 & 512) != 0 ? recipientRecord.isBlocked : false, (r78 & 1024) != 0 ? recipientRecord.muteUntil : 0, (r78 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? recipientRecord.messageVibrateState : null, (r78 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? recipientRecord.callVibrateState : null, (r78 & 8192) != 0 ? recipientRecord.messageRingtone : null, (r78 & 16384) != 0 ? recipientRecord.callRingtone : null, (r78 & 32768) != 0 ? recipientRecord.defaultSubscriptionId : 0, (r78 & 65536) != 0 ? recipientRecord.expireMessages : 0, (r78 & 131072) != 0 ? recipientRecord.registered : null, (r78 & 262144) != 0 ? recipientRecord.profileKey : null, (r78 & 524288) != 0 ? recipientRecord.expiringProfileKeyCredential : null, (r78 & 1048576) != 0 ? recipientRecord.systemProfileName : null, (r78 & 2097152) != 0 ? recipientRecord.systemDisplayName : null, (r78 & 4194304) != 0 ? recipientRecord.systemContactPhotoUri : null, (r78 & 8388608) != 0 ? recipientRecord.systemPhoneLabel : null, (r78 & 16777216) != 0 ? recipientRecord.systemContactUri : null, (r78 & 33554432) != 0 ? recipientRecord.signalProfileName : null, (r78 & 67108864) != 0 ? recipientRecord.signalProfileAvatar : null, (r78 & 134217728) != 0 ? recipientRecord.profileAvatarFileDetails : null, (r78 & SQLiteDatabase.CREATE_IF_NECESSARY) != 0 ? recipientRecord.profileSharing : false, (r78 & SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING) != 0 ? recipientRecord.lastProfileFetch : 0, (r78 & 1073741824) != 0 ? recipientRecord.notificationChannel : null, (r78 & Integer.MIN_VALUE) != 0 ? recipientRecord.unidentifiedAccessMode : null, (r79 & 1) != 0 ? recipientRecord.forceSmsSelection : false, (r79 & 2) != 0 ? recipientRecord.rawCapabilities : 0, (r79 & 4) != 0 ? recipientRecord.groupsV1MigrationCapability : null, (r79 & 8) != 0 ? recipientRecord.senderKeyCapability : null, (r79 & 16) != 0 ? recipientRecord.announcementGroupCapability : null, (r79 & 32) != 0 ? recipientRecord.changeNumberCapability : null, (r79 & 64) != 0 ? recipientRecord.storiesCapability : null, (r79 & 128) != 0 ? recipientRecord.giftBadgesCapability : null, (r79 & 256) != 0 ? recipientRecord.insightsBannerTier : null, (r79 & 512) != 0 ? recipientRecord.storageId : null, (r79 & 1024) != 0 ? recipientRecord.mentionSetting : null, (r79 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? recipientRecord.wallpaper : null, (r79 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? recipientRecord.chatColors : null, (r79 & 8192) != 0 ? recipientRecord.avatarColor : null, (r79 & 16384) != 0 ? recipientRecord.about : null, (r79 & 32768) != 0 ? recipientRecord.aboutEmoji : null, (r79 & 65536) != 0 ? recipientRecord.syncExtras : null, (r79 & 131072) != 0 ? recipientRecord.extras : null, (r79 & 262144) != 0 ? recipientRecord.hasGroupsInCommon : false, (r79 & 524288) != 0 ? recipientRecord.badges : null);
    }
}
