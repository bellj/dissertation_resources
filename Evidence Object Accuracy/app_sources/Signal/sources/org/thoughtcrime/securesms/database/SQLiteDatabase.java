package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteTransactionListener;
import android.os.CancellationSignal;
import android.util.Pair;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteQuery;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import net.zetetic.database.sqlcipher.SQLiteStatement;
import org.signal.core.util.tracing.Tracer;

/* loaded from: classes4.dex */
public class SQLiteDatabase implements SupportSQLiteDatabase {
    public static final int CONFLICT_ABORT;
    public static final int CONFLICT_FAIL;
    public static final int CONFLICT_IGNORE;
    public static final int CONFLICT_NONE;
    public static final int CONFLICT_REPLACE;
    public static final int CONFLICT_ROLLBACK;
    private static final String KEY_QUERY;
    private static final String KEY_TABLE;
    private static final String KEY_THREAD;
    private static final String NAME_LOCK;
    private static final ThreadLocal<Set<Runnable>> PENDING_POST_SUCCESSFUL_TRANSACTION_TASKS;
    private static final ThreadLocal<Set<Runnable>> POST_SUCCESSFUL_TRANSACTION_TASKS = new ThreadLocal<>();
    private final Tracer tracer = Tracer.getInstance();
    private final net.zetetic.database.sqlcipher.SQLiteDatabase wrapped;

    /* loaded from: classes4.dex */
    public interface Returnable<E> {
        E run();
    }

    static {
        ThreadLocal<Set<Runnable>> threadLocal = new ThreadLocal<>();
        PENDING_POST_SUCCESSFUL_TRANSACTION_TASKS = threadLocal;
        POST_SUCCESSFUL_TRANSACTION_TASKS = new ThreadLocal<>();
        threadLocal.set(new LinkedHashSet());
    }

    public SQLiteDatabase(net.zetetic.database.sqlcipher.SQLiteDatabase sQLiteDatabase) {
        this.wrapped = sQLiteDatabase;
    }

    private void traceLockStart() {
        this.tracer.start(NAME_LOCK, AttachmentDatabase.PREUPLOAD_MESSAGE_ID, "thread", Thread.currentThread().getName());
    }

    private void traceLockEnd() {
        this.tracer.end(NAME_LOCK, AttachmentDatabase.PREUPLOAD_MESSAGE_ID);
    }

    private void trace(String str, Runnable runnable) {
        this.tracer.start(str);
        runnable.run();
        this.tracer.end(str);
    }

    private void traceSql(String str, String str2, boolean z, Runnable runnable) {
        if (z) {
            traceLockStart();
        }
        this.tracer.start(str, KEY_QUERY, str2);
        runnable.run();
        this.tracer.end(str);
        if (z) {
            traceLockEnd();
        }
    }

    private <E> E traceSql(String str, String str2, boolean z, Returnable<E> returnable) {
        return (E) traceSql(str, null, str2, z, returnable);
    }

    private <E> E traceSql(String str, String str2, String str3, boolean z, Returnable<E> returnable) {
        if (z) {
            traceLockStart();
        }
        HashMap hashMap = new HashMap();
        if (str3 != null) {
            hashMap.put(KEY_QUERY, str3);
        }
        if (str2 != null) {
            hashMap.put(KEY_TABLE, str2);
        }
        this.tracer.start(str, hashMap);
        E run = returnable.run();
        this.tracer.end(str);
        if (z) {
            traceLockEnd();
        }
        return run;
    }

    public net.zetetic.database.sqlcipher.SQLiteDatabase getSqlCipherDatabase() {
        return this.wrapped;
    }

    public void runPostSuccessfulTransaction(Runnable runnable) {
        if (this.wrapped.inTransaction()) {
            getPendingPostSuccessfulTransactionTasks().add(runnable);
        } else {
            runnable.run();
        }
    }

    public void runPostSuccessfulTransaction(String str, Runnable runnable) {
        if (this.wrapped.inTransaction()) {
            getPendingPostSuccessfulTransactionTasks().add(new DedupedRunnable(str, runnable));
        } else {
            runnable.run();
        }
    }

    public Set<Runnable> getPendingPostSuccessfulTransactionTasks() {
        ThreadLocal<Set<Runnable>> threadLocal = PENDING_POST_SUCCESSFUL_TRANSACTION_TASKS;
        Set<Runnable> set = threadLocal.get();
        if (set != null) {
            return set;
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        threadLocal.set(linkedHashSet);
        return linkedHashSet;
    }

    public Set<Runnable> getPostSuccessfulTransactionTasks() {
        ThreadLocal<Set<Runnable>> threadLocal = POST_SUCCESSFUL_TRANSACTION_TASKS;
        Set<Runnable> set = threadLocal.get();
        if (set != null) {
            return set;
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        threadLocal.set(linkedHashSet);
        return linkedHashSet;
    }

    /* loaded from: classes4.dex */
    public static class DedupedRunnable implements Runnable {
        private final String key;
        private final Runnable runnable;

        protected DedupedRunnable(String str, Runnable runnable) {
            this.key = str;
            this.runnable = runnable;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.runnable.run();
        }

        @Override // java.lang.Object
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return this.key.equals(((DedupedRunnable) obj).key);
        }

        @Override // java.lang.Object
        public int hashCode() {
            return Objects.hash(this.key);
        }
    }

    public void beginTransactionWithListener(SQLiteTransactionListener sQLiteTransactionListener) {
        beginTransactionWithListener(new ConvertedTransactionListener(sQLiteTransactionListener));
    }

    public void beginTransactionWithListenerNonExclusive(SQLiteTransactionListener sQLiteTransactionListener) {
        beginTransactionWithListenerNonExclusive(new ConvertedTransactionListener(sQLiteTransactionListener));
    }

    @Override // androidx.sqlite.db.SupportSQLiteDatabase
    public Cursor query(String str) {
        return rawQuery(str, (String[]) null);
    }

    @Override // androidx.sqlite.db.SupportSQLiteDatabase
    public Cursor query(String str, Object[] objArr) {
        return rawQuery(str, objArr);
    }

    @Override // androidx.sqlite.db.SupportSQLiteDatabase
    public Cursor query(SupportSQLiteQuery supportSQLiteQuery) {
        DatabaseMonitor.onSql(supportSQLiteQuery.getSql(), null);
        return this.wrapped.query(supportSQLiteQuery);
    }

    public Cursor query(SupportSQLiteQuery supportSQLiteQuery, CancellationSignal cancellationSignal) {
        DatabaseMonitor.onSql(supportSQLiteQuery.getSql(), null);
        return this.wrapped.query(supportSQLiteQuery, cancellationSignal);
    }

    public long insert(String str, int i, ContentValues contentValues) throws SQLException {
        return insertWithOnConflict(str, null, contentValues, i);
    }

    @Override // androidx.sqlite.db.SupportSQLiteDatabase
    public int delete(String str, String str2, Object[] objArr) {
        return delete(str, str2, (String[]) objArr);
    }

    @Override // androidx.sqlite.db.SupportSQLiteDatabase
    public int update(String str, int i, ContentValues contentValues, String str2, Object[] objArr) {
        return updateWithOnConflict(str, contentValues, str2, (String[]) objArr, i);
    }

    public void setMaxSqlCacheSize(int i) {
        this.wrapped.setMaxSqlCacheSize(i);
    }

    public List<Pair<String, String>> getAttachedDbs() {
        return this.wrapped.getAttachedDbs();
    }

    public boolean isDatabaseIntegrityOk() {
        return this.wrapped.isDatabaseIntegrityOk();
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.wrapped.close();
    }

    @Override // androidx.sqlite.db.SupportSQLiteDatabase
    public void beginTransaction() {
        traceLockStart();
        if (this.wrapped.inTransaction()) {
            net.zetetic.database.sqlcipher.SQLiteDatabase sQLiteDatabase = this.wrapped;
            Objects.requireNonNull(sQLiteDatabase);
            trace("beginTransaction()", new Runnable() { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda18
                @Override // java.lang.Runnable
                public final void run() {
                    SQLiteDatabase.this.beginTransaction();
                }
            });
            return;
        }
        trace("beginTransaction()", new Runnable() { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda19
            @Override // java.lang.Runnable
            public final void run() {
                SQLiteDatabase.this.lambda$beginTransaction$0();
            }
        });
    }

    public /* synthetic */ void lambda$beginTransaction$0() {
        this.wrapped.beginTransactionWithListener(new net.zetetic.database.sqlcipher.SQLiteTransactionListener() { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase.1
            @Override // net.zetetic.database.sqlcipher.SQLiteTransactionListener
            public void onBegin() {
            }

            @Override // net.zetetic.database.sqlcipher.SQLiteTransactionListener
            public void onCommit() {
                Set pendingPostSuccessfulTransactionTasks = SQLiteDatabase.this.getPendingPostSuccessfulTransactionTasks();
                Set postSuccessfulTransactionTasks = SQLiteDatabase.this.getPostSuccessfulTransactionTasks();
                postSuccessfulTransactionTasks.clear();
                postSuccessfulTransactionTasks.addAll(pendingPostSuccessfulTransactionTasks);
                pendingPostSuccessfulTransactionTasks.clear();
            }

            @Override // net.zetetic.database.sqlcipher.SQLiteTransactionListener
            public void onRollback() {
                SQLiteDatabase.this.getPendingPostSuccessfulTransactionTasks().clear();
            }
        });
    }

    @Override // androidx.sqlite.db.SupportSQLiteDatabase
    public void endTransaction() {
        net.zetetic.database.sqlcipher.SQLiteDatabase sQLiteDatabase = this.wrapped;
        Objects.requireNonNull(sQLiteDatabase);
        trace("endTransaction()", new Runnable() { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda17
            @Override // java.lang.Runnable
            public final void run() {
                SQLiteDatabase.this.endTransaction();
            }
        });
        traceLockEnd();
        Set<Runnable> postSuccessfulTransactionTasks = getPostSuccessfulTransactionTasks();
        Iterator it = new HashSet(postSuccessfulTransactionTasks).iterator();
        while (it.hasNext()) {
            ((Runnable) it.next()).run();
        }
        postSuccessfulTransactionTasks.clear();
    }

    @Override // androidx.sqlite.db.SupportSQLiteDatabase
    public void setTransactionSuccessful() {
        net.zetetic.database.sqlcipher.SQLiteDatabase sQLiteDatabase = this.wrapped;
        Objects.requireNonNull(sQLiteDatabase);
        trace("setTransactionSuccessful()", new Runnable() { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda3
            @Override // java.lang.Runnable
            public final void run() {
                SQLiteDatabase.this.setTransactionSuccessful();
            }
        });
    }

    public Cursor query(boolean z, String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
        DatabaseMonitor.onQuery(z, str, strArr, str2, strArr2, str3, str4, str5, str6);
        return (Cursor) traceSql("query(9)", str, str2, false, new Returnable(z, str, strArr, str2, strArr2, str3, str4, str5, str6) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda8
            public final /* synthetic */ boolean f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ String[] f$3;
            public final /* synthetic */ String f$4;
            public final /* synthetic */ String[] f$5;
            public final /* synthetic */ String f$6;
            public final /* synthetic */ String f$7;
            public final /* synthetic */ String f$8;
            public final /* synthetic */ String f$9;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
                this.f$8 = r9;
                this.f$9 = r10;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$query$1(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9);
            }
        });
    }

    public /* synthetic */ Cursor lambda$query$1(boolean z, String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
        return this.wrapped.query(z, str, strArr, str2, strArr2, str3, str4, str5, str6);
    }

    public Cursor queryWithFactory(SQLiteDatabase.CursorFactory cursorFactory, boolean z, String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
        DatabaseMonitor.onQuery(z, str, strArr, str2, strArr2, str3, str4, str5, str6);
        return (Cursor) traceSql("queryWithFactory()", str, str2, false, new Returnable(cursorFactory, z, str, strArr, str2, strArr2, str3, str4, str5, str6) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda1
            public final /* synthetic */ SQLiteDatabase.CursorFactory f$1;
            public final /* synthetic */ String f$10;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ String[] f$4;
            public final /* synthetic */ String f$5;
            public final /* synthetic */ String[] f$6;
            public final /* synthetic */ String f$7;
            public final /* synthetic */ String f$8;
            public final /* synthetic */ String f$9;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
                this.f$8 = r9;
                this.f$9 = r10;
                this.f$10 = r11;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$queryWithFactory$2(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9, this.f$10);
            }
        });
    }

    public /* synthetic */ Cursor lambda$queryWithFactory$2(SQLiteDatabase.CursorFactory cursorFactory, boolean z, String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
        return this.wrapped.queryWithFactory(cursorFactory, z, str, strArr, str2, strArr2, str3, str4, str5, str6);
    }

    public Cursor query(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5) {
        DatabaseMonitor.onQuery(false, str, strArr, str2, strArr2, str3, str4, str5, null);
        return (Cursor) traceSql("query(7)", str, str2, false, new Returnable(str, strArr, str2, strArr2, str3, str4, str5) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda7
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String[] f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ String[] f$4;
            public final /* synthetic */ String f$5;
            public final /* synthetic */ String f$6;
            public final /* synthetic */ String f$7;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$query$3(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7);
            }
        });
    }

    public /* synthetic */ Cursor lambda$query$3(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5) {
        return this.wrapped.query(str, strArr, str2, strArr2, str3, str4, str5);
    }

    public Cursor query(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
        DatabaseMonitor.onQuery(false, str, strArr, str2, strArr2, str3, str4, str5, str6);
        return (Cursor) traceSql("query(8)", str, str2, false, new Returnable(str, strArr, str2, strArr2, str3, str4, str5, str6) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda12
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String[] f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ String[] f$4;
            public final /* synthetic */ String f$5;
            public final /* synthetic */ String f$6;
            public final /* synthetic */ String f$7;
            public final /* synthetic */ String f$8;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
                this.f$8 = r9;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$query$4(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8);
            }
        });
    }

    public /* synthetic */ Cursor lambda$query$4(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
        return this.wrapped.query(str, strArr, str2, strArr2, str3, str4, str5, str6);
    }

    public Cursor rawQuery(String str, String[] strArr) {
        DatabaseMonitor.onSql(str, strArr);
        return (Cursor) traceSql("rawQuery(2a)", str, false, (Returnable<Object>) new Returnable(str, strArr) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda14
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String[] f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$rawQuery$5(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ Cursor lambda$rawQuery$5(String str, String[] strArr) {
        return this.wrapped.rawQuery(str, strArr);
    }

    public Cursor rawQuery(String str, Object[] objArr) {
        DatabaseMonitor.onSql(str, objArr);
        return (Cursor) traceSql("rawQuery(2b)", str, false, (Returnable<Object>) new Returnable(str, objArr) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda13
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Object[] f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$rawQuery$6(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ Cursor lambda$rawQuery$6(String str, Object[] objArr) {
        return this.wrapped.rawQuery(str, objArr);
    }

    public Cursor rawQueryWithFactory(SQLiteDatabase.CursorFactory cursorFactory, String str, String[] strArr, String str2) {
        DatabaseMonitor.onSql(str, strArr);
        return (Cursor) traceSql("rawQueryWithFactory()", str, false, (Returnable<Object>) new Returnable(cursorFactory, str, strArr, str2) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda20
            public final /* synthetic */ SQLiteDatabase.CursorFactory f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ String[] f$3;
            public final /* synthetic */ String f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$rawQueryWithFactory$7(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    public /* synthetic */ Cursor lambda$rawQueryWithFactory$7(SQLiteDatabase.CursorFactory cursorFactory, String str, String[] strArr, String str2) {
        return this.wrapped.rawQueryWithFactory(cursorFactory, str, strArr, str2);
    }

    /* renamed from: rawQuery */
    public Cursor lambda$rawQuery$8(String str, String[] strArr, int i, int i2) {
        DatabaseMonitor.onSql(str, strArr);
        return (Cursor) traceSql("rawQuery(4)", str, false, (Returnable<Object>) new Returnable(str, strArr, i, i2) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda16
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String[] f$2;
            public final /* synthetic */ int f$3;
            public final /* synthetic */ int f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$rawQuery$8(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    public /* synthetic */ Long lambda$insert$9(String str, String str2, ContentValues contentValues) {
        return Long.valueOf(this.wrapped.insert(str, str2, contentValues));
    }

    public long insert(String str, String str2, ContentValues contentValues) {
        return ((Long) traceSql("insert()", str, null, true, new Returnable(str, str2, contentValues) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda10
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ ContentValues f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$insert$9(this.f$1, this.f$2, this.f$3);
            }
        })).longValue();
    }

    public /* synthetic */ Long lambda$insertOrThrow$10(String str, String str2, ContentValues contentValues) {
        return Long.valueOf(this.wrapped.insertOrThrow(str, str2, contentValues));
    }

    public long insertOrThrow(String str, String str2, ContentValues contentValues) throws net.zetetic.database.SQLException {
        return ((Long) traceSql("insertOrThrow()", str, null, true, new Returnable(str, str2, contentValues) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda9
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ ContentValues f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$insertOrThrow$10(this.f$1, this.f$2, this.f$3);
            }
        })).longValue();
    }

    public /* synthetic */ Long lambda$replace$11(String str, String str2, ContentValues contentValues) {
        return Long.valueOf(this.wrapped.replace(str, str2, contentValues));
    }

    public long replace(String str, String str2, ContentValues contentValues) {
        return ((Long) traceSql("replace()", str, null, true, new Returnable(str, str2, contentValues) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda5
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ ContentValues f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$replace$11(this.f$1, this.f$2, this.f$3);
            }
        })).longValue();
    }

    public /* synthetic */ Long lambda$replaceOrThrow$12(String str, String str2, ContentValues contentValues) {
        return Long.valueOf(this.wrapped.replaceOrThrow(str, str2, contentValues));
    }

    public long replaceOrThrow(String str, String str2, ContentValues contentValues) throws net.zetetic.database.SQLException {
        return ((Long) traceSql("replaceOrThrow()", str, null, true, new Returnable(str, str2, contentValues) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda6
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ ContentValues f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$replaceOrThrow$12(this.f$1, this.f$2, this.f$3);
            }
        })).longValue();
    }

    public /* synthetic */ Long lambda$insertWithOnConflict$13(String str, String str2, ContentValues contentValues, int i) {
        return Long.valueOf(this.wrapped.insertWithOnConflict(str, str2, contentValues, i));
    }

    public long insertWithOnConflict(String str, String str2, ContentValues contentValues, int i) {
        return ((Long) traceSql("insertWithOnConflict()", str, null, true, new Returnable(str, str2, contentValues, i) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ ContentValues f$3;
            public final /* synthetic */ int f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$insertWithOnConflict$13(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        })).longValue();
    }

    public int delete(String str, String str2, String[] strArr) {
        DatabaseMonitor.onDelete(str, str2, strArr);
        return ((Integer) traceSql("delete()", str, str2, true, new Returnable(str, str2, strArr) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda4
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ String[] f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$delete$14(this.f$1, this.f$2, this.f$3);
            }
        })).intValue();
    }

    public /* synthetic */ Integer lambda$delete$14(String str, String str2, String[] strArr) {
        return Integer.valueOf(this.wrapped.delete(str, str2, strArr));
    }

    public int update(String str, ContentValues contentValues, String str2, String[] strArr) {
        DatabaseMonitor.onUpdate(str, contentValues, str2, strArr);
        return ((Integer) traceSql("update()", str, str2, true, new Returnable(str, contentValues, str2, strArr) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda21
            public final /* synthetic */ String f$1;
            public final /* synthetic */ ContentValues f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ String[] f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$update$15(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        })).intValue();
    }

    public /* synthetic */ Integer lambda$update$15(String str, ContentValues contentValues, String str2, String[] strArr) {
        return Integer.valueOf(this.wrapped.update(str, contentValues, str2, strArr));
    }

    public int updateWithOnConflict(String str, ContentValues contentValues, String str2, String[] strArr, int i) {
        DatabaseMonitor.onUpdate(str, contentValues, str2, strArr);
        return ((Integer) traceSql("updateWithOnConflict()", str, str2, true, new Returnable(str, contentValues, str2, strArr, i) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda11
            public final /* synthetic */ String f$1;
            public final /* synthetic */ ContentValues f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ String[] f$4;
            public final /* synthetic */ int f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$updateWithOnConflict$16(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        })).intValue();
    }

    public /* synthetic */ Integer lambda$updateWithOnConflict$16(String str, ContentValues contentValues, String str2, String[] strArr, int i) {
        return Integer.valueOf(this.wrapped.updateWithOnConflict(str, contentValues, str2, strArr, i));
    }

    public void execSQL(String str) throws net.zetetic.database.SQLException {
        DatabaseMonitor.onSql(str, null);
        traceSql("execSQL(1)", str, true, (Runnable) new Runnable(str) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda22
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SQLiteDatabase.this.lambda$execSQL$17(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$execSQL$17(String str) {
        this.wrapped.execSQL(str);
    }

    public void rawExecSQL(String str) {
        DatabaseMonitor.onSql(str, null);
        traceSql("rawExecSQL()", str, true, (Returnable) new Returnable(str) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.database.SQLiteDatabase.Returnable
            public final Object run() {
                return SQLiteDatabase.this.lambda$rawExecSQL$18(this.f$1);
            }
        });
    }

    public /* synthetic */ Integer lambda$rawExecSQL$18(String str) {
        return Integer.valueOf(this.wrapped.rawExecSQL(str, new Object[0]));
    }

    public void execSQL(String str, Object[] objArr) throws net.zetetic.database.SQLException {
        DatabaseMonitor.onSql(str, null);
        traceSql("execSQL(2)", str, true, (Runnable) new Runnable(str, objArr) { // from class: org.thoughtcrime.securesms.database.SQLiteDatabase$$ExternalSyntheticLambda15
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Object[] f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SQLiteDatabase.this.lambda$execSQL$19(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$execSQL$19(String str, Object[] objArr) {
        this.wrapped.execSQL(str, objArr);
    }

    public boolean enableWriteAheadLogging() {
        return this.wrapped.enableWriteAheadLogging();
    }

    public void disableWriteAheadLogging() {
        this.wrapped.disableWriteAheadLogging();
    }

    public boolean isWriteAheadLoggingEnabled() {
        return this.wrapped.isWriteAheadLoggingEnabled();
    }

    public void setForeignKeyConstraintsEnabled(boolean z) {
        this.wrapped.setForeignKeyConstraintsEnabled(z);
    }

    public void beginTransactionWithListener(net.zetetic.database.sqlcipher.SQLiteTransactionListener sQLiteTransactionListener) {
        this.wrapped.beginTransactionWithListener(sQLiteTransactionListener);
    }

    public void beginTransactionNonExclusive() {
        this.wrapped.beginTransactionNonExclusive();
    }

    public void beginTransactionWithListenerNonExclusive(net.zetetic.database.sqlcipher.SQLiteTransactionListener sQLiteTransactionListener) {
        this.wrapped.beginTransactionWithListenerNonExclusive(sQLiteTransactionListener);
    }

    public boolean inTransaction() {
        return this.wrapped.inTransaction();
    }

    public boolean isDbLockedByCurrentThread() {
        return this.wrapped.isDbLockedByCurrentThread();
    }

    public boolean isDbLockedByOtherThreads() {
        return this.wrapped.isDbLockedByOtherThreads();
    }

    public boolean yieldIfContendedSafely() {
        return this.wrapped.yieldIfContendedSafely();
    }

    public boolean yieldIfContendedSafely(long j) {
        return this.wrapped.yieldIfContendedSafely(j);
    }

    public int getVersion() {
        return this.wrapped.getVersion();
    }

    public void setVersion(int i) {
        this.wrapped.setVersion(i);
    }

    public long getMaximumSize() {
        return this.wrapped.getMaximumSize();
    }

    public long setMaximumSize(long j) {
        return this.wrapped.setMaximumSize(j);
    }

    public long getPageSize() {
        return this.wrapped.getPageSize();
    }

    public void setPageSize(long j) {
        this.wrapped.setPageSize(j);
    }

    public SQLiteStatement compileStatement(String str) throws net.zetetic.database.SQLException {
        return this.wrapped.compileStatement(str);
    }

    public boolean isReadOnly() {
        return this.wrapped.isReadOnly();
    }

    public boolean isOpen() {
        return this.wrapped.isOpen();
    }

    public boolean needUpgrade(int i) {
        return this.wrapped.needUpgrade(i);
    }

    public final String getPath() {
        return this.wrapped.getPath();
    }

    public void setLocale(Locale locale) {
        this.wrapped.setLocale(locale);
    }

    /* loaded from: classes4.dex */
    private static class ConvertedTransactionListener implements net.zetetic.database.sqlcipher.SQLiteTransactionListener {
        private final SQLiteTransactionListener listener;

        ConvertedTransactionListener(SQLiteTransactionListener sQLiteTransactionListener) {
            this.listener = sQLiteTransactionListener;
        }

        @Override // net.zetetic.database.sqlcipher.SQLiteTransactionListener
        public void onBegin() {
            this.listener.onBegin();
        }

        @Override // net.zetetic.database.sqlcipher.SQLiteTransactionListener
        public void onCommit() {
            this.listener.onCommit();
        }

        @Override // net.zetetic.database.sqlcipher.SQLiteTransactionListener
        public void onRollback() {
            this.listener.onRollback();
        }
    }
}
