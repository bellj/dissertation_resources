package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.LongSerializer;

/* compiled from: DistributionListPrivacyMode.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0001\u0018\u0000 \f2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\fB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\b\u001a\u00020\u0003R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0007j\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "", "code", "", "(Ljava/lang/String;IJ)V", "isBlockList", "", "()Z", "serialize", "ONLY_WITH", "ALL_EXCEPT", "ALL", "Serializer", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum DistributionListPrivacyMode {
    ONLY_WITH(0),
    ALL_EXCEPT(1),
    ALL(2);
    
    public static final Serializer Serializer = new Serializer(null);
    private final long code;

    DistributionListPrivacyMode(long j) {
        this.code = j;
    }

    public final boolean isBlockList() {
        return this != ONLY_WITH;
    }

    public final long serialize() {
        return this.code;
    }

    /* compiled from: DistributionListPrivacyMode.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0015\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0002\u0010\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode$Serializer;", "Lorg/signal/core/util/LongSerializer;", "Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "()V", "deserialize", "data", "", "serialize", "(Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;)Ljava/lang/Long;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Serializer implements LongSerializer<DistributionListPrivacyMode> {
        public /* synthetic */ Serializer(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Serializer() {
        }

        @Override // org.signal.core.util.Serializer
        public /* bridge */ /* synthetic */ Object deserialize(Long l) {
            return deserialize(l.longValue());
        }

        public Long serialize(DistributionListPrivacyMode distributionListPrivacyMode) {
            Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "data");
            return Long.valueOf(distributionListPrivacyMode.serialize());
        }

        public DistributionListPrivacyMode deserialize(long j) {
            DistributionListPrivacyMode distributionListPrivacyMode = DistributionListPrivacyMode.ONLY_WITH;
            if (j != distributionListPrivacyMode.code) {
                distributionListPrivacyMode = DistributionListPrivacyMode.ALL_EXCEPT;
                if (j != distributionListPrivacyMode.code) {
                    distributionListPrivacyMode = DistributionListPrivacyMode.ALL;
                    if (j != distributionListPrivacyMode.code) {
                        throw new AssertionError("Unknown privacy mode: " + j);
                    }
                }
            }
            return distributionListPrivacyMode;
        }
    }
}
