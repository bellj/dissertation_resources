package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;

/* loaded from: classes4.dex */
public interface StoryTextPostOrBuilder extends MessageLiteOrBuilder {
    ChatColor getBackground();

    String getBody();

    ByteString getBodyBytes();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    StoryTextPost.Style getStyle();

    int getStyleValue();

    int getTextBackgroundColor();

    int getTextForegroundColor();

    boolean hasBackground();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
