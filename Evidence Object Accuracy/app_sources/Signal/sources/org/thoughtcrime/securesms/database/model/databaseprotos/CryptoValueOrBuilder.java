package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValue;

/* loaded from: classes4.dex */
public interface CryptoValueOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    CryptoValue.MobileCoinValue getMobileCoinValue();

    CryptoValue.ValueCase getValueCase();

    boolean hasMobileCoinValue();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
