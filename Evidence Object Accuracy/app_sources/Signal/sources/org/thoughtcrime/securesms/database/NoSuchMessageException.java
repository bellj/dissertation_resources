package org.thoughtcrime.securesms.database;

/* loaded from: classes4.dex */
public class NoSuchMessageException extends Exception {
    public NoSuchMessageException(String str) {
        super(str);
    }

    public NoSuchMessageException(Exception exc) {
        super(exc);
    }
}
