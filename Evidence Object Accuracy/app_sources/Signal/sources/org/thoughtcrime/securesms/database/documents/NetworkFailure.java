package org.thoughtcrime.securesms.database.documents;

import android.content.Context;
import android.text.TextUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes.dex */
public class NetworkFailure {
    @JsonProperty("a")
    private String address;
    @JsonProperty("r")
    private String recipientId;

    public NetworkFailure(RecipientId recipientId) {
        this.recipientId = recipientId.serialize();
        this.address = "";
    }

    public NetworkFailure() {
    }

    @JsonIgnore
    public RecipientId getRecipientId(Context context) {
        if (!TextUtils.isEmpty(this.recipientId)) {
            return RecipientId.from(this.recipientId);
        }
        return Recipient.external(context, this.address).getId();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NetworkFailure networkFailure = (NetworkFailure) obj;
        if (!Objects.equals(this.address, networkFailure.address) || !Objects.equals(this.recipientId, networkFailure.recipientId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.address, this.recipientId);
    }
}
