package org.thoughtcrime.securesms.database.loaders;

import android.content.Context;
import android.database.Cursor;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.loaders.MediaLoader;

/* loaded from: classes4.dex */
public final class ThreadMediaLoader extends MediaLoader {
    private final MediaLoader.MediaType mediaType;
    private final MediaDatabase.Sorting sorting;
    private final long threadId;

    public ThreadMediaLoader(Context context, long j, MediaLoader.MediaType mediaType, MediaDatabase.Sorting sorting) {
        super(context);
        this.threadId = j;
        this.mediaType = mediaType;
        this.sorting = sorting;
    }

    @Override // org.thoughtcrime.securesms.util.AbstractCursorLoader
    public Cursor getCursor() {
        return createThreadMediaCursor(this.context, this.threadId, this.mediaType, this.sorting);
    }

    public static Cursor createThreadMediaCursor(Context context, long j, MediaLoader.MediaType mediaType, MediaDatabase.Sorting sorting) {
        MediaDatabase media = SignalDatabase.media();
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$loaders$MediaLoader$MediaType[mediaType.ordinal()];
        if (i == 1) {
            return media.getGalleryMediaForThread(j, sorting);
        }
        if (i == 2) {
            return media.getDocumentMediaForThread(j, sorting);
        }
        if (i == 3) {
            return media.getAudioMediaForThread(j, sorting);
        }
        if (i == 4) {
            return media.getAllMediaForThread(j, sorting);
        }
        throw new AssertionError();
    }

    /* renamed from: org.thoughtcrime.securesms.database.loaders.ThreadMediaLoader$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$loaders$MediaLoader$MediaType;

        static {
            int[] iArr = new int[MediaLoader.MediaType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$loaders$MediaLoader$MediaType = iArr;
            try {
                iArr[MediaLoader.MediaType.GALLERY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$loaders$MediaLoader$MediaType[MediaLoader.MediaType.DOCUMENT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$loaders$MediaLoader$MediaType[MediaLoader.MediaType.AUDIO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$loaders$MediaLoader$MediaType[MediaLoader.MediaType.ALL.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }
}
