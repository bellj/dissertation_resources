package org.thoughtcrime.securesms.database;

import android.database.Cursor;
import net.zetetic.database.DatabaseErrorHandler;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* loaded from: classes4.dex */
public final class SqlCipherDeletingErrorHandler implements DatabaseErrorHandler {
    private static final String TAG = Log.tag(SqlCipherDeletingErrorHandler.class);
    private final String databaseName;

    public SqlCipherDeletingErrorHandler(String str) {
        this.databaseName = str;
    }

    @Override // net.zetetic.database.DatabaseErrorHandler
    public void onCorruption(SQLiteDatabase sQLiteDatabase) {
        String str = TAG;
        Log.e(str, "Database '" + this.databaseName + "' corrupted! Going to try to run some diagnostics.");
        Log.w(str, " ===== PRAGMA integrity_check =====");
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery("PRAGMA integrity_check", (String[]) null);
            while (rawQuery.moveToNext()) {
                Log.w(TAG, CursorUtil.readRowAsString(rawQuery));
            }
            rawQuery.close();
        } catch (Throwable th) {
            Log.e(TAG, "Failed to do integrity_check!", th);
        }
        Log.w(TAG, "===== PRAGMA cipher_integrity_check =====");
        try {
            Cursor rawQuery2 = sQLiteDatabase.rawQuery("PRAGMA cipher_integrity_check", (String[]) null);
            while (rawQuery2.moveToNext()) {
                Log.w(TAG, CursorUtil.readRowAsString(rawQuery2));
            }
            rawQuery2.close();
        } catch (Throwable th2) {
            Log.e(TAG, "Failed to do cipher_integrity_check!", th2);
        }
        ApplicationDependencies.getApplication().deleteDatabase(this.databaseName);
    }
}
