package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;

/* loaded from: classes4.dex */
public interface TemporalAuthCredentialResponsesOrBuilder extends MessageLiteOrBuilder {
    TemporalAuthCredentialResponse getCredentialResponse(int i);

    int getCredentialResponseCount();

    List<TemporalAuthCredentialResponse> getCredentialResponseList();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
