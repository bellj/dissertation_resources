package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import java.util.List;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public abstract class CursorRecyclerViewAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    static final long FOOTER_ID;
    static final int FOOTER_TYPE;
    static final long HEADER_ID;
    static final int HEADER_TYPE;
    private final Context context;
    private Cursor cursor;
    private View footer;
    private View header;
    private final DataSetObserver observer;
    private boolean valid;

    protected boolean arePayloadsValid(List<Object> list) {
        return false;
    }

    protected long getFastAccessItemId(int i) {
        return 0;
    }

    protected int getFastAccessItemViewType(int i) {
        return 0;
    }

    protected int getFastAccessSize() {
        return 0;
    }

    public int getItemViewType(Cursor cursor) {
        return 0;
    }

    protected boolean isFastAccessPosition(int i) {
        return false;
    }

    protected void onBindFastAccessItemViewHolder(VH vh, int i) {
    }

    protected void onBindFastAccessItemViewHolder(VH vh, int i, List<Object> list) {
    }

    public abstract void onBindItemViewHolder(VH vh, Cursor cursor);

    protected void onBindItemViewHolder(VH vh, Cursor cursor, List<Object> list) {
    }

    public abstract VH onCreateItemViewHolder(ViewGroup viewGroup, int i);

    public void onItemViewRecycled(VH vh) {
    }

    /* loaded from: classes4.dex */
    public static class HeaderFooterViewHolder extends RecyclerView.ViewHolder {
        private ViewGroup container;

        HeaderFooterViewHolder(View view) {
            super(view);
            this.container = (ViewGroup) view;
        }

        void bind(View view) {
            unbind();
            if (view != null) {
                this.container.addView(view);
            }
        }

        void unbind() {
            this.container.removeAllViews();
        }
    }

    public CursorRecyclerViewAdapter(Context context, Cursor cursor) {
        AdapterDataSetObserver adapterDataSetObserver = new AdapterDataSetObserver();
        this.observer = adapterDataSetObserver;
        this.context = context;
        this.cursor = cursor;
        if (cursor != null) {
            this.valid = true;
            cursor.registerDataSetObserver(adapterDataSetObserver);
        }
    }

    public Context getContext() {
        return this.context;
    }

    public Cursor getCursor() {
        return this.cursor;
    }

    public void setHeaderView(View view) {
        this.header = view;
    }

    public View getHeaderView() {
        return this.header;
    }

    public void setFooterView(View view) {
        this.footer = view;
    }

    public boolean hasHeaderView() {
        return this.header != null;
    }

    public boolean hasFooterView() {
        return this.footer != null;
    }

    public void changeCursor(Cursor cursor) {
        Cursor swapCursor = swapCursor(cursor);
        if (swapCursor != null) {
            swapCursor.close();
        }
    }

    public Cursor swapCursor(Cursor cursor) {
        Cursor cursor2 = this.cursor;
        if (cursor == cursor2) {
            return null;
        }
        if (cursor2 != null) {
            cursor2.unregisterDataSetObserver(this.observer);
        }
        this.cursor = cursor;
        if (cursor != null) {
            cursor.registerDataSetObserver(this.observer);
        }
        this.valid = this.cursor != null;
        notifyDataSetChanged();
        return cursor2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        if (!isActiveCursor()) {
            return 0;
        }
        return this.cursor.getCount() + getFastAccessSize() + (hasHeaderView() ? 1 : 0) + (hasFooterView() ? 1 : 0);
    }

    public int getCursorCount() {
        return this.cursor.getCount();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: androidx.recyclerview.widget.RecyclerView$ViewHolder */
    /* JADX WARN: Multi-variable type inference failed */
    public final void onViewRecycled(RecyclerView.ViewHolder viewHolder) {
        if (!(viewHolder instanceof HeaderFooterViewHolder)) {
            onItemViewRecycled(viewHolder);
        } else {
            ((HeaderFooterViewHolder) viewHolder).unbind();
        }
    }

    public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == Integer.MIN_VALUE || i == FOOTER_TYPE) {
            return new HeaderFooterViewHolder(LayoutInflater.from(this.context).inflate(R.layout.cursor_adapter_header_footer_view, viewGroup, false));
        }
        return onCreateItemViewHolder(viewGroup, i);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: androidx.recyclerview.widget.RecyclerView$ViewHolder */
    /* JADX WARN: Multi-variable type inference failed */
    public final void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i, List<Object> list) {
        if (!arePayloadsValid(list) || isHeaderPosition(i) || isFooterPosition(i)) {
            onBindViewHolder(viewHolder, i);
        } else if (isFastAccessPosition(i)) {
            onBindFastAccessItemViewHolder(viewHolder, i, list);
        } else {
            onBindItemViewHolder(viewHolder, getCursorAtPositionOrThrow(i), list);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: androidx.recyclerview.widget.RecyclerView$ViewHolder */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public final void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (isHeaderPosition(i)) {
            ((HeaderFooterViewHolder) viewHolder).bind(this.header);
        } else if (isFooterPosition(i)) {
            ((HeaderFooterViewHolder) viewHolder).bind(this.footer);
        } else if (isFastAccessPosition(i)) {
            onBindFastAccessItemViewHolder(viewHolder, i);
        } else {
            onBindItemViewHolder(viewHolder, getCursorAtPositionOrThrow(i));
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public final int getItemViewType(int i) {
        if (isHeaderPosition(i)) {
            return Integer.MIN_VALUE;
        }
        if (isFooterPosition(i)) {
            return FOOTER_TYPE;
        }
        if (isFastAccessPosition(i)) {
            return getFastAccessItemViewType(i);
        }
        return getItemViewType(getCursorAtPositionOrThrow(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public final long getItemId(int i) {
        if (isHeaderPosition(i)) {
            return HEADER_ID;
        }
        if (isFooterPosition(i)) {
            return FOOTER_ID;
        }
        if (isFastAccessPosition(i)) {
            return getFastAccessItemId(i);
        }
        long itemId = getItemId(getCursorAtPositionOrThrow(i));
        return itemId <= FOOTER_ID ? itemId + 2 : itemId;
    }

    public long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
    }

    public Cursor getCursorAtPositionOrThrow(int i) {
        if (!isActiveCursor()) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        } else if (this.cursor.moveToPosition(getCursorPosition(i))) {
            return this.cursor;
        } else {
            throw new IllegalStateException("couldn't move cursor to position " + i + " (actual cursor position " + getCursorPosition(i) + ")");
        }
    }

    public boolean isActiveCursor() {
        return this.valid && this.cursor != null;
    }

    protected boolean isFooterPosition(int i) {
        return hasFooterView() && i == getItemCount() - 1;
    }

    protected boolean isHeaderPosition(int i) {
        return hasHeaderView() && i == 0;
    }

    private int getCursorPosition(int i) {
        if (hasHeaderView()) {
            i--;
        }
        return i - getFastAccessSize();
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class AdapterDataSetObserver extends DataSetObserver {
        private AdapterDataSetObserver() {
            CursorRecyclerViewAdapter.this = r1;
        }

        public void onChanged() {
            super.onChanged();
            CursorRecyclerViewAdapter.this.valid = true;
        }

        public void onInvalidated() {
            super.onInvalidated();
            CursorRecyclerViewAdapter.this.valid = false;
        }
    }
}
