package org.thoughtcrime.securesms.database.model;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.UpdateDescription;
import org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DateUtils;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public class GroupCallUpdateMessageFactory implements UpdateDescription.SpannableFactory {
    private final Context context;
    private final GroupCallUpdateDetails groupCallUpdateDetails;
    private final List<ServiceId> joinedMembers;
    private final ACI selfAci;
    private final boolean withTime;

    public GroupCallUpdateMessageFactory(Context context, List<ServiceId> list, boolean z, GroupCallUpdateDetails groupCallUpdateDetails) {
        this.context = context;
        ArrayList arrayList = new ArrayList(list);
        this.joinedMembers = arrayList;
        this.withTime = z;
        this.groupCallUpdateDetails = groupCallUpdateDetails;
        ACI requireAci = SignalStore.account().requireAci();
        this.selfAci = requireAci;
        if (arrayList.remove(requireAci)) {
            arrayList.add(requireAci);
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.UpdateDescription.SpannableFactory
    public Spannable create() {
        return new SpannableString(createString());
    }

    private String createString() {
        String timeString = DateUtils.getTimeString(this.context, Locale.getDefault(), this.groupCallUpdateDetails.getStartedCallTimestamp());
        int size = this.joinedMembers.size();
        if (size != 0) {
            if (size != 1) {
                if (size != 2) {
                    int size2 = this.joinedMembers.size() - 2;
                    if (this.withTime) {
                        return this.context.getResources().getQuantityString(R.plurals.MessageRecord_s_s_and_d_others_are_in_the_group_call_s, size2, describe(this.joinedMembers.get(0)), describe(this.joinedMembers.get(1)), Integer.valueOf(size2), timeString);
                    }
                    return this.context.getResources().getQuantityString(R.plurals.MessageRecord_s_s_and_d_others_are_in_the_group_call, size2, describe(this.joinedMembers.get(0)), describe(this.joinedMembers.get(1)), Integer.valueOf(size2));
                } else if (this.withTime) {
                    return this.context.getString(R.string.MessageRecord_s_and_s_are_in_the_group_call_s1, describe(this.joinedMembers.get(0)), describe(this.joinedMembers.get(1)), timeString);
                } else {
                    return this.context.getString(R.string.MessageRecord_s_and_s_are_in_the_group_call, describe(this.joinedMembers.get(0)), describe(this.joinedMembers.get(1)));
                }
            } else if (this.joinedMembers.get(0).toString().equals(this.groupCallUpdateDetails.getStartedCallUuid())) {
                if (this.withTime) {
                    return this.context.getString(R.string.MessageRecord_s_started_a_group_call_s, describe(this.joinedMembers.get(0)), timeString);
                }
                return this.context.getString(R.string.MessageRecord_s_started_a_group_call, describe(this.joinedMembers.get(0)));
            } else if (Objects.equals(this.joinedMembers.get(0), this.selfAci)) {
                if (this.withTime) {
                    return this.context.getString(R.string.MessageRecord_you_are_in_the_group_call_s1, timeString);
                }
                return this.context.getString(R.string.MessageRecord_you_are_in_the_group_call);
            } else if (this.withTime) {
                return this.context.getString(R.string.MessageRecord_s_is_in_the_group_call_s, describe(this.joinedMembers.get(0)), timeString);
            } else {
                return this.context.getString(R.string.MessageRecord_s_is_in_the_group_call, describe(this.joinedMembers.get(0)));
            }
        } else if (this.withTime) {
            return this.context.getString(R.string.MessageRecord_group_call_s, timeString);
        } else {
            return this.context.getString(R.string.MessageRecord_group_call);
        }
    }

    private String describe(ServiceId serviceId) {
        if (serviceId.isUnknown()) {
            return this.context.getString(R.string.MessageRecord_unknown);
        }
        Recipient resolved = Recipient.resolved(RecipientId.from(serviceId));
        if (resolved.isSelf()) {
            return this.context.getString(R.string.MessageRecord_you);
        }
        return resolved.getShortDisplayName(this.context);
    }
}
