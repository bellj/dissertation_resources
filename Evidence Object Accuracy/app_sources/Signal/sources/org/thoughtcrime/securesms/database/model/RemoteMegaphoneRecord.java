package org.thoughtcrime.securesms.database.model;

import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* compiled from: RemoteMegaphoneRecord.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0002\b,\b\b\u0018\u00002\u00020\u0001:\u0001OBµ\u0001\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0006\u0012\u0006\u0010\u0015\u001a\u00020\u0006\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0003¢\u0006\u0002\u0010\u001aJ\t\u00107\u001a\u00020\u0003HÆ\u0003J\u000b\u00108\u001a\u0004\u0018\u00010\u000fHÆ\u0003J\u000b\u00109\u001a\u0004\u0018\u00010\u000fHÆ\u0003J\u000b\u0010:\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010;\u001a\u0004\u0018\u00010\u0013HÆ\u0003J\t\u0010<\u001a\u00020\u0006HÆ\u0003J\t\u0010=\u001a\u00020\u0006HÆ\u0003J\u000b\u0010>\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010?\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\t\u0010@\u001a\u00020\u0003HÆ\u0003J\t\u0010A\u001a\u00020\u0003HÆ\u0003J\t\u0010B\u001a\u00020\u0003HÆ\u0003J\t\u0010C\u001a\u00020\u0006HÆ\u0003J\u000b\u0010D\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\t\u0010E\u001a\u00020\tHÆ\u0003J\t\u0010F\u001a\u00020\u0003HÆ\u0003J\t\u0010G\u001a\u00020\u0003HÆ\u0003J\t\u0010H\u001a\u00020\u0003HÆ\u0003J\u000b\u0010I\u001a\u0004\u0018\u00010\u0006HÆ\u0003J×\u0001\u0010J\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00132\b\b\u0002\u0010\u0014\u001a\u00020\u00062\b\b\u0002\u0010\u0015\u001a\u00020\u00062\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u0018\u001a\u00020\u00032\b\b\u0002\u0010\u0019\u001a\u00020\u0003HÆ\u0001J\u0013\u0010K\u001a\u00020$2\b\u0010L\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010M\u001a\u00020\tHÖ\u0001J\t\u0010N\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\u0015\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0013\u0010\r\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001cR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001cR\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b!\u0010 R\u0011\u0010\u0019\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010 R\u0013\u0010#\u001a\u00020$8G¢\u0006\b\n\u0000\u001a\u0004\b#\u0010%R\u0013\u0010&\u001a\u00020$8G¢\u0006\b\n\u0000\u001a\u0004\b&\u0010%R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b'\u0010 R\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0013¢\u0006\b\n\u0000\u001a\u0004\b(\u0010)R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001cR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b+\u0010,R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000f¢\u0006\b\n\u0000\u001a\u0004\b-\u0010.R\u0013\u0010\u0016\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b/\u0010\u001cR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b0\u0010 R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\b\n\u0000\u001a\u0004\b1\u0010.R\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b2\u0010\u001cR\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b3\u0010 R\u0011\u0010\u0018\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b4\u0010 R\u0011\u0010\u0014\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b5\u0010\u001cR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b6\u0010\u001c¨\u0006P"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord;", "", ContactRepository.ID_COLUMN, "", "priority", RecipientDatabase.SERVICE_ID, "", "countries", "minimumVersion", "", "doNotShowBefore", "doNotShowAfter", "showForNumberOfDays", "conditionalId", "primaryActionId", "Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord$ActionId;", "secondaryActionId", "imageUrl", "imageUri", "Landroid/net/Uri;", MultiselectForwardFragment.DIALOG_TITLE, "body", "primaryActionText", "secondaryActionText", "shownAt", "finishedAt", "(JJLjava/lang/String;Ljava/lang/String;IJJJLjava/lang/String;Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord$ActionId;Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord$ActionId;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V", "getBody", "()Ljava/lang/String;", "getConditionalId", "getCountries", "getDoNotShowAfter", "()J", "getDoNotShowBefore", "getFinishedAt", "hasPrimaryAction", "", "()Z", "hasSecondaryAction", "getId", "getImageUri", "()Landroid/net/Uri;", "getImageUrl", "getMinimumVersion", "()I", "getPrimaryActionId", "()Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord$ActionId;", "getPrimaryActionText", "getPriority", "getSecondaryActionId", "getSecondaryActionText", "getShowForNumberOfDays", "getShownAt", "getTitle", "getUuid", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "ActionId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RemoteMegaphoneRecord {
    private final String body;
    private final String conditionalId;
    private final String countries;
    private final long doNotShowAfter;
    private final long doNotShowBefore;
    private final long finishedAt;
    private final boolean hasPrimaryAction;
    private final boolean hasSecondaryAction;
    private final long id;
    private final Uri imageUri;
    private final String imageUrl;
    private final int minimumVersion;
    private final ActionId primaryActionId;
    private final String primaryActionText;
    private final long priority;
    private final ActionId secondaryActionId;
    private final String secondaryActionText;
    private final long showForNumberOfDays;
    private final long shownAt;
    private final String title;
    private final String uuid;

    public final long component1() {
        return this.id;
    }

    public final ActionId component10() {
        return this.primaryActionId;
    }

    public final ActionId component11() {
        return this.secondaryActionId;
    }

    public final String component12() {
        return this.imageUrl;
    }

    public final Uri component13() {
        return this.imageUri;
    }

    public final String component14() {
        return this.title;
    }

    public final String component15() {
        return this.body;
    }

    public final String component16() {
        return this.primaryActionText;
    }

    public final String component17() {
        return this.secondaryActionText;
    }

    public final long component18() {
        return this.shownAt;
    }

    public final long component19() {
        return this.finishedAt;
    }

    public final long component2() {
        return this.priority;
    }

    public final String component3() {
        return this.uuid;
    }

    public final String component4() {
        return this.countries;
    }

    public final int component5() {
        return this.minimumVersion;
    }

    public final long component6() {
        return this.doNotShowBefore;
    }

    public final long component7() {
        return this.doNotShowAfter;
    }

    public final long component8() {
        return this.showForNumberOfDays;
    }

    public final String component9() {
        return this.conditionalId;
    }

    public final RemoteMegaphoneRecord copy(long j, long j2, String str, String str2, int i, long j3, long j4, long j5, String str3, ActionId actionId, ActionId actionId2, String str4, Uri uri, String str5, String str6, String str7, String str8, long j6, long j7) {
        Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
        Intrinsics.checkNotNullParameter(str5, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(str6, "body");
        return new RemoteMegaphoneRecord(j, j2, str, str2, i, j3, j4, j5, str3, actionId, actionId2, str4, uri, str5, str6, str7, str8, j6, j7);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RemoteMegaphoneRecord)) {
            return false;
        }
        RemoteMegaphoneRecord remoteMegaphoneRecord = (RemoteMegaphoneRecord) obj;
        return this.id == remoteMegaphoneRecord.id && this.priority == remoteMegaphoneRecord.priority && Intrinsics.areEqual(this.uuid, remoteMegaphoneRecord.uuid) && Intrinsics.areEqual(this.countries, remoteMegaphoneRecord.countries) && this.minimumVersion == remoteMegaphoneRecord.minimumVersion && this.doNotShowBefore == remoteMegaphoneRecord.doNotShowBefore && this.doNotShowAfter == remoteMegaphoneRecord.doNotShowAfter && this.showForNumberOfDays == remoteMegaphoneRecord.showForNumberOfDays && Intrinsics.areEqual(this.conditionalId, remoteMegaphoneRecord.conditionalId) && this.primaryActionId == remoteMegaphoneRecord.primaryActionId && this.secondaryActionId == remoteMegaphoneRecord.secondaryActionId && Intrinsics.areEqual(this.imageUrl, remoteMegaphoneRecord.imageUrl) && Intrinsics.areEqual(this.imageUri, remoteMegaphoneRecord.imageUri) && Intrinsics.areEqual(this.title, remoteMegaphoneRecord.title) && Intrinsics.areEqual(this.body, remoteMegaphoneRecord.body) && Intrinsics.areEqual(this.primaryActionText, remoteMegaphoneRecord.primaryActionText) && Intrinsics.areEqual(this.secondaryActionText, remoteMegaphoneRecord.secondaryActionText) && this.shownAt == remoteMegaphoneRecord.shownAt && this.finishedAt == remoteMegaphoneRecord.finishedAt;
    }

    public int hashCode() {
        int m = ((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.priority)) * 31) + this.uuid.hashCode()) * 31;
        String str = this.countries;
        int i = 0;
        int hashCode = (((((((((m + (str == null ? 0 : str.hashCode())) * 31) + this.minimumVersion) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.doNotShowBefore)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.doNotShowAfter)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.showForNumberOfDays)) * 31;
        String str2 = this.conditionalId;
        int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
        ActionId actionId = this.primaryActionId;
        int hashCode3 = (hashCode2 + (actionId == null ? 0 : actionId.hashCode())) * 31;
        ActionId actionId2 = this.secondaryActionId;
        int hashCode4 = (hashCode3 + (actionId2 == null ? 0 : actionId2.hashCode())) * 31;
        String str3 = this.imageUrl;
        int hashCode5 = (hashCode4 + (str3 == null ? 0 : str3.hashCode())) * 31;
        Uri uri = this.imageUri;
        int hashCode6 = (((((hashCode5 + (uri == null ? 0 : uri.hashCode())) * 31) + this.title.hashCode()) * 31) + this.body.hashCode()) * 31;
        String str4 = this.primaryActionText;
        int hashCode7 = (hashCode6 + (str4 == null ? 0 : str4.hashCode())) * 31;
        String str5 = this.secondaryActionText;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return ((((hashCode7 + i) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.shownAt)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.finishedAt);
    }

    public String toString() {
        return "RemoteMegaphoneRecord(id=" + this.id + ", priority=" + this.priority + ", uuid=" + this.uuid + ", countries=" + this.countries + ", minimumVersion=" + this.minimumVersion + ", doNotShowBefore=" + this.doNotShowBefore + ", doNotShowAfter=" + this.doNotShowAfter + ", showForNumberOfDays=" + this.showForNumberOfDays + ", conditionalId=" + this.conditionalId + ", primaryActionId=" + this.primaryActionId + ", secondaryActionId=" + this.secondaryActionId + ", imageUrl=" + this.imageUrl + ", imageUri=" + this.imageUri + ", title=" + this.title + ", body=" + this.body + ", primaryActionText=" + this.primaryActionText + ", secondaryActionText=" + this.secondaryActionText + ", shownAt=" + this.shownAt + ", finishedAt=" + this.finishedAt + ')';
    }

    public RemoteMegaphoneRecord(long j, long j2, String str, String str2, int i, long j3, long j4, long j5, String str3, ActionId actionId, ActionId actionId2, String str4, Uri uri, String str5, String str6, String str7, String str8, long j6, long j7) {
        Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
        Intrinsics.checkNotNullParameter(str5, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(str6, "body");
        this.id = j;
        this.priority = j2;
        this.uuid = str;
        this.countries = str2;
        this.minimumVersion = i;
        this.doNotShowBefore = j3;
        this.doNotShowAfter = j4;
        this.showForNumberOfDays = j5;
        this.conditionalId = str3;
        this.primaryActionId = actionId;
        this.secondaryActionId = actionId2;
        this.imageUrl = str4;
        this.imageUri = uri;
        this.title = str5;
        this.body = str6;
        this.primaryActionText = str7;
        this.secondaryActionText = str8;
        this.shownAt = j6;
        this.finishedAt = j7;
        boolean z = true;
        this.hasPrimaryAction = (actionId == null || str7 == null) ? false : true;
        this.hasSecondaryAction = (actionId2 == null || str8 == null) ? false : z;
    }

    public /* synthetic */ RemoteMegaphoneRecord(long j, long j2, String str, String str2, int i, long j3, long j4, long j5, String str3, ActionId actionId, ActionId actionId2, String str4, Uri uri, String str5, String str6, String str7, String str8, long j6, long j7, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? -1 : j, j2, str, str2, i, j3, j4, j5, str3, actionId, actionId2, str4, (i2 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? null : uri, str5, str6, str7, str8, (131072 & i2) != 0 ? 0 : j6, (i2 & 262144) != 0 ? 0 : j7);
    }

    public final long getId() {
        return this.id;
    }

    public final long getPriority() {
        return this.priority;
    }

    public final String getUuid() {
        return this.uuid;
    }

    public final String getCountries() {
        return this.countries;
    }

    public final int getMinimumVersion() {
        return this.minimumVersion;
    }

    public final long getDoNotShowBefore() {
        return this.doNotShowBefore;
    }

    public final long getDoNotShowAfter() {
        return this.doNotShowAfter;
    }

    public final long getShowForNumberOfDays() {
        return this.showForNumberOfDays;
    }

    public final String getConditionalId() {
        return this.conditionalId;
    }

    public final ActionId getPrimaryActionId() {
        return this.primaryActionId;
    }

    public final ActionId getSecondaryActionId() {
        return this.secondaryActionId;
    }

    public final String getImageUrl() {
        return this.imageUrl;
    }

    public final Uri getImageUri() {
        return this.imageUri;
    }

    public final String getTitle() {
        return this.title;
    }

    public final String getBody() {
        return this.body;
    }

    public final String getPrimaryActionText() {
        return this.primaryActionText;
    }

    public final String getSecondaryActionText() {
        return this.secondaryActionText;
    }

    public final long getShownAt() {
        return this.shownAt;
    }

    public final long getFinishedAt() {
        return this.finishedAt;
    }

    public final boolean hasPrimaryAction() {
        return this.hasPrimaryAction;
    }

    public final boolean hasSecondaryAction() {
        return this.hasSecondaryAction;
    }

    /* compiled from: RemoteMegaphoneRecord.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0001\u0018\u0000 \r2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\rB\u0019\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\f¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord$ActionId;", "", ContactRepository.ID_COLUMN, "", "isDonateAction", "", "(Ljava/lang/String;ILjava/lang/String;Z)V", "getId", "()Ljava/lang/String;", "()Z", "SNOOZE", "FINISH", "DONATE", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum ActionId {
        SNOOZE("snooze", false, 2, null),
        FINISH("finish", false, 2, null),
        DONATE("donate", true);
        
        public static final Companion Companion = new Companion(null);
        private final String id;
        private final boolean isDonateAction;

        ActionId(String str, boolean z) {
            this.id = str;
            this.isDonateAction = z;
        }

        /* synthetic */ ActionId(String str, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, (i & 2) != 0 ? false : z);
        }

        public final String getId() {
            return this.id;
        }

        public final boolean isDonateAction() {
            return this.isDonateAction;
        }

        /* compiled from: RemoteMegaphoneRecord.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord$ActionId$Companion;", "", "()V", "from", "Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord$ActionId;", ContactRepository.ID_COLUMN, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final ActionId from(String str) {
                ActionId[] values = ActionId.values();
                for (ActionId actionId : values) {
                    if (Intrinsics.areEqual(actionId.getId(), str)) {
                        return actionId;
                    }
                }
                return null;
            }
        }
    }
}
