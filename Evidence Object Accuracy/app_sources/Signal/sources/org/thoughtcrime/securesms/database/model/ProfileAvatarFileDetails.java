package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Charsets;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;

/* compiled from: ProfileAvatarFileDetails.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0006\u0010\u000f\u001a\u00020\u0010J\u0006\u0010\u0011\u001a\u00020\rJ\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/ProfileAvatarFileDetails;", "", "hashId", "", "lastModified", "(JJ)V", "getHashId", "()J", "getLastModified", "component1", "component2", "copy", "equals", "", "other", "getDiskCacheKeyBytes", "", "hasFile", "hashCode", "", "toString", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ProfileAvatarFileDetails {
    public static final Companion Companion = new Companion(null);
    public static final ProfileAvatarFileDetails NO_DETAILS = new ProfileAvatarFileDetails(0, 0);
    private final long hashId;
    private final long lastModified;

    public static /* synthetic */ ProfileAvatarFileDetails copy$default(ProfileAvatarFileDetails profileAvatarFileDetails, long j, long j2, int i, Object obj) {
        if ((i & 1) != 0) {
            j = profileAvatarFileDetails.hashId;
        }
        if ((i & 2) != 0) {
            j2 = profileAvatarFileDetails.lastModified;
        }
        return profileAvatarFileDetails.copy(j, j2);
    }

    public final long component1() {
        return this.hashId;
    }

    public final long component2() {
        return this.lastModified;
    }

    public final ProfileAvatarFileDetails copy(long j, long j2) {
        return new ProfileAvatarFileDetails(j, j2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProfileAvatarFileDetails)) {
            return false;
        }
        ProfileAvatarFileDetails profileAvatarFileDetails = (ProfileAvatarFileDetails) obj;
        return this.hashId == profileAvatarFileDetails.hashId && this.lastModified == profileAvatarFileDetails.lastModified;
    }

    public int hashCode() {
        return (SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.hashId) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.lastModified);
    }

    public String toString() {
        return "ProfileAvatarFileDetails(hashId=" + this.hashId + ", lastModified=" + this.lastModified + ')';
    }

    public ProfileAvatarFileDetails(long j, long j2) {
        this.hashId = j;
        this.lastModified = j2;
    }

    public final long getHashId() {
        return this.hashId;
    }

    public final long getLastModified() {
        return this.lastModified;
    }

    public final byte[] getDiskCacheKeyBytes() {
        byte[] bytes = toString().getBytes(Charsets.UTF_8);
        Intrinsics.checkNotNullExpressionValue(bytes, "this as java.lang.String).getBytes(charset)");
        return bytes;
    }

    public final boolean hasFile() {
        return !Intrinsics.areEqual(this, NO_DETAILS);
    }

    /* compiled from: ProfileAvatarFileDetails.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/ProfileAvatarFileDetails$Companion;", "", "()V", "NO_DETAILS", "Lorg/thoughtcrime/securesms/database/model/ProfileAvatarFileDetails;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
