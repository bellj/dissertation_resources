package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.database.Cursor;
import com.annimon.stream.Stream;
import j$.util.Collection$EL;
import j$.util.stream.Collectors;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.zetetic.database.sqlcipher.SQLiteQueryBuilder;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.notifications.v2.MessageNotifierV2;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.messages.multidevice.ReadMessage;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* loaded from: classes4.dex */
public class MmsSmsDatabase extends Database {
    public static final String MMS_TRANSPORT;
    private static final String[] PROJECTION = {"_id", MmsSmsColumns.UNIQUE_ROW_ID, "body", "type", "thread_id", "address", MmsSmsColumns.ADDRESS_DEVICE_ID, SmsDatabase.SUBJECT, "date_sent", MmsSmsColumns.NORMALIZED_DATE_RECEIVED, MmsSmsColumns.DATE_SERVER, MmsDatabase.MESSAGE_TYPE, MmsDatabase.MESSAGE_BOX, "status", MmsSmsColumns.UNIDENTIFIED, "part_count", "ct_l", "tr_id", "m_size", "exp", "st", "delivery_receipt_count", "read_receipt_count", MmsSmsColumns.MISMATCHED_IDENTITIES, "network_failures", MmsSmsColumns.SUBSCRIPTION_ID, "expires_in", MmsSmsColumns.EXPIRE_STARTED, MmsSmsColumns.NOTIFIED, TRANSPORT, "attachment_json", "quote_id", "quote_author", "quote_body", "quote_missing", "quote_attachment", "quote_type", "quote_mentions", "shared_contacts", "previews", MmsDatabase.VIEW_ONCE, "read", MmsSmsColumns.REACTIONS_UNREAD, MmsSmsColumns.REACTIONS_LAST_SEEN, MmsSmsColumns.REMOTE_DELETED, "mentions_self", MmsSmsColumns.NOTIFIED_TIMESTAMP, MmsSmsColumns.VIEWED_RECEIPT_COUNT, MmsSmsColumns.RECEIPT_TIMESTAMP, "ranges", MmsDatabase.STORY_TYPE, "parent_story_id"};
    public static final String SMS_TRANSPORT;
    private static final String SNIPPET_QUERY;
    private static final String TAG = Log.tag(MmsSmsDatabase.class);
    public static final String TRANSPORT;

    public MmsSmsDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public RecipientId getGroupAddedBy(long j) {
        long currentTimeMillis = System.currentTimeMillis();
        while (true) {
            Pair<RecipientId, Long> groupAddedBy = getGroupAddedBy(j, currentTimeMillis);
            if (groupAddedBy.first() != null) {
                return groupAddedBy.first();
            }
            long longValue = groupAddedBy.second().longValue();
            if (groupAddedBy.second().longValue() == -1) {
                return null;
            }
            currentTimeMillis = longValue;
        }
    }

    private Pair<RecipientId, Long> getGroupAddedBy(long j, long j2) {
        MmsDatabase mms = SignalDatabase.mms();
        SmsDatabase sms = SignalDatabase.sms();
        long latestGroupQuitTimestamp = mms.getLatestGroupQuitTimestamp(j, j2);
        return new Pair<>(sms.getOldestGroupUpdateSender(j, latestGroupQuitTimestamp), Long.valueOf(latestGroupQuitTimestamp));
    }

    public int getMessagePositionOnOrAfterTimestamp(long j, long j2) {
        Cursor queryTables = queryTables(new String[]{"COUNT(*)"}, "thread_id = " + j + " AND " + MmsSmsColumns.NORMALIZED_DATE_RECEIVED + " >= " + j2 + " AND " + MmsDatabase.STORY_TYPE + " = 0 AND parent_story_id <= 0", null, null, false);
        if (queryTables != null) {
            try {
                if (queryTables.moveToNext()) {
                    int i = queryTables.getInt(0);
                    queryTables.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    queryTables.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (queryTables != null) {
            queryTables.close();
        }
        return 0;
    }

    public MessageRecord getMessageFor(long j, RecipientId recipientId) {
        MessageRecord next;
        Recipient resolved = Recipient.resolved(recipientId);
        String[] strArr = PROJECTION;
        Cursor queryTables = queryTables(strArr, "date_sent = " + j, null, null, true);
        try {
            Reader readerFor = readerFor(queryTables);
            while (true) {
                next = readerFor.getNext();
                if (next != null) {
                    if ((!resolved.isSelf() || !next.isOutgoing()) && (resolved.isSelf() || !next.getIndividualRecipient().getId().equals(recipientId))) {
                    }
                } else if (queryTables == null) {
                    return null;
                } else {
                    queryTables.close();
                    return null;
                }
            }
            if (queryTables != null) {
                queryTables.close();
            }
            return next;
        } catch (Throwable th) {
            if (queryTables != null) {
                try {
                    queryTables.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public List<MessageRecord> getMessagesAfterVoiceNoteInclusive(long j, long j2) throws NoSuchMessageException {
        MessageRecord messageRecord = SignalDatabase.mms().getMessageRecord(j);
        List<MessageRecord> messagesInThreadAfterInclusive = SignalDatabase.mms().getMessagesInThreadAfterInclusive(messageRecord.getThreadId(), messageRecord.getDateReceived(), j2);
        messagesInThreadAfterInclusive.addAll(SignalDatabase.sms().getMessagesInThreadAfterInclusive(messageRecord.getThreadId(), messageRecord.getDateReceived(), j2));
        Collections.sort(messagesInThreadAfterInclusive, new Comparator() { // from class: org.thoughtcrime.securesms.database.MmsSmsDatabase$$ExternalSyntheticLambda2
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return MmsSmsDatabase.lambda$getMessagesAfterVoiceNoteInclusive$0((MessageRecord) obj, (MessageRecord) obj2);
            }
        });
        return Stream.of(messagesInThreadAfterInclusive).limit(j2).toList();
    }

    public static /* synthetic */ int lambda$getMessagesAfterVoiceNoteInclusive$0(MessageRecord messageRecord, MessageRecord messageRecord2) {
        return Long.compare(messageRecord.getDateReceived(), messageRecord2.getDateReceived());
    }

    public Cursor getConversation(long j, long j2, long j3) {
        String str;
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String str2 = "thread_id = " + j + " AND " + MmsDatabase.STORY_TYPE + " = 0 AND parent_story_id <= 0";
        if (j3 > 0 || j2 > 0) {
            str = j2 + ", " + j3;
        } else {
            str = null;
        }
        return signalReadableDatabase.rawQuery(buildQuery(PROJECTION, str2, "date_received DESC", str, false), (String[]) null);
    }

    public Cursor getConversation(long j) {
        return getConversation(j, 0, 0);
    }

    public MessageRecord getConversationSnippet(long j) throws NoSuchMessageException {
        Cursor conversationSnippetCursor = getConversationSnippetCursor(j);
        try {
            if (conversationSnippetCursor.moveToFirst()) {
                boolean requireBoolean = CursorUtil.requireBoolean(conversationSnippetCursor, TRANSPORT);
                long requireLong = CursorUtil.requireLong(conversationSnippetCursor, "_id");
                if (requireBoolean) {
                    MessageRecord messageRecord = SignalDatabase.mms().getMessageRecord(requireLong);
                    conversationSnippetCursor.close();
                    return messageRecord;
                }
                MessageRecord messageRecord2 = SignalDatabase.sms().getMessageRecord(requireLong);
                conversationSnippetCursor.close();
                return messageRecord2;
            }
            throw new NoSuchMessageException("no message");
        } catch (Throwable th) {
            if (conversationSnippetCursor != null) {
                try {
                    conversationSnippetCursor.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    Cursor getConversationSnippetCursor(long j) {
        return this.databaseHelper.getSignalReadableDatabase().rawQuery(SNIPPET_QUERY, SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(j)));
    }

    public long getConversationSnippetType(long j) throws NoSuchMessageException {
        Cursor rawQuery = this.databaseHelper.getSignalReadableDatabase().rawQuery(SNIPPET_QUERY, SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(j)));
        try {
            if (rawQuery.moveToFirst()) {
                long requireLong = CursorUtil.requireLong(rawQuery, "type");
                rawQuery.close();
                return requireLong;
            }
            throw new NoSuchMessageException("no message");
        } catch (Throwable th) {
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public Cursor getMessagesForNotificationState(Collection<MessageNotifierV2.StickyThread> collection) {
        StringBuilder sb = new StringBuilder();
        for (MessageNotifierV2.StickyThread stickyThread : collection) {
            if (sb.length() > 0) {
                sb.append(" OR ");
            }
            sb.append("(");
            sb.append("thread_id = ");
            sb.append(stickyThread.getConversationId().getThreadId());
            sb.append(" AND ");
            sb.append(MmsSmsColumns.NORMALIZED_DATE_RECEIVED);
            sb.append(" >= ");
            sb.append(stickyThread.getEarliestTimestamp());
            sb.append(getStickyWherePartForParentStoryId(stickyThread.getConversationId().getGroupStoryId()));
            sb.append(")");
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("notified = 0 AND is_story = 0 AND (read = 0 OR reactions_unread = 1");
        sb2.append(sb.length() > 0 ? " OR (" + ((Object) sb) + ")" : "");
        sb2.append(")");
        return queryTables(PROJECTION, sb2.toString(), "date_received ASC", null, true);
    }

    public boolean isQuoted(MessageRecord messageRecord) {
        Cursor query = getReadableDatabase().query("mms", new String[]{SubscriptionLevels.BOOST_LEVEL}, "quote_id = ?  AND quote_author = ?", SqlUtil.buildArgs(Long.valueOf(messageRecord.getDateSent()), (messageRecord.isOutgoing() ? Recipient.self() : messageRecord.getRecipient()).getId()), null, null, null, SubscriptionLevels.BOOST_LEVEL);
        try {
            boolean moveToFirst = query.moveToFirst();
            query.close();
            return moveToFirst;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public List<MessageRecord> getAllMessagesThatQuote(MessageId messageId) {
        try {
            MessageRecord messageRecord = messageId.isMms() ? SignalDatabase.mms().getMessageRecord(messageId.getId()) : SignalDatabase.sms().getMessageRecord(messageId.getId());
            RecipientId id = (messageRecord.isOutgoing() ? Recipient.self() : messageRecord.getRecipient()).getId();
            String str = "quote_id = " + messageRecord.getDateSent() + " AND quote_author = " + id.serialize();
            ArrayList arrayList = new ArrayList();
            Reader reader = new Reader(queryTables(PROJECTION, str, "date_received DESC", null, true));
            while (true) {
                try {
                    MessageRecord next = reader.getNext();
                    if (next != null) {
                        arrayList.add(next);
                        arrayList.addAll(getAllMessagesThatQuote(new MessageId(next.getId(), next.isMms())));
                    } else {
                        reader.close();
                        Collections.sort(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.database.MmsSmsDatabase$$ExternalSyntheticLambda0
                            @Override // java.util.Comparator
                            public final int compare(Object obj, Object obj2) {
                                return MmsSmsDatabase.lambda$getAllMessagesThatQuote$1((MessageRecord) obj, (MessageRecord) obj2);
                            }
                        });
                        return arrayList;
                    }
                } catch (Throwable th) {
                    try {
                        reader.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                    throw th;
                }
            }
        } catch (NoSuchMessageException unused) {
            throw new IllegalArgumentException("Invalid message ID!");
        }
    }

    public static /* synthetic */ int lambda$getAllMessagesThatQuote$1(MessageRecord messageRecord, MessageRecord messageRecord2) {
        if (messageRecord.getDateReceived() > messageRecord2.getDateReceived()) {
            return -1;
        }
        return messageRecord.getDateReceived() < messageRecord2.getDateReceived() ? 1 : 0;
    }

    private String getStickyWherePartForParentStoryId(Long l) {
        if (l == null) {
            return " AND parent_story_id <= 0";
        }
        return " AND parent_story_id = " + l;
    }

    public int getUnreadCount(long j) {
        int count;
        Cursor queryTables = queryTables(PROJECTION, "read = 0 AND is_story = 0 AND thread_id = " + j + " AND parent_story_id <= 0", null, null, false);
        if (queryTables != null) {
            try {
                count = queryTables.getCount();
            } catch (Throwable th) {
                try {
                    queryTables.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        } else {
            count = 0;
        }
        if (queryTables != null) {
            queryTables.close();
        }
        return count;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean checkMessageExists(org.thoughtcrime.securesms.database.model.MessageRecord r4) {
        /*
            r3 = this;
            boolean r0 = r4.isMms()
            if (r0 == 0) goto L_0x000b
            org.thoughtcrime.securesms.database.MmsDatabase r0 = org.thoughtcrime.securesms.database.SignalDatabase.mms()
            goto L_0x000f
        L_0x000b:
            org.thoughtcrime.securesms.database.SmsDatabase r0 = org.thoughtcrime.securesms.database.SignalDatabase.sms()
        L_0x000f:
            long r1 = r4.getId()
            android.database.Cursor r4 = r0.getMessageCursor(r1)
            if (r4 == 0) goto L_0x002b
            int r0 = r4.getCount()     // Catch: all -> 0x0021
            if (r0 <= 0) goto L_0x002b
            r0 = 1
            goto L_0x002c
        L_0x0021:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0026
            goto L_0x002a
        L_0x0026:
            r4 = move-exception
            r0.addSuppressed(r4)
        L_0x002a:
            throw r0
        L_0x002b:
            r0 = 0
        L_0x002c:
            if (r4 == 0) goto L_0x0031
            r4.close()
        L_0x0031:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.MmsSmsDatabase.checkMessageExists(org.thoughtcrime.securesms.database.model.MessageRecord):boolean");
    }

    public int getSecureConversationCount(long j) {
        if (j == -1) {
            return 0;
        }
        return SignalDatabase.sms().getSecureMessageCount(j) + SignalDatabase.mms().getSecureMessageCount(j);
    }

    public int getOutgoingSecureConversationCount(long j) {
        if (j == -1) {
            return 0;
        }
        return SignalDatabase.sms().getOutgoingSecureMessageCount(j) + SignalDatabase.mms().getOutgoingSecureMessageCount(j);
    }

    public int getConversationCount(long j) {
        return SignalDatabase.sms().getMessageCountForThread(j) + SignalDatabase.mms().getMessageCountForThread(j);
    }

    public int getConversationCount(long j, long j2) {
        return SignalDatabase.sms().getMessageCountForThread(j, j2) + SignalDatabase.mms().getMessageCountForThread(j, j2);
    }

    public int getInsecureSentCount(long j) {
        return SignalDatabase.sms().getInsecureMessagesSentForThread(j) + SignalDatabase.mms().getInsecureMessagesSentForThread(j);
    }

    public int getInsecureMessageCountForInsights() {
        return SignalDatabase.sms().getInsecureMessageCountForInsights() + SignalDatabase.mms().getInsecureMessageCountForInsights();
    }

    public int getMessageCountBeforeDate(long j) {
        Cursor queryTables = queryTables(new String[]{"COUNT(*)"}, "date_received < " + j, null, null, false);
        if (queryTables != null) {
            try {
                if (queryTables.moveToFirst()) {
                    int i = queryTables.getInt(0);
                    queryTables.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    queryTables.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (queryTables != null) {
            queryTables.close();
        }
        return 0;
    }

    public int getSecureMessageCountForInsights() {
        return SignalDatabase.sms().getSecureMessageCountForInsights() + SignalDatabase.mms().getSecureMessageCountForInsights();
    }

    public boolean hasMeaningfulMessage(long j) {
        if (j == -1) {
            return false;
        }
        if (SignalDatabase.sms().hasMeaningfulMessage(j) || SignalDatabase.mms().hasMeaningfulMessage(j)) {
            return true;
        }
        return false;
    }

    public long getThreadId(MessageId messageId) {
        if (messageId.isMms()) {
            return SignalDatabase.mms().getThreadIdForMessage(messageId.getId());
        }
        return SignalDatabase.sms().getThreadIdForMessage(messageId.getId());
    }

    @Deprecated
    public long getThreadForMessageId(long j) {
        long threadIdForMessage = SignalDatabase.sms().getThreadIdForMessage(j);
        return threadIdForMessage == -1 ? SignalDatabase.mms().getThreadIdForMessage(j) : threadIdForMessage;
    }

    public Collection<MessageDatabase.SyncMessageId> incrementDeliveryReceiptCounts(List<MessageDatabase.SyncMessageId> list, long j) {
        return incrementReceiptCounts(list, j, MessageDatabase.ReceiptType.DELIVERY);
    }

    public boolean incrementDeliveryReceiptCount(MessageDatabase.SyncMessageId syncMessageId, long j) {
        return incrementReceiptCount(syncMessageId, j, MessageDatabase.ReceiptType.DELIVERY);
    }

    public Collection<MessageDatabase.SyncMessageId> incrementReadReceiptCounts(List<MessageDatabase.SyncMessageId> list, long j) {
        return incrementReceiptCounts(list, j, MessageDatabase.ReceiptType.READ);
    }

    public boolean incrementReadReceiptCount(MessageDatabase.SyncMessageId syncMessageId, long j) {
        return incrementReceiptCount(syncMessageId, j, MessageDatabase.ReceiptType.READ);
    }

    public Collection<MessageDatabase.SyncMessageId> incrementViewedReceiptCounts(List<MessageDatabase.SyncMessageId> list, long j) {
        return incrementReceiptCounts(list, j, MessageDatabase.ReceiptType.VIEWED);
    }

    public boolean incrementViewedReceiptCount(MessageDatabase.SyncMessageId syncMessageId, long j) {
        return incrementReceiptCount(syncMessageId, j, MessageDatabase.ReceiptType.VIEWED);
    }

    public Collection<MessageDatabase.SyncMessageId> incrementViewedStoryReceiptCounts(List<MessageDatabase.SyncMessageId> list, long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        HashSet<MessageDatabase.MessageUpdate> hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        signalWritableDatabase.beginTransaction();
        try {
            for (MessageDatabase.SyncMessageId syncMessageId : list) {
                Set<MessageDatabase.MessageUpdate> incrementStoryReceiptCountInternal = incrementStoryReceiptCountInternal(syncMessageId, j, MessageDatabase.ReceiptType.VIEWED);
                if (incrementStoryReceiptCountInternal.size() > 0) {
                    hashSet.addAll(incrementStoryReceiptCountInternal);
                } else {
                    hashSet2.add(syncMessageId);
                }
            }
            signalWritableDatabase.setTransactionSuccessful();
            return hashSet2;
        } finally {
            signalWritableDatabase.endTransaction();
            for (MessageDatabase.MessageUpdate messageUpdate : hashSet) {
                ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(messageUpdate.getMessageId());
                ApplicationDependencies.getDatabaseObserver().notifyVerboseConversationListeners(Collections.singleton(Long.valueOf(messageUpdate.getThreadId())));
            }
            if (hashSet.size() > 0) {
                notifyConversationListListeners();
            }
        }
    }

    private boolean incrementReceiptCount(MessageDatabase.SyncMessageId syncMessageId, long j, MessageDatabase.ReceiptType receiptType) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ThreadDatabase threads = SignalDatabase.threads();
        Set<MessageDatabase.MessageUpdate> hashSet = new HashSet<>();
        signalWritableDatabase.beginTransaction();
        try {
            hashSet = incrementReceiptCountInternal(syncMessageId, j, receiptType);
            for (MessageDatabase.MessageUpdate messageUpdate : hashSet) {
                threads.update(messageUpdate.getThreadId(), false);
            }
            signalWritableDatabase.setTransactionSuccessful();
            if (hashSet.size() > 0) {
                return true;
            }
            return false;
        } finally {
            signalWritableDatabase.endTransaction();
            for (MessageDatabase.MessageUpdate next : hashSet) {
                ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(next.getMessageId());
            }
        }
    }

    private Collection<MessageDatabase.SyncMessageId> incrementReceiptCounts(List<MessageDatabase.SyncMessageId> list, long j, MessageDatabase.ReceiptType receiptType) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ThreadDatabase threads = SignalDatabase.threads();
        HashSet<MessageDatabase.MessageUpdate> hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        signalWritableDatabase.beginTransaction();
        try {
            for (MessageDatabase.SyncMessageId syncMessageId : list) {
                Set<MessageDatabase.MessageUpdate> incrementReceiptCountInternal = incrementReceiptCountInternal(syncMessageId, j, receiptType);
                if (incrementReceiptCountInternal.size() > 0) {
                    hashSet.addAll(incrementReceiptCountInternal);
                } else {
                    hashSet2.add(syncMessageId);
                }
            }
            for (MessageDatabase.MessageUpdate messageUpdate : hashSet) {
                threads.updateSilently(messageUpdate.getThreadId(), false);
            }
            signalWritableDatabase.setTransactionSuccessful();
            return hashSet2;
        } finally {
            signalWritableDatabase.endTransaction();
            for (MessageDatabase.MessageUpdate messageUpdate2 : hashSet) {
                ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(messageUpdate2.getMessageId());
                ApplicationDependencies.getDatabaseObserver().notifyVerboseConversationListeners(Collections.singleton(Long.valueOf(messageUpdate2.getThreadId())));
            }
            if (hashSet.size() > 0) {
                notifyConversationListListeners();
            }
        }
    }

    private Set<MessageDatabase.MessageUpdate> incrementReceiptCountInternal(MessageDatabase.SyncMessageId syncMessageId, long j, MessageDatabase.ReceiptType receiptType) {
        HashSet hashSet = new HashSet();
        hashSet.addAll(SignalDatabase.sms().incrementReceiptCount(syncMessageId, j, receiptType, false));
        hashSet.addAll(SignalDatabase.mms().incrementReceiptCount(syncMessageId, j, receiptType, false));
        return hashSet;
    }

    private Set<MessageDatabase.MessageUpdate> incrementStoryReceiptCountInternal(MessageDatabase.SyncMessageId syncMessageId, long j, MessageDatabase.ReceiptType receiptType) {
        return SignalDatabase.mms().incrementReceiptCount(syncMessageId, j, receiptType, true);
    }

    public void updateViewedStories(Set<MessageDatabase.SyncMessageId> set) {
        SignalDatabase.mms().updateViewedStories(set);
    }

    /* JADX INFO: finally extract failed */
    public Collection<MessageDatabase.SyncMessageId> setTimestampRead(Recipient recipient, List<ReadMessage> list, long j, Map<Long, Long> map) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        LinkedList<Pair> linkedList = new LinkedList();
        LinkedList<Pair> linkedList2 = new LinkedList();
        HashSet<Long> hashSet = new HashSet();
        LinkedList linkedList3 = new LinkedList();
        writableDatabase.beginTransaction();
        try {
            for (ReadMessage readMessage : list) {
                TimestampReadResult timestampRead = SignalDatabase.sms().setTimestampRead(new MessageDatabase.SyncMessageId(recipient.getId(), readMessage.getTimestamp()), j, map);
                TimestampReadResult timestampRead2 = SignalDatabase.mms().setTimestampRead(new MessageDatabase.SyncMessageId(recipient.getId(), readMessage.getTimestamp()), j, map);
                linkedList.addAll(timestampRead.expiring);
                linkedList2.addAll(timestampRead2.expiring);
                hashSet.addAll(timestampRead.threads);
                hashSet.addAll(timestampRead2.threads);
                if (timestampRead.threads.isEmpty() && timestampRead2.threads.isEmpty()) {
                    linkedList3.add(new MessageDatabase.SyncMessageId(recipient.getId(), readMessage.getTimestamp()));
                }
            }
            for (Long l : hashSet) {
                long longValue = l.longValue();
                SignalDatabase.threads().updateReadState(longValue);
                SignalDatabase.threads().setLastSeen(longValue);
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            for (Pair pair : linkedList) {
                ApplicationDependencies.getExpiringMessageManager().scheduleDeletion(((Long) pair.first()).longValue(), false, j, ((Long) pair.second()).longValue());
            }
            for (Pair pair2 : linkedList2) {
                ApplicationDependencies.getExpiringMessageManager().scheduleDeletion(((Long) pair2.first()).longValue(), true, j, ((Long) pair2.second()).longValue());
            }
            for (Long l2 : hashSet) {
                notifyConversationListeners(l2.longValue());
            }
            return linkedList3;
        } catch (Throwable th) {
            writableDatabase.endTransaction();
            throw th;
        }
    }

    public int getQuotedMessagePosition(long j, long j2, RecipientId recipientId) {
        Cursor queryTables = queryTables(new String[]{"date_sent", "address", MmsSmsColumns.REMOTE_DELETED}, "thread_id = " + j + " AND " + MmsDatabase.STORY_TYPE + " = 0 AND parent_story_id <= 0", "date_received DESC", null, false);
        try {
            boolean isSelf = Recipient.resolved(recipientId).isSelf();
            while (queryTables != null && queryTables.moveToNext()) {
                boolean z = false;
                if (queryTables.getLong(0) == j2) {
                    z = true;
                }
                boolean equals = recipientId.equals(RecipientId.from(CursorUtil.requireLong(queryTables, "address")));
                if (z && (equals || isSelf)) {
                    if (CursorUtil.requireBoolean(queryTables, MmsSmsColumns.REMOTE_DELETED)) {
                        queryTables.close();
                        return -1;
                    }
                    int position = queryTables.getPosition();
                    queryTables.close();
                    return position;
                }
            }
            if (queryTables != null) {
                queryTables.close();
            }
            return -1;
        } catch (Throwable th) {
            if (queryTables != null) {
                try {
                    queryTables.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public int getMessagePositionInConversation(long j, long j2, RecipientId recipientId) {
        Cursor queryTables = queryTables(new String[]{MmsSmsColumns.NORMALIZED_DATE_RECEIVED, "address", MmsSmsColumns.REMOTE_DELETED}, "thread_id = " + j + " AND " + MmsDatabase.STORY_TYPE + " = 0 AND parent_story_id <= 0", "date_received DESC", null, false);
        try {
            boolean isSelf = Recipient.resolved(recipientId).isSelf();
            while (queryTables != null && queryTables.moveToNext()) {
                boolean z = false;
                if (queryTables.getLong(0) == j2) {
                    z = true;
                }
                boolean equals = recipientId.equals(RecipientId.from(queryTables.getLong(1)));
                if (z && (equals || isSelf)) {
                    if (CursorUtil.requireBoolean(queryTables, MmsSmsColumns.REMOTE_DELETED)) {
                        queryTables.close();
                        return -1;
                    }
                    int position = queryTables.getPosition();
                    queryTables.close();
                    return position;
                }
            }
            if (queryTables != null) {
                queryTables.close();
            }
            return -1;
        } catch (Throwable th) {
            if (queryTables != null) {
                try {
                    queryTables.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public boolean hasReceivedAnyCallsSince(long j, long j2) {
        return SignalDatabase.sms().hasReceivedAnyCallsSince(j, j2);
    }

    public int getMessagePositionInConversation(long j, long j2) {
        return getMessagePositionInConversation(j, 0, j2);
    }

    public int getMessagePositionInConversation(long j, long j2, long j3) {
        String str;
        String str2;
        if (j2 > 0) {
            str2 = "thread_id = " + j + " AND " + MmsSmsColumns.NORMALIZED_DATE_RECEIVED + " < " + j3 + " AND " + MmsDatabase.STORY_TYPE + " = 0 AND parent_story_id = " + j2;
            str = "date_received ASC";
        } else {
            str2 = "thread_id = " + j + " AND " + MmsSmsColumns.NORMALIZED_DATE_RECEIVED + " > " + j3 + " AND " + MmsDatabase.STORY_TYPE + " = 0 AND parent_story_id <= 0";
            str = "date_received DESC";
        }
        Cursor queryTables = queryTables(new String[]{"COUNT(*)"}, str2, str, null, false);
        if (queryTables != null) {
            try {
                if (queryTables.moveToFirst()) {
                    int i = queryTables.getInt(0);
                    queryTables.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    queryTables.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (queryTables == null) {
            return -1;
        }
        queryTables.close();
        return -1;
    }

    public long getTimestampForFirstMessageAfterDate(long j) {
        Cursor queryTables = queryTables(new String[]{MmsSmsColumns.NORMALIZED_DATE_RECEIVED}, "date_received > " + j, "date_received ASC", SubscriptionLevels.BOOST_LEVEL, false);
        if (queryTables != null) {
            try {
                if (queryTables.moveToFirst()) {
                    long j2 = queryTables.getLong(0);
                    queryTables.close();
                    return j2;
                }
            } catch (Throwable th) {
                try {
                    queryTables.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (queryTables == null) {
            return 0;
        }
        queryTables.close();
        return 0;
    }

    public void setNotifiedTimestamp(long j, List<Long> list, List<Long> list2) {
        SignalDatabase.sms().setNotifiedTimestamp(j, list);
        SignalDatabase.mms().setNotifiedTimestamp(j, list2);
    }

    public int deleteMessagesInThreadBeforeDate(long j, long j2) {
        String str = TAG;
        Log.d(str, "deleteMessagesInThreadBeforeData(" + j + ", " + j2 + ")");
        return SignalDatabase.sms().deleteMessagesInThreadBeforeDate(j, j2) + SignalDatabase.mms().deleteMessagesInThreadBeforeDate(j, j2);
    }

    public void deleteAbandonedMessages() {
        Log.d(TAG, "deleteAbandonedMessages()");
        SignalDatabase.sms().deleteAbandonedMessages();
        SignalDatabase.mms().deleteAbandonedMessages();
    }

    public List<MessageDatabase.ReportSpamData> getReportSpamMessageServerData(long j, long j2, int i) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(SignalDatabase.sms().getReportSpamMessageServerGuids(j, j2));
        arrayList.addAll(SignalDatabase.mms().getReportSpamMessageServerGuids(j, j2));
        return (List) Collection$EL.stream(arrayList).sorted(new Comparator() { // from class: org.thoughtcrime.securesms.database.MmsSmsDatabase$$ExternalSyntheticLambda1
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return MmsSmsDatabase.lambda$getReportSpamMessageServerData$2((MessageDatabase.ReportSpamData) obj, (MessageDatabase.ReportSpamData) obj2);
            }
        }).limit((long) i).collect(Collectors.toList());
    }

    public static /* synthetic */ int lambda$getReportSpamMessageServerData$2(MessageDatabase.ReportSpamData reportSpamData, MessageDatabase.ReportSpamData reportSpamData2) {
        return -Long.compare(reportSpamData.getDateReceived(), reportSpamData2.getDateReceived());
    }

    private static String buildQuery(String[] strArr, String str, String str2, String str3, boolean z) {
        String str4 = z ? "json_group_array(json_object('_id', part._id, 'unique_id', part.unique_id, 'mid', part.mid,'data_size', part.data_size, 'file_name', part.file_name, '_data', part._data, 'ct', part.ct, 'cdn_number', part.cdn_number, 'cl', part.cl, 'fast_preflight_id', part.fast_preflight_id, 'voice_note', part.voice_note, 'borderless', part.borderless, 'video_gif', part.video_gif, 'width', part.width, 'height', part.height, 'quote', part.quote, 'cd', part.cd, 'name', part.name, 'pending_push', part.pending_push, 'caption', part.caption, 'sticker_pack_id', part.sticker_pack_id, 'sticker_pack_key', part.sticker_pack_key, 'sticker_id', part.sticker_id, 'sticker_emoji', part.sticker_emoji, 'blur_hash', part.blur_hash, 'transform_properties', part.transform_properties, 'display_order', part.display_order, 'upload_timestamp', part.upload_timestamp))" : "NULL";
        String[] strArr2 = {"date AS date_sent", "date_received AS date_received", "mms._id AS _id", "'MMS::' || mms._id || '::' || date AS unique_row_id", str4 + " AS attachment_json", "body", "read", "thread_id", "type", "address", MmsSmsColumns.ADDRESS_DEVICE_ID, SmsDatabase.SUBJECT, MmsDatabase.MESSAGE_TYPE, MmsDatabase.MESSAGE_BOX, "status", "part_count", "ct_l", "tr_id", "m_size", "exp", "st", MmsSmsColumns.UNIDENTIFIED, "delivery_receipt_count", "read_receipt_count", MmsSmsColumns.MISMATCHED_IDENTITIES, MmsSmsColumns.SUBSCRIPTION_ID, "expires_in", MmsSmsColumns.EXPIRE_STARTED, MmsSmsColumns.NOTIFIED, "network_failures", TRANSPORT, "quote_id", "quote_author", "quote_body", "quote_missing", "quote_attachment", "quote_type", "quote_mentions", "shared_contacts", "previews", MmsDatabase.VIEW_ONCE, MmsSmsColumns.REACTIONS_UNREAD, MmsSmsColumns.REACTIONS_LAST_SEEN, MmsSmsColumns.DATE_SERVER, MmsSmsColumns.REMOTE_DELETED, "mentions_self", MmsSmsColumns.NOTIFIED_TIMESTAMP, MmsSmsColumns.VIEWED_RECEIPT_COUNT, MmsSmsColumns.RECEIPT_TIMESTAMP, "ranges", MmsDatabase.STORY_TYPE, "parent_story_id"};
        String[] strArr3 = {"date_sent AS date_sent", "date AS date_received", "_id", "'SMS::' || _id || '::' || date_sent AS unique_row_id", "NULL AS attachment_json", "body", "read", "thread_id", "type", "address", MmsSmsColumns.ADDRESS_DEVICE_ID, SmsDatabase.SUBJECT, MmsDatabase.MESSAGE_TYPE, MmsDatabase.MESSAGE_BOX, "status", "part_count", "ct_l", "tr_id", "m_size", "exp", "st", MmsSmsColumns.UNIDENTIFIED, "delivery_receipt_count", "read_receipt_count", MmsSmsColumns.MISMATCHED_IDENTITIES, MmsSmsColumns.SUBSCRIPTION_ID, "expires_in", MmsSmsColumns.EXPIRE_STARTED, MmsSmsColumns.NOTIFIED, "network_failures", TRANSPORT, "quote_id", "quote_author", "quote_body", "quote_missing", "quote_attachment", "quote_type", "quote_mentions", "shared_contacts", "previews", MmsDatabase.VIEW_ONCE, MmsSmsColumns.REACTIONS_UNREAD, MmsSmsColumns.REACTIONS_LAST_SEEN, MmsSmsColumns.DATE_SERVER, MmsSmsColumns.REMOTE_DELETED, "mentions_self", MmsSmsColumns.NOTIFIED_TIMESTAMP, MmsSmsColumns.VIEWED_RECEIPT_COUNT, MmsSmsColumns.RECEIPT_TIMESTAMP, "ranges", "0 AS is_story", "0 AS parent_story_id"};
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        SQLiteQueryBuilder sQLiteQueryBuilder2 = new SQLiteQueryBuilder();
        if (z) {
            sQLiteQueryBuilder.setDistinct(true);
            sQLiteQueryBuilder2.setDistinct(true);
        }
        sQLiteQueryBuilder2.setTables("sms");
        if (z) {
            sQLiteQueryBuilder.setTables("mms LEFT OUTER JOIN part ON part.mid = mms._id");
        } else {
            sQLiteQueryBuilder.setTables("mms");
        }
        HashSet hashSet = new HashSet();
        hashSet.add("_id");
        hashSet.add("read");
        hashSet.add("thread_id");
        hashSet.add("body");
        hashSet.add("address");
        hashSet.add(MmsSmsColumns.ADDRESS_DEVICE_ID);
        hashSet.add("delivery_receipt_count");
        hashSet.add("read_receipt_count");
        hashSet.add(MmsSmsColumns.MISMATCHED_IDENTITIES);
        hashSet.add(MmsSmsColumns.SUBSCRIPTION_ID);
        hashSet.add("expires_in");
        hashSet.add(MmsSmsColumns.EXPIRE_STARTED);
        hashSet.add(MmsDatabase.MESSAGE_TYPE);
        hashSet.add(MmsDatabase.MESSAGE_BOX);
        hashSet.add("date");
        hashSet.add(MmsSmsColumns.NORMALIZED_DATE_RECEIVED);
        hashSet.add(MmsSmsColumns.DATE_SERVER);
        hashSet.add("part_count");
        hashSet.add("ct_l");
        hashSet.add("tr_id");
        hashSet.add("m_size");
        hashSet.add("exp");
        hashSet.add(MmsSmsColumns.NOTIFIED);
        hashSet.add("st");
        hashSet.add(MmsSmsColumns.UNIDENTIFIED);
        hashSet.add("network_failures");
        hashSet.add("quote_id");
        hashSet.add("quote_author");
        hashSet.add("quote_body");
        hashSet.add("quote_missing");
        hashSet.add("quote_attachment");
        hashSet.add("quote_type");
        hashSet.add("quote_mentions");
        hashSet.add("shared_contacts");
        hashSet.add("previews");
        hashSet.add(MmsDatabase.VIEW_ONCE);
        hashSet.add(MmsSmsColumns.REACTIONS_UNREAD);
        hashSet.add(MmsSmsColumns.REACTIONS_LAST_SEEN);
        hashSet.add(MmsSmsColumns.REMOTE_DELETED);
        hashSet.add("mentions_self");
        hashSet.add(MmsSmsColumns.NOTIFIED_TIMESTAMP);
        hashSet.add(MmsSmsColumns.VIEWED_RECEIPT_COUNT);
        hashSet.add(MmsSmsColumns.RECEIPT_TIMESTAMP);
        hashSet.add("ranges");
        hashSet.add(MmsDatabase.STORY_TYPE);
        hashSet.add("parent_story_id");
        HashSet hashSet2 = new HashSet();
        hashSet2.add("_id");
        hashSet2.add("body");
        hashSet2.add("address");
        hashSet2.add(MmsSmsColumns.ADDRESS_DEVICE_ID);
        hashSet2.add("read");
        hashSet2.add("thread_id");
        hashSet2.add("delivery_receipt_count");
        hashSet2.add("read_receipt_count");
        hashSet2.add(MmsSmsColumns.MISMATCHED_IDENTITIES);
        hashSet2.add(MmsSmsColumns.SUBSCRIPTION_ID);
        hashSet2.add("expires_in");
        hashSet2.add(MmsSmsColumns.EXPIRE_STARTED);
        hashSet2.add(MmsSmsColumns.NOTIFIED);
        hashSet2.add("type");
        hashSet2.add(SmsDatabase.SUBJECT);
        hashSet2.add("date_sent");
        hashSet2.add("date");
        hashSet2.add(MmsSmsColumns.DATE_SERVER);
        hashSet2.add("status");
        hashSet2.add(MmsSmsColumns.UNIDENTIFIED);
        hashSet2.add(MmsSmsColumns.REACTIONS_UNREAD);
        hashSet2.add(MmsSmsColumns.REACTIONS_LAST_SEEN);
        hashSet2.add(MmsSmsColumns.REMOTE_DELETED);
        hashSet2.add(MmsSmsColumns.NOTIFIED_TIMESTAMP);
        hashSet2.add(MmsSmsColumns.RECEIPT_TIMESTAMP);
        hashSet2.add("0 AS is_story");
        hashSet2.add("0 AS parent_story_id");
        String buildUnionQuery = new SQLiteQueryBuilder().buildUnionQuery(new String[]{sQLiteQueryBuilder2.buildUnionSubQuery(TRANSPORT, strArr3, hashSet2, 4, "sms", str, null, null, null), sQLiteQueryBuilder.buildUnionSubQuery(TRANSPORT, strArr2, hashSet, 4, "mms", str, null, z ? "mms._id" : null, null)}, str2, str3);
        SQLiteQueryBuilder sQLiteQueryBuilder3 = new SQLiteQueryBuilder();
        sQLiteQueryBuilder3.setTables("(" + buildUnionQuery + ")");
        return sQLiteQueryBuilder3.buildQuery(strArr, null, null, null, null, null, null);
    }

    private Cursor queryTables(String[] strArr, String str, String str2, String str3, boolean z) {
        return this.databaseHelper.getSignalReadableDatabase().rawQuery(buildQuery(strArr, str, str2, str3, z), (String[]) null);
    }

    public static Reader readerFor(Cursor cursor) {
        return new Reader(cursor);
    }

    /* loaded from: classes4.dex */
    public static class Reader implements Closeable {
        private final Cursor cursor;
        private MmsDatabase.Reader mmsReader;
        private SmsDatabase.Reader smsReader;

        public Reader(Cursor cursor) {
            this.cursor = cursor;
        }

        private SmsDatabase.Reader getSmsReader() {
            if (this.smsReader == null) {
                this.smsReader = SmsDatabase.readerFor(this.cursor);
            }
            return this.smsReader;
        }

        private MmsDatabase.Reader getMmsReader() {
            if (this.mmsReader == null) {
                this.mmsReader = MmsDatabase.readerFor(this.cursor);
            }
            return this.mmsReader;
        }

        public MessageRecord getNext() {
            Cursor cursor = this.cursor;
            if (cursor == null || !cursor.moveToNext()) {
                return null;
            }
            return getCurrent();
        }

        public MessageRecord getCurrent() {
            Cursor cursor = this.cursor;
            String string = cursor.getString(cursor.getColumnIndexOrThrow(MmsSmsDatabase.TRANSPORT));
            if ("mms".equals(string)) {
                return getMmsReader().getCurrent();
            }
            if ("sms".equals(string)) {
                return getSmsReader().getCurrent();
            }
            throw new AssertionError("Bad type: " + string);
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.cursor.close();
        }
    }

    /* loaded from: classes4.dex */
    public static final class TimestampReadResult {
        final List<Pair<Long, Long>> expiring;
        final List<Long> threads;

        public TimestampReadResult(List<Pair<Long, Long>> list, List<Long> list2) {
            this.expiring = list;
            this.threads = list2;
        }
    }
}
