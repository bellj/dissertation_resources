package org.thoughtcrime.securesms.database;

import com.annimon.stream.function.ToLongFunction;
import org.thoughtcrime.securesms.database.EarlyReceiptCache;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MmsDatabase$$ExternalSyntheticLambda11 implements ToLongFunction {
    @Override // com.annimon.stream.function.ToLongFunction
    public final long applyAsLong(Object obj) {
        return ((EarlyReceiptCache.Receipt) obj).getTimestamp();
    }
}
