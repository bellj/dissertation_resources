package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor;

/* loaded from: classes4.dex */
public final class StoryTextPost extends GeneratedMessageLite<StoryTextPost, Builder> implements StoryTextPostOrBuilder {
    public static final int BACKGROUND_FIELD_NUMBER;
    public static final int BODY_FIELD_NUMBER;
    private static final StoryTextPost DEFAULT_INSTANCE;
    private static volatile Parser<StoryTextPost> PARSER;
    public static final int STYLE_FIELD_NUMBER;
    public static final int TEXTBACKGROUNDCOLOR_FIELD_NUMBER;
    public static final int TEXTFOREGROUNDCOLOR_FIELD_NUMBER;
    private ChatColor background_;
    private String body_ = "";
    private int style_;
    private int textBackgroundColor_;
    private int textForegroundColor_;

    private StoryTextPost() {
    }

    /* loaded from: classes4.dex */
    public enum Style implements Internal.EnumLite {
        DEFAULT(0),
        REGULAR(1),
        BOLD(2),
        SERIF(3),
        SCRIPT(4),
        CONDENSED(5),
        UNRECOGNIZED(-1);
        
        public static final int BOLD_VALUE;
        public static final int CONDENSED_VALUE;
        public static final int DEFAULT_VALUE;
        public static final int REGULAR_VALUE;
        public static final int SCRIPT_VALUE;
        public static final int SERIF_VALUE;
        private static final Internal.EnumLiteMap<Style> internalValueMap = new Internal.EnumLiteMap<Style>() { // from class: org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost.Style.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public Style findValueByNumber(int i) {
                return Style.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        @Deprecated
        public static Style valueOf(int i) {
            return forNumber(i);
        }

        public static Style forNumber(int i) {
            if (i == 0) {
                return DEFAULT;
            }
            if (i == 1) {
                return REGULAR;
            }
            if (i == 2) {
                return BOLD;
            }
            if (i == 3) {
                return SERIF;
            }
            if (i == 4) {
                return SCRIPT;
            }
            if (i != 5) {
                return null;
            }
            return CONDENSED;
        }

        public static Internal.EnumLiteMap<Style> internalGetValueMap() {
            return internalValueMap;
        }

        public static Internal.EnumVerifier internalGetVerifier() {
            return StyleVerifier.INSTANCE;
        }

        /* loaded from: classes4.dex */
        private static final class StyleVerifier implements Internal.EnumVerifier {
            static final Internal.EnumVerifier INSTANCE = new StyleVerifier();

            private StyleVerifier() {
            }

            @Override // com.google.protobuf.Internal.EnumVerifier
            public boolean isInRange(int i) {
                return Style.forNumber(i) != null;
            }
        }

        Style(int i) {
            this.value = i;
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
    public String getBody() {
        return this.body_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
    public ByteString getBodyBytes() {
        return ByteString.copyFromUtf8(this.body_);
    }

    public void setBody(String str) {
        str.getClass();
        this.body_ = str;
    }

    public void clearBody() {
        this.body_ = getDefaultInstance().getBody();
    }

    public void setBodyBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.body_ = byteString.toStringUtf8();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
    public int getStyleValue() {
        return this.style_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
    public Style getStyle() {
        Style forNumber = Style.forNumber(this.style_);
        return forNumber == null ? Style.UNRECOGNIZED : forNumber;
    }

    public void setStyleValue(int i) {
        this.style_ = i;
    }

    public void setStyle(Style style) {
        this.style_ = style.getNumber();
    }

    public void clearStyle() {
        this.style_ = 0;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
    public int getTextForegroundColor() {
        return this.textForegroundColor_;
    }

    public void setTextForegroundColor(int i) {
        this.textForegroundColor_ = i;
    }

    public void clearTextForegroundColor() {
        this.textForegroundColor_ = 0;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
    public int getTextBackgroundColor() {
        return this.textBackgroundColor_;
    }

    public void setTextBackgroundColor(int i) {
        this.textBackgroundColor_ = i;
    }

    public void clearTextBackgroundColor() {
        this.textBackgroundColor_ = 0;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
    public boolean hasBackground() {
        return this.background_ != null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
    public ChatColor getBackground() {
        ChatColor chatColor = this.background_;
        return chatColor == null ? ChatColor.getDefaultInstance() : chatColor;
    }

    public void setBackground(ChatColor chatColor) {
        chatColor.getClass();
        this.background_ = chatColor;
    }

    public void mergeBackground(ChatColor chatColor) {
        chatColor.getClass();
        ChatColor chatColor2 = this.background_;
        if (chatColor2 == null || chatColor2 == ChatColor.getDefaultInstance()) {
            this.background_ = chatColor;
        } else {
            this.background_ = ChatColor.newBuilder(this.background_).mergeFrom((ChatColor.Builder) chatColor).buildPartial();
        }
    }

    public void clearBackground() {
        this.background_ = null;
    }

    public static StoryTextPost parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (StoryTextPost) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static StoryTextPost parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StoryTextPost) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static StoryTextPost parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (StoryTextPost) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static StoryTextPost parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StoryTextPost) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static StoryTextPost parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (StoryTextPost) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static StoryTextPost parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StoryTextPost) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static StoryTextPost parseFrom(InputStream inputStream) throws IOException {
        return (StoryTextPost) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StoryTextPost parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StoryTextPost) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StoryTextPost parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (StoryTextPost) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StoryTextPost parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StoryTextPost) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StoryTextPost parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (StoryTextPost) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static StoryTextPost parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StoryTextPost) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(StoryTextPost storyTextPost) {
        return DEFAULT_INSTANCE.createBuilder(storyTextPost);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<StoryTextPost, Builder> implements StoryTextPostOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(StoryTextPost.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
        public String getBody() {
            return ((StoryTextPost) this.instance).getBody();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
        public ByteString getBodyBytes() {
            return ((StoryTextPost) this.instance).getBodyBytes();
        }

        public Builder setBody(String str) {
            copyOnWrite();
            ((StoryTextPost) this.instance).setBody(str);
            return this;
        }

        public Builder clearBody() {
            copyOnWrite();
            ((StoryTextPost) this.instance).clearBody();
            return this;
        }

        public Builder setBodyBytes(ByteString byteString) {
            copyOnWrite();
            ((StoryTextPost) this.instance).setBodyBytes(byteString);
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
        public int getStyleValue() {
            return ((StoryTextPost) this.instance).getStyleValue();
        }

        public Builder setStyleValue(int i) {
            copyOnWrite();
            ((StoryTextPost) this.instance).setStyleValue(i);
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
        public Style getStyle() {
            return ((StoryTextPost) this.instance).getStyle();
        }

        public Builder setStyle(Style style) {
            copyOnWrite();
            ((StoryTextPost) this.instance).setStyle(style);
            return this;
        }

        public Builder clearStyle() {
            copyOnWrite();
            ((StoryTextPost) this.instance).clearStyle();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
        public int getTextForegroundColor() {
            return ((StoryTextPost) this.instance).getTextForegroundColor();
        }

        public Builder setTextForegroundColor(int i) {
            copyOnWrite();
            ((StoryTextPost) this.instance).setTextForegroundColor(i);
            return this;
        }

        public Builder clearTextForegroundColor() {
            copyOnWrite();
            ((StoryTextPost) this.instance).clearTextForegroundColor();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
        public int getTextBackgroundColor() {
            return ((StoryTextPost) this.instance).getTextBackgroundColor();
        }

        public Builder setTextBackgroundColor(int i) {
            copyOnWrite();
            ((StoryTextPost) this.instance).setTextBackgroundColor(i);
            return this;
        }

        public Builder clearTextBackgroundColor() {
            copyOnWrite();
            ((StoryTextPost) this.instance).clearTextBackgroundColor();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
        public boolean hasBackground() {
            return ((StoryTextPost) this.instance).hasBackground();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPostOrBuilder
        public ChatColor getBackground() {
            return ((StoryTextPost) this.instance).getBackground();
        }

        public Builder setBackground(ChatColor chatColor) {
            copyOnWrite();
            ((StoryTextPost) this.instance).setBackground(chatColor);
            return this;
        }

        public Builder setBackground(ChatColor.Builder builder) {
            copyOnWrite();
            ((StoryTextPost) this.instance).setBackground(builder.build());
            return this;
        }

        public Builder mergeBackground(ChatColor chatColor) {
            copyOnWrite();
            ((StoryTextPost) this.instance).mergeBackground(chatColor);
            return this;
        }

        public Builder clearBackground() {
            copyOnWrite();
            ((StoryTextPost) this.instance).clearBackground();
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new StoryTextPost();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0000\u0000\u0000\u0001Ȉ\u0002\f\u0003\u0004\u0004\u0004\u0005\t", new Object[]{"body_", "style_", "textForegroundColor_", "textBackgroundColor_", "background_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<StoryTextPost> parser = PARSER;
                if (parser == null) {
                    synchronized (StoryTextPost.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        StoryTextPost storyTextPost = new StoryTextPost();
        DEFAULT_INSTANCE = storyTextPost;
        GeneratedMessageLite.registerDefaultInstance(StoryTextPost.class, storyTextPost);
    }

    public static StoryTextPost getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<StoryTextPost> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
