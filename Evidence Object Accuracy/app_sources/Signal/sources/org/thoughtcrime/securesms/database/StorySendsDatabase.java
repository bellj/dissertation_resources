package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.database.Cursor;
import androidx.core.content.ContentValuesKt;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__IndentKt;
import org.signal.core.util.CursorExtensionsKt;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SQLiteDatabaseExtensionsKt;
import org.signal.core.util.SqlUtil;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SentStorySyncManifest;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* compiled from: StorySendsDatabase.kt */
@Metadata(d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 )2\u00020\u0001:\u0001)B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u000b\u001a\u00020\fJ\u0018\u0010\u0011\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0012\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\fJ\u0010\u0010\u0013\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u001c\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00100\u00152\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0016\u001a\u00020\fJ$\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00100\u00182\u0006\u0010\u0012\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0019\u001a\u00020\u000eJ\u001c\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00100\u00182\u0006\u0010\u0012\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\fJ\u001c\u0010\u001b\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00100\u0015J\u0014\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00152\u0006\u0010\u001f\u001a\u00020 J4\u0010!\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\f2\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00100#2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0019\u001a\u00020\u000e2\u0006\u0010$\u001a\u00020%J\u0016\u0010&\u001a\u00020\b2\u0006\u0010'\u001a\u00020\u00102\u0006\u0010(\u001a\u00020\u0010¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/database/StorySendsDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "applySentStoryManifest", "", "remoteManifest", "Lorg/thoughtcrime/securesms/database/SentStorySyncManifest;", "sentTimestamp", "", "canReply", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getFullSentStorySyncManifest", "messageId", "getLocalManifest", "getRecipientIdsForManifestUpdate", "", "deletedMessageId", "getRecipientsToSendTo", "", "allowsReplies", "getRemoteDeleteRecipients", "getSentStorySyncManifestForUpdate", "onlyInclude", "getStoryMessagesFor", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "syncMessageId", "Lorg/thoughtcrime/securesms/database/MessageDatabase$SyncMessageId;", "insert", "recipientIds", "", "distributionId", "Lorg/whispersystems/signalservice/api/push/DistributionId;", "remapRecipient", "oldId", "newId", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StorySendsDatabase extends Database {
    public static final String ALLOWS_REPLIES;
    private static final String CREATE_INDEX = "CREATE INDEX story_sends_recipient_id_sent_timestamp_allows_replies_index ON story_sends (recipient_id, sent_timestamp, allows_replies)";
    private static final String CREATE_TABLE = "CREATE TABLE story_sends (\n  _id INTEGER PRIMARY KEY,\n  message_id INTEGER NOT NULL REFERENCES mms (_id) ON DELETE CASCADE,\n  recipient_id INTEGER NOT NULL REFERENCES recipient (_id) ON DELETE CASCADE,\n  sent_timestamp INTEGER NOT NULL,\n  allows_replies INTEGER NOT NULL,\n  distribution_id TEXT NOT NULL REFERENCES distribution_list (distribution_id) ON DELETE CASCADE\n)";
    public static final Companion Companion = new Companion(null);
    public static final String DISTRIBUTION_ID;
    public static final String ID;
    public static final String MESSAGE_ID;
    public static final String RECIPIENT_ID;
    public static final String SENT_TIMESTAMP;
    public static final String TABLE_NAME;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StorySendsDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    /* compiled from: StorySendsDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0007R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/database/StorySendsDatabase$Companion;", "", "()V", "ALLOWS_REPLIES", "", "CREATE_INDEX", "getCREATE_INDEX", "()Ljava/lang/String;", "CREATE_TABLE", "getCREATE_TABLE", "DISTRIBUTION_ID", "ID", "MESSAGE_ID", "RECIPIENT_ID", "SENT_TIMESTAMP", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final String getCREATE_TABLE() {
            return StorySendsDatabase.CREATE_TABLE;
        }

        public final String getCREATE_INDEX() {
            return StorySendsDatabase.CREATE_INDEX;
        }
    }

    public final void insert(long j, Collection<? extends RecipientId> collection, long j2, boolean z, DistributionId distributionId) {
        Intrinsics.checkNotNullParameter(collection, "recipientIds");
        Intrinsics.checkNotNullParameter(distributionId, "distributionId");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(collection, 10));
            Iterator<T> it = collection.iterator();
            while (it.hasNext()) {
                arrayList.add(ContentValuesKt.contentValuesOf(TuplesKt.to("message_id", Long.valueOf(j)), TuplesKt.to("recipient_id", ((RecipientId) it.next()).serialize()), TuplesKt.to(SENT_TIMESTAMP, Long.valueOf(j2)), TuplesKt.to("allows_replies", Integer.valueOf(CursorExtensionsKt.toInt(z))), TuplesKt.to("distribution_id", distributionId.toString())));
            }
            for (SqlUtil.Query query : SqlUtil.buildBulkInsert(TABLE_NAME, new String[]{"message_id", "recipient_id", SENT_TIMESTAMP, "allows_replies", "distribution_id"}, arrayList)) {
                writableDatabase.execSQL(query.getWhere(), query.getWhereArgs());
            }
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    /* JADX WARN: Type inference failed for: r6v2, types: [java.lang.Throwable, java.lang.String[]] */
    public final List<RecipientId> getRecipientsToSendTo(long j, long j2, boolean z) {
        ArrayList arrayList = new ArrayList();
        th = 0;
        Cursor rawQuery = getReadableDatabase().rawQuery(StringsKt__IndentKt.trimIndent("\n      SELECT DISTINCT recipient_id\n      FROM story_sends\n      WHERE \n        message_id = " + j + "\n        AND recipient_id NOT IN (\n          SELECT recipient_id\n          FROM story_sends\n          WHERE\n            sent_timestamp = " + j2 + "\n            AND message_id < " + j + "\n            AND allows_replies >= " + CursorExtensionsKt.toInt(z) + "\n        )\n        AND recipient_id NOT IN (\n          SELECT recipient_id\n          FROM story_sends\n          WHERE\n            sent_timestamp = " + j2 + "\n            AND message_id > " + j + "\n            AND allows_replies > " + CursorExtensionsKt.toInt(z) + "\n        )\n    "), (String[]) th);
        while (rawQuery.moveToNext()) {
            try {
                Intrinsics.checkNotNullExpressionValue(rawQuery, "cursor");
                arrayList.add(RecipientId.from(CursorExtensionsKt.requireLong(rawQuery, "recipient_id")));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        return arrayList;
    }

    /* JADX WARN: Type inference failed for: r6v1, types: [java.lang.Throwable, java.lang.String[]] */
    public final List<RecipientId> getRemoteDeleteRecipients(long j, long j2) {
        ArrayList arrayList = new ArrayList();
        th = 0;
        Cursor rawQuery = getReadableDatabase().rawQuery(StringsKt__IndentKt.trimIndent("\n      SELECT recipient_id\n      FROM story_sends\n      WHERE\n        message_id = " + j + "\n        AND recipient_id NOT IN (\n          SELECT recipient_id\n          FROM story_sends\n          WHERE message_id != " + j + "\n          AND sent_timestamp = " + j2 + "\n          AND message_id IN (\n            SELECT _id\n            FROM mms\n            WHERE remote_deleted = 0\n          )\n        )\n    "), (String[]) th);
        while (rawQuery.moveToNext()) {
            try {
                Intrinsics.checkNotNullExpressionValue(rawQuery, "cursor");
                arrayList.add(RecipientId.from(CursorExtensionsKt.requireLong(rawQuery, "recipient_id")));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        return arrayList;
    }

    public final boolean canReply(RecipientId recipientId, long j) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{SubscriptionLevels.BOOST_LEVEL}, "recipient_id = ? AND sent_timestamp = ? AND allows_replies = ?", SqlUtil.buildArgs(recipientId, Long.valueOf(j), 1), null, null, null);
        try {
            th = null;
            return query.moveToFirst();
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final Set<MessageId> getStoryMessagesFor(MessageDatabase.SyncMessageId syncMessageId) {
        Intrinsics.checkNotNullParameter(syncMessageId, "syncMessageId");
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"message_id"}, "recipient_id = ? AND sent_timestamp = ?", SqlUtil.buildArgs(syncMessageId.getRecipientId(), Long.valueOf(syncMessageId.getTimetamp())), null, null, null);
        while (query.moveToNext()) {
            try {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                linkedHashSet.add(new MessageId(CursorExtensionsKt.requireLong(query, "message_id"), true));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return linkedHashSet;
    }

    public final void remapRecipient(RecipientId recipientId, RecipientId recipientId2) {
        Intrinsics.checkNotNullParameter(recipientId, "oldId");
        Intrinsics.checkNotNullParameter(recipientId2, "newId");
        getWritableDatabase().update(TABLE_NAME, ContentValuesKt.contentValuesOf(TuplesKt.to("recipient_id", recipientId2.serialize())), "recipient_id = ?", SqlUtil.buildArgs(recipientId));
    }

    /* JADX INFO: finally extract failed */
    public final SentStorySyncManifest getFullSentStorySyncManifest(long j, long j2) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Intrinsics.checkNotNullExpressionValue(readableDatabase, "readableDatabase");
        Cursor run = SQLiteDatabaseExtensionsKt.select(readableDatabase, "message_id").from(TABLE_NAME).where("sent_timestamp = ? AND\n(SELECT remote_deleted FROM mms WHERE _id = message_id) = 0", Long.valueOf(j2)).orderBy("message_id").limit(1).run();
        try {
            long requireLong = run.moveToFirst() ? CursorUtil.requireLong(run, "message_id") : -1;
            CloseableKt.closeFinally(run, null);
            if (requireLong == -1 || requireLong != j) {
                return null;
            }
            return getLocalManifest(j2);
        } finally {
            try {
                throw th;
            } catch (Throwable th) {
            }
        }
    }

    public final SentStorySyncManifest getSentStorySyncManifestForUpdate(long j, Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(set, "onlyInclude");
        List<SentStorySyncManifest.Entry> entries = getLocalManifest(j).getEntries();
        ArrayList arrayList = new ArrayList();
        for (Object obj : entries) {
            if (set.contains(((SentStorySyncManifest.Entry) obj).getRecipientId())) {
                arrayList.add(obj);
            }
        }
        return new SentStorySyncManifest(arrayList);
    }

    public final Set<RecipientId> getRecipientIdsForManifestUpdate(long j, long j2) {
        Cursor rawQuery = getReadableDatabase().rawQuery("SELECT recipient_id\nFROM story_sends\nWHERE sent_timestamp = ?\n    AND recipient_id IN (\n      SELECT recipient_id\n      FROM story_sends\n      WHERE message_id = ?\n    )\n    AND message_id IN (\n      SELECT _id\n      FROM mms\n      WHERE remote_deleted = 0\n    )", new Long[]{Long.valueOf(j), Long.valueOf(j2)});
        try {
            if (rawQuery.getCount() == 0) {
                List unused = CollectionsKt__CollectionsKt.emptyList();
            }
            HashSet hashSet = new HashSet();
            while (rawQuery.moveToNext()) {
                RecipientId from = RecipientId.from(CursorUtil.requireLong(rawQuery, "recipient_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(CursorUtil.requireLong(cursor, RECIPIENT_ID))");
                hashSet.add(from);
            }
            th = null;
            return hashSet;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final void applySentStoryManifest(SentStorySyncManifest sentStorySyncManifest, long j) {
        boolean z;
        boolean z2;
        boolean z3;
        Intrinsics.checkNotNullParameter(sentStorySyncManifest, "remoteManifest");
        if (!sentStorySyncManifest.getEntries().isEmpty()) {
            getWritableDatabase().beginTransaction();
            try {
                SentStorySyncManifest localManifest = getLocalManifest(j);
                Cursor query = getReadableDatabase().query(StringsKt__IndentKt.trimIndent("\n        SELECT mms._id as message_id, distribution_id\n        FROM mms\n        INNER JOIN distribution_list ON recipient_id = address\n        WHERE date = " + j + " AND distribution_id IS NOT NULL\n      "));
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                while (query.moveToNext()) {
                    DistributionId from = DistributionId.from(CursorUtil.requireString(query, "distribution_id"));
                    Long valueOf = Long.valueOf(CursorUtil.requireLong(query, "message_id"));
                    Intrinsics.checkNotNullExpressionValue(from, "distributionId");
                    linkedHashMap.put(from, valueOf);
                }
                CloseableKt.closeFinally(query, null);
                Set<SentStorySyncManifest.Row> flattenToRows = localManifest.flattenToRows(linkedHashMap);
                Set<SentStorySyncManifest.Row> flattenToRows2 = sentStorySyncManifest.flattenToRows(linkedHashMap);
                if (!Intrinsics.areEqual(flattenToRows, flattenToRows2)) {
                    ArrayList arrayList = new ArrayList();
                    for (Object obj : flattenToRows2) {
                        if (!flattenToRows.contains((SentStorySyncManifest.Row) obj)) {
                            arrayList.add(obj);
                        }
                    }
                    ArrayList<SentStorySyncManifest.Row> arrayList2 = new ArrayList();
                    for (Object obj2 : arrayList) {
                        SentStorySyncManifest.Row row = (SentStorySyncManifest.Row) obj2;
                        RecipientId component1 = row.component1();
                        long component2 = row.component2();
                        if (!(flattenToRows instanceof Collection) || !flattenToRows.isEmpty()) {
                            for (SentStorySyncManifest.Row row2 : flattenToRows) {
                                if (row2.getMessageId() == component2 && Intrinsics.areEqual(row2.getRecipientId(), component1)) {
                                    z3 = true;
                                    break;
                                }
                            }
                        }
                        z3 = false;
                        if (z3) {
                            arrayList2.add(obj2);
                        }
                    }
                    ArrayList<SentStorySyncManifest.Row> arrayList3 = new ArrayList();
                    for (Object obj3 : arrayList) {
                        SentStorySyncManifest.Row row3 = (SentStorySyncManifest.Row) obj3;
                        RecipientId component12 = row3.component1();
                        long component22 = row3.component2();
                        if (!(flattenToRows instanceof Collection) || !flattenToRows.isEmpty()) {
                            for (SentStorySyncManifest.Row row4 : flattenToRows) {
                                if (row4.getMessageId() != component22 || !Intrinsics.areEqual(row4.getRecipientId(), component12)) {
                                    z2 = false;
                                    continue;
                                } else {
                                    z2 = true;
                                    continue;
                                }
                                if (z2) {
                                    z = true;
                                    break;
                                }
                            }
                        }
                        z = false;
                        if (!z) {
                            arrayList3.add(obj3);
                        }
                    }
                    for (SentStorySyncManifest.Row row5 : arrayList2) {
                        RecipientId component13 = row5.component1();
                        long component23 = row5.component2();
                        boolean component3 = row5.component3();
                        DistributionId component4 = row5.component4();
                        SQLiteDatabase writableDatabase = getWritableDatabase();
                        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
                        SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME).values(ContentValuesKt.contentValuesOf(TuplesKt.to("allows_replies", Boolean.valueOf(component3)), TuplesKt.to("recipient_id", Long.valueOf(component13.toLong())), TuplesKt.to(SENT_TIMESTAMP, Long.valueOf(j)), TuplesKt.to("message_id", Long.valueOf(component23)), TuplesKt.to("distribution_id", component4.toString())));
                    }
                    for (SentStorySyncManifest.Row row6 : arrayList3) {
                        getWritableDatabase().insert(TABLE_NAME, (String) null, ContentValuesKt.contentValuesOf(TuplesKt.to("allows_replies", Boolean.valueOf(row6.component3())), TuplesKt.to("recipient_id", Long.valueOf(row6.component1().toLong())), TuplesKt.to(SENT_TIMESTAMP, Long.valueOf(j)), TuplesKt.to("message_id", Long.valueOf(row6.component2())), TuplesKt.to("distribution_id", row6.component4().toString())));
                    }
                    ArrayList arrayList4 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(flattenToRows, 10));
                    for (SentStorySyncManifest.Row row7 : flattenToRows) {
                        arrayList4.add(Long.valueOf(row7.getMessageId()));
                    }
                    List list = CollectionsKt___CollectionsKt.distinct(arrayList4);
                    ArrayList arrayList5 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(flattenToRows2, 10));
                    for (SentStorySyncManifest.Row row8 : flattenToRows2) {
                        arrayList5.add(Long.valueOf(row8.getMessageId()));
                    }
                    for (Number number : CollectionsKt___CollectionsKt.minus((Iterable) list, (Iterable) CollectionsKt___CollectionsKt.distinct(arrayList5))) {
                        SignalDatabase.Companion.mms().markAsRemoteDelete(number.longValue());
                    }
                    getWritableDatabase().setTransactionSuccessful();
                }
            } finally {
                getWritableDatabase().endTransaction();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    private final SentStorySyncManifest getLocalManifest(long j) {
        SentStorySyncManifest.Entry entry;
        Cursor rawQuery = getReadableDatabase().rawQuery("SELECT \n    recipient_id,\n    allows_replies,\n    distribution_id\nFROM story_sends\nWHERE story_sends.sent_timestamp = ? AND (\n  SELECT remote_deleted\n  FROM mms\n  WHERE _id = story_sends.message_id\n) = 0", new Long[]{Long.valueOf(j)});
        try {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            while (rawQuery.moveToNext()) {
                RecipientId from = RecipientId.from(CursorUtil.requireLong(rawQuery, "recipient_id"));
                DistributionId from2 = DistributionId.from(CursorUtil.requireString(rawQuery, "distribution_id"));
                boolean requireBoolean = CursorUtil.requireBoolean(rawQuery, "allows_replies");
                SentStorySyncManifest.Entry entry2 = (SentStorySyncManifest.Entry) linkedHashMap.get(from);
                if (entry2 == null || (entry = SentStorySyncManifest.Entry.copy$default(entry2, null, entry2.getAllowedToReply() | requireBoolean, CollectionsKt___CollectionsKt.plus((Collection<? extends DistributionId>) ((Collection<? extends Object>) entry2.getDistributionLists()), from2), 1, null)) == null) {
                    Intrinsics.checkNotNullExpressionValue(from, "recipientId");
                    entry = new SentStorySyncManifest.Entry(from, canReply(from, j), CollectionsKt__CollectionsJVMKt.listOf(from2));
                }
                Intrinsics.checkNotNullExpressionValue(from, "recipientId");
                linkedHashMap.put(from, entry);
            }
            CloseableKt.closeFinally(rawQuery, null);
            return new SentStorySyncManifest(CollectionsKt___CollectionsKt.toList(linkedHashMap.values()));
        } finally {
            try {
                throw th;
            } catch (Throwable th) {
            }
        }
    }
}
