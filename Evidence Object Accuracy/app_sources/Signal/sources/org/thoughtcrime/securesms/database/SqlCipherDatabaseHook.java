package org.thoughtcrime.securesms.database;

import net.zetetic.database.sqlcipher.SQLiteConnection;
import net.zetetic.database.sqlcipher.SQLiteDatabaseHook;

/* loaded from: classes4.dex */
public class SqlCipherDatabaseHook implements SQLiteDatabaseHook {
    @Override // net.zetetic.database.sqlcipher.SQLiteDatabaseHook
    public void preKey(SQLiteConnection sQLiteConnection) {
        sQLiteConnection.execute("PRAGMA cipher_default_kdf_iter = 1;", null, null);
        sQLiteConnection.execute("PRAGMA cipher_default_page_size = 4096;", null, null);
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteDatabaseHook
    public void postKey(SQLiteConnection sQLiteConnection) {
        sQLiteConnection.execute("PRAGMA cipher_compatibility = 3;", null, null);
        sQLiteConnection.execute("PRAGMA kdf_iter = '1';", null, null);
        sQLiteConnection.execute("PRAGMA cipher_page_size = 4096;", null, null);
    }
}
