package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiConsumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;

/* loaded from: classes4.dex */
public class SearchDatabase extends Database {
    public static final String BODY;
    public static final String CONVERSATION_RECIPIENT;
    public static final String[] CREATE_TABLE = {"CREATE VIRTUAL TABLE sms_fts USING fts5(body, thread_id UNINDEXED, content=sms, content_rowid=_id);", "CREATE TRIGGER sms_ai AFTER INSERT ON sms BEGIN\n  INSERT INTO sms_fts(rowid, body, thread_id) VALUES (new._id, new.body, new.thread_id);\nEND;\n", "CREATE TRIGGER sms_ad AFTER DELETE ON sms BEGIN\n  INSERT INTO sms_fts(sms_fts, rowid, body, thread_id) VALUES('delete', old._id, old.body, old.thread_id);\nEND;\n", "CREATE TRIGGER sms_au AFTER UPDATE ON sms BEGIN\n  INSERT INTO sms_fts(sms_fts, rowid, body, thread_id) VALUES('delete', old._id, old.body, old.thread_id);\n  INSERT INTO sms_fts(rowid, body, thread_id) VALUES(new._id, new.body, new.thread_id);\nEND;", "CREATE VIRTUAL TABLE mms_fts USING fts5(body, thread_id UNINDEXED, content=mms, content_rowid=_id);", "CREATE TRIGGER mms_ai AFTER INSERT ON mms BEGIN\n  INSERT INTO mms_fts(rowid, body, thread_id) VALUES (new._id, new.body, new.thread_id);\nEND;\n", "CREATE TRIGGER mms_ad AFTER DELETE ON mms BEGIN\n  INSERT INTO mms_fts(mms_fts, rowid, body, thread_id) VALUES('delete', old._id, old.body, old.thread_id);\nEND;\n", "CREATE TRIGGER mms_au AFTER UPDATE ON mms BEGIN\n  INSERT INTO mms_fts(mms_fts, rowid, body, thread_id) VALUES('delete', old._id, old.body, old.thread_id);\n  INSERT INTO mms_fts(rowid, body, thread_id) VALUES (new._id, new.body, new.thread_id);\nEND;"};
    public static final String ID;
    public static final String IS_MMS;
    private static final String MESSAGES_FOR_THREAD_QUERY;
    private static final String MESSAGES_QUERY;
    public static final String MESSAGE_ID;
    public static final String MESSAGE_RECIPIENT;
    public static final String MMS_FTS_TABLE_NAME;
    public static final String SMS_FTS_TABLE_NAME;
    public static final String SNIPPET;
    public static final String SNIPPET_WRAP;
    public static final String THREAD_ID;

    public SearchDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public Cursor queryMessages(String str) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String createFullTextSearchQuery = createFullTextSearchQuery(str);
        if (TextUtils.isEmpty(createFullTextSearchQuery)) {
            return null;
        }
        return signalReadableDatabase.rawQuery(MESSAGES_QUERY, new String[]{createFullTextSearchQuery, createFullTextSearchQuery});
    }

    public Cursor queryMessages(String str, long j) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String createFullTextSearchQuery = createFullTextSearchQuery(str);
        if (TextUtils.isEmpty(createFullTextSearchQuery)) {
            return null;
        }
        return signalReadableDatabase.rawQuery(MESSAGES_FOR_THREAD_QUERY, new String[]{createFullTextSearchQuery, String.valueOf(j), createFullTextSearchQuery, String.valueOf(j)});
    }

    private static String createFullTextSearchQuery(String str) {
        return ((StringBuilder) Stream.of(str.split(" ")).map(new SearchDatabase$$ExternalSyntheticLambda0()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.SearchDatabase$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return SearchDatabase.$r8$lambda$MJHH_G6RvIqZ5tzYK8L79b_KK9w((String) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.database.SearchDatabase$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SearchDatabase.m1713$r8$lambda$WMZ0IjILsYAJG5IbBbkLg2h2M0((String) obj);
            }
        }).collect(new SearchDatabase$$ExternalSyntheticLambda3(), new BiConsumer() { // from class: org.thoughtcrime.securesms.database.SearchDatabase$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.BiConsumer
            public final void accept(Object obj, Object obj2) {
                SearchDatabase.m1714$r8$lambda$wIY8E6gkXs_v0L26CWvQds7j2E((StringBuilder) obj, (String) obj2);
            }
        })).toString();
    }

    public static /* synthetic */ boolean lambda$createFullTextSearchQuery$0(String str) {
        return str.length() > 0;
    }

    public static /* synthetic */ void lambda$createFullTextSearchQuery$1(StringBuilder sb, String str) {
        sb.append(str);
        sb.append("* ");
    }

    public static String fullTextSearchEscape(String str) {
        return "\"" + str.replace("\"", "\"\"") + "\"";
    }
}
