package org.thoughtcrime.securesms.database;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MmsDatabase$Reader$$ExternalSyntheticLambda1 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return MmsDatabase.Reader.lambda$getMediaMmsMessageRecord$1((LinkPreview) obj);
    }
}
