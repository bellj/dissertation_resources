package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Pair;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.ModernEncryptingPartOutputStream;
import org.thoughtcrime.securesms.database.model.IncomingSticker;
import org.thoughtcrime.securesms.database.model.StickerPackRecord;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.stickers.BlessedPacks;
import org.thoughtcrime.securesms.stickers.StickerPackInstallEvent;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* loaded from: classes4.dex */
public class StickerDatabase extends Database {
    public static final String CONTENT_TYPE;
    private static final String COVER;
    public static final String[] CREATE_INDEXES = {"CREATE INDEX IF NOT EXISTS sticker_pack_id_index ON sticker (pack_id);", "CREATE INDEX IF NOT EXISTS sticker_sticker_id_index ON sticker (sticker_id);"};
    public static final String CREATE_TABLE;
    public static final String DIRECTORY;
    private static final String EMOJI;
    public static final String FILE_LENGTH;
    public static final String FILE_PATH;
    public static final String FILE_RANDOM;
    private static final String INSTALLED;
    private static final String LAST_USED;
    private static final String PACK_AUTHOR;
    static final String PACK_ID;
    private static final String PACK_KEY;
    private static final String PACK_ORDER;
    private static final String PACK_TITLE;
    private static final String STICKER_ID;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(StickerDatabase.class);
    public static final String _ID;
    private final AttachmentSecret attachmentSecret;

    public StickerDatabase(Context context, SignalDatabase signalDatabase, AttachmentSecret attachmentSecret) {
        super(context, signalDatabase);
        this.attachmentSecret = attachmentSecret;
    }

    public void insertSticker(IncomingSticker incomingSticker, InputStream inputStream, boolean z) throws IOException {
        FileInfo saveStickerImage = saveStickerImage(inputStream);
        ContentValues contentValues = new ContentValues();
        contentValues.put(PACK_ID, incomingSticker.getPackId());
        contentValues.put(PACK_KEY, incomingSticker.getPackKey());
        contentValues.put(PACK_TITLE, incomingSticker.getPackTitle());
        contentValues.put(PACK_AUTHOR, incomingSticker.getPackAuthor());
        contentValues.put(STICKER_ID, Integer.valueOf(incomingSticker.getStickerId()));
        contentValues.put("emoji", incomingSticker.getEmoji());
        contentValues.put(CONTENT_TYPE, incomingSticker.getContentType());
        contentValues.put(COVER, Integer.valueOf(incomingSticker.isCover() ? 1 : 0));
        contentValues.put(INSTALLED, Integer.valueOf(incomingSticker.isInstalled() ? 1 : 0));
        contentValues.put(FILE_PATH, saveStickerImage.getFile().getAbsolutePath());
        contentValues.put(FILE_LENGTH, Long.valueOf(saveStickerImage.getLength()));
        contentValues.put(FILE_RANDOM, saveStickerImage.getRandom());
        long insert = this.databaseHelper.getSignalWritableDatabase().insert(TABLE_NAME, (String) null, contentValues);
        if (insert == -1) {
            insert = (long) this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, "pack_id = ? AND sticker_id = ? AND cover = ?", SqlUtil.buildArgs(incomingSticker.getPackId(), Integer.valueOf(incomingSticker.getStickerId()), Integer.valueOf(incomingSticker.isCover() ? 1 : 0)));
        }
        if (insert > 0) {
            notifyStickerListeners();
            if (incomingSticker.isCover()) {
                notifyStickerPackListeners();
                if (incomingSticker.isInstalled() && z) {
                    broadcastInstallEvent(incomingSticker.getPackId());
                }
            }
        }
    }

    public StickerRecord getSticker(String str, int i, boolean z) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "pack_id = ? AND sticker_id = ? AND cover = ?", new String[]{str, String.valueOf(i), String.valueOf(z ? 1 : 0)}, null, null, SubscriptionLevels.BOOST_LEVEL);
        try {
            StickerRecord next = new StickerRecordReader(query).getNext();
            if (query != null) {
                query.close();
            }
            return next;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public StickerPackRecord getStickerPack(String str) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "pack_id = ? AND cover = ?", new String[]{str, SubscriptionLevels.BOOST_LEVEL}, null, null, null, SubscriptionLevels.BOOST_LEVEL);
        try {
            StickerPackRecord next = new StickerPackRecordReader(query).getNext();
            if (query != null) {
                query.close();
            }
            return next;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public Cursor getInstalledStickerPacks() {
        return this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "cover = ? AND installed = ?", new String[]{SubscriptionLevels.BOOST_LEVEL, SubscriptionLevels.BOOST_LEVEL}, null, null, "pack_order ASC");
    }

    public Cursor getStickersByEmoji(String str) {
        return this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "emoji LIKE ? AND cover = ?", new String[]{"%" + str + "%", "0"}, null, null, null);
    }

    public Cursor getAllStickerPacks() {
        return getAllStickerPacks(null);
    }

    public Cursor getAllStickerPacks(String str) {
        return this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "cover = ?", new String[]{SubscriptionLevels.BOOST_LEVEL}, null, null, "pack_order ASC", str);
    }

    public Cursor getStickersForPack(String str) {
        return this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "pack_id = ? AND cover = ?", new String[]{str, "0"}, null, null, "sticker_id ASC");
    }

    public Cursor getRecentlyUsedStickers(int i) {
        return this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "last_used > ? AND cover = ?", new String[]{"0", "0"}, null, null, "last_used DESC", String.valueOf(i));
    }

    public Set<String> getAllStickerFiles() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        HashSet hashSet = new HashSet();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, new String[]{FILE_PATH}, null, null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                hashSet.add(CursorUtil.requireString(query, FILE_PATH));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return hashSet;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.InputStream getStickerStream(long r10) throws java.io.IOException {
        /*
            r9 = this;
            r0 = 1
            java.lang.String[] r5 = new java.lang.String[r0]
            java.lang.String r0 = java.lang.String.valueOf(r10)
            r1 = 0
            r5[r1] = r0
            org.thoughtcrime.securesms.database.SignalDatabase r0 = r9.databaseHelper
            org.thoughtcrime.securesms.database.SQLiteDatabase r1 = r0.getSignalReadableDatabase()
            java.lang.String r2 = "sticker"
            r3 = 0
            java.lang.String r4 = "_id = ?"
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6, r7, r8)
            java.lang.String r1 = "getStickerStream("
            if (r0 == 0) goto L_0x0067
            boolean r2 = r0.moveToNext()     // Catch: all -> 0x0087
            if (r2 == 0) goto L_0x0067
            java.lang.String r2 = "file_path"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch: all -> 0x0087
            java.lang.String r2 = r0.getString(r2)     // Catch: all -> 0x0087
            java.lang.String r3 = "file_random"
            int r3 = r0.getColumnIndexOrThrow(r3)     // Catch: all -> 0x0087
            byte[] r3 = r0.getBlob(r3)     // Catch: all -> 0x0087
            if (r2 == 0) goto L_0x004d
            org.thoughtcrime.securesms.crypto.AttachmentSecret r10 = r9.attachmentSecret     // Catch: all -> 0x0087
            java.io.File r11 = new java.io.File     // Catch: all -> 0x0087
            r11.<init>(r2)     // Catch: all -> 0x0087
            r1 = 0
            java.io.InputStream r10 = org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream.createFor(r10, r3, r11, r1)     // Catch: all -> 0x0087
            r0.close()
            return r10
        L_0x004d:
            java.lang.String r2 = org.thoughtcrime.securesms.database.StickerDatabase.TAG     // Catch: all -> 0x0087
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch: all -> 0x0087
            r3.<init>()     // Catch: all -> 0x0087
            r3.append(r1)     // Catch: all -> 0x0087
            r3.append(r10)     // Catch: all -> 0x0087
            java.lang.String r10 = ") - No sticker data"
            r3.append(r10)     // Catch: all -> 0x0087
            java.lang.String r10 = r3.toString()     // Catch: all -> 0x0087
            org.signal.core.util.logging.Log.w(r2, r10)     // Catch: all -> 0x0087
            goto L_0x0080
        L_0x0067:
            java.lang.String r2 = org.thoughtcrime.securesms.database.StickerDatabase.TAG     // Catch: all -> 0x0087
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch: all -> 0x0087
            r3.<init>()     // Catch: all -> 0x0087
            r3.append(r1)     // Catch: all -> 0x0087
            r3.append(r10)     // Catch: all -> 0x0087
            java.lang.String r10 = ") - Sticker not found."
            r3.append(r10)     // Catch: all -> 0x0087
            java.lang.String r10 = r3.toString()     // Catch: all -> 0x0087
            org.signal.core.util.logging.Log.i(r2, r10)     // Catch: all -> 0x0087
        L_0x0080:
            if (r0 == 0) goto L_0x0085
            r0.close()
        L_0x0085:
            r10 = 0
            return r10
        L_0x0087:
            r10 = move-exception
            if (r0 == 0) goto L_0x0092
            r0.close()     // Catch: all -> 0x008e
            goto L_0x0092
        L_0x008e:
            r11 = move-exception
            r10.addSuppressed(r11)
        L_0x0092:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.StickerDatabase.getStickerStream(long):java.io.InputStream");
    }

    public boolean isPackInstalled(String str) {
        StickerPackRecord stickerPack = getStickerPack(str);
        return stickerPack != null && stickerPack.isInstalled();
    }

    public boolean isPackAvailableAsReference(String str) {
        return getStickerPack(str) != null;
    }

    public void updateStickerLastUsedTime(long j, long j2) {
        String[] strArr = {String.valueOf(j)};
        ContentValues contentValues = new ContentValues();
        contentValues.put(LAST_USED, Long.valueOf(j2));
        this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, "_id = ?", strArr);
        notifyStickerListeners();
        notifyStickerPackListeners();
    }

    public void markPackAsInstalled(String str, boolean z) {
        updatePackInstalled(this.databaseHelper.getSignalWritableDatabase(), str, true, z);
        notifyStickerPackListeners();
    }

    public void deleteOrphanedPacks() {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        String[] strArr = {"0"};
        signalWritableDatabase.beginTransaction();
        boolean z = false;
        try {
            Cursor rawQuery = signalWritableDatabase.rawQuery("SELECT pack_id FROM sticker WHERE installed = ? AND pack_id NOT IN (SELECT DISTINCT sticker_pack_id FROM part WHERE sticker_pack_id NOT NULL)", strArr);
            while (rawQuery != null && rawQuery.moveToNext()) {
                String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow(PACK_ID));
                if (!BlessedPacks.contains(string)) {
                    deletePack(signalWritableDatabase, string);
                    z = true;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            signalWritableDatabase.setTransactionSuccessful();
            if (z) {
                notifyStickerPackListeners();
                notifyStickerListeners();
            }
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public void uninstallPack(String str) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            updatePackInstalled(signalWritableDatabase, str, false, false);
            deleteStickersInPackExceptCover(signalWritableDatabase, str);
            signalWritableDatabase.setTransactionSuccessful();
            notifyStickerPackListeners();
            notifyStickerListeners();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public void updatePackOrder(List<StickerPackRecord> list) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        for (int i = 0; i < list.size(); i++) {
            try {
                String[] strArr = {list.get(i).getPackId(), SubscriptionLevels.BOOST_LEVEL};
                ContentValues contentValues = new ContentValues();
                contentValues.put(PACK_ORDER, Integer.valueOf(i));
                signalWritableDatabase.update(TABLE_NAME, contentValues, "pack_id = ? AND cover = ?", strArr);
            } finally {
                signalWritableDatabase.endTransaction();
            }
        }
        signalWritableDatabase.setTransactionSuccessful();
        notifyStickerPackListeners();
    }

    private void updatePackInstalled(SQLiteDatabase sQLiteDatabase, String str, boolean z, boolean z2) {
        StickerPackRecord stickerPack = getStickerPack(str);
        if (stickerPack == null || stickerPack.isInstalled() != z) {
            ContentValues contentValues = new ContentValues(1);
            contentValues.put(INSTALLED, Integer.valueOf(z ? 1 : 0));
            sQLiteDatabase.update(TABLE_NAME, contentValues, "pack_id = ?", new String[]{str});
            if (z && z2) {
                broadcastInstallEvent(str);
            }
        }
    }

    private FileInfo saveStickerImage(InputStream inputStream) throws IOException {
        File createTempFile = File.createTempFile(TABLE_NAME, ".mms", this.context.getDir(DIRECTORY, 0));
        Pair<byte[], OutputStream> createFor = ModernEncryptingPartOutputStream.createFor(this.attachmentSecret, createTempFile, false);
        return new FileInfo(createTempFile, StreamUtil.copy(inputStream, (OutputStream) createFor.second), (byte[]) createFor.first);
    }

    private void deleteSticker(SQLiteDatabase sQLiteDatabase, long j, String str) {
        sQLiteDatabase.delete(TABLE_NAME, "_id = ?", new String[]{String.valueOf(j)});
        if (!TextUtils.isEmpty(str)) {
            new File(str).delete();
        }
    }

    private void deletePack(SQLiteDatabase sQLiteDatabase, String str) {
        sQLiteDatabase.delete(TABLE_NAME, "pack_id = ?", new String[]{str});
        deleteStickersInPack(sQLiteDatabase, str);
    }

    /* JADX INFO: finally extract failed */
    private void deleteStickersInPack(SQLiteDatabase sQLiteDatabase, String str) {
        String[] strArr = {str};
        sQLiteDatabase.beginTransaction();
        try {
            Cursor query = sQLiteDatabase.query(TABLE_NAME, null, "pack_id = ?", strArr, null, null, null);
            while (query != null && query.moveToNext()) {
                deleteSticker(sQLiteDatabase, query.getLong(query.getColumnIndexOrThrow("_id")), query.getString(query.getColumnIndexOrThrow(FILE_PATH)));
            }
            if (query != null) {
                query.close();
            }
            sQLiteDatabase.setTransactionSuccessful();
            sQLiteDatabase.endTransaction();
            sQLiteDatabase.delete(TABLE_NAME, "pack_id = ?", strArr);
        } catch (Throwable th) {
            sQLiteDatabase.endTransaction();
            throw th;
        }
    }

    private void deleteStickersInPackExceptCover(SQLiteDatabase sQLiteDatabase, String str) {
        String[] strArr = {str, "0"};
        sQLiteDatabase.beginTransaction();
        try {
            Cursor query = sQLiteDatabase.query(TABLE_NAME, null, "pack_id = ? AND cover = ?", strArr, null, null, null);
            while (query != null && query.moveToNext()) {
                deleteSticker(sQLiteDatabase, query.getLong(query.getColumnIndexOrThrow("_id")), query.getString(query.getColumnIndexOrThrow(FILE_PATH)));
            }
            if (query != null) {
                query.close();
            }
            sQLiteDatabase.setTransactionSuccessful();
        } finally {
            sQLiteDatabase.endTransaction();
        }
    }

    private void broadcastInstallEvent(String str) {
        StickerPackRecord stickerPack = getStickerPack(str);
        if (stickerPack != null) {
            EventBus.getDefault().postSticky(new StickerPackInstallEvent(new DecryptableStreamUriLoader.DecryptableUri(stickerPack.getCover().getUri())));
        }
    }

    /* loaded from: classes4.dex */
    public static final class FileInfo {
        private final File file;
        private final long length;
        private final byte[] random;

        private FileInfo(File file, long j, byte[] bArr) {
            this.file = file;
            this.length = j;
            this.random = bArr;
        }

        public File getFile() {
            return this.file;
        }

        public long getLength() {
            return this.length;
        }

        public byte[] getRandom() {
            return this.random;
        }
    }

    /* loaded from: classes4.dex */
    public static final class StickerRecordReader implements Closeable {
        private final Cursor cursor;

        public StickerRecordReader(Cursor cursor) {
            this.cursor = cursor;
        }

        public StickerRecord getNext() {
            Cursor cursor = this.cursor;
            if (cursor == null || !cursor.moveToNext()) {
                return null;
            }
            return getCurrent();
        }

        public StickerRecord getCurrent() {
            Cursor cursor = this.cursor;
            long j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            Cursor cursor2 = this.cursor;
            String string = cursor2.getString(cursor2.getColumnIndexOrThrow(StickerDatabase.PACK_ID));
            Cursor cursor3 = this.cursor;
            String string2 = cursor3.getString(cursor3.getColumnIndexOrThrow(StickerDatabase.PACK_KEY));
            Cursor cursor4 = this.cursor;
            int i = cursor4.getInt(cursor4.getColumnIndexOrThrow(StickerDatabase.STICKER_ID));
            Cursor cursor5 = this.cursor;
            String string3 = cursor5.getString(cursor5.getColumnIndexOrThrow("emoji"));
            Cursor cursor6 = this.cursor;
            String string4 = cursor6.getString(cursor6.getColumnIndexOrThrow(StickerDatabase.CONTENT_TYPE));
            Cursor cursor7 = this.cursor;
            long j2 = cursor7.getLong(cursor7.getColumnIndexOrThrow(StickerDatabase.FILE_LENGTH));
            Cursor cursor8 = this.cursor;
            boolean z = true;
            if (cursor8.getInt(cursor8.getColumnIndexOrThrow(StickerDatabase.COVER)) != 1) {
                z = false;
            }
            return new StickerRecord(j, string, string2, i, string3, string4, j2, z);
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            Cursor cursor = this.cursor;
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class StickerPackRecordReader implements Closeable {
        private final Cursor cursor;

        public StickerPackRecordReader(Cursor cursor) {
            this.cursor = cursor;
        }

        public StickerPackRecord getNext() {
            Cursor cursor = this.cursor;
            if (cursor == null || !cursor.moveToNext()) {
                return null;
            }
            return getCurrent();
        }

        public StickerPackRecord getCurrent() {
            StickerRecord current = new StickerRecordReader(this.cursor).getCurrent();
            Cursor cursor = this.cursor;
            String string = cursor.getString(cursor.getColumnIndexOrThrow(StickerDatabase.PACK_ID));
            Cursor cursor2 = this.cursor;
            String string2 = cursor2.getString(cursor2.getColumnIndexOrThrow(StickerDatabase.PACK_KEY));
            Cursor cursor3 = this.cursor;
            String string3 = cursor3.getString(cursor3.getColumnIndexOrThrow(StickerDatabase.PACK_TITLE));
            Cursor cursor4 = this.cursor;
            String string4 = cursor4.getString(cursor4.getColumnIndexOrThrow(StickerDatabase.PACK_AUTHOR));
            Cursor cursor5 = this.cursor;
            return new StickerPackRecord(string, string2, string3, string4, current, cursor5.getInt(cursor5.getColumnIndexOrThrow(StickerDatabase.INSTALLED)) == 1);
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            Cursor cursor = this.cursor;
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
