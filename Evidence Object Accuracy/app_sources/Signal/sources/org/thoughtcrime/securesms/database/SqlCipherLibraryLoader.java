package org.thoughtcrime.securesms.database;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;

/* compiled from: SqlCipherLibraryLoader.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherLibraryLoader;", "", "()V", "LOCK", "Ljava/lang/Object;", "loaded", "", "load", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class SqlCipherLibraryLoader {
    public static final SqlCipherLibraryLoader INSTANCE = new SqlCipherLibraryLoader();
    private static final Object LOCK = new Object();
    private static volatile boolean loaded;

    private SqlCipherLibraryLoader() {
    }

    @JvmStatic
    public static final void load() {
        if (!loaded) {
            synchronized (LOCK) {
                if (!loaded) {
                    System.loadLibrary("sqlcipher");
                    loaded = true;
                }
                Unit unit = Unit.INSTANCE;
            }
        }
    }
}
