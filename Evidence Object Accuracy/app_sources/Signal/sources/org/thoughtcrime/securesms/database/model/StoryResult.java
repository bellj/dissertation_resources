package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: StoryResult.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/StoryResult;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "messageId", "", "messageSentTimestamp", "isOutgoing", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;JJZ)V", "()Z", "getMessageId", "()J", "getMessageSentTimestamp", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StoryResult {
    private final boolean isOutgoing;
    private final long messageId;
    private final long messageSentTimestamp;
    private final RecipientId recipientId;

    public StoryResult(RecipientId recipientId, long j, long j2, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.recipientId = recipientId;
        this.messageId = j;
        this.messageSentTimestamp = j2;
        this.isOutgoing = z;
    }

    public final RecipientId getRecipientId() {
        return this.recipientId;
    }

    public final long getMessageId() {
        return this.messageId;
    }

    public final long getMessageSentTimestamp() {
        return this.messageSentTimestamp;
    }

    public final boolean isOutgoing() {
        return this.isOutgoing;
    }
}
