package org.thoughtcrime.securesms.database;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__IndentKt;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import net.zetetic.database.sqlcipher.SQLiteOpenHelper;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.DatabaseSecret;
import org.thoughtcrime.securesms.crypto.DatabaseSecretProvider;
import org.thoughtcrime.securesms.database.model.LocalMetricsEvent;
import org.thoughtcrime.securesms.database.model.LocalMetricsSplit;

/* compiled from: LocalMetricsDatabase.kt */
@Metadata(d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0010\u0018\u0000 '2\u00020\u00012\u00020\u0002:\u0004'()*B\u0017\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0006\u0010\b\u001a\u00020\tJ\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0010\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0002J\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012J\b\u0010\u0014\u001a\u00020\u0015H\u0016J\u001a\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\u00120\u0017H\u0002J\u0016\u0010\u0018\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\u001bJ\u0010\u0010\u001c\u001a\u00020\t2\u0006\u0010\u001d\u001a\u00020\u0015H\u0016J\u0010\u0010\u001e\u001a\u00020\t2\u0006\u0010\u001d\u001a\u00020\u0015H\u0016J \u0010\u001f\u001a\u00020\t2\u0006\u0010\u001d\u001a\u00020\u00152\u0006\u0010 \u001a\u00020\u000f2\u0006\u0010!\u001a\u00020\u000fH\u0016J \u0010\"\u001a\u00020\u000b2\u0006\u0010#\u001a\u00020\r2\u0006\u0010$\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J \u0010%\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010&\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase;", "Lnet/zetetic/database/sqlcipher/SQLiteOpenHelper;", "Lorg/thoughtcrime/securesms/database/SignalDatabaseOpenHelper;", "application", "Landroid/app/Application;", "databaseSecret", "Lorg/thoughtcrime/securesms/crypto/DatabaseSecret;", "(Landroid/app/Application;Lorg/thoughtcrime/securesms/crypto/DatabaseSecret;)V", "clear", "", "eventPercent", "", "eventName", "", "percent", "", "getCount", "getMetrics", "", "Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase$EventMetrics;", "getSqlCipherDatabase", "Lnet/zetetic/database/sqlcipher/SQLiteDatabase;", "getUniqueEventNames", "", "insert", "currentTime", "event", "Lorg/thoughtcrime/securesms/database/model/LocalMetricsEvent;", "onCreate", "db", "onOpen", "onUpgrade", "oldVersion", "newVersion", "percentile", "table", "where", "splitPercent", "splitName", "Companion", "EventMetrics", "EventTotals", "SplitMetrics", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LocalMetricsDatabase extends SQLiteOpenHelper implements SignalDatabaseOpenHelper {
    private static final String CREATED_AT;
    private static final String[] CREATE_INDEXES = {"CREATE INDEX events_create_at_index ON events (created_at)", "CREATE INDEX events_event_name_split_name_index ON events (event_name, split_name)", "CREATE INDEX events_duration_index ON events (duration)"};
    private static final String CREATE_TABLE = "CREATE TABLE events (\n  _id INTEGER PRIMARY KEY,\n  created_at INTEGER NOT NULL, \n  event_id TEXT NOT NULL,\n  event_name TEXT NOT NULL,\n  split_name TEXT NOT NULL,\n  duration INTEGER NOT NULL\n)";
    public static final Companion Companion = new Companion(null);
    private static final String DATABASE_NAME;
    private static final int DATABASE_VERSION;
    private static final String DURATION;
    private static final String EVENT_ID;
    private static final String EVENT_NAME;
    private static final String ID;
    private static final long MAX_AGE = TimeUnit.DAYS.toMillis(7);
    private static final String SPLIT_NAME;
    private static final String TABLE_NAME;
    private static final String TAG = Log.tag(LocalMetricsDatabase.class);
    private static volatile LocalMetricsDatabase instance;

    public /* synthetic */ LocalMetricsDatabase(Application application, DatabaseSecret databaseSecret, DefaultConstructorMarker defaultConstructorMarker) {
        this(application, databaseSecret);
    }

    @JvmStatic
    public static final LocalMetricsDatabase getInstance(Application application) {
        return Companion.getInstance(application);
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
    }

    private LocalMetricsDatabase(Application application, DatabaseSecret databaseSecret) {
        super(application, DATABASE_NAME, databaseSecret.asString(), (SQLiteDatabase.CursorFactory) null, 1, 0, new SqlCipherDeletingErrorHandler(DATABASE_NAME), new SqlCipherDatabaseHook());
    }

    /* compiled from: LocalMetricsDatabase.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u001aH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0007R\u000e\u0010\b\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bXT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\n \u0015*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0004\u0018\u00010\u00178\u0002@\u0002X\u000e¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase$Companion;", "", "()V", "CREATED_AT", "", "CREATE_INDEXES", "", "[Ljava/lang/String;", "CREATE_TABLE", "DATABASE_NAME", "DATABASE_VERSION", "", "DURATION", "EVENT_ID", "EVENT_NAME", "ID", "MAX_AGE", "", "SPLIT_NAME", "TABLE_NAME", "TAG", "kotlin.jvm.PlatformType", "instance", "Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase;", "getInstance", "context", "Landroid/app/Application;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final LocalMetricsDatabase getInstance(Application application) {
            Intrinsics.checkNotNullParameter(application, "context");
            if (LocalMetricsDatabase.instance == null) {
                synchronized (LocalMetricsDatabase.class) {
                    if (LocalMetricsDatabase.instance == null) {
                        SqlCipherLibraryLoader.load();
                        DatabaseSecret orCreateDatabaseSecret = DatabaseSecretProvider.getOrCreateDatabaseSecret(application);
                        Intrinsics.checkNotNullExpressionValue(orCreateDatabaseSecret, "getOrCreateDatabaseSecret(context)");
                        LocalMetricsDatabase.instance = new LocalMetricsDatabase(application, orCreateDatabaseSecret, null);
                        LocalMetricsDatabase localMetricsDatabase = LocalMetricsDatabase.instance;
                        Intrinsics.checkNotNull(localMetricsDatabase);
                        localMetricsDatabase.setWriteAheadLoggingEnabled(true);
                    }
                    Unit unit = Unit.INSTANCE;
                }
            }
            LocalMetricsDatabase localMetricsDatabase2 = LocalMetricsDatabase.instance;
            Intrinsics.checkNotNull(localMetricsDatabase2);
            return localMetricsDatabase2;
        }
    }

    /* compiled from: LocalMetricsDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase$EventTotals;", "", "()V", "CREATE_VIEW", "", "getCREATE_VIEW", "()Ljava/lang/String;", "VIEW_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class EventTotals {
        private static final String CREATE_VIEW = "CREATE VIEW event_totals AS\n  SELECT event_id, event_name, SUM(duration) AS duration\n  FROM events\n  GROUP BY event_id";
        public static final EventTotals INSTANCE = new EventTotals();
        public static final String VIEW_NAME;

        private EventTotals() {
        }

        public final String getCREATE_VIEW() {
            return CREATE_VIEW;
        }
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
        Log.i(TAG, "onCreate()");
        sQLiteDatabase.execSQL(CREATE_TABLE);
        for (String str : CREATE_INDEXES) {
            sQLiteDatabase.execSQL(str);
        }
        sQLiteDatabase.execSQL(EventTotals.INSTANCE.getCREATE_VIEW());
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
        sQLiteDatabase.setForeignKeyConstraintsEnabled(true);
    }

    @Override // org.thoughtcrime.securesms.database.SignalDatabaseOpenHelper
    public SQLiteDatabase getSqlCipherDatabase() {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        return writableDatabase;
    }

    public final void insert(long j, LocalMetricsEvent localMetricsEvent) {
        Intrinsics.checkNotNullParameter(localMetricsEvent, "event");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (LocalMetricsSplit localMetricsSplit : localMetricsEvent.getSplits()) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("created_at", Long.valueOf(localMetricsEvent.getCreatedAt()));
                contentValues.put(EVENT_ID, localMetricsEvent.getEventId());
                contentValues.put(EVENT_NAME, localMetricsEvent.getEventName());
                contentValues.put(SPLIT_NAME, localMetricsSplit.getName());
                contentValues.put(DURATION, Long.valueOf(localMetricsSplit.getDuration()));
                Unit unit = Unit.INSTANCE;
                writableDatabase.insert(TABLE_NAME, (String) null, contentValues);
            }
            writableDatabase.delete(TABLE_NAME, "created_at < ?", SqlUtil.buildArgs(j - MAX_AGE));
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public final void clear() {
        getWritableDatabase().delete(TABLE_NAME, (String) null, (String[]) null);
    }

    public final List<EventMetrics> getMetrics() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        readableDatabase.beginTransaction();
        try {
            Map<String, List<String>> uniqueEventNames = getUniqueEventNames();
            ArrayList arrayList = new ArrayList(uniqueEventNames.size());
            Iterator<Map.Entry<String, List<String>>> it = uniqueEventNames.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, List<String>> next = it.next();
                String key = next.getKey();
                List<String> value = next.getValue();
                long count = getCount(key);
                int i = 50;
                long eventPercent = eventPercent(key, 50);
                long eventPercent2 = eventPercent(key, 90);
                long eventPercent3 = eventPercent(key, 99);
                ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(value, 10));
                for (String str : value) {
                    arrayList2.add(new SplitMetrics(str, splitPercent(key, str, i), splitPercent(key, str, 90), splitPercent(key, str, 99)));
                    it = it;
                    i = 50;
                }
                arrayList.add(new EventMetrics(key, count, eventPercent, eventPercent2, eventPercent3, arrayList2));
                it = it;
            }
            readableDatabase.setTransactionSuccessful();
            return arrayList;
        } finally {
            readableDatabase.endTransaction();
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Throwable, java.lang.String[]] */
    private final Map<String, List<String>> getUniqueEventNames() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        th = 0;
        Cursor rawQuery = getReadableDatabase().rawQuery("SELECT DISTINCT event_name, split_name FROM events", (String[]) th);
        while (rawQuery.moveToNext()) {
            try {
                String requireString = CursorUtil.requireString(rawQuery, EVENT_NAME);
                String requireString2 = CursorUtil.requireString(rawQuery, SPLIT_NAME);
                Intrinsics.checkNotNullExpressionValue(requireString, "eventName");
                Object obj = linkedHashMap.get(requireString);
                if (obj == null) {
                    obj = new ArrayList();
                    linkedHashMap.put(requireString, obj);
                }
                Intrinsics.checkNotNullExpressionValue(requireString2, "splitName");
                ((List) obj).add(requireString2);
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        return linkedHashMap;
    }

    private final long getCount(String str) {
        Cursor rawQuery = getReadableDatabase().rawQuery("SELECT COUNT(DISTINCT event_id) FROM events WHERE event_name = ?", SqlUtil.buildArgs(str));
        try {
            th = null;
            return rawQuery.moveToFirst() ? rawQuery.getLong(0) : 0;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    private final long eventPercent(String str, int i) {
        return percentile(EventTotals.VIEW_NAME, "event_name = '" + str + '\'', i);
    }

    private final long splitPercent(String str, String str2, int i) {
        return percentile(TABLE_NAME, "event_name = '" + str + "' AND split_name = '" + str2 + '\'', i);
    }

    /* JADX WARN: Type inference failed for: r5v1, types: [java.lang.Throwable, java.lang.String[]] */
    private final long percentile(String str, String str2, int i) {
        th = 0;
        Cursor rawQuery = getReadableDatabase().rawQuery(StringsKt__IndentKt.trimIndent("\n      SELECT duration\n      FROM " + str + "\n      WHERE " + str2 + "\n      ORDER BY duration ASC\n      LIMIT 1\n      OFFSET (SELECT COUNT(*)\n              FROM " + str + "\n              WHERE " + str2 + ") * " + i + " / 100 - 1\n    "), (String[]) th);
        try {
            return rawQuery.moveToFirst() ? rawQuery.getLong(0) : -1;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    /* compiled from: LocalMetricsDatabase.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0002\u0010\fJ\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0005HÆ\u0003J\u000f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u000b0\nHÆ\u0003JK\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00052\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nHÆ\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010 \u001a\u00020!HÖ\u0001J\t\u0010\"\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR\u0011\u0010\b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000eR\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase$EventMetrics;", "", "name", "", NewHtcHomeBadger.COUNT, "", "p50", "p90", "p99", "splits", "", "Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase$SplitMetrics;", "(Ljava/lang/String;JJJJLjava/util/List;)V", "getCount", "()J", "getName", "()Ljava/lang/String;", "getP50", "getP90", "getP99", "getSplits", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class EventMetrics {
        private final long count;
        private final String name;
        private final long p50;
        private final long p90;
        private final long p99;
        private final List<SplitMetrics> splits;

        public final String component1() {
            return this.name;
        }

        public final long component2() {
            return this.count;
        }

        public final long component3() {
            return this.p50;
        }

        public final long component4() {
            return this.p90;
        }

        public final long component5() {
            return this.p99;
        }

        public final List<SplitMetrics> component6() {
            return this.splits;
        }

        public final EventMetrics copy(String str, long j, long j2, long j3, long j4, List<SplitMetrics> list) {
            Intrinsics.checkNotNullParameter(str, "name");
            Intrinsics.checkNotNullParameter(list, "splits");
            return new EventMetrics(str, j, j2, j3, j4, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EventMetrics)) {
                return false;
            }
            EventMetrics eventMetrics = (EventMetrics) obj;
            return Intrinsics.areEqual(this.name, eventMetrics.name) && this.count == eventMetrics.count && this.p50 == eventMetrics.p50 && this.p90 == eventMetrics.p90 && this.p99 == eventMetrics.p99 && Intrinsics.areEqual(this.splits, eventMetrics.splits);
        }

        public int hashCode() {
            return (((((((((this.name.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.count)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.p50)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.p90)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.p99)) * 31) + this.splits.hashCode();
        }

        public String toString() {
            return "EventMetrics(name=" + this.name + ", count=" + this.count + ", p50=" + this.p50 + ", p90=" + this.p90 + ", p99=" + this.p99 + ", splits=" + this.splits + ')';
        }

        public EventMetrics(String str, long j, long j2, long j3, long j4, List<SplitMetrics> list) {
            Intrinsics.checkNotNullParameter(str, "name");
            Intrinsics.checkNotNullParameter(list, "splits");
            this.name = str;
            this.count = j;
            this.p50 = j2;
            this.p90 = j3;
            this.p99 = j4;
            this.splits = list;
        }

        public final String getName() {
            return this.name;
        }

        public final long getCount() {
            return this.count;
        }

        public final long getP50() {
            return this.p50;
        }

        public final long getP90() {
            return this.p90;
        }

        public final long getP99() {
            return this.p99;
        }

        public final List<SplitMetrics> getSplits() {
            return this.splits;
        }
    }

    /* compiled from: LocalMetricsDatabase.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/database/LocalMetricsDatabase$SplitMetrics;", "", "name", "", "p50", "", "p90", "p99", "(Ljava/lang/String;JJJ)V", "getName", "()Ljava/lang/String;", "getP50", "()J", "getP90", "getP99", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SplitMetrics {
        private final String name;
        private final long p50;
        private final long p90;
        private final long p99;

        public static /* synthetic */ SplitMetrics copy$default(SplitMetrics splitMetrics, String str, long j, long j2, long j3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = splitMetrics.name;
            }
            if ((i & 2) != 0) {
                j = splitMetrics.p50;
            }
            if ((i & 4) != 0) {
                j2 = splitMetrics.p90;
            }
            if ((i & 8) != 0) {
                j3 = splitMetrics.p99;
            }
            return splitMetrics.copy(str, j, j2, j3);
        }

        public final String component1() {
            return this.name;
        }

        public final long component2() {
            return this.p50;
        }

        public final long component3() {
            return this.p90;
        }

        public final long component4() {
            return this.p99;
        }

        public final SplitMetrics copy(String str, long j, long j2, long j3) {
            Intrinsics.checkNotNullParameter(str, "name");
            return new SplitMetrics(str, j, j2, j3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SplitMetrics)) {
                return false;
            }
            SplitMetrics splitMetrics = (SplitMetrics) obj;
            return Intrinsics.areEqual(this.name, splitMetrics.name) && this.p50 == splitMetrics.p50 && this.p90 == splitMetrics.p90 && this.p99 == splitMetrics.p99;
        }

        public int hashCode() {
            return (((((this.name.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.p50)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.p90)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.p99);
        }

        public String toString() {
            return "SplitMetrics(name=" + this.name + ", p50=" + this.p50 + ", p90=" + this.p90 + ", p99=" + this.p99 + ')';
        }

        public SplitMetrics(String str, long j, long j2, long j3) {
            Intrinsics.checkNotNullParameter(str, "name");
            this.name = str;
            this.p50 = j;
            this.p90 = j2;
            this.p99 = j3;
        }

        public final String getName() {
            return this.name;
        }

        public final long getP50() {
            return this.p50;
        }

        public final long getP90() {
            return this.p90;
        }

        public final long getP99() {
            return this.p99;
        }
    }
}
