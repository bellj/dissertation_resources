package org.thoughtcrime.securesms.database.helpers;

import android.database.Cursor;
import android.text.TextUtils;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SessionDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet;
import org.thoughtcrime.securesms.util.DelimiterUtil;

/* loaded from: classes4.dex */
public class RecipientIdCleanupHelper {
    private static final String TAG = Log.tag(RecipientIdCleanupHelper.class);

    public static void execute(SQLiteDatabase sQLiteDatabase) {
        Log.i(TAG, "Beginning migration.");
        long currentTimeMillis = System.currentTimeMillis();
        Pattern compile = Pattern.compile("^[0-9\\-+]+$");
        HashSet hashSet = new HashSet();
        Cursor query = sQLiteDatabase.query(RecipientDatabase.TABLE_NAME, new String[]{"_id", RecipientDatabase.PHONE}, "group_id IS NULL AND email IS NULL", null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                String string = query.getString(query.getColumnIndexOrThrow("_id"));
                String string2 = query.getString(query.getColumnIndexOrThrow(RecipientDatabase.PHONE));
                if (TextUtils.isEmpty(string2) || !compile.matcher(string2).matches()) {
                    String str = TAG;
                    Log.i(str, "Recipient ID " + string + " has non-numeric characters and can potentially be deleted.");
                    if (isIdUsed(sQLiteDatabase, "identities", "address", string) || isIdUsed(sQLiteDatabase, SessionDatabase.TABLE_NAME, "address", string) || isIdUsed(sQLiteDatabase, ThreadDatabase.TABLE_NAME, "recipient_ids", string) || isIdUsed(sQLiteDatabase, "sms", "address", string) || isIdUsed(sQLiteDatabase, "mms", "address", string) || isIdUsed(sQLiteDatabase, "mms", "quote_author", string) || isIdUsed(sQLiteDatabase, GroupReceiptDatabase.TABLE_NAME, "address", string) || isIdUsed(sQLiteDatabase, ChooseGroupStoryBottomSheet.RESULT_SET, "recipient_id", string)) {
                        Log.i(str, "Found that ID " + string + " is actually used in another table.");
                    } else {
                        Log.i(str, "Determined ID " + string + " is unused in non-group membership. Marking for potential deletion.");
                        hashSet.add(string);
                    }
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        for (String str2 : findUnusedInGroupMembership(sQLiteDatabase, hashSet)) {
            String str3 = TAG;
            Log.i(str3, "Deleting ID " + str2);
            sQLiteDatabase.delete(RecipientDatabase.TABLE_NAME, "_id = ?", new String[]{String.valueOf(str2)});
        }
        String str4 = TAG;
        Log.i(str4, "Migration took " + (System.currentTimeMillis() - currentTimeMillis) + " ms.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034 A[Catch: all -> 0x002f, TRY_LEAVE, TryCatch #1 {all -> 0x002f, blocks: (B:4:0x0028, B:11:0x0034), top: B:22:0x0028 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean isIdUsed(net.zetetic.database.sqlcipher.SQLiteDatabase r11, java.lang.String r12, java.lang.String r13, java.lang.String r14) {
        /*
            r0 = 1
            java.lang.String[] r3 = new java.lang.String[r0]
            r10 = 0
            r3[r10] = r13
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r13)
            java.lang.String r2 = " = ?"
            r1.append(r2)
            java.lang.String r4 = r1.toString()
            java.lang.String[] r5 = new java.lang.String[r0]
            r5[r10] = r14
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "1"
            r1 = r11
            r2 = r12
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)
            if (r11 == 0) goto L_0x0031
            boolean r1 = r11.moveToFirst()     // Catch: all -> 0x002f
            if (r1 == 0) goto L_0x0031
            goto L_0x0032
        L_0x002f:
            r12 = move-exception
            goto L_0x0060
        L_0x0031:
            r0 = 0
        L_0x0032:
            if (r0 == 0) goto L_0x006b
            java.lang.String r1 = org.thoughtcrime.securesms.database.helpers.RecipientIdCleanupHelper.TAG     // Catch: all -> 0x002f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch: all -> 0x002f
            r2.<init>()     // Catch: all -> 0x002f
            java.lang.String r3 = "Recipient "
            r2.append(r3)     // Catch: all -> 0x002f
            r2.append(r14)     // Catch: all -> 0x002f
            java.lang.String r14 = " was used in ("
            r2.append(r14)     // Catch: all -> 0x002f
            r2.append(r12)     // Catch: all -> 0x002f
            java.lang.String r12 = ", "
            r2.append(r12)     // Catch: all -> 0x002f
            r2.append(r13)     // Catch: all -> 0x002f
            java.lang.String r12 = ")"
            r2.append(r12)     // Catch: all -> 0x002f
            java.lang.String r12 = r2.toString()     // Catch: all -> 0x002f
            org.signal.core.util.logging.Log.i(r1, r12)     // Catch: all -> 0x002f
            goto L_0x006b
        L_0x0060:
            if (r11 == 0) goto L_0x006a
            r11.close()     // Catch: all -> 0x0066
            goto L_0x006a
        L_0x0066:
            r11 = move-exception
            r12.addSuppressed(r11)
        L_0x006a:
            throw r12
        L_0x006b:
            if (r11 == 0) goto L_0x0070
            r11.close()
        L_0x0070:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.helpers.RecipientIdCleanupHelper.isIdUsed(net.zetetic.database.sqlcipher.SQLiteDatabase, java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    private static Set<String> findUnusedInGroupMembership(SQLiteDatabase sQLiteDatabase, Set<String> set) {
        HashSet hashSet = new HashSet(set);
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT members FROM groups", (String[]) null);
        while (rawQuery != null) {
            try {
                if (!rawQuery.moveToNext()) {
                    break;
                }
                String[] split = DelimiterUtil.split(rawQuery.getString(rawQuery.getColumnIndexOrThrow("members")), ',');
                for (String str : split) {
                    if (hashSet.remove(str)) {
                        Log.i(TAG, "Recipient " + str + " was found in a group membership list.");
                    }
                }
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        return hashSet;
    }
}
