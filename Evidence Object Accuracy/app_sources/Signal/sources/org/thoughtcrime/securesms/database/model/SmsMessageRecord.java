package org.thoughtcrime.securesms.database.model;

import android.content.Context;
import android.text.SpannableString;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class SmsMessageRecord extends MessageRecord {
    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isMms() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isMmsNotification() {
        return false;
    }

    public SmsMessageRecord(long j, String str, Recipient recipient, Recipient recipient2, int i, long j2, long j3, long j4, int i2, long j5, long j6, int i3, Set<IdentityKeyMismatch> set, int i4, long j7, long j8, int i5, boolean z, List<ReactionRecord> list, boolean z2, long j9, long j10) {
        super(j, str, recipient, recipient2, i, j2, j3, j4, j6, i3, i2, j5, set, new HashSet(), i4, j7, j8, i5, z, list, z2, j9, 0, j10);
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord, org.thoughtcrime.securesms.database.model.DisplayRecord
    public long getType() {
        return this.type;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord, org.thoughtcrime.securesms.database.model.DisplayRecord
    public SpannableString getDisplayBody(Context context) {
        if (MmsSmsColumns.Types.isChatSessionRefresh(this.type)) {
            return MessageRecord.emphasisAdded(context.getString(R.string.MessageRecord_chat_session_refreshed));
        }
        if (isCorruptedKeyExchange()) {
            return MessageRecord.emphasisAdded(context.getString(R.string.SmsMessageRecord_received_corrupted_key_exchange_message));
        }
        if (isInvalidVersionKeyExchange()) {
            return MessageRecord.emphasisAdded(context.getString(R.string.SmsMessageRecord_received_key_exchange_message_for_invalid_protocol_version));
        }
        if (MmsSmsColumns.Types.isLegacyType(this.type)) {
            return MessageRecord.emphasisAdded(context.getString(R.string.MessageRecord_message_encrypted_with_a_legacy_protocol_version_that_is_no_longer_supported));
        }
        if (isBundleKeyExchange()) {
            return MessageRecord.emphasisAdded(context.getString(R.string.SmsMessageRecord_received_message_with_new_safety_number_tap_to_process));
        }
        if (isKeyExchange() && isOutgoing()) {
            return new SpannableString("");
        }
        if (isKeyExchange() && !isOutgoing()) {
            return MessageRecord.emphasisAdded(context.getString(R.string.ConversationItem_received_key_exchange_message_tap_to_process));
        }
        if (MmsSmsColumns.Types.isDuplicateMessageType(this.type)) {
            return MessageRecord.emphasisAdded(context.getString(R.string.SmsMessageRecord_duplicate_message));
        }
        if (MmsSmsColumns.Types.isNoRemoteSessionType(this.type)) {
            return MessageRecord.emphasisAdded(context.getString(R.string.MessageDisplayHelper_message_encrypted_for_non_existing_session));
        }
        if (isEndSession() && isOutgoing()) {
            return MessageRecord.emphasisAdded(context.getString(R.string.SmsMessageRecord_secure_session_reset));
        }
        if (isEndSession()) {
            return MessageRecord.emphasisAdded(context.getString(R.string.SmsMessageRecord_secure_session_reset_s, getIndividualRecipient().getDisplayName(context)));
        }
        if (MmsSmsColumns.Types.isUnsupportedMessageType(this.type)) {
            return MessageRecord.emphasisAdded(context.getString(R.string.SmsMessageRecord_this_message_could_not_be_processed_because_it_was_sent_from_a_newer_version));
        }
        if (MmsSmsColumns.Types.isInvalidMessageType(this.type)) {
            return MessageRecord.emphasisAdded(context.getString(R.string.SmsMessageRecord_error_handling_incoming_message));
        }
        return super.getDisplayBody(context);
    }

    public SmsMessageRecord withReactions(List<ReactionRecord> list) {
        return new SmsMessageRecord(getId(), getBody(), getRecipient(), getIndividualRecipient(), getRecipientDeviceId(), getDateSent(), getDateReceived(), getServerTimestamp(), getDeliveryReceiptCount(), getType(), getThreadId(), getDeliveryStatus(), getIdentityKeyMismatches(), getSubscriptionId(), getExpiresIn(), getExpireStarted(), getReadReceiptCount(), isUnidentified(), list, isRemoteDelete(), getNotifiedTimestamp(), getReceiptTimestamp());
    }
}
