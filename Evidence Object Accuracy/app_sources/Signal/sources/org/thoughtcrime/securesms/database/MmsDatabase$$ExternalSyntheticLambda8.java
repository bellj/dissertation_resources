package org.thoughtcrime.securesms.database;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.database.MessageDatabase;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MmsDatabase$$ExternalSyntheticLambda8 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return Long.valueOf(((MessageDatabase.MarkedMessageInfo) obj).getThreadId());
    }
}
