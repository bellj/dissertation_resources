package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaDataSource;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import com.bumptech.glide.Glide;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import j$.util.DesugarArrays;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SetUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.audio.AudioHash;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.ClassicDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.ModernEncryptingPartOutputStream;
import org.thoughtcrime.securesms.database.PartFileProtector;
import org.thoughtcrime.securesms.database.model.databaseprotos.AudioWaveFormData;
import org.thoughtcrime.securesms.mms.MediaStream;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.stickers.StickerLocator;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.FileUtils;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.StorageUtil;
import org.thoughtcrime.securesms.video.EncryptedMediaDataSource;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes.dex */
public class AttachmentDatabase extends Database {
    static final String ATTACHMENT_JSON_ALIAS;
    static final String BORDERLESS;
    static final String CAPTION;
    static final String CDN_NUMBER;
    static final String CONTENT_DISPOSITION;
    static final String CONTENT_LOCATION;
    static final String CONTENT_TYPE;
    public static final String[] CREATE_INDEXS = {"CREATE INDEX IF NOT EXISTS part_mms_id_index ON part (mid);", "CREATE INDEX IF NOT EXISTS pending_push_index ON part (pending_push);", "CREATE INDEX IF NOT EXISTS part_sticker_pack_id_index ON part (sticker_pack_id);", "CREATE INDEX IF NOT EXISTS part_data_hash_index ON part (data_hash);", "CREATE INDEX IF NOT EXISTS part_data_index ON part (_data);"};
    public static final String CREATE_TABLE;
    public static final String DATA;
    static final String DATA_HASH;
    public static final String DATA_RANDOM;
    static final String DIGEST;
    private static final String DIRECTORY;
    static final String DISPLAY_ORDER;
    static final String FAST_PREFLIGHT_ID;
    static final String FILE_NAME;
    static final String HEIGHT;
    public static final String MMS_ID;
    static final String NAME;
    private static final String PART_ID_WHERE;
    private static final String PART_ID_WHERE_NOT;
    public static final long PREUPLOAD_MESSAGE_ID;
    private static final String[] PROJECTION = {"_id", MMS_ID, CONTENT_TYPE, "name", CONTENT_DISPOSITION, CDN_NUMBER, CONTENT_LOCATION, DATA, TRANSFER_STATE, SIZE, FILE_NAME, UNIQUE_ID, DIGEST, FAST_PREFLIGHT_ID, "voice_note", BORDERLESS, VIDEO_GIF, "quote", DATA_RANDOM, WIDTH, HEIGHT, "caption", STICKER_PACK_ID, STICKER_PACK_KEY, STICKER_ID, STICKER_EMOJI, DATA_HASH, VISUAL_HASH, TRANSFORM_PROPERTIES, TRANSFER_FILE, DISPLAY_ORDER, UPLOAD_TIMESTAMP};
    static final String QUOTE;
    public static final String ROW_ID;
    public static final String SIZE;
    static final String STICKER_EMOJI;
    static final String STICKER_ID;
    public static final String STICKER_PACK_ID;
    public static final String STICKER_PACK_KEY;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(AttachmentDatabase.class);
    private static final String TRANSFER_FILE;
    public static final int TRANSFER_PROGRESS_DONE;
    public static final int TRANSFER_PROGRESS_FAILED;
    public static final int TRANSFER_PROGRESS_PENDING;
    public static final int TRANSFER_PROGRESS_STARTED;
    static final String TRANSFER_STATE;
    static final String TRANSFORM_PROPERTIES;
    public static final String UNIQUE_ID;
    static final String UPLOAD_TIMESTAMP;
    static final String VIDEO_GIF;
    static final String VISUAL_HASH;
    static final String VOICE_NOTE;
    static final String WIDTH;
    private final AttachmentSecret attachmentSecret;

    public AttachmentDatabase(Context context, SignalDatabase signalDatabase, AttachmentSecret attachmentSecret) {
        super(context, signalDatabase);
        this.attachmentSecret = attachmentSecret;
    }

    public InputStream getAttachmentStream(AttachmentId attachmentId, long j) throws IOException {
        InputStream dataStream = getDataStream(attachmentId, DATA, j);
        if (dataStream != null) {
            return dataStream;
        }
        throw new IOException("No stream for: " + attachmentId);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0022, code lost:
        if (r12.moveToFirst() != false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean containsStickerPackId(java.lang.String r12) {
        /*
            r11 = this;
            r0 = 1
            java.lang.String[] r5 = new java.lang.String[r0]
            r10 = 0
            r5[r10] = r12
            org.thoughtcrime.securesms.database.SignalDatabase r12 = r11.databaseHelper
            org.thoughtcrime.securesms.database.SQLiteDatabase r1 = r12.getSignalReadableDatabase()
            java.lang.String r2 = "part"
            r3 = 0
            java.lang.String r4 = "sticker_pack_id = ?"
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "1"
            android.database.Cursor r12 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)
            if (r12 == 0) goto L_0x002f
            boolean r1 = r12.moveToFirst()     // Catch: all -> 0x0025
            if (r1 == 0) goto L_0x002f
            goto L_0x0030
        L_0x0025:
            r0 = move-exception
            r12.close()     // Catch: all -> 0x002a
            goto L_0x002e
        L_0x002a:
            r12 = move-exception
            r0.addSuppressed(r12)
        L_0x002e:
            throw r0
        L_0x002f:
            r0 = 0
        L_0x0030:
            if (r12 == 0) goto L_0x0035
            r12.close()
        L_0x0035:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.AttachmentDatabase.containsStickerPackId(java.lang.String):boolean");
    }

    public void setTransferProgressFailed(AttachmentId attachmentId, long j) throws MmsException {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TRANSFER_STATE, (Integer) 3);
        signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId.toStrings());
        notifyConversationListeners(SignalDatabase.mms().getThreadIdForMessage(j));
    }

    public DatabaseAttachment getAttachment(AttachmentId attachmentId) {
        Cursor cursor;
        Throwable th;
        List<DatabaseAttachment> attachments;
        try {
            cursor = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, PROJECTION, PART_ID_WHERE, attachmentId.toStrings(), null, null, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst() && (attachments = getAttachments(cursor)) != null && attachments.size() > 0) {
                        DatabaseAttachment databaseAttachment = attachments.get(0);
                        cursor.close();
                        return databaseAttachment;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public List<DatabaseAttachment> getAttachmentsForMessage(long j) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        LinkedList linkedList = new LinkedList();
        Cursor cursor = null;
        try {
            String[] strArr = PROJECTION;
            cursor = signalReadableDatabase.query(TABLE_NAME, strArr, "mid = ?", new String[]{j + ""}, null, null, "unique_id ASC, _id ASC");
            while (cursor != null) {
                if (!cursor.moveToNext()) {
                    break;
                }
                linkedList.addAll(getAttachments(cursor));
            }
            return linkedList;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public Map<Long, List<DatabaseAttachment>> getAttachmentsForMessages(Collection<Long> collection) {
        if (collection.isEmpty()) {
            return Collections.emptyMap();
        }
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        SqlUtil.Query buildSingleCollectionQuery = SqlUtil.buildSingleCollectionQuery(MMS_ID, collection);
        HashMap hashMap = new HashMap();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, PROJECTION, buildSingleCollectionQuery.getWhere(), buildSingleCollectionQuery.getWhereArgs(), null, null, "unique_id ASC, _id ASC");
        while (query.moveToNext()) {
            try {
                DatabaseAttachment attachment = getAttachment(query);
                List list = (List) hashMap.get(Long.valueOf(attachment.getMmsId()));
                if (list == null) {
                    list = new LinkedList();
                    hashMap.put(Long.valueOf(attachment.getMmsId()), list);
                }
                list.add(attachment);
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return hashMap;
    }

    public boolean hasAttachment(AttachmentId attachmentId) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{"_id", UNIQUE_ID}, PART_ID_WHERE, attachmentId.toStrings(), null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    query.close();
                    return true;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query == null) {
            return false;
        }
        query.close();
        return false;
    }

    public List<DatabaseAttachment> getPendingAttachments() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        LinkedList linkedList = new LinkedList();
        Cursor cursor = null;
        try {
            cursor = signalReadableDatabase.query(TABLE_NAME, PROJECTION, "pending_push = ?", new String[]{String.valueOf(1)}, null, null, null);
            while (cursor != null) {
                if (!cursor.moveToNext()) {
                    break;
                }
                linkedList.addAll(getAttachments(cursor));
            }
            return linkedList;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public boolean deleteAttachmentsForMessage(long j) {
        Throwable th;
        String str = "_id";
        String str2 = CONTENT_TYPE;
        String str3 = TAG;
        Log.d(str3, "[deleteAttachmentsForMessage] mmsId: " + j);
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        Cursor cursor = null;
        try {
            String[] strArr = {DATA, str2, str, UNIQUE_ID};
            Cursor query = signalWritableDatabase.query(TABLE_NAME, strArr, "mid = ?", new String[]{j + ""}, null, null, null);
            while (query != null) {
                try {
                    if (!query.moveToNext()) {
                        break;
                    }
                    deleteAttachmentOnDisk(CursorUtil.requireString(query, DATA), CursorUtil.requireString(query, str2), new AttachmentId(CursorUtil.requireLong(query, str), CursorUtil.requireLong(query, UNIQUE_ID)));
                    str = str;
                    str2 = str2;
                } catch (Throwable th2) {
                    th = th2;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
            int delete = signalWritableDatabase.delete(TABLE_NAME, "mid = ?", new String[]{j + ""});
            notifyAttachmentListeners();
            return delete > 0;
        } catch (Throwable th3) {
            th = th3;
        }
    }

    public int deleteAbandonedPreuploadedAttachments() {
        int i = 0;
        Cursor query = this.databaseHelper.getSignalWritableDatabase().query(TABLE_NAME, null, "mid = ?", new String[]{String.valueOf((long) PREUPLOAD_MESSAGE_ID)}, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                deleteAttachment(new AttachmentId(query.getLong(query.getColumnIndexOrThrow("_id")), query.getLong(query.getColumnIndexOrThrow(UNIQUE_ID))));
                i++;
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return i;
    }

    public void deleteAttachmentFilesForViewOnceMessage(long j) {
        Throwable th;
        Log.d(TAG, "[deleteAttachmentFilesForViewOnceMessage] mmsId: " + j);
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        Cursor cursor = null;
        try {
            try {
                cursor = signalWritableDatabase.query(TABLE_NAME, new String[]{DATA, CONTENT_TYPE, "_id", UNIQUE_ID}, "mid = ?", new String[]{j + ""}, null, null, null);
                while (cursor != null) {
                    try {
                        if (!cursor.moveToNext()) {
                            break;
                        }
                        deleteAttachmentOnDisk(CursorUtil.requireString(cursor, DATA), CursorUtil.requireString(cursor, CONTENT_TYPE), new AttachmentId(CursorUtil.requireLong(cursor, "_id"), CursorUtil.requireLong(cursor, UNIQUE_ID)));
                    } catch (Throwable th2) {
                        th = th2;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put(DATA, (String) null);
                contentValues.put(DATA_RANDOM, (byte[]) null);
                contentValues.put(DATA_HASH, (String) null);
                contentValues.put(FILE_NAME, (String) null);
                contentValues.put("caption", (String) null);
                contentValues.put(SIZE, (Integer) 0);
                contentValues.put(WIDTH, (Integer) 0);
                contentValues.put(HEIGHT, (Integer) 0);
                contentValues.put(TRANSFER_STATE, (Integer) 0);
                contentValues.put(VISUAL_HASH, (String) null);
                contentValues.put(CONTENT_TYPE, MediaUtil.VIEW_ONCE);
                signalWritableDatabase.update(TABLE_NAME, contentValues, "mid = ?", new String[]{j + ""});
                notifyAttachmentListeners();
                long threadIdForMessage = SignalDatabase.mms().getThreadIdForMessage(j);
                if (threadIdForMessage > 0) {
                    notifyConversationListeners(threadIdForMessage);
                }
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        } catch (Throwable th4) {
            th = th4;
        }
    }

    public void deleteAttachment(AttachmentId attachmentId) {
        String str = TAG;
        Log.d(str, "[deleteAttachment] attachmentId: " + attachmentId);
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        Cursor query = signalWritableDatabase.query(TABLE_NAME, new String[]{DATA, CONTENT_TYPE}, PART_ID_WHERE, attachmentId.toStrings(), null, null, null);
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    String requireString = CursorUtil.requireString(query, DATA);
                    String requireString2 = CursorUtil.requireString(query, CONTENT_TYPE);
                    signalWritableDatabase.delete(TABLE_NAME, PART_ID_WHERE, attachmentId.toStrings());
                    deleteAttachmentOnDisk(requireString, requireString2, attachmentId);
                    notifyAttachmentListeners();
                    query.close();
                    return;
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        Log.w(str, "Tried to delete an attachment, but it didn't exist.");
        if (query != null) {
            query.close();
        }
    }

    public void trimAllAbandonedAttachments() {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        int delete = signalWritableDatabase.delete(TABLE_NAME, "mid != -8675309 AND mid NOT IN (SELECT _id FROM mms)", (String[]) null);
        if (delete > 0) {
            String str = TAG;
            Log.i(str, "Trimmed " + delete + " abandoned attachments.");
        }
    }

    public int deleteAbandonedAttachmentFiles() {
        File[] listFiles = this.context.getDir(DIRECTORY, 0).listFiles();
        if (listFiles == null) {
            return 0;
        }
        Set set = (Set) DesugarArrays.stream(listFiles).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.AttachmentDatabase$$ExternalSyntheticLambda2
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return AttachmentDatabase.lambda$deleteAbandonedAttachmentFiles$0((File) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.database.AttachmentDatabase$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((File) obj).getAbsolutePath();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toSet());
        HashSet hashSet = new HashSet();
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(true, TABLE_NAME, new String[]{DATA}, null, null, null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                hashSet.add(CursorUtil.requireString(query, DATA));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        hashSet.addAll(SignalDatabase.stickers().getAllStickerFiles());
        Set<String> difference = SetUtil.difference(set, hashSet);
        for (String str : difference) {
            new File(str).delete();
        }
        return difference.size();
    }

    public static /* synthetic */ boolean lambda$deleteAbandonedAttachmentFiles$0(File file) {
        return !PartFileProtector.isProtected(file);
    }

    public void deleteAllAttachments() {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, (String) null, (String[]) null);
        FileUtils.deleteDirectoryContents(this.context.getDir(DIRECTORY, 0));
        notifyAttachmentListeners();
    }

    /* JADX INFO: finally extract failed */
    private void deleteAttachmentOnDisk(String str, String str2, AttachmentId attachmentId) {
        DataUsageResult attachmentFileUsages = getAttachmentFileUsages(str, attachmentId);
        if (attachmentFileUsages.hasStrongReference()) {
            String str3 = TAG;
            Log.i(str3, "[deleteAttachmentOnDisk] Attachment in use. Skipping deletion. " + str + " " + attachmentId);
            return;
        }
        String str4 = TAG;
        Log.i(str4, "[deleteAttachmentOnDisk] No other strong uses of this attachment. Safe to delete. " + str + " " + attachmentId);
        if (!TextUtils.isEmpty(str)) {
            if (new File(str).delete()) {
                Log.i(str4, "[deleteAttachmentOnDisk] Deleted attachment file. " + str + " " + attachmentId);
                List<AttachmentId> removableWeakReferences = attachmentFileUsages.getRemovableWeakReferences();
                if (removableWeakReferences.size() > 0) {
                    Log.i(str4, String.format(Locale.US, "[deleteAttachmentOnDisk] Deleting %d weak references for %s", Integer.valueOf(removableWeakReferences.size()), str));
                    SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
                    signalWritableDatabase.beginTransaction();
                    try {
                        int i = 0;
                        for (AttachmentId attachmentId2 : removableWeakReferences) {
                            Log.i(TAG, String.format("[deleteAttachmentOnDisk] Clearing weak reference for %s %s", str, attachmentId2));
                            ContentValues contentValues = new ContentValues();
                            contentValues.putNull(DATA);
                            contentValues.putNull(DATA_RANDOM);
                            contentValues.putNull(DATA_HASH);
                            i += signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId2.toStrings());
                        }
                        signalWritableDatabase.setTransactionSuccessful();
                        signalWritableDatabase.endTransaction();
                        String format = String.format(Locale.US, "[deleteAttachmentOnDisk] Cleared %d/%d weak references for %s", Integer.valueOf(i), Integer.valueOf(removableWeakReferences.size()), str);
                        if (i != removableWeakReferences.size()) {
                            Log.w(TAG, format);
                        } else {
                            Log.i(TAG, format);
                        }
                    } catch (Throwable th) {
                        signalWritableDatabase.endTransaction();
                        throw th;
                    }
                }
            } else {
                Log.w(str4, "[deleteAttachmentOnDisk] Failed to delete attachment. " + str + " " + attachmentId);
            }
        }
        if (MediaUtil.isImageType(str2) || MediaUtil.isVideoType(str2)) {
            Glide.get(this.context).clearDiskCache();
        }
    }

    private DataUsageResult getAttachmentFileUsages(String str, AttachmentId attachmentId) {
        if (str == null) {
            return DataUsageResult.NOT_IN_USE;
        }
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String[] strArr = {str, Long.toString(attachmentId.getUniqueId()), Long.toString(attachmentId.getRowId())};
        LinkedList linkedList = new LinkedList();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, new String[]{"_id", UNIQUE_ID, "quote"}, "_data = ? AND unique_id != ? AND _id != ?", strArr, null, null, null, null);
        while (query.moveToNext()) {
            try {
                if (query.getInt(query.getColumnIndexOrThrow("quote")) == 1) {
                    linkedList.add(new AttachmentId(query.getLong(query.getColumnIndexOrThrow("_id")), query.getLong(query.getColumnIndexOrThrow(UNIQUE_ID))));
                } else {
                    DataUsageResult dataUsageResult = DataUsageResult.IN_USE;
                    query.close();
                    return dataUsageResult;
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return new DataUsageResult(linkedList);
    }

    public void insertAttachmentsForPlaceholder(long j, AttachmentId attachmentId, InputStream inputStream) throws MmsException {
        DatabaseAttachment attachment = getAttachment(attachmentId);
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues();
        DataInfo attachmentDataFileInfo = getAttachmentDataFileInfo(attachmentId, DATA);
        DataInfo attachmentData = setAttachmentData(inputStream, attachmentId);
        File transferFile = getTransferFile(this.databaseHelper.getSignalReadableDatabase(), attachmentId);
        if (attachmentDataFileInfo != null) {
            updateAttachmentDataHash(signalWritableDatabase, attachmentDataFileInfo.hash, attachmentData);
        }
        contentValues.put(DATA, attachmentData.file.getAbsolutePath());
        contentValues.put(SIZE, Long.valueOf(attachmentData.length));
        contentValues.put(DATA_RANDOM, attachmentData.random);
        contentValues.put(DATA_HASH, attachmentData.hash);
        String visualHashStringOrNull = getVisualHashStringOrNull(attachment);
        if (visualHashStringOrNull != null) {
            contentValues.put(VISUAL_HASH, visualHashStringOrNull);
        }
        contentValues.put(TRANSFER_STATE, (Integer) 0);
        contentValues.put(TRANSFER_FILE, (String) null);
        contentValues.put(TRANSFORM_PROPERTIES, TransformProperties.forSkipTransform().serialize());
        if (signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId.toStrings()) == 0) {
            attachmentData.file.delete();
        } else {
            long threadIdForMessage = SignalDatabase.mms().getThreadIdForMessage(j);
            SignalDatabase.threads().updateSnippetUriSilently(threadIdForMessage, PartAuthority.getAttachmentDataUri(attachmentId));
            notifyConversationListeners(threadIdForMessage);
            notifyConversationListListeners();
            notifyAttachmentListeners();
        }
        if (transferFile != null) {
            transferFile.delete();
        }
    }

    private static String getVisualHashStringOrNull(Attachment attachment) {
        if (attachment == null) {
            return null;
        }
        if (attachment.getBlurHash() != null) {
            return attachment.getBlurHash().getHash();
        }
        if (attachment.getAudioHash() != null) {
            return attachment.getAudioHash().getHash();
        }
        return null;
    }

    public void copyAttachmentData(AttachmentId attachmentId, AttachmentId attachmentId2) throws MmsException {
        DatabaseAttachment attachment = getAttachment(attachmentId);
        if (attachment != null) {
            SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
            DataInfo attachmentDataFileInfo = getAttachmentDataFileInfo(attachmentId, DATA);
            if (attachmentDataFileInfo != null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DATA, attachmentDataFileInfo.file.getAbsolutePath());
                contentValues.put(DATA_HASH, attachmentDataFileInfo.hash);
                contentValues.put(SIZE, Long.valueOf(attachmentDataFileInfo.length));
                contentValues.put(DATA_RANDOM, attachmentDataFileInfo.random);
                contentValues.put(TRANSFER_STATE, Integer.valueOf(attachment.getTransferState()));
                contentValues.put(CDN_NUMBER, Integer.valueOf(attachment.getCdnNumber()));
                contentValues.put(CONTENT_LOCATION, attachment.getLocation());
                contentValues.put(DIGEST, attachment.getDigest());
                contentValues.put(CONTENT_DISPOSITION, attachment.getKey());
                contentValues.put("name", attachment.getRelay());
                contentValues.put(SIZE, Long.valueOf(attachment.getSize()));
                contentValues.put(FAST_PREFLIGHT_ID, attachment.getFastPreflightId());
                contentValues.put(WIDTH, Integer.valueOf(attachment.getWidth()));
                contentValues.put(HEIGHT, Integer.valueOf(attachment.getHeight()));
                contentValues.put(CONTENT_TYPE, attachment.getContentType());
                contentValues.put(VISUAL_HASH, getVisualHashStringOrNull(attachment));
                signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId2.toStrings());
                return;
            }
            throw new MmsException("No attachment data found for source!");
        }
        throw new MmsException("Cannot find attachment for source!");
    }

    public void updateAttachmentCaption(AttachmentId attachmentId, String str) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("caption", str);
        this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId.toStrings());
    }

    public void updateDisplayOrder(Map<AttachmentId, Integer> map) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            for (Map.Entry<AttachmentId, Integer> entry : map.entrySet()) {
                ContentValues contentValues = new ContentValues(1);
                contentValues.put(DISPLAY_ORDER, entry.getValue());
                this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, PART_ID_WHERE, entry.getKey().toStrings());
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public void updateAttachmentAfterUpload(AttachmentId attachmentId, Attachment attachment, long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        DataInfo attachmentDataFileInfo = getAttachmentDataFileInfo(attachmentId, DATA);
        ContentValues contentValues = new ContentValues();
        contentValues.put(TRANSFER_STATE, (Integer) 0);
        contentValues.put(CDN_NUMBER, Integer.valueOf(attachment.getCdnNumber()));
        contentValues.put(CONTENT_LOCATION, attachment.getLocation());
        contentValues.put(DIGEST, attachment.getDigest());
        contentValues.put(CONTENT_DISPOSITION, attachment.getKey());
        contentValues.put("name", attachment.getRelay());
        contentValues.put(SIZE, Long.valueOf(attachment.getSize()));
        contentValues.put(FAST_PREFLIGHT_ID, attachment.getFastPreflightId());
        contentValues.put(VISUAL_HASH, getVisualHashStringOrNull(attachment));
        contentValues.put(UPLOAD_TIMESTAMP, Long.valueOf(j));
        if (attachmentDataFileInfo == null || attachmentDataFileInfo.hash == null) {
            signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId.toStrings());
        } else {
            updateAttachmentAndMatchingHashes(signalWritableDatabase, attachmentId, attachmentDataFileInfo.hash, contentValues);
        }
    }

    public DatabaseAttachment insertAttachmentForPreUpload(Attachment attachment) throws MmsException {
        Map<Attachment, AttachmentId> insertAttachmentsForMessage = insertAttachmentsForMessage(PREUPLOAD_MESSAGE_ID, Collections.singletonList(attachment), Collections.emptyList());
        if (!insertAttachmentsForMessage.values().isEmpty()) {
            DatabaseAttachment attachment2 = getAttachment(insertAttachmentsForMessage.values().iterator().next());
            if (attachment2 != null) {
                return attachment2;
            }
            throw new MmsException("Failed to retrieve attachment we just inserted!");
        }
        throw new MmsException("Bad attachment result!");
    }

    public void updateMessageId(Collection<AttachmentId> collection, long j, boolean z) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues(1);
            contentValues.put(MMS_ID, Long.valueOf(j));
            if (!z) {
                contentValues.putNull("caption");
            }
            for (AttachmentId attachmentId : collection) {
                signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId.toStrings());
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public Map<Attachment, AttachmentId> insertAttachmentsForMessage(long j, List<Attachment> list, List<Attachment> list2) throws MmsException {
        String str = TAG;
        Log.d(str, "insertParts(" + list.size() + ")");
        HashMap hashMap = new HashMap();
        for (Attachment attachment : list) {
            AttachmentId insertAttachment = insertAttachment(j, attachment, attachment.isQuote());
            hashMap.put(attachment, insertAttachment);
            String str2 = TAG;
            Log.i(str2, "Inserted attachment at ID: " + insertAttachment);
        }
        for (Attachment attachment2 : list2) {
            AttachmentId insertAttachment2 = insertAttachment(j, attachment2, true);
            hashMap.put(attachment2, insertAttachment2);
            String str3 = TAG;
            Log.i(str3, "Inserted quoted attachment at ID: " + insertAttachment2);
        }
        return hashMap;
    }

    public void updateAttachmentData(DatabaseAttachment databaseAttachment, MediaStream mediaStream, boolean z) throws MmsException, IOException {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        DataInfo attachmentDataFileInfo = getAttachmentDataFileInfo(databaseAttachment.getAttachmentId(), DATA);
        if (attachmentDataFileInfo != null) {
            File file = attachmentDataFileInfo.file;
            boolean z2 = z || attachmentDataFileInfo.hash == null;
            if (z2 && fileReferencedByMoreThanOneAttachment(file)) {
                Log.i(TAG, "Creating a new file as this one is used by more than one attachment");
                file = newFile();
            }
            DataInfo attachmentData = setAttachmentData(file, mediaStream.getStream(), databaseAttachment.getAttachmentId());
            ContentValues contentValues = new ContentValues();
            contentValues.put(SIZE, Long.valueOf(attachmentData.length));
            contentValues.put(CONTENT_TYPE, mediaStream.getMimeType());
            contentValues.put(WIDTH, Integer.valueOf(mediaStream.getWidth()));
            contentValues.put(HEIGHT, Integer.valueOf(mediaStream.getHeight()));
            contentValues.put(DATA, attachmentData.file.getAbsolutePath());
            contentValues.put(DATA_RANDOM, attachmentData.random);
            contentValues.put(DATA_HASH, attachmentData.hash);
            int updateAttachmentAndMatchingHashes = updateAttachmentAndMatchingHashes(signalWritableDatabase, databaseAttachment.getAttachmentId(), z2 ? attachmentData.hash : attachmentDataFileInfo.hash, contentValues);
            String str = TAG;
            Log.i(str, "[updateAttachmentData] Updated " + updateAttachmentAndMatchingHashes + " rows.");
            return;
        }
        throw new MmsException("No attachment data found!");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002b, code lost:
        if (r12.moveToNext() != false) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean fileReferencedByMoreThanOneAttachment(java.io.File r12) {
        /*
            r11 = this;
            org.thoughtcrime.securesms.database.SignalDatabase r0 = r11.databaseHelper
            org.thoughtcrime.securesms.database.SQLiteDatabase r1 = r0.getSignalReadableDatabase()
            r0 = 1
            java.lang.String[] r5 = new java.lang.String[r0]
            java.lang.String r12 = r12.getAbsolutePath()
            r10 = 0
            r5[r10] = r12
            java.lang.String r2 = "part"
            r3 = 0
            java.lang.String r4 = "_data = ?"
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "2"
            android.database.Cursor r12 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)
            if (r12 == 0) goto L_0x0038
            boolean r1 = r12.moveToFirst()     // Catch: all -> 0x002e
            if (r1 == 0) goto L_0x0038
            boolean r1 = r12.moveToNext()     // Catch: all -> 0x002e
            if (r1 == 0) goto L_0x0038
            goto L_0x0039
        L_0x002e:
            r0 = move-exception
            r12.close()     // Catch: all -> 0x0033
            goto L_0x0037
        L_0x0033:
            r12 = move-exception
            r0.addSuppressed(r12)
        L_0x0037:
            throw r0
        L_0x0038:
            r0 = 0
        L_0x0039:
            if (r12 == 0) goto L_0x003e
            r12.close()
        L_0x003e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.AttachmentDatabase.fileReferencedByMoreThanOneAttachment(java.io.File):boolean");
    }

    public void markAttachmentAsTransformed(AttachmentId attachmentId) {
        getWritableDatabase().beginTransaction();
        try {
            try {
                updateAttachmentTransformProperties(attachmentId, getTransformProperties(attachmentId).withSkipTransform());
                getWritableDatabase().setTransactionSuccessful();
            } catch (Exception e) {
                Log.w(TAG, "Could not mark attachment as transformed.", e);
            }
        } finally {
            getWritableDatabase().endTransaction();
        }
    }

    public TransformProperties getTransformProperties(AttachmentId attachmentId) {
        Cursor query = getWritableDatabase().query(TABLE_NAME, SqlUtil.buildArgs(TRANSFORM_PROPERTIES), PART_ID_WHERE, attachmentId.toStrings(), null, null, null, null);
        try {
            if (query.moveToFirst()) {
                TransformProperties parse = TransformProperties.parse(CursorUtil.requireString(query, TRANSFORM_PROPERTIES));
                query.close();
                return parse;
            }
            throw new AssertionError("No such attachment.");
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private void updateAttachmentTransformProperties(AttachmentId attachmentId, TransformProperties transformProperties) {
        DataInfo attachmentDataFileInfo = getAttachmentDataFileInfo(attachmentId, DATA);
        if (attachmentDataFileInfo == null) {
            Log.w(TAG, "[updateAttachmentTransformProperties] No data info found!");
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(TRANSFORM_PROPERTIES, transformProperties.serialize());
        int updateAttachmentAndMatchingHashes = updateAttachmentAndMatchingHashes(this.databaseHelper.getSignalWritableDatabase(), attachmentId, attachmentDataFileInfo.hash, contentValues);
        String str = TAG;
        Log.i(str, "[updateAttachmentTransformProperties] Updated " + updateAttachmentAndMatchingHashes + " rows.");
    }

    public File getOrCreateTransferFile(AttachmentId attachmentId) throws IOException {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        File transferFile = getTransferFile(signalWritableDatabase, attachmentId);
        if (transferFile != null) {
            return transferFile;
        }
        File newTransferFile = newTransferFile();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TRANSFER_FILE, newTransferFile.getAbsolutePath());
        signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId.toStrings());
        return newTransferFile;
    }

    private static File getTransferFile(SQLiteDatabase sQLiteDatabase, AttachmentId attachmentId) {
        String string;
        Cursor query = sQLiteDatabase.query(TABLE_NAME, new String[]{TRANSFER_FILE}, PART_ID_WHERE, attachmentId.toStrings(), null, null, SubscriptionLevels.BOOST_LEVEL);
        if (query != null) {
            try {
                if (query.moveToFirst() && (string = query.getString(query.getColumnIndexOrThrow(TRANSFER_FILE))) != null) {
                    File file = new File(string);
                    query.close();
                    return file;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query == null) {
            return null;
        }
        query.close();
        return null;
    }

    private static int updateAttachmentAndMatchingHashes(SQLiteDatabase sQLiteDatabase, AttachmentId attachmentId, String str, ContentValues contentValues) {
        return sQLiteDatabase.update(TABLE_NAME, contentValues, "(_id = ? AND unique_id = ?) OR (data_hash NOT NULL AND data_hash = ?)", new String[]{String.valueOf(attachmentId.getRowId()), String.valueOf(attachmentId.getUniqueId()), String.valueOf(str)});
    }

    private static void updateAttachmentDataHash(SQLiteDatabase sQLiteDatabase, String str, DataInfo dataInfo) {
        if (str != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DATA, dataInfo.file.getAbsolutePath());
            contentValues.put(DATA_RANDOM, dataInfo.random);
            contentValues.put(DATA_HASH, dataInfo.hash);
            sQLiteDatabase.update(TABLE_NAME, contentValues, "data_hash = ?", new String[]{str});
        }
    }

    public void updateAttachmentFileName(AttachmentId attachmentId, String str) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(FILE_NAME, StorageUtil.getCleanFileName(str));
        signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId.toStrings());
    }

    public void markAttachmentUploaded(long j, Attachment attachment) {
        ContentValues contentValues = new ContentValues(1);
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        contentValues.put(TRANSFER_STATE, (Integer) 0);
        signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, ((DatabaseAttachment) attachment).getAttachmentId().toStrings());
        notifyConversationListeners(SignalDatabase.mms().getThreadIdForMessage(j));
    }

    public void setTransferState(long j, Attachment attachment, int i) {
        if (attachment instanceof DatabaseAttachment) {
            setTransferState(j, ((DatabaseAttachment) attachment).getAttachmentId(), i);
            return;
        }
        throw new AssertionError("Attempt to update attachment that doesn't belong to DB!");
    }

    public void setTransferState(long j, AttachmentId attachmentId, int i) {
        ContentValues contentValues = new ContentValues(1);
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        contentValues.put(TRANSFER_STATE, Integer.valueOf(i));
        signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId.toStrings());
        notifyConversationListeners(SignalDatabase.mms().getThreadIdForMessage(j));
    }

    public Cursor getUnavailableStickerPacks() {
        return this.databaseHelper.getSignalReadableDatabase().rawQuery("SELECT DISTINCT sticker_pack_id, sticker_pack_key FROM part WHERE sticker_pack_id NOT NULL AND sticker_pack_key NOT NULL AND sticker_pack_id NOT IN (SELECT DISTINCT pack_id FROM sticker)", (String[]) null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasStickerAttachments() {
        /*
            r10 = this;
            org.thoughtcrime.securesms.database.SignalDatabase r0 = r10.databaseHelper
            org.thoughtcrime.securesms.database.SQLiteDatabase r1 = r0.getSignalReadableDatabase()
            java.lang.String r2 = "part"
            r3 = 0
            java.lang.String r4 = "sticker_pack_id NOT NULL"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "1"
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)
            if (r0 == 0) goto L_0x002b
            boolean r1 = r0.moveToFirst()     // Catch: all -> 0x0021
            if (r1 == 0) goto L_0x002b
            r1 = 1
            goto L_0x002c
        L_0x0021:
            r1 = move-exception
            r0.close()     // Catch: all -> 0x0026
            goto L_0x002a
        L_0x0026:
            r0 = move-exception
            r1.addSuppressed(r0)
        L_0x002a:
            throw r1
        L_0x002b:
            r1 = 0
        L_0x002c:
            if (r0 == 0) goto L_0x0031
            r0.close()
        L_0x0031:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.AttachmentDatabase.hasStickerAttachments():boolean");
    }

    protected InputStream getDataStream(AttachmentId attachmentId, String str, long j) {
        DataInfo attachmentDataFileInfo = getAttachmentDataFileInfo(attachmentId, str);
        if (attachmentDataFileInfo == null) {
            return null;
        }
        try {
            if (attachmentDataFileInfo.random != null && attachmentDataFileInfo.random.length == 32) {
                return ModernDecryptingPartInputStream.createFor(this.attachmentSecret, attachmentDataFileInfo.random, attachmentDataFileInfo.file, j);
            }
            InputStream createFor = ClassicDecryptingPartInputStream.createFor(this.attachmentSecret, attachmentDataFileInfo.file);
            long skip = createFor.skip(j);
            if (skip == j) {
                return createFor;
            }
            String str2 = TAG;
            Log.w(str2, "Skip failed: " + skip + " vs " + j);
            return null;
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    DataInfo getAttachmentDataFileInfo(AttachmentId attachmentId, String str) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{str, SIZE, DATA_RANDOM, DATA_HASH}, PART_ID_WHERE, attachmentId.toStrings(), null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    if (query.isNull(query.getColumnIndexOrThrow(str))) {
                        query.close();
                        return null;
                    }
                    DataInfo dataInfo = new DataInfo(new File(query.getString(query.getColumnIndexOrThrow(str))), query.getLong(query.getColumnIndexOrThrow(SIZE)), query.getBlob(query.getColumnIndexOrThrow(DATA_RANDOM)), query.getString(query.getColumnIndexOrThrow(DATA_HASH)));
                    query.close();
                    return dataInfo;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return null;
    }

    private DataInfo setAttachmentData(Uri uri, AttachmentId attachmentId) throws MmsException {
        try {
            return setAttachmentData(PartAuthority.getAttachmentStream(this.context, uri), attachmentId);
        } catch (IOException e) {
            throw new MmsException(e);
        }
    }

    private DataInfo setAttachmentData(InputStream inputStream, AttachmentId attachmentId) throws MmsException {
        try {
            return setAttachmentData(newFile(), inputStream, attachmentId);
        } catch (IOException e) {
            throw new MmsException(e);
        }
    }

    public File newFile() throws IOException {
        return newFile(this.context);
    }

    private File newTransferFile() throws IOException {
        return PartFileProtector.protect(new PartFileProtector.CreateFile(this.context.getDir(DIRECTORY, 0)) { // from class: org.thoughtcrime.securesms.database.AttachmentDatabase$$ExternalSyntheticLambda0
            public final /* synthetic */ File f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.database.PartFileProtector.CreateFile
            public final File create() {
                return File.createTempFile("transfer", ".mms", this.f$0);
            }
        });
    }

    public static File newFile(Context context) throws IOException {
        return PartFileProtector.protect(new PartFileProtector.CreateFile(context.getDir(DIRECTORY, 0)) { // from class: org.thoughtcrime.securesms.database.AttachmentDatabase$$ExternalSyntheticLambda1
            public final /* synthetic */ File f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.database.PartFileProtector.CreateFile
            public final File create() {
                return File.createTempFile(AttachmentDatabase.TABLE_NAME, ".mms", this.f$0);
            }
        });
    }

    private DataInfo setAttachmentData(File file, InputStream inputStream, AttachmentId attachmentId) throws MmsException {
        try {
            File newFile = newFile();
            DigestInputStream digestInputStream = new DigestInputStream(inputStream, MessageDigest.getInstance("SHA-256"));
            Pair<byte[], OutputStream> createFor = ModernEncryptingPartOutputStream.createFor(this.attachmentSecret, newFile, false);
            long copy = StreamUtil.copy(digestInputStream, (OutputStream) createFor.second);
            String encodeBytes = Base64.encodeBytes(digestInputStream.getMessageDigest().digest());
            if (newFile.renameTo(file)) {
                Optional<DataInfo> findDuplicateDataFileInfo = findDuplicateDataFileInfo(this.databaseHelper.getSignalWritableDatabase(), encodeBytes, attachmentId);
                if (findDuplicateDataFileInfo.isPresent()) {
                    String str = TAG;
                    Log.i(str, "[setAttachmentData] Duplicate data file found! " + findDuplicateDataFileInfo.get().file.getAbsolutePath());
                    if (!file.equals(findDuplicateDataFileInfo.get().file) && file.delete()) {
                        Log.i(str, "[setAttachmentData] Deleted original file. " + file);
                    }
                    return findDuplicateDataFileInfo.get();
                }
                String str2 = TAG;
                Log.i(str2, "[setAttachmentData] No matching attachment data found. " + file.getAbsolutePath());
                return new DataInfo(file, copy, (byte[]) createFor.first, encodeBytes);
            }
            String str3 = TAG;
            Log.w(str3, "Couldn't rename " + newFile.getPath() + " to " + file.getPath());
            newFile.delete();
            throw new IllegalStateException("Couldn't rename " + newFile.getPath() + " to " + file.getPath());
        } catch (IOException | NoSuchAlgorithmException e) {
            throw new MmsException(e);
        }
    }

    private static Optional<DataInfo> findDuplicateDataFileInfo(SQLiteDatabase sQLiteDatabase, String str, AttachmentId attachmentId) {
        Pair<String, String[]> buildSharedFileSelectorArgs = buildSharedFileSelectorArgs(str, attachmentId);
        Cursor query = sQLiteDatabase.query(TABLE_NAME, new String[]{DATA, DATA_RANDOM, SIZE, TRANSFORM_PROPERTIES}, (String) buildSharedFileSelectorArgs.first, (String[]) buildSharedFileSelectorArgs.second, null, null, null, SubscriptionLevels.BOOST_LEVEL);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    if (query.getCount() > 0) {
                        Optional<DataInfo> of = Optional.of(new DataInfo(new File(CursorUtil.requireString(query, DATA)), CursorUtil.requireLong(query, SIZE), CursorUtil.requireBlob(query, DATA_RANDOM), str));
                        query.close();
                        return of;
                    }
                    Optional<DataInfo> empty = Optional.empty();
                    query.close();
                    return empty;
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        Optional<DataInfo> empty2 = Optional.empty();
        if (query != null) {
            query.close();
        }
        return empty2;
    }

    private static Pair<String, String[]> buildSharedFileSelectorArgs(String str, AttachmentId attachmentId) {
        String[] strArr;
        String str2;
        if (attachmentId == null) {
            strArr = new String[]{str};
            str2 = "data_hash = ?";
        } else {
            String[] strArr2 = {Long.toString(attachmentId.getRowId()), Long.toString(attachmentId.getUniqueId()), str};
            str2 = "_id != ? AND unique_id != ? AND data_hash = ?";
            strArr = strArr2;
        }
        return Pair.create(str2, strArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010b A[Catch: JSONException -> 0x01b5, TryCatch #0 {JSONException -> 0x01b5, blocks: (B:3:0x0009, B:5:0x0010, B:7:0x001a, B:9:0x0020, B:10:0x0033, B:12:0x0039, B:14:0x0048, B:18:0x0077, B:20:0x007d, B:25:0x0087, B:29:0x00c9, B:33:0x00d6, B:37:0x00e4, B:41:0x00ff, B:43:0x010b, B:45:0x0134, B:49:0x013e, B:50:0x0146, B:52:0x014c, B:54:0x0159, B:56:0x01a3, B:58:0x01ac), top: B:63:0x0009 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x013e A[Catch: JSONException -> 0x01b5, TRY_ENTER, TryCatch #0 {JSONException -> 0x01b5, blocks: (B:3:0x0009, B:5:0x0010, B:7:0x001a, B:9:0x0020, B:10:0x0033, B:12:0x0039, B:14:0x0048, B:18:0x0077, B:20:0x007d, B:25:0x0087, B:29:0x00c9, B:33:0x00d6, B:37:0x00e4, B:41:0x00ff, B:43:0x010b, B:45:0x0134, B:49:0x013e, B:50:0x0146, B:52:0x014c, B:54:0x0159, B:56:0x01a3, B:58:0x01ac), top: B:63:0x0009 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x014c A[Catch: JSONException -> 0x01b5, TryCatch #0 {JSONException -> 0x01b5, blocks: (B:3:0x0009, B:5:0x0010, B:7:0x001a, B:9:0x0020, B:10:0x0033, B:12:0x0039, B:14:0x0048, B:18:0x0077, B:20:0x007d, B:25:0x0087, B:29:0x00c9, B:33:0x00d6, B:37:0x00e4, B:41:0x00ff, B:43:0x010b, B:45:0x0134, B:49:0x013e, B:50:0x0146, B:52:0x014c, B:54:0x0159, B:56:0x01a3, B:58:0x01ac), top: B:63:0x0009 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0157  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<org.thoughtcrime.securesms.attachments.DatabaseAttachment> getAttachments(android.database.Cursor r42) {
        /*
        // Method dump skipped, instructions count: 446
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.AttachmentDatabase.getAttachments(android.database.Cursor):java.util.List");
    }

    private DatabaseAttachment getAttachment(Cursor cursor) {
        StickerLocator stickerLocator;
        String str;
        int i;
        String string = cursor.getString(cursor.getColumnIndexOrThrow(CONTENT_TYPE));
        AttachmentId attachmentId = new AttachmentId(cursor.getLong(cursor.getColumnIndexOrThrow("_id")), cursor.getLong(cursor.getColumnIndexOrThrow(UNIQUE_ID)));
        long j = cursor.getLong(cursor.getColumnIndexOrThrow(MMS_ID));
        boolean z = !cursor.isNull(cursor.getColumnIndexOrThrow(DATA));
        boolean z2 = MediaUtil.isImageType(string) || MediaUtil.isVideoType(string);
        int i2 = cursor.getInt(cursor.getColumnIndexOrThrow(TRANSFER_STATE));
        long j2 = cursor.getLong(cursor.getColumnIndexOrThrow(SIZE));
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow(FILE_NAME));
        int i3 = cursor.getInt(cursor.getColumnIndexOrThrow(CDN_NUMBER));
        String string3 = cursor.getString(cursor.getColumnIndexOrThrow(CONTENT_LOCATION));
        String string4 = cursor.getString(cursor.getColumnIndexOrThrow(CONTENT_DISPOSITION));
        String string5 = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        byte[] blob = cursor.getBlob(cursor.getColumnIndexOrThrow(DIGEST));
        String string6 = cursor.getString(cursor.getColumnIndexOrThrow(FAST_PREFLIGHT_ID));
        boolean z3 = cursor.getInt(cursor.getColumnIndexOrThrow("voice_note")) == 1;
        boolean z4 = cursor.getInt(cursor.getColumnIndexOrThrow(BORDERLESS)) == 1;
        boolean z5 = cursor.getInt(cursor.getColumnIndexOrThrow(VIDEO_GIF)) == 1;
        int i4 = cursor.getInt(cursor.getColumnIndexOrThrow(WIDTH));
        int i5 = cursor.getInt(cursor.getColumnIndexOrThrow(HEIGHT));
        boolean z6 = cursor.getInt(cursor.getColumnIndexOrThrow("quote")) == 1;
        String string7 = cursor.getString(cursor.getColumnIndexOrThrow("caption"));
        if (cursor.getInt(cursor.getColumnIndexOrThrow(STICKER_ID)) >= 0) {
            i = i3;
            str = string2;
            stickerLocator = new StickerLocator(CursorUtil.requireString(cursor, STICKER_PACK_ID), CursorUtil.requireString(cursor, STICKER_PACK_KEY), CursorUtil.requireInt(cursor, STICKER_ID), CursorUtil.requireString(cursor, STICKER_EMOJI));
        } else {
            str = string2;
            i = i3;
            stickerLocator = null;
        }
        return new DatabaseAttachment(attachmentId, j, z, z2, string, i2, j2, str, i, string3, string4, string5, blob, string6, z3, z4, z5, i4, i5, z6, string7, stickerLocator, MediaUtil.isAudioType(string) ? null : BlurHash.parseOrNull(cursor.getString(cursor.getColumnIndexOrThrow(VISUAL_HASH))), MediaUtil.isAudioType(string) ? AudioHash.parseOrNull(cursor.getString(cursor.getColumnIndexOrThrow(VISUAL_HASH))) : null, TransformProperties.parse(cursor.getString(cursor.getColumnIndexOrThrow(TRANSFORM_PROPERTIES))), cursor.getInt(cursor.getColumnIndexOrThrow(DISPLAY_ORDER)), cursor.getLong(cursor.getColumnIndexOrThrow(UPLOAD_TIMESTAMP)));
    }

    private AttachmentId insertAttachment(long j, Attachment attachment, boolean z) throws MmsException {
        DataInfo dataInfo;
        Attachment attachment2;
        String str = TAG;
        Log.d(str, "Inserting attachment for mms id: " + j);
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (attachment.getUri() != null) {
                dataInfo = setAttachmentData(attachment.getUri(), (AttachmentId) null);
                Log.d(str, "Wrote part to file: " + dataInfo.file.getAbsolutePath());
            } else {
                dataInfo = null;
            }
            if (dataInfo == null || dataInfo.hash == null || (attachment2 = findTemplateAttachment(dataInfo.hash)) == null) {
                attachment2 = attachment;
            } else {
                Log.i(str, "Found a duplicate attachment upon insertion. Using it as a template.");
            }
            boolean z2 = true;
            boolean z3 = attachment2.getUploadTimestamp() > attachment.getUploadTimestamp() && attachment2.getTransferState() == 0 && attachment2.getTransformProperties().shouldSkipTransform() && attachment2.getDigest() != null && !attachment.getTransformProperties().isVideoEdited() && attachment2.getTransformProperties().sentMediaQuality == attachment.getTransformProperties().getSentMediaQuality();
            ContentValues contentValues = new ContentValues();
            contentValues.put(MMS_ID, Long.valueOf(j));
            contentValues.put(CONTENT_TYPE, attachment2.getContentType());
            contentValues.put(TRANSFER_STATE, Integer.valueOf(attachment.getTransferState()));
            contentValues.put(UNIQUE_ID, Long.valueOf(currentTimeMillis));
            contentValues.put(CDN_NUMBER, Integer.valueOf(z3 ? attachment2.getCdnNumber() : attachment.getCdnNumber()));
            contentValues.put(CONTENT_LOCATION, z3 ? attachment2.getLocation() : attachment.getLocation());
            contentValues.put(DIGEST, z3 ? attachment2.getDigest() : attachment.getDigest());
            contentValues.put(CONTENT_DISPOSITION, z3 ? attachment2.getKey() : attachment.getKey());
            contentValues.put("name", z3 ? attachment2.getRelay() : attachment.getRelay());
            contentValues.put(FILE_NAME, StorageUtil.getCleanFileName(attachment.getFileName()));
            contentValues.put(SIZE, Long.valueOf(attachment2.getSize()));
            contentValues.put(FAST_PREFLIGHT_ID, attachment.getFastPreflightId());
            contentValues.put("voice_note", Integer.valueOf(attachment.isVoiceNote() ? 1 : 0));
            contentValues.put(BORDERLESS, Integer.valueOf(attachment.isBorderless() ? 1 : 0));
            contentValues.put(VIDEO_GIF, Integer.valueOf(attachment.isVideoGif() ? 1 : 0));
            contentValues.put(WIDTH, Integer.valueOf(attachment2.getWidth()));
            contentValues.put(HEIGHT, Integer.valueOf(attachment2.getHeight()));
            contentValues.put("quote", Boolean.valueOf(z));
            contentValues.put("caption", attachment.getCaption());
            contentValues.put(UPLOAD_TIMESTAMP, Long.valueOf(z3 ? attachment2.getUploadTimestamp() : attachment.getUploadTimestamp()));
            if (attachment.getTransformProperties().isVideoEdited()) {
                contentValues.putNull(VISUAL_HASH);
                contentValues.put(TRANSFORM_PROPERTIES, attachment.getTransformProperties().serialize());
            } else {
                contentValues.put(VISUAL_HASH, getVisualHashStringOrNull(attachment2));
                contentValues.put(TRANSFORM_PROPERTIES, (z3 ? attachment2 : attachment).getTransformProperties().serialize());
            }
            if (attachment.isSticker()) {
                contentValues.put(STICKER_PACK_ID, attachment.getSticker().getPackId());
                contentValues.put(STICKER_PACK_KEY, attachment.getSticker().getPackKey());
                contentValues.put(STICKER_ID, Integer.valueOf(attachment.getSticker().getStickerId()));
                contentValues.put(STICKER_EMOJI, attachment.getSticker().getEmoji());
            }
            if (dataInfo != null) {
                contentValues.put(DATA, dataInfo.file.getAbsolutePath());
                contentValues.put(SIZE, Long.valueOf(dataInfo.length));
                contentValues.put(DATA_RANDOM, dataInfo.random);
                if (!attachment.getTransformProperties().isVideoEdited() && attachment.getTransformProperties().sentMediaQuality == attachment2.getTransformProperties().getSentMediaQuality()) {
                    contentValues.put(DATA_HASH, dataInfo.hash);
                }
                contentValues.putNull(DATA_HASH);
            }
            AttachmentId attachmentId = new AttachmentId(signalWritableDatabase.insert(TABLE_NAME, (String) null, contentValues), currentTimeMillis);
            if (!attachment.isSticker() || hasStickerAttachments()) {
                z2 = false;
            }
            signalWritableDatabase.setTransactionSuccessful();
            if (z2) {
                notifyStickerPackListeners();
            }
            notifyAttachmentListeners();
            return attachmentId;
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    private DatabaseAttachment findTemplateAttachment(String str) {
        Cursor query = this.databaseHelper.getSignalWritableDatabase().query(TABLE_NAME, null, "data_hash = ?", new String[]{str}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    DatabaseAttachment databaseAttachment = getAttachments(query).get(0);
                    query.close();
                    return databaseAttachment;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query == null) {
            return null;
        }
        query.close();
        return null;
    }

    public void writeAudioHash(AttachmentId attachmentId, AudioWaveFormData audioWaveFormData) {
        String str = TAG;
        Log.i(str, "updating part audio wave form for #" + attachmentId);
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        if (audioWaveFormData != null) {
            contentValues.put(VISUAL_HASH, new AudioHash(audioWaveFormData).getHash());
        } else {
            contentValues.putNull(VISUAL_HASH);
        }
        signalWritableDatabase.update(TABLE_NAME, contentValues, PART_ID_WHERE, attachmentId.toStrings());
    }

    public MediaDataSource mediaDataSourceFor(AttachmentId attachmentId) {
        DataInfo attachmentDataFileInfo = getAttachmentDataFileInfo(attachmentId, DATA);
        if (attachmentDataFileInfo != null) {
            return EncryptedMediaDataSource.createFor(this.attachmentSecret, attachmentDataFileInfo.file, attachmentDataFileInfo.random, attachmentDataFileInfo.length);
        }
        Log.w(TAG, "No data file found for video attachment...");
        return null;
    }

    /* loaded from: classes4.dex */
    public static class DataInfo {
        private final File file;
        private final String hash;
        private final long length;
        private final byte[] random;

        private DataInfo(File file, long j, byte[] bArr, String str) {
            this.file = file;
            this.length = j;
            this.random = bArr;
            this.hash = str;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            DataInfo dataInfo = (DataInfo) obj;
            if (this.length != dataInfo.length || !Objects.equals(this.file, dataInfo.file) || !Arrays.equals(this.random, dataInfo.random) || !Objects.equals(this.hash, dataInfo.hash)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (Objects.hash(this.file, Long.valueOf(this.length), this.hash) * 31) + Arrays.hashCode(this.random);
        }
    }

    /* loaded from: classes4.dex */
    public static final class DataUsageResult {
        private static final DataUsageResult IN_USE = new DataUsageResult(true, Collections.emptyList());
        private static final DataUsageResult NOT_IN_USE = new DataUsageResult(false, Collections.emptyList());
        private final boolean hasStrongReference;
        private final List<AttachmentId> removableWeakReferences;

        DataUsageResult(List<AttachmentId> list) {
            this(false, list);
        }

        private DataUsageResult(boolean z, List<AttachmentId> list) {
            if (!z || list.size() <= 0) {
                this.hasStrongReference = z;
                this.removableWeakReferences = list;
                return;
            }
            throw new AssertionError();
        }

        boolean hasStrongReference() {
            return this.hasStrongReference;
        }

        List<AttachmentId> getRemovableWeakReferences() {
            return this.removableWeakReferences;
        }
    }

    /* loaded from: classes.dex */
    public static final class TransformProperties {
        private static final int DEFAULT_MEDIA_QUALITY = SentMediaQuality.STANDARD.getCode();
        @JsonProperty
        private final int sentMediaQuality;
        @JsonProperty
        private final boolean skipTransform;
        @JsonProperty
        private final boolean videoTrim;
        @JsonProperty
        private final long videoTrimEndTimeUs;
        @JsonProperty
        private final long videoTrimStartTimeUs;

        @JsonCreator
        public TransformProperties(@JsonProperty("skipTransform") boolean z, @JsonProperty("videoTrim") boolean z2, @JsonProperty("videoTrimStartTimeUs") long j, @JsonProperty("videoTrimEndTimeUs") long j2, @JsonProperty("sentMediaQuality") int i) {
            this.skipTransform = z;
            this.videoTrim = z2;
            this.videoTrimStartTimeUs = j;
            this.videoTrimEndTimeUs = j2;
            this.sentMediaQuality = i;
        }

        public static TransformProperties empty() {
            return new TransformProperties(false, false, 0, 0, DEFAULT_MEDIA_QUALITY);
        }

        public static TransformProperties forSkipTransform() {
            return new TransformProperties(true, false, 0, 0, DEFAULT_MEDIA_QUALITY);
        }

        public static TransformProperties forVideoTrim(long j, long j2) {
            return new TransformProperties(false, true, j, j2, DEFAULT_MEDIA_QUALITY);
        }

        public static TransformProperties forSentMediaQuality(Optional<TransformProperties> optional, SentMediaQuality sentMediaQuality) {
            TransformProperties orElse = optional.orElse(empty());
            return new TransformProperties(orElse.skipTransform, orElse.videoTrim, orElse.videoTrimStartTimeUs, orElse.videoTrimEndTimeUs, sentMediaQuality.getCode());
        }

        public boolean shouldSkipTransform() {
            return this.skipTransform;
        }

        public boolean isVideoEdited() {
            return isVideoTrim();
        }

        public boolean isVideoTrim() {
            return this.videoTrim;
        }

        public long getVideoTrimStartTimeUs() {
            return this.videoTrimStartTimeUs;
        }

        public long getVideoTrimEndTimeUs() {
            return this.videoTrimEndTimeUs;
        }

        public int getSentMediaQuality() {
            return this.sentMediaQuality;
        }

        TransformProperties withSkipTransform() {
            return new TransformProperties(true, false, 0, 0, this.sentMediaQuality);
        }

        String serialize() {
            return JsonUtil.toJson(this);
        }

        static TransformProperties parse(String str) {
            if (str == null) {
                return empty();
            }
            try {
                return (TransformProperties) JsonUtil.fromJson(str, TransformProperties.class);
            } catch (IOException e) {
                Log.w(AttachmentDatabase.TAG, "Failed to parse TransformProperties!", e);
                return empty();
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || TransformProperties.class != obj.getClass()) {
                return false;
            }
            TransformProperties transformProperties = (TransformProperties) obj;
            if (this.skipTransform == transformProperties.skipTransform && this.videoTrim == transformProperties.videoTrim && this.videoTrimStartTimeUs == transformProperties.videoTrimStartTimeUs && this.videoTrimEndTimeUs == transformProperties.videoTrimEndTimeUs && this.sentMediaQuality == transformProperties.sentMediaQuality) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return Objects.hash(Boolean.valueOf(this.skipTransform), Boolean.valueOf(this.videoTrim), Long.valueOf(this.videoTrimStartTimeUs), Long.valueOf(this.videoTrimEndTimeUs), Integer.valueOf(this.sentMediaQuality));
        }
    }
}
