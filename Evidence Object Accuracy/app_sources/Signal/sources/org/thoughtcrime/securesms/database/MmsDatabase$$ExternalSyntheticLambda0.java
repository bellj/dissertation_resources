package org.thoughtcrime.securesms.database;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MmsDatabase$$ExternalSyntheticLambda0 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return ((DatabaseAttachment) obj).isQuote();
    }
}
