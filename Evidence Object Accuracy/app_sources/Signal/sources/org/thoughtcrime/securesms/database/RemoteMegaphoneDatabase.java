package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import androidx.core.content.ContentValuesKt;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.CursorExtensionsKt;
import org.signal.core.util.SQLiteDatabaseExtensionsKt;
import org.signal.core.util.UpdateBuilderPart1;
import org.signal.core.util.UpdateBuilderPart3;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.database.model.RemoteMegaphoneRecord;

/* compiled from: RemoteMegaphoneDatabase.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 $2\u00020\u0001:\u0001$B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rJ\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\b\b\u0002\u0010\u0010\u001a\u00020\u0011J\u000e\u0010\u0012\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u000eJ\u000e\u0010\u0014\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u0015\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0018\u0010\u0016\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018JD\u0010\u0019\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\u00112\b\u0010\u001b\u001a\u0004\u0018\u00010\n2\u0006\u0010\u001c\u001a\u00020\n2\u0006\u0010\u001d\u001a\u00020\n2\b\u0010\u001e\u001a\u0004\u0018\u00010\n2\b\u0010\u001f\u001a\u0004\u0018\u00010\nJ\f\u0010 \u001a\u00020!*\u00020\u000eH\u0002J\f\u0010\"\u001a\u00020\u000e*\u00020#H\u0002¨\u0006%"}, d2 = {"Lorg/thoughtcrime/securesms/database/RemoteMegaphoneDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "clear", "", "uuid", "", "clearImageUrl", "getAll", "", "Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord;", "getPotentialMegaphonesAndClearOld", "now", "", "insert", "record", "markFinished", "markShown", "setImageUri", "uri", "Landroid/net/Uri;", "update", RemoteMegaphoneDatabase.PRIORITY, RemoteMegaphoneDatabase.COUNTRIES, "title", "body", "primaryActionText", "secondaryActionText", "toContentValues", "Landroid/content/ContentValues;", "toRemoteMegaphoneRecord", "Landroid/database/Cursor;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RemoteMegaphoneDatabase extends Database {
    private static final String BODY;
    private static final String CONDITIONAL_ID;
    private static final String COUNTRIES;
    private static final String CREATE_TABLE = "CREATE TABLE remote_megaphone (\n  _id INTEGER PRIMARY KEY,\n  uuid TEXT UNIQUE NOT NULL,\n  priority INTEGER NOT NULL,\n  countries TEXT,\n  minimum_version INTEGER NOT NULL,\n  dont_show_before INTEGER NOT NULL,\n  dont_show_after INTEGER NOT NULL,\n  show_for_days INTEGER NOT NULL,\n  conditional_id TEXT,\n  primary_action_id TEXT,\n  secondary_action_id TEXT,\n  image_url TEXT,\n  image_uri TEXT DEFAULT NULL,\n  title TEXT NOT NULL,\n  body TEXT NOT NULL,\n  primary_action_text TEXT,\n  secondary_action_text TEXT,\n  shown_at INTEGER DEFAULT 0,\n  finished_at INTEGER DEFAULT 0\n)";
    public static final Companion Companion = new Companion(null);
    private static final String DONT_SHOW_AFTER;
    private static final String DONT_SHOW_BEFORE;
    private static final String FINISHED_AT;
    private static final String ID;
    private static final String IMAGE_BLOB_URI;
    private static final String IMAGE_URL;
    private static final String MINIMUM_VERSION;
    private static final String PRIMARY_ACTION_ID;
    private static final String PRIMARY_ACTION_TEXT;
    private static final String PRIORITY;
    private static final String SECONDARY_ACTION_ID;
    private static final String SECONDARY_ACTION_TEXT;
    private static final String SHOWN_AT;
    private static final String SHOW_FOR_DAYS;
    private static final String TABLE_NAME;
    private static final String TITLE;
    private static final String UUID;
    public static final int VERSION_FINISHED;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RemoteMegaphoneDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    /* compiled from: RemoteMegaphoneDatabase.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0017\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cXT¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/database/RemoteMegaphoneDatabase$Companion;", "", "()V", "BODY", "", "CONDITIONAL_ID", "COUNTRIES", "CREATE_TABLE", "getCREATE_TABLE", "()Ljava/lang/String;", "DONT_SHOW_AFTER", "DONT_SHOW_BEFORE", "FINISHED_AT", "ID", "IMAGE_BLOB_URI", "IMAGE_URL", "MINIMUM_VERSION", "PRIMARY_ACTION_ID", "PRIMARY_ACTION_TEXT", "PRIORITY", "SECONDARY_ACTION_ID", "SECONDARY_ACTION_TEXT", "SHOWN_AT", "SHOW_FOR_DAYS", "TABLE_NAME", "TITLE", "UUID", "VERSION_FINISHED", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final String getCREATE_TABLE() {
            return RemoteMegaphoneDatabase.CREATE_TABLE;
        }
    }

    public final void insert(RemoteMegaphoneRecord remoteMegaphoneRecord) {
        Intrinsics.checkNotNullParameter(remoteMegaphoneRecord, "record");
        getWritableDatabase().insert(TABLE_NAME, 5, toContentValues(remoteMegaphoneRecord));
    }

    public final void update(String str, long j, String str2, String str3, String str4, String str5, String str6) {
        Intrinsics.checkNotNullParameter(str, "uuid");
        Intrinsics.checkNotNullParameter(str3, "title");
        Intrinsics.checkNotNullParameter(str4, "body");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME).values(TuplesKt.to(PRIORITY, Long.valueOf(j)), TuplesKt.to(COUNTRIES, str2), TuplesKt.to("title", str3), TuplesKt.to("body", str4), TuplesKt.to(PRIMARY_ACTION_TEXT, str5), TuplesKt.to(SECONDARY_ACTION_TEXT, str6)).where("uuid = ?", str), 0, 1, null);
    }

    public final List<RemoteMegaphoneRecord> getAll() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Intrinsics.checkNotNullExpressionValue(readableDatabase, "readableDatabase");
        Cursor run = SQLiteDatabaseExtensionsKt.select(readableDatabase, new String[0]).from(TABLE_NAME).run();
        ArrayList arrayList = new ArrayList();
        while (run.moveToNext()) {
            try {
                toRemoteMegaphoneRecord(run);
                arrayList.add(toRemoteMegaphoneRecord(run));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        th = null;
        return arrayList;
    }

    public static /* synthetic */ List getPotentialMegaphonesAndClearOld$default(RemoteMegaphoneDatabase remoteMegaphoneDatabase, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            j = System.currentTimeMillis();
        }
        return remoteMegaphoneDatabase.getPotentialMegaphonesAndClearOld(j);
    }

    /* JADX INFO: finally extract failed */
    public final List<RemoteMegaphoneRecord> getPotentialMegaphonesAndClearOld(long j) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Intrinsics.checkNotNullExpressionValue(readableDatabase, "readableDatabase");
        Cursor run = SQLiteDatabaseExtensionsKt.select(readableDatabase, new String[0]).from(TABLE_NAME).where("finished_at = ? AND minimum_version <= ? AND (dont_show_after > ? AND dont_show_before < ?)", 0, Integer.valueOf((int) BuildConfig.CANONICAL_VERSION_CODE), Long.valueOf(j), Long.valueOf(j)).orderBy("priority DESC").run();
        ArrayList arrayList = new ArrayList();
        while (run.moveToNext()) {
            try {
                toRemoteMegaphoneRecord(run);
                arrayList.add(toRemoteMegaphoneRecord(run));
            } finally {
                try {
                    throw th;
                } catch (Throwable th) {
                }
            }
        }
        CloseableKt.closeFinally(run, null);
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            RemoteMegaphoneRecord remoteMegaphoneRecord = (RemoteMegaphoneRecord) obj;
            if (remoteMegaphoneRecord.getShownAt() > 0 && remoteMegaphoneRecord.getShowForNumberOfDays() > 0) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList();
        for (Object obj2 : arrayList2) {
            RemoteMegaphoneRecord remoteMegaphoneRecord2 = (RemoteMegaphoneRecord) obj2;
            if (remoteMegaphoneRecord2.getShownAt() + TimeUnit.DAYS.toMillis(remoteMegaphoneRecord2.getShowForNumberOfDays()) < j) {
                arrayList3.add(obj2);
            }
        }
        Set<RemoteMegaphoneRecord> set = CollectionsKt___CollectionsKt.toSet(arrayList3);
        for (RemoteMegaphoneRecord remoteMegaphoneRecord3 : set) {
            clear(remoteMegaphoneRecord3.getUuid());
        }
        return CollectionsKt___CollectionsKt.minus((Iterable) arrayList, (Iterable) set);
    }

    public final void setImageUri(String str, Uri uri) {
        Intrinsics.checkNotNullParameter(str, "uuid");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        UpdateBuilderPart1 update = SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME);
        Pair<String, ? extends Object>[] pairArr = new Pair[1];
        pairArr[0] = TuplesKt.to(IMAGE_BLOB_URI, uri != null ? uri.toString() : null);
        UpdateBuilderPart3.run$default(update.values(pairArr).where("uuid = ?", str), 0, 1, null);
    }

    public final void markShown(String str) {
        Intrinsics.checkNotNullParameter(str, "uuid");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME).values(TuplesKt.to(SHOWN_AT, Long.valueOf(System.currentTimeMillis()))).where("uuid = ?", str), 0, 1, null);
    }

    public final void markFinished(String str) {
        Intrinsics.checkNotNullParameter(str, "uuid");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME).values(TuplesKt.to(IMAGE_URL, null), TuplesKt.to(IMAGE_BLOB_URI, null), TuplesKt.to(FINISHED_AT, Long.valueOf(System.currentTimeMillis()))).where("uuid = ?", str), 0, 1, null);
    }

    public final void clearImageUrl(String str) {
        Intrinsics.checkNotNullParameter(str, "uuid");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME).values(TuplesKt.to(IMAGE_URL, null)).where("uuid = ?", str), 0, 1, null);
    }

    public final void clear(String str) {
        Intrinsics.checkNotNullParameter(str, "uuid");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME).values(TuplesKt.to(MINIMUM_VERSION, Integer.MAX_VALUE), TuplesKt.to(IMAGE_URL, null), TuplesKt.to(IMAGE_BLOB_URI, null)).where("uuid = ?", str), 0, 1, null);
    }

    private final ContentValues toContentValues(RemoteMegaphoneRecord remoteMegaphoneRecord) {
        Pair[] pairArr = new Pair[16];
        pairArr[0] = TuplesKt.to("uuid", remoteMegaphoneRecord.getUuid());
        pairArr[1] = TuplesKt.to(PRIORITY, Long.valueOf(remoteMegaphoneRecord.getPriority()));
        pairArr[2] = TuplesKt.to(COUNTRIES, remoteMegaphoneRecord.getCountries());
        pairArr[3] = TuplesKt.to(MINIMUM_VERSION, Integer.valueOf(remoteMegaphoneRecord.getMinimumVersion()));
        pairArr[4] = TuplesKt.to(DONT_SHOW_BEFORE, Long.valueOf(remoteMegaphoneRecord.getDoNotShowBefore()));
        pairArr[5] = TuplesKt.to(DONT_SHOW_AFTER, Long.valueOf(remoteMegaphoneRecord.getDoNotShowAfter()));
        pairArr[6] = TuplesKt.to(SHOW_FOR_DAYS, Long.valueOf(remoteMegaphoneRecord.getShowForNumberOfDays()));
        pairArr[7] = TuplesKt.to(CONDITIONAL_ID, remoteMegaphoneRecord.getConditionalId());
        RemoteMegaphoneRecord.ActionId primaryActionId = remoteMegaphoneRecord.getPrimaryActionId();
        String str = null;
        pairArr[8] = TuplesKt.to(PRIMARY_ACTION_ID, primaryActionId != null ? primaryActionId.getId() : null);
        RemoteMegaphoneRecord.ActionId secondaryActionId = remoteMegaphoneRecord.getSecondaryActionId();
        if (secondaryActionId != null) {
            str = secondaryActionId.getId();
        }
        pairArr[9] = TuplesKt.to(SECONDARY_ACTION_ID, str);
        pairArr[10] = TuplesKt.to(IMAGE_URL, remoteMegaphoneRecord.getImageUrl());
        pairArr[11] = TuplesKt.to("title", remoteMegaphoneRecord.getTitle());
        pairArr[12] = TuplesKt.to("body", remoteMegaphoneRecord.getBody());
        pairArr[13] = TuplesKt.to(PRIMARY_ACTION_TEXT, remoteMegaphoneRecord.getPrimaryActionText());
        pairArr[14] = TuplesKt.to(SECONDARY_ACTION_TEXT, remoteMegaphoneRecord.getSecondaryActionText());
        pairArr[15] = TuplesKt.to(FINISHED_AT, Long.valueOf(remoteMegaphoneRecord.getFinishedAt()));
        return ContentValuesKt.contentValuesOf(pairArr);
    }

    private final RemoteMegaphoneRecord toRemoteMegaphoneRecord(Cursor cursor) {
        Uri uri;
        long requireLong = CursorExtensionsKt.requireLong(cursor, "_id");
        String requireNonNullString = CursorExtensionsKt.requireNonNullString(cursor, "uuid");
        long requireLong2 = CursorExtensionsKt.requireLong(cursor, PRIORITY);
        String requireString = CursorExtensionsKt.requireString(cursor, COUNTRIES);
        int requireInt = CursorExtensionsKt.requireInt(cursor, MINIMUM_VERSION);
        long requireLong3 = CursorExtensionsKt.requireLong(cursor, DONT_SHOW_BEFORE);
        long requireLong4 = CursorExtensionsKt.requireLong(cursor, DONT_SHOW_AFTER);
        long requireLong5 = CursorExtensionsKt.requireLong(cursor, SHOW_FOR_DAYS);
        String requireString2 = CursorExtensionsKt.requireString(cursor, CONDITIONAL_ID);
        RemoteMegaphoneRecord.ActionId.Companion companion = RemoteMegaphoneRecord.ActionId.Companion;
        RemoteMegaphoneRecord.ActionId from = companion.from(CursorExtensionsKt.requireString(cursor, PRIMARY_ACTION_ID));
        RemoteMegaphoneRecord.ActionId from2 = companion.from(CursorExtensionsKt.requireString(cursor, SECONDARY_ACTION_ID));
        String requireString3 = CursorExtensionsKt.requireString(cursor, IMAGE_URL);
        String requireString4 = CursorExtensionsKt.requireString(cursor, IMAGE_BLOB_URI);
        if (requireString4 != null) {
            uri = Uri.parse(requireString4);
            Intrinsics.checkNotNullExpressionValue(uri, "Uri.parse(this)");
        } else {
            uri = null;
        }
        return new RemoteMegaphoneRecord(requireLong, requireLong2, requireNonNullString, requireString, requireInt, requireLong3, requireLong4, requireLong5, requireString2, from, from2, requireString3, uri, CursorExtensionsKt.requireNonNullString(cursor, "title"), CursorExtensionsKt.requireNonNullString(cursor, "body"), CursorExtensionsKt.requireString(cursor, PRIMARY_ACTION_TEXT), CursorExtensionsKt.requireString(cursor, SECONDARY_ACTION_TEXT), CursorExtensionsKt.requireLong(cursor, SHOWN_AT), CursorExtensionsKt.requireLong(cursor, FINISHED_AT));
    }
}
