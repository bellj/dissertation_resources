package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.database.Cursor;
import androidx.core.content.ContentValuesKt;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;

/* compiled from: DonationReceiptDatabase.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0010\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0006\u0010\f\u001a\u00020\rJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\n0\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011J\u0010\u0010\u0012\u001a\u00020\n2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/database/DonationReceiptDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "addReceipt", "", "record", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", "getReceipt", ContactRepository.ID_COLUMN, "", "getReceipts", "", "type", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "readRecord", "cursor", "Landroid/database/Cursor;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptDatabase extends Database {
    private static final String AMOUNT;
    private static final String[] CREATE_INDEXS = {"CREATE INDEX IF NOT EXISTS donation_receipt_type_index ON donation_receipt (receipt_type)", "CREATE INDEX IF NOT EXISTS donation_receipt_date_index ON donation_receipt (receipt_date)"};
    public static final String CREATE_TABLE = "CREATE TABLE donation_receipt (\n  _id INTEGER PRIMARY KEY AUTOINCREMENT,\n  receipt_type TEXT NOT NULL,\n  receipt_date INTEGER NOT NULL,\n  amount TEXT NOT NULL,\n  currency TEXT NOT NULL,\n  subscription_level INTEGER NOT NULL\n)";
    private static final String CURRENCY;
    public static final Companion Companion = new Companion(null);
    private static final String DATE;
    private static final String ID;
    private static final String SUBSCRIPTION_LEVEL;
    private static final String TABLE_NAME;
    private static final String TYPE;

    /* compiled from: DonationReceiptDatabase.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u000b\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0019\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006¢\u0006\n\n\u0002\u0010\t\u001a\u0004\b\u0007\u0010\bR\u0010\u0010\n\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/database/DonationReceiptDatabase$Companion;", "", "()V", "AMOUNT", "", "CREATE_INDEXS", "", "getCREATE_INDEXS", "()[Ljava/lang/String;", "[Ljava/lang/String;", "CREATE_TABLE", "CURRENCY", "DATE", "ID", "SUBSCRIPTION_LEVEL", "TABLE_NAME", "TYPE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final String[] getCREATE_INDEXS() {
            return DonationReceiptDatabase.CREATE_INDEXS;
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DonationReceiptDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    public final void addReceipt(DonationReceiptRecord donationReceiptRecord) {
        Intrinsics.checkNotNullParameter(donationReceiptRecord, "record");
        if (donationReceiptRecord.getId() == -1) {
            getWritableDatabase().insert(TABLE_NAME, (String) null, ContentValuesKt.contentValuesOf(TuplesKt.to(AMOUNT, donationReceiptRecord.getAmount().getAmount().toString()), TuplesKt.to(CURRENCY, donationReceiptRecord.getAmount().getCurrency().getCurrencyCode()), TuplesKt.to(DATE, Long.valueOf(donationReceiptRecord.getTimestamp())), TuplesKt.to(TYPE, donationReceiptRecord.getType().getCode()), TuplesKt.to(SUBSCRIPTION_LEVEL, Integer.valueOf(donationReceiptRecord.getSubscriptionLevel()))));
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Throwable] */
    public final DonationReceiptRecord getReceipt(long j) {
        DonationReceiptRecord donationReceiptRecord;
        Cursor query = getReadableDatabase().query(TABLE_NAME, null, "_id = ?", SqlUtil.buildArgs(j), null, null, null);
        try {
            th = 0;
            if (query.moveToNext()) {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                donationReceiptRecord = readRecord(query);
            } else {
                donationReceiptRecord = th;
            }
            return donationReceiptRecord;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final List<DonationReceiptRecord> getReceipts(DonationReceiptRecord.Type type) {
        Pair pair;
        th = null;
        if (type != null) {
            pair = TuplesKt.to("receipt_type = ?", SqlUtil.buildArgs(type.getCode()));
        } else {
            pair = TuplesKt.to(th, th);
        }
        Cursor query = getReadableDatabase().query(TABLE_NAME, null, (String) pair.component1(), (String[]) pair.component2(), null, null, "receipt_date DESC");
        try {
            ArrayList arrayList = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                arrayList.add(readRecord(query));
            }
            return arrayList;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    private final DonationReceiptRecord readRecord(Cursor cursor) {
        long requireLong = CursorUtil.requireLong(cursor, "_id");
        DonationReceiptRecord.Type.Companion companion = DonationReceiptRecord.Type.Companion;
        String requireString = CursorUtil.requireString(cursor, TYPE);
        Intrinsics.checkNotNullExpressionValue(requireString, "requireString(cursor, TYPE)");
        return new DonationReceiptRecord(requireLong, new FiatMoney(new BigDecimal(CursorUtil.requireString(cursor, AMOUNT)), Currency.getInstance(CursorUtil.requireString(cursor, CURRENCY))), CursorUtil.requireLong(cursor, DATE), companion.fromCode(requireString), CursorUtil.requireInt(cursor, SUBSCRIPTION_LEVEL));
    }
}
