package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import com.annimon.stream.Stream;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda5;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class MentionDatabase extends Database {
    public static final String[] CREATE_INDEXES = {"CREATE INDEX IF NOT EXISTS mention_message_id_index ON mention (message_id);", "CREATE INDEX IF NOT EXISTS mention_recipient_id_thread_id_index ON mention (recipient_id, thread_id);"};
    public static final String CREATE_TABLE;
    private static final String ID;
    public static final String MESSAGE_ID;
    private static final String RANGE_LENGTH;
    private static final String RANGE_START;
    static final String RECIPIENT_ID;
    public static final String TABLE_NAME;
    static final String THREAD_ID;

    public MentionDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public void insert(long j, long j2, Collection<Mention> collection) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            for (Mention mention : collection) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("thread_id", Long.valueOf(j));
                contentValues.put("message_id", Long.valueOf(j2));
                contentValues.put("recipient_id", Long.valueOf(mention.getRecipientId().toLong()));
                contentValues.put(RANGE_START, Integer.valueOf(mention.getStart()));
                contentValues.put(RANGE_LENGTH, Integer.valueOf(mention.getLength()));
                signalWritableDatabase.insert("mention", (String) null, contentValues);
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public List<Mention> getMentionsForMessage(long j) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        LinkedList linkedList = new LinkedList();
        Cursor query = signalReadableDatabase.query("mention", null, "message_id = ?", SqlUtil.buildArgs(j), null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                linkedList.add(new Mention(RecipientId.from(CursorUtil.requireLong(query, "recipient_id")), CursorUtil.requireInt(query, RANGE_START), CursorUtil.requireInt(query, RANGE_LENGTH)));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return linkedList;
    }

    public Map<Long, List<Mention>> getMentionsForMessages(Collection<Long> collection) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String join = TextUtils.join(",", collection);
        Cursor query = signalReadableDatabase.query("mention", null, "message_id IN (" + join + ")", null, null, null, null);
        try {
            Map<Long, List<Mention>> readMentions = readMentions(query);
            if (query != null) {
                query.close();
            }
            return readMentions;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public Map<Long, List<Mention>> getMentionsContainingRecipients(Collection<RecipientId> collection, long j) {
        return getMentionsContainingRecipients(collection, -1, j);
    }

    public Map<Long, List<Mention>> getMentionsContainingRecipients(Collection<RecipientId> collection, long j, long j2) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String str = " WHERE recipient_id IN (" + TextUtils.join(",", Stream.of(collection).map(new SafetyNumberChangeDialog$$ExternalSyntheticLambda5()).toList()) + ")";
        if (j != -1) {
            str = str + " AND thread_id = " + j;
        }
        Cursor rawQuery = signalReadableDatabase.rawQuery("SELECT * FROM mention WHERE message_id IN (" + ("SELECT DISTINCT message_id FROM mention" + str + " ORDER BY _id DESC LIMIT " + j2) + ")", (String[]) null);
        try {
            Map<Long, List<Mention>> readMentions = readMentions(rawQuery);
            if (rawQuery != null) {
                rawQuery.close();
            }
            return readMentions;
        } catch (Throwable th) {
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public void deleteMentionsForMessage(long j) {
        this.databaseHelper.getSignalWritableDatabase().delete("mention", "message_id = ?", SqlUtil.buildArgs(j));
    }

    public void deleteAbandonedMentions() {
        this.databaseHelper.getSignalWritableDatabase().delete("mention", "message_id NOT IN (SELECT _id FROM mms) OR thread_id NOT IN (SELECT _id FROM thread)", (String[]) null);
    }

    public void deleteAllMentions() {
        this.databaseHelper.getSignalWritableDatabase().delete("mention", (String) null, (String[]) null);
    }

    private Map<Long, List<Mention>> readMentions(Cursor cursor) {
        HashMap hashMap = new HashMap();
        while (cursor != null && cursor.moveToNext()) {
            long requireLong = CursorUtil.requireLong(cursor, "message_id");
            List list = (List) hashMap.get(Long.valueOf(requireLong));
            if (list == null) {
                list = new LinkedList();
                hashMap.put(Long.valueOf(requireLong), list);
            }
            list.add(new Mention(RecipientId.from(CursorUtil.requireLong(cursor, "recipient_id")), CursorUtil.requireInt(cursor, RANGE_START), CursorUtil.requireInt(cursor, RANGE_LENGTH)));
        }
        return hashMap;
    }
}
