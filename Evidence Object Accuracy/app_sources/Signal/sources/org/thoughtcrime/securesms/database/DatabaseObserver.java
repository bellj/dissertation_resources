package org.thoughtcrime.securesms.database;

import android.app.Application;
import j$.util.Collection$EL;
import j$.util.function.Consumer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.components.webrtc.SurfaceTextureEglRenderer$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.concurrent.SerialExecutor;

/* loaded from: classes4.dex */
public class DatabaseObserver {
    private static final String KEY_ALL_PAYMENTS;
    private static final String KEY_ATTACHMENTS;
    private static final String KEY_CHAT_COLORS;
    private static final String KEY_CONVERSATION;
    private static final String KEY_CONVERSATION_LIST;
    private static final String KEY_MESSAGE_INSERT;
    private static final String KEY_MESSAGE_UPDATE;
    private static final String KEY_NOTIFICATION_PROFILES;
    private static final String KEY_PAYMENT;
    private static final String KEY_RECIPIENT;
    private static final String KEY_STICKERS;
    private static final String KEY_STICKER_PACKS;
    private static final String KEY_STORY_OBSERVER;
    private static final String KEY_VERBOSE_CONVERSATION;
    private final Set<Observer> allPaymentsObservers = new HashSet();
    private final Application application;
    private final Set<Observer> attachmentObservers = new HashSet();
    private final Set<Observer> chatColorsObservers = new HashSet();
    private final Set<Observer> conversationListObservers = new HashSet();
    private final Map<Long, Set<Observer>> conversationObservers = new HashMap();
    private final Executor executor = new SerialExecutor(SignalExecutors.BOUNDED);
    private final Map<Long, Set<MessageObserver>> messageInsertObservers = new HashMap();
    private final Set<MessageObserver> messageUpdateObservers = new HashSet();
    private final Set<Observer> notificationProfileObservers = new HashSet();
    private final Map<UUID, Set<Observer>> paymentObservers = new HashMap();
    private final Set<Observer> stickerObservers = new HashSet();
    private final Set<Observer> stickerPackObservers = new HashSet();
    private final Map<RecipientId, Set<Observer>> storyObservers = new HashMap();
    private final Map<Long, Set<Observer>> verboseConversationObservers = new HashMap();

    /* loaded from: classes4.dex */
    public interface MessageObserver {
        void onMessageChanged(MessageId messageId);
    }

    /* loaded from: classes4.dex */
    public interface Observer {
        void onChanged();
    }

    public DatabaseObserver(Application application) {
        this.application = application;
    }

    public void registerConversationListObserver(Observer observer) {
        this.executor.execute(new Runnable(observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda14
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerConversationListObserver$0(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$registerConversationListObserver$0(Observer observer) {
        this.conversationListObservers.add(observer);
    }

    public void registerConversationObserver(long j, Observer observer) {
        this.executor.execute(new Runnable(j, observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda23
            public final /* synthetic */ long f$1;
            public final /* synthetic */ DatabaseObserver.Observer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerConversationObserver$1(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$registerConversationObserver$1(long j, Observer observer) {
        registerMapped(this.conversationObservers, Long.valueOf(j), observer);
    }

    public void registerVerboseConversationObserver(long j, Observer observer) {
        this.executor.execute(new Runnable(j, observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda21
            public final /* synthetic */ long f$1;
            public final /* synthetic */ DatabaseObserver.Observer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerVerboseConversationObserver$2(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$registerVerboseConversationObserver$2(long j, Observer observer) {
        registerMapped(this.verboseConversationObservers, Long.valueOf(j), observer);
    }

    public void registerPaymentObserver(UUID uuid, Observer observer) {
        this.executor.execute(new Runnable(uuid, observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda20
            public final /* synthetic */ UUID f$1;
            public final /* synthetic */ DatabaseObserver.Observer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerPaymentObserver$3(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$registerPaymentObserver$3(UUID uuid, Observer observer) {
        registerMapped(this.paymentObservers, uuid, observer);
    }

    public void registerAllPaymentsObserver(Observer observer) {
        this.executor.execute(new Runnable(observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda24
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerAllPaymentsObserver$4(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$registerAllPaymentsObserver$4(Observer observer) {
        this.allPaymentsObservers.add(observer);
    }

    public void registerChatColorsObserver(Observer observer) {
        this.executor.execute(new Runnable(observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda10
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerChatColorsObserver$5(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$registerChatColorsObserver$5(Observer observer) {
        this.chatColorsObservers.add(observer);
    }

    public void registerStickerObserver(Observer observer) {
        this.executor.execute(new Runnable(observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda27
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerStickerObserver$6(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$registerStickerObserver$6(Observer observer) {
        this.stickerObservers.add(observer);
    }

    public void registerStickerPackObserver(Observer observer) {
        this.executor.execute(new Runnable(observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda18
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerStickerPackObserver$7(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$registerStickerPackObserver$7(Observer observer) {
        this.stickerPackObservers.add(observer);
    }

    public void registerAttachmentObserver(Observer observer) {
        this.executor.execute(new Runnable(observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda15
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerAttachmentObserver$8(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$registerAttachmentObserver$8(Observer observer) {
        this.attachmentObservers.add(observer);
    }

    public void registerMessageUpdateObserver(MessageObserver messageObserver) {
        this.executor.execute(new Runnable(messageObserver) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda17
            public final /* synthetic */ DatabaseObserver.MessageObserver f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerMessageUpdateObserver$9(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$registerMessageUpdateObserver$9(MessageObserver messageObserver) {
        this.messageUpdateObservers.add(messageObserver);
    }

    public void registerMessageInsertObserver(long j, MessageObserver messageObserver) {
        this.executor.execute(new Runnable(j, messageObserver) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda7
            public final /* synthetic */ long f$1;
            public final /* synthetic */ DatabaseObserver.MessageObserver f$2;

            {
                this.f$1 = r2;
                this.f$2 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerMessageInsertObserver$10(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$registerMessageInsertObserver$10(long j, MessageObserver messageObserver) {
        registerMapped(this.messageInsertObservers, Long.valueOf(j), messageObserver);
    }

    public void registerNotificationProfileObserver(Observer observer) {
        this.executor.execute(new Runnable(observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda8
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerNotificationProfileObserver$11(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$registerNotificationProfileObserver$11(Observer observer) {
        this.notificationProfileObservers.add(observer);
    }

    public void registerStoryObserver(RecipientId recipientId, Observer observer) {
        this.executor.execute(new Runnable(recipientId, observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda11
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ DatabaseObserver.Observer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$registerStoryObserver$12(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$registerStoryObserver$12(RecipientId recipientId, Observer observer) {
        registerMapped(this.storyObservers, recipientId, observer);
    }

    public void unregisterObserver(Observer observer) {
        this.executor.execute(new Runnable(observer) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda0
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$unregisterObserver$13(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$unregisterObserver$13(Observer observer) {
        this.conversationListObservers.remove(observer);
        unregisterMapped(this.conversationObservers, observer);
        unregisterMapped(this.verboseConversationObservers, observer);
        unregisterMapped(this.paymentObservers, observer);
        this.chatColorsObservers.remove(observer);
        this.stickerObservers.remove(observer);
        this.stickerPackObservers.remove(observer);
        this.attachmentObservers.remove(observer);
        this.notificationProfileObservers.remove(observer);
        unregisterMapped(this.storyObservers, observer);
    }

    public void unregisterObserver(MessageObserver messageObserver) {
        this.executor.execute(new Runnable(messageObserver) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda19
            public final /* synthetic */ DatabaseObserver.MessageObserver f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$unregisterObserver$14(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$unregisterObserver$14(MessageObserver messageObserver) {
        this.messageUpdateObservers.remove(messageObserver);
        unregisterMapped(this.messageInsertObservers, messageObserver);
    }

    public void notifyConversationListeners(Set<Long> set) {
        for (Long l : set) {
            notifyConversationListeners(l.longValue());
        }
    }

    public void notifyConversationListeners(long j) {
        runPostSuccessfulTransaction(KEY_CONVERSATION + j, new Runnable(j) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda22
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyConversationListeners$15(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$notifyConversationListeners$15(long j) {
        notifyMapped(this.conversationObservers, Long.valueOf(j));
        notifyMapped(this.verboseConversationObservers, Long.valueOf(j));
    }

    public void notifyVerboseConversationListeners(Set<Long> set) {
        for (Long l : set) {
            long longValue = l.longValue();
            runPostSuccessfulTransaction(KEY_VERBOSE_CONVERSATION + longValue, new Runnable(longValue) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda4
                public final /* synthetic */ long f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DatabaseObserver.this.lambda$notifyVerboseConversationListeners$16(this.f$1);
                }
            });
        }
    }

    public /* synthetic */ void lambda$notifyVerboseConversationListeners$16(long j) {
        notifyMapped(this.verboseConversationObservers, Long.valueOf(j));
    }

    public void notifyConversationListListeners() {
        runPostSuccessfulTransaction(KEY_CONVERSATION_LIST, new Runnable() { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda30
            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyConversationListListeners$17();
            }
        });
    }

    public /* synthetic */ void lambda$notifyConversationListListeners$17() {
        for (Observer observer : this.conversationListObservers) {
            observer.onChanged();
        }
    }

    public void notifyPaymentListeners(UUID uuid) {
        runPostSuccessfulTransaction(KEY_PAYMENT + uuid.toString(), new Runnable(uuid) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda1
            public final /* synthetic */ UUID f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyPaymentListeners$18(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$notifyPaymentListeners$18(UUID uuid) {
        notifyMapped(this.paymentObservers, uuid);
    }

    public void notifyAllPaymentsListeners() {
        runPostSuccessfulTransaction(KEY_ALL_PAYMENTS, new Runnable() { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda6
            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyAllPaymentsListeners$19();
            }
        });
    }

    public /* synthetic */ void lambda$notifyAllPaymentsListeners$19() {
        notifySet(this.allPaymentsObservers);
    }

    public void notifyChatColorsListeners() {
        runPostSuccessfulTransaction(KEY_CHAT_COLORS, new Runnable() { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda28
            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyChatColorsListeners$20();
            }
        });
    }

    public /* synthetic */ void lambda$notifyChatColorsListeners$20() {
        for (Observer observer : this.chatColorsObservers) {
            observer.onChanged();
        }
    }

    public void notifyStickerObservers() {
        runPostSuccessfulTransaction(KEY_STICKERS, new Runnable() { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda25
            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyStickerObservers$21();
            }
        });
    }

    public /* synthetic */ void lambda$notifyStickerObservers$21() {
        notifySet(this.stickerObservers);
    }

    public void notifyStickerPackObservers() {
        runPostSuccessfulTransaction(KEY_STICKER_PACKS, new Runnable() { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda29
            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyStickerPackObservers$22();
            }
        });
    }

    public /* synthetic */ void lambda$notifyStickerPackObservers$22() {
        notifySet(this.stickerPackObservers);
    }

    public void notifyAttachmentObservers() {
        runPostSuccessfulTransaction(KEY_ATTACHMENTS, new Runnable() { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda16
            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyAttachmentObservers$23();
            }
        });
    }

    public /* synthetic */ void lambda$notifyAttachmentObservers$23() {
        notifySet(this.attachmentObservers);
    }

    public void notifyMessageUpdateObservers(MessageId messageId) {
        runPostSuccessfulTransaction(KEY_MESSAGE_UPDATE + messageId.toString(), new Runnable(messageId) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda12
            public final /* synthetic */ MessageId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyMessageUpdateObservers$25(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$notifyMessageUpdateObservers$25(MessageId messageId) {
        Collection$EL.stream(this.messageUpdateObservers).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda9
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                ((DatabaseObserver.MessageObserver) obj).onMessageChanged(MessageId.this);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    public void notifyMessageInsertObservers(long j, MessageId messageId) {
        runPostSuccessfulTransaction(KEY_MESSAGE_INSERT + messageId, new Runnable(j, messageId) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda13
            public final /* synthetic */ long f$1;
            public final /* synthetic */ MessageId f$2;

            {
                this.f$1 = r2;
                this.f$2 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyMessageInsertObservers$27(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$notifyMessageInsertObservers$27(long j, MessageId messageId) {
        Set<MessageObserver> set = this.messageInsertObservers.get(Long.valueOf(j));
        if (set != null) {
            Collection$EL.stream(set).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda5
                @Override // j$.util.function.Consumer
                public final void accept(Object obj) {
                    ((DatabaseObserver.MessageObserver) obj).onMessageChanged(MessageId.this);
                }

                @Override // j$.util.function.Consumer
                public /* synthetic */ Consumer andThen(Consumer consumer) {
                    return Consumer.CC.$default$andThen(this, consumer);
                }
            });
        }
    }

    public void notifyNotificationProfileObservers() {
        runPostSuccessfulTransaction(KEY_NOTIFICATION_PROFILES, new Runnable() { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda26
            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyNotificationProfileObservers$28();
            }
        });
    }

    public /* synthetic */ void lambda$notifyNotificationProfileObservers$28() {
        notifySet(this.notificationProfileObservers);
    }

    public void notifyRecipientChanged(RecipientId recipientId) {
        SignalDatabase.runPostSuccessfulTransaction(KEY_RECIPIENT + recipientId.serialize(), new Runnable() { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.lambda$notifyRecipientChanged$29(RecipientId.this);
            }
        });
    }

    public static /* synthetic */ void lambda$notifyRecipientChanged$29(RecipientId recipientId) {
        Recipient.live(recipientId).refresh();
    }

    public void notifyStoryObservers(RecipientId recipientId) {
        runPostSuccessfulTransaction(KEY_STORY_OBSERVER, new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda3
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$notifyStoryObservers$30(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$notifyStoryObservers$30(RecipientId recipientId) {
        notifyMapped(this.storyObservers, recipientId);
    }

    private void runPostSuccessfulTransaction(String str, Runnable runnable) {
        SignalDatabase.runPostSuccessfulTransaction(str, new Runnable(runnable) { // from class: org.thoughtcrime.securesms.database.DatabaseObserver$$ExternalSyntheticLambda31
            public final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DatabaseObserver.this.lambda$runPostSuccessfulTransaction$31(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$runPostSuccessfulTransaction$31(Runnable runnable) {
        this.executor.execute(runnable);
    }

    private <K, V> void registerMapped(Map<K, Set<V>> map, K k, V v) {
        Set<V> set = map.get(k);
        if (set == null) {
            set = new HashSet<>();
        }
        set.add(v);
        map.put(k, set);
    }

    private <K, V> void unregisterMapped(Map<K, Set<V>> map, V v) {
        for (Map.Entry<K, Set<V>> entry : map.entrySet()) {
            entry.getValue().remove(v);
        }
    }

    private static <K> void notifyMapped(Map<K, Set<Observer>> map, K k) {
        Set<Observer> set = map.get(k);
        if (set != null) {
            for (Observer observer : set) {
                observer.onChanged();
            }
        }
    }

    private static void notifySet(Set<Observer> set) {
        for (Observer observer : set) {
            observer.onChanged();
        }
    }

    void flush() {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        this.executor.execute(new SurfaceTextureEglRenderer$$ExternalSyntheticLambda0(countDownLatch));
        try {
            countDownLatch.await();
        } catch (InterruptedException unused) {
            throw new AssertionError();
        }
    }
}
