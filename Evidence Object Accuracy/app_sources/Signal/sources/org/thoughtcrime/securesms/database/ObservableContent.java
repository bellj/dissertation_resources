package org.thoughtcrime.securesms.database;

import android.database.ContentObserver;
import java.io.Closeable;

/* loaded from: classes4.dex */
public interface ObservableContent extends Closeable {
    void registerContentObserver(ContentObserver contentObserver);

    void unregisterContentObserver(ContentObserver contentObserver);
}
