package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;

/* compiled from: StoryType.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0001\u0018\u0000 \u00122\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0012B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\f\u001a\u00020\u0000R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\b8F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\tR\u0011\u0010\n\u001a\u00020\b8F¢\u0006\u0006\u001a\u0004\b\n\u0010\tR\u0011\u0010\u000b\u001a\u00020\b8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\tj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/StoryType;", "", "code", "", "(Ljava/lang/String;II)V", "getCode", "()I", "isStory", "", "()Z", "isStoryWithReplies", "isTextStory", "toTextStoryType", "NONE", "STORY_WITH_REPLIES", "STORY_WITHOUT_REPLIES", "TEXT_STORY_WITH_REPLIES", "TEXT_STORY_WITHOUT_REPLIES", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum StoryType {
    NONE(0),
    STORY_WITH_REPLIES(1),
    STORY_WITHOUT_REPLIES(2),
    TEXT_STORY_WITH_REPLIES(3),
    TEXT_STORY_WITHOUT_REPLIES(4);
    
    public static final Companion Companion = new Companion(null);
    private final int code;

    /* compiled from: StoryType.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[StoryType.values().length];
            iArr[StoryType.NONE.ordinal()] = 1;
            iArr[StoryType.STORY_WITH_REPLIES.ordinal()] = 2;
            iArr[StoryType.STORY_WITHOUT_REPLIES.ordinal()] = 3;
            iArr[StoryType.TEXT_STORY_WITH_REPLIES.ordinal()] = 4;
            iArr[StoryType.TEXT_STORY_WITHOUT_REPLIES.ordinal()] = 5;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    @JvmStatic
    public static final StoryType fromCode(int i) {
        return Companion.fromCode(i);
    }

    @JvmStatic
    public static final StoryType withReplies(boolean z) {
        return Companion.withReplies(z);
    }

    @JvmStatic
    public static final StoryType withoutReplies(boolean z) {
        return Companion.withoutReplies(z);
    }

    StoryType(int i) {
        this.code = i;
    }

    public final int getCode() {
        return this.code;
    }

    public final boolean isStory() {
        return this != NONE;
    }

    public final boolean isStoryWithReplies() {
        return this == STORY_WITH_REPLIES || this == TEXT_STORY_WITH_REPLIES;
    }

    public final boolean isTextStory() {
        return this == TEXT_STORY_WITHOUT_REPLIES || this == TEXT_STORY_WITH_REPLIES;
    }

    public final StoryType toTextStoryType() {
        int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
        if (i == 1) {
            return NONE;
        }
        if (i == 2) {
            return TEXT_STORY_WITH_REPLIES;
        }
        if (i == 3) {
            return TEXT_STORY_WITHOUT_REPLIES;
        }
        if (i == 4) {
            return TEXT_STORY_WITH_REPLIES;
        }
        if (i == 5) {
            return TEXT_STORY_WITHOUT_REPLIES;
        }
        throw new NoWhenBranchMatchedException();
    }

    /* compiled from: StoryType.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0007J\u0010\u0010\n\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0007¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/StoryType$Companion;", "", "()V", "fromCode", "Lorg/thoughtcrime/securesms/database/model/StoryType;", "code", "", "withReplies", "isTextStory", "", "withoutReplies", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final StoryType fromCode(int i) {
            if (i == 1) {
                return StoryType.STORY_WITH_REPLIES;
            }
            if (i == 2) {
                return StoryType.STORY_WITHOUT_REPLIES;
            }
            if (i == 3) {
                return StoryType.TEXT_STORY_WITH_REPLIES;
            }
            if (i != 4) {
                return StoryType.NONE;
            }
            return StoryType.TEXT_STORY_WITHOUT_REPLIES;
        }

        @JvmStatic
        public final StoryType withReplies(boolean z) {
            return z ? StoryType.TEXT_STORY_WITH_REPLIES : StoryType.STORY_WITH_REPLIES;
        }

        @JvmStatic
        public final StoryType withoutReplies(boolean z) {
            return z ? StoryType.TEXT_STORY_WITHOUT_REPLIES : StoryType.STORY_WITHOUT_REPLIES;
        }
    }
}
