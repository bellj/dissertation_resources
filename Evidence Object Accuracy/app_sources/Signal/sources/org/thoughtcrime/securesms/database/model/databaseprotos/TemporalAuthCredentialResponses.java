package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponse;

/* loaded from: classes4.dex */
public final class TemporalAuthCredentialResponses extends GeneratedMessageLite<TemporalAuthCredentialResponses, Builder> implements TemporalAuthCredentialResponsesOrBuilder {
    public static final int CREDENTIALRESPONSE_FIELD_NUMBER;
    private static final TemporalAuthCredentialResponses DEFAULT_INSTANCE;
    private static volatile Parser<TemporalAuthCredentialResponses> PARSER;
    private Internal.ProtobufList<TemporalAuthCredentialResponse> credentialResponse_ = GeneratedMessageLite.emptyProtobufList();

    private TemporalAuthCredentialResponses() {
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponsesOrBuilder
    public List<TemporalAuthCredentialResponse> getCredentialResponseList() {
        return this.credentialResponse_;
    }

    public List<? extends TemporalAuthCredentialResponseOrBuilder> getCredentialResponseOrBuilderList() {
        return this.credentialResponse_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponsesOrBuilder
    public int getCredentialResponseCount() {
        return this.credentialResponse_.size();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponsesOrBuilder
    public TemporalAuthCredentialResponse getCredentialResponse(int i) {
        return this.credentialResponse_.get(i);
    }

    public TemporalAuthCredentialResponseOrBuilder getCredentialResponseOrBuilder(int i) {
        return this.credentialResponse_.get(i);
    }

    private void ensureCredentialResponseIsMutable() {
        Internal.ProtobufList<TemporalAuthCredentialResponse> protobufList = this.credentialResponse_;
        if (!protobufList.isModifiable()) {
            this.credentialResponse_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setCredentialResponse(int i, TemporalAuthCredentialResponse temporalAuthCredentialResponse) {
        temporalAuthCredentialResponse.getClass();
        ensureCredentialResponseIsMutable();
        this.credentialResponse_.set(i, temporalAuthCredentialResponse);
    }

    public void addCredentialResponse(TemporalAuthCredentialResponse temporalAuthCredentialResponse) {
        temporalAuthCredentialResponse.getClass();
        ensureCredentialResponseIsMutable();
        this.credentialResponse_.add(temporalAuthCredentialResponse);
    }

    public void addCredentialResponse(int i, TemporalAuthCredentialResponse temporalAuthCredentialResponse) {
        temporalAuthCredentialResponse.getClass();
        ensureCredentialResponseIsMutable();
        this.credentialResponse_.add(i, temporalAuthCredentialResponse);
    }

    public void addAllCredentialResponse(Iterable<? extends TemporalAuthCredentialResponse> iterable) {
        ensureCredentialResponseIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.credentialResponse_);
    }

    public void clearCredentialResponse() {
        this.credentialResponse_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeCredentialResponse(int i) {
        ensureCredentialResponseIsMutable();
        this.credentialResponse_.remove(i);
    }

    public static TemporalAuthCredentialResponses parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static TemporalAuthCredentialResponses parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static TemporalAuthCredentialResponses parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static TemporalAuthCredentialResponses parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static TemporalAuthCredentialResponses parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static TemporalAuthCredentialResponses parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static TemporalAuthCredentialResponses parseFrom(InputStream inputStream) throws IOException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TemporalAuthCredentialResponses parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TemporalAuthCredentialResponses parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TemporalAuthCredentialResponses parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TemporalAuthCredentialResponses parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static TemporalAuthCredentialResponses parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TemporalAuthCredentialResponses) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(TemporalAuthCredentialResponses temporalAuthCredentialResponses) {
        return DEFAULT_INSTANCE.createBuilder(temporalAuthCredentialResponses);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<TemporalAuthCredentialResponses, Builder> implements TemporalAuthCredentialResponsesOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(TemporalAuthCredentialResponses.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponsesOrBuilder
        public List<TemporalAuthCredentialResponse> getCredentialResponseList() {
            return Collections.unmodifiableList(((TemporalAuthCredentialResponses) this.instance).getCredentialResponseList());
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponsesOrBuilder
        public int getCredentialResponseCount() {
            return ((TemporalAuthCredentialResponses) this.instance).getCredentialResponseCount();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponsesOrBuilder
        public TemporalAuthCredentialResponse getCredentialResponse(int i) {
            return ((TemporalAuthCredentialResponses) this.instance).getCredentialResponse(i);
        }

        public Builder setCredentialResponse(int i, TemporalAuthCredentialResponse temporalAuthCredentialResponse) {
            copyOnWrite();
            ((TemporalAuthCredentialResponses) this.instance).setCredentialResponse(i, temporalAuthCredentialResponse);
            return this;
        }

        public Builder setCredentialResponse(int i, TemporalAuthCredentialResponse.Builder builder) {
            copyOnWrite();
            ((TemporalAuthCredentialResponses) this.instance).setCredentialResponse(i, builder.build());
            return this;
        }

        public Builder addCredentialResponse(TemporalAuthCredentialResponse temporalAuthCredentialResponse) {
            copyOnWrite();
            ((TemporalAuthCredentialResponses) this.instance).addCredentialResponse(temporalAuthCredentialResponse);
            return this;
        }

        public Builder addCredentialResponse(int i, TemporalAuthCredentialResponse temporalAuthCredentialResponse) {
            copyOnWrite();
            ((TemporalAuthCredentialResponses) this.instance).addCredentialResponse(i, temporalAuthCredentialResponse);
            return this;
        }

        public Builder addCredentialResponse(TemporalAuthCredentialResponse.Builder builder) {
            copyOnWrite();
            ((TemporalAuthCredentialResponses) this.instance).addCredentialResponse(builder.build());
            return this;
        }

        public Builder addCredentialResponse(int i, TemporalAuthCredentialResponse.Builder builder) {
            copyOnWrite();
            ((TemporalAuthCredentialResponses) this.instance).addCredentialResponse(i, builder.build());
            return this;
        }

        public Builder addAllCredentialResponse(Iterable<? extends TemporalAuthCredentialResponse> iterable) {
            copyOnWrite();
            ((TemporalAuthCredentialResponses) this.instance).addAllCredentialResponse(iterable);
            return this;
        }

        public Builder clearCredentialResponse() {
            copyOnWrite();
            ((TemporalAuthCredentialResponses) this.instance).clearCredentialResponse();
            return this;
        }

        public Builder removeCredentialResponse(int i) {
            copyOnWrite();
            ((TemporalAuthCredentialResponses) this.instance).removeCredentialResponse(i);
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponses$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new TemporalAuthCredentialResponses();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"credentialResponse_", TemporalAuthCredentialResponse.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<TemporalAuthCredentialResponses> parser = PARSER;
                if (parser == null) {
                    synchronized (TemporalAuthCredentialResponses.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        TemporalAuthCredentialResponses temporalAuthCredentialResponses = new TemporalAuthCredentialResponses();
        DEFAULT_INSTANCE = temporalAuthCredentialResponses;
        GeneratedMessageLite.registerDefaultInstance(TemporalAuthCredentialResponses.class, temporalAuthCredentialResponses);
    }

    public static TemporalAuthCredentialResponses getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<TemporalAuthCredentialResponses> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
