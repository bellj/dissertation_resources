package org.thoughtcrime.securesms.database.model;

import android.content.Context;
import android.text.SpannableString;
import java.util.Collections;
import java.util.HashSet;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class NotificationMmsMessageRecord extends MmsMessageRecord {
    private final byte[] contentLocation;
    private final long expiry;
    private final long messageSize;
    private final int status;
    private final byte[] transactionId;

    @Override // org.thoughtcrime.securesms.database.model.MmsMessageRecord, org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isMediaPending() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isMmsNotification() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.database.model.DisplayRecord
    public boolean isOutgoing() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.database.model.DisplayRecord
    public boolean isPending() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isSecure() {
        return false;
    }

    public NotificationMmsMessageRecord(long j, Recipient recipient, Recipient recipient2, int i, long j2, long j3, int i2, long j4, byte[] bArr, long j5, long j6, int i3, byte[] bArr2, long j7, int i4, SlideDeck slideDeck, int i5, int i6, long j8, StoryType storyType, ParentStoryId parentStoryId, GiftBadge giftBadge) {
        super(j, "", recipient, recipient2, i, j2, j3, -1, j4, -1, i2, j7, new HashSet(), new HashSet(), i4, 0, 0, false, slideDeck, i5, null, Collections.emptyList(), Collections.emptyList(), false, Collections.emptyList(), false, 0, i6, j8, storyType, parentStoryId, giftBadge);
        this.contentLocation = bArr;
        this.messageSize = j5;
        this.expiry = j6;
        this.status = i3;
        this.transactionId = bArr2;
    }

    public byte[] getTransactionId() {
        return this.transactionId;
    }

    public int getStatus() {
        return this.status;
    }

    public byte[] getContentLocation() {
        return this.contentLocation;
    }

    public long getMessageSize() {
        return (this.messageSize + 1023) / 1024;
    }

    public long getExpiration() {
        return this.expiry * 1000;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord, org.thoughtcrime.securesms.database.model.DisplayRecord
    public SpannableString getDisplayBody(Context context) {
        int i = this.status;
        if (i == 1) {
            return MessageRecord.emphasisAdded(context.getString(R.string.NotificationMmsMessageRecord_multimedia_message));
        }
        if (i == 3) {
            return MessageRecord.emphasisAdded(context.getString(R.string.NotificationMmsMessageRecord_downloading_mms_message));
        }
        return MessageRecord.emphasisAdded(context.getString(R.string.NotificationMmsMessageRecord_error_downloading_mms_message));
    }
}
