package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes4.dex */
public final class GiftBadge extends GeneratedMessageLite<GiftBadge, Builder> implements GiftBadgeOrBuilder {
    private static final GiftBadge DEFAULT_INSTANCE;
    private static volatile Parser<GiftBadge> PARSER;
    public static final int REDEMPTIONSTATE_FIELD_NUMBER;
    public static final int REDEMPTIONTOKEN_FIELD_NUMBER;
    private int redemptionState_;
    private ByteString redemptionToken_ = ByteString.EMPTY;

    private GiftBadge() {
    }

    /* loaded from: classes4.dex */
    public enum RedemptionState implements Internal.EnumLite {
        PENDING(0),
        STARTED(1),
        REDEEMED(2),
        FAILED(3),
        UNRECOGNIZED(-1);
        
        public static final int FAILED_VALUE;
        public static final int PENDING_VALUE;
        public static final int REDEEMED_VALUE;
        public static final int STARTED_VALUE;
        private static final Internal.EnumLiteMap<RedemptionState> internalValueMap = new Internal.EnumLiteMap<RedemptionState>() { // from class: org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge.RedemptionState.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public RedemptionState findValueByNumber(int i) {
                return RedemptionState.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        @Deprecated
        public static RedemptionState valueOf(int i) {
            return forNumber(i);
        }

        public static RedemptionState forNumber(int i) {
            if (i == 0) {
                return PENDING;
            }
            if (i == 1) {
                return STARTED;
            }
            if (i == 2) {
                return REDEEMED;
            }
            if (i != 3) {
                return null;
            }
            return FAILED;
        }

        public static Internal.EnumLiteMap<RedemptionState> internalGetValueMap() {
            return internalValueMap;
        }

        public static Internal.EnumVerifier internalGetVerifier() {
            return RedemptionStateVerifier.INSTANCE;
        }

        /* loaded from: classes4.dex */
        private static final class RedemptionStateVerifier implements Internal.EnumVerifier {
            static final Internal.EnumVerifier INSTANCE = new RedemptionStateVerifier();

            private RedemptionStateVerifier() {
            }

            @Override // com.google.protobuf.Internal.EnumVerifier
            public boolean isInRange(int i) {
                return RedemptionState.forNumber(i) != null;
            }
        }

        RedemptionState(int i) {
            this.value = i;
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadgeOrBuilder
    public ByteString getRedemptionToken() {
        return this.redemptionToken_;
    }

    public void setRedemptionToken(ByteString byteString) {
        byteString.getClass();
        this.redemptionToken_ = byteString;
    }

    public void clearRedemptionToken() {
        this.redemptionToken_ = getDefaultInstance().getRedemptionToken();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadgeOrBuilder
    public int getRedemptionStateValue() {
        return this.redemptionState_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadgeOrBuilder
    public RedemptionState getRedemptionState() {
        RedemptionState forNumber = RedemptionState.forNumber(this.redemptionState_);
        return forNumber == null ? RedemptionState.UNRECOGNIZED : forNumber;
    }

    public void setRedemptionStateValue(int i) {
        this.redemptionState_ = i;
    }

    public void setRedemptionState(RedemptionState redemptionState) {
        this.redemptionState_ = redemptionState.getNumber();
    }

    public void clearRedemptionState() {
        this.redemptionState_ = 0;
    }

    public static GiftBadge parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (GiftBadge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static GiftBadge parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GiftBadge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static GiftBadge parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (GiftBadge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static GiftBadge parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GiftBadge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static GiftBadge parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (GiftBadge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static GiftBadge parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GiftBadge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static GiftBadge parseFrom(InputStream inputStream) throws IOException {
        return (GiftBadge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GiftBadge parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GiftBadge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GiftBadge parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (GiftBadge) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GiftBadge parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GiftBadge) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GiftBadge parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (GiftBadge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static GiftBadge parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GiftBadge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(GiftBadge giftBadge) {
        return DEFAULT_INSTANCE.createBuilder(giftBadge);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<GiftBadge, Builder> implements GiftBadgeOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(GiftBadge.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadgeOrBuilder
        public ByteString getRedemptionToken() {
            return ((GiftBadge) this.instance).getRedemptionToken();
        }

        public Builder setRedemptionToken(ByteString byteString) {
            copyOnWrite();
            ((GiftBadge) this.instance).setRedemptionToken(byteString);
            return this;
        }

        public Builder clearRedemptionToken() {
            copyOnWrite();
            ((GiftBadge) this.instance).clearRedemptionToken();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadgeOrBuilder
        public int getRedemptionStateValue() {
            return ((GiftBadge) this.instance).getRedemptionStateValue();
        }

        public Builder setRedemptionStateValue(int i) {
            copyOnWrite();
            ((GiftBadge) this.instance).setRedemptionStateValue(i);
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadgeOrBuilder
        public RedemptionState getRedemptionState() {
            return ((GiftBadge) this.instance).getRedemptionState();
        }

        public Builder setRedemptionState(RedemptionState redemptionState) {
            copyOnWrite();
            ((GiftBadge) this.instance).setRedemptionState(redemptionState);
            return this;
        }

        public Builder clearRedemptionState() {
            copyOnWrite();
            ((GiftBadge) this.instance).clearRedemptionState();
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new GiftBadge();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\f", new Object[]{"redemptionToken_", "redemptionState_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<GiftBadge> parser = PARSER;
                if (parser == null) {
                    synchronized (GiftBadge.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        GiftBadge giftBadge = new GiftBadge();
        DEFAULT_INSTANCE = giftBadge;
        GeneratedMessageLite.registerDefaultInstance(GiftBadge.class, giftBadge);
    }

    public static GiftBadge getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<GiftBadge> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
