package org.thoughtcrime.securesms.database.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public abstract class MmsMessageRecord extends MessageRecord {
    private final List<Contact> contacts;
    private final GiftBadge giftBadge;
    private final List<LinkPreview> linkPreviews;
    private final ParentStoryId parentStoryId;
    private final Quote quote;
    private final SlideDeck slideDeck;
    private final StoryType storyType;
    private final boolean viewOnce;

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isMms() {
        return true;
    }

    public MmsMessageRecord(long j, String str, Recipient recipient, Recipient recipient2, int i, long j2, long j3, long j4, long j5, int i2, int i3, long j6, Set<IdentityKeyMismatch> set, Set<NetworkFailure> set2, int i4, long j7, long j8, boolean z, SlideDeck slideDeck, int i5, Quote quote, List<Contact> list, List<LinkPreview> list2, boolean z2, List<ReactionRecord> list3, boolean z3, long j9, int i6, long j10, StoryType storyType, ParentStoryId parentStoryId, GiftBadge giftBadge) {
        super(j, str, recipient, recipient2, i, j2, j3, j4, j5, i2, i3, j6, set, set2, i4, j7, j8, i5, z2, list3, z3, j9, i6, j10);
        LinkedList linkedList = new LinkedList();
        this.contacts = linkedList;
        LinkedList linkedList2 = new LinkedList();
        this.linkPreviews = linkedList2;
        this.slideDeck = slideDeck;
        this.quote = quote;
        this.viewOnce = z;
        this.storyType = storyType;
        this.parentStoryId = parentStoryId;
        this.giftBadge = giftBadge;
        linkedList.addAll(list);
        linkedList2.addAll(list2);
    }

    public SlideDeck getSlideDeck() {
        return this.slideDeck;
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x0012  */
    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isMediaPending() {
        /*
            r3 = this;
            org.thoughtcrime.securesms.mms.SlideDeck r0 = r3.getSlideDeck()
            java.util.List r0 = r0.getSlides()
            java.util.Iterator r0 = r0.iterator()
        L_0x000c:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0026
            java.lang.Object r1 = r0.next()
            org.thoughtcrime.securesms.mms.Slide r1 = (org.thoughtcrime.securesms.mms.Slide) r1
            boolean r2 = r1.isInProgress()
            if (r2 != 0) goto L_0x0024
            boolean r1 = r1.isPendingDownload()
            if (r1 == 0) goto L_0x000c
        L_0x0024:
            r0 = 1
            return r0
        L_0x0026:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.model.MmsMessageRecord.isMediaPending():boolean");
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isViewOnce() {
        return this.viewOnce;
    }

    public StoryType getStoryType() {
        return this.storyType;
    }

    public ParentStoryId getParentStoryId() {
        return this.parentStoryId;
    }

    public boolean containsMediaSlide() {
        return this.slideDeck.containsMediaSlide();
    }

    public Quote getQuote() {
        return this.quote;
    }

    public List<Contact> getSharedContacts() {
        return this.contacts;
    }

    public List<LinkPreview> getLinkPreviews() {
        return this.linkPreviews;
    }

    public GiftBadge getGiftBadge() {
        return this.giftBadge;
    }
}
