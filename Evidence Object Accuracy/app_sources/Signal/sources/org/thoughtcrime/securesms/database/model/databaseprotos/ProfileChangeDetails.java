package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes4.dex */
public final class ProfileChangeDetails extends GeneratedMessageLite<ProfileChangeDetails, Builder> implements ProfileChangeDetailsOrBuilder {
    private static final ProfileChangeDetails DEFAULT_INSTANCE;
    private static volatile Parser<ProfileChangeDetails> PARSER;
    public static final int PROFILENAMECHANGE_FIELD_NUMBER;
    private StringChange profileNameChange_;

    /* loaded from: classes4.dex */
    public interface StringChangeOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getNew();

        ByteString getNewBytes();

        String getPrevious();

        ByteString getPreviousBytes();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private ProfileChangeDetails() {
    }

    /* loaded from: classes4.dex */
    public static final class StringChange extends GeneratedMessageLite<StringChange, Builder> implements StringChangeOrBuilder {
        private static final StringChange DEFAULT_INSTANCE;
        public static final int NEW_FIELD_NUMBER;
        private static volatile Parser<StringChange> PARSER;
        public static final int PREVIOUS_FIELD_NUMBER;
        private String new_ = "";
        private String previous_ = "";

        private StringChange() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails.StringChangeOrBuilder
        public String getPrevious() {
            return this.previous_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails.StringChangeOrBuilder
        public ByteString getPreviousBytes() {
            return ByteString.copyFromUtf8(this.previous_);
        }

        public void setPrevious(String str) {
            str.getClass();
            this.previous_ = str;
        }

        public void clearPrevious() {
            this.previous_ = getDefaultInstance().getPrevious();
        }

        public void setPreviousBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.previous_ = byteString.toStringUtf8();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails.StringChangeOrBuilder
        public String getNew() {
            return this.new_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails.StringChangeOrBuilder
        public ByteString getNewBytes() {
            return ByteString.copyFromUtf8(this.new_);
        }

        public void setNew(String str) {
            str.getClass();
            this.new_ = str;
        }

        public void clearNew() {
            this.new_ = getDefaultInstance().getNew();
        }

        public void setNewBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.new_ = byteString.toStringUtf8();
        }

        public static StringChange parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (StringChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static StringChange parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (StringChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static StringChange parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (StringChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static StringChange parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (StringChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static StringChange parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (StringChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static StringChange parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (StringChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static StringChange parseFrom(InputStream inputStream) throws IOException {
            return (StringChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static StringChange parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (StringChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static StringChange parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (StringChange) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static StringChange parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (StringChange) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static StringChange parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (StringChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static StringChange parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (StringChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(StringChange stringChange) {
            return DEFAULT_INSTANCE.createBuilder(stringChange);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<StringChange, Builder> implements StringChangeOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(StringChange.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails.StringChangeOrBuilder
            public String getPrevious() {
                return ((StringChange) this.instance).getPrevious();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails.StringChangeOrBuilder
            public ByteString getPreviousBytes() {
                return ((StringChange) this.instance).getPreviousBytes();
            }

            public Builder setPrevious(String str) {
                copyOnWrite();
                ((StringChange) this.instance).setPrevious(str);
                return this;
            }

            public Builder clearPrevious() {
                copyOnWrite();
                ((StringChange) this.instance).clearPrevious();
                return this;
            }

            public Builder setPreviousBytes(ByteString byteString) {
                copyOnWrite();
                ((StringChange) this.instance).setPreviousBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails.StringChangeOrBuilder
            public String getNew() {
                return ((StringChange) this.instance).getNew();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails.StringChangeOrBuilder
            public ByteString getNewBytes() {
                return ((StringChange) this.instance).getNewBytes();
            }

            public Builder setNew(String str) {
                copyOnWrite();
                ((StringChange) this.instance).setNew(str);
                return this;
            }

            public Builder clearNew() {
                copyOnWrite();
                ((StringChange) this.instance).clearNew();
                return this;
            }

            public Builder setNewBytes(ByteString byteString) {
                copyOnWrite();
                ((StringChange) this.instance).setNewBytes(byteString);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new StringChange();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001Ȉ\u0002Ȉ", new Object[]{"previous_", "new_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<StringChange> parser = PARSER;
                    if (parser == null) {
                        synchronized (StringChange.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            StringChange stringChange = new StringChange();
            DEFAULT_INSTANCE = stringChange;
            GeneratedMessageLite.registerDefaultInstance(StringChange.class, stringChange);
        }

        public static StringChange getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<StringChange> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetailsOrBuilder
    public boolean hasProfileNameChange() {
        return this.profileNameChange_ != null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetailsOrBuilder
    public StringChange getProfileNameChange() {
        StringChange stringChange = this.profileNameChange_;
        return stringChange == null ? StringChange.getDefaultInstance() : stringChange;
    }

    public void setProfileNameChange(StringChange stringChange) {
        stringChange.getClass();
        this.profileNameChange_ = stringChange;
    }

    public void mergeProfileNameChange(StringChange stringChange) {
        stringChange.getClass();
        StringChange stringChange2 = this.profileNameChange_;
        if (stringChange2 == null || stringChange2 == StringChange.getDefaultInstance()) {
            this.profileNameChange_ = stringChange;
        } else {
            this.profileNameChange_ = StringChange.newBuilder(this.profileNameChange_).mergeFrom((StringChange.Builder) stringChange).buildPartial();
        }
    }

    public void clearProfileNameChange() {
        this.profileNameChange_ = null;
    }

    public static ProfileChangeDetails parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ProfileChangeDetails parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ProfileChangeDetails parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ProfileChangeDetails parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ProfileChangeDetails parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ProfileChangeDetails parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ProfileChangeDetails parseFrom(InputStream inputStream) throws IOException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ProfileChangeDetails parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ProfileChangeDetails parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ProfileChangeDetails parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ProfileChangeDetails parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ProfileChangeDetails parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ProfileChangeDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ProfileChangeDetails profileChangeDetails) {
        return DEFAULT_INSTANCE.createBuilder(profileChangeDetails);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ProfileChangeDetails, Builder> implements ProfileChangeDetailsOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(ProfileChangeDetails.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetailsOrBuilder
        public boolean hasProfileNameChange() {
            return ((ProfileChangeDetails) this.instance).hasProfileNameChange();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetailsOrBuilder
        public StringChange getProfileNameChange() {
            return ((ProfileChangeDetails) this.instance).getProfileNameChange();
        }

        public Builder setProfileNameChange(StringChange stringChange) {
            copyOnWrite();
            ((ProfileChangeDetails) this.instance).setProfileNameChange(stringChange);
            return this;
        }

        public Builder setProfileNameChange(StringChange.Builder builder) {
            copyOnWrite();
            ((ProfileChangeDetails) this.instance).setProfileNameChange(builder.build());
            return this;
        }

        public Builder mergeProfileNameChange(StringChange stringChange) {
            copyOnWrite();
            ((ProfileChangeDetails) this.instance).mergeProfileNameChange(stringChange);
            return this;
        }

        public Builder clearProfileNameChange() {
            copyOnWrite();
            ((ProfileChangeDetails) this.instance).clearProfileNameChange();
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ProfileChangeDetails();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\t", new Object[]{"profileNameChange_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ProfileChangeDetails> parser = PARSER;
                if (parser == null) {
                    synchronized (ProfileChangeDetails.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ProfileChangeDetails profileChangeDetails = new ProfileChangeDetails();
        DEFAULT_INSTANCE = profileChangeDetails;
        GeneratedMessageLite.registerDefaultInstance(ProfileChangeDetails.class, profileChangeDetails);
    }

    public static ProfileChangeDetails getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ProfileChangeDetails> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
