package org.thoughtcrime.securesms.database.model;

import android.content.Context;
import android.text.SpannableString;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public abstract class DisplayRecord {
    private final String body;
    private final long dateReceived;
    private final long dateSent;
    private final int deliveryReceiptCount;
    private final int deliveryStatus;
    private final int readReceiptCount;
    private final Recipient recipient;
    private final long threadId;
    protected final long type;
    private final int viewReceiptCount;

    public abstract SpannableString getDisplayBody(Context context);

    public DisplayRecord(String str, Recipient recipient, long j, long j2, long j3, int i, int i2, long j4, int i3, int i4) {
        this.threadId = j3;
        this.recipient = recipient;
        this.dateSent = j;
        this.dateReceived = j2;
        this.type = j4;
        this.body = str;
        this.deliveryReceiptCount = i2;
        this.readReceiptCount = i3;
        this.deliveryStatus = i;
        this.viewReceiptCount = i4;
    }

    public String getBody() {
        String str = this.body;
        return str == null ? "" : str;
    }

    public boolean isFailed() {
        return MmsSmsColumns.Types.isFailedMessageType(this.type) || MmsSmsColumns.Types.isPendingSecureSmsFallbackType(this.type) || this.deliveryStatus >= 64;
    }

    public boolean isPending() {
        return MmsSmsColumns.Types.isPendingMessageType(this.type) && !MmsSmsColumns.Types.isIdentityVerified(this.type) && !MmsSmsColumns.Types.isIdentityDefault(this.type);
    }

    public long getType() {
        return this.type;
    }

    public boolean isSent() {
        return MmsSmsColumns.Types.isSentType(this.type);
    }

    public boolean isOutgoing() {
        return MmsSmsColumns.Types.isOutgoingMessageType(this.type);
    }

    public Recipient getRecipient() {
        return this.recipient.live().get();
    }

    public long getDateSent() {
        return this.dateSent;
    }

    public long getDateReceived() {
        return this.dateReceived;
    }

    public long getThreadId() {
        return this.threadId;
    }

    public boolean isKeyExchange() {
        return MmsSmsColumns.Types.isKeyExchangeType(this.type);
    }

    public boolean isEndSession() {
        return MmsSmsColumns.Types.isEndSessionType(this.type);
    }

    public boolean isGroupUpdate() {
        return MmsSmsColumns.Types.isGroupUpdate(this.type);
    }

    public boolean isGroupV2() {
        return MmsSmsColumns.Types.isGroupV2(this.type);
    }

    public boolean isGroupQuit() {
        return MmsSmsColumns.Types.isGroupQuit(this.type);
    }

    public boolean isGroupAction() {
        return isGroupUpdate() || isGroupQuit();
    }

    public boolean isExpirationTimerUpdate() {
        return MmsSmsColumns.Types.isExpirationTimerUpdate(this.type);
    }

    public boolean isCallLog() {
        return MmsSmsColumns.Types.isCallLog(this.type);
    }

    public boolean isJoined() {
        return MmsSmsColumns.Types.isJoinedType(this.type);
    }

    public boolean isIncomingAudioCall() {
        return MmsSmsColumns.Types.isIncomingAudioCall(this.type);
    }

    public boolean isIncomingVideoCall() {
        return MmsSmsColumns.Types.isIncomingVideoCall(this.type);
    }

    public boolean isOutgoingAudioCall() {
        return MmsSmsColumns.Types.isOutgoingAudioCall(this.type);
    }

    public boolean isOutgoingVideoCall() {
        return MmsSmsColumns.Types.isOutgoingVideoCall(this.type);
    }

    public final boolean isMissedAudioCall() {
        return MmsSmsColumns.Types.isMissedAudioCall(this.type);
    }

    public final boolean isMissedVideoCall() {
        return MmsSmsColumns.Types.isMissedVideoCall(this.type);
    }

    public final boolean isGroupCall() {
        return MmsSmsColumns.Types.isGroupCall(this.type);
    }

    public boolean isVerificationStatusChange() {
        return MmsSmsColumns.Types.isIdentityDefault(this.type) || MmsSmsColumns.Types.isIdentityVerified(this.type);
    }

    public boolean isProfileChange() {
        return MmsSmsColumns.Types.isProfileChange(this.type);
    }

    public boolean isChangeNumber() {
        return MmsSmsColumns.Types.isChangeNumber(this.type);
    }

    public boolean isBoostRequest() {
        return MmsSmsColumns.Types.isBoostRequest(this.type);
    }

    public int getDeliveryStatus() {
        return this.deliveryStatus;
    }

    public int getDeliveryReceiptCount() {
        return this.deliveryReceiptCount;
    }

    public int getReadReceiptCount() {
        return this.readReceiptCount;
    }

    public int getViewedReceiptCount() {
        return this.viewReceiptCount;
    }

    public boolean isDelivered() {
        int i = this.deliveryStatus;
        return (i >= 0 && i < 32) || this.deliveryReceiptCount > 0;
    }

    public boolean isRemoteViewed() {
        return this.viewReceiptCount > 0;
    }

    public boolean isRemoteRead() {
        return this.readReceiptCount > 0;
    }

    public boolean isPendingInsecureSmsFallback() {
        return MmsSmsColumns.Types.isPendingInsecureSmsFallbackType(this.type);
    }
}
