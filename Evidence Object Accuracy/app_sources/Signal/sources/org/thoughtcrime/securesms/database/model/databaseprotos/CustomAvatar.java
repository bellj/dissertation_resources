package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes4.dex */
public final class CustomAvatar extends GeneratedMessageLite<CustomAvatar, Builder> implements CustomAvatarOrBuilder {
    private static final CustomAvatar DEFAULT_INSTANCE;
    private static volatile Parser<CustomAvatar> PARSER;
    public static final int PHOTO_FIELD_NUMBER;
    public static final int TEXT_FIELD_NUMBER;
    public static final int VECTOR_FIELD_NUMBER;
    private int avatarCase_ = 0;
    private Object avatar_;

    /* loaded from: classes4.dex */
    public interface PhotoOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        long getSize();

        String getUri();

        ByteString getUriBytes();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes4.dex */
    public interface TextOrBuilder extends MessageLiteOrBuilder {
        String getColors();

        ByteString getColorsBytes();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getText();

        ByteString getTextBytes();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes4.dex */
    public interface VectorOrBuilder extends MessageLiteOrBuilder {
        String getColors();

        ByteString getColorsBytes();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getKey();

        ByteString getKeyBytes();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private CustomAvatar() {
    }

    /* loaded from: classes4.dex */
    public static final class Text extends GeneratedMessageLite<Text, Builder> implements TextOrBuilder {
        public static final int COLORS_FIELD_NUMBER;
        private static final Text DEFAULT_INSTANCE;
        private static volatile Parser<Text> PARSER;
        public static final int TEXT_FIELD_NUMBER;
        private String colors_ = "";
        private String text_ = "";

        private Text() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.TextOrBuilder
        public String getText() {
            return this.text_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.TextOrBuilder
        public ByteString getTextBytes() {
            return ByteString.copyFromUtf8(this.text_);
        }

        public void setText(String str) {
            str.getClass();
            this.text_ = str;
        }

        public void clearText() {
            this.text_ = getDefaultInstance().getText();
        }

        public void setTextBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.text_ = byteString.toStringUtf8();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.TextOrBuilder
        public String getColors() {
            return this.colors_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.TextOrBuilder
        public ByteString getColorsBytes() {
            return ByteString.copyFromUtf8(this.colors_);
        }

        public void setColors(String str) {
            str.getClass();
            this.colors_ = str;
        }

        public void clearColors() {
            this.colors_ = getDefaultInstance().getColors();
        }

        public void setColorsBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.colors_ = byteString.toStringUtf8();
        }

        public static Text parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Text) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Text parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Text) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Text parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Text) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Text parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Text) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Text parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Text) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Text parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Text) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Text parseFrom(InputStream inputStream) throws IOException {
            return (Text) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Text parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Text) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Text parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Text) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Text parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Text) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Text parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Text) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Text parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Text) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Text text) {
            return DEFAULT_INSTANCE.createBuilder(text);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Text, Builder> implements TextOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Text.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.TextOrBuilder
            public String getText() {
                return ((Text) this.instance).getText();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.TextOrBuilder
            public ByteString getTextBytes() {
                return ((Text) this.instance).getTextBytes();
            }

            public Builder setText(String str) {
                copyOnWrite();
                ((Text) this.instance).setText(str);
                return this;
            }

            public Builder clearText() {
                copyOnWrite();
                ((Text) this.instance).clearText();
                return this;
            }

            public Builder setTextBytes(ByteString byteString) {
                copyOnWrite();
                ((Text) this.instance).setTextBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.TextOrBuilder
            public String getColors() {
                return ((Text) this.instance).getColors();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.TextOrBuilder
            public ByteString getColorsBytes() {
                return ((Text) this.instance).getColorsBytes();
            }

            public Builder setColors(String str) {
                copyOnWrite();
                ((Text) this.instance).setColors(str);
                return this;
            }

            public Builder clearColors() {
                copyOnWrite();
                ((Text) this.instance).clearColors();
                return this;
            }

            public Builder setColorsBytes(ByteString byteString) {
                copyOnWrite();
                ((Text) this.instance).setColorsBytes(byteString);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Text();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001Ȉ\u0002Ȉ", new Object[]{"text_", "colors_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Text> parser = PARSER;
                    if (parser == null) {
                        synchronized (Text.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Text text = new Text();
            DEFAULT_INSTANCE = text;
            GeneratedMessageLite.registerDefaultInstance(Text.class, text);
        }

        public static Text getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Text> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class Vector extends GeneratedMessageLite<Vector, Builder> implements VectorOrBuilder {
        public static final int COLORS_FIELD_NUMBER;
        private static final Vector DEFAULT_INSTANCE;
        public static final int KEY_FIELD_NUMBER;
        private static volatile Parser<Vector> PARSER;
        private String colors_ = "";
        private String key_ = "";

        private Vector() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.VectorOrBuilder
        public String getKey() {
            return this.key_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.VectorOrBuilder
        public ByteString getKeyBytes() {
            return ByteString.copyFromUtf8(this.key_);
        }

        public void setKey(String str) {
            str.getClass();
            this.key_ = str;
        }

        public void clearKey() {
            this.key_ = getDefaultInstance().getKey();
        }

        public void setKeyBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.key_ = byteString.toStringUtf8();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.VectorOrBuilder
        public String getColors() {
            return this.colors_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.VectorOrBuilder
        public ByteString getColorsBytes() {
            return ByteString.copyFromUtf8(this.colors_);
        }

        public void setColors(String str) {
            str.getClass();
            this.colors_ = str;
        }

        public void clearColors() {
            this.colors_ = getDefaultInstance().getColors();
        }

        public void setColorsBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.colors_ = byteString.toStringUtf8();
        }

        public static Vector parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Vector) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Vector parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Vector) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Vector parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Vector) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Vector parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Vector) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Vector parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Vector) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Vector parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Vector) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Vector parseFrom(InputStream inputStream) throws IOException {
            return (Vector) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Vector parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Vector) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Vector parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Vector) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Vector parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Vector) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Vector parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Vector) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Vector parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Vector) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Vector vector) {
            return DEFAULT_INSTANCE.createBuilder(vector);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Vector, Builder> implements VectorOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Vector.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.VectorOrBuilder
            public String getKey() {
                return ((Vector) this.instance).getKey();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.VectorOrBuilder
            public ByteString getKeyBytes() {
                return ((Vector) this.instance).getKeyBytes();
            }

            public Builder setKey(String str) {
                copyOnWrite();
                ((Vector) this.instance).setKey(str);
                return this;
            }

            public Builder clearKey() {
                copyOnWrite();
                ((Vector) this.instance).clearKey();
                return this;
            }

            public Builder setKeyBytes(ByteString byteString) {
                copyOnWrite();
                ((Vector) this.instance).setKeyBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.VectorOrBuilder
            public String getColors() {
                return ((Vector) this.instance).getColors();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.VectorOrBuilder
            public ByteString getColorsBytes() {
                return ((Vector) this.instance).getColorsBytes();
            }

            public Builder setColors(String str) {
                copyOnWrite();
                ((Vector) this.instance).setColors(str);
                return this;
            }

            public Builder clearColors() {
                copyOnWrite();
                ((Vector) this.instance).clearColors();
                return this;
            }

            public Builder setColorsBytes(ByteString byteString) {
                copyOnWrite();
                ((Vector) this.instance).setColorsBytes(byteString);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Vector();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001Ȉ\u0002Ȉ", new Object[]{"key_", "colors_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Vector> parser = PARSER;
                    if (parser == null) {
                        synchronized (Vector.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Vector vector = new Vector();
            DEFAULT_INSTANCE = vector;
            GeneratedMessageLite.registerDefaultInstance(Vector.class, vector);
        }

        public static Vector getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Vector> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes4.dex */
    public static final class Photo extends GeneratedMessageLite<Photo, Builder> implements PhotoOrBuilder {
        private static final Photo DEFAULT_INSTANCE;
        private static volatile Parser<Photo> PARSER;
        public static final int SIZE_FIELD_NUMBER;
        public static final int URI_FIELD_NUMBER;
        private long size_;
        private String uri_ = "";

        private Photo() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.PhotoOrBuilder
        public String getUri() {
            return this.uri_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.PhotoOrBuilder
        public ByteString getUriBytes() {
            return ByteString.copyFromUtf8(this.uri_);
        }

        public void setUri(String str) {
            str.getClass();
            this.uri_ = str;
        }

        public void clearUri() {
            this.uri_ = getDefaultInstance().getUri();
        }

        public void setUriBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.uri_ = byteString.toStringUtf8();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.PhotoOrBuilder
        public long getSize() {
            return this.size_;
        }

        public void setSize(long j) {
            this.size_ = j;
        }

        public void clearSize() {
            this.size_ = 0;
        }

        public static Photo parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Photo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Photo parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Photo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Photo parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Photo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Photo parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Photo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Photo parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Photo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Photo parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Photo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Photo parseFrom(InputStream inputStream) throws IOException {
            return (Photo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Photo parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Photo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Photo parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Photo) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Photo parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Photo) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Photo parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Photo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Photo parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Photo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Photo photo) {
            return DEFAULT_INSTANCE.createBuilder(photo);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Photo, Builder> implements PhotoOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Photo.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.PhotoOrBuilder
            public String getUri() {
                return ((Photo) this.instance).getUri();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.PhotoOrBuilder
            public ByteString getUriBytes() {
                return ((Photo) this.instance).getUriBytes();
            }

            public Builder setUri(String str) {
                copyOnWrite();
                ((Photo) this.instance).setUri(str);
                return this;
            }

            public Builder clearUri() {
                copyOnWrite();
                ((Photo) this.instance).clearUri();
                return this;
            }

            public Builder setUriBytes(ByteString byteString) {
                copyOnWrite();
                ((Photo) this.instance).setUriBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar.PhotoOrBuilder
            public long getSize() {
                return ((Photo) this.instance).getSize();
            }

            public Builder setSize(long j) {
                copyOnWrite();
                ((Photo) this.instance).setSize(j);
                return this;
            }

            public Builder clearSize() {
                copyOnWrite();
                ((Photo) this.instance).clearSize();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Photo();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001Ȉ\u0002\u0002", new Object[]{"uri_", "size_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Photo> parser = PARSER;
                    if (parser == null) {
                        synchronized (Photo.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Photo photo = new Photo();
            DEFAULT_INSTANCE = photo;
            GeneratedMessageLite.registerDefaultInstance(Photo.class, photo);
        }

        public static Photo getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Photo> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes4.dex */
    public enum AvatarCase {
        TEXT(1),
        VECTOR(2),
        PHOTO(3),
        AVATAR_NOT_SET(0);
        
        private final int value;

        AvatarCase(int i) {
            this.value = i;
        }

        @Deprecated
        public static AvatarCase valueOf(int i) {
            return forNumber(i);
        }

        public static AvatarCase forNumber(int i) {
            if (i == 0) {
                return AVATAR_NOT_SET;
            }
            if (i == 1) {
                return TEXT;
            }
            if (i == 2) {
                return VECTOR;
            }
            if (i != 3) {
                return null;
            }
            return PHOTO;
        }

        public int getNumber() {
            return this.value;
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
    public AvatarCase getAvatarCase() {
        return AvatarCase.forNumber(this.avatarCase_);
    }

    public void clearAvatar() {
        this.avatarCase_ = 0;
        this.avatar_ = null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
    public boolean hasText() {
        return this.avatarCase_ == 1;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
    public Text getText() {
        if (this.avatarCase_ == 1) {
            return (Text) this.avatar_;
        }
        return Text.getDefaultInstance();
    }

    public void setText(Text text) {
        text.getClass();
        this.avatar_ = text;
        this.avatarCase_ = 1;
    }

    public void mergeText(Text text) {
        text.getClass();
        if (this.avatarCase_ != 1 || this.avatar_ == Text.getDefaultInstance()) {
            this.avatar_ = text;
        } else {
            this.avatar_ = Text.newBuilder((Text) this.avatar_).mergeFrom((Text.Builder) text).buildPartial();
        }
        this.avatarCase_ = 1;
    }

    public void clearText() {
        if (this.avatarCase_ == 1) {
            this.avatarCase_ = 0;
            this.avatar_ = null;
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
    public boolean hasVector() {
        return this.avatarCase_ == 2;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
    public Vector getVector() {
        if (this.avatarCase_ == 2) {
            return (Vector) this.avatar_;
        }
        return Vector.getDefaultInstance();
    }

    public void setVector(Vector vector) {
        vector.getClass();
        this.avatar_ = vector;
        this.avatarCase_ = 2;
    }

    public void mergeVector(Vector vector) {
        vector.getClass();
        if (this.avatarCase_ != 2 || this.avatar_ == Vector.getDefaultInstance()) {
            this.avatar_ = vector;
        } else {
            this.avatar_ = Vector.newBuilder((Vector) this.avatar_).mergeFrom((Vector.Builder) vector).buildPartial();
        }
        this.avatarCase_ = 2;
    }

    public void clearVector() {
        if (this.avatarCase_ == 2) {
            this.avatarCase_ = 0;
            this.avatar_ = null;
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
    public boolean hasPhoto() {
        return this.avatarCase_ == 3;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
    public Photo getPhoto() {
        if (this.avatarCase_ == 3) {
            return (Photo) this.avatar_;
        }
        return Photo.getDefaultInstance();
    }

    public void setPhoto(Photo photo) {
        photo.getClass();
        this.avatar_ = photo;
        this.avatarCase_ = 3;
    }

    public void mergePhoto(Photo photo) {
        photo.getClass();
        if (this.avatarCase_ != 3 || this.avatar_ == Photo.getDefaultInstance()) {
            this.avatar_ = photo;
        } else {
            this.avatar_ = Photo.newBuilder((Photo) this.avatar_).mergeFrom((Photo.Builder) photo).buildPartial();
        }
        this.avatarCase_ = 3;
    }

    public void clearPhoto() {
        if (this.avatarCase_ == 3) {
            this.avatarCase_ = 0;
            this.avatar_ = null;
        }
    }

    public static CustomAvatar parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (CustomAvatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static CustomAvatar parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (CustomAvatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static CustomAvatar parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (CustomAvatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static CustomAvatar parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (CustomAvatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static CustomAvatar parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (CustomAvatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static CustomAvatar parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (CustomAvatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static CustomAvatar parseFrom(InputStream inputStream) throws IOException {
        return (CustomAvatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static CustomAvatar parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (CustomAvatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static CustomAvatar parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (CustomAvatar) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static CustomAvatar parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (CustomAvatar) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static CustomAvatar parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (CustomAvatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static CustomAvatar parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (CustomAvatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(CustomAvatar customAvatar) {
        return DEFAULT_INSTANCE.createBuilder(customAvatar);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<CustomAvatar, Builder> implements CustomAvatarOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(CustomAvatar.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
        public AvatarCase getAvatarCase() {
            return ((CustomAvatar) this.instance).getAvatarCase();
        }

        public Builder clearAvatar() {
            copyOnWrite();
            ((CustomAvatar) this.instance).clearAvatar();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
        public boolean hasText() {
            return ((CustomAvatar) this.instance).hasText();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
        public Text getText() {
            return ((CustomAvatar) this.instance).getText();
        }

        public Builder setText(Text text) {
            copyOnWrite();
            ((CustomAvatar) this.instance).setText(text);
            return this;
        }

        public Builder setText(Text.Builder builder) {
            copyOnWrite();
            ((CustomAvatar) this.instance).setText(builder.build());
            return this;
        }

        public Builder mergeText(Text text) {
            copyOnWrite();
            ((CustomAvatar) this.instance).mergeText(text);
            return this;
        }

        public Builder clearText() {
            copyOnWrite();
            ((CustomAvatar) this.instance).clearText();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
        public boolean hasVector() {
            return ((CustomAvatar) this.instance).hasVector();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
        public Vector getVector() {
            return ((CustomAvatar) this.instance).getVector();
        }

        public Builder setVector(Vector vector) {
            copyOnWrite();
            ((CustomAvatar) this.instance).setVector(vector);
            return this;
        }

        public Builder setVector(Vector.Builder builder) {
            copyOnWrite();
            ((CustomAvatar) this.instance).setVector(builder.build());
            return this;
        }

        public Builder mergeVector(Vector vector) {
            copyOnWrite();
            ((CustomAvatar) this.instance).mergeVector(vector);
            return this;
        }

        public Builder clearVector() {
            copyOnWrite();
            ((CustomAvatar) this.instance).clearVector();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
        public boolean hasPhoto() {
            return ((CustomAvatar) this.instance).hasPhoto();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatarOrBuilder
        public Photo getPhoto() {
            return ((CustomAvatar) this.instance).getPhoto();
        }

        public Builder setPhoto(Photo photo) {
            copyOnWrite();
            ((CustomAvatar) this.instance).setPhoto(photo);
            return this;
        }

        public Builder setPhoto(Photo.Builder builder) {
            copyOnWrite();
            ((CustomAvatar) this.instance).setPhoto(builder.build());
            return this;
        }

        public Builder mergePhoto(Photo photo) {
            copyOnWrite();
            ((CustomAvatar) this.instance).mergePhoto(photo);
            return this;
        }

        public Builder clearPhoto() {
            copyOnWrite();
            ((CustomAvatar) this.instance).clearPhoto();
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new CustomAvatar();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0001\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001<\u0000\u0002<\u0000\u0003<\u0000", new Object[]{"avatar_", "avatarCase_", Text.class, Vector.class, Photo.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<CustomAvatar> parser = PARSER;
                if (parser == null) {
                    synchronized (CustomAvatar.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        CustomAvatar customAvatar = new CustomAvatar();
        DEFAULT_INSTANCE = customAvatar;
        GeneratedMessageLite.registerDefaultInstance(CustomAvatar.class, customAvatar);
    }

    public static CustomAvatar getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<CustomAvatar> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
