package org.thoughtcrime.securesms.database.model;

import java.util.concurrent.Callable;
import org.thoughtcrime.securesms.database.model.StoryViewState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoryViewState$Companion$$ExternalSyntheticLambda0 implements Callable {
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return StoryViewState.Companion.m1743getForRecipientId$lambda0();
    }
}
