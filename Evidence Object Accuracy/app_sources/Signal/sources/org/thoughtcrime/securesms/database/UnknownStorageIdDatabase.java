package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;
import org.whispersystems.signalservice.api.storage.StorageId;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public class UnknownStorageIdDatabase extends Database {
    public static final String[] CREATE_INDEXES = {"CREATE INDEX IF NOT EXISTS storage_key_type_index ON storage_key (type);"};
    public static final String CREATE_TABLE;
    private static final String ID;
    private static final String STORAGE_ID;
    private static final String TABLE_NAME;
    private static final String TYPE;

    public UnknownStorageIdDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public List<StorageId> getAllUnknownIds() {
        ArrayList arrayList = new ArrayList();
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, null, null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                String requireString = CursorUtil.requireString(query, STORAGE_ID);
                try {
                    arrayList.add(StorageId.forType(Base64.decode(requireString), CursorUtil.requireInt(query, "type")));
                } catch (IOException e) {
                    throw new AssertionError(e);
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return arrayList;
    }

    public List<StorageId> getAllWithTypes(List<Integer> list) {
        ArrayList arrayList = new ArrayList();
        SqlUtil.Query buildSingleCollectionQuery = SqlUtil.buildSingleCollectionQuery("type", list);
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, buildSingleCollectionQuery.getWhere(), buildSingleCollectionQuery.getWhereArgs(), null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                String requireString = CursorUtil.requireString(query, STORAGE_ID);
                try {
                    arrayList.add(StorageId.forType(Base64.decode(requireString), CursorUtil.requireInt(query, "type")));
                } catch (IOException e) {
                    throw new AssertionError(e);
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return arrayList;
    }

    public SignalStorageRecord getById(byte[] bArr) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "key = ?", new String[]{Base64.encodeBytes(bArr)}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    SignalStorageRecord forUnknown = SignalStorageRecord.forUnknown(StorageId.forType(bArr, CursorUtil.requireInt(query, "type")));
                    query.close();
                    return forUnknown;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return null;
    }

    public void insert(Collection<SignalStorageRecord> collection) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        Preconditions.checkArgument(signalWritableDatabase.inTransaction(), "Must be in a transaction!");
        for (SignalStorageRecord signalStorageRecord : collection) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("type", Integer.valueOf(signalStorageRecord.getType()));
            contentValues.put(STORAGE_ID, Base64.encodeBytes(signalStorageRecord.getId().getRaw()));
            signalWritableDatabase.insert(TABLE_NAME, (String) null, contentValues);
        }
    }

    public void delete(Collection<StorageId> collection) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        Preconditions.checkArgument(signalWritableDatabase.inTransaction(), "Must be in a transaction!");
        Iterator<StorageId> it = collection.iterator();
        while (it.hasNext()) {
            signalWritableDatabase.delete(TABLE_NAME, "key = ?", SqlUtil.buildArgs(Base64.encodeBytes(it.next().getRaw())));
        }
    }

    public void deleteAll() {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, (String) null, (String[]) null);
    }
}
