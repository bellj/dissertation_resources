package org.thoughtcrime.securesms.database.model;

import android.text.TextUtils;
import j$.util.Optional;
import java.util.Objects;

/* loaded from: classes4.dex */
public final class StickerPackRecord {
    private final Optional<String> author;
    private final StickerRecord cover;
    private final boolean installed;
    private final String packId;
    private final String packKey;
    private final Optional<String> title;

    public StickerPackRecord(String str, String str2, String str3, String str4, StickerRecord stickerRecord, boolean z) {
        this.packId = str;
        this.packKey = str2;
        this.title = TextUtils.isEmpty(str3) ? Optional.empty() : Optional.of(str3);
        this.author = TextUtils.isEmpty(str4) ? Optional.empty() : Optional.of(str4);
        this.cover = stickerRecord;
        this.installed = z;
    }

    public String getPackId() {
        return this.packId;
    }

    public String getPackKey() {
        return this.packKey;
    }

    public Optional<String> getTitle() {
        return this.title;
    }

    public Optional<String> getAuthor() {
        return this.author;
    }

    public StickerRecord getCover() {
        return this.cover;
    }

    public boolean isInstalled() {
        return this.installed;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || StickerPackRecord.class != obj.getClass()) {
            return false;
        }
        StickerPackRecord stickerPackRecord = (StickerPackRecord) obj;
        if (this.installed != stickerPackRecord.installed || !this.packId.equals(stickerPackRecord.packId) || !this.packKey.equals(stickerPackRecord.packKey) || !this.title.equals(stickerPackRecord.title) || !this.author.equals(stickerPackRecord.author) || !this.cover.equals(stickerPackRecord.cover)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.packId, this.packKey, this.title, this.author, this.cover, Boolean.valueOf(this.installed));
    }
}
