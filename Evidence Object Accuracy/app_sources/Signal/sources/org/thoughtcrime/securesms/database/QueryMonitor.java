package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import kotlin.Metadata;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: QueryMonitor.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J/\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\bH&¢\u0006\u0002\u0010\tJo\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\b2\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\b2\b\u0010\u000e\u001a\u0004\u0018\u00010\u00052\b\u0010\u000f\u001a\u0004\u0018\u00010\u00052\b\u0010\u0010\u001a\u0004\u0018\u00010\u00052\b\u0010\u0011\u001a\u0004\u0018\u00010\u0005H&¢\u0006\u0002\u0010\u0012J%\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00052\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\bH&¢\u0006\u0002\u0010\u0015J7\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u00182\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\bH&¢\u0006\u0002\u0010\u0019¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/database/QueryMonitor;", "", "onDelete", "", "table", "", "selection", MultiselectForwardFragment.ARGS, "", "(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V", "onQuery", "distinct", "", "projection", "groupBy", "having", "orderBy", "limit", "(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "onSql", "sql", "(Ljava/lang/String;[Ljava/lang/Object;)V", "onUpdate", "values", "Landroid/content/ContentValues;", "(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface QueryMonitor {
    void onDelete(String str, String str2, Object[] objArr);

    void onQuery(boolean z, String str, String[] strArr, String str2, Object[] objArr, String str3, String str4, String str5, String str6);

    void onSql(String str, Object[] objArr);

    void onUpdate(String str, ContentValues contentValues, String str2, Object[] objArr);
}
