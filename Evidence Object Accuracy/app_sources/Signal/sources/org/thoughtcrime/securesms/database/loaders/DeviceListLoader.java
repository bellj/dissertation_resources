package org.thoughtcrime.securesms.database.loaders;

import android.content.Context;
import android.text.TextUtils;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.thoughtcrime.securesms.devicelist.Device;
import org.thoughtcrime.securesms.devicelist.DeviceNameProtos;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.AsyncLoader;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceInfo;

/* loaded from: classes4.dex */
public class DeviceListLoader extends AsyncLoader<List<Device>> {
    private static final String TAG = Log.tag(DeviceListLoader.class);
    private final SignalServiceAccountManager accountManager;

    public DeviceListLoader(Context context, SignalServiceAccountManager signalServiceAccountManager) {
        super(context);
        this.accountManager = signalServiceAccountManager;
    }

    @Override // androidx.loader.content.AsyncTaskLoader
    public List<Device> loadInBackground() {
        try {
            List<Device> list = Stream.of(this.accountManager.getDevices()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.loaders.DeviceListLoader$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return DeviceListLoader.lambda$loadInBackground$0((DeviceInfo) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.database.loaders.DeviceListLoader$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return DeviceListLoader.this.mapToDevice((DeviceInfo) obj);
                }
            }).toList();
            Collections.sort(list, new DeviceComparator());
            return list;
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    public static /* synthetic */ boolean lambda$loadInBackground$0(DeviceInfo deviceInfo) {
        return deviceInfo.getId() != 1;
    }

    public Device mapToDevice(DeviceInfo deviceInfo) {
        Throwable e;
        try {
            if (TextUtils.isEmpty(deviceInfo.getName()) || deviceInfo.getName().length() < 4) {
                throw new IOException("Invalid DeviceInfo name.");
            }
            DeviceNameProtos.DeviceName parseFrom = DeviceNameProtos.DeviceName.parseFrom(Base64.decode(deviceInfo.getName()));
            if (parseFrom.hasCiphertext() && parseFrom.hasEphemeralPublic() && parseFrom.hasSyntheticIv()) {
                return new Device(deviceInfo.getId(), new String(decryptName(parseFrom, SignalStore.account().getAciIdentityKey())), deviceInfo.getCreated(), deviceInfo.getLastSeen());
            }
            throw new IOException("Got a DeviceName that wasn't properly populated.");
        } catch (IOException e2) {
            Log.w(TAG, "Failed while reading the protobuf.", e2);
            return new Device(deviceInfo.getId(), deviceInfo.getName(), deviceInfo.getCreated(), deviceInfo.getLastSeen());
        } catch (GeneralSecurityException e3) {
            e = e3;
            Log.w(TAG, "Failed during decryption.", e);
            return new Device(deviceInfo.getId(), deviceInfo.getName(), deviceInfo.getCreated(), deviceInfo.getLastSeen());
        } catch (InvalidKeyException e4) {
            e = e4;
            Log.w(TAG, "Failed during decryption.", e);
            return new Device(deviceInfo.getId(), deviceInfo.getName(), deviceInfo.getCreated(), deviceInfo.getLastSeen());
        }
    }

    public static byte[] decryptName(DeviceNameProtos.DeviceName deviceName, IdentityKeyPair identityKeyPair) throws InvalidKeyException, GeneralSecurityException {
        byte[] byteArray = deviceName.getSyntheticIv().toByteArray();
        byte[] byteArray2 = deviceName.getCiphertext().toByteArray();
        byte[] calculateAgreement = Curve.calculateAgreement(Curve.decodePoint(deviceName.getEphemeralPublic().toByteArray(), 0), identityKeyPair.getPrivateKey());
        Mac instance = Mac.getInstance("HmacSHA256");
        instance.init(new SecretKeySpec(calculateAgreement, "HmacSHA256"));
        instance.init(new SecretKeySpec(instance.doFinal("cipher".getBytes()), "HmacSHA256"));
        byte[] doFinal = instance.doFinal(byteArray);
        Cipher instance2 = Cipher.getInstance("AES/CTR/NoPadding");
        instance2.init(2, new SecretKeySpec(doFinal, "AES"), new IvParameterSpec(new byte[16]));
        byte[] doFinal2 = instance2.doFinal(byteArray2);
        instance.init(new SecretKeySpec(calculateAgreement, "HmacSHA256"));
        instance.init(new SecretKeySpec(instance.doFinal("auth".getBytes()), "HmacSHA256"));
        if (MessageDigest.isEqual(ByteUtil.trim(instance.doFinal(doFinal2), 16), byteArray)) {
            return doFinal2;
        }
        throw new GeneralSecurityException("The computed syntheticIv didn't match the actual syntheticIv.");
    }

    /* loaded from: classes4.dex */
    public static class DeviceComparator implements Comparator<Device> {
        private DeviceComparator() {
        }

        public int compare(Device device, Device device2) {
            if (device.getCreated() < device2.getCreated()) {
                return -1;
            }
            return device.getCreated() != device2.getCreated() ? 1 : 0;
        }
    }
}
