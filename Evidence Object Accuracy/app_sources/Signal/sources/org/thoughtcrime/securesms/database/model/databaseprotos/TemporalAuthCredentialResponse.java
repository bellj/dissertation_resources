package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes4.dex */
public final class TemporalAuthCredentialResponse extends GeneratedMessageLite<TemporalAuthCredentialResponse, Builder> implements TemporalAuthCredentialResponseOrBuilder {
    public static final int AUTHCREDENTIALRESPONSE_FIELD_NUMBER;
    public static final int DATE_FIELD_NUMBER;
    private static final TemporalAuthCredentialResponse DEFAULT_INSTANCE;
    private static volatile Parser<TemporalAuthCredentialResponse> PARSER;
    private ByteString authCredentialResponse_ = ByteString.EMPTY;
    private long date_;

    private TemporalAuthCredentialResponse() {
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponseOrBuilder
    public long getDate() {
        return this.date_;
    }

    public void setDate(long j) {
        this.date_ = j;
    }

    public void clearDate() {
        this.date_ = 0;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponseOrBuilder
    public ByteString getAuthCredentialResponse() {
        return this.authCredentialResponse_;
    }

    public void setAuthCredentialResponse(ByteString byteString) {
        byteString.getClass();
        this.authCredentialResponse_ = byteString;
    }

    public void clearAuthCredentialResponse() {
        this.authCredentialResponse_ = getDefaultInstance().getAuthCredentialResponse();
    }

    public static TemporalAuthCredentialResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static TemporalAuthCredentialResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static TemporalAuthCredentialResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static TemporalAuthCredentialResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static TemporalAuthCredentialResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static TemporalAuthCredentialResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static TemporalAuthCredentialResponse parseFrom(InputStream inputStream) throws IOException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TemporalAuthCredentialResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TemporalAuthCredentialResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TemporalAuthCredentialResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TemporalAuthCredentialResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static TemporalAuthCredentialResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TemporalAuthCredentialResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(TemporalAuthCredentialResponse temporalAuthCredentialResponse) {
        return DEFAULT_INSTANCE.createBuilder(temporalAuthCredentialResponse);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<TemporalAuthCredentialResponse, Builder> implements TemporalAuthCredentialResponseOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(TemporalAuthCredentialResponse.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponseOrBuilder
        public long getDate() {
            return ((TemporalAuthCredentialResponse) this.instance).getDate();
        }

        public Builder setDate(long j) {
            copyOnWrite();
            ((TemporalAuthCredentialResponse) this.instance).setDate(j);
            return this;
        }

        public Builder clearDate() {
            copyOnWrite();
            ((TemporalAuthCredentialResponse) this.instance).clearDate();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponseOrBuilder
        public ByteString getAuthCredentialResponse() {
            return ((TemporalAuthCredentialResponse) this.instance).getAuthCredentialResponse();
        }

        public Builder setAuthCredentialResponse(ByteString byteString) {
            copyOnWrite();
            ((TemporalAuthCredentialResponse) this.instance).setAuthCredentialResponse(byteString);
            return this;
        }

        public Builder clearAuthCredentialResponse() {
            copyOnWrite();
            ((TemporalAuthCredentialResponse) this.instance).clearAuthCredentialResponse();
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponse$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new TemporalAuthCredentialResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0002\u0002\n", new Object[]{"date_", "authCredentialResponse_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<TemporalAuthCredentialResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (TemporalAuthCredentialResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        TemporalAuthCredentialResponse temporalAuthCredentialResponse = new TemporalAuthCredentialResponse();
        DEFAULT_INSTANCE = temporalAuthCredentialResponse;
        GeneratedMessageLite.registerDefaultInstance(TemporalAuthCredentialResponse.class, temporalAuthCredentialResponse);
    }

    public static TemporalAuthCredentialResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<TemporalAuthCredentialResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
