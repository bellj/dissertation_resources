package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;

/* loaded from: classes4.dex */
public interface BodyRangeListOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    BodyRangeList.BodyRange getRanges(int i);

    int getRangesCount();

    List<BodyRangeList.BodyRange> getRangesList();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
