package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails;

/* loaded from: classes4.dex */
public interface ProfileChangeDetailsOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    ProfileChangeDetails.StringChange getProfileNameChange();

    boolean hasProfileNameChange();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
