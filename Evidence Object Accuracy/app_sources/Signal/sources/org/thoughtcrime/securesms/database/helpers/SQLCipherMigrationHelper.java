package org.thoughtcrime.securesms.database.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Pair;
import com.annimon.stream.function.BiFunction;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.gifts.Gifts;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;
import org.thoughtcrime.securesms.crypto.AsymmetricMasterCipher;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.crypto.MasterCipher;
import org.thoughtcrime.securesms.crypto.MasterSecret;
import org.thoughtcrime.securesms.crypto.MasterSecretUtil;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet;
import org.thoughtcrime.securesms.migrations.LegacyMigrationJob;
import org.thoughtcrime.securesms.service.GenericForegroundService;
import org.thoughtcrime.securesms.service.webrtc.SignalCallManager;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class SQLCipherMigrationHelper {
    private static final long ENCRYPTION_ASYMMETRIC_BIT;
    private static final long ENCRYPTION_SYMMETRIC_BIT;
    private static final String TAG = Log.tag(SQLCipherMigrationHelper.class);

    public static void migratePlaintext(Context context, SQLiteDatabase sQLiteDatabase, net.zetetic.database.sqlcipher.SQLiteDatabase sQLiteDatabase2) {
        sQLiteDatabase2.beginTransaction();
        int id = GenericForegroundService.startForegroundTask(context, context.getString(R.string.SQLCipherMigrationHelper_migrating_signal_database)).getId();
        try {
            copyTable("identities", sQLiteDatabase, sQLiteDatabase2, null);
            copyTable("push", sQLiteDatabase, sQLiteDatabase2, null);
            copyTable(ChooseGroupStoryBottomSheet.RESULT_SET, sQLiteDatabase, sQLiteDatabase2, null);
            copyTable("recipient_preferences", sQLiteDatabase, sQLiteDatabase2, null);
            copyTable(GroupReceiptDatabase.TABLE_NAME, sQLiteDatabase, sQLiteDatabase2, null);
            sQLiteDatabase2.setTransactionSuccessful();
        } finally {
            sQLiteDatabase2.endTransaction();
            GenericForegroundService.stopForegroundTask(context, id);
        }
    }

    public static void migrateCiphertext(Context context, MasterSecret masterSecret, SQLiteDatabase sQLiteDatabase, net.zetetic.database.sqlcipher.SQLiteDatabase sQLiteDatabase2, LegacyMigrationJob.DatabaseUpgradeListener databaseUpgradeListener) {
        MasterCipher masterCipher = new MasterCipher(masterSecret);
        AsymmetricMasterCipher asymmetricMasterCipher = new AsymmetricMasterCipher(MasterSecretUtil.getAsymmetricMasterSecret(context, masterSecret));
        sQLiteDatabase2.beginTransaction();
        int id = GenericForegroundService.startForegroundTask(context, context.getString(R.string.SQLCipherMigrationHelper_migrating_signal_database)).getId();
        try {
            copyTable("sms", sQLiteDatabase, sQLiteDatabase2, new BiFunction(asymmetricMasterCipher, databaseUpgradeListener, WebRtcCallView.FADE_OUT_DELAY) { // from class: org.thoughtcrime.securesms.database.helpers.SQLCipherMigrationHelper$$ExternalSyntheticLambda0
                public final /* synthetic */ AsymmetricMasterCipher f$1;
                public final /* synthetic */ LegacyMigrationJob.DatabaseUpgradeListener f$2;
                public final /* synthetic */ int f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // com.annimon.stream.function.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return SQLCipherMigrationHelper.lambda$migrateCiphertext$0(MasterCipher.this, this.f$1, this.f$2, this.f$3, (ContentValues) obj, (Pair) obj2);
                }
            });
            copyTable("mms", sQLiteDatabase, sQLiteDatabase2, new BiFunction(asymmetricMasterCipher, databaseUpgradeListener, WebRtcCallView.FADE_OUT_DELAY) { // from class: org.thoughtcrime.securesms.database.helpers.SQLCipherMigrationHelper$$ExternalSyntheticLambda1
                public final /* synthetic */ AsymmetricMasterCipher f$1;
                public final /* synthetic */ LegacyMigrationJob.DatabaseUpgradeListener f$2;
                public final /* synthetic */ int f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // com.annimon.stream.function.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return SQLCipherMigrationHelper.lambda$migrateCiphertext$1(MasterCipher.this, this.f$1, this.f$2, this.f$3, (ContentValues) obj, (Pair) obj2);
                }
            });
            copyTable(AttachmentDatabase.TABLE_NAME, sQLiteDatabase, sQLiteDatabase2, new BiFunction(asymmetricMasterCipher, databaseUpgradeListener, WebRtcCallView.FADE_OUT_DELAY) { // from class: org.thoughtcrime.securesms.database.helpers.SQLCipherMigrationHelper$$ExternalSyntheticLambda2
                public final /* synthetic */ AsymmetricMasterCipher f$1;
                public final /* synthetic */ LegacyMigrationJob.DatabaseUpgradeListener f$2;
                public final /* synthetic */ int f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // com.annimon.stream.function.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return SQLCipherMigrationHelper.lambda$migrateCiphertext$2(MasterCipher.this, this.f$1, this.f$2, this.f$3, (ContentValues) obj, (Pair) obj2);
                }
            });
            copyTable(ThreadDatabase.TABLE_NAME, sQLiteDatabase, sQLiteDatabase2, new BiFunction(asymmetricMasterCipher, databaseUpgradeListener, WebRtcCallView.FADE_OUT_DELAY) { // from class: org.thoughtcrime.securesms.database.helpers.SQLCipherMigrationHelper$$ExternalSyntheticLambda3
                public final /* synthetic */ AsymmetricMasterCipher f$1;
                public final /* synthetic */ LegacyMigrationJob.DatabaseUpgradeListener f$2;
                public final /* synthetic */ int f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // com.annimon.stream.function.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return SQLCipherMigrationHelper.lambda$migrateCiphertext$3(MasterCipher.this, this.f$1, this.f$2, this.f$3, (ContentValues) obj, (Pair) obj2);
                }
            });
            copyTable("drafts", sQLiteDatabase, sQLiteDatabase2, new BiFunction(databaseUpgradeListener, WebRtcCallView.FADE_OUT_DELAY) { // from class: org.thoughtcrime.securesms.database.helpers.SQLCipherMigrationHelper$$ExternalSyntheticLambda4
                public final /* synthetic */ LegacyMigrationJob.DatabaseUpgradeListener f$1;
                public final /* synthetic */ int f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // com.annimon.stream.function.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return SQLCipherMigrationHelper.lambda$migrateCiphertext$4(MasterCipher.this, this.f$1, this.f$2, (ContentValues) obj, (Pair) obj2);
                }
            });
            AttachmentSecretProvider.getInstance(context).setClassicKey(context, masterSecret.getEncryptionKey().getEncoded(), masterSecret.getMacKey().getEncoded());
            TextSecurePreferences.setNeedsSqlCipherMigration(context, false);
            sQLiteDatabase2.setTransactionSuccessful();
        } finally {
            sQLiteDatabase2.endTransaction();
            GenericForegroundService.stopForegroundTask(context, id);
        }
    }

    public static /* synthetic */ ContentValues lambda$migrateCiphertext$0(MasterCipher masterCipher, AsymmetricMasterCipher asymmetricMasterCipher, LegacyMigrationJob.DatabaseUpgradeListener databaseUpgradeListener, int i, ContentValues contentValues, Pair pair) {
        Pair<Long, String> plaintextBody = getPlaintextBody(masterCipher, asymmetricMasterCipher, contentValues.getAsLong("type").longValue(), contentValues.getAsString("body"));
        contentValues.put("body", (String) plaintextBody.second);
        contentValues.put("type", (Long) plaintextBody.first);
        if (databaseUpgradeListener != null && ((Integer) pair.first).intValue() % MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH == 0) {
            databaseUpgradeListener.setProgress(getTotalProgress(0, ((Integer) pair.first).intValue(), ((Integer) pair.second).intValue()), i);
        }
        return contentValues;
    }

    public static /* synthetic */ ContentValues lambda$migrateCiphertext$1(MasterCipher masterCipher, AsymmetricMasterCipher asymmetricMasterCipher, LegacyMigrationJob.DatabaseUpgradeListener databaseUpgradeListener, int i, ContentValues contentValues, Pair pair) {
        Pair<Long, String> plaintextBody = getPlaintextBody(masterCipher, asymmetricMasterCipher, contentValues.getAsLong(MmsDatabase.MESSAGE_BOX).longValue(), contentValues.getAsString("body"));
        contentValues.put("body", (String) plaintextBody.second);
        contentValues.put(MmsDatabase.MESSAGE_BOX, (Long) plaintextBody.first);
        if (databaseUpgradeListener != null && ((Integer) pair.first).intValue() % MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH == 0) {
            databaseUpgradeListener.setProgress(getTotalProgress(MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH, ((Integer) pair.first).intValue(), ((Integer) pair.second).intValue()), i);
        }
        return contentValues;
    }

    public static /* synthetic */ ContentValues lambda$migrateCiphertext$2(MasterCipher masterCipher, AsymmetricMasterCipher asymmetricMasterCipher, LegacyMigrationJob.DatabaseUpgradeListener databaseUpgradeListener, int i, ContentValues contentValues, Pair pair) {
        byte[] bArr;
        String asString = contentValues.getAsString("file_name");
        String asString2 = contentValues.getAsString("cd");
        try {
            if (!TextUtils.isEmpty(asString)) {
                contentValues.put("file_name", masterCipher.decryptBody(asString));
            }
        } catch (InvalidMessageException e) {
            Log.w(TAG, e);
        }
        try {
            if (!TextUtils.isEmpty(asString2)) {
                if (asString2.startsWith("?ASYNC-")) {
                    bArr = asymmetricMasterCipher.decryptBytes(Base64.decode(asString2.substring(7)));
                } else {
                    bArr = masterCipher.decryptBytes(Base64.decode(asString2));
                }
                contentValues.put("cd", Base64.encodeBytes(bArr));
            }
        } catch (IOException | InvalidMessageException e2) {
            Log.w(TAG, e2);
        }
        if (databaseUpgradeListener != null && ((Integer) pair.first).intValue() % MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH == 0) {
            databaseUpgradeListener.setProgress(getTotalProgress(SignalCallManager.BUSY_TONE_LENGTH, ((Integer) pair.first).intValue(), ((Integer) pair.second).intValue()), i);
        }
        return contentValues;
    }

    public static /* synthetic */ ContentValues lambda$migrateCiphertext$3(MasterCipher masterCipher, AsymmetricMasterCipher asymmetricMasterCipher, LegacyMigrationJob.DatabaseUpgradeListener databaseUpgradeListener, int i, ContentValues contentValues, Pair pair) {
        Long asLong = contentValues.getAsLong(ThreadDatabase.SNIPPET_TYPE);
        if (asLong == null) {
            asLong = 0L;
        }
        Pair<Long, String> plaintextBody = getPlaintextBody(masterCipher, asymmetricMasterCipher, asLong.longValue(), contentValues.getAsString("snippet"));
        contentValues.put("snippet", (String) plaintextBody.second);
        contentValues.put(ThreadDatabase.SNIPPET_TYPE, (Long) plaintextBody.first);
        if (databaseUpgradeListener != null && ((Integer) pair.first).intValue() % MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH == 0) {
            databaseUpgradeListener.setProgress(getTotalProgress(Gifts.GOOGLE_PAY_REQUEST_CODE, ((Integer) pair.first).intValue(), ((Integer) pair.second).intValue()), i);
        }
        return contentValues;
    }

    public static /* synthetic */ ContentValues lambda$migrateCiphertext$4(MasterCipher masterCipher, LegacyMigrationJob.DatabaseUpgradeListener databaseUpgradeListener, int i, ContentValues contentValues, Pair pair) {
        String asString = contentValues.getAsString("type");
        String asString2 = contentValues.getAsString(DraftDatabase.DRAFT_VALUE);
        try {
            if (!TextUtils.isEmpty(asString)) {
                contentValues.put("type", masterCipher.decryptBody(asString));
            }
            if (!TextUtils.isEmpty(asString2)) {
                contentValues.put(DraftDatabase.DRAFT_VALUE, masterCipher.decryptBody(asString2));
            }
        } catch (InvalidMessageException e) {
            Log.w(TAG, e);
        }
        if (databaseUpgradeListener != null && ((Integer) pair.first).intValue() % MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH == 0) {
            databaseUpgradeListener.setProgress(getTotalProgress(4000, ((Integer) pair.first).intValue(), ((Integer) pair.second).intValue()), i);
        }
        return contentValues;
    }

    private static void copyTable(String str, SQLiteDatabase sQLiteDatabase, net.zetetic.database.sqlcipher.SQLiteDatabase sQLiteDatabase2, BiFunction<ContentValues, Pair<Integer, Integer>, ContentValues> biFunction) {
        int count;
        Set<String> tableColumns = getTableColumns(str, sQLiteDatabase2);
        Cursor query = sQLiteDatabase.query(str, null, null, null, null, null, null);
        if (query != null) {
            try {
                count = query.getCount();
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        } else {
            count = 0;
        }
        int i = 1;
        while (query != null) {
            if (!query.moveToNext()) {
                break;
            }
            ContentValues contentValues = new ContentValues();
            for (int i2 = 0; i2 < query.getColumnCount(); i2++) {
                String columnName = query.getColumnName(i2);
                if (tableColumns.contains(columnName)) {
                    int type = query.getType(i2);
                    if (type == 1) {
                        contentValues.put(columnName, Long.valueOf(query.getLong(i2)));
                    } else if (type == 2) {
                        contentValues.put(columnName, Float.valueOf(query.getFloat(i2)));
                    } else if (type == 3) {
                        contentValues.put(columnName, query.getString(i2));
                    } else if (type == 4) {
                        contentValues.put(columnName, query.getBlob(i2));
                    }
                }
            }
            if (biFunction != null) {
                i++;
                contentValues = biFunction.apply(contentValues, new Pair<>(Integer.valueOf(i), Integer.valueOf(count)));
            }
            sQLiteDatabase2.insert(str, (String) null, contentValues);
        }
        if (query != null) {
            query.close();
        }
    }

    private static Pair<Long, String> getPlaintextBody(MasterCipher masterCipher, AsymmetricMasterCipher asymmetricMasterCipher, long j, String str) {
        String decryptBody;
        try {
            if (!TextUtils.isEmpty(str)) {
                if ((ENCRYPTION_SYMMETRIC_BIT & j) != 0) {
                    decryptBody = masterCipher.decryptBody(str);
                } else if ((ENCRYPTION_ASYMMETRIC_BIT & j) != 0) {
                    decryptBody = asymmetricMasterCipher.decryptBody(str);
                }
                str = decryptBody;
            }
        } catch (IOException | InvalidMessageException e) {
            Log.w(TAG, e);
        }
        return new Pair<>(Long.valueOf(2147483647L & j & -1073741825), str);
    }

    private static Set<String> getTableColumns(String str, net.zetetic.database.sqlcipher.SQLiteDatabase sQLiteDatabase) {
        HashSet hashSet = new HashSet();
        Cursor rawQuery = sQLiteDatabase.rawQuery("PRAGMA table_info(" + str + ")", (String[]) null);
        while (rawQuery != null) {
            try {
                if (!rawQuery.moveToNext()) {
                    break;
                }
                hashSet.add(rawQuery.getString(1));
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        return hashSet;
    }

    private static int getTotalProgress(int i, int i2, int i3) {
        double d = (double) i2;
        double d2 = (double) i3;
        Double.isNaN(d);
        Double.isNaN(d2);
        return i + ((int) ((d / d2) * 1000.0d));
    }
}
