package org.thoughtcrime.securesms.database.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsKt;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* compiled from: MessageId.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\u0013\u0010\u0010\u001a\u00020\u00052\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012HÖ\u0003J\t\u0010\u0013\u001a\u00020\u000fHÖ\u0001J\u0006\u0010\u0014\u001a\u00020\u0015J\t\u0010\u0016\u001a\u00020\u0015HÖ\u0001J\u0019\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0004\u001a\u00020\u00058\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/MessageId;", "Landroid/os/Parcelable;", ContactRepository.ID_COLUMN, "", "mms", "", "(JZ)V", "getId", "()J", "isMms", "()Z", "component1", "component2", "copy", "describeContents", "", "equals", "other", "", "hashCode", "serialize", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageId implements Parcelable {
    public static final Parcelable.Creator<MessageId> CREATOR = new Creator();
    public static final Companion Companion = new Companion(null);
    private final long id;
    private final boolean mms;

    /* compiled from: MessageId.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Creator implements Parcelable.Creator<MessageId> {
        @Override // android.os.Parcelable.Creator
        public final MessageId createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            return new MessageId(parcel.readLong(), parcel.readInt() != 0);
        }

        @Override // android.os.Parcelable.Creator
        public final MessageId[] newArray(int i) {
            return new MessageId[i];
        }
    }

    public static /* synthetic */ MessageId copy$default(MessageId messageId, long j, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            j = messageId.id;
        }
        if ((i & 2) != 0) {
            z = messageId.mms;
        }
        return messageId.copy(j, z);
    }

    @JvmStatic
    public static final MessageId deserialize(String str) {
        return Companion.deserialize(str);
    }

    @JvmStatic
    public static final MessageId fromNullable(long j, boolean z) {
        return Companion.fromNullable(j, z);
    }

    public final long component1() {
        return this.id;
    }

    public final boolean component2() {
        return this.mms;
    }

    public final MessageId copy(long j, boolean z) {
        return new MessageId(j, z);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageId)) {
            return false;
        }
        MessageId messageId = (MessageId) obj;
        return this.id == messageId.id && this.mms == messageId.mms;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int m = SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id) * 31;
        boolean z = this.mms;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return m + i;
    }

    @Override // java.lang.Object
    public String toString() {
        return "MessageId(id=" + this.id + ", mms=" + this.mms + ')';
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "out");
        parcel.writeLong(this.id);
        parcel.writeInt(this.mms ? 1 : 0);
    }

    public MessageId(long j, boolean z) {
        this.id = j;
        this.mms = z;
    }

    public final long getId() {
        return this.id;
    }

    public final boolean isMms() {
        return this.mms;
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: long : 0x0005: IGET  (r1v0 long A[REMOVE]) = (r3v0 'this' org.thoughtcrime.securesms.database.model.MessageId A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.database.model.MessageId.id long), ('|' char), (wrap: boolean : 0x000f: IGET  (r1v2 boolean A[REMOVE]) = (r3v0 'this' org.thoughtcrime.securesms.database.model.MessageId A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.database.model.MessageId.mms boolean)] */
    public final String serialize() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.id);
        sb.append('|');
        sb.append(this.mms);
        return sb.toString();
    }

    /* compiled from: MessageId.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u001a\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/MessageId$Companion;", "", "()V", "deserialize", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "serialized", "", "fromNullable", ContactRepository.ID_COLUMN, "", "mms", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final MessageId fromNullable(long j, boolean z) {
            if (j > 0) {
                return new MessageId(j, z);
            }
            return null;
        }

        @JvmStatic
        public final MessageId deserialize(String str) {
            Intrinsics.checkNotNullParameter(str, "serialized");
            List list = StringsKt__StringsKt.split$default((CharSequence) str, new String[]{"|"}, false, 0, 6, (Object) null);
            return new MessageId(Long.parseLong((String) list.get(0)), Boolean.parseBoolean((String) list.get(1)));
        }
    }
}
