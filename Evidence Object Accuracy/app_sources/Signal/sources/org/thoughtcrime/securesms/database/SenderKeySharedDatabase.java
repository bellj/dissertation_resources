package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.DistributionId;

/* loaded from: classes4.dex */
public class SenderKeySharedDatabase extends Database {
    public static final String ADDRESS;
    public static final String CREATE_TABLE;
    public static final String DEVICE;
    public static final String DISTRIBUTION_ID;
    private static final String ID;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(SenderKeySharedDatabase.class);
    public static final String TIMESTAMP;

    public SenderKeySharedDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public void markAsShared(DistributionId distributionId, Collection<SignalProtocolAddress> collection) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            for (SignalProtocolAddress signalProtocolAddress : collection) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("address", signalProtocolAddress.getName());
                contentValues.put("device", Integer.valueOf(signalProtocolAddress.getDeviceId()));
                contentValues.put("distribution_id", distributionId.toString());
                contentValues.put("timestamp", Long.valueOf(System.currentTimeMillis()));
                signalWritableDatabase.insertWithOnConflict(TABLE_NAME, null, contentValues, 5);
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public Set<SignalProtocolAddress> getSharedWith(DistributionId distributionId) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String[] buildArgs = SqlUtil.buildArgs(distributionId);
        HashSet hashSet = new HashSet();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, new String[]{"address", "device"}, "distribution_id = ?", buildArgs, null, null, null);
        while (query.moveToNext()) {
            try {
                hashSet.add(new SignalProtocolAddress(CursorUtil.requireString(query, "address"), CursorUtil.requireInt(query, "device")));
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return hashSet;
    }

    public void delete(DistributionId distributionId, Collection<SignalProtocolAddress> collection) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            for (SignalProtocolAddress signalProtocolAddress : collection) {
                signalWritableDatabase.delete(TABLE_NAME, "distribution_id = ? AND address = ? AND device = ?", SqlUtil.buildArgs(distributionId, signalProtocolAddress.getName(), Integer.valueOf(signalProtocolAddress.getDeviceId())));
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public void deleteAllFor(DistributionId distributionId) {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, "distribution_id = ?", SqlUtil.buildArgs(distributionId));
    }

    public void deleteAllFor(Collection<SignalProtocolAddress> collection) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            for (SignalProtocolAddress signalProtocolAddress : collection) {
                signalWritableDatabase.delete(TABLE_NAME, "address = ? AND device = ?", SqlUtil.buildArgs(signalProtocolAddress.getName(), Integer.valueOf(signalProtocolAddress.getDeviceId())));
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public void deleteAllFor(RecipientId recipientId) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        Recipient resolved = Recipient.resolved(recipientId);
        if (resolved.hasServiceId()) {
            signalWritableDatabase.delete(TABLE_NAME, "address = ?", SqlUtil.buildArgs(resolved.requireServiceId().toString()));
            return;
        }
        String str = TAG;
        Log.w(str, "Recipient doesn't have a UUID! " + recipientId);
    }

    public void deleteAll() {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, (String) null, (String[]) null);
    }

    public Cursor getAllSharedWithCursor() {
        return this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, null, null, null, null, "distribution_id, address, device");
    }
}
