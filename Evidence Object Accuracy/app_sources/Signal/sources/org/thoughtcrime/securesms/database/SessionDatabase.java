package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.database.Cursor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysJvmKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import net.zetetic.database.sqlcipher.SQLiteStatement;
import org.signal.core.util.CursorExtensionsKt;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SQLiteDatabaseExtensionsKt;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.state.SessionRecord;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* compiled from: SessionDatabase.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u001f2\u00020\u0001:\u0002\u001f B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0016\u0010\r\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fJ\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\t\u001a\u00020\nJ\u001c\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fJ$\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\t\u001a\u00020\n2\u000e\u0010\u0014\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000f0\u0011J\u001c\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00160\u00112\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u000e\u001a\u00020\u000fJ\u0016\u0010\u0019\u001a\u00020\u00182\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fJ$\u0010\u001a\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001b0\u00112\u0006\u0010\t\u001a\u00020\n2\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\f0\u0011J\u0018\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u001e\u0010\u001d\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u001e\u001a\u00020\u001b¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/database/SessionDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "delete", "", "serviceId", "Lorg/whispersystems/signalservice/api/push/ServiceId;", "address", "Lorg/signal/libsignal/protocol/SignalProtocolAddress;", "deleteAllFor", "addressName", "", "getAll", "", "Lorg/thoughtcrime/securesms/database/SessionDatabase$SessionRow;", "getAllFor", "addressNames", "getSubDevices", "", "hasAnySessionFor", "", "hasSessionFor", "load", "Lorg/signal/libsignal/protocol/state/SessionRecord;", "addresses", "store", "record", "Companion", "SessionRow", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SessionDatabase extends Database {
    public static final String ACCOUNT_ID;
    public static final String ADDRESS;
    public static final String CREATE_TABLE;
    public static final Companion Companion = new Companion(null);
    public static final String DEVICE;
    public static final String ID;
    public static final String RECORD;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(SessionDatabase.class);

    /* compiled from: SessionDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \f*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/database/SessionDatabase$Companion;", "", "()V", "ACCOUNT_ID", "", "ADDRESS", "CREATE_TABLE", "DEVICE", "ID", "RECORD", "TABLE_NAME", "TAG", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SessionDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    public final void store(ServiceId serviceId, SignalProtocolAddress signalProtocolAddress, SessionRecord sessionRecord) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Intrinsics.checkNotNullParameter(signalProtocolAddress, "address");
        Intrinsics.checkNotNullParameter(sessionRecord, "record");
        boolean z = false;
        if (signalProtocolAddress.getName().charAt(0) != '+') {
            z = true;
        }
        if (z) {
            SQLiteStatement compileStatement = getWritableDatabase().compileStatement("INSERT INTO sessions (account_id, address, device, record) VALUES (?, ?, ?, ?) ON CONFLICT (account_id, address, device) DO UPDATE SET record = excluded.record");
            th = null;
            try {
                compileStatement.bindString(1, serviceId.toString());
                compileStatement.bindString(2, signalProtocolAddress.getName());
                compileStatement.bindLong(3, (long) signalProtocolAddress.getDeviceId());
                compileStatement.bindBlob(4, sessionRecord.serialize());
                compileStatement.execute();
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        } else {
            throw new IllegalArgumentException("Cannot insert an e164 into this table!".toString());
        }
    }

    /* JADX WARN: Type inference failed for: r1v3, types: [java.lang.Throwable, org.signal.libsignal.protocol.state.SessionRecord] */
    public final SessionRecord load(ServiceId serviceId, SignalProtocolAddress signalProtocolAddress) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Intrinsics.checkNotNullParameter(signalProtocolAddress, "address");
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"record"}, "account_id = ? AND address = ? AND device = ?", SqlUtil.buildArgs(serviceId, signalProtocolAddress.getName(), Integer.valueOf(signalProtocolAddress.getDeviceId())), null, null, null);
        try {
            th = 0;
            if (query.moveToFirst()) {
                try {
                    Intrinsics.checkNotNullExpressionValue(query, "cursor");
                    return new SessionRecord(CursorExtensionsKt.requireNonNullBlob(query, "record"));
                } catch (IOException e) {
                    Log.w(TAG, e);
                }
            }
            Unit unit = Unit.INSTANCE;
            return th;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final List<SessionRecord> load(ServiceId serviceId, List<? extends SignalProtocolAddress> list) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Intrinsics.checkNotNullParameter(list, "addresses");
        String[] strArr = {"address", "device", "record"};
        ArrayList arrayList = new ArrayList(list.size());
        LinkedHashMap linkedHashMap = new LinkedHashMap(list.size());
        Iterator<? extends SignalProtocolAddress> it = list.iterator();
        while (true) {
            th = null;
            if (!it.hasNext()) {
                break;
            }
            SignalProtocolAddress signalProtocolAddress = (SignalProtocolAddress) it.next();
            arrayList.add(SqlUtil.buildArgs(serviceId, signalProtocolAddress.getName(), Integer.valueOf(signalProtocolAddress.getDeviceId())));
            linkedHashMap.put(signalProtocolAddress, th);
        }
        for (SqlUtil.Query query : SqlUtil.buildCustomCollectionQuery("account_id = ? AND address = ? AND device = ?", arrayList)) {
            Cursor query2 = getReadableDatabase().query(TABLE_NAME, strArr, query.getWhere(), query.getWhereArgs(), null, null, null);
            while (query2.moveToNext()) {
                try {
                    Intrinsics.checkNotNullExpressionValue(query2, "cursor");
                    try {
                        linkedHashMap.put(new SignalProtocolAddress(CursorExtensionsKt.requireNonNullString(query2, "address"), CursorExtensionsKt.requireInt(query2, "device")), new SessionRecord(CursorExtensionsKt.requireNonNullBlob(query2, "record")));
                    } catch (IOException e) {
                        Log.w(TAG, e);
                    }
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        Collection values = linkedHashMap.values();
        Intrinsics.checkNotNullExpressionValue(values, "sessions.values");
        return CollectionsKt___CollectionsKt.toList(values);
    }

    public final List<SessionRow> getAllFor(ServiceId serviceId, String str) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Intrinsics.checkNotNullParameter(str, "addressName");
        ArrayList arrayList = new ArrayList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, null, "account_id = ? AND address = ?", SqlUtil.buildArgs(serviceId, str), null, null, null);
        while (query.moveToNext()) {
            try {
                try {
                    String requireString = CursorUtil.requireString(query, "address");
                    Intrinsics.checkNotNullExpressionValue(requireString, "requireString(cursor, ADDRESS)");
                    arrayList.add(new SessionRow(requireString, CursorUtil.requireInt(query, "device"), new SessionRecord(CursorUtil.requireBlob(query, "record"))));
                } catch (IOException e) {
                    Log.w(TAG, e);
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return arrayList;
    }

    public final List<SessionRow> getAllFor(ServiceId serviceId, List<String> list) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Intrinsics.checkNotNullParameter(list, "addressNames");
        SqlUtil.Query buildSingleCollectionQuery = SqlUtil.buildSingleCollectionQuery("address", list);
        LinkedList linkedList = new LinkedList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, null, "account_id = ? AND (" + buildSingleCollectionQuery.getWhere() + ')', (String[]) ArraysKt___ArraysJvmKt.plus((Object[]) new String[]{serviceId.toString()}, (Object[]) buildSingleCollectionQuery.getWhereArgs()), null, null, null);
        while (query.moveToNext()) {
            try {
                try {
                    String requireString = CursorUtil.requireString(query, "address");
                    Intrinsics.checkNotNullExpressionValue(requireString, "requireString(cursor, ADDRESS)");
                    int requireInt = CursorUtil.requireInt(query, "device");
                    Intrinsics.checkNotNullExpressionValue(query, "cursor");
                    linkedList.add(new SessionRow(requireString, requireInt, new SessionRecord(CursorExtensionsKt.requireNonNullBlob(query, "record"))));
                } catch (IOException e) {
                    Log.w(TAG, e);
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return linkedList;
    }

    public final List<SessionRow> getAll(ServiceId serviceId) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        ArrayList arrayList = new ArrayList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, null, "account_id = ?", SqlUtil.buildArgs(serviceId), null, null, null);
        while (query.moveToNext()) {
            try {
                try {
                    Intrinsics.checkNotNullExpressionValue(query, "cursor");
                    arrayList.add(new SessionRow(CursorExtensionsKt.requireNonNullString(query, "address"), CursorExtensionsKt.requireInt(query, "device"), new SessionRecord(CursorExtensionsKt.requireNonNullBlob(query, "record"))));
                } catch (IOException e) {
                    Log.w(TAG, e);
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return arrayList;
    }

    public final List<Integer> getSubDevices(ServiceId serviceId, String str) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Intrinsics.checkNotNullParameter(str, "addressName");
        String[] buildArgs = SqlUtil.buildArgs(serviceId, str, 1);
        ArrayList arrayList = new ArrayList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"device"}, "account_id = ? AND address = ? AND device != ?", buildArgs, null, null, null);
        while (query.moveToNext()) {
            try {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                arrayList.add(Integer.valueOf(CursorExtensionsKt.requireInt(query, "device")));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return arrayList;
    }

    public final void delete(ServiceId serviceId, SignalProtocolAddress signalProtocolAddress) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Intrinsics.checkNotNullParameter(signalProtocolAddress, "address");
        getWritableDatabase().delete(TABLE_NAME, "account_id = ? AND address = ? AND device = ?", SqlUtil.buildArgs(serviceId, signalProtocolAddress.getName(), Integer.valueOf(signalProtocolAddress.getDeviceId())));
    }

    public final void deleteAllFor(ServiceId serviceId, String str) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Intrinsics.checkNotNullParameter(str, "addressName");
        getWritableDatabase().delete(TABLE_NAME, "account_id = ? AND address = ?", SqlUtil.buildArgs(serviceId, str));
    }

    public final boolean hasSessionFor(ServiceId serviceId, String str) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Intrinsics.checkNotNullParameter(str, "addressName");
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{SubscriptionLevels.BOOST_LEVEL}, "account_id = ? AND address = ?", SqlUtil.buildArgs(serviceId, str), null, null, null, SubscriptionLevels.BOOST_LEVEL);
        try {
            th = null;
            return query.moveToFirst();
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final boolean hasAnySessionFor(String str) {
        Intrinsics.checkNotNullParameter(str, "addressName");
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Intrinsics.checkNotNullExpressionValue(readableDatabase, "readableDatabase");
        Cursor run = SQLiteDatabaseExtensionsKt.select(readableDatabase, SubscriptionLevels.BOOST_LEVEL).from(TABLE_NAME).where("address = ?", str).run();
        try {
            th = null;
            return run.moveToFirst();
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    /* compiled from: SessionDatabase.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/database/SessionDatabase$SessionRow;", "", "address", "", "deviceId", "", "record", "Lorg/signal/libsignal/protocol/state/SessionRecord;", "(Ljava/lang/String;ILorg/signal/libsignal/protocol/state/SessionRecord;)V", "getAddress", "()Ljava/lang/String;", "getDeviceId", "()I", "getRecord", "()Lorg/signal/libsignal/protocol/state/SessionRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SessionRow {
        private final String address;
        private final int deviceId;
        private final SessionRecord record;

        public SessionRow(String str, int i, SessionRecord sessionRecord) {
            Intrinsics.checkNotNullParameter(str, "address");
            Intrinsics.checkNotNullParameter(sessionRecord, "record");
            this.address = str;
            this.deviceId = i;
            this.record = sessionRecord;
        }

        public final String getAddress() {
            return this.address;
        }

        public final int getDeviceId() {
            return this.deviceId;
        }

        public final SessionRecord getRecord() {
            return this.record;
        }
    }
}
