package org.thoughtcrime.securesms.database.documents;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashSet;
import java.util.Set;

/* loaded from: classes.dex */
public class IdentityKeyMismatchSet implements Document<IdentityKeyMismatch> {
    @JsonProperty("m")
    private Set<IdentityKeyMismatch> mismatches;

    public IdentityKeyMismatchSet() {
        this.mismatches = new HashSet();
    }

    public IdentityKeyMismatchSet(Set<IdentityKeyMismatch> set) {
        this.mismatches = set;
    }

    @Override // org.thoughtcrime.securesms.database.documents.Document
    public int size() {
        Set<IdentityKeyMismatch> set = this.mismatches;
        if (set == null) {
            return 0;
        }
        return set.size();
    }

    @Override // org.thoughtcrime.securesms.database.documents.Document
    @JsonIgnore
    public Set<IdentityKeyMismatch> getItems() {
        return this.mismatches;
    }
}
