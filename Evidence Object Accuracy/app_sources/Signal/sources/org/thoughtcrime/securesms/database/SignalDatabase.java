package org.thoughtcrime.securesms.database;

import android.app.Application;
import android.content.Context;
import java.io.File;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import net.zetetic.database.sqlcipher.SQLiteOpenHelper;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.DatabaseSecret;
import org.thoughtcrime.securesms.crypto.MasterSecret;
import org.thoughtcrime.securesms.database.StorySendsDatabase;
import org.thoughtcrime.securesms.database.helpers.ClassicOpenHelper;
import org.thoughtcrime.securesms.database.helpers.PreKeyMigrationHelper;
import org.thoughtcrime.securesms.database.helpers.SQLCipherMigrationHelper;
import org.thoughtcrime.securesms.database.helpers.SessionStoreMigrationHelper;
import org.thoughtcrime.securesms.database.helpers.SignalDatabaseMigrations;
import org.thoughtcrime.securesms.database.model.AvatarPickerDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.RefreshPreKeysJob;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet;
import org.thoughtcrime.securesms.migrations.LegacyMigrationJob;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* compiled from: SignalDatabase.kt */
@Metadata(d1 = {"\u0000î\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\b\u0016\u0018\u0000 ·\u00012\u00020\u00012\u00020\u0002:\u0002·\u0001B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ*\u0010¦\u0001\u001a\u00030§\u00012\u0007\u0010¨\u0001\u001a\u00020c2\u000f\u0010©\u0001\u001a\n\u0012\u0005\u0012\u00030«\u00010ª\u0001H\u0002¢\u0006\u0003\u0010¬\u0001J\t\u0010­\u0001\u001a\u00020cH\u0016J\t\u0010®\u0001\u001a\u00020cH\u0016J\t\u0010¯\u0001\u001a\u00020cH\u0016J\u0013\u0010°\u0001\u001a\u00030§\u00012\u0007\u0010¨\u0001\u001a\u00020cH\u0016J\u0013\u0010±\u0001\u001a\u00030§\u00012\u0007\u0010¨\u0001\u001a\u00020cH\u0016J\u0013\u0010²\u0001\u001a\u00030§\u00012\u0007\u0010¨\u0001\u001a\u00020cH\u0016J'\u0010³\u0001\u001a\u00030§\u00012\u0007\u0010¨\u0001\u001a\u00020c2\b\u0010´\u0001\u001a\u00030µ\u00012\b\u0010¶\u0001\u001a\u00030µ\u0001H\u0016R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u0017¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u001a\u001a\u00020\u001b¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u001e\u001a\u00020\u001f¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\"\u001a\u00020#¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0011\u0010&\u001a\u00020'¢\u0006\b\n\u0000\u001a\u0004\b(\u0010)R\u0011\u0010*\u001a\u00020+¢\u0006\b\n\u0000\u001a\u0004\b,\u0010-R\u0011\u0010.\u001a\u00020/¢\u0006\b\n\u0000\u001a\u0004\b0\u00101R\u0011\u00102\u001a\u000203¢\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u0011\u00106\u001a\u000207¢\u0006\b\n\u0000\u001a\u0004\b8\u00109R\u0011\u0010:\u001a\u00020;¢\u0006\b\n\u0000\u001a\u0004\b<\u0010=R\u0011\u0010>\u001a\u00020?¢\u0006\b\n\u0000\u001a\u0004\b@\u0010AR\u0011\u0010B\u001a\u00020C¢\u0006\b\n\u0000\u001a\u0004\bD\u0010ER\u0011\u0010F\u001a\u00020G¢\u0006\b\n\u0000\u001a\u0004\bH\u0010IR\u0011\u0010J\u001a\u00020K¢\u0006\b\n\u0000\u001a\u0004\bL\u0010MR\u0011\u0010N\u001a\u00020O¢\u0006\b\n\u0000\u001a\u0004\bP\u0010QR\u0011\u0010R\u001a\u00020S¢\u0006\b\n\u0000\u001a\u0004\bT\u0010UR\u0011\u0010V\u001a\u00020W¢\u0006\b\n\u0000\u001a\u0004\bX\u0010YR\u0011\u0010Z\u001a\u00020[¢\u0006\b\n\u0000\u001a\u0004\b\\\u0010]R\u0011\u0010^\u001a\u00020_¢\u0006\b\n\u0000\u001a\u0004\b`\u0010aR\u0014\u0010b\u001a\u00020c8VX\u0004¢\u0006\u0006\u001a\u0004\bd\u0010eR\u0014\u0010f\u001a\u00020c8VX\u0004¢\u0006\u0006\u001a\u0004\bg\u0010eR\u0011\u0010h\u001a\u00020i¢\u0006\b\n\u0000\u001a\u0004\bj\u0010kR\u0011\u0010l\u001a\u00020m¢\u0006\b\n\u0000\u001a\u0004\bn\u0010oR\u0011\u0010p\u001a\u00020q¢\u0006\b\n\u0000\u001a\u0004\br\u0010sR\u0011\u0010t\u001a\u00020u¢\u0006\b\n\u0000\u001a\u0004\bv\u0010wR\u0011\u0010x\u001a\u00020y¢\u0006\b\n\u0000\u001a\u0004\bz\u0010{R\u0011\u0010|\u001a\u00020}¢\u0006\b\n\u0000\u001a\u0004\b~\u0010R\u0015\u0010\u0001\u001a\u00030\u0001¢\u0006\n\n\u0000\u001a\u0006\b\u0001\u0010\u0001R\u0015\u0010\u0001\u001a\u00030\u0001¢\u0006\n\n\u0000\u001a\u0006\b\u0001\u0010\u0001R\u0018\u0010\u0001\u001a\u00030\u00018VX\u0004¢\u0006\b\u001a\u0006\b\u0001\u0010\u0001R\u0018\u0010\u0001\u001a\u00030\u00018VX\u0004¢\u0006\b\u001a\u0006\b\u0001\u0010\u0001R\u0015\u0010\u0001\u001a\u00030\u0001¢\u0006\n\n\u0000\u001a\u0006\b\u0001\u0010\u0001R\u0015\u0010\u0001\u001a\u00030\u0001¢\u0006\n\n\u0000\u001a\u0006\b\u0001\u0010\u0001R\u0015\u0010\u0001\u001a\u00030\u0001¢\u0006\n\n\u0000\u001a\u0006\b\u0001\u0010\u0001R\u0015\u0010\u0001\u001a\u00030\u0001¢\u0006\n\n\u0000\u001a\u0006\b\u0001\u0010\u0001R\u0015\u0010\u0001\u001a\u00030\u0001¢\u0006\n\n\u0000\u001a\u0006\b \u0001\u0010¡\u0001R\u0015\u0010¢\u0001\u001a\u00030£\u0001¢\u0006\n\n\u0000\u001a\u0006\b¤\u0001\u0010¥\u0001¨\u0006¸\u0001"}, d2 = {"Lorg/thoughtcrime/securesms/database/SignalDatabase;", "Lnet/zetetic/database/sqlcipher/SQLiteOpenHelper;", "Lorg/thoughtcrime/securesms/database/SignalDatabaseOpenHelper;", "context", "Landroid/app/Application;", "databaseSecret", "Lorg/thoughtcrime/securesms/crypto/DatabaseSecret;", "attachmentSecret", "Lorg/thoughtcrime/securesms/crypto/AttachmentSecret;", "(Landroid/app/Application;Lorg/thoughtcrime/securesms/crypto/DatabaseSecret;Lorg/thoughtcrime/securesms/crypto/AttachmentSecret;)V", "attachments", "Lorg/thoughtcrime/securesms/database/AttachmentDatabase;", "getAttachments", "()Lorg/thoughtcrime/securesms/database/AttachmentDatabase;", "avatarPickerDatabase", "Lorg/thoughtcrime/securesms/database/model/AvatarPickerDatabase;", "getAvatarPickerDatabase", "()Lorg/thoughtcrime/securesms/database/model/AvatarPickerDatabase;", "cdsDatabase", "Lorg/thoughtcrime/securesms/database/CdsDatabase;", "getCdsDatabase", "()Lorg/thoughtcrime/securesms/database/CdsDatabase;", "chatColorsDatabase", "Lorg/thoughtcrime/securesms/database/ChatColorsDatabase;", "getChatColorsDatabase", "()Lorg/thoughtcrime/securesms/database/ChatColorsDatabase;", "distributionListDatabase", "Lorg/thoughtcrime/securesms/database/DistributionListDatabase;", "getDistributionListDatabase", "()Lorg/thoughtcrime/securesms/database/DistributionListDatabase;", "donationReceiptDatabase", "Lorg/thoughtcrime/securesms/database/DonationReceiptDatabase;", "getDonationReceiptDatabase", "()Lorg/thoughtcrime/securesms/database/DonationReceiptDatabase;", "draftDatabase", "Lorg/thoughtcrime/securesms/database/DraftDatabase;", "getDraftDatabase", "()Lorg/thoughtcrime/securesms/database/DraftDatabase;", "emojiSearchDatabase", "Lorg/thoughtcrime/securesms/database/EmojiSearchDatabase;", "getEmojiSearchDatabase", "()Lorg/thoughtcrime/securesms/database/EmojiSearchDatabase;", "groupCallRingDatabase", "Lorg/thoughtcrime/securesms/database/GroupCallRingDatabase;", "getGroupCallRingDatabase", "()Lorg/thoughtcrime/securesms/database/GroupCallRingDatabase;", "groupDatabase", "Lorg/thoughtcrime/securesms/database/GroupDatabase;", "getGroupDatabase", "()Lorg/thoughtcrime/securesms/database/GroupDatabase;", "groupReceiptDatabase", "Lorg/thoughtcrime/securesms/database/GroupReceiptDatabase;", "getGroupReceiptDatabase", "()Lorg/thoughtcrime/securesms/database/GroupReceiptDatabase;", "identityDatabase", "Lorg/thoughtcrime/securesms/database/IdentityDatabase;", "getIdentityDatabase", "()Lorg/thoughtcrime/securesms/database/IdentityDatabase;", "media", "Lorg/thoughtcrime/securesms/database/MediaDatabase;", "getMedia", "()Lorg/thoughtcrime/securesms/database/MediaDatabase;", "mentionDatabase", "Lorg/thoughtcrime/securesms/database/MentionDatabase;", "getMentionDatabase", "()Lorg/thoughtcrime/securesms/database/MentionDatabase;", "messageSendLogDatabase", "Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase;", "getMessageSendLogDatabase", "()Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase;", "mms", "Lorg/thoughtcrime/securesms/database/MmsDatabase;", "getMms", "()Lorg/thoughtcrime/securesms/database/MmsDatabase;", "mmsSmsDatabase", "Lorg/thoughtcrime/securesms/database/MmsSmsDatabase;", "getMmsSmsDatabase", "()Lorg/thoughtcrime/securesms/database/MmsSmsDatabase;", "notificationProfileDatabase", "Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase;", "getNotificationProfileDatabase", "()Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase;", "paymentDatabase", "Lorg/thoughtcrime/securesms/database/PaymentDatabase;", "getPaymentDatabase", "()Lorg/thoughtcrime/securesms/database/PaymentDatabase;", "pendingRetryReceiptDatabase", "Lorg/thoughtcrime/securesms/database/PendingRetryReceiptDatabase;", "getPendingRetryReceiptDatabase", "()Lorg/thoughtcrime/securesms/database/PendingRetryReceiptDatabase;", "preKeyDatabase", "Lorg/thoughtcrime/securesms/database/OneTimePreKeyDatabase;", "getPreKeyDatabase", "()Lorg/thoughtcrime/securesms/database/OneTimePreKeyDatabase;", "pushDatabase", "Lorg/thoughtcrime/securesms/database/PushDatabase;", "getPushDatabase", "()Lorg/thoughtcrime/securesms/database/PushDatabase;", "rawReadableDatabase", "Lnet/zetetic/database/sqlcipher/SQLiteDatabase;", "getRawReadableDatabase", "()Lnet/zetetic/database/sqlcipher/SQLiteDatabase;", "rawWritableDatabase", "getRawWritableDatabase", "reactionDatabase", "Lorg/thoughtcrime/securesms/database/ReactionDatabase;", "getReactionDatabase", "()Lorg/thoughtcrime/securesms/database/ReactionDatabase;", "recipientDatabase", "Lorg/thoughtcrime/securesms/database/RecipientDatabase;", "getRecipientDatabase", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase;", "remappedRecordsDatabase", "Lorg/thoughtcrime/securesms/database/RemappedRecordsDatabase;", "getRemappedRecordsDatabase", "()Lorg/thoughtcrime/securesms/database/RemappedRecordsDatabase;", "remoteMegaphoneDatabase", "Lorg/thoughtcrime/securesms/database/RemoteMegaphoneDatabase;", "getRemoteMegaphoneDatabase", "()Lorg/thoughtcrime/securesms/database/RemoteMegaphoneDatabase;", "searchDatabase", "Lorg/thoughtcrime/securesms/database/SearchDatabase;", "getSearchDatabase", "()Lorg/thoughtcrime/securesms/database/SearchDatabase;", "senderKeyDatabase", "Lorg/thoughtcrime/securesms/database/SenderKeyDatabase;", "getSenderKeyDatabase", "()Lorg/thoughtcrime/securesms/database/SenderKeyDatabase;", "senderKeySharedDatabase", "Lorg/thoughtcrime/securesms/database/SenderKeySharedDatabase;", "getSenderKeySharedDatabase", "()Lorg/thoughtcrime/securesms/database/SenderKeySharedDatabase;", "sessionDatabase", "Lorg/thoughtcrime/securesms/database/SessionDatabase;", "getSessionDatabase", "()Lorg/thoughtcrime/securesms/database/SessionDatabase;", "signalReadableDatabase", "Lorg/thoughtcrime/securesms/database/SQLiteDatabase;", "getSignalReadableDatabase", "()Lorg/thoughtcrime/securesms/database/SQLiteDatabase;", "signalWritableDatabase", "getSignalWritableDatabase", "signedPreKeyDatabase", "Lorg/thoughtcrime/securesms/database/SignedPreKeyDatabase;", "getSignedPreKeyDatabase", "()Lorg/thoughtcrime/securesms/database/SignedPreKeyDatabase;", "sms", "Lorg/thoughtcrime/securesms/database/SmsDatabase;", "getSms", "()Lorg/thoughtcrime/securesms/database/SmsDatabase;", "stickerDatabase", "Lorg/thoughtcrime/securesms/database/StickerDatabase;", "getStickerDatabase", "()Lorg/thoughtcrime/securesms/database/StickerDatabase;", "storageIdDatabase", "Lorg/thoughtcrime/securesms/database/UnknownStorageIdDatabase;", "getStorageIdDatabase", "()Lorg/thoughtcrime/securesms/database/UnknownStorageIdDatabase;", "storySendsDatabase", "Lorg/thoughtcrime/securesms/database/StorySendsDatabase;", "getStorySendsDatabase", "()Lorg/thoughtcrime/securesms/database/StorySendsDatabase;", ThreadDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/database/ThreadDatabase;", "getThread", "()Lorg/thoughtcrime/securesms/database/ThreadDatabase;", "executeStatements", "", "db", "statements", "", "", "(Lnet/zetetic/database/sqlcipher/SQLiteDatabase;[Ljava/lang/String;)V", "getReadableDatabase", "getSqlCipherDatabase", "getWritableDatabase", "markCurrent", "onCreate", "onOpen", "onUpgrade", "oldVersion", "", "newVersion", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public class SignalDatabase extends SQLiteOpenHelper implements SignalDatabaseOpenHelper {
    public static final Companion Companion = new Companion(null);
    private static final String DATABASE_NAME;
    private static final String TAG = Log.tag(SignalDatabase.class);
    private static volatile SignalDatabase instance;
    private final AttachmentDatabase attachments;
    private final AvatarPickerDatabase avatarPickerDatabase;
    private final CdsDatabase cdsDatabase;
    private final ChatColorsDatabase chatColorsDatabase;
    private final Application context;
    private final DistributionListDatabase distributionListDatabase;
    private final DonationReceiptDatabase donationReceiptDatabase;
    private final DraftDatabase draftDatabase;
    private final EmojiSearchDatabase emojiSearchDatabase;
    private final GroupCallRingDatabase groupCallRingDatabase;
    private final GroupDatabase groupDatabase;
    private final GroupReceiptDatabase groupReceiptDatabase;
    private final IdentityDatabase identityDatabase;
    private final MediaDatabase media;
    private final MentionDatabase mentionDatabase;
    private final MessageSendLogDatabase messageSendLogDatabase;
    private final MmsDatabase mms;
    private final MmsSmsDatabase mmsSmsDatabase;
    private final NotificationProfileDatabase notificationProfileDatabase;
    private final PaymentDatabase paymentDatabase;
    private final PendingRetryReceiptDatabase pendingRetryReceiptDatabase;
    private final OneTimePreKeyDatabase preKeyDatabase;
    private final PushDatabase pushDatabase;
    private final ReactionDatabase reactionDatabase;
    private final RecipientDatabase recipientDatabase;
    private final RemappedRecordsDatabase remappedRecordsDatabase;
    private final RemoteMegaphoneDatabase remoteMegaphoneDatabase;
    private final SearchDatabase searchDatabase;
    private final SenderKeyDatabase senderKeyDatabase;
    private final SenderKeySharedDatabase senderKeySharedDatabase;
    private final SessionDatabase sessionDatabase;
    private final SignedPreKeyDatabase signedPreKeyDatabase;
    private final SmsDatabase sms;
    private final StickerDatabase stickerDatabase;
    private final UnknownStorageIdDatabase storageIdDatabase;
    private final StorySendsDatabase storySendsDatabase;
    private final ThreadDatabase thread;

    @JvmStatic
    public static final AttachmentDatabase attachments() {
        return Companion.attachments();
    }

    @JvmStatic
    public static final AvatarPickerDatabase avatarPicker() {
        return Companion.avatarPicker();
    }

    @JvmStatic
    public static final CdsDatabase cds() {
        return Companion.cds();
    }

    @JvmStatic
    public static final ChatColorsDatabase chatColors() {
        return Companion.chatColors();
    }

    @JvmStatic
    public static final boolean databaseFileExists(Context context) {
        return Companion.databaseFileExists(context);
    }

    @JvmStatic
    public static final DistributionListDatabase distributionLists() {
        return Companion.distributionLists();
    }

    @JvmStatic
    public static final DonationReceiptDatabase donationReceipts() {
        return Companion.donationReceipts();
    }

    @JvmStatic
    public static final DraftDatabase drafts() {
        return Companion.drafts();
    }

    @JvmStatic
    public static final EmojiSearchDatabase emojiSearch() {
        return Companion.emojiSearch();
    }

    public static final SQLiteDatabase getBackupDatabase() {
        return Companion.getBackupDatabase();
    }

    @JvmStatic
    public static final File getDatabaseFile(Context context) {
        return Companion.getDatabaseFile(context);
    }

    public static final SignalDatabase getInstance() {
        return Companion.getInstance();
    }

    public static final SQLiteDatabase getRawDatabase() {
        return Companion.getRawDatabase();
    }

    @JvmStatic
    public static final GroupCallRingDatabase groupCallRings() {
        return Companion.groupCallRings();
    }

    @JvmStatic
    public static final GroupReceiptDatabase groupReceipts() {
        return Companion.groupReceipts();
    }

    @JvmStatic
    public static final GroupDatabase groups() {
        return Companion.groups();
    }

    @JvmStatic
    public static final boolean hasTable(String str) {
        return Companion.hasTable(str);
    }

    @JvmStatic
    public static final IdentityDatabase identities() {
        return Companion.identities();
    }

    public static final boolean inTransaction() {
        return Companion.inTransaction();
    }

    @JvmStatic
    public static final void init(Application application, DatabaseSecret databaseSecret, AttachmentSecret attachmentSecret) {
        Companion.init(application, databaseSecret, attachmentSecret);
    }

    @JvmStatic
    public static final MediaDatabase media() {
        return Companion.media();
    }

    @JvmStatic
    public static final MentionDatabase mentions() {
        return Companion.mentions();
    }

    @JvmStatic
    public static final MessageSendLogDatabase messageLog() {
        return Companion.messageLog();
    }

    @JvmStatic
    public static final SearchDatabase messageSearch() {
        return Companion.messageSearch();
    }

    @JvmStatic
    public static final MmsDatabase mms() {
        return Companion.mms();
    }

    @JvmStatic
    public static final MmsSmsDatabase mmsSms() {
        return Companion.mmsSms();
    }

    @JvmStatic
    public static final NotificationProfileDatabase notificationProfiles() {
        return Companion.notificationProfiles();
    }

    @JvmStatic
    public static final void onApplicationLevelUpgrade(Context context, MasterSecret masterSecret, int i, LegacyMigrationJob.DatabaseUpgradeListener databaseUpgradeListener) {
        Companion.onApplicationLevelUpgrade(context, masterSecret, i, databaseUpgradeListener);
    }

    @JvmStatic
    public static final OneTimePreKeyDatabase oneTimePreKeys() {
        return Companion.oneTimePreKeys();
    }

    @JvmStatic
    public static final PaymentDatabase payments() {
        return Companion.payments();
    }

    @JvmStatic
    public static final PendingRetryReceiptDatabase pendingRetryReceipts() {
        return Companion.pendingRetryReceipts();
    }

    @JvmStatic
    public static final PushDatabase push() {
        return Companion.push();
    }

    @JvmStatic
    public static final ReactionDatabase reactions() {
        return Companion.reactions();
    }

    @JvmStatic
    public static final RecipientDatabase recipients() {
        return Companion.recipients();
    }

    @JvmStatic
    public static final RemappedRecordsDatabase remappedRecords() {
        return Companion.remappedRecords();
    }

    @JvmStatic
    public static final RemoteMegaphoneDatabase remoteMegaphones() {
        return Companion.remoteMegaphones();
    }

    @JvmStatic
    public static final void runInTransaction(Runnable runnable) {
        Companion.runInTransaction(runnable);
    }

    @JvmStatic
    public static final void runPostSuccessfulTransaction(Runnable runnable) {
        Companion.runPostSuccessfulTransaction(runnable);
    }

    @JvmStatic
    public static final void runPostSuccessfulTransaction(String str, Runnable runnable) {
        Companion.runPostSuccessfulTransaction(str, runnable);
    }

    @JvmStatic
    public static final SenderKeySharedDatabase senderKeyShared() {
        return Companion.senderKeyShared();
    }

    @JvmStatic
    public static final SenderKeyDatabase senderKeys() {
        return Companion.senderKeys();
    }

    @JvmStatic
    public static final SessionDatabase sessions() {
        return Companion.sessions();
    }

    @JvmStatic
    public static final SignedPreKeyDatabase signedPreKeys() {
        return Companion.signedPreKeys();
    }

    @JvmStatic
    public static final SmsDatabase sms() {
        return Companion.sms();
    }

    @JvmStatic
    public static final StickerDatabase stickers() {
        return Companion.stickers();
    }

    @JvmStatic
    public static final StorySendsDatabase storySends() {
        return Companion.storySends();
    }

    @JvmStatic
    public static final ThreadDatabase threads() {
        return Companion.threads();
    }

    @JvmStatic
    public static final void triggerDatabaseAccess() {
        Companion.triggerDatabaseAccess();
    }

    @JvmStatic
    public static final UnknownStorageIdDatabase unknownStorageIds() {
        return Companion.unknownStorageIds();
    }

    @JvmStatic
    public static final void upgradeRestored(SQLiteDatabase sQLiteDatabase) {
        Companion.upgradeRestored(sQLiteDatabase);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SignalDatabase(Application application, DatabaseSecret databaseSecret, AttachmentSecret attachmentSecret) {
        super(application, DATABASE_NAME, databaseSecret.asString(), (SQLiteDatabase.CursorFactory) null, (int) SignalDatabaseMigrations.DATABASE_VERSION, 0, new SqlCipherErrorHandler(DATABASE_NAME), new SqlCipherDatabaseHook());
        Intrinsics.checkNotNullParameter(application, "context");
        Intrinsics.checkNotNullParameter(databaseSecret, "databaseSecret");
        Intrinsics.checkNotNullParameter(attachmentSecret, "attachmentSecret");
        this.context = application;
        this.sms = new SmsDatabase(application, this);
        this.mms = new MmsDatabase(application, this);
        this.attachments = new AttachmentDatabase(application, this, attachmentSecret);
        this.media = new MediaDatabase(application, this);
        this.thread = new ThreadDatabase(application, this);
        this.mmsSmsDatabase = new MmsSmsDatabase(application, this);
        this.identityDatabase = new IdentityDatabase(application, this);
        this.draftDatabase = new DraftDatabase(application, this);
        this.pushDatabase = new PushDatabase(application, this);
        this.groupDatabase = new GroupDatabase(application, this);
        this.recipientDatabase = new RecipientDatabase(application, this);
        this.groupReceiptDatabase = new GroupReceiptDatabase(application, this);
        this.preKeyDatabase = new OneTimePreKeyDatabase(application, this);
        this.signedPreKeyDatabase = new SignedPreKeyDatabase(application, this);
        this.sessionDatabase = new SessionDatabase(application, this);
        this.senderKeyDatabase = new SenderKeyDatabase(application, this);
        this.senderKeySharedDatabase = new SenderKeySharedDatabase(application, this);
        this.pendingRetryReceiptDatabase = new PendingRetryReceiptDatabase(application, this);
        this.searchDatabase = new SearchDatabase(application, this);
        this.stickerDatabase = new StickerDatabase(application, this, attachmentSecret);
        this.storageIdDatabase = new UnknownStorageIdDatabase(application, this);
        this.remappedRecordsDatabase = new RemappedRecordsDatabase(application, this);
        this.mentionDatabase = new MentionDatabase(application, this);
        this.paymentDatabase = new PaymentDatabase(application, this);
        this.chatColorsDatabase = new ChatColorsDatabase(application, this);
        this.emojiSearchDatabase = new EmojiSearchDatabase(application, this);
        this.messageSendLogDatabase = new MessageSendLogDatabase(application, this);
        this.avatarPickerDatabase = new AvatarPickerDatabase(application, this);
        this.groupCallRingDatabase = new GroupCallRingDatabase(application, this);
        this.reactionDatabase = new ReactionDatabase(application, this);
        this.notificationProfileDatabase = new NotificationProfileDatabase(application, this);
        this.donationReceiptDatabase = new DonationReceiptDatabase(application, this);
        this.distributionListDatabase = new DistributionListDatabase(application, this);
        this.storySendsDatabase = new StorySendsDatabase(application, this);
        this.cdsDatabase = new CdsDatabase(application, this);
        this.remoteMegaphoneDatabase = new RemoteMegaphoneDatabase(application, this);
    }

    public final SmsDatabase getSms() {
        return this.sms;
    }

    public final MmsDatabase getMms() {
        return this.mms;
    }

    public final AttachmentDatabase getAttachments() {
        return this.attachments;
    }

    public final MediaDatabase getMedia() {
        return this.media;
    }

    public final ThreadDatabase getThread() {
        return this.thread;
    }

    public final MmsSmsDatabase getMmsSmsDatabase() {
        return this.mmsSmsDatabase;
    }

    public final IdentityDatabase getIdentityDatabase() {
        return this.identityDatabase;
    }

    public final DraftDatabase getDraftDatabase() {
        return this.draftDatabase;
    }

    public final PushDatabase getPushDatabase() {
        return this.pushDatabase;
    }

    public final GroupDatabase getGroupDatabase() {
        return this.groupDatabase;
    }

    public final RecipientDatabase getRecipientDatabase() {
        return this.recipientDatabase;
    }

    public final GroupReceiptDatabase getGroupReceiptDatabase() {
        return this.groupReceiptDatabase;
    }

    public final OneTimePreKeyDatabase getPreKeyDatabase() {
        return this.preKeyDatabase;
    }

    public final SignedPreKeyDatabase getSignedPreKeyDatabase() {
        return this.signedPreKeyDatabase;
    }

    public final SessionDatabase getSessionDatabase() {
        return this.sessionDatabase;
    }

    public final SenderKeyDatabase getSenderKeyDatabase() {
        return this.senderKeyDatabase;
    }

    public final SenderKeySharedDatabase getSenderKeySharedDatabase() {
        return this.senderKeySharedDatabase;
    }

    public final PendingRetryReceiptDatabase getPendingRetryReceiptDatabase() {
        return this.pendingRetryReceiptDatabase;
    }

    public final SearchDatabase getSearchDatabase() {
        return this.searchDatabase;
    }

    public final StickerDatabase getStickerDatabase() {
        return this.stickerDatabase;
    }

    public final UnknownStorageIdDatabase getStorageIdDatabase() {
        return this.storageIdDatabase;
    }

    public final RemappedRecordsDatabase getRemappedRecordsDatabase() {
        return this.remappedRecordsDatabase;
    }

    public final MentionDatabase getMentionDatabase() {
        return this.mentionDatabase;
    }

    public final PaymentDatabase getPaymentDatabase() {
        return this.paymentDatabase;
    }

    public final ChatColorsDatabase getChatColorsDatabase() {
        return this.chatColorsDatabase;
    }

    public final EmojiSearchDatabase getEmojiSearchDatabase() {
        return this.emojiSearchDatabase;
    }

    public final MessageSendLogDatabase getMessageSendLogDatabase() {
        return this.messageSendLogDatabase;
    }

    public final AvatarPickerDatabase getAvatarPickerDatabase() {
        return this.avatarPickerDatabase;
    }

    public final GroupCallRingDatabase getGroupCallRingDatabase() {
        return this.groupCallRingDatabase;
    }

    public final ReactionDatabase getReactionDatabase() {
        return this.reactionDatabase;
    }

    public final NotificationProfileDatabase getNotificationProfileDatabase() {
        return this.notificationProfileDatabase;
    }

    public final DonationReceiptDatabase getDonationReceiptDatabase() {
        return this.donationReceiptDatabase;
    }

    public final DistributionListDatabase getDistributionListDatabase() {
        return this.distributionListDatabase;
    }

    public final StorySendsDatabase getStorySendsDatabase() {
        return this.storySendsDatabase;
    }

    public final CdsDatabase getCdsDatabase() {
        return this.cdsDatabase;
    }

    public final RemoteMegaphoneDatabase getRemoteMegaphoneDatabase() {
        return this.remoteMegaphoneDatabase;
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
        sQLiteDatabase.setForeignKeyConstraintsEnabled(true);
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
        sQLiteDatabase.execSQL(SmsDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(MmsDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(AttachmentDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(ThreadDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(IdentityDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(DraftDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(PushDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(GroupDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(RecipientDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(GroupReceiptDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(OneTimePreKeyDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(SignedPreKeyDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(SessionDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(SenderKeyDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(SenderKeySharedDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(PendingRetryReceiptDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(StickerDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(UnknownStorageIdDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(MentionDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(PaymentDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(ChatColorsDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(EmojiSearchDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(AvatarPickerDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(GroupCallRingDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(ReactionDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(DonationReceiptDatabase.CREATE_TABLE);
        StorySendsDatabase.Companion companion = StorySendsDatabase.Companion;
        sQLiteDatabase.execSQL(companion.getCREATE_TABLE());
        sQLiteDatabase.execSQL(CdsDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(RemoteMegaphoneDatabase.Companion.getCREATE_TABLE());
        String[] strArr = SearchDatabase.CREATE_TABLE;
        Intrinsics.checkNotNullExpressionValue(strArr, "CREATE_TABLE");
        executeStatements(sQLiteDatabase, strArr);
        String[] strArr2 = RemappedRecordsDatabase.CREATE_TABLE;
        Intrinsics.checkNotNullExpressionValue(strArr2, "CREATE_TABLE");
        executeStatements(sQLiteDatabase, strArr2);
        executeStatements(sQLiteDatabase, MessageSendLogDatabase.CREATE_TABLE);
        executeStatements(sQLiteDatabase, NotificationProfileDatabase.CREATE_TABLE);
        executeStatements(sQLiteDatabase, DistributionListDatabase.CREATE_TABLE);
        executeStatements(sQLiteDatabase, RecipientDatabase.Companion.getCREATE_INDEXS());
        String[] strArr3 = SmsDatabase.CREATE_INDEXS;
        Intrinsics.checkNotNullExpressionValue(strArr3, "CREATE_INDEXS");
        executeStatements(sQLiteDatabase, strArr3);
        String[] strArr4 = MmsDatabase.CREATE_INDEXS;
        Intrinsics.checkNotNullExpressionValue(strArr4, "CREATE_INDEXS");
        executeStatements(sQLiteDatabase, strArr4);
        String[] strArr5 = AttachmentDatabase.CREATE_INDEXS;
        Intrinsics.checkNotNullExpressionValue(strArr5, "CREATE_INDEXS");
        executeStatements(sQLiteDatabase, strArr5);
        String[] strArr6 = ThreadDatabase.CREATE_INDEXS;
        Intrinsics.checkNotNullExpressionValue(strArr6, "CREATE_INDEXS");
        executeStatements(sQLiteDatabase, strArr6);
        String[] strArr7 = DraftDatabase.CREATE_INDEXS;
        Intrinsics.checkNotNullExpressionValue(strArr7, "CREATE_INDEXS");
        executeStatements(sQLiteDatabase, strArr7);
        String[] strArr8 = GroupDatabase.CREATE_INDEXS;
        Intrinsics.checkNotNullExpressionValue(strArr8, "CREATE_INDEXS");
        executeStatements(sQLiteDatabase, strArr8);
        String[] strArr9 = GroupReceiptDatabase.CREATE_INDEXES;
        Intrinsics.checkNotNullExpressionValue(strArr9, "CREATE_INDEXES");
        executeStatements(sQLiteDatabase, strArr9);
        String[] strArr10 = StickerDatabase.CREATE_INDEXES;
        Intrinsics.checkNotNullExpressionValue(strArr10, "CREATE_INDEXES");
        executeStatements(sQLiteDatabase, strArr10);
        String[] strArr11 = UnknownStorageIdDatabase.CREATE_INDEXES;
        Intrinsics.checkNotNullExpressionValue(strArr11, "CREATE_INDEXES");
        executeStatements(sQLiteDatabase, strArr11);
        String[] strArr12 = MentionDatabase.CREATE_INDEXES;
        Intrinsics.checkNotNullExpressionValue(strArr12, "CREATE_INDEXES");
        executeStatements(sQLiteDatabase, strArr12);
        String[] strArr13 = PaymentDatabase.CREATE_INDEXES;
        Intrinsics.checkNotNullExpressionValue(strArr13, "CREATE_INDEXES");
        executeStatements(sQLiteDatabase, strArr13);
        executeStatements(sQLiteDatabase, MessageSendLogDatabase.CREATE_INDEXES);
        executeStatements(sQLiteDatabase, GroupCallRingDatabase.CREATE_INDEXES);
        executeStatements(sQLiteDatabase, NotificationProfileDatabase.CREATE_INDEXES);
        executeStatements(sQLiteDatabase, DonationReceiptDatabase.Companion.getCREATE_INDEXS());
        sQLiteDatabase.execSQL(companion.getCREATE_INDEX());
        executeStatements(sQLiteDatabase, DistributionListDatabase.CREATE_INDEXES);
        executeStatements(sQLiteDatabase, MessageSendLogDatabase.CREATE_TRIGGERS);
        executeStatements(sQLiteDatabase, ReactionDatabase.CREATE_TRIGGERS);
        DistributionListDatabase.Companion.insertInitialDistributionListAtCreationTime(sQLiteDatabase);
        if (this.context.getDatabasePath(ClassicOpenHelper.NAME).exists()) {
            android.database.sqlite.SQLiteDatabase writableDatabase = new ClassicOpenHelper(this.context).getWritableDatabase();
            SQLCipherMigrationHelper.migratePlaintext(this.context, writableDatabase, sQLiteDatabase);
            MasterSecret masterSecret = KeyCachingService.getMasterSecret(this.context);
            if (masterSecret != null) {
                SQLCipherMigrationHelper.migrateCiphertext(this.context, masterSecret, writableDatabase, sQLiteDatabase, null);
            } else {
                TextSecurePreferences.setNeedsSqlCipherMigration(this.context, true);
            }
            if (!PreKeyMigrationHelper.migratePreKeys(this.context, sQLiteDatabase)) {
                ApplicationDependencies.getJobManager().add(new RefreshPreKeysJob());
            }
            SessionStoreMigrationHelper.migrateSessions(this.context, sQLiteDatabase);
            PreKeyMigrationHelper.cleanUpPreKeys(this.context);
        }
    }

    /* JADX INFO: finally extract failed */
    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
        String str = TAG;
        Log.i(str, "Upgrading database: " + i + ", " + i2);
        long currentTimeMillis = System.currentTimeMillis();
        sQLiteDatabase.beginTransaction();
        try {
            SignalDatabaseMigrations.migrate(this.context, sQLiteDatabase, i, i2);
            sQLiteDatabase.setTransactionSuccessful();
            sQLiteDatabase.endTransaction();
            SignalDatabaseMigrations.migratePostTransaction(this.context, i);
            Log.i(str, "Upgrade complete. Took " + (System.currentTimeMillis() - currentTimeMillis) + " ms.");
        } catch (Throwable th) {
            sQLiteDatabase.endTransaction();
            throw th;
        }
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public SQLiteDatabase getReadableDatabase() {
        throw new UnsupportedOperationException("Call getSignalReadableDatabase() instead!");
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public SQLiteDatabase getWritableDatabase() {
        throw new UnsupportedOperationException("Call getSignalWritableDatabase() instead!");
    }

    public SQLiteDatabase getRawReadableDatabase() {
        SQLiteDatabase readableDatabase = super.getReadableDatabase();
        Intrinsics.checkNotNullExpressionValue(readableDatabase, "super.getReadableDatabase()");
        return readableDatabase;
    }

    public SQLiteDatabase getRawWritableDatabase() {
        SQLiteDatabase writableDatabase = super.getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "super.getWritableDatabase()");
        return writableDatabase;
    }

    public SQLiteDatabase getSignalReadableDatabase() {
        return new SQLiteDatabase(super.getReadableDatabase());
    }

    public SQLiteDatabase getSignalWritableDatabase() {
        return new SQLiteDatabase(super.getWritableDatabase());
    }

    @Override // org.thoughtcrime.securesms.database.SignalDatabaseOpenHelper
    public SQLiteDatabase getSqlCipherDatabase() {
        SQLiteDatabase writableDatabase = super.getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "super.getWritableDatabase()");
        return writableDatabase;
    }

    public void markCurrent(SQLiteDatabase sQLiteDatabase) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
        sQLiteDatabase.setVersion(SignalDatabaseMigrations.DATABASE_VERSION);
    }

    private final void executeStatements(SQLiteDatabase sQLiteDatabase, String[] strArr) {
        for (String str : strArr) {
            sQLiteDatabase.execSQL(str);
        }
    }

    /* compiled from: SignalDatabase.kt */
    @Metadata(d1 = {"\u0000\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0013\u0010\u0001\u001a\u0002012\b\u0010\u0001\u001a\u00030\u0001H\u0007J\u0014\u0010\u0001\u001a\u00030\u00012\b\u0010\u0001\u001a\u00030\u0001H\u0007J\u0012\u0010\u0001\u001a\u0002012\u0007\u0010\u0001\u001a\u00020\u0004H\u0007J(\u0010\u0001\u001a\u00030\u00012\b\u0010\u0001\u001a\u00030\u00012\b\u0010\u0001\u001a\u00030\u00012\b\u0010\u0001\u001a\u00030\u0001H\u0007J4\u0010\u0001\u001a\u00030\u00012\b\u0010\u0001\u001a\u00030\u00012\b\u0010\u0001\u001a\u00030\u00012\b\u0010\u0001\u001a\u00030\u00012\n\u0010\u0001\u001a\u0005\u0018\u00010\u0001H\u0007J\u0014\u0010\u0001\u001a\u00030\u00012\b\u0010\u0001\u001a\u00030\u0001H\u0007J\u0014\u0010\u0001\u001a\u00030\u00012\b\u0010\u0001\u001a\u00030\u0001H\u0007J\u001d\u0010\u0001\u001a\u00030\u00012\u0007\u0010 \u0001\u001a\u00020\u00042\b\u0010\u0001\u001a\u00030\u0001H\u0007J\n\u0010¡\u0001\u001a\u00030\u0001H\u0007J\u0013\u0010¢\u0001\u001a\u00030\u00012\u0007\u0010£\u0001\u001a\u00020\u000eH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\b8G¢\u0006\u0006\u001a\u0004\b\u0007\u0010\tR\u0011\u0010\n\u001a\u00020\u000b8G¢\u0006\u0006\u001a\u0004\b\n\u0010\fR\u001a\u0010\r\u001a\u00020\u000e8FX\u0004¢\u0006\f\u0012\u0004\b\u000f\u0010\u0002\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u00138G¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0014R\u0011\u0010\u0015\u001a\u00020\u00168G¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0017R\u0011\u0010\u0018\u001a\u00020\u00198G¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c8G¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001dR\u0011\u0010\u001e\u001a\u00020\u001f8G¢\u0006\u0006\u001a\u0004\b\u001e\u0010 R\u0011\u0010!\u001a\u00020\"8G¢\u0006\u0006\u001a\u0004\b!\u0010#R\u0011\u0010$\u001a\u00020%8G¢\u0006\u0006\u001a\u0004\b$\u0010&R\u0011\u0010'\u001a\u00020(8G¢\u0006\u0006\u001a\u0004\b'\u0010)R\u0011\u0010*\u001a\u00020+8G¢\u0006\u0006\u001a\u0004\b*\u0010,R\u0011\u0010-\u001a\u00020.8G¢\u0006\u0006\u001a\u0004\b-\u0010/R\u001a\u00100\u001a\u0002018GX\u0004¢\u0006\f\u0012\u0004\b2\u0010\u0002\u001a\u0004\b0\u00103R*\u00106\u001a\u0004\u0018\u0001052\b\u00104\u001a\u0004\u0018\u0001058\u0006@BX\u000e¢\u0006\u000e\n\u0000\u0012\u0004\b7\u0010\u0002\u001a\u0004\b8\u00109R\u0011\u0010:\u001a\u00020;8G¢\u0006\u0006\u001a\u0004\b:\u0010<R\u0011\u0010=\u001a\u00020>8G¢\u0006\u0006\u001a\u0004\b=\u0010?R\u0011\u0010@\u001a\u00020A8G¢\u0006\u0006\u001a\u0004\b@\u0010BR\u0011\u0010C\u001a\u00020D8G¢\u0006\u0006\u001a\u0004\bC\u0010ER\u0011\u0010F\u001a\u00020G8G¢\u0006\u0006\u001a\u0004\bF\u0010HR\u0011\u0010I\u001a\u00020J8G¢\u0006\u0006\u001a\u0004\bI\u0010KR\u0011\u0010L\u001a\u00020M8G¢\u0006\u0006\u001a\u0004\bL\u0010NR\u0011\u0010O\u001a\u00020P8G¢\u0006\u0006\u001a\u0004\bO\u0010QR\u0011\u0010R\u001a\u00020S8G¢\u0006\u0006\u001a\u0004\bR\u0010TR\u0011\u0010U\u001a\u00020V8G¢\u0006\u0006\u001a\u0004\bU\u0010WR\u0011\u0010X\u001a\u00020Y8G¢\u0006\u0006\u001a\u0004\bX\u0010ZR\u001a\u0010[\u001a\u00020\u000e8FX\u0004¢\u0006\f\u0012\u0004\b\\\u0010\u0002\u001a\u0004\b]\u0010\u0011R\u0011\u0010^\u001a\u00020_8G¢\u0006\u0006\u001a\u0004\b^\u0010`R\u0011\u0010a\u001a\u00020b8G¢\u0006\u0006\u001a\u0004\ba\u0010cR\u0011\u0010d\u001a\u00020e8G¢\u0006\u0006\u001a\u0004\bd\u0010fR\u0011\u0010g\u001a\u00020h8G¢\u0006\u0006\u001a\u0004\bg\u0010iR\u0011\u0010j\u001a\u00020k8G¢\u0006\u0006\u001a\u0004\bj\u0010lR\u0011\u0010m\u001a\u00020n8G¢\u0006\u0006\u001a\u0004\bm\u0010oR\u0011\u0010p\u001a\u00020q8G¢\u0006\u0006\u001a\u0004\bp\u0010rR\u0011\u0010s\u001a\u00020t8G¢\u0006\u0006\u001a\u0004\bs\u0010uR\u0011\u0010v\u001a\u00020w8G¢\u0006\u0006\u001a\u0004\bv\u0010xR\u0011\u0010y\u001a\u00020z8G¢\u0006\u0006\u001a\u0004\by\u0010{R\u0011\u0010|\u001a\u00020}8G¢\u0006\u0006\u001a\u0004\b|\u0010~R\u0013\u0010\u001a\u00030\u00018G¢\u0006\u0007\u001a\u0005\b\u0010\u0001R\u0015\u0010\u0001\u001a\u00030\u00018G¢\u0006\b\u001a\u0006\b\u0001\u0010\u0001¨\u0006¤\u0001"}, d2 = {"Lorg/thoughtcrime/securesms/database/SignalDatabase$Companion;", "", "()V", "DATABASE_NAME", "", "TAG", "kotlin.jvm.PlatformType", "attachments", "Lorg/thoughtcrime/securesms/database/AttachmentDatabase;", "()Lorg/thoughtcrime/securesms/database/AttachmentDatabase;", "avatarPicker", "Lorg/thoughtcrime/securesms/database/model/AvatarPickerDatabase;", "()Lorg/thoughtcrime/securesms/database/model/AvatarPickerDatabase;", "backupDatabase", "Lnet/zetetic/database/sqlcipher/SQLiteDatabase;", "getBackupDatabase$annotations", "getBackupDatabase", "()Lnet/zetetic/database/sqlcipher/SQLiteDatabase;", CdsDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/database/CdsDatabase;", "()Lorg/thoughtcrime/securesms/database/CdsDatabase;", "chatColors", "Lorg/thoughtcrime/securesms/database/ChatColorsDatabase;", "()Lorg/thoughtcrime/securesms/database/ChatColorsDatabase;", "distributionLists", "Lorg/thoughtcrime/securesms/database/DistributionListDatabase;", "()Lorg/thoughtcrime/securesms/database/DistributionListDatabase;", "donationReceipts", "Lorg/thoughtcrime/securesms/database/DonationReceiptDatabase;", "()Lorg/thoughtcrime/securesms/database/DonationReceiptDatabase;", "drafts", "Lorg/thoughtcrime/securesms/database/DraftDatabase;", "()Lorg/thoughtcrime/securesms/database/DraftDatabase;", "emojiSearch", "Lorg/thoughtcrime/securesms/database/EmojiSearchDatabase;", "()Lorg/thoughtcrime/securesms/database/EmojiSearchDatabase;", "groupCallRings", "Lorg/thoughtcrime/securesms/database/GroupCallRingDatabase;", "()Lorg/thoughtcrime/securesms/database/GroupCallRingDatabase;", "groupReceipts", "Lorg/thoughtcrime/securesms/database/GroupReceiptDatabase;", "()Lorg/thoughtcrime/securesms/database/GroupReceiptDatabase;", ChooseGroupStoryBottomSheet.RESULT_SET, "Lorg/thoughtcrime/securesms/database/GroupDatabase;", "()Lorg/thoughtcrime/securesms/database/GroupDatabase;", "identities", "Lorg/thoughtcrime/securesms/database/IdentityDatabase;", "()Lorg/thoughtcrime/securesms/database/IdentityDatabase;", "inTransaction", "", "inTransaction$annotations", "()Z", "<set-?>", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "instance", "getInstance$annotations", "getInstance", "()Lorg/thoughtcrime/securesms/database/SignalDatabase;", "media", "Lorg/thoughtcrime/securesms/database/MediaDatabase;", "()Lorg/thoughtcrime/securesms/database/MediaDatabase;", "mentions", "Lorg/thoughtcrime/securesms/database/MentionDatabase;", "()Lorg/thoughtcrime/securesms/database/MentionDatabase;", "messageLog", "Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase;", "()Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase;", "messageSearch", "Lorg/thoughtcrime/securesms/database/SearchDatabase;", "()Lorg/thoughtcrime/securesms/database/SearchDatabase;", "mms", "Lorg/thoughtcrime/securesms/database/MmsDatabase;", "()Lorg/thoughtcrime/securesms/database/MmsDatabase;", "mmsSms", "Lorg/thoughtcrime/securesms/database/MmsSmsDatabase;", "()Lorg/thoughtcrime/securesms/database/MmsSmsDatabase;", "notificationProfiles", "Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase;", "()Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase;", "oneTimePreKeys", "Lorg/thoughtcrime/securesms/database/OneTimePreKeyDatabase;", "()Lorg/thoughtcrime/securesms/database/OneTimePreKeyDatabase;", PaymentDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/database/PaymentDatabase;", "()Lorg/thoughtcrime/securesms/database/PaymentDatabase;", "pendingRetryReceipts", "Lorg/thoughtcrime/securesms/database/PendingRetryReceiptDatabase;", "()Lorg/thoughtcrime/securesms/database/PendingRetryReceiptDatabase;", "push", "Lorg/thoughtcrime/securesms/database/PushDatabase;", "()Lorg/thoughtcrime/securesms/database/PushDatabase;", "rawDatabase", "getRawDatabase$annotations", "getRawDatabase", "reactions", "Lorg/thoughtcrime/securesms/database/ReactionDatabase;", "()Lorg/thoughtcrime/securesms/database/ReactionDatabase;", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "Lorg/thoughtcrime/securesms/database/RecipientDatabase;", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase;", "remappedRecords", "Lorg/thoughtcrime/securesms/database/RemappedRecordsDatabase;", "()Lorg/thoughtcrime/securesms/database/RemappedRecordsDatabase;", "remoteMegaphones", "Lorg/thoughtcrime/securesms/database/RemoteMegaphoneDatabase;", "()Lorg/thoughtcrime/securesms/database/RemoteMegaphoneDatabase;", "senderKeyShared", "Lorg/thoughtcrime/securesms/database/SenderKeySharedDatabase;", "()Lorg/thoughtcrime/securesms/database/SenderKeySharedDatabase;", "senderKeys", "Lorg/thoughtcrime/securesms/database/SenderKeyDatabase;", "()Lorg/thoughtcrime/securesms/database/SenderKeyDatabase;", SessionDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/database/SessionDatabase;", "()Lorg/thoughtcrime/securesms/database/SessionDatabase;", "signedPreKeys", "Lorg/thoughtcrime/securesms/database/SignedPreKeyDatabase;", "()Lorg/thoughtcrime/securesms/database/SignedPreKeyDatabase;", "sms", "Lorg/thoughtcrime/securesms/database/SmsDatabase;", "()Lorg/thoughtcrime/securesms/database/SmsDatabase;", StickerDatabase.DIRECTORY, "Lorg/thoughtcrime/securesms/database/StickerDatabase;", "()Lorg/thoughtcrime/securesms/database/StickerDatabase;", "storySends", "Lorg/thoughtcrime/securesms/database/StorySendsDatabase;", "()Lorg/thoughtcrime/securesms/database/StorySendsDatabase;", "threads", "Lorg/thoughtcrime/securesms/database/ThreadDatabase;", "()Lorg/thoughtcrime/securesms/database/ThreadDatabase;", "unknownStorageIds", "Lorg/thoughtcrime/securesms/database/UnknownStorageIdDatabase;", "()Lorg/thoughtcrime/securesms/database/UnknownStorageIdDatabase;", "databaseFileExists", "context", "Landroid/content/Context;", "getDatabaseFile", "Ljava/io/File;", "hasTable", "table", "init", "", "application", "Landroid/app/Application;", "databaseSecret", "Lorg/thoughtcrime/securesms/crypto/DatabaseSecret;", "attachmentSecret", "Lorg/thoughtcrime/securesms/crypto/AttachmentSecret;", "onApplicationLevelUpgrade", "masterSecret", "Lorg/thoughtcrime/securesms/crypto/MasterSecret;", "fromVersion", "", "listener", "Lorg/thoughtcrime/securesms/migrations/LegacyMigrationJob$DatabaseUpgradeListener;", "runInTransaction", "operation", "Ljava/lang/Runnable;", "runPostSuccessfulTransaction", "task", "dedupeKey", "triggerDatabaseAccess", "upgradeRestored", "database", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public static /* synthetic */ void getBackupDatabase$annotations() {
        }

        @JvmStatic
        public static /* synthetic */ void getInstance$annotations() {
        }

        @JvmStatic
        public static /* synthetic */ void getRawDatabase$annotations() {
        }

        @JvmStatic
        public static /* synthetic */ void inTransaction$annotations() {
        }

        private Companion() {
        }

        public final SignalDatabase getInstance() {
            return SignalDatabase.instance;
        }

        @JvmStatic
        public final void init(Application application, DatabaseSecret databaseSecret, AttachmentSecret attachmentSecret) {
            Intrinsics.checkNotNullParameter(application, "application");
            Intrinsics.checkNotNullParameter(databaseSecret, "databaseSecret");
            Intrinsics.checkNotNullParameter(attachmentSecret, "attachmentSecret");
            if (getInstance() == null) {
                synchronized (SignalDatabase.class) {
                    Companion companion = SignalDatabase.Companion;
                    if (companion.getInstance() == null) {
                        SignalDatabase.instance = new SignalDatabase(application, databaseSecret, attachmentSecret);
                        SignalDatabase instance = companion.getInstance();
                        Intrinsics.checkNotNull(instance);
                        instance.setWriteAheadLoggingEnabled(true);
                    }
                    Unit unit = Unit.INSTANCE;
                }
            }
        }

        public final SQLiteDatabase getRawDatabase() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getRawWritableDatabase();
        }

        public final SQLiteDatabase getBackupDatabase() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getRawReadableDatabase();
        }

        public final boolean inTransaction() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getRawWritableDatabase().inTransaction();
        }

        @JvmStatic
        public final void runPostSuccessfulTransaction(String str, Runnable runnable) {
            Intrinsics.checkNotNullParameter(str, "dedupeKey");
            Intrinsics.checkNotNullParameter(runnable, "task");
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            instance.getSignalReadableDatabase().runPostSuccessfulTransaction(str, runnable);
        }

        @JvmStatic
        public final void runPostSuccessfulTransaction(Runnable runnable) {
            Intrinsics.checkNotNullParameter(runnable, "task");
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            instance.getSignalReadableDatabase().runPostSuccessfulTransaction(runnable);
        }

        @JvmStatic
        public final boolean databaseFileExists(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return context.getDatabasePath(SignalDatabase.DATABASE_NAME).exists();
        }

        @JvmStatic
        public final File getDatabaseFile(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            File databasePath = context.getDatabasePath(SignalDatabase.DATABASE_NAME);
            Intrinsics.checkNotNullExpressionValue(databasePath, "context.getDatabasePath(DATABASE_NAME)");
            return databasePath;
        }

        @JvmStatic
        public final void upgradeRestored(SQLiteDatabase sQLiteDatabase) {
            Intrinsics.checkNotNullParameter(sQLiteDatabase, "database");
            synchronized (SignalDatabase.class) {
                Companion companion = SignalDatabase.Companion;
                SignalDatabase instance = companion.getInstance();
                Intrinsics.checkNotNull(instance);
                instance.onUpgrade(sQLiteDatabase, sQLiteDatabase.getVersion(), -1);
                SignalDatabase instance2 = companion.getInstance();
                Intrinsics.checkNotNull(instance2);
                instance2.markCurrent(sQLiteDatabase);
                SignalDatabase instance3 = companion.getInstance();
                Intrinsics.checkNotNull(instance3);
                instance3.getSms().deleteAbandonedMessages();
                SignalDatabase instance4 = companion.getInstance();
                Intrinsics.checkNotNull(instance4);
                instance4.getMms().deleteAbandonedMessages();
                SignalDatabase instance5 = companion.getInstance();
                Intrinsics.checkNotNull(instance5);
                instance5.getMms().trimEntriesForExpiredMessages();
                SignalDatabase instance6 = companion.getInstance();
                Intrinsics.checkNotNull(instance6);
                instance6.getReactionDatabase().deleteAbandonedReactions();
                SignalDatabase instance7 = companion.getInstance();
                Intrinsics.checkNotNull(instance7);
                instance7.getRawWritableDatabase().execSQL("DROP TABLE IF EXISTS key_value");
                SignalDatabase instance8 = companion.getInstance();
                Intrinsics.checkNotNull(instance8);
                instance8.getRawWritableDatabase().execSQL("DROP TABLE IF EXISTS megaphone");
                SignalDatabase instance9 = companion.getInstance();
                Intrinsics.checkNotNull(instance9);
                instance9.getRawWritableDatabase().execSQL("DROP TABLE IF EXISTS job_spec");
                SignalDatabase instance10 = companion.getInstance();
                Intrinsics.checkNotNull(instance10);
                instance10.getRawWritableDatabase().execSQL("DROP TABLE IF EXISTS constraint_spec");
                SignalDatabase instance11 = companion.getInstance();
                Intrinsics.checkNotNull(instance11);
                instance11.getRawWritableDatabase().execSQL("DROP TABLE IF EXISTS dependency_spec");
                SignalDatabase instance12 = companion.getInstance();
                Intrinsics.checkNotNull(instance12);
                instance12.getRawWritableDatabase().close();
                companion.triggerDatabaseAccess();
                Unit unit = Unit.INSTANCE;
            }
        }

        @JvmStatic
        public final boolean hasTable(String str) {
            Intrinsics.checkNotNullParameter(str, "table");
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return SqlUtil.tableExists(instance.getRawReadableDatabase(), str);
        }

        @JvmStatic
        public final void triggerDatabaseAccess() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            instance.getSignalWritableDatabase();
        }

        @JvmStatic
        public final void onApplicationLevelUpgrade(Context context, MasterSecret masterSecret, int i, LegacyMigrationJob.DatabaseUpgradeListener databaseUpgradeListener) {
            ClassicOpenHelper classicOpenHelper;
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(masterSecret, "masterSecret");
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            instance.getSignalWritableDatabase();
            if (i < 73) {
                classicOpenHelper = new ClassicOpenHelper(context);
                classicOpenHelper.onApplicationLevelUpgrade(context, masterSecret, i, databaseUpgradeListener);
            } else {
                classicOpenHelper = null;
            }
            if (i < 334 && TextSecurePreferences.getNeedsSqlCipherMigration(context)) {
                if (classicOpenHelper == null) {
                    classicOpenHelper = new ClassicOpenHelper(context);
                }
                android.database.sqlite.SQLiteDatabase writableDatabase = classicOpenHelper.getWritableDatabase();
                SignalDatabase instance2 = getInstance();
                Intrinsics.checkNotNull(instance2);
                SQLCipherMigrationHelper.migrateCiphertext(context, masterSecret, writableDatabase, instance2.getRawWritableDatabase(), databaseUpgradeListener);
            }
        }

        @JvmStatic
        public final void runInTransaction(Runnable runnable) {
            Intrinsics.checkNotNullParameter(runnable, "operation");
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            instance.getSignalWritableDatabase().beginTransaction();
            try {
                runnable.run();
                SignalDatabase instance2 = getInstance();
                Intrinsics.checkNotNull(instance2);
                instance2.getSignalWritableDatabase().setTransactionSuccessful();
            } finally {
                SignalDatabase instance3 = getInstance();
                Intrinsics.checkNotNull(instance3);
                instance3.getSignalWritableDatabase().endTransaction();
            }
        }

        @JvmStatic
        public final AttachmentDatabase attachments() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getAttachments();
        }

        @JvmStatic
        public final AvatarPickerDatabase avatarPicker() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getAvatarPickerDatabase();
        }

        @JvmStatic
        public final CdsDatabase cds() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getCdsDatabase();
        }

        @JvmStatic
        public final ChatColorsDatabase chatColors() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getChatColorsDatabase();
        }

        @JvmStatic
        public final DistributionListDatabase distributionLists() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getDistributionListDatabase();
        }

        @JvmStatic
        public final DonationReceiptDatabase donationReceipts() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getDonationReceiptDatabase();
        }

        @JvmStatic
        public final DraftDatabase drafts() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getDraftDatabase();
        }

        @JvmStatic
        public final EmojiSearchDatabase emojiSearch() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getEmojiSearchDatabase();
        }

        @JvmStatic
        public final GroupCallRingDatabase groupCallRings() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getGroupCallRingDatabase();
        }

        @JvmStatic
        public final GroupReceiptDatabase groupReceipts() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getGroupReceiptDatabase();
        }

        @JvmStatic
        public final GroupDatabase groups() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getGroupDatabase();
        }

        @JvmStatic
        public final IdentityDatabase identities() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getIdentityDatabase();
        }

        @JvmStatic
        public final MediaDatabase media() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getMedia();
        }

        @JvmStatic
        public final MentionDatabase mentions() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getMentionDatabase();
        }

        @JvmStatic
        public final SearchDatabase messageSearch() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getSearchDatabase();
        }

        @JvmStatic
        public final MessageSendLogDatabase messageLog() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getMessageSendLogDatabase();
        }

        @JvmStatic
        public final MmsDatabase mms() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getMms();
        }

        @JvmStatic
        public final MmsSmsDatabase mmsSms() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getMmsSmsDatabase();
        }

        @JvmStatic
        public final NotificationProfileDatabase notificationProfiles() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getNotificationProfileDatabase();
        }

        @JvmStatic
        public final PaymentDatabase payments() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getPaymentDatabase();
        }

        @JvmStatic
        public final PendingRetryReceiptDatabase pendingRetryReceipts() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getPendingRetryReceiptDatabase();
        }

        @JvmStatic
        public final OneTimePreKeyDatabase oneTimePreKeys() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getPreKeyDatabase();
        }

        @JvmStatic
        public final PushDatabase push() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getPushDatabase();
        }

        @JvmStatic
        public final RecipientDatabase recipients() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getRecipientDatabase();
        }

        @JvmStatic
        public final SignedPreKeyDatabase signedPreKeys() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getSignedPreKeyDatabase();
        }

        @JvmStatic
        public final SmsDatabase sms() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getSms();
        }

        @JvmStatic
        public final ThreadDatabase threads() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getThread();
        }

        @JvmStatic
        public final ReactionDatabase reactions() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getReactionDatabase();
        }

        @JvmStatic
        public final RemappedRecordsDatabase remappedRecords() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getRemappedRecordsDatabase();
        }

        @JvmStatic
        public final SenderKeyDatabase senderKeys() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getSenderKeyDatabase();
        }

        @JvmStatic
        public final SenderKeySharedDatabase senderKeyShared() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getSenderKeySharedDatabase();
        }

        @JvmStatic
        public final SessionDatabase sessions() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getSessionDatabase();
        }

        @JvmStatic
        public final StickerDatabase stickers() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getStickerDatabase();
        }

        @JvmStatic
        public final StorySendsDatabase storySends() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getStorySendsDatabase();
        }

        @JvmStatic
        public final UnknownStorageIdDatabase unknownStorageIds() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getStorageIdDatabase();
        }

        @JvmStatic
        public final RemoteMegaphoneDatabase remoteMegaphones() {
            SignalDatabase instance = getInstance();
            Intrinsics.checkNotNull(instance);
            return instance.getRemoteMegaphoneDatabase();
        }
    }
}
