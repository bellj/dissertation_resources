package org.thoughtcrime.securesms.database.model;

import org.thoughtcrime.securesms.database.MmsSmsColumns;

/* loaded from: classes4.dex */
public final class StatusUtil {
    public static boolean isDelivered(long j, int i) {
        return (j >= 0 && j < 32) || i > 0;
    }

    private StatusUtil() {
    }

    public static boolean isPending(long j) {
        return MmsSmsColumns.Types.isPendingMessageType(j) && !MmsSmsColumns.Types.isIdentityVerified(j) && !MmsSmsColumns.Types.isIdentityDefault(j);
    }

    public static boolean isFailed(long j, long j2) {
        return MmsSmsColumns.Types.isFailedMessageType(j) || MmsSmsColumns.Types.isPendingSecureSmsFallbackType(j) || j2 >= 64;
    }

    public static boolean isVerificationStatusChange(long j) {
        return MmsSmsColumns.Types.isIdentityDefault(j) || MmsSmsColumns.Types.isIdentityVerified(j);
    }
}
