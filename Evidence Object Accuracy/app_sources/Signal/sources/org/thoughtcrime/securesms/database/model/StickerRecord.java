package org.thoughtcrime.securesms.database.model;

import android.net.Uri;
import java.util.Objects;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class StickerRecord {
    private final String contentType;
    private final String emoji;
    private final boolean isCover;
    private final String packId;
    private final String packKey;
    private final long rowId;
    private final long size;
    private final int stickerId;

    public StickerRecord(long j, String str, String str2, int i, String str3, String str4, long j2, boolean z) {
        this.rowId = j;
        this.packId = str;
        this.packKey = str2;
        this.stickerId = i;
        this.emoji = str3;
        this.contentType = str4;
        this.size = j2;
        this.isCover = z;
    }

    public long getRowId() {
        return this.rowId;
    }

    public String getPackId() {
        return this.packId;
    }

    public String getPackKey() {
        return this.packKey;
    }

    public int getStickerId() {
        return this.stickerId;
    }

    public Uri getUri() {
        return PartAuthority.getStickerUri(this.rowId);
    }

    public String getEmoji() {
        return this.emoji;
    }

    public String getContentType() {
        return Util.isEmpty(this.contentType) ? MediaUtil.IMAGE_WEBP : this.contentType;
    }

    public long getSize() {
        return this.size;
    }

    public boolean isCover() {
        return this.isCover;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || StickerRecord.class != obj.getClass()) {
            return false;
        }
        StickerRecord stickerRecord = (StickerRecord) obj;
        if (this.rowId != stickerRecord.rowId || this.stickerId != stickerRecord.stickerId || this.size != stickerRecord.size || this.isCover != stickerRecord.isCover || !this.packId.equals(stickerRecord.packId) || !this.packKey.equals(stickerRecord.packKey) || !this.emoji.equals(stickerRecord.emoji) || !Objects.equals(this.contentType, stickerRecord.contentType)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(Long.valueOf(this.rowId), this.packId, this.packKey, Integer.valueOf(this.stickerId), this.emoji, this.contentType, Long.valueOf(this.size), Boolean.valueOf(this.isCover));
    }
}
