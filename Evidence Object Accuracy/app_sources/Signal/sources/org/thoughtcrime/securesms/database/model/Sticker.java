package org.thoughtcrime.securesms.database.model;

import org.thoughtcrime.securesms.attachments.Attachment;

/* loaded from: classes4.dex */
public class Sticker {
    private final Attachment attachment;
    private final String packId;
    private final String packKey;
    private final int stickerId;

    public Sticker(String str, String str2, int i, Attachment attachment) {
        this.packId = str;
        this.packKey = str2;
        this.stickerId = i;
        this.attachment = attachment;
    }

    public String getPackId() {
        return this.packId;
    }

    public String getPackKey() {
        return this.packKey;
    }

    public int getStickerId() {
        return this.stickerId;
    }

    public Attachment getAttachment() {
        return this.attachment;
    }
}
