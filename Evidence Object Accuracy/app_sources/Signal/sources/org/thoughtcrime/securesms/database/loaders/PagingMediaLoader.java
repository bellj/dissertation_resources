package org.thoughtcrime.securesms.database.loaders;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import androidx.core.util.Pair;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.util.AsyncLoader;

/* loaded from: classes4.dex */
public final class PagingMediaLoader extends AsyncLoader<Pair<Cursor, Integer>> {
    private static final String TAG = Log.tag(PagingMediaLoader.class);
    private final boolean leftIsRecent;
    private final DatabaseObserver.Observer observer = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.database.loaders.PagingMediaLoader$$ExternalSyntheticLambda1
        @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
        public final void onChanged() {
            PagingMediaLoader.this.lambda$new$0();
        }
    };
    private final MediaDatabase.Sorting sorting;
    private final long threadId;
    private final Uri uri;

    public PagingMediaLoader(Context context, long j, Uri uri, boolean z, MediaDatabase.Sorting sorting) {
        super(context);
        this.threadId = j;
        this.uri = uri;
        this.leftIsRecent = z;
        this.sorting = sorting;
    }

    public /* synthetic */ void lambda$new$0() {
        ThreadUtil.runOnMain(new Runnable() { // from class: org.thoughtcrime.securesms.database.loaders.PagingMediaLoader$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                PagingMediaLoader.this.onContentChanged();
            }
        });
    }

    @Override // androidx.loader.content.AsyncTaskLoader
    public Pair<Cursor, Integer> loadInBackground() {
        ApplicationDependencies.getDatabaseObserver().registerAttachmentObserver(this.observer);
        MediaDatabase media = SignalDatabase.media();
        long j = this.threadId;
        Cursor galleryMediaForThread = media.getGalleryMediaForThread(j, this.sorting, j == -1);
        while (galleryMediaForThread.moveToNext()) {
            if (PartAuthority.getAttachmentDataUri(new AttachmentId(galleryMediaForThread.getLong(galleryMediaForThread.getColumnIndexOrThrow("_id")), galleryMediaForThread.getLong(galleryMediaForThread.getColumnIndexOrThrow(AttachmentDatabase.UNIQUE_ID)))).equals(this.uri)) {
                return new Pair<>(galleryMediaForThread, Integer.valueOf(this.leftIsRecent ? galleryMediaForThread.getPosition() : (galleryMediaForThread.getCount() - 1) - galleryMediaForThread.getPosition()));
            }
        }
        return null;
    }

    @Override // androidx.loader.content.Loader
    protected void onAbandon() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
    }
}
