package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public final class BodyRangeList extends GeneratedMessageLite<BodyRangeList, Builder> implements BodyRangeListOrBuilder {
    private static final BodyRangeList DEFAULT_INSTANCE;
    private static volatile Parser<BodyRangeList> PARSER;
    public static final int RANGES_FIELD_NUMBER;
    private Internal.ProtobufList<BodyRange> ranges_ = GeneratedMessageLite.emptyProtobufList();

    /* loaded from: classes4.dex */
    public interface BodyRangeOrBuilder extends MessageLiteOrBuilder {
        BodyRange.AssociatedValueCase getAssociatedValueCase();

        BodyRange.Button getButton();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        int getLength();

        String getLink();

        ByteString getLinkBytes();

        String getMentionUuid();

        ByteString getMentionUuidBytes();

        int getStart();

        BodyRange.Style getStyle();

        int getStyleValue();

        boolean hasButton();

        boolean hasLink();

        boolean hasMentionUuid();

        boolean hasStyle();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private BodyRangeList() {
    }

    /* loaded from: classes4.dex */
    public static final class BodyRange extends GeneratedMessageLite<BodyRange, Builder> implements BodyRangeOrBuilder {
        public static final int BUTTON_FIELD_NUMBER;
        private static final BodyRange DEFAULT_INSTANCE;
        public static final int LENGTH_FIELD_NUMBER;
        public static final int LINK_FIELD_NUMBER;
        public static final int MENTIONUUID_FIELD_NUMBER;
        private static volatile Parser<BodyRange> PARSER;
        public static final int START_FIELD_NUMBER;
        public static final int STYLE_FIELD_NUMBER;
        private int associatedValueCase_ = 0;
        private Object associatedValue_;
        private int length_;
        private int start_;

        /* loaded from: classes4.dex */
        public interface ButtonOrBuilder extends MessageLiteOrBuilder {
            String getAction();

            ByteString getActionBytes();

            @Override // com.google.protobuf.MessageLiteOrBuilder
            /* synthetic */ MessageLite getDefaultInstanceForType();

            String getLabel();

            ByteString getLabelBytes();

            @Override // com.google.protobuf.MessageLiteOrBuilder
            /* synthetic */ boolean isInitialized();
        }

        private BodyRange() {
        }

        /* loaded from: classes4.dex */
        public enum Style implements Internal.EnumLite {
            BOLD(0),
            ITALIC(1),
            UNRECOGNIZED(-1);
            
            public static final int BOLD_VALUE;
            public static final int ITALIC_VALUE;
            private static final Internal.EnumLiteMap<Style> internalValueMap = new Internal.EnumLiteMap<Style>() { // from class: org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRange.Style.1
                @Override // com.google.protobuf.Internal.EnumLiteMap
                public Style findValueByNumber(int i) {
                    return Style.forNumber(i);
                }
            };
            private final int value;

            @Override // com.google.protobuf.Internal.EnumLite
            public final int getNumber() {
                if (this != UNRECOGNIZED) {
                    return this.value;
                }
                throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
            }

            @Deprecated
            public static Style valueOf(int i) {
                return forNumber(i);
            }

            public static Style forNumber(int i) {
                if (i == 0) {
                    return BOLD;
                }
                if (i != 1) {
                    return null;
                }
                return ITALIC;
            }

            public static Internal.EnumLiteMap<Style> internalGetValueMap() {
                return internalValueMap;
            }

            public static Internal.EnumVerifier internalGetVerifier() {
                return StyleVerifier.INSTANCE;
            }

            /* loaded from: classes4.dex */
            private static final class StyleVerifier implements Internal.EnumVerifier {
                static final Internal.EnumVerifier INSTANCE = new StyleVerifier();

                private StyleVerifier() {
                }

                @Override // com.google.protobuf.Internal.EnumVerifier
                public boolean isInRange(int i) {
                    return Style.forNumber(i) != null;
                }
            }

            Style(int i) {
                this.value = i;
            }
        }

        /* loaded from: classes4.dex */
        public static final class Button extends GeneratedMessageLite<Button, Builder> implements ButtonOrBuilder {
            public static final int ACTION_FIELD_NUMBER;
            private static final Button DEFAULT_INSTANCE;
            public static final int LABEL_FIELD_NUMBER;
            private static volatile Parser<Button> PARSER;
            private String action_ = "";
            private String label_ = "";

            private Button() {
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRange.ButtonOrBuilder
            public String getLabel() {
                return this.label_;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRange.ButtonOrBuilder
            public ByteString getLabelBytes() {
                return ByteString.copyFromUtf8(this.label_);
            }

            public void setLabel(String str) {
                str.getClass();
                this.label_ = str;
            }

            public void clearLabel() {
                this.label_ = getDefaultInstance().getLabel();
            }

            public void setLabelBytes(ByteString byteString) {
                AbstractMessageLite.checkByteStringIsUtf8(byteString);
                this.label_ = byteString.toStringUtf8();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRange.ButtonOrBuilder
            public String getAction() {
                return this.action_;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRange.ButtonOrBuilder
            public ByteString getActionBytes() {
                return ByteString.copyFromUtf8(this.action_);
            }

            public void setAction(String str) {
                str.getClass();
                this.action_ = str;
            }

            public void clearAction() {
                this.action_ = getDefaultInstance().getAction();
            }

            public void setActionBytes(ByteString byteString) {
                AbstractMessageLite.checkByteStringIsUtf8(byteString);
                this.action_ = byteString.toStringUtf8();
            }

            public static Button parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (Button) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static Button parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (Button) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static Button parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (Button) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static Button parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (Button) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static Button parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (Button) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static Button parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (Button) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static Button parseFrom(InputStream inputStream) throws IOException {
                return (Button) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static Button parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (Button) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static Button parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Button) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static Button parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (Button) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static Button parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (Button) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static Button parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (Button) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(Button button) {
                return DEFAULT_INSTANCE.createBuilder(button);
            }

            /* loaded from: classes4.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<Button, Builder> implements ButtonOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(Button.DEFAULT_INSTANCE);
                }

                @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRange.ButtonOrBuilder
                public String getLabel() {
                    return ((Button) this.instance).getLabel();
                }

                @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRange.ButtonOrBuilder
                public ByteString getLabelBytes() {
                    return ((Button) this.instance).getLabelBytes();
                }

                public Builder setLabel(String str) {
                    copyOnWrite();
                    ((Button) this.instance).setLabel(str);
                    return this;
                }

                public Builder clearLabel() {
                    copyOnWrite();
                    ((Button) this.instance).clearLabel();
                    return this;
                }

                public Builder setLabelBytes(ByteString byteString) {
                    copyOnWrite();
                    ((Button) this.instance).setLabelBytes(byteString);
                    return this;
                }

                @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRange.ButtonOrBuilder
                public String getAction() {
                    return ((Button) this.instance).getAction();
                }

                @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRange.ButtonOrBuilder
                public ByteString getActionBytes() {
                    return ((Button) this.instance).getActionBytes();
                }

                public Builder setAction(String str) {
                    copyOnWrite();
                    ((Button) this.instance).setAction(str);
                    return this;
                }

                public Builder clearAction() {
                    copyOnWrite();
                    ((Button) this.instance).clearAction();
                    return this;
                }

                public Builder setActionBytes(ByteString byteString) {
                    copyOnWrite();
                    ((Button) this.instance).setActionBytes(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new Button();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001Ȉ\u0002Ȉ", new Object[]{"label_", "action_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<Button> parser = PARSER;
                        if (parser == null) {
                            synchronized (Button.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                Button button = new Button();
                DEFAULT_INSTANCE = button;
                GeneratedMessageLite.registerDefaultInstance(Button.class, button);
            }

            public static Button getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<Button> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes4.dex */
        public enum AssociatedValueCase {
            MENTIONUUID(3),
            STYLE(4),
            LINK(5),
            BUTTON(6),
            ASSOCIATEDVALUE_NOT_SET(0);
            
            private final int value;

            AssociatedValueCase(int i) {
                this.value = i;
            }

            @Deprecated
            public static AssociatedValueCase valueOf(int i) {
                return forNumber(i);
            }

            public static AssociatedValueCase forNumber(int i) {
                if (i == 0) {
                    return ASSOCIATEDVALUE_NOT_SET;
                }
                if (i == 3) {
                    return MENTIONUUID;
                }
                if (i == 4) {
                    return STYLE;
                }
                if (i == 5) {
                    return LINK;
                }
                if (i != 6) {
                    return null;
                }
                return BUTTON;
            }

            public int getNumber() {
                return this.value;
            }
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public AssociatedValueCase getAssociatedValueCase() {
            return AssociatedValueCase.forNumber(this.associatedValueCase_);
        }

        public void clearAssociatedValue() {
            this.associatedValueCase_ = 0;
            this.associatedValue_ = null;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public int getStart() {
            return this.start_;
        }

        public void setStart(int i) {
            this.start_ = i;
        }

        public void clearStart() {
            this.start_ = 0;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public int getLength() {
            return this.length_;
        }

        public void setLength(int i) {
            this.length_ = i;
        }

        public void clearLength() {
            this.length_ = 0;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public boolean hasMentionUuid() {
            return this.associatedValueCase_ == 3;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public String getMentionUuid() {
            return this.associatedValueCase_ == 3 ? (String) this.associatedValue_ : "";
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public ByteString getMentionUuidBytes() {
            return ByteString.copyFromUtf8(this.associatedValueCase_ == 3 ? (String) this.associatedValue_ : "");
        }

        public void setMentionUuid(String str) {
            str.getClass();
            this.associatedValueCase_ = 3;
            this.associatedValue_ = str;
        }

        public void clearMentionUuid() {
            if (this.associatedValueCase_ == 3) {
                this.associatedValueCase_ = 0;
                this.associatedValue_ = null;
            }
        }

        public void setMentionUuidBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.associatedValue_ = byteString.toStringUtf8();
            this.associatedValueCase_ = 3;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public boolean hasStyle() {
            return this.associatedValueCase_ == 4;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public int getStyleValue() {
            if (this.associatedValueCase_ == 4) {
                return ((Integer) this.associatedValue_).intValue();
            }
            return 0;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public Style getStyle() {
            if (this.associatedValueCase_ != 4) {
                return Style.BOLD;
            }
            Style forNumber = Style.forNumber(((Integer) this.associatedValue_).intValue());
            return forNumber == null ? Style.UNRECOGNIZED : forNumber;
        }

        public void setStyleValue(int i) {
            this.associatedValueCase_ = 4;
            this.associatedValue_ = Integer.valueOf(i);
        }

        public void setStyle(Style style) {
            this.associatedValue_ = Integer.valueOf(style.getNumber());
            this.associatedValueCase_ = 4;
        }

        public void clearStyle() {
            if (this.associatedValueCase_ == 4) {
                this.associatedValueCase_ = 0;
                this.associatedValue_ = null;
            }
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public boolean hasLink() {
            return this.associatedValueCase_ == 5;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public String getLink() {
            return this.associatedValueCase_ == 5 ? (String) this.associatedValue_ : "";
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public ByteString getLinkBytes() {
            return ByteString.copyFromUtf8(this.associatedValueCase_ == 5 ? (String) this.associatedValue_ : "");
        }

        public void setLink(String str) {
            str.getClass();
            this.associatedValueCase_ = 5;
            this.associatedValue_ = str;
        }

        public void clearLink() {
            if (this.associatedValueCase_ == 5) {
                this.associatedValueCase_ = 0;
                this.associatedValue_ = null;
            }
        }

        public void setLinkBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.associatedValue_ = byteString.toStringUtf8();
            this.associatedValueCase_ = 5;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public boolean hasButton() {
            return this.associatedValueCase_ == 6;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
        public Button getButton() {
            if (this.associatedValueCase_ == 6) {
                return (Button) this.associatedValue_;
            }
            return Button.getDefaultInstance();
        }

        public void setButton(Button button) {
            button.getClass();
            this.associatedValue_ = button;
            this.associatedValueCase_ = 6;
        }

        public void mergeButton(Button button) {
            button.getClass();
            if (this.associatedValueCase_ != 6 || this.associatedValue_ == Button.getDefaultInstance()) {
                this.associatedValue_ = button;
            } else {
                this.associatedValue_ = Button.newBuilder((Button) this.associatedValue_).mergeFrom((Button.Builder) button).buildPartial();
            }
            this.associatedValueCase_ = 6;
        }

        public void clearButton() {
            if (this.associatedValueCase_ == 6) {
                this.associatedValueCase_ = 0;
                this.associatedValue_ = null;
            }
        }

        public static BodyRange parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (BodyRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static BodyRange parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (BodyRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static BodyRange parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (BodyRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static BodyRange parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (BodyRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static BodyRange parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (BodyRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static BodyRange parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (BodyRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static BodyRange parseFrom(InputStream inputStream) throws IOException {
            return (BodyRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static BodyRange parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (BodyRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static BodyRange parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (BodyRange) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static BodyRange parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (BodyRange) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static BodyRange parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (BodyRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static BodyRange parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (BodyRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(BodyRange bodyRange) {
            return DEFAULT_INSTANCE.createBuilder(bodyRange);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<BodyRange, Builder> implements BodyRangeOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(BodyRange.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public AssociatedValueCase getAssociatedValueCase() {
                return ((BodyRange) this.instance).getAssociatedValueCase();
            }

            public Builder clearAssociatedValue() {
                copyOnWrite();
                ((BodyRange) this.instance).clearAssociatedValue();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public int getStart() {
                return ((BodyRange) this.instance).getStart();
            }

            public Builder setStart(int i) {
                copyOnWrite();
                ((BodyRange) this.instance).setStart(i);
                return this;
            }

            public Builder clearStart() {
                copyOnWrite();
                ((BodyRange) this.instance).clearStart();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public int getLength() {
                return ((BodyRange) this.instance).getLength();
            }

            public Builder setLength(int i) {
                copyOnWrite();
                ((BodyRange) this.instance).setLength(i);
                return this;
            }

            public Builder clearLength() {
                copyOnWrite();
                ((BodyRange) this.instance).clearLength();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public boolean hasMentionUuid() {
                return ((BodyRange) this.instance).hasMentionUuid();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public String getMentionUuid() {
                return ((BodyRange) this.instance).getMentionUuid();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public ByteString getMentionUuidBytes() {
                return ((BodyRange) this.instance).getMentionUuidBytes();
            }

            public Builder setMentionUuid(String str) {
                copyOnWrite();
                ((BodyRange) this.instance).setMentionUuid(str);
                return this;
            }

            public Builder clearMentionUuid() {
                copyOnWrite();
                ((BodyRange) this.instance).clearMentionUuid();
                return this;
            }

            public Builder setMentionUuidBytes(ByteString byteString) {
                copyOnWrite();
                ((BodyRange) this.instance).setMentionUuidBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public boolean hasStyle() {
                return ((BodyRange) this.instance).hasStyle();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public int getStyleValue() {
                return ((BodyRange) this.instance).getStyleValue();
            }

            public Builder setStyleValue(int i) {
                copyOnWrite();
                ((BodyRange) this.instance).setStyleValue(i);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public Style getStyle() {
                return ((BodyRange) this.instance).getStyle();
            }

            public Builder setStyle(Style style) {
                copyOnWrite();
                ((BodyRange) this.instance).setStyle(style);
                return this;
            }

            public Builder clearStyle() {
                copyOnWrite();
                ((BodyRange) this.instance).clearStyle();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public boolean hasLink() {
                return ((BodyRange) this.instance).hasLink();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public String getLink() {
                return ((BodyRange) this.instance).getLink();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public ByteString getLinkBytes() {
                return ((BodyRange) this.instance).getLinkBytes();
            }

            public Builder setLink(String str) {
                copyOnWrite();
                ((BodyRange) this.instance).setLink(str);
                return this;
            }

            public Builder clearLink() {
                copyOnWrite();
                ((BodyRange) this.instance).clearLink();
                return this;
            }

            public Builder setLinkBytes(ByteString byteString) {
                copyOnWrite();
                ((BodyRange) this.instance).setLinkBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public boolean hasButton() {
                return ((BodyRange) this.instance).hasButton();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList.BodyRangeOrBuilder
            public Button getButton() {
                return ((BodyRange) this.instance).getButton();
            }

            public Builder setButton(Button button) {
                copyOnWrite();
                ((BodyRange) this.instance).setButton(button);
                return this;
            }

            public Builder setButton(Button.Builder builder) {
                copyOnWrite();
                ((BodyRange) this.instance).setButton(builder.build());
                return this;
            }

            public Builder mergeButton(Button button) {
                copyOnWrite();
                ((BodyRange) this.instance).mergeButton(button);
                return this;
            }

            public Builder clearButton() {
                copyOnWrite();
                ((BodyRange) this.instance).clearButton();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new BodyRange();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0006\u0001\u0000\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0004\u0002\u0004\u0003Ȼ\u0000\u0004?\u0000\u0005Ȼ\u0000\u0006<\u0000", new Object[]{"associatedValue_", "associatedValueCase_", "start_", "length_", Button.class});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<BodyRange> parser = PARSER;
                    if (parser == null) {
                        synchronized (BodyRange.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            BodyRange bodyRange = new BodyRange();
            DEFAULT_INSTANCE = bodyRange;
            GeneratedMessageLite.registerDefaultInstance(BodyRange.class, bodyRange);
        }

        public static BodyRange getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<BodyRange> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeListOrBuilder
    public List<BodyRange> getRangesList() {
        return this.ranges_;
    }

    public List<? extends BodyRangeOrBuilder> getRangesOrBuilderList() {
        return this.ranges_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeListOrBuilder
    public int getRangesCount() {
        return this.ranges_.size();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeListOrBuilder
    public BodyRange getRanges(int i) {
        return this.ranges_.get(i);
    }

    public BodyRangeOrBuilder getRangesOrBuilder(int i) {
        return this.ranges_.get(i);
    }

    private void ensureRangesIsMutable() {
        Internal.ProtobufList<BodyRange> protobufList = this.ranges_;
        if (!protobufList.isModifiable()) {
            this.ranges_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setRanges(int i, BodyRange bodyRange) {
        bodyRange.getClass();
        ensureRangesIsMutable();
        this.ranges_.set(i, bodyRange);
    }

    public void addRanges(BodyRange bodyRange) {
        bodyRange.getClass();
        ensureRangesIsMutable();
        this.ranges_.add(bodyRange);
    }

    public void addRanges(int i, BodyRange bodyRange) {
        bodyRange.getClass();
        ensureRangesIsMutable();
        this.ranges_.add(i, bodyRange);
    }

    public void addAllRanges(Iterable<? extends BodyRange> iterable) {
        ensureRangesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.ranges_);
    }

    public void clearRanges() {
        this.ranges_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeRanges(int i) {
        ensureRangesIsMutable();
        this.ranges_.remove(i);
    }

    public static BodyRangeList parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (BodyRangeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static BodyRangeList parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BodyRangeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static BodyRangeList parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (BodyRangeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static BodyRangeList parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BodyRangeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static BodyRangeList parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (BodyRangeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static BodyRangeList parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BodyRangeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static BodyRangeList parseFrom(InputStream inputStream) throws IOException {
        return (BodyRangeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static BodyRangeList parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BodyRangeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static BodyRangeList parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (BodyRangeList) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static BodyRangeList parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BodyRangeList) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static BodyRangeList parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (BodyRangeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static BodyRangeList parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BodyRangeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(BodyRangeList bodyRangeList) {
        return DEFAULT_INSTANCE.createBuilder(bodyRangeList);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<BodyRangeList, Builder> implements BodyRangeListOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(BodyRangeList.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeListOrBuilder
        public List<BodyRange> getRangesList() {
            return Collections.unmodifiableList(((BodyRangeList) this.instance).getRangesList());
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeListOrBuilder
        public int getRangesCount() {
            return ((BodyRangeList) this.instance).getRangesCount();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeListOrBuilder
        public BodyRange getRanges(int i) {
            return ((BodyRangeList) this.instance).getRanges(i);
        }

        public Builder setRanges(int i, BodyRange bodyRange) {
            copyOnWrite();
            ((BodyRangeList) this.instance).setRanges(i, bodyRange);
            return this;
        }

        public Builder setRanges(int i, BodyRange.Builder builder) {
            copyOnWrite();
            ((BodyRangeList) this.instance).setRanges(i, builder.build());
            return this;
        }

        public Builder addRanges(BodyRange bodyRange) {
            copyOnWrite();
            ((BodyRangeList) this.instance).addRanges(bodyRange);
            return this;
        }

        public Builder addRanges(int i, BodyRange bodyRange) {
            copyOnWrite();
            ((BodyRangeList) this.instance).addRanges(i, bodyRange);
            return this;
        }

        public Builder addRanges(BodyRange.Builder builder) {
            copyOnWrite();
            ((BodyRangeList) this.instance).addRanges(builder.build());
            return this;
        }

        public Builder addRanges(int i, BodyRange.Builder builder) {
            copyOnWrite();
            ((BodyRangeList) this.instance).addRanges(i, builder.build());
            return this;
        }

        public Builder addAllRanges(Iterable<? extends BodyRange> iterable) {
            copyOnWrite();
            ((BodyRangeList) this.instance).addAllRanges(iterable);
            return this;
        }

        public Builder clearRanges() {
            copyOnWrite();
            ((BodyRangeList) this.instance).clearRanges();
            return this;
        }

        public Builder removeRanges(int i) {
            copyOnWrite();
            ((BodyRangeList) this.instance).removeRanges(i);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new BodyRangeList();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"ranges_", BodyRange.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<BodyRangeList> parser = PARSER;
                if (parser == null) {
                    synchronized (BodyRangeList.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        BodyRangeList bodyRangeList = new BodyRangeList();
        DEFAULT_INSTANCE = bodyRangeList;
        GeneratedMessageLite.registerDefaultInstance(BodyRangeList.class, bodyRangeList);
    }

    public static BodyRangeList getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<BodyRangeList> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
