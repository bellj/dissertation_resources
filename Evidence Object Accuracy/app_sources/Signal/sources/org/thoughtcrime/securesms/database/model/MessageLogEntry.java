package org.thoughtcrime.securesms.database.model;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* compiled from: MessageLogEntry.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\u0002\u0010\rJ\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001e\u001a\u00020\tHÆ\u0003J\u000f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003JA\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0001J\u0013\u0010!\u001a\u00020\u00152\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010#\u001a\u00020$HÖ\u0001J\t\u0010%\u001a\u00020&HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0014\u001a\u00020\u00158G¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001a¨\u0006'"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/MessageLogEntry;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "dateSent", "", "content", "Lorg/whispersystems/signalservice/internal/push/SignalServiceProtos$Content;", "contentHint", "Lorg/whispersystems/signalservice/api/crypto/ContentHint;", "relatedMessages", "", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;JLorg/whispersystems/signalservice/internal/push/SignalServiceProtos$Content;Lorg/whispersystems/signalservice/api/crypto/ContentHint;Ljava/util/List;)V", "getContent", "()Lorg/whispersystems/signalservice/internal/push/SignalServiceProtos$Content;", "getContentHint", "()Lorg/whispersystems/signalservice/api/crypto/ContentHint;", "getDateSent", "()J", "hasRelatedMessage", "", "()Z", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getRelatedMessages", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageLogEntry {
    private final SignalServiceProtos.Content content;
    private final ContentHint contentHint;
    private final long dateSent;
    private final RecipientId recipientId;
    private final List<MessageId> relatedMessages;

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: org.thoughtcrime.securesms.database.model.MessageLogEntry */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ MessageLogEntry copy$default(MessageLogEntry messageLogEntry, RecipientId recipientId, long j, SignalServiceProtos.Content content, ContentHint contentHint, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            recipientId = messageLogEntry.recipientId;
        }
        if ((i & 2) != 0) {
            j = messageLogEntry.dateSent;
        }
        if ((i & 4) != 0) {
            content = messageLogEntry.content;
        }
        if ((i & 8) != 0) {
            contentHint = messageLogEntry.contentHint;
        }
        if ((i & 16) != 0) {
            list = messageLogEntry.relatedMessages;
        }
        return messageLogEntry.copy(recipientId, j, content, contentHint, list);
    }

    public final RecipientId component1() {
        return this.recipientId;
    }

    public final long component2() {
        return this.dateSent;
    }

    public final SignalServiceProtos.Content component3() {
        return this.content;
    }

    public final ContentHint component4() {
        return this.contentHint;
    }

    public final List<MessageId> component5() {
        return this.relatedMessages;
    }

    public final MessageLogEntry copy(RecipientId recipientId, long j, SignalServiceProtos.Content content, ContentHint contentHint, List<MessageId> list) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(content, "content");
        Intrinsics.checkNotNullParameter(contentHint, "contentHint");
        Intrinsics.checkNotNullParameter(list, "relatedMessages");
        return new MessageLogEntry(recipientId, j, content, contentHint, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageLogEntry)) {
            return false;
        }
        MessageLogEntry messageLogEntry = (MessageLogEntry) obj;
        return Intrinsics.areEqual(this.recipientId, messageLogEntry.recipientId) && this.dateSent == messageLogEntry.dateSent && Intrinsics.areEqual(this.content, messageLogEntry.content) && this.contentHint == messageLogEntry.contentHint && Intrinsics.areEqual(this.relatedMessages, messageLogEntry.relatedMessages);
    }

    public int hashCode() {
        return (((((((this.recipientId.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.dateSent)) * 31) + this.content.hashCode()) * 31) + this.contentHint.hashCode()) * 31) + this.relatedMessages.hashCode();
    }

    public String toString() {
        return "MessageLogEntry(recipientId=" + this.recipientId + ", dateSent=" + this.dateSent + ", content=" + this.content + ", contentHint=" + this.contentHint + ", relatedMessages=" + this.relatedMessages + ')';
    }

    public MessageLogEntry(RecipientId recipientId, long j, SignalServiceProtos.Content content, ContentHint contentHint, List<MessageId> list) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(content, "content");
        Intrinsics.checkNotNullParameter(contentHint, "contentHint");
        Intrinsics.checkNotNullParameter(list, "relatedMessages");
        this.recipientId = recipientId;
        this.dateSent = j;
        this.content = content;
        this.contentHint = contentHint;
        this.relatedMessages = list;
    }

    public final RecipientId getRecipientId() {
        return this.recipientId;
    }

    public final long getDateSent() {
        return this.dateSent;
    }

    public final SignalServiceProtos.Content getContent() {
        return this.content;
    }

    public final ContentHint getContentHint() {
        return this.contentHint;
    }

    public final List<MessageId> getRelatedMessages() {
        return this.relatedMessages;
    }

    public final boolean hasRelatedMessage() {
        return !this.relatedMessages.isEmpty();
    }
}
