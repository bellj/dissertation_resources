package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import androidx.core.content.ContentValuesKt;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.CursorExtensionsKt;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.DeleteBuilderPart1;
import org.signal.core.util.LongSerializer;
import org.signal.core.util.SQLiteDatabaseExtensionsKt;
import org.signal.core.util.SelectBuilderPart2;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SentStorySyncManifest;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListPartialRecord;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyData;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* compiled from: DistributionListDatabase.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0012\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 O2\u00020\u0001:\u0003OPQB\u0019\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u001e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u000f\u001a\u00020\b2\u0006\u0010\u0010\u001a\u00020\u0011J\u0014\u0010\u0012\u001a\u00020\b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00110\u0014J\\\u0010\u0015\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0016\u001a\u00020\u00172\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00192\b\b\u0002\u0010\u001a\u001a\u00020\u001b2\b\b\u0002\u0010\u001c\u001a\u00020\u001d2\b\b\u0002\u0010\u001e\u001a\u00020\u001f2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010!2\b\b\u0002\u0010\"\u001a\u00020\u001d2\b\b\u0002\u0010\u000b\u001a\u00020\fJ\b\u0010#\u001a\u00020\u0017H\u0002J\b\u0010$\u001a\u00020\u0017H\u0002J\u0018\u0010%\u001a\u00020\b2\u0006\u0010&\u001a\u00020\n2\b\b\u0002\u0010\u001e\u001a\u00020\u001fJ\u001c\u0010'\u001a\u00020\b2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00192\u0006\u0010)\u001a\u00020*J\u0016\u0010+\u001a\u00020\b2\u0006\u0010,\u001a\u00020\u000e2\u0006\u0010)\u001a\u00020*J\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0019J\u001a\u0010.\u001a\u0004\u0018\u00010/2\b\u00100\u001a\u0004\u0018\u00010\u00172\u0006\u00101\u001a\u00020\u001dJ\f\u00102\u001a\b\u0012\u0004\u0012\u0002030\u0019J\u0010\u00104\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\t\u001a\u00020\nJ\u0010\u00105\u001a\u0004\u0018\u00010*2\u0006\u0010\t\u001a\u00020\nJ\u0010\u00106\u001a\u0004\u0018\u00010*2\u0006\u0010\t\u001a\u00020\nJ\u000e\u00107\u001a\u0002082\u0006\u0010\t\u001a\u00020\nJ\u0014\u00109\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00192\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010:\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010;\u001a\u00020<J\u000e\u0010=\u001a\u00020>2\u0006\u0010\t\u001a\u00020\nJ\u0010\u0010?\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\nH\u0002J\u0018\u0010@\u001a\u0002082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u001c\u0010A\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00192\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0010\u0010B\u001a\u0004\u0018\u00010\u000e2\u0006\u0010&\u001a\u00020\nJ\u0010\u0010C\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u001a\u001a\u00020\u001bJ\u0010\u0010D\u001a\u0004\u0018\u00010\u000e2\u0006\u0010)\u001a\u00020\u0011J\u000e\u0010E\u001a\u00020F2\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010G\u001a\u00020\b2\u0006\u0010H\u001a\u00020\u000e2\u0006\u0010I\u001a\u00020\u000eJ\u000e\u0010J\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010J\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u001e\u0010K\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eJ\u0016\u0010L\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u001c\u001a\u00020\u001dJ\u0016\u0010M\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017J\u0016\u0010N\u001a\u00020\b2\u0006\u0010&\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f¨\u0006R"}, d2 = {"Lorg/thoughtcrime/securesms/database/DistributionListDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "addMemberToList", "", "listId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "privacyMode", "Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "member", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "applyStorageSyncStoryDistributionListInsert", "insert", "Lorg/whispersystems/signalservice/api/storage/SignalStoryDistributionListRecord;", "applyStorageSyncStoryDistributionListUpdate", "update", "Lorg/thoughtcrime/securesms/storage/StorageRecordUpdate;", "createList", "name", "", "members", "", "distributionId", "Lorg/whispersystems/signalservice/api/push/DistributionId;", "allowsReplies", "", "deletionTimestamp", "", "storageId", "", "isUnknown", "createUniqueNameForDeletedStory", "createUniqueNameForUnknownDistributionId", "deleteList", "distributionListId", "excludeAllFromStory", "recipientIds", "record", "Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "excludeFromStory", "recipientId", "getAllListRecipients", "getAllListsForContactSelectionUiCursor", "Landroid/database/Cursor;", "query", "includeMyStory", "getCustomListsForUi", "Lorg/thoughtcrime/securesms/database/model/DistributionListPartialRecord;", "getDistributionId", "getList", "getListForStorageSync", "getMemberCount", "", "getMembers", "getOrCreateByDistributionId", "manifest", "Lorg/thoughtcrime/securesms/database/SentStorySyncManifest;", "getPrivacyData", "Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyData;", "getPrivacyMode", "getRawMemberCount", "getRawMembers", "getRecipientId", "getRecipientIdByDistributionId", "getRecipientIdForSyncRecord", "getStoryType", "Lorg/thoughtcrime/securesms/database/model/StoryType;", "remapRecipient", "oldId", "newId", "removeAllMembers", "removeMemberFromList", "setAllowsReplies", "setName", "setPrivacyMode", "Companion", "ListTable", "MembershipTable", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DistributionListDatabase extends Database {
    public static final String[] CREATE_INDEXES = {MembershipTable.CREATE_INDEX};
    public static final String[] CREATE_TABLE = {ListTable.INSTANCE.getCREATE_TABLE(), MembershipTable.CREATE_TABLE};
    public static final Companion Companion = new Companion(null);
    public static final String DISTRIBUTION_ID;
    public static final String LIST_TABLE_NAME;
    public static final String RECIPIENT_ID;
    private static final String TAG = Log.tag(DistributionListDatabase.class);

    /* compiled from: DistributionListDatabase.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[DistributionListPrivacyMode.values().length];
            iArr[DistributionListPrivacyMode.ALL.ordinal()] = 1;
            iArr[DistributionListPrivacyMode.ALL_EXCEPT.ordinal()] = 2;
            iArr[DistributionListPrivacyMode.ONLY_WITH.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public DistributionListDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    /* compiled from: DistributionListDatabase.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0006R\u000e\u0010\b\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \f*\u0004\u0018\u00010\u00050\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/database/DistributionListDatabase$Companion;", "", "()V", "CREATE_INDEXES", "", "", "[Ljava/lang/String;", "CREATE_TABLE", "DISTRIBUTION_ID", "LIST_TABLE_NAME", "RECIPIENT_ID", "TAG", "kotlin.jvm.PlatformType", "insertInitialDistributionListAtCreationTime", "", "db", "Lnet/zetetic/database/sqlcipher/SQLiteDatabase;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final void insertInitialDistributionListAtCreationTime(SQLiteDatabase sQLiteDatabase) {
            Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
            long insert = sQLiteDatabase.insert(RecipientDatabase.TABLE_NAME, (String) null, ContentValuesKt.contentValuesOf(TuplesKt.to(RecipientDatabase.GROUP_TYPE, Integer.valueOf(RecipientDatabase.GroupType.DISTRIBUTION_LIST.getId())), TuplesKt.to(RecipientDatabase.DISTRIBUTION_LIST_ID, 1L), TuplesKt.to(RecipientDatabase.STORAGE_SERVICE_ID, Base64.encodeBytes(StorageSyncHelper.generateKey())), TuplesKt.to(RecipientDatabase.PROFILE_SHARING, 1)));
            DistributionId distributionId = DistributionId.MY_STORY;
            sQLiteDatabase.insert("distribution_list", (String) null, ContentValuesKt.contentValuesOf(TuplesKt.to("_id", 1L), TuplesKt.to("name", distributionId.toString()), TuplesKt.to("distribution_id", distributionId.toString()), TuplesKt.to("recipient_id", Long.valueOf(insert)), TuplesKt.to("privacy_mode", Long.valueOf(DistributionListPrivacyMode.ALL.serialize()))));
        }
    }

    /* compiled from: DistributionListDatabase.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u0011\n\u0002\b\b\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0019\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00040\u000e¢\u0006\n\n\u0002\u0010\u0011\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/database/DistributionListDatabase$ListTable;", "", "()V", "ALLOWS_REPLIES", "", "CREATE_TABLE", "getCREATE_TABLE", "()Ljava/lang/String;", "DELETION_TIMESTAMP", "DISTRIBUTION_ID", "ID", "IS_NOT_DELETED", "IS_UNKNOWN", "LIST_UI_PROJECTION", "", "getLIST_UI_PROJECTION", "()[Ljava/lang/String;", "[Ljava/lang/String;", "NAME", "PRIVACY_MODE", "RECIPIENT_ID", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ListTable {
        public static final String ALLOWS_REPLIES;
        private static final String CREATE_TABLE = ("\n      CREATE TABLE distribution_list (\n        _id INTEGER PRIMARY KEY AUTOINCREMENT,\n        name TEXT UNIQUE NOT NULL,\n        distribution_id TEXT UNIQUE NOT NULL,\n        recipient_id INTEGER UNIQUE REFERENCES recipient (_id),\n        allows_replies INTEGER DEFAULT 1,\n        deletion_timestamp INTEGER DEFAULT 0,\n        is_unknown INTEGER DEFAULT 0,\n        privacy_mode INTEGER DEFAULT " + DistributionListPrivacyMode.ONLY_WITH.serialize() + "\n      )\n    ");
        public static final String DELETION_TIMESTAMP;
        public static final String DISTRIBUTION_ID;
        public static final String ID;
        public static final ListTable INSTANCE = new ListTable();
        public static final String IS_NOT_DELETED;
        public static final String IS_UNKNOWN;
        private static final String[] LIST_UI_PROJECTION = {"_id", "name", "recipient_id", "allows_replies", IS_UNKNOWN, "privacy_mode"};
        public static final String NAME;
        public static final String PRIVACY_MODE;
        public static final String RECIPIENT_ID;
        public static final String TABLE_NAME;

        private ListTable() {
        }

        public final String getCREATE_TABLE() {
            return CREATE_TABLE;
        }

        public final String[] getLIST_UI_PROJECTION() {
            return LIST_UI_PROJECTION;
        }
    }

    /* compiled from: DistributionListDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/DistributionListDatabase$MembershipTable;", "", "()V", "CREATE_INDEX", "", "CREATE_TABLE", "ID", "LIST_ID", "PRIVACY_MODE", "RECIPIENT_ID", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class MembershipTable {
        public static final String CREATE_INDEX;
        public static final String CREATE_TABLE;
        public static final String ID;
        public static final MembershipTable INSTANCE = new MembershipTable();
        public static final String LIST_ID;
        public static final String PRIVACY_MODE;
        public static final String RECIPIENT_ID;
        public static final String TABLE_NAME;

        private MembershipTable() {
        }
    }

    public final boolean setName(DistributionListId distributionListId, String str) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        Intrinsics.checkNotNullParameter(str, "name");
        if (getWritableDatabase().updateWithOnConflict("distribution_list", ContentValuesKt.contentValuesOf(TuplesKt.to("name", str)), "_id = ?", SqlUtil.buildArgs(distributionListId), 4) == 1) {
            return true;
        }
        return false;
    }

    public final void setPrivacyMode(DistributionListId distributionListId, DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        getWritableDatabase().update("distribution_list", ContentValuesKt.contentValuesOf(TuplesKt.to("privacy_mode", Long.valueOf(distributionListPrivacyMode.serialize()))), "_id = ?", SqlUtil.buildArgs(distributionListId));
    }

    public final Cursor getAllListsForContactSelectionUiCursor(String str, boolean z) {
        String str2;
        String[] buildArgs;
        SQLiteDatabase readableDatabase = getReadableDatabase();
        if (!(str == null || str.length() == 0) || !z) {
            str2 = str == null || str.length() == 0 ? "_id != ? AND deletion_timestamp == 0" : z ? "(name GLOB ? OR _id == ?) AND deletion_timestamp == 0 AND NOT is_unknown" : "name GLOB ? AND _id != ? AND deletion_timestamp == 0 AND NOT is_unknown";
        } else {
            str2 = ListTable.IS_NOT_DELETED;
        }
        if (!(str == null || str.length() == 0) || !z) {
            if (str == null || str.length() == 0) {
                buildArgs = SqlUtil.buildArgs(1);
            } else {
                buildArgs = SqlUtil.buildArgs(SqlUtil.buildCaseInsensitiveGlobPattern(str), 1L);
            }
        } else {
            buildArgs = null;
        }
        return readableDatabase.query("distribution_list", ListTable.INSTANCE.getLIST_UI_PROJECTION(), str2, buildArgs, null, null, null);
    }

    public final List<RecipientId> getAllListRecipients() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Intrinsics.checkNotNullExpressionValue(readableDatabase, "readableDatabase");
        Cursor run = SQLiteDatabaseExtensionsKt.select(readableDatabase, "recipient_id").from("distribution_list").run();
        ArrayList arrayList = new ArrayList();
        while (run.moveToNext()) {
            try {
                RecipientId.from(CursorExtensionsKt.requireLong(run, "recipient_id"));
                arrayList.add(RecipientId.from(CursorExtensionsKt.requireLong(run, "recipient_id")));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        th = null;
        return arrayList;
    }

    public final List<DistributionListPartialRecord> getCustomListsForUi() {
        Cursor query = getReadableDatabase().query("distribution_list", ListTable.INSTANCE.getLIST_UI_PROJECTION(), "_id != 1 AND deletion_timestamp == 0", null, null, null, null);
        if (query == null) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        th = null;
        try {
            ArrayList arrayList = new ArrayList();
            while (query.moveToNext()) {
                DistributionListId from = DistributionListId.from(CursorUtil.requireLong(query, "_id"));
                String requireString = CursorUtil.requireString(query, "name");
                boolean requireBoolean = CursorUtil.requireBoolean(query, "allows_replies");
                RecipientId from2 = RecipientId.from(CursorUtil.requireLong(query, "recipient_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(CursorUtil.requireLong(cursor, ListTable.ID))");
                Intrinsics.checkNotNullExpressionValue(requireString, "requireString(cursor, ListTable.NAME)");
                Intrinsics.checkNotNullExpressionValue(from2, "from(CursorUtil.requireL… ListTable.RECIPIENT_ID))");
                arrayList.add(new DistributionListPartialRecord(from, requireString, from2, requireBoolean, CursorUtil.requireBoolean(query, ListTable.IS_UNKNOWN), (DistributionListPrivacyMode) CursorExtensionsKt.requireObject(query, "privacy_mode", DistributionListPrivacyMode.Serializer)));
            }
            return arrayList;
        } catch (Throwable th) {
            try {
                throw th;
            } finally {
                CloseableKt.closeFinally(query, th);
            }
        }
    }

    public final RecipientId getOrCreateByDistributionId(DistributionId distributionId, SentStorySyncManifest sentStorySyncManifest) {
        Intrinsics.checkNotNullParameter(distributionId, "distributionId");
        Intrinsics.checkNotNullParameter(sentStorySyncManifest, "manifest");
        getWritableDatabase().beginTransaction();
        try {
            RecipientId recipientIdByDistributionId = getRecipientIdByDistributionId(distributionId);
            if (recipientIdByDistributionId == null) {
                List<SentStorySyncManifest.Entry> entries = sentStorySyncManifest.getEntries();
                ArrayList<SentStorySyncManifest.Entry> arrayList = new ArrayList();
                for (Object obj : entries) {
                    if (((SentStorySyncManifest.Entry) obj).getDistributionLists().contains(distributionId)) {
                        arrayList.add(obj);
                    }
                }
                ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
                for (SentStorySyncManifest.Entry entry : arrayList) {
                    arrayList2.add(entry.getRecipientId());
                }
                DistributionListId createList$default = createList$default(this, createUniqueNameForUnknownDistributionId(), arrayList2, distributionId, false, 0, null, true, null, 184, null);
                if (createList$default != null) {
                    RecipientId recipientId = getRecipientId(createList$default);
                    if (recipientId != null) {
                        getWritableDatabase().setTransactionSuccessful();
                        return recipientId;
                    }
                    throw new AssertionError("Failed to retrieve recipient for newly created list");
                }
                throw new AssertionError("Failed to create distribution list for unknown id.");
            }
            getWritableDatabase().setTransactionSuccessful();
            return recipientIdByDistributionId;
        } finally {
            getWritableDatabase().endTransaction();
        }
    }

    public static /* synthetic */ DistributionListId createList$default(DistributionListDatabase distributionListDatabase, String str, List list, DistributionId distributionId, boolean z, long j, byte[] bArr, boolean z2, DistributionListPrivacyMode distributionListPrivacyMode, int i, Object obj) {
        DistributionId distributionId2;
        DistributionListPrivacyMode distributionListPrivacyMode2;
        if ((i & 4) != 0) {
            DistributionId from = DistributionId.from(UUID.randomUUID());
            Intrinsics.checkNotNullExpressionValue(from, "from(UUID.randomUUID())");
            distributionId2 = from;
        } else {
            distributionId2 = distributionId;
        }
        boolean z3 = (i & 8) != 0 ? true : z;
        long j2 = (i & 16) != 0 ? 0 : j;
        byte[] bArr2 = (i & 32) != 0 ? null : bArr;
        boolean z4 = (i & 64) != 0 ? false : z2;
        if ((i & 128) != 0) {
            distributionListPrivacyMode2 = DistributionListPrivacyMode.ONLY_WITH;
        } else {
            distributionListPrivacyMode2 = distributionListPrivacyMode;
        }
        return distributionListDatabase.createList(str, list, distributionId2, z3, j2, bArr2, z4, distributionListPrivacyMode2);
    }

    public final DistributionListId createList(String str, List<? extends RecipientId> list, DistributionId distributionId, boolean z, long j, byte[] bArr, boolean z2, DistributionListPrivacyMode distributionListPrivacyMode) {
        Throwable th;
        String str2 = str;
        Intrinsics.checkNotNullParameter(str, "name");
        Intrinsics.checkNotNullParameter(list, "members");
        Intrinsics.checkNotNullParameter(distributionId, "distributionId");
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            if (j != 0) {
                str2 = createUniqueNameForDeletedStory();
            }
            contentValues.put("name", str2);
            contentValues.put("distribution_id", distributionId.toString());
            contentValues.put("allows_replies", Boolean.valueOf(j == 0 ? z : false));
            contentValues.putNull("recipient_id");
            contentValues.put(ListTable.DELETION_TIMESTAMP, Long.valueOf(j));
            contentValues.put(ListTable.IS_UNKNOWN, Boolean.valueOf(z2));
            contentValues.put("privacy_mode", Long.valueOf(distributionListPrivacyMode.serialize()));
            long insert = getWritableDatabase().insert("distribution_list", (String) null, contentValues);
            if (insert < 0) {
                writableDatabase.endTransaction();
                return null;
            }
            RecipientDatabase recipients = SignalDatabase.Companion.recipients();
            DistributionListId from = DistributionListId.from(insert);
            Intrinsics.checkNotNullExpressionValue(from, "from(id)");
            RecipientId orInsertFromDistributionListId = recipients.getOrInsertFromDistributionListId(from, bArr);
            SQLiteDatabase writableDatabase2 = getWritableDatabase();
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("recipient_id", orInsertFromDistributionListId.serialize());
            Unit unit = Unit.INSTANCE;
            writableDatabase2.update("distribution_list", contentValues2, "_id = ?", SqlUtil.buildArgs(insert));
            for (RecipientId recipientId : list) {
                DistributionListId from2 = DistributionListId.from(insert);
                Intrinsics.checkNotNullExpressionValue(from2, "from(id)");
                try {
                    addMemberToList(from2, distributionListPrivacyMode, recipientId);
                } catch (Throwable th2) {
                    th = th2;
                    writableDatabase.endTransaction();
                    throw th;
                }
            }
            writableDatabase.setTransactionSuccessful();
            DistributionListId from3 = DistributionListId.from(insert);
            writableDatabase.endTransaction();
            return from3;
        } catch (Throwable th3) {
            th = th3;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v6, resolved type: org.thoughtcrime.securesms.recipients.RecipientId */
    /* JADX DEBUG: Multi-variable search result rejected for r0v7, resolved type: org.thoughtcrime.securesms.recipients.RecipientId */
    /* JADX WARN: Multi-variable type inference failed */
    public final RecipientId getRecipientIdByDistributionId(DistributionId distributionId) {
        Intrinsics.checkNotNullParameter(distributionId, "distributionId");
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Intrinsics.checkNotNullExpressionValue(readableDatabase, "readableDatabase");
        SelectBuilderPart2 from = SQLiteDatabaseExtensionsKt.select(readableDatabase, "recipient_id").from("distribution_list");
        String distributionId2 = distributionId.toString();
        Intrinsics.checkNotNullExpressionValue(distributionId2, "distributionId.toString()");
        Cursor run = from.where("distribution_id = ?", distributionId2).run();
        try {
            Throwable th = null;
            return run.moveToFirst() ? RecipientId.from(CursorUtil.requireLong(run, "recipient_id")) : th;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final StoryType getStoryType(DistributionListId distributionListId) {
        StoryType storyType;
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        Cursor query = getReadableDatabase().query("distribution_list", new String[]{"allows_replies"}, "_id = ? AND deletion_timestamp == 0", SqlUtil.buildArgs(distributionListId), null, null, null);
        try {
            if (query.moveToFirst()) {
                if (CursorUtil.requireBoolean(query, "allows_replies")) {
                    storyType = StoryType.STORY_WITH_REPLIES;
                } else {
                    storyType = StoryType.STORY_WITHOUT_REPLIES;
                }
                th = null;
                return storyType;
            }
            throw new IllegalStateException("Distribution list not in database.".toString());
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final void setAllowsReplies(DistributionListId distributionListId, boolean z) {
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        getWritableDatabase().update("distribution_list", ContentValuesKt.contentValuesOf(TuplesKt.to("allows_replies", Boolean.valueOf(z))), "_id = ? AND deletion_timestamp == 0", SqlUtil.buildArgs(distributionListId));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v3, resolved type: org.thoughtcrime.securesms.database.model.DistributionListRecord */
    /* JADX DEBUG: Multi-variable search result rejected for r4v4, resolved type: org.thoughtcrime.securesms.database.model.DistributionListRecord */
    /* JADX WARN: Multi-variable type inference failed */
    public final DistributionListRecord getList(DistributionListId distributionListId) {
        DistributionListRecord distributionListRecord;
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        Cursor query = getReadableDatabase().query("distribution_list", null, "_id = ? AND deletion_timestamp == 0", SqlUtil.buildArgs(distributionListId), null, null, null);
        try {
            Throwable th = null;
            if (query.moveToFirst()) {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                DistributionListId from = DistributionListId.from(CursorExtensionsKt.requireLong(query, "_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(cursor.requireLong(ListTable.ID))");
                DistributionListPrivacyMode distributionListPrivacyMode = (DistributionListPrivacyMode) CursorExtensionsKt.requireObject(query, "privacy_mode", DistributionListPrivacyMode.Serializer);
                String requireNonNullString = CursorExtensionsKt.requireNonNullString(query, "name");
                DistributionId from2 = DistributionId.from(CursorExtensionsKt.requireNonNullString(query, "distribution_id"));
                Intrinsics.checkNotNullExpressionValue(from2, "from(cursor.requireNonNu…stTable.DISTRIBUTION_ID))");
                distributionListRecord = new DistributionListRecord(from, requireNonNullString, from2, CursorUtil.requireBoolean(query, "allows_replies"), getRawMembers(from, distributionListPrivacyMode), getMembers(from), 0, CursorUtil.requireBoolean(query, ListTable.IS_UNKNOWN), distributionListPrivacyMode);
            } else {
                distributionListRecord = th;
            }
            return distributionListRecord;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: org.thoughtcrime.securesms.database.model.DistributionListRecord */
    /* JADX DEBUG: Multi-variable search result rejected for r0v6, resolved type: org.thoughtcrime.securesms.database.model.DistributionListRecord */
    /* JADX WARN: Multi-variable type inference failed */
    public final DistributionListRecord getListForStorageSync(DistributionListId distributionListId) {
        Throwable th;
        DistributionListRecord distributionListRecord;
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        Cursor query = getReadableDatabase().query("distribution_list", null, "_id = ?", SqlUtil.buildArgs(distributionListId), null, null, null);
        try {
            Throwable th2 = null;
            if (query.moveToFirst()) {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                DistributionListId from = DistributionListId.from(CursorExtensionsKt.requireLong(query, "_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(cursor.requireLong(ListTable.ID))");
                DistributionListPrivacyMode distributionListPrivacyMode = (DistributionListPrivacyMode) CursorExtensionsKt.requireObject(query, "privacy_mode", DistributionListPrivacyMode.Serializer);
                String requireNonNullString = CursorExtensionsKt.requireNonNullString(query, "name");
                DistributionId from2 = DistributionId.from(CursorExtensionsKt.requireNonNullString(query, "distribution_id"));
                Intrinsics.checkNotNullExpressionValue(from2, "from(cursor.requireNonNu…stTable.DISTRIBUTION_ID))");
                try {
                    distributionListRecord = new DistributionListRecord(from, requireNonNullString, from2, CursorUtil.requireBoolean(query, "allows_replies"), getRawMembers(from, distributionListPrivacyMode), CollectionsKt__CollectionsKt.emptyList(), CursorExtensionsKt.requireLong(query, ListTable.DELETION_TIMESTAMP), CursorUtil.requireBoolean(query, ListTable.IS_UNKNOWN), distributionListPrivacyMode);
                } catch (Throwable th3) {
                    th = th3;
                    try {
                        throw th;
                    } finally {
                        CloseableKt.closeFinally(query, th);
                    }
                }
            } else {
                distributionListRecord = th2;
            }
            return distributionListRecord;
        } catch (Throwable th4) {
            th = th4;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: org.whispersystems.signalservice.api.push.DistributionId */
    /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: org.whispersystems.signalservice.api.push.DistributionId */
    /* JADX WARN: Multi-variable type inference failed */
    public final DistributionId getDistributionId(DistributionListId distributionListId) {
        DistributionId distributionId;
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        Cursor query = getReadableDatabase().query("distribution_list", new String[]{"distribution_id"}, "_id = ? AND deletion_timestamp == 0", SqlUtil.buildArgs(distributionListId), null, null, null);
        try {
            Throwable th = null;
            if (query.moveToFirst()) {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                distributionId = DistributionId.from(CursorExtensionsKt.requireString(query, "distribution_id"));
            } else {
                distributionId = th;
            }
            return distributionId;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Throwable, java.util.List<org.thoughtcrime.securesms.recipients.RecipientId>] */
    public final List<RecipientId> getMembers(DistributionListId distributionListId) {
        DistributionListPrivacyMode distributionListPrivacyMode;
        Cursor signalContacts;
        List list;
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
        SQLiteDatabaseExtensionsKt.withinTransaction(getReadableDatabase(), new Function1<SQLiteDatabase, Unit>(ref$ObjectRef, this, distributionListId, ref$ObjectRef2) { // from class: org.thoughtcrime.securesms.database.DistributionListDatabase$getMembers$1
            final /* synthetic */ DistributionListId $listId;
            final /* synthetic */ Ref$ObjectRef<DistributionListPrivacyMode> $privacyMode;
            final /* synthetic */ Ref$ObjectRef<List<RecipientId>> $rawMembers;
            final /* synthetic */ DistributionListDatabase this$0;

            /* access modifiers changed from: package-private */
            {
                this.$privacyMode = r1;
                this.this$0 = r2;
                this.$listId = r3;
                this.$rawMembers = r4;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(SQLiteDatabase sQLiteDatabase) {
                invoke(sQLiteDatabase);
                return Unit.INSTANCE;
            }

            /* JADX WARN: Type inference failed for: r0v1, types: [org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode, T] */
            /* JADX WARN: Type inference failed for: r0v3, types: [java.util.List, T] */
            public final void invoke(SQLiteDatabase sQLiteDatabase) {
                DistributionListPrivacyMode distributionListPrivacyMode2;
                this.$privacyMode.element = this.this$0.getPrivacyMode(this.$listId);
                Ref$ObjectRef<List<RecipientId>> ref$ObjectRef3 = this.$rawMembers;
                DistributionListDatabase distributionListDatabase = this.this$0;
                DistributionListId distributionListId2 = this.$listId;
                DistributionListPrivacyMode distributionListPrivacyMode3 = this.$privacyMode.element;
                if (distributionListPrivacyMode3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("privacyMode");
                    distributionListPrivacyMode2 = null;
                } else {
                    distributionListPrivacyMode2 = distributionListPrivacyMode3;
                }
                ref$ObjectRef3.element = distributionListDatabase.getRawMembers(distributionListId2, distributionListPrivacyMode2);
            }
        });
        T t = ref$ObjectRef.element;
        th = 0;
        if (t == 0) {
            Intrinsics.throwUninitializedPropertyAccessException("privacyMode");
            distributionListPrivacyMode = th;
        } else {
            distributionListPrivacyMode = (DistributionListPrivacyMode) t;
        }
        int i = WhenMappings.$EnumSwitchMapping$0[distributionListPrivacyMode.ordinal()];
        if (i == 1) {
            signalContacts = SignalDatabase.Companion.recipients().getSignalContacts(false);
            Intrinsics.checkNotNull(signalContacts);
            ArrayList arrayList = new ArrayList();
            while (signalContacts.moveToNext()) {
                try {
                    LongSerializer<RecipientId> longSerializer = RecipientId.SERIALIZER;
                    Intrinsics.checkNotNullExpressionValue(longSerializer, "SERIALIZER");
                    RecipientId recipientId = (RecipientId) CursorExtensionsKt.requireObject(signalContacts, "_id", longSerializer);
                    Intrinsics.checkNotNullExpressionValue(longSerializer, "SERIALIZER");
                    arrayList.add((RecipientId) CursorExtensionsKt.requireObject(signalContacts, "_id", longSerializer));
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            return arrayList;
        } else if (i == 2) {
            signalContacts = SignalDatabase.Companion.recipients().getSignalContacts(false);
            Intrinsics.checkNotNull(signalContacts);
            ArrayList arrayList2 = new ArrayList();
            while (signalContacts.moveToNext()) {
                try {
                    LongSerializer<RecipientId> longSerializer2 = RecipientId.SERIALIZER;
                    Intrinsics.checkNotNullExpressionValue(longSerializer2, "SERIALIZER");
                    RecipientId recipientId2 = (RecipientId) CursorExtensionsKt.requireObject(signalContacts, "_id", longSerializer2);
                    T t2 = ref$ObjectRef2.element;
                    if (t2 == 0) {
                        Intrinsics.throwUninitializedPropertyAccessException("rawMembers");
                        list = th;
                    } else {
                        list = (List) t2;
                    }
                    if (!list.contains(recipientId2)) {
                        Intrinsics.checkNotNullExpressionValue(longSerializer2, "SERIALIZER");
                        arrayList2.add((RecipientId) CursorExtensionsKt.requireObject(signalContacts, "_id", longSerializer2));
                    }
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            return arrayList2;
        } else if (i == 3) {
            T t3 = ref$ObjectRef2.element;
            if (t3 != 0) {
                return (List) t3;
            }
            Intrinsics.throwUninitializedPropertyAccessException("rawMembers");
            return th;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public final List<RecipientId> getRawMembers(DistributionListId distributionListId, DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        ArrayList arrayList = new ArrayList();
        Cursor query = getReadableDatabase().query(MembershipTable.TABLE_NAME, null, "list_id = ? AND privacy_mode = ?", SqlUtil.buildArgs(distributionListId, Long.valueOf(distributionListPrivacyMode.serialize())), null, null, null);
        while (query.moveToNext()) {
            try {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                RecipientId from = RecipientId.from(CursorExtensionsKt.requireLong(query, "recipient_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(cursor.requireLong(…rshipTable.RECIPIENT_ID))");
                arrayList.add(from);
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return arrayList;
    }

    public final int getMemberCount(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        return getPrivacyData(distributionListId).getMemberCount();
    }

    public final DistributionListPrivacyData getPrivacyData(DistributionListId distributionListId) {
        DistributionListPrivacyMode distributionListPrivacyMode;
        int i;
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        Ref$IntRef ref$IntRef = new Ref$IntRef();
        Ref$IntRef ref$IntRef2 = new Ref$IntRef();
        SQLiteDatabaseExtensionsKt.withinTransaction(getReadableDatabase(), new Function1<SQLiteDatabase, Unit>(ref$ObjectRef, this, distributionListId, ref$IntRef, ref$IntRef2) { // from class: org.thoughtcrime.securesms.database.DistributionListDatabase$getPrivacyData$1
            final /* synthetic */ DistributionListId $listId;
            final /* synthetic */ Ref$ObjectRef<DistributionListPrivacyMode> $privacyMode;
            final /* synthetic */ Ref$IntRef $rawMemberCount;
            final /* synthetic */ Ref$IntRef $totalContactCount;
            final /* synthetic */ DistributionListDatabase this$0;

            /* access modifiers changed from: package-private */
            {
                this.$privacyMode = r1;
                this.this$0 = r2;
                this.$listId = r3;
                this.$rawMemberCount = r4;
                this.$totalContactCount = r5;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(SQLiteDatabase sQLiteDatabase) {
                invoke(sQLiteDatabase);
                return Unit.INSTANCE;
            }

            /* JADX WARN: Type inference failed for: r0v1, types: [org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode, T] */
            public final void invoke(SQLiteDatabase sQLiteDatabase) {
                DistributionListPrivacyMode distributionListPrivacyMode2;
                this.$privacyMode.element = this.this$0.getPrivacyMode(this.$listId);
                Ref$IntRef ref$IntRef3 = this.$rawMemberCount;
                DistributionListDatabase distributionListDatabase = this.this$0;
                DistributionListId distributionListId2 = this.$listId;
                DistributionListPrivacyMode distributionListPrivacyMode3 = this.$privacyMode.element;
                if (distributionListPrivacyMode3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("privacyMode");
                    distributionListPrivacyMode2 = null;
                } else {
                    distributionListPrivacyMode2 = distributionListPrivacyMode3;
                }
                ref$IntRef3.element = distributionListDatabase.getRawMemberCount(distributionListId2, distributionListPrivacyMode2);
                this.$totalContactCount.element = SignalDatabase.Companion.recipients().getSignalContactsCount(false);
            }
        });
        T t = ref$ObjectRef.element;
        DistributionListPrivacyMode distributionListPrivacyMode2 = null;
        if (t == 0) {
            Intrinsics.throwUninitializedPropertyAccessException("privacyMode");
            distributionListPrivacyMode = null;
        } else {
            distributionListPrivacyMode = (DistributionListPrivacyMode) t;
        }
        int i2 = WhenMappings.$EnumSwitchMapping$0[distributionListPrivacyMode.ordinal()];
        if (i2 == 1) {
            i = ref$IntRef2.element;
        } else if (i2 == 2) {
            i = ref$IntRef2.element - ref$IntRef.element;
        } else if (i2 == 3) {
            i = ref$IntRef.element;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        T t2 = ref$ObjectRef.element;
        if (t2 == 0) {
            Intrinsics.throwUninitializedPropertyAccessException("privacyMode");
        } else {
            distributionListPrivacyMode2 = (DistributionListPrivacyMode) t2;
        }
        return new DistributionListPrivacyData(distributionListPrivacyMode2, ref$IntRef.element, i);
    }

    public final int getRawMemberCount(DistributionListId distributionListId, DistributionListPrivacyMode distributionListPrivacyMode) {
        int i = 0;
        Cursor query = getReadableDatabase().query(MembershipTable.TABLE_NAME, SqlUtil.buildArgs("COUNT(*)"), "list_id = ? AND privacy_mode = ?", SqlUtil.buildArgs(distributionListId, Long.valueOf(distributionListPrivacyMode.serialize())), null, null, null);
        try {
            if (query.moveToFirst()) {
                i = query.getInt(0);
            }
            th = null;
            return i;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final DistributionListPrivacyMode getPrivacyMode(DistributionListId distributionListId) {
        DistributionListPrivacyMode distributionListPrivacyMode;
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Intrinsics.checkNotNullExpressionValue(readableDatabase, "readableDatabase");
        SelectBuilderPart2 from = SQLiteDatabaseExtensionsKt.select(readableDatabase, "privacy_mode").from("distribution_list");
        String serialize = distributionListId.serialize();
        Intrinsics.checkNotNullExpressionValue(serialize, "listId.serialize()");
        Cursor run = from.where("_id = ?", serialize).run();
        try {
            if (run.moveToFirst()) {
                distributionListPrivacyMode = (DistributionListPrivacyMode) CursorExtensionsKt.requireObject(run, "privacy_mode", DistributionListPrivacyMode.Serializer);
            } else {
                distributionListPrivacyMode = DistributionListPrivacyMode.ONLY_WITH;
            }
            th = null;
            return distributionListPrivacyMode;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final void removeMemberFromList(DistributionListId distributionListId, DistributionListPrivacyMode distributionListPrivacyMode, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        Intrinsics.checkNotNullParameter(recipientId, "member");
        getWritableDatabase().delete(MembershipTable.TABLE_NAME, "list_id = ? AND  recipient_id = ? AND privacy_mode = ?", SqlUtil.buildArgs(distributionListId, recipientId, Long.valueOf(distributionListPrivacyMode.serialize())));
    }

    public final void addMemberToList(DistributionListId distributionListId, DistributionListPrivacyMode distributionListPrivacyMode, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        Intrinsics.checkNotNullParameter(recipientId, "member");
        ContentValues contentValues = new ContentValues();
        contentValues.put(MembershipTable.LIST_ID, distributionListId.serialize());
        contentValues.put("recipient_id", recipientId.serialize());
        contentValues.put("privacy_mode", Long.valueOf(distributionListPrivacyMode.serialize()));
        getWritableDatabase().insert(MembershipTable.TABLE_NAME, (String) null, contentValues);
    }

    public final void removeAllMembers(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        DeleteBuilderPart1 delete = SQLiteDatabaseExtensionsKt.delete(writableDatabase, MembershipTable.TABLE_NAME);
        String serialize = distributionListId.serialize();
        Intrinsics.checkNotNullExpressionValue(serialize, "listId.serialize()");
        delete.where("list_id = ?", serialize).run();
    }

    public final void removeAllMembers(DistributionListId distributionListId, DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListId, "listId");
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        DeleteBuilderPart1 delete = SQLiteDatabaseExtensionsKt.delete(writableDatabase, MembershipTable.TABLE_NAME);
        String serialize = distributionListId.serialize();
        Intrinsics.checkNotNullExpressionValue(serialize, "listId.serialize()");
        delete.where("list_id = ? AND privacy_mode = ?", serialize, Long.valueOf(distributionListPrivacyMode.serialize())).run();
    }

    public final void remapRecipient(RecipientId recipientId, RecipientId recipientId2) {
        Intrinsics.checkNotNullParameter(recipientId, "oldId");
        Intrinsics.checkNotNullParameter(recipientId2, "newId");
        ContentValues contentValues = new ContentValues();
        contentValues.put("recipient_id", recipientId2.serialize());
        getWritableDatabase().update(MembershipTable.TABLE_NAME, contentValues, "recipient_id = ?", SqlUtil.buildArgs(recipientId));
    }

    public static /* synthetic */ void deleteList$default(DistributionListDatabase distributionListDatabase, DistributionListId distributionListId, long j, int i, Object obj) {
        if ((i & 2) != 0) {
            j = System.currentTimeMillis();
        }
        distributionListDatabase.deleteList(distributionListId, j);
    }

    public final void deleteList(DistributionListId distributionListId, long j) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        getWritableDatabase().update("distribution_list", ContentValuesKt.contentValuesOf(TuplesKt.to("name", createUniqueNameForDeletedStory()), TuplesKt.to("allows_replies", Boolean.FALSE), TuplesKt.to(ListTable.DELETION_TIMESTAMP, Long.valueOf(j))), "_id = ?", SqlUtil.buildArgs(distributionListId));
        getWritableDatabase().delete(MembershipTable.TABLE_NAME, "list_id = ?", SqlUtil.buildArgs(distributionListId));
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.Throwable, org.thoughtcrime.securesms.recipients.RecipientId] */
    public final RecipientId getRecipientIdForSyncRecord(SignalStoryDistributionListRecord signalStoryDistributionListRecord) {
        Intrinsics.checkNotNullParameter(signalStoryDistributionListRecord, "record");
        UUID parseOrNull = UuidUtil.parseOrNull(signalStoryDistributionListRecord.getIdentifier());
        th = 0;
        if (parseOrNull == null) {
            return th;
        }
        Cursor query = getReadableDatabase().query("distribution_list", new String[]{"recipient_id"}, "distribution_id = ?", SqlUtil.buildArgs(DistributionId.from(parseOrNull).toString()), null, null, null);
        if (query == null) {
            return th;
        }
        try {
            return query.moveToFirst() ? RecipientId.from(CursorUtil.requireLong(query, "recipient_id")) : th;
        } catch (Throwable th) {
            try {
                throw th;
            } finally {
                CloseableKt.closeFinally(query, th);
            }
        }
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.Throwable, org.thoughtcrime.securesms.recipients.RecipientId] */
    public final RecipientId getRecipientId(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        Cursor query = getReadableDatabase().query("distribution_list", new String[]{"recipient_id"}, "_id = ?", SqlUtil.buildArgs(distributionListId), null, null, null);
        th = 0;
        if (query == null) {
            return th;
        }
        try {
            return query.moveToFirst() ? RecipientId.from(CursorUtil.requireLong(query, "recipient_id")) : th;
        } catch (Throwable th) {
            try {
                throw th;
            } finally {
                CloseableKt.closeFinally(query, th);
            }
        }
    }

    public final void applyStorageSyncStoryDistributionListInsert(SignalStoryDistributionListRecord signalStoryDistributionListRecord) {
        DistributionListPrivacyMode distributionListPrivacyMode;
        Intrinsics.checkNotNullParameter(signalStoryDistributionListRecord, "insert");
        DistributionId from = DistributionId.from(UuidUtil.parseOrThrow(signalStoryDistributionListRecord.getIdentifier()));
        if (!Intrinsics.areEqual(from, DistributionId.MY_STORY)) {
            if (signalStoryDistributionListRecord.isBlockList() && signalStoryDistributionListRecord.getRecipients().isEmpty()) {
                distributionListPrivacyMode = DistributionListPrivacyMode.ALL;
            } else if (signalStoryDistributionListRecord.isBlockList()) {
                distributionListPrivacyMode = DistributionListPrivacyMode.ALL_EXCEPT;
            } else {
                distributionListPrivacyMode = DistributionListPrivacyMode.ONLY_WITH;
            }
            String name = signalStoryDistributionListRecord.getName();
            List<SignalServiceAddress> recipients = signalStoryDistributionListRecord.getRecipients();
            Intrinsics.checkNotNullExpressionValue(recipients, "insert.recipients");
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(recipients, 10));
            for (SignalServiceAddress signalServiceAddress : recipients) {
                arrayList.add(RecipientId.from(signalServiceAddress));
            }
            boolean allowsReplies = signalStoryDistributionListRecord.allowsReplies();
            long deletedAtTimestamp = signalStoryDistributionListRecord.getDeletedAtTimestamp();
            byte[] raw = signalStoryDistributionListRecord.getId().getRaw();
            Intrinsics.checkNotNullExpressionValue(name, "name");
            Intrinsics.checkNotNullExpressionValue(from, "distributionId");
            createList$default(this, name, arrayList, from, allowsReplies, deletedAtTimestamp, raw, false, distributionListPrivacyMode, 64, null);
            return;
        }
        throw new AssertionError("Should never try to insert My Story");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.lang.Throwable] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0063  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void applyStorageSyncStoryDistributionListUpdate(org.thoughtcrime.securesms.storage.StorageRecordUpdate<org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord> r15) {
        /*
            r14 = this;
            java.lang.String r0 = "update"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r15, r0)
            org.whispersystems.signalservice.api.storage.SignalRecord r0 = r15.getNew()
            org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord r0 = (org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord) r0
            byte[] r0 = r0.getIdentifier()
            java.util.UUID r0 = org.whispersystems.signalservice.api.util.UuidUtil.parseOrThrow(r0)
            org.whispersystems.signalservice.api.push.DistributionId r5 = org.whispersystems.signalservice.api.push.DistributionId.from(r0)
            org.thoughtcrime.securesms.database.SQLiteDatabase r6 = r14.getReadableDatabase()
            java.lang.String r0 = "_id"
            java.lang.String[] r8 = new java.lang.String[]{r0}
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = r5.toString()
            r3 = 0
            r1[r3] = r2
            java.lang.String[] r10 = org.signal.core.util.SqlUtil.buildArgs(r1)
            java.lang.String r7 = "distribution_list"
            java.lang.String r9 = "distribution_id = ?"
            r11 = 0
            r12 = 0
            r13 = 0
            android.database.Cursor r1 = r6.query(r7, r8, r9, r10, r11, r12, r13)
            r2 = 0
            if (r1 == 0) goto L_0x0055
            boolean r3 = r1.moveToFirst()     // Catch: all -> 0x004e
            if (r3 != 0) goto L_0x0044
            goto L_0x0055
        L_0x0044:
            long r3 = org.signal.core.util.CursorUtil.requireLong(r1, r0)     // Catch: all -> 0x004e
            org.thoughtcrime.securesms.database.model.DistributionListId r0 = org.thoughtcrime.securesms.database.model.DistributionListId.from(r3)     // Catch: all -> 0x004e
            r6 = r0
            goto L_0x0056
        L_0x004e:
            r15 = move-exception
            throw r15     // Catch: all -> 0x0050
        L_0x0050:
            r0 = move-exception
            kotlin.io.CloseableKt.closeFinally(r1, r15)
            throw r0
        L_0x0055:
            r6 = r2
        L_0x0056:
            kotlin.io.CloseableKt.closeFinally(r1, r2)
            if (r6 != 0) goto L_0x0063
            java.lang.String r15 = org.thoughtcrime.securesms.database.DistributionListDatabase.TAG
            java.lang.String r0 = "Cannot find required distribution list."
            org.signal.core.util.logging.Log.w(r15, r0)
            return
        L_0x0063:
            org.thoughtcrime.securesms.recipients.RecipientId r0 = r14.getRecipientId(r6)
            kotlin.jvm.internal.Intrinsics.checkNotNull(r0)
            org.thoughtcrime.securesms.database.SignalDatabase$Companion r1 = org.thoughtcrime.securesms.database.SignalDatabase.Companion
            org.thoughtcrime.securesms.database.RecipientDatabase r1 = r1.recipients()
            org.whispersystems.signalservice.api.storage.SignalRecord r2 = r15.getNew()
            org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord r2 = (org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord) r2
            org.whispersystems.signalservice.api.storage.StorageId r2 = r2.getId()
            byte[] r2 = r2.getRaw()
            r1.updateStorageId(r0, r2)
            org.whispersystems.signalservice.api.storage.SignalRecord r0 = r15.getNew()
            org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord r0 = (org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord) r0
            long r0 = r0.getDeletedAtTimestamp()
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x00b7
            java.util.UUID r0 = r5.asUuid()
            org.whispersystems.signalservice.api.push.DistributionId r1 = org.whispersystems.signalservice.api.push.DistributionId.MY_STORY
            java.util.UUID r1 = r1.asUuid()
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00a9
            java.lang.String r15 = org.thoughtcrime.securesms.database.DistributionListDatabase.TAG
            java.lang.String r0 = "Refusing to delete My Story."
            org.signal.core.util.logging.Log.w(r15, r0)
            return
        L_0x00a9:
            org.whispersystems.signalservice.api.storage.SignalRecord r15 = r15.getNew()
            org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord r15 = (org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord) r15
            long r0 = r15.getDeletedAtTimestamp()
            r14.deleteList(r6, r0)
            return
        L_0x00b7:
            org.whispersystems.signalservice.api.storage.SignalRecord r0 = r15.getNew()
            org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord r0 = (org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord) r0
            boolean r0 = r0.isBlockList()
            if (r0 == 0) goto L_0x00d6
            org.whispersystems.signalservice.api.storage.SignalRecord r0 = r15.getNew()
            org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord r0 = (org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord) r0
            java.util.List r0 = r0.getRecipients()
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x00d6
            org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode r0 = org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode.ALL
            goto L_0x00e7
        L_0x00d6:
            org.whispersystems.signalservice.api.storage.SignalRecord r0 = r15.getNew()
            org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord r0 = (org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord) r0
            boolean r0 = r0.isBlockList()
            if (r0 == 0) goto L_0x00e5
            org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode r0 = org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode.ALL_EXCEPT
            goto L_0x00e7
        L_0x00e5:
            org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode r0 = org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode.ONLY_WITH
        L_0x00e7:
            r3 = r0
            org.thoughtcrime.securesms.database.SQLiteDatabase r0 = r14.getWritableDatabase()
            org.thoughtcrime.securesms.database.DistributionListDatabase$applyStorageSyncStoryDistributionListUpdate$1 r7 = new org.thoughtcrime.securesms.database.DistributionListDatabase$applyStorageSyncStoryDistributionListUpdate$1
            r1 = r7
            r2 = r15
            r4 = r14
            r1.<init>(r2, r3, r4, r5, r6)
            org.signal.core.util.SQLiteDatabaseExtensionsKt.withinTransaction(r0, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.DistributionListDatabase.applyStorageSyncStoryDistributionListUpdate(org.thoughtcrime.securesms.storage.StorageRecordUpdate):void");
    }

    private final String createUniqueNameForDeletedStory() {
        return "DELETED-" + UUID.randomUUID();
    }

    private final String createUniqueNameForUnknownDistributionId() {
        return "DELETED-" + UUID.randomUUID();
    }

    public final void excludeFromStory(RecipientId recipientId, DistributionListRecord distributionListRecord) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(distributionListRecord, "record");
        excludeAllFromStory(CollectionsKt__CollectionsJVMKt.listOf(recipientId), distributionListRecord);
    }

    public final void excludeAllFromStory(List<? extends RecipientId> list, DistributionListRecord distributionListRecord) {
        Intrinsics.checkNotNullParameter(list, "recipientIds");
        Intrinsics.checkNotNullParameter(distributionListRecord, "record");
        SQLiteDatabaseExtensionsKt.withinTransaction(getWritableDatabase(), new Function1<SQLiteDatabase, Unit>(distributionListRecord, list, this) { // from class: org.thoughtcrime.securesms.database.DistributionListDatabase$excludeAllFromStory$1
            final /* synthetic */ List<RecipientId> $recipientIds;
            final /* synthetic */ DistributionListRecord $record;
            final /* synthetic */ DistributionListDatabase this$0;

            /* compiled from: DistributionListDatabase.kt */
            @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
            /* loaded from: classes4.dex */
            public /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    int[] iArr = new int[DistributionListPrivacyMode.values().length];
                    iArr[DistributionListPrivacyMode.ONLY_WITH.ordinal()] = 1;
                    iArr[DistributionListPrivacyMode.ALL_EXCEPT.ordinal()] = 2;
                    iArr[DistributionListPrivacyMode.ALL.ordinal()] = 3;
                    $EnumSwitchMapping$0 = iArr;
                }
            }

            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$record = r1;
                this.$recipientIds = r2;
                this.this$0 = r3;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(SQLiteDatabase sQLiteDatabase) {
                invoke(sQLiteDatabase);
                return Unit.INSTANCE;
            }

            public final void invoke(SQLiteDatabase sQLiteDatabase) {
                int i = WhenMappings.$EnumSwitchMapping$0[this.$record.getPrivacyMode().ordinal()];
                if (i == 1) {
                    List<RecipientId> list2 = this.$recipientIds;
                    DistributionListDatabase distributionListDatabase = this.this$0;
                    DistributionListRecord distributionListRecord2 = this.$record;
                    for (RecipientId recipientId : list2) {
                        distributionListDatabase.removeMemberFromList(distributionListRecord2.getId(), distributionListRecord2.getPrivacyMode(), recipientId);
                    }
                } else if (i == 2) {
                    List<RecipientId> list3 = this.$recipientIds;
                    DistributionListDatabase distributionListDatabase2 = this.this$0;
                    DistributionListRecord distributionListRecord3 = this.$record;
                    for (RecipientId recipientId2 : list3) {
                        distributionListDatabase2.addMemberToList(distributionListRecord3.getId(), distributionListRecord3.getPrivacyMode(), recipientId2);
                    }
                } else if (i == 3) {
                    DistributionListDatabase distributionListDatabase3 = this.this$0;
                    DistributionListId id = this.$record.getId();
                    DistributionListPrivacyMode distributionListPrivacyMode = DistributionListPrivacyMode.ALL_EXCEPT;
                    distributionListDatabase3.removeAllMembers(id, distributionListPrivacyMode);
                    this.this$0.setPrivacyMode(this.$record.getId(), distributionListPrivacyMode);
                    List<RecipientId> list4 = this.$recipientIds;
                    DistributionListDatabase distributionListDatabase4 = this.this$0;
                    DistributionListRecord distributionListRecord4 = this.$record;
                    for (RecipientId recipientId3 : list4) {
                        distributionListDatabase4.addMemberToList(distributionListRecord4.getId(), DistributionListPrivacyMode.ALL_EXCEPT, recipientId3);
                    }
                }
            }
        });
    }
}
