package org.thoughtcrime.securesms.database.model;

/* loaded from: classes4.dex */
public class IncomingSticker {
    private final String contentType;
    private final String emoji;
    private final boolean isCover;
    private final boolean isInstalled;
    private final String packAuthor;
    private final String packId;
    private final String packKey;
    private final String packTitle;
    private final int stickerId;

    public IncomingSticker(String str, String str2, String str3, String str4, int i, String str5, String str6, boolean z, boolean z2) {
        this.packId = str;
        this.packKey = str2;
        this.packTitle = str3;
        this.packAuthor = str4;
        this.stickerId = i;
        this.emoji = str5;
        this.contentType = str6;
        this.isCover = z;
        this.isInstalled = z2;
    }

    public String getPackKey() {
        return this.packKey;
    }

    public String getPackId() {
        return this.packId;
    }

    public String getPackTitle() {
        return this.packTitle;
    }

    public String getPackAuthor() {
        return this.packAuthor;
    }

    public int getStickerId() {
        return this.stickerId;
    }

    public String getEmoji() {
        return this.emoji;
    }

    public String getContentType() {
        return this.contentType;
    }

    public boolean isCover() {
        return this.isCover;
    }

    public boolean isInstalled() {
        return this.isInstalled;
    }
}
