package org.thoughtcrime.securesms.database.model;

import android.text.SpannableString;
import java.util.List;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class Quote {
    private final SlideDeck attachment;
    private final RecipientId author;
    private final long id;
    private final List<Mention> mentions;
    private final boolean missing;
    private final QuoteModel.Type quoteType;
    private final CharSequence text;

    public Quote(long j, RecipientId recipientId, CharSequence charSequence, boolean z, SlideDeck slideDeck, List<Mention> list, QuoteModel.Type type) {
        this.id = j;
        this.author = recipientId;
        this.missing = z;
        this.attachment = slideDeck;
        this.mentions = list;
        this.quoteType = type;
        SpannableString spannableString = new SpannableString(charSequence);
        MentionAnnotation.setMentionAnnotations(spannableString, list);
        this.text = spannableString;
    }

    public Quote withAttachment(SlideDeck slideDeck) {
        return new Quote(this.id, this.author, this.text, this.missing, slideDeck, this.mentions, this.quoteType);
    }

    public long getId() {
        return this.id;
    }

    public RecipientId getAuthor() {
        return this.author;
    }

    public CharSequence getDisplayText() {
        return this.text;
    }

    public boolean isOriginalMissing() {
        return this.missing;
    }

    public SlideDeck getAttachment() {
        return this.attachment;
    }

    public QuoteModel.Type getQuoteType() {
        return this.quoteType;
    }
}
