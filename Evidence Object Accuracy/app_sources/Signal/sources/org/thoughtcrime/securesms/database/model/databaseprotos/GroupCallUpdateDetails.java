package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public final class GroupCallUpdateDetails extends GeneratedMessageLite<GroupCallUpdateDetails, Builder> implements GroupCallUpdateDetailsOrBuilder {
    private static final GroupCallUpdateDetails DEFAULT_INSTANCE;
    public static final int ERAID_FIELD_NUMBER;
    public static final int INCALLUUIDS_FIELD_NUMBER;
    public static final int ISCALLFULL_FIELD_NUMBER;
    private static volatile Parser<GroupCallUpdateDetails> PARSER;
    public static final int STARTEDCALLTIMESTAMP_FIELD_NUMBER;
    public static final int STARTEDCALLUUID_FIELD_NUMBER;
    private String eraId_ = "";
    private Internal.ProtobufList<String> inCallUuids_ = GeneratedMessageLite.emptyProtobufList();
    private boolean isCallFull_;
    private long startedCallTimestamp_;
    private String startedCallUuid_ = "";

    private GroupCallUpdateDetails() {
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
    public String getEraId() {
        return this.eraId_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
    public ByteString getEraIdBytes() {
        return ByteString.copyFromUtf8(this.eraId_);
    }

    public void setEraId(String str) {
        str.getClass();
        this.eraId_ = str;
    }

    public void clearEraId() {
        this.eraId_ = getDefaultInstance().getEraId();
    }

    public void setEraIdBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.eraId_ = byteString.toStringUtf8();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
    public String getStartedCallUuid() {
        return this.startedCallUuid_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
    public ByteString getStartedCallUuidBytes() {
        return ByteString.copyFromUtf8(this.startedCallUuid_);
    }

    public void setStartedCallUuid(String str) {
        str.getClass();
        this.startedCallUuid_ = str;
    }

    public void clearStartedCallUuid() {
        this.startedCallUuid_ = getDefaultInstance().getStartedCallUuid();
    }

    public void setStartedCallUuidBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.startedCallUuid_ = byteString.toStringUtf8();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
    public long getStartedCallTimestamp() {
        return this.startedCallTimestamp_;
    }

    public void setStartedCallTimestamp(long j) {
        this.startedCallTimestamp_ = j;
    }

    public void clearStartedCallTimestamp() {
        this.startedCallTimestamp_ = 0;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
    public List<String> getInCallUuidsList() {
        return this.inCallUuids_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
    public int getInCallUuidsCount() {
        return this.inCallUuids_.size();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
    public String getInCallUuids(int i) {
        return this.inCallUuids_.get(i);
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
    public ByteString getInCallUuidsBytes(int i) {
        return ByteString.copyFromUtf8(this.inCallUuids_.get(i));
    }

    private void ensureInCallUuidsIsMutable() {
        Internal.ProtobufList<String> protobufList = this.inCallUuids_;
        if (!protobufList.isModifiable()) {
            this.inCallUuids_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setInCallUuids(int i, String str) {
        str.getClass();
        ensureInCallUuidsIsMutable();
        this.inCallUuids_.set(i, str);
    }

    public void addInCallUuids(String str) {
        str.getClass();
        ensureInCallUuidsIsMutable();
        this.inCallUuids_.add(str);
    }

    public void addAllInCallUuids(Iterable<String> iterable) {
        ensureInCallUuidsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.inCallUuids_);
    }

    public void clearInCallUuids() {
        this.inCallUuids_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void addInCallUuidsBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        ensureInCallUuidsIsMutable();
        this.inCallUuids_.add(byteString.toStringUtf8());
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
    public boolean getIsCallFull() {
        return this.isCallFull_;
    }

    public void setIsCallFull(boolean z) {
        this.isCallFull_ = z;
    }

    public void clearIsCallFull() {
        this.isCallFull_ = false;
    }

    public static GroupCallUpdateDetails parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static GroupCallUpdateDetails parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static GroupCallUpdateDetails parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static GroupCallUpdateDetails parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static GroupCallUpdateDetails parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static GroupCallUpdateDetails parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static GroupCallUpdateDetails parseFrom(InputStream inputStream) throws IOException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupCallUpdateDetails parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupCallUpdateDetails parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupCallUpdateDetails parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupCallUpdateDetails parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static GroupCallUpdateDetails parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupCallUpdateDetails) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(GroupCallUpdateDetails groupCallUpdateDetails) {
        return DEFAULT_INSTANCE.createBuilder(groupCallUpdateDetails);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<GroupCallUpdateDetails, Builder> implements GroupCallUpdateDetailsOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(GroupCallUpdateDetails.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
        public String getEraId() {
            return ((GroupCallUpdateDetails) this.instance).getEraId();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
        public ByteString getEraIdBytes() {
            return ((GroupCallUpdateDetails) this.instance).getEraIdBytes();
        }

        public Builder setEraId(String str) {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).setEraId(str);
            return this;
        }

        public Builder clearEraId() {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).clearEraId();
            return this;
        }

        public Builder setEraIdBytes(ByteString byteString) {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).setEraIdBytes(byteString);
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
        public String getStartedCallUuid() {
            return ((GroupCallUpdateDetails) this.instance).getStartedCallUuid();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
        public ByteString getStartedCallUuidBytes() {
            return ((GroupCallUpdateDetails) this.instance).getStartedCallUuidBytes();
        }

        public Builder setStartedCallUuid(String str) {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).setStartedCallUuid(str);
            return this;
        }

        public Builder clearStartedCallUuid() {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).clearStartedCallUuid();
            return this;
        }

        public Builder setStartedCallUuidBytes(ByteString byteString) {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).setStartedCallUuidBytes(byteString);
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
        public long getStartedCallTimestamp() {
            return ((GroupCallUpdateDetails) this.instance).getStartedCallTimestamp();
        }

        public Builder setStartedCallTimestamp(long j) {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).setStartedCallTimestamp(j);
            return this;
        }

        public Builder clearStartedCallTimestamp() {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).clearStartedCallTimestamp();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
        public List<String> getInCallUuidsList() {
            return Collections.unmodifiableList(((GroupCallUpdateDetails) this.instance).getInCallUuidsList());
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
        public int getInCallUuidsCount() {
            return ((GroupCallUpdateDetails) this.instance).getInCallUuidsCount();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
        public String getInCallUuids(int i) {
            return ((GroupCallUpdateDetails) this.instance).getInCallUuids(i);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
        public ByteString getInCallUuidsBytes(int i) {
            return ((GroupCallUpdateDetails) this.instance).getInCallUuidsBytes(i);
        }

        public Builder setInCallUuids(int i, String str) {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).setInCallUuids(i, str);
            return this;
        }

        public Builder addInCallUuids(String str) {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).addInCallUuids(str);
            return this;
        }

        public Builder addAllInCallUuids(Iterable<String> iterable) {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).addAllInCallUuids(iterable);
            return this;
        }

        public Builder clearInCallUuids() {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).clearInCallUuids();
            return this;
        }

        public Builder addInCallUuidsBytes(ByteString byteString) {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).addInCallUuidsBytes(byteString);
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetailsOrBuilder
        public boolean getIsCallFull() {
            return ((GroupCallUpdateDetails) this.instance).getIsCallFull();
        }

        public Builder setIsCallFull(boolean z) {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).setIsCallFull(z);
            return this;
        }

        public Builder clearIsCallFull() {
            copyOnWrite();
            ((GroupCallUpdateDetails) this.instance).clearIsCallFull();
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new GroupCallUpdateDetails();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0000\u0001\u0000\u0001Ȉ\u0002Ȉ\u0003\u0002\u0004Ț\u0005\u0007", new Object[]{"eraId_", "startedCallUuid_", "startedCallTimestamp_", "inCallUuids_", "isCallFull_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<GroupCallUpdateDetails> parser = PARSER;
                if (parser == null) {
                    synchronized (GroupCallUpdateDetails.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        GroupCallUpdateDetails groupCallUpdateDetails = new GroupCallUpdateDetails();
        DEFAULT_INSTANCE = groupCallUpdateDetails;
        GeneratedMessageLite.registerDefaultInstance(GroupCallUpdateDetails.class, groupCallUpdateDetails);
    }

    public static GroupCallUpdateDetails getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<GroupCallUpdateDetails> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
