package org.thoughtcrime.securesms.database.helpers;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import java.util.HashSet;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SessionDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet;
import org.thoughtcrime.securesms.phonenumbers.NumberUtil;
import org.thoughtcrime.securesms.util.DelimiterUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class RecipientIdMigrationHelper {
    private static final String TAG = Log.tag(RecipientIdMigrationHelper.class);

    public static void execute(SQLiteDatabase sQLiteDatabase) {
        String str;
        String str2;
        String str3 = TAG;
        Log.i(str3, "Starting the recipient ID migration.");
        long currentTimeMillis = System.currentTimeMillis();
        Log.i(str3, "Starting inserts for missing recipients.");
        sQLiteDatabase.execSQL(buildInsertMissingRecipientStatement("identities", "address"));
        sQLiteDatabase.execSQL(buildInsertMissingRecipientStatement(SessionDatabase.TABLE_NAME, "address"));
        sQLiteDatabase.execSQL(buildInsertMissingRecipientStatement(ThreadDatabase.TABLE_NAME, "recipient_ids"));
        String str4 = "sms";
        sQLiteDatabase.execSQL(buildInsertMissingRecipientStatement(str4, "address"));
        String str5 = "mms";
        sQLiteDatabase.execSQL(buildInsertMissingRecipientStatement(str5, "address"));
        String str6 = "quote_author";
        String str7 = "mute_until";
        sQLiteDatabase.execSQL(buildInsertMissingRecipientStatement(str5, str6));
        String str8 = "notification_channel";
        sQLiteDatabase.execSQL(buildInsertMissingRecipientStatement(GroupReceiptDatabase.TABLE_NAME, "address"));
        String str9 = "call_vibrate";
        String str10 = "call_ringtone";
        sQLiteDatabase.execSQL(buildInsertMissingRecipientStatement(ChooseGroupStoryBottomSheet.RESULT_SET, "group_id"));
        StringBuilder sb = new StringBuilder();
        sb.append("Finished inserts for missing recipients in ");
        String str11 = "group_id";
        sb.append(System.currentTimeMillis() - currentTimeMillis);
        sb.append(" ms.");
        Log.i(str3, sb.toString());
        long currentTimeMillis2 = System.currentTimeMillis();
        Log.i(str3, "Starting updates for invalid or missing addresses.");
        sQLiteDatabase.execSQL(buildMissingAddressUpdateStatement(str4, "address"));
        sQLiteDatabase.execSQL(buildMissingAddressUpdateStatement(str5, "address"));
        sQLiteDatabase.execSQL(buildMissingAddressUpdateStatement(str5, str6));
        Log.i(str3, "Finished updates for invalid or missing addresses in " + (System.currentTimeMillis() - currentTimeMillis2) + " ms.");
        sQLiteDatabase.execSQL("ALTER TABLE groups ADD COLUMN recipient_id INTEGER DEFAULT 0");
        long currentTimeMillis3 = System.currentTimeMillis();
        Log.i(str3, "Starting recipient ID updates.");
        sQLiteDatabase.execSQL(buildUpdateAddressToRecipientIdStatement("identities", "address"));
        sQLiteDatabase.execSQL(buildUpdateAddressToRecipientIdStatement(SessionDatabase.TABLE_NAME, "address"));
        sQLiteDatabase.execSQL(buildUpdateAddressToRecipientIdStatement(ThreadDatabase.TABLE_NAME, "recipient_ids"));
        sQLiteDatabase.execSQL(buildUpdateAddressToRecipientIdStatement(str4, "address"));
        sQLiteDatabase.execSQL(buildUpdateAddressToRecipientIdStatement(str5, "address"));
        sQLiteDatabase.execSQL(buildUpdateAddressToRecipientIdStatement(str5, str6));
        sQLiteDatabase.execSQL(buildUpdateAddressToRecipientIdStatement(GroupReceiptDatabase.TABLE_NAME, "address"));
        sQLiteDatabase.execSQL("UPDATE groups SET recipient_id = (SELECT _id FROM recipient_preferences WHERE recipient_preferences.recipient_ids = groups.group_id)");
        Log.i(str3, "Finished recipient ID updates in " + (System.currentTimeMillis() - currentTimeMillis3) + " ms.");
        long currentTimeMillis4 = System.currentTimeMillis();
        Log.i(str3, "Starting to find missing group recipients.");
        HashSet<String> hashSet = new HashSet();
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT members FROM groups", (String[]) null);
        while (rawQuery != null) {
            try {
                if (!rawQuery.moveToNext()) {
                    break;
                }
                String[] split = DelimiterUtil.split(rawQuery.getString(rawQuery.getColumnIndexOrThrow("members")), ',');
                int length = split.length;
                int i = 0;
                while (i < length) {
                    String unescape = DelimiterUtil.unescape(split[i], ',');
                    if (!TextUtils.isEmpty(unescape) && !recipientExists(sQLiteDatabase, unescape)) {
                        hashSet.add(unescape);
                    }
                    i++;
                    length = length;
                    str4 = str4;
                }
                str6 = str6;
                str5 = str5;
            } finally {
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        String str12 = TAG;
        Log.i(str12, "Finished finding " + hashSet.size() + " missing group recipients in " + (System.currentTimeMillis() - currentTimeMillis4) + " ms.");
        long currentTimeMillis5 = System.currentTimeMillis();
        Log.i(str12, "Starting the insert of missing group recipients.");
        for (String str13 : hashSet) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("recipient_ids", str13);
            sQLiteDatabase.insert("recipient_preferences", (String) null, contentValues);
        }
        String str14 = TAG;
        Log.i(str14, "Finished inserting missing group recipients in " + (System.currentTimeMillis() - currentTimeMillis5) + " ms.");
        long currentTimeMillis6 = System.currentTimeMillis();
        Log.i(str14, "Starting group recipient ID updates.");
        rawQuery = sQLiteDatabase.rawQuery("SELECT _id, members FROM groups", (String[]) null);
        while (rawQuery != null) {
            try {
                if (!rawQuery.moveToNext()) {
                    break;
                }
                long j = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_id"));
                String[] split2 = DelimiterUtil.split(rawQuery.getString(rawQuery.getColumnIndexOrThrow("members")), ',');
                long[] jArr = new long[split2.length];
                int i2 = 0;
                while (i2 < split2.length) {
                    jArr[i2] = requireRecipientId(sQLiteDatabase, DelimiterUtil.unescape(split2[i2], ','));
                    i2++;
                    split2 = split2;
                }
                sQLiteDatabase.execSQL("UPDATE groups SET members = ? WHERE _id = ?", new String[]{Util.join(jArr, ","), String.valueOf(j)});
            } finally {
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS group_recipient_id_index ON groups (recipient_id)");
        String str15 = TAG;
        Log.i(str15, "Finished group recipient ID updates in " + (System.currentTimeMillis() - currentTimeMillis6) + " ms.");
        long currentTimeMillis7 = System.currentTimeMillis();
        Log.i(str15, "Starting to copy the recipient table.");
        sQLiteDatabase.execSQL("CREATE TABLE recipient (_id INTEGER PRIMARY KEY AUTOINCREMENT, uuid TEXT UNIQUE DEFAULT NULL, phone TEXT UNIQUE DEFAULT NULL, email TEXT UNIQUE DEFAULT NULL, group_id TEXT UNIQUE DEFAULT NULL, blocked INTEGER DEFAULT 0, message_ringtone TEXT DEFAULT NULL, message_vibrate INTEGER DEFAULT 0, call_ringtone TEXT DEFAULT NULL, call_vibrate INTEGER DEFAULT 0, notification_channel TEXT DEFAULT NULL, mute_until INTEGER DEFAULT 0, color TEXT DEFAULT NULL, seen_invite_reminder INTEGER DEFAULT 0, default_subscription_id INTEGER DEFAULT -1, message_expiration_time INTEGER DEFAULT 0, registered INTEGER DEFAULT 0, system_display_name TEXT DEFAULT NULL, system_photo_uri TEXT DEFAULT NULL, system_phone_label TEXT DEFAULT NULL, system_contact_uri TEXT DEFAULT NULL, profile_key TEXT DEFAULT NULL, signal_profile_name TEXT DEFAULT NULL, signal_profile_avatar TEXT DEFAULT NULL, profile_sharing INTEGER DEFAULT 0, unidentified_access_mode INTEGER DEFAULT 0, force_sms_selection INTEGER DEFAULT 0)");
        String str16 = "recipient_ids";
        Cursor query = sQLiteDatabase.query("recipient_preferences", null, null, null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                String string = query.getString(query.getColumnIndexOrThrow(str16));
                boolean isEncodedGroup = GroupId.isEncodedGroup(string);
                boolean z = !isEncodedGroup && NumberUtil.isValidEmail(string);
                boolean z2 = !isEncodedGroup && !z;
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("_id", Long.valueOf(query.getLong(query.getColumnIndexOrThrow("_id"))));
                contentValues2.put(RecipientDatabase.SERVICE_ID, (String) null);
                contentValues2.put(RecipientDatabase.PHONE, z2 ? string : null);
                contentValues2.put(RecipientDatabase.EMAIL, z ? string : null);
                if (isEncodedGroup) {
                    str = str11;
                    str2 = string;
                } else {
                    str = str11;
                    str2 = null;
                }
                contentValues2.put(str, str2);
                contentValues2.put(RecipientDatabase.BLOCKED, Integer.valueOf(query.getInt(query.getColumnIndexOrThrow("block"))));
                contentValues2.put("message_ringtone", query.getString(query.getColumnIndexOrThrow("notification")));
                contentValues2.put("message_vibrate", query.getString(query.getColumnIndexOrThrow("vibrate")));
                contentValues2.put(str10, query.getString(query.getColumnIndexOrThrow(str10)));
                contentValues2.put(str9, query.getString(query.getColumnIndexOrThrow(str9)));
                contentValues2.put(str8, query.getString(query.getColumnIndexOrThrow(str8)));
                str10 = str10;
                contentValues2.put(str7, Long.valueOf(query.getLong(query.getColumnIndexOrThrow(str7))));
                str11 = str;
                contentValues2.put(NotificationProfileDatabase.NotificationProfileTable.COLOR, query.getString(query.getColumnIndexOrThrow(NotificationProfileDatabase.NotificationProfileTable.COLOR)));
                contentValues2.put("seen_invite_reminder", Integer.valueOf(query.getInt(query.getColumnIndexOrThrow("seen_invite_reminder"))));
                contentValues2.put("default_subscription_id", Integer.valueOf(query.getInt(query.getColumnIndexOrThrow("default_subscription_id"))));
                contentValues2.put("message_expiration_time", Integer.valueOf(query.getInt(query.getColumnIndexOrThrow("expire_messages"))));
                contentValues2.put(RecipientDatabase.REGISTERED, Integer.valueOf(query.getInt(query.getColumnIndexOrThrow(RecipientDatabase.REGISTERED))));
                contentValues2.put(RecipientDatabase.SYSTEM_JOINED_NAME, query.getString(query.getColumnIndexOrThrow(RecipientDatabase.SYSTEM_JOINED_NAME)));
                contentValues2.put("system_photo_uri", query.getString(query.getColumnIndexOrThrow("system_contact_photo")));
                contentValues2.put(RecipientDatabase.SYSTEM_PHONE_LABEL, query.getString(query.getColumnIndexOrThrow(RecipientDatabase.SYSTEM_PHONE_LABEL)));
                contentValues2.put("system_contact_uri", query.getString(query.getColumnIndexOrThrow("system_contact_uri")));
                contentValues2.put("profile_key", query.getString(query.getColumnIndexOrThrow("profile_key")));
                contentValues2.put("signal_profile_name", query.getString(query.getColumnIndexOrThrow("signal_profile_name")));
                contentValues2.put("signal_profile_avatar", query.getString(query.getColumnIndexOrThrow("signal_profile_avatar")));
                contentValues2.put(RecipientDatabase.PROFILE_SHARING, Integer.valueOf(query.getInt(query.getColumnIndexOrThrow("profile_sharing_approval"))));
                contentValues2.put("unidentified_access_mode", Integer.valueOf(query.getInt(query.getColumnIndexOrThrow("unidentified_access_mode"))));
                contentValues2.put(RecipientDatabase.FORCE_SMS_SELECTION, Integer.valueOf(query.getInt(query.getColumnIndexOrThrow(RecipientDatabase.FORCE_SMS_SELECTION))));
                sQLiteDatabase.insert(RecipientDatabase.TABLE_NAME, (String) null, contentValues2);
                str16 = str16;
                str9 = str9;
                str8 = str8;
                str7 = str7;
            } finally {
                try {
                    query.close();
                } catch (Throwable th) {
                    th.addSuppressed(th);
                }
            }
        }
        if (query != null) {
            query.close();
        }
        sQLiteDatabase.execSQL("DROP TABLE recipient_preferences");
        String str17 = TAG;
        Log.i(str17, "Finished copying the recipient table in " + (System.currentTimeMillis() - currentTimeMillis7) + " ms.");
        long currentTimeMillis8 = System.currentTimeMillis();
        Log.i(str17, "Starting DB integrity sanity checks.");
        assertEmptyQuery(sQLiteDatabase, "identities", buildSanityCheckQuery("identities", "address"));
        assertEmptyQuery(sQLiteDatabase, SessionDatabase.TABLE_NAME, buildSanityCheckQuery(SessionDatabase.TABLE_NAME, "address"));
        assertEmptyQuery(sQLiteDatabase, ChooseGroupStoryBottomSheet.RESULT_SET, buildSanityCheckQuery(ChooseGroupStoryBottomSheet.RESULT_SET, "recipient_id"));
        assertEmptyQuery(sQLiteDatabase, ThreadDatabase.TABLE_NAME, buildSanityCheckQuery(ThreadDatabase.TABLE_NAME, str16));
        assertEmptyQuery(sQLiteDatabase, str4, buildSanityCheckQuery(str4, "address"));
        assertEmptyQuery(sQLiteDatabase, "mms -- address", buildSanityCheckQuery(str5, "address"));
        assertEmptyQuery(sQLiteDatabase, "mms -- quote_author", buildSanityCheckQuery(str5, str6));
        assertEmptyQuery(sQLiteDatabase, GroupReceiptDatabase.TABLE_NAME, buildSanityCheckQuery(GroupReceiptDatabase.TABLE_NAME, "address"));
        Log.i(str17, "Finished DB integrity sanity checks in " + (System.currentTimeMillis() - currentTimeMillis8) + " ms.");
        Log.i(str17, "Finished recipient ID migration in " + (System.currentTimeMillis() - currentTimeMillis) + " ms.");
    }

    private static String buildUpdateAddressToRecipientIdStatement(String str, String str2) {
        return "UPDATE " + str + " SET " + str2 + "=(SELECT _id FROM recipient_preferences WHERE recipient_preferences.recipient_ids = " + str + "." + str2 + ")";
    }

    private static String buildInsertMissingRecipientStatement(String str, String str2) {
        return "INSERT INTO recipient_preferences(recipient_ids) SELECT DISTINCT " + str2 + " FROM " + str + " WHERE " + str2 + " != '' AND " + str2 + " != 'insert-address-column' AND " + str2 + " NOT NULL AND " + str2 + " NOT IN (SELECT recipient_ids FROM recipient_preferences)";
    }

    private static String buildMissingAddressUpdateStatement(String str, String str2) {
        return "UPDATE " + str + " SET " + str2 + " = -1 WHERE " + str2 + " = '' OR " + str2 + " IS NULL OR " + str2 + " = 'insert-address-token'";
    }

    private static boolean recipientExists(SQLiteDatabase sQLiteDatabase, String str) {
        return getRecipientId(sQLiteDatabase, str) != null;
    }

    private static Long getRecipientId(SQLiteDatabase sQLiteDatabase, String str) {
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT _id FROM recipient_preferences WHERE recipient_ids = ?", new String[]{str});
        if (rawQuery != null) {
            try {
                if (rawQuery.moveToFirst()) {
                    Long valueOf = Long.valueOf(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_id")));
                    rawQuery.close();
                    return valueOf;
                }
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        return null;
    }

    private static long requireRecipientId(SQLiteDatabase sQLiteDatabase, String str) {
        Long recipientId = getRecipientId(sQLiteDatabase, str);
        if (recipientId != null) {
            return recipientId.longValue();
        }
        throw new MissingRecipientError(str);
    }

    private static String buildSanityCheckQuery(String str, String str2) {
        return "SELECT " + str2 + " FROM " + str + " WHERE " + str2 + " != -1 AND " + str2 + " NOT IN (SELECT _id FROM recipient)";
    }

    private static void assertEmptyQuery(SQLiteDatabase sQLiteDatabase, String str, String str2) {
        Cursor rawQuery = sQLiteDatabase.rawQuery(str2, (String[]) null);
        if (rawQuery != null) {
            try {
                if (rawQuery.moveToFirst()) {
                    throw new FailedSanityCheckError(str);
                }
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
    }

    /* loaded from: classes4.dex */
    public static final class MissingRecipientError extends AssertionError {
        MissingRecipientError(String str) {
            super("Could not find recipient with address " + str);
        }
    }

    /* loaded from: classes4.dex */
    public static final class FailedSanityCheckError extends AssertionError {
        FailedSanityCheckError(String str) {
            super("Sanity check failed for tag '" + str + "'");
        }
    }
}
