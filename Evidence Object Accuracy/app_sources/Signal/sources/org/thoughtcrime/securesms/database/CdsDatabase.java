package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import androidx.core.content.ContentValuesKt;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.CursorExtensionsKt;
import org.signal.core.util.SQLiteDatabaseExtensionsKt;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;

/* compiled from: CdsDatabase.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\bJ\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nJ\"\u0010\f\u001a\u00020\b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/database/CdsDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "clearAll", "", "getAllE164s", "", "", "updateAfterCdsQuery", "newE164s", "seenE164s", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CdsDatabase extends Database {
    public static final String CREATE_TABLE;
    public static final Companion Companion = new Companion(null);
    public static final String E164;
    private static final String ID;
    private static final String LAST_SEEN_AT;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(CdsDatabase.class);

    /* compiled from: CdsDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \n*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/CdsDatabase$Companion;", "", "()V", "CREATE_TABLE", "", "E164", "ID", "LAST_SEEN_AT", "TABLE_NAME", "TAG", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CdsDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    public final Set<String> getAllE164s() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Intrinsics.checkNotNullExpressionValue(readableDatabase, "readableDatabase");
        Cursor run = SQLiteDatabaseExtensionsKt.select(readableDatabase, E164).from(TABLE_NAME).run();
        while (run.moveToNext()) {
            try {
                linkedHashSet.add(CursorExtensionsKt.requireNonNullString(run, E164));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return linkedHashSet;
    }

    public final void updateAfterCdsQuery(Set<String> set, Set<String> set2) {
        Intrinsics.checkNotNullParameter(set, "newE164s");
        Intrinsics.checkNotNullParameter(set2, "seenE164s");
        long currentTimeMillis = System.currentTimeMillis();
        getWritableDatabase().beginTransaction();
        try {
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
            Iterator<T> it = set.iterator();
            while (it.hasNext()) {
                arrayList.add(ContentValuesKt.contentValuesOf(TuplesKt.to(E164, (String) it.next())));
            }
            for (SqlUtil.Query query : SqlUtil.buildBulkInsert(TABLE_NAME, new String[]{E164}, arrayList)) {
                getWritableDatabase().execSQL(query.getWhere(), query.getWhereArgs());
            }
            ContentValues contentValuesOf = ContentValuesKt.contentValuesOf(TuplesKt.to(LAST_SEEN_AT, Long.valueOf(currentTimeMillis)));
            for (SqlUtil.Query query2 : SqlUtil.buildCollectionQuery(E164, set2)) {
                getWritableDatabase().update(TABLE_NAME, contentValuesOf, query2.getWhere(), query2.getWhereArgs());
            }
            getWritableDatabase().setTransactionSuccessful();
        } finally {
            getWritableDatabase().endTransaction();
        }
    }

    public final void clearAll() {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        SQLiteDatabaseExtensionsKt.delete(writableDatabase, TABLE_NAME).run();
    }
}
