package org.thoughtcrime.securesms.database;

import android.content.Context;
import java.util.Set;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* loaded from: classes.dex */
public abstract class Database {
    protected static final String[] COUNT = {"COUNT(*)"};
    protected static final String ID_WHERE;
    protected final Context context;
    protected SignalDatabase databaseHelper;

    public Database(Context context, SignalDatabase signalDatabase) {
        this.context = context;
        this.databaseHelper = signalDatabase;
    }

    public void notifyConversationListeners(Set<Long> set) {
        ApplicationDependencies.getDatabaseObserver().notifyConversationListeners(set);
        for (Long l : set) {
            notifyConversationListeners(l.longValue());
        }
    }

    public void notifyConversationListeners(long j) {
        ApplicationDependencies.getDatabaseObserver().notifyConversationListeners(j);
    }

    public void notifyVerboseConversationListeners(Set<Long> set) {
        ApplicationDependencies.getDatabaseObserver().notifyVerboseConversationListeners(set);
    }

    public void notifyConversationListListeners() {
        ApplicationDependencies.getDatabaseObserver().notifyConversationListListeners();
    }

    public void notifyStickerPackListeners() {
        ApplicationDependencies.getDatabaseObserver().notifyStickerPackObservers();
    }

    public void notifyStickerListeners() {
        ApplicationDependencies.getDatabaseObserver().notifyStickerObservers();
    }

    public void notifyAttachmentListeners() {
        ApplicationDependencies.getDatabaseObserver().notifyAttachmentObservers();
    }

    public void reset(SignalDatabase signalDatabase) {
        this.databaseHelper = signalDatabase;
    }

    public SQLiteDatabase getReadableDatabase() {
        return this.databaseHelper.getSignalReadableDatabase();
    }

    public SQLiteDatabase getWritableDatabase() {
        return this.databaseHelper.getSignalWritableDatabase();
    }
}
