package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;
import org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList;

@Deprecated
/* loaded from: classes4.dex */
public interface ReactionListOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    ReactionList.Reaction getReactions(int i);

    int getReactionsCount();

    List<ReactionList.Reaction> getReactionsList();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
