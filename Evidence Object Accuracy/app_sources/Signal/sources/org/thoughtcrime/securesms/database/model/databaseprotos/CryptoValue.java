package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes4.dex */
public final class CryptoValue extends GeneratedMessageLite<CryptoValue, Builder> implements CryptoValueOrBuilder {
    private static final CryptoValue DEFAULT_INSTANCE;
    public static final int MOBILECOINVALUE_FIELD_NUMBER;
    private static volatile Parser<CryptoValue> PARSER;
    private int valueCase_ = 0;
    private Object value_;

    /* loaded from: classes4.dex */
    public interface MobileCoinValueOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getPicoMobileCoin();

        ByteString getPicoMobileCoinBytes();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private CryptoValue() {
    }

    /* loaded from: classes4.dex */
    public static final class MobileCoinValue extends GeneratedMessageLite<MobileCoinValue, Builder> implements MobileCoinValueOrBuilder {
        private static final MobileCoinValue DEFAULT_INSTANCE;
        private static volatile Parser<MobileCoinValue> PARSER;
        public static final int PICOMOBILECOIN_FIELD_NUMBER;
        private String picoMobileCoin_ = "";

        private MobileCoinValue() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValue.MobileCoinValueOrBuilder
        public String getPicoMobileCoin() {
            return this.picoMobileCoin_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValue.MobileCoinValueOrBuilder
        public ByteString getPicoMobileCoinBytes() {
            return ByteString.copyFromUtf8(this.picoMobileCoin_);
        }

        public void setPicoMobileCoin(String str) {
            str.getClass();
            this.picoMobileCoin_ = str;
        }

        public void clearPicoMobileCoin() {
            this.picoMobileCoin_ = getDefaultInstance().getPicoMobileCoin();
        }

        public void setPicoMobileCoinBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.picoMobileCoin_ = byteString.toStringUtf8();
        }

        public static MobileCoinValue parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (MobileCoinValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static MobileCoinValue parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (MobileCoinValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static MobileCoinValue parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (MobileCoinValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static MobileCoinValue parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (MobileCoinValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static MobileCoinValue parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (MobileCoinValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static MobileCoinValue parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (MobileCoinValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static MobileCoinValue parseFrom(InputStream inputStream) throws IOException {
            return (MobileCoinValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static MobileCoinValue parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (MobileCoinValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static MobileCoinValue parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (MobileCoinValue) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static MobileCoinValue parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (MobileCoinValue) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static MobileCoinValue parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (MobileCoinValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static MobileCoinValue parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (MobileCoinValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(MobileCoinValue mobileCoinValue) {
            return DEFAULT_INSTANCE.createBuilder(mobileCoinValue);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinValue, Builder> implements MobileCoinValueOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(MobileCoinValue.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValue.MobileCoinValueOrBuilder
            public String getPicoMobileCoin() {
                return ((MobileCoinValue) this.instance).getPicoMobileCoin();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValue.MobileCoinValueOrBuilder
            public ByteString getPicoMobileCoinBytes() {
                return ((MobileCoinValue) this.instance).getPicoMobileCoinBytes();
            }

            public Builder setPicoMobileCoin(String str) {
                copyOnWrite();
                ((MobileCoinValue) this.instance).setPicoMobileCoin(str);
                return this;
            }

            public Builder clearPicoMobileCoin() {
                copyOnWrite();
                ((MobileCoinValue) this.instance).clearPicoMobileCoin();
                return this;
            }

            public Builder setPicoMobileCoinBytes(ByteString byteString) {
                copyOnWrite();
                ((MobileCoinValue) this.instance).setPicoMobileCoinBytes(byteString);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new MobileCoinValue();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001Ȉ", new Object[]{"picoMobileCoin_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<MobileCoinValue> parser = PARSER;
                    if (parser == null) {
                        synchronized (MobileCoinValue.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            MobileCoinValue mobileCoinValue = new MobileCoinValue();
            DEFAULT_INSTANCE = mobileCoinValue;
            GeneratedMessageLite.registerDefaultInstance(MobileCoinValue.class, mobileCoinValue);
        }

        public static MobileCoinValue getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<MobileCoinValue> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValue$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public enum ValueCase {
        MOBILECOINVALUE(1),
        VALUE_NOT_SET(0);
        
        private final int value;

        ValueCase(int i) {
            this.value = i;
        }

        @Deprecated
        public static ValueCase valueOf(int i) {
            return forNumber(i);
        }

        public static ValueCase forNumber(int i) {
            if (i == 0) {
                return VALUE_NOT_SET;
            }
            if (i != 1) {
                return null;
            }
            return MOBILECOINVALUE;
        }

        public int getNumber() {
            return this.value;
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValueOrBuilder
    public ValueCase getValueCase() {
        return ValueCase.forNumber(this.valueCase_);
    }

    public void clearValue() {
        this.valueCase_ = 0;
        this.value_ = null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValueOrBuilder
    public boolean hasMobileCoinValue() {
        return this.valueCase_ == 1;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValueOrBuilder
    public MobileCoinValue getMobileCoinValue() {
        if (this.valueCase_ == 1) {
            return (MobileCoinValue) this.value_;
        }
        return MobileCoinValue.getDefaultInstance();
    }

    public void setMobileCoinValue(MobileCoinValue mobileCoinValue) {
        mobileCoinValue.getClass();
        this.value_ = mobileCoinValue;
        this.valueCase_ = 1;
    }

    public void mergeMobileCoinValue(MobileCoinValue mobileCoinValue) {
        mobileCoinValue.getClass();
        if (this.valueCase_ != 1 || this.value_ == MobileCoinValue.getDefaultInstance()) {
            this.value_ = mobileCoinValue;
        } else {
            this.value_ = MobileCoinValue.newBuilder((MobileCoinValue) this.value_).mergeFrom((MobileCoinValue.Builder) mobileCoinValue).buildPartial();
        }
        this.valueCase_ = 1;
    }

    public void clearMobileCoinValue() {
        if (this.valueCase_ == 1) {
            this.valueCase_ = 0;
            this.value_ = null;
        }
    }

    public static CryptoValue parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (CryptoValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static CryptoValue parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (CryptoValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static CryptoValue parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (CryptoValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static CryptoValue parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (CryptoValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static CryptoValue parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (CryptoValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static CryptoValue parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (CryptoValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static CryptoValue parseFrom(InputStream inputStream) throws IOException {
        return (CryptoValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static CryptoValue parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (CryptoValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static CryptoValue parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (CryptoValue) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static CryptoValue parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (CryptoValue) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static CryptoValue parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (CryptoValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static CryptoValue parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (CryptoValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(CryptoValue cryptoValue) {
        return DEFAULT_INSTANCE.createBuilder(cryptoValue);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<CryptoValue, Builder> implements CryptoValueOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(CryptoValue.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValueOrBuilder
        public ValueCase getValueCase() {
            return ((CryptoValue) this.instance).getValueCase();
        }

        public Builder clearValue() {
            copyOnWrite();
            ((CryptoValue) this.instance).clearValue();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValueOrBuilder
        public boolean hasMobileCoinValue() {
            return ((CryptoValue) this.instance).hasMobileCoinValue();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValueOrBuilder
        public MobileCoinValue getMobileCoinValue() {
            return ((CryptoValue) this.instance).getMobileCoinValue();
        }

        public Builder setMobileCoinValue(MobileCoinValue mobileCoinValue) {
            copyOnWrite();
            ((CryptoValue) this.instance).setMobileCoinValue(mobileCoinValue);
            return this;
        }

        public Builder setMobileCoinValue(MobileCoinValue.Builder builder) {
            copyOnWrite();
            ((CryptoValue) this.instance).setMobileCoinValue(builder.build());
            return this;
        }

        public Builder mergeMobileCoinValue(MobileCoinValue mobileCoinValue) {
            copyOnWrite();
            ((CryptoValue) this.instance).mergeMobileCoinValue(mobileCoinValue);
            return this;
        }

        public Builder clearMobileCoinValue() {
            copyOnWrite();
            ((CryptoValue) this.instance).clearMobileCoinValue();
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new CryptoValue();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0001\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001<\u0000", new Object[]{"value_", "valueCase_", MobileCoinValue.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<CryptoValue> parser = PARSER;
                if (parser == null) {
                    synchronized (CryptoValue.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        CryptoValue cryptoValue = new CryptoValue();
        DEFAULT_INSTANCE = cryptoValue;
        GeneratedMessageLite.registerDefaultInstance(CryptoValue.class, cryptoValue);
    }

    public static CryptoValue getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<CryptoValue> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
