package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: DatabaseMonitor.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J/\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\t2\u000e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\fH\u0007¢\u0006\u0002\u0010\rJo\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\b\u001a\u00020\t2\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\f2\b\u0010\n\u001a\u0004\u0018\u00010\t2\u000e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\f2\b\u0010\u0012\u001a\u0004\u0018\u00010\t2\b\u0010\u0013\u001a\u0004\u0018\u00010\t2\b\u0010\u0014\u001a\u0004\u0018\u00010\t2\b\u0010\u0015\u001a\u0004\u0018\u00010\tH\u0007¢\u0006\u0002\u0010\u0016J%\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0018\u001a\u00020\t2\u000e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\fH\u0007¢\u0006\u0002\u0010\u0019J7\u0010\u001a\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\u001c2\b\u0010\n\u001a\u0004\u0018\u00010\t2\u000e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\fH\u0007¢\u0006\u0002\u0010\u001dR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/database/DatabaseMonitor;", "", "()V", "queryMonitor", "Lorg/thoughtcrime/securesms/database/QueryMonitor;", "initialize", "", "onDelete", "table", "", "selection", MultiselectForwardFragment.ARGS, "", "(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V", "onQuery", "distinct", "", "projection", "groupBy", "having", "orderBy", "limit", "(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "onSql", "sql", "(Ljava/lang/String;[Ljava/lang/Object;)V", "onUpdate", "values", "Landroid/content/ContentValues;", "(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DatabaseMonitor {
    public static final DatabaseMonitor INSTANCE = new DatabaseMonitor();
    private static QueryMonitor queryMonitor;

    private DatabaseMonitor() {
    }

    public final void initialize(QueryMonitor queryMonitor2) {
        queryMonitor = queryMonitor2;
    }

    @JvmStatic
    public static final void onSql(String str, Object[] objArr) {
        Intrinsics.checkNotNullParameter(str, "sql");
        QueryMonitor queryMonitor2 = queryMonitor;
        if (queryMonitor2 != null) {
            queryMonitor2.onSql(str, objArr);
        }
    }

    @JvmStatic
    public static final void onQuery(boolean z, String str, String[] strArr, String str2, Object[] objArr, String str3, String str4, String str5, String str6) {
        Intrinsics.checkNotNullParameter(str, "table");
        QueryMonitor queryMonitor2 = queryMonitor;
        if (queryMonitor2 != null) {
            queryMonitor2.onQuery(z, str, strArr, str2, objArr, str3, str4, str5, str6);
        }
    }

    @JvmStatic
    public static final void onDelete(String str, String str2, Object[] objArr) {
        Intrinsics.checkNotNullParameter(str, "table");
        QueryMonitor queryMonitor2 = queryMonitor;
        if (queryMonitor2 != null) {
            queryMonitor2.onDelete(str, str2, objArr);
        }
    }

    @JvmStatic
    public static final void onUpdate(String str, ContentValues contentValues, String str2, Object[] objArr) {
        Intrinsics.checkNotNullParameter(str, "table");
        Intrinsics.checkNotNullParameter(contentValues, "values");
        QueryMonitor queryMonitor2 = queryMonitor;
        if (queryMonitor2 != null) {
            queryMonitor2.onUpdate(str, contentValues, str2, objArr);
        }
    }
}
