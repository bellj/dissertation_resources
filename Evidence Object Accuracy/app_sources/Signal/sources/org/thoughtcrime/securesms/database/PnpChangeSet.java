package org.thoughtcrime.securesms.database;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* compiled from: PnpOperations.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpChangeSet;", "", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/database/PnpIdResolver;", "operations", "", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "(Lorg/thoughtcrime/securesms/database/PnpIdResolver;Ljava/util/List;)V", "getId", "()Lorg/thoughtcrime/securesms/database/PnpIdResolver;", "getOperations", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PnpChangeSet {
    private final PnpIdResolver id;
    private final List<PnpOperation> operations;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.database.PnpChangeSet */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ PnpChangeSet copy$default(PnpChangeSet pnpChangeSet, PnpIdResolver pnpIdResolver, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            pnpIdResolver = pnpChangeSet.id;
        }
        if ((i & 2) != 0) {
            list = pnpChangeSet.operations;
        }
        return pnpChangeSet.copy(pnpIdResolver, list);
    }

    public final PnpIdResolver component1() {
        return this.id;
    }

    public final List<PnpOperation> component2() {
        return this.operations;
    }

    public final PnpChangeSet copy(PnpIdResolver pnpIdResolver, List<? extends PnpOperation> list) {
        Intrinsics.checkNotNullParameter(pnpIdResolver, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(list, "operations");
        return new PnpChangeSet(pnpIdResolver, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PnpChangeSet)) {
            return false;
        }
        PnpChangeSet pnpChangeSet = (PnpChangeSet) obj;
        return Intrinsics.areEqual(this.id, pnpChangeSet.id) && Intrinsics.areEqual(this.operations, pnpChangeSet.operations);
    }

    public int hashCode() {
        return (this.id.hashCode() * 31) + this.operations.hashCode();
    }

    public String toString() {
        return "PnpChangeSet(id=" + this.id + ", operations=" + this.operations + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.database.PnpOperation> */
    /* JADX WARN: Multi-variable type inference failed */
    public PnpChangeSet(PnpIdResolver pnpIdResolver, List<? extends PnpOperation> list) {
        Intrinsics.checkNotNullParameter(pnpIdResolver, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(list, "operations");
        this.id = pnpIdResolver;
        this.operations = list;
    }

    public final PnpIdResolver getId() {
        return this.id;
    }

    public /* synthetic */ PnpChangeSet(PnpIdResolver pnpIdResolver, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(pnpIdResolver, (i & 2) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list);
    }

    public final List<PnpOperation> getOperations() {
        return this.operations;
    }
}
