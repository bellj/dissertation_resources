package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class DraftDatabase extends Database {
    public static final String[] CREATE_INDEXS = {"CREATE INDEX IF NOT EXISTS draft_thread_index ON drafts (thread_id);"};
    public static final String CREATE_TABLE;
    public static final String DRAFT_TYPE;
    public static final String DRAFT_VALUE;
    public static final String ID;
    static final String TABLE_NAME;
    private static final String TAG = Log.tag(DraftDatabase.class);
    public static final String THREAD_ID;

    public DraftDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public void replaceDrafts(long j, List<Draft> list) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        try {
            signalWritableDatabase.beginTransaction();
            int delete = signalWritableDatabase.delete(TABLE_NAME, "thread_id = ?", SqlUtil.buildArgs(j));
            String str = TAG;
            Log.d(str, "[replaceDrafts] Deleted " + delete + " rows for thread " + j);
            for (Draft draft : list) {
                ContentValues contentValues = new ContentValues(3);
                contentValues.put("thread_id", Long.valueOf(j));
                contentValues.put("type", draft.getType());
                contentValues.put(DRAFT_VALUE, draft.getValue());
                signalWritableDatabase.insert(TABLE_NAME, (String) null, contentValues);
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public void clearDrafts(long j) {
        int delete = this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, "thread_id = ?", SqlUtil.buildArgs(j));
        String str = TAG;
        Log.d(str, "[clearDrafts] Deleted " + delete + " rows for thread " + j);
    }

    public void clearDrafts(Set<Long> set) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        StringBuilder sb = new StringBuilder();
        LinkedList linkedList = new LinkedList();
        for (Long l : set) {
            long longValue = l.longValue();
            sb.append(" OR ");
            sb.append("thread_id");
            sb.append(" = ?");
            linkedList.add(String.valueOf(longValue));
        }
        signalWritableDatabase.delete(TABLE_NAME, sb.toString().substring(4), (String[]) linkedList.toArray(new String[0]));
    }

    public void clearAllDrafts() {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, (String) null, (String[]) null);
    }

    public Drafts getDrafts(long j) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        Drafts drafts = new Drafts();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, null, "thread_id = ?", new String[]{j + ""}, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                drafts.add(new Draft(query.getString(query.getColumnIndexOrThrow("type")), query.getString(query.getColumnIndexOrThrow(DRAFT_VALUE))));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return drafts;
    }

    public Drafts getAllVoiceNoteDrafts() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        Drafts drafts = new Drafts();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, null, "type = ?", SqlUtil.buildArgs(Draft.VOICE_NOTE), null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                drafts.add(new Draft(CursorUtil.requireString(query, "type"), CursorUtil.requireString(query, DRAFT_VALUE)));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return drafts;
    }

    /* loaded from: classes4.dex */
    public static class Draft {
        public static final String AUDIO;
        public static final String IMAGE;
        public static final String LOCATION;
        public static final String MENTION;
        public static final String QUOTE;
        public static final String TEXT;
        public static final String VIDEO;
        public static final String VOICE_NOTE;
        private final String type;
        private final String value;

        public Draft(String str, String str2) {
            this.type = str;
            this.value = str2;
        }

        public String getType() {
            return this.type;
        }

        public String getValue() {
            return this.value;
        }

        String getSnippet(Context context) {
            String str = this.type;
            str.hashCode();
            char c = 65535;
            switch (str.hashCode()) {
                case -1515030817:
                    if (str.equals(VOICE_NOTE)) {
                        c = 0;
                        break;
                    }
                    break;
                case 3556653:
                    if (str.equals(TEXT)) {
                        c = 1;
                        break;
                    }
                    break;
                case 93166550:
                    if (str.equals("audio")) {
                        c = 2;
                        break;
                    }
                    break;
                case 100313435:
                    if (str.equals(IMAGE)) {
                        c = 3;
                        break;
                    }
                    break;
                case 107953788:
                    if (str.equals(QUOTE)) {
                        c = 4;
                        break;
                    }
                    break;
                case 112202875:
                    if (str.equals("video")) {
                        c = 5;
                        break;
                    }
                    break;
                case 1901043637:
                    if (str.equals(LOCATION)) {
                        c = 6;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    return context.getString(R.string.DraftDatabase_Draft_voice_note);
                case 1:
                    return this.value;
                case 2:
                    return context.getString(R.string.DraftDatabase_Draft_audio_snippet);
                case 3:
                    return context.getString(R.string.DraftDatabase_Draft_image_snippet);
                case 4:
                    return context.getString(R.string.DraftDatabase_Draft_quote_snippet);
                case 5:
                    return context.getString(R.string.DraftDatabase_Draft_video_snippet);
                case 6:
                    return context.getString(R.string.DraftDatabase_Draft_location_snippet);
                default:
                    return null;
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class Drafts extends LinkedList<Draft> {
        public Draft getDraftOfType(String str) {
            Iterator<Draft> it = iterator();
            while (it.hasNext()) {
                Draft next = it.next();
                if (str.equals(next.getType())) {
                    return next;
                }
            }
            return null;
        }

        public String getSnippet(Context context) {
            Draft draftOfType = getDraftOfType(Draft.TEXT);
            if (draftOfType != null) {
                return draftOfType.getSnippet(context);
            }
            return size() > 0 ? get(0).getSnippet(context) : "";
        }

        public Uri getUriSnippet() {
            Draft draftOfType = getDraftOfType(Draft.IMAGE);
            if (draftOfType == null || draftOfType.getValue() == null) {
                return null;
            }
            return Uri.parse(draftOfType.getValue());
        }
    }
}
