package org.thoughtcrime.securesms.database;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessageRecipient;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* compiled from: SentStorySyncManifest.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b\b\u0018\u0000 \u001d2\u00020\u0001:\u0003\u001d\u001e\u001fB\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J \u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\u0011J\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00120\u000eJ*\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00032\u0006\u0010\u0016\u001a\u00020\u00042\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\u0011H\u0002J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001a0\u000eJ\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/database/SentStorySyncManifest;", "", "entries", "", "Lorg/thoughtcrime/securesms/database/SentStorySyncManifest$Entry;", "(Ljava/util/List;)V", "getEntries", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "flattenToRows", "", "Lorg/thoughtcrime/securesms/database/SentStorySyncManifest$Row;", "distributionIdToMessageIdMap", "", "Lorg/whispersystems/signalservice/api/push/DistributionId;", "", "getDistributionIdSet", "getRowsForEntry", "entry", "hashCode", "", "toRecipientsSet", "Lorg/whispersystems/signalservice/api/messages/SignalServiceStoryMessageRecipient;", "toString", "", "Companion", "Entry", "Row", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SentStorySyncManifest {
    public static final Companion Companion = new Companion(null);
    private final List<Entry> entries;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.database.SentStorySyncManifest */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SentStorySyncManifest copy$default(SentStorySyncManifest sentStorySyncManifest, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = sentStorySyncManifest.entries;
        }
        return sentStorySyncManifest.copy(list);
    }

    @JvmStatic
    public static final SentStorySyncManifest fromRecipientsSet(Set<? extends SignalServiceStoryMessageRecipient> set) {
        return Companion.fromRecipientsSet(set);
    }

    public final List<Entry> component1() {
        return this.entries;
    }

    public final SentStorySyncManifest copy(List<Entry> list) {
        Intrinsics.checkNotNullParameter(list, "entries");
        return new SentStorySyncManifest(list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof SentStorySyncManifest) && Intrinsics.areEqual(this.entries, ((SentStorySyncManifest) obj).entries);
    }

    public int hashCode() {
        return this.entries.hashCode();
    }

    public String toString() {
        return "SentStorySyncManifest(entries=" + this.entries + ')';
    }

    public SentStorySyncManifest(List<Entry> list) {
        Intrinsics.checkNotNullParameter(list, "entries");
        this.entries = list;
    }

    public final List<Entry> getEntries() {
        return this.entries;
    }

    /* compiled from: SentStorySyncManifest.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0005HÆ\u0003J\u000f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003J-\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00052\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/database/SentStorySyncManifest$Entry;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "allowedToReply", "", "distributionLists", "", "Lorg/whispersystems/signalservice/api/push/DistributionId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;ZLjava/util/List;)V", "getAllowedToReply", "()Z", "getDistributionLists", "()Ljava/util/List;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Entry {
        private final boolean allowedToReply;
        private final List<DistributionId> distributionLists;
        private final RecipientId recipientId;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.database.SentStorySyncManifest$Entry */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Entry copy$default(Entry entry, RecipientId recipientId, boolean z, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = entry.recipientId;
            }
            if ((i & 2) != 0) {
                z = entry.allowedToReply;
            }
            if ((i & 4) != 0) {
                list = entry.distributionLists;
            }
            return entry.copy(recipientId, z, list);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final boolean component2() {
            return this.allowedToReply;
        }

        public final List<DistributionId> component3() {
            return this.distributionLists;
        }

        public final Entry copy(RecipientId recipientId, boolean z, List<DistributionId> list) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(list, "distributionLists");
            return new Entry(recipientId, z, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Entry)) {
                return false;
            }
            Entry entry = (Entry) obj;
            return Intrinsics.areEqual(this.recipientId, entry.recipientId) && this.allowedToReply == entry.allowedToReply && Intrinsics.areEqual(this.distributionLists, entry.distributionLists);
        }

        public int hashCode() {
            int hashCode = this.recipientId.hashCode() * 31;
            boolean z = this.allowedToReply;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return ((hashCode + i) * 31) + this.distributionLists.hashCode();
        }

        public String toString() {
            return "Entry(recipientId=" + this.recipientId + ", allowedToReply=" + this.allowedToReply + ", distributionLists=" + this.distributionLists + ')';
        }

        public Entry(RecipientId recipientId, boolean z, List<DistributionId> list) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(list, "distributionLists");
            this.recipientId = recipientId;
            this.allowedToReply = z;
            this.distributionLists = list;
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final boolean getAllowedToReply() {
            return this.allowedToReply;
        }

        public /* synthetic */ Entry(RecipientId recipientId, boolean z, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(recipientId, (i & 2) != 0 ? false : z, (i & 4) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list);
        }

        public final List<DistributionId> getDistributionLists() {
            return this.distributionLists;
        }
    }

    /* compiled from: SentStorySyncManifest.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0016\u001a\u00020\tHÆ\u0003J1\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\tHÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00072\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/database/SentStorySyncManifest$Row;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "messageId", "", "allowsReplies", "", "distributionId", "Lorg/whispersystems/signalservice/api/push/DistributionId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;JZLorg/whispersystems/signalservice/api/push/DistributionId;)V", "getAllowsReplies", "()Z", "getDistributionId", "()Lorg/whispersystems/signalservice/api/push/DistributionId;", "getMessageId", "()J", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Row {
        private final boolean allowsReplies;
        private final DistributionId distributionId;
        private final long messageId;
        private final RecipientId recipientId;

        public static /* synthetic */ Row copy$default(Row row, RecipientId recipientId, long j, boolean z, DistributionId distributionId, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = row.recipientId;
            }
            if ((i & 2) != 0) {
                j = row.messageId;
            }
            if ((i & 4) != 0) {
                z = row.allowsReplies;
            }
            if ((i & 8) != 0) {
                distributionId = row.distributionId;
            }
            return row.copy(recipientId, j, z, distributionId);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final long component2() {
            return this.messageId;
        }

        public final boolean component3() {
            return this.allowsReplies;
        }

        public final DistributionId component4() {
            return this.distributionId;
        }

        public final Row copy(RecipientId recipientId, long j, boolean z, DistributionId distributionId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(distributionId, "distributionId");
            return new Row(recipientId, j, z, distributionId);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Row)) {
                return false;
            }
            Row row = (Row) obj;
            return Intrinsics.areEqual(this.recipientId, row.recipientId) && this.messageId == row.messageId && this.allowsReplies == row.allowsReplies && Intrinsics.areEqual(this.distributionId, row.distributionId);
        }

        public int hashCode() {
            int hashCode = ((this.recipientId.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.messageId)) * 31;
            boolean z = this.allowsReplies;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return ((hashCode + i) * 31) + this.distributionId.hashCode();
        }

        public String toString() {
            return "Row(recipientId=" + this.recipientId + ", messageId=" + this.messageId + ", allowsReplies=" + this.allowsReplies + ", distributionId=" + this.distributionId + ')';
        }

        public Row(RecipientId recipientId, long j, boolean z, DistributionId distributionId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(distributionId, "distributionId");
            this.recipientId = recipientId;
            this.messageId = j;
            this.allowsReplies = z;
            this.distributionId = distributionId;
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final long getMessageId() {
            return this.messageId;
        }

        public final boolean getAllowsReplies() {
            return this.allowsReplies;
        }

        public final DistributionId getDistributionId() {
            return this.distributionId;
        }
    }

    public final Set<DistributionId> getDistributionIdSet() {
        List<Entry> list = this.entries;
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (Entry entry : list) {
            arrayList.add(entry.getDistributionLists());
        }
        return CollectionsKt___CollectionsKt.toSet(CollectionsKt__IterablesKt.flatten(arrayList));
    }

    public final Set<SignalServiceStoryMessageRecipient> toRecipientsSet() {
        List<Entry> list = this.entries;
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (Entry entry : list) {
            arrayList.add(entry.getRecipientId());
        }
        List<Recipient> resolvedList = Recipient.resolvedList(arrayList);
        Intrinsics.checkNotNullExpressionValue(resolvedList, "resolvedList(entries.map { it.recipientId })");
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(resolvedList, 10));
        for (Recipient recipient : resolvedList) {
            ServiceId requireServiceId = recipient.requireServiceId();
            Intrinsics.checkNotNullExpressionValue(requireServiceId, "recipient.requireServiceId()");
            for (Entry entry2 : this.entries) {
                if (Intrinsics.areEqual(entry2.getRecipientId(), recipient.getId())) {
                    SignalServiceAddress signalServiceAddress = new SignalServiceAddress(requireServiceId);
                    List<DistributionId> distributionLists = entry2.getDistributionLists();
                    ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(distributionLists, 10));
                    for (DistributionId distributionId : distributionLists) {
                        arrayList3.add(distributionId.toString());
                    }
                    arrayList2.add(new SignalServiceStoryMessageRecipient(signalServiceAddress, arrayList3, entry2.getAllowedToReply()));
                }
            }
            throw new NoSuchElementException("Collection contains no element matching the predicate.");
        }
        return CollectionsKt___CollectionsKt.toSet(arrayList2);
    }

    public final Set<Row> flattenToRows(Map<DistributionId, Long> map) {
        Intrinsics.checkNotNullParameter(map, "distributionIdToMessageIdMap");
        List<Entry> list = this.entries;
        ArrayList arrayList = new ArrayList();
        for (Entry entry : list) {
            boolean unused = CollectionsKt__MutableCollectionsKt.addAll(arrayList, getRowsForEntry(entry, map));
        }
        return CollectionsKt___CollectionsKt.toSet(arrayList);
    }

    private final List<Row> getRowsForEntry(Entry entry, Map<DistributionId, Long> map) {
        List<DistributionId> distributionLists = entry.getDistributionLists();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(distributionLists, 10));
        Iterator<T> it = distributionLists.iterator();
        while (true) {
            long j = -1;
            if (!it.hasNext()) {
                break;
            }
            DistributionId distributionId = (DistributionId) it.next();
            RecipientId recipientId = entry.getRecipientId();
            boolean allowedToReply = entry.getAllowedToReply();
            Long l = map.get(distributionId);
            if (l != null) {
                j = l.longValue();
            }
            arrayList.add(new Row(recipientId, j, allowedToReply, distributionId));
        }
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (!(((Row) obj).getMessageId() == -1)) {
                arrayList2.add(obj);
            }
        }
        return arrayList2;
    }

    /* compiled from: SentStorySyncManifest.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/database/SentStorySyncManifest$Companion;", "", "()V", "fromRecipientsSet", "Lorg/thoughtcrime/securesms/database/SentStorySyncManifest;", "recipientsSet", "", "Lorg/whispersystems/signalservice/api/messages/SignalServiceStoryMessageRecipient;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final SentStorySyncManifest fromRecipientsSet(Set<? extends SignalServiceStoryMessageRecipient> set) {
            Intrinsics.checkNotNullParameter(set, "recipientsSet");
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
            for (SignalServiceStoryMessageRecipient signalServiceStoryMessageRecipient : set) {
                RecipientId from = RecipientId.from(signalServiceStoryMessageRecipient.getSignalServiceAddress());
                Intrinsics.checkNotNullExpressionValue(from, "from(recipient.signalServiceAddress)");
                boolean isAllowedToReply = signalServiceStoryMessageRecipient.isAllowedToReply();
                List<String> distributionListIds = signalServiceStoryMessageRecipient.getDistributionListIds();
                Intrinsics.checkNotNullExpressionValue(distributionListIds, "recipient.distributionListIds");
                ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(distributionListIds, 10));
                for (String str : distributionListIds) {
                    arrayList2.add(DistributionId.from(str));
                }
                arrayList.add(new Entry(from, isAllowedToReply, arrayList2));
            }
            return new SentStorySyncManifest(arrayList);
        }
    }
}
