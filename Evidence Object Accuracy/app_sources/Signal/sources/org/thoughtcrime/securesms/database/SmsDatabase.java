package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.google.android.mms.pdu_alt.NotificationInd;
import com.google.protobuf.ByteString;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Consumer;
import j$.util.function.Predicate;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import net.zetetic.database.sqlcipher.SQLiteStatement;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatchSet;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.model.GroupCallUpdateDetailsUtil;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.SmsMessageRecord;
import org.thoughtcrime.securesms.database.model.StoryResult;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails;
import org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupMigrationMembershipChange;
import org.thoughtcrime.securesms.jobs.TrimThreadJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.IncomingMediaMessage;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.revealable.ViewOnceExpirationInfo;
import org.thoughtcrime.securesms.sms.IncomingGroupUpdateMessage;
import org.thoughtcrime.securesms.sms.IncomingTextMessage;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* loaded from: classes4.dex */
public class SmsDatabase extends MessageDatabase {
    public static final String[] CREATE_INDEXS = {"CREATE INDEX IF NOT EXISTS sms_read_and_notified_and_thread_id_index ON sms(read,notified,thread_id);", "CREATE INDEX IF NOT EXISTS sms_type_index ON sms (type);", "CREATE INDEX IF NOT EXISTS sms_date_sent_index ON sms (date_sent, address, thread_id);", "CREATE INDEX IF NOT EXISTS sms_date_server_index ON sms (date_server);", "CREATE INDEX IF NOT EXISTS sms_thread_date_index ON sms (thread_id, date);", "CREATE INDEX IF NOT EXISTS sms_reactions_unread_index ON sms (reactions_unread);"};
    public static final String CREATE_TABLE;
    static final String DATE_RECEIVED;
    static final String DATE_SENT;
    static final long IGNORABLE_TYPESMASK_WHEN_COUNTING;
    private static final String[] MESSAGE_PROJECTION = {"_id", "thread_id", "address", MmsSmsColumns.ADDRESS_DEVICE_ID, PERSON, "date AS date_received", "date_sent AS date_sent", MmsSmsColumns.DATE_SERVER, PROTOCOL, "read", "status", "type", REPLY_PATH_PRESENT, SUBJECT, "body", SERVICE_CENTER, "delivery_receipt_count", MmsSmsColumns.MISMATCHED_IDENTITIES, MmsSmsColumns.SUBSCRIPTION_ID, "expires_in", MmsSmsColumns.EXPIRE_STARTED, MmsSmsColumns.NOTIFIED, "read_receipt_count", MmsSmsColumns.UNIDENTIFIED, MmsSmsColumns.REACTIONS_UNREAD, MmsSmsColumns.REACTIONS_LAST_SEEN, MmsSmsColumns.REMOTE_DELETED, MmsSmsColumns.NOTIFIED_TIMESTAMP, MmsSmsColumns.RECEIPT_TIMESTAMP};
    public static final String PERSON;
    public static final String PROTOCOL;
    public static final String REPLY_PATH_PRESENT;
    public static final String SERVICE_CENTER;
    public static final String STATUS;
    public static final String SUBJECT;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(SmsDatabase.class);
    public static final String TYPE;
    private static final EarlyReceiptCache earlyDeliveryReceiptCache = new EarlyReceiptCache("SmsDelivery");

    /* loaded from: classes4.dex */
    public static class Status {
        public static final int STATUS_COMPLETE;
        public static final int STATUS_FAILED;
        public static final int STATUS_NONE;
        public static final int STATUS_PENDING;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    protected String getDateReceivedColumnName() {
        return "date";
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    protected String getDateSentColumnName() {
        return "date_sent";
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    protected String getTableName() {
        return "sms";
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    protected String getTypeField() {
        return "type";
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.MarkedMessageInfo setIncomingMessageViewed(long j) {
        return null;
    }

    public SmsDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    /* JADX INFO: finally extract failed */
    private void updateTypeBitmask(long j, long j2, long j3) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            signalWritableDatabase.execSQL("UPDATE sms SET type = (type & " + (68719476735L - j2) + " | " + j3 + " ) WHERE _id = ?", SqlUtil.buildArgs(j));
            SignalDatabase.threads().updateSnippetTypeSilently(getThreadIdForMessage(j));
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(new MessageId(j, false));
            ApplicationDependencies.getDatabaseObserver().notifyConversationListListeners();
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public RecipientId getOldestGroupUpdateSender(long j, long j2) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("sms", new String[]{"address"}, "thread_id = ? AND type & ? AND date >= ?", new String[]{String.valueOf(j), String.valueOf(10551316L), String.valueOf(j2)}, null, null, SubscriptionLevels.BOOST_LEVEL);
        try {
            if (query.moveToFirst()) {
                RecipientId from = RecipientId.from(CursorUtil.requireLong(query, "address"));
                query.close();
                return from;
            }
            query.close();
            return null;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public long getThreadIdForMessage(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("sms", MessageDatabase.THREAD_ID_PROJECTION, "_id = ?", SqlUtil.buildArgs(j), null, null, null);
        try {
            if (query.moveToFirst()) {
                long requireLong = CursorUtil.requireLong(query, "thread_id");
                query.close();
                return requireLong;
            }
            query.close();
            return -1;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int getMessageCountForThread(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("sms", Database.COUNT, "thread_id = ?", SqlUtil.buildArgs(j), null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int getMessageCountForThread(long j, long j2) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("sms", new String[]{"COUNT(*)"}, "thread_id = ? AND date < ?", new String[]{String.valueOf(j), String.valueOf(j2)}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasMeaningfulMessage(long r11) {
        /*
            r10 = this;
            org.thoughtcrime.securesms.database.SignalDatabase r0 = r10.databaseHelper
            org.thoughtcrime.securesms.database.SQLiteDatabase r1 = r0.getSignalReadableDatabase()
            org.signal.core.util.SqlUtil$Query r11 = r10.buildMeaningfulMessagesQuery(r11)
            java.lang.String r12 = "1"
            java.lang.String[] r3 = new java.lang.String[]{r12}
            java.lang.String r4 = r11.getWhere()
            java.lang.String[] r5 = r11.getWhereArgs()
            java.lang.String r2 = "sms"
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "1"
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)
            if (r11 == 0) goto L_0x0037
            boolean r12 = r11.moveToFirst()     // Catch: all -> 0x002d
            if (r12 == 0) goto L_0x0037
            r12 = 1
            goto L_0x0038
        L_0x002d:
            r12 = move-exception
            r11.close()     // Catch: all -> 0x0032
            goto L_0x0036
        L_0x0032:
            r11 = move-exception
            r12.addSuppressed(r11)
        L_0x0036:
            throw r12
        L_0x0037:
            r12 = 0
        L_0x0038:
            if (r11 == 0) goto L_0x003d
            r11.close()
        L_0x003d:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.SmsDatabase.hasMeaningfulMessage(long):boolean");
    }

    private SqlUtil.Query buildMeaningfulMessagesQuery(long j) {
        return SqlUtil.buildQuery("thread_id = ? AND (NOT type & ? AND type != ? AND type != ? AND type != ? AND type & 720896 != 720896)", Long.valueOf(j), Long.valueOf((long) IGNORABLE_TYPESMASK_WHEN_COUNTING), 7L, 14L, 15L);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsEndSession(long j) {
        updateTypeBitmask(j, 65280, 4194304);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsInvalidVersionKeyExchange(long j) {
        updateTypeBitmask(j, 0, 2048);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsSecure(long j) {
        updateTypeBitmask(j, 0, 8388608);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsInsecure(long j) {
        updateTypeBitmask(j, 8388608, 0);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsPush(long j) {
        updateTypeBitmask(j, 0, 2097152);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsForcedSms(long j) {
        updateTypeBitmask(j, 2097152, 64);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsRateLimited(long j) {
        updateTypeBitmask(j, 0, 128);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void clearRateLimitStatus(Collection<Long> collection) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            for (Long l : collection) {
                updateTypeBitmask(l.longValue(), 128, 0);
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsDecryptFailed(long j) {
        updateTypeBitmask(j, MmsSmsColumns.Types.ENCRYPTION_MASK, 268435456);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsNoSession(long j) {
        updateTypeBitmask(j, MmsSmsColumns.Types.ENCRYPTION_MASK, 134217728);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsUnsupportedProtocolVersion(long j) {
        updateTypeBitmask(j, 31, 5);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsInvalidMessage(long j) {
        updateTypeBitmask(j, 31, 6);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsLegacyVersion(long j) {
        updateTypeBitmask(j, MmsSmsColumns.Types.ENCRYPTION_MASK, 33554432);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsOutbox(long j) {
        updateTypeBitmask(j, 31, 21);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsPendingInsecureSmsFallback(long j) {
        updateTypeBitmask(j, 31, 26);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsSent(long j, boolean z) {
        updateTypeBitmask(j, 31, (z ? 10485760 : 0) | 23);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsSending(long j) {
        updateTypeBitmask(j, 31, 22);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsMissedCall(long j, boolean z) {
        updateTypeBitmask(j, 68719476735L, z ? 8 : 3);
    }

    /* JADX INFO: finally extract failed */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsRemoteDelete(long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(MmsSmsColumns.REMOTE_DELETED, (Integer) 1);
            contentValues.putNull("body");
            signalWritableDatabase.update("sms", contentValues, "_id = ?", new String[]{String.valueOf(j)});
            long threadIdForMessage = getThreadIdForMessage(j);
            SignalDatabase.reactions().deleteReactions(new MessageId(j, false));
            SignalDatabase.threads().update(threadIdForMessage, false);
            SignalDatabase.messageLog().deleteAllRelatedToMessage(j, false);
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            notifyConversationListeners(threadIdForMessage);
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markUnidentified(long j, boolean z) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(MmsSmsColumns.UNIDENTIFIED, Integer.valueOf(z ? 1 : 0));
        this.databaseHelper.getSignalWritableDatabase().update("sms", contentValues, "_id = ?", new String[]{String.valueOf(j)});
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markExpireStarted(long j) {
        markExpireStarted(j, System.currentTimeMillis());
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markExpireStarted(long j, long j2) {
        markExpireStarted(Collections.singleton(Long.valueOf(j)), j2);
    }

    /* JADX INFO: finally extract failed */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markExpireStarted(Collection<Long> collection, long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            long j2 = -1;
            for (Long l : collection) {
                long longValue = l.longValue();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MmsSmsColumns.EXPIRE_STARTED, Long.valueOf(j));
                signalWritableDatabase.update("sms", contentValues, "_id = ? AND (expire_started = 0 OR expire_started > ?)", new String[]{String.valueOf(longValue), String.valueOf(j)});
                if (j2 < 0) {
                    j2 = getThreadIdForMessage(longValue);
                }
            }
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            SignalDatabase.threads().update(j2, false);
            notifyConversationListeners(j2);
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markSmsStatus(long j, int i) {
        String str = TAG;
        Log.i(str, "Updating ID: " + j + " to status: " + i);
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", Integer.valueOf(i));
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.update("sms", contentValues, "_id = ?", new String[]{j + ""});
        long threadIdForMessage = getThreadIdForMessage(j);
        SignalDatabase.threads().update(threadIdForMessage, false);
        notifyConversationListeners(threadIdForMessage);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsSentFailed(long j) {
        updateTypeBitmask(j, 31, 24);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsNotified(long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MmsSmsColumns.NOTIFIED, (Integer) 1);
        contentValues.put(MmsSmsColumns.REACTIONS_LAST_SEEN, Long.valueOf(System.currentTimeMillis()));
        signalWritableDatabase.update("sms", contentValues, "_id = ?", new String[]{String.valueOf(j)});
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Set<MessageDatabase.MessageUpdate> incrementReceiptCount(MessageDatabase.SyncMessageId syncMessageId, long j, MessageDatabase.ReceiptType receiptType, boolean z) {
        HashSet hashSet;
        HashSet hashSet2;
        if (receiptType == MessageDatabase.ReceiptType.VIEWED) {
            return Collections.emptySet();
        }
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        HashSet hashSet3 = new HashSet();
        Cursor query = signalWritableDatabase.query("sms", new String[]{"_id", "thread_id", "address", "type", "delivery_receipt_count", "read_receipt_count", MmsSmsColumns.RECEIPT_TIMESTAMP}, "date_sent = ?", new String[]{String.valueOf(syncMessageId.getTimetamp())}, null, null, null, null);
        while (query.moveToNext()) {
            try {
                if (MmsSmsColumns.Types.isOutgoingMessageType(CursorUtil.requireLong(query, "type"))) {
                    if (RecipientId.from(CursorUtil.requireLong(query, "address")).equals(syncMessageId.getRecipientId())) {
                        long requireLong = CursorUtil.requireLong(query, "_id");
                        long requireLong2 = CursorUtil.requireLong(query, "thread_id");
                        String columnName = receiptType.getColumnName();
                        boolean z2 = CursorUtil.requireLong(query, columnName) == 0;
                        long requireLong3 = CursorUtil.requireLong(query, MmsSmsColumns.RECEIPT_TIMESTAMP);
                        if (z2) {
                            hashSet2 = hashSet3;
                            requireLong3 = Math.max(requireLong3, j);
                        } else {
                            hashSet2 = hashSet3;
                        }
                        signalWritableDatabase.execSQL("UPDATE sms SET " + columnName + " = " + columnName + " + 1, " + MmsSmsColumns.RECEIPT_TIMESTAMP + " = ? WHERE _id = ?", SqlUtil.buildArgs(Long.valueOf(requireLong3), Long.valueOf(requireLong)));
                        hashSet = hashSet2;
                        hashSet.add(new MessageDatabase.MessageUpdate(requireLong2, new MessageId(requireLong, false)));
                    } else {
                        hashSet = hashSet3;
                    }
                    hashSet3 = hashSet;
                } else {
                    hashSet3 = hashSet3;
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        if (hashSet3.isEmpty() && receiptType == MessageDatabase.ReceiptType.DELIVERY) {
            earlyDeliveryReceiptCache.increment(syncMessageId.getTimetamp(), syncMessageId.getRecipientId(), j);
        }
        query.close();
        return hashSet3;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MmsSmsDatabase.TimestampReadResult setTimestampRead(MessageDatabase.SyncMessageId syncMessageId, long j, Map<Long, Long> map) {
        long j2;
        long j3;
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        LinkedList linkedList = new LinkedList();
        String[] strArr = {"_id", "thread_id", "address", "type", "expires_in", MmsSmsColumns.EXPIRE_STARTED};
        String[] buildArgs = SqlUtil.buildArgs(syncMessageId.getTimetamp());
        LinkedList linkedList2 = new LinkedList();
        Cursor query = signalWritableDatabase.query("sms", strArr, "date_sent = ?", buildArgs, null, null, null);
        while (query.moveToNext()) {
            try {
                RecipientId recipientId = syncMessageId.getRecipientId();
                if (RecipientId.from(query.getLong(query.getColumnIndexOrThrow("address"))).equals(recipientId) || recipientId.equals(Recipient.self().getId())) {
                    long j4 = query.getLong(query.getColumnIndexOrThrow("_id"));
                    long j5 = query.getLong(query.getColumnIndexOrThrow("thread_id"));
                    long j6 = query.getLong(query.getColumnIndexOrThrow("expires_in"));
                    long j7 = query.getLong(query.getColumnIndexOrThrow(MmsSmsColumns.EXPIRE_STARTED));
                    if (j7 > 0) {
                        j2 = j5;
                        j3 = Math.min(j, j7);
                    } else {
                        j2 = j5;
                        j3 = j;
                    }
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("read", (Integer) 1);
                    contentValues.put(MmsSmsColumns.REACTIONS_UNREAD, (Integer) 0);
                    contentValues.put(MmsSmsColumns.REACTIONS_LAST_SEEN, Long.valueOf(System.currentTimeMillis()));
                    if (j6 > 0) {
                        contentValues.put(MmsSmsColumns.EXPIRE_STARTED, Long.valueOf(j3));
                        linkedList.add(new Pair(Long.valueOf(j4), Long.valueOf(j6)));
                    }
                    signalWritableDatabase.update("sms", contentValues, "_id = ?", SqlUtil.buildArgs(j4));
                    linkedList2.add(Long.valueOf(j2));
                    Long l = map.get(Long.valueOf(j2));
                    map.put(Long.valueOf(j2), Long.valueOf(l != null ? Math.max(l.longValue(), syncMessageId.getTimetamp()) : syncMessageId.getTimetamp()));
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return new MmsSmsDatabase.TimestampReadResult(linkedList, linkedList2);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setEntireThreadRead(long j) {
        return setMessagesRead("thread_id = ?", new String[]{String.valueOf(j)});
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setMessagesReadSince(long j, long j2) {
        if (j2 == -1) {
            return setMessagesRead("thread_id = ? AND (read = 0 OR (reactions_unread = 1 AND (" + getOutgoingTypeClause() + ")))", new String[]{String.valueOf(j)});
        }
        return setMessagesRead("thread_id = ? AND (read = 0 OR (reactions_unread = 1 AND ( " + getOutgoingTypeClause() + " ))) AND date <= ?", new String[]{String.valueOf(j), String.valueOf(j2)});
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setAllMessagesRead() {
        return setMessagesRead("read = 0 OR (reactions_unread = 1 AND (" + getOutgoingTypeClause() + "))", null);
    }

    private List<MessageDatabase.MarkedMessageInfo> setMessagesRead(String str, String[] strArr) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        LinkedList linkedList = new LinkedList();
        RecipientId releaseChannelRecipientId = SignalStore.releaseChannelValues().getReleaseChannelRecipientId();
        signalWritableDatabase.beginTransaction();
        Cursor cursor = null;
        try {
            cursor = signalWritableDatabase.query("sms", new String[]{"_id", "address", "date_sent", "type", "expires_in", MmsSmsColumns.EXPIRE_STARTED, "thread_id"}, str, strArr, null, null, null);
            while (cursor != null && cursor.moveToNext()) {
                if (MmsSmsColumns.Types.isSecureType(CursorUtil.requireLong(cursor, "type"))) {
                    long requireLong = CursorUtil.requireLong(cursor, "thread_id");
                    RecipientId from = RecipientId.from(CursorUtil.requireLong(cursor, "address"));
                    long requireLong2 = CursorUtil.requireLong(cursor, "date_sent");
                    long requireLong3 = CursorUtil.requireLong(cursor, "_id");
                    long requireLong4 = CursorUtil.requireLong(cursor, "expires_in");
                    long requireLong5 = CursorUtil.requireLong(cursor, MmsSmsColumns.EXPIRE_STARTED);
                    MessageDatabase.SyncMessageId syncMessageId = new MessageDatabase.SyncMessageId(from, requireLong2);
                    MessageDatabase.ExpirationInfo expirationInfo = new MessageDatabase.ExpirationInfo(requireLong3, requireLong4, requireLong5, false);
                    if (!from.equals(releaseChannelRecipientId)) {
                        linkedList.add(new MessageDatabase.MarkedMessageInfo(requireLong, syncMessageId, new MessageId(requireLong3, false), expirationInfo));
                    }
                }
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("read", (Integer) 1);
            contentValues.put(MmsSmsColumns.REACTIONS_UNREAD, (Integer) 0);
            contentValues.put(MmsSmsColumns.REACTIONS_LAST_SEEN, Long.valueOf(System.currentTimeMillis()));
            signalWritableDatabase.update("sms", contentValues, str, strArr);
            signalWritableDatabase.setTransactionSuccessful();
            return linkedList;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            signalWritableDatabase.endTransaction();
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.InsertResult updateBundleMessageBody(long j, String str) {
        return updateMessageBodyAndType(j, str, 68719476735L, 10485780);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> getViewedIncomingMessages(long j) {
        return Collections.emptyList();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setIncomingMessagesViewed(List<Long> list) {
        return Collections.emptyList();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setOutgoingGiftsRevealed(List<Long> list) {
        throw new UnsupportedOperationException();
    }

    private MessageDatabase.InsertResult updateMessageBodyAndType(long j, String str, long j2, long j3) {
        this.databaseHelper.getSignalWritableDatabase().execSQL("UPDATE sms SET body = ?, type = (type & " + (68719476735L - j2) + " | " + j3 + ") WHERE _id = ?", new String[]{str, j + ""});
        long threadIdForMessage = getThreadIdForMessage(j);
        SignalDatabase.threads().update(threadIdForMessage, true);
        notifyConversationListeners(threadIdForMessage);
        return new MessageDatabase.InsertResult(j, threadIdForMessage);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x005a, code lost:
        if (r11.moveToFirst() != false) goto L_0x0068;
     */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasReceivedAnyCallsSince(long r11, long r13) {
        /*
            r10 = this;
            org.thoughtcrime.securesms.database.SignalDatabase r0 = r10.databaseHelper
            org.thoughtcrime.securesms.database.SQLiteDatabase r1 = r0.getSignalReadableDatabase()
            r0 = 1
            java.lang.Object[] r2 = new java.lang.Object[r0]
            java.lang.String r3 = "type"
            r9 = 0
            r2[r9] = r3
            java.lang.String[] r3 = org.signal.core.util.SqlUtil.buildArgs(r2)
            r2 = 6
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.Long r11 = java.lang.Long.valueOf(r11)
            r2[r9] = r11
            java.lang.Long r11 = java.lang.Long.valueOf(r13)
            r2[r0] = r11
            r11 = 1
            java.lang.Long r11 = java.lang.Long.valueOf(r11)
            r12 = 2
            r2[r12] = r11
            r11 = 10
            java.lang.Long r11 = java.lang.Long.valueOf(r11)
            r12 = 3
            r2[r12] = r11
            r11 = 3
            java.lang.Long r11 = java.lang.Long.valueOf(r11)
            r12 = 4
            r2[r12] = r11
            r11 = 8
            java.lang.Long r11 = java.lang.Long.valueOf(r11)
            r12 = 5
            r2[r12] = r11
            java.lang.String[] r5 = org.signal.core.util.SqlUtil.buildArgs(r2)
            java.lang.String r2 = "sms"
            java.lang.String r4 = "thread_id = ? AND date > ? AND (type = ? OR type = ? OR type = ? OR type =?)"
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8)
            if (r11 == 0) goto L_0x0067
            boolean r12 = r11.moveToFirst()     // Catch: all -> 0x005d
            if (r12 == 0) goto L_0x0067
            goto L_0x0068
        L_0x005d:
            r12 = move-exception
            r11.close()     // Catch: all -> 0x0062
            goto L_0x0066
        L_0x0062:
            r11 = move-exception
            r12.addSuppressed(r11)
        L_0x0066:
            throw r12
        L_0x0067:
            r0 = 0
        L_0x0068:
            if (r11 == 0) goto L_0x006d
            r11.close()
        L_0x006d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.SmsDatabase.hasReceivedAnyCallsSince(long, long):boolean");
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Pair<Long, Long> insertReceivedCall(RecipientId recipientId, boolean z) {
        return insertCallLog(recipientId, z ? 10 : 1, false, System.currentTimeMillis());
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Pair<Long, Long> insertOutgoingCall(RecipientId recipientId, boolean z) {
        return insertCallLog(recipientId, z ? 11 : 2, false, System.currentTimeMillis());
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Pair<Long, Long> insertMissedCall(RecipientId recipientId, long j, boolean z) {
        return insertCallLog(recipientId, z ? 8 : 3, true, j);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00c8  */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void insertOrUpdateGroupCall(org.thoughtcrime.securesms.recipients.RecipientId r14, org.thoughtcrime.securesms.recipients.RecipientId r15, long r16, java.lang.String r18, java.util.Collection<java.util.UUID> r19, boolean r20) {
        /*
            r13 = this;
            r7 = r13
            org.thoughtcrime.securesms.database.SignalDatabase r0 = r7.databaseHelper
            org.thoughtcrime.securesms.database.SQLiteDatabase r8 = r0.getSignalWritableDatabase()
            org.thoughtcrime.securesms.recipients.Recipient r0 = org.thoughtcrime.securesms.recipients.Recipient.resolved(r14)
            org.thoughtcrime.securesms.database.ThreadDatabase r1 = org.thoughtcrime.securesms.database.SignalDatabase.threads()
            long r9 = r1.getOrCreateThreadIdFor(r0)
            r1 = r13
            r2 = r9
            r4 = r18
            r5 = r19
            r6 = r20
            boolean r0 = r1.updatePreviousGroupCall(r2, r4, r5, r6)
            r8.beginTransaction()     // Catch: all -> 0x010a
            r1 = 1
            if (r0 != 0) goto L_0x00f6
            boolean r0 = org.thoughtcrime.securesms.util.Util.isEmpty(r18)     // Catch: all -> 0x010a
            if (r0 != 0) goto L_0x00f6
            org.thoughtcrime.securesms.recipients.Recipient r0 = org.thoughtcrime.securesms.recipients.Recipient.self()     // Catch: all -> 0x010a
            org.whispersystems.signalservice.api.push.ServiceId r2 = r0.requireServiceId()     // Catch: all -> 0x010a
            java.util.UUID r2 = r2.uuid()     // Catch: all -> 0x010a
            r3 = r19
            boolean r2 = r3.contains(r2)     // Catch: all -> 0x010a
            r4 = 0
            if (r2 != 0) goto L_0x004e
            org.thoughtcrime.securesms.recipients.RecipientId r0 = r0.getId()     // Catch: all -> 0x010a
            r2 = r15
            boolean r0 = r0.equals(r15)     // Catch: all -> 0x010a
            if (r0 == 0) goto L_0x004c
            goto L_0x004f
        L_0x004c:
            r0 = 0
            goto L_0x0050
        L_0x004e:
            r2 = r15
        L_0x004f:
            r0 = 1
        L_0x0050:
            org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails$Builder r5 = org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails.newBuilder()     // Catch: all -> 0x010a
            java.lang.String r6 = org.thoughtcrime.securesms.util.Util.emptyIfNull(r18)     // Catch: all -> 0x010a
            org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails$Builder r5 = r5.setEraId(r6)     // Catch: all -> 0x010a
            org.thoughtcrime.securesms.recipients.Recipient r6 = org.thoughtcrime.securesms.recipients.Recipient.resolved(r15)     // Catch: all -> 0x010a
            org.whispersystems.signalservice.api.push.ServiceId r6 = r6.requireServiceId()     // Catch: all -> 0x010a
            java.lang.String r6 = r6.toString()     // Catch: all -> 0x010a
            org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails$Builder r5 = r5.setStartedCallUuid(r6)     // Catch: all -> 0x010a
            r11 = r16
            org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails$Builder r5 = r5.setStartedCallTimestamp(r11)     // Catch: all -> 0x010a
            com.annimon.stream.Stream r3 = com.annimon.stream.Stream.of(r19)     // Catch: all -> 0x010a
            org.thoughtcrime.securesms.database.SmsDatabase$$ExternalSyntheticLambda4 r6 = new org.thoughtcrime.securesms.database.SmsDatabase$$ExternalSyntheticLambda4     // Catch: all -> 0x010a
            r6.<init>()     // Catch: all -> 0x010a
            com.annimon.stream.Stream r3 = r3.map(r6)     // Catch: all -> 0x010a
            java.util.List r3 = r3.toList()     // Catch: all -> 0x010a
            org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails$Builder r3 = r5.addAllInCallUuids(r3)     // Catch: all -> 0x010a
            r5 = r20
            org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails$Builder r3 = r3.setIsCallFull(r5)     // Catch: all -> 0x010a
            com.google.protobuf.GeneratedMessageLite r3 = r3.build()     // Catch: all -> 0x010a
            org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails r3 = (org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails) r3     // Catch: all -> 0x010a
            byte[] r3 = r3.toByteArray()     // Catch: all -> 0x010a
            java.lang.String r3 = org.thoughtcrime.securesms.util.Base64.encodeBytes(r3)     // Catch: all -> 0x010a
            android.content.ContentValues r5 = new android.content.ContentValues     // Catch: all -> 0x010a
            r5.<init>()     // Catch: all -> 0x010a
            java.lang.String r6 = "address"
            java.lang.String r2 = r15.serialize()     // Catch: all -> 0x010a
            r5.put(r6, r2)     // Catch: all -> 0x010a
            java.lang.String r2 = "address_device_id"
            java.lang.Integer r6 = java.lang.Integer.valueOf(r1)     // Catch: all -> 0x010a
            r5.put(r2, r6)     // Catch: all -> 0x010a
            java.lang.String r2 = "date"
            java.lang.Long r6 = java.lang.Long.valueOf(r16)     // Catch: all -> 0x010a
            r5.put(r2, r6)     // Catch: all -> 0x010a
            java.lang.String r2 = "date_sent"
            java.lang.Long r6 = java.lang.Long.valueOf(r16)     // Catch: all -> 0x010a
            r5.put(r2, r6)     // Catch: all -> 0x010a
            java.lang.String r2 = "read"
            if (r0 == 0) goto L_0x00c9
            r4 = 1
        L_0x00c9:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch: all -> 0x010a
            r5.put(r2, r0)     // Catch: all -> 0x010a
            java.lang.String r0 = "body"
            r5.put(r0, r3)     // Catch: all -> 0x010a
            java.lang.String r0 = "type"
            r2 = 12
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch: all -> 0x010a
            r5.put(r0, r2)     // Catch: all -> 0x010a
            java.lang.String r0 = "thread_id"
            java.lang.Long r2 = java.lang.Long.valueOf(r9)     // Catch: all -> 0x010a
            r5.put(r0, r2)     // Catch: all -> 0x010a
            java.lang.String r0 = "sms"
            r2 = 0
            r8.insert(r0, r2, r5)     // Catch: all -> 0x010a
            org.thoughtcrime.securesms.database.ThreadDatabase r0 = org.thoughtcrime.securesms.database.SignalDatabase.threads()     // Catch: all -> 0x010a
            r0.incrementUnread(r9, r1)     // Catch: all -> 0x010a
        L_0x00f6:
            org.thoughtcrime.securesms.database.ThreadDatabase r0 = org.thoughtcrime.securesms.database.SignalDatabase.threads()     // Catch: all -> 0x010a
            r0.update(r9, r1)     // Catch: all -> 0x010a
            r8.setTransactionSuccessful()     // Catch: all -> 0x010a
            r8.endTransaction()
            r13.notifyConversationListeners(r9)
            org.thoughtcrime.securesms.jobs.TrimThreadJob.enqueueAsync(r9)
            return
        L_0x010a:
            r0 = move-exception
            r8.endTransaction()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.SmsDatabase.insertOrUpdateGroupCall(org.thoughtcrime.securesms.recipients.RecipientId, org.thoughtcrime.securesms.recipients.RecipientId, long, java.lang.String, java.util.Collection, boolean):void");
    }

    /* JADX INFO: finally extract failed */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertOrUpdateGroupCall(RecipientId recipientId, RecipientId recipientId2, long j, String str) {
        boolean z;
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        try {
            signalWritableDatabase.beginTransaction();
            long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(Recipient.resolved(recipientId));
            Reader reader = new Reader(signalWritableDatabase.query("sms", MESSAGE_PROJECTION, "type = ? AND thread_id = ?", SqlUtil.buildArgs(12L, Long.valueOf(orCreateThreadIdFor)), null, null, "date DESC", SubscriptionLevels.BOOST_LEVEL));
            SmsMessageRecord next = reader.getNext();
            if (next != null) {
                GroupCallUpdateDetails parse = GroupCallUpdateDetailsUtil.parse(next.getBody());
                z = parse.getEraId().equals(str) && !Util.isEmpty(str);
                if (!z) {
                    String createUpdatedBody = GroupCallUpdateDetailsUtil.createUpdatedBody(parse, Collections.emptyList(), false);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("body", createUpdatedBody);
                    signalWritableDatabase.update("sms", contentValues, "_id = ?", SqlUtil.buildArgs(next.getId()));
                }
            } else {
                z = false;
            }
            reader.close();
            if (!z && !Util.isEmpty(str)) {
                String encodeBytes = Base64.encodeBytes(GroupCallUpdateDetails.newBuilder().setEraId(Util.emptyIfNull(str)).setStartedCallUuid(Recipient.resolved(recipientId2).requireServiceId().toString()).setStartedCallTimestamp(j).addAllInCallUuids(Collections.emptyList()).setIsCallFull(false).build().toByteArray());
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("address", recipientId2.serialize());
                contentValues2.put(MmsSmsColumns.ADDRESS_DEVICE_ID, (Integer) 1);
                contentValues2.put("date", Long.valueOf(j));
                contentValues2.put("date_sent", Long.valueOf(j));
                contentValues2.put("read", (Integer) 0);
                contentValues2.put("body", encodeBytes);
                contentValues2.put("type", (Long) 12L);
                contentValues2.put("thread_id", Long.valueOf(orCreateThreadIdFor));
                signalWritableDatabase.insert("sms", (String) null, contentValues2);
                SignalDatabase.threads().incrementUnread(orCreateThreadIdFor, 1);
            }
            SignalDatabase.threads().update(orCreateThreadIdFor, true);
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            notifyConversationListeners(orCreateThreadIdFor);
            TrimThreadJob.enqueueAsync(orCreateThreadIdFor);
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean updatePreviousGroupCall(long j, String str, Collection<UUID> collection, boolean z) {
        List list;
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        boolean z2 = false;
        Reader reader = new Reader(signalWritableDatabase.query("sms", MESSAGE_PROJECTION, "type = ? AND thread_id = ?", SqlUtil.buildArgs(12L, Long.valueOf(j)), null, null, "date DESC", SubscriptionLevels.BOOST_LEVEL));
        try {
            SmsMessageRecord next = reader.getNext();
            if (next == null) {
                reader.close();
                return false;
            }
            GroupCallUpdateDetails parse = GroupCallUpdateDetailsUtil.parse(next.getBody());
            boolean contains = collection.contains(SignalStore.account().requireAci().uuid());
            boolean z3 = parse.getEraId().equals(str) && !Util.isEmpty(str);
            if (z3) {
                list = Stream.of(collection).map(new SmsDatabase$$ExternalSyntheticLambda4()).toList();
            } else {
                list = Collections.emptyList();
            }
            String createUpdatedBody = GroupCallUpdateDetailsUtil.createUpdatedBody(parse, list, z);
            ContentValues contentValues = new ContentValues();
            contentValues.put("body", createUpdatedBody);
            if (z3 && contains) {
                contentValues.put("read", (Integer) 1);
            }
            SqlUtil.Query buildTrueUpdateQuery = SqlUtil.buildTrueUpdateQuery("_id = ?", SqlUtil.buildArgs(next.getId()), contentValues);
            if (signalWritableDatabase.update("sms", contentValues, buildTrueUpdateQuery.getWhere(), buildTrueUpdateQuery.getWhereArgs()) > 0) {
                z2 = true;
            }
            if (z2) {
                notifyConversationListeners(j);
            }
            reader.close();
            return z3;
        } catch (Throwable th) {
            try {
                reader.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    private Pair<Long, Long> insertCallLog(RecipientId recipientId, long j, boolean z, long j2) {
        long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(Recipient.resolved(recipientId));
        ContentValues contentValues = new ContentValues(6);
        contentValues.put("address", recipientId.serialize());
        contentValues.put(MmsSmsColumns.ADDRESS_DEVICE_ID, (Integer) 1);
        contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("date_sent", Long.valueOf(j2));
        contentValues.put("read", Integer.valueOf(!z ? 1 : 0));
        contentValues.put("type", Long.valueOf(j));
        contentValues.put("thread_id", Long.valueOf(orCreateThreadIdFor));
        long insert = this.databaseHelper.getSignalWritableDatabase().insert("sms", (String) null, contentValues);
        if (z) {
            SignalDatabase.threads().incrementUnread(orCreateThreadIdFor, 1);
        }
        SignalDatabase.threads().update(orCreateThreadIdFor, true);
        notifyConversationListeners(orCreateThreadIdFor);
        TrimThreadJob.enqueueAsync(orCreateThreadIdFor);
        return new Pair<>(Long.valueOf(insert), Long.valueOf(orCreateThreadIdFor));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Set<Long> getAllRateLimitedMessageIds() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        HashSet hashSet = new HashSet();
        Cursor query = signalReadableDatabase.query("sms", new String[]{"_id"}, "(type & 68719476735 & 128) > 0", null, null, null, null);
        while (query.moveToNext()) {
            try {
                hashSet.add(Long.valueOf(CursorUtil.requireLong(query, "_id")));
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return hashSet;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageRecord> getProfileChangeDetailsRecords(long j, long j2) {
        Reader readerFor = readerFor(queryMessages("thread_id = ? AND date >= ? AND type = ?", SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(j2), 7L), true, -1));
        try {
            ArrayList arrayList = new ArrayList(readerFor.getCount());
            while (readerFor.getNext() != null) {
                arrayList.add(readerFor.getCurrent());
            }
            readerFor.close();
            return arrayList;
        } catch (Throwable th) {
            if (readerFor != null) {
                try {
                    readerFor.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertProfileNameChangeMessages(Recipient recipient, String str, String str2) {
        ThreadDatabase threads = SignalDatabase.threads();
        List<GroupDatabase.GroupRecord> groupsContainingMember = SignalDatabase.groups().getGroupsContainingMember(recipient.getId(), false);
        LinkedList linkedList = new LinkedList();
        String encodeBytes = Base64.encodeBytes(ProfileChangeDetails.newBuilder().setProfileNameChange(ProfileChangeDetails.StringChange.newBuilder().setNew(str).setPrevious(str2)).build().toByteArray());
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            linkedList.add(threads.getThreadIdFor(recipient.getId()));
            for (GroupDatabase.GroupRecord groupRecord : groupsContainingMember) {
                if (groupRecord.isActive()) {
                    linkedList.add(threads.getThreadIdFor(groupRecord.getRecipientId()));
                }
            }
            Stream.of(linkedList).withoutNulls().forEach(new Consumer(recipient, encodeBytes, signalWritableDatabase) { // from class: org.thoughtcrime.securesms.database.SmsDatabase$$ExternalSyntheticLambda5
                public final /* synthetic */ Recipient f$1;
                public final /* synthetic */ String f$2;
                public final /* synthetic */ SQLiteDatabase f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    SmsDatabase.this.lambda$insertProfileNameChangeMessages$0(this.f$1, this.f$2, this.f$3, (Long) obj);
                }
            });
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            Stream.of(linkedList).withoutNulls().forEach(new Consumer() { // from class: org.thoughtcrime.securesms.database.SmsDatabase$$ExternalSyntheticLambda6
                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    TrimThreadJob.enqueueAsync(((Long) obj).longValue());
                }
            });
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    public /* synthetic */ void lambda$insertProfileNameChangeMessages$0(Recipient recipient, String str, SQLiteDatabase sQLiteDatabase, Long l) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("address", recipient.getId().serialize());
        contentValues.put(MmsSmsColumns.ADDRESS_DEVICE_ID, (Integer) 1);
        contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("date_sent", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("read", (Integer) 1);
        contentValues.put("type", (Long) 7L);
        contentValues.put("thread_id", l);
        contentValues.put("body", str);
        sQLiteDatabase.insert("sms", (String) null, contentValues);
        notifyConversationListeners(l.longValue());
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertGroupV1MigrationEvents(RecipientId recipientId, long j, GroupMigrationMembershipChange groupMigrationMembershipChange) {
        insertGroupV1MigrationNotification(recipientId, j);
        if (!groupMigrationMembershipChange.isEmpty()) {
            insertGroupV1MigrationMembershipChanges(recipientId, j, groupMigrationMembershipChange);
        }
        notifyConversationListeners(j);
        TrimThreadJob.enqueueAsync(j);
    }

    private void insertGroupV1MigrationNotification(RecipientId recipientId, long j) {
        insertGroupV1MigrationMembershipChanges(recipientId, j, GroupMigrationMembershipChange.empty());
    }

    private void insertGroupV1MigrationMembershipChanges(RecipientId recipientId, long j, GroupMigrationMembershipChange groupMigrationMembershipChange) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("address", recipientId.serialize());
        contentValues.put(MmsSmsColumns.ADDRESS_DEVICE_ID, (Integer) 1);
        contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("date_sent", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("read", (Integer) 1);
        contentValues.put("type", (Long) 9L);
        contentValues.put("thread_id", Long.valueOf(j));
        if (!groupMigrationMembershipChange.isEmpty()) {
            contentValues.put("body", groupMigrationMembershipChange.serialize());
        }
        this.databaseHelper.getSignalWritableDatabase().insert("sms", (String) null, contentValues);
    }

    /* JADX INFO: finally extract failed */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertNumberChangeMessages(Recipient recipient) {
        ThreadDatabase threads = SignalDatabase.threads();
        List<GroupDatabase.GroupRecord> groupsContainingMember = SignalDatabase.groups().getGroupsContainingMember(recipient.getId(), false);
        LinkedList linkedList = new LinkedList();
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            linkedList.add(threads.getThreadIdFor(recipient.getId()));
            for (GroupDatabase.GroupRecord groupRecord : groupsContainingMember) {
                if (groupRecord.isActive()) {
                    linkedList.add(threads.getThreadIdFor(groupRecord.getRecipientId()));
                }
            }
            Collection$EL.stream(linkedList).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.SmsDatabase$$ExternalSyntheticLambda0
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m((Long) obj);
                }
            }).forEach(new j$.util.function.Consumer(signalWritableDatabase) { // from class: org.thoughtcrime.securesms.database.SmsDatabase$$ExternalSyntheticLambda1
                public final /* synthetic */ SQLiteDatabase f$1;

                {
                    this.f$1 = r2;
                }

                @Override // j$.util.function.Consumer
                public final void accept(Object obj) {
                    SmsDatabase.lambda$insertNumberChangeMessages$1(Recipient.this, this.f$1, (Long) obj);
                }

                @Override // j$.util.function.Consumer
                public /* synthetic */ j$.util.function.Consumer andThen(j$.util.function.Consumer consumer) {
                    return Consumer.CC.$default$andThen(this, consumer);
                }
            });
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            Collection$EL.stream(linkedList).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.SmsDatabase$$ExternalSyntheticLambda2
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m((Long) obj);
                }
            }).forEach(new j$.util.function.Consumer() { // from class: org.thoughtcrime.securesms.database.SmsDatabase$$ExternalSyntheticLambda3
                @Override // j$.util.function.Consumer
                public final void accept(Object obj) {
                    SmsDatabase.this.lambda$insertNumberChangeMessages$2((Long) obj);
                }

                @Override // j$.util.function.Consumer
                public /* synthetic */ j$.util.function.Consumer andThen(j$.util.function.Consumer consumer) {
                    return Consumer.CC.$default$andThen(this, consumer);
                }
            });
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    public static /* synthetic */ void lambda$insertNumberChangeMessages$1(Recipient recipient, SQLiteDatabase sQLiteDatabase, Long l) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("address", recipient.getId().serialize());
        contentValues.put(MmsSmsColumns.ADDRESS_DEVICE_ID, (Integer) 1);
        contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("date_sent", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("read", (Integer) 1);
        contentValues.put("type", (Long) 14L);
        contentValues.put("thread_id", l);
        contentValues.putNull("body");
        sQLiteDatabase.insert("sms", (String) null, contentValues);
    }

    public /* synthetic */ void lambda$insertNumberChangeMessages$2(Long l) {
        TrimThreadJob.enqueueAsync(l.longValue());
        SignalDatabase.threads().update(l.longValue(), true);
        notifyConversationListeners(l.longValue());
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertBoostRequestMessage(RecipientId recipientId, long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("address", recipientId.serialize());
        contentValues.put(MmsSmsColumns.ADDRESS_DEVICE_ID, (Integer) 1);
        contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("date_sent", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("read", (Integer) 1);
        contentValues.put("type", (Long) 15L);
        contentValues.put("thread_id", Long.valueOf(j));
        contentValues.putNull("body");
        getWritableDatabase().insert("sms", (String) null, contentValues);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005c, code lost:
        if (r0.isQuit() != false) goto L_0x0044;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0226  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x023d  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0251  */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public j$.util.Optional<org.thoughtcrime.securesms.database.MessageDatabase.InsertResult> insertMessageInbox(org.thoughtcrime.securesms.sms.IncomingTextMessage r12, long r13) {
        /*
        // Method dump skipped, instructions count: 606
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.SmsDatabase.insertMessageInbox(org.thoughtcrime.securesms.sms.IncomingTextMessage, long):j$.util.Optional");
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Optional<MessageDatabase.InsertResult> insertMessageInbox(IncomingTextMessage incomingTextMessage) {
        return insertMessageInbox(incomingTextMessage, 20);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.InsertResult insertChatSessionRefreshedMessage(RecipientId recipientId, long j, long j2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(Recipient.resolved(recipientId));
        ContentValues contentValues = new ContentValues();
        contentValues.put("address", recipientId.serialize());
        contentValues.put(MmsSmsColumns.ADDRESS_DEVICE_ID, Long.valueOf(j));
        contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("date_sent", Long.valueOf(j2));
        contentValues.put(MmsSmsColumns.DATE_SERVER, (Integer) -1);
        contentValues.put("read", (Integer) 0);
        contentValues.put("type", (Long) 278921216L);
        contentValues.put("thread_id", Long.valueOf(orCreateThreadIdFor));
        long insert = signalWritableDatabase.insert("sms", (String) null, contentValues);
        SignalDatabase.threads().incrementUnread(orCreateThreadIdFor, 1);
        SignalDatabase.threads().update(orCreateThreadIdFor, true);
        notifyConversationListeners(orCreateThreadIdFor);
        TrimThreadJob.enqueueAsync(orCreateThreadIdFor);
        return new MessageDatabase.InsertResult(insert, orCreateThreadIdFor);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertBadDecryptMessage(RecipientId recipientId, int i, long j, long j2, long j3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("address", recipientId.serialize());
        contentValues.put(MmsSmsColumns.ADDRESS_DEVICE_ID, Integer.valueOf(i));
        contentValues.put("date_sent", Long.valueOf(j));
        contentValues.put("date", Long.valueOf(j2));
        contentValues.put(MmsSmsColumns.DATE_SERVER, (Integer) -1);
        contentValues.put("read", (Integer) 0);
        contentValues.put("type", (Long) 13L);
        contentValues.put("thread_id", Long.valueOf(j3));
        this.databaseHelper.getSignalWritableDatabase().insert("sms", (String) null, contentValues);
        SignalDatabase.threads().incrementUnread(j3, 1);
        SignalDatabase.threads().update(j3, true);
        notifyConversationListeners(j3);
        TrimThreadJob.enqueueAsync(j3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x00fb  */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long insertMessageOutbox(long r8, org.thoughtcrime.securesms.sms.OutgoingTextMessage r10, boolean r11, long r12, org.thoughtcrime.securesms.database.MessageDatabase.InsertListener r14) {
        /*
        // Method dump skipped, instructions count: 318
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.SmsDatabase.insertMessageOutbox(long, org.thoughtcrime.securesms.sms.OutgoingTextMessage, boolean, long, org.thoughtcrime.securesms.database.MessageDatabase$InsertListener):long");
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Cursor getExpirationStartedMessages() {
        return this.databaseHelper.getSignalReadableDatabase().query("sms", MESSAGE_PROJECTION, "expire_started > 0", null, null, null, null);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public SmsMessageRecord getSmsMessage(long j) throws NoSuchMessageException {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String[] strArr = MESSAGE_PROJECTION;
        Reader reader = new Reader(signalReadableDatabase.query("sms", strArr, "_id = ?", new String[]{j + ""}, null, null, null));
        SmsMessageRecord next = reader.getNext();
        reader.close();
        if (next != null) {
            return next;
        }
        throw new NoSuchMessageException("No message for ID: " + j);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Cursor getMessageCursor(long j) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String[] strArr = MESSAGE_PROJECTION;
        return signalReadableDatabase.query("sms", strArr, "_id = ?", new String[]{j + ""}, null, null, null);
    }

    /* JADX INFO: finally extract failed */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean deleteMessage(long j) {
        String str = TAG;
        Log.d(str, "deleteMessage(" + j + ")");
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            long threadIdForMessage = getThreadIdForMessage(j);
            signalWritableDatabase.delete("sms", "_id = ?", new String[]{j + ""});
            SignalDatabase.threads().setLastScrolled(threadIdForMessage, 0);
            boolean update = SignalDatabase.threads().update(threadIdForMessage, false, true);
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            notifyConversationListeners(threadIdForMessage);
            return update;
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void ensureMigration() {
        this.databaseHelper.getSignalWritableDatabase();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean isStory(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getOutgoingStoriesTo(RecipientId recipientId) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getAllOutgoingStories(boolean z, int i) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getAllOutgoingStoriesAt(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<StoryResult> getOrderedStoryRecipientsAndIds(boolean z) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getAllStoriesFor(RecipientId recipientId, int i) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean isOutgoingStoryAlreadyInDatabase(RecipientId recipientId, long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageId getStoryId(RecipientId recipientId, long j) throws NoSuchMessageException {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int getNumberOfStoryReplies(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<RecipientId> getUnreadStoryThreadRecipientIds() {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean containsStories(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean hasSelfReplyInStory(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean hasSelfReplyInGroupStory(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Cursor getStoryReplies(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Long getOldestStorySendTimestamp(boolean z) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public StoryViewState getStoryViewState(RecipientId recipientId) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void updateViewedStories(Set<MessageDatabase.SyncMessageId> set) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int deleteStoriesOlderThan(long j, boolean z) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getUnreadStories(RecipientId recipientId, int i) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public ParentStoryId.GroupReply getParentStoryIdForGroupReply(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setGroupStoryMessagesReadSince(long j, long j2, long j3) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void deleteGroupStoryReplies(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageRecord getMessageRecord(long j) throws NoSuchMessageException {
        return getSmsMessage(j);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageRecord getMessageRecordOrNull(long j) {
        try {
            return getSmsMessage(j);
        } catch (NoSuchMessageException unused) {
            return null;
        }
    }

    private boolean isDuplicate(IncomingTextMessage incomingTextMessage, long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("sms", new String[]{SubscriptionLevels.BOOST_LEVEL}, "date_sent = ? AND address = ? AND thread_id = ?", SqlUtil.buildArgs(Long.valueOf(incomingTextMessage.getSentTimestampMillis()), incomingTextMessage.getSender().serialize(), Long.valueOf(j)), null, null, null, SubscriptionLevels.BOOST_LEVEL);
        try {
            boolean moveToFirst = query.moveToFirst();
            query.close();
            return moveToFirst;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void deleteThread(long j) {
        String str = TAG;
        Log.d(str, "deleteThread(" + j + ")");
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.delete("sms", "thread_id = ?", new String[]{j + ""});
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int deleteMessagesInThreadBeforeDate(long j, long j2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        return signalWritableDatabase.delete("sms", "thread_id = ? AND date < " + j2, SqlUtil.buildArgs(j));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void deleteAbandonedMessages() {
        int delete = this.databaseHelper.getSignalWritableDatabase().delete("sms", "thread_id NOT IN (SELECT _id FROM thread)", (String[]) null);
        if (delete > 0) {
            String str = TAG;
            Log.i(str, "Deleted " + delete + " abandoned messages");
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageRecord> getMessagesInThreadAfterInclusive(long j, long j2, long j3) {
        Reader readerFor = readerFor(queryMessages("sms.thread_id = ? AND sms." + getDateReceivedColumnName() + " >= ?", SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(j2)), false, j3));
        try {
            ArrayList arrayList = new ArrayList(readerFor.cursor.getCount());
            while (readerFor.getNext() != null) {
                arrayList.add(readerFor.getCurrent());
            }
            readerFor.close();
            return arrayList;
        } catch (Throwable th) {
            if (readerFor != null) {
                try {
                    readerFor.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private Cursor queryMessages(String str, String[] strArr, boolean z, long j) {
        String str2;
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String[] strArr2 = MESSAGE_PROJECTION;
        String str3 = z ? "_id DESC" : null;
        if (j > 0) {
            str2 = String.valueOf(j);
        } else {
            str2 = null;
        }
        return signalReadableDatabase.query("sms", strArr2, str, strArr, null, null, str3, str2);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void deleteThreads(Set<Long> set) {
        Log.d(TAG, "deleteThreads(count: " + set.size() + ")");
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        String str = "";
        for (Long l : set) {
            str = str + "thread_id = '" + l.longValue() + "' OR ";
        }
        signalWritableDatabase.delete("sms", str.substring(0, str.length() - 4), (String[]) null);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void deleteAllThreads() {
        Log.d(TAG, "deleteAllThreads()");
        this.databaseHelper.getSignalWritableDatabase().delete("sms", (String) null, (String[]) null);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public SQLiteDatabase beginTransaction() {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        return signalWritableDatabase;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void setTransactionSuccessful() {
        this.databaseHelper.getSignalWritableDatabase().setTransactionSuccessful();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void endTransaction(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.setTransactionSuccessful();
        sQLiteDatabase.endTransaction();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void endTransaction() {
        this.databaseHelper.getSignalWritableDatabase().endTransaction();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public SQLiteStatement createInsertStatement(SQLiteDatabase sQLiteDatabase) {
        return sQLiteDatabase.compileStatement("INSERT INTO sms (address, person, date_sent, date, protocol, read, status, type, reply_path_present, subject, body, service_center, thread_id)  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public ViewOnceExpirationInfo getNearestExpiringViewOnceMessage() {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean isSent(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public long getLatestGroupQuitTimestamp(long j, long j2) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean isGroupQuitMessage(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Pair<RecipientId, Long> getOldestUnreadMentionDetails(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int getUnreadMentionCount(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void addFailures(long j, List<NetworkFailure> list) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void setNetworkFailures(long j, Set<NetworkFailure> set) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markDownloadState(long j, long j2) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Optional<MessageDatabase.MmsNotificationInfo> getNotification(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public OutgoingMediaMessage getOutgoingMessage(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Optional<MessageDatabase.InsertResult> insertMessageInbox(IncomingMediaMessage incomingMediaMessage, String str, long j) throws MmsException {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Pair<Long, Long> insertMessageInbox(NotificationInd notificationInd, int i) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Optional<MessageDatabase.InsertResult> insertSecureDecryptedMessageInbox(IncomingMediaMessage incomingMediaMessage, long j) throws MmsException {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public long insertMessageOutbox(OutgoingMediaMessage outgoingMediaMessage, long j, boolean z, MessageDatabase.InsertListener insertListener) throws MmsException {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public long insertMessageOutbox(OutgoingMediaMessage outgoingMediaMessage, long j, boolean z, int i, MessageDatabase.InsertListener insertListener) throws MmsException {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markIncomingNotificationReceived(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markGiftRedemptionCompleted(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markGiftRedemptionStarted(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markGiftRedemptionFailed(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getMessages(Collection<Long> collection) {
        throw new UnsupportedOperationException();
    }

    public static Reader readerFor(Cursor cursor) {
        return new Reader(cursor);
    }

    public static OutgoingMessageReader readerFor(OutgoingTextMessage outgoingTextMessage, long j, long j2) {
        return new OutgoingMessageReader(outgoingTextMessage, j, j2);
    }

    /* loaded from: classes4.dex */
    public static class OutgoingMessageReader {
        private final long id;
        private final OutgoingTextMessage message;
        private final long threadId;

        public OutgoingMessageReader(OutgoingTextMessage outgoingTextMessage, long j, long j2) {
            this.message = outgoingTextMessage;
            this.threadId = j;
            this.id = j2;
        }

        public MessageRecord getCurrent() {
            return new SmsMessageRecord(this.id, this.message.getMessageBody(), this.message.getRecipient(), this.message.getRecipient(), 1, System.currentTimeMillis(), System.currentTimeMillis(), -1, 0, this.message.isSecureMessage() ? MmsSmsColumns.Types.getOutgoingEncryptedMessageType() : MmsSmsColumns.Types.getOutgoingSmsMessageType(), this.threadId, 0, new HashSet(), this.message.getSubscriptionId(), this.message.getExpiresIn(), System.currentTimeMillis(), 0, false, Collections.emptyList(), false, 0, -1);
        }
    }

    /* loaded from: classes4.dex */
    public static class Reader implements Closeable {
        private final Context context = ApplicationDependencies.getApplication();
        private final Cursor cursor;

        public Reader(Cursor cursor) {
            this.cursor = cursor;
        }

        public SmsMessageRecord getNext() {
            Cursor cursor = this.cursor;
            if (cursor == null || !cursor.moveToNext()) {
                return null;
            }
            return getCurrent();
        }

        public int getCount() {
            Cursor cursor = this.cursor;
            if (cursor == null) {
                return 0;
            }
            return cursor.getCount();
        }

        public SmsMessageRecord getCurrent() {
            Cursor cursor = this.cursor;
            long j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            Cursor cursor2 = this.cursor;
            long j2 = cursor2.getLong(cursor2.getColumnIndexOrThrow("address"));
            Cursor cursor3 = this.cursor;
            int i = cursor3.getInt(cursor3.getColumnIndexOrThrow(MmsSmsColumns.ADDRESS_DEVICE_ID));
            Cursor cursor4 = this.cursor;
            long j3 = cursor4.getLong(cursor4.getColumnIndexOrThrow("type"));
            Cursor cursor5 = this.cursor;
            long j4 = cursor5.getLong(cursor5.getColumnIndexOrThrow(MmsSmsColumns.NORMALIZED_DATE_RECEIVED));
            Cursor cursor6 = this.cursor;
            long j5 = cursor6.getLong(cursor6.getColumnIndexOrThrow("date_sent"));
            Cursor cursor7 = this.cursor;
            long j6 = cursor7.getLong(cursor7.getColumnIndexOrThrow(MmsSmsColumns.DATE_SERVER));
            Cursor cursor8 = this.cursor;
            long j7 = cursor8.getLong(cursor8.getColumnIndexOrThrow("thread_id"));
            Cursor cursor9 = this.cursor;
            int i2 = cursor9.getInt(cursor9.getColumnIndexOrThrow("status"));
            Cursor cursor10 = this.cursor;
            int i3 = cursor10.getInt(cursor10.getColumnIndexOrThrow("delivery_receipt_count"));
            Cursor cursor11 = this.cursor;
            int i4 = cursor11.getInt(cursor11.getColumnIndexOrThrow("read_receipt_count"));
            Cursor cursor12 = this.cursor;
            String string = cursor12.getString(cursor12.getColumnIndexOrThrow(MmsSmsColumns.MISMATCHED_IDENTITIES));
            Cursor cursor13 = this.cursor;
            int i5 = cursor13.getInt(cursor13.getColumnIndexOrThrow(MmsSmsColumns.SUBSCRIPTION_ID));
            Cursor cursor14 = this.cursor;
            long j8 = cursor14.getLong(cursor14.getColumnIndexOrThrow("expires_in"));
            Cursor cursor15 = this.cursor;
            long j9 = cursor15.getLong(cursor15.getColumnIndexOrThrow(MmsSmsColumns.EXPIRE_STARTED));
            Cursor cursor16 = this.cursor;
            String string2 = cursor16.getString(cursor16.getColumnIndexOrThrow("body"));
            Cursor cursor17 = this.cursor;
            boolean z = cursor17.getInt(cursor17.getColumnIndexOrThrow(MmsSmsColumns.UNIDENTIFIED)) == 1;
            Cursor cursor18 = this.cursor;
            boolean z2 = cursor18.getInt(cursor18.getColumnIndexOrThrow(MmsSmsColumns.REMOTE_DELETED)) == 1;
            long requireLong = CursorUtil.requireLong(this.cursor, MmsSmsColumns.NOTIFIED_TIMESTAMP);
            long requireLong2 = CursorUtil.requireLong(this.cursor, MmsSmsColumns.RECEIPT_TIMESTAMP);
            int i6 = !TextSecurePreferences.isReadReceiptsEnabled(this.context) ? 0 : i4;
            Set<IdentityKeyMismatch> mismatches = getMismatches(string);
            Recipient recipient = Recipient.live(RecipientId.from(j2)).get();
            return new SmsMessageRecord(j, string2, recipient, recipient, i, j5, j4, j6, i3, j3, j7, i2, mismatches, i5, j8, j9, i6, z, Collections.emptyList(), z2, requireLong, requireLong2);
        }

        private Set<IdentityKeyMismatch> getMismatches(String str) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    return ((IdentityKeyMismatchSet) JsonUtils.fromJson(str, IdentityKeyMismatchSet.class)).getItems();
                }
            } catch (IOException e) {
                Log.w(SmsDatabase.TAG, e);
            }
            return Collections.emptySet();
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.cursor.close();
        }
    }

    /* JADX INFO: finally extract failed */
    Optional<MessageDatabase.InsertResult> collapseJoinRequestEventsIfPossible(long j, IncomingGroupUpdateMessage incomingGroupUpdateMessage) {
        MessageDatabase.InsertResult insertResult;
        String str;
        long j2;
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            MmsSmsDatabase.Reader readerFor = MmsSmsDatabase.readerFor(SignalDatabase.mmsSms().getConversation(j, 0, 2));
            MessageRecord next = readerFor.getNext();
            if (next != null && next.isGroupV2()) {
                Optional<ByteString> changeEditor = incomingGroupUpdateMessage.getChangeEditor();
                if (changeEditor.isPresent() && next.isGroupV2JoinRequest(changeEditor.get())) {
                    MessageRecord next2 = readerFor.getNext();
                    if (next2 == null || !next2.isGroupV2JoinRequest(changeEditor.get())) {
                        j2 = next.getId();
                        str = MessageRecord.createNewContextWithAppendedDeleteJoinRequest(next, incomingGroupUpdateMessage.getChangeRevision(), changeEditor.get());
                    } else {
                        j2 = next2.getId();
                        str = MessageRecord.createNewContextWithAppendedDeleteJoinRequest(next2, incomingGroupUpdateMessage.getChangeRevision(), changeEditor.get());
                        deleteMessage(next.getId());
                    }
                    ContentValues contentValues = new ContentValues(1);
                    contentValues.put("body", str);
                    getWritableDatabase().update("sms", contentValues, "_id = ?", SqlUtil.buildArgs(j2));
                    insertResult = new MessageDatabase.InsertResult(j2, j);
                    readerFor.close();
                    writableDatabase.setTransactionSuccessful();
                    writableDatabase.endTransaction();
                    return Optional.ofNullable(insertResult);
                }
            }
            insertResult = null;
            readerFor.close();
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            return Optional.ofNullable(insertResult);
        } catch (Throwable th) {
            writableDatabase.endTransaction();
            throw th;
        }
    }
}
