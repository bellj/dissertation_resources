package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public final class DeviceLastResetTime extends GeneratedMessageLite<DeviceLastResetTime, Builder> implements DeviceLastResetTimeOrBuilder {
    private static final DeviceLastResetTime DEFAULT_INSTANCE;
    private static volatile Parser<DeviceLastResetTime> PARSER;
    public static final int RESETTIME_FIELD_NUMBER;
    private Internal.ProtobufList<Pair> resetTime_ = GeneratedMessageLite.emptyProtobufList();

    /* loaded from: classes4.dex */
    public interface PairOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        int getDeviceId();

        long getLastResetTime();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private DeviceLastResetTime() {
    }

    /* loaded from: classes4.dex */
    public static final class Pair extends GeneratedMessageLite<Pair, Builder> implements PairOrBuilder {
        private static final Pair DEFAULT_INSTANCE;
        public static final int DEVICEID_FIELD_NUMBER;
        public static final int LASTRESETTIME_FIELD_NUMBER;
        private static volatile Parser<Pair> PARSER;
        private int deviceId_;
        private long lastResetTime_;

        private Pair() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTime.PairOrBuilder
        public int getDeviceId() {
            return this.deviceId_;
        }

        public void setDeviceId(int i) {
            this.deviceId_ = i;
        }

        public void clearDeviceId() {
            this.deviceId_ = 0;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTime.PairOrBuilder
        public long getLastResetTime() {
            return this.lastResetTime_;
        }

        public void setLastResetTime(long j) {
            this.lastResetTime_ = j;
        }

        public void clearLastResetTime() {
            this.lastResetTime_ = 0;
        }

        public static Pair parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Pair) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Pair parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Pair) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Pair parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Pair) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Pair parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Pair) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Pair parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Pair) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Pair parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Pair) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Pair parseFrom(InputStream inputStream) throws IOException {
            return (Pair) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Pair parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Pair) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Pair parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Pair) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Pair parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Pair) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Pair parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Pair) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Pair parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Pair) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Pair pair) {
            return DEFAULT_INSTANCE.createBuilder(pair);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Pair, Builder> implements PairOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Pair.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTime.PairOrBuilder
            public int getDeviceId() {
                return ((Pair) this.instance).getDeviceId();
            }

            public Builder setDeviceId(int i) {
                copyOnWrite();
                ((Pair) this.instance).setDeviceId(i);
                return this;
            }

            public Builder clearDeviceId() {
                copyOnWrite();
                ((Pair) this.instance).clearDeviceId();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTime.PairOrBuilder
            public long getLastResetTime() {
                return ((Pair) this.instance).getLastResetTime();
            }

            public Builder setLastResetTime(long j) {
                copyOnWrite();
                ((Pair) this.instance).setLastResetTime(j);
                return this;
            }

            public Builder clearLastResetTime() {
                copyOnWrite();
                ((Pair) this.instance).clearLastResetTime();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Pair();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0004\u0002\u0002", new Object[]{"deviceId_", "lastResetTime_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Pair> parser = PARSER;
                    if (parser == null) {
                        synchronized (Pair.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Pair pair = new Pair();
            DEFAULT_INSTANCE = pair;
            GeneratedMessageLite.registerDefaultInstance(Pair.class, pair);
        }

        public static Pair getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Pair> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTime$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTimeOrBuilder
    public List<Pair> getResetTimeList() {
        return this.resetTime_;
    }

    public List<? extends PairOrBuilder> getResetTimeOrBuilderList() {
        return this.resetTime_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTimeOrBuilder
    public int getResetTimeCount() {
        return this.resetTime_.size();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTimeOrBuilder
    public Pair getResetTime(int i) {
        return this.resetTime_.get(i);
    }

    public PairOrBuilder getResetTimeOrBuilder(int i) {
        return this.resetTime_.get(i);
    }

    private void ensureResetTimeIsMutable() {
        Internal.ProtobufList<Pair> protobufList = this.resetTime_;
        if (!protobufList.isModifiable()) {
            this.resetTime_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setResetTime(int i, Pair pair) {
        pair.getClass();
        ensureResetTimeIsMutable();
        this.resetTime_.set(i, pair);
    }

    public void addResetTime(Pair pair) {
        pair.getClass();
        ensureResetTimeIsMutable();
        this.resetTime_.add(pair);
    }

    public void addResetTime(int i, Pair pair) {
        pair.getClass();
        ensureResetTimeIsMutable();
        this.resetTime_.add(i, pair);
    }

    public void addAllResetTime(Iterable<? extends Pair> iterable) {
        ensureResetTimeIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.resetTime_);
    }

    public void clearResetTime() {
        this.resetTime_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeResetTime(int i) {
        ensureResetTimeIsMutable();
        this.resetTime_.remove(i);
    }

    public static DeviceLastResetTime parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DeviceLastResetTime parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DeviceLastResetTime parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DeviceLastResetTime parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DeviceLastResetTime parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DeviceLastResetTime parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DeviceLastResetTime parseFrom(InputStream inputStream) throws IOException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DeviceLastResetTime parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DeviceLastResetTime parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DeviceLastResetTime parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DeviceLastResetTime parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DeviceLastResetTime parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DeviceLastResetTime) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DeviceLastResetTime deviceLastResetTime) {
        return DEFAULT_INSTANCE.createBuilder(deviceLastResetTime);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DeviceLastResetTime, Builder> implements DeviceLastResetTimeOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DeviceLastResetTime.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTimeOrBuilder
        public List<Pair> getResetTimeList() {
            return Collections.unmodifiableList(((DeviceLastResetTime) this.instance).getResetTimeList());
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTimeOrBuilder
        public int getResetTimeCount() {
            return ((DeviceLastResetTime) this.instance).getResetTimeCount();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTimeOrBuilder
        public Pair getResetTime(int i) {
            return ((DeviceLastResetTime) this.instance).getResetTime(i);
        }

        public Builder setResetTime(int i, Pair pair) {
            copyOnWrite();
            ((DeviceLastResetTime) this.instance).setResetTime(i, pair);
            return this;
        }

        public Builder setResetTime(int i, Pair.Builder builder) {
            copyOnWrite();
            ((DeviceLastResetTime) this.instance).setResetTime(i, builder.build());
            return this;
        }

        public Builder addResetTime(Pair pair) {
            copyOnWrite();
            ((DeviceLastResetTime) this.instance).addResetTime(pair);
            return this;
        }

        public Builder addResetTime(int i, Pair pair) {
            copyOnWrite();
            ((DeviceLastResetTime) this.instance).addResetTime(i, pair);
            return this;
        }

        public Builder addResetTime(Pair.Builder builder) {
            copyOnWrite();
            ((DeviceLastResetTime) this.instance).addResetTime(builder.build());
            return this;
        }

        public Builder addResetTime(int i, Pair.Builder builder) {
            copyOnWrite();
            ((DeviceLastResetTime) this.instance).addResetTime(i, builder.build());
            return this;
        }

        public Builder addAllResetTime(Iterable<? extends Pair> iterable) {
            copyOnWrite();
            ((DeviceLastResetTime) this.instance).addAllResetTime(iterable);
            return this;
        }

        public Builder clearResetTime() {
            copyOnWrite();
            ((DeviceLastResetTime) this.instance).clearResetTime();
            return this;
        }

        public Builder removeResetTime(int i) {
            copyOnWrite();
            ((DeviceLastResetTime) this.instance).removeResetTime(i);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DeviceLastResetTime();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"resetTime_", Pair.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DeviceLastResetTime> parser = PARSER;
                if (parser == null) {
                    synchronized (DeviceLastResetTime.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DeviceLastResetTime deviceLastResetTime = new DeviceLastResetTime();
        DEFAULT_INSTANCE = deviceLastResetTime;
        GeneratedMessageLite.registerDefaultInstance(DeviceLastResetTime.class, deviceLastResetTime);
    }

    public static DeviceLastResetTime getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DeviceLastResetTime> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
