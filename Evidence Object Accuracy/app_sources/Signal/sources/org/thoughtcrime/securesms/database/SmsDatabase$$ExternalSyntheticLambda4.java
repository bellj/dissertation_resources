package org.thoughtcrime.securesms.database;

import com.annimon.stream.function.Function;
import java.util.UUID;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SmsDatabase$$ExternalSyntheticLambda4 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((UUID) obj).toString();
    }
}
