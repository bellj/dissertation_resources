package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes4.dex */
public final class RecipientExtras extends GeneratedMessageLite<RecipientExtras, Builder> implements RecipientExtrasOrBuilder {
    private static final RecipientExtras DEFAULT_INSTANCE;
    public static final int HIDESTORY_FIELD_NUMBER;
    public static final int LASTSTORYVIEW_FIELD_NUMBER;
    public static final int MANUALLYSHOWNAVATAR_FIELD_NUMBER;
    private static volatile Parser<RecipientExtras> PARSER;
    private boolean hideStory_;
    private long lastStoryView_;
    private boolean manuallyShownAvatar_;

    private RecipientExtras() {
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.RecipientExtrasOrBuilder
    public boolean getManuallyShownAvatar() {
        return this.manuallyShownAvatar_;
    }

    public void setManuallyShownAvatar(boolean z) {
        this.manuallyShownAvatar_ = z;
    }

    public void clearManuallyShownAvatar() {
        this.manuallyShownAvatar_ = false;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.RecipientExtrasOrBuilder
    public boolean getHideStory() {
        return this.hideStory_;
    }

    public void setHideStory(boolean z) {
        this.hideStory_ = z;
    }

    public void clearHideStory() {
        this.hideStory_ = false;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.RecipientExtrasOrBuilder
    public long getLastStoryView() {
        return this.lastStoryView_;
    }

    public void setLastStoryView(long j) {
        this.lastStoryView_ = j;
    }

    public void clearLastStoryView() {
        this.lastStoryView_ = 0;
    }

    public static RecipientExtras parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (RecipientExtras) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static RecipientExtras parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RecipientExtras) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static RecipientExtras parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (RecipientExtras) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static RecipientExtras parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RecipientExtras) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static RecipientExtras parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (RecipientExtras) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static RecipientExtras parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RecipientExtras) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static RecipientExtras parseFrom(InputStream inputStream) throws IOException {
        return (RecipientExtras) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static RecipientExtras parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RecipientExtras) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static RecipientExtras parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (RecipientExtras) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static RecipientExtras parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RecipientExtras) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static RecipientExtras parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (RecipientExtras) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static RecipientExtras parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RecipientExtras) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(RecipientExtras recipientExtras) {
        return DEFAULT_INSTANCE.createBuilder(recipientExtras);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<RecipientExtras, Builder> implements RecipientExtrasOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(RecipientExtras.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.RecipientExtrasOrBuilder
        public boolean getManuallyShownAvatar() {
            return ((RecipientExtras) this.instance).getManuallyShownAvatar();
        }

        public Builder setManuallyShownAvatar(boolean z) {
            copyOnWrite();
            ((RecipientExtras) this.instance).setManuallyShownAvatar(z);
            return this;
        }

        public Builder clearManuallyShownAvatar() {
            copyOnWrite();
            ((RecipientExtras) this.instance).clearManuallyShownAvatar();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.RecipientExtrasOrBuilder
        public boolean getHideStory() {
            return ((RecipientExtras) this.instance).getHideStory();
        }

        public Builder setHideStory(boolean z) {
            copyOnWrite();
            ((RecipientExtras) this.instance).setHideStory(z);
            return this;
        }

        public Builder clearHideStory() {
            copyOnWrite();
            ((RecipientExtras) this.instance).clearHideStory();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.RecipientExtrasOrBuilder
        public long getLastStoryView() {
            return ((RecipientExtras) this.instance).getLastStoryView();
        }

        public Builder setLastStoryView(long j) {
            copyOnWrite();
            ((RecipientExtras) this.instance).setLastStoryView(j);
            return this;
        }

        public Builder clearLastStoryView() {
            copyOnWrite();
            ((RecipientExtras) this.instance).clearLastStoryView();
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.RecipientExtras$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new RecipientExtras();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u0007\u0002\u0007\u0003\u0002", new Object[]{"manuallyShownAvatar_", "hideStory_", "lastStoryView_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<RecipientExtras> parser = PARSER;
                if (parser == null) {
                    synchronized (RecipientExtras.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        RecipientExtras recipientExtras = new RecipientExtras();
        DEFAULT_INSTANCE = recipientExtras;
        GeneratedMessageLite.registerDefaultInstance(RecipientExtras.class, recipientExtras);
    }

    public static RecipientExtras getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<RecipientExtras> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
