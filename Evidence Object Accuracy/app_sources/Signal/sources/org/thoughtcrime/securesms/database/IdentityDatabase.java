package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import j$.util.Optional;
import java.io.IOException;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.IdentityStoreRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.IdentityUtil;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes4.dex */
public class IdentityDatabase extends Database {
    static final String ADDRESS;
    public static final String CREATE_TABLE;
    private static final String FIRST_USE;
    private static final String ID;
    static final String IDENTITY_KEY;
    private static final String NONBLOCKING_APPROVAL;
    static final String TABLE_NAME;
    private static final String TAG = Log.tag(IdentityDatabase.class);
    private static final String TIMESTAMP;
    static final String VERIFIED;

    /* renamed from: org.thoughtcrime.securesms.database.IdentityDatabase$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus;

        static {
            int[] iArr = new int[VerifiedStatus.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus = iArr;
            try {
                iArr[VerifiedStatus.DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[VerifiedStatus.VERIFIED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[VerifiedStatus.UNVERIFIED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public enum VerifiedStatus {
        DEFAULT,
        VERIFIED,
        UNVERIFIED;

        public int toInt() {
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[ordinal()];
            if (i == 1) {
                return 0;
            }
            if (i == 2) {
                return 1;
            }
            if (i == 3) {
                return 2;
            }
            throw new AssertionError();
        }

        public static VerifiedStatus forState(int i) {
            if (i == 0) {
                return DEFAULT;
            }
            if (i == 1) {
                return VERIFIED;
            }
            if (i == 2) {
                return UNVERIFIED;
            }
            throw new AssertionError("No such state: " + i);
        }
    }

    public IdentityDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public IdentityStoreRecord getIdentityStoreRecord(String str) {
        try {
            Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "address = ?", SqlUtil.buildArgs(str), null, null, null);
            try {
                if (query.moveToFirst()) {
                    String requireString = CursorUtil.requireString(query, IDENTITY_KEY);
                    long requireLong = CursorUtil.requireLong(query, "timestamp");
                    int requireInt = CursorUtil.requireInt(query, VERIFIED);
                    IdentityStoreRecord identityStoreRecord = new IdentityStoreRecord(str, new IdentityKey(Base64.decode(requireString), 0), VerifiedStatus.forState(requireInt), CursorUtil.requireBoolean(query, FIRST_USE), requireLong, CursorUtil.requireBoolean(query, NONBLOCKING_APPROVAL));
                    query.close();
                    return identityStoreRecord;
                }
                if (!UuidUtil.isUuid(str)) {
                    Log.i(TAG, "Could not find identity for E164 either.");
                } else if (SignalDatabase.recipients().containsPhoneOrUuid(str)) {
                    Recipient external = Recipient.external(this.context, str);
                    if (!external.hasE164() || UuidUtil.isUuid(external.requireE164())) {
                        Log.i(TAG, "Could not find identity for UUID, and our recipient doesn't have an E164.");
                    } else {
                        Log.i(TAG, "Could not find identity for UUID. Attempting E164.");
                        IdentityStoreRecord identityStoreRecord2 = getIdentityStoreRecord(external.requireE164());
                        query.close();
                        return identityStoreRecord2;
                    }
                } else {
                    Log.i(TAG, "Could not find identity for UUID, and we don't have a recipient.");
                }
                query.close();
                return null;
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        } catch (IOException | InvalidKeyException e) {
            throw new AssertionError(e);
        }
    }

    public void saveIdentity(String str, RecipientId recipientId, IdentityKey identityKey, VerifiedStatus verifiedStatus, boolean z, long j, boolean z2) {
        saveIdentityInternal(str, recipientId, identityKey, verifiedStatus, z, j, z2);
        SignalDatabase.recipients().markNeedsSync(recipientId);
        StorageSyncHelper.scheduleSyncForDataChange();
    }

    public void setApproval(String str, RecipientId recipientId, boolean z) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(2);
        contentValues.put(NONBLOCKING_APPROVAL, Boolean.valueOf(z));
        signalWritableDatabase.update(TABLE_NAME, contentValues, "address = ?", SqlUtil.buildArgs(str));
        SignalDatabase.recipients().markNeedsSync(recipientId);
        StorageSyncHelper.scheduleSyncForDataChange();
    }

    public void setVerified(String str, RecipientId recipientId, IdentityKey identityKey, VerifiedStatus verifiedStatus) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        String[] buildArgs = SqlUtil.buildArgs(str, Base64.encodeBytes(identityKey.serialize()));
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(VERIFIED, Integer.valueOf(verifiedStatus.toInt()));
        if (signalWritableDatabase.update(TABLE_NAME, contentValues, "address = ? AND identity_key = ?", buildArgs) > 0) {
            Optional<IdentityRecord> identityRecord = getIdentityRecord(str);
            if (identityRecord.isPresent()) {
                EventBus.getDefault().post(identityRecord.get());
            }
            SignalDatabase.recipients().markNeedsSync(recipientId);
            StorageSyncHelper.scheduleSyncForDataChange();
        }
    }

    public void updateIdentityAfterSync(String str, RecipientId recipientId, IdentityKey identityKey, VerifiedStatus verifiedStatus) {
        Optional<IdentityRecord> identityRecord = getIdentityRecord(str);
        boolean isPresent = identityRecord.isPresent();
        boolean hasMatchingKey = hasMatchingKey(str, identityKey);
        boolean z = hasMatchingKey && hasMatchingStatus(str, identityKey, verifiedStatus);
        if (!hasMatchingKey || !z) {
            saveIdentityInternal(str, recipientId, identityKey, verifiedStatus, !isPresent, System.currentTimeMillis(), true);
            Optional<IdentityRecord> identityRecord2 = getIdentityRecord(str);
            if (identityRecord2.isPresent()) {
                EventBus.getDefault().post(identityRecord2.get());
            }
            ApplicationDependencies.getProtocolStore().aci().identities().invalidate(str);
        }
        if (isPresent && !hasMatchingKey) {
            String str2 = TAG;
            Log.w(str2, "Updated identity key during storage sync for " + str + " | Existing: " + identityRecord.get().getIdentityKey().hashCode() + ", New: " + identityKey.hashCode());
            IdentityUtil.markIdentityUpdate(this.context, recipientId);
        }
    }

    public void delete(String str) {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, "address = ?", SqlUtil.buildArgs(str));
    }

    private Optional<IdentityRecord> getIdentityRecord(String str) {
        try {
            Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "address = ?", SqlUtil.buildArgs(str), null, null, null);
            if (query.moveToFirst()) {
                Optional<IdentityRecord> of = Optional.of(getIdentityRecord(query));
                query.close();
                return of;
            }
            query.close();
            return Optional.empty();
        } catch (IOException | InvalidKeyException e) {
            throw new AssertionError(e);
        }
    }

    private boolean hasMatchingKey(String str, IdentityKey identityKey) {
        boolean z = false;
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "address = ? AND identity_key = ?", SqlUtil.buildArgs(str, Base64.encodeBytes(identityKey.serialize())), null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    z = true;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return z;
    }

    private boolean hasMatchingStatus(String str, IdentityKey identityKey, VerifiedStatus verifiedStatus) {
        boolean z = false;
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "address = ? AND identity_key = ? AND verified = ?", SqlUtil.buildArgs(str, Base64.encodeBytes(identityKey.serialize()), Integer.valueOf(verifiedStatus.toInt())), null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    z = true;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return z;
    }

    private static IdentityRecord getIdentityRecord(Cursor cursor) throws IOException, InvalidKeyException {
        String requireString = CursorUtil.requireString(cursor, "address");
        String requireString2 = CursorUtil.requireString(cursor, IDENTITY_KEY);
        long requireLong = CursorUtil.requireLong(cursor, "timestamp");
        int requireInt = CursorUtil.requireInt(cursor, VERIFIED);
        boolean requireBoolean = CursorUtil.requireBoolean(cursor, NONBLOCKING_APPROVAL);
        boolean requireBoolean2 = CursorUtil.requireBoolean(cursor, FIRST_USE);
        return new IdentityRecord(RecipientId.fromSidOrE164(requireString), new IdentityKey(Base64.decode(requireString2), 0), VerifiedStatus.forState(requireInt), requireBoolean2, requireLong, requireBoolean);
    }

    private void saveIdentityInternal(String str, RecipientId recipientId, IdentityKey identityKey, VerifiedStatus verifiedStatus, boolean z, long j, boolean z2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        String encodeBytes = Base64.encodeBytes(identityKey.serialize());
        ContentValues contentValues = new ContentValues();
        contentValues.put("address", str);
        contentValues.put(IDENTITY_KEY, encodeBytes);
        contentValues.put("timestamp", Long.valueOf(j));
        contentValues.put(VERIFIED, Integer.valueOf(verifiedStatus.toInt()));
        contentValues.put(NONBLOCKING_APPROVAL, Integer.valueOf(z2 ? 1 : 0));
        contentValues.put(FIRST_USE, Integer.valueOf(z ? 1 : 0));
        signalWritableDatabase.replace(TABLE_NAME, null, contentValues);
        EventBus.getDefault().post(new IdentityRecord(recipientId, identityKey, verifiedStatus, z, j, z2));
    }
}
