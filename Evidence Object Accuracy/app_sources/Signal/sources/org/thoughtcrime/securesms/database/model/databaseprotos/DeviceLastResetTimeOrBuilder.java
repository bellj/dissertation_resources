package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;
import org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTime;

/* loaded from: classes4.dex */
public interface DeviceLastResetTimeOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    DeviceLastResetTime.Pair getResetTime(int i);

    int getResetTimeCount();

    List<DeviceLastResetTime.Pair> getResetTimeList();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
