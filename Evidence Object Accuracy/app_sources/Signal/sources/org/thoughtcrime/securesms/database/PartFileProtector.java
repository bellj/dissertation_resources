package org.thoughtcrime.securesms.database;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* loaded from: classes4.dex */
public final class PartFileProtector {
    private static final long PROTECTED_DURATION = TimeUnit.MINUTES.toMillis(10);
    private static final Map<String, Long> protectedFiles = new HashMap();

    /* loaded from: classes4.dex */
    public interface CreateFile {
        File create() throws IOException;
    }

    public static synchronized File protect(CreateFile createFile) throws IOException {
        File create;
        synchronized (PartFileProtector.class) {
            create = createFile.create();
            protectedFiles.put(create.getAbsolutePath(), Long.valueOf(System.currentTimeMillis()));
        }
        return create;
    }

    public static synchronized boolean isProtected(File file) {
        boolean z;
        synchronized (PartFileProtector.class) {
            long j = 0;
            Map<String, Long> map = protectedFiles;
            Long l = map.get(file.getAbsolutePath());
            if (l != null) {
                j = Math.max(l.longValue(), file.lastModified());
            }
            z = j > System.currentTimeMillis() - PROTECTED_DURATION;
            if (!z) {
                map.remove(file.getAbsolutePath());
            }
        }
        return z;
    }
}
