package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import j$.util.Optional;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.mms.LegacyMmsConnection;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class ApnDatabase {
    private static final String APN_COLUMN;
    private static final String ASSET_PATH = ("databases" + File.separator + DATABASE_NAME);
    private static final String AUTH_TYPE_COLUMN;
    private static final String BASE_SELECTION;
    private static final String BEARER_COLUMN;
    private static final String CARRIER_COLUMN;
    private static final String CARRIER_ENABLED_COLUMN;
    private static final String DATABASE_NAME;
    private static final String ID_COLUMN;
    private static final String MCC_COLUMN;
    private static final String MCC_MNC_COLUMN;
    private static final String MMSC_COLUMN;
    private static final String MMS_PORT_COLUMN;
    private static final String MMS_PROXY_COLUMN;
    private static final String MNC_COLUMN;
    private static final String MVNO_MATCH_DATA_COLUMN;
    private static final String MVNO_TYPE_COLUMN;
    private static final String PASSWORD_COLUMN;
    private static final String PORT_COLUMN;
    private static final String PROTOCOL_COLUMN;
    private static final String PROXY_COLUMN;
    private static final String ROAMING_PROTOCOL_COLUMN;
    private static final String SERVER_COLUMN;
    private static final String TABLE_NAME;
    private static final String TAG = Log.tag(ApnDatabase.class);
    private static final String TYPE_COLUMN;
    private static final String USER_COLUMN;
    private static ApnDatabase instance = null;
    private final Context context;
    private final SQLiteDatabase db;

    public static synchronized ApnDatabase getInstance(Context context) throws IOException {
        ApnDatabase apnDatabase;
        synchronized (ApnDatabase.class) {
            if (instance == null) {
                instance = new ApnDatabase(context.getApplicationContext());
            }
            apnDatabase = instance;
        }
        return apnDatabase;
    }

    private ApnDatabase(Context context) throws IOException {
        this.context = context;
        File databasePath = context.getDatabasePath(DATABASE_NAME);
        if (databasePath.getParentFile().exists() || databasePath.getParentFile().mkdir()) {
            StreamUtil.copy(context.getAssets().open(ASSET_PATH, 2), new FileOutputStream(databasePath));
            try {
                this.db = SQLiteDatabase.openDatabase(context.getDatabasePath(DATABASE_NAME).getPath(), null, 17);
            } catch (SQLiteException e) {
                throw new IOException(e);
            }
        } else {
            throw new IOException("couldn't make databases directory");
        }
    }

    private LegacyMmsConnection.Apn getCustomApnParameters() {
        String trim = TextSecurePreferences.getMmscUrl(this.context).trim();
        if (!TextUtils.isEmpty(trim) && !trim.startsWith("http")) {
            trim = "http://" + trim;
        }
        return new LegacyMmsConnection.Apn(trim, TextSecurePreferences.getMmscProxy(this.context), TextSecurePreferences.getMmscProxyPort(this.context), TextSecurePreferences.getMmscUsername(this.context), TextSecurePreferences.getMmscPassword(this.context));
    }

    public LegacyMmsConnection.Apn getDefaultApnParameters(String str, String str2) {
        if (str == null) {
            Log.w(TAG, "mccmnc was null, returning null");
            return LegacyMmsConnection.Apn.EMPTY;
        }
        Cursor cursor = null;
        if (str2 != null) {
            try {
                String str3 = TAG;
                Log.d(str3, "Querying table for MCC+MNC " + str + " and APN name " + str2);
                cursor = this.db.query(TABLE_NAME, null, "mccmnc = ? AND apn = ?", new String[]{str, str2}, null, null, null);
            } catch (Throwable th) {
                if (0 != 0) {
                    cursor.close();
                }
                throw th;
            }
        }
        if (cursor == null || !cursor.moveToFirst()) {
            if (cursor != null) {
                cursor.close();
            }
            String str4 = TAG;
            Log.d(str4, "Querying table for MCC+MNC " + str + " without APN name");
            cursor = this.db.query(TABLE_NAME, null, BASE_SELECTION, new String[]{str}, null, null, null);
        }
        if (cursor == null || !cursor.moveToFirst()) {
            Log.w(TAG, "No matching APNs found, returning null");
            LegacyMmsConnection.Apn apn = LegacyMmsConnection.Apn.EMPTY;
            if (cursor != null) {
                cursor.close();
            }
            return apn;
        }
        LegacyMmsConnection.Apn apn2 = new LegacyMmsConnection.Apn(cursor.getString(cursor.getColumnIndexOrThrow(MMSC_COLUMN)), cursor.getString(cursor.getColumnIndexOrThrow(MMS_PROXY_COLUMN)), cursor.getString(cursor.getColumnIndexOrThrow(MMS_PORT_COLUMN)), cursor.getString(cursor.getColumnIndexOrThrow(USER_COLUMN)), cursor.getString(cursor.getColumnIndexOrThrow(PASSWORD_COLUMN)));
        String str5 = TAG;
        Log.d(str5, "Returning preferred APN " + apn2);
        cursor.close();
        return apn2;
    }

    public Optional<LegacyMmsConnection.Apn> getMmsConnectionParameters(String str, String str2) {
        LegacyMmsConnection.Apn apn = new LegacyMmsConnection.Apn(getCustomApnParameters(), getDefaultApnParameters(str, str2), TextSecurePreferences.getUseCustomMmsc(this.context), TextSecurePreferences.getUseCustomMmscProxy(this.context), TextSecurePreferences.getUseCustomMmscProxyPort(this.context), TextSecurePreferences.getUseCustomMmscUsername(this.context), TextSecurePreferences.getUseCustomMmscPassword(this.context));
        if (TextUtils.isEmpty(apn.getMmsc())) {
            return Optional.empty();
        }
        return Optional.of(apn);
    }
}
