package org.thoughtcrime.securesms.database;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.PnpOperation;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceId;

/* compiled from: PnpOperations.kt */
@Metadata(d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b \n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u0000 >2\u00020\u0001:\u0001>Bo\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\t\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\t\u0012\b\u0010\f\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000e\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000e¢\u0006\u0002\u0010\u0011J\u000b\u0010#\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u000eHÆ\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010'\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010(\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010*\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u000eHÆ\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u000eHÆ\u0003J\u0001\u0010-\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000eHÆ\u0001J\u0013\u0010.\u001a\u00020/2\b\u00100\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00101\u001a\u000202HÖ\u0001J\u0014\u00103\u001a\u00020\u00002\f\u00104\u001a\b\u0012\u0004\u0012\u00020605J\t\u00107\u001a\u00020\u0003HÖ\u0001J,\u00108\u001a\u000209*\b\u0012\u0004\u0012\u00020\u000e0:2\u0006\u0010;\u001a\u00020\t2\u0012\u0010<\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0=R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0013\u0010\f\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0017R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0017R\u0013\u0010\n\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0017R\u0013\u0010\u001b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0017R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0013\u0010\r\u001a\u0004\u0018\u00010\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0015R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u000e¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0015¨\u0006?"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpDataSet;", "", CdsDatabase.E164, "", RecipientDatabase.PNI_COLUMN, "Lorg/whispersystems/signalservice/api/push/PNI;", "aci", "Lorg/whispersystems/signalservice/api/push/ACI;", "byE164", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "byPniSid", "byPniOnly", "byAciSid", "e164Record", "Lorg/thoughtcrime/securesms/database/model/RecipientRecord;", "pniSidRecord", "aciSidRecord", "(Ljava/lang/String;Lorg/whispersystems/signalservice/api/push/PNI;Lorg/whispersystems/signalservice/api/push/ACI;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/database/model/RecipientRecord;Lorg/thoughtcrime/securesms/database/model/RecipientRecord;Lorg/thoughtcrime/securesms/database/model/RecipientRecord;)V", "getAci", "()Lorg/whispersystems/signalservice/api/push/ACI;", "getAciSidRecord", "()Lorg/thoughtcrime/securesms/database/model/RecipientRecord;", "getByAciSid", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getByE164", "getByPniOnly", "getByPniSid", "commonId", "getCommonId", "getE164", "()Ljava/lang/String;", "getE164Record", "getPni", "()Lorg/whispersystems/signalservice/api/push/PNI;", "getPniSidRecord", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "perform", "operations", "", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "toString", "replace", "", "", "recipientId", "update", "Lkotlin/Function1;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PnpDataSet {
    public static final Companion Companion = new Companion(null);
    private final ACI aci;
    private final RecipientRecord aciSidRecord;
    private final RecipientId byAciSid;
    private final RecipientId byE164;
    private final RecipientId byPniOnly;
    private final RecipientId byPniSid;
    private final RecipientId commonId;
    private final String e164;
    private final RecipientRecord e164Record;
    private final PNI pni;
    private final RecipientRecord pniSidRecord;

    public final String component1() {
        return this.e164;
    }

    public final RecipientRecord component10() {
        return this.aciSidRecord;
    }

    public final PNI component2() {
        return this.pni;
    }

    public final ACI component3() {
        return this.aci;
    }

    public final RecipientId component4() {
        return this.byE164;
    }

    public final RecipientId component5() {
        return this.byPniSid;
    }

    public final RecipientId component6() {
        return this.byPniOnly;
    }

    public final RecipientId component7() {
        return this.byAciSid;
    }

    public final RecipientRecord component8() {
        return this.e164Record;
    }

    public final RecipientRecord component9() {
        return this.pniSidRecord;
    }

    public final PnpDataSet copy(String str, PNI pni, ACI aci, RecipientId recipientId, RecipientId recipientId2, RecipientId recipientId3, RecipientId recipientId4, RecipientRecord recipientRecord, RecipientRecord recipientRecord2, RecipientRecord recipientRecord3) {
        return new PnpDataSet(str, pni, aci, recipientId, recipientId2, recipientId3, recipientId4, recipientRecord, recipientRecord2, recipientRecord3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PnpDataSet)) {
            return false;
        }
        PnpDataSet pnpDataSet = (PnpDataSet) obj;
        return Intrinsics.areEqual(this.e164, pnpDataSet.e164) && Intrinsics.areEqual(this.pni, pnpDataSet.pni) && Intrinsics.areEqual(this.aci, pnpDataSet.aci) && Intrinsics.areEqual(this.byE164, pnpDataSet.byE164) && Intrinsics.areEqual(this.byPniSid, pnpDataSet.byPniSid) && Intrinsics.areEqual(this.byPniOnly, pnpDataSet.byPniOnly) && Intrinsics.areEqual(this.byAciSid, pnpDataSet.byAciSid) && Intrinsics.areEqual(this.e164Record, pnpDataSet.e164Record) && Intrinsics.areEqual(this.pniSidRecord, pnpDataSet.pniSidRecord) && Intrinsics.areEqual(this.aciSidRecord, pnpDataSet.aciSidRecord);
    }

    public int hashCode() {
        String str = this.e164;
        int i = 0;
        int hashCode = (str == null ? 0 : str.hashCode()) * 31;
        PNI pni = this.pni;
        int hashCode2 = (hashCode + (pni == null ? 0 : pni.hashCode())) * 31;
        ACI aci = this.aci;
        int hashCode3 = (hashCode2 + (aci == null ? 0 : aci.hashCode())) * 31;
        RecipientId recipientId = this.byE164;
        int hashCode4 = (hashCode3 + (recipientId == null ? 0 : recipientId.hashCode())) * 31;
        RecipientId recipientId2 = this.byPniSid;
        int hashCode5 = (hashCode4 + (recipientId2 == null ? 0 : recipientId2.hashCode())) * 31;
        RecipientId recipientId3 = this.byPniOnly;
        int hashCode6 = (hashCode5 + (recipientId3 == null ? 0 : recipientId3.hashCode())) * 31;
        RecipientId recipientId4 = this.byAciSid;
        int hashCode7 = (hashCode6 + (recipientId4 == null ? 0 : recipientId4.hashCode())) * 31;
        RecipientRecord recipientRecord = this.e164Record;
        int hashCode8 = (hashCode7 + (recipientRecord == null ? 0 : recipientRecord.hashCode())) * 31;
        RecipientRecord recipientRecord2 = this.pniSidRecord;
        int hashCode9 = (hashCode8 + (recipientRecord2 == null ? 0 : recipientRecord2.hashCode())) * 31;
        RecipientRecord recipientRecord3 = this.aciSidRecord;
        if (recipientRecord3 != null) {
            i = recipientRecord3.hashCode();
        }
        return hashCode9 + i;
    }

    public String toString() {
        return "PnpDataSet(e164=" + this.e164 + ", pni=" + this.pni + ", aci=" + this.aci + ", byE164=" + this.byE164 + ", byPniSid=" + this.byPniSid + ", byPniOnly=" + this.byPniOnly + ", byAciSid=" + this.byAciSid + ", e164Record=" + this.e164Record + ", pniSidRecord=" + this.pniSidRecord + ", aciSidRecord=" + this.aciSidRecord + ')';
    }

    public PnpDataSet(String str, PNI pni, ACI aci, RecipientId recipientId, RecipientId recipientId2, RecipientId recipientId3, RecipientId recipientId4, RecipientRecord recipientRecord, RecipientRecord recipientRecord2, RecipientRecord recipientRecord3) {
        this.e164 = str;
        this.pni = pni;
        this.aci = aci;
        this.byE164 = recipientId;
        this.byPniSid = recipientId2;
        this.byPniOnly = recipientId3;
        this.byAciSid = recipientId4;
        this.e164Record = recipientRecord;
        this.pniSidRecord = recipientRecord2;
        this.aciSidRecord = recipientRecord3;
        this.commonId = Companion.findCommonId(CollectionsKt__CollectionsKt.listOf((Object[]) new RecipientId[]{recipientId, recipientId2, recipientId3, recipientId4}));
    }

    public /* synthetic */ PnpDataSet(String str, PNI pni, ACI aci, RecipientId recipientId, RecipientId recipientId2, RecipientId recipientId3, RecipientId recipientId4, RecipientRecord recipientRecord, RecipientRecord recipientRecord2, RecipientRecord recipientRecord3, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, pni, aci, recipientId, recipientId2, recipientId3, recipientId4, (i & 128) != 0 ? null : recipientRecord, (i & 256) != 0 ? null : recipientRecord2, (i & 512) != 0 ? null : recipientRecord3);
    }

    public final String getE164() {
        return this.e164;
    }

    public final PNI getPni() {
        return this.pni;
    }

    public final ACI getAci() {
        return this.aci;
    }

    public final RecipientId getByE164() {
        return this.byE164;
    }

    public final RecipientId getByPniSid() {
        return this.byPniSid;
    }

    public final RecipientId getByPniOnly() {
        return this.byPniOnly;
    }

    public final RecipientId getByAciSid() {
        return this.byAciSid;
    }

    public final RecipientRecord getE164Record() {
        return this.e164Record;
    }

    public final RecipientRecord getPniSidRecord() {
        return this.pniSidRecord;
    }

    public final RecipientRecord getAciSidRecord() {
        return this.aciSidRecord;
    }

    public final RecipientId getCommonId() {
        return this.commonId;
    }

    public final PnpDataSet perform(List<? extends PnpOperation> list) {
        RecipientRecord recipientRecord;
        RecipientRecord recipientRecord2;
        RecipientRecord recipientRecord3;
        Object obj;
        Object obj2;
        Object obj3;
        Intrinsics.checkNotNullParameter(list, "operations");
        if (list.isEmpty()) {
            return this;
        }
        Set<RecipientRecord> set = CollectionsKt___CollectionsKt.toMutableSet(CollectionsKt__CollectionsKt.listOfNotNull((Object[]) new RecipientRecord[]{this.e164Record, this.pniSidRecord, this.aciSidRecord}));
        for (PnpOperation pnpOperation : list) {
            if (pnpOperation instanceof PnpOperation.Update) {
                replace(set, ((PnpOperation.Update) pnpOperation).getRecipientId(), new Function1<RecipientRecord, RecipientRecord>(pnpOperation) { // from class: org.thoughtcrime.securesms.database.PnpDataSet$perform$1
                    final /* synthetic */ PnpOperation $operation;

                    /* access modifiers changed from: package-private */
                    {
                        this.$operation = r1;
                    }

                    public final RecipientRecord invoke(RecipientRecord recipientRecord4) {
                        Intrinsics.checkNotNullParameter(recipientRecord4, "record");
                        String e164 = ((PnpOperation.Update) this.$operation).getE164();
                        PNI pni = ((PnpOperation.Update) this.$operation).getPni();
                        ServiceId aci = ((PnpOperation.Update) this.$operation).getAci();
                        if (aci == null) {
                            aci = ((PnpOperation.Update) this.$operation).getPni();
                        }
                        return RecipientRecord.copy$default(recipientRecord4, null, aci, pni, null, e164, null, null, null, null, false, 0, null, null, null, null, 0, 0, null, null, null, null, null, null, null, null, null, null, null, false, 0, null, null, false, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, -23, 1048575, null);
                    }
                });
            } else if (pnpOperation instanceof PnpOperation.RemoveE164) {
                replace(set, ((PnpOperation.RemoveE164) pnpOperation).getRecipientId(), PnpDataSet$perform$2.INSTANCE);
            } else if (pnpOperation instanceof PnpOperation.RemovePni) {
                replace(set, ((PnpOperation.RemovePni) pnpOperation).getRecipientId(), PnpDataSet$perform$3.INSTANCE);
            } else if (pnpOperation instanceof PnpOperation.SetAci) {
                replace(set, ((PnpOperation.SetAci) pnpOperation).getRecipientId(), new Function1<RecipientRecord, RecipientRecord>(pnpOperation) { // from class: org.thoughtcrime.securesms.database.PnpDataSet$perform$4
                    final /* synthetic */ PnpOperation $operation;

                    /* access modifiers changed from: package-private */
                    {
                        this.$operation = r1;
                    }

                    public final RecipientRecord invoke(RecipientRecord recipientRecord4) {
                        Intrinsics.checkNotNullParameter(recipientRecord4, "it");
                        return RecipientRecord.copy$default(recipientRecord4, null, ((PnpOperation.SetAci) this.$operation).getAci(), null, null, null, null, null, null, null, false, 0, null, null, null, null, 0, 0, null, null, null, null, null, null, null, null, null, null, null, false, 0, null, null, false, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, -3, 1048575, null);
                    }
                });
            } else if (pnpOperation instanceof PnpOperation.SetE164) {
                replace(set, ((PnpOperation.SetE164) pnpOperation).getRecipientId(), new Function1<RecipientRecord, RecipientRecord>(pnpOperation) { // from class: org.thoughtcrime.securesms.database.PnpDataSet$perform$5
                    final /* synthetic */ PnpOperation $operation;

                    /* access modifiers changed from: package-private */
                    {
                        this.$operation = r1;
                    }

                    public final RecipientRecord invoke(RecipientRecord recipientRecord4) {
                        Intrinsics.checkNotNullParameter(recipientRecord4, "it");
                        return RecipientRecord.copy$default(recipientRecord4, null, null, null, null, ((PnpOperation.SetE164) this.$operation).getE164(), null, null, null, null, false, 0, null, null, null, null, 0, 0, null, null, null, null, null, null, null, null, null, null, null, false, 0, null, null, false, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, -17, 1048575, null);
                    }
                });
            } else if (pnpOperation instanceof PnpOperation.SetPni) {
                replace(set, ((PnpOperation.SetPni) pnpOperation).getRecipientId(), new Function1<RecipientRecord, RecipientRecord>(pnpOperation) { // from class: org.thoughtcrime.securesms.database.PnpDataSet$perform$6
                    final /* synthetic */ PnpOperation $operation;

                    /* access modifiers changed from: package-private */
                    {
                        this.$operation = r1;
                    }

                    public final RecipientRecord invoke(RecipientRecord recipientRecord4) {
                        ServiceId serviceId;
                        Intrinsics.checkNotNullParameter(recipientRecord4, "record");
                        PNI pni = ((PnpOperation.SetPni) this.$operation).getPni();
                        if (recipientRecord4.sidIsPni()) {
                            serviceId = ((PnpOperation.SetPni) this.$operation).getPni();
                        } else {
                            serviceId = recipientRecord4.getServiceId();
                            if (serviceId == null) {
                                serviceId = ((PnpOperation.SetPni) this.$operation).getPni();
                            }
                        }
                        return RecipientRecord.copy$default(recipientRecord4, null, serviceId, pni, null, null, null, null, null, null, false, 0, null, null, null, null, 0, 0, null, null, null, null, null, null, null, null, null, null, null, false, 0, null, null, false, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, -7, 1048575, null);
                    }
                });
            } else if (pnpOperation instanceof PnpOperation.Merge) {
                for (RecipientRecord recipientRecord4 : set) {
                    PnpOperation.Merge merge = (PnpOperation.Merge) pnpOperation;
                    if (Intrinsics.areEqual(recipientRecord4.getId(), merge.getPrimaryId())) {
                        for (RecipientRecord recipientRecord5 : set) {
                            if (Intrinsics.areEqual(recipientRecord5.getId(), merge.getSecondaryId())) {
                                replace(set, recipientRecord4.getId(), new Function1<RecipientRecord, RecipientRecord>(recipientRecord4, recipientRecord5) { // from class: org.thoughtcrime.securesms.database.PnpDataSet$perform$7
                                    final /* synthetic */ RecipientRecord $primary;
                                    final /* synthetic */ RecipientRecord $secondary;

                                    /* access modifiers changed from: package-private */
                                    {
                                        this.$primary = r1;
                                        this.$secondary = r2;
                                    }

                                    public final RecipientRecord invoke(RecipientRecord recipientRecord6) {
                                        Intrinsics.checkNotNullParameter(recipientRecord6, "<anonymous parameter 0>");
                                        String e164 = this.$primary.getE164();
                                        if (e164 == null) {
                                            e164 = this.$secondary.getE164();
                                        }
                                        PNI pni = this.$primary.getPni();
                                        if (pni == null) {
                                            pni = this.$secondary.getPni();
                                        }
                                        ServiceId serviceId = this.$primary.getServiceId();
                                        if (serviceId == null) {
                                            serviceId = this.$secondary.getServiceId();
                                        }
                                        return RecipientRecord.copy$default(this.$primary, null, serviceId, pni, null, e164, null, null, null, null, false, 0, null, null, null, null, 0, 0, null, null, null, null, null, null, null, null, null, null, null, false, 0, null, null, false, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, -23, 1048575, null);
                                    }
                                });
                                set.remove(recipientRecord5);
                            }
                        }
                        throw new NoSuchElementException("Collection contains no element matching the predicate.");
                    }
                }
                throw new NoSuchElementException("Collection contains no element matching the predicate.");
            } else if (!(pnpOperation instanceof PnpOperation.SessionSwitchoverInsert)) {
                boolean z = pnpOperation instanceof PnpOperation.ChangeNumberInsert;
            }
        }
        RecipientId recipientId = null;
        if (this.e164 != null) {
            Iterator<T> it = set.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj3 = null;
                    break;
                }
                obj3 = it.next();
                if (Intrinsics.areEqual(((RecipientRecord) obj3).getE164(), this.e164)) {
                    break;
                }
            }
            recipientRecord = (RecipientRecord) obj3;
        } else {
            recipientRecord = null;
        }
        if (this.pni != null) {
            Iterator<T> it2 = set.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    obj2 = null;
                    break;
                }
                obj2 = it2.next();
                if (Intrinsics.areEqual(((RecipientRecord) obj2).getServiceId(), this.pni)) {
                    break;
                }
            }
            recipientRecord2 = (RecipientRecord) obj2;
        } else {
            recipientRecord2 = null;
        }
        if (this.aci != null) {
            Iterator<T> it3 = set.iterator();
            while (true) {
                if (!it3.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it3.next();
                if (Intrinsics.areEqual(((RecipientRecord) obj).getServiceId(), this.aci)) {
                    break;
                }
            }
            recipientRecord3 = (RecipientRecord) obj;
        } else {
            recipientRecord3 = null;
        }
        String str = this.e164;
        PNI pni = this.pni;
        ACI aci = this.aci;
        RecipientId id = recipientRecord != null ? recipientRecord.getId() : null;
        RecipientId id2 = recipientRecord2 != null ? recipientRecord2.getId() : null;
        RecipientId recipientId2 = this.byPniOnly;
        if (recipientRecord3 != null) {
            recipientId = recipientRecord3.getId();
        }
        return new PnpDataSet(str, pni, aci, id, id2, recipientId2, recipientId, recipientRecord, recipientRecord2, recipientRecord3);
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u000e\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006H\u0002¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpDataSet$Companion;", "", "()V", "findCommonId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "ids", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final RecipientId findCommonId(List<? extends RecipientId> list) {
            List list2 = CollectionsKt___CollectionsKt.filterNotNull(list);
            if (list2.isEmpty()) {
                return null;
            }
            boolean z = true;
            if (!list2.isEmpty()) {
                Iterator it = list2.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (!Intrinsics.areEqual((RecipientId) it.next(), list2.get(0))) {
                            z = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (z) {
                return (RecipientId) list2.get(0);
            }
            return null;
        }
    }

    public final void replace(Set<RecipientRecord> set, RecipientId recipientId, Function1<? super RecipientRecord, RecipientRecord> function1) {
        Intrinsics.checkNotNullParameter(set, "<this>");
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(function1, "update");
        for (RecipientRecord recipientRecord : set) {
            if (Intrinsics.areEqual(recipientRecord.getId(), recipientId)) {
                set.remove(recipientRecord);
                set.add(function1.invoke(recipientRecord));
                return;
            }
        }
        throw new NoSuchElementException("Collection contains no element matching the predicate.");
    }
}
