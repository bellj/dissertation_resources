package org.thoughtcrime.securesms.database;

import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Predicate;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public class RemappedRecords {
    private static final RemappedRecords INSTANCE = new RemappedRecords();
    private static final String TAG = Log.tag(RemappedRecords.class);
    private Map<RecipientId, RecipientId> recipientMap;
    private Map<Long, Long> threadMap;

    private RemappedRecords() {
    }

    public static RemappedRecords getInstance() {
        return INSTANCE;
    }

    public Optional<RecipientId> getRecipient(RecipientId recipientId) {
        ensureRecipientMapIsPopulated();
        return Optional.ofNullable(this.recipientMap.get(recipientId));
    }

    public Optional<Long> getThread(long j) {
        ensureThreadMapIsPopulated();
        return Optional.ofNullable(this.threadMap.get(Long.valueOf(j)));
    }

    public boolean areAnyRemapped(Collection<RecipientId> collection) {
        ensureRecipientMapIsPopulated();
        return Collection$EL.stream(collection).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.database.RemappedRecords$$ExternalSyntheticLambda0
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return RemappedRecords.this.lambda$areAnyRemapped$0((RecipientId) obj);
            }
        });
    }

    public /* synthetic */ boolean lambda$areAnyRemapped$0(RecipientId recipientId) {
        return this.recipientMap.containsKey(recipientId);
    }

    public Set<RecipientId> remap(Collection<RecipientId> collection) {
        ensureRecipientMapIsPopulated();
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (RecipientId recipientId : collection) {
            if (this.recipientMap.containsKey(recipientId)) {
                linkedHashSet.add(this.recipientMap.get(recipientId));
            } else {
                linkedHashSet.add(recipientId);
            }
        }
        return linkedHashSet;
    }

    public String buildRemapDescription(Collection<RecipientId> collection) {
        StringBuilder sb = new StringBuilder();
        for (RecipientId recipientId : collection) {
            if (this.recipientMap.containsKey(recipientId)) {
                sb.append(recipientId);
                sb.append(" -> ");
                sb.append(this.recipientMap.get(recipientId));
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    public void addRecipient(RecipientId recipientId, RecipientId recipientId2) {
        String str = TAG;
        Log.w(str, "[Recipient] Remapping " + recipientId + " to " + recipientId2);
        Preconditions.checkArgument(recipientId.equals(recipientId2) ^ true, "Cannot remap an ID to the same thing!");
        ensureInTransaction();
        ensureRecipientMapIsPopulated();
        this.recipientMap.put(recipientId, recipientId2);
        SignalDatabase.remappedRecords().addRecipientMapping(recipientId, recipientId2);
    }

    public void addThread(long j, long j2) {
        String str = TAG;
        Log.w(str, "[Thread] Remapping " + j + " to " + j2);
        Preconditions.checkArgument(j != j2, "Cannot remap an ID to the same thing!");
        ensureInTransaction();
        ensureThreadMapIsPopulated();
        this.threadMap.put(Long.valueOf(j), Long.valueOf(j2));
        SignalDatabase.remappedRecords().addThreadMapping(j, j2);
    }

    public void resetCache() {
        this.recipientMap = null;
    }

    private void ensureRecipientMapIsPopulated() {
        if (this.recipientMap == null) {
            this.recipientMap = SignalDatabase.remappedRecords().getAllRecipientMappings();
        }
    }

    private void ensureThreadMapIsPopulated() {
        if (this.threadMap == null) {
            this.threadMap = SignalDatabase.remappedRecords().getAllThreadMappings();
        }
    }

    private void ensureInTransaction() {
        if (!SignalDatabase.inTransaction()) {
            throw new IllegalStateException("Must be in a transaction!");
        }
    }
}
