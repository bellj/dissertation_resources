package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import com.annimon.stream.Stream;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import net.zetetic.database.sqlcipher.SQLiteStatement;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class SmsMigrator {
    private static final String TAG = Log.tag(SmsMigrator.class);

    /* loaded from: classes4.dex */
    public interface SmsMigrationProgressListener {
        void progressUpdate(ProgressDescription progressDescription);
    }

    /* loaded from: classes4.dex */
    private static class SystemColumns {
        private static final String ADDRESS;
        private static final String BODY;
        private static final String DATE_RECEIVED;
        private static final String PERSON;
        private static final String PROTOCOL;
        private static final String READ;
        private static final String REPLY_PATH_PRESENT;
        private static final String SERVICE_CENTER;
        private static final String STATUS;
        private static final String SUBJECT;
        private static final String TYPE;

        private SystemColumns() {
        }
    }

    private static void addStringToStatement(SQLiteStatement sQLiteStatement, Cursor cursor, int i, String str) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow(str);
        if (cursor.isNull(columnIndexOrThrow)) {
            sQLiteStatement.bindNull(i);
        } else {
            sQLiteStatement.bindString(i, cursor.getString(columnIndexOrThrow));
        }
    }

    private static void addIntToStatement(SQLiteStatement sQLiteStatement, Cursor cursor, int i, String str) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow(str);
        if (cursor.isNull(columnIndexOrThrow)) {
            sQLiteStatement.bindNull(i);
        } else {
            sQLiteStatement.bindLong(i, cursor.getLong(columnIndexOrThrow));
        }
    }

    private static void addTranslatedTypeToStatement(SQLiteStatement sQLiteStatement, Cursor cursor, int i, String str) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow(str);
        if (cursor.isNull(columnIndexOrThrow)) {
            sQLiteStatement.bindLong(i, 20);
        } else {
            sQLiteStatement.bindLong(i, MmsSmsColumns.Types.translateFromSystemBaseType(cursor.getLong(columnIndexOrThrow)));
        }
    }

    private static boolean isAppropriateTypeForMigration(Cursor cursor, int i) {
        long translateFromSystemBaseType = MmsSmsColumns.Types.translateFromSystemBaseType(cursor.getLong(i));
        return translateFromSystemBaseType == 20 || translateFromSystemBaseType == 23 || translateFromSystemBaseType == 24;
    }

    private static void getContentValuesForRow(Context context, Cursor cursor, long j, SQLiteStatement sQLiteStatement) {
        sQLiteStatement.bindString(1, Recipient.external(context, cursor.getString(cursor.getColumnIndexOrThrow("address"))).getId().serialize());
        addIntToStatement(sQLiteStatement, cursor, 2, SmsDatabase.PERSON);
        addIntToStatement(sQLiteStatement, cursor, 3, "date");
        addIntToStatement(sQLiteStatement, cursor, 4, "date");
        addIntToStatement(sQLiteStatement, cursor, 5, SmsDatabase.PROTOCOL);
        addIntToStatement(sQLiteStatement, cursor, 6, "read");
        addIntToStatement(sQLiteStatement, cursor, 7, "status");
        addTranslatedTypeToStatement(sQLiteStatement, cursor, 8, "type");
        addIntToStatement(sQLiteStatement, cursor, 9, SmsDatabase.REPLY_PATH_PRESENT);
        addStringToStatement(sQLiteStatement, cursor, 10, SmsDatabase.SUBJECT);
        addStringToStatement(sQLiteStatement, cursor, 11, "body");
        addStringToStatement(sQLiteStatement, cursor, 12, SmsDatabase.SERVICE_CENTER);
        sQLiteStatement.bindLong(13, j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String getTheirCanonicalAddress(android.content.Context r7, java.lang.String r8) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "content://mms-sms/canonical-address/"
            r0.append(r1)
            r0.append(r8)
            java.lang.String r8 = r0.toString()
            android.net.Uri r1 = android.net.Uri.parse(r8)
            r8 = 0
            android.content.ContentResolver r0 = r7.getContentResolver()     // Catch: IllegalStateException -> 0x0040, all -> 0x003b
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5)     // Catch: IllegalStateException -> 0x0040, all -> 0x003b
            if (r7 == 0) goto L_0x0035
            boolean r0 = r7.moveToFirst()     // Catch: IllegalStateException -> 0x0033, all -> 0x004d
            if (r0 == 0) goto L_0x0035
            r0 = 0
            java.lang.String r8 = r7.getString(r0)     // Catch: IllegalStateException -> 0x0033, all -> 0x004d
            r7.close()
            return r8
        L_0x0033:
            r0 = move-exception
            goto L_0x0042
        L_0x0035:
            if (r7 == 0) goto L_0x003a
            r7.close()
        L_0x003a:
            return r8
        L_0x003b:
            r7 = move-exception
            r6 = r8
            r8 = r7
            r7 = r6
            goto L_0x004e
        L_0x0040:
            r0 = move-exception
            r7 = r8
        L_0x0042:
            java.lang.String r1 = org.thoughtcrime.securesms.database.SmsMigrator.TAG     // Catch: all -> 0x004d
            org.signal.core.util.logging.Log.w(r1, r0)     // Catch: all -> 0x004d
            if (r7 == 0) goto L_0x004c
            r7.close()
        L_0x004c:
            return r8
        L_0x004d:
            r8 = move-exception
        L_0x004e:
            if (r7 == 0) goto L_0x0053
            r7.close()
        L_0x0053:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.SmsMigrator.getTheirCanonicalAddress(android.content.Context, java.lang.String):java.lang.String");
    }

    private static Set<Recipient> getOurRecipients(Context context, String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str.trim(), " ");
        HashSet hashSet = new HashSet();
        while (stringTokenizer.hasMoreTokens()) {
            String theirCanonicalAddress = getTheirCanonicalAddress(context, stringTokenizer.nextToken());
            if (theirCanonicalAddress != null) {
                hashSet.add(Recipient.external(context, theirCanonicalAddress));
            }
        }
        if (hashSet.isEmpty()) {
            return null;
        }
        return hashSet;
    }

    private static void migrateConversation(Context context, SmsMigrationProgressListener smsMigrationProgressListener, ProgressDescription progressDescription, long j, long j2) {
        Cursor cursor;
        Throwable th;
        SmsDatabase sms = SignalDatabase.sms();
        SQLiteStatement sQLiteStatement = null;
        try {
            try {
                cursor = context.getContentResolver().query(Uri.parse("content://sms/conversations/" + j), null, null, null, null);
                try {
                    SQLiteDatabase beginTransaction = sms.beginTransaction();
                    sQLiteStatement = sms.createInsertStatement(beginTransaction);
                    while (cursor != null && cursor.moveToNext()) {
                        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("address");
                        int columnIndex = cursor.getColumnIndex("type");
                        if (!cursor.isNull(columnIndexOrThrow) && (cursor.isNull(columnIndex) || isAppropriateTypeForMigration(cursor, columnIndex))) {
                            getContentValuesForRow(context, cursor, j2, sQLiteStatement);
                            sQLiteStatement.execute();
                        }
                        smsMigrationProgressListener.progressUpdate(new ProgressDescription(progressDescription, cursor.getCount(), cursor.getPosition()));
                    }
                    sms.endTransaction(beginTransaction);
                    SignalDatabase.threads().update(j2, true);
                    SignalDatabase.threads().setLastScrolled(j2, 0);
                    SignalDatabase.threads().notifyConversationListeners(j2);
                    if (sQLiteStatement != null) {
                        sQLiteStatement.close();
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (sQLiteStatement != null) {
                        sQLiteStatement.close();
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } catch (SQLiteException e) {
                Log.w(TAG, e);
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public static void migrateDatabase(Context context, SmsMigrationProgressListener smsMigrationProgressListener) {
        ThreadDatabase threads = SignalDatabase.threads();
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(Uri.parse("content://mms-sms/conversations?simple=true"), null, null, null, "date ASC");
            while (cursor != null) {
                if (!cursor.moveToNext()) {
                    break;
                }
                long j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                Set<Recipient> ourRecipients = getOurRecipients(context, cursor.getString(cursor.getColumnIndexOrThrow("recipient_ids")));
                ProgressDescription progressDescription = new ProgressDescription(cursor.getCount(), cursor.getPosition(), 100, 0);
                if (ourRecipients != null) {
                    if (ourRecipients.size() == 1) {
                        migrateConversation(context, smsMigrationProgressListener, progressDescription, j, threads.getOrCreateThreadIdFor(ourRecipients.iterator().next()));
                    } else if (ourRecipients.size() > 1) {
                        ourRecipients.add(Recipient.self());
                        migrateConversation(context, smsMigrationProgressListener, progressDescription, j, threads.getOrCreateThreadIdFor(Recipient.resolved(SignalDatabase.recipients().getOrInsertFromGroupId(SignalDatabase.groups().getOrCreateMmsGroupForMembers(Stream.of(ourRecipients).map(new ContactUtil$$ExternalSyntheticLambda3()).toList()))), 2));
                    }
                }
                progressDescription.incrementPrimaryComplete();
                smsMigrationProgressListener.progressUpdate(progressDescription);
            }
            context.getSharedPreferences("SecureSMS", 0).edit().putBoolean("migrated", true).apply();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class ProgressDescription {
        public int primaryComplete;
        public final int primaryTotal;
        public final int secondaryComplete;
        public final int secondaryTotal;

        ProgressDescription(int i, int i2, int i3, int i4) {
            this.primaryTotal = i;
            this.primaryComplete = i2;
            this.secondaryTotal = i3;
            this.secondaryComplete = i4;
        }

        ProgressDescription(ProgressDescription progressDescription, int i, int i2) {
            this.primaryComplete = progressDescription.primaryComplete;
            this.primaryTotal = progressDescription.primaryTotal;
            this.secondaryComplete = i2;
            this.secondaryTotal = i;
        }

        void incrementPrimaryComplete() {
            this.primaryComplete++;
        }
    }
}
