package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.database.Cursor;
import java.util.List;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public class MediaDatabase extends Database {
    private static final String ALL_MEDIA_QUERY = String.format(BASE_MEDIA_QUERY, "ct NOT LIKE 'text/x-signal-plain'");
    public static final int ALL_THREADS;
    private static final String AUDIO_MEDIA_QUERY = String.format(BASE_MEDIA_QUERY, "ct LIKE 'audio/%'");
    private static final String BASE_MEDIA_QUERY;
    private static final String DOCUMENT_MEDIA_QUERY = String.format(BASE_MEDIA_QUERY, "ct LIKE 'image/svg%' OR (ct NOT LIKE 'image/%' AND ct NOT LIKE 'video/%' AND ct NOT LIKE 'audio/%' AND ct NOT LIKE 'text/x-signal-plain')");
    private static final String GALLERY_MEDIA_QUERY = String.format(BASE_MEDIA_QUERY, "ct NOT LIKE 'image/svg%' AND (ct LIKE 'image/%' OR ct LIKE 'video/%')");
    private static final String THREAD_RECIPIENT_ID;
    private static final String UNIQUE_MEDIA_QUERY;

    public MediaDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public Cursor getGalleryMediaForThread(long j, Sorting sorting) {
        return getGalleryMediaForThread(j, sorting, false);
    }

    public Cursor getGalleryMediaForThread(long j, Sorting sorting, boolean z) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String applyToQuery = sorting.applyToQuery(applyEqualityOperator(j, GALLERY_MEDIA_QUERY));
        return signalReadableDatabase.rawQuery(applyToQuery, new String[]{j + ""});
    }

    public Cursor getDocumentMediaForThread(long j, Sorting sorting) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String applyToQuery = sorting.applyToQuery(applyEqualityOperator(j, DOCUMENT_MEDIA_QUERY));
        return signalReadableDatabase.rawQuery(applyToQuery, new String[]{j + ""});
    }

    public Cursor getAudioMediaForThread(long j, Sorting sorting) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String applyToQuery = sorting.applyToQuery(applyEqualityOperator(j, AUDIO_MEDIA_QUERY));
        return signalReadableDatabase.rawQuery(applyToQuery, new String[]{j + ""});
    }

    public Cursor getAllMediaForThread(long j, Sorting sorting) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String applyToQuery = sorting.applyToQuery(applyEqualityOperator(j, ALL_MEDIA_QUERY));
        return signalReadableDatabase.rawQuery(applyToQuery, new String[]{j + ""});
    }

    private static String applyEqualityOperator(long j, String str) {
        return str.replace("__EQUALITY__", j == -1 ? "!=" : "=");
    }

    public StorageBreakdown getStorageBreakdown() {
        StorageBreakdown storageBreakdown = new StorageBreakdown();
        Cursor rawQuery = this.databaseHelper.getSignalReadableDatabase().rawQuery(UNIQUE_MEDIA_QUERY, new String[0]);
        try {
            int columnIndexOrThrow = rawQuery.getColumnIndexOrThrow(AttachmentDatabase.SIZE);
            int columnIndexOrThrow2 = rawQuery.getColumnIndexOrThrow("ct");
            while (rawQuery.moveToNext()) {
                int i = rawQuery.getInt(columnIndexOrThrow);
                switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[MediaUtil.getSlideTypeFromContentType(rawQuery.getString(columnIndexOrThrow2)).ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                        StorageBreakdown.access$114(storageBreakdown, (long) i);
                        break;
                    case 4:
                        StorageBreakdown.access$214(storageBreakdown, (long) i);
                        break;
                    case 5:
                        StorageBreakdown.access$314(storageBreakdown, (long) i);
                        break;
                    case 6:
                    case 7:
                        StorageBreakdown.access$414(storageBreakdown, (long) i);
                        break;
                }
            }
            rawQuery.close();
            return storageBreakdown;
        } catch (Throwable th) {
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.database.MediaDatabase$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType;

        static {
            int[] iArr = new int[MediaUtil.SlideType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType = iArr;
            try {
                iArr[MediaUtil.SlideType.GIF.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[MediaUtil.SlideType.IMAGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[MediaUtil.SlideType.MMS.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[MediaUtil.SlideType.VIDEO.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[MediaUtil.SlideType.AUDIO.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[MediaUtil.SlideType.LONG_TEXT.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$MediaUtil$SlideType[MediaUtil.SlideType.DOCUMENT.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class MediaRecord {
        private final DatabaseAttachment attachment;
        private final long date;
        private final boolean outgoing;
        private final RecipientId recipientId;
        private final long threadId;
        private final RecipientId threadRecipientId;

        private MediaRecord(DatabaseAttachment databaseAttachment, RecipientId recipientId, RecipientId recipientId2, long j, long j2, boolean z) {
            this.attachment = databaseAttachment;
            this.recipientId = recipientId;
            this.threadRecipientId = recipientId2;
            this.threadId = j;
            this.date = j2;
            this.outgoing = z;
        }

        public static MediaRecord from(Context context, Cursor cursor) {
            long j;
            List<DatabaseAttachment> attachments = SignalDatabase.attachments().getAttachments(cursor);
            RecipientId from = RecipientId.from(cursor.getLong(cursor.getColumnIndexOrThrow("address")));
            long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("thread_id"));
            boolean isOutgoingMessageType = MmsSmsColumns.Types.isOutgoingMessageType(cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.MESSAGE_BOX)));
            if (MmsSmsColumns.Types.isPushType(cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.MESSAGE_BOX)))) {
                j = cursor.getLong(cursor.getColumnIndexOrThrow("date"));
            } else {
                j = cursor.getLong(cursor.getColumnIndexOrThrow(MmsSmsColumns.NORMALIZED_DATE_RECEIVED));
            }
            return new MediaRecord((attachments == null || attachments.size() <= 0) ? null : attachments.get(0), from, RecipientId.from(cursor.getLong(cursor.getColumnIndexOrThrow(MediaDatabase.THREAD_RECIPIENT_ID))), j2, j, isOutgoingMessageType);
        }

        public DatabaseAttachment getAttachment() {
            return this.attachment;
        }

        public String getContentType() {
            return this.attachment.getContentType();
        }

        public RecipientId getRecipientId() {
            return this.recipientId;
        }

        public RecipientId getThreadRecipientId() {
            return this.threadRecipientId;
        }

        public long getThreadId() {
            return this.threadId;
        }

        public long getDate() {
            return this.date;
        }

        public boolean isOutgoing() {
            return this.outgoing;
        }
    }

    /* loaded from: classes4.dex */
    public enum Sorting {
        Newest("part.mid DESC, part.display_order DESC, part._id DESC"),
        Oldest("part.mid ASC, part.display_order DESC, part._id ASC"),
        Largest("part.data_size DESC, part.display_order DESC");
        
        private final String postFix;

        Sorting(String str) {
            this.postFix = " ORDER BY " + str;
        }

        public String applyToQuery(String str) {
            return str + this.postFix;
        }

        public boolean isRelatedToFileSize() {
            return this == Largest;
        }
    }

    /* loaded from: classes4.dex */
    public static final class StorageBreakdown {
        private long audioSize;
        private long documentSize;
        private long photoSize;
        private long videoSize;

        static /* synthetic */ long access$114(StorageBreakdown storageBreakdown, long j) {
            long j2 = storageBreakdown.photoSize + j;
            storageBreakdown.photoSize = j2;
            return j2;
        }

        static /* synthetic */ long access$214(StorageBreakdown storageBreakdown, long j) {
            long j2 = storageBreakdown.videoSize + j;
            storageBreakdown.videoSize = j2;
            return j2;
        }

        static /* synthetic */ long access$314(StorageBreakdown storageBreakdown, long j) {
            long j2 = storageBreakdown.audioSize + j;
            storageBreakdown.audioSize = j2;
            return j2;
        }

        static /* synthetic */ long access$414(StorageBreakdown storageBreakdown, long j) {
            long j2 = storageBreakdown.documentSize + j;
            storageBreakdown.documentSize = j2;
            return j2;
        }

        public long getPhotoSize() {
            return this.photoSize;
        }

        public long getVideoSize() {
            return this.videoSize;
        }

        public long getAudioSize() {
            return this.audioSize;
        }

        public long getDocumentSize() {
            return this.documentSize;
        }
    }
}
