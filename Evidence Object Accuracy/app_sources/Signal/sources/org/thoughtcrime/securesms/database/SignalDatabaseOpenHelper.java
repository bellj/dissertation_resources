package org.thoughtcrime.securesms.database;

import net.zetetic.database.sqlcipher.SQLiteDatabase;

/* loaded from: classes.dex */
public interface SignalDatabaseOpenHelper {
    String getDatabaseName();

    SQLiteDatabase getSqlCipherDatabase();
}
