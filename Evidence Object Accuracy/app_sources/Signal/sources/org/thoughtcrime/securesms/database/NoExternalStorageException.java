package org.thoughtcrime.securesms.database;

/* loaded from: classes4.dex */
public class NoExternalStorageException extends Exception {
    public NoExternalStorageException() {
    }

    public NoExternalStorageException(String str) {
        super(str);
    }

    public NoExternalStorageException(Throwable th) {
        super(th);
    }

    public NoExternalStorageException(String str, Throwable th) {
        super(str, th);
    }
}
