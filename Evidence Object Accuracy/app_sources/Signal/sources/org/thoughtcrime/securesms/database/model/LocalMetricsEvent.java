package org.thoughtcrime.securesms.database.model;

import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;

/* compiled from: LocalMetricsEvent.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0005HÆ\u0003J\u000f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003J7\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\b\u0010\u001c\u001a\u00020\u0005H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/LocalMetricsEvent;", "", "createdAt", "", "eventId", "", "eventName", "splits", "", "Lorg/thoughtcrime/securesms/database/model/LocalMetricsSplit;", "(JLjava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getCreatedAt", "()J", "getEventId", "()Ljava/lang/String;", "getEventName", "getSplits", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LocalMetricsEvent {
    private final long createdAt;
    private final String eventId;
    private final String eventName;
    private final List<LocalMetricsSplit> splits;

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: org.thoughtcrime.securesms.database.model.LocalMetricsEvent */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ LocalMetricsEvent copy$default(LocalMetricsEvent localMetricsEvent, long j, String str, String str2, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            j = localMetricsEvent.createdAt;
        }
        if ((i & 2) != 0) {
            str = localMetricsEvent.eventId;
        }
        if ((i & 4) != 0) {
            str2 = localMetricsEvent.eventName;
        }
        if ((i & 8) != 0) {
            list = localMetricsEvent.splits;
        }
        return localMetricsEvent.copy(j, str, str2, list);
    }

    public final long component1() {
        return this.createdAt;
    }

    public final String component2() {
        return this.eventId;
    }

    public final String component3() {
        return this.eventName;
    }

    public final List<LocalMetricsSplit> component4() {
        return this.splits;
    }

    public final LocalMetricsEvent copy(long j, String str, String str2, List<LocalMetricsSplit> list) {
        Intrinsics.checkNotNullParameter(str, "eventId");
        Intrinsics.checkNotNullParameter(str2, "eventName");
        Intrinsics.checkNotNullParameter(list, "splits");
        return new LocalMetricsEvent(j, str, str2, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocalMetricsEvent)) {
            return false;
        }
        LocalMetricsEvent localMetricsEvent = (LocalMetricsEvent) obj;
        return this.createdAt == localMetricsEvent.createdAt && Intrinsics.areEqual(this.eventId, localMetricsEvent.eventId) && Intrinsics.areEqual(this.eventName, localMetricsEvent.eventName) && Intrinsics.areEqual(this.splits, localMetricsEvent.splits);
    }

    public int hashCode() {
        return (((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.createdAt) * 31) + this.eventId.hashCode()) * 31) + this.eventName.hashCode()) * 31) + this.splits.hashCode();
    }

    public LocalMetricsEvent(long j, String str, String str2, List<LocalMetricsSplit> list) {
        Intrinsics.checkNotNullParameter(str, "eventId");
        Intrinsics.checkNotNullParameter(str2, "eventName");
        Intrinsics.checkNotNullParameter(list, "splits");
        this.createdAt = j;
        this.eventId = str;
        this.eventName = str2;
        this.splits = list;
    }

    public final long getCreatedAt() {
        return this.createdAt;
    }

    public final String getEventId() {
        return this.eventId;
    }

    public final String getEventName() {
        return this.eventName;
    }

    public final List<LocalMetricsSplit> getSplits() {
        return this.splits;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(this.eventName);
        sb.append("] total: ");
        long j = 0;
        for (LocalMetricsSplit localMetricsSplit : this.splits) {
            j += localMetricsSplit.getDuration();
        }
        sb.append(j);
        sb.append(" | ");
        List<LocalMetricsSplit> list = this.splits;
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (LocalMetricsSplit localMetricsSplit2 : list) {
            arrayList.add(localMetricsSplit2.toString());
        }
        sb.append(CollectionsKt___CollectionsKt.joinToString$default(arrayList, ", ", null, null, 0, null, null, 62, null));
        return sb.toString();
    }
}
