package org.thoughtcrime.securesms.database.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class Mention implements Comparable<Mention>, Parcelable {
    public static final Parcelable.Creator<Mention> CREATOR = new Parcelable.Creator<Mention>() { // from class: org.thoughtcrime.securesms.database.model.Mention.1
        @Override // android.os.Parcelable.Creator
        public Mention createFromParcel(Parcel parcel) {
            return new Mention(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public Mention[] newArray(int i) {
            return new Mention[i];
        }
    };
    private final int length;
    private final RecipientId recipientId;
    private final int start;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public Mention(RecipientId recipientId, int i, int i2) {
        this.recipientId = recipientId;
        this.start = i;
        this.length = i2;
    }

    protected Mention(Parcel parcel) {
        this.recipientId = (RecipientId) parcel.readParcelable(RecipientId.class.getClassLoader());
        this.start = parcel.readInt();
        this.length = parcel.readInt();
    }

    public RecipientId getRecipientId() {
        return this.recipientId;
    }

    public int getStart() {
        return this.start;
    }

    public int getLength() {
        return this.length;
    }

    public int compareTo(Mention mention) {
        return Integer.compare(this.start, mention.start);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Objects.hash(this.recipientId, Integer.valueOf(this.start), Integer.valueOf(this.length));
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Mention mention = (Mention) obj;
        if (this.recipientId.equals(mention.recipientId) && this.start == mention.start && this.length == mention.length) {
            return true;
        }
        return false;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.recipientId, i);
        parcel.writeInt(this.start);
        parcel.writeInt(this.length);
    }
}
