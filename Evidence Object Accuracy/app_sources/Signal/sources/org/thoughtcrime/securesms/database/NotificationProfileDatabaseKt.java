package org.thoughtcrime.securesms.database;

import j$.time.DayOfWeek;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.collections.CollectionsKt___CollectionsKt;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* compiled from: NotificationProfileDatabase.kt */
@Metadata(bv = {}, d1 = {"\u0000\u000e\n\u0002\u0010\u001c\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u001a\u0012\u0010\u0003\u001a\u00020\u0002*\b\u0012\u0004\u0012\u00020\u00010\u0000H\u0002\u001a\f\u0010\u0004\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\f\u0010\u0003\u001a\u00020\u0002*\u00020\u0001H\u0002¨\u0006\u0005"}, d2 = {"", "j$/time/DayOfWeek", "", "serialize", "toDayOfWeek", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class NotificationProfileDatabaseKt {

    /* compiled from: NotificationProfileDatabase.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[DayOfWeek.values().length];
            iArr[DayOfWeek.MONDAY.ordinal()] = 1;
            iArr[DayOfWeek.TUESDAY.ordinal()] = 2;
            iArr[DayOfWeek.WEDNESDAY.ordinal()] = 3;
            iArr[DayOfWeek.THURSDAY.ordinal()] = 4;
            iArr[DayOfWeek.FRIDAY.ordinal()] = 5;
            iArr[DayOfWeek.SATURDAY.ordinal()] = 6;
            iArr[DayOfWeek.SUNDAY.ordinal()] = 7;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public static final String serialize(Iterable<? extends DayOfWeek> iterable) {
        return CollectionsKt___CollectionsKt.joinToString$default(iterable, ",", null, null, 0, null, NotificationProfileDatabaseKt$serialize$1.INSTANCE, 30, null);
    }

    public static final DayOfWeek toDayOfWeek(String str) {
        switch (str.hashCode()) {
            case 49:
                if (str.equals(SubscriptionLevels.BOOST_LEVEL)) {
                    return DayOfWeek.MONDAY;
                }
                break;
            case 50:
                if (str.equals("2")) {
                    return DayOfWeek.TUESDAY;
                }
                break;
            case 51:
                if (str.equals("3")) {
                    return DayOfWeek.WEDNESDAY;
                }
                break;
            case 52:
                if (str.equals("4")) {
                    return DayOfWeek.THURSDAY;
                }
                break;
            case 53:
                if (str.equals("5")) {
                    return DayOfWeek.FRIDAY;
                }
                break;
            case 54:
                if (str.equals("6")) {
                    return DayOfWeek.SATURDAY;
                }
                break;
            case 55:
                if (str.equals("7")) {
                    return DayOfWeek.SUNDAY;
                }
                break;
        }
        throw new AssertionError("Value (" + str + ") does not map to a day");
    }

    public static final String serialize(DayOfWeek dayOfWeek) {
        switch (WhenMappings.$EnumSwitchMapping$0[dayOfWeek.ordinal()]) {
            case 1:
                return SubscriptionLevels.BOOST_LEVEL;
            case 2:
                return "2";
            case 3:
                return "3";
            case 4:
                return "4";
            case 5:
                return "5";
            case 6:
                return "6";
            case 7:
                return "7";
            default:
                throw new NoWhenBranchMatchedException();
        }
    }
}
