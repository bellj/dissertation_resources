package org.thoughtcrime.securesms.database.model;

import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoryViewState$Companion$$ExternalSyntheticLambda2 implements ObservableOnSubscribe {
    public final /* synthetic */ RecipientId f$0;

    public /* synthetic */ StoryViewState$Companion$$ExternalSyntheticLambda2(RecipientId recipientId) {
        this.f$0 = recipientId;
    }

    @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
    public final void subscribe(ObservableEmitter observableEmitter) {
        StoryViewState.Companion.m1746getState$lambda8(this.f$0, observableEmitter);
    }
}
