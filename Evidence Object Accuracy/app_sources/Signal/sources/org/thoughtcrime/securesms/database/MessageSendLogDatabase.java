package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysJvmKt;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import net.zetetic.database.sqlcipher.SQLiteConstraintException;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageLogEntry;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.RecipientAccessList;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* compiled from: MessageSendLogDatabase.kt */
@Metadata(d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 02\u00020\u0001:\u000501234B\u0019\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J6\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0013\u001a\u00020\u0014J\u0016\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u0017J$\u0010\u0018\u001a\u00020\u00142\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\b0\u001a2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u001cJ\u001e\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u001cJ \u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0019\u001a\u00020\bJ<\u0010 \u001a\u00020\b2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\"0\u001a2\u0006\u0010\u0019\u001a\u00020\b2\u0006\u0010#\u001a\u00020$2\u0006\u0010\u000f\u001a\u00020\u00102\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00120\u001aH\u0002J:\u0010&\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\b2\f\u0010'\u001a\b\u0012\u0004\u0012\u00020(0\u001a2\f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u000e0\u001a2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J4\u0010&\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00120\u001aJ.\u0010&\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0016\u0010*\u001a\u00020\u00142\u0006\u0010+\u001a\u00020\u000b2\u0006\u0010,\u001a\u00020\u000bJ\u0016\u0010-\u001a\u00020\u00142\u0006\u0010.\u001a\u00020\b2\u0006\u0010/\u001a\u00020\b¨\u00065"}, d2 = {"Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "addRecipientToExistingEntryIfPossible", "", "payloadId", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "sentTimestamp", "sendMessageResult", "Lorg/whispersystems/signalservice/api/messages/SendMessageResult;", "contentHint", "Lorg/whispersystems/signalservice/api/crypto/ContentHint;", "messageId", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "deleteAll", "", "deleteAllRelatedToMessage", "mms", "", "deleteEntriesForRecipient", "dateSent", "", "device", "", "deleteEntryForRecipient", "getLogEntry", "Lorg/thoughtcrime/securesms/database/model/MessageLogEntry;", "insert", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase$RecipientDevice;", "content", "Lorg/whispersystems/signalservice/internal/push/SignalServiceProtos$Content;", "messageIds", "insertIfPossible", "possibleRecipients", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "results", "remapRecipient", "oldRecipientId", "newRecipientId", "trimOldMessages", "currentTime", "maxAge", "Companion", "MessageTable", "PayloadTable", "RecipientDevice", "RecipientTable", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class MessageSendLogDatabase extends Database {
    public static final String[] CREATE_INDEXES;
    public static final String[] CREATE_TABLE = {PayloadTable.CREATE_TABLE, RecipientTable.CREATE_TABLE, MessageTable.CREATE_TABLE};
    public static final String[] CREATE_TRIGGERS;
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(MessageSendLogDatabase.class);

    public MessageSendLogDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    /* compiled from: MessageSendLogDatabase.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0006R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\t\u001a\n \n*\u0004\u0018\u00010\u00050\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase$Companion;", "", "()V", "CREATE_INDEXES", "", "", "[Ljava/lang/String;", "CREATE_TABLE", "CREATE_TRIGGERS", "TAG", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    static {
        Companion = new Companion(null);
        TAG = Log.tag(MessageSendLogDatabase.class);
        CREATE_TABLE = new String[]{PayloadTable.CREATE_TABLE, RecipientTable.CREATE_TABLE, MessageTable.CREATE_TABLE};
        PayloadTable payloadTable = PayloadTable.INSTANCE;
        CREATE_INDEXES = (String[]) ArraysKt___ArraysJvmKt.plus(ArraysKt___ArraysJvmKt.plus((Object[]) payloadTable.getCREATE_INDEXES(), (Object[]) RecipientTable.INSTANCE.getCREATE_INDEXES()), (Object[]) MessageTable.INSTANCE.getCREATE_INDEXES());
        CREATE_TRIGGERS = payloadTable.getCREATE_TRIGGERS();
    }

    /* compiled from: MessageSendLogDatabase.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\n\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0019\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007¢\u0006\n\n\u0002\u0010\n\u001a\u0004\b\b\u0010\tR\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0019\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007¢\u0006\n\n\u0002\u0010\n\u001a\u0004\b\r\u0010\tR\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase$PayloadTable;", "", "()V", "CONTENT", "", "CONTENT_HINT", "CREATE_INDEXES", "", "getCREATE_INDEXES", "()[Ljava/lang/String;", "[Ljava/lang/String;", "CREATE_TABLE", "CREATE_TRIGGERS", "getCREATE_TRIGGERS", "DATE_SENT", "ID", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class PayloadTable {
        public static final String CONTENT;
        public static final String CONTENT_HINT;
        private static final String[] CREATE_INDEXES = {"CREATE INDEX msl_payload_date_sent_index ON msl_payload (date_sent)"};
        public static final String CREATE_TABLE;
        private static final String[] CREATE_TRIGGERS = {"\n        CREATE TRIGGER msl_sms_delete AFTER DELETE ON sms \n        BEGIN \n        \tDELETE FROM msl_payload WHERE _id IN (SELECT payload_id FROM msl_message WHERE message_id = old._id AND is_mms = 0);\n        END\n      ", "\n        CREATE TRIGGER msl_mms_delete AFTER DELETE ON mms \n        BEGIN \n        \tDELETE FROM msl_payload WHERE _id IN (SELECT payload_id FROM msl_message WHERE message_id = old._id AND is_mms = 1);\n        END\n      ", "\n        CREATE TRIGGER msl_attachment_delete AFTER DELETE ON part\n        BEGIN\n        \tDELETE FROM msl_payload WHERE _id IN (SELECT payload_id FROM msl_message WHERE message_id = old.mid AND is_mms = 1);\n        END\n      "};
        public static final String DATE_SENT;
        public static final String ID;
        public static final PayloadTable INSTANCE = new PayloadTable();
        public static final String TABLE_NAME;

        private PayloadTable() {
        }

        public final String[] getCREATE_INDEXES() {
            return CREATE_INDEXES;
        }

        public final String[] getCREATE_TRIGGERS() {
            return CREATE_TRIGGERS;
        }
    }

    /* compiled from: MessageSendLogDatabase.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\n\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0019\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\n\n\u0002\u0010\b\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\t\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase$RecipientTable;", "", "()V", "CREATE_INDEXES", "", "", "getCREATE_INDEXES", "()[Ljava/lang/String;", "[Ljava/lang/String;", "CREATE_TABLE", "DEVICE", "ID", "PAYLOAD_ID", "RECIPIENT_ID", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class RecipientTable {
        private static final String[] CREATE_INDEXES = {"CREATE INDEX msl_recipient_recipient_index ON msl_recipient (recipient_id, device, payload_id)", "CREATE INDEX msl_recipient_payload_index ON msl_recipient (payload_id)"};
        public static final String CREATE_TABLE;
        public static final String DEVICE;
        public static final String ID;
        public static final RecipientTable INSTANCE = new RecipientTable();
        public static final String PAYLOAD_ID;
        public static final String RECIPIENT_ID;
        public static final String TABLE_NAME;

        private RecipientTable() {
        }

        public final String[] getCREATE_INDEXES() {
            return CREATE_INDEXES;
        }
    }

    /* compiled from: MessageSendLogDatabase.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\n\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0019\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\n\n\u0002\u0010\b\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\t\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase$MessageTable;", "", "()V", "CREATE_INDEXES", "", "", "getCREATE_INDEXES", "()[Ljava/lang/String;", "[Ljava/lang/String;", "CREATE_TABLE", "ID", "IS_MMS", "MESSAGE_ID", "PAYLOAD_ID", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class MessageTable {
        private static final String[] CREATE_INDEXES = {"CREATE INDEX msl_message_message_index ON msl_message (message_id, is_mms, payload_id)"};
        public static final String CREATE_TABLE;
        public static final String ID;
        public static final MessageTable INSTANCE = new MessageTable();
        public static final String IS_MMS;
        public static final String MESSAGE_ID;
        public static final String PAYLOAD_ID;
        public static final String TABLE_NAME;

        private MessageTable() {
        }

        public final String[] getCREATE_INDEXES() {
            return CREATE_INDEXES;
        }
    }

    public final long insertIfPossible(RecipientId recipientId, long j, SendMessageResult sendMessageResult, ContentHint contentHint, MessageId messageId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(sendMessageResult, "sendMessageResult");
        Intrinsics.checkNotNullParameter(contentHint, "contentHint");
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        if (!FeatureFlags.retryReceipts() || !sendMessageResult.isSuccess() || !sendMessageResult.getSuccess().getContent().isPresent()) {
            return -1;
        }
        List<Integer> devices = sendMessageResult.getSuccess().getDevices();
        Intrinsics.checkNotNullExpressionValue(devices, "sendMessageResult.success.devices");
        List<RecipientDevice> list = CollectionsKt__CollectionsJVMKt.listOf(new RecipientDevice(recipientId, devices));
        SignalServiceProtos.Content content = sendMessageResult.getSuccess().getContent().get();
        Intrinsics.checkNotNullExpressionValue(content, "sendMessageResult.success.content.get()");
        return insert(list, j, content, contentHint, CollectionsKt__CollectionsJVMKt.listOf(messageId));
    }

    public final long insertIfPossible(RecipientId recipientId, long j, SendMessageResult sendMessageResult, ContentHint contentHint, List<MessageId> list) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(sendMessageResult, "sendMessageResult");
        Intrinsics.checkNotNullParameter(contentHint, "contentHint");
        Intrinsics.checkNotNullParameter(list, "messageIds");
        if (!FeatureFlags.retryReceipts() || !sendMessageResult.isSuccess() || !sendMessageResult.getSuccess().getContent().isPresent()) {
            return -1;
        }
        List<Integer> devices = sendMessageResult.getSuccess().getDevices();
        Intrinsics.checkNotNullExpressionValue(devices, "sendMessageResult.success.devices");
        List<RecipientDevice> list2 = CollectionsKt__CollectionsJVMKt.listOf(new RecipientDevice(recipientId, devices));
        SignalServiceProtos.Content content = sendMessageResult.getSuccess().getContent().get();
        Intrinsics.checkNotNullExpressionValue(content, "sendMessageResult.success.content.get()");
        return insert(list2, j, content, contentHint, list);
    }

    public final long insertIfPossible(long j, List<? extends Recipient> list, List<? extends SendMessageResult> list2, ContentHint contentHint, MessageId messageId) {
        boolean z;
        Intrinsics.checkNotNullParameter(list, "possibleRecipients");
        Intrinsics.checkNotNullParameter(list2, "results");
        Intrinsics.checkNotNullParameter(contentHint, "contentHint");
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        if (!FeatureFlags.retryReceipts()) {
            return -1;
        }
        RecipientAccessList recipientAccessList = new RecipientAccessList(list);
        ArrayList<SendMessageResult> arrayList = new ArrayList();
        Iterator<T> it = list2.iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            SendMessageResult sendMessageResult = (SendMessageResult) next;
            if (!sendMessageResult.isSuccess() || !sendMessageResult.getSuccess().getContent().isPresent()) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        for (SendMessageResult sendMessageResult2 : arrayList) {
            SignalServiceAddress address = sendMessageResult2.getAddress();
            Intrinsics.checkNotNullExpressionValue(address, "result.address");
            RecipientId id = recipientAccessList.requireByAddress(address).getId();
            Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
            List<Integer> devices = sendMessageResult2.getSuccess().getDevices();
            Intrinsics.checkNotNullExpressionValue(devices, "result.success.devices");
            arrayList2.add(new RecipientDevice(id, devices));
        }
        if (arrayList2.isEmpty()) {
            return -1;
        }
        for (SendMessageResult sendMessageResult3 : list2) {
            if (!sendMessageResult3.isSuccess() || !sendMessageResult3.getSuccess().getContent().isPresent()) {
                z = false;
                continue;
            } else {
                z = true;
                continue;
            }
            if (z) {
                SignalServiceProtos.Content content = sendMessageResult3.getSuccess().getContent().get();
                Intrinsics.checkNotNullExpressionValue(content, "results.first { it.isSuc…t }.success.content.get()");
                return insert(arrayList2, j, content, contentHint, CollectionsKt__CollectionsJVMKt.listOf(messageId));
            }
        }
        throw new NoSuchElementException("Collection contains no element matching the predicate.");
    }

    public final long addRecipientToExistingEntryIfPossible(long j, RecipientId recipientId, long j2, SendMessageResult sendMessageResult, ContentHint contentHint, MessageId messageId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(sendMessageResult, "sendMessageResult");
        Intrinsics.checkNotNullParameter(contentHint, "contentHint");
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        if (!FeatureFlags.retryReceipts()) {
            return j;
        }
        if (sendMessageResult.isSuccess() && sendMessageResult.getSuccess().getContent().isPresent()) {
            SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
            signalWritableDatabase.beginTransaction();
            try {
                List<Integer> devices = sendMessageResult.getSuccess().getDevices();
                Intrinsics.checkNotNullExpressionValue(devices, "sendMessageResult.success.devices");
                for (Integer num : devices) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("payload_id", Long.valueOf(j));
                    contentValues.put("recipient_id", recipientId.serialize());
                    contentValues.put("device", num);
                    signalWritableDatabase.insert(RecipientTable.TABLE_NAME, (String) null, contentValues);
                }
                signalWritableDatabase.setTransactionSuccessful();
            } catch (SQLiteConstraintException unused) {
                Log.w(TAG, "Failed to append to existing entry. Creating a new one.");
                long insertIfPossible = insertIfPossible(recipientId, j2, sendMessageResult, contentHint, messageId);
                signalWritableDatabase.setTransactionSuccessful();
                return insertIfPossible;
            } finally {
                signalWritableDatabase.endTransaction();
            }
        }
        return j;
    }

    private final long insert(List<RecipientDevice> list, long j, SignalServiceProtos.Content content, ContentHint contentHint, List<MessageId> list2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("date_sent", Long.valueOf(j));
            contentValues.put("content", content.toByteArray());
            contentValues.put(PayloadTable.CONTENT_HINT, Integer.valueOf(contentHint.getType()));
            long insert = signalWritableDatabase.insert(PayloadTable.TABLE_NAME, (String) null, contentValues);
            ArrayList arrayList = new ArrayList();
            for (RecipientDevice recipientDevice : list) {
                for (Number number : recipientDevice.getDevices()) {
                    int intValue = number.intValue();
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("payload_id", Long.valueOf(insert));
                    contentValues2.put("recipient_id", recipientDevice.getRecipientId().serialize());
                    contentValues2.put("device", Integer.valueOf(intValue));
                    arrayList.add(contentValues2);
                }
            }
            for (SqlUtil.Query query : SqlUtil.buildBulkInsert(RecipientTable.TABLE_NAME, new String[]{"payload_id", "recipient_id", "device"}, arrayList)) {
                signalWritableDatabase.execSQL(query.getWhere(), query.getWhereArgs());
            }
            ArrayList arrayList2 = new ArrayList();
            for (MessageId messageId : list2) {
                ContentValues contentValues3 = new ContentValues();
                contentValues3.put("payload_id", Long.valueOf(insert));
                contentValues3.put("message_id", Long.valueOf(messageId.getId()));
                contentValues3.put("is_mms", Integer.valueOf(messageId.isMms() ? 1 : 0));
                arrayList2.add(contentValues3);
            }
            for (SqlUtil.Query query2 : SqlUtil.buildBulkInsert(MessageTable.TABLE_NAME, new String[]{"payload_id", "message_id", "is_mms"}, arrayList2)) {
                signalWritableDatabase.execSQL(query2.getWhere(), query2.getWhereArgs());
            }
            signalWritableDatabase.setTransactionSuccessful();
            return insert;
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [java.lang.Throwable, org.thoughtcrime.securesms.database.model.MessageLogEntry] */
    public final MessageLogEntry getLogEntry(RecipientId recipientId, int i, long j) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        th = 0;
        if (!FeatureFlags.retryReceipts()) {
            return th;
        }
        trimOldMessages(System.currentTimeMillis(), FeatureFlags.retryRespondMaxAge());
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        Cursor query = signalReadableDatabase.query("msl_payload LEFT JOIN msl_recipient ON msl_payload._id = msl_recipient.payload_id", null, "date_sent = ? AND recipient_id = ? AND device = ?", SqlUtil.buildArgs(Long.valueOf(j), recipientId, Integer.valueOf(i)), null, null, null);
        try {
            if (query.moveToFirst()) {
                Cursor query2 = signalReadableDatabase.query(MessageTable.TABLE_NAME, null, "payload_id  = ?", SqlUtil.buildArgs(CursorUtil.requireLong(query, "payload_id")), null, null, null);
                ArrayList arrayList = new ArrayList();
                while (query2.moveToNext()) {
                    arrayList.add(new MessageId(CursorUtil.requireLong(query2, "message_id"), CursorUtil.requireBoolean(query2, "is_mms")));
                }
                RecipientId from = RecipientId.from(CursorUtil.requireLong(query, "recipient_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(CursorUtil.requireL…pientTable.RECIPIENT_ID))");
                long requireLong = CursorUtil.requireLong(query, "date_sent");
                SignalServiceProtos.Content parseFrom = SignalServiceProtos.Content.parseFrom(CursorUtil.requireBlob(query, "content"));
                Intrinsics.checkNotNullExpressionValue(parseFrom, "parseFrom(CursorUtil.req…r, PayloadTable.CONTENT))");
                ContentHint fromType = ContentHint.fromType(CursorUtil.requireInt(query, PayloadTable.CONTENT_HINT));
                Intrinsics.checkNotNullExpressionValue(fromType, "fromType(CursorUtil.requ…yloadTable.CONTENT_HINT))");
                MessageLogEntry messageLogEntry = new MessageLogEntry(from, requireLong, parseFrom, fromType, arrayList);
                CloseableKt.closeFinally(query2, th);
                return messageLogEntry;
            }
            Unit unit = Unit.INSTANCE;
            return th;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final void deleteAllRelatedToMessage(long j, boolean z) {
        if (FeatureFlags.retryReceipts()) {
            this.databaseHelper.getSignalWritableDatabase().delete(PayloadTable.TABLE_NAME, "_id IN (SELECT payload_id FROM msl_message WHERE message_id = ? AND is_mms = ?)", SqlUtil.buildArgs(Long.valueOf(j), Integer.valueOf(z ? 1 : 0)));
        }
    }

    public final void deleteEntryForRecipient(long j, RecipientId recipientId, int i) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        if (FeatureFlags.retryReceipts()) {
            deleteEntriesForRecipient(CollectionsKt__CollectionsJVMKt.listOf(Long.valueOf(j)), recipientId, i);
        }
    }

    public final void deleteEntriesForRecipient(List<Long> list, RecipientId recipientId, int i) {
        Intrinsics.checkNotNullParameter(list, "dateSent");
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        if (FeatureFlags.retryReceipts()) {
            SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
            signalWritableDatabase.beginTransaction();
            try {
                signalWritableDatabase.delete(RecipientTable.TABLE_NAME, "\n        recipient_id = ? AND\n        device = ? AND\n        payload_id IN (\n          SELECT _id \n          FROM msl_payload \n          WHERE date_sent IN (" + CollectionsKt___CollectionsKt.joinToString$default(list, ",", null, null, 0, null, null, 62, null) + ") \n        )", SqlUtil.buildArgs(recipientId, Integer.valueOf(i)));
                signalWritableDatabase.delete(PayloadTable.TABLE_NAME, "_id NOT IN (SELECT payload_id FROM msl_recipient)", (String[]) null);
                signalWritableDatabase.setTransactionSuccessful();
            } finally {
                signalWritableDatabase.endTransaction();
            }
        }
    }

    public final void deleteAll() {
        if (FeatureFlags.retryReceipts()) {
            this.databaseHelper.getSignalWritableDatabase().delete(PayloadTable.TABLE_NAME, (String) null, (String[]) null);
        }
    }

    public final void trimOldMessages(long j, long j2) {
        if (FeatureFlags.retryReceipts()) {
            this.databaseHelper.getSignalWritableDatabase().delete(PayloadTable.TABLE_NAME, "date_sent < ?", SqlUtil.buildArgs(j - j2));
        }
    }

    public final void remapRecipient(RecipientId recipientId, RecipientId recipientId2) {
        Intrinsics.checkNotNullParameter(recipientId, "oldRecipientId");
        Intrinsics.checkNotNullParameter(recipientId2, "newRecipientId");
        ContentValues contentValues = new ContentValues();
        contentValues.put("recipient_id", recipientId2.serialize());
        this.databaseHelper.getSignalWritableDatabase().update(RecipientTable.TABLE_NAME, contentValues, "recipient_id = ?", SqlUtil.buildArgs(recipientId.serialize()));
    }

    /* compiled from: MessageSendLogDatabase.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/database/MessageSendLogDatabase$RecipientDevice;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "devices", "", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Ljava/util/List;)V", "getDevices", "()Ljava/util/List;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RecipientDevice {
        private final List<Integer> devices;
        private final RecipientId recipientId;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.database.MessageSendLogDatabase$RecipientDevice */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ RecipientDevice copy$default(RecipientDevice recipientDevice, RecipientId recipientId, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = recipientDevice.recipientId;
            }
            if ((i & 2) != 0) {
                list = recipientDevice.devices;
            }
            return recipientDevice.copy(recipientId, list);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final List<Integer> component2() {
            return this.devices;
        }

        public final RecipientDevice copy(RecipientId recipientId, List<Integer> list) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(list, "devices");
            return new RecipientDevice(recipientId, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof RecipientDevice)) {
                return false;
            }
            RecipientDevice recipientDevice = (RecipientDevice) obj;
            return Intrinsics.areEqual(this.recipientId, recipientDevice.recipientId) && Intrinsics.areEqual(this.devices, recipientDevice.devices);
        }

        public int hashCode() {
            return (this.recipientId.hashCode() * 31) + this.devices.hashCode();
        }

        public String toString() {
            return "RecipientDevice(recipientId=" + this.recipientId + ", devices=" + this.devices + ')';
        }

        public RecipientDevice(RecipientId recipientId, List<Integer> list) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(list, "devices");
            this.recipientId = recipientId;
            this.devices = list;
        }

        public final List<Integer> getDevices() {
            return this.devices;
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }
    }
}
