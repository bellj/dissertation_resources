package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;

/* loaded from: classes4.dex */
public interface RecipientExtrasOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    boolean getHideStory();

    long getLastStoryView();

    boolean getManuallyShownAvatar();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
