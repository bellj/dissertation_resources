package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ReactionRecord.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0007HÆ\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001J\t\u0010\u001b\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/ReactionRecord;", "", "emoji", "", "author", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "dateSent", "", "dateReceived", "(Ljava/lang/String;Lorg/thoughtcrime/securesms/recipients/RecipientId;JJ)V", "getAuthor", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getDateReceived", "()J", "getDateSent", "getEmoji", "()Ljava/lang/String;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ReactionRecord {
    private final RecipientId author;
    private final long dateReceived;
    private final long dateSent;
    private final String emoji;

    public static /* synthetic */ ReactionRecord copy$default(ReactionRecord reactionRecord, String str, RecipientId recipientId, long j, long j2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = reactionRecord.emoji;
        }
        if ((i & 2) != 0) {
            recipientId = reactionRecord.author;
        }
        if ((i & 4) != 0) {
            j = reactionRecord.dateSent;
        }
        if ((i & 8) != 0) {
            j2 = reactionRecord.dateReceived;
        }
        return reactionRecord.copy(str, recipientId, j, j2);
    }

    public final String component1() {
        return this.emoji;
    }

    public final RecipientId component2() {
        return this.author;
    }

    public final long component3() {
        return this.dateSent;
    }

    public final long component4() {
        return this.dateReceived;
    }

    public final ReactionRecord copy(String str, RecipientId recipientId, long j, long j2) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        Intrinsics.checkNotNullParameter(recipientId, "author");
        return new ReactionRecord(str, recipientId, j, j2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReactionRecord)) {
            return false;
        }
        ReactionRecord reactionRecord = (ReactionRecord) obj;
        return Intrinsics.areEqual(this.emoji, reactionRecord.emoji) && Intrinsics.areEqual(this.author, reactionRecord.author) && this.dateSent == reactionRecord.dateSent && this.dateReceived == reactionRecord.dateReceived;
    }

    public int hashCode() {
        return (((((this.emoji.hashCode() * 31) + this.author.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.dateSent)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.dateReceived);
    }

    public String toString() {
        return "ReactionRecord(emoji=" + this.emoji + ", author=" + this.author + ", dateSent=" + this.dateSent + ", dateReceived=" + this.dateReceived + ')';
    }

    public ReactionRecord(String str, RecipientId recipientId, long j, long j2) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        Intrinsics.checkNotNullParameter(recipientId, "author");
        this.emoji = str;
        this.author = recipientId;
        this.dateSent = j;
        this.dateReceived = j2;
    }

    public final String getEmoji() {
        return this.emoji;
    }

    public final RecipientId getAuthor() {
        return this.author;
    }

    public final long getDateSent() {
        return this.dateSent;
    }

    public final long getDateReceived() {
        return this.dateReceived;
    }
}
