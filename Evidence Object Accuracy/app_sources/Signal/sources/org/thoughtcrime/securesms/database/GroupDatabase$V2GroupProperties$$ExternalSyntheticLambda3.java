package org.thoughtcrime.securesms.database;

import j$.util.function.Function;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.thoughtcrime.securesms.database.GroupDatabase;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupDatabase$V2GroupProperties$$ExternalSyntheticLambda3 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        DecryptedPendingMember decryptedPendingMember = (DecryptedPendingMember) obj;
        return GroupDatabase.MemberLevel.PENDING_MEMBER;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
