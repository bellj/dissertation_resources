package org.thoughtcrime.securesms.database.model;

import io.reactivex.rxjava3.functions.Cancellable;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.model.StoryViewState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoryViewState$Companion$$ExternalSyntheticLambda4 implements Cancellable {
    public final /* synthetic */ DatabaseObserver.Observer f$0;

    public /* synthetic */ StoryViewState$Companion$$ExternalSyntheticLambda4(DatabaseObserver.Observer observer) {
        this.f$0 = observer;
    }

    @Override // io.reactivex.rxjava3.functions.Cancellable
    public final void cancel() {
        StoryViewState.Companion.m1748getState$lambda8$lambda7(this.f$0);
    }
}
