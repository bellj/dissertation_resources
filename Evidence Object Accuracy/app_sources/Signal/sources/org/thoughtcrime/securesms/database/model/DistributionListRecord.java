package org.thoughtcrime.securesms.database.model;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.DistributionId;

/* compiled from: DistributionListRecord.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u001e\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BY\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\t\u0012\u0006\u0010\u0011\u001a\u00020\u0012¢\u0006\u0002\u0010\u0013J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0005HÆ\u0003J\t\u0010%\u001a\u00020\u0007HÆ\u0003J\t\u0010&\u001a\u00020\tHÆ\u0003J\u000f\u0010'\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003J\u000f\u0010(\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003J\t\u0010)\u001a\u00020\u000fHÆ\u0003J\t\u0010*\u001a\u00020\tHÆ\u0003J\t\u0010+\u001a\u00020\u0012HÆ\u0003Jo\u0010,\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\t2\b\b\u0002\u0010\u0011\u001a\u00020\u0012HÆ\u0001J\u0013\u0010-\u001a\u00020\t2\b\u0010.\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\f\u0010/\u001a\b\u0012\u0004\u0012\u00020\f0\u000bJ\t\u00100\u001a\u000201HÖ\u0001J\t\u00102\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u0010\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0015R\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001d¨\u00063"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "name", "", "distributionId", "Lorg/whispersystems/signalservice/api/push/DistributionId;", "allowsReplies", "", "rawMembers", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "members", "deletedAtTimestamp", "", "isUnknown", "privacyMode", "Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;Ljava/lang/String;Lorg/whispersystems/signalservice/api/push/DistributionId;ZLjava/util/List;Ljava/util/List;JZLorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;)V", "getAllowsReplies", "()Z", "getDeletedAtTimestamp", "()J", "getDistributionId", "()Lorg/whispersystems/signalservice/api/push/DistributionId;", "getId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getMembers", "()Ljava/util/List;", "getName", "()Ljava/lang/String;", "getPrivacyMode", "()Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "getRawMembers", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "getMembersToSync", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DistributionListRecord {
    private final boolean allowsReplies;
    private final long deletedAtTimestamp;
    private final DistributionId distributionId;
    private final DistributionListId id;
    private final boolean isUnknown;
    private final List<RecipientId> members;
    private final String name;
    private final DistributionListPrivacyMode privacyMode;
    private final List<RecipientId> rawMembers;

    /* compiled from: DistributionListRecord.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[DistributionListPrivacyMode.values().length];
            iArr[DistributionListPrivacyMode.ALL.ordinal()] = 1;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public final DistributionListId component1() {
        return this.id;
    }

    public final String component2() {
        return this.name;
    }

    public final DistributionId component3() {
        return this.distributionId;
    }

    public final boolean component4() {
        return this.allowsReplies;
    }

    public final List<RecipientId> component5() {
        return this.rawMembers;
    }

    public final List<RecipientId> component6() {
        return this.members;
    }

    public final long component7() {
        return this.deletedAtTimestamp;
    }

    public final boolean component8() {
        return this.isUnknown;
    }

    public final DistributionListPrivacyMode component9() {
        return this.privacyMode;
    }

    public final DistributionListRecord copy(DistributionListId distributionListId, String str, DistributionId distributionId, boolean z, List<? extends RecipientId> list, List<? extends RecipientId> list2, long j, boolean z2, DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(str, "name");
        Intrinsics.checkNotNullParameter(distributionId, "distributionId");
        Intrinsics.checkNotNullParameter(list, "rawMembers");
        Intrinsics.checkNotNullParameter(list2, "members");
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        return new DistributionListRecord(distributionListId, str, distributionId, z, list, list2, j, z2, distributionListPrivacyMode);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DistributionListRecord)) {
            return false;
        }
        DistributionListRecord distributionListRecord = (DistributionListRecord) obj;
        return Intrinsics.areEqual(this.id, distributionListRecord.id) && Intrinsics.areEqual(this.name, distributionListRecord.name) && Intrinsics.areEqual(this.distributionId, distributionListRecord.distributionId) && this.allowsReplies == distributionListRecord.allowsReplies && Intrinsics.areEqual(this.rawMembers, distributionListRecord.rawMembers) && Intrinsics.areEqual(this.members, distributionListRecord.members) && this.deletedAtTimestamp == distributionListRecord.deletedAtTimestamp && this.isUnknown == distributionListRecord.isUnknown && this.privacyMode == distributionListRecord.privacyMode;
    }

    public int hashCode() {
        int hashCode = ((((this.id.hashCode() * 31) + this.name.hashCode()) * 31) + this.distributionId.hashCode()) * 31;
        boolean z = this.allowsReplies;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int hashCode2 = (((((((hashCode + i2) * 31) + this.rawMembers.hashCode()) * 31) + this.members.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.deletedAtTimestamp)) * 31;
        boolean z2 = this.isUnknown;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        return ((hashCode2 + i) * 31) + this.privacyMode.hashCode();
    }

    public String toString() {
        return "DistributionListRecord(id=" + this.id + ", name=" + this.name + ", distributionId=" + this.distributionId + ", allowsReplies=" + this.allowsReplies + ", rawMembers=" + this.rawMembers + ", members=" + this.members + ", deletedAtTimestamp=" + this.deletedAtTimestamp + ", isUnknown=" + this.isUnknown + ", privacyMode=" + this.privacyMode + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
    /* JADX WARN: Multi-variable type inference failed */
    public DistributionListRecord(DistributionListId distributionListId, String str, DistributionId distributionId, boolean z, List<? extends RecipientId> list, List<? extends RecipientId> list2, long j, boolean z2, DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(str, "name");
        Intrinsics.checkNotNullParameter(distributionId, "distributionId");
        Intrinsics.checkNotNullParameter(list, "rawMembers");
        Intrinsics.checkNotNullParameter(list2, "members");
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        this.id = distributionListId;
        this.name = str;
        this.distributionId = distributionId;
        this.allowsReplies = z;
        this.rawMembers = list;
        this.members = list2;
        this.deletedAtTimestamp = j;
        this.isUnknown = z2;
        this.privacyMode = distributionListPrivacyMode;
    }

    public final DistributionListId getId() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final DistributionId getDistributionId() {
        return this.distributionId;
    }

    public final boolean getAllowsReplies() {
        return this.allowsReplies;
    }

    public final List<RecipientId> getRawMembers() {
        return this.rawMembers;
    }

    public final List<RecipientId> getMembers() {
        return this.members;
    }

    public final long getDeletedAtTimestamp() {
        return this.deletedAtTimestamp;
    }

    public final boolean isUnknown() {
        return this.isUnknown;
    }

    public final DistributionListPrivacyMode getPrivacyMode() {
        return this.privacyMode;
    }

    public final List<RecipientId> getMembersToSync() {
        if (WhenMappings.$EnumSwitchMapping$0[this.privacyMode.ordinal()] == 1) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        return this.rawMembers;
    }
}
