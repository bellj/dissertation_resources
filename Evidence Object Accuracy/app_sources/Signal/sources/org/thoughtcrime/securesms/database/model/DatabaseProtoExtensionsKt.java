package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;

/* compiled from: DatabaseProtoExtensions.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u001a*\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006\u001a\"\u0010\b\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\t\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006\u001a\"\u0010\n\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006¨\u0006\r"}, d2 = {"addButton", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList$Builder;", EmojiSearchDatabase.LABEL, "", "action", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "", "length", "addLink", "link", "addStyle", "style", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList$BodyRange$Style;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DatabaseProtoExtensionsKt {
    public static final BodyRangeList.Builder addStyle(BodyRangeList.Builder builder, BodyRangeList.BodyRange.Style style, int i, int i2) {
        Intrinsics.checkNotNullParameter(builder, "<this>");
        Intrinsics.checkNotNullParameter(style, "style");
        builder.addRanges(BodyRangeList.BodyRange.newBuilder().setStyle(style).setStart(i).setLength(i2));
        return builder;
    }

    public static final BodyRangeList.Builder addLink(BodyRangeList.Builder builder, String str, int i, int i2) {
        Intrinsics.checkNotNullParameter(builder, "<this>");
        Intrinsics.checkNotNullParameter(str, "link");
        builder.addRanges(BodyRangeList.BodyRange.newBuilder().setLink(str).setStart(i).setLength(i2));
        return builder;
    }

    public static final BodyRangeList.Builder addButton(BodyRangeList.Builder builder, String str, String str2, int i, int i2) {
        Intrinsics.checkNotNullParameter(builder, "<this>");
        Intrinsics.checkNotNullParameter(str, EmojiSearchDatabase.LABEL);
        Intrinsics.checkNotNullParameter(str2, "action");
        builder.addRanges(BodyRangeList.BodyRange.newBuilder().setButton(BodyRangeList.BodyRange.Button.newBuilder().setLabel(str).setAction(str2)).setStart(i).setLength(i2));
        return builder;
    }
}
