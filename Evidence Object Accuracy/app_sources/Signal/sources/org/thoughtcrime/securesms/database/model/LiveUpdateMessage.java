package org.thoughtcrime.securesms.database.model;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public final class LiveUpdateMessage {
    public static LiveData<SpannableString> fromMessageDescription(Context context, UpdateDescription updateDescription, int i, boolean z) {
        LiveData liveData;
        if (updateDescription.isStringStatic()) {
            return LiveDataUtil.just(toSpannable(context, updateDescription, updateDescription.getStaticSpannable(), i, z));
        }
        List list = Stream.of(updateDescription.getMentioned()).map(new Function() { // from class: org.thoughtcrime.securesms.database.model.LiveUpdateMessage$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return LiveUpdateMessage.lambda$fromMessageDescription$0((ServiceId) obj);
            }
        }).toList();
        if (list.isEmpty()) {
            liveData = LiveDataUtil.just(new Object());
        } else {
            liveData = LiveDataUtil.merge(list);
        }
        return Transformations.map(liveData, new androidx.arch.core.util.Function(context, updateDescription, i, z) { // from class: org.thoughtcrime.securesms.database.model.LiveUpdateMessage$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ UpdateDescription f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return LiveUpdateMessage.lambda$fromMessageDescription$1(this.f$0, this.f$1, this.f$2, this.f$3, obj);
            }
        });
    }

    public static /* synthetic */ LiveData lambda$fromMessageDescription$0(ServiceId serviceId) {
        return Recipient.resolved(RecipientId.from(serviceId)).live().getLiveData();
    }

    public static /* synthetic */ SpannableString lambda$fromMessageDescription$1(Context context, UpdateDescription updateDescription, int i, boolean z, Object obj) {
        return toSpannable(context, updateDescription, updateDescription.getSpannable(), i, z);
    }

    public static LiveData<SpannableString> recipientToStringAsync(RecipientId recipientId, j$.util.function.Function<Recipient, SpannableString> function) {
        LiveData<Recipient> liveDataResolved = Recipient.live(recipientId).getLiveDataResolved();
        Objects.requireNonNull(function);
        return Transformations.map(liveDataResolved, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.database.model.LiveUpdateMessage$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return (SpannableString) j$.util.function.Function.this.apply((Recipient) obj);
            }
        });
    }

    private static SpannableString toSpannable(Context context, UpdateDescription updateDescription, Spannable spannable, int i, boolean z) {
        boolean isDarkTheme = ThemeUtil.isDarkTheme(context);
        int iconResource = updateDescription.getIconResource();
        int darkTint = isDarkTheme ? updateDescription.getDarkTint() : updateDescription.getLightTint();
        if (darkTint != 0) {
            i = darkTint;
        }
        if (iconResource == 0) {
            return new SpannableString(spannable);
        }
        Drawable requireDrawable = ContextUtil.requireDrawable(context, iconResource);
        requireDrawable.setBounds(0, 0, requireDrawable.getIntrinsicWidth(), requireDrawable.getIntrinsicHeight());
        requireDrawable.setColorFilter(i, PorterDuff.Mode.SRC_ATOP);
        InsetDrawable insetDrawable = new InsetDrawable(requireDrawable, 0, z ? ViewUtil.dpToPx(2) : 0, 0, 0);
        insetDrawable.setBounds(0, 0, requireDrawable.getIntrinsicWidth(), insetDrawable.getIntrinsicHeight());
        ColorDrawable colorDrawable = new ColorDrawable(0);
        colorDrawable.setBounds(0, 0, ViewUtil.dpToPx(8), requireDrawable.getIntrinsicHeight());
        return new SpannableString(SpanUtil.color(i, new SpannableStringBuilder().append(SpanUtil.buildImageSpan(requireDrawable)).append(SpanUtil.buildImageSpan(colorDrawable)).append((CharSequence) spannable)));
    }
}
