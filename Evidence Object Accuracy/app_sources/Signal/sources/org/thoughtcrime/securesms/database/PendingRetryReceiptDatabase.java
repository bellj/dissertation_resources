package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.thoughtcrime.securesms.database.model.PendingRetryReceiptModel;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class PendingRetryReceiptDatabase extends Database {
    private static final String AUTHOR;
    public static final String CREATE_TABLE;
    private static final String DEVICE;
    private static final String ID;
    private static final String RECEIVED_TIMESTAMP;
    private static final String SENT_TIMESTAMP;
    public static final String TABLE_NAME;
    private static final String THREAD_ID;

    public PendingRetryReceiptDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public PendingRetryReceiptModel insert(RecipientId recipientId, int i, long j, long j2, long j3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AUTHOR, recipientId.serialize());
        contentValues.put("device", Integer.valueOf(i));
        contentValues.put("sent_timestamp", Long.valueOf(j));
        contentValues.put(RECEIVED_TIMESTAMP, Long.valueOf(j2));
        contentValues.put("thread_id", Long.valueOf(j3));
        return new PendingRetryReceiptModel(this.databaseHelper.getSignalWritableDatabase().insertWithOnConflict(TABLE_NAME, null, contentValues, 5), recipientId, i, j, j2, j3);
    }

    public List<PendingRetryReceiptModel> getAll() {
        LinkedList linkedList = new LinkedList();
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, null, null, null, null, null);
        while (query.moveToNext()) {
            try {
                linkedList.add(fromCursor(query));
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return linkedList;
    }

    public void delete(PendingRetryReceiptModel pendingRetryReceiptModel) {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, "_id = ?", SqlUtil.buildArgs(pendingRetryReceiptModel.getId()));
    }

    private static PendingRetryReceiptModel fromCursor(Cursor cursor) {
        return new PendingRetryReceiptModel(CursorUtil.requireLong(cursor, "_id"), RecipientId.from(CursorUtil.requireString(cursor, AUTHOR)), CursorUtil.requireInt(cursor, "device"), CursorUtil.requireLong(cursor, "sent_timestamp"), CursorUtil.requireLong(cursor, RECEIVED_TIMESTAMP), CursorUtil.requireLong(cursor, "thread_id"));
    }
}
