package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;

/* loaded from: classes4.dex */
public interface GroupCallUpdateDetailsOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    String getEraId();

    ByteString getEraIdBytes();

    String getInCallUuids(int i);

    ByteString getInCallUuidsBytes(int i);

    int getInCallUuidsCount();

    List<String> getInCallUuidsList();

    boolean getIsCallFull();

    long getStartedCallTimestamp();

    String getStartedCallUuid();

    ByteString getStartedCallUuidBytes();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
