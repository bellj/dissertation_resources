package org.thoughtcrime.securesms.database.loaders;

import android.content.Context;
import org.thoughtcrime.securesms.util.AbstractCursorLoader;

/* loaded from: classes4.dex */
public abstract class MediaLoader extends AbstractCursorLoader {

    /* loaded from: classes4.dex */
    public enum MediaType {
        GALLERY,
        DOCUMENT,
        AUDIO,
        ALL
    }

    public MediaLoader(Context context) {
        super(context);
    }
}
