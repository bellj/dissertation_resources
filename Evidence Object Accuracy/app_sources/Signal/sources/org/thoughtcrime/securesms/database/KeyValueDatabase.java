package org.thoughtcrime.securesms.database;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import net.zetetic.database.sqlcipher.SQLiteOpenHelper;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.DatabaseSecret;
import org.thoughtcrime.securesms.crypto.DatabaseSecretProvider;
import org.thoughtcrime.securesms.keyvalue.KeyValueDataSet;
import org.thoughtcrime.securesms.keyvalue.KeyValuePersistentStorage;

/* loaded from: classes4.dex */
public class KeyValueDatabase extends SQLiteOpenHelper implements SignalDatabaseOpenHelper, KeyValuePersistentStorage {
    private static final String CREATE_TABLE;
    private static final String DATABASE_NAME;
    private static final int DATABASE_VERSION;
    private static final String ID;
    private static final String KEY;
    private static final String TABLE_NAME;
    private static final String TAG = Log.tag(KeyValueDatabase.class);
    private static final String TYPE;
    private static final String VALUE;
    private static volatile KeyValueDatabase instance;
    private final Application application;

    public static KeyValueDatabase getInstance(Application application) {
        if (instance == null) {
            synchronized (KeyValueDatabase.class) {
                if (instance == null) {
                    SqlCipherLibraryLoader.load();
                    instance = new KeyValueDatabase(application, DatabaseSecretProvider.getOrCreateDatabaseSecret(application));
                    instance.setWriteAheadLoggingEnabled(true);
                }
            }
        }
        return instance;
    }

    public static boolean exists(Context context) {
        return context.getDatabasePath(DATABASE_NAME).exists();
    }

    private KeyValueDatabase(Application application, DatabaseSecret databaseSecret) {
        super(application, DATABASE_NAME, databaseSecret.asString(), (SQLiteDatabase.CursorFactory) null, 1, 0, new SqlCipherErrorHandler(DATABASE_NAME), new SqlCipherDatabaseHook());
        this.application = application;
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        String str = TAG;
        Log.i(str, "onCreate()");
        sQLiteDatabase.execSQL(CREATE_TABLE);
        if (SignalDatabase.hasTable(TABLE_NAME)) {
            Log.i(str, "Found old key_value table. Migrating data.");
            migrateDataFromPreviousDatabase(SignalDatabase.getRawDatabase(), sQLiteDatabase);
        }
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        String str = TAG;
        Log.i(str, "onUpgrade(" + i + ", " + i2 + ")");
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        Log.i(TAG, "onOpen()");
        sQLiteDatabase.setForeignKeyConstraintsEnabled(true);
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.database.KeyValueDatabase$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                KeyValueDatabase.lambda$onOpen$0();
            }
        });
    }

    public static /* synthetic */ void lambda$onOpen$0() {
        if (SignalDatabase.hasTable(TABLE_NAME)) {
            Log.i(TAG, "Dropping original key_value table from the main database.");
            SignalDatabase.getRawDatabase().execSQL("DROP TABLE key_value");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x009c  */
    @Override // org.thoughtcrime.securesms.keyvalue.KeyValuePersistentStorage
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.thoughtcrime.securesms.keyvalue.KeyValueDataSet getDataSet() {
        /*
            r9 = this;
            org.thoughtcrime.securesms.keyvalue.KeyValueDataSet r0 = new org.thoughtcrime.securesms.keyvalue.KeyValueDataSet
            r0.<init>()
            net.zetetic.database.sqlcipher.SQLiteDatabase r1 = r9.getReadableDatabase()
            java.lang.String r2 = "key_value"
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8)
        L_0x0015:
            if (r1 == 0) goto L_0x009a
            boolean r2 = r1.moveToNext()     // Catch: all -> 0x0090
            if (r2 == 0) goto L_0x009a
            java.lang.String r2 = "type"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch: all -> 0x0090
            int r2 = r1.getInt(r2)     // Catch: all -> 0x0090
            org.thoughtcrime.securesms.database.KeyValueDatabase$Type r2 = org.thoughtcrime.securesms.database.KeyValueDatabase.Type.fromId(r2)     // Catch: all -> 0x0090
            java.lang.String r3 = "key"
            int r3 = r1.getColumnIndexOrThrow(r3)     // Catch: all -> 0x0090
            java.lang.String r3 = r1.getString(r3)     // Catch: all -> 0x0090
            int[] r4 = org.thoughtcrime.securesms.database.KeyValueDatabase.AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$KeyValueDatabase$Type     // Catch: all -> 0x0090
            int r2 = r2.ordinal()     // Catch: all -> 0x0090
            r2 = r4[r2]     // Catch: all -> 0x0090
            java.lang.String r4 = "value"
            switch(r2) {
                case 1: goto L_0x0084;
                case 2: goto L_0x0073;
                case 3: goto L_0x0067;
                case 4: goto L_0x005b;
                case 5: goto L_0x004f;
                case 6: goto L_0x0043;
                default: goto L_0x0042;
            }
        L_0x0042:
            goto L_0x0015
        L_0x0043:
            int r2 = r1.getColumnIndexOrThrow(r4)     // Catch: all -> 0x0090
            java.lang.String r2 = r1.getString(r2)     // Catch: all -> 0x0090
            r0.putString(r3, r2)     // Catch: all -> 0x0090
            goto L_0x0015
        L_0x004f:
            int r2 = r1.getColumnIndexOrThrow(r4)     // Catch: all -> 0x0090
            long r4 = r1.getLong(r2)     // Catch: all -> 0x0090
            r0.putLong(r3, r4)     // Catch: all -> 0x0090
            goto L_0x0015
        L_0x005b:
            int r2 = r1.getColumnIndexOrThrow(r4)     // Catch: all -> 0x0090
            int r2 = r1.getInt(r2)     // Catch: all -> 0x0090
            r0.putInteger(r3, r2)     // Catch: all -> 0x0090
            goto L_0x0015
        L_0x0067:
            int r2 = r1.getColumnIndexOrThrow(r4)     // Catch: all -> 0x0090
            float r2 = r1.getFloat(r2)     // Catch: all -> 0x0090
            r0.putFloat(r3, r2)     // Catch: all -> 0x0090
            goto L_0x0015
        L_0x0073:
            int r2 = r1.getColumnIndexOrThrow(r4)     // Catch: all -> 0x0090
            int r2 = r1.getInt(r2)     // Catch: all -> 0x0090
            r4 = 1
            if (r2 != r4) goto L_0x007f
            goto L_0x0080
        L_0x007f:
            r4 = 0
        L_0x0080:
            r0.putBoolean(r3, r4)     // Catch: all -> 0x0090
            goto L_0x0015
        L_0x0084:
            int r2 = r1.getColumnIndexOrThrow(r4)     // Catch: all -> 0x0090
            byte[] r2 = r1.getBlob(r2)     // Catch: all -> 0x0090
            r0.putBlob(r3, r2)     // Catch: all -> 0x0090
            goto L_0x0015
        L_0x0090:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x0095
            goto L_0x0099
        L_0x0095:
            r1 = move-exception
            r0.addSuppressed(r1)
        L_0x0099:
            throw r0
        L_0x009a:
            if (r1 == 0) goto L_0x009f
            r1.close()
        L_0x009f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.KeyValueDatabase.getDataSet():org.thoughtcrime.securesms.keyvalue.KeyValueDataSet");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.database.KeyValueDatabase$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$KeyValueDatabase$Type;

        static {
            int[] iArr = new int[Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$KeyValueDatabase$Type = iArr;
            try {
                iArr[Type.BLOB.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$KeyValueDatabase$Type[Type.BOOLEAN.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$KeyValueDatabase$Type[Type.FLOAT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$KeyValueDatabase$Type[Type.INTEGER.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$KeyValueDatabase$Type[Type.LONG.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$KeyValueDatabase$Type[Type.STRING.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValuePersistentStorage
    public void writeDataSet(KeyValueDataSet keyValueDataSet, Collection<String> collection) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (Map.Entry<String, Object> entry : keyValueDataSet.getValues().entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                Class type = keyValueDataSet.getType(key);
                ContentValues contentValues = new ContentValues(3);
                contentValues.put(KEY, key);
                if (type == byte[].class) {
                    contentValues.put("value", (byte[]) value);
                    contentValues.put("type", Integer.valueOf(Type.BLOB.getId()));
                } else if (type == Boolean.class) {
                    contentValues.put("value", Boolean.valueOf(((Boolean) value).booleanValue()));
                    contentValues.put("type", Integer.valueOf(Type.BOOLEAN.getId()));
                } else if (type == Float.class) {
                    contentValues.put("value", Float.valueOf(((Float) value).floatValue()));
                    contentValues.put("type", Integer.valueOf(Type.FLOAT.getId()));
                } else if (type == Integer.class) {
                    contentValues.put("value", Integer.valueOf(((Integer) value).intValue()));
                    contentValues.put("type", Integer.valueOf(Type.INTEGER.getId()));
                } else if (type == Long.class) {
                    contentValues.put("value", Long.valueOf(((Long) value).longValue()));
                    contentValues.put("type", Integer.valueOf(Type.LONG.getId()));
                } else if (type == String.class) {
                    contentValues.put("value", (String) value);
                    contentValues.put("type", Integer.valueOf(Type.STRING.getId()));
                } else {
                    throw new AssertionError("Unknown type: " + type);
                }
                writableDatabase.insertWithOnConflict(TABLE_NAME, null, contentValues, 5);
            }
            Iterator<String> it = collection.iterator();
            while (it.hasNext()) {
                writableDatabase.delete(TABLE_NAME, "key = ?", new String[]{it.next()});
            }
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    @Override // org.thoughtcrime.securesms.database.SignalDatabaseOpenHelper
    public SQLiteDatabase getSqlCipherDatabase() {
        return getWritableDatabase();
    }

    private static void migrateDataFromPreviousDatabase(SQLiteDatabase sQLiteDatabase, SQLiteDatabase sQLiteDatabase2) {
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM key_value", (String[]) null);
        while (rawQuery.moveToNext()) {
            try {
                int requireInt = CursorUtil.requireInt(rawQuery, "type");
                ContentValues contentValues = new ContentValues();
                contentValues.put(KEY, CursorUtil.requireString(rawQuery, KEY));
                contentValues.put("type", Integer.valueOf(requireInt));
                if (requireInt == 0) {
                    contentValues.put("value", CursorUtil.requireBlob(rawQuery, "value"));
                } else if (requireInt == 1) {
                    contentValues.put("value", Boolean.valueOf(CursorUtil.requireBoolean(rawQuery, "value")));
                } else if (requireInt == 2) {
                    contentValues.put("value", Float.valueOf(CursorUtil.requireFloat(rawQuery, "value")));
                } else if (requireInt == 3) {
                    contentValues.put("value", Integer.valueOf(CursorUtil.requireInt(rawQuery, "value")));
                } else if (requireInt == 4) {
                    contentValues.put("value", Long.valueOf(CursorUtil.requireLong(rawQuery, "value")));
                } else if (requireInt == 5) {
                    contentValues.put("value", CursorUtil.requireString(rawQuery, "value"));
                }
                sQLiteDatabase2.insert(TABLE_NAME, (String) null, contentValues);
            } catch (Throwable th) {
                if (rawQuery != null) {
                    try {
                        rawQuery.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        rawQuery.close();
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public enum Type {
        BLOB(0),
        BOOLEAN(1),
        FLOAT(2),
        INTEGER(3),
        LONG(4),
        STRING(5);
        
        final int id;

        Type(int i) {
            this.id = i;
        }

        public int getId() {
            return this.id;
        }

        public static Type fromId(int i) {
            return values()[i];
        }
    }
}
