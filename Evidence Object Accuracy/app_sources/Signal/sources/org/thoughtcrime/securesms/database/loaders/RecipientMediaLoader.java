package org.thoughtcrime.securesms.database.loaders;

import android.content.Context;
import android.database.Cursor;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.loaders.MediaLoader;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class RecipientMediaLoader extends MediaLoader {
    private final MediaLoader.MediaType mediaType;
    private final RecipientId recipientId;
    private final MediaDatabase.Sorting sorting;

    public RecipientMediaLoader(Context context, RecipientId recipientId, MediaLoader.MediaType mediaType, MediaDatabase.Sorting sorting) {
        super(context);
        this.recipientId = recipientId;
        this.mediaType = mediaType;
        this.sorting = sorting;
    }

    @Override // org.thoughtcrime.securesms.util.AbstractCursorLoader
    public Cursor getCursor() {
        RecipientId recipientId = this.recipientId;
        if (recipientId == null || recipientId.isUnknown()) {
            return null;
        }
        return ThreadMediaLoader.createThreadMediaCursor(this.context, SignalDatabase.threads().getOrCreateThreadIdFor(Recipient.resolved(this.recipientId)), this.mediaType, this.sorting);
    }
}
