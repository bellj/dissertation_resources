package org.thoughtcrime.securesms.database;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.database.model.PendingRetryReceiptModel;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* compiled from: PendingRetryReceiptCache.kt */
@Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u001aB\u0011\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\bJ\b\u0010\u000e\u001a\u00020\fH\u0002J\u0018\u0010\u000f\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\b\u0010\u0014\u001a\u0004\u0018\u00010\bJ.\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0019\u001a\u00020\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/database/PendingRetryReceiptCache;", "", "database", "Lorg/thoughtcrime/securesms/database/PendingRetryReceiptDatabase;", "(Lorg/thoughtcrime/securesms/database/PendingRetryReceiptDatabase;)V", "pendingRetries", "", "Lorg/thoughtcrime/securesms/database/PendingRetryReceiptCache$RemoteMessageId;", "Lorg/thoughtcrime/securesms/database/model/PendingRetryReceiptModel;", "populated", "", "delete", "", "model", "ensurePopulated", "get", "author", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "sentTimestamp", "", "getOldest", "insert", "authorDevice", "", "receivedTimestamp", "threadId", "RemoteMessageId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PendingRetryReceiptCache {
    private final PendingRetryReceiptDatabase database;
    private final Map<RemoteMessageId, PendingRetryReceiptModel> pendingRetries;
    private boolean populated;

    public PendingRetryReceiptCache() {
        this(null, 1, null);
    }

    public PendingRetryReceiptCache(PendingRetryReceiptDatabase pendingRetryReceiptDatabase) {
        Intrinsics.checkNotNullParameter(pendingRetryReceiptDatabase, "database");
        this.database = pendingRetryReceiptDatabase;
        this.pendingRetries = new HashMap();
    }

    public /* synthetic */ PendingRetryReceiptCache(PendingRetryReceiptDatabase pendingRetryReceiptDatabase, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? SignalDatabase.Companion.pendingRetryReceipts() : pendingRetryReceiptDatabase);
    }

    public final void insert(RecipientId recipientId, int i, long j, long j2, long j3) {
        Intrinsics.checkNotNullParameter(recipientId, "author");
        if (FeatureFlags.retryReceipts()) {
            ensurePopulated();
            synchronized (this.pendingRetries) {
                PendingRetryReceiptModel insert = this.database.insert(recipientId, i, j, j2, j3);
                Intrinsics.checkNotNullExpressionValue(insert, "database.insert(author, …eivedTimestamp, threadId)");
                this.pendingRetries.put(new RemoteMessageId(recipientId, j), insert);
                Unit unit = Unit.INSTANCE;
            }
        }
    }

    public final PendingRetryReceiptModel get(RecipientId recipientId, long j) {
        PendingRetryReceiptModel pendingRetryReceiptModel;
        Intrinsics.checkNotNullParameter(recipientId, "author");
        if (!FeatureFlags.retryReceipts()) {
            return null;
        }
        ensurePopulated();
        synchronized (this.pendingRetries) {
            pendingRetryReceiptModel = this.pendingRetries.get(new RemoteMessageId(recipientId, j));
        }
        return pendingRetryReceiptModel;
    }

    public final PendingRetryReceiptModel getOldest() {
        PendingRetryReceiptModel pendingRetryReceiptModel;
        Object obj = null;
        if (!FeatureFlags.retryReceipts()) {
            return null;
        }
        ensurePopulated();
        synchronized (this.pendingRetries) {
            Iterator<T> it = this.pendingRetries.values().iterator();
            if (it.hasNext()) {
                obj = it.next();
                if (it.hasNext()) {
                    long receivedTimestamp = ((PendingRetryReceiptModel) obj).getReceivedTimestamp();
                    do {
                        Object next = it.next();
                        long receivedTimestamp2 = ((PendingRetryReceiptModel) next).getReceivedTimestamp();
                        if (receivedTimestamp > receivedTimestamp2) {
                            obj = next;
                            receivedTimestamp = receivedTimestamp2;
                        }
                    } while (it.hasNext());
                }
            }
            pendingRetryReceiptModel = (PendingRetryReceiptModel) obj;
        }
        return pendingRetryReceiptModel;
    }

    public final void delete(PendingRetryReceiptModel pendingRetryReceiptModel) {
        Intrinsics.checkNotNullParameter(pendingRetryReceiptModel, "model");
        if (FeatureFlags.retryReceipts()) {
            ensurePopulated();
            synchronized (this.pendingRetries) {
                this.pendingRetries.remove(new RemoteMessageId(pendingRetryReceiptModel.getAuthor(), pendingRetryReceiptModel.getSentTimestamp()));
                this.database.delete(pendingRetryReceiptModel);
                Unit unit = Unit.INSTANCE;
            }
        }
    }

    private final void ensurePopulated() {
        if (!this.populated) {
            synchronized (this.pendingRetries) {
                if (!this.populated) {
                    List<PendingRetryReceiptModel> all = this.database.getAll();
                    Intrinsics.checkNotNullExpressionValue(all, "database.all");
                    for (PendingRetryReceiptModel pendingRetryReceiptModel : all) {
                        Map<RemoteMessageId, PendingRetryReceiptModel> map = this.pendingRetries;
                        RemoteMessageId remoteMessageId = new RemoteMessageId(pendingRetryReceiptModel.getAuthor(), pendingRetryReceiptModel.getSentTimestamp());
                        Intrinsics.checkNotNullExpressionValue(pendingRetryReceiptModel, "model");
                        map.put(remoteMessageId, pendingRetryReceiptModel);
                    }
                    this.populated = true;
                }
                Unit unit = Unit.INSTANCE;
            }
        }
    }

    /* compiled from: PendingRetryReceiptCache.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/database/PendingRetryReceiptCache$RemoteMessageId;", "", "author", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "sentTimestamp", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;J)V", "getAuthor", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getSentTimestamp", "()J", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RemoteMessageId {
        private final RecipientId author;
        private final long sentTimestamp;

        public static /* synthetic */ RemoteMessageId copy$default(RemoteMessageId remoteMessageId, RecipientId recipientId, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = remoteMessageId.author;
            }
            if ((i & 2) != 0) {
                j = remoteMessageId.sentTimestamp;
            }
            return remoteMessageId.copy(recipientId, j);
        }

        public final RecipientId component1() {
            return this.author;
        }

        public final long component2() {
            return this.sentTimestamp;
        }

        public final RemoteMessageId copy(RecipientId recipientId, long j) {
            Intrinsics.checkNotNullParameter(recipientId, "author");
            return new RemoteMessageId(recipientId, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof RemoteMessageId)) {
                return false;
            }
            RemoteMessageId remoteMessageId = (RemoteMessageId) obj;
            return Intrinsics.areEqual(this.author, remoteMessageId.author) && this.sentTimestamp == remoteMessageId.sentTimestamp;
        }

        public int hashCode() {
            return (this.author.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.sentTimestamp);
        }

        public String toString() {
            return "RemoteMessageId(author=" + this.author + ", sentTimestamp=" + this.sentTimestamp + ')';
        }

        public RemoteMessageId(RecipientId recipientId, long j) {
            Intrinsics.checkNotNullParameter(recipientId, "author");
            this.author = recipientId;
            this.sentTimestamp = j;
        }

        public final RecipientId getAuthor() {
            return this.author;
        }

        public final long getSentTimestamp() {
            return this.sentTimestamp;
        }
    }
}
