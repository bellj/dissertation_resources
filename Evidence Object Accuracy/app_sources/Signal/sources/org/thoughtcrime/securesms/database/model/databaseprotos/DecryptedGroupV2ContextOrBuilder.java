package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public interface DecryptedGroupV2ContextOrBuilder extends MessageLiteOrBuilder {
    DecryptedGroupChange getChange();

    SignalServiceProtos.GroupContextV2 getContext();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    DecryptedGroup getGroupState();

    DecryptedGroup getPreviousGroupState();

    boolean hasChange();

    boolean hasContext();

    boolean hasGroupState();

    boolean hasPreviousGroupState();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
