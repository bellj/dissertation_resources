package org.thoughtcrime.securesms.database.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.TextUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber$PhoneNumber;
import com.google.i18n.phonenumbers.ShortNumberInfo;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import org.signal.core.util.Hex;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.ClassicDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.MasterCipher;
import org.thoughtcrime.securesms.crypto.MasterSecret;
import org.thoughtcrime.securesms.crypto.MasterSecretUtil;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.PushDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SessionDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet;
import org.thoughtcrime.securesms.migrations.LegacyMigrationJob;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.phonenumbers.NumberUtil;
import org.thoughtcrime.securesms.util.DelimiterUtil;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* loaded from: classes4.dex */
public class ClassicOpenHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION;
    private static final int GROUP_RECEIPT_TRACKING;
    private static final int INTERNAL_DIRECTORY;
    private static final int INTERNAL_SYSTEM_DISPLAY_NAME;
    private static final int INTRODUCED_ARCHIVE_VERSION;
    private static final int INTRODUCED_COLOR_PREFERENCE_VERSION;
    private static final int INTRODUCED_CONVERSATION_LIST_STATUS_VERSION;
    private static final int INTRODUCED_CONVERSATION_LIST_THUMBNAILS_VERSION;
    private static final int INTRODUCED_DATE_SENT_VERSION;
    private static final int INTRODUCED_DB_OPTIMIZATIONS_VERSION;
    private static final int INTRODUCED_DELIVERY_RECEIPTS;
    private static final int INTRODUCED_DIGEST;
    private static final int INTRODUCED_DOCUMENTS;
    private static final int INTRODUCED_DRAFTS_VERSION;
    private static final int INTRODUCED_ENVELOPE_CONTENT_VERSION;
    private static final int INTRODUCED_EXPIRE_MESSAGES_VERSION;
    private static final int INTRODUCED_FAST_PREFLIGHT;
    private static final int INTRODUCED_GROUP_DATABASE_VERSION;
    private static final int INTRODUCED_IDENTITIES_VERSION;
    private static final int INTRODUCED_IDENTITY_COLUMN_VERSION;
    private static final int INTRODUCED_IDENTITY_TIMESTAMP;
    private static final int INTRODUCED_INDEXES_VERSION;
    private static final int INTRODUCED_INVITE_REMINDERS_VERSION;
    private static final int INTRODUCED_LAST_SEEN;
    private static final int INTRODUCED_MMS_BODY_VERSION;
    private static final int INTRODUCED_MMS_FROM_VERSION;
    private static final int INTRODUCED_NEW_TYPES_VERSION;
    private static final int INTRODUCED_NOTIFIED;
    private static final int INTRODUCED_PART_DATA_SIZE_VERSION;
    private static final int INTRODUCED_PUSH_DATABASE_VERSION;
    private static final int INTRODUCED_PUSH_FIX_VERSION;
    private static final int INTRODUCED_RECIPIENT_PREFS_DB;
    private static final int INTRODUCED_SUBSCRIPTION_ID_VERSION;
    private static final int INTRODUCED_THUMBNAILS_VERSION;
    private static final int INTRODUCED_TOFU_IDENTITY_VERSION;
    private static final int INTRODUCED_UNIQUE_PART_IDS_VERSION;
    private static final int INTRODUCED_VOICE_NOTES;
    private static final int MIGRATED_CONVERSATION_LIST_STATUS_VERSION;
    private static final int MORE_RECIPIENT_FIELDS;
    public static final String NAME;
    private static final int NO_MORE_CANONICAL_ADDRESS_DATABASE;
    private static final int NO_MORE_RECIPIENTS_PLURAL;
    private static final int PROFILES;
    private static final int PROFILE_SHARING_APPROVAL;
    private static final int READ_RECEIPTS;
    private static final int SANIFY_ATTACHMENT_DOWNLOAD;
    private static final String TAG = Log.tag(ClassicOpenHelper.class);
    private static final int UNREAD_COUNT_VERSION;
    private static final int UNSEEN_NUMBER_OFFER;
    private final Context context;

    public ClassicOpenHelper(Context context) {
        super(context, NAME, (SQLiteDatabase.CursorFactory) null, 47);
        this.context = context.getApplicationContext();
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL(SmsDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(MmsDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(AttachmentDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(ThreadDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(IdentityDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(DraftDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(PushDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(GroupDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(RecipientDatabase.CREATE_TABLE);
        sQLiteDatabase.execSQL(GroupReceiptDatabase.CREATE_TABLE);
        executeStatements(sQLiteDatabase, SmsDatabase.CREATE_INDEXS);
        executeStatements(sQLiteDatabase, MmsDatabase.CREATE_INDEXS);
        executeStatements(sQLiteDatabase, AttachmentDatabase.CREATE_INDEXS);
        executeStatements(sQLiteDatabase, ThreadDatabase.CREATE_INDEXS);
        executeStatements(sQLiteDatabase, DraftDatabase.CREATE_INDEXS);
        executeStatements(sQLiteDatabase, GroupDatabase.CREATE_INDEXS);
        executeStatements(sQLiteDatabase, GroupReceiptDatabase.CREATE_INDEXES);
    }

    public void onApplicationLevelUpgrade(Context context, MasterSecret masterSecret, int i, LegacyMigrationJob.DatabaseUpgradeListener databaseUpgradeListener) {
        String str;
        String str2;
        String str3;
        String str4;
        MasterSecret masterSecret2;
        Cursor cursor;
        Throwable th;
        File[] listFiles;
        Cursor cursor2;
        IOException e;
        long j;
        String string;
        boolean z;
        File file;
        InputStream inputStream;
        int i2;
        int i3;
        String str5;
        int i4;
        String str6;
        Cursor query;
        Cursor query2;
        int i5;
        String str7;
        MasterCipher masterCipher;
        InvalidMessageException e2;
        String str8;
        String str9;
        String str10;
        String str11;
        InvalidMessageException e3;
        String[] strArr;
        String[] strArr2;
        StringBuilder sb;
        LegacyMigrationJob.DatabaseUpgradeListener databaseUpgradeListener2 = databaseUpgradeListener;
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        String str12 = "body";
        String str13 = "type";
        if (i < 46) {
            String str14 = "?TextSecureKeyExchange";
            String str15 = "";
            String str16 = "_id";
            MasterCipher masterCipher2 = new MasterCipher(masterSecret);
            String str17 = "?TextSecureKeyExchangs";
            Cursor query3 = writableDatabase.query("sms", new String[]{"COUNT(*)"}, "type & -2147483648 != 0", null, null, null, null);
            if (query3 == null || !query3.moveToFirst()) {
                i2 = 0;
            } else {
                int i6 = query3.getInt(0);
                query3.close();
                i2 = i6;
            }
            Cursor query4 = writableDatabase.query(ThreadDatabase.TABLE_NAME, new String[]{"COUNT(*)"}, "snippet_type & -2147483648 != 0", null, null, null, null);
            if (query4 == null || !query4.moveToFirst()) {
                i3 = 0;
            } else {
                i3 = query4.getInt(0);
                query4.close();
            }
            String str18 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Upgrade count: ");
            int i7 = i2 + i3;
            sb2.append(i7);
            Log.i(str18, sb2.toString());
            Cursor cursor3 = null;
            int i8 = 0;
            while (true) {
                Log.i(TAG, "Looping SMS cursor...");
                if (cursor3 != null) {
                    cursor3.close();
                }
                str5 = ",";
                str3 = str16;
                int i9 = i8;
                i4 = i7;
                str6 = str17;
                String str19 = str13;
                query = writableDatabase.query("sms", new String[]{str16, str13, str12}, "type & -2147483648 != 0", null, null, null, "_id", i8 + ",500");
                while (query != null && query.moveToNext()) {
                    databaseUpgradeListener2.setProgress(query.getPosition() + i9, i4);
                    try {
                    } catch (InvalidMessageException e4) {
                        e3 = e4;
                        str9 = str19;
                    }
                    try {
                        String decryptBody = masterCipher2.decryptBody(query.getString(query.getColumnIndexOrThrow(str12)));
                        long j2 = query.getLong(query.getColumnIndexOrThrow(str19));
                        long j3 = query.getLong(query.getColumnIndexOrThrow(str3));
                        i4 = i4;
                        try {
                            if (decryptBody.startsWith(str14)) {
                                str14 = str14;
                                try {
                                    long j4 = j2 | 32768;
                                    str9 = str19;
                                    try {
                                        strArr2 = new String[3];
                                        strArr2[0] = masterCipher2.encryptBody(decryptBody.substring(22));
                                        sb = new StringBuilder();
                                        sb.append(j4);
                                    } catch (InvalidMessageException e5) {
                                        e3 = e5;
                                        masterCipher2 = masterCipher2;
                                        str8 = str12;
                                        str10 = str6;
                                        str11 = str15;
                                        Log.w(TAG, e3);
                                        str15 = str11;
                                        i9 = i9;
                                        str6 = str10;
                                        str19 = str9;
                                        str12 = str8;
                                    }
                                    try {
                                        sb.append(str15);
                                        strArr2[1] = sb.toString();
                                        strArr2[2] = j3 + str15;
                                        writableDatabase.execSQL("UPDATE sms SET body = ?, type = ? WHERE _id = ?", strArr2);
                                        str11 = str15;
                                        masterCipher2 = masterCipher2;
                                        str8 = str12;
                                        str10 = str6;
                                    } catch (InvalidMessageException e6) {
                                        e3 = e6;
                                        str11 = str15;
                                        masterCipher2 = masterCipher2;
                                        str8 = str12;
                                        str10 = str6;
                                        Log.w(TAG, e3);
                                        str15 = str11;
                                        i9 = i9;
                                        str6 = str10;
                                        str19 = str9;
                                        str12 = str8;
                                    }
                                } catch (InvalidMessageException e7) {
                                    e3 = e7;
                                    str9 = str19;
                                    masterCipher2 = masterCipher2;
                                    str8 = str12;
                                    str10 = str6;
                                    str11 = str15;
                                    Log.w(TAG, e3);
                                    str15 = str11;
                                    i9 = i9;
                                    str6 = str10;
                                    str19 = str9;
                                    str12 = str8;
                                }
                            } else {
                                str9 = str19;
                                str14 = str14;
                                str11 = str15;
                                try {
                                    if (decryptBody.startsWith("?TextSecureKeyExchangd")) {
                                        try {
                                            long j5 = j2 | 40960;
                                            str8 = str12;
                                            try {
                                                strArr = new String[3];
                                            } catch (InvalidMessageException e8) {
                                                e3 = e8;
                                                masterCipher2 = masterCipher2;
                                                str10 = str6;
                                                Log.w(TAG, e3);
                                                str15 = str11;
                                                i9 = i9;
                                                str6 = str10;
                                                str19 = str9;
                                                str12 = str8;
                                            }
                                            try {
                                                strArr[0] = masterCipher2.encryptBody(decryptBody.substring(22));
                                                strArr[1] = j5 + str11;
                                                strArr[2] = j3 + str11;
                                                writableDatabase.execSQL("UPDATE sms SET body = ?, type = ? WHERE _id = ?", strArr);
                                                masterCipher2 = masterCipher2;
                                                str10 = str6;
                                            } catch (InvalidMessageException e9) {
                                                e3 = e9;
                                                masterCipher2 = masterCipher2;
                                                str10 = str6;
                                                Log.w(TAG, e3);
                                                str15 = str11;
                                                i9 = i9;
                                                str6 = str10;
                                                str19 = str9;
                                                str12 = str8;
                                            }
                                        } catch (InvalidMessageException e10) {
                                            e3 = e10;
                                            str8 = str12;
                                        }
                                    } else {
                                        str8 = str12;
                                        str10 = str6;
                                        try {
                                            if (decryptBody.startsWith(str10)) {
                                                String encryptBody = masterCipher2.encryptBody(decryptBody.substring(22));
                                                masterCipher2 = masterCipher2;
                                                try {
                                                    writableDatabase.execSQL("UPDATE sms SET body = ?, type = ? WHERE _id = ?", new String[]{encryptBody, (j2 | 49152) + str11, j3 + str11});
                                                } catch (InvalidMessageException e11) {
                                                    e3 = e11;
                                                    Log.w(TAG, e3);
                                                    str15 = str11;
                                                    i9 = i9;
                                                    str6 = str10;
                                                    str19 = str9;
                                                    str12 = str8;
                                                }
                                            } else {
                                                masterCipher2 = masterCipher2;
                                            }
                                        } catch (InvalidMessageException e12) {
                                            e3 = e12;
                                            masterCipher2 = masterCipher2;
                                            Log.w(TAG, e3);
                                            str15 = str11;
                                            i9 = i9;
                                            str6 = str10;
                                            str19 = str9;
                                            str12 = str8;
                                        }
                                    }
                                } catch (InvalidMessageException e13) {
                                    e3 = e13;
                                    masterCipher2 = masterCipher2;
                                    str8 = str12;
                                    str10 = str6;
                                    Log.w(TAG, e3);
                                    str15 = str11;
                                    i9 = i9;
                                    str6 = str10;
                                    str19 = str9;
                                    str12 = str8;
                                }
                            }
                        } catch (InvalidMessageException e14) {
                            e3 = e14;
                            str9 = str19;
                            masterCipher2 = masterCipher2;
                            str14 = str14;
                        }
                    } catch (InvalidMessageException e15) {
                        e3 = e15;
                        str9 = str19;
                        masterCipher2 = masterCipher2;
                        i4 = i4;
                        str8 = str12;
                        str10 = str6;
                        str11 = str15;
                        Log.w(TAG, e3);
                        str15 = str11;
                        i9 = i9;
                        str6 = str10;
                        str19 = str9;
                        str12 = str8;
                    }
                    str15 = str11;
                    i9 = i9;
                    str6 = str10;
                    str19 = str9;
                    str12 = str8;
                }
                str2 = str19;
                str = str12;
                str4 = str15;
                int i10 = i9 + 500;
                if (query == null || query.getCount() <= 0) {
                    break;
                }
                i8 = i10;
                str15 = str4;
                str16 = str3;
                cursor3 = query;
                str17 = str6;
                str13 = str2;
                str12 = str;
                i7 = i4;
            }
            Cursor cursor4 = null;
            int i11 = 0;
            while (true) {
                Log.i(TAG, "Looping thread cursor...");
                if (cursor4 != null) {
                    cursor4.close();
                }
                String str20 = "snippet";
                String str21 = ThreadDatabase.SNIPPET_TYPE;
                MasterCipher masterCipher3 = masterCipher2;
                int i12 = i4;
                String str22 = str14;
                query2 = writableDatabase.query(ThreadDatabase.TABLE_NAME, new String[]{str3, ThreadDatabase.SNIPPET_TYPE, "snippet"}, "snippet_type & -2147483648 != 0", null, null, null, "_id", i11 + str5 + 500);
                while (query2 != null && query2.moveToNext()) {
                    databaseUpgradeListener2.setProgress(i2 + query2.getPosition(), i12);
                    try {
                        String string2 = query2.getString(query2.getColumnIndexOrThrow(str20));
                        try {
                            long j6 = query2.getLong(query2.getColumnIndexOrThrow(str21));
                            long j7 = query2.getLong(query2.getColumnIndexOrThrow(str3));
                            if (!TextUtils.isEmpty(string2)) {
                                masterCipher = masterCipher3;
                                try {
                                    string2 = masterCipher.decryptBody(string2);
                                } catch (InvalidMessageException e16) {
                                    e2 = e16;
                                    i5 = i12;
                                    str20 = str20;
                                    str21 = str21;
                                    str7 = str22;
                                    Log.w(TAG, e2);
                                    masterCipher3 = masterCipher;
                                    str22 = str7;
                                    i12 = i5;
                                }
                            } else {
                                masterCipher = masterCipher3;
                            }
                            i5 = i12;
                            try {
                                str7 = str22;
                                if (string2.startsWith(str22)) {
                                    str20 = str20;
                                    try {
                                        str21 = str21;
                                        try {
                                            writableDatabase.execSQL("UPDATE thread SET snippet = ?, snippet_type = ? WHERE _id = ?", new String[]{masterCipher.encryptBody(string2.substring(22)), (j6 | 32768) + str4, j7 + str4});
                                        } catch (InvalidMessageException e17) {
                                            e2 = e17;
                                            Log.w(TAG, e2);
                                            masterCipher3 = masterCipher;
                                            str22 = str7;
                                            i12 = i5;
                                        }
                                    } catch (InvalidMessageException e18) {
                                        e2 = e18;
                                        str21 = str21;
                                        Log.w(TAG, e2);
                                        masterCipher3 = masterCipher;
                                        str22 = str7;
                                        i12 = i5;
                                    }
                                } else {
                                    str20 = str20;
                                    str21 = str21;
                                    if (string2.startsWith("?TextSecureKeyExchangd")) {
                                        writableDatabase.execSQL("UPDATE thread SET snippet = ?, snippet_type = ? WHERE _id = ?", new String[]{masterCipher.encryptBody(string2.substring(22)), (j6 | 40960) + str4, j7 + str4});
                                    } else if (string2.startsWith(str6)) {
                                        writableDatabase.execSQL("UPDATE thread SET snippet = ?, snippet_type = ? WHERE _id = ?", new String[]{masterCipher.encryptBody(string2.substring(22)), (j6 | 49152) + str4, j7 + str4});
                                    }
                                }
                            } catch (InvalidMessageException e19) {
                                e2 = e19;
                                str7 = str22;
                                str20 = str20;
                            }
                        } catch (InvalidMessageException e20) {
                            e2 = e20;
                            i5 = i12;
                            str20 = str20;
                            str21 = str21;
                            masterCipher = masterCipher3;
                            str7 = str22;
                            Log.w(TAG, e2);
                            masterCipher3 = masterCipher;
                            str22 = str7;
                            i12 = i5;
                        }
                    } catch (InvalidMessageException e21) {
                        e2 = e21;
                        i5 = i12;
                        str20 = str20;
                    }
                    masterCipher3 = masterCipher;
                    str22 = str7;
                    i12 = i5;
                }
                i4 = i12;
                str14 = str22;
                i11 += 500;
                if (query2 == null || query2.getCount() <= 0) {
                    break;
                }
                cursor4 = query2;
                masterCipher2 = masterCipher3;
                query = query;
                str5 = str5;
            }
            if (query != null) {
                query.close();
            }
            if (query2 != null) {
                query2.close();
            }
        } else {
            str4 = "";
            str3 = "_id";
            str2 = str13;
            str = str12;
        }
        if (i < 46) {
            String str23 = TAG;
            Log.i(str23, "Update MMS bodies...");
            masterSecret2 = masterSecret;
            MasterCipher masterCipher4 = new MasterCipher(masterSecret2);
            Cursor query5 = writableDatabase.query("mms", new String[]{str3}, "msg_box & 2147483648 != 0", null, null, null, null);
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Got MMS rows: ");
            sb3.append(query5 == null ? "null" : Integer.valueOf(query5.getCount()));
            Log.i(str23, sb3.toString());
            while (query5 != null && query5.moveToNext()) {
                databaseUpgradeListener2.setProgress(query5.getPosition(), query5.getCount());
                long j8 = query5.getLong(query5.getColumnIndexOrThrow(str3));
                String str24 = "encrypted";
                String str25 = AttachmentDatabase.DATA;
                String str26 = "ct";
                Cursor query6 = writableDatabase.query(AttachmentDatabase.TABLE_NAME, new String[]{str3, "ct", AttachmentDatabase.DATA, "encrypted"}, "mid = ?", new String[]{j8 + str4}, null, null, null);
                int i13 = 0;
                String str27 = null;
                while (query6 != null && query6.moveToNext()) {
                    String string3 = query6.getString(query6.getColumnIndexOrThrow(str26));
                    if (MediaUtil.isTextType(string3)) {
                        try {
                            j = query6.getLong(query6.getColumnIndexOrThrow(str3));
                            try {
                                string = query6.getString(query6.getColumnIndexOrThrow(str25));
                                try {
                                    cursor2 = query6;
                                    z = query6.getInt(query6.getColumnIndexOrThrow(str24)) == 1;
                                } catch (IOException e22) {
                                    e = e22;
                                    cursor2 = query6;
                                }
                            } catch (IOException e23) {
                                e = e23;
                                cursor2 = query6;
                                str26 = str26;
                                str25 = str25;
                            }
                        } catch (IOException e24) {
                            e = e24;
                            cursor2 = query6;
                            str26 = str26;
                        }
                        try {
                            file = new File(string);
                            str26 = str26;
                        } catch (IOException e25) {
                            e = e25;
                            str26 = str26;
                            str25 = str25;
                            str24 = str24;
                            Log.w(TAG, e);
                            query6 = cursor2;
                        }
                        try {
                            str25 = str25;
                        } catch (IOException e26) {
                            e = e26;
                            str25 = str25;
                            str24 = str24;
                            Log.w(TAG, e);
                            query6 = cursor2;
                        }
                        try {
                            str24 = str24;
                            try {
                                AttachmentSecret attachmentSecret = new AttachmentSecret(masterSecret.getEncryptionKey().getEncoded(), masterSecret.getMacKey().getEncoded(), null);
                                if (z) {
                                    inputStream = ClassicDecryptingPartInputStream.createFor(attachmentSecret, file);
                                } else {
                                    inputStream = new FileInputStream(file);
                                }
                                str27 = str27 == null ? StreamUtil.readFullyAsString(inputStream) : str27 + " " + StreamUtil.readFullyAsString(inputStream);
                                file.delete();
                                writableDatabase.delete(AttachmentDatabase.TABLE_NAME, "_id = ?", new String[]{j + str4});
                            } catch (IOException e27) {
                                e = e27;
                                Log.w(TAG, e);
                                query6 = cursor2;
                            }
                        } catch (IOException e28) {
                            e = e28;
                            str24 = str24;
                            Log.w(TAG, e);
                            query6 = cursor2;
                        }
                    } else {
                        cursor2 = query6;
                        str26 = str26;
                        if (MediaUtil.isAudioType(string3) || MediaUtil.isImageType(string3) || MediaUtil.isVideoType(string3)) {
                            i13++;
                        }
                    }
                    query6 = cursor2;
                }
                if (!TextUtils.isEmpty(str27)) {
                    str27 = masterCipher4.encryptBody(str27);
                    writableDatabase.execSQL("UPDATE mms SET body = ?, part_count = ? WHERE _id = ?", new String[]{str27, i13 + str4, j8 + str4});
                } else {
                    writableDatabase.execSQL("UPDATE mms SET part_count = ? WHERE _id = ?", new String[]{i13 + str4, j8 + str4});
                }
                Log.i(TAG, "Updated body: " + str27 + " and part_count: " + i13);
                databaseUpgradeListener2 = databaseUpgradeListener;
                query5 = query5;
            }
        } else {
            masterSecret2 = masterSecret;
        }
        if (i < 50) {
            File file2 = new File(context.getFilesDir() + File.separator + SessionDatabase.TABLE_NAME);
            if (file2.exists() && file2.isDirectory() && (listFiles = file2.listFiles()) != null) {
                for (File file3 : listFiles) {
                    String name = file3.getName();
                    if (name.matches("[0-9]+")) {
                        Long.parseLong(name);
                    }
                }
            }
        }
        if (i < 73 && !MasterSecretUtil.hasAsymmericMasterSecret(context)) {
            MasterSecretUtil.generateAsymmetricMasterSecret(context, masterSecret);
            MasterCipher masterCipher5 = new MasterCipher(masterSecret2);
            try {
                Cursor query7 = writableDatabase.query("sms", new String[]{str3, str, str2}, "type & ? == 0", new String[]{String.valueOf((long) MmsSmsColumns.Types.ENCRYPTION_MASK)}, null, null, null);
                while (query7.moveToNext()) {
                    try {
                        long j9 = query7.getLong(0);
                        String string4 = query7.getString(1);
                        long j10 = query7.getLong(2);
                        String encryptBody2 = masterCipher5.encryptBody(string4);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(str, encryptBody2);
                        contentValues.put(str2, Long.valueOf(j10 | -2147483648L));
                        writableDatabase.update("sms", contentValues, "_id = ?", new String[]{String.valueOf(j9)});
                    } catch (Throwable th2) {
                        th = th2;
                        cursor = query7;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
                query7.close();
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        }
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        ApplicationDependencies.getMessageNotifier().updateNotification(context);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        int i3;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        SQLiteDatabase sQLiteDatabase2;
        String str6;
        String str7;
        ClassicOpenHelper classicOpenHelper;
        String str8;
        String str9;
        String str10;
        String str11;
        String str12;
        Cursor query;
        int i4;
        String str13;
        String str14;
        String str15;
        File[] fileArr;
        SQLiteDatabase sQLiteDatabase3;
        NumberFormatException e;
        String str16;
        String str17;
        String str18;
        IOException e2;
        ContentValues contentValues;
        String str19;
        String str20;
        String str21;
        IOException e3;
        ContentValues contentValues2;
        String str22;
        String str23;
        IOException e4;
        LinkedList linkedList;
        ContentValues contentValues3;
        String str24;
        String str25;
        String str26;
        String str27;
        String str28;
        String str29;
        char c;
        SQLiteDatabase sQLiteDatabase4;
        SQLiteConstraintException e5;
        String str30;
        sQLiteDatabase.beginTransaction();
        if (i < 2) {
            sQLiteDatabase.execSQL("CREATE TABLE identities (_id INTEGER PRIMARY KEY, key TEXT UNIQUE, name TEXT UNIQUE, mac TEXT);");
        }
        if (i < 3) {
            executeStatements(sQLiteDatabase, new String[]{"CREATE INDEX IF NOT EXISTS sms_thread_id_index ON sms (thread_id);", "CREATE INDEX IF NOT EXISTS sms_read_index ON sms (read);", "CREATE INDEX IF NOT EXISTS sms_read_and_thread_id_index ON sms (read,thread_id);", "CREATE INDEX IF NOT EXISTS sms_type_index ON sms (type);"});
            executeStatements(sQLiteDatabase, new String[]{"CREATE INDEX IF NOT EXISTS mms_thread_id_index ON mms (thread_id);", "CREATE INDEX IF NOT EXISTS mms_read_index ON mms (read);", "CREATE INDEX IF NOT EXISTS mms_read_and_thread_id_index ON mms (read,thread_id);", "CREATE INDEX IF NOT EXISTS mms_message_box_index ON mms (msg_box);"});
            executeStatements(sQLiteDatabase, new String[]{"CREATE INDEX IF NOT EXISTS part_mms_id_index ON part (mid);"});
            executeStatements(sQLiteDatabase, new String[]{"CREATE INDEX IF NOT EXISTS thread_recipient_ids_index ON thread (recipient_ids);"});
            executeStatements(sQLiteDatabase, new String[]{"CREATE INDEX IF NOT EXISTS mms_addresses_mms_id_index ON mms_addresses (mms_id);"});
        }
        if (i < 4) {
            sQLiteDatabase.execSQL("ALTER TABLE sms ADD COLUMN date_sent INTEGER;");
            sQLiteDatabase.execSQL("UPDATE sms SET date_sent = date;");
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN date_received INTEGER;");
            sQLiteDatabase.execSQL("UPDATE mms SET date_received = date;");
        }
        if (i < 5) {
            sQLiteDatabase.execSQL(DraftDatabase.CREATE_TABLE);
            executeStatements(sQLiteDatabase, new String[]{"CREATE INDEX IF NOT EXISTS draft_thread_index ON drafts (thread_id);"});
        }
        if (i < 6) {
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"20", SubscriptionLevels.BOOST_LEVEL});
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"21", "43"});
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"22", "4"});
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"23", "2"});
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"24", "5"});
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"8388629", "42"});
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"8388631", "44"});
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"8388628", "45"});
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"276824084", "46"});
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"20", "47"});
            sQLiteDatabase.execSQL("UPDATE sms SET type = ? WHERE type = ?", new String[]{"142606356", "48"});
            sQLiteDatabase.execSQL("UPDATE sms SET body = substr(body, ?), type = type | ? WHERE body LIKE ?", new String[]{"24", "2147483648", "?TextSecureLocalEncrypt%"});
            sQLiteDatabase.execSQL("UPDATE sms SET body = substr(body, ?), type = type | ? WHERE body LIKE ?", new String[]{"34", "1073741824", "?TextSecureAsymmetricLocalEncrypt%"});
            sQLiteDatabase.execSQL("UPDATE sms SET body = substr(body, ?), type = type | ? WHERE body LIKE ?", new String[]{"29", "545259520", "?TextSecureAsymmetricEncrypt%"});
            sQLiteDatabase.execSQL("UPDATE sms SET body = substr(body, ?), type = type | ? WHERE body LIKE ?", new String[]{"23", "32768", "?TextSecureKeyExchange%"});
            sQLiteDatabase.execSQL("UPDATE sms SET body = substr(body, ?), type = type | ? WHERE body LIKE ?", new String[]{"23", "40960", "?TextSecureKeyExchangd%"});
            sQLiteDatabase.execSQL("UPDATE sms SET body = substr(body, ?), type = type | ? WHERE body LIKE ?", new String[]{"23", "49152", "?TextSecureKeyExchangs%"});
            sQLiteDatabase.execSQL("UPDATE mms SET msg_box = ? WHERE msg_box = ?", new String[]{"2147483668", SubscriptionLevels.BOOST_LEVEL});
            sQLiteDatabase.execSQL("UPDATE mms SET msg_box = ? WHERE msg_box = ?", new String[]{"2147483671", "2"});
            sQLiteDatabase.execSQL("UPDATE mms SET msg_box = ? WHERE msg_box = ?", new String[]{"2147483669", "4"});
            sQLiteDatabase.execSQL("UPDATE mms SET msg_box = ? WHERE msg_box = ?", new String[]{"2147483672", "12"});
            sQLiteDatabase.execSQL("UPDATE mms SET msg_box = ? WHERE msg_box = ?", new String[]{"2155872277", "5"});
            sQLiteDatabase.execSQL("UPDATE mms SET msg_box = ? WHERE msg_box = ?", new String[]{"2155872279", "6"});
            sQLiteDatabase.execSQL("UPDATE mms SET msg_box = ? WHERE msg_box = ?", new String[]{"545259540", "7"});
            sQLiteDatabase.execSQL("UPDATE mms SET msg_box = ? WHERE msg_box = ?", new String[]{"2155872276", "8"});
            sQLiteDatabase.execSQL("UPDATE mms SET msg_box = ? WHERE msg_box = ?", new String[]{"142606356", "9"});
            sQLiteDatabase.execSQL("UPDATE mms SET msg_box = ? WHERE msg_box = ?", new String[]{"276824084", "10"});
            sQLiteDatabase.execSQL("ALTER TABLE thread ADD COLUMN snippet_type INTEGER;");
            sQLiteDatabase.execSQL("UPDATE thread SET snippet = substr(snippet, ?), snippet_type = ? WHERE snippet LIKE ?", new String[]{"24", "2147483648", "?TextSecureLocalEncrypt%"});
            sQLiteDatabase.execSQL("UPDATE thread SET snippet = substr(snippet, ?), snippet_type = ? WHERE snippet LIKE ?", new String[]{"34", "1073741824", "?TextSecureAsymmetricLocalEncrypt%"});
            sQLiteDatabase.execSQL("UPDATE thread SET snippet = substr(snippet, ?), snippet_type = ? WHERE snippet LIKE ?", new String[]{"29", "545259520", "?TextSecureAsymmetricEncrypt%"});
            sQLiteDatabase.execSQL("UPDATE thread SET snippet = substr(snippet, ?), snippet_type = ? WHERE snippet LIKE ?", new String[]{"23", "32768", "?TextSecureKeyExchange%"});
            sQLiteDatabase.execSQL("UPDATE thread SET snippet = substr(snippet, ?), snippet_type = ? WHERE snippet LIKE ?", new String[]{"23", "49152", "?TextSecureKeyExchangs%"});
            sQLiteDatabase.execSQL("UPDATE thread SET snippet = substr(snippet, ?), snippet_type = ? WHERE snippet LIKE ?", new String[]{"23", "40960", "?TextSecureKeyExchangd%"});
        }
        if (i < 7) {
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN body TEXT");
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN part_count INTEGER");
        }
        if (i < 8) {
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN address TEXT");
            i3 = 1;
            Cursor query2 = sQLiteDatabase.query("mms_addresses", null, "type = ?", new String[]{"137"}, null, null, null);
            while (query2 != null && query2.moveToNext()) {
                long j = query2.getLong(query2.getColumnIndexOrThrow(GroupReceiptDatabase.MMS_ID));
                String string = query2.getString(query2.getColumnIndexOrThrow("address"));
                if (!TextUtils.isEmpty(string)) {
                    sQLiteDatabase.execSQL("UPDATE mms SET address = ? WHERE _id = ?", new String[]{string, j + ""});
                }
            }
            if (query2 != null) {
                query2.close();
            }
        } else {
            i3 = 1;
        }
        if (i < 9) {
            sQLiteDatabase.execSQL("DROP TABLE identities");
            sQLiteDatabase.execSQL("CREATE TABLE identities (_id INTEGER PRIMARY KEY, recipient INTEGER UNIQUE, key TEXT, mac TEXT);");
        }
        if (i < 10) {
            sQLiteDatabase.execSQL("CREATE TABLE push (_id INTEGER PRIMARY KEY, type INTEGER, source TEXT, destinations TEXT, body TEXT, TIMESTAMP INTEGER);");
            sQLiteDatabase.execSQL("ALTER TABLE part ADD COLUMN pending_push INTEGER;");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS pending_push_index ON part (pending_push);");
        }
        if (i < 11) {
            sQLiteDatabase.execSQL("CREATE TABLE groups (_id INTEGER PRIMARY KEY, group_id TEXT, title TEXT, members TEXT, avatar BLOB, avatar_id INTEGER, avatar_key BLOB, avatar_content_type TEXT, avatar_relay TEXT, timestamp INTEGER, active INTEGER DEFAULT 1);");
            sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS group_id_index ON groups (GROUP_ID);");
            sQLiteDatabase.execSQL("ALTER TABLE push ADD COLUMN device_id INTEGER DEFAULT 1;");
            sQLiteDatabase.execSQL("ALTER TABLE sms ADD COLUMN address_device_id INTEGER DEFAULT 1;");
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN address_device_id INTEGER DEFAULT 1;");
        }
        if (i < 12) {
            sQLiteDatabase.execSQL("CREATE TEMPORARY table push_backup (_id INTEGER PRIMARY KEY, type INTEGER, source, TEXT, destinations TEXT, body TEXT, timestamp INTEGER, device_id INTEGER DEFAULT 1);");
            sQLiteDatabase.execSQL("INSERT INTO push_backup(_id, type, source, body, timestamp, device_id) SELECT _id, type, source, body, timestamp, device_id FROM push;");
            sQLiteDatabase.execSQL("DROP TABLE push");
            sQLiteDatabase.execSQL("CREATE TABLE push (_id INTEGER PRIMARY KEY, type INTEGER, source TEXT, body TEXT, timestamp INTEGER, device_id INTEGER DEFAULT 1);");
            sQLiteDatabase.execSQL("INSERT INTO push (_id, type, source, body, timestamp, device_id) SELECT _id, type, source, body, timestamp, device_id FROM push_backup;");
            sQLiteDatabase.execSQL("DROP TABLE push_backup;");
        }
        if (i < 13) {
            sQLiteDatabase.execSQL("ALTER TABLE sms ADD COLUMN delivery_receipt_count INTEGER DEFAULT 0;");
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN delivery_receipt_count INTEGER DEFAULT 0;");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS sms_date_sent_index ON sms (date_sent);");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS mms_date_sent_index ON mms (date);");
        }
        if (i < 14) {
            sQLiteDatabase.execSQL("ALTER TABLE part ADD COLUMN data_size INTEGER DEFAULT 0;");
        }
        if (i < 15) {
            sQLiteDatabase.execSQL("ALTER TABLE part ADD COLUMN thumbnail TEXT;");
            sQLiteDatabase.execSQL("ALTER TABLE part ADD COLUMN aspect_ratio REAL;");
        }
        if (i < 16) {
            sQLiteDatabase.execSQL("ALTER TABLE sms ADD COLUMN mismatched_identities TEXT");
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN mismatched_identities TEXT");
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN network_failures TEXT");
        }
        if (i < 17) {
            sQLiteDatabase.execSQL("ALTER TABLE part ADD COLUMN unique_id INTEGER NOT NULL DEFAULT 0");
        }
        if (i < 18) {
            sQLiteDatabase.execSQL("CREATE TABLE recipient_preferences (_id INTEGER PRIMARY KEY, recipient_ids TEXT UNIQUE, block INTEGER DEFAULT 0, notification TEXT DEFAULT NULL, vibrate INTEGER DEFAULT 0, mute_until INTEGER DEFAULT 0)");
        }
        if (i < 19) {
            sQLiteDatabase.execSQL("ALTER TABLE push ADD COLUMN content TEXT");
        }
        if (i < 20) {
            sQLiteDatabase.execSQL("ALTER TABLE recipient_preferences ADD COLUMN color TEXT DEFAULT NULL");
        }
        if (i < 21) {
            sQLiteDatabase.execSQL("UPDATE mms SET date_received = (date_received * 1000), date = (date * 1000);");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS sms_thread_date_index ON sms (thread_id, date);");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS mms_thread_date_index ON mms (thread_id, date_received);");
        }
        if (i < 22) {
            sQLiteDatabase.execSQL("ALTER TABLE recipient_preferences ADD COLUMN seen_invite_reminder INTEGER DEFAULT 0");
        }
        if (i < 23) {
            sQLiteDatabase.execSQL("ALTER TABLE thread ADD COLUMN snippet_uri TEXT DEFAULT NULL");
        }
        if (i < 24) {
            sQLiteDatabase.execSQL("ALTER TABLE thread ADD COLUMN archived INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS archived_index ON thread (archived)");
        }
        if (i < 25) {
            sQLiteDatabase.execSQL("ALTER TABLE thread ADD COLUMN status INTEGER DEFAULT -1");
            sQLiteDatabase.execSQL("ALTER TABLE thread ADD COLUMN delivery_receipt_count INTEGER DEFAULT 0");
        }
        if (i < 26) {
            str = "_id";
            Cursor query3 = sQLiteDatabase.query(ThreadDatabase.TABLE_NAME, new String[]{"_id"}, null, null, null, null, null);
            while (query3 != null && query3.moveToNext()) {
                long j2 = query3.getLong(query3.getColumnIndexOrThrow(str));
                String[] strArr = new String[i3];
                strArr[0] = j2 + "";
                Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT DISTINCT date AS date_received, status, delivery_receipt_count FROM sms WHERE (thread_id = ?1) UNION ALL SELECT DISTINCT date_received, -1 AS status, delivery_receipt_count FROM mms WHERE (thread_id = ?1) ORDER BY date_received DESC LIMIT 1", strArr);
                if (rawQuery != null && rawQuery.moveToNext()) {
                    int i5 = rawQuery.getInt(rawQuery.getColumnIndexOrThrow("status"));
                    int i6 = rawQuery.getInt(rawQuery.getColumnIndexOrThrow("delivery_receipt_count"));
                    String[] strArr2 = new String[3];
                    strArr2[0] = i5 + "";
                    strArr2[i3] = i6 + "";
                    strArr2[2] = j2 + "";
                    sQLiteDatabase.execSQL("UPDATE thread SET status = ?, delivery_receipt_count = ? WHERE _id = ?", strArr2);
                }
            }
        } else {
            str = "_id";
        }
        if (i < 27) {
            sQLiteDatabase.execSQL("ALTER TABLE recipient_preferences ADD COLUMN default_subscription_id INTEGER DEFAULT -1");
            sQLiteDatabase.execSQL("ALTER TABLE sms ADD COLUMN subscription_id INTEGER DEFAULT -1");
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN subscription_id INTEGER DEFAULT -1");
        }
        if (i < 28) {
            sQLiteDatabase.execSQL("ALTER TABLE recipient_preferences ADD COLUMN expire_messages INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE sms ADD COLUMN expires_in INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN expires_in INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE sms ADD COLUMN expire_started INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN expire_started INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE thread ADD COLUMN expires_in INTEGER DEFAULT 0");
        }
        if (i < 29) {
            sQLiteDatabase.execSQL("ALTER TABLE thread ADD COLUMN last_seen INTEGER DEFAULT 0");
        }
        if (i < 30) {
            sQLiteDatabase.execSQL("ALTER TABLE part ADD COLUMN digest BLOB");
            sQLiteDatabase.execSQL("ALTER TABLE groups ADD COLUMN avatar_digest BLOB");
        }
        if (i < 31) {
            sQLiteDatabase.execSQL("ALTER TABLE sms ADD COLUMN notified INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE mms ADD COLUMN notified INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("DROP INDEX sms_read_and_thread_id_index");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS sms_read_and_notified_and_thread_id_index ON sms(read,notified,thread_id)");
            sQLiteDatabase.execSQL("DROP INDEX mms_read_and_thread_id_index");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS mms_read_and_notified_and_thread_id_index ON mms(read,notified,thread_id)");
        }
        if (i < 32) {
            sQLiteDatabase.execSQL("ALTER TABLE part ADD COLUMN file_name TEXT");
        }
        if (i < 33) {
            sQLiteDatabase.execSQL("ALTER TABLE part ADD COLUMN fast_preflight_id TEXT");
        }
        if (i < 34) {
            sQLiteDatabase.execSQL("ALTER TABLE part ADD COLUMN voice_note INTEGER DEFAULT 0");
        }
        if (i < 35) {
            sQLiteDatabase.execSQL("ALTER TABLE identities ADD COLUMN timestamp INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE identities ADD COLUMN first_use INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE identities ADD COLUMN nonblocking_approval INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE identities ADD COLUMN verified INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("DROP INDEX archived_index");
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS archived_count_index ON thread (archived, message_count)");
        }
        if (i < 36) {
            sQLiteDatabase.execSQL("UPDATE part SET pending_push = '2' WHERE pending_push = '1'");
        }
        String str31 = "_id = ?";
        if (i >= 37 || !isValidNumber(TextSecurePreferences.getStringPreference(this.context, "pref_local_number", null))) {
            str7 = "address";
            classicOpenHelper = this;
            str2 = "recipient_preferences";
            str4 = "pref_local_number";
            str3 = "recipient_ids";
            sQLiteDatabase2 = sQLiteDatabase;
            str5 = "mms";
            str6 = str;
        } else {
            String str32 = "address";
            SQLiteDatabase readableDatabase = new SQLiteOpenHelper(this.context, "canonical_address.db", null, 1) { // from class: org.thoughtcrime.securesms.database.helpers.ClassicOpenHelper.1
                @Override // android.database.sqlite.SQLiteOpenHelper
                public void onUpgrade(SQLiteDatabase sQLiteDatabase5, int i7, int i8) {
                }

                @Override // android.database.sqlite.SQLiteOpenHelper
                public void onCreate(SQLiteDatabase sQLiteDatabase5) {
                    throw new AssertionError("No canonical address DB?");
                }
            }.getReadableDatabase();
            NumberMigrator numberMigrator = new NumberMigrator(TextSecurePreferences.getStringPreference(this.context, "pref_local_number", null));
            String str33 = "recipient_preferences";
            str4 = "pref_local_number";
            String str34 = "mms";
            String str35 = str31;
            Cursor query4 = sQLiteDatabase.query(ThreadDatabase.TABLE_NAME, new String[]{str, "recipient_ids"}, null, null, null, null, null);
            while (query4 != null && query4.moveToNext()) {
                int i7 = 0;
                long j3 = query4.getLong(0);
                String string2 = query4.getString(1);
                String[] split = string2.split(" ");
                String[] strArr3 = new String[split.length];
                while (i7 < split.length) {
                    Cursor query5 = readableDatabase.query("canonical_addresses", new String[]{str32}, "_id = ?", new String[]{split[i7]}, null, null, null);
                    if (query5 == null || !query5.moveToFirst()) {
                        str30 = str34;
                        if (TextUtils.isEmpty(split[i7]) || split[i7].equals("-1")) {
                            strArr3[i7] = "Unknown";
                        } else {
                            throw new AssertionError("Unable to resolve: " + split[i7] + ", recipientIdsList: '" + string2 + "'");
                        }
                    } else {
                        str30 = str34;
                        strArr3[i7] = DelimiterUtil.escape(numberMigrator.migrate(query5.getString(0)), ' ');
                    }
                    if (query5 != null) {
                        query5.close();
                    }
                    i7++;
                    str34 = str30;
                }
                Arrays.sort(strArr3);
                ContentValues contentValues4 = new ContentValues(1);
                contentValues4.put("recipient_ids", Util.join(strArr3, " "));
                sQLiteDatabase.update(ThreadDatabase.TABLE_NAME, contentValues4, str35, new String[]{String.valueOf(j3)});
                str34 = str34;
            }
            String str36 = str34;
            if (query4 != null) {
                query4.close();
            }
            sQLiteDatabase.execSQL("CREATE TABLE identities_migrated (_id INTEGER PRIMARY KEY, address TEXT UNIQUE, key TEXT, first_use INTEGER DEFAULT 0, timestamp INTEGER DEFAULT 0, verified INTEGER DEFAULT 0, nonblocking_approval INTEGER DEFAULT 0);");
            Cursor query6 = sQLiteDatabase.query("identities", new String[]{"_id, recipient, key, first_use, timestamp, verified, nonblocking_approval"}, null, null, null, null, null);
            while (query6 != null && query6.moveToNext()) {
                query6.getLong(0);
                long j4 = query6.getLong(1);
                String string3 = query6.getString(2);
                int i8 = query6.getInt(3);
                long j5 = query6.getLong(4);
                int i9 = query6.getInt(5);
                int i10 = query6.getInt(6);
                ContentValues contentValues5 = new ContentValues(6);
                Cursor query7 = readableDatabase.query("canonical_addresses", new String[]{str32}, "_id = ?", new String[]{String.valueOf(j4)}, null, null, null);
                if (query7 == null || !query7.moveToFirst()) {
                    throw new AssertionError("Unable to resolve: " + j4);
                }
                contentValues5.put(str32, numberMigrator.migrate(query7.getString(0)));
                contentValues5.put("key", string3);
                contentValues5.put("first_use", Integer.valueOf(i8));
                contentValues5.put("timestamp", Long.valueOf(j5));
                contentValues5.put("verified", Integer.valueOf(i9));
                contentValues5.put("nonblocking_approval", Integer.valueOf(i10));
                query7.close();
                sQLiteDatabase.insert("identities_migrated", null, contentValues5);
                str32 = str32;
                str35 = str35;
            }
            String str37 = str35;
            str7 = str32;
            if (query6 != null) {
                query6.close();
            }
            sQLiteDatabase.execSQL("DROP TABLE identities");
            sQLiteDatabase.execSQL("ALTER TABLE identities_migrated RENAME TO identities");
            int i11 = 0;
            Cursor query8 = sQLiteDatabase.query("recipient_preferences", new String[]{str, "recipient_ids"}, null, null, null, null, null);
            while (query8 != null && query8.moveToNext()) {
                long j6 = query8.getLong(i11);
                int i12 = 1;
                String string4 = query8.getString(1);
                String[] split2 = string4.split(" ");
                String[] strArr4 = new String[split2.length];
                int i13 = 0;
                while (i13 < split2.length) {
                    String[] strArr5 = new String[i12];
                    strArr5[i11] = split2[i13];
                    Cursor query9 = readableDatabase.query("canonical_addresses", new String[]{str7}, "_id = ?", strArr5, null, null, null);
                    if (query9 != null && query9.moveToFirst()) {
                        strArr4[i13] = DelimiterUtil.escape(numberMigrator.migrate(query9.getString(i11)), ' ');
                    } else if (TextUtils.isEmpty(split2[i13]) || split2[i13].equals("-1")) {
                        strArr4[i13] = "Unknown";
                    } else {
                        throw new AssertionError("Unable to resolve: " + split2[i13] + ", recipientIdsList: '" + string4 + "'");
                    }
                    if (query9 != null) {
                        query9.close();
                    }
                    i13++;
                    i11 = 0;
                    i12 = 1;
                }
                Arrays.sort(strArr4);
                ContentValues contentValues6 = new ContentValues(1);
                contentValues6.put("recipient_ids", Util.join(strArr4, " "));
                try {
                    sQLiteDatabase4 = sQLiteDatabase;
                    str29 = str37;
                    str28 = str33;
                    c = 0;
                } catch (SQLiteConstraintException e6) {
                    e5 = e6;
                    sQLiteDatabase4 = sQLiteDatabase;
                    str29 = str37;
                    str28 = str33;
                    c = 0;
                }
                try {
                    sQLiteDatabase4.update(str28, contentValues6, str29, new String[]{String.valueOf(j6)});
                } catch (SQLiteConstraintException e7) {
                    e5 = e7;
                    Log.w(TAG, e5);
                    String[] strArr6 = new String[1];
                    strArr6[c] = String.valueOf(j6);
                    sQLiteDatabase4.delete(str28, str29, strArr6);
                    str37 = str29;
                    str33 = str28;
                    i11 = 0;
                }
                str37 = str29;
                str33 = str28;
                i11 = 0;
            }
            sQLiteDatabase2 = sQLiteDatabase;
            if (query8 != null) {
                query8.close();
            }
            str6 = str;
            String str38 = str37;
            str3 = "recipient_ids";
            str2 = str33;
            Cursor query10 = sQLiteDatabase.query("sms", new String[]{str6, str7}, null, null, null, null, null);
            while (query10 != null && query10.moveToNext()) {
                long j7 = query10.getLong(0);
                String string5 = query10.getString(1);
                if (!TextUtils.isEmpty(string5)) {
                    ContentValues contentValues7 = new ContentValues(1);
                    contentValues7.put(str7, numberMigrator.migrate(string5));
                    str27 = str38;
                    sQLiteDatabase2.update("sms", contentValues7, str27, new String[]{String.valueOf(j7)});
                } else {
                    str27 = str38;
                }
                str38 = str27;
            }
            if (query10 != null) {
                query10.close();
            }
            String str39 = str38;
            char c2 = 0;
            Cursor query11 = sQLiteDatabase.query("mms", new String[]{str6, str7}, null, null, null, null, null);
            while (query11 != null && query11.moveToNext()) {
                long j8 = query11.getLong(0);
                String string6 = query11.getString(1);
                if (!TextUtils.isEmpty(string6)) {
                    ContentValues contentValues8 = new ContentValues(1);
                    contentValues8.put(str7, numberMigrator.migrate(string6));
                    str25 = str36;
                    str26 = str39;
                    sQLiteDatabase2.update(str25, contentValues8, str26, new String[]{String.valueOf(j8)});
                } else {
                    str25 = str36;
                    str26 = str39;
                }
                str39 = str26;
                str36 = str25;
            }
            if (query11 != null) {
                query11.close();
            }
            String str40 = str39;
            String str41 = str36;
            Cursor query12 = sQLiteDatabase.query("mms_addresses", new String[]{str6, str7}, null, null, null, null, null);
            while (query12 != null && query12.moveToNext()) {
                long j9 = query12.getLong(0);
                String string7 = query12.getString(1);
                if (TextUtils.isEmpty(string7) || "insert-address-token".equals(string7)) {
                    str24 = str40;
                } else {
                    ContentValues contentValues9 = new ContentValues(1);
                    contentValues9.put(str7, numberMigrator.migrate(string7));
                    str24 = str40;
                    sQLiteDatabase2.update("mms_addresses", contentValues9, str24, new String[]{String.valueOf(j9)});
                }
                str40 = str24;
            }
            if (query12 != null) {
                query12.close();
            }
            String[] strArr7 = {str6, MmsSmsColumns.MISMATCHED_IDENTITIES};
            String str42 = MmsSmsColumns.MISMATCHED_IDENTITIES;
            String str43 = str40;
            Cursor query13 = sQLiteDatabase.query("sms", strArr7, "mismatched_identities IS NOT NULL", null, null, null, null);
            while (query13 != null && query13.moveToNext()) {
                long j10 = query13.getLong(0);
                String string8 = query13.getString(1);
                if (!TextUtils.isEmpty(string8)) {
                    try {
                        linkedList = new LinkedList();
                        for (PreCanonicalAddressIdentityMismatchDocument preCanonicalAddressIdentityMismatchDocument : ((PreCanonicalAddressIdentityMismatchList) JsonUtils.fromJson(string8, PreCanonicalAddressIdentityMismatchList.class)).list) {
                            Cursor query14 = readableDatabase.query("canonical_addresses", new String[]{str7}, "_id = ?", new String[]{String.valueOf(preCanonicalAddressIdentityMismatchDocument.recipientId)}, null, null, null);
                            if (query14 == null || !query14.moveToFirst()) {
                                throw new AssertionError("Unable to resolve: " + preCanonicalAddressIdentityMismatchDocument.recipientId);
                                break;
                            }
                            linkedList.add(new PostCanonicalAddressIdentityMismatchDocument(numberMigrator.migrate(query14.getString(0)), preCanonicalAddressIdentityMismatchDocument.identityKey));
                            query14.close();
                        }
                        contentValues3 = new ContentValues(1);
                        str22 = str42;
                    } catch (IOException e8) {
                        e4 = e8;
                        str22 = str42;
                    }
                    try {
                        contentValues3.put(str22, JsonUtils.toJson(new PostCanonicalAddressIdentityMismatchList(linkedList)));
                        str23 = str43;
                    } catch (IOException e9) {
                        e4 = e9;
                        str23 = str43;
                        Log.w(TAG, e4);
                        str43 = str23;
                        str42 = str22;
                    }
                    try {
                        sQLiteDatabase2.update("sms", contentValues3, str23, new String[]{String.valueOf(j10)});
                    } catch (IOException e10) {
                        e4 = e10;
                        Log.w(TAG, e4);
                        str43 = str23;
                        str42 = str22;
                    }
                } else {
                    str22 = str42;
                    str23 = str43;
                }
                str43 = str23;
                str42 = str22;
            }
            if (query13 != null) {
                query13.close();
            }
            String str44 = str43;
            String str45 = str42;
            Cursor query15 = sQLiteDatabase.query("mms", new String[]{str6, str42}, "mismatched_identities IS NOT NULL", null, null, null, null);
            while (query15 != null && query15.moveToNext()) {
                long j11 = query15.getLong(0);
                String string9 = query15.getString(1);
                if (!TextUtils.isEmpty(string9)) {
                    try {
                        LinkedList linkedList2 = new LinkedList();
                        for (PreCanonicalAddressIdentityMismatchDocument preCanonicalAddressIdentityMismatchDocument2 : ((PreCanonicalAddressIdentityMismatchList) JsonUtils.fromJson(string9, PreCanonicalAddressIdentityMismatchList.class)).list) {
                            Cursor query16 = readableDatabase.query("canonical_addresses", new String[]{str7}, "_id = ?", new String[]{String.valueOf(preCanonicalAddressIdentityMismatchDocument2.recipientId)}, null, null, null);
                            if (query16 == null || !query16.moveToFirst()) {
                                throw new AssertionError("Unable to resolve: " + preCanonicalAddressIdentityMismatchDocument2.recipientId);
                                break;
                            }
                            linkedList2.add(new PostCanonicalAddressIdentityMismatchDocument(numberMigrator.migrate(query16.getString(0)), preCanonicalAddressIdentityMismatchDocument2.identityKey));
                            query16.close();
                        }
                        contentValues2 = new ContentValues(1);
                        str21 = str45;
                        try {
                            contentValues2.put(str21, JsonUtils.toJson(new PostCanonicalAddressIdentityMismatchList(linkedList2)));
                            str19 = str41;
                            str20 = str44;
                        } catch (IOException e11) {
                            e3 = e11;
                            str19 = str41;
                            str20 = str44;
                        }
                    } catch (IOException e12) {
                        e3 = e12;
                        str19 = str41;
                        str20 = str44;
                        str21 = str45;
                    }
                    try {
                        sQLiteDatabase2.update(str19, contentValues2, str20, new String[]{String.valueOf(j11)});
                    } catch (IOException e13) {
                        e3 = e13;
                        Log.w(TAG, e3);
                        str45 = str21;
                        str44 = str20;
                        str41 = str19;
                    }
                } else {
                    str19 = str41;
                    str20 = str44;
                    str21 = str45;
                }
                str45 = str21;
                str44 = str20;
                str41 = str19;
            }
            if (query15 != null) {
                query15.close();
            }
            String str46 = str44;
            String str47 = str41;
            Cursor query17 = sQLiteDatabase.query("mms", new String[]{str6, "network_failures"}, "network_failures IS NOT NULL", null, null, null, null);
            while (query17 != null && query17.moveToNext()) {
                long j12 = query17.getLong(0);
                String string10 = query17.getString(1);
                if (!TextUtils.isEmpty(string10)) {
                    try {
                        LinkedList linkedList3 = new LinkedList();
                        for (PreCanonicalAddressNetworkFailureDocument preCanonicalAddressNetworkFailureDocument : ((PreCanonicalAddressNetworkFailureList) JsonUtils.fromJson(string10, PreCanonicalAddressNetworkFailureList.class)).list) {
                            Cursor query18 = readableDatabase.query("canonical_addresses", new String[]{str7}, "_id = ?", new String[]{String.valueOf(preCanonicalAddressNetworkFailureDocument.recipientId)}, null, null, null);
                            if (query18 == null || !query18.moveToFirst()) {
                                throw new AssertionError("Unable to resolve: " + preCanonicalAddressNetworkFailureDocument.recipientId);
                                break;
                            }
                            linkedList3.add(new PostCanonicalAddressNetworkFailureDocument(numberMigrator.migrate(query18.getString(0))));
                            query18.close();
                        }
                        contentValues = new ContentValues(1);
                        contentValues.put("network_failures", JsonUtils.toJson(new PostCanonicalAddressNetworkFailureList(linkedList3)));
                        str18 = str46;
                        str17 = str47;
                    } catch (IOException e14) {
                        e2 = e14;
                        str18 = str46;
                        str17 = str47;
                    }
                    try {
                        sQLiteDatabase2.update(str17, contentValues, str18, new String[]{String.valueOf(j12)});
                    } catch (IOException e15) {
                        e2 = e15;
                        Log.w(TAG, e2);
                        str46 = str18;
                        str47 = str17;
                    }
                } else {
                    str18 = str46;
                    str17 = str47;
                }
                str46 = str18;
                str47 = str17;
            }
            str31 = str46;
            str5 = str47;
            classicOpenHelper = this;
            File file = new File(classicOpenHelper.context.getFilesDir(), "sessions-v2");
            if (file.exists() && file.isDirectory()) {
                File[] listFiles = file.listFiles();
                int length = listFiles.length;
                int i14 = 0;
                while (i14 < length) {
                    File file2 = listFiles[i14];
                    try {
                        String[] split3 = file2.getName().split("[.]");
                        long parseLong = Long.parseLong(split3[c2]);
                        int parseInt = split3.length > 1 ? Integer.parseInt(split3[1]) : 1;
                        Cursor query19 = readableDatabase.query("canonical_addresses", new String[]{str7}, "_id = ?", new String[]{String.valueOf(parseLong)}, null, null, null);
                        if (query19 == null || !query19.moveToNext()) {
                            sQLiteDatabase3 = readableDatabase;
                            fileArr = listFiles;
                        } else {
                            String string11 = query19.getString(0);
                            File parentFile = file2.getParentFile();
                            sQLiteDatabase3 = readableDatabase;
                            try {
                                StringBuilder sb = new StringBuilder();
                                sb.append(string11);
                                if (parseInt != 1) {
                                    StringBuilder sb2 = new StringBuilder();
                                    fileArr = listFiles;
                                    try {
                                        sb2.append(".");
                                        sb2.append(parseInt);
                                        str16 = sb2.toString();
                                    } catch (NumberFormatException e16) {
                                        e = e16;
                                        Log.w(TAG, e);
                                        i14++;
                                        readableDatabase = sQLiteDatabase3;
                                        listFiles = fileArr;
                                        c2 = 0;
                                    }
                                } else {
                                    fileArr = listFiles;
                                    str16 = "";
                                }
                                sb.append(str16);
                                File file3 = new File(parentFile, sb.toString());
                                if (!file2.renameTo(file3)) {
                                    Log.w(TAG, "Session rename failed: " + file3);
                                }
                            } catch (NumberFormatException e17) {
                                e = e17;
                                fileArr = listFiles;
                                Log.w(TAG, e);
                                i14++;
                                readableDatabase = sQLiteDatabase3;
                                listFiles = fileArr;
                                c2 = 0;
                            }
                        }
                        if (query19 != null) {
                            query19.close();
                        }
                    } catch (NumberFormatException e18) {
                        e = e18;
                        sQLiteDatabase3 = readableDatabase;
                    }
                    i14++;
                    readableDatabase = sQLiteDatabase3;
                    listFiles = fileArr;
                    c2 = 0;
                }
            }
        }
        if (i < 38) {
            sQLiteDatabase2.execSQL("ALTER TABLE groups ADD COLUMN mms INTEGER DEFAULT 0");
            str11 = str3;
            String str48 = str31;
            Cursor query20 = sQLiteDatabase.query(ThreadDatabase.TABLE_NAME, new String[]{str6, str11}, null, null, null, null, null);
            while (query20 != null && query20.moveToNext()) {
                long j13 = query20.getLong(0);
                String string12 = query20.getString(1);
                String[] split4 = DelimiterUtil.split(string12, ' ');
                if (split4.length == 1) {
                    ContentValues contentValues10 = new ContentValues();
                    contentValues10.put(str11, DelimiterUtil.unescape(string12, ' '));
                    str14 = str48;
                    sQLiteDatabase2.update(ThreadDatabase.TABLE_NAME, contentValues10, str14, new String[]{String.valueOf(j13)});
                    str15 = str2;
                } else {
                    str14 = str48;
                    byte[] bArr = new byte[16];
                    LinkedList linkedList4 = new LinkedList();
                    new SecureRandom().nextBytes(bArr);
                    int length2 = split4.length;
                    int i15 = 0;
                    while (i15 < length2) {
                        linkedList4.add(DelimiterUtil.escape(DelimiterUtil.unescape(split4[i15], ' '), ','));
                        i15++;
                        length2 = length2;
                        split4 = split4;
                    }
                    linkedList4.add(DelimiterUtil.escape(TextSecurePreferences.getStringPreference(classicOpenHelper.context, str4, null), ','));
                    Collections.sort(linkedList4);
                    String str49 = "__signal_mms_group__!" + Hex.toStringCondensed(bArr);
                    ContentValues contentValues11 = new ContentValues();
                    ContentValues contentValues12 = new ContentValues();
                    contentValues11.put("group_id", str49);
                    contentValues11.put("members", Util.join((Collection) linkedList4, ","));
                    contentValues11.put(str5, (Integer) 1);
                    contentValues12.put(str11, str49);
                    sQLiteDatabase2.insert(ChooseGroupStoryBottomSheet.RESULT_SET, null, contentValues11);
                    sQLiteDatabase2.update(ThreadDatabase.TABLE_NAME, contentValues12, str14, new String[]{String.valueOf(j13)});
                    str15 = str2;
                    sQLiteDatabase2.update(str15, contentValues12, "recipient_ids = ?", new String[]{string12});
                }
                str2 = str15;
                str48 = str14;
            }
            if (query20 != null) {
                query20.close();
            }
            String str50 = str2;
            str8 = str4;
            Cursor query21 = sQLiteDatabase.query("recipient_preferences", new String[]{str6, str11}, null, null, null, null, null);
            while (query21 != null && query21.moveToNext()) {
                long j14 = query21.getLong(0);
                String string13 = query21.getString(1);
                if (DelimiterUtil.split(string13, ' ').length == 1) {
                    ContentValues contentValues13 = new ContentValues();
                    contentValues13.put(str11, DelimiterUtil.unescape(string13, ' '));
                    str13 = str50;
                    sQLiteDatabase2.update(str13, contentValues13, str48, new String[]{String.valueOf(j14)});
                } else {
                    str13 = str50;
                    Log.w(TAG, "Found preferences for MMS thread that appears to be gone: " + string13);
                    sQLiteDatabase2.delete(str13, str48, new String[]{String.valueOf(j14)});
                }
                str50 = str13;
            }
            str9 = str50;
            if (query21 != null) {
                query21.close();
            }
            str10 = null;
            Cursor rawQuery2 = sQLiteDatabase2.rawQuery("SELECT mms._id, thread.recipient_ids FROM mms, thread WHERE mms.address IS NULL AND mms.thread_id = thread._id", null);
            while (rawQuery2 != null && rawQuery2.moveToNext()) {
                long j15 = rawQuery2.getLong(0);
                ContentValues contentValues14 = new ContentValues(1);
                contentValues14.put(str7, rawQuery2.getString(1));
                sQLiteDatabase2.update(str5, contentValues14, str48, new String[]{String.valueOf(j15)});
            }
            if (rawQuery2 != null) {
                rawQuery2.close();
            }
        } else {
            str8 = str4;
            str11 = str3;
            str9 = str2;
            str10 = null;
        }
        if (i < 39) {
            sQLiteDatabase2.execSQL("ALTER TABLE recipient_preferences ADD COLUMN registered INTEGER DEFAULT 0");
            if (isValidNumber(TextSecurePreferences.getStringPreference(classicOpenHelper.context, str8, str10))) {
                Cursor query22 = new OldDirectoryDatabaseHelper(classicOpenHelper.context).getWritableDatabase().query("directory", new String[]{"number", RecipientDatabase.REGISTERED}, null, null, null, null, null);
                while (query22 != null && query22.moveToNext()) {
                    String migrate = new NumberMigrator(TextSecurePreferences.getStringPreference(classicOpenHelper.context, str8, str10)).migrate(query22.getString(0));
                    ContentValues contentValues15 = new ContentValues(1);
                    contentValues15.put(RecipientDatabase.REGISTERED, Integer.valueOf(query22.getInt(1) == 1 ? 1 : 2));
                    if (sQLiteDatabase2.update(str9, contentValues15, "recipient_ids = ?", new String[]{migrate}) < 1) {
                        contentValues15.put(str11, migrate);
                        sQLiteDatabase2.insert(str9, str10, contentValues15);
                    }
                }
                if (query22 != null) {
                    query22.close();
                }
            }
        }
        if (i < 40) {
            sQLiteDatabase2.execSQL("ALTER TABLE recipient_preferences ADD COLUMN system_display_name TEXT DEFAULT NULL");
        }
        if (i < 41) {
            sQLiteDatabase2.execSQL("ALTER TABLE recipient_preferences ADD COLUMN profile_key TEXT DEFAULT NULL");
            sQLiteDatabase2.execSQL("ALTER TABLE recipient_preferences ADD COLUMN signal_profile_name TEXT DEFAULT NULL");
            sQLiteDatabase2.execSQL("ALTER TABLE recipient_preferences ADD COLUMN signal_profile_avatar TEXT DEFAULT NULL");
        }
        if (i < 42) {
            sQLiteDatabase2.execSQL("ALTER TABLE recipient_preferences ADD COLUMN profile_sharing_approval INTEGER DEFAULT 0");
        }
        if (i < 43) {
            sQLiteDatabase2.execSQL("ALTER TABLE thread ADD COLUMN has_sent INTEGER DEFAULT 0");
        }
        if (i < 44) {
            sQLiteDatabase2.execSQL("ALTER TABLE sms ADD COLUMN read_receipt_count INTEGER DEFAULT 0");
            sQLiteDatabase2.execSQL("ALTER TABLE mms ADD COLUMN read_receipt_count INTEGER DEFAULT 0");
            sQLiteDatabase2.execSQL("ALTER TABLE thread ADD COLUMN read_receipt_count INTEGER DEFAULT 0");
        }
        if (i < 45) {
            sQLiteDatabase2.execSQL("CREATE TABLE group_receipts (_id INTEGER PRIMARY KEY, mms_id  INTEGER, address TEXT, status INTEGER, timestamp INTEGER)");
            sQLiteDatabase2.execSQL("CREATE INDEX IF NOT EXISTS group_receipt_mms_id_index ON group_receipts (mms_id)");
        }
        if (i < 46) {
            sQLiteDatabase2.execSQL("ALTER TABLE thread ADD COLUMN unread_count INTEGER DEFAULT 0");
            str12 = str9;
            query = sQLiteDatabase.query(ThreadDatabase.TABLE_NAME, new String[]{str6}, "read = 0", null, null, null, null);
            while (query != null) {
                try {
                    if (!query.moveToNext()) {
                        break;
                    }
                    long j16 = query.getLong(0);
                    Cursor rawQuery3 = sQLiteDatabase2.rawQuery("SELECT COUNT(*) FROM sms WHERE thread_id = ? AND read = '0'", new String[]{String.valueOf(j16)});
                    if (rawQuery3 == null || !rawQuery3.moveToFirst()) {
                        i4 = 0;
                    } else {
                        i4 = rawQuery3.getInt(0) + 0;
                    }
                    if (rawQuery3 != null) {
                        rawQuery3.close();
                    }
                    Cursor rawQuery4 = sQLiteDatabase2.rawQuery("SELECT COUNT(*) FROM mms WHERE thread_id = ? AND read = '0'", new String[]{String.valueOf(j16)});
                    if (rawQuery4 != null && rawQuery4.moveToFirst()) {
                        i4 += rawQuery4.getInt(0);
                    }
                    if (rawQuery4 != null) {
                        rawQuery4.close();
                    }
                    sQLiteDatabase2.execSQL("UPDATE thread SET unread_count = ? WHERE _id = ?", new String[]{String.valueOf(i4), String.valueOf(j16)});
                } finally {
                }
            }
            if (query != null) {
                query.close();
            }
        } else {
            str12 = str9;
        }
        if (i < 47) {
            sQLiteDatabase2.execSQL("ALTER TABLE recipient_preferences ADD COLUMN system_contact_photo TEXT DEFAULT NULL");
            sQLiteDatabase2.execSQL("ALTER TABLE recipient_preferences ADD COLUMN system_phone_label TEXT DEFAULT NULL");
            sQLiteDatabase2.execSQL("ALTER TABLE recipient_preferences ADD COLUMN system_contact_uri TEXT DEFAULT NULL");
            if (Permissions.hasAny(classicOpenHelper.context, "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS")) {
                query = sQLiteDatabase.query("recipient_preferences", null, null, null, null, null, null);
                while (query != null) {
                    try {
                        if (!query.moveToNext()) {
                            break;
                        }
                        String string14 = query.getString(query.getColumnIndexOrThrow(str11));
                        if (!TextUtils.isEmpty(string14) && !GroupId.isEncodedGroup(string14) && !NumberUtil.isValidEmail(string14)) {
                            Cursor query23 = classicOpenHelper.context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(string14)), new String[]{"display_name", "lookup", "_id", "number", EmojiSearchDatabase.LABEL, "photo_uri"}, null, null, null);
                            if (query23 != null && query23.moveToFirst()) {
                                ContentValues contentValues16 = new ContentValues(3);
                                contentValues16.put("system_contact_photo", query23.getString(5));
                                contentValues16.put(RecipientDatabase.SYSTEM_PHONE_LABEL, query23.getString(4));
                                contentValues16.put("system_contact_uri", ContactsContract.Contacts.getLookupUri(query23.getLong(2), query23.getString(1)).toString());
                                sQLiteDatabase2.update(str12, contentValues16, "recipient_ids = ?", new String[]{string14});
                            }
                            if (query23 != null) {
                                query23.close();
                            }
                        }
                    } finally {
                    }
                }
                if (query != null) {
                    query.close();
                }
            }
        }
        sQLiteDatabase.setTransactionSuccessful();
        sQLiteDatabase.endTransaction();
    }

    private void executeStatements(SQLiteDatabase sQLiteDatabase, String[] strArr) {
        for (String str : strArr) {
            sQLiteDatabase.execSQL(str);
        }
    }

    /* loaded from: classes.dex */
    private static class PreCanonicalAddressIdentityMismatchList {
        @JsonProperty("m")
        private List<PreCanonicalAddressIdentityMismatchDocument> list;

        private PreCanonicalAddressIdentityMismatchList() {
        }
    }

    /* loaded from: classes.dex */
    private static class PostCanonicalAddressIdentityMismatchList {
        @JsonProperty("m")
        private List<PostCanonicalAddressIdentityMismatchDocument> list;

        public PostCanonicalAddressIdentityMismatchList(List<PostCanonicalAddressIdentityMismatchDocument> list) {
            this.list = list;
        }
    }

    /* loaded from: classes.dex */
    public static class PreCanonicalAddressIdentityMismatchDocument {
        @JsonProperty("k")
        private String identityKey;
        @JsonProperty("r")
        private long recipientId;

        private PreCanonicalAddressIdentityMismatchDocument() {
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes.dex */
    public static class PostCanonicalAddressIdentityMismatchDocument {
        @JsonProperty("a")
        private String address;
        @JsonProperty("k")
        private String identityKey;

        public PostCanonicalAddressIdentityMismatchDocument() {
        }

        public PostCanonicalAddressIdentityMismatchDocument(String str, String str2) {
            this.address = str;
            this.identityKey = str2;
        }
    }

    /* loaded from: classes.dex */
    private static class PreCanonicalAddressNetworkFailureList {
        @JsonProperty("l")
        private List<PreCanonicalAddressNetworkFailureDocument> list;

        private PreCanonicalAddressNetworkFailureList() {
        }
    }

    /* loaded from: classes.dex */
    private static class PostCanonicalAddressNetworkFailureList {
        @JsonProperty("l")
        private List<PostCanonicalAddressNetworkFailureDocument> list;

        public PostCanonicalAddressNetworkFailureList(List<PostCanonicalAddressNetworkFailureDocument> list) {
            this.list = list;
        }
    }

    /* loaded from: classes.dex */
    public static class PreCanonicalAddressNetworkFailureDocument {
        @JsonProperty("r")
        private long recipientId;

        private PreCanonicalAddressNetworkFailureDocument() {
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes.dex */
    public static class PostCanonicalAddressNetworkFailureDocument {
        @JsonProperty("a")
        private String address;

        public PostCanonicalAddressNetworkFailureDocument() {
        }

        public PostCanonicalAddressNetworkFailureDocument(String str) {
            this.address = str;
        }
    }

    private static boolean isValidNumber(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            PhoneNumberUtil.getInstance().parse(str, null);
            return true;
        } catch (NumberParseException unused) {
            return false;
        }
    }

    /* loaded from: classes4.dex */
    private static class NumberMigrator {
        private static final Set<String> SHORT_COUNTRIES = new HashSet<String>() { // from class: org.thoughtcrime.securesms.database.helpers.ClassicOpenHelper.NumberMigrator.1
            {
                add("NU");
                add("TK");
                add("NC");
                add("AC");
            }
        };
        private static final String TAG = Log.tag(NumberMigrator.class);
        private final Pattern ALPHA_PATTERN = Pattern.compile("[a-zA-Z]");
        private final String localCountryCode;
        private final Phonenumber$PhoneNumber localNumber;
        private final String localNumberString;
        private final PhoneNumberUtil phoneNumberUtil;

        public NumberMigrator(String str) {
            PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
            this.phoneNumberUtil = instance;
            try {
                this.localNumberString = str;
                Phonenumber$PhoneNumber parse = instance.parse(str, null);
                this.localNumber = parse;
                this.localCountryCode = instance.getRegionCodeForNumber(parse);
            } catch (NumberParseException e) {
                throw new AssertionError(e);
            }
        }

        public String migrate(String str) {
            if (str == null) {
                return "Unknown";
            }
            if (str.startsWith("__textsecure_group__!")) {
                return str;
            }
            if (this.ALPHA_PATTERN.matcher(str).find()) {
                return str.trim();
            }
            String replaceAll = str.replaceAll("[^0-9+]", "");
            if (replaceAll.length() == 0) {
                if (TextUtils.isEmpty(str.trim())) {
                    return "Unknown";
                }
                return str.trim();
            } else if (replaceAll.length() <= 6 && ("DE".equals(this.localCountryCode) || "FI".equals(this.localCountryCode) || "SK".equals(this.localCountryCode))) {
                return replaceAll;
            } else {
                if (replaceAll.length() <= 4 && !SHORT_COUNTRIES.contains(this.localCountryCode)) {
                    return replaceAll;
                }
                try {
                    Phonenumber$PhoneNumber parse = this.phoneNumberUtil.parse(replaceAll, this.localCountryCode);
                    if (ShortNumberInfo.getInstance().isPossibleShortNumberForRegion(parse, this.localCountryCode)) {
                        return replaceAll;
                    }
                    return this.phoneNumberUtil.format(parse, PhoneNumberUtil.PhoneNumberFormat.E164);
                } catch (NumberParseException e) {
                    Log.w(TAG, e);
                    if (replaceAll.charAt(0) == '+') {
                        return replaceAll;
                    }
                    String str2 = this.localNumberString;
                    if (str2.charAt(0) == '+') {
                        str2 = str2.substring(1);
                    }
                    if (str2.length() == replaceAll.length() || replaceAll.length() > str2.length()) {
                        return "+" + str;
                    }
                    int length = str2.length() - replaceAll.length();
                    return "+" + str2.substring(0, length) + replaceAll;
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    private static class OldDirectoryDatabaseHelper extends SQLiteOpenHelper {
        private static final String CREATE_TABLE;
        private static final String DATABASE_NAME;
        private static final int DATABASE_VERSION;
        private static final String ID;
        private static final int INTRODUCED_CHANGE_FROM_TOKEN_TO_E164_NUMBER;
        private static final int INTRODUCED_VIDEO_COLUMN;
        private static final int INTRODUCED_VOICE_COLUMN;
        private static final String NUMBER;
        private static final String REGISTERED;
        private static final String RELAY;
        private static final String TABLE_NAME;
        private static final String TIMESTAMP;
        private static final String VIDEO;
        private static final String VOICE;

        public OldDirectoryDatabaseHelper(Context context) {
            super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 5);
        }

        @Override // android.database.sqlite.SQLiteOpenHelper
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL(CREATE_TABLE);
        }

        @Override // android.database.sqlite.SQLiteOpenHelper
        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            if (i < 2) {
                sQLiteDatabase.execSQL("DROP TABLE directory;");
                sQLiteDatabase.execSQL("CREATE TABLE directory ( _id INTEGER PRIMARY KEY, number TEXT UNIQUE, registered INTEGER, relay TEXT, supports_sms INTEGER, timestamp INTEGER);");
            }
            if (i < 4) {
                sQLiteDatabase.execSQL("ALTER TABLE directory ADD COLUMN voice INTEGER;");
            }
            if (i < 5) {
                sQLiteDatabase.execSQL("ALTER TABLE directory ADD COLUMN video INTEGER;");
            }
        }
    }
}
