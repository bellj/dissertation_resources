package org.thoughtcrime.securesms.database;

import com.annimon.stream.Stream;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.mobilecoin.lib.KeyImage;
import com.mobilecoin.lib.Receipt;
import com.mobilecoin.lib.RistrettoPublic;
import com.mobilecoin.lib.Transaction;
import com.mobilecoin.lib.exceptions.SerializationException;
import java.util.List;
import org.thoughtcrime.securesms.payments.proto.PaymentMetaData;

/* loaded from: classes4.dex */
public final class PaymentMetaDataUtil {
    public static PaymentMetaData parseOrThrow(byte[] bArr) {
        try {
            return PaymentMetaData.parseFrom(bArr);
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalStateException(e);
        }
    }

    public static PaymentMetaData fromReceipt(byte[] bArr) throws SerializationException {
        PaymentMetaData.MobileCoinTxoIdentification.Builder newBuilder = PaymentMetaData.MobileCoinTxoIdentification.newBuilder();
        if (bArr != null) {
            addReceiptData(bArr, newBuilder);
        }
        return PaymentMetaData.newBuilder().setMobileCoinTxoIdentification(newBuilder).build();
    }

    public static PaymentMetaData fromKeysAndImages(List<ByteString> list, List<ByteString> list2) {
        PaymentMetaData.MobileCoinTxoIdentification.Builder newBuilder = PaymentMetaData.MobileCoinTxoIdentification.newBuilder();
        newBuilder.addAllKeyImages(list2);
        newBuilder.addAllPublicKey(list);
        return PaymentMetaData.newBuilder().setMobileCoinTxoIdentification(newBuilder).build();
    }

    public static PaymentMetaData fromReceiptAndTransaction(byte[] bArr, byte[] bArr2) throws SerializationException {
        PaymentMetaData.MobileCoinTxoIdentification.Builder newBuilder = PaymentMetaData.MobileCoinTxoIdentification.newBuilder();
        if (bArr2 != null) {
            addTransactionData(bArr2, newBuilder);
        } else if (bArr != null) {
            addReceiptData(bArr, newBuilder);
        }
        return PaymentMetaData.newBuilder().setMobileCoinTxoIdentification(newBuilder).build();
    }

    private static void addReceiptData(byte[] bArr, PaymentMetaData.MobileCoinTxoIdentification.Builder builder) throws SerializationException {
        addPublicKey(builder, Receipt.fromBytes(bArr).getPublicKey());
    }

    private static void addTransactionData(byte[] bArr, PaymentMetaData.MobileCoinTxoIdentification.Builder builder) throws SerializationException {
        Transaction fromBytes = Transaction.fromBytes(bArr);
        for (KeyImage keyImage : fromBytes.getKeyImages()) {
            builder.addKeyImages(ByteString.copyFrom(keyImage.getData()));
        }
        for (RistrettoPublic ristrettoPublic : fromBytes.getOutputPublicKeys()) {
            addPublicKey(builder, ristrettoPublic);
        }
    }

    private static void addPublicKey(PaymentMetaData.MobileCoinTxoIdentification.Builder builder, RistrettoPublic ristrettoPublic) {
        builder.addPublicKey(ByteString.copyFrom(ristrettoPublic.getKeyBytes()));
    }

    public static byte[] receiptPublic(PaymentMetaData paymentMetaData) {
        return ((ByteString) Stream.of(paymentMetaData.getMobileCoinTxoIdentification().getPublicKeyList()).single()).toByteArray();
    }
}
