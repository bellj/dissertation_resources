package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: DistributionListPartialRecord.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001b\u001a\u00020\tHÆ\u0003J\t\u0010\u001c\u001a\u00020\tHÆ\u0003J\t\u0010\u001d\u001a\u00020\fHÆ\u0003JE\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\fHÆ\u0001J\u0013\u0010\u001f\u001a\u00020\t2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010!\u001a\u00020\"HÖ\u0001J\t\u0010#\u001a\u00020$HÖ\u0001R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017¨\u0006%"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/DistributionListPartialRecord;", "", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "name", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "allowsReplies", "", "isUnknown", "privacyMode", "Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;Ljava/lang/CharSequence;Lorg/thoughtcrime/securesms/recipients/RecipientId;ZZLorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;)V", "getAllowsReplies", "()Z", "getId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getName", "()Ljava/lang/CharSequence;", "getPrivacyMode", "()Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DistributionListPartialRecord {
    private final boolean allowsReplies;
    private final DistributionListId id;
    private final boolean isUnknown;
    private final CharSequence name;
    private final DistributionListPrivacyMode privacyMode;
    private final RecipientId recipientId;

    public static /* synthetic */ DistributionListPartialRecord copy$default(DistributionListPartialRecord distributionListPartialRecord, DistributionListId distributionListId, CharSequence charSequence, RecipientId recipientId, boolean z, boolean z2, DistributionListPrivacyMode distributionListPrivacyMode, int i, Object obj) {
        if ((i & 1) != 0) {
            distributionListId = distributionListPartialRecord.id;
        }
        if ((i & 2) != 0) {
            charSequence = distributionListPartialRecord.name;
        }
        if ((i & 4) != 0) {
            recipientId = distributionListPartialRecord.recipientId;
        }
        if ((i & 8) != 0) {
            z = distributionListPartialRecord.allowsReplies;
        }
        if ((i & 16) != 0) {
            z2 = distributionListPartialRecord.isUnknown;
        }
        if ((i & 32) != 0) {
            distributionListPrivacyMode = distributionListPartialRecord.privacyMode;
        }
        return distributionListPartialRecord.copy(distributionListId, charSequence, recipientId, z, z2, distributionListPrivacyMode);
    }

    public final DistributionListId component1() {
        return this.id;
    }

    public final CharSequence component2() {
        return this.name;
    }

    public final RecipientId component3() {
        return this.recipientId;
    }

    public final boolean component4() {
        return this.allowsReplies;
    }

    public final boolean component5() {
        return this.isUnknown;
    }

    public final DistributionListPrivacyMode component6() {
        return this.privacyMode;
    }

    public final DistributionListPartialRecord copy(DistributionListId distributionListId, CharSequence charSequence, RecipientId recipientId, boolean z, boolean z2, DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(charSequence, "name");
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        return new DistributionListPartialRecord(distributionListId, charSequence, recipientId, z, z2, distributionListPrivacyMode);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DistributionListPartialRecord)) {
            return false;
        }
        DistributionListPartialRecord distributionListPartialRecord = (DistributionListPartialRecord) obj;
        return Intrinsics.areEqual(this.id, distributionListPartialRecord.id) && Intrinsics.areEqual(this.name, distributionListPartialRecord.name) && Intrinsics.areEqual(this.recipientId, distributionListPartialRecord.recipientId) && this.allowsReplies == distributionListPartialRecord.allowsReplies && this.isUnknown == distributionListPartialRecord.isUnknown && this.privacyMode == distributionListPartialRecord.privacyMode;
    }

    public int hashCode() {
        int hashCode = ((((this.id.hashCode() * 31) + this.name.hashCode()) * 31) + this.recipientId.hashCode()) * 31;
        boolean z = this.allowsReplies;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode + i2) * 31;
        boolean z2 = this.isUnknown;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        return ((i5 + i) * 31) + this.privacyMode.hashCode();
    }

    public String toString() {
        return "DistributionListPartialRecord(id=" + this.id + ", name=" + ((Object) this.name) + ", recipientId=" + this.recipientId + ", allowsReplies=" + this.allowsReplies + ", isUnknown=" + this.isUnknown + ", privacyMode=" + this.privacyMode + ')';
    }

    public DistributionListPartialRecord(DistributionListId distributionListId, CharSequence charSequence, RecipientId recipientId, boolean z, boolean z2, DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(charSequence, "name");
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        this.id = distributionListId;
        this.name = charSequence;
        this.recipientId = recipientId;
        this.allowsReplies = z;
        this.isUnknown = z2;
        this.privacyMode = distributionListPrivacyMode;
    }

    public final DistributionListId getId() {
        return this.id;
    }

    public final CharSequence getName() {
        return this.name;
    }

    public final RecipientId getRecipientId() {
        return this.recipientId;
    }

    public final boolean getAllowsReplies() {
        return this.allowsReplies;
    }

    public final boolean isUnknown() {
        return this.isUnknown;
    }

    public final DistributionListPrivacyMode getPrivacyMode() {
        return this.privacyMode;
    }
}
