package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.text.TextUtils;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiStrings;
import org.thoughtcrime.securesms.contactshare.ContactUtil;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.mms.GifSlide;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.StickerSlide;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class ThreadBodyUtil {
    private static final String TAG = Log.tag(ThreadBodyUtil.class);

    private ThreadBodyUtil() {
    }

    public static String getFormattedBodyFor(Context context, MessageRecord messageRecord) {
        if (messageRecord.isMms()) {
            return getFormattedBodyForMms(context, (MmsMessageRecord) messageRecord);
        }
        return messageRecord.getBody();
    }

    private static String getFormattedBodyForMms(Context context, MmsMessageRecord mmsMessageRecord) {
        if (mmsMessageRecord.getSharedContacts().size() > 0) {
            return ContactUtil.getStringSummary(context, mmsMessageRecord.getSharedContacts().get(0)).toString();
        }
        if (mmsMessageRecord.getSlideDeck().getDocumentSlide() != null) {
            return format(context, mmsMessageRecord, EmojiStrings.FILE, R.string.ThreadRecord_file);
        }
        if (mmsMessageRecord.getSlideDeck().getAudioSlide() != null) {
            return format(context, mmsMessageRecord, EmojiStrings.AUDIO, R.string.ThreadRecord_voice_message);
        }
        if (MessageRecordUtil.hasSticker(mmsMessageRecord)) {
            return format(context, mmsMessageRecord, getStickerEmoji(mmsMessageRecord), R.string.ThreadRecord_sticker);
        }
        if (MessageRecordUtil.hasGiftBadge(mmsMessageRecord)) {
            return String.format("%s %s", EmojiStrings.GIFT, getGiftSummary(context, mmsMessageRecord));
        }
        if (MessageRecordUtil.isStoryReaction(mmsMessageRecord)) {
            return getStoryReactionSummary(context, mmsMessageRecord);
        }
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        for (Slide slide : mmsMessageRecord.getSlideDeck().getSlides()) {
            z2 |= slide.hasVideo();
            z3 |= slide.hasImage();
            z |= (slide instanceof GifSlide) || slide.isVideoGif();
        }
        if (z) {
            return format(context, mmsMessageRecord, EmojiStrings.GIF, R.string.ThreadRecord_gif);
        }
        if (z2) {
            return format(context, mmsMessageRecord, EmojiStrings.VIDEO, R.string.ThreadRecord_video);
        }
        if (z3) {
            return format(context, mmsMessageRecord, EmojiStrings.PHOTO, R.string.ThreadRecord_photo);
        }
        if (TextUtils.isEmpty(mmsMessageRecord.getBody())) {
            return context.getString(R.string.ThreadRecord_media_message);
        }
        return getBody(context, mmsMessageRecord);
    }

    private static String getGiftSummary(Context context, MessageRecord messageRecord) {
        if (messageRecord.isOutgoing()) {
            return context.getString(R.string.ThreadRecord__you_sent_a_gift);
        }
        if (messageRecord.getViewedReceiptCount() > 0) {
            return context.getString(R.string.ThreadRecord__you_redeemed_a_gift_badge);
        }
        return context.getString(R.string.ThreadRecord__you_received_a_gift);
    }

    private static String getStoryReactionSummary(Context context, MessageRecord messageRecord) {
        if (messageRecord.isOutgoing()) {
            return context.getString(R.string.ThreadRecord__reacted_s_to_their_story, messageRecord.getDisplayBody(context));
        }
        return context.getString(R.string.ThreadRecord__reacted_s_to_your_story, messageRecord.getDisplayBody(context));
    }

    private static String format(Context context, MessageRecord messageRecord, String str, int i) {
        return String.format("%s %s", str, getBodyOrDefault(context, messageRecord, i));
    }

    private static String getBodyOrDefault(Context context, MessageRecord messageRecord, int i) {
        return TextUtils.isEmpty(messageRecord.getBody()) ? context.getString(i) : getBody(context, messageRecord);
    }

    private static String getBody(Context context, MessageRecord messageRecord) {
        return MentionUtil.updateBodyWithDisplayNames(context, messageRecord, messageRecord.getBody()).toString();
    }

    private static String getStickerEmoji(MessageRecord messageRecord) {
        StickerSlide stickerSlide = ((MmsMessageRecord) messageRecord).getSlideDeck().getStickerSlide();
        Objects.requireNonNull(stickerSlide);
        if (Util.isEmpty(stickerSlide.getEmoji())) {
            return EmojiStrings.STICKER;
        }
        return stickerSlide.getEmoji();
    }
}
