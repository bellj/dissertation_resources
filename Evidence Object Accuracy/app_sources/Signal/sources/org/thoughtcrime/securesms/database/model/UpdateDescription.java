package org.thoughtcrime.securesms.database.model;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public final class UpdateDescription {
    private final int darkTint;
    private final int lightIconResource;
    private final int lightTint;
    private final Collection<ServiceId> mentioned;
    private final Spannable staticString;
    private final SpannableFactory stringFactory;

    /* loaded from: classes4.dex */
    public interface SpannableFactory {
        Spannable create();
    }

    private UpdateDescription(Collection<ServiceId> collection, SpannableFactory spannableFactory, Spannable spannable, int i, int i2, int i3) {
        if (spannable == null && spannableFactory == null) {
            throw new AssertionError();
        }
        this.mentioned = collection;
        this.stringFactory = spannableFactory;
        this.staticString = spannable;
        this.lightIconResource = i;
        this.lightTint = i2;
        this.darkTint = i3;
    }

    public static UpdateDescription mentioning(Collection<ServiceId> collection, SpannableFactory spannableFactory, int i) {
        return new UpdateDescription(ServiceId.filterKnown(collection), spannableFactory, null, i, 0, 0);
    }

    public static UpdateDescription staticDescription(String str, int i) {
        return new UpdateDescription(Collections.emptyList(), null, new SpannableString(str), i, 0, 0);
    }

    public static UpdateDescription staticDescription(Spannable spannable, int i) {
        return new UpdateDescription(Collections.emptyList(), null, spannable, i, 0, 0);
    }

    public static UpdateDescription staticDescription(String str, int i, int i2, int i3) {
        return new UpdateDescription(Collections.emptyList(), null, new SpannableString(str), i, i2, i3);
    }

    public boolean isStringStatic() {
        return this.staticString != null;
    }

    public Spannable getStaticSpannable() {
        Spannable spannable = this.staticString;
        if (spannable != null) {
            return spannable;
        }
        throw new UnsupportedOperationException();
    }

    public Spannable getSpannable() {
        Spannable spannable = this.staticString;
        if (spannable != null) {
            return spannable;
        }
        return this.stringFactory.create();
    }

    public Collection<ServiceId> getMentioned() {
        return this.mentioned;
    }

    public int getIconResource() {
        return this.lightIconResource;
    }

    public int getLightTint() {
        return this.lightTint;
    }

    public int getDarkTint() {
        return this.darkTint;
    }

    public static UpdateDescription concatWithNewLines(List<UpdateDescription> list) {
        if (list.size() == 0) {
            throw new AssertionError();
        } else if (list.size() == 1) {
            return list.get(0);
        } else {
            if (allAreStatic(list)) {
                return staticDescription(concatStaticLines(list), list.get(0).getIconResource());
            }
            HashSet hashSet = new HashSet();
            for (UpdateDescription updateDescription : list) {
                hashSet.addAll(updateDescription.getMentioned());
            }
            return mentioning(hashSet, new SpannableFactory(list) { // from class: org.thoughtcrime.securesms.database.model.UpdateDescription$$ExternalSyntheticLambda0
                public final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                @Override // org.thoughtcrime.securesms.database.model.UpdateDescription.SpannableFactory
                public final Spannable create() {
                    return UpdateDescription.concatLines(this.f$0);
                }
            }, list.get(0).getIconResource());
        }
    }

    private static boolean allAreStatic(Collection<UpdateDescription> collection) {
        for (UpdateDescription updateDescription : collection) {
            if (!updateDescription.isStringStatic()) {
                return false;
            }
        }
        return true;
    }

    public static Spannable concatLines(List<UpdateDescription> list) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i > 0) {
                spannableStringBuilder.append('\n');
            }
            spannableStringBuilder.append((CharSequence) list.get(i).getSpannable());
        }
        return spannableStringBuilder;
    }

    private static Spannable concatStaticLines(List<UpdateDescription> list) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i > 0) {
                spannableStringBuilder.append('\n');
            }
            spannableStringBuilder.append((CharSequence) list.get(i).getStaticSpannable());
        }
        return spannableStringBuilder;
    }
}
