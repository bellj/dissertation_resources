package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class ContentValuesBuilder {
    private final ContentValues contentValues;

    public ContentValuesBuilder(ContentValues contentValues) {
        this.contentValues = contentValues;
    }

    public void add(String str, String str2, EncodedStringValue encodedStringValue) {
        if (encodedStringValue != null) {
            this.contentValues.put(str, Util.toIsoString(encodedStringValue.getTextString()));
            this.contentValues.put(str2, Integer.valueOf(encodedStringValue.getCharacterSet()));
        }
    }

    public void add(String str, byte[] bArr) {
        if (bArr != null) {
            this.contentValues.put(str, Util.toIsoString(bArr));
        }
    }

    public void add(String str, int i) {
        if (i != 0) {
            this.contentValues.put(str, Integer.valueOf(i));
        }
    }

    public void add(String str, long j) {
        if (j != -1) {
            this.contentValues.put(str, Long.valueOf(j));
        }
    }

    public ContentValues getContentValues() {
        return this.contentValues;
    }
}
