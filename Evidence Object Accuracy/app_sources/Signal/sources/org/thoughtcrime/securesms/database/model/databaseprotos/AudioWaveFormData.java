package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes4.dex */
public final class AudioWaveFormData extends GeneratedMessageLite<AudioWaveFormData, Builder> implements AudioWaveFormDataOrBuilder {
    private static final AudioWaveFormData DEFAULT_INSTANCE;
    public static final int DURATIONUS_FIELD_NUMBER;
    private static volatile Parser<AudioWaveFormData> PARSER;
    public static final int WAVEFORM_FIELD_NUMBER;
    private long durationUs_;
    private ByteString waveForm_ = ByteString.EMPTY;

    private AudioWaveFormData() {
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.AudioWaveFormDataOrBuilder
    public long getDurationUs() {
        return this.durationUs_;
    }

    public void setDurationUs(long j) {
        this.durationUs_ = j;
    }

    public void clearDurationUs() {
        this.durationUs_ = 0;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.AudioWaveFormDataOrBuilder
    public ByteString getWaveForm() {
        return this.waveForm_;
    }

    public void setWaveForm(ByteString byteString) {
        byteString.getClass();
        this.waveForm_ = byteString;
    }

    public void clearWaveForm() {
        this.waveForm_ = getDefaultInstance().getWaveForm();
    }

    public static AudioWaveFormData parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (AudioWaveFormData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static AudioWaveFormData parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AudioWaveFormData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static AudioWaveFormData parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (AudioWaveFormData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static AudioWaveFormData parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AudioWaveFormData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static AudioWaveFormData parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (AudioWaveFormData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static AudioWaveFormData parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AudioWaveFormData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static AudioWaveFormData parseFrom(InputStream inputStream) throws IOException {
        return (AudioWaveFormData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static AudioWaveFormData parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AudioWaveFormData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static AudioWaveFormData parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (AudioWaveFormData) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static AudioWaveFormData parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AudioWaveFormData) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static AudioWaveFormData parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (AudioWaveFormData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static AudioWaveFormData parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AudioWaveFormData) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(AudioWaveFormData audioWaveFormData) {
        return DEFAULT_INSTANCE.createBuilder(audioWaveFormData);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<AudioWaveFormData, Builder> implements AudioWaveFormDataOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(AudioWaveFormData.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.AudioWaveFormDataOrBuilder
        public long getDurationUs() {
            return ((AudioWaveFormData) this.instance).getDurationUs();
        }

        public Builder setDurationUs(long j) {
            copyOnWrite();
            ((AudioWaveFormData) this.instance).setDurationUs(j);
            return this;
        }

        public Builder clearDurationUs() {
            copyOnWrite();
            ((AudioWaveFormData) this.instance).clearDurationUs();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.AudioWaveFormDataOrBuilder
        public ByteString getWaveForm() {
            return ((AudioWaveFormData) this.instance).getWaveForm();
        }

        public Builder setWaveForm(ByteString byteString) {
            copyOnWrite();
            ((AudioWaveFormData) this.instance).setWaveForm(byteString);
            return this;
        }

        public Builder clearWaveForm() {
            copyOnWrite();
            ((AudioWaveFormData) this.instance).clearWaveForm();
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.AudioWaveFormData$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new AudioWaveFormData();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0002\u0002\n", new Object[]{"durationUs_", "waveForm_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<AudioWaveFormData> parser = PARSER;
                if (parser == null) {
                    synchronized (AudioWaveFormData.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        AudioWaveFormData audioWaveFormData = new AudioWaveFormData();
        DEFAULT_INSTANCE = audioWaveFormData;
        GeneratedMessageLite.registerDefaultInstance(AudioWaveFormData.class, audioWaveFormData);
    }

    public static AudioWaveFormData getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<AudioWaveFormData> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
