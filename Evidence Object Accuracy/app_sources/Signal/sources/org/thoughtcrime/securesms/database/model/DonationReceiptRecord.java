package org.thoughtcrime.securesms.database.model;

import java.util.Currency;
import java.util.NoSuchElementException;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;

/* compiled from: DonationReceiptRecord.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0011\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\b\b\u0018\u0000 !2\u00020\u0001:\u0002!\"B/\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\bHÆ\u0003J\t\u0010\u0019\u001a\u00020\nHÆ\u0003J;\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\nHÆ\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001e\u001a\u00020\nHÖ\u0001J\t\u0010\u001f\u001a\u00020 HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", "", ContactRepository.ID_COLUMN, "", "amount", "Lorg/signal/core/util/money/FiatMoney;", "timestamp", "type", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "subscriptionLevel", "", "(JLorg/signal/core/util/money/FiatMoney;JLorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;I)V", "getAmount", "()Lorg/signal/core/util/money/FiatMoney;", "getId", "()J", "getSubscriptionLevel", "()I", "getTimestamp", "getType", "()Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "toString", "", "Companion", "Type", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptRecord {
    public static final Companion Companion = new Companion(null);
    private final FiatMoney amount;
    private final long id;
    private final int subscriptionLevel;
    private final long timestamp;
    private final Type type;

    @JvmStatic
    public static final DonationReceiptRecord createForSubscription(ActiveSubscription.Subscription subscription) {
        return Companion.createForSubscription(subscription);
    }

    public final long component1() {
        return this.id;
    }

    public final FiatMoney component2() {
        return this.amount;
    }

    public final long component3() {
        return this.timestamp;
    }

    public final Type component4() {
        return this.type;
    }

    public final int component5() {
        return this.subscriptionLevel;
    }

    public final DonationReceiptRecord copy(long j, FiatMoney fiatMoney, long j2, Type type, int i) {
        Intrinsics.checkNotNullParameter(fiatMoney, "amount");
        Intrinsics.checkNotNullParameter(type, "type");
        return new DonationReceiptRecord(j, fiatMoney, j2, type, i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DonationReceiptRecord)) {
            return false;
        }
        DonationReceiptRecord donationReceiptRecord = (DonationReceiptRecord) obj;
        return this.id == donationReceiptRecord.id && Intrinsics.areEqual(this.amount, donationReceiptRecord.amount) && this.timestamp == donationReceiptRecord.timestamp && this.type == donationReceiptRecord.type && this.subscriptionLevel == donationReceiptRecord.subscriptionLevel;
    }

    public int hashCode() {
        return (((((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id) * 31) + this.amount.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.timestamp)) * 31) + this.type.hashCode()) * 31) + this.subscriptionLevel;
    }

    public String toString() {
        return "DonationReceiptRecord(id=" + this.id + ", amount=" + this.amount + ", timestamp=" + this.timestamp + ", type=" + this.type + ", subscriptionLevel=" + this.subscriptionLevel + ')';
    }

    public DonationReceiptRecord(long j, FiatMoney fiatMoney, long j2, Type type, int i) {
        Intrinsics.checkNotNullParameter(fiatMoney, "amount");
        Intrinsics.checkNotNullParameter(type, "type");
        this.id = j;
        this.amount = fiatMoney;
        this.timestamp = j2;
        this.type = type;
        this.subscriptionLevel = i;
    }

    public /* synthetic */ DonationReceiptRecord(long j, FiatMoney fiatMoney, long j2, Type type, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? -1 : j, fiatMoney, j2, type, i);
    }

    public final long getId() {
        return this.id;
    }

    public final FiatMoney getAmount() {
        return this.amount;
    }

    public final long getTimestamp() {
        return this.timestamp;
    }

    public final Type getType() {
        return this.type;
    }

    public final int getSubscriptionLevel() {
        return this.subscriptionLevel;
    }

    /* compiled from: DonationReceiptRecord.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\b\u0001\u0018\u0000 \n2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\nB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "RECURRING", Badge.BOOST_BADGE_ID, Badge.GIFT_BADGE_ID, "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Type {
        RECURRING("recurring"),
        BOOST("boost"),
        GIFT("gift");
        
        public static final Companion Companion = new Companion(null);
        private final String code;

        Type(String str) {
            this.code = str;
        }

        public final String getCode() {
            return this.code;
        }

        /* compiled from: DonationReceiptRecord.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type$Companion;", "", "()V", "fromCode", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "code", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final Type fromCode(String str) {
                Intrinsics.checkNotNullParameter(str, "code");
                Type[] values = Type.values();
                for (Type type : values) {
                    if (Intrinsics.areEqual(type.getCode(), str)) {
                        return type;
                    }
                }
                throw new NoSuchElementException("Array contains no element matching the predicate.");
            }
        }
    }

    /* compiled from: DonationReceiptRecord.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0007¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Companion;", "", "()V", "createForBoost", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", "amount", "Lorg/signal/core/util/money/FiatMoney;", "createForGift", "createForSubscription", "subscription", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription$Subscription;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final DonationReceiptRecord createForSubscription(ActiveSubscription.Subscription subscription) {
            Intrinsics.checkNotNullParameter(subscription, "subscription");
            Currency instance = Currency.getInstance(subscription.getCurrency());
            return new DonationReceiptRecord(-1, new FiatMoney(subscription.getAmount().movePointLeft(instance.getDefaultFractionDigits()), instance), System.currentTimeMillis(), Type.RECURRING, subscription.getLevel());
        }

        public final DonationReceiptRecord createForBoost(FiatMoney fiatMoney) {
            Intrinsics.checkNotNullParameter(fiatMoney, "amount");
            return new DonationReceiptRecord(-1, fiatMoney, System.currentTimeMillis(), Type.BOOST, -1);
        }

        public final DonationReceiptRecord createForGift(FiatMoney fiatMoney) {
            Intrinsics.checkNotNullParameter(fiatMoney, "amount");
            return new DonationReceiptRecord(-1, fiatMoney, System.currentTimeMillis(), Type.GIFT, -1);
        }
    }
}
