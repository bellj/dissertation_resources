package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ServiceMessageId.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\b\u0010\u0013\u001a\u00020\u0014H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/ServiceMessageId;", "", "sender", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "sentTimestamp", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;J)V", "getSender", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getSentTimestamp", "()J", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ServiceMessageId {
    private final RecipientId sender;
    private final long sentTimestamp;

    public static /* synthetic */ ServiceMessageId copy$default(ServiceMessageId serviceMessageId, RecipientId recipientId, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            recipientId = serviceMessageId.sender;
        }
        if ((i & 2) != 0) {
            j = serviceMessageId.sentTimestamp;
        }
        return serviceMessageId.copy(recipientId, j);
    }

    public final RecipientId component1() {
        return this.sender;
    }

    public final long component2() {
        return this.sentTimestamp;
    }

    public final ServiceMessageId copy(RecipientId recipientId, long j) {
        Intrinsics.checkNotNullParameter(recipientId, "sender");
        return new ServiceMessageId(recipientId, j);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ServiceMessageId)) {
            return false;
        }
        ServiceMessageId serviceMessageId = (ServiceMessageId) obj;
        return Intrinsics.areEqual(this.sender, serviceMessageId.sender) && this.sentTimestamp == serviceMessageId.sentTimestamp;
    }

    public int hashCode() {
        return (this.sender.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.sentTimestamp);
    }

    public ServiceMessageId(RecipientId recipientId, long j) {
        Intrinsics.checkNotNullParameter(recipientId, "sender");
        this.sender = recipientId;
        this.sentTimestamp = j;
    }

    public final RecipientId getSender() {
        return this.sender;
    }

    public final long getSentTimestamp() {
        return this.sentTimestamp;
    }

    public String toString() {
        return "MessageId(" + this.sender + ", " + this.sentTimestamp + ')';
    }
}
