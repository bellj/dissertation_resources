package org.thoughtcrime.securesms.database;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: RecipientDatabase.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "kotlin.jvm.PlatformType", "invoke", "(Lorg/thoughtcrime/securesms/recipients/Recipient;)Ljava/lang/Boolean;"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RecipientDatabase$getRecipientsForRoutineProfileFetch$2 extends Lambda implements Function1<Recipient, Boolean> {
    public static final RecipientDatabase$getRecipientsForRoutineProfileFetch$2 INSTANCE = new RecipientDatabase$getRecipientsForRoutineProfileFetch$2();

    RecipientDatabase$getRecipientsForRoutineProfileFetch$2() {
        super(1);
    }

    public final Boolean invoke(Recipient recipient) {
        return Boolean.valueOf(recipient.isSelf());
    }
}
