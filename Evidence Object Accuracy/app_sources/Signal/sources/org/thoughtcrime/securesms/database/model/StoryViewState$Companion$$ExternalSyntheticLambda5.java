package org.thoughtcrime.securesms.database.model;

import io.reactivex.rxjava3.functions.Function;
import org.thoughtcrime.securesms.database.model.StoryViewState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoryViewState$Companion$$ExternalSyntheticLambda5 implements Function {
    @Override // io.reactivex.rxjava3.functions.Function
    public final Object apply(Object obj) {
        return StoryViewState.Companion.m1745getForRecipientId$lambda5$lambda4((Object[]) obj);
    }
}
