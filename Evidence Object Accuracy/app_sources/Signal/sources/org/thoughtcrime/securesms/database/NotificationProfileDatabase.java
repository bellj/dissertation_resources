package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import j$.time.DayOfWeek;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import kotlin.text.StringsKt__StringsKt;
import net.zetetic.database.sqlcipher.SQLiteConstraintException;
import org.signal.core.util.CursorExtensionsKt;
import org.signal.core.util.SqlUtil;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfileSchedule;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: NotificationProfileDatabase.kt */
@Metadata(d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0006\u0018\u0000 ,2\u00020\u0001:\u0005,-./0B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ&\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\nJ\u000e\u0010\u0015\u001a\u00020\u00162\u0006\u0010\t\u001a\u00020\nJ\u0010\u0010\u0017\u001a\u00020\b2\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u0010\u0010\u0017\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\f0\u001b2\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\t\u001a\u00020\nH\u0002J\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\b0\u001fJ\u0016\u0010 \u001a\u00020\u00162\u0006\u0010!\u001a\u00020\f2\u0006\u0010\"\u001a\u00020\fJ\u0016\u0010#\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u001c\u0010$\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\f0\u001bJ\u001e\u0010&\u001a\u00020\u000e2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0010J\u000e\u0010&\u001a\u00020\u000e2\u0006\u0010'\u001a\u00020\bJ\u0018\u0010(\u001a\u00020\u00162\u0006\u0010)\u001a\u00020\u001d2\b\b\u0002\u0010*\u001a\u00020+¨\u00061"}, d2 = {"Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "addAllowedRecipient", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "profileId", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "createProfile", "Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileChangeResult;", "name", "", "emoji", NotificationProfileTable.COLOR, "Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "createdAt", "deleteProfile", "", "getProfile", "cursor", "Landroid/database/Cursor;", "getProfileAllowedMembers", "", "getProfileSchedule", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfileSchedule;", "getProfiles", "", "remapRecipient", "oldId", "newId", "removeAllowedRecipient", "setAllowedRecipients", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "updateProfile", "profile", "updateSchedule", "schedule", "silent", "", "Companion", "NotificationProfileAllowedMembersTable", "NotificationProfileChangeResult", "NotificationProfileScheduleTable", "NotificationProfileTable", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfileDatabase extends Database {
    public static final String[] CREATE_INDEXES = {NotificationProfileScheduleTable.CREATE_INDEX, NotificationProfileAllowedMembersTable.CREATE_INDEX};
    public static final String[] CREATE_TABLE = {NotificationProfileTable.INSTANCE.getCREATE_TABLE(), NotificationProfileScheduleTable.INSTANCE.getCREATE_TABLE(), NotificationProfileAllowedMembersTable.INSTANCE.getCREATE_TABLE()};
    public static final Companion Companion = new Companion(null);

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public NotificationProfileDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    /* compiled from: NotificationProfileDatabase.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0006¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$Companion;", "", "()V", "CREATE_INDEXES", "", "", "[Ljava/lang/String;", "CREATE_TABLE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* compiled from: NotificationProfileDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0011\u0010\b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileTable;", "", "()V", "ALLOW_ALL_CALLS", "", "ALLOW_ALL_MENTIONS", "COLOR", "CREATED_AT", "CREATE_TABLE", "getCREATE_TABLE", "()Ljava/lang/String;", "EMOJI", "ID", "NAME", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class NotificationProfileTable {
        public static final String ALLOW_ALL_CALLS;
        public static final String ALLOW_ALL_MENTIONS;
        public static final String COLOR;
        public static final String CREATED_AT;
        private static final String CREATE_TABLE = "CREATE TABLE notification_profile (\n  _id INTEGER PRIMARY KEY AUTOINCREMENT,\n  name TEXT NOT NULL UNIQUE,\n  emoji TEXT NOT NULL,\n  color TEXT NOT NULL,\n  created_at INTEGER NOT NULL,\n  allow_all_calls INTEGER NOT NULL DEFAULT 0,\n  allow_all_mentions INTEGER NOT NULL DEFAULT 0\n)";
        public static final String EMOJI;
        public static final String ID;
        public static final NotificationProfileTable INSTANCE = new NotificationProfileTable();
        public static final String NAME;
        public static final String TABLE_NAME;

        private NotificationProfileTable() {
        }

        public final String getCREATE_TABLE() {
            return CREATE_TABLE;
        }
    }

    /* compiled from: NotificationProfileDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\r\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0007R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileScheduleTable;", "", "()V", "CREATE_INDEX", "", "CREATE_TABLE", "getCREATE_TABLE", "()Ljava/lang/String;", "DAYS_ENABLED", "DEFAULT_DAYS", "getDEFAULT_DAYS", "ENABLED", "END", "ID", "NOTIFICATION_PROFILE_ID", "START", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NotificationProfileScheduleTable {
        public static final String CREATE_INDEX;
        private static final String CREATE_TABLE = "CREATE TABLE notification_profile_schedule (\n  _id INTEGER PRIMARY KEY AUTOINCREMENT,\n  notification_profile_id INTEGER NOT NULL REFERENCES notification_profile (_id) ON DELETE CASCADE,\n  enabled INTEGER NOT NULL DEFAULT 0,\n  start INTEGER NOT NULL,\n  end INTEGER NOT NULL,\n  days_enabled TEXT NOT NULL\n)";
        public static final String DAYS_ENABLED;
        private static final String DEFAULT_DAYS = NotificationProfileDatabaseKt.access$serialize(CollectionsKt__CollectionsKt.listOf((Object[]) new DayOfWeek[]{DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY}));
        public static final String ENABLED;
        public static final String END;
        public static final String ID;
        public static final NotificationProfileScheduleTable INSTANCE = new NotificationProfileScheduleTable();
        public static final String NOTIFICATION_PROFILE_ID;
        public static final String START;
        public static final String TABLE_NAME;

        private NotificationProfileScheduleTable() {
        }

        public final String getDEFAULT_DAYS() {
            return DEFAULT_DAYS;
        }

        public final String getCREATE_TABLE() {
            return CREATE_TABLE;
        }
    }

    /* compiled from: NotificationProfileDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileAllowedMembersTable;", "", "()V", "CREATE_INDEX", "", "CREATE_TABLE", "getCREATE_TABLE", "()Ljava/lang/String;", "ID", "NOTIFICATION_PROFILE_ID", "RECIPIENT_ID", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class NotificationProfileAllowedMembersTable {
        public static final String CREATE_INDEX;
        private static final String CREATE_TABLE = "CREATE TABLE notification_profile_allowed_members (\n  _id INTEGER PRIMARY KEY AUTOINCREMENT,\n  notification_profile_id INTEGER NOT NULL REFERENCES notification_profile (_id) ON DELETE CASCADE,\n  recipient_id INTEGER NOT NULL,\n  UNIQUE(notification_profile_id, recipient_id) ON CONFLICT REPLACE\n)";
        public static final String ID;
        public static final NotificationProfileAllowedMembersTable INSTANCE = new NotificationProfileAllowedMembersTable();
        public static final String NOTIFICATION_PROFILE_ID;
        public static final String RECIPIENT_ID;
        public static final String TABLE_NAME;

        private NotificationProfileAllowedMembersTable() {
        }

        public final String getCREATE_TABLE() {
            return CREATE_TABLE;
        }
    }

    public final NotificationProfileChangeResult createProfile(String str, String str2, AvatarColor avatarColor, long j) {
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        Intrinsics.checkNotNullParameter(str, "name");
        Intrinsics.checkNotNullParameter(str2, "emoji");
        Intrinsics.checkNotNullParameter(avatarColor, NotificationProfileTable.COLOR);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("name", str);
            contentValues.put("emoji", str2);
            contentValues.put(NotificationProfileTable.COLOR, avatarColor.serialize());
            contentValues.put("created_at", Long.valueOf(j));
            long insert = writableDatabase.insert(NotificationProfileTable.TABLE_NAME, (String) null, contentValues);
            if (insert < 0) {
                NotificationProfileChangeResult.DuplicateName duplicateName = NotificationProfileChangeResult.DuplicateName.INSTANCE;
                writableDatabase.endTransaction();
                ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
                return duplicateName;
            }
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("notification_profile_id", Long.valueOf(insert));
            contentValues2.put(NotificationProfileScheduleTable.START, (Integer) 900);
            contentValues2.put(NotificationProfileScheduleTable.END, (Integer) 1700);
            contentValues2.put(NotificationProfileScheduleTable.DAYS_ENABLED, NotificationProfileScheduleTable.INSTANCE.getDEFAULT_DAYS());
            writableDatabase.insert(NotificationProfileScheduleTable.TABLE_NAME, (String) null, contentValues2);
            writableDatabase.setTransactionSuccessful();
            sQLiteDatabase = writableDatabase;
            try {
                NotificationProfileChangeResult.Success success = new NotificationProfileChangeResult.Success(new NotificationProfile(insert, str, str2, null, j, false, false, getProfileSchedule(insert), null, 360, null));
                sQLiteDatabase.endTransaction();
                ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
                return success;
            } catch (Throwable th2) {
                th = th2;
                sQLiteDatabase.endTransaction();
                ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            sQLiteDatabase = writableDatabase;
        }
    }

    public final NotificationProfileChangeResult updateProfile(long j, String str, String str2) {
        Intrinsics.checkNotNullParameter(str, "name");
        Intrinsics.checkNotNullParameter(str2, "emoji");
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", str);
        contentValues.put("emoji", str2);
        SqlUtil.Query buildTrueUpdateQuery = SqlUtil.buildTrueUpdateQuery("_id = ?", SqlUtil.buildArgs(j), contentValues);
        try {
            if (getWritableDatabase().update(NotificationProfileTable.TABLE_NAME, contentValues, buildTrueUpdateQuery.getWhere(), buildTrueUpdateQuery.getWhereArgs()) > 0) {
                ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
            }
            NotificationProfile profile = getProfile(j);
            Intrinsics.checkNotNull(profile);
            return new NotificationProfileChangeResult.Success(profile);
        } catch (SQLiteConstraintException unused) {
            return NotificationProfileChangeResult.DuplicateName.INSTANCE;
        }
    }

    public final NotificationProfileChangeResult updateProfile(NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(notificationProfile, "profile");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("name", notificationProfile.getName());
            contentValues.put("emoji", notificationProfile.getEmoji());
            contentValues.put(NotificationProfileTable.ALLOW_ALL_CALLS, Integer.valueOf(CursorExtensionsKt.toInt(notificationProfile.getAllowAllCalls())));
            contentValues.put(NotificationProfileTable.ALLOW_ALL_MENTIONS, Integer.valueOf(CursorExtensionsKt.toInt(notificationProfile.getAllowAllMentions())));
            SqlUtil.Query buildTrueUpdateQuery = SqlUtil.buildTrueUpdateQuery("_id = ?", SqlUtil.buildArgs(notificationProfile.getId()), contentValues);
            writableDatabase.update(NotificationProfileTable.TABLE_NAME, contentValues, buildTrueUpdateQuery.getWhere(), buildTrueUpdateQuery.getWhereArgs());
            updateSchedule(notificationProfile.getSchedule(), true);
            writableDatabase.delete(NotificationProfileAllowedMembersTable.TABLE_NAME, "notification_profile_id = ?", SqlUtil.buildArgs(notificationProfile.getId()));
            for (RecipientId recipientId : notificationProfile.getAllowedMembers()) {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("notification_profile_id", Long.valueOf(notificationProfile.getId()));
                contentValues2.put("recipient_id", recipientId.serialize());
                writableDatabase.insert(NotificationProfileAllowedMembersTable.TABLE_NAME, (String) null, contentValues2);
            }
            writableDatabase.setTransactionSuccessful();
            NotificationProfile profile = getProfile(notificationProfile.getId());
            Intrinsics.checkNotNull(profile);
            return new NotificationProfileChangeResult.Success(profile);
        } catch (SQLiteConstraintException unused) {
            return NotificationProfileChangeResult.DuplicateName.INSTANCE;
        } finally {
            writableDatabase.endTransaction();
            ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
        }
    }

    public static /* synthetic */ void updateSchedule$default(NotificationProfileDatabase notificationProfileDatabase, NotificationProfileSchedule notificationProfileSchedule, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        notificationProfileDatabase.updateSchedule(notificationProfileSchedule, z);
    }

    public final void updateSchedule(NotificationProfileSchedule notificationProfileSchedule, boolean z) {
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "schedule");
        ContentValues contentValues = new ContentValues();
        contentValues.put(NotificationProfileScheduleTable.ENABLED, Integer.valueOf(CursorExtensionsKt.toInt(notificationProfileSchedule.getEnabled())));
        contentValues.put(NotificationProfileScheduleTable.START, Integer.valueOf(notificationProfileSchedule.getStart()));
        contentValues.put(NotificationProfileScheduleTable.END, Integer.valueOf(notificationProfileSchedule.getEnd()));
        contentValues.put(NotificationProfileScheduleTable.DAYS_ENABLED, NotificationProfileDatabaseKt.serialize(notificationProfileSchedule.getDaysEnabled()));
        SqlUtil.Query buildTrueUpdateQuery = SqlUtil.buildTrueUpdateQuery("_id = ?", SqlUtil.buildArgs(notificationProfileSchedule.getId()), contentValues);
        getWritableDatabase().update(NotificationProfileScheduleTable.TABLE_NAME, contentValues, buildTrueUpdateQuery.getWhere(), buildTrueUpdateQuery.getWhereArgs());
        if (!z) {
            ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
        }
    }

    public final NotificationProfile setAllowedRecipients(long j, Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(set, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            writableDatabase.delete(NotificationProfileAllowedMembersTable.TABLE_NAME, "notification_profile_id = ?", SqlUtil.buildArgs(j));
            for (RecipientId recipientId : set) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("notification_profile_id", Long.valueOf(j));
                contentValues.put("recipient_id", recipientId.serialize());
                writableDatabase.insert(NotificationProfileAllowedMembersTable.TABLE_NAME, (String) null, contentValues);
            }
            writableDatabase.setTransactionSuccessful();
            NotificationProfile profile = getProfile(j);
            Intrinsics.checkNotNull(profile);
            return profile;
        } finally {
            writableDatabase.endTransaction();
            ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
        }
    }

    public final NotificationProfile addAllowedRecipient(long j, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        ContentValues contentValues = new ContentValues();
        contentValues.put("notification_profile_id", Long.valueOf(j));
        contentValues.put("recipient_id", recipientId.serialize());
        getWritableDatabase().insert(NotificationProfileAllowedMembersTable.TABLE_NAME, (String) null, contentValues);
        ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
        NotificationProfile profile = getProfile(j);
        Intrinsics.checkNotNull(profile);
        return profile;
    }

    public final NotificationProfile removeAllowedRecipient(long j, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        getWritableDatabase().delete(NotificationProfileAllowedMembersTable.TABLE_NAME, "notification_profile_id = ? AND recipient_id = ?", SqlUtil.buildArgs(Long.valueOf(j), recipientId));
        ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
        NotificationProfile profile = getProfile(j);
        Intrinsics.checkNotNull(profile);
        return profile;
    }

    public final List<NotificationProfile> getProfiles() {
        ArrayList arrayList = new ArrayList();
        Cursor query = getReadableDatabase().query(NotificationProfileTable.TABLE_NAME, null, null, null, null, null, null);
        while (query.moveToNext()) {
            try {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                arrayList.add(getProfile(query));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return arrayList;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Throwable] */
    public final NotificationProfile getProfile(long j) {
        NotificationProfile notificationProfile;
        Cursor query = getReadableDatabase().query(NotificationProfileTable.TABLE_NAME, null, "_id = ?", SqlUtil.buildArgs(j), null, null, null);
        try {
            th = 0;
            if (query.moveToFirst()) {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                notificationProfile = getProfile(query);
            } else {
                notificationProfile = th;
            }
            return notificationProfile;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final void deleteProfile(long j) {
        getWritableDatabase().delete(NotificationProfileTable.TABLE_NAME, "_id = ?", SqlUtil.buildArgs(j));
        ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
    }

    public final void remapRecipient(RecipientId recipientId, RecipientId recipientId2) {
        Intrinsics.checkNotNullParameter(recipientId, "oldId");
        Intrinsics.checkNotNullParameter(recipientId2, "newId");
        String[] buildArgs = SqlUtil.buildArgs(recipientId);
        ContentValues contentValues = new ContentValues();
        contentValues.put("recipient_id", recipientId2.serialize());
        this.databaseHelper.getSignalWritableDatabase().update(NotificationProfileAllowedMembersTable.TABLE_NAME, contentValues, "recipient_id = ?", buildArgs);
        ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
    }

    private final NotificationProfile getProfile(Cursor cursor) {
        long requireLong = CursorExtensionsKt.requireLong(cursor, "_id");
        String requireString = CursorExtensionsKt.requireString(cursor, "name");
        Intrinsics.checkNotNull(requireString);
        String requireString2 = CursorExtensionsKt.requireString(cursor, "emoji");
        Intrinsics.checkNotNull(requireString2);
        AvatarColor deserialize = AvatarColor.deserialize(CursorExtensionsKt.requireString(cursor, NotificationProfileTable.COLOR));
        Intrinsics.checkNotNullExpressionValue(deserialize, "deserialize(cursor.requi…ationProfileTable.COLOR))");
        return new NotificationProfile(requireLong, requireString, requireString2, deserialize, CursorExtensionsKt.requireLong(cursor, "created_at"), CursorExtensionsKt.requireBoolean(cursor, NotificationProfileTable.ALLOW_ALL_CALLS), CursorExtensionsKt.requireBoolean(cursor, NotificationProfileTable.ALLOW_ALL_MENTIONS), getProfileSchedule(requireLong), getProfileAllowedMembers(requireLong));
    }

    private final NotificationProfileSchedule getProfileSchedule(long j) {
        SqlUtil.Query buildQuery = SqlUtil.buildQuery("notification_profile_id = ?", Long.valueOf(j));
        Cursor query = getReadableDatabase().query(NotificationProfileScheduleTable.TABLE_NAME, null, buildQuery.getWhere(), buildQuery.getWhereArgs(), null, null, null);
        try {
            if (query.moveToFirst()) {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                String requireString = CursorExtensionsKt.requireString(query, NotificationProfileScheduleTable.DAYS_ENABLED);
                if (requireString == null) {
                    requireString = "";
                }
                List list = StringsKt__StringsKt.split$default((CharSequence) requireString, new String[]{","}, false, 0, 6, (Object) null);
                ArrayList<String> arrayList = new ArrayList();
                for (Object obj : list) {
                    if (!StringsKt__StringsJVMKt.isBlank((String) obj)) {
                        arrayList.add(obj);
                    }
                }
                ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
                for (String str : arrayList) {
                    arrayList2.add(NotificationProfileDatabaseKt.toDayOfWeek(str));
                }
                th = null;
                return new NotificationProfileSchedule(CursorExtensionsKt.requireLong(query, "_id"), CursorExtensionsKt.requireBoolean(query, NotificationProfileScheduleTable.ENABLED), CursorExtensionsKt.requireInt(query, NotificationProfileScheduleTable.START), CursorExtensionsKt.requireInt(query, NotificationProfileScheduleTable.END), CollectionsKt___CollectionsKt.toSet(arrayList2));
            }
            throw new AssertionError("No schedule for " + j);
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    private final Set<RecipientId> getProfileAllowedMembers(long j) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        SqlUtil.Query buildQuery = SqlUtil.buildQuery("notification_profile_id = ?", Long.valueOf(j));
        Cursor query = getReadableDatabase().query(NotificationProfileAllowedMembersTable.TABLE_NAME, null, buildQuery.getWhere(), buildQuery.getWhereArgs(), null, null, null);
        while (query.moveToNext()) {
            try {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                linkedHashSet.add(RecipientId.from(CursorExtensionsKt.requireLong(query, "recipient_id")));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return linkedHashSet;
    }

    /* compiled from: NotificationProfileDatabase.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileChangeResult;", "", "()V", "DuplicateName", "Success", "Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileChangeResult$Success;", "Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileChangeResult$DuplicateName;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class NotificationProfileChangeResult {
        public /* synthetic */ NotificationProfileChangeResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: NotificationProfileDatabase.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileChangeResult$Success;", "Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileChangeResult;", "notificationProfile", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "(Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;)V", "getNotificationProfile", "()Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Success extends NotificationProfileChangeResult {
            private final NotificationProfile notificationProfile;

            public static /* synthetic */ Success copy$default(Success success, NotificationProfile notificationProfile, int i, Object obj) {
                if ((i & 1) != 0) {
                    notificationProfile = success.notificationProfile;
                }
                return success.copy(notificationProfile);
            }

            public final NotificationProfile component1() {
                return this.notificationProfile;
            }

            public final Success copy(NotificationProfile notificationProfile) {
                Intrinsics.checkNotNullParameter(notificationProfile, "notificationProfile");
                return new Success(notificationProfile);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Success) && Intrinsics.areEqual(this.notificationProfile, ((Success) obj).notificationProfile);
            }

            public int hashCode() {
                return this.notificationProfile.hashCode();
            }

            public String toString() {
                return "Success(notificationProfile=" + this.notificationProfile + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Success(NotificationProfile notificationProfile) {
                super(null);
                Intrinsics.checkNotNullParameter(notificationProfile, "notificationProfile");
                this.notificationProfile = notificationProfile;
            }

            public final NotificationProfile getNotificationProfile() {
                return this.notificationProfile;
            }
        }

        private NotificationProfileChangeResult() {
        }

        /* compiled from: NotificationProfileDatabase.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileChangeResult$DuplicateName;", "Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileChangeResult;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class DuplicateName extends NotificationProfileChangeResult {
            public static final DuplicateName INSTANCE = new DuplicateName();

            private DuplicateName() {
                super(null);
            }
        }
    }
}
