package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public final class BadgeList extends GeneratedMessageLite<BadgeList, Builder> implements BadgeListOrBuilder {
    public static final int BADGES_FIELD_NUMBER;
    private static final BadgeList DEFAULT_INSTANCE;
    private static volatile Parser<BadgeList> PARSER;
    private Internal.ProtobufList<Badge> badges_ = GeneratedMessageLite.emptyProtobufList();

    /* loaded from: classes4.dex */
    public interface BadgeOrBuilder extends MessageLiteOrBuilder {
        String getCategory();

        ByteString getCategoryBytes();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getDescription();

        ByteString getDescriptionBytes();

        long getExpiration();

        String getId();

        ByteString getIdBytes();

        String getImageDensity();

        ByteString getImageDensityBytes();

        String getImageUrl();

        ByteString getImageUrlBytes();

        String getName();

        ByteString getNameBytes();

        boolean getVisible();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private BadgeList() {
    }

    /* loaded from: classes4.dex */
    public static final class Badge extends GeneratedMessageLite<Badge, Builder> implements BadgeOrBuilder {
        public static final int CATEGORY_FIELD_NUMBER;
        private static final Badge DEFAULT_INSTANCE;
        public static final int DESCRIPTION_FIELD_NUMBER;
        public static final int EXPIRATION_FIELD_NUMBER;
        public static final int ID_FIELD_NUMBER;
        public static final int IMAGEDENSITY_FIELD_NUMBER;
        public static final int IMAGEURL_FIELD_NUMBER;
        public static final int NAME_FIELD_NUMBER;
        private static volatile Parser<Badge> PARSER;
        public static final int VISIBLE_FIELD_NUMBER;
        private String category_ = "";
        private String description_ = "";
        private long expiration_;
        private String id_ = "";
        private String imageDensity_ = "";
        private String imageUrl_ = "";
        private String name_ = "";
        private boolean visible_;

        private Badge() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public String getId() {
            return this.id_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public ByteString getIdBytes() {
            return ByteString.copyFromUtf8(this.id_);
        }

        public void setId(String str) {
            str.getClass();
            this.id_ = str;
        }

        public void clearId() {
            this.id_ = getDefaultInstance().getId();
        }

        public void setIdBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.id_ = byteString.toStringUtf8();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public String getCategory() {
            return this.category_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public ByteString getCategoryBytes() {
            return ByteString.copyFromUtf8(this.category_);
        }

        public void setCategory(String str) {
            str.getClass();
            this.category_ = str;
        }

        public void clearCategory() {
            this.category_ = getDefaultInstance().getCategory();
        }

        public void setCategoryBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.category_ = byteString.toStringUtf8();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public String getName() {
            return this.name_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public ByteString getNameBytes() {
            return ByteString.copyFromUtf8(this.name_);
        }

        public void setName(String str) {
            str.getClass();
            this.name_ = str;
        }

        public void clearName() {
            this.name_ = getDefaultInstance().getName();
        }

        public void setNameBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.name_ = byteString.toStringUtf8();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public String getDescription() {
            return this.description_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public ByteString getDescriptionBytes() {
            return ByteString.copyFromUtf8(this.description_);
        }

        public void setDescription(String str) {
            str.getClass();
            this.description_ = str;
        }

        public void clearDescription() {
            this.description_ = getDefaultInstance().getDescription();
        }

        public void setDescriptionBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.description_ = byteString.toStringUtf8();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public String getImageUrl() {
            return this.imageUrl_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public ByteString getImageUrlBytes() {
            return ByteString.copyFromUtf8(this.imageUrl_);
        }

        public void setImageUrl(String str) {
            str.getClass();
            this.imageUrl_ = str;
        }

        public void clearImageUrl() {
            this.imageUrl_ = getDefaultInstance().getImageUrl();
        }

        public void setImageUrlBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.imageUrl_ = byteString.toStringUtf8();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public long getExpiration() {
            return this.expiration_;
        }

        public void setExpiration(long j) {
            this.expiration_ = j;
        }

        public void clearExpiration() {
            this.expiration_ = 0;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public boolean getVisible() {
            return this.visible_;
        }

        public void setVisible(boolean z) {
            this.visible_ = z;
        }

        public void clearVisible() {
            this.visible_ = false;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public String getImageDensity() {
            return this.imageDensity_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
        public ByteString getImageDensityBytes() {
            return ByteString.copyFromUtf8(this.imageDensity_);
        }

        public void setImageDensity(String str) {
            str.getClass();
            this.imageDensity_ = str;
        }

        public void clearImageDensity() {
            this.imageDensity_ = getDefaultInstance().getImageDensity();
        }

        public void setImageDensityBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.imageDensity_ = byteString.toStringUtf8();
        }

        public static Badge parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Badge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Badge parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Badge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Badge parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Badge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Badge parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Badge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Badge parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Badge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Badge parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Badge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Badge parseFrom(InputStream inputStream) throws IOException {
            return (Badge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Badge parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Badge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Badge parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Badge) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Badge parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Badge) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Badge parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Badge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Badge parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Badge) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Badge badge) {
            return DEFAULT_INSTANCE.createBuilder(badge);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Badge, Builder> implements BadgeOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Badge.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public String getId() {
                return ((Badge) this.instance).getId();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public ByteString getIdBytes() {
                return ((Badge) this.instance).getIdBytes();
            }

            public Builder setId(String str) {
                copyOnWrite();
                ((Badge) this.instance).setId(str);
                return this;
            }

            public Builder clearId() {
                copyOnWrite();
                ((Badge) this.instance).clearId();
                return this;
            }

            public Builder setIdBytes(ByteString byteString) {
                copyOnWrite();
                ((Badge) this.instance).setIdBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public String getCategory() {
                return ((Badge) this.instance).getCategory();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public ByteString getCategoryBytes() {
                return ((Badge) this.instance).getCategoryBytes();
            }

            public Builder setCategory(String str) {
                copyOnWrite();
                ((Badge) this.instance).setCategory(str);
                return this;
            }

            public Builder clearCategory() {
                copyOnWrite();
                ((Badge) this.instance).clearCategory();
                return this;
            }

            public Builder setCategoryBytes(ByteString byteString) {
                copyOnWrite();
                ((Badge) this.instance).setCategoryBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public String getName() {
                return ((Badge) this.instance).getName();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public ByteString getNameBytes() {
                return ((Badge) this.instance).getNameBytes();
            }

            public Builder setName(String str) {
                copyOnWrite();
                ((Badge) this.instance).setName(str);
                return this;
            }

            public Builder clearName() {
                copyOnWrite();
                ((Badge) this.instance).clearName();
                return this;
            }

            public Builder setNameBytes(ByteString byteString) {
                copyOnWrite();
                ((Badge) this.instance).setNameBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public String getDescription() {
                return ((Badge) this.instance).getDescription();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public ByteString getDescriptionBytes() {
                return ((Badge) this.instance).getDescriptionBytes();
            }

            public Builder setDescription(String str) {
                copyOnWrite();
                ((Badge) this.instance).setDescription(str);
                return this;
            }

            public Builder clearDescription() {
                copyOnWrite();
                ((Badge) this.instance).clearDescription();
                return this;
            }

            public Builder setDescriptionBytes(ByteString byteString) {
                copyOnWrite();
                ((Badge) this.instance).setDescriptionBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public String getImageUrl() {
                return ((Badge) this.instance).getImageUrl();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public ByteString getImageUrlBytes() {
                return ((Badge) this.instance).getImageUrlBytes();
            }

            public Builder setImageUrl(String str) {
                copyOnWrite();
                ((Badge) this.instance).setImageUrl(str);
                return this;
            }

            public Builder clearImageUrl() {
                copyOnWrite();
                ((Badge) this.instance).clearImageUrl();
                return this;
            }

            public Builder setImageUrlBytes(ByteString byteString) {
                copyOnWrite();
                ((Badge) this.instance).setImageUrlBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public long getExpiration() {
                return ((Badge) this.instance).getExpiration();
            }

            public Builder setExpiration(long j) {
                copyOnWrite();
                ((Badge) this.instance).setExpiration(j);
                return this;
            }

            public Builder clearExpiration() {
                copyOnWrite();
                ((Badge) this.instance).clearExpiration();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public boolean getVisible() {
                return ((Badge) this.instance).getVisible();
            }

            public Builder setVisible(boolean z) {
                copyOnWrite();
                ((Badge) this.instance).setVisible(z);
                return this;
            }

            public Builder clearVisible() {
                copyOnWrite();
                ((Badge) this.instance).clearVisible();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public String getImageDensity() {
                return ((Badge) this.instance).getImageDensity();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.BadgeOrBuilder
            public ByteString getImageDensityBytes() {
                return ((Badge) this.instance).getImageDensityBytes();
            }

            public Builder setImageDensity(String str) {
                copyOnWrite();
                ((Badge) this.instance).setImageDensity(str);
                return this;
            }

            public Builder clearImageDensity() {
                copyOnWrite();
                ((Badge) this.instance).clearImageDensity();
                return this;
            }

            public Builder setImageDensityBytes(ByteString byteString) {
                copyOnWrite();
                ((Badge) this.instance).setImageDensityBytes(byteString);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Badge();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\b\u0000\u0000\u0001\b\b\u0000\u0000\u0000\u0001Ȉ\u0002Ȉ\u0003Ȉ\u0004Ȉ\u0005Ȉ\u0006\u0003\u0007\u0007\bȈ", new Object[]{"id_", "category_", "name_", "description_", "imageUrl_", "expiration_", "visible_", "imageDensity_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Badge> parser = PARSER;
                    if (parser == null) {
                        synchronized (Badge.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Badge badge = new Badge();
            DEFAULT_INSTANCE = badge;
            GeneratedMessageLite.registerDefaultInstance(Badge.class, badge);
        }

        public static Badge getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Badge> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeListOrBuilder
    public List<Badge> getBadgesList() {
        return this.badges_;
    }

    public List<? extends BadgeOrBuilder> getBadgesOrBuilderList() {
        return this.badges_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeListOrBuilder
    public int getBadgesCount() {
        return this.badges_.size();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeListOrBuilder
    public Badge getBadges(int i) {
        return this.badges_.get(i);
    }

    public BadgeOrBuilder getBadgesOrBuilder(int i) {
        return this.badges_.get(i);
    }

    private void ensureBadgesIsMutable() {
        Internal.ProtobufList<Badge> protobufList = this.badges_;
        if (!protobufList.isModifiable()) {
            this.badges_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setBadges(int i, Badge badge) {
        badge.getClass();
        ensureBadgesIsMutable();
        this.badges_.set(i, badge);
    }

    public void addBadges(Badge badge) {
        badge.getClass();
        ensureBadgesIsMutable();
        this.badges_.add(badge);
    }

    public void addBadges(int i, Badge badge) {
        badge.getClass();
        ensureBadgesIsMutable();
        this.badges_.add(i, badge);
    }

    public void addAllBadges(Iterable<? extends Badge> iterable) {
        ensureBadgesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.badges_);
    }

    public void clearBadges() {
        this.badges_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeBadges(int i) {
        ensureBadgesIsMutable();
        this.badges_.remove(i);
    }

    public static BadgeList parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (BadgeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static BadgeList parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BadgeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static BadgeList parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (BadgeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static BadgeList parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BadgeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static BadgeList parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (BadgeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static BadgeList parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BadgeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static BadgeList parseFrom(InputStream inputStream) throws IOException {
        return (BadgeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static BadgeList parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BadgeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static BadgeList parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (BadgeList) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static BadgeList parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BadgeList) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static BadgeList parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (BadgeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static BadgeList parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BadgeList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(BadgeList badgeList) {
        return DEFAULT_INSTANCE.createBuilder(badgeList);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<BadgeList, Builder> implements BadgeListOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(BadgeList.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeListOrBuilder
        public List<Badge> getBadgesList() {
            return Collections.unmodifiableList(((BadgeList) this.instance).getBadgesList());
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeListOrBuilder
        public int getBadgesCount() {
            return ((BadgeList) this.instance).getBadgesCount();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.BadgeListOrBuilder
        public Badge getBadges(int i) {
            return ((BadgeList) this.instance).getBadges(i);
        }

        public Builder setBadges(int i, Badge badge) {
            copyOnWrite();
            ((BadgeList) this.instance).setBadges(i, badge);
            return this;
        }

        public Builder setBadges(int i, Badge.Builder builder) {
            copyOnWrite();
            ((BadgeList) this.instance).setBadges(i, builder.build());
            return this;
        }

        public Builder addBadges(Badge badge) {
            copyOnWrite();
            ((BadgeList) this.instance).addBadges(badge);
            return this;
        }

        public Builder addBadges(int i, Badge badge) {
            copyOnWrite();
            ((BadgeList) this.instance).addBadges(i, badge);
            return this;
        }

        public Builder addBadges(Badge.Builder builder) {
            copyOnWrite();
            ((BadgeList) this.instance).addBadges(builder.build());
            return this;
        }

        public Builder addBadges(int i, Badge.Builder builder) {
            copyOnWrite();
            ((BadgeList) this.instance).addBadges(i, builder.build());
            return this;
        }

        public Builder addAllBadges(Iterable<? extends Badge> iterable) {
            copyOnWrite();
            ((BadgeList) this.instance).addAllBadges(iterable);
            return this;
        }

        public Builder clearBadges() {
            copyOnWrite();
            ((BadgeList) this.instance).clearBadges();
            return this;
        }

        public Builder removeBadges(int i) {
            copyOnWrite();
            ((BadgeList) this.instance).removeBadges(i);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new BadgeList();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"badges_", Badge.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<BadgeList> parser = PARSER;
                if (parser == null) {
                    synchronized (BadgeList.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        BadgeList badgeList = new BadgeList();
        DEFAULT_INSTANCE = badgeList;
        GeneratedMessageLite.registerDefaultInstance(BadgeList.class, badgeList);
    }

    public static BadgeList getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<BadgeList> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
