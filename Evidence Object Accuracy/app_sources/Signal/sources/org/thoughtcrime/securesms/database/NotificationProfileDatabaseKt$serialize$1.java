package org.thoughtcrime.securesms.database;

import j$.time.DayOfWeek;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: NotificationProfileDatabase.kt */
@Metadata(bv = {}, d1 = {"\u0000\f\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"j$/time/DayOfWeek", "it", "", "invoke", "(Lj$/time/DayOfWeek;)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class NotificationProfileDatabaseKt$serialize$1 extends Lambda implements Function1<DayOfWeek, CharSequence> {
    public static final NotificationProfileDatabaseKt$serialize$1 INSTANCE = new NotificationProfileDatabaseKt$serialize$1();

    NotificationProfileDatabaseKt$serialize$1() {
        super(1);
    }

    public final CharSequence invoke(DayOfWeek dayOfWeek) {
        Intrinsics.checkNotNullParameter(dayOfWeek, "it");
        return NotificationProfileDatabaseKt.serialize(dayOfWeek);
    }
}
