package org.thoughtcrime.securesms.database.model;

import io.reactivex.rxjava3.core.ObservableEmitter;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoryViewState$Companion$$ExternalSyntheticLambda3 implements DatabaseObserver.Observer {
    public final /* synthetic */ ObservableEmitter f$0;
    public final /* synthetic */ RecipientId f$1;

    public /* synthetic */ StoryViewState$Companion$$ExternalSyntheticLambda3(ObservableEmitter observableEmitter, RecipientId recipientId) {
        this.f$0 = observableEmitter;
        this.f$1 = recipientId;
    }

    @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
    public final void onChanged() {
        StoryViewState.Companion.m1747getState$lambda8$lambda6(this.f$0, this.f$1);
    }
}
