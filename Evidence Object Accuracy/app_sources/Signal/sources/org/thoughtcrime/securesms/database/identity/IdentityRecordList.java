package org.thoughtcrime.securesms.database.identity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class IdentityRecordList {
    public static final IdentityRecordList EMPTY = new IdentityRecordList(Collections.emptyList());
    private final List<IdentityRecord> identityRecords;
    private final boolean isUnverified;
    private final boolean isVerified;

    public IdentityRecordList(Collection<IdentityRecord> collection) {
        ArrayList arrayList = new ArrayList(collection);
        this.identityRecords = arrayList;
        this.isVerified = isVerified(arrayList);
        this.isUnverified = isUnverified(arrayList);
    }

    public List<IdentityRecord> getIdentityRecords() {
        return Collections.unmodifiableList(this.identityRecords);
    }

    public boolean isVerified() {
        return this.isVerified;
    }

    public boolean isUnverified() {
        return this.isUnverified;
    }

    private static boolean isVerified(Collection<IdentityRecord> collection) {
        for (IdentityRecord identityRecord : collection) {
            if (identityRecord.getVerifiedStatus() != IdentityDatabase.VerifiedStatus.VERIFIED) {
                return false;
            }
        }
        if (collection.size() > 0) {
            return true;
        }
        return false;
    }

    private static boolean isUnverified(Collection<IdentityRecord> collection) {
        for (IdentityRecord identityRecord : collection) {
            if (identityRecord.getVerifiedStatus() == IdentityDatabase.VerifiedStatus.UNVERIFIED) {
                return true;
            }
        }
        return false;
    }

    public boolean isUnverified(boolean z) {
        for (IdentityRecord identityRecord : this.identityRecords) {
            if (!z || !identityRecord.isFirstUse()) {
                if (identityRecord.getVerifiedStatus() == IdentityDatabase.VerifiedStatus.UNVERIFIED) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isUntrusted(boolean z) {
        for (IdentityRecord identityRecord : this.identityRecords) {
            if (!z || !identityRecord.isFirstUse()) {
                if (isUntrusted(identityRecord)) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<IdentityRecord> getUntrustedRecords() {
        ArrayList arrayList = new ArrayList(this.identityRecords.size());
        for (IdentityRecord identityRecord : this.identityRecords) {
            if (isUntrusted(identityRecord)) {
                arrayList.add(identityRecord);
            }
        }
        return arrayList;
    }

    public List<Recipient> getUntrustedRecipients() {
        ArrayList arrayList = new ArrayList(this.identityRecords.size());
        for (IdentityRecord identityRecord : this.identityRecords) {
            if (isUntrusted(identityRecord)) {
                arrayList.add(Recipient.resolved(identityRecord.getRecipientId()));
            }
        }
        return arrayList;
    }

    public List<IdentityRecord> getUnverifiedRecords() {
        ArrayList arrayList = new ArrayList(this.identityRecords.size());
        for (IdentityRecord identityRecord : this.identityRecords) {
            if (identityRecord.getVerifiedStatus() == IdentityDatabase.VerifiedStatus.UNVERIFIED) {
                arrayList.add(identityRecord);
            }
        }
        return arrayList;
    }

    public List<Recipient> getUnverifiedRecipients() {
        ArrayList arrayList = new ArrayList(this.identityRecords.size());
        for (IdentityRecord identityRecord : this.identityRecords) {
            if (identityRecord.getVerifiedStatus() == IdentityDatabase.VerifiedStatus.UNVERIFIED) {
                arrayList.add(Recipient.resolved(identityRecord.getRecipientId()));
            }
        }
        return arrayList;
    }

    private static boolean isUntrusted(IdentityRecord identityRecord) {
        return !identityRecord.isApprovedNonBlocking() && System.currentTimeMillis() - identityRecord.getTimestamp() < TimeUnit.SECONDS.toMillis(5);
    }
}
