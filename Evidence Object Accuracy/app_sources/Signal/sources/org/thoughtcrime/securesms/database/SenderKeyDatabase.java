package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.groups.state.SenderKeyRecord;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.whispersystems.signalservice.api.push.DistributionId;

/* loaded from: classes4.dex */
public class SenderKeyDatabase extends Database {
    public static final String ADDRESS;
    public static final String CREATED_AT;
    public static final String CREATE_TABLE;
    public static final String DEVICE;
    public static final String DISTRIBUTION_ID;
    private static final String ID;
    public static final String RECORD;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(SenderKeyDatabase.class);

    public SenderKeyDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public void store(SignalProtocolAddress signalProtocolAddress, DistributionId distributionId, SenderKeyRecord senderKeyRecord) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("record", senderKeyRecord.serialize());
            if (signalWritableDatabase.update(TABLE_NAME, contentValues, "address = ? AND device = ? AND distribution_id = ?", SqlUtil.buildArgs(signalProtocolAddress.getName(), Integer.valueOf(signalProtocolAddress.getDeviceId()), distributionId)) <= 0) {
                String str = TAG;
                Log.d(str, "New sender key " + distributionId + " from " + signalProtocolAddress);
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("address", signalProtocolAddress.getName());
                contentValues2.put("device", Integer.valueOf(signalProtocolAddress.getDeviceId()));
                contentValues2.put("distribution_id", distributionId.toString());
                contentValues2.put("record", senderKeyRecord.serialize());
                contentValues2.put("created_at", Long.valueOf(System.currentTimeMillis()));
                signalWritableDatabase.insertWithOnConflict(TABLE_NAME, null, contentValues2, 5);
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public SenderKeyRecord load(SignalProtocolAddress signalProtocolAddress, DistributionId distributionId) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{"record"}, "address = ? AND device = ? AND distribution_id = ?", SqlUtil.buildArgs(signalProtocolAddress.getName(), Integer.valueOf(signalProtocolAddress.getDeviceId()), distributionId), null, null, null);
        try {
            if (query.moveToFirst()) {
                try {
                    SenderKeyRecord senderKeyRecord = new SenderKeyRecord(CursorUtil.requireBlob(query, "record"));
                    query.close();
                    return senderKeyRecord;
                } catch (InvalidMessageException e) {
                    Log.w(TAG, e);
                }
            }
            query.close();
            return null;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public long getCreatedTime(SignalProtocolAddress signalProtocolAddress, DistributionId distributionId) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{"created_at"}, "address = ? AND device = ? AND distribution_id = ?", SqlUtil.buildArgs(signalProtocolAddress.getName(), Integer.valueOf(signalProtocolAddress.getDeviceId()), distributionId), null, null, null);
        try {
            if (query.moveToFirst()) {
                long requireLong = CursorUtil.requireLong(query, "created_at");
                query.close();
                return requireLong;
            }
            query.close();
            return -1;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public void deleteAllFor(String str, DistributionId distributionId) {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, "address = ? AND distribution_id = ?", SqlUtil.buildArgs(str, distributionId));
    }

    public Cursor getAllCreatedBySelf() {
        return this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{"_id", "distribution_id", "created_at"}, "address = ?", SqlUtil.buildArgs(SignalStore.account().requireAci()), null, null, "created_at DESC");
    }

    public void deleteAll() {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, (String) null, (String[]) null);
    }
}
