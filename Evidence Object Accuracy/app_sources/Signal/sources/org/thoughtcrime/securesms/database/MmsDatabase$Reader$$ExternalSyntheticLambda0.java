package org.thoughtcrime.securesms.database;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MmsDatabase$Reader$$ExternalSyntheticLambda0 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return MmsDatabase.Reader.lambda$getMediaMmsMessageRecord$0((LinkPreview) obj);
    }
}
