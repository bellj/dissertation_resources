package org.thoughtcrime.securesms.database.model;

import android.net.Uri;
import j$.util.Optional;
import java.util.Arrays;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.CdsDatabase;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceId;

/* compiled from: RecipientRecord.kt */
@Metadata(bv = {}, d1 = {"\u0000Ï\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0003\b¥\u0001\b\b\u0018\u00002\u00020\u0001:\u0002û\u0001Bê\u0003\u0012\u0006\u0010Y\u001a\u00020\r\u0012\b\u0010Z\u001a\u0004\u0018\u00010\b\u0012\b\u0010[\u001a\u0004\u0018\u00010\u0010\u0012\b\u0010\\\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010]\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010^\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010_\u001a\u0004\u0018\u00010\u0016\u0012\b\u0010`\u001a\u0004\u0018\u00010\u0018\u0012\u0006\u0010a\u001a\u00020\u001a\u0012\u0006\u0010b\u001a\u00020\u0006\u0012\u0006\u0010c\u001a\u00020\u001d\u0012\u0006\u0010d\u001a\u00020\u001f\u0012\u0006\u0010e\u001a\u00020\u001f\u0012\b\u0010f\u001a\u0004\u0018\u00010\"\u0012\b\u0010g\u001a\u0004\u0018\u00010\"\u0012\u0006\u0010h\u001a\u00020\u0002\u0012\u0006\u0010i\u001a\u00020\u0002\u0012\u0006\u0010j\u001a\u00020&\u0012\b\u0010k\u001a\u0004\u0018\u00010(\u0012\b\u0010l\u001a\u0004\u0018\u00010*\u0012\u0006\u0010m\u001a\u00020,\u0012\b\u0010n\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010o\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010p\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010q\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010r\u001a\u00020,\u0012\b\u0010s\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010t\u001a\u000204\u0012\u0006\u0010u\u001a\u00020\u0006\u0012\u0006\u0010v\u001a\u00020\u001d\u0012\b\u0010w\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010x\u001a\u000209\u0012\u0006\u0010y\u001a\u00020\u0006\u0012\u0006\u0010z\u001a\u00020\u001d\u0012\u0006\u0010{\u001a\u00020=\u0012\u0006\u0010|\u001a\u00020=\u0012\u0006\u0010}\u001a\u00020=\u0012\u0006\u0010~\u001a\u00020=\u0012\u0006\u0010\u001a\u00020=\u0012\u0007\u0010\u0001\u001a\u00020=\u0012\u0007\u0010\u0001\u001a\u00020D\u0012\t\u0010\u0001\u001a\u0004\u0018\u00010(\u0012\u0007\u0010\u0001\u001a\u00020G\u0012\t\u0010\u0001\u001a\u0004\u0018\u00010I\u0012\t\u0010\u0001\u001a\u0004\u0018\u00010K\u0012\u0007\u0010\u0001\u001a\u00020M\u0012\t\u0010\u0001\u001a\u0004\u0018\u00010\u0012\u0012\t\u0010\u0001\u001a\u0004\u0018\u00010\u0012\u0012\u0007\u0010\u0001\u001a\u00020Q\u0012\t\u0010\u0001\u001a\u0004\u0018\u00010S\u0012\u0007\u0010\u0001\u001a\u00020\u0006\u0012\r\u0010\u0001\u001a\b\u0012\u0004\u0012\u00020W0V¢\u0006\u0006\bù\u0001\u0010ú\u0001J\t\u0010\u0003\u001a\u00020\u0002HÂ\u0003J\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00020\u0004J\u0006\u0010\u0007\u001a\u00020\u0006J\u000e\u0010\n\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\bJ\u0006\u0010\u000b\u001a\u00020\u0006J\u0006\u0010\f\u001a\u00020\u0006J\t\u0010\u000e\u001a\u00020\rHÆ\u0003J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\bHÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÆ\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÆ\u0003J\t\u0010\u001b\u001a\u00020\u001aHÆ\u0003J\t\u0010\u001c\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001e\u001a\u00020\u001dHÆ\u0003J\t\u0010 \u001a\u00020\u001fHÆ\u0003J\t\u0010!\u001a\u00020\u001fHÆ\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\"HÆ\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\"HÆ\u0003J\t\u0010%\u001a\u00020\u0002HÆ\u0003J\t\u0010'\u001a\u00020&HÆ\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010(HÆ\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010*HÆ\u0003J\t\u0010-\u001a\u00020,HÆ\u0003J\u000b\u0010.\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\u000b\u0010/\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\u000b\u00100\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\u000b\u00101\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\t\u00102\u001a\u00020,HÆ\u0003J\u000b\u00103\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\t\u00105\u001a\u000204HÆ\u0003J\t\u00106\u001a\u00020\u0006HÆ\u0003J\t\u00107\u001a\u00020\u001dHÆ\u0003J\u000b\u00108\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\t\u0010:\u001a\u000209HÆ\u0003J\t\u0010;\u001a\u00020\u0006HÆ\u0003J\t\u0010<\u001a\u00020\u001dHÆ\u0003J\t\u0010>\u001a\u00020=HÆ\u0003J\t\u0010?\u001a\u00020=HÆ\u0003J\t\u0010@\u001a\u00020=HÆ\u0003J\t\u0010A\u001a\u00020=HÆ\u0003J\t\u0010B\u001a\u00020=HÆ\u0003J\t\u0010C\u001a\u00020=HÆ\u0003J\t\u0010E\u001a\u00020DHÆ\u0003J\u000b\u0010F\u001a\u0004\u0018\u00010(HÆ\u0003J\t\u0010H\u001a\u00020GHÆ\u0003J\u000b\u0010J\u001a\u0004\u0018\u00010IHÆ\u0003J\u000b\u0010L\u001a\u0004\u0018\u00010KHÆ\u0003J\t\u0010N\u001a\u00020MHÆ\u0003J\u000b\u0010O\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\u000b\u0010P\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\t\u0010R\u001a\u00020QHÆ\u0003J\u000b\u0010T\u001a\u0004\u0018\u00010SHÆ\u0003J\t\u0010U\u001a\u00020\u0006HÆ\u0003J\u000f\u0010X\u001a\b\u0012\u0004\u0012\u00020W0VHÆ\u0003JÓ\u0004\u0010\u0001\u001a\u00020\u00002\b\b\u0002\u0010Y\u001a\u00020\r2\n\b\u0002\u0010Z\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010[\u001a\u0004\u0018\u00010\u00102\n\b\u0002\u0010\\\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010]\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010^\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010_\u001a\u0004\u0018\u00010\u00162\n\b\u0002\u0010`\u001a\u0004\u0018\u00010\u00182\b\b\u0002\u0010a\u001a\u00020\u001a2\b\b\u0002\u0010b\u001a\u00020\u00062\b\b\u0002\u0010c\u001a\u00020\u001d2\b\b\u0002\u0010d\u001a\u00020\u001f2\b\b\u0002\u0010e\u001a\u00020\u001f2\n\b\u0002\u0010f\u001a\u0004\u0018\u00010\"2\n\b\u0002\u0010g\u001a\u0004\u0018\u00010\"2\b\b\u0002\u0010h\u001a\u00020\u00022\b\b\u0002\u0010i\u001a\u00020\u00022\b\b\u0002\u0010j\u001a\u00020&2\n\b\u0002\u0010k\u001a\u0004\u0018\u00010(2\n\b\u0002\u0010l\u001a\u0004\u0018\u00010*2\b\b\u0002\u0010m\u001a\u00020,2\n\b\u0002\u0010n\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010o\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010p\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010q\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u0010r\u001a\u00020,2\n\b\u0002\u0010s\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u0010t\u001a\u0002042\b\b\u0002\u0010u\u001a\u00020\u00062\b\b\u0002\u0010v\u001a\u00020\u001d2\n\b\u0002\u0010w\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u0010x\u001a\u0002092\b\b\u0002\u0010y\u001a\u00020\u00062\b\b\u0002\u0010z\u001a\u00020\u001d2\b\b\u0002\u0010{\u001a\u00020=2\b\b\u0002\u0010|\u001a\u00020=2\b\b\u0002\u0010}\u001a\u00020=2\b\b\u0002\u0010~\u001a\u00020=2\b\b\u0002\u0010\u001a\u00020=2\t\b\u0002\u0010\u0001\u001a\u00020=2\t\b\u0002\u0010\u0001\u001a\u00020D2\u000b\b\u0002\u0010\u0001\u001a\u0004\u0018\u00010(2\t\b\u0002\u0010\u0001\u001a\u00020G2\u000b\b\u0002\u0010\u0001\u001a\u0004\u0018\u00010I2\u000b\b\u0002\u0010\u0001\u001a\u0004\u0018\u00010K2\t\b\u0002\u0010\u0001\u001a\u00020M2\u000b\b\u0002\u0010\u0001\u001a\u0004\u0018\u00010\u00122\u000b\b\u0002\u0010\u0001\u001a\u0004\u0018\u00010\u00122\t\b\u0002\u0010\u0001\u001a\u00020Q2\u000b\b\u0002\u0010\u0001\u001a\u0004\u0018\u00010S2\t\b\u0002\u0010\u0001\u001a\u00020\u00062\u000f\b\u0002\u0010\u0001\u001a\b\u0012\u0004\u0012\u00020W0VHÆ\u0001J\n\u0010\u0001\u001a\u00020\u0012HÖ\u0001J\n\u0010\u0001\u001a\u00020\u0002HÖ\u0001J\u0015\u0010\u0001\u001a\u00020\u00062\t\u0010\u0001\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u001a\u0010Y\u001a\u00020\r8\u0006¢\u0006\u000f\n\u0005\bY\u0010\u0001\u001a\u0006\b\u0001\u0010\u0001R\u001c\u0010Z\u001a\u0004\u0018\u00010\b8\u0006¢\u0006\u000f\n\u0005\bZ\u0010\u0001\u001a\u0006\b\u0001\u0010\u0001R\u001c\u0010[\u001a\u0004\u0018\u00010\u00108\u0006¢\u0006\u000f\n\u0005\b[\u0010\u0001\u001a\u0006\b\u0001\u0010\u0001R\u001c\u0010\\\u001a\u0004\u0018\u00010\u00128\u0006¢\u0006\u000f\n\u0005\b\\\u0010\u0001\u001a\u0006\b\u0001\u0010\u0001R\u001c\u0010]\u001a\u0004\u0018\u00010\u00128\u0006¢\u0006\u000f\n\u0005\b]\u0010\u0001\u001a\u0006\b\u0001\u0010\u0001R\u001c\u0010^\u001a\u0004\u0018\u00010\u00128\u0006¢\u0006\u000f\n\u0005\b^\u0010\u0001\u001a\u0006\b\u0001\u0010\u0001R\u001c\u0010_\u001a\u0004\u0018\u00010\u00168\u0006¢\u0006\u000f\n\u0005\b_\u0010 \u0001\u001a\u0006\b¡\u0001\u0010¢\u0001R\u001c\u0010`\u001a\u0004\u0018\u00010\u00188\u0006¢\u0006\u000f\n\u0005\b`\u0010£\u0001\u001a\u0006\b¤\u0001\u0010¥\u0001R\u001a\u0010a\u001a\u00020\u001a8\u0006¢\u0006\u000f\n\u0005\ba\u0010¦\u0001\u001a\u0006\b§\u0001\u0010¨\u0001R\u0019\u0010b\u001a\u00020\u00068\u0006¢\u0006\u000e\n\u0005\bb\u0010©\u0001\u001a\u0005\bb\u0010ª\u0001R\u001a\u0010c\u001a\u00020\u001d8\u0006¢\u0006\u000f\n\u0005\bc\u0010«\u0001\u001a\u0006\b¬\u0001\u0010­\u0001R\u001a\u0010d\u001a\u00020\u001f8\u0006¢\u0006\u000f\n\u0005\bd\u0010®\u0001\u001a\u0006\b¯\u0001\u0010°\u0001R\u001a\u0010e\u001a\u00020\u001f8\u0006¢\u0006\u000f\n\u0005\be\u0010®\u0001\u001a\u0006\b±\u0001\u0010°\u0001R\u001c\u0010f\u001a\u0004\u0018\u00010\"8\u0006¢\u0006\u000f\n\u0005\bf\u0010²\u0001\u001a\u0006\b³\u0001\u0010´\u0001R\u001c\u0010g\u001a\u0004\u0018\u00010\"8\u0006¢\u0006\u000f\n\u0005\bg\u0010²\u0001\u001a\u0006\bµ\u0001\u0010´\u0001R\u0015\u0010h\u001a\u00020\u00028\u0002X\u0004¢\u0006\u0007\n\u0005\bh\u0010¶\u0001R\u001a\u0010i\u001a\u00020\u00028\u0006¢\u0006\u000f\n\u0005\bi\u0010¶\u0001\u001a\u0006\b·\u0001\u0010¸\u0001R\u001a\u0010j\u001a\u00020&8\u0006¢\u0006\u000f\n\u0005\bj\u0010¹\u0001\u001a\u0006\bº\u0001\u0010»\u0001R\u001c\u0010k\u001a\u0004\u0018\u00010(8\u0006¢\u0006\u000f\n\u0005\bk\u0010¼\u0001\u001a\u0006\b½\u0001\u0010¾\u0001R\u001c\u0010l\u001a\u0004\u0018\u00010*8\u0006¢\u0006\u000f\n\u0005\bl\u0010¿\u0001\u001a\u0006\bÀ\u0001\u0010Á\u0001R\u001a\u0010m\u001a\u00020,8\u0006¢\u0006\u000f\n\u0005\bm\u0010Â\u0001\u001a\u0006\bÃ\u0001\u0010Ä\u0001R\u001c\u0010n\u001a\u0004\u0018\u00010\u00128\u0006¢\u0006\u000f\n\u0005\bn\u0010\u0001\u001a\u0006\bÅ\u0001\u0010\u0001R\u001c\u0010o\u001a\u0004\u0018\u00010\u00128\u0006¢\u0006\u000f\n\u0005\bo\u0010\u0001\u001a\u0006\bÆ\u0001\u0010\u0001R\u001c\u0010p\u001a\u0004\u0018\u00010\u00128\u0006¢\u0006\u000f\n\u0005\bp\u0010\u0001\u001a\u0006\bÇ\u0001\u0010\u0001R\u001c\u0010q\u001a\u0004\u0018\u00010\u00128\u0006¢\u0006\u000f\n\u0005\bq\u0010\u0001\u001a\u0006\bÈ\u0001\u0010\u0001R\u001a\u0010r\u001a\u00020,8\u0007¢\u0006\u000f\n\u0005\br\u0010Â\u0001\u001a\u0006\bÉ\u0001\u0010Ä\u0001R\u001c\u0010s\u001a\u0004\u0018\u00010\u00128\u0007¢\u0006\u000f\n\u0005\bs\u0010\u0001\u001a\u0006\bÊ\u0001\u0010\u0001R\u001a\u0010t\u001a\u0002048\u0006¢\u0006\u000f\n\u0005\bt\u0010Ë\u0001\u001a\u0006\bÌ\u0001\u0010Í\u0001R\u001a\u0010u\u001a\u00020\u00068\u0007¢\u0006\u000f\n\u0005\bu\u0010©\u0001\u001a\u0006\bÎ\u0001\u0010ª\u0001R\u001a\u0010v\u001a\u00020\u001d8\u0006¢\u0006\u000f\n\u0005\bv\u0010«\u0001\u001a\u0006\bÏ\u0001\u0010­\u0001R\u001c\u0010w\u001a\u0004\u0018\u00010\u00128\u0006¢\u0006\u000f\n\u0005\bw\u0010\u0001\u001a\u0006\bÐ\u0001\u0010\u0001R\u001a\u0010x\u001a\u0002098\u0006¢\u0006\u000f\n\u0005\bx\u0010Ñ\u0001\u001a\u0006\bÒ\u0001\u0010Ó\u0001R\u001a\u0010y\u001a\u00020\u00068\u0007¢\u0006\u000f\n\u0005\by\u0010©\u0001\u001a\u0006\bÔ\u0001\u0010ª\u0001R\u001a\u0010z\u001a\u00020\u001d8\u0006¢\u0006\u000f\n\u0005\bz\u0010«\u0001\u001a\u0006\bÕ\u0001\u0010­\u0001R\u001a\u0010{\u001a\u00020=8\u0006¢\u0006\u000f\n\u0005\b{\u0010Ö\u0001\u001a\u0006\b×\u0001\u0010Ø\u0001R\u001a\u0010|\u001a\u00020=8\u0006¢\u0006\u000f\n\u0005\b|\u0010Ö\u0001\u001a\u0006\bÙ\u0001\u0010Ø\u0001R\u001a\u0010}\u001a\u00020=8\u0006¢\u0006\u000f\n\u0005\b}\u0010Ö\u0001\u001a\u0006\bÚ\u0001\u0010Ø\u0001R\u001a\u0010~\u001a\u00020=8\u0006¢\u0006\u000f\n\u0005\b~\u0010Ö\u0001\u001a\u0006\bÛ\u0001\u0010Ø\u0001R\u001a\u0010\u001a\u00020=8\u0006¢\u0006\u000f\n\u0005\b\u0010Ö\u0001\u001a\u0006\bÜ\u0001\u0010Ø\u0001R\u001c\u0010\u0001\u001a\u00020=8\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010Ö\u0001\u001a\u0006\bÝ\u0001\u0010Ø\u0001R\u001c\u0010\u0001\u001a\u00020D8\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010Þ\u0001\u001a\u0006\bß\u0001\u0010à\u0001R\u001e\u0010\u0001\u001a\u0004\u0018\u00010(8\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010¼\u0001\u001a\u0006\bá\u0001\u0010¾\u0001R\u001c\u0010\u0001\u001a\u00020G8\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010â\u0001\u001a\u0006\bã\u0001\u0010ä\u0001R\u001e\u0010\u0001\u001a\u0004\u0018\u00010I8\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010å\u0001\u001a\u0006\bæ\u0001\u0010ç\u0001R\u001e\u0010\u0001\u001a\u0004\u0018\u00010K8\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010è\u0001\u001a\u0006\bé\u0001\u0010ê\u0001R\u001c\u0010\u0001\u001a\u00020M8\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010ë\u0001\u001a\u0006\bì\u0001\u0010í\u0001R\u001e\u0010\u0001\u001a\u0004\u0018\u00010\u00128\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010\u0001\u001a\u0006\bî\u0001\u0010\u0001R\u001e\u0010\u0001\u001a\u0004\u0018\u00010\u00128\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010\u0001\u001a\u0006\bï\u0001\u0010\u0001R\u001c\u0010\u0001\u001a\u00020Q8\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010ð\u0001\u001a\u0006\bñ\u0001\u0010ò\u0001R\u001e\u0010\u0001\u001a\u0004\u0018\u00010S8\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010ó\u0001\u001a\u0006\bô\u0001\u0010õ\u0001R\u001c\u0010\u0001\u001a\u00020\u00068\u0007¢\u0006\u0010\n\u0006\b\u0001\u0010©\u0001\u001a\u0006\b\u0001\u0010ª\u0001R\"\u0010\u0001\u001a\b\u0012\u0004\u0012\u00020W0V8\u0006¢\u0006\u0010\n\u0006\b\u0001\u0010ö\u0001\u001a\u0006\b÷\u0001\u0010ø\u0001¨\u0006ü\u0001"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/RecipientRecord;", "", "", "component16", "j$/util/Optional", "getDefaultSubscriptionId", "", "e164Only", "Lorg/whispersystems/signalservice/api/push/ServiceId;", "sid", "sidOnly", "sidIsPni", "pniAndAci", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "Lorg/whispersystems/signalservice/api/push/PNI;", "component3", "", "component4", "component5", "component6", "Lorg/thoughtcrime/securesms/groups/GroupId;", "component7", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "component8", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$GroupType;", "component9", "component10", "", "component11", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;", "component12", "component13", "Landroid/net/Uri;", "component14", "component15", "component17", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RegisteredState;", "component18", "", "component19", "Lorg/signal/libsignal/zkgroup/profiles/ExpiringProfileKeyCredential;", "component20", "Lorg/thoughtcrime/securesms/profiles/ProfileName;", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "Lorg/thoughtcrime/securesms/database/model/ProfileAvatarFileDetails;", "component28", "component29", "component30", "component31", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$UnidentifiedAccessMode;", "component32", "component33", "component34", "Lorg/thoughtcrime/securesms/recipients/Recipient$Capability;", "component35", "component36", "component37", "component38", "component39", "component40", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$InsightsBannerTier;", "component41", "component42", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;", "component43", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "component44", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "component45", "Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "component46", "component47", "component48", "Lorg/thoughtcrime/securesms/database/model/RecipientRecord$SyncExtras;", "component49", "Lorg/thoughtcrime/securesms/recipients/Recipient$Extras;", "component50", "component51", "", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "component52", ContactRepository.ID_COLUMN, "serviceId", RecipientDatabase.PNI_COLUMN, "username", CdsDatabase.E164, RecipientDatabase.EMAIL, "groupId", "distributionListId", "groupType", "isBlocked", "muteUntil", "messageVibrateState", "callVibrateState", "messageRingtone", "callRingtone", "defaultSubscriptionId", "expireMessages", RecipientDatabase.REGISTERED, "profileKey", "expiringProfileKeyCredential", "systemProfileName", "systemDisplayName", "systemContactPhotoUri", "systemPhoneLabel", "systemContactUri", "signalProfileName", "signalProfileAvatar", "profileAvatarFileDetails", "profileSharing", "lastProfileFetch", "notificationChannel", "unidentifiedAccessMode", "forceSmsSelection", "rawCapabilities", "groupsV1MigrationCapability", "senderKeyCapability", "announcementGroupCapability", "changeNumberCapability", "storiesCapability", "giftBadgesCapability", "insightsBannerTier", "storageId", "mentionSetting", "wallpaper", "chatColors", "avatarColor", RecipientDatabase.ABOUT, "aboutEmoji", "syncExtras", "extras", "hasGroupsInCommon", "badges", "copy", "toString", "hashCode", "other", "equals", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lorg/whispersystems/signalservice/api/push/ServiceId;", "getServiceId", "()Lorg/whispersystems/signalservice/api/push/ServiceId;", "Lorg/whispersystems/signalservice/api/push/PNI;", "getPni", "()Lorg/whispersystems/signalservice/api/push/PNI;", "Ljava/lang/String;", "getUsername", "()Ljava/lang/String;", "getE164", "getEmail", "Lorg/thoughtcrime/securesms/groups/GroupId;", "getGroupId", "()Lorg/thoughtcrime/securesms/groups/GroupId;", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getDistributionListId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$GroupType;", "getGroupType", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$GroupType;", "Z", "()Z", "J", "getMuteUntil", "()J", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;", "getMessageVibrateState", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;", "getCallVibrateState", "Landroid/net/Uri;", "getMessageRingtone", "()Landroid/net/Uri;", "getCallRingtone", "I", "getExpireMessages", "()I", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RegisteredState;", "getRegistered", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$RegisteredState;", "[B", "getProfileKey", "()[B", "Lorg/signal/libsignal/zkgroup/profiles/ExpiringProfileKeyCredential;", "getExpiringProfileKeyCredential", "()Lorg/signal/libsignal/zkgroup/profiles/ExpiringProfileKeyCredential;", "Lorg/thoughtcrime/securesms/profiles/ProfileName;", "getSystemProfileName", "()Lorg/thoughtcrime/securesms/profiles/ProfileName;", "getSystemDisplayName", "getSystemContactPhotoUri", "getSystemPhoneLabel", "getSystemContactUri", "getProfileName", "getProfileAvatar", "Lorg/thoughtcrime/securesms/database/model/ProfileAvatarFileDetails;", "getProfileAvatarFileDetails", "()Lorg/thoughtcrime/securesms/database/model/ProfileAvatarFileDetails;", "isProfileSharing", "getLastProfileFetch", "getNotificationChannel", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$UnidentifiedAccessMode;", "getUnidentifiedAccessMode", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$UnidentifiedAccessMode;", "isForceSmsSelection", "getRawCapabilities", "Lorg/thoughtcrime/securesms/recipients/Recipient$Capability;", "getGroupsV1MigrationCapability", "()Lorg/thoughtcrime/securesms/recipients/Recipient$Capability;", "getSenderKeyCapability", "getAnnouncementGroupCapability", "getChangeNumberCapability", "getStoriesCapability", "getGiftBadgesCapability", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$InsightsBannerTier;", "getInsightsBannerTier", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$InsightsBannerTier;", "getStorageId", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;", "getMentionSetting", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "getWallpaper", "()Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "getChatColors", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "getAvatarColor", "()Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "getAbout", "getAboutEmoji", "Lorg/thoughtcrime/securesms/database/model/RecipientRecord$SyncExtras;", "getSyncExtras", "()Lorg/thoughtcrime/securesms/database/model/RecipientRecord$SyncExtras;", "Lorg/thoughtcrime/securesms/recipients/Recipient$Extras;", "getExtras", "()Lorg/thoughtcrime/securesms/recipients/Recipient$Extras;", "Ljava/util/List;", "getBadges", "()Ljava/util/List;", "<init>", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/whispersystems/signalservice/api/push/ServiceId;Lorg/whispersystems/signalservice/api/push/PNI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/thoughtcrime/securesms/groups/GroupId;Lorg/thoughtcrime/securesms/database/model/DistributionListId;Lorg/thoughtcrime/securesms/database/RecipientDatabase$GroupType;ZJLorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;Landroid/net/Uri;Landroid/net/Uri;IILorg/thoughtcrime/securesms/database/RecipientDatabase$RegisteredState;[BLorg/signal/libsignal/zkgroup/profiles/ExpiringProfileKeyCredential;Lorg/thoughtcrime/securesms/profiles/ProfileName;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/thoughtcrime/securesms/profiles/ProfileName;Ljava/lang/String;Lorg/thoughtcrime/securesms/database/model/ProfileAvatarFileDetails;ZJLjava/lang/String;Lorg/thoughtcrime/securesms/database/RecipientDatabase$UnidentifiedAccessMode;ZJLorg/thoughtcrime/securesms/recipients/Recipient$Capability;Lorg/thoughtcrime/securesms/recipients/Recipient$Capability;Lorg/thoughtcrime/securesms/recipients/Recipient$Capability;Lorg/thoughtcrime/securesms/recipients/Recipient$Capability;Lorg/thoughtcrime/securesms/recipients/Recipient$Capability;Lorg/thoughtcrime/securesms/recipients/Recipient$Capability;Lorg/thoughtcrime/securesms/database/RecipientDatabase$InsightsBannerTier;[BLorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;Ljava/lang/String;Ljava/lang/String;Lorg/thoughtcrime/securesms/database/model/RecipientRecord$SyncExtras;Lorg/thoughtcrime/securesms/recipients/Recipient$Extras;ZLjava/util/List;)V", "SyncExtras", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class RecipientRecord {
    private final String about;
    private final String aboutEmoji;
    private final Recipient.Capability announcementGroupCapability;
    private final AvatarColor avatarColor;
    private final List<Badge> badges;
    private final Uri callRingtone;
    private final RecipientDatabase.VibrateState callVibrateState;
    private final Recipient.Capability changeNumberCapability;
    private final ChatColors chatColors;
    private final int defaultSubscriptionId;
    private final DistributionListId distributionListId;
    private final String e164;
    private final String email;
    private final int expireMessages;
    private final ExpiringProfileKeyCredential expiringProfileKeyCredential;
    private final Recipient.Extras extras;
    private final boolean forceSmsSelection;
    private final Recipient.Capability giftBadgesCapability;
    private final GroupId groupId;
    private final RecipientDatabase.GroupType groupType;
    private final Recipient.Capability groupsV1MigrationCapability;
    private final boolean hasGroupsInCommon;
    private final RecipientId id;
    private final RecipientDatabase.InsightsBannerTier insightsBannerTier;
    private final boolean isBlocked;
    private final long lastProfileFetch;
    private final RecipientDatabase.MentionSetting mentionSetting;
    private final Uri messageRingtone;
    private final RecipientDatabase.VibrateState messageVibrateState;
    private final long muteUntil;
    private final String notificationChannel;
    private final PNI pni;
    private final ProfileAvatarFileDetails profileAvatarFileDetails;
    private final byte[] profileKey;
    private final boolean profileSharing;
    private final long rawCapabilities;
    private final RecipientDatabase.RegisteredState registered;
    private final Recipient.Capability senderKeyCapability;
    private final ServiceId serviceId;
    private final String signalProfileAvatar;
    private final ProfileName signalProfileName;
    private final byte[] storageId;
    private final Recipient.Capability storiesCapability;
    private final SyncExtras syncExtras;
    private final String systemContactPhotoUri;
    private final String systemContactUri;
    private final String systemDisplayName;
    private final String systemPhoneLabel;
    private final ProfileName systemProfileName;
    private final RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode;
    private final String username;
    private final ChatWallpaper wallpaper;

    private final int component16() {
        return this.defaultSubscriptionId;
    }

    public final RecipientId component1() {
        return this.id;
    }

    public final boolean component10() {
        return this.isBlocked;
    }

    public final long component11() {
        return this.muteUntil;
    }

    public final RecipientDatabase.VibrateState component12() {
        return this.messageVibrateState;
    }

    public final RecipientDatabase.VibrateState component13() {
        return this.callVibrateState;
    }

    public final Uri component14() {
        return this.messageRingtone;
    }

    public final Uri component15() {
        return this.callRingtone;
    }

    public final int component17() {
        return this.expireMessages;
    }

    public final RecipientDatabase.RegisteredState component18() {
        return this.registered;
    }

    public final byte[] component19() {
        return this.profileKey;
    }

    public final ServiceId component2() {
        return this.serviceId;
    }

    public final ExpiringProfileKeyCredential component20() {
        return this.expiringProfileKeyCredential;
    }

    public final ProfileName component21() {
        return this.systemProfileName;
    }

    public final String component22() {
        return this.systemDisplayName;
    }

    public final String component23() {
        return this.systemContactPhotoUri;
    }

    public final String component24() {
        return this.systemPhoneLabel;
    }

    public final String component25() {
        return this.systemContactUri;
    }

    public final ProfileName component26() {
        return this.signalProfileName;
    }

    public final String component27() {
        return this.signalProfileAvatar;
    }

    public final ProfileAvatarFileDetails component28() {
        return this.profileAvatarFileDetails;
    }

    public final boolean component29() {
        return this.profileSharing;
    }

    public final PNI component3() {
        return this.pni;
    }

    public final long component30() {
        return this.lastProfileFetch;
    }

    public final String component31() {
        return this.notificationChannel;
    }

    public final RecipientDatabase.UnidentifiedAccessMode component32() {
        return this.unidentifiedAccessMode;
    }

    public final boolean component33() {
        return this.forceSmsSelection;
    }

    public final long component34() {
        return this.rawCapabilities;
    }

    public final Recipient.Capability component35() {
        return this.groupsV1MigrationCapability;
    }

    public final Recipient.Capability component36() {
        return this.senderKeyCapability;
    }

    public final Recipient.Capability component37() {
        return this.announcementGroupCapability;
    }

    public final Recipient.Capability component38() {
        return this.changeNumberCapability;
    }

    public final Recipient.Capability component39() {
        return this.storiesCapability;
    }

    public final String component4() {
        return this.username;
    }

    public final Recipient.Capability component40() {
        return this.giftBadgesCapability;
    }

    public final RecipientDatabase.InsightsBannerTier component41() {
        return this.insightsBannerTier;
    }

    public final byte[] component42() {
        return this.storageId;
    }

    public final RecipientDatabase.MentionSetting component43() {
        return this.mentionSetting;
    }

    public final ChatWallpaper component44() {
        return this.wallpaper;
    }

    public final ChatColors component45() {
        return this.chatColors;
    }

    public final AvatarColor component46() {
        return this.avatarColor;
    }

    public final String component47() {
        return this.about;
    }

    public final String component48() {
        return this.aboutEmoji;
    }

    public final SyncExtras component49() {
        return this.syncExtras;
    }

    public final String component5() {
        return this.e164;
    }

    public final Recipient.Extras component50() {
        return this.extras;
    }

    public final boolean component51() {
        return this.hasGroupsInCommon;
    }

    public final List<Badge> component52() {
        return this.badges;
    }

    public final String component6() {
        return this.email;
    }

    public final GroupId component7() {
        return this.groupId;
    }

    public final DistributionListId component8() {
        return this.distributionListId;
    }

    public final RecipientDatabase.GroupType component9() {
        return this.groupType;
    }

    public final RecipientRecord copy(RecipientId recipientId, ServiceId serviceId, PNI pni, String str, String str2, String str3, GroupId groupId, DistributionListId distributionListId, RecipientDatabase.GroupType groupType, boolean z, long j, RecipientDatabase.VibrateState vibrateState, RecipientDatabase.VibrateState vibrateState2, Uri uri, Uri uri2, int i, int i2, RecipientDatabase.RegisteredState registeredState, byte[] bArr, ExpiringProfileKeyCredential expiringProfileKeyCredential, ProfileName profileName, String str4, String str5, String str6, String str7, ProfileName profileName2, String str8, ProfileAvatarFileDetails profileAvatarFileDetails, boolean z2, long j2, String str9, RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode, boolean z3, long j3, Recipient.Capability capability, Recipient.Capability capability2, Recipient.Capability capability3, Recipient.Capability capability4, Recipient.Capability capability5, Recipient.Capability capability6, RecipientDatabase.InsightsBannerTier insightsBannerTier, byte[] bArr2, RecipientDatabase.MentionSetting mentionSetting, ChatWallpaper chatWallpaper, ChatColors chatColors, AvatarColor avatarColor, String str10, String str11, SyncExtras syncExtras, Recipient.Extras extras, boolean z4, List<Badge> list) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(groupType, "groupType");
        Intrinsics.checkNotNullParameter(vibrateState, "messageVibrateState");
        Intrinsics.checkNotNullParameter(vibrateState2, "callVibrateState");
        Intrinsics.checkNotNullParameter(registeredState, RecipientDatabase.REGISTERED);
        Intrinsics.checkNotNullParameter(profileName, "systemProfileName");
        Intrinsics.checkNotNullParameter(profileName2, "signalProfileName");
        Intrinsics.checkNotNullParameter(profileAvatarFileDetails, "profileAvatarFileDetails");
        Intrinsics.checkNotNullParameter(unidentifiedAccessMode, "unidentifiedAccessMode");
        Intrinsics.checkNotNullParameter(capability, "groupsV1MigrationCapability");
        Intrinsics.checkNotNullParameter(capability2, "senderKeyCapability");
        Intrinsics.checkNotNullParameter(capability3, "announcementGroupCapability");
        Intrinsics.checkNotNullParameter(capability4, "changeNumberCapability");
        Intrinsics.checkNotNullParameter(capability5, "storiesCapability");
        Intrinsics.checkNotNullParameter(capability6, "giftBadgesCapability");
        Intrinsics.checkNotNullParameter(insightsBannerTier, "insightsBannerTier");
        Intrinsics.checkNotNullParameter(mentionSetting, "mentionSetting");
        Intrinsics.checkNotNullParameter(avatarColor, "avatarColor");
        Intrinsics.checkNotNullParameter(syncExtras, "syncExtras");
        Intrinsics.checkNotNullParameter(list, "badges");
        return new RecipientRecord(recipientId, serviceId, pni, str, str2, str3, groupId, distributionListId, groupType, z, j, vibrateState, vibrateState2, uri, uri2, i, i2, registeredState, bArr, expiringProfileKeyCredential, profileName, str4, str5, str6, str7, profileName2, str8, profileAvatarFileDetails, z2, j2, str9, unidentifiedAccessMode, z3, j3, capability, capability2, capability3, capability4, capability5, capability6, insightsBannerTier, bArr2, mentionSetting, chatWallpaper, chatColors, avatarColor, str10, str11, syncExtras, extras, z4, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RecipientRecord)) {
            return false;
        }
        RecipientRecord recipientRecord = (RecipientRecord) obj;
        return Intrinsics.areEqual(this.id, recipientRecord.id) && Intrinsics.areEqual(this.serviceId, recipientRecord.serviceId) && Intrinsics.areEqual(this.pni, recipientRecord.pni) && Intrinsics.areEqual(this.username, recipientRecord.username) && Intrinsics.areEqual(this.e164, recipientRecord.e164) && Intrinsics.areEqual(this.email, recipientRecord.email) && Intrinsics.areEqual(this.groupId, recipientRecord.groupId) && Intrinsics.areEqual(this.distributionListId, recipientRecord.distributionListId) && this.groupType == recipientRecord.groupType && this.isBlocked == recipientRecord.isBlocked && this.muteUntil == recipientRecord.muteUntil && this.messageVibrateState == recipientRecord.messageVibrateState && this.callVibrateState == recipientRecord.callVibrateState && Intrinsics.areEqual(this.messageRingtone, recipientRecord.messageRingtone) && Intrinsics.areEqual(this.callRingtone, recipientRecord.callRingtone) && this.defaultSubscriptionId == recipientRecord.defaultSubscriptionId && this.expireMessages == recipientRecord.expireMessages && this.registered == recipientRecord.registered && Intrinsics.areEqual(this.profileKey, recipientRecord.profileKey) && Intrinsics.areEqual(this.expiringProfileKeyCredential, recipientRecord.expiringProfileKeyCredential) && Intrinsics.areEqual(this.systemProfileName, recipientRecord.systemProfileName) && Intrinsics.areEqual(this.systemDisplayName, recipientRecord.systemDisplayName) && Intrinsics.areEqual(this.systemContactPhotoUri, recipientRecord.systemContactPhotoUri) && Intrinsics.areEqual(this.systemPhoneLabel, recipientRecord.systemPhoneLabel) && Intrinsics.areEqual(this.systemContactUri, recipientRecord.systemContactUri) && Intrinsics.areEqual(this.signalProfileName, recipientRecord.signalProfileName) && Intrinsics.areEqual(this.signalProfileAvatar, recipientRecord.signalProfileAvatar) && Intrinsics.areEqual(this.profileAvatarFileDetails, recipientRecord.profileAvatarFileDetails) && this.profileSharing == recipientRecord.profileSharing && this.lastProfileFetch == recipientRecord.lastProfileFetch && Intrinsics.areEqual(this.notificationChannel, recipientRecord.notificationChannel) && this.unidentifiedAccessMode == recipientRecord.unidentifiedAccessMode && this.forceSmsSelection == recipientRecord.forceSmsSelection && this.rawCapabilities == recipientRecord.rawCapabilities && this.groupsV1MigrationCapability == recipientRecord.groupsV1MigrationCapability && this.senderKeyCapability == recipientRecord.senderKeyCapability && this.announcementGroupCapability == recipientRecord.announcementGroupCapability && this.changeNumberCapability == recipientRecord.changeNumberCapability && this.storiesCapability == recipientRecord.storiesCapability && this.giftBadgesCapability == recipientRecord.giftBadgesCapability && this.insightsBannerTier == recipientRecord.insightsBannerTier && Intrinsics.areEqual(this.storageId, recipientRecord.storageId) && this.mentionSetting == recipientRecord.mentionSetting && Intrinsics.areEqual(this.wallpaper, recipientRecord.wallpaper) && Intrinsics.areEqual(this.chatColors, recipientRecord.chatColors) && this.avatarColor == recipientRecord.avatarColor && Intrinsics.areEqual(this.about, recipientRecord.about) && Intrinsics.areEqual(this.aboutEmoji, recipientRecord.aboutEmoji) && Intrinsics.areEqual(this.syncExtras, recipientRecord.syncExtras) && Intrinsics.areEqual(this.extras, recipientRecord.extras) && this.hasGroupsInCommon == recipientRecord.hasGroupsInCommon && Intrinsics.areEqual(this.badges, recipientRecord.badges);
    }

    public int hashCode() {
        int hashCode = this.id.hashCode() * 31;
        ServiceId serviceId = this.serviceId;
        int i = 0;
        int hashCode2 = (hashCode + (serviceId == null ? 0 : serviceId.hashCode())) * 31;
        PNI pni = this.pni;
        int hashCode3 = (hashCode2 + (pni == null ? 0 : pni.hashCode())) * 31;
        String str = this.username;
        int hashCode4 = (hashCode3 + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.e164;
        int hashCode5 = (hashCode4 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.email;
        int hashCode6 = (hashCode5 + (str3 == null ? 0 : str3.hashCode())) * 31;
        GroupId groupId = this.groupId;
        int hashCode7 = (hashCode6 + (groupId == null ? 0 : groupId.hashCode())) * 31;
        DistributionListId distributionListId = this.distributionListId;
        int hashCode8 = (((hashCode7 + (distributionListId == null ? 0 : distributionListId.hashCode())) * 31) + this.groupType.hashCode()) * 31;
        boolean z = this.isBlocked;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int m = (((((((hashCode8 + i3) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.muteUntil)) * 31) + this.messageVibrateState.hashCode()) * 31) + this.callVibrateState.hashCode()) * 31;
        Uri uri = this.messageRingtone;
        int hashCode9 = (m + (uri == null ? 0 : uri.hashCode())) * 31;
        Uri uri2 = this.callRingtone;
        int hashCode10 = (((((((hashCode9 + (uri2 == null ? 0 : uri2.hashCode())) * 31) + this.defaultSubscriptionId) * 31) + this.expireMessages) * 31) + this.registered.hashCode()) * 31;
        byte[] bArr = this.profileKey;
        int hashCode11 = (hashCode10 + (bArr == null ? 0 : Arrays.hashCode(bArr))) * 31;
        ExpiringProfileKeyCredential expiringProfileKeyCredential = this.expiringProfileKeyCredential;
        int hashCode12 = (((hashCode11 + (expiringProfileKeyCredential == null ? 0 : expiringProfileKeyCredential.hashCode())) * 31) + this.systemProfileName.hashCode()) * 31;
        String str4 = this.systemDisplayName;
        int hashCode13 = (hashCode12 + (str4 == null ? 0 : str4.hashCode())) * 31;
        String str5 = this.systemContactPhotoUri;
        int hashCode14 = (hashCode13 + (str5 == null ? 0 : str5.hashCode())) * 31;
        String str6 = this.systemPhoneLabel;
        int hashCode15 = (hashCode14 + (str6 == null ? 0 : str6.hashCode())) * 31;
        String str7 = this.systemContactUri;
        int hashCode16 = (((hashCode15 + (str7 == null ? 0 : str7.hashCode())) * 31) + this.signalProfileName.hashCode()) * 31;
        String str8 = this.signalProfileAvatar;
        int hashCode17 = (((hashCode16 + (str8 == null ? 0 : str8.hashCode())) * 31) + this.profileAvatarFileDetails.hashCode()) * 31;
        boolean z2 = this.profileSharing;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int m2 = (((hashCode17 + i6) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.lastProfileFetch)) * 31;
        String str9 = this.notificationChannel;
        int hashCode18 = (((m2 + (str9 == null ? 0 : str9.hashCode())) * 31) + this.unidentifiedAccessMode.hashCode()) * 31;
        boolean z3 = this.forceSmsSelection;
        if (z3) {
            z3 = true;
        }
        int i9 = z3 ? 1 : 0;
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int m3 = (((((((((((((((((hashCode18 + i9) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.rawCapabilities)) * 31) + this.groupsV1MigrationCapability.hashCode()) * 31) + this.senderKeyCapability.hashCode()) * 31) + this.announcementGroupCapability.hashCode()) * 31) + this.changeNumberCapability.hashCode()) * 31) + this.storiesCapability.hashCode()) * 31) + this.giftBadgesCapability.hashCode()) * 31) + this.insightsBannerTier.hashCode()) * 31;
        byte[] bArr2 = this.storageId;
        int hashCode19 = (((m3 + (bArr2 == null ? 0 : Arrays.hashCode(bArr2))) * 31) + this.mentionSetting.hashCode()) * 31;
        ChatWallpaper chatWallpaper = this.wallpaper;
        int hashCode20 = (hashCode19 + (chatWallpaper == null ? 0 : chatWallpaper.hashCode())) * 31;
        ChatColors chatColors = this.chatColors;
        int hashCode21 = (((hashCode20 + (chatColors == null ? 0 : chatColors.hashCode())) * 31) + this.avatarColor.hashCode()) * 31;
        String str10 = this.about;
        int hashCode22 = (hashCode21 + (str10 == null ? 0 : str10.hashCode())) * 31;
        String str11 = this.aboutEmoji;
        int hashCode23 = (((hashCode22 + (str11 == null ? 0 : str11.hashCode())) * 31) + this.syncExtras.hashCode()) * 31;
        Recipient.Extras extras = this.extras;
        if (extras != null) {
            i = extras.hashCode();
        }
        int i12 = (hashCode23 + i) * 31;
        boolean z4 = this.hasGroupsInCommon;
        if (!z4) {
            i2 = z4 ? 1 : 0;
        }
        return ((i12 + i2) * 31) + this.badges.hashCode();
    }

    public String toString() {
        return "RecipientRecord(id=" + this.id + ", serviceId=" + this.serviceId + ", pni=" + this.pni + ", username=" + this.username + ", e164=" + this.e164 + ", email=" + this.email + ", groupId=" + this.groupId + ", distributionListId=" + this.distributionListId + ", groupType=" + this.groupType + ", isBlocked=" + this.isBlocked + ", muteUntil=" + this.muteUntil + ", messageVibrateState=" + this.messageVibrateState + ", callVibrateState=" + this.callVibrateState + ", messageRingtone=" + this.messageRingtone + ", callRingtone=" + this.callRingtone + ", defaultSubscriptionId=" + this.defaultSubscriptionId + ", expireMessages=" + this.expireMessages + ", registered=" + this.registered + ", profileKey=" + Arrays.toString(this.profileKey) + ", expiringProfileKeyCredential=" + this.expiringProfileKeyCredential + ", systemProfileName=" + this.systemProfileName + ", systemDisplayName=" + this.systemDisplayName + ", systemContactPhotoUri=" + this.systemContactPhotoUri + ", systemPhoneLabel=" + this.systemPhoneLabel + ", systemContactUri=" + this.systemContactUri + ", signalProfileName=" + this.signalProfileName + ", signalProfileAvatar=" + this.signalProfileAvatar + ", profileAvatarFileDetails=" + this.profileAvatarFileDetails + ", profileSharing=" + this.profileSharing + ", lastProfileFetch=" + this.lastProfileFetch + ", notificationChannel=" + this.notificationChannel + ", unidentifiedAccessMode=" + this.unidentifiedAccessMode + ", forceSmsSelection=" + this.forceSmsSelection + ", rawCapabilities=" + this.rawCapabilities + ", groupsV1MigrationCapability=" + this.groupsV1MigrationCapability + ", senderKeyCapability=" + this.senderKeyCapability + ", announcementGroupCapability=" + this.announcementGroupCapability + ", changeNumberCapability=" + this.changeNumberCapability + ", storiesCapability=" + this.storiesCapability + ", giftBadgesCapability=" + this.giftBadgesCapability + ", insightsBannerTier=" + this.insightsBannerTier + ", storageId=" + Arrays.toString(this.storageId) + ", mentionSetting=" + this.mentionSetting + ", wallpaper=" + this.wallpaper + ", chatColors=" + this.chatColors + ", avatarColor=" + this.avatarColor + ", about=" + this.about + ", aboutEmoji=" + this.aboutEmoji + ", syncExtras=" + this.syncExtras + ", extras=" + this.extras + ", hasGroupsInCommon=" + this.hasGroupsInCommon + ", badges=" + this.badges + ')';
    }

    public RecipientRecord(RecipientId recipientId, ServiceId serviceId, PNI pni, String str, String str2, String str3, GroupId groupId, DistributionListId distributionListId, RecipientDatabase.GroupType groupType, boolean z, long j, RecipientDatabase.VibrateState vibrateState, RecipientDatabase.VibrateState vibrateState2, Uri uri, Uri uri2, int i, int i2, RecipientDatabase.RegisteredState registeredState, byte[] bArr, ExpiringProfileKeyCredential expiringProfileKeyCredential, ProfileName profileName, String str4, String str5, String str6, String str7, ProfileName profileName2, String str8, ProfileAvatarFileDetails profileAvatarFileDetails, boolean z2, long j2, String str9, RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode, boolean z3, long j3, Recipient.Capability capability, Recipient.Capability capability2, Recipient.Capability capability3, Recipient.Capability capability4, Recipient.Capability capability5, Recipient.Capability capability6, RecipientDatabase.InsightsBannerTier insightsBannerTier, byte[] bArr2, RecipientDatabase.MentionSetting mentionSetting, ChatWallpaper chatWallpaper, ChatColors chatColors, AvatarColor avatarColor, String str10, String str11, SyncExtras syncExtras, Recipient.Extras extras, boolean z4, List<Badge> list) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(groupType, "groupType");
        Intrinsics.checkNotNullParameter(vibrateState, "messageVibrateState");
        Intrinsics.checkNotNullParameter(vibrateState2, "callVibrateState");
        Intrinsics.checkNotNullParameter(registeredState, RecipientDatabase.REGISTERED);
        Intrinsics.checkNotNullParameter(profileName, "systemProfileName");
        Intrinsics.checkNotNullParameter(profileName2, "signalProfileName");
        Intrinsics.checkNotNullParameter(profileAvatarFileDetails, "profileAvatarFileDetails");
        Intrinsics.checkNotNullParameter(unidentifiedAccessMode, "unidentifiedAccessMode");
        Intrinsics.checkNotNullParameter(capability, "groupsV1MigrationCapability");
        Intrinsics.checkNotNullParameter(capability2, "senderKeyCapability");
        Intrinsics.checkNotNullParameter(capability3, "announcementGroupCapability");
        Intrinsics.checkNotNullParameter(capability4, "changeNumberCapability");
        Intrinsics.checkNotNullParameter(capability5, "storiesCapability");
        Intrinsics.checkNotNullParameter(capability6, "giftBadgesCapability");
        Intrinsics.checkNotNullParameter(insightsBannerTier, "insightsBannerTier");
        Intrinsics.checkNotNullParameter(mentionSetting, "mentionSetting");
        Intrinsics.checkNotNullParameter(avatarColor, "avatarColor");
        Intrinsics.checkNotNullParameter(syncExtras, "syncExtras");
        Intrinsics.checkNotNullParameter(list, "badges");
        this.id = recipientId;
        this.serviceId = serviceId;
        this.pni = pni;
        this.username = str;
        this.e164 = str2;
        this.email = str3;
        this.groupId = groupId;
        this.distributionListId = distributionListId;
        this.groupType = groupType;
        this.isBlocked = z;
        this.muteUntil = j;
        this.messageVibrateState = vibrateState;
        this.callVibrateState = vibrateState2;
        this.messageRingtone = uri;
        this.callRingtone = uri2;
        this.defaultSubscriptionId = i;
        this.expireMessages = i2;
        this.registered = registeredState;
        this.profileKey = bArr;
        this.expiringProfileKeyCredential = expiringProfileKeyCredential;
        this.systemProfileName = profileName;
        this.systemDisplayName = str4;
        this.systemContactPhotoUri = str5;
        this.systemPhoneLabel = str6;
        this.systemContactUri = str7;
        this.signalProfileName = profileName2;
        this.signalProfileAvatar = str8;
        this.profileAvatarFileDetails = profileAvatarFileDetails;
        this.profileSharing = z2;
        this.lastProfileFetch = j2;
        this.notificationChannel = str9;
        this.unidentifiedAccessMode = unidentifiedAccessMode;
        this.forceSmsSelection = z3;
        this.rawCapabilities = j3;
        this.groupsV1MigrationCapability = capability;
        this.senderKeyCapability = capability2;
        this.announcementGroupCapability = capability3;
        this.changeNumberCapability = capability4;
        this.storiesCapability = capability5;
        this.giftBadgesCapability = capability6;
        this.insightsBannerTier = insightsBannerTier;
        this.storageId = bArr2;
        this.mentionSetting = mentionSetting;
        this.wallpaper = chatWallpaper;
        this.chatColors = chatColors;
        this.avatarColor = avatarColor;
        this.about = str10;
        this.aboutEmoji = str11;
        this.syncExtras = syncExtras;
        this.extras = extras;
        this.hasGroupsInCommon = z4;
        this.badges = list;
    }

    public final RecipientId getId() {
        return this.id;
    }

    public final ServiceId getServiceId() {
        return this.serviceId;
    }

    public final PNI getPni() {
        return this.pni;
    }

    public final String getUsername() {
        return this.username;
    }

    public final String getE164() {
        return this.e164;
    }

    public final String getEmail() {
        return this.email;
    }

    public final GroupId getGroupId() {
        return this.groupId;
    }

    public final DistributionListId getDistributionListId() {
        return this.distributionListId;
    }

    public final RecipientDatabase.GroupType getGroupType() {
        return this.groupType;
    }

    public final boolean isBlocked() {
        return this.isBlocked;
    }

    public final long getMuteUntil() {
        return this.muteUntil;
    }

    public final RecipientDatabase.VibrateState getMessageVibrateState() {
        return this.messageVibrateState;
    }

    public final RecipientDatabase.VibrateState getCallVibrateState() {
        return this.callVibrateState;
    }

    public final Uri getMessageRingtone() {
        return this.messageRingtone;
    }

    public final Uri getCallRingtone() {
        return this.callRingtone;
    }

    public final int getExpireMessages() {
        return this.expireMessages;
    }

    public final RecipientDatabase.RegisteredState getRegistered() {
        return this.registered;
    }

    public final byte[] getProfileKey() {
        return this.profileKey;
    }

    public final ExpiringProfileKeyCredential getExpiringProfileKeyCredential() {
        return this.expiringProfileKeyCredential;
    }

    public final ProfileName getSystemProfileName() {
        return this.systemProfileName;
    }

    public final String getSystemDisplayName() {
        return this.systemDisplayName;
    }

    public final String getSystemContactPhotoUri() {
        return this.systemContactPhotoUri;
    }

    public final String getSystemPhoneLabel() {
        return this.systemPhoneLabel;
    }

    public final String getSystemContactUri() {
        return this.systemContactUri;
    }

    public final ProfileName getProfileName() {
        return this.signalProfileName;
    }

    public final String getProfileAvatar() {
        return this.signalProfileAvatar;
    }

    public final ProfileAvatarFileDetails getProfileAvatarFileDetails() {
        return this.profileAvatarFileDetails;
    }

    public final boolean isProfileSharing() {
        return this.profileSharing;
    }

    public final long getLastProfileFetch() {
        return this.lastProfileFetch;
    }

    public final String getNotificationChannel() {
        return this.notificationChannel;
    }

    public final RecipientDatabase.UnidentifiedAccessMode getUnidentifiedAccessMode() {
        return this.unidentifiedAccessMode;
    }

    public final boolean isForceSmsSelection() {
        return this.forceSmsSelection;
    }

    public final long getRawCapabilities() {
        return this.rawCapabilities;
    }

    public final Recipient.Capability getGroupsV1MigrationCapability() {
        return this.groupsV1MigrationCapability;
    }

    public final Recipient.Capability getSenderKeyCapability() {
        return this.senderKeyCapability;
    }

    public final Recipient.Capability getAnnouncementGroupCapability() {
        return this.announcementGroupCapability;
    }

    public final Recipient.Capability getChangeNumberCapability() {
        return this.changeNumberCapability;
    }

    public final Recipient.Capability getStoriesCapability() {
        return this.storiesCapability;
    }

    public final Recipient.Capability getGiftBadgesCapability() {
        return this.giftBadgesCapability;
    }

    public final RecipientDatabase.InsightsBannerTier getInsightsBannerTier() {
        return this.insightsBannerTier;
    }

    public final byte[] getStorageId() {
        return this.storageId;
    }

    public final RecipientDatabase.MentionSetting getMentionSetting() {
        return this.mentionSetting;
    }

    public final ChatWallpaper getWallpaper() {
        return this.wallpaper;
    }

    public final ChatColors getChatColors() {
        return this.chatColors;
    }

    public final AvatarColor getAvatarColor() {
        return this.avatarColor;
    }

    public final String getAbout() {
        return this.about;
    }

    public final String getAboutEmoji() {
        return this.aboutEmoji;
    }

    public final SyncExtras getSyncExtras() {
        return this.syncExtras;
    }

    public final Recipient.Extras getExtras() {
        return this.extras;
    }

    public final boolean hasGroupsInCommon() {
        return this.hasGroupsInCommon;
    }

    public final List<Badge> getBadges() {
        return this.badges;
    }

    public final Optional<Integer> getDefaultSubscriptionId() {
        String str;
        Optional<Integer> optional;
        int i = this.defaultSubscriptionId;
        if (i != -1) {
            optional = Optional.of(Integer.valueOf(i));
            str = "of(defaultSubscriptionId)";
        } else {
            optional = Optional.empty();
            str = "empty()";
        }
        Intrinsics.checkNotNullExpressionValue(optional, str);
        return optional;
    }

    public final boolean e164Only() {
        return this.e164 != null && this.serviceId == null;
    }

    public final boolean sidOnly(ServiceId serviceId) {
        PNI pni;
        Intrinsics.checkNotNullParameter(serviceId, "sid");
        return this.e164 == null && Intrinsics.areEqual(this.serviceId, serviceId) && ((pni = this.pni) == null || Intrinsics.areEqual(pni, serviceId));
    }

    public final boolean sidIsPni() {
        PNI pni;
        ServiceId serviceId = this.serviceId;
        return (serviceId == null || (pni = this.pni) == null || !Intrinsics.areEqual(serviceId, pni)) ? false : true;
    }

    public final boolean pniAndAci() {
        PNI pni;
        ServiceId serviceId = this.serviceId;
        return (serviceId == null || (pni = this.pni) == null || Intrinsics.areEqual(serviceId, pni)) ? false : true;
    }

    /* compiled from: RecipientRecord.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0014\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B;\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0002\u0010\fJ\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\bHÆ\u0003J\t\u0010\u0019\u001a\u00020\nHÆ\u0003J\t\u0010\u001a\u001a\u00020\nHÆ\u0003JK\u0010\u001b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\nHÆ\u0001J\u0013\u0010\u001c\u001a\u00020\n2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001e\u001a\u00020\u001fHÖ\u0001J\t\u0010 \u001a\u00020!HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0013R\u0011\u0010\u000b\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\u0013R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0010¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/database/model/RecipientRecord$SyncExtras;", "", "storageProto", "", "groupMasterKey", "Lorg/signal/libsignal/zkgroup/groups/GroupMasterKey;", "identityKey", "identityStatus", "Lorg/thoughtcrime/securesms/database/IdentityDatabase$VerifiedStatus;", "isArchived", "", "isForcedUnread", "([BLorg/signal/libsignal/zkgroup/groups/GroupMasterKey;[BLorg/thoughtcrime/securesms/database/IdentityDatabase$VerifiedStatus;ZZ)V", "getGroupMasterKey", "()Lorg/signal/libsignal/zkgroup/groups/GroupMasterKey;", "getIdentityKey", "()[B", "getIdentityStatus", "()Lorg/thoughtcrime/securesms/database/IdentityDatabase$VerifiedStatus;", "()Z", "getStorageProto", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SyncExtras {
        private final GroupMasterKey groupMasterKey;
        private final byte[] identityKey;
        private final IdentityDatabase.VerifiedStatus identityStatus;
        private final boolean isArchived;
        private final boolean isForcedUnread;
        private final byte[] storageProto;

        public static /* synthetic */ SyncExtras copy$default(SyncExtras syncExtras, byte[] bArr, GroupMasterKey groupMasterKey, byte[] bArr2, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                bArr = syncExtras.storageProto;
            }
            if ((i & 2) != 0) {
                groupMasterKey = syncExtras.groupMasterKey;
            }
            if ((i & 4) != 0) {
                bArr2 = syncExtras.identityKey;
            }
            if ((i & 8) != 0) {
                verifiedStatus = syncExtras.identityStatus;
            }
            if ((i & 16) != 0) {
                z = syncExtras.isArchived;
            }
            if ((i & 32) != 0) {
                z2 = syncExtras.isForcedUnread;
            }
            return syncExtras.copy(bArr, groupMasterKey, bArr2, verifiedStatus, z, z2);
        }

        public final byte[] component1() {
            return this.storageProto;
        }

        public final GroupMasterKey component2() {
            return this.groupMasterKey;
        }

        public final byte[] component3() {
            return this.identityKey;
        }

        public final IdentityDatabase.VerifiedStatus component4() {
            return this.identityStatus;
        }

        public final boolean component5() {
            return this.isArchived;
        }

        public final boolean component6() {
            return this.isForcedUnread;
        }

        public final SyncExtras copy(byte[] bArr, GroupMasterKey groupMasterKey, byte[] bArr2, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, boolean z2) {
            Intrinsics.checkNotNullParameter(verifiedStatus, "identityStatus");
            return new SyncExtras(bArr, groupMasterKey, bArr2, verifiedStatus, z, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SyncExtras)) {
                return false;
            }
            SyncExtras syncExtras = (SyncExtras) obj;
            return Intrinsics.areEqual(this.storageProto, syncExtras.storageProto) && Intrinsics.areEqual(this.groupMasterKey, syncExtras.groupMasterKey) && Intrinsics.areEqual(this.identityKey, syncExtras.identityKey) && this.identityStatus == syncExtras.identityStatus && this.isArchived == syncExtras.isArchived && this.isForcedUnread == syncExtras.isForcedUnread;
        }

        public int hashCode() {
            byte[] bArr = this.storageProto;
            int i = 0;
            int hashCode = (bArr == null ? 0 : Arrays.hashCode(bArr)) * 31;
            GroupMasterKey groupMasterKey = this.groupMasterKey;
            int hashCode2 = (hashCode + (groupMasterKey == null ? 0 : groupMasterKey.hashCode())) * 31;
            byte[] bArr2 = this.identityKey;
            if (bArr2 != null) {
                i = Arrays.hashCode(bArr2);
            }
            int hashCode3 = (((hashCode2 + i) * 31) + this.identityStatus.hashCode()) * 31;
            boolean z = this.isArchived;
            int i2 = 1;
            if (z) {
                z = true;
            }
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            int i6 = (hashCode3 + i3) * 31;
            boolean z2 = this.isForcedUnread;
            if (!z2) {
                i2 = z2 ? 1 : 0;
            }
            return i6 + i2;
        }

        public String toString() {
            return "SyncExtras(storageProto=" + Arrays.toString(this.storageProto) + ", groupMasterKey=" + this.groupMasterKey + ", identityKey=" + Arrays.toString(this.identityKey) + ", identityStatus=" + this.identityStatus + ", isArchived=" + this.isArchived + ", isForcedUnread=" + this.isForcedUnread + ')';
        }

        public SyncExtras(byte[] bArr, GroupMasterKey groupMasterKey, byte[] bArr2, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, boolean z2) {
            Intrinsics.checkNotNullParameter(verifiedStatus, "identityStatus");
            this.storageProto = bArr;
            this.groupMasterKey = groupMasterKey;
            this.identityKey = bArr2;
            this.identityStatus = verifiedStatus;
            this.isArchived = z;
            this.isForcedUnread = z2;
        }

        public final byte[] getStorageProto() {
            return this.storageProto;
        }

        public final GroupMasterKey getGroupMasterKey() {
            return this.groupMasterKey;
        }

        public final byte[] getIdentityKey() {
            return this.identityKey;
        }

        public final IdentityDatabase.VerifiedStatus getIdentityStatus() {
            return this.identityStatus;
        }

        public final boolean isArchived() {
            return this.isArchived;
        }

        public final boolean isForcedUnread() {
            return this.isForcedUnread;
        }
    }
}
