package org.thoughtcrime.securesms.database.helpers;

import android.content.ContentValues;
import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.Conversions;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.state.SessionRecord;
import org.thoughtcrime.securesms.database.SessionDatabase;

/* loaded from: classes4.dex */
public final class SessionStoreMigrationHelper {
    private static final int ARCHIVE_STATES_VERSION;
    private static final int CURRENT_VERSION;
    private static final Object FILE_LOCK = new Object();
    private static final int PLAINTEXT_VERSION;
    private static final String SESSIONS_DIRECTORY_V2;
    private static final int SINGLE_STATE_VERSION;
    private static final String TAG = Log.tag(SessionStoreMigrationHelper.class);

    public static void migrateSessions(Context context, SQLiteDatabase sQLiteDatabase) {
        File[] listFiles;
        String str;
        int parseInt;
        FileInputStream fileInputStream;
        int readInteger;
        SessionRecord sessionRecord;
        File file = new File(context.getFilesDir(), SESSIONS_DIRECTORY_V2);
        if (file.exists() && (listFiles = file.listFiles()) != null) {
            for (File file2 : listFiles) {
                try {
                    String[] split = file2.getName().split("[.]");
                    str = split[0];
                    parseInt = split.length > 1 ? Integer.parseInt(split[1]) : 1;
                    fileInputStream = new FileInputStream(file2);
                    readInteger = readInteger(fileInputStream);
                } catch (IOException | NumberFormatException | InvalidMessageException e) {
                    Log.w(TAG, e);
                }
                if (readInteger <= 3) {
                    byte[] readBlob = readBlob(fileInputStream);
                    fileInputStream.close();
                    if (readInteger >= 3) {
                        if (readInteger == 1) {
                            Log.i(TAG, "Migrating single state version: " + file2.getAbsolutePath());
                            sessionRecord = new SessionRecord(readBlob);
                        } else if (readInteger >= 2) {
                            Log.i(TAG, "Migrating session: " + file2.getAbsolutePath());
                            sessionRecord = new SessionRecord(readBlob);
                        } else {
                            throw new AssertionError("Unknown version: " + readInteger + ", " + file2.getAbsolutePath());
                        }
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("address", str);
                        contentValues.put("device", Integer.valueOf(parseInt));
                        contentValues.put("record", sessionRecord.serialize());
                        sQLiteDatabase.insert(SessionDatabase.TABLE_NAME, (String) null, contentValues);
                    } else {
                        throw new AssertionError("Not plaintext: " + readInteger + ", " + file2.getAbsolutePath());
                    }
                } else {
                    throw new AssertionError("Unknown version: " + readInteger + ", " + file2.getAbsolutePath());
                }
            }
        }
    }

    private static byte[] readBlob(FileInputStream fileInputStream) throws IOException {
        int readInteger = readInteger(fileInputStream);
        byte[] bArr = new byte[readInteger];
        fileInputStream.read(bArr, 0, readInteger);
        return bArr;
    }

    private static int readInteger(FileInputStream fileInputStream) throws IOException {
        byte[] bArr = new byte[4];
        fileInputStream.read(bArr, 0, 4);
        return Conversions.byteArrayToInt(bArr);
    }
}
