package org.thoughtcrime.securesms.database.loaders;

import android.content.Context;
import android.database.Cursor;
import android.util.SparseArray;
import androidx.loader.content.AsyncTaskLoader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.loaders.MediaLoader;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.CalendarDateOnly;

/* loaded from: classes4.dex */
public final class GroupedThreadMediaLoader extends AsyncTaskLoader<GroupedThreadMedia> {
    private static final String TAG = Log.tag(GroupedThreadMediaLoader.class);
    private final MediaLoader.MediaType mediaType;
    private final DatabaseObserver.Observer observer = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader$$ExternalSyntheticLambda0
        @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
        public final void onChanged() {
            GroupedThreadMediaLoader.m1728$r8$lambda$i2auOey33nXjd7LAhU_8miEYo(GroupedThreadMediaLoader.this);
        }
    };
    private final MediaDatabase.Sorting sorting;
    private final long threadId;

    /* loaded from: classes4.dex */
    public static abstract class GroupedThreadMedia {
        public abstract MediaDatabase.MediaRecord get(int i, int i2);

        public abstract String getName(int i);

        public abstract int getSectionCount();

        public abstract int getSectionItemCount(int i);
    }

    /* loaded from: classes4.dex */
    public interface GroupingMethod {
        int groupForRecord(MediaDatabase.MediaRecord mediaRecord);

        String groupName(int i);
    }

    public GroupedThreadMediaLoader(Context context, long j, MediaLoader.MediaType mediaType, MediaDatabase.Sorting sorting) {
        super(context);
        this.threadId = j;
        this.mediaType = mediaType;
        this.sorting = sorting;
        onContentChanged();
    }

    public /* synthetic */ void lambda$new$0() {
        ThreadUtil.runOnMain(new Runnable() { // from class: org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                GroupedThreadMediaLoader.this.onContentChanged();
            }
        });
    }

    @Override // androidx.loader.content.Loader
    protected void onStartLoading() {
        if (takeContentChanged()) {
            forceLoad();
        }
    }

    @Override // androidx.loader.content.Loader
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override // androidx.loader.content.Loader
    protected void onAbandon() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
    }

    @Override // androidx.loader.content.AsyncTaskLoader
    public GroupedThreadMedia loadInBackground() {
        GroupingMethod groupingMethod;
        Context context = getContext();
        if (this.sorting.isRelatedToFileSize()) {
            groupingMethod = new RoughSizeGroupingMethod(context);
        } else {
            groupingMethod = new DateGroupingMethod(context, CalendarDateOnly.getInstance());
        }
        PopulatedGroupedThreadMedia populatedGroupedThreadMedia = new PopulatedGroupedThreadMedia(groupingMethod);
        ApplicationDependencies.getDatabaseObserver().registerAttachmentObserver(this.observer);
        Cursor createThreadMediaCursor = ThreadMediaLoader.createThreadMediaCursor(context, this.threadId, this.mediaType, this.sorting);
        while (createThreadMediaCursor != null) {
            try {
                if (!createThreadMediaCursor.moveToNext()) {
                    break;
                }
                populatedGroupedThreadMedia.add(MediaDatabase.MediaRecord.from(context, createThreadMediaCursor));
            } catch (Throwable th) {
                try {
                    createThreadMediaCursor.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (createThreadMediaCursor != null) {
            createThreadMediaCursor.close();
        }
        MediaDatabase.Sorting sorting = this.sorting;
        if (sorting == MediaDatabase.Sorting.Oldest || sorting == MediaDatabase.Sorting.Largest) {
            return new ReversedGroupedThreadMedia(populatedGroupedThreadMedia);
        }
        return populatedGroupedThreadMedia;
    }

    /* loaded from: classes4.dex */
    public static class DateGroupingMethod implements GroupingMethod {
        private static final int THIS_MONTH;
        private static final int THIS_WEEK;
        private static final int TODAY;
        private static final int YESTERDAY;
        private final Context context;
        private final long thisMonthStart;
        private final long thisWeekStart;
        private final long todayStart;
        private final long yesterdayStart;

        DateGroupingMethod(Context context, Calendar calendar) {
            this.context = context;
            this.todayStart = calendar.getTimeInMillis();
            this.yesterdayStart = getTimeInMillis(calendar, 6, -1);
            this.thisWeekStart = getTimeInMillis(calendar, 6, -6);
            this.thisMonthStart = getTimeInMillis(calendar, 6, -30);
        }

        private static long getTimeInMillis(Calendar calendar, int i, int i2) {
            Calendar calendar2 = (Calendar) calendar.clone();
            calendar2.add(i, i2);
            return calendar2.getTimeInMillis();
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupingMethod
        public int groupForRecord(MediaDatabase.MediaRecord mediaRecord) {
            long date = mediaRecord.getDate();
            if (date > this.todayStart) {
                return Integer.MIN_VALUE;
            }
            if (date > this.yesterdayStart) {
                return YESTERDAY;
            }
            if (date > this.thisWeekStart) {
                return THIS_WEEK;
            }
            if (date > this.thisMonthStart) {
                return THIS_MONTH;
            }
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(date);
            return -((instance.get(1) * 12) + instance.get(2));
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupingMethod
        public String groupName(int i) {
            switch (i) {
                case Integer.MIN_VALUE:
                    return this.context.getString(R.string.BucketedThreadMedia_Today);
                case YESTERDAY:
                    return this.context.getString(R.string.BucketedThreadMedia_Yesterday);
                case THIS_WEEK:
                    return this.context.getString(R.string.BucketedThreadMedia_This_week);
                case THIS_MONTH:
                    return this.context.getString(R.string.BucketedThreadMedia_This_month);
                default:
                    int i2 = -i;
                    Calendar instance = Calendar.getInstance();
                    instance.set(1, i2 / 12);
                    instance.set(2, i2 % 12);
                    return new SimpleDateFormat("MMMM, yyyy", Locale.getDefault()).format(instance.getTime());
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class RoughSizeGroupingMethod implements GroupingMethod {
        private static final int LARGE;
        private static final int MB;
        private static final int MEDIUM;
        private static final int SMALL;
        private final String largeDescription;
        private final String mediumDescription;
        private final String smallDescription;

        RoughSizeGroupingMethod(Context context) {
            this.smallDescription = context.getString(R.string.BucketedThreadMedia_Small);
            this.mediumDescription = context.getString(R.string.BucketedThreadMedia_Medium);
            this.largeDescription = context.getString(R.string.BucketedThreadMedia_Large);
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupingMethod
        public int groupForRecord(MediaDatabase.MediaRecord mediaRecord) {
            long size = mediaRecord.getAttachment().getSize();
            if (size < 1048576) {
                return 0;
            }
            return size < 20971520 ? 1 : 2;
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupingMethod
        public String groupName(int i) {
            if (i == 0) {
                return this.smallDescription;
            }
            if (i == 1) {
                return this.mediumDescription;
            }
            if (i == 2) {
                return this.largeDescription;
            }
            throw new AssertionError();
        }
    }

    /* loaded from: classes4.dex */
    public static class EmptyGroupedThreadMedia extends GroupedThreadMedia {
        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public int getSectionCount() {
            return 0;
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public int getSectionItemCount(int i) {
            return 0;
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public MediaDatabase.MediaRecord get(int i, int i2) {
            throw new AssertionError();
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public String getName(int i) {
            throw new AssertionError();
        }
    }

    /* loaded from: classes4.dex */
    public static class ReversedGroupedThreadMedia extends GroupedThreadMedia {
        private final GroupedThreadMedia decorated;

        ReversedGroupedThreadMedia(GroupedThreadMedia groupedThreadMedia) {
            this.decorated = groupedThreadMedia;
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public int getSectionCount() {
            return this.decorated.getSectionCount();
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public int getSectionItemCount(int i) {
            return this.decorated.getSectionItemCount(getReversedSection(i));
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public MediaDatabase.MediaRecord get(int i, int i2) {
            return this.decorated.get(getReversedSection(i), i2);
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public String getName(int i) {
            return this.decorated.getName(getReversedSection(i));
        }

        private int getReversedSection(int i) {
            return (this.decorated.getSectionCount() - 1) - i;
        }
    }

    /* loaded from: classes4.dex */
    public static class PopulatedGroupedThreadMedia extends GroupedThreadMedia {
        private final GroupingMethod groupingMethod;
        private final SparseArray<List<MediaDatabase.MediaRecord>> records;

        private PopulatedGroupedThreadMedia(GroupingMethod groupingMethod) {
            this.records = new SparseArray<>();
            this.groupingMethod = groupingMethod;
        }

        public void add(MediaDatabase.MediaRecord mediaRecord) {
            int groupForRecord = this.groupingMethod.groupForRecord(mediaRecord);
            List<MediaDatabase.MediaRecord> list = this.records.get(groupForRecord);
            if (list == null) {
                list = new LinkedList<>();
                this.records.put(groupForRecord, list);
            }
            list.add(mediaRecord);
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public int getSectionCount() {
            return this.records.size();
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public int getSectionItemCount(int i) {
            SparseArray<List<MediaDatabase.MediaRecord>> sparseArray = this.records;
            return sparseArray.get(sparseArray.keyAt(i)).size();
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public MediaDatabase.MediaRecord get(int i, int i2) {
            SparseArray<List<MediaDatabase.MediaRecord>> sparseArray = this.records;
            return sparseArray.get(sparseArray.keyAt(i)).get(i2);
        }

        @Override // org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader.GroupedThreadMedia
        public String getName(int i) {
            return this.groupingMethod.groupName(this.records.keyAt(i));
        }
    }
}
