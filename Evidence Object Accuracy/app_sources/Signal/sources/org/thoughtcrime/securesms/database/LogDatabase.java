package org.thoughtcrime.securesms.database;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import j$.util.Iterator;
import j$.util.function.Consumer;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.markers.KMappedMarker;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import net.zetetic.database.sqlcipher.SQLiteOpenHelper;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SQLiteDatabaseExtensionsKt;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.crypto.DatabaseSecret;
import org.thoughtcrime.securesms.crypto.DatabaseSecretProvider;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.model.LogEntry;
import org.thoughtcrime.securesms.util.ByteUnit;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* compiled from: LogDatabase.kt */
@Metadata(d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 *2\u00020\u00012\u00020\u0002:\u0003*+,B\u0017\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u0006\u0010\f\u001a\u00020\rJ\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0010\u001a\u00020\u0011J$\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0010\u001a\u00020\u0011J'\u0010\u0019\u001a\u00020\u00112\b\u0010\u001a\u001a\u0004\u0018\u00010\u00162\u000e\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u001cH\u0002¢\u0006\u0002\u0010\u001dJ\b\u0010\u001e\u001a\u00020\u001fH\u0016J\u001c\u0010 \u001a\u00020\r2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00152\u0006\u0010\"\u001a\u00020\u0011J\u0010\u0010#\u001a\u00020\r2\u0006\u0010$\u001a\u00020\u001fH\u0016J\u0010\u0010%\u001a\u00020\r2\u0006\u0010$\u001a\u00020\u001fH\u0016J \u0010&\u001a\u00020\r2\u0006\u0010$\u001a\u00020\u001f2\u0006\u0010'\u001a\u00020\u00132\u0006\u0010(\u001a\u00020\u0013H\u0016J\u0006\u0010)\u001a\u00020\r¨\u0006-"}, d2 = {"Lorg/thoughtcrime/securesms/database/LogDatabase;", "Lnet/zetetic/database/sqlcipher/SQLiteOpenHelper;", "Lorg/thoughtcrime/securesms/database/SignalDatabaseOpenHelper;", "application", "Landroid/app/Application;", "databaseSecret", "Lorg/thoughtcrime/securesms/crypto/DatabaseSecret;", "(Landroid/app/Application;Lorg/thoughtcrime/securesms/crypto/DatabaseSecret;)V", "buildValues", "Landroid/content/ContentValues;", LogDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/database/model/LogEntry;", "clearKeepLonger", "", "getAllBeforeTime", "Lorg/thoughtcrime/securesms/database/LogDatabase$Reader;", "time", "", "getLogCountBeforeTime", "", "getRangeBeforeTime", "", "", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "length", "getSize", "query", MultiselectForwardFragment.ARGS, "", "(Ljava/lang/String;[Ljava/lang/String;)J", "getSqlCipherDatabase", "Lnet/zetetic/database/sqlcipher/SQLiteDatabase;", "insert", "logs", "currentTime", "onCreate", "db", "onOpen", "onUpgrade", "oldVersion", "newVersion", "trimToSize", "Companion", "CursorReader", "Reader", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class LogDatabase extends SQLiteOpenHelper implements SignalDatabaseOpenHelper {
    private static final String BODY;
    private static final String CREATED_AT;
    private static final String[] CREATE_INDEXES = {"CREATE INDEX keep_longer_index ON log (keep_longer)", "CREATE INDEX log_created_at_keep_longer_index ON log (created_at, keep_longer)"};
    private static final String CREATE_TABLE = "CREATE TABLE log (\n  _id INTEGER PRIMARY KEY,\n  created_at INTEGER, \n  keep_longer INTEGER DEFAULT 0,\n  body TEXT,\n  size INTEGER\n)";
    public static final Companion Companion = new Companion(null);
    private static final String DATABASE_NAME;
    private static final int DATABASE_VERSION;
    private static final long DEFAULT_LIFESPAN;
    private static final String ID;
    private static final String KEEP_LONGER;
    private static final long LONGER_LIFESPAN;
    private static final long MAX_FILE_SIZE = ByteUnit.MEGABYTES.toBytes(20);
    private static final String SIZE;
    private static final String TABLE_NAME;
    private static final String TAG = Log.tag(LogDatabase.class);
    private static volatile LogDatabase instance;

    /* compiled from: LogDatabase.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010(\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/database/LogDatabase$Reader;", "", "", "Ljava/io/Closeable;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Reader extends Iterator<String>, Closeable, KMappedMarker {
    }

    public /* synthetic */ LogDatabase(Application application, DatabaseSecret databaseSecret, DefaultConstructorMarker defaultConstructorMarker) {
        this(application, databaseSecret);
    }

    @JvmStatic
    public static final LogDatabase getInstance(Application application) {
        return Companion.getInstance(application);
    }

    private LogDatabase(Application application, DatabaseSecret databaseSecret) {
        super(application, DATABASE_NAME, databaseSecret.asString(), (SQLiteDatabase.CursorFactory) null, 2, 0, new SqlCipherDeletingErrorHandler(DATABASE_NAME), new SqlCipherDatabaseHook());
    }

    /* compiled from: LogDatabase.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u001bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007X\u0004¢\u0006\u0004\n\u0002\u0010\bR\u000e\u0010\t\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fXT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0015\u001a\n \u0016*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0004\u0018\u00010\u00188\u0002@\u0002X\u000e¢\u0006\u0002\n\u0000¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/database/LogDatabase$Companion;", "", "()V", "BODY", "", "CREATED_AT", "CREATE_INDEXES", "", "[Ljava/lang/String;", "CREATE_TABLE", "DATABASE_NAME", "DATABASE_VERSION", "", "DEFAULT_LIFESPAN", "", "ID", "KEEP_LONGER", "LONGER_LIFESPAN", "MAX_FILE_SIZE", "SIZE", "TABLE_NAME", "TAG", "kotlin.jvm.PlatformType", "instance", "Lorg/thoughtcrime/securesms/database/LogDatabase;", "getInstance", "context", "Landroid/app/Application;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final LogDatabase getInstance(Application application) {
            Intrinsics.checkNotNullParameter(application, "context");
            if (LogDatabase.instance == null) {
                synchronized (LogDatabase.class) {
                    if (LogDatabase.instance == null) {
                        SqlCipherLibraryLoader.load();
                        DatabaseSecret orCreateDatabaseSecret = DatabaseSecretProvider.getOrCreateDatabaseSecret(application);
                        Intrinsics.checkNotNullExpressionValue(orCreateDatabaseSecret, "getOrCreateDatabaseSecret(context)");
                        LogDatabase.instance = new LogDatabase(application, orCreateDatabaseSecret, null);
                        LogDatabase logDatabase = LogDatabase.instance;
                        Intrinsics.checkNotNull(logDatabase);
                        logDatabase.setWriteAheadLoggingEnabled(true);
                    }
                    Unit unit = Unit.INSTANCE;
                }
            }
            LogDatabase logDatabase2 = LogDatabase.instance;
            Intrinsics.checkNotNull(logDatabase2);
            return logDatabase2;
        }
    }

    static {
        Companion = new Companion(null);
        TAG = Log.tag(LogDatabase.class);
        MAX_FILE_SIZE = ByteUnit.MEGABYTES.toBytes(20);
        TimeUnit timeUnit = TimeUnit.DAYS;
        DEFAULT_LIFESPAN = timeUnit.toMillis(3);
        LONGER_LIFESPAN = timeUnit.toMillis(21);
        CREATE_TABLE = "CREATE TABLE log (\n  _id INTEGER PRIMARY KEY,\n  created_at INTEGER, \n  keep_longer INTEGER DEFAULT 0,\n  body TEXT,\n  size INTEGER\n)";
        CREATE_INDEXES = new String[]{"CREATE INDEX keep_longer_index ON log (keep_longer)", "CREATE INDEX log_created_at_keep_longer_index ON log (created_at, keep_longer)"};
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
        Log.i(TAG, "onCreate()");
        sQLiteDatabase.execSQL(CREATE_TABLE);
        for (String str : CREATE_INDEXES) {
            sQLiteDatabase.execSQL(str);
        }
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
        String str = TAG;
        Log.i(str, "onUpgrade(" + i + ", " + i2 + ')');
        if (i < 2) {
            sQLiteDatabase.execSQL("DROP TABLE log");
            sQLiteDatabase.execSQL("CREATE TABLE log (_id INTEGER PRIMARY KEY, created_at INTEGER, keep_longer INTEGER DEFAULT 0, body TEXT, size INTEGER)");
            sQLiteDatabase.execSQL("CREATE INDEX keep_longer_index ON log (keep_longer)");
            sQLiteDatabase.execSQL("CREATE INDEX log_created_at_keep_longer_index ON log (created_at, keep_longer)");
        }
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
        sQLiteDatabase.setForeignKeyConstraintsEnabled(true);
    }

    @Override // org.thoughtcrime.securesms.database.SignalDatabaseOpenHelper
    public SQLiteDatabase getSqlCipherDatabase() {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        return writableDatabase;
    }

    public final void insert(List<LogEntry> list, long j) {
        Intrinsics.checkNotNullParameter(list, "logs");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (LogEntry logEntry : list) {
                writableDatabase.insert(TABLE_NAME, (String) null, buildValues(logEntry));
            }
            writableDatabase.delete(TABLE_NAME, "(created_at < ? AND keep_longer = ?) OR (created_at < ? AND keep_longer = ?)", SqlUtil.buildArgs(Long.valueOf(j - DEFAULT_LIFESPAN), 0, Long.valueOf(j - LONGER_LIFESPAN), 1));
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public final Reader getAllBeforeTime(long j) {
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"body"}, "created_at < ?", SqlUtil.buildArgs(j), null, null, null);
        Intrinsics.checkNotNullExpressionValue(query, "readableDatabase.query(T…(time), null, null, null)");
        return new CursorReader(query);
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r12v0 int), (',' char), (r13v0 int)] */
    public final List<String> getRangeBeforeTime(int i, int i2, long j) {
        ArrayList arrayList = new ArrayList();
        String[] buildArgs = SqlUtil.buildArgs(j);
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(',');
        sb.append(i2);
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"body"}, "created_at < ?", buildArgs, null, null, null, sb.toString());
        while (query.moveToNext()) {
            try {
                String requireString = CursorUtil.requireString(query, "body");
                Intrinsics.checkNotNullExpressionValue(requireString, "requireString(cursor, BODY)");
                arrayList.add(requireString);
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return arrayList;
    }

    public final void trimToSize() {
        long currentTimeMillis = System.currentTimeMillis();
        Stopwatch stopwatch = new Stopwatch("trim");
        long size = getSize("keep_longer = ?", new String[]{SubscriptionLevels.BOOST_LEVEL});
        long j = MAX_FILE_SIZE;
        long j2 = j - size;
        stopwatch.split("keepers-size");
        if (j2 > 0) {
            double d = (double) j;
            Double.isNaN(d);
            double d2 = d * 0.01d;
            long j3 = currentTimeMillis - DEFAULT_LIFESPAN;
            long j4 = currentTimeMillis;
            long j5 = 0;
            while (true) {
                long j6 = (long) 2;
                if (j3 >= j4 - j6) {
                    break;
                }
                j5 = (j3 + j4) / j6;
                long size2 = getSize("created_at > ? AND created_at < ? AND keep_longer = ?", SqlUtil.buildArgs(Long.valueOf(j5), Long.valueOf(currentTimeMillis), 0));
                if (size2 <= j2) {
                    if (size2 >= j2 || ((double) (j2 - size2)) < d2) {
                        break;
                    }
                    j4 = j5;
                } else {
                    j3 = j5;
                }
            }
            stopwatch.split("binary-search");
            getWritableDatabase().delete(TABLE_NAME, "created_at < ? AND keep_longer = ?", SqlUtil.buildArgs(Long.valueOf(j5), 0));
            stopwatch.split("delete");
            stopwatch.stop(TAG);
        } else if (Math.abs(j2) > j / ((long) 2)) {
            SQLiteDatabase readableDatabase = getReadableDatabase();
            Intrinsics.checkNotNullExpressionValue(readableDatabase, "readableDatabase");
            int tableRowCount = SQLiteDatabaseExtensionsKt.getTableRowCount(readableDatabase, TABLE_NAME);
            getWritableDatabase().execSQL("DELETE FROM log WHERE _id < (SELECT MAX(_id) FROM (SELECT _id FROM log LIMIT " + (tableRowCount / 2) + "))");
        } else {
            getWritableDatabase().delete(TABLE_NAME, "keep_longer = ?", new String[]{"0"});
        }
    }

    public final int getLogCountBeforeTime(long j) {
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"COUNT(*)"}, "created_at < ?", SqlUtil.buildArgs(j), null, null, null);
        try {
            int i = 0;
            if (query.moveToFirst()) {
                i = query.getInt(0);
            }
            th = null;
            return i;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final void clearKeepLonger() {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        SQLiteDatabaseExtensionsKt.delete(writableDatabase, TABLE_NAME).where("keep_longer = ?", 1).run();
    }

    private final ContentValues buildValues(LogEntry logEntry) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("created_at", Long.valueOf(logEntry.getCreatedAt()));
        contentValues.put(KEEP_LONGER, Integer.valueOf(logEntry.getKeepLonger() ? 1 : 0));
        contentValues.put("body", logEntry.getBody());
        contentValues.put("size", Integer.valueOf(logEntry.getBody().length()));
        return contentValues;
    }

    private final long getSize(String str, String[] strArr) {
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"SUM(size)"}, str, strArr, null, null, null);
        try {
            th = null;
            return query.moveToFirst() ? query.getLong(0) : 0;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    /* compiled from: LogDatabase.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\t\u0010\u0007\u001a\u00020\bH\u0002J\t\u0010\t\u001a\u00020\nH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/LogDatabase$CursorReader;", "Lorg/thoughtcrime/securesms/database/LogDatabase$Reader;", "cursor", "Landroid/database/Cursor;", "(Landroid/database/Cursor;)V", "close", "", "hasNext", "", "next", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CursorReader implements Reader, j$.util.Iterator {
        private final Cursor cursor;

        @Override // j$.util.Iterator
        public /* synthetic */ void forEachRemaining(Consumer consumer) {
            Iterator.CC.$default$forEachRemaining(this, consumer);
        }

        @Override // java.util.Iterator
        public /* synthetic */ void forEachRemaining(java.util.function.Consumer<? super String> consumer) {
            forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
        }

        @Override // java.util.Iterator, j$.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public CursorReader(Cursor cursor) {
            Intrinsics.checkNotNullParameter(cursor, "cursor");
            this.cursor = cursor;
        }

        @Override // java.util.Iterator, j$.util.Iterator
        public boolean hasNext() {
            return !this.cursor.isLast() && this.cursor.getCount() > 0;
        }

        @Override // java.util.Iterator, j$.util.Iterator
        public String next() {
            this.cursor.moveToNext();
            String requireString = CursorUtil.requireString(this.cursor, "body");
            Intrinsics.checkNotNullExpressionValue(requireString, "requireString(cursor, BODY)");
            return requireString;
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.cursor.close();
        }
    }
}
