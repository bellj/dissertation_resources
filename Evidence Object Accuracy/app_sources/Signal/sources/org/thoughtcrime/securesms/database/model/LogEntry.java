package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;

/* compiled from: LogEntry.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00052\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0007HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/LogEntry;", "", "createdAt", "", "keepLonger", "", "body", "", "(JZLjava/lang/String;)V", "getBody", "()Ljava/lang/String;", "getCreatedAt", "()J", "getKeepLonger", "()Z", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LogEntry {
    private final String body;
    private final long createdAt;
    private final boolean keepLonger;

    public static /* synthetic */ LogEntry copy$default(LogEntry logEntry, long j, boolean z, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            j = logEntry.createdAt;
        }
        if ((i & 2) != 0) {
            z = logEntry.keepLonger;
        }
        if ((i & 4) != 0) {
            str = logEntry.body;
        }
        return logEntry.copy(j, z, str);
    }

    public final long component1() {
        return this.createdAt;
    }

    public final boolean component2() {
        return this.keepLonger;
    }

    public final String component3() {
        return this.body;
    }

    public final LogEntry copy(long j, boolean z, String str) {
        Intrinsics.checkNotNullParameter(str, "body");
        return new LogEntry(j, z, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LogEntry)) {
            return false;
        }
        LogEntry logEntry = (LogEntry) obj;
        return this.createdAt == logEntry.createdAt && this.keepLonger == logEntry.keepLonger && Intrinsics.areEqual(this.body, logEntry.body);
    }

    public int hashCode() {
        int m = SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.createdAt) * 31;
        boolean z = this.keepLonger;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return ((m + i) * 31) + this.body.hashCode();
    }

    public String toString() {
        return "LogEntry(createdAt=" + this.createdAt + ", keepLonger=" + this.keepLonger + ", body=" + this.body + ')';
    }

    public LogEntry(long j, boolean z, String str) {
        Intrinsics.checkNotNullParameter(str, "body");
        this.createdAt = j;
        this.keepLonger = z;
        this.body = str;
    }

    public final long getCreatedAt() {
        return this.createdAt;
    }

    public final boolean getKeepLonger() {
        return this.keepLonger;
    }

    public final String getBody() {
        return this.body;
    }
}
