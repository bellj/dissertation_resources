package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class DecryptedGroupV2Context extends GeneratedMessageLite<DecryptedGroupV2Context, Builder> implements DecryptedGroupV2ContextOrBuilder {
    public static final int CHANGE_FIELD_NUMBER;
    public static final int CONTEXT_FIELD_NUMBER;
    private static final DecryptedGroupV2Context DEFAULT_INSTANCE;
    public static final int GROUPSTATE_FIELD_NUMBER;
    private static volatile Parser<DecryptedGroupV2Context> PARSER;
    public static final int PREVIOUSGROUPSTATE_FIELD_NUMBER;
    private DecryptedGroupChange change_;
    private SignalServiceProtos.GroupContextV2 context_;
    private DecryptedGroup groupState_;
    private DecryptedGroup previousGroupState_;

    private DecryptedGroupV2Context() {
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
    public boolean hasContext() {
        return this.context_ != null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
    public SignalServiceProtos.GroupContextV2 getContext() {
        SignalServiceProtos.GroupContextV2 groupContextV2 = this.context_;
        return groupContextV2 == null ? SignalServiceProtos.GroupContextV2.getDefaultInstance() : groupContextV2;
    }

    public void setContext(SignalServiceProtos.GroupContextV2 groupContextV2) {
        groupContextV2.getClass();
        this.context_ = groupContextV2;
    }

    public void mergeContext(SignalServiceProtos.GroupContextV2 groupContextV2) {
        groupContextV2.getClass();
        SignalServiceProtos.GroupContextV2 groupContextV22 = this.context_;
        if (groupContextV22 == null || groupContextV22 == SignalServiceProtos.GroupContextV2.getDefaultInstance()) {
            this.context_ = groupContextV2;
        } else {
            this.context_ = SignalServiceProtos.GroupContextV2.newBuilder(this.context_).mergeFrom((SignalServiceProtos.GroupContextV2.Builder) groupContextV2).buildPartial();
        }
    }

    public void clearContext() {
        this.context_ = null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
    public boolean hasChange() {
        return this.change_ != null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
    public DecryptedGroupChange getChange() {
        DecryptedGroupChange decryptedGroupChange = this.change_;
        return decryptedGroupChange == null ? DecryptedGroupChange.getDefaultInstance() : decryptedGroupChange;
    }

    public void setChange(DecryptedGroupChange decryptedGroupChange) {
        decryptedGroupChange.getClass();
        this.change_ = decryptedGroupChange;
    }

    public void mergeChange(DecryptedGroupChange decryptedGroupChange) {
        decryptedGroupChange.getClass();
        DecryptedGroupChange decryptedGroupChange2 = this.change_;
        if (decryptedGroupChange2 == null || decryptedGroupChange2 == DecryptedGroupChange.getDefaultInstance()) {
            this.change_ = decryptedGroupChange;
        } else {
            this.change_ = DecryptedGroupChange.newBuilder(this.change_).mergeFrom((DecryptedGroupChange.Builder) decryptedGroupChange).buildPartial();
        }
    }

    public void clearChange() {
        this.change_ = null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
    public boolean hasGroupState() {
        return this.groupState_ != null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
    public DecryptedGroup getGroupState() {
        DecryptedGroup decryptedGroup = this.groupState_;
        return decryptedGroup == null ? DecryptedGroup.getDefaultInstance() : decryptedGroup;
    }

    public void setGroupState(DecryptedGroup decryptedGroup) {
        decryptedGroup.getClass();
        this.groupState_ = decryptedGroup;
    }

    public void mergeGroupState(DecryptedGroup decryptedGroup) {
        decryptedGroup.getClass();
        DecryptedGroup decryptedGroup2 = this.groupState_;
        if (decryptedGroup2 == null || decryptedGroup2 == DecryptedGroup.getDefaultInstance()) {
            this.groupState_ = decryptedGroup;
        } else {
            this.groupState_ = DecryptedGroup.newBuilder(this.groupState_).mergeFrom((DecryptedGroup.Builder) decryptedGroup).buildPartial();
        }
    }

    public void clearGroupState() {
        this.groupState_ = null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
    public boolean hasPreviousGroupState() {
        return this.previousGroupState_ != null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
    public DecryptedGroup getPreviousGroupState() {
        DecryptedGroup decryptedGroup = this.previousGroupState_;
        return decryptedGroup == null ? DecryptedGroup.getDefaultInstance() : decryptedGroup;
    }

    public void setPreviousGroupState(DecryptedGroup decryptedGroup) {
        decryptedGroup.getClass();
        this.previousGroupState_ = decryptedGroup;
    }

    public void mergePreviousGroupState(DecryptedGroup decryptedGroup) {
        decryptedGroup.getClass();
        DecryptedGroup decryptedGroup2 = this.previousGroupState_;
        if (decryptedGroup2 == null || decryptedGroup2 == DecryptedGroup.getDefaultInstance()) {
            this.previousGroupState_ = decryptedGroup;
        } else {
            this.previousGroupState_ = DecryptedGroup.newBuilder(this.previousGroupState_).mergeFrom((DecryptedGroup.Builder) decryptedGroup).buildPartial();
        }
    }

    public void clearPreviousGroupState() {
        this.previousGroupState_ = null;
    }

    public static DecryptedGroupV2Context parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DecryptedGroupV2Context parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DecryptedGroupV2Context parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DecryptedGroupV2Context parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DecryptedGroupV2Context parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DecryptedGroupV2Context parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DecryptedGroupV2Context parseFrom(InputStream inputStream) throws IOException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedGroupV2Context parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedGroupV2Context parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedGroupV2Context parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedGroupV2Context parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DecryptedGroupV2Context parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroupV2Context) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DecryptedGroupV2Context decryptedGroupV2Context) {
        return DEFAULT_INSTANCE.createBuilder(decryptedGroupV2Context);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DecryptedGroupV2Context, Builder> implements DecryptedGroupV2ContextOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DecryptedGroupV2Context.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
        public boolean hasContext() {
            return ((DecryptedGroupV2Context) this.instance).hasContext();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
        public SignalServiceProtos.GroupContextV2 getContext() {
            return ((DecryptedGroupV2Context) this.instance).getContext();
        }

        public Builder setContext(SignalServiceProtos.GroupContextV2 groupContextV2) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).setContext(groupContextV2);
            return this;
        }

        public Builder setContext(SignalServiceProtos.GroupContextV2.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).setContext(builder.build());
            return this;
        }

        public Builder mergeContext(SignalServiceProtos.GroupContextV2 groupContextV2) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).mergeContext(groupContextV2);
            return this;
        }

        public Builder clearContext() {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).clearContext();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
        public boolean hasChange() {
            return ((DecryptedGroupV2Context) this.instance).hasChange();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
        public DecryptedGroupChange getChange() {
            return ((DecryptedGroupV2Context) this.instance).getChange();
        }

        public Builder setChange(DecryptedGroupChange decryptedGroupChange) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).setChange(decryptedGroupChange);
            return this;
        }

        public Builder setChange(DecryptedGroupChange.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).setChange(builder.build());
            return this;
        }

        public Builder mergeChange(DecryptedGroupChange decryptedGroupChange) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).mergeChange(decryptedGroupChange);
            return this;
        }

        public Builder clearChange() {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).clearChange();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
        public boolean hasGroupState() {
            return ((DecryptedGroupV2Context) this.instance).hasGroupState();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
        public DecryptedGroup getGroupState() {
            return ((DecryptedGroupV2Context) this.instance).getGroupState();
        }

        public Builder setGroupState(DecryptedGroup decryptedGroup) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).setGroupState(decryptedGroup);
            return this;
        }

        public Builder setGroupState(DecryptedGroup.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).setGroupState(builder.build());
            return this;
        }

        public Builder mergeGroupState(DecryptedGroup decryptedGroup) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).mergeGroupState(decryptedGroup);
            return this;
        }

        public Builder clearGroupState() {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).clearGroupState();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
        public boolean hasPreviousGroupState() {
            return ((DecryptedGroupV2Context) this.instance).hasPreviousGroupState();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2ContextOrBuilder
        public DecryptedGroup getPreviousGroupState() {
            return ((DecryptedGroupV2Context) this.instance).getPreviousGroupState();
        }

        public Builder setPreviousGroupState(DecryptedGroup decryptedGroup) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).setPreviousGroupState(decryptedGroup);
            return this;
        }

        public Builder setPreviousGroupState(DecryptedGroup.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).setPreviousGroupState(builder.build());
            return this;
        }

        public Builder mergePreviousGroupState(DecryptedGroup decryptedGroup) {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).mergePreviousGroupState(decryptedGroup);
            return this;
        }

        public Builder clearPreviousGroupState() {
            copyOnWrite();
            ((DecryptedGroupV2Context) this.instance).clearPreviousGroupState();
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2Context$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DecryptedGroupV2Context();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\t\u0002\t\u0003\t\u0004\t", new Object[]{"context_", "change_", "groupState_", "previousGroupState_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DecryptedGroupV2Context> parser = PARSER;
                if (parser == null) {
                    synchronized (DecryptedGroupV2Context.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DecryptedGroupV2Context decryptedGroupV2Context = new DecryptedGroupV2Context();
        DEFAULT_INSTANCE = decryptedGroupV2Context;
        GeneratedMessageLite.registerDefaultInstance(DecryptedGroupV2Context.class, decryptedGroupV2Context);
    }

    public static DecryptedGroupV2Context getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DecryptedGroupV2Context> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
