package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.thoughtcrime.securesms.database.model.EmojiSearchData;
import org.thoughtcrime.securesms.util.FtsUtil;

/* loaded from: classes4.dex */
public class EmojiSearchDatabase extends Database {
    public static final String CREATE_TABLE;
    public static final String EMOJI;
    public static final String LABEL;
    public static final String TABLE_NAME;

    public EmojiSearchDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public List<String> query(String str, int i) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String createPrefixMatchString = FtsUtil.createPrefixMatchString(str);
        LinkedList linkedList = new LinkedList();
        if (TextUtils.isEmpty(createPrefixMatchString)) {
            return linkedList;
        }
        Cursor query = signalReadableDatabase.query(true, TABLE_NAME, new String[]{"emoji"}, "label MATCH (?)", SqlUtil.buildArgs(createPrefixMatchString), null, null, "rank", String.valueOf(i));
        while (query.moveToNext()) {
            try {
                linkedList.add(CursorUtil.requireString(query, "emoji"));
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return linkedList;
    }

    public void setSearchIndex(List<EmojiSearchData> list) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        signalReadableDatabase.beginTransaction();
        try {
            signalReadableDatabase.delete(TABLE_NAME, (String) null, (String[]) null);
            for (EmojiSearchData emojiSearchData : list) {
                for (String str : emojiSearchData.getTags()) {
                    ContentValues contentValues = new ContentValues(2);
                    contentValues.put(LABEL, str);
                    contentValues.put("emoji", emojiSearchData.getEmoji());
                    signalReadableDatabase.insert(TABLE_NAME, (String) null, contentValues);
                }
            }
            signalReadableDatabase.setTransactionSuccessful();
        } finally {
            signalReadableDatabase.endTransaction();
        }
    }
}
