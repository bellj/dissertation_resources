package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor;

/* loaded from: classes4.dex */
public interface ChatColorOrBuilder extends MessageLiteOrBuilder {
    ChatColor.ChatColorCase getChatColorCase();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    ChatColor.LinearGradient getLinearGradient();

    ChatColor.SingleColor getSingleColor();

    boolean hasLinearGradient();

    boolean hasSingleColor();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
