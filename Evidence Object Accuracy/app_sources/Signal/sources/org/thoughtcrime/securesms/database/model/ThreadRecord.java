package org.thoughtcrime.securesms.database.model;

import android.net.Uri;
import java.util.Objects;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public final class ThreadRecord {
    private final boolean archived;
    private final String body;
    private final String contentType;
    private final long date;
    private final int deliveryReceiptCount;
    private final long deliveryStatus;
    private final int distributionType;
    private final long expiresIn;
    private final ThreadDatabase.Extra extra;
    private final boolean forcedUnread;
    private final boolean isPinned;
    private final long lastSeen;
    private final boolean meaningfulMessages;
    private final int readReceiptCount;
    private final Recipient recipient;
    private final Uri snippetUri;
    private final long threadId;
    private final long type;
    private final int unreadCount;

    private ThreadRecord(Builder builder) {
        this.threadId = builder.threadId;
        this.body = builder.body;
        this.recipient = builder.recipient;
        this.date = builder.date;
        this.type = builder.type;
        this.deliveryStatus = builder.deliveryStatus;
        this.deliveryReceiptCount = builder.deliveryReceiptCount;
        this.readReceiptCount = builder.readReceiptCount;
        this.snippetUri = builder.snippetUri;
        this.contentType = builder.contentType;
        this.extra = builder.extra;
        this.meaningfulMessages = builder.meaningfulMessages;
        this.unreadCount = builder.unreadCount;
        this.forcedUnread = builder.forcedUnread;
        this.distributionType = builder.distributionType;
        this.archived = builder.archived;
        this.expiresIn = builder.expiresIn;
        this.lastSeen = builder.lastSeen;
        this.isPinned = builder.isPinned;
    }

    public long getThreadId() {
        return this.threadId;
    }

    public Recipient getRecipient() {
        return this.recipient;
    }

    public Uri getSnippetUri() {
        return this.snippetUri;
    }

    public String getBody() {
        return this.body;
    }

    public ThreadDatabase.Extra getExtra() {
        return this.extra;
    }

    public String getContentType() {
        return this.contentType;
    }

    public boolean hasMeaningfulMessages() {
        return this.meaningfulMessages;
    }

    public int getUnreadCount() {
        return this.unreadCount;
    }

    public boolean isForcedUnread() {
        return this.forcedUnread;
    }

    public boolean isRead() {
        return this.unreadCount == 0 && !this.forcedUnread;
    }

    public long getDate() {
        return this.date;
    }

    public boolean isArchived() {
        return this.archived;
    }

    public long getType() {
        return this.type;
    }

    public int getDistributionType() {
        return this.distributionType;
    }

    public long getExpiresIn() {
        return this.expiresIn;
    }

    public long getLastSeen() {
        return this.lastSeen;
    }

    public boolean isOutgoing() {
        return MmsSmsColumns.Types.isOutgoingMessageType(this.type);
    }

    public boolean isOutgoingAudioCall() {
        return MmsSmsColumns.Types.isOutgoingAudioCall(this.type);
    }

    public boolean isOutgoingVideoCall() {
        return MmsSmsColumns.Types.isOutgoingVideoCall(this.type);
    }

    public boolean isVerificationStatusChange() {
        return StatusUtil.isVerificationStatusChange(this.type);
    }

    public boolean isPending() {
        return StatusUtil.isPending(this.type);
    }

    public boolean isFailed() {
        return StatusUtil.isFailed(this.type, this.deliveryStatus);
    }

    public boolean isRemoteRead() {
        return this.readReceiptCount > 0;
    }

    public boolean isPendingInsecureSmsFallback() {
        return MmsSmsColumns.Types.isPendingInsecureSmsFallbackType(this.type);
    }

    public boolean isDelivered() {
        return StatusUtil.isDelivered(this.deliveryStatus, this.deliveryReceiptCount);
    }

    public RecipientId getGroupAddedBy() {
        ThreadDatabase.Extra extra = this.extra;
        if (extra == null || extra.getGroupAddedBy() == null) {
            return null;
        }
        return RecipientId.from(this.extra.getGroupAddedBy());
    }

    public RecipientId getIndividualRecipientId() {
        ThreadDatabase.Extra extra = this.extra;
        if (extra != null && extra.getIndividualRecipientId() != null) {
            return RecipientId.from(this.extra.getIndividualRecipientId());
        }
        if (getRecipient().isGroup()) {
            return RecipientId.UNKNOWN;
        }
        return getRecipient().getId();
    }

    public RecipientId getGroupMessageSender() {
        RecipientId id = getRecipient().getId();
        RecipientId individualRecipientId = getIndividualRecipientId();
        return id.equals(individualRecipientId) ? Recipient.self().getId() : individualRecipientId;
    }

    public boolean isGv2Invite() {
        ThreadDatabase.Extra extra = this.extra;
        return extra != null && extra.isGv2Invite();
    }

    public boolean isMessageRequestAccepted() {
        ThreadDatabase.Extra extra = this.extra;
        if (extra != null) {
            return extra.isMessageRequestAccepted();
        }
        return true;
    }

    public boolean isPinned() {
        return this.isPinned;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ThreadRecord.class != obj.getClass()) {
            return false;
        }
        ThreadRecord threadRecord = (ThreadRecord) obj;
        if (this.threadId == threadRecord.threadId && this.type == threadRecord.type && this.date == threadRecord.date && this.deliveryStatus == threadRecord.deliveryStatus && this.deliveryReceiptCount == threadRecord.deliveryReceiptCount && this.readReceiptCount == threadRecord.readReceiptCount && this.meaningfulMessages == threadRecord.meaningfulMessages && this.unreadCount == threadRecord.unreadCount && this.forcedUnread == threadRecord.forcedUnread && this.distributionType == threadRecord.distributionType && this.archived == threadRecord.archived && this.expiresIn == threadRecord.expiresIn && this.lastSeen == threadRecord.lastSeen && this.isPinned == threadRecord.isPinned && this.body.equals(threadRecord.body) && this.recipient.equals(threadRecord.recipient) && Objects.equals(this.snippetUri, threadRecord.snippetUri) && Objects.equals(this.contentType, threadRecord.contentType) && Objects.equals(this.extra, threadRecord.extra)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Objects.hash(Long.valueOf(this.threadId), this.body, this.recipient, Long.valueOf(this.type), Long.valueOf(this.date), Long.valueOf(this.deliveryStatus), Integer.valueOf(this.deliveryReceiptCount), Integer.valueOf(this.readReceiptCount), this.snippetUri, this.contentType, this.extra, Boolean.valueOf(this.meaningfulMessages), Integer.valueOf(this.unreadCount), Boolean.valueOf(this.forcedUnread), Integer.valueOf(this.distributionType), Boolean.valueOf(this.archived), Long.valueOf(this.expiresIn), Long.valueOf(this.lastSeen), Boolean.valueOf(this.isPinned));
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private boolean archived;
        private String body;
        private String contentType;
        private long date;
        private int deliveryReceiptCount;
        private long deliveryStatus;
        private int distributionType;
        private long expiresIn;
        private ThreadDatabase.Extra extra;
        private boolean forcedUnread;
        private boolean isPinned;
        private long lastSeen;
        private boolean meaningfulMessages;
        private int readReceiptCount;
        private Recipient recipient = Recipient.UNKNOWN;
        private Uri snippetUri;
        private long threadId;
        private long type;
        private int unreadCount;

        public Builder(long j) {
            this.threadId = j;
        }

        public Builder setBody(String str) {
            this.body = str;
            return this;
        }

        public Builder setRecipient(Recipient recipient) {
            this.recipient = recipient;
            return this;
        }

        public Builder setType(long j) {
            this.type = j;
            return this;
        }

        public Builder setThreadId(long j) {
            this.threadId = j;
            return this;
        }

        public Builder setDate(long j) {
            this.date = j;
            return this;
        }

        public Builder setDeliveryStatus(long j) {
            this.deliveryStatus = j;
            return this;
        }

        public Builder setDeliveryReceiptCount(int i) {
            this.deliveryReceiptCount = i;
            return this;
        }

        public Builder setReadReceiptCount(int i) {
            this.readReceiptCount = i;
            return this;
        }

        public Builder setSnippetUri(Uri uri) {
            this.snippetUri = uri;
            return this;
        }

        public Builder setContentType(String str) {
            this.contentType = str;
            return this;
        }

        public Builder setExtra(ThreadDatabase.Extra extra) {
            this.extra = extra;
            return this;
        }

        public Builder setMeaningfulMessages(boolean z) {
            this.meaningfulMessages = z;
            return this;
        }

        public Builder setUnreadCount(int i) {
            this.unreadCount = i;
            return this;
        }

        public Builder setForcedUnread(boolean z) {
            this.forcedUnread = z;
            return this;
        }

        public Builder setDistributionType(int i) {
            this.distributionType = i;
            return this;
        }

        public Builder setArchived(boolean z) {
            this.archived = z;
            return this;
        }

        public Builder setExpiresIn(long j) {
            this.expiresIn = j;
            return this;
        }

        public Builder setLastSeen(long j) {
            this.lastSeen = j;
            return this;
        }

        public Builder setPinned(boolean z) {
            this.isPinned = z;
            return this;
        }

        public ThreadRecord build() {
            if (this.distributionType == 2) {
                Preconditions.checkArgument(this.threadId > 0);
                Preconditions.checkNotNull(this.body);
                Preconditions.checkNotNull(this.recipient);
            }
            return new ThreadRecord(this);
        }
    }
}
