package org.thoughtcrime.securesms.database;

import com.annimon.stream.function.Predicate;
import java.util.Set;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MmsDatabase$$ExternalSyntheticLambda6 implements Predicate {
    public final /* synthetic */ Set f$0;

    public /* synthetic */ MmsDatabase$$ExternalSyntheticLambda6(Set set) {
        this.f$0 = set;
    }

    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return this.f$0.contains((DatabaseAttachment) obj);
    }
}
