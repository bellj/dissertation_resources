package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.text.SpannableStringBuilder;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.google.protobuf.InvalidProtocolBufferException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public final class MentionUtil {
    static final String MENTION_PLACEHOLDER;
    public static final char MENTION_STARTER;

    private MentionUtil() {
    }

    public static CharSequence updateBodyWithDisplayNames(Context context, MessageRecord messageRecord) {
        return updateBodyWithDisplayNames(context, messageRecord, messageRecord.getDisplayBody(context));
    }

    public static CharSequence updateBodyWithDisplayNames(Context context, MessageRecord messageRecord, CharSequence charSequence) {
        CharSequence body;
        return (!messageRecord.isMms() || (body = updateBodyAndMentionsWithDisplayNames(context, charSequence, SignalDatabase.mentions().getMentionsForMessage(messageRecord.getId())).getBody()) == null) ? charSequence : body;
    }

    public static UpdatedBodyAndMentions updateBodyAndMentionsWithDisplayNames(Context context, MessageRecord messageRecord, List<Mention> list) {
        return updateBodyAndMentionsWithDisplayNames(context, messageRecord.getDisplayBody(context), list);
    }

    public static /* synthetic */ CharSequence lambda$updateBodyAndMentionsWithDisplayNames$0(Context context, Mention mention) {
        return MENTION_STARTER + Recipient.resolved(mention.getRecipientId()).getMentionDisplayName(context);
    }

    public static UpdatedBodyAndMentions updateBodyAndMentionsWithDisplayNames(Context context, CharSequence charSequence, List<Mention> list) {
        return update(charSequence, list, new Function(context) { // from class: org.thoughtcrime.securesms.database.MentionUtil$$ExternalSyntheticLambda3
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MentionUtil.lambda$updateBodyAndMentionsWithDisplayNames$0(this.f$0, (Mention) obj);
            }
        });
    }

    public static UpdatedBodyAndMentions updateBodyAndMentionsWithPlaceholders(CharSequence charSequence, List<Mention> list) {
        return update(charSequence, list, new Function() { // from class: org.thoughtcrime.securesms.database.MentionUtil$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                Mention mention = (Mention) obj;
                return MentionUtil.MENTION_PLACEHOLDER;
            }
        });
    }

    private static UpdatedBodyAndMentions update(CharSequence charSequence, List<Mention> list, Function<Mention, CharSequence> function) {
        if (charSequence == null || list.isEmpty()) {
            return new UpdatedBodyAndMentions(charSequence, list);
        }
        TreeSet<Mention> treeSet = new TreeSet(list);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        ArrayList arrayList = new ArrayList();
        int i = 0;
        for (Mention mention : treeSet) {
            if (!invalidMention(charSequence, mention)) {
                spannableStringBuilder.append(charSequence.subSequence(i, mention.getStart()));
                CharSequence apply = function.apply(mention);
                Mention mention2 = new Mention(mention.getRecipientId(), spannableStringBuilder.length(), apply.length());
                spannableStringBuilder.append(apply);
                arrayList.add(mention2);
                i = mention.getStart() + mention.getLength();
            }
        }
        if (i < charSequence.length()) {
            spannableStringBuilder.append(charSequence.subSequence(i, charSequence.length()));
        }
        return new UpdatedBodyAndMentions(spannableStringBuilder.toString(), arrayList);
    }

    public static BodyRangeList mentionsToBodyRangeList(List<Mention> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        BodyRangeList.Builder newBuilder = BodyRangeList.newBuilder();
        for (Mention mention : list) {
            newBuilder.addRanges(BodyRangeList.BodyRange.newBuilder().setMentionUuid(Recipient.resolved(mention.getRecipientId()).requireServiceId().toString()).setStart(mention.getStart()).setLength(mention.getLength()));
        }
        return newBuilder.build();
    }

    public static List<Mention> bodyRangeListToMentions(Context context, byte[] bArr) {
        if (bArr == null) {
            return Collections.emptyList();
        }
        try {
            return Stream.of(BodyRangeList.parseFrom(bArr).getRangesList()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.MentionUtil$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return MentionUtil.lambda$bodyRangeListToMentions$2((BodyRangeList.BodyRange) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.database.MentionUtil$$ExternalSyntheticLambda2
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return MentionUtil.lambda$bodyRangeListToMentions$3((BodyRangeList.BodyRange) obj);
                }
            }).toList();
        } catch (InvalidProtocolBufferException unused) {
            return Collections.emptyList();
        }
    }

    public static /* synthetic */ boolean lambda$bodyRangeListToMentions$2(BodyRangeList.BodyRange bodyRange) {
        return bodyRange.getAssociatedValueCase() == BodyRangeList.BodyRange.AssociatedValueCase.MENTIONUUID;
    }

    public static /* synthetic */ Mention lambda$bodyRangeListToMentions$3(BodyRangeList.BodyRange bodyRange) {
        return new Mention(Recipient.externalPush(ServiceId.parseOrThrow(bodyRange.getMentionUuid())).getId(), bodyRange.getStart(), bodyRange.getLength());
    }

    /* renamed from: org.thoughtcrime.securesms.database.MentionUtil$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$MentionSetting;

        static {
            int[] iArr = new int[RecipientDatabase.MentionSetting.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$MentionSetting = iArr;
            try {
                iArr[RecipientDatabase.MentionSetting.ALWAYS_NOTIFY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$MentionSetting[RecipientDatabase.MentionSetting.DO_NOT_NOTIFY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public static String getMentionSettingDisplayValue(Context context, RecipientDatabase.MentionSetting mentionSetting) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$MentionSetting[mentionSetting.ordinal()];
        if (i == 1) {
            return context.getString(R.string.GroupMentionSettingDialog_always_notify_me);
        }
        if (i == 2) {
            return context.getString(R.string.GroupMentionSettingDialog_dont_notify_me);
        }
        throw new IllegalArgumentException("Unknown mention setting: " + mentionSetting);
    }

    private static boolean invalidMention(CharSequence charSequence, Mention mention) {
        int start = mention.getStart();
        int length = mention.getLength();
        return start < 0 || length < 0 || start + length > charSequence.length();
    }

    /* loaded from: classes4.dex */
    public static class UpdatedBodyAndMentions {
        private final CharSequence body;
        private final List<Mention> mentions;

        public UpdatedBodyAndMentions(CharSequence charSequence, List<Mention> list) {
            this.body = charSequence;
            this.mentions = list;
        }

        public CharSequence getBody() {
            return this.body;
        }

        public List<Mention> getMentions() {
            return this.mentions;
        }

        public String getBodyAsString() {
            CharSequence charSequence = this.body;
            if (charSequence != null) {
                return charSequence.toString();
            }
            return null;
        }
    }
}
