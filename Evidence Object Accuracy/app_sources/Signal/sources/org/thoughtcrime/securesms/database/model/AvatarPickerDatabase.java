package org.thoughtcrime.securesms.database.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__IndentKt;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.Database;
import org.thoughtcrime.securesms.database.SQLiteDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar;
import org.thoughtcrime.securesms.groups.GroupId;

/* compiled from: AvatarPickerDatabase.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\fJ\u0018\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0002J\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\n0\f2\u0006\u0010\u000e\u001a\u00020\u000fJ\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\n0\fJ\u000e\u0010\u0012\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u001a\u0010\u0013\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0002J\u0016\u0010\u0014\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0015\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u0016\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0014\u0010\u0017\u001a\u00020\n*\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\f\u0010\u001b\u001a\u00020\u0018*\u00020\nH\u0002¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/AvatarPickerDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "deleteAvatar", "", AvatarPickerDatabase.AVATAR, "Lorg/thoughtcrime/securesms/avatar/Avatar;", "getAllAvatars", "", "getAvatars", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "getAvatarsForGroup", "getAvatarsForSelf", "markUsage", "saveAvatar", "saveAvatarForGroup", "saveAvatarForSelf", "update", "toAvatar", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/CustomAvatar;", ContactRepository.ID_COLUMN, "", "toProto", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AvatarPickerDatabase extends Database {
    private static final String AVATAR;
    public static final String CREATE_TABLE = StringsKt__IndentKt.trimIndent("\n      CREATE TABLE " + TABLE_NAME + " (\n        _id INTEGER PRIMARY KEY AUTOINCREMENT,\n        " + LAST_USED + " INTEGER DEFAULT 0,\n        group_id TEXT DEFAULT NULL,\n        " + AVATAR + " BLOB NOT NULL\n      )\n    ");
    public static final Companion Companion = new Companion(null);
    private static final String GROUP_ID;
    private static final String ID;
    private static final String LAST_USED;
    public static final String TABLE_NAME;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AvatarPickerDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    /* compiled from: AvatarPickerDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/AvatarPickerDatabase$Companion;", "", "()V", "AVATAR", "", "CREATE_TABLE", "GROUP_ID", "ID", "LAST_USED", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final Avatar saveAvatarForSelf(Avatar avatar) {
        Intrinsics.checkNotNullParameter(avatar, AVATAR);
        return saveAvatar(avatar, null);
    }

    public final Avatar saveAvatarForGroup(Avatar avatar, GroupId groupId) {
        Intrinsics.checkNotNullParameter(avatar, AVATAR);
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        return saveAvatar(avatar, groupId);
    }

    public final void markUsage(Avatar avatar) {
        Intrinsics.checkNotNullParameter(avatar, AVATAR);
        Avatar.DatabaseId databaseId = avatar.getDatabaseId();
        if (databaseId instanceof Avatar.DatabaseId.Saved) {
            SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
            String[] buildArgs = SqlUtil.buildArgs(((Avatar.DatabaseId.Saved) databaseId).getId());
            ContentValues contentValues = new ContentValues(1);
            contentValues.put(LAST_USED, Long.valueOf(System.currentTimeMillis()));
            signalWritableDatabase.update(TABLE_NAME, contentValues, "_id = ?", buildArgs);
            return;
        }
        throw new IllegalArgumentException("Must save this avatar before trying to mark usage.");
    }

    public final void update(Avatar avatar) {
        Intrinsics.checkNotNullParameter(avatar, AVATAR);
        Avatar.DatabaseId databaseId = avatar.getDatabaseId();
        if (databaseId instanceof Avatar.DatabaseId.Saved) {
            SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
            ContentValues contentValues = new ContentValues(1);
            contentValues.put(AVATAR, toProto(avatar).toByteArray());
            signalWritableDatabase.update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(((Avatar.DatabaseId.Saved) databaseId).getId()));
            return;
        }
        throw new IllegalArgumentException("Cannot update an unsaved avatar");
    }

    public final void deleteAvatar(Avatar avatar) {
        Intrinsics.checkNotNullParameter(avatar, AVATAR);
        Avatar.DatabaseId databaseId = avatar.getDatabaseId();
        if (databaseId instanceof Avatar.DatabaseId.Saved) {
            this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, "_id = ?", SqlUtil.buildArgs(((Avatar.DatabaseId.Saved) databaseId).getId()));
            return;
        }
        throw new IllegalArgumentException("Cannot delete an unsaved avatar.");
    }

    private final Avatar saveAvatar(Avatar avatar, GroupId groupId) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        Avatar.DatabaseId databaseId = avatar.getDatabaseId();
        if (databaseId instanceof Avatar.DatabaseId.DoNotPersist) {
            throw new IllegalArgumentException("Cannot persist this avatar");
        } else if (databaseId instanceof Avatar.DatabaseId.Saved) {
            ContentValues contentValues = new ContentValues(2);
            contentValues.put(AVATAR, toProto(avatar).toByteArray());
            signalWritableDatabase.update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(((Avatar.DatabaseId.Saved) databaseId).getId()));
            return avatar;
        } else {
            ContentValues contentValues2 = new ContentValues(4);
            contentValues2.put(AVATAR, toProto(avatar).toByteArray());
            if (groupId != null) {
                contentValues2.put("group_id", groupId.toString());
            }
            long insert = signalWritableDatabase.insert(TABLE_NAME, (String) null, contentValues2);
            if (insert != -1) {
                return avatar.withDatabaseId(new Avatar.DatabaseId.Saved(insert));
            }
            throw new AssertionError("Failed to save avatar");
        }
    }

    public final List<Avatar> getAllAvatars() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        ArrayList arrayList = new ArrayList();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, SqlUtil.buildArgs("_id", AVATAR), null, null, null, null, null);
        if (query != null) {
            th = null;
            while (query.moveToNext()) {
                try {
                    long requireLong = CursorUtil.requireLong(query, "_id");
                    CustomAvatar parseFrom = CustomAvatar.parseFrom(CursorUtil.requireBlob(query, AVATAR));
                    Intrinsics.checkNotNullExpressionValue(parseFrom, "proto");
                    arrayList.add(toAvatar(parseFrom, requireLong));
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        return arrayList;
    }

    public final List<Avatar> getAvatarsForSelf() {
        return getAvatars(null);
    }

    public final List<Avatar> getAvatarsForGroup(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        return getAvatars(groupId);
    }

    private final List<Avatar> getAvatars(GroupId groupId) {
        Pair pair;
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        ArrayList arrayList = new ArrayList();
        th = null;
        if (groupId == null) {
            pair = new Pair("group_id is NULL", th);
        } else {
            pair = new Pair("group_id = ?", SqlUtil.buildArgs(groupId));
        }
        Cursor query = signalReadableDatabase.query(TABLE_NAME, SqlUtil.buildArgs("_id", AVATAR), (String) pair.component1(), (String[]) pair.component2(), null, null, "last_used DESC");
        if (query != null) {
            while (query.moveToNext()) {
                try {
                    long requireLong = CursorUtil.requireLong(query, "_id");
                    CustomAvatar parseFrom = CustomAvatar.parseFrom(CursorUtil.requireBlob(query, AVATAR));
                    Intrinsics.checkNotNullExpressionValue(parseFrom, "proto");
                    arrayList.add(toAvatar(parseFrom, requireLong));
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        return arrayList;
    }

    private final CustomAvatar toProto(Avatar avatar) {
        if (avatar instanceof Avatar.Photo) {
            CustomAvatar build = CustomAvatar.newBuilder().setPhoto(CustomAvatar.Photo.newBuilder().setUri(((Avatar.Photo) avatar).getUri().toString())).build();
            Intrinsics.checkNotNullExpressionValue(build, "newBuilder().setPhoto(Cu….uri.toString())).build()");
            return build;
        } else if (avatar instanceof Avatar.Text) {
            Avatar.Text text = (Avatar.Text) avatar;
            CustomAvatar build2 = CustomAvatar.newBuilder().setText(CustomAvatar.Text.newBuilder().setText(text.getText()).setColors(text.getColor().getCode())).build();
            Intrinsics.checkNotNullExpressionValue(build2, "newBuilder().setText(Cus…this.color.code)).build()");
            return build2;
        } else if (avatar instanceof Avatar.Vector) {
            Avatar.Vector vector = (Avatar.Vector) avatar;
            CustomAvatar build3 = CustomAvatar.newBuilder().setVector(CustomAvatar.Vector.newBuilder().setKey(vector.getKey()).setColors(vector.getColor().getCode())).build();
            Intrinsics.checkNotNullExpressionValue(build3, "newBuilder().setVector(C…this.color.code)).build()");
            return build3;
        } else {
            throw new AssertionError();
        }
    }

    private final Avatar toAvatar(CustomAvatar customAvatar, long j) {
        if (customAvatar.hasPhoto()) {
            Uri parse = Uri.parse(customAvatar.getPhoto().getUri());
            Intrinsics.checkNotNullExpressionValue(parse, "parse(photo.uri)");
            return new Avatar.Photo(parse, customAvatar.getPhoto().getSize(), new Avatar.DatabaseId.Saved(j));
        } else if (customAvatar.hasText()) {
            String text = customAvatar.getText().getText();
            Intrinsics.checkNotNullExpressionValue(text, "text.text");
            Avatars avatars = Avatars.INSTANCE;
            Avatars.ColorPair colorPair = avatars.getColorMap().get(customAvatar.getText().getColors());
            if (colorPair == null) {
                colorPair = avatars.getColors().get(0);
            }
            return new Avatar.Text(text, colorPair, new Avatar.DatabaseId.Saved(j));
        } else if (customAvatar.hasVector()) {
            String key = customAvatar.getVector().getKey();
            Intrinsics.checkNotNullExpressionValue(key, "vector.key");
            Avatars avatars2 = Avatars.INSTANCE;
            Avatars.ColorPair colorPair2 = avatars2.getColorMap().get(customAvatar.getVector().getColors());
            if (colorPair2 == null) {
                colorPair2 = avatars2.getColors().get(0);
            }
            return new Avatar.Vector(key, colorPair2, new Avatar.DatabaseId.Saved(j));
        } else {
            throw new AssertionError();
        }
    }
}
