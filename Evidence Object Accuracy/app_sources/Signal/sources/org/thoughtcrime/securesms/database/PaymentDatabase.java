package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.protobuf.InvalidProtocolBufferException;
import com.mobilecoin.lib.exceptions.SerializationException;
import j$.util.function.Function;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.model.databaseprotos.CryptoValue;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.payments.CryptoValueUtil;
import org.thoughtcrime.securesms.payments.Direction;
import org.thoughtcrime.securesms.payments.FailureReason;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddress;
import org.thoughtcrime.securesms.payments.Payee;
import org.thoughtcrime.securesms.payments.Payment;
import org.thoughtcrime.securesms.payments.State;
import org.thoughtcrime.securesms.payments.proto.PaymentMetaData;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.whispersystems.signalservice.api.payments.Money;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* loaded from: classes4.dex */
public final class PaymentDatabase extends Database {
    private static final String ADDRESS;
    private static final String AMOUNT;
    private static final String BLOCK_INDEX;
    private static final String BLOCK_TIME;
    public static final String[] CREATE_INDEXES = {"CREATE INDEX IF NOT EXISTS timestamp_direction_index ON payments (timestamp, direction);", "CREATE INDEX IF NOT EXISTS timestamp_index ON payments (timestamp);", "CREATE UNIQUE INDEX IF NOT EXISTS receipt_public_key_index ON payments (receipt_public_key);"};
    public static final String CREATE_TABLE;
    private static final String DIRECTION;
    private static final String FAILURE;
    private static final String FEE;
    private static final String ID;
    private static final String META_DATA;
    private static final String NOTE;
    private static final String PAYMENT_UUID;
    private static final String PUBLIC_KEY;
    private static final String RECEIPT;
    private static final String RECIPIENT_ID;
    private static final String SEEN;
    private static final String STATE;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(PaymentDatabase.class);
    private static final String TIMESTAMP;
    private static final String TRANSACTION;
    private final MutableLiveData<Object> changeSignal = new MutableLiveData<>(new Object());

    public PaymentDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public void createIncomingPayment(UUID uuid, RecipientId recipientId, long j, String str, Money money, Money money2, byte[] bArr) throws PublicKeyConflictException, SerializationException {
        create(uuid, recipientId, null, j, 0, str, Direction.RECEIVED, State.SUBMITTED, money, money2, null, bArr, null, false);
    }

    public void createOutgoingPayment(UUID uuid, RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress, long j, String str, Money money) {
        try {
            create(uuid, recipientId, mobileCoinPublicAddress, j, 0, str, Direction.SENT, State.INITIAL, money, money.toZero(), null, null, null, true);
        } catch (SerializationException e) {
            throw new IllegalArgumentException(e);
        } catch (PublicKeyConflictException e2) {
            Log.w(TAG, "Tried to create payment but the public key appears already in the database", e2);
            throw new IllegalArgumentException(e2);
        }
    }

    public void createSuccessfulPayment(UUID uuid, RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress, long j, long j2, String str, Money money, Money money2, byte[] bArr, PaymentMetaData paymentMetaData) throws SerializationException {
        try {
            create(uuid, recipientId, mobileCoinPublicAddress, j, j2, str, Direction.SENT, State.SUCCESSFUL, money, money2, null, bArr, paymentMetaData, true);
        } catch (PublicKeyConflictException e) {
            Log.w(TAG, "Tried to create payment but the public key appears already in the database", e);
            throw new AssertionError(e);
        }
    }

    public void createDefrag(UUID uuid, RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress, long j, Money money, byte[] bArr, byte[] bArr2) {
        try {
            create(uuid, recipientId, mobileCoinPublicAddress, j, 0, "", Direction.SENT, State.SUBMITTED, money.toZero(), money, bArr, bArr2, null, true);
        } catch (SerializationException e) {
            throw new IllegalArgumentException(e);
        } catch (PublicKeyConflictException e2) {
            Log.w(TAG, "Tried to create payment but the public key appears already in the database", e2);
            throw new AssertionError(e2);
        }
    }

    private void create(UUID uuid, RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress, long j, long j2, String str, Direction direction, State state, Money money, Money money2, byte[] bArr, byte[] bArr2, PaymentMetaData paymentMetaData, boolean z) throws PublicKeyConflictException, SerializationException {
        if (recipientId == null && mobileCoinPublicAddress == null) {
            throw new AssertionError();
        } else if (money.isNegative()) {
            throw new AssertionError();
        } else if (!money2.isNegative()) {
            SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
            ContentValues contentValues = new ContentValues(15);
            contentValues.put("uuid", uuid.toString());
            if (recipientId == null || recipientId.isUnknown()) {
                contentValues.put("recipient", (Integer) 0);
            } else {
                contentValues.put("recipient", recipientId.serialize());
            }
            if (mobileCoinPublicAddress == null) {
                contentValues.putNull(ADDRESS);
            } else {
                contentValues.put(ADDRESS, mobileCoinPublicAddress.getPaymentAddressBase58());
            }
            contentValues.put("timestamp", Long.valueOf(j));
            contentValues.put(BLOCK_INDEX, Long.valueOf(j2));
            contentValues.put(NOTE, str);
            contentValues.put(DIRECTION, Integer.valueOf(direction.serialize()));
            contentValues.put(STATE, Integer.valueOf(state.serialize()));
            contentValues.put(AMOUNT, CryptoValueUtil.moneyToCryptoValue(money).toByteArray());
            contentValues.put(FEE, CryptoValueUtil.moneyToCryptoValue(money2).toByteArray());
            if (bArr != null) {
                contentValues.put(TRANSACTION, bArr);
            } else {
                contentValues.putNull(TRANSACTION);
            }
            if (bArr2 != null) {
                contentValues.put(RECEIPT, bArr2);
                contentValues.put(PUBLIC_KEY, Base64.encodeBytes(PaymentMetaDataUtil.receiptPublic(PaymentMetaDataUtil.fromReceipt(bArr2))));
            } else {
                contentValues.putNull(RECEIPT);
                contentValues.putNull(PUBLIC_KEY);
            }
            if (paymentMetaData != null) {
                contentValues.put(META_DATA, paymentMetaData.toByteArray());
            } else {
                contentValues.put(META_DATA, PaymentMetaDataUtil.fromReceiptAndTransaction(bArr2, bArr).toByteArray());
            }
            contentValues.put(SEEN, Integer.valueOf(z ? 1 : 0));
            if (signalWritableDatabase.insert(TABLE_NAME, (String) null, contentValues) != -1) {
                notifyChanged(uuid);
                return;
            }
            throw new PublicKeyConflictException();
        } else {
            throw new AssertionError();
        }
    }

    public void deleteAll() {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, (String) null, (String[]) null);
        Log.i(TAG, "Deleted all records");
    }

    public boolean delete(UUID uuid) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        String[] strArr = {uuid.toString()};
        signalWritableDatabase.beginTransaction();
        try {
            int delete = signalWritableDatabase.delete(TABLE_NAME, "uuid = ?", strArr);
            if (delete <= 1) {
                signalWritableDatabase.setTransactionSuccessful();
                if (delete > 0) {
                    notifyChanged(uuid);
                }
                if (delete > 0) {
                    return true;
                }
                return false;
            }
            Log.w(TAG, "More than one row matches criteria");
            throw new AssertionError();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public List<PaymentTransaction> getAll() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        LinkedList linkedList = new LinkedList();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, null, null, null, null, null, "timestamp DESC");
        while (query.moveToNext()) {
            try {
                linkedList.add(readPayment(query));
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return linkedList;
    }

    public void markAllSeen() {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        LinkedList<UUID> linkedList = new LinkedList();
        String[] buildArgs = SqlUtil.buildArgs("uuid");
        String[] buildArgs2 = SqlUtil.buildArgs(SubscriptionLevels.BOOST_LEVEL);
        contentValues.put(SEEN, (Integer) 1);
        try {
            signalWritableDatabase.beginTransaction();
            Cursor query = signalWritableDatabase.query(TABLE_NAME, buildArgs, "seen != ?", buildArgs2, null, null, null);
            while (query != null && query.moveToNext()) {
                linkedList.add(UUID.fromString(CursorUtil.requireString(query, "uuid")));
            }
            if (query != null) {
                query.close();
            }
            int update = !linkedList.isEmpty() ? signalWritableDatabase.update(TABLE_NAME, contentValues, null, null) : -1;
            signalWritableDatabase.setTransactionSuccessful();
            if (update > 0) {
                for (UUID uuid : linkedList) {
                    notifyUuidChanged(uuid);
                }
                notifyChanged();
            }
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public void markPaymentSeen(UUID uuid) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        String[] strArr = {uuid.toString()};
        contentValues.put(SEEN, (Integer) 1);
        if (signalWritableDatabase.update(TABLE_NAME, contentValues, "uuid = ?", strArr) > 0) {
            notifyChanged(uuid);
        }
    }

    public List<PaymentTransaction> getUnseenPayments() {
        LinkedList linkedList = new LinkedList();
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "seen = 0 AND state = " + State.SUCCESSFUL.serialize(), null, null, null, null);
        while (query.moveToNext()) {
            try {
                linkedList.add(readPayment(query));
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return linkedList;
    }

    public PaymentTransaction getPayment(UUID uuid) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "uuid = ?", new String[]{uuid.toString()}, null, null, null);
        try {
            if (query.moveToNext()) {
                PaymentTransaction readPayment = readPayment(query);
                if (!query.moveToNext()) {
                    query.close();
                    return readPayment;
                }
                throw new AssertionError("Multiple records for one UUID");
            }
            query.close();
            return null;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public /* synthetic */ List lambda$getAllLive$0(Object obj) {
        return getAll();
    }

    public LiveData<List<PaymentTransaction>> getAllLive() {
        return LiveDataUtil.mapAsync(this.changeSignal, new Function() { // from class: org.thoughtcrime.securesms.database.PaymentDatabase$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PaymentDatabase.this.lambda$getAllLive$0(obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public boolean markPaymentSubmitted(UUID uuid, byte[] bArr, byte[] bArr2, Money money) throws PublicKeyConflictException {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        String[] strArr = {uuid.toString()};
        ContentValues contentValues = new ContentValues(6);
        contentValues.put(STATE, Integer.valueOf(State.SUBMITTED.serialize()));
        contentValues.put(TRANSACTION, bArr);
        contentValues.put(RECEIPT, bArr2);
        try {
            contentValues.put(PUBLIC_KEY, Base64.encodeBytes(PaymentMetaDataUtil.receiptPublic(PaymentMetaDataUtil.fromReceipt(bArr2))));
            contentValues.put(META_DATA, PaymentMetaDataUtil.fromReceiptAndTransaction(bArr2, bArr).toByteArray());
            contentValues.put(FEE, CryptoValueUtil.moneyToCryptoValue(money).toByteArray());
            signalWritableDatabase.beginTransaction();
            try {
                int update = signalWritableDatabase.update(TABLE_NAME, contentValues, "uuid = ?", strArr);
                if (update == -1) {
                    throw new PublicKeyConflictException();
                } else if (update <= 1) {
                    signalWritableDatabase.setTransactionSuccessful();
                    if (update > 0) {
                        notifyChanged(uuid);
                    }
                    if (update > 0) {
                        return true;
                    }
                    return false;
                } else {
                    Log.w(TAG, "More than one row matches criteria");
                    throw new AssertionError();
                }
            } finally {
                signalWritableDatabase.endTransaction();
            }
        } catch (SerializationException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public boolean markPaymentSuccessful(UUID uuid, long j) {
        return markPayment(uuid, State.SUCCESSFUL, null, null, Long.valueOf(j));
    }

    public boolean markReceivedPaymentSuccessful(UUID uuid, Money money, long j) {
        return markPayment(uuid, State.SUCCESSFUL, money, null, Long.valueOf(j));
    }

    public boolean markPaymentFailed(UUID uuid, FailureReason failureReason) {
        return markPayment(uuid, State.FAILED, null, failureReason, null);
    }

    private boolean markPayment(UUID uuid, State state, Money money, FailureReason failureReason, Long l) {
        int i;
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        String[] strArr = {uuid.toString()};
        ContentValues contentValues = new ContentValues(3);
        contentValues.put(STATE, Integer.valueOf(state.serialize()));
        if (money != null) {
            contentValues.put(AMOUNT, CryptoValueUtil.moneyToCryptoValue(money).toByteArray());
        }
        if (state == State.FAILED) {
            if (failureReason != null) {
                i = failureReason.serialize();
            } else {
                i = FailureReason.UNKNOWN.serialize();
            }
            contentValues.put(FAILURE, Integer.valueOf(i));
        } else if (failureReason == null) {
            contentValues.putNull(FAILURE);
        } else {
            throw new AssertionError();
        }
        if (l != null) {
            contentValues.put(BLOCK_INDEX, l);
        }
        signalWritableDatabase.beginTransaction();
        try {
            int update = signalWritableDatabase.update(TABLE_NAME, contentValues, "uuid = ?", strArr);
            if (update <= 1) {
                signalWritableDatabase.setTransactionSuccessful();
                if (update > 0) {
                    notifyChanged(uuid);
                }
                if (update > 0) {
                    return true;
                }
                return false;
            }
            Log.w(TAG, "More than one row matches criteria");
            throw new AssertionError();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public boolean updateBlockDetails(UUID uuid, long j, long j2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        String[] strArr = {uuid.toString()};
        ContentValues contentValues = new ContentValues(2);
        contentValues.put(BLOCK_INDEX, Long.valueOf(j));
        contentValues.put(BLOCK_TIME, Long.valueOf(j2));
        signalWritableDatabase.beginTransaction();
        try {
            int update = signalWritableDatabase.update(TABLE_NAME, contentValues, "uuid = ?", strArr);
            if (update <= 1) {
                signalWritableDatabase.setTransactionSuccessful();
                if (update > 0) {
                    notifyChanged(uuid);
                }
                if (update > 0) {
                    return true;
                }
                return false;
            }
            Log.w(TAG, "More than one row matches criteria");
            throw new AssertionError();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    private static PaymentTransaction readPayment(Cursor cursor) {
        return new PaymentTransaction(UUID.fromString(CursorUtil.requireString(cursor, "uuid")), getRecipientId(cursor), MobileCoinPublicAddress.fromBase58NullableOrThrow(CursorUtil.requireString(cursor, ADDRESS)), CursorUtil.requireLong(cursor, "timestamp"), Direction.deserialize(CursorUtil.requireInt(cursor, DIRECTION)), State.deserialize(CursorUtil.requireInt(cursor, STATE)), FailureReason.deserialize(CursorUtil.requireInt(cursor, FAILURE)), CursorUtil.requireString(cursor, NOTE), getMoneyValue(CursorUtil.requireBlob(cursor, AMOUNT)), getMoneyValue(CursorUtil.requireBlob(cursor, FEE)), CursorUtil.requireBlob(cursor, TRANSACTION), CursorUtil.requireBlob(cursor, RECEIPT), PaymentMetaDataUtil.parseOrThrow(CursorUtil.requireBlob(cursor, META_DATA)), Long.valueOf(CursorUtil.requireLong(cursor, BLOCK_INDEX)), CursorUtil.requireLong(cursor, BLOCK_TIME), CursorUtil.requireBoolean(cursor, SEEN));
    }

    private static RecipientId getRecipientId(Cursor cursor) {
        long requireLong = CursorUtil.requireLong(cursor, "recipient");
        if (requireLong == 0) {
            return null;
        }
        return RecipientId.from(requireLong);
    }

    private static Money getMoneyValue(byte[] bArr) {
        try {
            return CryptoValueUtil.cryptoValueToMoney(CryptoValue.parseFrom(bArr));
        } catch (InvalidProtocolBufferException e) {
            throw new AssertionError(e);
        }
    }

    private void notifyChanged(UUID uuid) {
        notifyChanged();
        notifyUuidChanged(uuid);
    }

    private void notifyChanged() {
        this.changeSignal.postValue(new Object());
        ApplicationDependencies.getDatabaseObserver().notifyAllPaymentsListeners();
    }

    private void notifyUuidChanged(UUID uuid) {
        if (uuid != null) {
            ApplicationDependencies.getDatabaseObserver().notifyPaymentListeners(uuid);
        }
    }

    /* loaded from: classes4.dex */
    public static final class PaymentTransaction implements Payment {
        private final Money amount;
        private final Long blockIndex;
        private final long blockTimestamp;
        private final Direction direction;
        private final FailureReason failureReason;
        private final Money fee;
        private final String note;
        private final Payee payee;
        private final PaymentMetaData paymentMetaData;
        private final byte[] receipt;
        private final boolean seen;
        private final State state;
        private final long timestamp;
        private final byte[] transaction;
        private final UUID uuid;

        @Override // org.thoughtcrime.securesms.payments.Payment
        public /* synthetic */ Money getAmountPlusFeeWithDirection() {
            return Payment.CC.$default$getAmountPlusFeeWithDirection(this);
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public /* synthetic */ Money getAmountWithDirection() {
            return Payment.CC.$default$getAmountWithDirection(this);
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public /* synthetic */ long getDisplayTimestamp() {
            return Payment.CC.$default$getDisplayTimestamp(this);
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public /* synthetic */ boolean isDefrag() {
            return Payment.CC.$default$isDefrag(this);
        }

        PaymentTransaction(UUID uuid, RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress, long j, Direction direction, State state, FailureReason failureReason, String str, Money money, Money money2, byte[] bArr, byte[] bArr2, PaymentMetaData paymentMetaData, Long l, long j2, boolean z) {
            this.uuid = uuid;
            this.paymentMetaData = paymentMetaData;
            this.payee = PaymentDatabase.fromPaymentTransaction(recipientId, mobileCoinPublicAddress);
            this.timestamp = j;
            this.direction = direction;
            this.state = state;
            this.failureReason = failureReason;
            this.note = str;
            this.amount = money;
            this.fee = money2;
            this.transaction = bArr;
            this.receipt = bArr2;
            this.blockIndex = l;
            this.blockTimestamp = j2;
            this.seen = z;
            if (money.isNegative()) {
                throw new AssertionError();
            }
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public UUID getUuid() {
            return this.uuid;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public Payee getPayee() {
            return this.payee;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public long getBlockIndex() {
            return this.blockIndex.longValue();
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public long getBlockTimestamp() {
            return this.blockTimestamp;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public long getTimestamp() {
            return this.timestamp;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public Direction getDirection() {
            return this.direction;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public State getState() {
            return this.state;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public FailureReason getFailureReason() {
            return this.failureReason;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public String getNote() {
            return this.note;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public Money getAmount() {
            return this.amount;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public Money getFee() {
            return this.fee;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public PaymentMetaData getPaymentMetaData() {
            return this.paymentMetaData;
        }

        @Override // org.thoughtcrime.securesms.payments.Payment
        public boolean isSeen() {
            return this.seen;
        }

        public byte[] getTransaction() {
            return this.transaction;
        }

        public byte[] getReceipt() {
            return this.receipt;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PaymentTransaction)) {
                return false;
            }
            PaymentTransaction paymentTransaction = (PaymentTransaction) obj;
            if (this.timestamp != paymentTransaction.timestamp || !this.uuid.equals(paymentTransaction.uuid) || !this.payee.equals(paymentTransaction.payee) || this.direction != paymentTransaction.direction || this.state != paymentTransaction.state || !this.note.equals(paymentTransaction.note) || !this.amount.equals(paymentTransaction.amount) || !Arrays.equals(this.transaction, paymentTransaction.transaction) || !Arrays.equals(this.receipt, paymentTransaction.receipt) || !this.paymentMetaData.equals(paymentTransaction.paymentMetaData)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            long j = this.timestamp;
            return (((((((((((((((((this.uuid.hashCode() * 31) + this.payee.hashCode()) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + this.direction.hashCode()) * 31) + this.state.hashCode()) * 31) + this.note.hashCode()) * 31) + this.amount.hashCode()) * 31) + Arrays.hashCode(this.transaction)) * 31) + Arrays.hashCode(this.receipt)) * 31) + this.paymentMetaData.hashCode();
        }
    }

    public static Payee fromPaymentTransaction(RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress) {
        if (recipientId == null && mobileCoinPublicAddress == null) {
            throw new AssertionError();
        } else if (recipientId != null) {
            return Payee.fromRecipientAndAddress(recipientId, mobileCoinPublicAddress);
        } else {
            return new Payee(mobileCoinPublicAddress);
        }
    }

    /* loaded from: classes4.dex */
    public final class PublicKeyConflictException extends Exception {
        private PublicKeyConflictException() {
            PaymentDatabase.this = r1;
        }
    }
}
