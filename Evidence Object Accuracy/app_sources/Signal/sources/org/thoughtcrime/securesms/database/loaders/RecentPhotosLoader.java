package org.thoughtcrime.securesms.database.loaders;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import androidx.loader.content.CursorLoader;
import org.thoughtcrime.securesms.permissions.Permissions;

/* loaded from: classes4.dex */
public class RecentPhotosLoader extends CursorLoader {
    public static Uri BASE_URL = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    private static final String[] PROJECTION = {"_id", "datetaken", "date_modified", "orientation", "mime_type", "bucket_id", "_size", "width", "height"};
    private static final String SELECTION = (Build.VERSION.SDK_INT > 28 ? "is_pending != 1" : "_data IS NULL");
    private final Context context;

    public RecentPhotosLoader(Context context) {
        super(context);
        this.context = context.getApplicationContext();
    }

    @Override // androidx.loader.content.CursorLoader, androidx.loader.content.AsyncTaskLoader
    public Cursor loadInBackground() {
        if (Permissions.hasAll(this.context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            return this.context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, PROJECTION, SELECTION, null, "date_modified DESC");
        }
        return null;
    }
}
