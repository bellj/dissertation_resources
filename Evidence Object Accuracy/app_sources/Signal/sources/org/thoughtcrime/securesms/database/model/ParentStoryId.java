package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* compiled from: ParentStoryId.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \r2\u00020\u0001:\u0003\r\u000e\u000fB\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0007\u001a\u00020\bJ\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\nJ\b\u0010\f\u001a\u00020\u0003H&R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\u0002\u0010\u0011¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/ParentStoryId;", "", ContactRepository.ID_COLUMN, "", "(J)V", "getId", "()J", "asMessageId", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "isDirectReply", "", "isGroupReply", "serialize", "Companion", "DirectReply", "GroupReply", "Lorg/thoughtcrime/securesms/database/model/ParentStoryId$GroupReply;", "Lorg/thoughtcrime/securesms/database/model/ParentStoryId$DirectReply;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class ParentStoryId {
    public static final Companion Companion = new Companion(null);
    private final long id;

    public /* synthetic */ ParentStoryId(long j, DefaultConstructorMarker defaultConstructorMarker) {
        this(j);
    }

    @JvmStatic
    public static final ParentStoryId deserialize(long j) {
        return Companion.deserialize(j);
    }

    public abstract long serialize();

    private ParentStoryId(long j) {
        this.id = j;
    }

    protected final long getId() {
        return this.id;
    }

    public final MessageId asMessageId() {
        return new MessageId(Math.abs(this.id), true);
    }

    public final boolean isGroupReply() {
        return serialize() > 0;
    }

    public final boolean isDirectReply() {
        return !isGroupReply();
    }

    /* compiled from: ParentStoryId.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0003H\u0016¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/ParentStoryId$GroupReply;", "Lorg/thoughtcrime/securesms/database/model/ParentStoryId;", ContactRepository.ID_COLUMN, "", "(J)V", "serialize", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GroupReply extends ParentStoryId {
        public GroupReply(long j) {
            super(j, null);
        }

        @Override // org.thoughtcrime.securesms.database.model.ParentStoryId
        public long serialize() {
            return Math.abs(getId());
        }
    }

    /* compiled from: ParentStoryId.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0003H\u0016¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/ParentStoryId$DirectReply;", "Lorg/thoughtcrime/securesms/database/model/ParentStoryId;", ContactRepository.ID_COLUMN, "", "(J)V", "serialize", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class DirectReply extends ParentStoryId {
        public DirectReply(long j) {
            super(j, null);
        }

        @Override // org.thoughtcrime.securesms.database.model.ParentStoryId
        public long serialize() {
            return -Math.abs(getId());
        }
    }

    /* compiled from: ParentStoryId.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/ParentStoryId$Companion;", "", "()V", "deserialize", "Lorg/thoughtcrime/securesms/database/model/ParentStoryId;", ContactRepository.ID_COLUMN, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final ParentStoryId deserialize(long j) {
            if (j > 0) {
                return new GroupReply(j);
            }
            if (j < 0) {
                return new DirectReply(j);
            }
            return null;
        }
    }
}
