package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;

/* loaded from: classes4.dex */
public interface TemporalAuthCredentialResponseOrBuilder extends MessageLiteOrBuilder {
    ByteString getAuthCredentialResponse();

    long getDate();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
