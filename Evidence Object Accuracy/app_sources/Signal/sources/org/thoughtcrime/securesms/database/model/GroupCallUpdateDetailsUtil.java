package org.thoughtcrime.securesms.database.model;

import java.io.IOException;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class GroupCallUpdateDetailsUtil {
    private static final String TAG = Log.tag(GroupCallUpdateDetailsUtil.class);

    private GroupCallUpdateDetailsUtil() {
    }

    public static GroupCallUpdateDetails parse(String str) {
        GroupCallUpdateDetails defaultInstance = GroupCallUpdateDetails.getDefaultInstance();
        if (str == null) {
            return defaultInstance;
        }
        try {
            return GroupCallUpdateDetails.parseFrom(Base64.decode(str));
        } catch (IOException e) {
            Log.w(TAG, "Group call update details could not be read", e);
            return defaultInstance;
        }
    }

    public static String createUpdatedBody(GroupCallUpdateDetails groupCallUpdateDetails, List<String> list, boolean z) {
        GroupCallUpdateDetails.Builder clearInCallUuids = groupCallUpdateDetails.toBuilder().setIsCallFull(z).clearInCallUuids();
        if (Util.hasItems(list)) {
            clearInCallUuids.addAllInCallUuids(list);
        }
        return Base64.encodeBytes(clearInCallUuids.build().toByteArray());
    }
}
