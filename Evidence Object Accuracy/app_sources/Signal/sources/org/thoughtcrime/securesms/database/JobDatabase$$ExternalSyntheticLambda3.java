package org.thoughtcrime.securesms.database;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class JobDatabase$$ExternalSyntheticLambda3 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return ((JobSpec) obj).isMemoryOnly();
    }
}
