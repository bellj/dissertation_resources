package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import j$.util.Optional;
import java.io.Closeable;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes4.dex */
public class PushDatabase extends Database {
    public static final String CONTENT;
    public static final String CREATE_TABLE;
    public static final String DEVICE_ID;
    public static final String ID;
    public static final String LEGACY_MSG;
    public static final String SERVER_DELIVERED_TIMESTAMP;
    public static final String SERVER_GUID;
    public static final String SERVER_RECEIVED_TIMESTAMP;
    public static final String SOURCE_E164;
    public static final String SOURCE_UUID;
    private static final String TABLE_NAME;
    private static final String TAG = Log.tag(PushDatabase.class);
    public static final String TIMESTAMP;
    public static final String TYPE;

    public PushDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public long insert(SignalServiceEnvelope signalServiceEnvelope) {
        if (find(signalServiceEnvelope).isPresent()) {
            return -1;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", Integer.valueOf(signalServiceEnvelope.getType()));
        contentValues.put(SOURCE_UUID, signalServiceEnvelope.getSourceUuid().orElse(null));
        contentValues.put(SOURCE_E164, signalServiceEnvelope.getSourceE164().orElse(null));
        contentValues.put(DEVICE_ID, Integer.valueOf(signalServiceEnvelope.getSourceDevice()));
        String str = "";
        contentValues.put("body", signalServiceEnvelope.hasLegacyMessage() ? Base64.encodeBytes(signalServiceEnvelope.getLegacyMessage()) : str);
        if (signalServiceEnvelope.hasContent()) {
            str = Base64.encodeBytes(signalServiceEnvelope.getContent());
        }
        contentValues.put("content", str);
        contentValues.put("timestamp", Long.valueOf(signalServiceEnvelope.getTimestamp()));
        contentValues.put(SERVER_RECEIVED_TIMESTAMP, Long.valueOf(signalServiceEnvelope.getServerReceivedTimestamp()));
        contentValues.put(SERVER_DELIVERED_TIMESTAMP, Long.valueOf(signalServiceEnvelope.getServerDeliveredTimestamp()));
        contentValues.put("server_guid", signalServiceEnvelope.getServerGuid());
        return this.databaseHelper.getSignalWritableDatabase().insert(TABLE_NAME, (String) null, contentValues);
    }

    public SignalServiceEnvelope get(long j) throws NoSuchMessageException {
        Throwable th;
        IOException e;
        Cursor cursor = null;
        byte[] bArr = null;
        cursor = null;
        try {
            try {
                Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "_id = ?", new String[]{String.valueOf(j)}, null, null, null);
                if (query != null) {
                    try {
                        if (query.moveToNext()) {
                            String string = query.getString(query.getColumnIndexOrThrow("body"));
                            String string2 = query.getString(query.getColumnIndexOrThrow("content"));
                            String string3 = query.getString(query.getColumnIndexOrThrow(SOURCE_UUID));
                            String string4 = query.getString(query.getColumnIndexOrThrow(SOURCE_E164));
                            int i = query.getInt(query.getColumnIndexOrThrow("type"));
                            Optional<SignalServiceAddress> fromRaw = SignalServiceAddress.fromRaw(string3, string4);
                            int i2 = query.getInt(query.getColumnIndexOrThrow(DEVICE_ID));
                            long j2 = query.getLong(query.getColumnIndexOrThrow("timestamp"));
                            byte[] decode = Util.isEmpty(string) ? null : Base64.decode(string);
                            if (!Util.isEmpty(string2)) {
                                bArr = Base64.decode(string2);
                            }
                            SignalServiceEnvelope signalServiceEnvelope = new SignalServiceEnvelope(i, fromRaw, i2, j2, decode, bArr, query.getLong(query.getColumnIndexOrThrow(SERVER_RECEIVED_TIMESTAMP)), query.getLong(query.getColumnIndexOrThrow(SERVER_DELIVERED_TIMESTAMP)), query.getString(query.getColumnIndexOrThrow("server_guid")), "");
                            query.close();
                            return signalServiceEnvelope;
                        }
                    } catch (IOException e2) {
                        e = e2;
                        cursor = query;
                        Log.w(TAG, e);
                        throw new NoSuchMessageException(e);
                    } catch (Throwable th2) {
                        th = th2;
                        cursor = query;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
                if (query != null) {
                    query.close();
                }
                throw new NoSuchMessageException("Not found");
            } catch (IOException e3) {
                e = e3;
            }
        } catch (Throwable th3) {
            th = th3;
        }
    }

    public Cursor getPending() {
        return this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, null, null, null, null, null);
    }

    public void delete(long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.delete(TABLE_NAME, "_id = ?", new String[]{j + ""});
    }

    public Reader readerFor(Cursor cursor) {
        return new Reader(cursor);
    }

    private Optional<Long> find(SignalServiceEnvelope signalServiceEnvelope) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String[] strArr = new String[7];
        strArr[0] = String.valueOf(signalServiceEnvelope.getType());
        strArr[1] = String.valueOf(signalServiceEnvelope.getSourceDevice());
        String str = "";
        strArr[2] = signalServiceEnvelope.hasLegacyMessage() ? Base64.encodeBytes(signalServiceEnvelope.getLegacyMessage()) : str;
        if (signalServiceEnvelope.hasContent()) {
            str = Base64.encodeBytes(signalServiceEnvelope.getContent());
        }
        strArr[3] = str;
        strArr[4] = String.valueOf(signalServiceEnvelope.getTimestamp());
        strArr[5] = String.valueOf(signalServiceEnvelope.getSourceUuid().orElse(null));
        strArr[6] = String.valueOf(signalServiceEnvelope.getSourceE164().orElse(null));
        Cursor query = signalReadableDatabase.query(TABLE_NAME, null, "type = ? AND device_id = ? AND body = ? AND content = ? AND timestamp = ? AND ((source NOT NULL AND source = ?) OR (source_uuid NOT NULL AND source_uuid = ?))", strArr, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    Optional<Long> of = Optional.of(Long.valueOf(query.getLong(query.getColumnIndexOrThrow("_id"))));
                    query.close();
                    return of;
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        Optional<Long> empty = Optional.empty();
        if (query != null) {
            query.close();
        }
        return empty;
    }

    /* loaded from: classes4.dex */
    public static class Reader implements Closeable {
        private final Cursor cursor;

        public Reader(Cursor cursor) {
            this.cursor = cursor;
        }

        public SignalServiceEnvelope getNext() {
            try {
                Cursor cursor = this.cursor;
                byte[] bArr = null;
                if (cursor != null && cursor.moveToNext()) {
                    Cursor cursor2 = this.cursor;
                    int i = cursor2.getInt(cursor2.getColumnIndexOrThrow("type"));
                    Cursor cursor3 = this.cursor;
                    String string = cursor3.getString(cursor3.getColumnIndexOrThrow(PushDatabase.SOURCE_UUID));
                    Cursor cursor4 = this.cursor;
                    String string2 = cursor4.getString(cursor4.getColumnIndexOrThrow(PushDatabase.SOURCE_E164));
                    Cursor cursor5 = this.cursor;
                    int i2 = cursor5.getInt(cursor5.getColumnIndexOrThrow(PushDatabase.DEVICE_ID));
                    Cursor cursor6 = this.cursor;
                    String string3 = cursor6.getString(cursor6.getColumnIndexOrThrow("body"));
                    Cursor cursor7 = this.cursor;
                    String string4 = cursor7.getString(cursor7.getColumnIndexOrThrow("content"));
                    Cursor cursor8 = this.cursor;
                    long j = cursor8.getLong(cursor8.getColumnIndexOrThrow("timestamp"));
                    Cursor cursor9 = this.cursor;
                    long j2 = cursor9.getLong(cursor9.getColumnIndexOrThrow(PushDatabase.SERVER_RECEIVED_TIMESTAMP));
                    Cursor cursor10 = this.cursor;
                    long j3 = cursor10.getLong(cursor10.getColumnIndexOrThrow(PushDatabase.SERVER_DELIVERED_TIMESTAMP));
                    Cursor cursor11 = this.cursor;
                    String string5 = cursor11.getString(cursor11.getColumnIndexOrThrow("server_guid"));
                    Optional<SignalServiceAddress> fromRaw = SignalServiceAddress.fromRaw(string, string2);
                    byte[] decode = string3 != null ? Base64.decode(string3) : null;
                    if (string4 != null) {
                        bArr = Base64.decode(string4);
                    }
                    return new SignalServiceEnvelope(i, fromRaw, i2, j, decode, bArr, j2, j3, string5, "");
                }
                return null;
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.cursor.close();
        }
    }
}
