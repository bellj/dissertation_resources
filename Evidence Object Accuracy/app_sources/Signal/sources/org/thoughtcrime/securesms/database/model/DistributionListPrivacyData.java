package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: DistributionListPrivacyData.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyData;", "", "privacyMode", "Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "rawMemberCount", "", "memberCount", "(Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;II)V", "getMemberCount", "()I", "getPrivacyMode", "()Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "getRawMemberCount", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DistributionListPrivacyData {
    private final int memberCount;
    private final DistributionListPrivacyMode privacyMode;
    private final int rawMemberCount;

    public static /* synthetic */ DistributionListPrivacyData copy$default(DistributionListPrivacyData distributionListPrivacyData, DistributionListPrivacyMode distributionListPrivacyMode, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            distributionListPrivacyMode = distributionListPrivacyData.privacyMode;
        }
        if ((i3 & 2) != 0) {
            i = distributionListPrivacyData.rawMemberCount;
        }
        if ((i3 & 4) != 0) {
            i2 = distributionListPrivacyData.memberCount;
        }
        return distributionListPrivacyData.copy(distributionListPrivacyMode, i, i2);
    }

    public final DistributionListPrivacyMode component1() {
        return this.privacyMode;
    }

    public final int component2() {
        return this.rawMemberCount;
    }

    public final int component3() {
        return this.memberCount;
    }

    public final DistributionListPrivacyData copy(DistributionListPrivacyMode distributionListPrivacyMode, int i, int i2) {
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        return new DistributionListPrivacyData(distributionListPrivacyMode, i, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DistributionListPrivacyData)) {
            return false;
        }
        DistributionListPrivacyData distributionListPrivacyData = (DistributionListPrivacyData) obj;
        return this.privacyMode == distributionListPrivacyData.privacyMode && this.rawMemberCount == distributionListPrivacyData.rawMemberCount && this.memberCount == distributionListPrivacyData.memberCount;
    }

    public int hashCode() {
        return (((this.privacyMode.hashCode() * 31) + this.rawMemberCount) * 31) + this.memberCount;
    }

    public String toString() {
        return "DistributionListPrivacyData(privacyMode=" + this.privacyMode + ", rawMemberCount=" + this.rawMemberCount + ", memberCount=" + this.memberCount + ')';
    }

    public DistributionListPrivacyData(DistributionListPrivacyMode distributionListPrivacyMode, int i, int i2) {
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        this.privacyMode = distributionListPrivacyMode;
        this.rawMemberCount = i;
        this.memberCount = i2;
    }

    public final DistributionListPrivacyMode getPrivacyMode() {
        return this.privacyMode;
    }

    public final int getRawMemberCount() {
        return this.rawMemberCount;
    }

    public final int getMemberCount() {
        return this.memberCount;
    }
}
