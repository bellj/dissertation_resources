package org.thoughtcrime.securesms.database.model;

import android.content.Context;
import android.text.SpannableString;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class MediaMmsMessageRecord extends MmsMessageRecord {
    private static final String TAG = Log.tag(MediaMmsMessageRecord.class);
    private final boolean mentionsSelf;
    private final BodyRangeList messageRanges;
    private final int partCount;

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isMmsNotification() {
        return false;
    }

    public MediaMmsMessageRecord(long j, Recipient recipient, Recipient recipient2, int i, long j2, long j3, long j4, int i2, long j5, String str, SlideDeck slideDeck, int i3, long j6, Set<IdentityKeyMismatch> set, Set<NetworkFailure> set2, int i4, long j7, long j8, boolean z, int i5, Quote quote, List<Contact> list, List<LinkPreview> list2, boolean z2, List<ReactionRecord> list3, boolean z3, boolean z4, long j9, int i6, long j10, BodyRangeList bodyRangeList, StoryType storyType, ParentStoryId parentStoryId, GiftBadge giftBadge) {
        super(j, str, recipient, recipient2, i, j2, j3, j4, j5, -1, i2, j6, set, set2, i4, j7, j8, z, slideDeck, i5, quote, list, list2, z2, list3, z3, j9, i6, j10, storyType, parentStoryId, giftBadge);
        this.partCount = i3;
        this.mentionsSelf = z4;
        this.messageRanges = bodyRangeList;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean hasSelfMention() {
        return this.mentionsSelf;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord, org.thoughtcrime.securesms.database.model.DisplayRecord
    public SpannableString getDisplayBody(Context context) {
        if (MmsSmsColumns.Types.isChatSessionRefresh(this.type)) {
            return MessageRecord.emphasisAdded(context.getString(R.string.MmsMessageRecord_bad_encrypted_mms_message));
        }
        if (MmsSmsColumns.Types.isDuplicateMessageType(this.type)) {
            return MessageRecord.emphasisAdded(context.getString(R.string.SmsMessageRecord_duplicate_message));
        }
        if (MmsSmsColumns.Types.isNoRemoteSessionType(this.type)) {
            return MessageRecord.emphasisAdded(context.getString(R.string.MmsMessageRecord_mms_message_encrypted_for_non_existing_session));
        }
        if (isLegacyMessage()) {
            return MessageRecord.emphasisAdded(context.getString(R.string.MessageRecord_message_encrypted_with_a_legacy_protocol_version_that_is_no_longer_supported));
        }
        return super.getDisplayBody(context);
    }

    public int getPartCount() {
        return this.partCount;
    }

    public BodyRangeList getMessageRanges() {
        return this.messageRanges;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean hasMessageRanges() {
        return this.messageRanges != null;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public BodyRangeList requireMessageRanges() {
        BodyRangeList bodyRangeList = this.messageRanges;
        Objects.requireNonNull(bodyRangeList);
        return bodyRangeList;
    }

    public MediaMmsMessageRecord withReactions(List<ReactionRecord> list) {
        return new MediaMmsMessageRecord(getId(), getRecipient(), getIndividualRecipient(), getRecipientDeviceId(), getDateSent(), getDateReceived(), getServerTimestamp(), getDeliveryReceiptCount(), getThreadId(), getBody(), getSlideDeck(), getPartCount(), getType(), getIdentityKeyMismatches(), getNetworkFailures(), getSubscriptionId(), getExpiresIn(), getExpireStarted(), isViewOnce(), getReadReceiptCount(), getQuote(), getSharedContacts(), getLinkPreviews(), isUnidentified(), list, isRemoteDelete(), this.mentionsSelf, getNotifiedTimestamp(), getViewedReceiptCount(), getReceiptTimestamp(), getMessageRanges(), getStoryType(), getParentStoryId(), getGiftBadge());
    }

    public MediaMmsMessageRecord withoutQuote() {
        return new MediaMmsMessageRecord(getId(), getRecipient(), getIndividualRecipient(), getRecipientDeviceId(), getDateSent(), getDateReceived(), getServerTimestamp(), getDeliveryReceiptCount(), getThreadId(), getBody(), getSlideDeck(), getPartCount(), getType(), getIdentityKeyMismatches(), getNetworkFailures(), getSubscriptionId(), getExpiresIn(), getExpireStarted(), isViewOnce(), getReadReceiptCount(), null, getSharedContacts(), getLinkPreviews(), isUnidentified(), getReactions(), isRemoteDelete(), this.mentionsSelf, getNotifiedTimestamp(), getViewedReceiptCount(), getReceiptTimestamp(), getMessageRanges(), getStoryType(), getParentStoryId(), getGiftBadge());
    }

    public MediaMmsMessageRecord withAttachments(Context context, List<DatabaseAttachment> list) {
        HashMap hashMap = new HashMap();
        for (DatabaseAttachment databaseAttachment : list) {
            hashMap.put(databaseAttachment.getAttachmentId(), databaseAttachment);
        }
        List<Contact> updateContacts = updateContacts(getSharedContacts(), hashMap);
        List<LinkPreview> updateLinkPreviews = updateLinkPreviews(getLinkPreviews(), hashMap);
        Quote updateQuote = updateQuote(context, getQuote(), list);
        return new MediaMmsMessageRecord(getId(), getRecipient(), getIndividualRecipient(), getRecipientDeviceId(), getDateSent(), getDateReceived(), getServerTimestamp(), getDeliveryReceiptCount(), getThreadId(), getBody(), MmsDatabase.Reader.buildSlideDeck(context, (List) Collection$EL.stream(list).filter(new Predicate((Set) Collection$EL.stream(updateContacts).map(new Function() { // from class: org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((Contact) obj).getAvatarAttachment();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord$$ExternalSyntheticLambda1
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m((Attachment) obj);
            }
        }).collect(Collectors.toSet())) { // from class: org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord$$ExternalSyntheticLambda5
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return MediaMmsMessageRecord.lambda$withAttachments$0(this.f$0, (DatabaseAttachment) obj);
            }
        }).filter(new Predicate((Set) Collection$EL.stream(updateLinkPreviews).map(new Function() { // from class: org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((LinkPreview) obj).getThumbnail();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).filter(new MediaMmsMessageRecord$$ExternalSyntheticLambda3()).map(new Function() { // from class: org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return (Attachment) ((Optional) obj).get();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toSet())) { // from class: org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord$$ExternalSyntheticLambda6
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return MediaMmsMessageRecord.lambda$withAttachments$1(this.f$0, (DatabaseAttachment) obj);
            }
        }).collect(Collectors.toList())), getPartCount(), getType(), getIdentityKeyMismatches(), getNetworkFailures(), getSubscriptionId(), getExpiresIn(), getExpireStarted(), isViewOnce(), getReadReceiptCount(), updateQuote, updateContacts, updateLinkPreviews, isUnidentified(), getReactions(), isRemoteDelete(), this.mentionsSelf, getNotifiedTimestamp(), getViewedReceiptCount(), getReceiptTimestamp(), getMessageRanges(), getStoryType(), getParentStoryId(), getGiftBadge());
    }

    public static /* synthetic */ boolean lambda$withAttachments$0(Set set, DatabaseAttachment databaseAttachment) {
        return !set.contains(databaseAttachment);
    }

    public static /* synthetic */ boolean lambda$withAttachments$1(Set set, DatabaseAttachment databaseAttachment) {
        return !set.contains(databaseAttachment);
    }

    private static List<Contact> updateContacts(List<Contact> list, Map<AttachmentId, DatabaseAttachment> map) {
        return (List) Collection$EL.stream(list).map(new Function(map) { // from class: org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord$$ExternalSyntheticLambda9
            public final /* synthetic */ Map f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MediaMmsMessageRecord.lambda$updateContacts$2(this.f$0, (Contact) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
    }

    public static /* synthetic */ Contact lambda$updateContacts$2(Map map, Contact contact) {
        if (contact.getAvatar() == null) {
            return contact;
        }
        return new Contact(contact, new Contact.Avatar(contact.getAvatar().getAttachmentId(), (DatabaseAttachment) map.get(contact.getAvatar().getAttachmentId()), contact.getAvatar().isProfile()));
    }

    private static List<LinkPreview> updateLinkPreviews(List<LinkPreview> list, Map<AttachmentId, DatabaseAttachment> map) {
        return (List) Collection$EL.stream(list).map(new Function(map) { // from class: org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord$$ExternalSyntheticLambda8
            public final /* synthetic */ Map f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MediaMmsMessageRecord.lambda$updateLinkPreviews$3(this.f$0, (LinkPreview) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
    }

    public static /* synthetic */ LinkPreview lambda$updateLinkPreviews$3(Map map, LinkPreview linkPreview) {
        DatabaseAttachment databaseAttachment;
        return (linkPreview.getAttachmentId() == null || (databaseAttachment = (DatabaseAttachment) map.get(linkPreview.getAttachmentId())) == null) ? linkPreview : new LinkPreview(linkPreview.getUrl(), linkPreview.getTitle(), linkPreview.getDescription(), linkPreview.getDate(), databaseAttachment);
    }

    private static Quote updateQuote(Context context, Quote quote, List<DatabaseAttachment> list) {
        if (quote == null) {
            return null;
        }
        return quote.withAttachment(new SlideDeck(context, (List) Collection$EL.stream(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord$$ExternalSyntheticLambda7
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return ((DatabaseAttachment) obj).isQuote();
            }
        }).collect(Collectors.toList())));
    }
}
