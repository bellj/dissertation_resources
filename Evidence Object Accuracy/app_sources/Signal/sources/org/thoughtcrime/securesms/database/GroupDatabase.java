package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.io.Closeable;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SetUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.Member;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.EnabledState;
import org.thoughtcrime.securesms.crypto.SenderKeyUtil;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupAccessControl;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupMigrationMembershipChange;
import org.thoughtcrime.securesms.jobs.RequestGroupV2InfoJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.groupsv2.DecryptedGroupUtil;
import org.whispersystems.signalservice.api.groupsv2.GroupChangeReconstruct;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes4.dex */
public class GroupDatabase extends Database {
    static final String ACTIVE;
    @Deprecated
    private static final String AUTH_SERVICE_ID;
    private static final String AVATAR_CONTENT_TYPE;
    private static final String AVATAR_DIGEST;
    private static final String AVATAR_ID;
    private static final String AVATAR_KEY;
    private static final String AVATAR_RELAY;
    public static final String[] CREATE_INDEXS = {"CREATE UNIQUE INDEX IF NOT EXISTS group_id_index ON groups (group_id);", "CREATE UNIQUE INDEX IF NOT EXISTS group_recipient_id_index ON groups (recipient_id);", "CREATE UNIQUE INDEX IF NOT EXISTS expected_v2_id_index ON groups (expected_v2_id);", "CREATE UNIQUE INDEX IF NOT EXISTS group_distribution_id_index ON groups(distribution_id);"};
    public static final String CREATE_TABLE;
    private static final String DISPLAY_AS_STORY;
    private static final String DISTRIBUTION_ID;
    private static final String EXPECTED_V2_ID;
    static final String GROUP_ID;
    private static final String[] GROUP_PROJECTION;
    private static final String ID;
    static final String MEMBERS;
    static final String MMS;
    public static final String RECIPIENT_ID;
    static final String TABLE_NAME;
    private static final String TAG = Log.tag(GroupDatabase.class);
    private static final String TIMESTAMP;
    private static final String TITLE;
    static final List<String> TYPED_GROUP_PROJECTION;
    private static final String UNMIGRATED_V1_MEMBERS;
    public static final String V2_DECRYPTED_GROUP;
    public static final String V2_MASTER_KEY;
    private static final String V2_REVISION;

    static {
        TAG = Log.tag(GroupDatabase.class);
        CREATE_INDEXS = new String[]{"CREATE UNIQUE INDEX IF NOT EXISTS group_id_index ON groups (group_id);", "CREATE UNIQUE INDEX IF NOT EXISTS group_recipient_id_index ON groups (recipient_id);", "CREATE UNIQUE INDEX IF NOT EXISTS expected_v2_id_index ON groups (expected_v2_id);", "CREATE UNIQUE INDEX IF NOT EXISTS group_distribution_id_index ON groups(distribution_id);"};
        String[] strArr = {"group_id", "recipient_id", "title", MEMBERS, UNMIGRATED_V1_MEMBERS, AVATAR_ID, AVATAR_KEY, AVATAR_CONTENT_TYPE, AVATAR_RELAY, AVATAR_DIGEST, "timestamp", ACTIVE, "mms", V2_MASTER_KEY, V2_REVISION, V2_DECRYPTED_GROUP};
        GROUP_PROJECTION = strArr;
        TYPED_GROUP_PROJECTION = Stream.of(strArr).map(new Function() { // from class: org.thoughtcrime.securesms.database.GroupDatabase$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return GroupDatabase.lambda$static$0((String) obj);
            }
        }).toList();
    }

    public static /* synthetic */ String lambda$static$0(String str) {
        return "groups." + str;
    }

    public GroupDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public Optional<GroupRecord> getGroup(RecipientId recipientId) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", null, "recipient_id = ?", new String[]{recipientId.serialize()}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    Optional<GroupRecord> group = getGroup(query);
                    query.close();
                    return group;
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        Optional<GroupRecord> empty = Optional.empty();
        if (query != null) {
            query.close();
        }
        return empty;
    }

    public Optional<GroupRecord> getGroup(GroupId groupId) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        Cursor query = signalWritableDatabase.query("groups", null, "group_id = ?", SqlUtil.buildArgs(groupId.toString()), null, null, null);
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    Optional<GroupRecord> group = getGroup(query);
                    if (!group.isPresent() || !RemappedRecords.getInstance().areAnyRemapped(group.get().getMembers())) {
                        Optional<GroupRecord> group2 = getGroup(query);
                        query.close();
                        return group2;
                    }
                    String buildRemapDescription = RemappedRecords.getInstance().buildRemapDescription(group.get().getMembers());
                    String str = TAG;
                    Log.w(str, "Found a group with remapped recipients in it's membership list! Updating the list. GroupId: " + groupId + ", Remaps: " + buildRemapDescription, true);
                    Set<RecipientId> remap = RemappedRecords.getInstance().remap(group.get().getMembers());
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(MEMBERS, RecipientId.toSerializedList(remap));
                    if (signalWritableDatabase.update("groups", contentValues, "group_id = ?", SqlUtil.buildArgs(groupId)) > 0) {
                        Optional<GroupRecord> group3 = getGroup(groupId);
                        query.close();
                        return group3;
                    }
                    throw new IllegalStateException("Failed to update group with remapped recipients!");
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        Optional<GroupRecord> empty = Optional.empty();
        if (query != null) {
            query.close();
        }
        return empty;
    }

    public boolean groupExists(GroupId groupId) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", null, "group_id = ?", new String[]{groupId.toString()}, null, null, null);
        try {
            boolean moveToNext = query.moveToNext();
            query.close();
            return moveToNext;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public Optional<GroupRecord> getGroupV1ByExpectedV2(GroupId.V2 v2) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", GROUP_PROJECTION, "expected_v2_id = ?", SqlUtil.buildArgs(v2), null, null, null);
        try {
            if (query.moveToFirst()) {
                Optional<GroupRecord> group = getGroup(query);
                query.close();
                return group;
            }
            Optional<GroupRecord> empty = Optional.empty();
            query.close();
            return empty;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public Optional<GroupRecord> getGroupByDistributionId(DistributionId distributionId) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", null, "distribution_id = ?", SqlUtil.buildArgs(distributionId), null, null, null);
        try {
            if (query.moveToFirst()) {
                Optional<GroupRecord> group = getGroup(query);
                query.close();
                return group;
            }
            Optional<GroupRecord> empty = Optional.empty();
            query.close();
            return empty;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public void removeUnmigratedV1Members(GroupId.V2 v2, List<RecipientId> list) {
        Optional<GroupRecord> group = getGroup(v2);
        if (!group.isPresent()) {
            Log.w(TAG, "Couldn't find the group!", new Throwable());
            return;
        }
        List<RecipientId> unmigratedV1Members = group.get().getUnmigratedV1Members();
        unmigratedV1Members.removeAll(list);
        ContentValues contentValues = new ContentValues();
        contentValues.put(UNMIGRATED_V1_MEMBERS, unmigratedV1Members.isEmpty() ? null : RecipientId.toSerializedList(unmigratedV1Members));
        this.databaseHelper.getSignalWritableDatabase().update("groups", contentValues, "group_id = ?", SqlUtil.buildArgs(v2));
        Recipient.live(Recipient.externalGroupExact(v2).getId()).refresh();
    }

    Optional<GroupRecord> getGroup(Cursor cursor) {
        return Optional.ofNullable(new Reader(cursor).getCurrent());
    }

    public int getGroupV2Revision(GroupId.V2 v2) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", null, "group_id = ?", new String[]{v2.toString()}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    int i = query.getInt(query.getColumnIndexOrThrow(V2_REVISION));
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return -1;
    }

    public GroupRecord requireGroup(GroupId groupId) {
        Optional<GroupRecord> group = getGroup(groupId);
        if (group.isPresent()) {
            return group.get();
        }
        throw new AssertionError("Group not found");
    }

    public boolean isUnknownGroup(GroupId groupId) {
        Optional<GroupRecord> group = getGroup(groupId);
        if (!group.isPresent()) {
            return true;
        }
        boolean z = !group.get().hasAvatar() && TextUtils.isEmpty(group.get().getTitle());
        boolean z2 = group.get().getMembers().isEmpty() || (group.get().getMembers().size() == 1 && group.get().getMembers().contains(Recipient.self().getId()));
        if (!z || !z2) {
            return false;
        }
        return true;
    }

    public Reader queryGroupsByTitle(String str, boolean z, boolean z2, boolean z3) {
        String str2;
        String[] strArr;
        String str3;
        String buildCaseInsensitiveGlobPattern = SqlUtil.buildCaseInsensitiveGlobPattern(str);
        if (z) {
            strArr = SqlUtil.buildArgs(buildCaseInsensitiveGlobPattern, 1);
            str2 = "title GLOB ? AND (active = ? OR recipient_id IN (SELECT thread_recipient_id FROM thread))";
        } else {
            strArr = SqlUtil.buildArgs(buildCaseInsensitiveGlobPattern, 1);
            str2 = "title GLOB ? AND active = ?";
        }
        if (z2) {
            str2 = str2 + " AND expected_v2_id IS NULL";
        }
        if (z3) {
            str3 = str2 + " AND mms = 0";
        } else {
            str3 = str2;
        }
        return new Reader(this.databaseHelper.getSignalReadableDatabase().query("groups", null, str3, strArr, null, null, "title COLLATE NOCASE ASC"));
    }

    public DistributionId getOrCreateDistributionId(GroupId.V2 v2) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String[] buildArgs = SqlUtil.buildArgs(v2);
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", new String[]{"distribution_id"}, "group_id = ?", buildArgs, null, null, null);
        try {
            if (query.moveToFirst()) {
                Optional<String> string = CursorUtil.getString(query, "distribution_id");
                if (string.isPresent()) {
                    DistributionId from = DistributionId.from(string.get());
                    query.close();
                    return from;
                }
                Log.w(TAG, "Missing distributionId! Creating one.");
                DistributionId create = DistributionId.create();
                ContentValues contentValues = new ContentValues(1);
                contentValues.put("distribution_id", create.toString());
                if (signalReadableDatabase.update("groups", contentValues, "group_id = ?", buildArgs) >= 1) {
                    query.close();
                    return create;
                }
                throw new IllegalStateException("Tried to create a distributionId for " + v2 + ", but it doesn't exist!");
            }
            throw new IllegalStateException("Group " + v2 + " doesn't exist!");
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public GroupId.Mms getOrCreateMmsGroupForMembers(List<RecipientId> list) {
        Collections.sort(list);
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", new String[]{"group_id"}, "members = ? AND mms = ?", new String[]{RecipientId.toSerializedList(list), SubscriptionLevels.BOOST_LEVEL}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    GroupId.Mms requireMms = GroupId.parseOrThrow(query.getString(query.getColumnIndexOrThrow("group_id"))).requireMms();
                    query.close();
                    return requireMms;
                }
            } finally {
                if (query != null) {
                    query.close();
                }
            }
        }
        GroupId.Mms createMms = GroupId.createMms(new SecureRandom());
        create(createMms, (String) null, list);
        return createMms;
    }

    public List<String> getPushGroupNamesContainingMember(RecipientId recipientId) {
        return Stream.of(getPushGroupsContainingMember(recipientId)).map(new Function() { // from class: org.thoughtcrime.securesms.database.GroupDatabase$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return GroupDatabase.this.lambda$getPushGroupNamesContainingMember$1((GroupDatabase.GroupRecord) obj);
            }
        }).toList();
    }

    public /* synthetic */ String lambda$getPushGroupNamesContainingMember$1(GroupRecord groupRecord) {
        return Recipient.resolved(groupRecord.getRecipientId()).getDisplayName(this.context);
    }

    public List<GroupRecord> getPushGroupsContainingMember(RecipientId recipientId) {
        return getGroupsContainingMember(recipientId, true);
    }

    public List<GroupRecord> getGroupsContainingMember(RecipientId recipientId, boolean z) {
        return getGroupsContainingMember(recipientId, z, false);
    }

    public List<GroupRecord> getGroupsContainingMember(RecipientId recipientId, boolean z, boolean z2) {
        String[] strArr;
        String str;
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String[] buildArgs = SqlUtil.buildArgs("%" + recipientId.serialize() + "%");
        String str2 = "members LIKE ?";
        if (z) {
            str2 = str2 + " AND mms = ?";
            buildArgs = SqlUtil.appendArg(buildArgs, "0");
        }
        if (!z2) {
            strArr = SqlUtil.appendArg(buildArgs, SubscriptionLevels.BOOST_LEVEL);
            str = str2 + " AND active = ?";
        } else {
            strArr = buildArgs;
            str = str2;
        }
        LinkedList linkedList = new LinkedList();
        Cursor query = signalReadableDatabase.query("groups INNER JOIN thread ON groups.recipient_id = thread.thread_recipient_id", null, str, strArr, null, null, "thread.date DESC");
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                } else if (RecipientId.serializedListContains(query.getString(query.getColumnIndexOrThrow(MEMBERS)), recipientId)) {
                    linkedList.add(new Reader(query).getCurrent());
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return linkedList;
    }

    public Reader getGroups() {
        return new Reader(this.databaseHelper.getSignalReadableDatabase().query("groups", null, null, null, null, null, null));
    }

    public int getActiveGroupCount() {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", new String[]{"COUNT(*)"}, "active = 1", null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    public List<RecipientId> getGroupMemberIds(GroupId groupId, MemberSet memberSet) {
        if (groupId.isV2()) {
            return (List) getGroup(groupId).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.database.GroupDatabase$$ExternalSyntheticLambda2
                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return GroupDatabase.lambda$getGroupMemberIds$2(GroupDatabase.MemberSet.this, (GroupDatabase.GroupRecord) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).orElse(Collections.emptyList());
        }
        List<RecipientId> currentMembers = getCurrentMembers(groupId);
        if (!memberSet.includeSelf) {
            currentMembers.remove(Recipient.self().getId());
        }
        return currentMembers;
    }

    public static /* synthetic */ List lambda$getGroupMemberIds$2(MemberSet memberSet, GroupRecord groupRecord) {
        return groupRecord.requireV2GroupProperties().getMemberRecipientIds(memberSet);
    }

    public List<Recipient> getGroupMembers(GroupId groupId, MemberSet memberSet) {
        if (groupId.isV2()) {
            return (List) getGroup(groupId).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.database.GroupDatabase$$ExternalSyntheticLambda0
                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return GroupDatabase.lambda$getGroupMembers$3(GroupDatabase.MemberSet.this, (GroupDatabase.GroupRecord) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).orElse(Collections.emptyList());
        }
        List<RecipientId> currentMembers = getCurrentMembers(groupId);
        ArrayList arrayList = new ArrayList(currentMembers.size());
        for (RecipientId recipientId : currentMembers) {
            Recipient resolved = Recipient.resolved(recipientId);
            if (memberSet.includeSelf || !resolved.isSelf()) {
                arrayList.add(resolved);
            }
        }
        return arrayList;
    }

    public static /* synthetic */ List lambda$getGroupMembers$3(MemberSet memberSet, GroupRecord groupRecord) {
        return groupRecord.requireV2GroupProperties().getMemberRecipients(memberSet);
    }

    public void create(GroupId.V1 v1, String str, Collection<RecipientId> collection, SignalServiceAttachmentPointer signalServiceAttachmentPointer, String str2) {
        if (!groupExists(v1.deriveV2MigrationGroupId())) {
            create(v1, str, collection, signalServiceAttachmentPointer, str2, null, null);
            return;
        }
        throw new LegacyGroupInsertException(v1);
    }

    public void create(GroupId.Mms mms, String str, Collection<RecipientId> collection) {
        if (Util.isEmpty(str)) {
            str = null;
        }
        create(mms, str, collection, null, null, null, null);
    }

    public GroupId.V2 create(GroupMasterKey groupMasterKey, DecryptedGroup decryptedGroup) {
        return create(groupMasterKey, decryptedGroup, false);
    }

    public GroupId.V2 create(GroupMasterKey groupMasterKey, DecryptedGroup decryptedGroup, boolean z) {
        GroupId.V2 v2 = GroupId.v2(groupMasterKey);
        if (z || !getGroupV1ByExpectedV2(v2).isPresent()) {
            if (z) {
                Log.w(TAG, "Forcing the creation of a group even though we already have a V1 ID!");
            }
            create(v2, decryptedGroup.getTitle(), Collections.emptyList(), null, null, groupMasterKey, decryptedGroup);
            return v2;
        }
        throw new MissedGroupMigrationInsertException(v2);
    }

    /* JADX INFO: finally extract failed */
    public void fixMissingMasterKey(ServiceId serviceId, GroupMasterKey groupMasterKey) {
        GroupId.V2 v2 = GroupId.v2(groupMasterKey);
        if (getGroupV1ByExpectedV2(v2).isPresent()) {
            Log.w(TAG, "There already exists a V1 group that should be migrated into this group. But if the recipient already exists, there's not much we can do here.");
        }
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            String[] buildArgs = SqlUtil.buildArgs(v2);
            ContentValues contentValues = new ContentValues();
            contentValues.put(V2_MASTER_KEY, groupMasterKey.serialize());
            if (signalWritableDatabase.update("groups", contentValues, "group_id = ?", buildArgs) < 1) {
                String str = TAG;
                Log.w(str, "No group entry. Creating restore placeholder for " + v2);
                create(groupMasterKey, DecryptedGroup.newBuilder().setRevision(-2).build(), true);
            } else {
                Log.w(TAG, "Had a group entry, but it was missing a master key. Updated.");
            }
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            String str2 = TAG;
            Log.w(str2, "Scheduling request for latest group info for " + v2);
            ApplicationDependencies.getJobManager().add(new RequestGroupV2InfoJob(v2));
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    private void create(GroupId groupId, String str, Collection<RecipientId> collection, SignalServiceAttachmentPointer signalServiceAttachmentPointer, String str2, GroupMasterKey groupMasterKey, DecryptedGroup decryptedGroup) {
        RecipientDatabase recipients = SignalDatabase.recipients();
        RecipientId orInsertFromGroupId = recipients.getOrInsertFromGroupId(groupId);
        List<? extends RecipientId> arrayList = new ArrayList<>(new HashSet(collection));
        Collections.sort(arrayList);
        ContentValues contentValues = new ContentValues();
        contentValues.put("recipient_id", orInsertFromGroupId.serialize());
        contentValues.put("group_id", groupId.toString());
        contentValues.put("title", str);
        contentValues.put(MEMBERS, RecipientId.toSerializedList(arrayList));
        int i = 0;
        if (signalServiceAttachmentPointer != null) {
            contentValues.put(AVATAR_ID, signalServiceAttachmentPointer.getRemoteId().getV2().get());
            contentValues.put(AVATAR_KEY, signalServiceAttachmentPointer.getKey());
            contentValues.put(AVATAR_CONTENT_TYPE, signalServiceAttachmentPointer.getContentType());
            contentValues.put(AVATAR_DIGEST, signalServiceAttachmentPointer.getDigest().orElse(null));
        } else {
            contentValues.put(AVATAR_ID, (Integer) 0);
        }
        contentValues.put(AVATAR_RELAY, str2);
        contentValues.put("timestamp", Long.valueOf(System.currentTimeMillis()));
        if (groupId.isV2()) {
            if (decryptedGroup != null && gv2GroupActive(decryptedGroup)) {
                i = 1;
            }
            contentValues.put(ACTIVE, Integer.valueOf(i));
            contentValues.put("distribution_id", DistributionId.create().toString());
        } else if (groupId.isV1()) {
            contentValues.put(ACTIVE, (Integer) 1);
            contentValues.put(EXPECTED_V2_ID, groupId.requireV1().deriveV2MigrationGroupId().toString());
        } else {
            contentValues.put(ACTIVE, (Integer) 1);
        }
        contentValues.put("mms", Boolean.valueOf(groupId.isMms()));
        if (groupMasterKey != null) {
            if (decryptedGroup != null) {
                groupId.requireV2();
                arrayList = getV2GroupMembers(decryptedGroup, true);
                contentValues.put(V2_MASTER_KEY, groupMasterKey.serialize());
                contentValues.put(V2_REVISION, Integer.valueOf(decryptedGroup.getRevision()));
                contentValues.put(V2_DECRYPTED_GROUP, decryptedGroup.toByteArray());
                contentValues.put(MEMBERS, RecipientId.toSerializedList(arrayList));
            } else {
                throw new AssertionError("V2 master key but no group state");
            }
        } else if (groupId.isV2()) {
            throw new AssertionError("V2 group id but no master key");
        }
        this.databaseHelper.getSignalWritableDatabase().insert("groups", (String) null, contentValues);
        if (decryptedGroup != null && decryptedGroup.hasDisappearingMessagesTimer()) {
            recipients.setExpireMessages(orInsertFromGroupId, decryptedGroup.getDisappearingMessagesTimer().getDuration());
        }
        if (arrayList != null && (groupId.isMms() || Recipient.resolved(orInsertFromGroupId).isProfileSharing())) {
            recipients.setHasGroupsInCommon(arrayList);
        }
        Recipient.live(orInsertFromGroupId).refresh();
        notifyConversationListListeners();
    }

    public void update(GroupId.V1 v1, String str, SignalServiceAttachmentPointer signalServiceAttachmentPointer) {
        ContentValues contentValues = new ContentValues();
        if (str != null) {
            contentValues.put("title", str);
        }
        if (signalServiceAttachmentPointer != null) {
            contentValues.put(AVATAR_ID, signalServiceAttachmentPointer.getRemoteId().getV2().get());
            contentValues.put(AVATAR_CONTENT_TYPE, signalServiceAttachmentPointer.getContentType());
            contentValues.put(AVATAR_KEY, signalServiceAttachmentPointer.getKey());
            contentValues.put(AVATAR_DIGEST, signalServiceAttachmentPointer.getDigest().orElse(null));
        } else {
            contentValues.put(AVATAR_ID, (Integer) 0);
        }
        this.databaseHelper.getSignalWritableDatabase().update("groups", contentValues, "group_id = ?", new String[]{v1.toString()});
        Recipient.live(SignalDatabase.recipients().getOrInsertFromGroupId(v1)).refresh();
        notifyConversationListListeners();
    }

    public GroupId.V2 migrateToV2(long j, GroupId.V1 v1, DecryptedGroup decryptedGroup) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        GroupId.V2 deriveV2MigrationGroupId = v1.deriveV2MigrationGroupId();
        GroupMasterKey deriveV2MigrationMasterKey = v1.deriveV2MigrationMasterKey();
        signalWritableDatabase.beginTransaction();
        try {
            GroupRecord groupRecord = getGroup(v1).get();
            ContentValues contentValues = new ContentValues();
            contentValues.put("group_id", deriveV2MigrationGroupId.toString());
            contentValues.put(V2_MASTER_KEY, deriveV2MigrationMasterKey.serialize());
            contentValues.put("distribution_id", DistributionId.create().toString());
            contentValues.putNull(EXPECTED_V2_ID);
            List<RecipientId> uuidsToRecipientIds = uuidsToRecipientIds(DecryptedGroupUtil.membersToUuidList(decryptedGroup.getMembersList()));
            List<RecipientId> uuidsToRecipientIds2 = uuidsToRecipientIds(DecryptedGroupUtil.pendingToUuidList(decryptedGroup.getPendingMembersList()));
            uuidsToRecipientIds.addAll(uuidsToRecipientIds2);
            ArrayList arrayList = new ArrayList(SetUtil.difference(groupRecord.getMembers(), uuidsToRecipientIds));
            List concatenatedList = Util.concatenatedList(uuidsToRecipientIds2, arrayList);
            contentValues.put(UNMIGRATED_V1_MEMBERS, concatenatedList.isEmpty() ? null : RecipientId.toSerializedList(concatenatedList));
            if (signalWritableDatabase.update("groups", contentValues, "group_id = ?", SqlUtil.buildArgs(v1.toString())) == 1) {
                SignalDatabase.recipients().updateGroupId(v1, deriveV2MigrationGroupId);
                update(deriveV2MigrationMasterKey, decryptedGroup);
                SignalDatabase.sms().insertGroupV1MigrationEvents(groupRecord.getRecipientId(), j, new GroupMigrationMembershipChange(uuidsToRecipientIds2, arrayList));
                signalWritableDatabase.setTransactionSuccessful();
                return deriveV2MigrationGroupId;
            }
            throw new AssertionError();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public void update(GroupMasterKey groupMasterKey, DecryptedGroup decryptedGroup) {
        update(GroupId.v2(groupMasterKey), decryptedGroup);
    }

    public void update(GroupId.V2 v2, DecryptedGroup decryptedGroup) {
        RecipientDatabase recipients = SignalDatabase.recipients();
        RecipientId orInsertFromGroupId = recipients.getOrInsertFromGroupId(v2);
        Optional<GroupRecord> group = getGroup(v2);
        String title = decryptedGroup.getTitle();
        ContentValues contentValues = new ContentValues();
        if (group.isPresent() && group.get().getUnmigratedV1Members().size() > 0 && group.get().isV2Group()) {
            HashSet hashSet = new HashSet(group.get().getUnmigratedV1Members());
            DecryptedGroupChange reconstructGroupChange = GroupChangeReconstruct.reconstructGroupChange(group.get().requireV2GroupProperties().getDecryptedGroup(), decryptedGroup);
            List<RecipientId> uuidsToRecipientIds = uuidsToRecipientIds(DecryptedGroupUtil.membersToUuidList(reconstructGroupChange.getNewMembersList()));
            List<RecipientId> uuidsToRecipientIds2 = uuidsToRecipientIds(DecryptedGroupUtil.removedMembersUuidList(reconstructGroupChange));
            List<RecipientId> uuidsToRecipientIds3 = uuidsToRecipientIds(DecryptedGroupUtil.pendingToUuidList(reconstructGroupChange.getNewPendingMembersList()));
            List<RecipientId> uuidsToRecipientIds4 = uuidsToRecipientIds(DecryptedGroupUtil.removedPendingMembersUuidList(reconstructGroupChange));
            List<RecipientId> uuidsToRecipientIds5 = uuidsToRecipientIds(DecryptedGroupUtil.membersToUuidList(reconstructGroupChange.getPromotePendingMembersList()));
            hashSet.removeAll(uuidsToRecipientIds);
            hashSet.removeAll(uuidsToRecipientIds2);
            hashSet.removeAll(uuidsToRecipientIds3);
            hashSet.removeAll(uuidsToRecipientIds4);
            hashSet.removeAll(uuidsToRecipientIds5);
            contentValues.put(UNMIGRATED_V1_MEMBERS, hashSet.isEmpty() ? null : RecipientId.toSerializedList(hashSet));
        }
        List<RecipientId> v2GroupMembers = getV2GroupMembers(decryptedGroup, true);
        contentValues.put("title", title);
        contentValues.put(V2_REVISION, Integer.valueOf(decryptedGroup.getRevision()));
        contentValues.put(V2_DECRYPTED_GROUP, decryptedGroup.toByteArray());
        contentValues.put(MEMBERS, RecipientId.toSerializedList(v2GroupMembers));
        contentValues.put(ACTIVE, Integer.valueOf(gv2GroupActive(decryptedGroup) ? 1 : 0));
        DistributionId distributionId = group.get().getDistributionId();
        Objects.requireNonNull(distributionId);
        if (group.isPresent() && group.get().isV2Group()) {
            ArrayList<UUID> removedMembersUuidList = DecryptedGroupUtil.removedMembersUuidList(GroupChangeReconstruct.reconstructGroupChange(group.get().requireV2GroupProperties().getDecryptedGroup(), decryptedGroup));
            if (removedMembersUuidList.size() > 0) {
                String str = TAG;
                Log.i(str, removedMembersUuidList.size() + " members were removed from group " + v2 + ". Rotating the DistributionId " + distributionId);
                SenderKeyUtil.rotateOurKey(distributionId);
            }
        }
        this.databaseHelper.getSignalWritableDatabase().update("groups", contentValues, "group_id = ?", new String[]{v2.toString()});
        if (decryptedGroup.hasDisappearingMessagesTimer()) {
            recipients.setExpireMessages(orInsertFromGroupId, decryptedGroup.getDisappearingMessagesTimer().getDuration());
        }
        if (v2.isMms() || Recipient.resolved(orInsertFromGroupId).isProfileSharing()) {
            recipients.setHasGroupsInCommon(v2GroupMembers);
        }
        Recipient.live(orInsertFromGroupId).refresh();
        notifyConversationListListeners();
    }

    public void updateTitle(GroupId.V1 v1, String str) {
        updateTitle((GroupId) v1, str);
    }

    public void updateTitle(GroupId.Mms mms, String str) {
        if (Util.isEmpty(str)) {
            str = null;
        }
        updateTitle((GroupId) mms, str);
    }

    private void updateTitle(GroupId groupId, String str) {
        if (groupId.isV1() || groupId.isMms()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("title", str);
            this.databaseHelper.getSignalWritableDatabase().update("groups", contentValues, "group_id = ?", new String[]{groupId.toString()});
            Recipient.live(SignalDatabase.recipients().getOrInsertFromGroupId(groupId)).refresh();
            return;
        }
        throw new AssertionError();
    }

    public void onAvatarUpdated(GroupId groupId, boolean z) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(AVATAR_ID, Long.valueOf(z ? Math.abs(new SecureRandom().nextLong()) : 0));
        this.databaseHelper.getSignalWritableDatabase().update("groups", contentValues, "group_id = ?", new String[]{groupId.toString()});
        Recipient.live(SignalDatabase.recipients().getOrInsertFromGroupId(groupId)).refresh();
    }

    public void updateMembers(GroupId groupId, List<RecipientId> list) {
        Collections.sort(list);
        ContentValues contentValues = new ContentValues();
        contentValues.put(MEMBERS, RecipientId.toSerializedList(list));
        contentValues.put(ACTIVE, (Integer) 1);
        this.databaseHelper.getSignalWritableDatabase().update("groups", contentValues, "group_id = ?", new String[]{groupId.toString()});
        Recipient.live(SignalDatabase.recipients().getOrInsertFromGroupId(groupId)).refresh();
    }

    public void remove(GroupId groupId, RecipientId recipientId) {
        List<RecipientId> currentMembers = getCurrentMembers(groupId);
        currentMembers.remove(recipientId);
        ContentValues contentValues = new ContentValues();
        contentValues.put(MEMBERS, RecipientId.toSerializedList(currentMembers));
        this.databaseHelper.getSignalWritableDatabase().update("groups", contentValues, "group_id = ?", new String[]{groupId.toString()});
        Recipient.live(SignalDatabase.recipients().getOrInsertFromGroupId(groupId)).refresh();
    }

    private static boolean gv2GroupActive(DecryptedGroup decryptedGroup) {
        ACI requireAci = SignalStore.account().requireAci();
        return DecryptedGroupUtil.findMemberByUuid(decryptedGroup.getMembersList(), requireAci.uuid()).isPresent() || DecryptedGroupUtil.findPendingByUuid(decryptedGroup.getPendingMembersList(), requireAci.uuid()).isPresent();
    }

    private List<RecipientId> getCurrentMembers(GroupId groupId) {
        Cursor cursor = null;
        try {
            cursor = this.databaseHelper.getSignalReadableDatabase().query("groups", new String[]{MEMBERS}, "group_id = ?", new String[]{groupId.toString()}, null, null, null);
            if (cursor == null || !cursor.moveToFirst()) {
                return new LinkedList();
            }
            List<RecipientId> fromSerializedList = RecipientId.fromSerializedList(cursor.getString(cursor.getColumnIndexOrThrow(MEMBERS)));
            cursor.close();
            return fromSerializedList;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public boolean isActive(GroupId groupId) {
        Optional<GroupRecord> group = getGroup(groupId);
        return group.isPresent() && group.get().isActive();
    }

    public void setActive(GroupId groupId, boolean z) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ACTIVE, Integer.valueOf(z ? 1 : 0));
        signalWritableDatabase.update("groups", contentValues, "group_id = ?", new String[]{groupId.toString()});
    }

    public boolean isCurrentMember(GroupId.Push push, RecipientId recipientId) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", new String[]{MEMBERS}, "group_id = ?", new String[]{push.toString()}, null, null, null);
        try {
            if (query.moveToNext()) {
                boolean serializedListContains = RecipientId.serializedListContains(query.getString(query.getColumnIndexOrThrow(MEMBERS)), recipientId);
                query.close();
                return serializedListContains;
            }
            query.close();
            return false;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private static List<RecipientId> uuidsToRecipientIds(List<UUID> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (UUID uuid : list) {
            if (UuidUtil.UNKNOWN_UUID.equals(uuid)) {
                Log.w(TAG, "Seen unknown UUID in members list");
            } else {
                RecipientId from = RecipientId.from(ServiceId.from(uuid));
                Optional<RecipientId> recipient = RemappedRecords.getInstance().getRecipient(from);
                if (recipient.isPresent()) {
                    String str = TAG;
                    Log.w(str, "Saw that " + from + " remapped to " + recipient + ". Using the mapping.");
                    arrayList.add(recipient.get());
                } else {
                    arrayList.add(from);
                }
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    private static List<RecipientId> getV2GroupMembers(DecryptedGroup decryptedGroup, boolean z) {
        List<RecipientId> uuidsToRecipientIds = uuidsToRecipientIds(DecryptedGroupUtil.membersToUuidList(decryptedGroup.getMembersList()));
        if (!RemappedRecords.getInstance().areAnyRemapped(uuidsToRecipientIds)) {
            return uuidsToRecipientIds;
        }
        if (z) {
            Log.w(TAG, "Found remapped records where we shouldn't. Clearing cache and trying again.");
            RecipientId.clearCache();
            RemappedRecords.getInstance().resetCache();
            return getV2GroupMembers(decryptedGroup, false);
        }
        throw new IllegalStateException("Remapped records in group membership!");
    }

    public List<GroupId.V2> getAllGroupV2Ids() {
        LinkedList linkedList = new LinkedList();
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", new String[]{"group_id"}, null, null, null, null, null);
        while (query.moveToNext()) {
            try {
                GroupId parseOrThrow = GroupId.parseOrThrow(query.getString(query.getColumnIndexOrThrow("group_id")));
                if (parseOrThrow.isV2()) {
                    linkedList.add(parseOrThrow.requireV2());
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return linkedList;
    }

    public Map<GroupId.V2, GroupId.V1> getAllExpectedV2Ids() {
        HashMap hashMap = new HashMap();
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("groups", new String[]{"group_id", EXPECTED_V2_ID}, "expected_v2_id NOT NULL", null, null, null, null);
        while (query.moveToNext()) {
            try {
                hashMap.put(GroupId.parseOrThrow(query.getString(query.getColumnIndexOrThrow(EXPECTED_V2_ID))).requireV2(), GroupId.parseOrThrow(query.getString(query.getColumnIndexOrThrow("group_id"))).requireV1());
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return hashMap;
    }

    /* loaded from: classes4.dex */
    public static class Reader implements Closeable {
        public final Cursor cursor;

        public Reader(Cursor cursor) {
            this.cursor = cursor;
        }

        public GroupRecord getNext() {
            Cursor cursor = this.cursor;
            if (cursor == null || !cursor.moveToNext()) {
                return null;
            }
            return getCurrent();
        }

        public int getCount() {
            Cursor cursor = this.cursor;
            if (cursor == null) {
                return 0;
            }
            return cursor.getCount();
        }

        public GroupRecord getCurrent() {
            Cursor cursor = this.cursor;
            if (!(cursor == null || cursor.getString(cursor.getColumnIndexOrThrow("group_id")) == null)) {
                Cursor cursor2 = this.cursor;
                if (cursor2.getLong(cursor2.getColumnIndexOrThrow("recipient_id")) != 0) {
                    return new GroupRecord(GroupId.parseOrThrow(CursorUtil.requireString(this.cursor, "group_id")), RecipientId.from(CursorUtil.requireString(this.cursor, "recipient_id")), CursorUtil.requireString(this.cursor, "title"), CursorUtil.requireString(this.cursor, GroupDatabase.MEMBERS), CursorUtil.requireString(this.cursor, GroupDatabase.UNMIGRATED_V1_MEMBERS), CursorUtil.requireLong(this.cursor, GroupDatabase.AVATAR_ID), CursorUtil.requireBlob(this.cursor, GroupDatabase.AVATAR_KEY), CursorUtil.requireString(this.cursor, GroupDatabase.AVATAR_CONTENT_TYPE), CursorUtil.requireString(this.cursor, GroupDatabase.AVATAR_RELAY), CursorUtil.requireBoolean(this.cursor, GroupDatabase.ACTIVE), CursorUtil.requireBlob(this.cursor, GroupDatabase.AVATAR_DIGEST), CursorUtil.requireBoolean(this.cursor, "mms"), CursorUtil.requireBlob(this.cursor, GroupDatabase.V2_MASTER_KEY), CursorUtil.requireInt(this.cursor, GroupDatabase.V2_REVISION), CursorUtil.requireBlob(this.cursor, GroupDatabase.V2_DECRYPTED_GROUP), (DistributionId) CursorUtil.getString(this.cursor, "distribution_id").map(new GroupDatabase$Reader$$ExternalSyntheticLambda0()).orElse(null));
                }
            }
            return null;
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            Cursor cursor = this.cursor;
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class GroupRecord {
        private final boolean active;
        private final String avatarContentType;
        private final byte[] avatarDigest;
        private final long avatarId;
        private final byte[] avatarKey;
        private final DistributionId distributionId;
        private final GroupId id;
        private final List<RecipientId> members;
        private final boolean mms;
        private final RecipientId recipientId;
        private final String relay;
        private final String title;
        private final List<RecipientId> unmigratedV1Members;
        private final V2GroupProperties v2GroupProperties;

        public GroupRecord(GroupId groupId, RecipientId recipientId, String str, String str2, String str3, long j, byte[] bArr, String str4, String str5, boolean z, byte[] bArr2, boolean z2, byte[] bArr3, int i, byte[] bArr4, DistributionId distributionId) {
            this.id = groupId;
            this.recipientId = recipientId;
            this.title = str;
            this.avatarId = j;
            this.avatarKey = bArr;
            this.avatarDigest = bArr2;
            this.avatarContentType = str4;
            this.relay = str5;
            this.active = z;
            this.mms = z2;
            this.distributionId = distributionId;
            V2GroupProperties v2GroupProperties = null;
            if (!(bArr3 == null || bArr4 == null)) {
                try {
                    v2GroupProperties = new V2GroupProperties(new GroupMasterKey(bArr3), i, bArr4);
                } catch (InvalidInputException e) {
                    throw new AssertionError(e);
                }
            }
            this.v2GroupProperties = v2GroupProperties;
            if (!TextUtils.isEmpty(str2)) {
                this.members = RecipientId.fromSerializedList(str2);
            } else {
                this.members = Collections.emptyList();
            }
            if (!TextUtils.isEmpty(str3)) {
                this.unmigratedV1Members = RecipientId.fromSerializedList(str3);
            } else {
                this.unmigratedV1Members = Collections.emptyList();
            }
        }

        public GroupId getId() {
            return this.id;
        }

        public RecipientId getRecipientId() {
            return this.recipientId;
        }

        public String getTitle() {
            return this.title;
        }

        public String getDescription() {
            V2GroupProperties v2GroupProperties = this.v2GroupProperties;
            return v2GroupProperties != null ? v2GroupProperties.getDecryptedGroup().getDescription() : "";
        }

        public boolean isAnnouncementGroup() {
            V2GroupProperties v2GroupProperties = this.v2GroupProperties;
            if (v2GroupProperties == null || v2GroupProperties.getDecryptedGroup().getIsAnnouncementGroup() != EnabledState.ENABLED) {
                return false;
            }
            return true;
        }

        public List<RecipientId> getMembers() {
            return this.members;
        }

        public List<Recipient> getAdmins() {
            V2GroupProperties v2GroupProperties = this.v2GroupProperties;
            if (v2GroupProperties != null) {
                return v2GroupProperties.getAdmins((List) Collection$EL.stream(this.members).map(new GroupDatabase$GroupRecord$$ExternalSyntheticLambda0()).collect(Collectors.toList()));
            }
            return Collections.emptyList();
        }

        public List<RecipientId> getUnmigratedV1Members() {
            return this.unmigratedV1Members;
        }

        public boolean hasAvatar() {
            return this.avatarId != 0;
        }

        public long getAvatarId() {
            return this.avatarId;
        }

        public byte[] getAvatarKey() {
            return this.avatarKey;
        }

        public byte[] getAvatarDigest() {
            return this.avatarDigest;
        }

        public String getAvatarContentType() {
            return this.avatarContentType;
        }

        public String getRelay() {
            return this.relay;
        }

        public boolean isActive() {
            return this.active;
        }

        public boolean isMms() {
            return this.mms;
        }

        public DistributionId getDistributionId() {
            return this.distributionId;
        }

        public boolean isV1Group() {
            return !this.mms && !isV2Group();
        }

        public boolean isV2Group() {
            return this.v2GroupProperties != null;
        }

        public V2GroupProperties requireV2GroupProperties() {
            V2GroupProperties v2GroupProperties = this.v2GroupProperties;
            if (v2GroupProperties != null) {
                return v2GroupProperties;
            }
            throw new AssertionError();
        }

        public boolean isAdmin(Recipient recipient) {
            return isV2Group() && requireV2GroupProperties().isAdmin(recipient);
        }

        public MemberLevel memberLevel(Recipient recipient) {
            if (isV2Group()) {
                MemberLevel memberLevel = requireV2GroupProperties().memberLevel(recipient.getServiceId());
                return (!recipient.isSelf() || memberLevel != MemberLevel.NOT_A_MEMBER) ? memberLevel : requireV2GroupProperties().memberLevel(Optional.ofNullable(SignalStore.account().getPni()));
            } else if (isMms() && recipient.isSelf()) {
                return MemberLevel.FULL_MEMBER;
            } else {
                if (this.members.contains(recipient.getId())) {
                    return MemberLevel.FULL_MEMBER;
                }
                return MemberLevel.NOT_A_MEMBER;
            }
        }

        public GroupAccessControl getMembershipAdditionAccessControl() {
            if (isV2Group()) {
                if (requireV2GroupProperties().getDecryptedGroup().getAccessControl().getMembers() == AccessControl.AccessRequired.MEMBER) {
                    return GroupAccessControl.ALL_MEMBERS;
                }
                return GroupAccessControl.ONLY_ADMINS;
            } else if (isV1Group()) {
                return GroupAccessControl.NO_ONE;
            } else {
                return this.id.isV1() ? GroupAccessControl.ALL_MEMBERS : GroupAccessControl.ONLY_ADMINS;
            }
        }

        public GroupAccessControl getAttributesAccessControl() {
            if (isV2Group()) {
                if (requireV2GroupProperties().getDecryptedGroup().getAccessControl().getAttributes() == AccessControl.AccessRequired.MEMBER) {
                    return GroupAccessControl.ALL_MEMBERS;
                }
                return GroupAccessControl.ONLY_ADMINS;
            } else if (isV1Group()) {
                return GroupAccessControl.NO_ONE;
            } else {
                return GroupAccessControl.ALL_MEMBERS;
            }
        }

        public boolean isPendingMember(Recipient recipient) {
            if (!isV2Group()) {
                return false;
            }
            Optional<ServiceId> serviceId = recipient.getServiceId();
            if (serviceId.isPresent()) {
                return DecryptedGroupUtil.findPendingByUuid(requireV2GroupProperties().getDecryptedGroup().getPendingMembersList(), serviceId.get().uuid()).isPresent();
            }
            return false;
        }
    }

    /* loaded from: classes4.dex */
    public static class V2GroupProperties {
        private DecryptedGroup decryptedGroup;
        private final byte[] decryptedGroupBytes;
        private final GroupMasterKey groupMasterKey;
        private final int groupRevision;

        private V2GroupProperties(GroupMasterKey groupMasterKey, int i, byte[] bArr) {
            this.groupMasterKey = groupMasterKey;
            this.groupRevision = i;
            this.decryptedGroupBytes = bArr;
        }

        public GroupMasterKey getGroupMasterKey() {
            return this.groupMasterKey;
        }

        public int getGroupRevision() {
            return this.groupRevision;
        }

        public DecryptedGroup getDecryptedGroup() {
            try {
                if (this.decryptedGroup == null) {
                    this.decryptedGroup = DecryptedGroup.parseFrom(this.decryptedGroupBytes);
                }
                return this.decryptedGroup;
            } catch (InvalidProtocolBufferException e) {
                throw new AssertionError(e);
            }
        }

        public boolean isAdmin(Recipient recipient) {
            Optional<ServiceId> serviceId = recipient.getServiceId();
            if (!serviceId.isPresent()) {
                return false;
            }
            return ((Boolean) DecryptedGroupUtil.findMemberByUuid(getDecryptedGroup().getMembersList(), serviceId.get().uuid()).map(new GroupDatabase$V2GroupProperties$$ExternalSyntheticLambda0()).orElse(Boolean.FALSE)).booleanValue();
        }

        public static /* synthetic */ Boolean lambda$isAdmin$0(DecryptedMember decryptedMember) {
            return Boolean.valueOf(decryptedMember.getRole() == Member.Role.ADMINISTRATOR);
        }

        public List<Recipient> getAdmins(List<Recipient> list) {
            return (List) Collection$EL.stream(list).filter(new GroupDatabase$V2GroupProperties$$ExternalSyntheticLambda1(this)).collect(Collectors.toList());
        }

        public MemberLevel memberLevel(Optional<ServiceId> optional) {
            if (!optional.isPresent()) {
                return MemberLevel.NOT_A_MEMBER;
            }
            DecryptedGroup decryptedGroup = getDecryptedGroup();
            return (MemberLevel) DecryptedGroupUtil.findMemberByUuid(decryptedGroup.getMembersList(), optional.get().uuid()).map(new GroupDatabase$V2GroupProperties$$ExternalSyntheticLambda2()).orElse((MemberLevel) DecryptedGroupUtil.findPendingByUuid(decryptedGroup.getPendingMembersList(), optional.get().uuid()).map(new GroupDatabase$V2GroupProperties$$ExternalSyntheticLambda3()).orElse((MemberLevel) DecryptedGroupUtil.findRequestingByUuid(decryptedGroup.getRequestingMembersList(), optional.get().uuid()).map(new GroupDatabase$V2GroupProperties$$ExternalSyntheticLambda4()).orElse(MemberLevel.NOT_A_MEMBER)));
        }

        public static /* synthetic */ MemberLevel lambda$memberLevel$1(DecryptedMember decryptedMember) {
            if (decryptedMember.getRole() == Member.Role.ADMINISTRATOR) {
                return MemberLevel.ADMINISTRATOR;
            }
            return MemberLevel.FULL_MEMBER;
        }

        public List<Recipient> getMemberRecipients(MemberSet memberSet) {
            return Recipient.resolvedList(getMemberRecipientIds(memberSet));
        }

        public List<RecipientId> getMemberRecipientIds(MemberSet memberSet) {
            int i;
            boolean z = memberSet.includeSelf;
            DecryptedGroup decryptedGroup = getDecryptedGroup();
            UUID uuid = SignalStore.account().requireAci().uuid();
            ArrayList arrayList = new ArrayList(decryptedGroup.getMembersCount() + decryptedGroup.getPendingMembersCount());
            Iterator<UUID> it = DecryptedGroupUtil.toUuidList(decryptedGroup.getMembersList()).iterator();
            int i2 = 0;
            while (it.hasNext()) {
                UUID next = it.next();
                if (UuidUtil.UNKNOWN_UUID.equals(next)) {
                    i2++;
                } else if (z || !uuid.equals(next)) {
                    arrayList.add(RecipientId.from(ServiceId.from(next)));
                }
            }
            if (memberSet.includePending) {
                Iterator<UUID> it2 = DecryptedGroupUtil.pendingToUuidList(decryptedGroup.getPendingMembersList()).iterator();
                i = 0;
                while (it2.hasNext()) {
                    UUID next2 = it2.next();
                    if (UuidUtil.UNKNOWN_UUID.equals(next2)) {
                        i++;
                    } else if (z || !uuid.equals(next2)) {
                        arrayList.add(RecipientId.from(ServiceId.from(next2)));
                    }
                }
            } else {
                i = 0;
            }
            if (i2 + i > 0) {
                Log.w(GroupDatabase.TAG, String.format(Locale.US, "Group contains %d + %d unknown pending and full members", Integer.valueOf(i), Integer.valueOf(i2)));
            }
            return arrayList;
        }

        public Set<UUID> getBannedMembers() {
            return DecryptedGroupUtil.bannedMembersToUuidSet(getDecryptedGroup().getBannedMembersList());
        }
    }

    public List<GroupId> getGroupsToDisplayAsStories() throws BadGroupIdException {
        Cursor query = getReadableDatabase().query("groups", SqlUtil.buildArgs("group_id"), "display_as_story = ? AND active = ?", SqlUtil.buildArgs(1, 1), null, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() != 0) {
                    ArrayList arrayList = new ArrayList(query.getCount());
                    while (query.moveToNext()) {
                        arrayList.add(GroupId.parse(CursorUtil.requireString(query, "group_id")));
                    }
                    query.close();
                    return arrayList;
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        List<GroupId> emptyList = Collections.emptyList();
        if (query != null) {
            query.close();
        }
        return emptyList;
    }

    public void markDisplayAsStory(GroupId groupId) {
        markDisplayAsStory(groupId, true);
    }

    public void markDisplayAsStory(GroupId groupId, boolean z) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(DISPLAY_AS_STORY, Boolean.valueOf(z));
        getWritableDatabase().update("groups", contentValues, "group_id = ?", SqlUtil.buildArgs(groupId.toString()));
    }

    /* loaded from: classes4.dex */
    public enum MemberSet {
        FULL_MEMBERS_INCLUDING_SELF(true, false),
        FULL_MEMBERS_EXCLUDING_SELF(false, false),
        FULL_MEMBERS_AND_PENDING_INCLUDING_SELF(true, true),
        FULL_MEMBERS_AND_PENDING_EXCLUDING_SELF(false, true);
        
        private final boolean includePending;
        private final boolean includeSelf;

        MemberSet(boolean z, boolean z2) {
            this.includeSelf = z;
            this.includePending = z2;
        }
    }

    /* loaded from: classes4.dex */
    public enum MemberLevel {
        NOT_A_MEMBER(false),
        PENDING_MEMBER(false),
        REQUESTING_MEMBER(false),
        FULL_MEMBER(true),
        ADMINISTRATOR(true);
        
        private final boolean inGroup;

        MemberLevel(boolean z) {
            this.inGroup = z;
        }

        public boolean isInGroup() {
            return this.inGroup;
        }
    }

    /* loaded from: classes4.dex */
    public static class LegacyGroupInsertException extends IllegalStateException {
        public LegacyGroupInsertException(GroupId groupId) {
            super("Tried to create a new GV1 entry when we already had a migrated GV2! " + groupId);
        }
    }

    /* loaded from: classes4.dex */
    public static class MissedGroupMigrationInsertException extends IllegalStateException {
        public MissedGroupMigrationInsertException(GroupId groupId) {
            super("Tried to create a new GV2 entry when we already had a V1 group that mapped to the new ID! " + groupId);
        }
    }
}
