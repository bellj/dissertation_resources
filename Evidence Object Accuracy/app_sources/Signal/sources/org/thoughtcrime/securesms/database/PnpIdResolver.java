package org.thoughtcrime.securesms.database;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;

/* compiled from: PnpOperations.kt */
@Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpIdResolver;", "", "()V", "PnpInsert", "PnpNoopId", "Lorg/thoughtcrime/securesms/database/PnpIdResolver$PnpNoopId;", "Lorg/thoughtcrime/securesms/database/PnpIdResolver$PnpInsert;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class PnpIdResolver {
    public /* synthetic */ PnpIdResolver(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    private PnpIdResolver() {
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpIdResolver$PnpNoopId;", "Lorg/thoughtcrime/securesms/database/PnpIdResolver;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class PnpNoopId extends PnpIdResolver {
        private final RecipientId recipientId;

        public static /* synthetic */ PnpNoopId copy$default(PnpNoopId pnpNoopId, RecipientId recipientId, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = pnpNoopId.recipientId;
            }
            return pnpNoopId.copy(recipientId);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final PnpNoopId copy(RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return new PnpNoopId(recipientId);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof PnpNoopId) && Intrinsics.areEqual(this.recipientId, ((PnpNoopId) obj).recipientId);
        }

        public int hashCode() {
            return this.recipientId.hashCode();
        }

        public String toString() {
            return "PnpNoopId(recipientId=" + this.recipientId + ')';
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public PnpNoopId(RecipientId recipientId) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
        }
    }

    /* compiled from: PnpOperations.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B#\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0007HÆ\u0003J-\u0010\u0012\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/database/PnpIdResolver$PnpInsert;", "Lorg/thoughtcrime/securesms/database/PnpIdResolver;", CdsDatabase.E164, "", RecipientDatabase.PNI_COLUMN, "Lorg/whispersystems/signalservice/api/push/PNI;", "aci", "Lorg/whispersystems/signalservice/api/push/ACI;", "(Ljava/lang/String;Lorg/whispersystems/signalservice/api/push/PNI;Lorg/whispersystems/signalservice/api/push/ACI;)V", "getAci", "()Lorg/whispersystems/signalservice/api/push/ACI;", "getE164", "()Ljava/lang/String;", "getPni", "()Lorg/whispersystems/signalservice/api/push/PNI;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class PnpInsert extends PnpIdResolver {
        private final ACI aci;
        private final String e164;
        private final PNI pni;

        public static /* synthetic */ PnpInsert copy$default(PnpInsert pnpInsert, String str, PNI pni, ACI aci, int i, Object obj) {
            if ((i & 1) != 0) {
                str = pnpInsert.e164;
            }
            if ((i & 2) != 0) {
                pni = pnpInsert.pni;
            }
            if ((i & 4) != 0) {
                aci = pnpInsert.aci;
            }
            return pnpInsert.copy(str, pni, aci);
        }

        public final String component1() {
            return this.e164;
        }

        public final PNI component2() {
            return this.pni;
        }

        public final ACI component3() {
            return this.aci;
        }

        public final PnpInsert copy(String str, PNI pni, ACI aci) {
            return new PnpInsert(str, pni, aci);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PnpInsert)) {
                return false;
            }
            PnpInsert pnpInsert = (PnpInsert) obj;
            return Intrinsics.areEqual(this.e164, pnpInsert.e164) && Intrinsics.areEqual(this.pni, pnpInsert.pni) && Intrinsics.areEqual(this.aci, pnpInsert.aci);
        }

        public int hashCode() {
            String str = this.e164;
            int i = 0;
            int hashCode = (str == null ? 0 : str.hashCode()) * 31;
            PNI pni = this.pni;
            int hashCode2 = (hashCode + (pni == null ? 0 : pni.hashCode())) * 31;
            ACI aci = this.aci;
            if (aci != null) {
                i = aci.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            return "PnpInsert(e164=" + this.e164 + ", pni=" + this.pni + ", aci=" + this.aci + ')';
        }

        public final String getE164() {
            return this.e164;
        }

        public final PNI getPni() {
            return this.pni;
        }

        public final ACI getAci() {
            return this.aci;
        }

        public PnpInsert(String str, PNI pni, ACI aci) {
            super(null);
            this.e164 = str;
            this.pni = pni;
            this.aci = aci;
        }
    }
}
