package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public final class ChatColor extends GeneratedMessageLite<ChatColor, Builder> implements ChatColorOrBuilder {
    private static final ChatColor DEFAULT_INSTANCE;
    public static final int LINEARGRADIENT_FIELD_NUMBER;
    private static volatile Parser<ChatColor> PARSER;
    public static final int SINGLECOLOR_FIELD_NUMBER;
    private int chatColorCase_ = 0;
    private Object chatColor_;

    /* loaded from: classes4.dex */
    public interface FileOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getUri();

        ByteString getUriBytes();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes4.dex */
    public interface LinearGradientOrBuilder extends MessageLiteOrBuilder {
        int getColors(int i);

        int getColorsCount();

        List<Integer> getColorsList();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        float getPositions(int i);

        int getPositionsCount();

        List<Float> getPositionsList();

        float getRotation();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes4.dex */
    public interface SingleColorOrBuilder extends MessageLiteOrBuilder {
        int getColor();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private ChatColor() {
    }

    /* loaded from: classes4.dex */
    public static final class SingleColor extends GeneratedMessageLite<SingleColor, Builder> implements SingleColorOrBuilder {
        public static final int COLOR_FIELD_NUMBER;
        private static final SingleColor DEFAULT_INSTANCE;
        private static volatile Parser<SingleColor> PARSER;
        private int color_;

        private SingleColor() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.SingleColorOrBuilder
        public int getColor() {
            return this.color_;
        }

        public void setColor(int i) {
            this.color_ = i;
        }

        public void clearColor() {
            this.color_ = 0;
        }

        public static SingleColor parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (SingleColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static SingleColor parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (SingleColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static SingleColor parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (SingleColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static SingleColor parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (SingleColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static SingleColor parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (SingleColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static SingleColor parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (SingleColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static SingleColor parseFrom(InputStream inputStream) throws IOException {
            return (SingleColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static SingleColor parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (SingleColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static SingleColor parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SingleColor) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static SingleColor parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (SingleColor) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static SingleColor parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (SingleColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static SingleColor parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (SingleColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(SingleColor singleColor) {
            return DEFAULT_INSTANCE.createBuilder(singleColor);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<SingleColor, Builder> implements SingleColorOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(SingleColor.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.SingleColorOrBuilder
            public int getColor() {
                return ((SingleColor) this.instance).getColor();
            }

            public Builder setColor(int i) {
                copyOnWrite();
                ((SingleColor) this.instance).setColor(i);
                return this;
            }

            public Builder clearColor() {
                copyOnWrite();
                ((SingleColor) this.instance).clearColor();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new SingleColor();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\u0004", new Object[]{"color_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<SingleColor> parser = PARSER;
                    if (parser == null) {
                        synchronized (SingleColor.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            SingleColor singleColor = new SingleColor();
            DEFAULT_INSTANCE = singleColor;
            GeneratedMessageLite.registerDefaultInstance(SingleColor.class, singleColor);
        }

        public static SingleColor getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<SingleColor> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class LinearGradient extends GeneratedMessageLite<LinearGradient, Builder> implements LinearGradientOrBuilder {
        public static final int COLORS_FIELD_NUMBER;
        private static final LinearGradient DEFAULT_INSTANCE;
        private static volatile Parser<LinearGradient> PARSER;
        public static final int POSITIONS_FIELD_NUMBER;
        public static final int ROTATION_FIELD_NUMBER;
        private int colorsMemoizedSerializedSize = -1;
        private Internal.IntList colors_ = GeneratedMessageLite.emptyIntList();
        private int positionsMemoizedSerializedSize = -1;
        private Internal.FloatList positions_ = GeneratedMessageLite.emptyFloatList();
        private float rotation_;

        private LinearGradient() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
        public float getRotation() {
            return this.rotation_;
        }

        public void setRotation(float f) {
            this.rotation_ = f;
        }

        public void clearRotation() {
            this.rotation_ = 0.0f;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
        public List<Integer> getColorsList() {
            return this.colors_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
        public int getColorsCount() {
            return this.colors_.size();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
        public int getColors(int i) {
            return this.colors_.getInt(i);
        }

        private void ensureColorsIsMutable() {
            Internal.IntList intList = this.colors_;
            if (!intList.isModifiable()) {
                this.colors_ = GeneratedMessageLite.mutableCopy(intList);
            }
        }

        public void setColors(int i, int i2) {
            ensureColorsIsMutable();
            this.colors_.setInt(i, i2);
        }

        public void addColors(int i) {
            ensureColorsIsMutable();
            this.colors_.addInt(i);
        }

        public void addAllColors(Iterable<? extends Integer> iterable) {
            ensureColorsIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.colors_);
        }

        public void clearColors() {
            this.colors_ = GeneratedMessageLite.emptyIntList();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
        public List<Float> getPositionsList() {
            return this.positions_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
        public int getPositionsCount() {
            return this.positions_.size();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
        public float getPositions(int i) {
            return this.positions_.getFloat(i);
        }

        private void ensurePositionsIsMutable() {
            Internal.FloatList floatList = this.positions_;
            if (!floatList.isModifiable()) {
                this.positions_ = GeneratedMessageLite.mutableCopy(floatList);
            }
        }

        public void setPositions(int i, float f) {
            ensurePositionsIsMutable();
            this.positions_.setFloat(i, f);
        }

        public void addPositions(float f) {
            ensurePositionsIsMutable();
            this.positions_.addFloat(f);
        }

        public void addAllPositions(Iterable<? extends Float> iterable) {
            ensurePositionsIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.positions_);
        }

        public void clearPositions() {
            this.positions_ = GeneratedMessageLite.emptyFloatList();
        }

        public static LinearGradient parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (LinearGradient) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static LinearGradient parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (LinearGradient) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static LinearGradient parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (LinearGradient) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static LinearGradient parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (LinearGradient) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static LinearGradient parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (LinearGradient) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static LinearGradient parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (LinearGradient) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static LinearGradient parseFrom(InputStream inputStream) throws IOException {
            return (LinearGradient) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static LinearGradient parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (LinearGradient) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static LinearGradient parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (LinearGradient) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static LinearGradient parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (LinearGradient) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static LinearGradient parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (LinearGradient) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static LinearGradient parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (LinearGradient) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(LinearGradient linearGradient) {
            return DEFAULT_INSTANCE.createBuilder(linearGradient);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<LinearGradient, Builder> implements LinearGradientOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(LinearGradient.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
            public float getRotation() {
                return ((LinearGradient) this.instance).getRotation();
            }

            public Builder setRotation(float f) {
                copyOnWrite();
                ((LinearGradient) this.instance).setRotation(f);
                return this;
            }

            public Builder clearRotation() {
                copyOnWrite();
                ((LinearGradient) this.instance).clearRotation();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
            public List<Integer> getColorsList() {
                return Collections.unmodifiableList(((LinearGradient) this.instance).getColorsList());
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
            public int getColorsCount() {
                return ((LinearGradient) this.instance).getColorsCount();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
            public int getColors(int i) {
                return ((LinearGradient) this.instance).getColors(i);
            }

            public Builder setColors(int i, int i2) {
                copyOnWrite();
                ((LinearGradient) this.instance).setColors(i, i2);
                return this;
            }

            public Builder addColors(int i) {
                copyOnWrite();
                ((LinearGradient) this.instance).addColors(i);
                return this;
            }

            public Builder addAllColors(Iterable<? extends Integer> iterable) {
                copyOnWrite();
                ((LinearGradient) this.instance).addAllColors(iterable);
                return this;
            }

            public Builder clearColors() {
                copyOnWrite();
                ((LinearGradient) this.instance).clearColors();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
            public List<Float> getPositionsList() {
                return Collections.unmodifiableList(((LinearGradient) this.instance).getPositionsList());
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
            public int getPositionsCount() {
                return ((LinearGradient) this.instance).getPositionsCount();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.LinearGradientOrBuilder
            public float getPositions(int i) {
                return ((LinearGradient) this.instance).getPositions(i);
            }

            public Builder setPositions(int i, float f) {
                copyOnWrite();
                ((LinearGradient) this.instance).setPositions(i, f);
                return this;
            }

            public Builder addPositions(float f) {
                copyOnWrite();
                ((LinearGradient) this.instance).addPositions(f);
                return this;
            }

            public Builder addAllPositions(Iterable<? extends Float> iterable) {
                copyOnWrite();
                ((LinearGradient) this.instance).addAllPositions(iterable);
                return this;
            }

            public Builder clearPositions() {
                copyOnWrite();
                ((LinearGradient) this.instance).clearPositions();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new LinearGradient();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0002\u0000\u0001\u0001\u0002'\u0003$", new Object[]{"rotation_", "colors_", "positions_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<LinearGradient> parser = PARSER;
                    if (parser == null) {
                        synchronized (LinearGradient.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            LinearGradient linearGradient = new LinearGradient();
            DEFAULT_INSTANCE = linearGradient;
            GeneratedMessageLite.registerDefaultInstance(LinearGradient.class, linearGradient);
        }

        public static LinearGradient getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<LinearGradient> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes4.dex */
    public static final class File extends GeneratedMessageLite<File, Builder> implements FileOrBuilder {
        private static final File DEFAULT_INSTANCE;
        private static volatile Parser<File> PARSER;
        public static final int URI_FIELD_NUMBER;
        private String uri_ = "";

        private File() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.FileOrBuilder
        public String getUri() {
            return this.uri_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.FileOrBuilder
        public ByteString getUriBytes() {
            return ByteString.copyFromUtf8(this.uri_);
        }

        public void setUri(String str) {
            str.getClass();
            this.uri_ = str;
        }

        public void clearUri() {
            this.uri_ = getDefaultInstance().getUri();
        }

        public void setUriBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.uri_ = byteString.toStringUtf8();
        }

        public static File parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (File) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static File parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (File) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static File parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (File) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static File parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (File) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static File parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (File) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static File parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (File) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static File parseFrom(InputStream inputStream) throws IOException {
            return (File) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static File parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (File) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static File parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (File) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static File parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (File) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static File parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (File) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static File parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (File) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(File file) {
            return DEFAULT_INSTANCE.createBuilder(file);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<File, Builder> implements FileOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(File.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.FileOrBuilder
            public String getUri() {
                return ((File) this.instance).getUri();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor.FileOrBuilder
            public ByteString getUriBytes() {
                return ((File) this.instance).getUriBytes();
            }

            public Builder setUri(String str) {
                copyOnWrite();
                ((File) this.instance).setUri(str);
                return this;
            }

            public Builder clearUri() {
                copyOnWrite();
                ((File) this.instance).clearUri();
                return this;
            }

            public Builder setUriBytes(ByteString byteString) {
                copyOnWrite();
                ((File) this.instance).setUriBytes(byteString);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new File();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001Ȉ", new Object[]{"uri_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<File> parser = PARSER;
                    if (parser == null) {
                        synchronized (File.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            File file = new File();
            DEFAULT_INSTANCE = file;
            GeneratedMessageLite.registerDefaultInstance(File.class, file);
        }

        public static File getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<File> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes4.dex */
    public enum ChatColorCase {
        SINGLECOLOR(1),
        LINEARGRADIENT(2),
        CHATCOLOR_NOT_SET(0);
        
        private final int value;

        ChatColorCase(int i) {
            this.value = i;
        }

        @Deprecated
        public static ChatColorCase valueOf(int i) {
            return forNumber(i);
        }

        public static ChatColorCase forNumber(int i) {
            if (i == 0) {
                return CHATCOLOR_NOT_SET;
            }
            if (i == 1) {
                return SINGLECOLOR;
            }
            if (i != 2) {
                return null;
            }
            return LINEARGRADIENT;
        }

        public int getNumber() {
            return this.value;
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColorOrBuilder
    public ChatColorCase getChatColorCase() {
        return ChatColorCase.forNumber(this.chatColorCase_);
    }

    public void clearChatColor() {
        this.chatColorCase_ = 0;
        this.chatColor_ = null;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColorOrBuilder
    public boolean hasSingleColor() {
        return this.chatColorCase_ == 1;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColorOrBuilder
    public SingleColor getSingleColor() {
        if (this.chatColorCase_ == 1) {
            return (SingleColor) this.chatColor_;
        }
        return SingleColor.getDefaultInstance();
    }

    public void setSingleColor(SingleColor singleColor) {
        singleColor.getClass();
        this.chatColor_ = singleColor;
        this.chatColorCase_ = 1;
    }

    public void mergeSingleColor(SingleColor singleColor) {
        singleColor.getClass();
        if (this.chatColorCase_ != 1 || this.chatColor_ == SingleColor.getDefaultInstance()) {
            this.chatColor_ = singleColor;
        } else {
            this.chatColor_ = SingleColor.newBuilder((SingleColor) this.chatColor_).mergeFrom((SingleColor.Builder) singleColor).buildPartial();
        }
        this.chatColorCase_ = 1;
    }

    public void clearSingleColor() {
        if (this.chatColorCase_ == 1) {
            this.chatColorCase_ = 0;
            this.chatColor_ = null;
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColorOrBuilder
    public boolean hasLinearGradient() {
        return this.chatColorCase_ == 2;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColorOrBuilder
    public LinearGradient getLinearGradient() {
        if (this.chatColorCase_ == 2) {
            return (LinearGradient) this.chatColor_;
        }
        return LinearGradient.getDefaultInstance();
    }

    public void setLinearGradient(LinearGradient linearGradient) {
        linearGradient.getClass();
        this.chatColor_ = linearGradient;
        this.chatColorCase_ = 2;
    }

    public void mergeLinearGradient(LinearGradient linearGradient) {
        linearGradient.getClass();
        if (this.chatColorCase_ != 2 || this.chatColor_ == LinearGradient.getDefaultInstance()) {
            this.chatColor_ = linearGradient;
        } else {
            this.chatColor_ = LinearGradient.newBuilder((LinearGradient) this.chatColor_).mergeFrom((LinearGradient.Builder) linearGradient).buildPartial();
        }
        this.chatColorCase_ = 2;
    }

    public void clearLinearGradient() {
        if (this.chatColorCase_ == 2) {
            this.chatColorCase_ = 0;
            this.chatColor_ = null;
        }
    }

    public static ChatColor parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ChatColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ChatColor parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ChatColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ChatColor parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ChatColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ChatColor parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ChatColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ChatColor parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ChatColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ChatColor parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ChatColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ChatColor parseFrom(InputStream inputStream) throws IOException {
        return (ChatColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ChatColor parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ChatColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ChatColor parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ChatColor) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ChatColor parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ChatColor) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ChatColor parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ChatColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ChatColor parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ChatColor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ChatColor chatColor) {
        return DEFAULT_INSTANCE.createBuilder(chatColor);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ChatColor, Builder> implements ChatColorOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(ChatColor.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColorOrBuilder
        public ChatColorCase getChatColorCase() {
            return ((ChatColor) this.instance).getChatColorCase();
        }

        public Builder clearChatColor() {
            copyOnWrite();
            ((ChatColor) this.instance).clearChatColor();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColorOrBuilder
        public boolean hasSingleColor() {
            return ((ChatColor) this.instance).hasSingleColor();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColorOrBuilder
        public SingleColor getSingleColor() {
            return ((ChatColor) this.instance).getSingleColor();
        }

        public Builder setSingleColor(SingleColor singleColor) {
            copyOnWrite();
            ((ChatColor) this.instance).setSingleColor(singleColor);
            return this;
        }

        public Builder setSingleColor(SingleColor.Builder builder) {
            copyOnWrite();
            ((ChatColor) this.instance).setSingleColor(builder.build());
            return this;
        }

        public Builder mergeSingleColor(SingleColor singleColor) {
            copyOnWrite();
            ((ChatColor) this.instance).mergeSingleColor(singleColor);
            return this;
        }

        public Builder clearSingleColor() {
            copyOnWrite();
            ((ChatColor) this.instance).clearSingleColor();
            return this;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColorOrBuilder
        public boolean hasLinearGradient() {
            return ((ChatColor) this.instance).hasLinearGradient();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ChatColorOrBuilder
        public LinearGradient getLinearGradient() {
            return ((ChatColor) this.instance).getLinearGradient();
        }

        public Builder setLinearGradient(LinearGradient linearGradient) {
            copyOnWrite();
            ((ChatColor) this.instance).setLinearGradient(linearGradient);
            return this;
        }

        public Builder setLinearGradient(LinearGradient.Builder builder) {
            copyOnWrite();
            ((ChatColor) this.instance).setLinearGradient(builder.build());
            return this;
        }

        public Builder mergeLinearGradient(LinearGradient linearGradient) {
            copyOnWrite();
            ((ChatColor) this.instance).mergeLinearGradient(linearGradient);
            return this;
        }

        public Builder clearLinearGradient() {
            copyOnWrite();
            ((ChatColor) this.instance).clearLinearGradient();
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ChatColor();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0001\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001<\u0000\u0002<\u0000", new Object[]{"chatColor_", "chatColorCase_", SingleColor.class, LinearGradient.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ChatColor> parser = PARSER;
                if (parser == null) {
                    synchronized (ChatColor.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ChatColor chatColor = new ChatColor();
        DEFAULT_INSTANCE = chatColor;
        GeneratedMessageLite.registerDefaultInstance(ChatColor.class, chatColor);
    }

    public static ChatColor getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ChatColor> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
