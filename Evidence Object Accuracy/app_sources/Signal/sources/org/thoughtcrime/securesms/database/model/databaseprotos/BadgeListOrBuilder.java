package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;
import org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList;

/* loaded from: classes4.dex */
public interface BadgeListOrBuilder extends MessageLiteOrBuilder {
    BadgeList.Badge getBadges(int i);

    int getBadgesCount();

    List<BadgeList.Badge> getBadgesList();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
