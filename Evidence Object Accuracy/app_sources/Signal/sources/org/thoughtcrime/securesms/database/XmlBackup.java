package org.thoughtcrime.securesms.database;

import android.text.TextUtils;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

/* loaded from: classes4.dex */
public class XmlBackup {
    private static final String ADDRESS;
    private static final String BODY;
    private static final String CONTACT_NAME;
    private static final String DATE;
    private static final String LOCKED;
    private static final String PROTOCOL;
    private static final String READ;
    private static final String READABLE_DATE;
    private static final String SC_TOA;
    private static final String SERVICE_CENTER;
    private static final String STATUS;
    private static final String SUBJECT;
    private static final String TOA;
    private static final String TYPE;
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
    private final XmlPullParser parser;

    public XmlBackup(String str) throws XmlPullParserException, FileNotFoundException {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        this.parser = newPullParser;
        newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
        newPullParser.setInput(new FileInputStream(str), null);
    }

    public XmlBackupItem getNext() throws IOException, XmlPullParserException {
        int attributeCount;
        while (this.parser.next() != 1) {
            if (this.parser.getEventType() == 2 && this.parser.getName().equalsIgnoreCase("sms") && (attributeCount = this.parser.getAttributeCount()) > 0) {
                XmlBackupItem xmlBackupItem = new XmlBackupItem();
                for (int i = 0; i < attributeCount; i++) {
                    String attributeName = this.parser.getAttributeName(i);
                    if (attributeName.equals("protocol")) {
                        xmlBackupItem.protocol = Integer.parseInt(this.parser.getAttributeValue(i));
                    } else if (attributeName.equals("address")) {
                        xmlBackupItem.address = this.parser.getAttributeValue(i);
                    } else if (attributeName.equals(CONTACT_NAME)) {
                        xmlBackupItem.contactName = this.parser.getAttributeValue(i);
                    } else if (attributeName.equals("date")) {
                        xmlBackupItem.date = Long.parseLong(this.parser.getAttributeValue(i));
                    } else if (attributeName.equals(READABLE_DATE)) {
                        xmlBackupItem.readableDate = this.parser.getAttributeValue(i);
                    } else if (attributeName.equals("type")) {
                        xmlBackupItem.type = Integer.parseInt(this.parser.getAttributeValue(i));
                    } else if (attributeName.equals("subject")) {
                        xmlBackupItem.subject = this.parser.getAttributeValue(i);
                    } else if (attributeName.equals("body")) {
                        xmlBackupItem.body = this.parser.getAttributeValue(i);
                    } else if (attributeName.equals("service_center")) {
                        xmlBackupItem.serviceCenter = this.parser.getAttributeValue(i);
                    } else if (attributeName.equals("read")) {
                        xmlBackupItem.read = Integer.parseInt(this.parser.getAttributeValue(i));
                    } else if (attributeName.equals("status")) {
                        xmlBackupItem.status = Integer.parseInt(this.parser.getAttributeValue(i));
                    }
                }
                return xmlBackupItem;
            }
        }
        return null;
    }

    /* loaded from: classes4.dex */
    public static class XmlBackupItem {
        private String address;
        private String body;
        private String contactName;
        private long date;
        private int protocol;
        private int read;
        private String readableDate;
        private String serviceCenter;
        private int status;
        private String subject;
        private int type;

        public XmlBackupItem() {
        }

        public XmlBackupItem(int i, String str, String str2, long j, int i2, String str3, String str4, String str5, int i3, int i4) {
            this.protocol = i;
            this.address = str;
            this.contactName = str2;
            this.date = j;
            this.readableDate = XmlBackup.dateFormatter.format(Long.valueOf(j));
            this.type = i2;
            this.subject = str3;
            this.body = str4;
            this.serviceCenter = str5;
            this.read = i3;
            this.status = i4;
        }

        public int getProtocol() {
            return this.protocol;
        }

        public String getAddress() {
            return this.address;
        }

        public String getContactName() {
            return this.contactName;
        }

        public long getDate() {
            return this.date;
        }

        public String getReadableDate() {
            return this.readableDate;
        }

        public int getType() {
            return this.type;
        }

        public String getSubject() {
            return this.subject;
        }

        public String getBody() {
            return this.body;
        }

        public String getServiceCenter() {
            return this.serviceCenter;
        }

        public int getRead() {
            return this.read;
        }

        public int getStatus() {
            return this.status;
        }
    }

    /* loaded from: classes4.dex */
    public static class Writer {
        private static final String CLOSE_ATTRIBUTE;
        private static final String CLOSE_EMPTYTAG;
        private static final String CLOSE_TAG_SMSES;
        private static final String CREATED_BY;
        private static final String OPEN_ATTRIBUTE;
        private static final String OPEN_TAG_SMS;
        private static final String OPEN_TAG_SMSES;
        private static final Pattern PATTERN = Pattern.compile("[^ -퟿]");
        private static final String XML_HEADER;
        private final BufferedWriter bufferedWriter;

        public Writer(String str, int i) throws IOException {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(str, false));
            this.bufferedWriter = bufferedWriter;
            bufferedWriter.write(XML_HEADER);
            bufferedWriter.newLine();
            bufferedWriter.write(CREATED_BY);
            bufferedWriter.newLine();
            bufferedWriter.write(String.format(Locale.ROOT, OPEN_TAG_SMSES, Integer.valueOf(i)));
        }

        public void writeItem(XmlBackupItem xmlBackupItem) throws IOException {
            StringBuilder sb = new StringBuilder();
            sb.append(OPEN_TAG_SMS);
            appendAttribute(sb, "protocol", Integer.valueOf(xmlBackupItem.getProtocol()));
            appendAttribute(sb, "address", escapeXML(xmlBackupItem.getAddress()));
            appendAttribute(sb, XmlBackup.CONTACT_NAME, escapeXML(xmlBackupItem.getContactName()));
            appendAttribute(sb, "date", Long.valueOf(xmlBackupItem.getDate()));
            appendAttribute(sb, XmlBackup.READABLE_DATE, xmlBackupItem.getReadableDate());
            appendAttribute(sb, "type", Integer.valueOf(xmlBackupItem.getType()));
            appendAttribute(sb, "subject", escapeXML(xmlBackupItem.getSubject()));
            appendAttribute(sb, "body", escapeXML(xmlBackupItem.getBody()));
            appendAttribute(sb, XmlBackup.TOA, "null");
            appendAttribute(sb, XmlBackup.SC_TOA, "null");
            appendAttribute(sb, "service_center", xmlBackupItem.getServiceCenter());
            appendAttribute(sb, "read", Integer.valueOf(xmlBackupItem.getRead()));
            appendAttribute(sb, "status", Integer.valueOf(xmlBackupItem.getStatus()));
            appendAttribute(sb, XmlBackup.LOCKED, 0);
            sb.append(CLOSE_EMPTYTAG);
            this.bufferedWriter.newLine();
            this.bufferedWriter.write(sb.toString());
        }

        private <T> void appendAttribute(StringBuilder sb, String str, T t) {
            sb.append(str);
            sb.append(OPEN_ATTRIBUTE);
            sb.append(t);
            sb.append(CLOSE_ATTRIBUTE);
        }

        public void close() throws IOException {
            this.bufferedWriter.newLine();
            this.bufferedWriter.write(CLOSE_TAG_SMSES);
            this.bufferedWriter.close();
        }

        private String escapeXML(String str) {
            if (TextUtils.isEmpty(str)) {
                return str;
            }
            Matcher matcher = PATTERN.matcher(str.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;").replace("'", "&apos;"));
            StringBuffer stringBuffer = new StringBuffer();
            while (matcher.find()) {
                char[] charArray = matcher.group(0).toCharArray();
                String str2 = "";
                for (char c : charArray) {
                    str2 = str2 + "&#" + ((int) c) + ";";
                }
                matcher.appendReplacement(stringBuffer, str2);
            }
            matcher.appendTail(stringBuffer);
            return stringBuffer.toString();
        }
    }
}
