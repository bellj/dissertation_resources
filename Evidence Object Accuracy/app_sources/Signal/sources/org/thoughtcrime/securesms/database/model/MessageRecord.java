package org.thoughtcrime.securesms.database.model;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import androidx.core.content.ContextCompat;
import com.annimon.stream.Stream;
import com.google.protobuf.ByteString;
import j$.util.Collection$EL;
import j$.util.function.Consumer;
import j$.util.function.Function;
import j$.util.function.Predicate;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import org.signal.core.util.StringUtil;
import org.signal.core.util.logging.Log;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiProvider;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiParser;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.model.UpdateDescription;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;
import org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2Context;
import org.thoughtcrime.securesms.database.model.databaseprotos.GroupCallUpdateDetails;
import org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails;
import org.thoughtcrime.securesms.emoji.EmojiSource;
import org.thoughtcrime.securesms.emoji.JumboEmoji;
import org.thoughtcrime.securesms.groups.GroupMigrationMembershipChange;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.ExpirationUtil;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.groupsv2.DecryptedGroupUtil;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes4.dex */
public abstract class MessageRecord extends DisplayRecord {
    private static final String TAG = Log.tag(MessageRecord.class);
    private final long expireStarted;
    private final long expiresIn;
    private final long id;
    private final Recipient individualRecipient;
    protected Boolean isJumboji = null;
    private final Set<IdentityKeyMismatch> mismatches;
    private final Set<NetworkFailure> networkFailures;
    private final long notifiedTimestamp;
    private final List<ReactionRecord> reactions;
    private final long receiptTimestamp;
    private final int recipientDeviceId;
    private final boolean remoteDelete;
    private final long serverTimestamp;
    private final int subscriptionId;
    private final boolean unidentified;

    public boolean hasMessageRanges() {
        return false;
    }

    public boolean hasSelfMention() {
        return false;
    }

    public boolean isInMemoryMessageRecord() {
        return false;
    }

    public boolean isMediaPending() {
        return false;
    }

    public abstract boolean isMms();

    public abstract boolean isMmsNotification();

    public boolean isViewOnce() {
        return false;
    }

    public MessageRecord(long j, String str, Recipient recipient, Recipient recipient2, int i, long j2, long j3, long j4, long j5, int i2, int i3, long j6, Set<IdentityKeyMismatch> set, Set<NetworkFailure> set2, int i4, long j7, long j8, int i5, boolean z, List<ReactionRecord> list, boolean z2, long j9, int i6, long j10) {
        super(str, recipient, j2, j3, j5, i2, i3, j6, i5, i6);
        this.id = j;
        this.individualRecipient = recipient2;
        this.recipientDeviceId = i;
        this.mismatches = set;
        this.networkFailures = set2;
        this.subscriptionId = i4;
        this.expiresIn = j7;
        this.expireStarted = j8;
        this.unidentified = z;
        this.reactions = list;
        this.serverTimestamp = j4;
        this.remoteDelete = z2;
        this.notifiedTimestamp = j9;
        this.receiptTimestamp = j10;
    }

    public boolean isSecure() {
        return MmsSmsColumns.Types.isSecureType(this.type);
    }

    public boolean isLegacyMessage() {
        return MmsSmsColumns.Types.isLegacyType(this.type);
    }

    @Override // org.thoughtcrime.securesms.database.model.DisplayRecord
    public SpannableString getDisplayBody(Context context) {
        return getDisplayBody(context, null);
    }

    public SpannableString getDisplayBody(Context context, Consumer<RecipientId> consumer) {
        UpdateDescription updateDisplayBody = getUpdateDisplayBody(context, consumer);
        if (updateDisplayBody != null) {
            return new SpannableString(updateDisplayBody.getSpannable());
        }
        return new SpannableString(getBody());
    }

    public UpdateDescription getUpdateDisplayBody(Context context, Consumer<RecipientId> consumer) {
        if (isGroupUpdate() && isGroupV2()) {
            return getGv2ChangeDescription(context, getBody(), consumer);
        }
        if (isGroupUpdate() && isOutgoing()) {
            return staticUpdateDescription(context.getString(R.string.MessageRecord_you_updated_group), R.drawable.ic_update_group_16);
        }
        if (isGroupUpdate()) {
            return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda1
                public final /* synthetic */ Context f$1;

                {
                    this.f$1 = r2;
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return MessageRecord.this.lambda$getUpdateDisplayBody$0(this.f$1, (Recipient) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }, R.drawable.ic_update_group_16);
        }
        if (isGroupQuit() && isOutgoing()) {
            return staticUpdateDescription(context.getString(R.string.MessageRecord_left_group), R.drawable.ic_update_group_leave_16);
        }
        if (isGroupQuit()) {
            return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda6
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return MessageRecord.lambda$getUpdateDisplayBody$1(this.f$0, (Recipient) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }, R.drawable.ic_update_group_leave_16);
        }
        if (isIncomingAudioCall()) {
            return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda7
                public final /* synthetic */ Context f$1;

                {
                    this.f$1 = r2;
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return MessageRecord.this.lambda$getUpdateDisplayBody$2(this.f$1, (Recipient) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }, R.drawable.ic_update_audio_call_incoming_16);
        }
        if (isIncomingVideoCall()) {
            return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda8
                public final /* synthetic */ Context f$1;

                {
                    this.f$1 = r2;
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return MessageRecord.this.lambda$getUpdateDisplayBody$3(this.f$1, (Recipient) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }, R.drawable.ic_update_video_call_incoming_16);
        }
        if (isOutgoingAudioCall()) {
            return staticUpdateDescription(context.getString(R.string.MessageRecord_you_called_date, getCallDateString(context)), R.drawable.ic_update_audio_call_outgoing_16);
        }
        if (isOutgoingVideoCall()) {
            return staticUpdateDescription(context.getString(R.string.MessageRecord_you_called_date, getCallDateString(context)), R.drawable.ic_update_video_call_outgoing_16);
        }
        if (isMissedAudioCall()) {
            return staticUpdateDescription(context.getString(R.string.MessageRecord_missed_audio_call_date, getCallDateString(context)), R.drawable.ic_update_audio_call_missed_16, ContextCompat.getColor(context, R.color.core_red_shade), ContextCompat.getColor(context, R.color.core_red));
        }
        if (isMissedVideoCall()) {
            return staticUpdateDescription(context.getString(R.string.MessageRecord_missed_video_call_date, getCallDateString(context)), R.drawable.ic_update_video_call_missed_16, ContextCompat.getColor(context, R.color.core_red_shade), ContextCompat.getColor(context, R.color.core_red));
        }
        if (isGroupCall()) {
            return getGroupCallUpdateDescription(context, getBody(), true);
        }
        if (isJoined()) {
            return staticUpdateDescription(context.getString(R.string.MessageRecord_s_joined_signal, getIndividualRecipient().getDisplayName(context)), R.drawable.ic_update_group_add_16);
        }
        if (isExpirationTimerUpdate()) {
            int expiresIn = (int) (getExpiresIn() / 1000);
            if (expiresIn > 0) {
                String expirationDisplayValue = ExpirationUtil.getExpirationDisplayValue(context, expiresIn);
                if (isOutgoing()) {
                    return staticUpdateDescription(context.getString(R.string.MessageRecord_you_set_disappearing_message_time_to_s, expirationDisplayValue), R.drawable.ic_update_timer_16);
                }
                return fromRecipient(getIndividualRecipient(), new Function(context, expirationDisplayValue) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda10
                    public final /* synthetic */ Context f$0;
                    public final /* synthetic */ String f$1;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function andThen(Function function) {
                        return Function.CC.$default$andThen(this, function);
                    }

                    @Override // j$.util.function.Function
                    public final Object apply(Object obj) {
                        return MessageRecord.lambda$getUpdateDisplayBody$5(this.f$0, this.f$1, (Recipient) obj);
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function compose(Function function) {
                        return Function.CC.$default$compose(this, function);
                    }
                }, R.drawable.ic_update_timer_16);
            } else if (isOutgoing()) {
                return staticUpdateDescription(context.getString(R.string.MessageRecord_you_disabled_disappearing_messages), R.drawable.ic_update_timer_disabled_16);
            } else {
                return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda9
                    public final /* synthetic */ Context f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function andThen(Function function) {
                        return Function.CC.$default$andThen(this, function);
                    }

                    @Override // j$.util.function.Function
                    public final Object apply(Object obj) {
                        return MessageRecord.lambda$getUpdateDisplayBody$4(this.f$0, (Recipient) obj);
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function compose(Function function) {
                        return Function.CC.$default$compose(this, function);
                    }
                }, R.drawable.ic_update_timer_disabled_16);
            }
        } else if (isIdentityUpdate()) {
            return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda11
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return MessageRecord.lambda$getUpdateDisplayBody$6(this.f$0, (Recipient) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }, R.drawable.ic_update_safety_number_16);
        } else {
            if (isIdentityVerified()) {
                if (isOutgoing()) {
                    return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda12
                        public final /* synthetic */ Context f$0;

                        {
                            this.f$0 = r1;
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function andThen(Function function) {
                            return Function.CC.$default$andThen(this, function);
                        }

                        @Override // j$.util.function.Function
                        public final Object apply(Object obj) {
                            return MessageRecord.lambda$getUpdateDisplayBody$7(this.f$0, (Recipient) obj);
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function compose(Function function) {
                            return Function.CC.$default$compose(this, function);
                        }
                    }, R.drawable.ic_update_verified_16);
                }
                return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda13
                    public final /* synthetic */ Context f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function andThen(Function function) {
                        return Function.CC.$default$andThen(this, function);
                    }

                    @Override // j$.util.function.Function
                    public final Object apply(Object obj) {
                        return MessageRecord.lambda$getUpdateDisplayBody$8(this.f$0, (Recipient) obj);
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function compose(Function function) {
                        return Function.CC.$default$compose(this, function);
                    }
                }, R.drawable.ic_update_verified_16);
            } else if (isIdentityDefault()) {
                if (isOutgoing()) {
                    return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda14
                        public final /* synthetic */ Context f$0;

                        {
                            this.f$0 = r1;
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function andThen(Function function) {
                            return Function.CC.$default$andThen(this, function);
                        }

                        @Override // j$.util.function.Function
                        public final Object apply(Object obj) {
                            return MessageRecord.lambda$getUpdateDisplayBody$9(this.f$0, (Recipient) obj);
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function compose(Function function) {
                            return Function.CC.$default$compose(this, function);
                        }
                    }, R.drawable.ic_update_info_16);
                }
                return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda2
                    public final /* synthetic */ Context f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function andThen(Function function) {
                        return Function.CC.$default$andThen(this, function);
                    }

                    @Override // j$.util.function.Function
                    public final Object apply(Object obj) {
                        return MessageRecord.lambda$getUpdateDisplayBody$10(this.f$0, (Recipient) obj);
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function compose(Function function) {
                        return Function.CC.$default$compose(this, function);
                    }
                }, R.drawable.ic_update_info_16);
            } else if (isProfileChange()) {
                return staticUpdateDescription(getProfileChangeDescription(context), R.drawable.ic_update_profile_16);
            } else {
                if (isChangeNumber()) {
                    return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda3
                        public final /* synthetic */ Context f$0;

                        {
                            this.f$0 = r1;
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function andThen(Function function) {
                            return Function.CC.$default$andThen(this, function);
                        }

                        @Override // j$.util.function.Function
                        public final Object apply(Object obj) {
                            return MessageRecord.lambda$getUpdateDisplayBody$11(this.f$0, (Recipient) obj);
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function compose(Function function) {
                            return Function.CC.$default$compose(this, function);
                        }
                    }, R.drawable.ic_phone_16);
                }
                if (isBoostRequest()) {
                    return staticUpdateDescription(context.getString(R.string.MessageRecord_like_this_new_feature_help_support_signal_with_a_one_time_donation), 0);
                }
                if (isEndSession()) {
                    if (isOutgoing()) {
                        return staticUpdateDescription(context.getString(R.string.SmsMessageRecord_secure_session_reset), R.drawable.ic_update_info_16);
                    }
                    return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda4
                        public final /* synthetic */ Context f$0;

                        {
                            this.f$0 = r1;
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function andThen(Function function) {
                            return Function.CC.$default$andThen(this, function);
                        }

                        @Override // j$.util.function.Function
                        public final Object apply(Object obj) {
                            return MessageRecord.lambda$getUpdateDisplayBody$12(this.f$0, (Recipient) obj);
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function compose(Function function) {
                            return Function.CC.$default$compose(this, function);
                        }
                    }, R.drawable.ic_update_info_16);
                } else if (isGroupV1MigrationEvent()) {
                    return getGroupMigrationEventDescription(context);
                } else {
                    if (isChatSessionRefresh()) {
                        return staticUpdateDescription(context.getString(R.string.MessageRecord_chat_session_refreshed), R.drawable.ic_refresh_16);
                    }
                    if (isBadDecryptType()) {
                        return fromRecipient(getIndividualRecipient(), new Function(context) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda5
                            public final /* synthetic */ Context f$0;

                            {
                                this.f$0 = r1;
                            }

                            @Override // j$.util.function.Function
                            public /* synthetic */ Function andThen(Function function) {
                                return Function.CC.$default$andThen(this, function);
                            }

                            @Override // j$.util.function.Function
                            public final Object apply(Object obj) {
                                return MessageRecord.lambda$getUpdateDisplayBody$13(this.f$0, (Recipient) obj);
                            }

                            @Override // j$.util.function.Function
                            public /* synthetic */ Function compose(Function function) {
                                return Function.CC.$default$compose(this, function);
                            }
                        }, R.drawable.ic_error_outline_14);
                    }
                    return null;
                }
            }
        }
    }

    public /* synthetic */ String lambda$getUpdateDisplayBody$0(Context context, Recipient recipient) {
        return GroupUtil.getNonV2GroupDescription(context, getBody()).toString(recipient);
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$1(Context context, Recipient recipient) {
        return context.getString(R.string.ConversationItem_group_action_left, recipient.getDisplayName(context));
    }

    public /* synthetic */ String lambda$getUpdateDisplayBody$2(Context context, Recipient recipient) {
        return context.getString(R.string.MessageRecord_s_called_you_date, recipient.getDisplayName(context), getCallDateString(context));
    }

    public /* synthetic */ String lambda$getUpdateDisplayBody$3(Context context, Recipient recipient) {
        return context.getString(R.string.MessageRecord_s_called_you_date, recipient.getDisplayName(context), getCallDateString(context));
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$4(Context context, Recipient recipient) {
        return context.getString(R.string.MessageRecord_s_disabled_disappearing_messages, recipient.getDisplayName(context));
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$5(Context context, String str, Recipient recipient) {
        return context.getString(R.string.MessageRecord_s_set_disappearing_message_time_to_s, recipient.getDisplayName(context), str);
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$6(Context context, Recipient recipient) {
        return context.getString(R.string.MessageRecord_your_safety_number_with_s_has_changed, recipient.getDisplayName(context));
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$7(Context context, Recipient recipient) {
        return context.getString(R.string.MessageRecord_you_marked_your_safety_number_with_s_verified, recipient.getDisplayName(context));
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$8(Context context, Recipient recipient) {
        return context.getString(R.string.MessageRecord_you_marked_your_safety_number_with_s_verified_from_another_device, recipient.getDisplayName(context));
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$9(Context context, Recipient recipient) {
        return context.getString(R.string.MessageRecord_you_marked_your_safety_number_with_s_unverified, recipient.getDisplayName(context));
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$10(Context context, Recipient recipient) {
        return context.getString(R.string.MessageRecord_you_marked_your_safety_number_with_s_unverified_from_another_device, recipient.getDisplayName(context));
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$11(Context context, Recipient recipient) {
        return context.getString(R.string.MessageRecord_s_changed_their_phone_number, recipient.getDisplayName(context));
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$12(Context context, Recipient recipient) {
        return context.getString(R.string.SmsMessageRecord_secure_session_reset_s, recipient.getDisplayName(context));
    }

    public static /* synthetic */ String lambda$getUpdateDisplayBody$13(Context context, Recipient recipient) {
        return context.getString(R.string.MessageRecord_a_message_from_s_couldnt_be_delivered, recipient.getDisplayName(context));
    }

    public boolean isDisplayBodyEmpty(Context context) {
        return getUpdateDisplayBody(context, null) == null && getBody().isEmpty();
    }

    public boolean isSelfCreatedGroup() {
        DecryptedGroupV2Context decryptedGroupV2Context = getDecryptedGroupV2Context();
        if (decryptedGroupV2Context == null) {
            return false;
        }
        return selfCreatedGroup(decryptedGroupV2Context.getChange());
    }

    DecryptedGroupV2Context getDecryptedGroupV2Context() {
        if (!isGroupUpdate() || !isGroupV2()) {
            return null;
        }
        try {
            return DecryptedGroupV2Context.parseFrom(Base64.decode(getBody()));
        } catch (IOException e) {
            Log.w(TAG, "GV2 Message update detail could not be read", e);
            return null;
        }
    }

    private static boolean selfCreatedGroup(DecryptedGroupChange decryptedGroupChange) {
        return decryptedGroupChange.getRevision() == 0 && decryptedGroupChange.getEditor().equals(UuidUtil.toByteString(SignalStore.account().requireAci().uuid()));
    }

    public static UpdateDescription getGv2ChangeDescription(Context context, String str, Consumer<RecipientId> consumer) {
        try {
            DecryptedGroupV2Context parseFrom = DecryptedGroupV2Context.parseFrom(Base64.decode(str));
            GroupsV2UpdateMessageProducer groupsV2UpdateMessageProducer = new GroupsV2UpdateMessageProducer(context, SignalStore.account().getServiceIds(), consumer);
            if (parseFrom.hasChange() && (parseFrom.getGroupState().getRevision() != 0 || parseFrom.hasPreviousGroupState())) {
                return UpdateDescription.concatWithNewLines(groupsV2UpdateMessageProducer.describeChanges(parseFrom.getPreviousGroupState(), parseFrom.getChange()));
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(groupsV2UpdateMessageProducer.describeNewGroup(parseFrom.getGroupState(), parseFrom.getChange()));
            if (parseFrom.getChange().hasNewTimer()) {
                groupsV2UpdateMessageProducer.describeNewTimer(parseFrom.getChange(), arrayList);
            }
            if (selfCreatedGroup(parseFrom.getChange())) {
                arrayList.add(staticUpdateDescription(context.getString(R.string.MessageRecord_invite_friends_to_this_group), 0));
            }
            return UpdateDescription.concatWithNewLines(arrayList);
        } catch (IOException | IllegalArgumentException e) {
            Log.w(TAG, "GV2 Message update detail could not be read", e);
            return staticUpdateDescription(context.getString(R.string.MessageRecord_group_updated), R.drawable.ic_update_group_16);
        }
    }

    public InviteAddState getGv2AddInviteState() {
        UUID fromByteStringOrNull;
        DecryptedGroupV2Context decryptedGroupV2Context = getDecryptedGroupV2Context();
        if (decryptedGroupV2Context == null) {
            return null;
        }
        boolean isPresent = DecryptedGroupUtil.findPendingByUuid(decryptedGroupV2Context.getGroupState().getPendingMembersList(), SignalStore.account().requireAci().uuid()).isPresent();
        if (decryptedGroupV2Context.hasChange() && (fromByteStringOrNull = UuidUtil.fromByteStringOrNull(decryptedGroupV2Context.getChange().getEditor())) != null) {
            return new InviteAddState(isPresent, fromByteStringOrNull);
        }
        Log.w(TAG, "GV2 Message editor could not be determined");
        return null;
    }

    private String getCallDateString(Context context) {
        return DateUtils.getSimpleRelativeTimeSpanString(context, Locale.getDefault(), getDateSent());
    }

    private static UpdateDescription fromRecipient(Recipient recipient, Function<Recipient, String> function, int i) {
        return UpdateDescription.mentioning(Collections.singletonList(recipient.getServiceId().orElse(ServiceId.UNKNOWN)), new UpdateDescription.SpannableFactory(recipient) { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda0
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.database.model.UpdateDescription.SpannableFactory
            public final Spannable create() {
                return MessageRecord.lambda$fromRecipient$14(Function.this, this.f$1);
            }
        }, i);
    }

    public static /* synthetic */ Spannable lambda$fromRecipient$14(Function function, Recipient recipient) {
        return new SpannableString((CharSequence) function.apply(recipient.resolve()));
    }

    private static UpdateDescription staticUpdateDescription(String str, int i) {
        return UpdateDescription.staticDescription(str, i);
    }

    private static UpdateDescription staticUpdateDescription(String str, int i, int i2, int i3) {
        return UpdateDescription.staticDescription(str, i, i2, i3);
    }

    private String getProfileChangeDescription(Context context) {
        try {
            ProfileChangeDetails parseFrom = ProfileChangeDetails.parseFrom(Base64.decode(getBody()));
            if (parseFrom.hasProfileNameChange()) {
                String displayName = getIndividualRecipient().getDisplayName(context);
                String isolateBidi = StringUtil.isolateBidi(ProfileName.fromSerialized(parseFrom.getProfileNameChange().getNew()).toString());
                String isolateBidi2 = StringUtil.isolateBidi(ProfileName.fromSerialized(parseFrom.getProfileNameChange().getPrevious()).toString());
                if (getIndividualRecipient().isSystemContact()) {
                    return context.getString(R.string.MessageRecord_changed_their_profile_name_from_to, displayName, isolateBidi2, isolateBidi);
                }
                return context.getString(R.string.MessageRecord_changed_their_profile_name_to, isolateBidi2, isolateBidi);
            }
        } catch (IOException e) {
            Log.w(TAG, "Profile name change details could not be read", e);
        }
        return context.getString(R.string.MessageRecord_changed_their_profile, getIndividualRecipient().getDisplayName(context));
    }

    private UpdateDescription getGroupMigrationEventDescription(Context context) {
        if (Util.isEmpty(getBody())) {
            return staticUpdateDescription(context.getString(R.string.MessageRecord_this_group_was_updated_to_a_new_group), R.drawable.ic_update_group_role_16);
        }
        GroupMigrationMembershipChange groupV1MigrationMembershipChanges = getGroupV1MigrationMembershipChanges();
        ArrayList arrayList = new ArrayList(2);
        if (groupV1MigrationMembershipChanges.getPending().size() == 1 && groupV1MigrationMembershipChanges.getPending().get(0).equals(Recipient.self().getId())) {
            arrayList.add(staticUpdateDescription(context.getString(R.string.MessageRecord_you_couldnt_be_added_to_the_new_group_and_have_been_invited_to_join), R.drawable.ic_update_group_add_16));
        } else if (groupV1MigrationMembershipChanges.getPending().size() > 0) {
            int size = groupV1MigrationMembershipChanges.getPending().size();
            arrayList.add(staticUpdateDescription(context.getResources().getQuantityString(R.plurals.MessageRecord_members_couldnt_be_added_to_the_new_group_and_have_been_invited, size, Integer.valueOf(size)), R.drawable.ic_update_group_add_16));
        }
        if (groupV1MigrationMembershipChanges.getDropped().size() > 0) {
            int size2 = groupV1MigrationMembershipChanges.getDropped().size();
            arrayList.add(staticUpdateDescription(context.getResources().getQuantityString(R.plurals.MessageRecord_members_couldnt_be_added_to_the_new_group_and_have_been_removed, size2, Integer.valueOf(size2)), R.drawable.ic_update_group_remove_16));
        }
        return UpdateDescription.concatWithNewLines(arrayList);
    }

    public static UpdateDescription getGroupCallUpdateDescription(Context context, String str, boolean z) {
        GroupCallUpdateDetails parse = GroupCallUpdateDetailsUtil.parse(str);
        List list = Stream.of(parse.getInCallUuidsList()).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda15
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return UuidUtil.parseOrNull((String) obj);
            }
        }).withoutNulls().map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda16
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ServiceId.from((UUID) obj);
            }
        }).toList();
        return UpdateDescription.mentioning(list, new GroupCallUpdateMessageFactory(context, list, z, parse), R.drawable.ic_video_16);
    }

    public boolean isGroupV2DescriptionUpdate() {
        DecryptedGroupV2Context decryptedGroupV2Context = getDecryptedGroupV2Context();
        if (decryptedGroupV2Context == null || !decryptedGroupV2Context.hasChange() || !getDecryptedGroupV2Context().getChange().hasNewDescription()) {
            return false;
        }
        return true;
    }

    public String getGroupV2DescriptionUpdate() {
        DecryptedGroupV2Context decryptedGroupV2Context = getDecryptedGroupV2Context();
        if (decryptedGroupV2Context == null || !decryptedGroupV2Context.getChange().hasNewDescription()) {
            return "";
        }
        return decryptedGroupV2Context.getChange().getNewDescription().getValue();
    }

    public boolean isGroupV2JoinRequest(ServiceId serviceId) {
        if (serviceId == null) {
            return false;
        }
        return isGroupV2JoinRequest(UuidUtil.toByteString(serviceId.uuid()));
    }

    public boolean isGroupV2JoinRequest(ByteString byteString) {
        DecryptedGroupV2Context decryptedGroupV2Context = getDecryptedGroupV2Context();
        if (decryptedGroupV2Context == null || !decryptedGroupV2Context.hasChange()) {
            return false;
        }
        DecryptedGroupChange change = decryptedGroupV2Context.getChange();
        if (!change.getEditor().equals(byteString) || !Collection$EL.stream(change.getNewRequestingMembersList()).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.database.model.MessageRecord$$ExternalSyntheticLambda17
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return MessageRecord.lambda$isGroupV2JoinRequest$15(ByteString.this, (DecryptedRequestingMember) obj);
            }
        })) {
            return false;
        }
        return true;
    }

    public static /* synthetic */ boolean lambda$isGroupV2JoinRequest$15(ByteString byteString, DecryptedRequestingMember decryptedRequestingMember) {
        return decryptedRequestingMember.getUuid().equals(byteString);
    }

    public boolean isCollapsedGroupV2JoinUpdate() {
        return isCollapsedGroupV2JoinUpdate(null);
    }

    public boolean isCollapsedGroupV2JoinUpdate(ServiceId serviceId) {
        DecryptedGroupV2Context decryptedGroupV2Context = getDecryptedGroupV2Context();
        if (decryptedGroupV2Context == null || !decryptedGroupV2Context.hasChange()) {
            return false;
        }
        DecryptedGroupChange change = decryptedGroupV2Context.getChange();
        if (change.getNewRequestingMembersCount() <= 0 || change.getDeleteRequestingMembersCount() <= 0) {
            return false;
        }
        if (serviceId == null || change.getEditor().equals(UuidUtil.toByteString(serviceId.uuid()))) {
            return true;
        }
        return false;
    }

    public static String createNewContextWithAppendedDeleteJoinRequest(MessageRecord messageRecord, int i, ByteString byteString) {
        DecryptedGroupV2Context decryptedGroupV2Context = messageRecord.getDecryptedGroupV2Context();
        if (decryptedGroupV2Context == null || !decryptedGroupV2Context.hasChange()) {
            throw new AssertionError("Attempting to modify a message with no change");
        }
        return Base64.encodeBytes(decryptedGroupV2Context.toBuilder().setChange(decryptedGroupV2Context.getChange().toBuilder().setRevision(i).addDeleteRequestingMembers(byteString)).build().toByteArray());
    }

    public long getId() {
        return this.id;
    }

    public boolean isPush() {
        return MmsSmsColumns.Types.isPushType(this.type) && !MmsSmsColumns.Types.isForcedSms(this.type);
    }

    public long getTimestamp() {
        if ((isPush() || isCallLog()) && getDateSent() < getDateReceived()) {
            return getDateSent();
        }
        return getDateReceived();
    }

    public long getServerTimestamp() {
        return this.serverTimestamp;
    }

    public boolean isForcedSms() {
        return MmsSmsColumns.Types.isForcedSms(this.type);
    }

    public boolean isIdentityVerified() {
        return MmsSmsColumns.Types.isIdentityVerified(this.type);
    }

    public boolean isIdentityDefault() {
        return MmsSmsColumns.Types.isIdentityDefault(this.type);
    }

    public boolean isIdentityMismatchFailure() {
        Set<IdentityKeyMismatch> set = this.mismatches;
        return set != null && !set.isEmpty();
    }

    public boolean isBundleKeyExchange() {
        return MmsSmsColumns.Types.isBundleKeyExchange(this.type);
    }

    public boolean isContentBundleKeyExchange() {
        return MmsSmsColumns.Types.isContentBundleKeyExchange(this.type);
    }

    public boolean isRateLimited() {
        return MmsSmsColumns.Types.isRateLimited(this.type);
    }

    public boolean isIdentityUpdate() {
        return MmsSmsColumns.Types.isIdentityUpdate(this.type);
    }

    public boolean isCorruptedKeyExchange() {
        return MmsSmsColumns.Types.isCorruptedKeyExchange(this.type);
    }

    public boolean isBadDecryptType() {
        return MmsSmsColumns.Types.isBadDecryptType(this.type);
    }

    public boolean isInvalidVersionKeyExchange() {
        return MmsSmsColumns.Types.isInvalidVersionKeyExchange(this.type);
    }

    public boolean isGroupV1MigrationEvent() {
        return MmsSmsColumns.Types.isGroupV1MigrationEvent(this.type);
    }

    public GroupMigrationMembershipChange getGroupV1MigrationMembershipChanges() {
        if (isGroupV1MigrationEvent()) {
            return GroupMigrationMembershipChange.deserialize(getBody());
        }
        return GroupMigrationMembershipChange.empty();
    }

    public boolean isUpdate() {
        return isGroupAction() || isJoined() || isExpirationTimerUpdate() || isCallLog() || isEndSession() || isIdentityUpdate() || isIdentityVerified() || isIdentityDefault() || isProfileChange() || isGroupV1MigrationEvent() || isChatSessionRefresh() || isBadDecryptType() || isChangeNumber() || isBoostRequest();
    }

    public Recipient getIndividualRecipient() {
        return this.individualRecipient.live().get();
    }

    public int getRecipientDeviceId() {
        return this.recipientDeviceId;
    }

    @Override // org.thoughtcrime.securesms.database.model.DisplayRecord
    public long getType() {
        return this.type;
    }

    public Set<IdentityKeyMismatch> getIdentityKeyMismatches() {
        return this.mismatches;
    }

    public Set<NetworkFailure> getNetworkFailures() {
        return this.networkFailures;
    }

    public boolean hasNetworkFailures() {
        Set<NetworkFailure> set = this.networkFailures;
        return set != null && !set.isEmpty();
    }

    public boolean hasFailedWithNetworkFailures() {
        return isFailed() && ((getRecipient().isPushGroup() && hasNetworkFailures()) || !isIdentityMismatchFailure());
    }

    public boolean isChatSessionRefresh() {
        return MmsSmsColumns.Types.isChatSessionRefresh(this.type);
    }

    public static SpannableString emphasisAdded(String str) {
        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(new RelativeSizeSpan(0.9f), 0, str.length(), 33);
        spannableString.setSpan(new StyleSpan(2), 0, str.length(), 33);
        return spannableString;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof MessageRecord)) {
            MessageRecord messageRecord = (MessageRecord) obj;
            if (messageRecord.getId() == getId() && messageRecord.isMms() == isMms()) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return (int) getId();
    }

    public int getSubscriptionId() {
        return this.subscriptionId;
    }

    public long getExpiresIn() {
        return this.expiresIn;
    }

    public long getExpireStarted() {
        return this.expireStarted;
    }

    public boolean isUnidentified() {
        return this.unidentified;
    }

    public boolean isRemoteDelete() {
        return this.remoteDelete;
    }

    public List<ReactionRecord> getReactions() {
        return this.reactions;
    }

    public long getNotifiedTimestamp() {
        return this.notifiedTimestamp;
    }

    public long getIncomingStoryViewedAtTimestamp() {
        if (isOutgoing()) {
            return -1;
        }
        return this.receiptTimestamp;
    }

    public long getReceiptTimestamp() {
        if (!isOutgoing()) {
            return getDateSent();
        }
        return this.receiptTimestamp;
    }

    public boolean isJumbomoji(Context context) {
        if (this.isJumboji == null) {
            if (getBody().length() <= EmojiSource.getLatest().getMaxEmojiLength() * 5) {
                EmojiParser.CandidateList candidates = EmojiProvider.getCandidates(getDisplayBody(context));
                this.isJumboji = Boolean.valueOf(candidates != null && candidates.allEmojis && candidates.size() <= 5 && (candidates.hasJumboForAll() || JumboEmoji.canDownloadJumbo(context)));
            } else {
                this.isJumboji = Boolean.FALSE;
            }
        }
        return this.isJumboji.booleanValue();
    }

    public BodyRangeList requireMessageRanges() {
        throw null;
    }

    /* loaded from: classes4.dex */
    public static final class InviteAddState {
        private final UUID addedOrInvitedBy;
        private final boolean invited;

        public InviteAddState(boolean z, UUID uuid) {
            this.invited = z;
            this.addedOrInvitedBy = uuid;
        }

        public UUID getAddedOrInvitedBy() {
            return this.addedOrInvitedBy;
        }

        public boolean isInvited() {
            return this.invited;
        }
    }
}
