package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.SqlUtil;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* loaded from: classes4.dex */
public class GroupReceiptDatabase extends Database {
    public static final String[] CREATE_INDEXES = {"CREATE INDEX IF NOT EXISTS group_receipt_mms_id_index ON group_receipts (mms_id);"};
    public static final String CREATE_TABLE;
    private static final String ID;
    public static final String MMS_ID;
    static final String RECIPIENT_ID;
    private static final String STATUS;
    public static final int STATUS_DELIVERED;
    public static final int STATUS_READ;
    public static final int STATUS_SKIPPED;
    public static final int STATUS_UNDELIVERED;
    public static final int STATUS_UNKNOWN;
    public static final int STATUS_VIEWED;
    public static final String TABLE_NAME;
    private static final String TIMESTAMP;
    private static final String UNIDENTIFIED;

    public GroupReceiptDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public void insert(Collection<RecipientId> collection, long j, int i, long j2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ArrayList arrayList = new ArrayList(collection.size());
        for (RecipientId recipientId : collection) {
            ContentValues contentValues = new ContentValues(4);
            contentValues.put(MMS_ID, Long.valueOf(j));
            contentValues.put("address", recipientId.serialize());
            contentValues.put("status", Integer.valueOf(i));
            contentValues.put("timestamp", Long.valueOf(j2));
            arrayList.add(contentValues);
        }
        for (SqlUtil.Query query : SqlUtil.buildBulkInsert(TABLE_NAME, new String[]{MMS_ID, "address", "status", "timestamp"}, arrayList)) {
            signalWritableDatabase.execSQL(query.getWhere(), query.getWhereArgs());
        }
    }

    public void update(RecipientId recipientId, long j, int i, long j2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(2);
        contentValues.put("status", Integer.valueOf(i));
        contentValues.put("timestamp", Long.valueOf(j2));
        signalWritableDatabase.update(TABLE_NAME, contentValues, "mms_id = ? AND address = ? AND status < ?", new String[]{String.valueOf(j), recipientId.serialize(), String.valueOf(i)});
    }

    public void setUnidentified(Collection<Pair<RecipientId, Boolean>> collection, long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            for (Pair<RecipientId, Boolean> pair : collection) {
                ContentValues contentValues = new ContentValues(1);
                contentValues.put("unidentified", Integer.valueOf(pair.second().booleanValue() ? 1 : 0));
                signalWritableDatabase.update(TABLE_NAME, contentValues, "mms_id = ? AND address = ?", new String[]{String.valueOf(j), pair.first().serialize()});
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public void setSkipped(Collection<RecipientId> collection, long j) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            Iterator<RecipientId> it = collection.iterator();
            while (it.hasNext()) {
                ContentValues contentValues = new ContentValues(1);
                contentValues.put("status", (Integer) 4);
                writableDatabase.update(TABLE_NAME, contentValues, "mms_id = ? AND address = ?", new String[]{String.valueOf(j), it.next().serialize()});
            }
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public List<GroupReceiptInfo> getGroupReceiptInfo(long j) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        LinkedList linkedList = new LinkedList();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, null, "mms_id = ?", new String[]{String.valueOf(j)}, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                linkedList.add(new GroupReceiptInfo(RecipientId.from(query.getLong(query.getColumnIndexOrThrow("address"))), query.getInt(query.getColumnIndexOrThrow("status")), query.getLong(query.getColumnIndexOrThrow("timestamp")), query.getInt(query.getColumnIndexOrThrow("unidentified")) == 1));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return linkedList;
    }

    public GroupReceiptInfo getGroupReceiptInfo(long j, RecipientId recipientId) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "mms_id = ? AND address = ?", SqlUtil.buildArgs(Long.valueOf(j), recipientId), null, null, SubscriptionLevels.BOOST_LEVEL);
        try {
            if (query.moveToFirst()) {
                GroupReceiptInfo groupReceiptInfo = new GroupReceiptInfo(RecipientId.from(query.getLong(query.getColumnIndexOrThrow("address"))), query.getInt(query.getColumnIndexOrThrow("status")), query.getLong(query.getColumnIndexOrThrow("timestamp")), query.getInt(query.getColumnIndexOrThrow("unidentified")) == 1);
                query.close();
                return groupReceiptInfo;
            }
            query.close();
            return null;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public void deleteRowsForMessage(long j) {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, "mms_id = ?", new String[]{String.valueOf(j)});
    }

    public void deleteAbandonedRows() {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, "mms_id NOT IN (SELECT _id FROM mms)", (String[]) null);
    }

    public void deleteAllRows() {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, (String) null, (String[]) null);
    }

    /* loaded from: classes4.dex */
    public static class GroupReceiptInfo {
        private final RecipientId recipientId;
        private final int status;
        private final long timestamp;
        private final boolean unidentified;

        GroupReceiptInfo(RecipientId recipientId, int i, long j, boolean z) {
            this.recipientId = recipientId;
            this.status = i;
            this.timestamp = j;
            this.unidentified = z;
        }

        public RecipientId getRecipientId() {
            return this.recipientId;
        }

        public int getStatus() {
            return this.status;
        }

        public long getTimestamp() {
            return this.timestamp;
        }

        public boolean isUnidentified() {
            return this.unidentified;
        }
    }
}
