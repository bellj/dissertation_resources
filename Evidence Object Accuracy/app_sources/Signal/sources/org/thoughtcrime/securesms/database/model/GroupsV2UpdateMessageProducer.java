package org.thoughtcrime.securesms.database.model;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.View;
import androidx.core.content.ContextCompat;
import com.google.protobuf.ByteString;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Consumer;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.signal.core.util.StringUtil;
import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.Member;
import org.signal.storageservice.protos.groups.local.DecryptedApproveMember;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedModifyMemberRole;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMemberRemoval;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.signal.storageservice.protos.groups.local.EnabledState;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.UpdateDescription;
import org.thoughtcrime.securesms.groups.GV2AccessLevelUtil;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ExpirationUtil;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.whispersystems.signalservice.api.groupsv2.DecryptedGroupUtil;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.ServiceIds;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes4.dex */
public final class GroupsV2UpdateMessageProducer {
    private final Context context;
    private final Consumer<RecipientId> recipientClickHandler;
    private final ServiceIds selfIds;

    public GroupsV2UpdateMessageProducer(Context context, ServiceIds serviceIds, Consumer<RecipientId> consumer) {
        this.context = context;
        this.selfIds = serviceIds;
        this.recipientClickHandler = consumer;
    }

    public UpdateDescription describeNewGroup(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange) {
        Optional<DecryptedPendingMember> findPendingByUuid = DecryptedGroupUtil.findPendingByUuid(decryptedGroup.getPendingMembersList(), this.selfIds.getAci().uuid());
        if (!findPendingByUuid.isPresent() && this.selfIds.getPni() != null) {
            findPendingByUuid = DecryptedGroupUtil.findPendingByUuid(decryptedGroup.getPendingMembersList(), this.selfIds.getPni().uuid());
        }
        if (findPendingByUuid.isPresent()) {
            return updateDescription(R.string.MessageRecord_s_invited_you_to_the_group, findPendingByUuid.get().getAddedByUuid(), R.drawable.ic_update_group_add_16);
        }
        ByteString editor = decryptedGroupChange.getEditor();
        if (!editor.isEmpty()) {
            if (this.selfIds.matches(editor)) {
                return updateDescription(this.context.getString(R.string.MessageRecord_you_created_the_group), R.drawable.ic_update_group_16);
            }
            return updateDescription(R.string.MessageRecord_s_added_you, editor, R.drawable.ic_update_group_add_16);
        } else if (DecryptedGroupUtil.findMemberByUuid(decryptedGroup.getMembersList(), this.selfIds.getAci().uuid()).isPresent() || (this.selfIds.getPni() != null && DecryptedGroupUtil.findMemberByUuid(decryptedGroup.getMembersList(), this.selfIds.getPni().uuid()).isPresent())) {
            return updateDescription(this.context.getString(R.string.MessageRecord_you_joined_the_group), R.drawable.ic_update_group_add_16);
        } else {
            return updateDescription(this.context.getString(R.string.MessageRecord_group_updated), R.drawable.ic_update_group_16);
        }
    }

    public List<UpdateDescription> describeChanges(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange) {
        if (DecryptedGroup.getDefaultInstance().equals(decryptedGroup)) {
            decryptedGroup = null;
        }
        LinkedList linkedList = new LinkedList();
        if (decryptedGroupChange.getEditor().isEmpty() || UuidUtil.UNKNOWN_UUID.equals(UuidUtil.fromByteString(decryptedGroupChange.getEditor()))) {
            describeUnknownEditorMemberAdditions(decryptedGroupChange, linkedList);
            describeUnknownEditorModifyMemberRoles(decryptedGroupChange, linkedList);
            describeUnknownEditorInvitations(decryptedGroupChange, linkedList);
            describeUnknownEditorRevokedInvitations(decryptedGroupChange, linkedList);
            describeUnknownEditorPromotePending(decryptedGroupChange, linkedList);
            describeUnknownEditorNewTitle(decryptedGroupChange, linkedList);
            describeUnknownEditorNewDescription(decryptedGroupChange, linkedList);
            describeUnknownEditorNewAvatar(decryptedGroupChange, linkedList);
            describeUnknownEditorNewTimer(decryptedGroupChange, linkedList);
            describeUnknownEditorNewAttributeAccess(decryptedGroupChange, linkedList);
            describeUnknownEditorNewMembershipAccess(decryptedGroupChange, linkedList);
            describeUnknownEditorNewGroupInviteLinkAccess(decryptedGroup, decryptedGroupChange, linkedList);
            describeRequestingMembers(decryptedGroupChange, linkedList);
            describeUnknownEditorRequestingMembersApprovals(decryptedGroupChange, linkedList);
            describeUnknownEditorRequestingMembersDeletes(decryptedGroupChange, linkedList);
            describeUnknownEditorAnnouncementGroupChange(decryptedGroupChange, linkedList);
            describeUnknownEditorPromotePendingPniAci(decryptedGroupChange, linkedList);
            describeUnknownEditorMemberRemovals(decryptedGroupChange, linkedList);
            if (linkedList.isEmpty()) {
                describeUnknownEditorUnknownChange(linkedList);
            }
        } else {
            describeMemberAdditions(decryptedGroupChange, linkedList);
            describeModifyMemberRoles(decryptedGroupChange, linkedList);
            describeInvitations(decryptedGroupChange, linkedList);
            describeRevokedInvitations(decryptedGroupChange, linkedList);
            describePromotePending(decryptedGroupChange, linkedList);
            describeNewTitle(decryptedGroupChange, linkedList);
            describeNewDescription(decryptedGroupChange, linkedList);
            describeNewAvatar(decryptedGroupChange, linkedList);
            describeNewTimer(decryptedGroupChange, linkedList);
            describeNewAttributeAccess(decryptedGroupChange, linkedList);
            describeNewMembershipAccess(decryptedGroupChange, linkedList);
            describeNewGroupInviteLinkAccess(decryptedGroup, decryptedGroupChange, linkedList);
            describeRequestingMembers(decryptedGroupChange, linkedList);
            describeRequestingMembersApprovals(decryptedGroupChange, linkedList);
            describeRequestingMembersDeletes(decryptedGroupChange, linkedList);
            describeAnnouncementGroupChange(decryptedGroupChange, linkedList);
            describePromotePendingPniAci(decryptedGroupChange, linkedList);
            describeMemberRemovals(decryptedGroupChange, linkedList);
            if (linkedList.isEmpty()) {
                describeUnknownChange(decryptedGroupChange, linkedList);
            }
        }
        return linkedList;
    }

    private void describeUnknownChange(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        if (this.selfIds.matches(decryptedGroupChange.getEditor())) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_updated_group), R.drawable.ic_update_group_16));
        } else {
            list.add(updateDescription(R.string.MessageRecord_s_updated_group, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_16));
        }
    }

    private void describeUnknownEditorUnknownChange(List<UpdateDescription> list) {
        list.add(updateDescription(this.context.getString(R.string.MessageRecord_the_group_was_updated), R.drawable.ic_update_group_16));
    }

    private void describeMemberAdditions(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        for (DecryptedMember decryptedMember : decryptedGroupChange.getNewMembersList()) {
            boolean matches2 = this.selfIds.matches(decryptedMember.getUuid());
            if (matches) {
                if (matches2) {
                    list.add(0, updateDescription(this.context.getString(R.string.MessageRecord_you_joined_the_group_via_the_group_link), R.drawable.ic_update_group_accept_16));
                } else {
                    list.add(updateDescription(R.string.MessageRecord_you_added_s, decryptedMember.getUuid(), R.drawable.ic_update_group_add_16));
                }
            } else if (matches2) {
                list.add(0, updateDescription(R.string.MessageRecord_s_added_you, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_add_16));
            } else if (decryptedMember.getUuid().equals(decryptedGroupChange.getEditor())) {
                list.add(updateDescription(R.string.MessageRecord_s_joined_the_group_via_the_group_link, decryptedMember.getUuid(), R.drawable.ic_update_group_accept_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_added_s, decryptedGroupChange.getEditor(), decryptedMember.getUuid(), R.drawable.ic_update_group_add_16));
            }
        }
    }

    private void describeUnknownEditorMemberAdditions(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        for (DecryptedMember decryptedMember : decryptedGroupChange.getNewMembersList()) {
            if (this.selfIds.matches(decryptedMember.getUuid())) {
                list.add(0, updateDescription(this.context.getString(R.string.MessageRecord_you_joined_the_group), R.drawable.ic_update_group_add_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_joined_the_group, decryptedMember.getUuid(), R.drawable.ic_update_group_add_16));
            }
        }
    }

    private void describeMemberRemovals(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        for (ByteString byteString : decryptedGroupChange.getDeleteMembersList()) {
            boolean matches2 = this.selfIds.matches(byteString);
            if (matches) {
                if (matches2) {
                    list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_left_the_group), R.drawable.ic_update_group_leave_16));
                } else {
                    list.add(updateDescription(R.string.MessageRecord_you_removed_s, byteString, R.drawable.ic_update_group_remove_16));
                }
            } else if (matches2) {
                list.add(updateDescription(R.string.MessageRecord_s_removed_you_from_the_group, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_remove_16));
            } else if (byteString.equals(decryptedGroupChange.getEditor())) {
                list.add(updateDescription(R.string.MessageRecord_s_left_the_group, byteString, R.drawable.ic_update_group_leave_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_removed_s, decryptedGroupChange.getEditor(), byteString, R.drawable.ic_update_group_remove_16));
            }
        }
    }

    private void describeUnknownEditorMemberRemovals(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        for (ByteString byteString : decryptedGroupChange.getDeleteMembersList()) {
            if (this.selfIds.matches(byteString)) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_are_no_longer_in_the_group), R.drawable.ic_update_group_leave_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_is_no_longer_in_the_group, byteString, R.drawable.ic_update_group_leave_16));
            }
        }
    }

    private void describeModifyMemberRoles(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        for (DecryptedModifyMemberRole decryptedModifyMemberRole : decryptedGroupChange.getModifyMemberRolesList()) {
            boolean matches2 = this.selfIds.matches(decryptedModifyMemberRole.getUuid());
            if (decryptedModifyMemberRole.getRole() == Member.Role.ADMINISTRATOR) {
                if (matches) {
                    list.add(updateDescription(R.string.MessageRecord_you_made_s_an_admin, decryptedModifyMemberRole.getUuid(), R.drawable.ic_update_group_role_16));
                } else if (matches2) {
                    list.add(updateDescription(R.string.MessageRecord_s_made_you_an_admin, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_role_16));
                } else {
                    list.add(updateDescription(R.string.MessageRecord_s_made_s_an_admin, decryptedGroupChange.getEditor(), decryptedModifyMemberRole.getUuid(), R.drawable.ic_update_group_role_16));
                }
            } else if (matches) {
                list.add(updateDescription(R.string.MessageRecord_you_revoked_admin_privileges_from_s, decryptedModifyMemberRole.getUuid(), R.drawable.ic_update_group_role_16));
            } else if (matches2) {
                list.add(updateDescription(R.string.MessageRecord_s_revoked_your_admin_privileges, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_role_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_revoked_admin_privileges_from_s, decryptedGroupChange.getEditor(), decryptedModifyMemberRole.getUuid(), R.drawable.ic_update_group_role_16));
            }
        }
    }

    private void describeUnknownEditorModifyMemberRoles(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        for (DecryptedModifyMemberRole decryptedModifyMemberRole : decryptedGroupChange.getModifyMemberRolesList()) {
            boolean matches = this.selfIds.matches(decryptedModifyMemberRole.getUuid());
            if (decryptedModifyMemberRole.getRole() == Member.Role.ADMINISTRATOR) {
                if (matches) {
                    list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_are_now_an_admin), R.drawable.ic_update_group_role_16));
                } else {
                    list.add(updateDescription(R.string.MessageRecord_s_is_now_an_admin, decryptedModifyMemberRole.getUuid(), R.drawable.ic_update_group_role_16));
                }
            } else if (matches) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_are_no_longer_an_admin), R.drawable.ic_update_group_role_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_is_no_longer_an_admin, decryptedModifyMemberRole.getUuid(), R.drawable.ic_update_group_role_16));
            }
        }
    }

    private void describeInvitations(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        int i = 0;
        for (DecryptedPendingMember decryptedPendingMember : decryptedGroupChange.getNewPendingMembersList()) {
            if (this.selfIds.matches(decryptedPendingMember.getUuid())) {
                list.add(0, updateDescription(R.string.MessageRecord_s_invited_you_to_the_group, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_add_16));
            } else if (matches) {
                list.add(updateDescription(R.string.MessageRecord_you_invited_s_to_the_group, decryptedPendingMember.getUuid(), R.drawable.ic_update_group_add_16));
            } else {
                i++;
            }
        }
        if (i > 0) {
            list.add(updateDescription(R.plurals.MessageRecord_s_invited_members, i, decryptedGroupChange.getEditor(), Integer.valueOf(i), R.drawable.ic_update_group_add_16));
        }
    }

    private void describeUnknownEditorInvitations(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        int i = 0;
        for (DecryptedPendingMember decryptedPendingMember : decryptedGroupChange.getNewPendingMembersList()) {
            if (!this.selfIds.matches(decryptedPendingMember.getUuid())) {
                i++;
            } else if (UuidUtil.UNKNOWN_UUID.equals(UuidUtil.fromByteStringOrUnknown(decryptedPendingMember.getAddedByUuid()))) {
                list.add(0, updateDescription(this.context.getString(R.string.MessageRecord_you_were_invited_to_the_group), R.drawable.ic_update_group_add_16));
            } else {
                list.add(0, updateDescription(R.string.MessageRecord_s_invited_you_to_the_group, decryptedPendingMember.getAddedByUuid(), R.drawable.ic_update_group_add_16));
            }
        }
        if (i > 0) {
            list.add(updateDescription(this.context.getResources().getQuantityString(R.plurals.MessageRecord_d_people_were_invited_to_the_group, i, Integer.valueOf(i)), R.drawable.ic_update_group_add_16));
        }
    }

    private void describeRevokedInvitations(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        int i = 0;
        for (DecryptedPendingMemberRemoval decryptedPendingMemberRemoval : decryptedGroupChange.getDeletePendingMembersList()) {
            if (decryptedPendingMemberRemoval.getUuid().equals(decryptedGroupChange.getEditor())) {
                if (matches) {
                    list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_declined_the_invitation_to_the_group), R.drawable.ic_update_group_decline_16));
                } else {
                    list.add(updateDescription(this.context.getString(R.string.MessageRecord_someone_declined_an_invitation_to_the_group), R.drawable.ic_update_group_decline_16));
                }
            } else if (this.selfIds.matches(decryptedPendingMemberRemoval.getUuid())) {
                list.add(updateDescription(R.string.MessageRecord_s_revoked_your_invitation_to_the_group, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_decline_16));
            } else {
                i++;
            }
        }
        if (i <= 0) {
            return;
        }
        if (matches) {
            list.add(updateDescription(this.context.getResources().getQuantityString(R.plurals.MessageRecord_you_revoked_invites, i, Integer.valueOf(i)), R.drawable.ic_update_group_decline_16));
        } else {
            list.add(updateDescription(R.plurals.MessageRecord_s_revoked_invites, i, decryptedGroupChange.getEditor(), Integer.valueOf(i), R.drawable.ic_update_group_decline_16));
        }
    }

    private void describeUnknownEditorRevokedInvitations(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        int i = 0;
        for (DecryptedPendingMemberRemoval decryptedPendingMemberRemoval : decryptedGroupChange.getDeletePendingMembersList()) {
            if (this.selfIds.matches(decryptedPendingMemberRemoval.getUuid())) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_an_admin_revoked_your_invitation_to_the_group), R.drawable.ic_update_group_decline_16));
            } else {
                i++;
            }
        }
        if (i > 0) {
            list.add(updateDescription(this.context.getResources().getQuantityString(R.plurals.MessageRecord_d_invitations_were_revoked, i, Integer.valueOf(i)), R.drawable.ic_update_group_decline_16));
        }
    }

    private void describePromotePending(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        for (DecryptedMember decryptedMember : decryptedGroupChange.getPromotePendingMembersList()) {
            ByteString uuid = decryptedMember.getUuid();
            boolean matches2 = this.selfIds.matches(uuid);
            if (matches) {
                if (matches2) {
                    list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_accepted_invite), R.drawable.ic_update_group_accept_16));
                } else {
                    list.add(updateDescription(R.string.MessageRecord_you_added_invited_member_s, uuid, R.drawable.ic_update_group_add_16));
                }
            } else if (matches2) {
                list.add(updateDescription(R.string.MessageRecord_s_added_you, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_add_16));
            } else if (uuid.equals(decryptedGroupChange.getEditor())) {
                list.add(updateDescription(R.string.MessageRecord_s_accepted_invite, uuid, R.drawable.ic_update_group_accept_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_added_invited_member_s, decryptedGroupChange.getEditor(), uuid, R.drawable.ic_update_group_add_16));
            }
        }
    }

    private void describeUnknownEditorPromotePending(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        for (DecryptedMember decryptedMember : decryptedGroupChange.getPromotePendingMembersList()) {
            ByteString uuid = decryptedMember.getUuid();
            if (this.selfIds.matches(uuid)) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_joined_the_group), R.drawable.ic_update_group_add_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_joined_the_group, uuid, R.drawable.ic_update_group_add_16));
            }
        }
    }

    private void describeNewTitle(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        if (decryptedGroupChange.hasNewTitle()) {
            String isolateBidi = StringUtil.isolateBidi(decryptedGroupChange.getNewTitle().getValue());
            if (matches) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_changed_the_group_name_to_s, isolateBidi), R.drawable.ic_update_group_name_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_changed_the_group_name_to_s, decryptedGroupChange.getEditor(), isolateBidi, R.drawable.ic_update_group_name_16));
            }
        }
    }

    private void describeNewDescription(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        if (!decryptedGroupChange.hasNewDescription()) {
            return;
        }
        if (matches) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_changed_the_group_description), R.drawable.ic_update_group_name_16));
        } else {
            list.add(updateDescription(R.string.MessageRecord_s_changed_the_group_description, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_name_16));
        }
    }

    private void describeUnknownEditorNewTitle(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        if (decryptedGroupChange.hasNewTitle()) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_the_group_name_has_changed_to_s, StringUtil.isolateBidi(decryptedGroupChange.getNewTitle().getValue())), R.drawable.ic_update_group_name_16));
        }
    }

    private void describeUnknownEditorNewDescription(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        if (decryptedGroupChange.hasNewDescription()) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_the_group_description_has_changed), R.drawable.ic_update_group_name_16));
        }
    }

    private void describeNewAvatar(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        if (!decryptedGroupChange.hasNewAvatar()) {
            return;
        }
        if (matches) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_changed_the_group_avatar), R.drawable.ic_update_group_avatar_16));
        } else {
            list.add(updateDescription(R.string.MessageRecord_s_changed_the_group_avatar, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_avatar_16));
        }
    }

    private void describeUnknownEditorNewAvatar(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        if (decryptedGroupChange.hasNewAvatar()) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_the_group_group_avatar_has_been_changed), R.drawable.ic_update_group_avatar_16));
        }
    }

    public void describeNewTimer(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        if (decryptedGroupChange.hasNewTimer()) {
            String expirationDisplayValue = ExpirationUtil.getExpirationDisplayValue(this.context, decryptedGroupChange.getNewTimer().getDuration());
            if (matches) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_set_disappearing_message_time_to_s, expirationDisplayValue), R.drawable.ic_update_timer_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_set_disappearing_message_time_to_s, decryptedGroupChange.getEditor(), expirationDisplayValue, R.drawable.ic_update_timer_16));
            }
        }
    }

    private void describeUnknownEditorNewTimer(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        if (decryptedGroupChange.hasNewTimer()) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_disappearing_message_time_set_to_s, ExpirationUtil.getExpirationDisplayValue(this.context, decryptedGroupChange.getNewTimer().getDuration())), R.drawable.ic_update_timer_16));
        }
    }

    private void describeNewAttributeAccess(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        if (decryptedGroupChange.getNewAttributeAccess() != AccessControl.AccessRequired.UNKNOWN) {
            String gV2AccessLevelUtil = GV2AccessLevelUtil.toString(this.context, decryptedGroupChange.getNewAttributeAccess());
            if (matches) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_changed_who_can_edit_group_info_to_s, gV2AccessLevelUtil), R.drawable.ic_update_group_role_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_changed_who_can_edit_group_info_to_s, decryptedGroupChange.getEditor(), gV2AccessLevelUtil, R.drawable.ic_update_group_role_16));
            }
        }
    }

    private void describeUnknownEditorNewAttributeAccess(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        if (decryptedGroupChange.getNewAttributeAccess() != AccessControl.AccessRequired.UNKNOWN) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_who_can_edit_group_info_has_been_changed_to_s, GV2AccessLevelUtil.toString(this.context, decryptedGroupChange.getNewAttributeAccess())), R.drawable.ic_update_group_role_16));
        }
    }

    private void describeNewMembershipAccess(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        if (decryptedGroupChange.getNewMemberAccess() != AccessControl.AccessRequired.UNKNOWN) {
            String gV2AccessLevelUtil = GV2AccessLevelUtil.toString(this.context, decryptedGroupChange.getNewMemberAccess());
            if (matches) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_changed_who_can_edit_group_membership_to_s, gV2AccessLevelUtil), R.drawable.ic_update_group_role_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_changed_who_can_edit_group_membership_to_s, decryptedGroupChange.getEditor(), gV2AccessLevelUtil, R.drawable.ic_update_group_role_16));
            }
        }
    }

    private void describeUnknownEditorNewMembershipAccess(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        if (decryptedGroupChange.getNewMemberAccess() != AccessControl.AccessRequired.UNKNOWN) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_who_can_edit_group_membership_has_been_changed_to_s, GV2AccessLevelUtil.toString(this.context, decryptedGroupChange.getNewMemberAccess())), R.drawable.ic_update_group_role_16));
        }
    }

    private void describeNewGroupInviteLinkAccess(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        AccessControl.AccessRequired addFromInviteLink = decryptedGroup != null ? decryptedGroup.getAccessControl().getAddFromInviteLink() : null;
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        boolean z = false;
        int i = AnonymousClass1.$SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[decryptedGroupChange.getNewInviteLinkAccess().ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    if (matches) {
                        list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_turned_off_the_group_link), R.drawable.ic_update_group_role_16));
                    } else {
                        list.add(updateDescription(R.string.MessageRecord_s_turned_off_the_group_link, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_role_16));
                    }
                }
                if (!z && decryptedGroupChange.getNewInviteLinkPassword().size() > 0) {
                    if (matches) {
                        list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_reset_the_group_link), R.drawable.ic_update_group_role_16));
                        return;
                    } else {
                        list.add(updateDescription(R.string.MessageRecord_s_reset_the_group_link, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_role_16));
                        return;
                    }
                }
            } else if (matches) {
                if (addFromInviteLink == AccessControl.AccessRequired.ANY) {
                    list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_turned_on_admin_approval_for_the_group_link), R.drawable.ic_update_group_role_16));
                } else {
                    list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_turned_on_the_group_link_with_admin_approval_on), R.drawable.ic_update_group_role_16));
                }
            } else if (addFromInviteLink == AccessControl.AccessRequired.ANY) {
                list.add(updateDescription(R.string.MessageRecord_s_turned_on_admin_approval_for_the_group_link, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_role_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_turned_on_the_group_link_with_admin_approval_on, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_role_16));
            }
        } else if (matches) {
            if (addFromInviteLink == AccessControl.AccessRequired.ADMINISTRATOR) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_turned_off_admin_approval_for_the_group_link), R.drawable.ic_update_group_role_16));
            } else {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_turned_on_the_group_link_with_admin_approval_off), R.drawable.ic_update_group_role_16));
            }
        } else if (addFromInviteLink == AccessControl.AccessRequired.ADMINISTRATOR) {
            list.add(updateDescription(R.string.MessageRecord_s_turned_off_admin_approval_for_the_group_link, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_role_16));
        } else {
            list.add(updateDescription(R.string.MessageRecord_s_turned_on_the_group_link_with_admin_approval_off, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_role_16));
        }
        z = true;
        if (!z) {
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.GroupsV2UpdateMessageProducer$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired;

        static {
            int[] iArr = new int[AccessControl.AccessRequired.values().length];
            $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired = iArr;
            try {
                iArr[AccessControl.AccessRequired.ANY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[AccessControl.AccessRequired.ADMINISTRATOR.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[AccessControl.AccessRequired.UNSATISFIABLE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private void describeUnknownEditorNewGroupInviteLinkAccess(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        AccessControl.AccessRequired addFromInviteLink = decryptedGroup != null ? decryptedGroup.getAccessControl().getAddFromInviteLink() : null;
        int i = AnonymousClass1.$SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[decryptedGroupChange.getNewInviteLinkAccess().ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    list.add(updateDescription(this.context.getString(R.string.MessageRecord_the_group_link_has_been_turned_off), R.drawable.ic_update_group_role_16));
                }
            } else if (addFromInviteLink == AccessControl.AccessRequired.ANY) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_the_admin_approval_for_the_group_link_has_been_turned_on), R.drawable.ic_update_group_role_16));
            } else {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_the_group_link_has_been_turned_on_with_admin_approval_on), R.drawable.ic_update_group_role_16));
            }
        } else if (addFromInviteLink == AccessControl.AccessRequired.ADMINISTRATOR) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_the_admin_approval_for_the_group_link_has_been_turned_off), R.drawable.ic_update_group_role_16));
        } else {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_the_group_link_has_been_turned_on_with_admin_approval_off), R.drawable.ic_update_group_role_16));
        }
        if (decryptedGroupChange.getNewInviteLinkPassword().size() > 0) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_the_group_link_has_been_reset), R.drawable.ic_update_group_role_16));
        }
    }

    private void describeRequestingMembers(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        HashSet hashSet = new HashSet(decryptedGroupChange.getDeleteRequestingMembersList());
        for (DecryptedRequestingMember decryptedRequestingMember : decryptedGroupChange.getNewRequestingMembersList()) {
            if (this.selfIds.matches(decryptedRequestingMember.getUuid())) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_sent_a_request_to_join_the_group), R.drawable.ic_update_group_16));
            } else if (hashSet.contains(decryptedRequestingMember.getUuid())) {
                list.add(updateDescription(R.plurals.MessageRecord_s_requested_and_cancelled_their_request_to_join_via_the_group_link, decryptedGroupChange.getDeleteRequestingMembersCount(), decryptedRequestingMember.getUuid(), Integer.valueOf(decryptedGroupChange.getDeleteRequestingMembersCount()), R.drawable.ic_update_group_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_requested_to_join_via_the_group_link, decryptedRequestingMember.getUuid(), R.drawable.ic_update_group_16));
            }
        }
    }

    private void describeRequestingMembersApprovals(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        for (DecryptedApproveMember decryptedApproveMember : decryptedGroupChange.getPromoteRequestingMembersList()) {
            if (this.selfIds.matches(decryptedApproveMember.getUuid())) {
                list.add(updateDescription(R.string.MessageRecord_s_approved_your_request_to_join_the_group, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_accept_16));
            } else if (this.selfIds.matches(decryptedGroupChange.getEditor())) {
                list.add(updateDescription(R.string.MessageRecord_you_approved_a_request_to_join_the_group_from_s, decryptedApproveMember.getUuid(), R.drawable.ic_update_group_accept_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_approved_a_request_to_join_the_group_from_s, decryptedGroupChange.getEditor(), decryptedApproveMember.getUuid(), R.drawable.ic_update_group_accept_16));
            }
        }
    }

    private void describeUnknownEditorRequestingMembersApprovals(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        for (DecryptedApproveMember decryptedApproveMember : decryptedGroupChange.getPromoteRequestingMembersList()) {
            if (this.selfIds.matches(decryptedApproveMember.getUuid())) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_your_request_to_join_the_group_has_been_approved), R.drawable.ic_update_group_accept_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_a_request_to_join_the_group_from_s_has_been_approved, decryptedApproveMember.getUuid(), R.drawable.ic_update_group_accept_16));
            }
        }
    }

    private void describeRequestingMembersDeletes(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        Set set = (Set) Collection$EL.stream(decryptedGroupChange.getNewRequestingMembersList()).map(new Function() { // from class: org.thoughtcrime.securesms.database.model.GroupsV2UpdateMessageProducer$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((DecryptedRequestingMember) obj).getUuid();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toSet());
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        for (ByteString byteString : decryptedGroupChange.getDeleteRequestingMembersList()) {
            if (!set.contains(byteString)) {
                if (this.selfIds.matches(byteString)) {
                    if (matches) {
                        list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_canceled_your_request_to_join_the_group), R.drawable.ic_update_group_decline_16));
                    } else {
                        list.add(updateDescription(this.context.getString(R.string.MessageRecord_your_request_to_join_the_group_has_been_denied_by_an_admin), R.drawable.ic_update_group_decline_16));
                    }
                } else if (decryptedGroupChange.getEditor().equals(byteString)) {
                    list.add(updateDescription(R.string.MessageRecord_s_canceled_their_request_to_join_the_group, byteString, R.drawable.ic_update_group_decline_16));
                } else {
                    list.add(updateDescription(R.string.MessageRecord_s_denied_a_request_to_join_the_group_from_s, decryptedGroupChange.getEditor(), byteString, R.drawable.ic_update_group_decline_16));
                }
            }
        }
    }

    private void describeUnknownEditorRequestingMembersDeletes(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        for (ByteString byteString : decryptedGroupChange.getDeleteRequestingMembersList()) {
            if (this.selfIds.matches(byteString)) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_your_request_to_join_the_group_has_been_denied_by_an_admin), R.drawable.ic_update_group_decline_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_a_request_to_join_the_group_from_s_has_been_denied, byteString, R.drawable.ic_update_group_decline_16));
            }
        }
    }

    private void describeAnnouncementGroupChange(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        if (decryptedGroupChange.getNewIsAnnouncementGroup() == EnabledState.ENABLED) {
            if (matches) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_allow_only_admins_to_send), R.drawable.ic_update_group_role_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_allow_only_admins_to_send, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_role_16));
            }
        } else if (decryptedGroupChange.getNewIsAnnouncementGroup() != EnabledState.DISABLED) {
        } else {
            if (matches) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_allow_all_members_to_send), R.drawable.ic_update_group_role_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_allow_all_members_to_send, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_role_16));
            }
        }
    }

    private void describeUnknownEditorAnnouncementGroupChange(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        if (decryptedGroupChange.getNewIsAnnouncementGroup() == EnabledState.ENABLED) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_allow_only_admins_to_send), R.drawable.ic_update_group_role_16));
        } else if (decryptedGroupChange.getNewIsAnnouncementGroup() == EnabledState.DISABLED) {
            list.add(updateDescription(this.context.getString(R.string.MessageRecord_allow_all_members_to_send), R.drawable.ic_update_group_role_16));
        }
    }

    private void describePromotePendingPniAci(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        boolean matches = this.selfIds.matches(decryptedGroupChange.getEditor());
        for (DecryptedMember decryptedMember : decryptedGroupChange.getPromotePendingPniAciMembersList()) {
            ByteString uuid = decryptedMember.getUuid();
            boolean matches2 = this.selfIds.matches(uuid);
            if (matches) {
                if (matches2) {
                    list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_accepted_invite), R.drawable.ic_update_group_accept_16));
                } else {
                    list.add(updateDescription(R.string.MessageRecord_you_added_invited_member_s, uuid, R.drawable.ic_update_group_add_16));
                }
            } else if (matches2) {
                list.add(updateDescription(R.string.MessageRecord_s_added_you, decryptedGroupChange.getEditor(), R.drawable.ic_update_group_add_16));
            } else if (uuid.equals(decryptedGroupChange.getEditor())) {
                list.add(updateDescription(R.string.MessageRecord_s_accepted_invite, uuid, R.drawable.ic_update_group_accept_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_added_invited_member_s, decryptedGroupChange.getEditor(), uuid, R.drawable.ic_update_group_add_16));
            }
        }
    }

    private void describeUnknownEditorPromotePendingPniAci(DecryptedGroupChange decryptedGroupChange, List<UpdateDescription> list) {
        for (DecryptedMember decryptedMember : decryptedGroupChange.getPromotePendingPniAciMembersList()) {
            ByteString uuid = decryptedMember.getUuid();
            if (this.selfIds.matches(uuid)) {
                list.add(updateDescription(this.context.getString(R.string.MessageRecord_you_joined_the_group), R.drawable.ic_update_group_add_16));
            } else {
                list.add(updateDescription(R.string.MessageRecord_s_joined_the_group, uuid, R.drawable.ic_update_group_add_16));
            }
        }
    }

    private static UpdateDescription updateDescription(String str, int i) {
        return UpdateDescription.staticDescription(str, i);
    }

    private UpdateDescription updateDescription(int i, ByteString byteString, int i2) {
        ServiceId fromByteStringOrUnknown = ServiceId.fromByteStringOrUnknown(byteString);
        return UpdateDescription.mentioning(Collections.singletonList(fromByteStringOrUnknown), new UpdateDescription.SpannableFactory(RecipientId.from(fromByteStringOrUnknown), i) { // from class: org.thoughtcrime.securesms.database.model.GroupsV2UpdateMessageProducer$$ExternalSyntheticLambda5
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.database.model.UpdateDescription.SpannableFactory
            public final Spannable create() {
                return GroupsV2UpdateMessageProducer.$r8$lambda$f3x9NSacurSvNx87era6lPxcd1w(GroupsV2UpdateMessageProducer.this, this.f$1, this.f$2);
            }
        }, i2);
    }

    public /* synthetic */ Spannable lambda$updateDescription$0(RecipientId recipientId, int i) {
        List singletonList = Collections.singletonList(recipientId);
        return makeRecipientsClickable(this.context, this.context.getString(i, makePlaceholders(singletonList, null)), singletonList, this.recipientClickHandler);
    }

    private UpdateDescription updateDescription(int i, ByteString byteString, ByteString byteString2, int i2) {
        ServiceId fromByteStringOrUnknown = ServiceId.fromByteStringOrUnknown(byteString);
        ServiceId fromByteStringOrUnknown2 = ServiceId.fromByteStringOrUnknown(byteString2);
        return UpdateDescription.mentioning(Arrays.asList(fromByteStringOrUnknown, fromByteStringOrUnknown2), new UpdateDescription.SpannableFactory(RecipientId.from(fromByteStringOrUnknown), RecipientId.from(fromByteStringOrUnknown2), i) { // from class: org.thoughtcrime.securesms.database.model.GroupsV2UpdateMessageProducer$$ExternalSyntheticLambda0
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ RecipientId f$2;
            public final /* synthetic */ int f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.model.UpdateDescription.SpannableFactory
            public final Spannable create() {
                return GroupsV2UpdateMessageProducer.m1730$r8$lambda$KV1ZbwMn4N0jZDtKfcYhdWwfKY(GroupsV2UpdateMessageProducer.this, this.f$1, this.f$2, this.f$3);
            }
        }, i2);
    }

    public /* synthetic */ Spannable lambda$updateDescription$1(RecipientId recipientId, RecipientId recipientId2, int i) {
        List asList = Arrays.asList(recipientId, recipientId2);
        return makeRecipientsClickable(this.context, this.context.getString(i, makePlaceholders(asList, null)), asList, this.recipientClickHandler);
    }

    private UpdateDescription updateDescription(int i, ByteString byteString, Object obj, int i2) {
        ServiceId fromByteStringOrUnknown = ServiceId.fromByteStringOrUnknown(byteString);
        return UpdateDescription.mentioning(Collections.singletonList(fromByteStringOrUnknown), new UpdateDescription.SpannableFactory(RecipientId.from(fromByteStringOrUnknown), i, obj) { // from class: org.thoughtcrime.securesms.database.model.GroupsV2UpdateMessageProducer$$ExternalSyntheticLambda3
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ Object f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.model.UpdateDescription.SpannableFactory
            public final Spannable create() {
                return GroupsV2UpdateMessageProducer.$r8$lambda$9i9VWM21JtkxTcz9NLpavRSD_LA(GroupsV2UpdateMessageProducer.this, this.f$1, this.f$2, this.f$3);
            }
        }, i2);
    }

    public /* synthetic */ Spannable lambda$updateDescription$2(RecipientId recipientId, int i, Object obj) {
        List singletonList = Collections.singletonList(recipientId);
        return makeRecipientsClickable(this.context, this.context.getString(i, makePlaceholders(singletonList, Collections.singletonList(obj))), singletonList, this.recipientClickHandler);
    }

    private UpdateDescription updateDescription(int i, int i2, ByteString byteString, Object obj, int i3) {
        ServiceId fromByteStringOrUnknown = ServiceId.fromByteStringOrUnknown(byteString);
        return UpdateDescription.mentioning(Collections.singletonList(fromByteStringOrUnknown), new UpdateDescription.SpannableFactory(RecipientId.from(fromByteStringOrUnknown), i, i2, obj) { // from class: org.thoughtcrime.securesms.database.model.GroupsV2UpdateMessageProducer$$ExternalSyntheticLambda4
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ int f$3;
            public final /* synthetic */ Object f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // org.thoughtcrime.securesms.database.model.UpdateDescription.SpannableFactory
            public final Spannable create() {
                return GroupsV2UpdateMessageProducer.$r8$lambda$nXag5eT60gmjPgjiM46utfkBuXg(GroupsV2UpdateMessageProducer.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        }, i3);
    }

    public /* synthetic */ Spannable lambda$updateDescription$3(RecipientId recipientId, int i, int i2, Object obj) {
        List singletonList = Collections.singletonList(recipientId);
        return makeRecipientsClickable(this.context, this.context.getResources().getQuantityString(i, i2, makePlaceholders(singletonList, Collections.singletonList(obj))), singletonList, this.recipientClickHandler);
    }

    private static Object[] makePlaceholders(List<RecipientId> list, List<Object> list2) {
        List list3 = (List) Collection$EL.stream(list).map(new Function() { // from class: org.thoughtcrime.securesms.database.model.GroupsV2UpdateMessageProducer$$ExternalSyntheticLambda6
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return GroupsV2UpdateMessageProducer.makePlaceholder((RecipientId) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
        if (list2 != null) {
            list3.addAll(list2);
        }
        return list3.toArray();
    }

    static Spannable makeRecipientsClickable(Context context, String str, List<RecipientId> list, Consumer<RecipientId> consumer) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        HashMap hashMap = new HashMap();
        for (RecipientId recipientId : list) {
            hashMap.put(makePlaceholder(recipientId), recipientId);
        }
        int i = 0;
        while (i < str.length()) {
            Map.Entry entry = null;
            int i2 = Integer.MAX_VALUE;
            for (Map.Entry entry2 : hashMap.entrySet()) {
                int indexOf = str.indexOf((String) entry2.getKey(), i);
                if (indexOf >= 0 && indexOf < i2) {
                    entry = entry2;
                    i2 = indexOf;
                }
            }
            if (entry != null) {
                RecipientId recipientId2 = (RecipientId) entry.getValue();
                spannableStringBuilder.append((CharSequence) str.substring(i, i2));
                spannableStringBuilder.append(SpanUtil.clickable(Recipient.resolved(recipientId2).getDisplayName(context), ContextCompat.getColor(context, R.color.conversation_item_update_text_color), new View.OnClickListener(consumer) { // from class: org.thoughtcrime.securesms.database.model.GroupsV2UpdateMessageProducer$$ExternalSyntheticLambda1
                    public final /* synthetic */ Consumer f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        GroupsV2UpdateMessageProducer.m1731$r8$lambda$qKBtFjEB48Q0897khlXXcI4xE4(RecipientId.this, this.f$1, view);
                    }
                }));
                i = i2 + ((String) entry.getKey()).length();
            } else {
                spannableStringBuilder.append((CharSequence) str.substring(i));
                i = str.length();
            }
        }
        return spannableStringBuilder;
    }

    public static /* synthetic */ void lambda$makeRecipientsClickable$4(RecipientId recipientId, Consumer consumer, View view) {
        if (!recipientId.isUnknown() && consumer != null) {
            consumer.accept(recipientId);
        }
    }

    public static String makePlaceholder(RecipientId recipientId) {
        return "{{SPAN_PLACEHOLDER_" + recipientId + "}}";
    }
}
