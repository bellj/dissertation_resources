package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MergeCursor;
import android.net.Uri;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.fasterxml.jackson.annotation.JsonProperty;
import j$.util.Optional;
import j$.util.function.Function;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function2;
import org.jsoup.helper.StringUtil;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.mms.StickerSlide;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientDetails;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.storage.SignalAccountRecord;
import org.whispersystems.signalservice.api.storage.SignalContactRecord;
import org.whispersystems.signalservice.api.storage.SignalGroupV1Record;
import org.whispersystems.signalservice.api.storage.SignalGroupV2Record;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;
import org.whispersystems.signalservice.api.util.OptionalUtil;

/* loaded from: classes4.dex */
public class ThreadDatabase extends Database {
    public static final String ARCHIVED;
    private static final List<String> COMBINED_THREAD_RECIPIENT_GROUP_PROJECTION;
    public static final String[] CREATE_INDEXS = {"CREATE INDEX IF NOT EXISTS thread_recipient_id_index ON thread (thread_recipient_id);", "CREATE INDEX IF NOT EXISTS archived_count_index ON thread (archived, message_count);", "CREATE INDEX IF NOT EXISTS thread_pinned_index ON thread (pinned);"};
    public static final String CREATE_TABLE = ("CREATE TABLE thread (_id INTEGER PRIMARY KEY AUTOINCREMENT, date INTEGER DEFAULT 0, message_count INTEGER DEFAULT 0, thread_recipient_id INTEGER, snippet TEXT, snippet_charset INTEGER DEFAULT 0, read INTEGER DEFAULT " + ReadStatus.READ.serialize() + ", type INTEGER DEFAULT 0, " + ERROR + " INTEGER DEFAULT 0, " + SNIPPET_TYPE + " INTEGER DEFAULT 0, " + SNIPPET_URI + " TEXT DEFAULT NULL, " + SNIPPET_CONTENT_TYPE + " TEXT DEFAULT NULL, " + SNIPPET_EXTRAS + " TEXT DEFAULT NULL, " + ARCHIVED + " INTEGER DEFAULT 0, status INTEGER DEFAULT 0, delivery_receipt_count INTEGER DEFAULT 0, expires_in INTEGER DEFAULT 0, " + LAST_SEEN + " INTEGER DEFAULT 0, " + HAS_SENT + " INTEGER DEFAULT 0, read_receipt_count INTEGER DEFAULT 0, " + UNREAD_COUNT + " INTEGER DEFAULT 0, " + LAST_SCROLLED + " INTEGER DEFAULT 0, " + PINNED + " INTEGER DEFAULT 0);");
    public static final String DATE;
    public static final String DELIVERY_RECEIPT_COUNT;
    private static final String ERROR;
    public static final String EXPIRES_IN;
    public static final String HAS_SENT;
    public static final String ID;
    private static final String LAST_SCROLLED;
    public static final String LAST_SEEN;
    public static final String MEANINGFUL_MESSAGES;
    public static final long NO_TRIM_BEFORE_DATE_SET;
    public static final int NO_TRIM_MESSAGE_COUNT_SET;
    static final String PINNED;
    public static final String READ;
    public static final String READ_RECEIPT_COUNT;
    public static final String RECIPIENT_ID;
    private static final String[] RECIPIENT_ID_PROJECTION = {RECIPIENT_ID};
    public static final String SNIPPET;
    private static final String SNIPPET_CHARSET;
    public static final String SNIPPET_CONTENT_TYPE;
    public static final String SNIPPET_EXTRAS;
    public static final String SNIPPET_TYPE;
    public static final String SNIPPET_URI;
    public static final String STATUS;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(ThreadDatabase.class);
    private static final String[] THREAD_PROJECTION;
    public static final String TYPE;
    private static final List<String> TYPED_THREAD_PROJECTION;
    public static final String UNREAD_COUNT;

    /* loaded from: classes4.dex */
    public static class DistributionTypes {
        public static final int ARCHIVE;
        public static final int BROADCAST;
        public static final int CONVERSATION;
        public static final int DEFAULT;
        public static final int INBOX_ZERO;
    }

    public static /* synthetic */ Long $r8$lambda$wdvfXXjZijlMvtEY2iU3JPtU_GE(Long l) {
        return -1L;
    }

    public static /* synthetic */ Long lambda$setRead$1(Long l) {
        return l;
    }

    public static /* synthetic */ String lambda$unpinConversations$5(Long l) {
        return "?";
    }

    static {
        TAG = Log.tag(ThreadDatabase.class);
        CREATE_TABLE = "CREATE TABLE thread (_id INTEGER PRIMARY KEY AUTOINCREMENT, date INTEGER DEFAULT 0, message_count INTEGER DEFAULT 0, thread_recipient_id INTEGER, snippet TEXT, snippet_charset INTEGER DEFAULT 0, read INTEGER DEFAULT " + ReadStatus.READ.serialize() + ", type INTEGER DEFAULT 0, " + ERROR + " INTEGER DEFAULT 0, " + SNIPPET_TYPE + " INTEGER DEFAULT 0, " + SNIPPET_URI + " TEXT DEFAULT NULL, " + SNIPPET_CONTENT_TYPE + " TEXT DEFAULT NULL, " + SNIPPET_EXTRAS + " TEXT DEFAULT NULL, " + ARCHIVED + " INTEGER DEFAULT 0, status INTEGER DEFAULT 0, delivery_receipt_count INTEGER DEFAULT 0, expires_in INTEGER DEFAULT 0, " + LAST_SEEN + " INTEGER DEFAULT 0, " + HAS_SENT + " INTEGER DEFAULT 0, read_receipt_count INTEGER DEFAULT 0, " + UNREAD_COUNT + " INTEGER DEFAULT 0, " + LAST_SCROLLED + " INTEGER DEFAULT 0, " + PINNED + " INTEGER DEFAULT 0);";
        CREATE_INDEXS = new String[]{"CREATE INDEX IF NOT EXISTS thread_recipient_id_index ON thread (thread_recipient_id);", "CREATE INDEX IF NOT EXISTS archived_count_index ON thread (archived, message_count);", "CREATE INDEX IF NOT EXISTS thread_pinned_index ON thread (pinned);"};
        String[] strArr = {"_id", "date", MEANINGFUL_MESSAGES, RECIPIENT_ID, "snippet", SNIPPET_CHARSET, "read", UNREAD_COUNT, "type", ERROR, SNIPPET_TYPE, SNIPPET_URI, SNIPPET_CONTENT_TYPE, SNIPPET_EXTRAS, ARCHIVED, "status", "delivery_receipt_count", "expires_in", LAST_SEEN, "read_receipt_count", LAST_SCROLLED, PINNED};
        THREAD_PROJECTION = strArr;
        List<String> list = Stream.of(strArr).map(new Function() { // from class: org.thoughtcrime.securesms.database.ThreadDatabase$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ThreadDatabase.lambda$static$0((String) obj);
            }
        }).toList();
        TYPED_THREAD_PROJECTION = list;
        COMBINED_THREAD_RECIPIENT_GROUP_PROJECTION = Stream.concat(Stream.concat(Stream.of(list), Stream.of(RecipientDatabase.TYPED_RECIPIENT_PROJECTION_NO_ID)), Stream.of(GroupDatabase.TYPED_GROUP_PROJECTION)).toList();
        RECIPIENT_ID_PROJECTION = new String[]{RECIPIENT_ID};
    }

    public static /* synthetic */ String lambda$static$0(String str) {
        return "thread." + str;
    }

    public ThreadDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    private long createThreadForRecipient(RecipientId recipientId, boolean z, int i) {
        if (!recipientId.isUnknown()) {
            ContentValues contentValues = new ContentValues(4);
            long currentTimeMillis = System.currentTimeMillis();
            contentValues.put("date", Long.valueOf(currentTimeMillis - (currentTimeMillis % 1000)));
            contentValues.put(RECIPIENT_ID, recipientId.serialize());
            if (z) {
                contentValues.put("type", Integer.valueOf(i));
            }
            contentValues.put(MEANINGFUL_MESSAGES, (Integer) 0);
            long insert = this.databaseHelper.getSignalWritableDatabase().insert(TABLE_NAME, (String) null, contentValues);
            Recipient.live(recipientId).refresh();
            return insert;
        }
        throw new AssertionError("Cannot create a thread for an unknown recipient!");
    }

    private void updateThread(long j, boolean z, String str, Uri uri, String str2, Extra extra, long j2, int i, int i2, long j3, boolean z2, long j4, int i3) {
        String json;
        String str3 = null;
        if (extra != null) {
            try {
                json = JsonUtils.toJson(extra);
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        } else {
            json = null;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", Long.valueOf(j2 - (j2 % 1000)));
        contentValues.put("snippet", str);
        if (uri != null) {
            str3 = uri.toString();
        }
        contentValues.put(SNIPPET_URI, str3);
        contentValues.put(SNIPPET_TYPE, Long.valueOf(j3));
        contentValues.put(SNIPPET_CONTENT_TYPE, str2);
        contentValues.put(SNIPPET_EXTRAS, json);
        contentValues.put(MEANINGFUL_MESSAGES, Integer.valueOf(z ? 1 : 0));
        contentValues.put("status", Integer.valueOf(i));
        contentValues.put("delivery_receipt_count", Integer.valueOf(i2));
        contentValues.put("read_receipt_count", Integer.valueOf(i3));
        contentValues.put("expires_in", Long.valueOf(j4));
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(j));
        if (z2) {
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put(ARCHIVED, (Integer) 0);
            SqlUtil.Query buildTrueUpdateQuery = SqlUtil.buildTrueUpdateQuery("_id = ?", SqlUtil.buildArgs(j), contentValues2);
            if (signalWritableDatabase.update(TABLE_NAME, contentValues2, buildTrueUpdateQuery.getWhere(), buildTrueUpdateQuery.getWhereArgs()) > 0) {
                StorageSyncHelper.scheduleSyncForDataChange();
            }
        }
    }

    public void updateSnippetUriSilently(long j, Uri uri) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SNIPPET_URI, uri != null ? uri.toString() : null);
        this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(j));
    }

    public void updateSnippet(long j, String str, Uri uri, long j2, long j3, boolean z) {
        String str2;
        if (!isSilentType(j3)) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("date", Long.valueOf(j2 - (j2 % 1000)));
            contentValues.put("snippet", str);
            contentValues.put(SNIPPET_TYPE, Long.valueOf(j3));
            if (uri == null) {
                str2 = null;
            } else {
                str2 = uri.toString();
            }
            contentValues.put(SNIPPET_URI, str2);
            if (z) {
                contentValues.put(ARCHIVED, (Integer) 0);
            }
            this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(j));
            notifyConversationListListeners();
        }
    }

    public void trimAllThreads(int i, long j) {
        if (i != Integer.MAX_VALUE || j != 0) {
            SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
            AttachmentDatabase attachments = SignalDatabase.attachments();
            GroupReceiptDatabase groupReceipts = SignalDatabase.groupReceipts();
            MmsSmsDatabase mmsSms = SignalDatabase.mmsSms();
            MentionDatabase mentions = SignalDatabase.mentions();
            String str = "_id";
            Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{str}, null, null, null, null, null);
            while (query != null) {
                try {
                    if (!query.moveToNext()) {
                        break;
                    }
                    trimThreadInternal(CursorUtil.requireLong(query, str), i, j);
                    str = str;
                } catch (Throwable th) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
            signalWritableDatabase.beginTransaction();
            try {
                mmsSms.deleteAbandonedMessages();
                attachments.trimAllAbandonedAttachments();
                groupReceipts.deleteAbandonedRows();
                mentions.deleteAbandonedMentions();
                int deleteAbandonedAttachmentFiles = attachments.deleteAbandonedAttachmentFiles();
                signalWritableDatabase.setTransactionSuccessful();
                if (deleteAbandonedAttachmentFiles > 0) {
                    String str2 = TAG;
                    Log.i(str2, "Trim all threads caused " + deleteAbandonedAttachmentFiles + " attachments to be deleted.");
                }
                notifyAttachmentListeners();
                notifyStickerPackListeners();
            } finally {
                signalWritableDatabase.endTransaction();
            }
        }
    }

    public void trimThread(long j, int i, long j2) {
        if (i != Integer.MAX_VALUE || j2 != 0) {
            SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
            AttachmentDatabase attachments = SignalDatabase.attachments();
            GroupReceiptDatabase groupReceipts = SignalDatabase.groupReceipts();
            MmsSmsDatabase mmsSms = SignalDatabase.mmsSms();
            MentionDatabase mentions = SignalDatabase.mentions();
            signalWritableDatabase.beginTransaction();
            try {
                trimThreadInternal(j, i, j2);
                mmsSms.deleteAbandonedMessages();
                attachments.trimAllAbandonedAttachments();
                groupReceipts.deleteAbandonedRows();
                mentions.deleteAbandonedMentions();
                int deleteAbandonedAttachmentFiles = attachments.deleteAbandonedAttachmentFiles();
                signalWritableDatabase.setTransactionSuccessful();
                if (deleteAbandonedAttachmentFiles > 0) {
                    String str = TAG;
                    Log.i(str, "Trim thread " + j + " caused " + deleteAbandonedAttachmentFiles + " attachments to be deleted.");
                }
                notifyAttachmentListeners();
                notifyStickerPackListeners();
            } finally {
                signalWritableDatabase.endTransaction();
            }
        }
    }

    private void trimThreadInternal(long j, int i, long j2) {
        if (i != Integer.MAX_VALUE || j2 != 0) {
            if (i != Integer.MAX_VALUE) {
                Cursor conversation = SignalDatabase.mmsSms().getConversation(j);
                if (conversation != null && i > 0) {
                    try {
                        if (conversation.getCount() > i) {
                            conversation.moveToPosition(i - 1);
                            j2 = Math.max(j2, conversation.getLong(conversation.getColumnIndexOrThrow(MmsSmsColumns.NORMALIZED_DATE_RECEIVED)));
                        }
                    } catch (Throwable th) {
                        try {
                            conversation.close();
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                        }
                        throw th;
                    }
                }
                if (conversation != null) {
                    conversation.close();
                }
            }
            if (j2 != 0) {
                String str = TAG;
                Log.i(str, "Trimming thread: " + j + " before: " + j2);
                int deleteMessagesInThreadBeforeDate = SignalDatabase.mmsSms().deleteMessagesInThreadBeforeDate(j, j2);
                if (deleteMessagesInThreadBeforeDate > 0) {
                    Log.i(str, "Trimming deleted " + deleteMessagesInThreadBeforeDate + " messages thread: " + j);
                    setLastScrolled(j, 0);
                    update(j, false);
                    notifyConversationListeners(j);
                    return;
                }
                Log.i(str, "Trimming deleted no messages thread: " + j);
            }
        }
    }

    public List<MessageDatabase.MarkedMessageInfo> setAllThreadsRead() {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("read", Integer.valueOf(ReadStatus.READ.serialize()));
        contentValues.put(UNREAD_COUNT, (Integer) 0);
        signalWritableDatabase.update(TABLE_NAME, contentValues, null, null);
        List<MessageDatabase.MarkedMessageInfo> allMessagesRead = SignalDatabase.sms().setAllMessagesRead();
        List<MessageDatabase.MarkedMessageInfo> allMessagesRead2 = SignalDatabase.mms().setAllMessagesRead();
        SignalDatabase.sms().setAllReactionsSeen();
        SignalDatabase.mms().setAllReactionsSeen();
        notifyConversationListListeners();
        return Util.concatenatedList(allMessagesRead, allMessagesRead2);
    }

    public boolean hasCalledSince(Recipient recipient, long j) {
        return hasReceivedAnyCallsSince(getOrCreateThreadIdFor(recipient), j);
    }

    public boolean hasReceivedAnyCallsSince(long j, long j2) {
        return SignalDatabase.mmsSms().hasReceivedAnyCallsSince(j, j2);
    }

    public List<MessageDatabase.MarkedMessageInfo> setEntireThreadRead(long j) {
        setRead(j, false);
        return Util.concatenatedList(SignalDatabase.sms().setEntireThreadRead(j), SignalDatabase.mms().setEntireThreadRead(j));
    }

    public List<MessageDatabase.MarkedMessageInfo> setRead(long j, boolean z) {
        return setReadSince(Collections.singletonMap(Long.valueOf(j), -1L), z);
    }

    public List<MessageDatabase.MarkedMessageInfo> setRead(ConversationId conversationId, boolean z) {
        if (conversationId.getGroupStoryId() == null) {
            return setRead(conversationId.getThreadId(), z);
        }
        return setGroupStoryReadSince(conversationId.getThreadId(), conversationId.getGroupStoryId().longValue(), System.currentTimeMillis());
    }

    public List<MessageDatabase.MarkedMessageInfo> setReadSince(ConversationId conversationId, boolean z, long j) {
        if (conversationId.getGroupStoryId() != null) {
            return setGroupStoryReadSince(conversationId.getThreadId(), conversationId.getGroupStoryId().longValue(), j);
        }
        return setReadSince(conversationId.getThreadId(), z, j);
    }

    public List<MessageDatabase.MarkedMessageInfo> setReadSince(long j, boolean z, long j2) {
        return setReadSince(Collections.singletonMap(Long.valueOf(j), Long.valueOf(j2)), z);
    }

    public List<MessageDatabase.MarkedMessageInfo> setRead(Collection<Long> collection, boolean z) {
        return setReadSince((Map) Stream.of(collection).collect(Collectors.toMap(new Function() { // from class: org.thoughtcrime.securesms.database.ThreadDatabase$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ThreadDatabase.lambda$setRead$1((Long) obj);
            }
        }, new Function() { // from class: org.thoughtcrime.securesms.database.ThreadDatabase$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ThreadDatabase.$r8$lambda$wdvfXXjZijlMvtEY2iU3JPtU_GE((Long) obj);
            }
        })), z);
    }

    public List<MessageDatabase.MarkedMessageInfo> setGroupStoryReadSince(long j, long j2, long j3) {
        return SignalDatabase.mms().setGroupStoryMessagesReadSince(j, j2, j3);
    }

    /* JADX INFO: finally extract failed */
    public List<MessageDatabase.MarkedMessageInfo> setReadSince(Map<Long, Long> map, boolean z) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        LinkedList linkedList = new LinkedList();
        LinkedList linkedList2 = new LinkedList();
        signalWritableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues(2);
            contentValues.put("read", Integer.valueOf(ReadStatus.READ.serialize()));
            boolean z2 = false;
            for (Map.Entry<Long, Long> entry : map.entrySet()) {
                long longValue = entry.getKey().longValue();
                long longValue2 = entry.getValue().longValue();
                if (z) {
                    contentValues.put(LAST_SEEN, Long.valueOf(longValue2 == -1 ? System.currentTimeMillis() : longValue2));
                }
                ThreadRecord threadRecord = getThreadRecord(Long.valueOf(longValue));
                linkedList.addAll(SignalDatabase.sms().setMessagesReadSince(longValue, longValue2));
                linkedList2.addAll(SignalDatabase.mms().setMessagesReadSince(longValue, longValue2));
                SignalDatabase.sms().setReactionsSeen(longValue, longValue2);
                SignalDatabase.mms().setReactionsSeen(longValue, longValue2);
                contentValues.put(UNREAD_COUNT, Integer.valueOf(SignalDatabase.mmsSms().getUnreadCount(longValue)));
                signalWritableDatabase.update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(longValue));
                if (threadRecord != null && threadRecord.isForcedUnread()) {
                    SignalDatabase.recipients().markNeedsSync(threadRecord.getRecipient().getId());
                    z2 = true;
                }
            }
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            notifyVerboseConversationListeners(map.keySet());
            notifyConversationListListeners();
            if (z2) {
                StorageSyncHelper.scheduleSyncForDataChange();
            }
            return Util.concatenatedList(linkedList, linkedList2);
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    public void setForcedUnread(Collection<Long> collection) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        List<RecipientId> emptyList = Collections.emptyList();
        signalWritableDatabase.beginTransaction();
        try {
            SqlUtil.Query buildSingleCollectionQuery = SqlUtil.buildSingleCollectionQuery("_id", collection);
            ContentValues contentValues = new ContentValues();
            contentValues.put("read", Integer.valueOf(ReadStatus.FORCED_UNREAD.serialize()));
            signalWritableDatabase.update(TABLE_NAME, contentValues, buildSingleCollectionQuery.getWhere(), buildSingleCollectionQuery.getWhereArgs());
            emptyList = getRecipientIdsForThreadIds(collection);
            SignalDatabase.recipients().markNeedsSyncWithoutRefresh(emptyList);
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
            for (RecipientId next : emptyList) {
                Recipient.live(next).refresh();
            }
            StorageSyncHelper.scheduleSyncForDataChange();
            notifyConversationListListeners();
        }
    }

    public static /* synthetic */ Long lambda$getUnreadThreadCount$3(Cursor cursor) {
        Objects.requireNonNull(cursor);
        return (Long) CursorUtil.getAggregateOrDefault(cursor, 0L, new j$.util.function.Function(cursor) { // from class: org.thoughtcrime.securesms.database.ThreadDatabase$$ExternalSyntheticLambda2
            public final /* synthetic */ Cursor f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Long.valueOf(this.f$0.getLong(((Integer) obj).intValue()));
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public Long getUnreadThreadCount() {
        return (Long) getUnreadThreadIdAggregate(SqlUtil.COUNT, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.database.ThreadDatabase$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ThreadDatabase.lambda$getUnreadThreadCount$3((Cursor) obj);
            }
        });
    }

    public long getUnreadMessageCount(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, SqlUtil.buildArgs(UNREAD_COUNT), "_id = ?", SqlUtil.buildArgs(j), null, null, null);
        try {
            if (query.moveToFirst()) {
                long requireLong = CursorUtil.requireLong(query, UNREAD_COUNT);
                query.close();
                return requireLong;
            }
            query.close();
            return 0;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public String getUnreadThreadIdList() {
        return (String) getUnreadThreadIdAggregate(SqlUtil.buildArgs("GROUP_CONCAT(_id)"), new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.database.ThreadDatabase$$ExternalSyntheticLambda3
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ThreadDatabase.lambda$getUnreadThreadIdList$4((Cursor) obj);
            }
        });
    }

    public static /* synthetic */ String lambda$getUnreadThreadIdList$4(Cursor cursor) {
        Objects.requireNonNull(cursor);
        return (String) CursorUtil.getAggregateOrDefault(cursor, null, new j$.util.function.Function(cursor) { // from class: org.thoughtcrime.securesms.database.ThreadDatabase$$ExternalSyntheticLambda6
            public final /* synthetic */ Cursor f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return this.f$0.getString(((Integer) obj).intValue());
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    private <T> T getUnreadThreadIdAggregate(String[] strArr, androidx.arch.core.util.Function<Cursor, T> function) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, strArr, "read != " + ReadStatus.READ.serialize() + " AND " + ARCHIVED + " = 0 AND " + MEANINGFUL_MESSAGES + " != 0", null, null, null, null);
        try {
            T apply = function.apply(query);
            if (query != null) {
                query.close();
            }
            return apply;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public void incrementUnread(long j, int i) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.execSQL("UPDATE thread SET read = " + ReadStatus.UNREAD.serialize() + ", " + UNREAD_COUNT + " = " + UNREAD_COUNT + " + ?, " + LAST_SCROLLED + " = ? WHERE _id = ?", SqlUtil.buildArgs(Integer.valueOf(i), 0, Long.valueOf(j)));
    }

    public void setDistributionType(long j, int i) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("type", Integer.valueOf(i));
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.update(TABLE_NAME, contentValues, "_id = ?", new String[]{j + ""});
        notifyConversationListListeners();
    }

    public int getDistributionType(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{"type"}, "_id = ?", new String[]{String.valueOf(j)}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    return query.getInt(query.getColumnIndexOrThrow("type"));
                }
            } finally {
                query.close();
            }
        }
        if (query != null) {
        }
        return 2;
    }

    public Cursor getFilteredConversationList(List<RecipientId> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        List partition = Util.partition(list, 900);
        LinkedList linkedList = new LinkedList();
        Iterator it = partition.iterator();
        while (true) {
            int i = 0;
            if (!it.hasNext()) {
                break;
            }
            List<RecipientId> list2 = (List) it.next();
            String[] strArr = new String[list2.size()];
            String str = "thread.thread_recipient_id = ?";
            for (int i2 = 0; i2 < list2.size() - 1; i2++) {
                str = str + " OR thread.thread_recipient_id = ?";
            }
            for (RecipientId recipientId : list2) {
                strArr[i] = recipientId.serialize();
                i++;
            }
            linkedList.add(signalReadableDatabase.rawQuery(createQuery(str, 0), strArr));
        }
        return linkedList.size() > 1 ? new MergeCursor((Cursor[]) linkedList.toArray(new Cursor[linkedList.size()])) : (Cursor) linkedList.get(0);
    }

    public Cursor getRecentConversationList(int i, boolean z, boolean z2) {
        return getRecentConversationList(i, z, false, false, z2, false, false);
    }

    public Cursor getRecentConversationList(int i, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String str = !z ? "message_count != 0 AND (groups.active IS NULL OR groups.active = 1)" : "message_count != 0";
        if (z3) {
            str = str + " AND recipient.group_id NOT NULL";
        }
        if (z2) {
            str = str + " AND recipient.group_id IS NULL";
        }
        if (z4) {
            str = str + " AND recipient.group_type != " + RecipientDatabase.GroupType.SIGNAL_V1.getId();
        }
        if (z5) {
            str = (str + " AND ((recipient.group_id NOT NULL AND recipient.group_type != " + RecipientDatabase.GroupType.MMS.getId() + ") OR " + RecipientDatabase.TABLE_NAME + "." + RecipientDatabase.REGISTERED + " = " + RecipientDatabase.RegisteredState.REGISTERED.getId() + ")") + " AND recipient.force_sms_selection = 0";
        }
        if (z6) {
            str = str + " AND thread_recipient_id != " + Recipient.self().getId().toLong();
        }
        String str2 = (str + " AND archived = 0") + " AND recipient.blocked = 0";
        if (SignalStore.releaseChannelValues().getReleaseChannelRecipientId() != null) {
            str2 = str2 + " AND thread_recipient_id != " + SignalStore.releaseChannelValues().getReleaseChannelRecipientId().toLong();
        }
        return signalReadableDatabase.rawQuery(createQuery(str2, 0L, (long) i, true), (String[]) null);
    }

    public Cursor getRecentPushConversationList(int i, boolean z) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String str = !z ? " AND groups.active = 1" : "";
        return signalReadableDatabase.rawQuery(createQuery("message_count != 0 AND (registered = " + RecipientDatabase.RegisteredState.REGISTERED.getId() + " OR (" + ChooseGroupStoryBottomSheet.RESULT_SET + ".group_id NOT NULL AND " + ChooseGroupStoryBottomSheet.RESULT_SET + ".mms = 0" + str + "))", 0L, (long) i, true), (String[]) null);
    }

    public List<ThreadRecord> getRecentV1Groups(int i) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String createQuery = createQuery("message_count != 0 AND (groups.active = 1 AND groups.master_key IS NULL AND groups.mms = 0)", 0L, (long) i, true);
        ArrayList arrayList = new ArrayList();
        Reader readerFor = readerFor(signalReadableDatabase.rawQuery(createQuery, (String[]) null));
        while (true) {
            try {
                ThreadRecord next = readerFor.getNext();
                if (next != null) {
                    arrayList.add(next);
                } else {
                    readerFor.close();
                    return arrayList;
                }
            } catch (Throwable th) {
                if (readerFor != null) {
                    try {
                        readerFor.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
    }

    public Cursor getArchivedConversationList() {
        return getConversationList(SubscriptionLevels.BOOST_LEVEL);
    }

    public boolean isArchived(RecipientId recipientId) {
        boolean z = true;
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{ARCHIVED}, "thread_recipient_id = ?", new String[]{recipientId.serialize()}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    if (query.getInt(query.getColumnIndexOrThrow(ARCHIVED)) != 1) {
                        z = false;
                    }
                    query.close();
                    return z;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return false;
    }

    public void setArchived(Set<Long> set, boolean z) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        List<RecipientId> emptyList = Collections.emptyList();
        signalReadableDatabase.beginTransaction();
        try {
            for (Long l : set) {
                long longValue = l.longValue();
                ContentValues contentValues = new ContentValues(2);
                String str = "0";
                if (z) {
                    contentValues.put(PINNED, str);
                }
                if (z) {
                    str = SubscriptionLevels.BOOST_LEVEL;
                }
                contentValues.put(ARCHIVED, str);
                signalReadableDatabase.update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(longValue));
            }
            emptyList = getRecipientIdsForThreadIds(set);
            SignalDatabase.recipients().markNeedsSyncWithoutRefresh(emptyList);
            signalReadableDatabase.setTransactionSuccessful();
        } finally {
            signalReadableDatabase.endTransaction();
            for (RecipientId next : emptyList) {
                Recipient.live(next).refresh();
            }
            notifyConversationListListeners();
            StorageSyncHelper.scheduleSyncForDataChange();
        }
    }

    public Set<RecipientId> getArchivedRecipients() {
        HashSet hashSet = new HashSet();
        Cursor archivedConversationList = getArchivedConversationList();
        while (archivedConversationList != null) {
            try {
                if (!archivedConversationList.moveToNext()) {
                    break;
                }
                hashSet.add(RecipientId.from(archivedConversationList.getLong(archivedConversationList.getColumnIndexOrThrow(RECIPIENT_ID))));
            } catch (Throwable th) {
                try {
                    archivedConversationList.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (archivedConversationList != null) {
            archivedConversationList.close();
        }
        return hashSet;
    }

    public Map<RecipientId, Integer> getInboxPositions() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String createQuery = createQuery("message_count != ?", 0);
        HashMap hashMap = new HashMap();
        Cursor rawQuery = signalReadableDatabase.rawQuery(createQuery, new String[]{"0"});
        int i = 0;
        while (rawQuery != null) {
            try {
                if (!rawQuery.moveToNext()) {
                    break;
                }
                hashMap.put(RecipientId.from(rawQuery.getLong(rawQuery.getColumnIndexOrThrow(RECIPIENT_ID))), Integer.valueOf(i));
                i++;
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        return hashMap;
    }

    public Cursor getArchivedConversationList(long j, long j2) {
        return getConversationList(SubscriptionLevels.BOOST_LEVEL, j, j2);
    }

    private Cursor getConversationList(String str) {
        return getConversationList(str, 0, 0);
    }

    public Cursor getUnarchivedConversationList(boolean z, long j, long j2) {
        String str;
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        StringBuilder sb = new StringBuilder();
        sb.append(PINNED);
        sb.append(z ? " != 0" : " = 0");
        String sb2 = sb.toString();
        String str2 = "archived = 0 AND " + (z ? "" : "message_count != 0 AND ") + sb2;
        if (z) {
            str = createQuery(str2, "pinned ASC", j, j2);
        } else {
            str = createQuery(str2, j, j2, false);
        }
        return signalReadableDatabase.rawQuery(str, new String[0]);
    }

    private Cursor getConversationList(String str, long j, long j2) {
        return this.databaseHelper.getSignalReadableDatabase().rawQuery(createQuery("archived = ? AND message_count != 0", j, j2, false), new String[]{str});
    }

    public int getArchivedConversationListCount() {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{"COUNT(*)"}, "archived = ? AND message_count != 0", new String[]{SubscriptionLevels.BOOST_LEVEL}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    public int getPinnedConversationListCount() {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{"COUNT(*)"}, "archived = 0 AND pinned != 0", null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    public int getUnarchivedConversationListCount() {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{"COUNT(*)"}, "archived = 0 AND (message_count != 0 OR pinned != 0)", null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    public List<RecipientId> getPinnedRecipientIds() {
        String[] strArr = {"_id", RECIPIENT_ID};
        LinkedList linkedList = new LinkedList();
        Cursor pinned = getPinned(strArr);
        while (pinned.moveToNext()) {
            try {
                linkedList.add(RecipientId.from(CursorUtil.requireLong(pinned, RECIPIENT_ID)));
            } catch (Throwable th) {
                if (pinned != null) {
                    try {
                        pinned.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        pinned.close();
        return linkedList;
    }

    public List<Long> getPinnedThreadIds() {
        LinkedList linkedList = new LinkedList();
        Cursor pinned = getPinned(new String[]{"_id"});
        while (pinned.moveToNext()) {
            try {
                linkedList.add(Long.valueOf(CursorUtil.requireLong(pinned, "_id")));
            } catch (Throwable th) {
                if (pinned != null) {
                    try {
                        pinned.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        pinned.close();
        return linkedList;
    }

    private Cursor getPinned(String[] strArr) {
        return this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, strArr, "pinned > ?", SqlUtil.buildArgs(0), null, null, "pinned ASC");
    }

    public void restorePins(Collection<Long> collection) {
        String str = TAG;
        Log.d(str, "Restoring pinned threads " + StringUtil.join(collection, ","));
        pinConversations(collection, true);
    }

    public void pinConversations(Collection<Long> collection) {
        String str = TAG;
        Log.d(str, "Pinning threads " + StringUtil.join(collection, ","));
        pinConversations(collection, false);
    }

    /* JADX INFO: finally extract failed */
    private void pinConversations(Collection<Long> collection, boolean z) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        LinkedHashSet<Long> linkedHashSet = new LinkedHashSet(collection);
        try {
            signalWritableDatabase.beginTransaction();
            if (z) {
                ContentValues contentValues = new ContentValues(1);
                contentValues.put(PINNED, (Integer) 0);
                signalWritableDatabase.update(TABLE_NAME, contentValues, "pinned > ?", SqlUtil.buildArgs(0));
            }
            int pinnedConversationListCount = getPinnedConversationListCount();
            if (pinnedConversationListCount > 0 && z) {
                throw new AssertionError();
            }
            for (Long l : linkedHashSet) {
                long longValue = l.longValue();
                ContentValues contentValues2 = new ContentValues(1);
                pinnedConversationListCount++;
                contentValues2.put(PINNED, Integer.valueOf(pinnedConversationListCount));
                signalWritableDatabase.update(TABLE_NAME, contentValues2, "_id = ?", SqlUtil.buildArgs(longValue));
            }
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            notifyConversationListListeners();
            notifyConversationListListeners();
            SignalDatabase.recipients().markNeedsSync(Recipient.self().getId());
            StorageSyncHelper.scheduleSyncForDataChange();
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            notifyConversationListListeners();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public void unpinConversations(Collection<Long> collection) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        String str = "_id IN (" + StringUtil.join(Stream.of(collection).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.database.ThreadDatabase$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ThreadDatabase.lambda$unpinConversations$5((Long) obj);
            }
        }).toList(), ",") + ")";
        List<Long> pinnedThreadIds = getPinnedThreadIds();
        pinnedThreadIds.removeAll(collection);
        contentValues.put(PINNED, (Integer) 0);
        signalWritableDatabase.beginTransaction();
        try {
            signalWritableDatabase.update(TABLE_NAME, contentValues, str, SqlUtil.buildArgs(Stream.of(collection).toArray()));
            CollectionsKt___CollectionsKt.forEachIndexed(pinnedThreadIds, new Function2() { // from class: org.thoughtcrime.securesms.database.ThreadDatabase$$ExternalSyntheticLambda8
                @Override // kotlin.jvm.functions.Function2
                public final Object invoke(Object obj, Object obj2) {
                    return ThreadDatabase.lambda$unpinConversations$6(SQLiteDatabase.this, (Integer) obj, (Long) obj2);
                }
            });
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            notifyConversationListListeners();
            SignalDatabase.recipients().markNeedsSync(Recipient.self().getId());
            StorageSyncHelper.scheduleSyncForDataChange();
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    public static /* synthetic */ Unit lambda$unpinConversations$6(SQLiteDatabase sQLiteDatabase, Integer num, Long l) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(PINNED, Integer.valueOf(num.intValue() + 1));
        sQLiteDatabase.update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(l.longValue()));
        return Unit.INSTANCE;
    }

    public void archiveConversation(long j) {
        setArchived(Collections.singleton(Long.valueOf(j)), true);
    }

    public void unarchiveConversation(long j) {
        setArchived(Collections.singleton(Long.valueOf(j)), false);
    }

    public void setLastSeen(long j) {
        setLastSeenSilently(j);
        notifyConversationListListeners();
    }

    public void setLastSeenSilently(long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(LAST_SEEN, Long.valueOf(System.currentTimeMillis()));
        signalWritableDatabase.update(TABLE_NAME, contentValues, "_id = ?", new String[]{String.valueOf(j)});
    }

    public void setLastScrolled(long j, long j2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(LAST_SCROLLED, Long.valueOf(j2));
        signalWritableDatabase.update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(j));
    }

    public ConversationMetadata getConversationMetadata(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{LAST_SEEN, HAS_SENT, LAST_SCROLLED}, "_id = ?", new String[]{String.valueOf(j)}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    ConversationMetadata conversationMetadata = new ConversationMetadata(query.getLong(query.getColumnIndexOrThrow(LAST_SEEN)), query.getLong(query.getColumnIndexOrThrow(HAS_SENT)) == 1, query.getLong(query.getColumnIndexOrThrow(LAST_SCROLLED)));
                    query.close();
                    return conversationMetadata;
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        ConversationMetadata conversationMetadata2 = new ConversationMetadata(-1, false, -1);
        if (query != null) {
            query.close();
        }
        return conversationMetadata2;
    }

    /* JADX INFO: finally extract failed */
    public void deleteConversation(long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        RecipientId recipientIdForThreadId = getRecipientIdForThreadId(j);
        signalWritableDatabase.beginTransaction();
        try {
            SignalDatabase.sms().deleteThread(j);
            SignalDatabase.mms().deleteThread(j);
            SignalDatabase.drafts().clearDrafts(j);
            signalWritableDatabase.delete(TABLE_NAME, "_id = ?", new String[]{j + ""});
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            notifyConversationListListeners();
            notifyConversationListeners(j);
            ConversationUtil.clearShortcuts(this.context, Collections.singleton(recipientIdForThreadId));
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public void deleteConversations(Set<Long> set) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        List<RecipientId> recipientIdsForThreadIds = getRecipientIdsForThreadIds(set);
        signalWritableDatabase.beginTransaction();
        try {
            SignalDatabase.sms().deleteThreads(set);
            SignalDatabase.mms().deleteThreads(set);
            SignalDatabase.drafts().clearDrafts(set);
            StringBuilder sb = new StringBuilder();
            for (Long l : set) {
                long longValue = l.longValue();
                if (sb.length() > 0) {
                    sb.append(" OR ");
                }
                sb.append("_id = '");
                sb.append(longValue);
                sb.append("'");
            }
            signalWritableDatabase.delete(TABLE_NAME, sb.toString(), (String[]) null);
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            notifyConversationListListeners();
            notifyConversationListeners(set);
            ConversationUtil.clearShortcuts(this.context, recipientIdsForThreadIds);
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public void deleteAllConversations() {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            SignalDatabase.messageLog().deleteAll();
            SignalDatabase.sms().deleteAllThreads();
            SignalDatabase.mms().deleteAllThreads();
            SignalDatabase.drafts().clearAllDrafts();
            signalWritableDatabase.delete(TABLE_NAME, (String) null, (String[]) null);
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            notifyConversationListListeners();
            ConversationUtil.clearAllShortcuts(this.context);
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    public long getThreadIdIfExistsFor(RecipientId recipientId) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{"_id"}, "thread_recipient_id = ?", new String[]{recipientId.serialize()}, null, null, null, SubscriptionLevels.BOOST_LEVEL);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    long requireLong = CursorUtil.requireLong(query, "_id");
                    query.close();
                    return requireLong;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return -1;
    }

    public Map<RecipientId, Long> getThreadIdsIfExistsFor(RecipientId... recipientIdArr) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        SqlUtil.Query buildSingleCollectionQuery = SqlUtil.buildSingleCollectionQuery(RECIPIENT_ID, Arrays.asList(recipientIdArr));
        HashMap hashMap = new HashMap();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, new String[]{"_id", RECIPIENT_ID}, buildSingleCollectionQuery.getWhere(), buildSingleCollectionQuery.getWhereArgs(), null, null, null, SubscriptionLevels.BOOST_LEVEL);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                hashMap.put(RecipientId.from(CursorUtil.requireString(query, RECIPIENT_ID)), Long.valueOf(CursorUtil.requireLong(query, "_id")));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return hashMap;
    }

    public long getOrCreateValidThreadId(Recipient recipient, long j) {
        return getOrCreateValidThreadId(recipient, j, 2);
    }

    public long getOrCreateValidThreadId(Recipient recipient, long j, int i) {
        if (j == -1) {
            return getOrCreateThreadIdFor(recipient, i);
        }
        Optional<Long> thread = RemappedRecords.getInstance().getThread(j);
        if (!thread.isPresent()) {
            return j;
        }
        String str = TAG;
        Log.i(str, "Using remapped threadId: " + j + " -> " + thread.get());
        return thread.get().longValue();
    }

    public long getOrCreateThreadIdFor(Recipient recipient) {
        return getOrCreateThreadIdFor(recipient, 2);
    }

    public long getOrCreateThreadIdFor(Recipient recipient, int i) {
        Long threadIdFor = getThreadIdFor(recipient.getId());
        if (threadIdFor != null) {
            return threadIdFor.longValue();
        }
        return createThreadForRecipient(recipient.getId(), recipient.isGroup(), i);
    }

    public Long getThreadIdFor(RecipientId recipientId) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, new String[]{"_id"}, "thread_recipient_id = ?", new String[]{recipientId.serialize()}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    Long valueOf = Long.valueOf(query.getLong(query.getColumnIndexOrThrow("_id")));
                    query.close();
                    return valueOf;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return null;
    }

    public RecipientId getRecipientIdForThreadId(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, RECIPIENT_ID_PROJECTION, "_id = ?", SqlUtil.buildArgs(j), null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    RecipientId from = RecipientId.from(query.getLong(query.getColumnIndexOrThrow(RECIPIENT_ID)));
                    query.close();
                    return from;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query == null) {
            return null;
        }
        query.close();
        return null;
    }

    public Recipient getRecipientForThreadId(long j) {
        RecipientId recipientIdForThreadId = getRecipientIdForThreadId(j);
        if (recipientIdForThreadId == null) {
            return null;
        }
        return Recipient.resolved(recipientIdForThreadId);
    }

    public List<RecipientId> getRecipientIdsForThreadIds(Collection<Long> collection) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        SqlUtil.Query buildSingleCollectionQuery = SqlUtil.buildSingleCollectionQuery("_id", collection);
        ArrayList arrayList = new ArrayList(collection.size());
        Cursor query = signalReadableDatabase.query(TABLE_NAME, new String[]{RECIPIENT_ID}, buildSingleCollectionQuery.getWhere(), buildSingleCollectionQuery.getWhereArgs(), null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                arrayList.add(RecipientId.from(CursorUtil.requireLong(query, RECIPIENT_ID)));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return arrayList;
    }

    public boolean hasThread(RecipientId recipientId) {
        return getThreadIdIfExistsFor(recipientId) > -1;
    }

    public void updateLastSeenAndMarkSentAndLastScrolledSilenty(long j) {
        ContentValues contentValues = new ContentValues(3);
        contentValues.put(LAST_SEEN, Long.valueOf(System.currentTimeMillis()));
        contentValues.put(HAS_SENT, (Integer) 1);
        contentValues.put(LAST_SCROLLED, (Integer) 0);
        this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(j));
    }

    public void setHasSentSilently(long j, boolean z) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(HAS_SENT, Integer.valueOf(z ? 1 : 0));
        this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, "_id = ?", new String[]{String.valueOf(j)});
    }

    public void updateReadState(long j) {
        ThreadRecord threadRecord = getThreadRecord(Long.valueOf(j));
        int unreadCount = SignalDatabase.mmsSms().getUnreadCount(j);
        ContentValues contentValues = new ContentValues();
        contentValues.put("read", Integer.valueOf((unreadCount == 0 ? ReadStatus.READ : ReadStatus.UNREAD).serialize()));
        contentValues.put(UNREAD_COUNT, Integer.valueOf(unreadCount));
        this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(j));
        notifyConversationListListeners();
        if (threadRecord != null && threadRecord.isForcedUnread()) {
            SignalDatabase.recipients().markNeedsSync(threadRecord.getRecipient().getId());
            StorageSyncHelper.scheduleSyncForDataChange();
        }
    }

    public void applyStorageSyncUpdate(RecipientId recipientId, SignalContactRecord signalContactRecord) {
        applyStorageSyncUpdate(recipientId, signalContactRecord.isArchived(), signalContactRecord.isForcedUnread());
    }

    public void applyStorageSyncUpdate(RecipientId recipientId, SignalGroupV1Record signalGroupV1Record) {
        applyStorageSyncUpdate(recipientId, signalGroupV1Record.isArchived(), signalGroupV1Record.isForcedUnread());
    }

    public void applyStorageSyncUpdate(RecipientId recipientId, SignalGroupV2Record signalGroupV2Record) {
        applyStorageSyncUpdate(recipientId, signalGroupV2Record.isArchived(), signalGroupV2Record.isForcedUnread());
    }

    /* JADX INFO: finally extract failed */
    public void applyStorageSyncUpdate(RecipientId recipientId, SignalAccountRecord signalAccountRecord) {
        Recipient recipient;
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            applyStorageSyncUpdate(recipientId, signalAccountRecord.isNoteToSelfArchived(), signalAccountRecord.isNoteToSelfForcedUnread());
            ContentValues contentValues = new ContentValues();
            contentValues.put(PINNED, (Integer) 0);
            signalWritableDatabase.update(TABLE_NAME, contentValues, null, null);
            int i = 1;
            for (SignalAccountRecord.PinnedConversation pinnedConversation : signalAccountRecord.getPinnedConversations()) {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put(PINNED, Integer.valueOf(i));
                if (pinnedConversation.getContact().isPresent()) {
                    recipient = Recipient.externalPush(pinnedConversation.getContact().get());
                } else if (pinnedConversation.getGroupV1Id().isPresent()) {
                    try {
                        recipient = Recipient.externalGroupExact(GroupId.v1(pinnedConversation.getGroupV1Id().get()));
                    } catch (BadGroupIdException e) {
                        Log.w(TAG, "Failed to parse pinned groupV1 ID!", e);
                    }
                } else {
                    if (pinnedConversation.getGroupV2MasterKey().isPresent()) {
                        try {
                            recipient = Recipient.externalGroupExact(GroupId.v2(new GroupMasterKey(pinnedConversation.getGroupV2MasterKey().get())));
                        } catch (InvalidInputException e2) {
                            Log.w(TAG, "Failed to parse pinned groupV2 master key!", e2);
                        }
                    } else {
                        Log.w(TAG, "Empty pinned conversation on the AccountRecord?");
                    }
                    recipient = null;
                }
                if (recipient != null) {
                    signalWritableDatabase.update(TABLE_NAME, contentValues2, "thread_recipient_id = ?", SqlUtil.buildArgs(recipient.getId()));
                }
                i++;
            }
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            notifyConversationListListeners();
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    private void applyStorageSyncUpdate(RecipientId recipientId, boolean z, boolean z2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ARCHIVED, Integer.valueOf(z ? 1 : 0));
        Long threadIdFor = getThreadIdFor(recipientId);
        if (z2) {
            contentValues.put("read", Integer.valueOf(ReadStatus.FORCED_UNREAD.serialize()));
        } else if (threadIdFor != null) {
            int unreadCount = SignalDatabase.mmsSms().getUnreadCount(threadIdFor.longValue());
            contentValues.put("read", Integer.valueOf((unreadCount == 0 ? ReadStatus.READ : ReadStatus.UNREAD).serialize()));
            contentValues.put(UNREAD_COUNT, Integer.valueOf(unreadCount));
        }
        this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, "thread_recipient_id = ?", SqlUtil.buildArgs(recipientId));
        if (threadIdFor != null) {
            notifyConversationListeners(threadIdFor.longValue());
        }
    }

    public boolean update(long j, boolean z) {
        return update(j, z, true, true);
    }

    public boolean updateSilently(long j, boolean z) {
        return update(j, z, true, false);
    }

    public boolean update(long j, boolean z, boolean z2) {
        return update(j, z, z2, true);
    }

    private boolean update(long j, boolean z, boolean z2, boolean z3) {
        MmsSmsDatabase mmsSms = SignalDatabase.mmsSms();
        boolean hasMeaningfulMessage = mmsSms.hasMeaningfulMessage(j);
        boolean contains = getPinnedThreadIds().contains(Long.valueOf(j));
        boolean z4 = z2 && !contains && !SignalDatabase.mms().containsStories(j);
        if (!hasMeaningfulMessage) {
            if (z4) {
                deleteConversation(j);
                return true;
            } else if (!contains) {
                return false;
            }
        }
        try {
            MessageRecord conversationSnippet = mmsSms.getConversationSnippet(j);
            updateThread(j, hasMeaningfulMessage, ThreadBodyUtil.getFormattedBodyFor(this.context, conversationSnippet), getAttachmentUriFor(conversationSnippet), getContentTypeFor(conversationSnippet), getExtrasFor(conversationSnippet), conversationSnippet.getTimestamp(), conversationSnippet.getDeliveryStatus(), conversationSnippet.getDeliveryReceiptCount(), conversationSnippet.getType(), z, conversationSnippet.getExpiresIn(), conversationSnippet.getReadReceiptCount());
            if (z3) {
                notifyConversationListListeners();
            }
            return false;
        } catch (NoSuchMessageException unused) {
            if (z4) {
                deleteConversation(j);
            }
            if (contains) {
                updateThread(j, hasMeaningfulMessage, null, null, null, null, 0, 0, 0, 0, z, 0, 0);
            }
            return true;
        }
    }

    public void updateSnippetTypeSilently(long j) {
        if (j != -1) {
            try {
                long conversationSnippetType = SignalDatabase.mmsSms().getConversationSnippetType(j);
                ContentValues contentValues = new ContentValues(1);
                contentValues.put(SNIPPET_TYPE, Long.valueOf(conversationSnippetType));
                this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(j));
            } catch (NoSuchMessageException unused) {
                String str = TAG;
                Log.w(str, "Unable to find snippet message for thread: " + j);
            }
        }
    }

    public ThreadRecord getThreadRecordFor(Recipient recipient) {
        ThreadRecord threadRecord = getThreadRecord(Long.valueOf(getOrCreateThreadIdFor(recipient)));
        Objects.requireNonNull(threadRecord);
        return threadRecord;
    }

    public Set<RecipientId> getAllThreadRecipients() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        HashSet hashSet = new HashSet();
        Cursor query = signalReadableDatabase.query(TABLE_NAME, new String[]{RECIPIENT_ID}, null, null, null, null, null);
        while (query.moveToNext()) {
            try {
                hashSet.add(RecipientId.from(CursorUtil.requireString(query, RECIPIENT_ID)));
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return hashSet;
    }

    public MergeResult merge(RecipientId recipientId, RecipientId recipientId2) {
        if (this.databaseHelper.getSignalWritableDatabase().inTransaction()) {
            String str = TAG;
            Log.w(str, "Merging threads. Primary: " + recipientId + ", Secondary: " + recipientId2, true);
            ThreadRecord threadRecord = getThreadRecord(getThreadIdFor(recipientId));
            ThreadRecord threadRecord2 = getThreadRecord(getThreadIdFor(recipientId2));
            if (threadRecord != null && threadRecord2 == null) {
                Log.w(str, "[merge] Only had a thread for primary. Returning that.", true);
                return new MergeResult(threadRecord.getThreadId(), -1, false);
            } else if (threadRecord == null && threadRecord2 != null) {
                Log.w(str, "[merge] Only had a thread for secondary. Updating it to have the recipientId of the primary.", true);
                ContentValues contentValues = new ContentValues();
                contentValues.put(RECIPIENT_ID, recipientId.serialize());
                this.databaseHelper.getSignalWritableDatabase().update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(threadRecord2.getThreadId()));
                return new MergeResult(threadRecord2.getThreadId(), -1, false);
            } else if (threadRecord == null && threadRecord2 == null) {
                Log.w(str, "[merge] No thread for either.");
                return new MergeResult(-1, -1, false);
            } else {
                Log.w(str, "[merge] Had a thread for both. Deleting the secondary and merging the attributes together.", true);
                SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
                signalWritableDatabase.delete(TABLE_NAME, "_id = ?", SqlUtil.buildArgs(threadRecord2.getThreadId()));
                if (threadRecord.getExpiresIn() != threadRecord2.getExpiresIn()) {
                    ContentValues contentValues2 = new ContentValues();
                    if (threadRecord.getExpiresIn() == 0) {
                        contentValues2.put("expires_in", Long.valueOf(threadRecord2.getExpiresIn()));
                    } else if (threadRecord2.getExpiresIn() == 0) {
                        contentValues2.put("expires_in", Long.valueOf(threadRecord.getExpiresIn()));
                    } else {
                        contentValues2.put("expires_in", Long.valueOf(Math.min(threadRecord.getExpiresIn(), threadRecord2.getExpiresIn())));
                    }
                    signalWritableDatabase.update(TABLE_NAME, contentValues2, "_id = ?", SqlUtil.buildArgs(threadRecord.getThreadId()));
                }
                ContentValues contentValues3 = new ContentValues();
                contentValues3.put("thread_id", Long.valueOf(threadRecord.getThreadId()));
                signalWritableDatabase.update("drafts", contentValues3, "thread_id = ?", SqlUtil.buildArgs(threadRecord2.getThreadId()));
                ContentValues contentValues4 = new ContentValues();
                contentValues4.put("thread_id", Long.valueOf(threadRecord.getThreadId()));
                signalWritableDatabase.update(SearchDatabase.SMS_FTS_TABLE_NAME, contentValues4, "thread_id = ?", SqlUtil.buildArgs(threadRecord2.getThreadId()));
                signalWritableDatabase.update(SearchDatabase.MMS_FTS_TABLE_NAME, contentValues4, "thread_id = ?", SqlUtil.buildArgs(threadRecord2.getThreadId()));
                RemappedRecords.getInstance().addThread(threadRecord2.getThreadId(), threadRecord.getThreadId());
                return new MergeResult(threadRecord.getThreadId(), threadRecord2.getThreadId(), true);
            }
        } else {
            throw new IllegalStateException("Must be in a transaction!");
        }
    }

    public ThreadRecord getThreadRecord(Long l) {
        if (l == null) {
            return null;
        }
        Cursor rawQuery = this.databaseHelper.getSignalReadableDatabase().rawQuery(createQuery("thread._id = ?", 1), SqlUtil.buildArgs(l.longValue()));
        if (rawQuery != null) {
            try {
                if (rawQuery.moveToFirst()) {
                    ThreadRecord current = readerFor(rawQuery).getCurrent();
                    rawQuery.close();
                    return current;
                }
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        return null;
    }

    private Uri getAttachmentUriFor(MessageRecord messageRecord) {
        if (messageRecord.isMms() && !messageRecord.isMmsNotification() && !messageRecord.isGroupAction()) {
            SlideDeck slideDeck = ((MediaMmsMessageRecord) messageRecord).getSlideDeck();
            Slide slide = (Slide) OptionalUtil.or(Optional.ofNullable(slideDeck.getThumbnailSlide()), Optional.ofNullable(slideDeck.getStickerSlide())).orElse(null);
            if (slide != null && !((MmsMessageRecord) messageRecord).isViewOnce()) {
                return slide.getUri();
            }
        }
        return null;
    }

    private String getContentTypeFor(MessageRecord messageRecord) {
        if (!messageRecord.isMms()) {
            return null;
        }
        SlideDeck slideDeck = ((MmsMessageRecord) messageRecord).getSlideDeck();
        if (slideDeck.getSlides().size() > 0) {
            return slideDeck.getSlides().get(0).getContentType();
        }
        return null;
    }

    private Extra getExtrasFor(MessageRecord messageRecord) {
        Recipient recipient = messageRecord.isOutgoing() ? messageRecord.getRecipient() : getRecipientForThreadId(messageRecord.getThreadId());
        boolean isMessageRequestAccepted = RecipientUtil.isMessageRequestAccepted(this.context, Long.valueOf(messageRecord.getThreadId()), recipient);
        RecipientId id = messageRecord.getIndividualRecipient().getId();
        if (!isMessageRequestAccepted && recipient != null) {
            if (recipient.isPushGroup()) {
                if (recipient.isPushV2Group()) {
                    MessageRecord.InviteAddState gv2AddInviteState = messageRecord.getGv2AddInviteState();
                    if (gv2AddInviteState != null) {
                        RecipientId from = RecipientId.from(ServiceId.from(gv2AddInviteState.getAddedOrInvitedBy()));
                        if (gv2AddInviteState.isInvited()) {
                            String str = TAG;
                            Log.i(str, "GV2 invite message request from " + from);
                            return Extra.forGroupV2invite(from, id);
                        }
                        String str2 = TAG;
                        Log.i(str2, "GV2 message request from " + from);
                        return Extra.forGroupMessageRequest(from, id);
                    }
                    Log.w(TAG, "Falling back to unknown message request state for GV2 message");
                    return Extra.forMessageRequest(id);
                }
                RecipientId groupAddedBy = SignalDatabase.mmsSms().getGroupAddedBy(messageRecord.getThreadId());
                if (groupAddedBy != null) {
                    return Extra.forGroupMessageRequest(groupAddedBy, id);
                }
            }
            return Extra.forMessageRequest(id);
        } else if (messageRecord.isRemoteDelete()) {
            return Extra.forRemoteDelete(id);
        } else {
            if (messageRecord.isViewOnce()) {
                return Extra.forViewOnce(id);
            }
            if (messageRecord.isMms()) {
                MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) messageRecord;
                if (mmsMessageRecord.getSlideDeck().getStickerSlide() != null) {
                    StickerSlide stickerSlide = mmsMessageRecord.getSlideDeck().getStickerSlide();
                    Objects.requireNonNull(stickerSlide);
                    return Extra.forSticker(stickerSlide.getEmoji(), id);
                }
            }
            if (messageRecord.isMms() && ((MmsMessageRecord) messageRecord).getSlideDeck().getSlides().size() > 1) {
                return Extra.forAlbum(id);
            }
            if (recipient == null || !recipient.isGroup()) {
                return null;
            }
            return Extra.forDefault(id);
        }
    }

    private String createQuery(String str, long j) {
        return createQuery(str, 0L, j, false);
    }

    private String createQuery(String str, long j, long j2, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(z ? "thread.pinned DESC, " : "");
        sb.append(TABLE_NAME);
        sb.append(".");
        sb.append("date");
        sb.append(" DESC");
        return createQuery(str, sb.toString(), j, j2);
    }

    private String createQuery(String str, String str2, long j, long j2) {
        String str3 = "SELECT " + Util.join(COMBINED_THREAD_RECIPIENT_GROUP_PROJECTION, ",") + " FROM " + TABLE_NAME + " LEFT OUTER JOIN " + RecipientDatabase.TABLE_NAME + " ON " + TABLE_NAME + "." + RECIPIENT_ID + " = " + RecipientDatabase.TABLE_NAME + "._id LEFT OUTER JOIN " + ChooseGroupStoryBottomSheet.RESULT_SET + " ON " + TABLE_NAME + "." + RECIPIENT_ID + " = " + ChooseGroupStoryBottomSheet.RESULT_SET + ".recipient_id WHERE " + str + " ORDER BY " + str2;
        if (j2 > 0) {
            str3 = str3 + " LIMIT " + j2;
        }
        if (j <= 0) {
            return str3;
        }
        return str3 + " OFFSET " + j;
    }

    private boolean isSilentType(long j) {
        return MmsSmsColumns.Types.isProfileChange(j) || MmsSmsColumns.Types.isGroupV1MigrationEvent(j) || MmsSmsColumns.Types.isChangeNumber(j) || MmsSmsColumns.Types.isBoostRequest(j) || MmsSmsColumns.Types.isGroupV2LeaveOnly(j);
    }

    public Reader readerFor(Cursor cursor) {
        return new Reader(cursor);
    }

    /* loaded from: classes4.dex */
    public class Reader extends StaticReader {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Reader(Cursor cursor) {
            super(cursor, r1.context);
            ThreadDatabase.this = r1;
        }
    }

    /* loaded from: classes4.dex */
    public static class StaticReader implements Closeable {
        private final Context context;
        private final Cursor cursor;

        public StaticReader(Cursor cursor, Context context) {
            this.cursor = cursor;
            this.context = context;
        }

        public ThreadRecord getNext() {
            Cursor cursor = this.cursor;
            if (cursor == null || !cursor.moveToNext()) {
                return null;
            }
            return getCurrent();
        }

        public ThreadRecord getCurrent() {
            Recipient recipient;
            int i;
            RecipientId from = RecipientId.from(CursorUtil.requireLong(this.cursor, ThreadDatabase.RECIPIENT_ID));
            RecipientRecord record = SignalDatabase.recipients().getRecord(this.context, this.cursor, ThreadDatabase.RECIPIENT_ID);
            boolean z = true;
            if (record.getGroupId() != null) {
                GroupDatabase.GroupRecord current = new GroupDatabase.Reader(this.cursor).getCurrent();
                if (current != null) {
                    recipient = new Recipient(from, new RecipientDetails(current.getTitle(), null, current.hasAvatar() ? Optional.of(Long.valueOf(current.getAvatarId())) : Optional.empty(), false, false, record.getRegistered(), record, null, false), false);
                } else {
                    recipient = Recipient.live(from).get();
                }
            } else {
                recipient = new Recipient(from, RecipientDetails.forIndividual(this.context, record), true);
            }
            if (TextSecurePreferences.isReadReceiptsEnabled(this.context)) {
                Cursor cursor = this.cursor;
                i = cursor.getInt(cursor.getColumnIndexOrThrow("read_receipt_count"));
            } else {
                i = 0;
            }
            Cursor cursor2 = this.cursor;
            String string = cursor2.getString(cursor2.getColumnIndexOrThrow(ThreadDatabase.SNIPPET_EXTRAS));
            Extra extra = null;
            if (string != null) {
                try {
                    extra = (Extra) JsonUtils.fromJson(string, Extra.class);
                } catch (IOException unused) {
                    Log.w(ThreadDatabase.TAG, "Failed to decode extras!");
                }
            }
            Cursor cursor3 = this.cursor;
            ThreadRecord.Builder recipient2 = new ThreadRecord.Builder(cursor3.getLong(cursor3.getColumnIndexOrThrow("_id"))).setRecipient(recipient);
            Cursor cursor4 = this.cursor;
            ThreadRecord.Builder type = recipient2.setType((long) cursor4.getInt(cursor4.getColumnIndexOrThrow(ThreadDatabase.SNIPPET_TYPE)));
            Cursor cursor5 = this.cursor;
            ThreadRecord.Builder distributionType = type.setDistributionType(cursor5.getInt(cursor5.getColumnIndexOrThrow("type")));
            Cursor cursor6 = this.cursor;
            ThreadRecord.Builder body = distributionType.setBody(Util.emptyIfNull(cursor6.getString(cursor6.getColumnIndexOrThrow("snippet"))));
            Cursor cursor7 = this.cursor;
            ThreadRecord.Builder archived = body.setDate(cursor7.getLong(cursor7.getColumnIndexOrThrow("date"))).setArchived(CursorUtil.requireInt(this.cursor, ThreadDatabase.ARCHIVED) != 0);
            Cursor cursor8 = this.cursor;
            ThreadRecord.Builder deliveryStatus = archived.setDeliveryStatus((long) cursor8.getInt(cursor8.getColumnIndexOrThrow("status")));
            Cursor cursor9 = this.cursor;
            ThreadRecord.Builder readReceiptCount = deliveryStatus.setDeliveryReceiptCount(cursor9.getInt(cursor9.getColumnIndexOrThrow("delivery_receipt_count"))).setReadReceiptCount(i);
            Cursor cursor10 = this.cursor;
            ThreadRecord.Builder expiresIn = readReceiptCount.setExpiresIn(cursor10.getLong(cursor10.getColumnIndexOrThrow("expires_in")));
            Cursor cursor11 = this.cursor;
            ThreadRecord.Builder snippetUri = expiresIn.setLastSeen(cursor11.getLong(cursor11.getColumnIndexOrThrow(ThreadDatabase.LAST_SEEN))).setSnippetUri(getSnippetUri(this.cursor));
            Cursor cursor12 = this.cursor;
            ThreadRecord.Builder contentType = snippetUri.setContentType(cursor12.getString(cursor12.getColumnIndexOrThrow(ThreadDatabase.SNIPPET_CONTENT_TYPE)));
            Cursor cursor13 = this.cursor;
            ThreadRecord.Builder meaningfulMessages = contentType.setMeaningfulMessages(cursor13.getLong(cursor13.getColumnIndexOrThrow(ThreadDatabase.MEANINGFUL_MESSAGES)) > 0);
            Cursor cursor14 = this.cursor;
            ThreadRecord.Builder unreadCount = meaningfulMessages.setUnreadCount(cursor14.getInt(cursor14.getColumnIndexOrThrow(ThreadDatabase.UNREAD_COUNT)));
            Cursor cursor15 = this.cursor;
            if (cursor15.getInt(cursor15.getColumnIndexOrThrow("read")) != ReadStatus.FORCED_UNREAD.serialize()) {
                z = false;
            }
            return unreadCount.setForcedUnread(z).setPinned(CursorUtil.requireBoolean(this.cursor, ThreadDatabase.PINNED)).setExtra(extra).build();
        }

        private Uri getSnippetUri(Cursor cursor) {
            if (cursor.isNull(cursor.getColumnIndexOrThrow(ThreadDatabase.SNIPPET_URI))) {
                return null;
            }
            try {
                return Uri.parse(cursor.getString(cursor.getColumnIndexOrThrow(ThreadDatabase.SNIPPET_URI)));
            } catch (IllegalArgumentException e) {
                Log.w(ThreadDatabase.TAG, e);
                return null;
            }
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            Cursor cursor = this.cursor;
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /* loaded from: classes.dex */
    public static final class Extra {
        @JsonProperty
        private final String groupAddedBy;
        @JsonProperty
        private final String individualRecipientId;
        @JsonProperty
        private final boolean isAlbum;
        @JsonProperty
        private final boolean isGv2Invite;
        @JsonProperty
        private final boolean isMessageRequestAccepted;
        @JsonProperty
        private final boolean isRemoteDelete;
        @JsonProperty
        private final boolean isRevealable;
        @JsonProperty
        private final boolean isSticker;
        @JsonProperty
        private final String stickerEmoji;

        public Extra(@JsonProperty("isRevealable") boolean z, @JsonProperty("isSticker") boolean z2, @JsonProperty("stickerEmoji") String str, @JsonProperty("isAlbum") boolean z3, @JsonProperty("isRemoteDelete") boolean z4, @JsonProperty("isMessageRequestAccepted") boolean z5, @JsonProperty("isGv2Invite") boolean z6, @JsonProperty("groupAddedBy") String str2, @JsonProperty("individualRecipientId") String str3) {
            this.isRevealable = z;
            this.isSticker = z2;
            this.stickerEmoji = str;
            this.isAlbum = z3;
            this.isRemoteDelete = z4;
            this.isMessageRequestAccepted = z5;
            this.isGv2Invite = z6;
            this.groupAddedBy = str2;
            this.individualRecipientId = str3;
        }

        public static Extra forViewOnce(RecipientId recipientId) {
            return new Extra(true, false, null, false, false, true, false, null, recipientId.serialize());
        }

        public static Extra forSticker(String str, RecipientId recipientId) {
            return new Extra(false, true, str, false, false, true, false, null, recipientId.serialize());
        }

        public static Extra forAlbum(RecipientId recipientId) {
            return new Extra(false, false, null, true, false, true, false, null, recipientId.serialize());
        }

        public static Extra forRemoteDelete(RecipientId recipientId) {
            return new Extra(false, false, null, false, true, true, false, null, recipientId.serialize());
        }

        public static Extra forMessageRequest(RecipientId recipientId) {
            return new Extra(false, false, null, false, false, false, false, null, recipientId.serialize());
        }

        public static Extra forGroupMessageRequest(RecipientId recipientId, RecipientId recipientId2) {
            return new Extra(false, false, null, false, false, false, false, recipientId.serialize(), recipientId2.serialize());
        }

        public static Extra forGroupV2invite(RecipientId recipientId, RecipientId recipientId2) {
            return new Extra(false, false, null, false, false, false, true, recipientId.serialize(), recipientId2.serialize());
        }

        public static Extra forDefault(RecipientId recipientId) {
            return new Extra(false, false, null, false, false, true, false, null, recipientId.serialize());
        }

        public boolean isViewOnce() {
            return this.isRevealable;
        }

        public boolean isSticker() {
            return this.isSticker;
        }

        public String getStickerEmoji() {
            return this.stickerEmoji;
        }

        public boolean isAlbum() {
            return this.isAlbum;
        }

        public boolean isRemoteDelete() {
            return this.isRemoteDelete;
        }

        public boolean isMessageRequestAccepted() {
            return this.isMessageRequestAccepted;
        }

        public boolean isGv2Invite() {
            return this.isGv2Invite;
        }

        public String getGroupAddedBy() {
            return this.groupAddedBy;
        }

        public String getIndividualRecipientId() {
            return this.individualRecipientId;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Extra.class != obj.getClass()) {
                return false;
            }
            Extra extra = (Extra) obj;
            if (this.isRevealable == extra.isRevealable && this.isSticker == extra.isSticker && this.isAlbum == extra.isAlbum && this.isRemoteDelete == extra.isRemoteDelete && this.isMessageRequestAccepted == extra.isMessageRequestAccepted && this.isGv2Invite == extra.isGv2Invite && Objects.equals(this.stickerEmoji, extra.stickerEmoji) && Objects.equals(this.groupAddedBy, extra.groupAddedBy) && Objects.equals(this.individualRecipientId, extra.individualRecipientId)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return Objects.hash(Boolean.valueOf(this.isRevealable), Boolean.valueOf(this.isSticker), this.stickerEmoji, Boolean.valueOf(this.isAlbum), Boolean.valueOf(this.isRemoteDelete), Boolean.valueOf(this.isMessageRequestAccepted), Boolean.valueOf(this.isGv2Invite), this.groupAddedBy, this.individualRecipientId);
        }
    }

    /* loaded from: classes4.dex */
    public enum ReadStatus {
        READ(1),
        UNREAD(0),
        FORCED_UNREAD(2);
        
        private final int value;

        ReadStatus(int i) {
            this.value = i;
        }

        public static ReadStatus deserialize(int i) {
            ReadStatus[] values = values();
            for (ReadStatus readStatus : values) {
                if (readStatus.value == i) {
                    return readStatus;
                }
            }
            throw new IllegalArgumentException("No matching status for value " + i);
        }

        public int serialize() {
            return this.value;
        }
    }

    /* loaded from: classes4.dex */
    public static class ConversationMetadata {
        private final boolean hasSent;
        private final long lastScrolled;
        private final long lastSeen;

        public ConversationMetadata(long j, boolean z, long j2) {
            this.lastSeen = j;
            this.hasSent = z;
            this.lastScrolled = j2;
        }

        public long getLastSeen() {
            return this.lastSeen;
        }

        public boolean hasSent() {
            return this.hasSent;
        }

        public long getLastScrolled() {
            return this.lastScrolled;
        }
    }

    /* loaded from: classes4.dex */
    public static final class MergeResult {
        final boolean neededMerge;
        final long previousThreadId;
        final long threadId;

        private MergeResult(long j, long j2, boolean z) {
            this.threadId = j;
            this.previousThreadId = j2;
            this.neededMerge = z;
        }
    }
}
