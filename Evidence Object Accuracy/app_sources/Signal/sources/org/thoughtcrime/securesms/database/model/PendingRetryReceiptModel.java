package org.thoughtcrime.securesms.database.model;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: PendingRetryReceiptModel.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003¢\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003JE\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001f\u001a\u00020\u0007HÖ\u0001J\t\u0010 \u001a\u00020!HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0011R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0011¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/database/model/PendingRetryReceiptModel;", "", ContactRepository.ID_COLUMN, "", "author", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "authorDevice", "", "sentTimestamp", "receivedTimestamp", "threadId", "(JLorg/thoughtcrime/securesms/recipients/RecipientId;IJJJ)V", "getAuthor", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getAuthorDevice", "()I", "getId", "()J", "getReceivedTimestamp", "getSentTimestamp", "getThreadId", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PendingRetryReceiptModel {
    private final RecipientId author;
    private final int authorDevice;
    private final long id;
    private final long receivedTimestamp;
    private final long sentTimestamp;
    private final long threadId;

    public final long component1() {
        return this.id;
    }

    public final RecipientId component2() {
        return this.author;
    }

    public final int component3() {
        return this.authorDevice;
    }

    public final long component4() {
        return this.sentTimestamp;
    }

    public final long component5() {
        return this.receivedTimestamp;
    }

    public final long component6() {
        return this.threadId;
    }

    public final PendingRetryReceiptModel copy(long j, RecipientId recipientId, int i, long j2, long j3, long j4) {
        Intrinsics.checkNotNullParameter(recipientId, "author");
        return new PendingRetryReceiptModel(j, recipientId, i, j2, j3, j4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PendingRetryReceiptModel)) {
            return false;
        }
        PendingRetryReceiptModel pendingRetryReceiptModel = (PendingRetryReceiptModel) obj;
        return this.id == pendingRetryReceiptModel.id && Intrinsics.areEqual(this.author, pendingRetryReceiptModel.author) && this.authorDevice == pendingRetryReceiptModel.authorDevice && this.sentTimestamp == pendingRetryReceiptModel.sentTimestamp && this.receivedTimestamp == pendingRetryReceiptModel.receivedTimestamp && this.threadId == pendingRetryReceiptModel.threadId;
    }

    public int hashCode() {
        return (((((((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id) * 31) + this.author.hashCode()) * 31) + this.authorDevice) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.sentTimestamp)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.receivedTimestamp)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.threadId);
    }

    public String toString() {
        return "PendingRetryReceiptModel(id=" + this.id + ", author=" + this.author + ", authorDevice=" + this.authorDevice + ", sentTimestamp=" + this.sentTimestamp + ", receivedTimestamp=" + this.receivedTimestamp + ", threadId=" + this.threadId + ')';
    }

    public PendingRetryReceiptModel(long j, RecipientId recipientId, int i, long j2, long j3, long j4) {
        Intrinsics.checkNotNullParameter(recipientId, "author");
        this.id = j;
        this.author = recipientId;
        this.authorDevice = i;
        this.sentTimestamp = j2;
        this.receivedTimestamp = j3;
        this.threadId = j4;
    }

    public final long getId() {
        return this.id;
    }

    public final RecipientId getAuthor() {
        return this.author;
    }

    public final int getAuthorDevice() {
        return this.authorDevice;
    }

    public final long getSentTimestamp() {
        return this.sentTimestamp;
    }

    public final long getReceivedTimestamp() {
        return this.receivedTimestamp;
    }

    public final long getThreadId() {
        return this.threadId;
    }
}
