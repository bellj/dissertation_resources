package org.thoughtcrime.securesms.database.model;

import android.content.Context;
import j$.util.function.Consumer;
import java.util.Collections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ExpirationUtil;

/* loaded from: classes4.dex */
public class InMemoryMessageRecord extends MessageRecord {
    private static final int FORCE_BUBBLE_ID;
    private static final int NO_GROUPS_IN_COMMON_ID;
    private static final int UNIVERSAL_EXPIRE_TIMER_ID;

    public int getActionButtonText() {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isInMemoryMessageRecord() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isMms() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.database.model.MessageRecord
    public boolean isMmsNotification() {
        return false;
    }

    public boolean showActionButton() {
        return false;
    }

    private InMemoryMessageRecord(long j, String str, Recipient recipient, long j2, long j3) {
        super(j, str, recipient, recipient, 1, System.currentTimeMillis(), System.currentTimeMillis(), System.currentTimeMillis(), j2, 0, 0, j3, Collections.emptySet(), Collections.emptySet(), -1, 0, System.currentTimeMillis(), 0, false, Collections.emptyList(), false, 0, 0, -1);
    }

    /* loaded from: classes4.dex */
    public static final class NoGroupsInCommon extends InMemoryMessageRecord {
        private final boolean isGroup;

        @Override // org.thoughtcrime.securesms.database.model.InMemoryMessageRecord
        public int getActionButtonText() {
            return R.string.ConversationUpdateItem_learn_more;
        }

        @Override // org.thoughtcrime.securesms.database.model.MessageRecord
        public boolean isUpdate() {
            return true;
        }

        @Override // org.thoughtcrime.securesms.database.model.InMemoryMessageRecord
        public boolean showActionButton() {
            return true;
        }

        public NoGroupsInCommon(long j, boolean z) {
            super(-1, "", Recipient.UNKNOWN, j, 0);
            this.isGroup = z;
        }

        @Override // org.thoughtcrime.securesms.database.model.MessageRecord
        public UpdateDescription getUpdateDisplayBody(Context context, Consumer<RecipientId> consumer) {
            return UpdateDescription.staticDescription(context.getString(this.isGroup ? R.string.ConversationUpdateItem_no_contacts_in_this_group_review_requests_carefully : R.string.ConversationUpdateItem_no_groups_in_common_review_requests_carefully), (int) R.drawable.ic_update_info_16);
        }

        public boolean isGroup() {
            return this.isGroup;
        }
    }

    /* loaded from: classes4.dex */
    public static final class UniversalExpireTimerUpdate extends InMemoryMessageRecord {
        @Override // org.thoughtcrime.securesms.database.model.MessageRecord
        public boolean isUpdate() {
            return true;
        }

        public UniversalExpireTimerUpdate(long j) {
            super(-2, "", Recipient.UNKNOWN, j, 0);
        }

        @Override // org.thoughtcrime.securesms.database.model.MessageRecord
        public UpdateDescription getUpdateDisplayBody(Context context, Consumer<RecipientId> consumer) {
            return UpdateDescription.staticDescription(context.getString(R.string.ConversationUpdateItem_the_disappearing_message_time_will_be_set_to_s_when_you_message_them, ExpirationUtil.getExpirationDisplayValue(context, SignalStore.settings().getUniversalExpireTimer())), (int) R.drawable.ic_update_timer_16);
        }
    }

    /* loaded from: classes4.dex */
    public static final class ForceConversationBubble extends InMemoryMessageRecord {
        public ForceConversationBubble(Recipient recipient, long j) {
            super(-3, "", recipient, j, 0);
        }
    }
}
