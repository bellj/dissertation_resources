package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.google.android.mms.pdu_alt.NotificationInd;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import net.zetetic.database.sqlcipher.SQLiteStatement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SQLiteDatabaseExtensionsKt;
import org.signal.core.util.SelectBuilderPart2;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.attachments.MmsNotificationAttachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.MentionUtil;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatchSet;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.documents.NetworkFailureSet;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.NotificationMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.Quote;
import org.thoughtcrime.securesms.database.model.SmsMessageRecord;
import org.thoughtcrime.securesms.database.model.StoryResult;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupMigrationMembershipChange;
import org.thoughtcrime.securesms.jobs.TrimThreadJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.IncomingMediaMessage;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.revealable.ViewOnceExpirationInfo;
import org.thoughtcrime.securesms.revealable.ViewOnceUtil;
import org.thoughtcrime.securesms.sms.IncomingTextMessage;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* loaded from: classes4.dex */
public class MmsDatabase extends MessageDatabase {
    static final String CONTENT_LOCATION;
    public static final String[] CREATE_INDEXS = {"CREATE INDEX IF NOT EXISTS mms_read_and_notified_and_thread_id_index ON mms(read,notified,thread_id);", "CREATE INDEX IF NOT EXISTS mms_message_box_index ON mms (msg_box);", "CREATE INDEX IF NOT EXISTS mms_date_sent_index ON mms (date, address, thread_id);", "CREATE INDEX IF NOT EXISTS mms_date_server_index ON mms (date_server);", "CREATE INDEX IF NOT EXISTS mms_thread_date_index ON mms (thread_id, date_received);", "CREATE INDEX IF NOT EXISTS mms_reactions_unread_index ON mms (reactions_unread);", "CREATE INDEX IF NOT EXISTS mms_is_story_index ON mms (is_story);", "CREATE INDEX IF NOT EXISTS mms_parent_story_id_index ON mms (parent_story_id);", "CREATE INDEX IF NOT EXISTS mms_thread_story_parent_story_index ON mms (thread_id, date_received,is_story,parent_story_id);", "CREATE INDEX IF NOT EXISTS mms_quote_id_quote_author_index ON mms(quote_id, quote_author);"};
    public static final String CREATE_TABLE;
    static final String DATE_RECEIVED;
    static final String DATE_SENT;
    static final String EXPIRY;
    private static final String IS_STORY_CLAUSE;
    static final String LINK_PREVIEWS;
    static final String MENTIONS_SELF;
    public static final String MESSAGE_BOX;
    static final String MESSAGE_RANGES;
    static final String MESSAGE_SIZE;
    public static final String MESSAGE_TYPE;
    private static final String[] MMS_PROJECTION = {"mms._id AS _id", "thread_id", "date AS date_sent", "date_received AS date_received", MmsSmsColumns.DATE_SERVER, MESSAGE_BOX, "read", CONTENT_LOCATION, EXPIRY, MESSAGE_TYPE, MESSAGE_SIZE, STATUS, TRANSACTION_ID, "body", PART_COUNT, "address", MmsSmsColumns.ADDRESS_DEVICE_ID, "delivery_receipt_count", "read_receipt_count", MmsSmsColumns.MISMATCHED_IDENTITIES, NETWORK_FAILURE, MmsSmsColumns.SUBSCRIPTION_ID, "expires_in", MmsSmsColumns.EXPIRE_STARTED, MmsSmsColumns.NOTIFIED, QUOTE_ID, QUOTE_AUTHOR, QUOTE_BODY, QUOTE_ATTACHMENT, QUOTE_TYPE, QUOTE_MISSING, QUOTE_MENTIONS, SHARED_CONTACTS, LINK_PREVIEWS, MmsSmsColumns.UNIDENTIFIED, VIEW_ONCE, MmsSmsColumns.REACTIONS_UNREAD, MmsSmsColumns.REACTIONS_LAST_SEEN, MmsSmsColumns.REMOTE_DELETED, MENTIONS_SELF, MmsSmsColumns.NOTIFIED_TIMESTAMP, MmsSmsColumns.VIEWED_RECEIPT_COUNT, MmsSmsColumns.RECEIPT_TIMESTAMP, MESSAGE_RANGES, STORY_TYPE, PARENT_STORY_ID, "json_group_array(json_object('_id', part._id, 'unique_id', part.unique_id, 'mid', part.mid, 'data_size', part.data_size, 'file_name', part.file_name, '_data', part._data, 'ct', part.ct, 'cdn_number', part.cdn_number, 'cl', part.cl, 'fast_preflight_id', part.fast_preflight_id,'voice_note', part.voice_note,'borderless', part.borderless,'video_gif', part.video_gif,'width', part.width,'height', part.height,'quote', part.quote, 'cd', part.cd, 'name', part.name, 'pending_push', part.pending_push, 'caption', part.caption, 'sticker_pack_id', part.sticker_pack_id, 'sticker_pack_key', part.sticker_pack_key, 'sticker_id', part.sticker_id, 'sticker_emoji', part.sticker_emoji, 'blur_hash', part.blur_hash, 'transform_properties', part.transform_properties, 'display_order', part.display_order, 'upload_timestamp', part.upload_timestamp)) AS attachment_json"};
    static final String NETWORK_FAILURE;
    private static final String OUTGOING_INSECURE_MESSAGES_CLAUSE;
    private static final String OUTGOING_SECURE_MESSAGES_CLAUSE;
    static final String PARENT_STORY_ID;
    static final String PART_COUNT;
    static final String QUOTE_ATTACHMENT;
    static final String QUOTE_AUTHOR;
    static final String QUOTE_BODY;
    static final String QUOTE_ID;
    static final String QUOTE_MENTIONS;
    static final String QUOTE_MISSING;
    static final String QUOTE_TYPE;
    private static final String RAW_ID_WHERE;
    static final String SHARED_CONTACTS;
    static final String STATUS;
    public static final String STORY_TYPE;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(MmsDatabase.class);
    static final String TRANSACTION_ID;
    public static final String VIEW_ONCE;
    private final EarlyReceiptCache earlyDeliveryReceiptCache = new EarlyReceiptCache("MmsDelivery");

    /* loaded from: classes4.dex */
    public static class Status {
        public static final int DOWNLOAD_APN_UNAVAILABLE;
        public static final int DOWNLOAD_CONNECTING;
        public static final int DOWNLOAD_HARD_FAILURE;
        public static final int DOWNLOAD_INITIALIZED;
        public static final int DOWNLOAD_NO_CONNECTIVITY;
        public static final int DOWNLOAD_SOFT_FAILURE;
    }

    public static /* synthetic */ Attachment lambda$getOutgoingMessage$0(DatabaseAttachment databaseAttachment) {
        return databaseAttachment;
    }

    public static /* synthetic */ boolean lambda$getOutgoingMessage$1(Attachment attachment) {
        return attachment != null;
    }

    public static /* synthetic */ Attachment lambda$getOutgoingMessage$4(DatabaseAttachment databaseAttachment) {
        return databaseAttachment;
    }

    public static /* synthetic */ boolean lambda$insertMediaMessage$7(Attachment attachment) {
        return attachment != null;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    protected String getDateReceivedColumnName() {
        return "date_received";
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    protected String getDateSentColumnName() {
        return "date";
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    protected String getTableName() {
        return "mms";
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    protected String getTypeField() {
        return MESSAGE_BOX;
    }

    public MmsDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public RecipientId getOldestGroupUpdateSender(long j, long j2) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Cursor getExpirationStartedMessages() {
        return rawQuery("expire_started > 0", null);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public SmsMessageRecord getSmsMessage(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Cursor getMessageCursor(long j) {
        return internalGetMessage(j);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean hasReceivedAnyCallsSince(long j, long j2) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsEndSession(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsInvalidVersionKeyExchange(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsSecure(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsPush(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsDecryptFailed(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsNoSession(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsUnsupportedProtocolVersion(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsInvalidMessage(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsLegacyVersion(long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsMissedCall(long j, boolean z) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markSmsStatus(long j, int i) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.InsertResult updateBundleMessageBody(long j, String str) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> getViewedIncomingMessages(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(getTableName(), new String[]{"_id", "address", "date", MESSAGE_BOX, "thread_id"}, "thread_id = ? AND viewed_receipt_count > 0 AND msg_box & 20 = 20", SqlUtil.buildArgs(j), null, null, null, null);
        try {
            if (query == null) {
                List<MessageDatabase.MarkedMessageInfo> emptyList = Collections.emptyList();
                if (query != null) {
                    query.close();
                }
                return emptyList;
            }
            ArrayList arrayList = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                arrayList.add(new MessageDatabase.MarkedMessageInfo(j, new MessageDatabase.SyncMessageId(RecipientId.from(CursorUtil.requireLong(query, "address")), CursorUtil.requireLong(query, "date")), new MessageId(CursorUtil.requireLong(query, "_id"), true), null));
            }
            query.close();
            return arrayList;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.MarkedMessageInfo setIncomingMessageViewed(long j) {
        List<MessageDatabase.MarkedMessageInfo> incomingMessagesViewed = setIncomingMessagesViewed(Collections.singletonList(Long.valueOf(j)));
        if (incomingMessagesViewed.isEmpty()) {
            return null;
        }
        return incomingMessagesViewed.get(0);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setIncomingMessagesViewed(List<Long> list) {
        Throwable th;
        if (list.isEmpty()) {
            return Collections.emptyList();
        }
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        String[] strArr = {"_id", "address", "date", MESSAGE_BOX, "thread_id"};
        String str = "_id IN (" + Util.join(list, ",") + ") AND " + MmsSmsColumns.VIEWED_RECEIPT_COUNT + " = 0";
        LinkedList linkedList = new LinkedList();
        signalWritableDatabase.beginTransaction();
        try {
            Cursor query = signalWritableDatabase.query("mms", strArr, str, null, null, null, null);
            while (query != null) {
                try {
                    if (!query.moveToNext()) {
                        break;
                    }
                    long requireLong = CursorUtil.requireLong(query, MESSAGE_BOX);
                    if (MmsSmsColumns.Types.isSecureType(requireLong) && MmsSmsColumns.Types.isInboxType(requireLong)) {
                        linkedList.add(new MessageDatabase.MarkedMessageInfo(CursorUtil.requireLong(query, "thread_id"), new MessageDatabase.SyncMessageId(RecipientId.from(CursorUtil.requireLong(query, "address")), CursorUtil.requireLong(query, "date")), new MessageId(CursorUtil.requireLong(query, "_id"), true), null));
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(MmsSmsColumns.VIEWED_RECEIPT_COUNT, (Integer) 1);
                        contentValues.put(MmsSmsColumns.RECEIPT_TIMESTAMP, Long.valueOf(System.currentTimeMillis()));
                        signalWritableDatabase.update("mms", contentValues, "_id = ?", SqlUtil.buildArgs(CursorUtil.requireLong(query, "_id")));
                    }
                } catch (Throwable th2) {
                    if (query != null) {
                        try {
                            query.close();
                        } catch (Throwable th3) {
                            th = th3;
                            signalWritableDatabase.endTransaction();
                            throw th;
                        }
                    }
                    throw th2;
                }
            }
            signalWritableDatabase.setTransactionSuccessful();
            if (query != null) {
                query.close();
            }
            signalWritableDatabase.endTransaction();
            notifyConversationListeners((Set) Stream.of(linkedList).map(new MmsDatabase$$ExternalSyntheticLambda8()).collect(Collectors.toSet()));
            notifyConversationListListeners();
            return linkedList;
        } catch (Throwable th4) {
            th = th4;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setOutgoingGiftsRevealed(List<Long> list) {
        boolean z = true;
        String[] buildArgs = SqlUtil.buildArgs("_id", "address", "date", "thread_id");
        String str = "_id IN (" + Util.join(list, ",") + ") AND (" + getOutgoingTypeClause() + ") AND (" + getTypeField() + " & " + MmsSmsColumns.Types.SPECIAL_TYPES_MASK + " = " + MmsSmsColumns.Types.SPECIAL_TYPE_GIFT_BADGE + ") AND " + MmsSmsColumns.VIEWED_RECEIPT_COUNT + " = 0";
        LinkedList linkedList = new LinkedList();
        getWritableDatabase().beginTransaction();
        try {
            Cursor query = getWritableDatabase().query("mms", buildArgs, str, null, null, null, null);
            while (query.moveToNext()) {
                try {
                    long requireLong = CursorUtil.requireLong(query, "_id");
                    linkedList.add(new MessageDatabase.MarkedMessageInfo(CursorUtil.requireLong(query, "thread_id"), new MessageDatabase.SyncMessageId(RecipientId.from(CursorUtil.requireLong(query, "address")), CursorUtil.requireLong(query, "date")), new MessageId(requireLong, z), null));
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(MmsSmsColumns.VIEWED_RECEIPT_COUNT, (Integer) 1);
                    contentValues.put(MmsSmsColumns.RECEIPT_TIMESTAMP, Long.valueOf(System.currentTimeMillis()));
                    getWritableDatabase().update("mms", contentValues, "_id = ?", SqlUtil.buildArgs(requireLong));
                    z = true;
                } catch (Throwable th) {
                    if (query != null) {
                        try {
                            query.close();
                        }
                    }
                    throw th;
                }
            }
            getWritableDatabase().setTransactionSuccessful();
            query.close();
            getWritableDatabase().endTransaction();
            notifyConversationListeners((Set) Stream.of(linkedList).map(new MmsDatabase$$ExternalSyntheticLambda8()).collect(Collectors.toSet()));
            return linkedList;
        } catch (Throwable th2) {
            getWritableDatabase().endTransaction();
            throw th2;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Pair<Long, Long> insertReceivedCall(RecipientId recipientId, boolean z) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Pair<Long, Long> insertOutgoingCall(RecipientId recipientId, boolean z) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Pair<Long, Long> insertMissedCall(RecipientId recipientId, long j, boolean z) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertOrUpdateGroupCall(RecipientId recipientId, RecipientId recipientId2, long j, String str, Collection<UUID> collection, boolean z) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertOrUpdateGroupCall(RecipientId recipientId, RecipientId recipientId2, long j, String str) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean updatePreviousGroupCall(long j, String str, Collection<UUID> collection, boolean z) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Optional<MessageDatabase.InsertResult> insertMessageInbox(IncomingTextMessage incomingTextMessage, long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Optional<MessageDatabase.InsertResult> insertMessageInbox(IncomingTextMessage incomingTextMessage) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public long insertMessageOutbox(long j, OutgoingTextMessage outgoingTextMessage, boolean z, long j2, MessageDatabase.InsertListener insertListener) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertProfileNameChangeMessages(Recipient recipient, String str, String str2) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertGroupV1MigrationEvents(RecipientId recipientId, long j, GroupMigrationMembershipChange groupMigrationMembershipChange) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertNumberChangeMessages(Recipient recipient) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertBoostRequestMessage(RecipientId recipientId, long j) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void endTransaction(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.endTransaction();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public SQLiteStatement createInsertStatement(SQLiteDatabase sQLiteDatabase) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void ensureMigration() {
        this.databaseHelper.getSignalWritableDatabase();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0034  */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isStory(long r11) {
        /*
            r10 = this;
            org.thoughtcrime.securesms.database.SignalDatabase r0 = r10.databaseHelper
            org.thoughtcrime.securesms.database.SQLiteDatabase r1 = r0.getSignalReadableDatabase()
            java.lang.String r0 = "1"
            java.lang.String[] r3 = new java.lang.String[]{r0}
            java.lang.String[] r5 = org.signal.core.util.SqlUtil.buildArgs(r11)
            java.lang.String r2 = "mms"
            java.lang.String r4 = "is_story > 0 AND remote_deleted = 0 AND _id = ?"
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "1"
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)
            if (r11 == 0) goto L_0x0031
            boolean r12 = r11.moveToFirst()     // Catch: all -> 0x0027
            if (r12 == 0) goto L_0x0031
            r12 = 1
            goto L_0x0032
        L_0x0027:
            r12 = move-exception
            r11.close()     // Catch: all -> 0x002c
            goto L_0x0030
        L_0x002c:
            r11 = move-exception
            r12.addSuppressed(r11)
        L_0x0030:
            throw r12
        L_0x0031:
            r12 = 0
        L_0x0032:
            if (r11 == 0) goto L_0x0037
            r11.close()
        L_0x0037:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.MmsDatabase.isStory(long):boolean");
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getOutgoingStoriesTo(RecipientId recipientId) {
        String[] strArr;
        String str;
        Long threadIdFor = Recipient.resolved(recipientId).isGroup() ? SignalDatabase.threads().getThreadIdFor(recipientId) : null;
        String str2 = "is_story > 0 AND remote_deleted = 0 AND (" + getOutgoingTypeClause() + ")";
        if (threadIdFor == null) {
            str = str2 + " AND address = ?";
            strArr = SqlUtil.buildArgs(recipientId);
        } else {
            strArr = SqlUtil.buildArgs(threadIdFor.longValue());
            str = str2 + " AND thread_id = ?";
        }
        return new Reader(rawQuery(str, strArr));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getAllOutgoingStories(boolean z, int i) {
        return new Reader(rawQuery("is_story > 0 AND remote_deleted = 0 AND (" + getOutgoingTypeClause() + ")", null, z, (long) i));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getAllOutgoingStoriesAt(long j) {
        return new Reader(rawQuery("is_story > 0 AND remote_deleted = 0 AND date = ? AND (" + getOutgoingTypeClause() + ")", SqlUtil.buildArgs(j), false, -1));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getAllStoriesFor(RecipientId recipientId, int i) {
        return new Reader(rawQuery("is_story > 0 AND remote_deleted = 0 AND thread_id = ?", SqlUtil.buildArgs(SignalDatabase.threads().getThreadIdIfExistsFor(recipientId)), false, (long) i));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.Reader getUnreadStories(RecipientId recipientId, int i) {
        long threadIdIfExistsFor = SignalDatabase.threads().getThreadIdIfExistsFor(recipientId);
        return new Reader(rawQuery("is_story > 0 AND remote_deleted = 0 AND NOT (" + getOutgoingTypeClause() + ")  AND thread_id = ? AND " + MmsSmsColumns.VIEWED_RECEIPT_COUNT + " = ?", SqlUtil.buildArgs(Long.valueOf(threadIdIfExistsFor), 0), false, (long) i));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public ParentStoryId.GroupReply getParentStoryIdForGroupReply(long j) {
        Cursor query = getReadableDatabase().query("mms", SqlUtil.buildArgs(PARENT_STORY_ID), "_id = ?", SqlUtil.buildArgs(j), null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    ParentStoryId deserialize = ParentStoryId.deserialize(CursorUtil.requireLong(query, PARENT_STORY_ID));
                    if (deserialize == null || !deserialize.isGroupReply()) {
                        query.close();
                        return null;
                    }
                    ParentStoryId.GroupReply groupReply = (ParentStoryId.GroupReply) deserialize;
                    query.close();
                    return groupReply;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return null;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public StoryViewState getStoryViewState(RecipientId recipientId) {
        if (!Stories.isFeatureEnabled()) {
            return StoryViewState.NONE;
        }
        return getStoryViewState(SignalDatabase.threads().getThreadIdIfExistsFor(recipientId));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void updateViewedStories(Set<MessageDatabase.SyncMessageId> set) {
        String join = Util.join((List<Long>) ((List) Collection$EL.stream(set).map(new Function() { // from class: org.thoughtcrime.securesms.database.MmsDatabase$$ExternalSyntheticLambda9
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Long.valueOf(((MessageDatabase.SyncMessageId) obj).getTimetamp());
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(j$.util.stream.Collectors.toList())), ",");
        String[] buildArgs = SqlUtil.buildArgs("address");
        String str = "is_story > 0 AND remote_deleted = 0 AND date IN (" + join + ") AND NOT (" + getOutgoingTypeClause() + ") AND " + MmsSmsColumns.VIEWED_RECEIPT_COUNT + " > 0";
        try {
            getWritableDatabase().beginTransaction();
            Cursor query = getWritableDatabase().query("mms", buildArgs, str, null, null, null, null);
            while (query != null && query.moveToNext()) {
                SignalDatabase.recipients().updateLastStoryViewTimestamp(Recipient.resolved(RecipientId.from(CursorUtil.requireLong(query, "address"))).getId());
            }
            if (query != null) {
                query.close();
            }
            getWritableDatabase().setTransactionSuccessful();
        } finally {
            getWritableDatabase().endTransaction();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0082, code lost:
        if (r5.getInt(0) == 1) goto L_0x0090;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    org.thoughtcrime.securesms.database.model.StoryViewState getStoryViewState(long r5) {
        /*
            r4 = this;
            java.lang.String[] r0 = org.signal.core.util.SqlUtil.buildArgs(r5)
            org.thoughtcrime.securesms.database.SQLiteDatabase r1 = r4.getReadableDatabase()
            java.lang.String r2 = "SELECT EXISTS(SELECT 1 FROM mms WHERE is_story > 0 AND remote_deleted = 0 AND thread_id = ? LIMIT 1)"
            android.database.Cursor r0 = r1.rawQuery(r2, r0)
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0030
            boolean r3 = r0.moveToFirst()     // Catch: all -> 0x0026
            if (r3 == 0) goto L_0x0030
            boolean r3 = r0.isNull(r2)     // Catch: all -> 0x0026
            if (r3 != 0) goto L_0x0030
            int r3 = r0.getInt(r2)     // Catch: all -> 0x0026
            if (r3 != r1) goto L_0x0030
            r3 = 1
            goto L_0x0031
        L_0x0026:
            r5 = move-exception
            r0.close()     // Catch: all -> 0x002b
            goto L_0x002f
        L_0x002b:
            r6 = move-exception
            r5.addSuppressed(r6)
        L_0x002f:
            throw r5
        L_0x0030:
            r3 = 0
        L_0x0031:
            if (r0 == 0) goto L_0x0036
            r0.close()
        L_0x0036:
            if (r3 != 0) goto L_0x003b
            org.thoughtcrime.securesms.database.model.StoryViewState r5 = org.thoughtcrime.securesms.database.model.StoryViewState.NONE
            return r5
        L_0x003b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "SELECT EXISTS(SELECT 1 FROM mms WHERE is_story > 0 AND remote_deleted = 0 AND thread_id = ? AND viewed_receipt_count = ? AND NOT ("
            r0.append(r3)
            java.lang.String r3 = r4.getOutgoingTypeClause()
            r0.append(r3)
            java.lang.String r3 = ") LIMIT 1)"
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r3[r2] = r5
            java.lang.Integer r5 = java.lang.Integer.valueOf(r2)
            r3[r1] = r5
            java.lang.String[] r5 = org.signal.core.util.SqlUtil.buildArgs(r3)
            org.thoughtcrime.securesms.database.SQLiteDatabase r6 = r4.getReadableDatabase()
            android.database.Cursor r5 = r6.rawQuery(r0, r5)
            if (r5 == 0) goto L_0x008f
            boolean r6 = r5.moveToFirst()     // Catch: all -> 0x0085
            if (r6 == 0) goto L_0x008f
            boolean r6 = r5.isNull(r2)     // Catch: all -> 0x0085
            if (r6 != 0) goto L_0x008f
            int r6 = r5.getInt(r2)     // Catch: all -> 0x0085
            if (r6 != r1) goto L_0x008f
            goto L_0x0090
        L_0x0085:
            r6 = move-exception
            r5.close()     // Catch: all -> 0x008a
            goto L_0x008e
        L_0x008a:
            r5 = move-exception
            r6.addSuppressed(r5)
        L_0x008e:
            throw r6
        L_0x008f:
            r1 = 0
        L_0x0090:
            if (r5 == 0) goto L_0x0095
            r5.close()
        L_0x0095:
            if (r1 == 0) goto L_0x009a
            org.thoughtcrime.securesms.database.model.StoryViewState r5 = org.thoughtcrime.securesms.database.model.StoryViewState.UNVIEWED
            return r5
        L_0x009a:
            org.thoughtcrime.securesms.database.model.StoryViewState r5 = org.thoughtcrime.securesms.database.model.StoryViewState.VIEWED
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.MmsDatabase.getStoryViewState(long):org.thoughtcrime.securesms.database.model.StoryViewState");
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean isOutgoingStoryAlreadyInDatabase(RecipientId recipientId, long j) {
        boolean z = false;
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{"COUNT(*)"}, "address = ? AND is_story > 0 AND date = ? AND (" + getOutgoingTypeClause() + ")", SqlUtil.buildArgs(recipientId, Long.valueOf(j)), null, null, null, SubscriptionLevels.BOOST_LEVEL);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    if (query.getInt(0) > 0) {
                        z = true;
                    }
                    query.close();
                    return z;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageId getStoryId(RecipientId recipientId, long j) throws NoSuchMessageException {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{"_id", "address"}, "is_story > 0 AND remote_deleted = 0 AND date = ?", SqlUtil.buildArgs(j), null, null, null, SubscriptionLevels.BOOST_LEVEL);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    RecipientId from = RecipientId.from(query.getLong(query.getColumnIndexOrThrow("address")));
                    if (Recipient.self().getId().equals(recipientId) || from.equals(recipientId)) {
                        MessageId messageId = new MessageId(CursorUtil.requireLong(query, "_id"), true);
                        query.close();
                        return messageId;
                    }
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        throw new NoSuchMessageException("No story sent at " + j);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<RecipientId> getUnreadStoryThreadRecipientIds() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor rawQuery = readableDatabase.rawQuery("SELECT DISTINCT thread_recipient_id\nFROM mms\nJOIN thread\nON mms.thread_id = thread._id\nWHERE is_story > 0 AND remote_deleted = 0 AND (" + getOutgoingTypeClause() + ") = 0 AND " + MmsSmsColumns.VIEWED_RECEIPT_COUNT + " = 0", (String[]) null);
        if (rawQuery != null) {
            try {
                ArrayList arrayList = new ArrayList(rawQuery.getCount());
                while (rawQuery.moveToNext()) {
                    arrayList.add(RecipientId.from(rawQuery.getLong(0)));
                }
                rawQuery.close();
                return arrayList;
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        } else {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return Collections.emptyList();
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<StoryResult> getOrderedStoryRecipientsAndIds(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("WHERE is_story > 0 AND remote_deleted = 0");
        sb.append(z ? " AND is_outgoing != 0" : "");
        sb.append("\n");
        String sb2 = sb.toString();
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor rawQuery = readableDatabase.rawQuery("SELECT\n mms.date AS sent_timestamp,\n mms._id AS mms_id,\n thread_recipient_id,\n (" + getOutgoingTypeClause() + ") AS is_outgoing,\n viewed_receipt_count,\n mms.date,\n receipt_timestamp,\n (" + getOutgoingTypeClause() + ") = 0 AND viewed_receipt_count = 0 AS is_unread\nFROM mms\nJOIN thread\nON mms.thread_id = thread._id\n" + sb2 + "ORDER BY\nis_unread DESC,\nCASE\nWHEN is_outgoing = 0 AND viewed_receipt_count = 0 THEN mms.date\nWHEN is_outgoing = 0 AND viewed_receipt_count > 0 THEN receipt_timestamp\nWHEN is_outgoing = 1 THEN mms.date\nEND DESC", (String[]) null);
        if (rawQuery != null) {
            try {
                ArrayList arrayList = new ArrayList(rawQuery.getCount());
                while (rawQuery.moveToNext()) {
                    arrayList.add(new StoryResult(RecipientId.from(CursorUtil.requireLong(rawQuery, ThreadDatabase.RECIPIENT_ID)), CursorUtil.requireLong(rawQuery, GroupReceiptDatabase.MMS_ID), CursorUtil.requireLong(rawQuery, StorySendsDatabase.SENT_TIMESTAMP), CursorUtil.requireBoolean(rawQuery, "is_outgoing")));
                }
                rawQuery.close();
                return arrayList;
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        } else {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return Collections.emptyList();
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Cursor getStoryReplies(long j) {
        return rawQuery("parent_story_id = ?", SqlUtil.buildArgs(j), false, 0);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int getNumberOfStoryReplies(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{"COUNT(*)"}, "parent_story_id = ?", SqlUtil.buildArgs(j), null, null, null, null);
        int i = 0;
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    i = query.getInt(0);
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0034  */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean containsStories(long r11) {
        /*
            r10 = this;
            org.thoughtcrime.securesms.database.SignalDatabase r0 = r10.databaseHelper
            org.thoughtcrime.securesms.database.SQLiteDatabase r1 = r0.getSignalReadableDatabase()
            java.lang.String r0 = "1"
            java.lang.String[] r3 = new java.lang.String[]{r0}
            java.lang.String[] r5 = org.signal.core.util.SqlUtil.buildArgs(r11)
            java.lang.String r2 = "mms"
            java.lang.String r4 = "thread_id = ? AND is_story > 0"
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "1"
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)
            if (r11 == 0) goto L_0x0031
            boolean r12 = r11.moveToNext()     // Catch: all -> 0x0027
            if (r12 == 0) goto L_0x0031
            r12 = 1
            goto L_0x0032
        L_0x0027:
            r12 = move-exception
            r11.close()     // Catch: all -> 0x002c
            goto L_0x0030
        L_0x002c:
            r11 = move-exception
            r12.addSuppressed(r11)
        L_0x0030:
            throw r12
        L_0x0031:
            r12 = 0
        L_0x0032:
            if (r11 == 0) goto L_0x0037
            r11.close()
        L_0x0037:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.MmsDatabase.containsStories(long):boolean");
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean hasSelfReplyInStory(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{"COUNT(*)"}, "parent_story_id = ? AND (" + getOutgoingTypeClause() + ")", SqlUtil.buildArgs(-j), null, null, null, null);
        boolean z = false;
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    if (query.getInt(0) > 0) {
                        z = true;
                    }
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return z;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean hasSelfReplyInGroupStory(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{"COUNT(*)"}, "parent_story_id = ? AND (" + getOutgoingTypeClause() + ") AND (" + MESSAGE_BOX + " & " + MmsSmsColumns.Types.SPECIAL_TYPES_MASK + " != " + MmsSmsColumns.Types.SPECIAL_TYPE_STORY_REACTION + ")", SqlUtil.buildArgs(j), null, null, null, null);
        boolean z = false;
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    if (query.getInt(0) > 0) {
                        z = true;
                    }
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Long getOldestStorySendTimestamp(boolean r12) {
        /*
            r11 = this;
            long r0 = getReleaseChannelThreadId(r12)
            org.thoughtcrime.securesms.database.SignalDatabase r12 = r11.databaseHelper
            org.thoughtcrime.securesms.database.SQLiteDatabase r2 = r12.getSignalReadableDatabase()
            java.lang.String r12 = "date"
            java.lang.String[] r4 = new java.lang.String[]{r12}
            java.lang.String[] r6 = org.signal.core.util.SqlUtil.buildArgs(r0)
            java.lang.String r3 = "mms"
            java.lang.String r5 = "is_story > 0 AND remote_deleted = 0 AND thread_id != ?"
            r7 = 0
            r8 = 0
            java.lang.String r9 = "date ASC"
            java.lang.String r10 = "1"
            android.database.Cursor r12 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)
            if (r12 == 0) goto L_0x003e
            boolean r0 = r12.moveToNext()     // Catch: all -> 0x0034
            if (r0 == 0) goto L_0x003e
            r0 = 0
            long r0 = r12.getLong(r0)     // Catch: all -> 0x0034
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch: all -> 0x0034
            goto L_0x003f
        L_0x0034:
            r0 = move-exception
            r12.close()     // Catch: all -> 0x0039
            goto L_0x003d
        L_0x0039:
            r12 = move-exception
            r0.addSuppressed(r12)
        L_0x003d:
            throw r0
        L_0x003e:
            r0 = 0
        L_0x003f:
            if (r12 == 0) goto L_0x0044
            r12.close()
        L_0x0044:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.MmsDatabase.getOldestStorySendTimestamp(boolean):java.lang.Long");
    }

    private static long getReleaseChannelThreadId(boolean z) {
        RecipientId releaseChannelRecipientId;
        Long threadIdFor;
        if (z || (releaseChannelRecipientId = SignalStore.releaseChannelValues().getReleaseChannelRecipientId()) == null || (threadIdFor = SignalDatabase.threads().getThreadIdFor(releaseChannelRecipientId)) == null) {
            return -1;
        }
        return threadIdFor.longValue();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void deleteGroupStoryReplies(long j) {
        this.databaseHelper.getSignalWritableDatabase().delete("mms", "parent_story_id = ?", SqlUtil.buildArgs(j));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int deleteStoriesOlderThan(long j, boolean z) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            String[] buildArgs = SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(getReleaseChannelThreadId(z)));
            signalWritableDatabase.execSQL("DELETE FROM mms WHERE parent_story_id > 0 AND parent_story_id IN (SELECT _id FROM mms WHERE is_story > 0 AND remote_deleted = 0 AND date < ? AND thread_id != ?)", buildArgs);
            signalWritableDatabase.execSQL("UPDATE mms SET quote_missing = 1, quote_body = '' WHERE parent_story_id < 0 AND ABS(parent_story_id) IN (SELECT _id FROM mms WHERE is_story > 0 AND remote_deleted = 0 AND date < ? AND thread_id != ?)", buildArgs);
            Cursor query = signalWritableDatabase.query("mms", new String[]{"address"}, "is_story > 0 AND remote_deleted = 0 AND date < ? AND thread_id != ?", buildArgs, null, null, null);
            while (query != null && query.moveToNext()) {
                ApplicationDependencies.getDatabaseObserver().notifyStoryObservers(RecipientId.from(query.getLong(query.getColumnIndexOrThrow("address"))));
            }
            if (query != null) {
                query.close();
            }
            Cursor query2 = signalWritableDatabase.query("mms", new String[]{"_id"}, "is_story > 0 AND remote_deleted = 0 AND date < ? AND thread_id != ?", buildArgs, null, null, null);
            int count = query2.getCount();
            while (query2.moveToNext()) {
                deleteMessage(query2.getLong(query2.getColumnIndexOrThrow("_id")));
            }
            query2.close();
            signalWritableDatabase.setTransactionSuccessful();
            return count;
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean isGroupQuitMessage(long j) {
        long outgoingEncryptedMessageType = MmsSmsColumns.Types.getOutgoingEncryptedMessageType() | 131072;
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{"_id"}, "_id = ? AND msg_box & " + outgoingEncryptedMessageType + " = " + outgoingEncryptedMessageType + " AND " + MESSAGE_BOX + " & 524288 = 0", SqlUtil.buildArgs(j), null, null, null, null);
        try {
            if (query.getCount() == 1) {
                query.close();
                return true;
            }
            query.close();
            return false;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public long getLatestGroupQuitTimestamp(long j, long j2) {
        long outgoingEncryptedMessageType = MmsSmsColumns.Types.getOutgoingEncryptedMessageType() | 131072;
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{"date"}, "thread_id = ? AND msg_box & " + outgoingEncryptedMessageType + " = " + outgoingEncryptedMessageType + " AND " + MESSAGE_BOX + " & 524288 = 0 AND date < ?", new String[]{String.valueOf(j), String.valueOf(j2)}, null, null, "date DESC", SubscriptionLevels.BOOST_LEVEL);
        try {
            if (query.moveToFirst()) {
                long requireLong = CursorUtil.requireLong(query, "date");
                query.close();
                return requireLong;
            }
            query.close();
            return -1;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int getMessageCountForThread(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", Database.COUNT, "thread_id = ? AND is_story = ? AND parent_story_id <= ?", SqlUtil.buildArgs(Long.valueOf(j), 0, 0), null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int getMessageCountForThread(long j, long j2) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", Database.COUNT, "thread_id = ? AND date_received < ? AND is_story = ? AND parent_story_id <= ?", SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(j2), 0, 0), null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean hasMeaningfulMessage(long j) {
        boolean z = false;
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{SubscriptionLevels.BOOST_LEVEL}, "thread_id = ? AND is_story = ? AND parent_story_id <= ?", SqlUtil.buildArgs(Long.valueOf(j), 0, 0), null, null, null, SubscriptionLevels.BOOST_LEVEL);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    z = true;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return z;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void addFailures(long j, List<NetworkFailure> list) {
        try {
            addToDocument(j, NETWORK_FAILURE, (List) list, NetworkFailureSet.class);
        } catch (IOException e) {
            Log.w(TAG, e);
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void setNetworkFailures(long j, Set<NetworkFailure> set) {
        try {
            setDocument(this.databaseHelper.getSignalWritableDatabase(), j, NETWORK_FAILURE, new NetworkFailureSet(set));
        } catch (IOException e) {
            Log.w(TAG, e);
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Set<MessageDatabase.MessageUpdate> incrementReceiptCount(MessageDatabase.SyncMessageId syncMessageId, long j, MessageDatabase.ReceiptType receiptType, boolean z) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        HashSet hashSet = new HashSet();
        String str6 = "_id";
        String str7 = "thread_id";
        String str8 = MESSAGE_BOX;
        String str9 = "address";
        SelectBuilderPart2 from = SQLiteDatabaseExtensionsKt.select(signalWritableDatabase, str6, str7, str8, str9, receiptType.getColumnName(), MmsSmsColumns.RECEIPT_TIMESTAMP).from("mms");
        StringBuilder sb = new StringBuilder();
        sb.append("date = ?");
        sb.append(z ? " AND is_story > 0 AND remote_deleted = 0" : "");
        Cursor run = from.where(sb.toString(), Long.valueOf(syncMessageId.getTimetamp())).run();
        while (run.moveToNext()) {
            try {
                if (MmsSmsColumns.Types.isOutgoingMessageType(CursorUtil.requireLong(run, str8))) {
                    RecipientId from2 = RecipientId.from(CursorUtil.requireLong(run, str9));
                    RecipientId recipientId = syncMessageId.getRecipientId();
                    String columnName = receiptType.getColumnName();
                    if (!recipientId.equals(from2) && !Recipient.resolved(from2).isGroup()) {
                        str4 = str6;
                        str = str7;
                        str3 = str8;
                        str2 = str9;
                        str6 = str4;
                        str8 = str3;
                        str9 = str2;
                        str7 = str;
                    }
                    str3 = str8;
                    long requireLong = CursorUtil.requireLong(run, str6);
                    str2 = str9;
                    long requireLong2 = CursorUtil.requireLong(run, str7);
                    int groupStatus = receiptType.getGroupStatus();
                    boolean z2 = CursorUtil.requireLong(run, columnName) == 0;
                    str = str7;
                    long requireLong3 = CursorUtil.requireLong(run, MmsSmsColumns.RECEIPT_TIMESTAMP);
                    if (z2) {
                        str5 = str6;
                        requireLong3 = Math.max(requireLong3, j);
                    } else {
                        str5 = str6;
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("UPDATE mms SET ");
                    sb2.append(columnName);
                    sb2.append(" = ");
                    sb2.append(columnName);
                    sb2.append(" + 1, ");
                    sb2.append(MmsSmsColumns.RECEIPT_TIMESTAMP);
                    sb2.append(" = ? WHERE ");
                    str4 = str5;
                    sb2.append(str4);
                    sb2.append(" = ?");
                    signalWritableDatabase.execSQL(sb2.toString(), SqlUtil.buildArgs(Long.valueOf(requireLong3), Long.valueOf(requireLong)));
                    SignalDatabase.groupReceipts().update(recipientId, requireLong, groupStatus, j);
                    hashSet.add(new MessageDatabase.MessageUpdate(requireLong2, new MessageId(requireLong, true)));
                    str6 = str4;
                    str8 = str3;
                    str9 = str2;
                    str7 = str;
                } else {
                    str7 = str7;
                }
            } catch (Throwable th) {
                if (run != null) {
                    try {
                        run.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        if (hashSet.size() > 0 && receiptType == MessageDatabase.ReceiptType.DELIVERY) {
            this.earlyDeliveryReceiptCache.increment(syncMessageId.getTimetamp(), syncMessageId.getRecipientId(), j);
        }
        run.close();
        hashSet.addAll(incrementStoryReceiptCount(syncMessageId, j, receiptType));
        return hashSet;
    }

    private Set<MessageDatabase.MessageUpdate> incrementStoryReceiptCount(MessageDatabase.SyncMessageId syncMessageId, long j, MessageDatabase.ReceiptType receiptType) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        HashSet hashSet = new HashSet();
        String columnName = receiptType.getColumnName();
        for (MessageId messageId : SignalDatabase.storySends().getStoryMessagesFor(syncMessageId)) {
            signalWritableDatabase.execSQL("UPDATE mms SET " + columnName + " = " + columnName + " + 1, " + MmsSmsColumns.RECEIPT_TIMESTAMP + " = CASE WHEN " + columnName + " = 0 THEN MAX(" + MmsSmsColumns.RECEIPT_TIMESTAMP + ", ?) ELSE " + MmsSmsColumns.RECEIPT_TIMESTAMP + " END WHERE _id = ?", SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(messageId.getId())));
            SignalDatabase.groupReceipts().update(syncMessageId.getRecipientId(), messageId.getId(), receiptType.getGroupStatus(), j);
            hashSet.add(new MessageDatabase.MessageUpdate(-1, messageId));
        }
        return hashSet;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public long getThreadIdForMessage(long j) {
        Throwable th;
        Cursor cursor;
        try {
            cursor = this.databaseHelper.getSignalReadableDatabase().rawQuery("SELECT thread_id FROM mms WHERE _id = ?", new String[]{j + ""});
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        long j2 = cursor.getLong(0);
                        cursor.close();
                        return j2;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return -1;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    private long getThreadIdFor(IncomingMediaMessage incomingMediaMessage) {
        if (incomingMediaMessage.getGroupId() != null) {
            return SignalDatabase.threads().getOrCreateThreadIdFor(Recipient.resolved(SignalDatabase.recipients().getOrInsertFromPossiblyMigratedGroupId(incomingMediaMessage.getGroupId())));
        }
        return SignalDatabase.threads().getOrCreateThreadIdFor(Recipient.resolved(incomingMediaMessage.getFrom()));
    }

    private long getThreadIdFor(NotificationInd notificationInd) {
        return SignalDatabase.threads().getOrCreateThreadIdFor(Recipient.external(this.context, (notificationInd.getFrom() == null || notificationInd.getFrom().getTextString() == null) ? "" : Util.toIsoString(notificationInd.getFrom().getTextString())));
    }

    private Cursor rawQuery(String str, String[] strArr) {
        return rawQuery(str, strArr, false, 0);
    }

    private Cursor rawQuery(String str, String[] strArr, boolean z, long j) {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        String str2 = "SELECT " + Util.join(MMS_PROJECTION, ",") + " FROM mms LEFT OUTER JOIN " + AttachmentDatabase.TABLE_NAME + " ON (mms._id = " + AttachmentDatabase.TABLE_NAME + "." + AttachmentDatabase.MMS_ID + ") WHERE " + str + " GROUP BY mms._id";
        if (z) {
            str2 = str2 + " ORDER BY mms._id DESC";
        }
        if (j > 0) {
            str2 = str2 + " LIMIT " + j;
        }
        return signalReadableDatabase.rawQuery(str2, strArr);
    }

    private Cursor internalGetMessage(long j) {
        return rawQuery(RAW_ID_WHERE, new String[]{j + ""});
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageRecord getMessageRecord(long j) throws NoSuchMessageException {
        Cursor rawQuery = rawQuery(RAW_ID_WHERE, new String[]{j + ""});
        try {
            MessageRecord next = new Reader(rawQuery).getNext();
            if (next != null) {
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return next;
            }
            throw new NoSuchMessageException("No message for ID: " + j);
        } catch (Throwable th) {
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageRecord getMessageRecordOrNull(long j) {
        Cursor rawQuery = rawQuery(RAW_ID_WHERE, new String[]{j + ""});
        try {
            MessageRecord next = new Reader(rawQuery).getNext();
            if (rawQuery != null) {
                rawQuery.close();
            }
            return next;
        } catch (Throwable th) {
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Reader getMessages(Collection<Long> collection) {
        String join = TextUtils.join(",", collection);
        return readerFor(rawQuery("mms._id IN (" + join + ")", null));
    }

    private void updateMailboxBitmask(long j, long j2, long j3, Optional<Long> optional) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            signalWritableDatabase.execSQL("UPDATE mms SET msg_box = (msg_box & " + (68719476735L - j2) + " | " + j3 + " ) WHERE _id = ?", new String[]{j + ""});
            if (optional.isPresent()) {
                SignalDatabase.threads().updateSnippetTypeSilently(optional.get().longValue());
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsOutbox(long j) {
        updateMailboxBitmask(j, 31, 21, Optional.of(Long.valueOf(getThreadIdForMessage(j))));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsForcedSms(long j) {
        updateMailboxBitmask(j, 2097152, 64, Optional.of(Long.valueOf(getThreadIdForMessage(j))));
        ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(new MessageId(j, true));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsRateLimited(long j) {
        updateMailboxBitmask(j, 0, 128, Optional.of(Long.valueOf(getThreadIdForMessage(j))));
        ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(new MessageId(j, true));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void clearRateLimitStatus(Collection<Long> collection) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            for (Long l : collection) {
                long longValue = l.longValue();
                updateMailboxBitmask(longValue, 128, 0, Optional.of(Long.valueOf(getThreadIdForMessage(longValue))));
            }
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsPendingInsecureSmsFallback(long j) {
        updateMailboxBitmask(j, 31, 26, Optional.of(Long.valueOf(getThreadIdForMessage(j))));
        ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(new MessageId(j, true));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsSending(long j) {
        updateMailboxBitmask(j, 31, 22, Optional.of(Long.valueOf(getThreadIdForMessage(j))));
        ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(new MessageId(j, true));
        ApplicationDependencies.getDatabaseObserver().notifyConversationListListeners();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsSentFailed(long j) {
        updateMailboxBitmask(j, 31, 24, Optional.of(Long.valueOf(getThreadIdForMessage(j))));
        ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(new MessageId(j, true));
        ApplicationDependencies.getDatabaseObserver().notifyConversationListListeners();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsSent(long j, boolean z) {
        updateMailboxBitmask(j, 31, (z ? 10485760 : 0) | 23, Optional.of(Long.valueOf(getThreadIdForMessage(j))));
        ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(new MessageId(j, true));
        ApplicationDependencies.getDatabaseObserver().notifyConversationListListeners();
    }

    /* JADX INFO: finally extract failed */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsRemoteDelete(long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(MmsSmsColumns.REMOTE_DELETED, (Integer) 1);
            contentValues.putNull("body");
            contentValues.putNull(QUOTE_BODY);
            contentValues.putNull(QUOTE_AUTHOR);
            contentValues.putNull(QUOTE_ATTACHMENT);
            contentValues.putNull(QUOTE_TYPE);
            contentValues.putNull(QUOTE_ID);
            contentValues.putNull(LINK_PREVIEWS);
            contentValues.putNull(SHARED_CONTACTS);
            signalWritableDatabase.update("mms", contentValues, "_id = ?", new String[]{String.valueOf(j)});
            boolean deleteAttachmentsForMessage = SignalDatabase.attachments().deleteAttachmentsForMessage(j);
            SignalDatabase.mentions().deleteMentionsForMessage(j);
            SignalDatabase.messageLog().deleteAllRelatedToMessage(j, true);
            SignalDatabase.reactions().deleteReactions(new MessageId(j, true));
            deleteGroupStoryReplies(j);
            SignalDatabase.threads().update(getThreadIdForMessage(j), false);
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(new MessageId(j, true));
            ApplicationDependencies.getDatabaseObserver().notifyConversationListListeners();
            if (deleteAttachmentsForMessage) {
                ApplicationDependencies.getDatabaseObserver().notifyAttachmentObservers();
            }
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markDownloadState(long j, long j2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(STATUS, Long.valueOf(j2));
        signalWritableDatabase.update("mms", contentValues, "_id = ?", new String[]{j + ""});
        ApplicationDependencies.getDatabaseObserver().notifyMessageUpdateObservers(new MessageId(j, true));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsInsecure(long j) {
        updateMailboxBitmask(j, 8388608, 0, Optional.empty());
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markUnidentified(long j, boolean z) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MmsSmsColumns.UNIDENTIFIED, Integer.valueOf(z ? 1 : 0));
        this.databaseHelper.getSignalWritableDatabase().update("mms", contentValues, "_id = ?", new String[]{String.valueOf(j)});
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markExpireStarted(long j) {
        markExpireStarted(j, System.currentTimeMillis());
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markExpireStarted(long j, long j2) {
        markExpireStarted(Collections.singleton(Long.valueOf(j)), j2);
    }

    /* JADX INFO: finally extract failed */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markExpireStarted(Collection<Long> collection, long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            long j2 = -1;
            for (Long l : collection) {
                long longValue = l.longValue();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MmsSmsColumns.EXPIRE_STARTED, Long.valueOf(j));
                signalWritableDatabase.update("mms", contentValues, "_id = ? AND (expire_started = 0 OR expire_started > ?)", new String[]{String.valueOf(longValue), String.valueOf(j)});
                if (j2 < 0) {
                    j2 = getThreadIdForMessage(longValue);
                }
            }
            signalWritableDatabase.setTransactionSuccessful();
            signalWritableDatabase.endTransaction();
            SignalDatabase.threads().update(j2, false);
            notifyConversationListeners(j2);
        } catch (Throwable th) {
            signalWritableDatabase.endTransaction();
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markAsNotified(long j) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MmsSmsColumns.NOTIFIED, (Integer) 1);
        contentValues.put(MmsSmsColumns.REACTIONS_LAST_SEEN, Long.valueOf(System.currentTimeMillis()));
        signalWritableDatabase.update("mms", contentValues, "_id = ?", new String[]{String.valueOf(j)});
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setMessagesReadSince(long j, long j2) {
        if (j2 == -1) {
            return setMessagesRead("thread_id = ? AND is_story = 0 AND parent_story_id <= 0 AND (read = 0 OR (reactions_unread = 1 AND (" + getOutgoingTypeClause() + ")))", new String[]{String.valueOf(j)});
        }
        return setMessagesRead("thread_id = ? AND is_story = 0 AND parent_story_id <= 0 AND (read = 0 OR (reactions_unread = 1 AND ( " + getOutgoingTypeClause() + " ))) AND date_received <= ?", new String[]{String.valueOf(j), String.valueOf(j2)});
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setGroupStoryMessagesReadSince(long j, long j2, long j3) {
        if (j3 == -1) {
            return setMessagesRead("thread_id = ? AND is_story = 0 AND parent_story_id = ? AND (read = 0 OR (reactions_unread = 1 AND (" + getOutgoingTypeClause() + ")))", SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(j2)));
        }
        return setMessagesRead("thread_id = ? AND is_story = 0 AND parent_story_id = ? AND (read = 0 OR (reactions_unread = 1 AND ( " + getOutgoingTypeClause() + " ))) AND date_received <= ?", SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(j2), Long.valueOf(j3)));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setEntireThreadRead(long j) {
        return setMessagesRead("thread_id = ? AND is_story = 0 AND parent_story_id <= 0", new String[]{String.valueOf(j)});
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageDatabase.MarkedMessageInfo> setAllMessagesRead() {
        return setMessagesRead("is_story = 0 AND parent_story_id <= 0 AND (read = 0 OR (reactions_unread = 1 AND (" + getOutgoingTypeClause() + ")))", null);
    }

    private List<MessageDatabase.MarkedMessageInfo> setMessagesRead(String str, String[] strArr) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        LinkedList linkedList = new LinkedList();
        RecipientId releaseChannelRecipientId = SignalStore.releaseChannelValues().getReleaseChannelRecipientId();
        signalWritableDatabase.beginTransaction();
        Cursor cursor = null;
        try {
            cursor = signalWritableDatabase.query("mms", new String[]{"_id", "address", "date", MESSAGE_BOX, "expires_in", MmsSmsColumns.EXPIRE_STARTED, "thread_id"}, str, strArr, null, null, null);
            while (cursor != null && cursor.moveToNext()) {
                if (MmsSmsColumns.Types.isSecureType(CursorUtil.requireLong(cursor, MESSAGE_BOX))) {
                    long requireLong = CursorUtil.requireLong(cursor, "thread_id");
                    RecipientId from = RecipientId.from(CursorUtil.requireLong(cursor, "address"));
                    long requireLong2 = CursorUtil.requireLong(cursor, "date");
                    long requireLong3 = CursorUtil.requireLong(cursor, "_id");
                    long requireLong4 = CursorUtil.requireLong(cursor, "expires_in");
                    long requireLong5 = CursorUtil.requireLong(cursor, MmsSmsColumns.EXPIRE_STARTED);
                    MessageDatabase.SyncMessageId syncMessageId = new MessageDatabase.SyncMessageId(from, requireLong2);
                    MessageDatabase.ExpirationInfo expirationInfo = new MessageDatabase.ExpirationInfo(requireLong3, requireLong4, requireLong5, true);
                    if (!from.equals(releaseChannelRecipientId)) {
                        linkedList.add(new MessageDatabase.MarkedMessageInfo(requireLong, syncMessageId, new MessageId(requireLong3, true), expirationInfo));
                    }
                }
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("read", (Integer) 1);
            contentValues.put(MmsSmsColumns.REACTIONS_UNREAD, (Integer) 0);
            contentValues.put(MmsSmsColumns.REACTIONS_LAST_SEEN, Long.valueOf(System.currentTimeMillis()));
            signalWritableDatabase.update("mms", contentValues, str, strArr);
            signalWritableDatabase.setTransactionSuccessful();
            return linkedList;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            signalWritableDatabase.endTransaction();
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MmsSmsDatabase.TimestampReadResult setTimestampRead(MessageDatabase.SyncMessageId syncMessageId, long j, Map<Long, Long> map) {
        long j2;
        long j3;
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        LinkedList linkedList = new LinkedList();
        String[] strArr = {"_id", "thread_id", MESSAGE_BOX, "expires_in", MmsSmsColumns.EXPIRE_STARTED, "address"};
        String[] buildArgs = SqlUtil.buildArgs(syncMessageId.getTimetamp());
        LinkedList linkedList2 = new LinkedList();
        Cursor query = signalWritableDatabase.query("mms", strArr, "date = ?", buildArgs, null, null, null);
        while (query.moveToNext()) {
            try {
                RecipientId from = RecipientId.from(query.getLong(query.getColumnIndexOrThrow("address")));
                RecipientId recipientId = syncMessageId.getRecipientId();
                if (recipientId.equals(from) || Recipient.resolved(from).isGroup() || recipientId.equals(Recipient.self().getId())) {
                    long j4 = query.getLong(query.getColumnIndexOrThrow("_id"));
                    long j5 = query.getLong(query.getColumnIndexOrThrow("thread_id"));
                    long j6 = query.getLong(query.getColumnIndexOrThrow("expires_in"));
                    long j7 = query.getLong(query.getColumnIndexOrThrow(MmsSmsColumns.EXPIRE_STARTED));
                    if (j7 > 0) {
                        j2 = j5;
                        j3 = Math.min(j, j7);
                    } else {
                        j2 = j5;
                        j3 = j;
                    }
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("read", (Integer) 1);
                    contentValues.put(MmsSmsColumns.REACTIONS_UNREAD, (Integer) 0);
                    contentValues.put(MmsSmsColumns.REACTIONS_LAST_SEEN, Long.valueOf(System.currentTimeMillis()));
                    if (j6 > 0) {
                        contentValues.put(MmsSmsColumns.EXPIRE_STARTED, Long.valueOf(j3));
                        linkedList.add(new Pair(Long.valueOf(j4), Long.valueOf(j6)));
                    }
                    signalWritableDatabase.update("mms", contentValues, "_id = ?", SqlUtil.buildArgs(j4));
                    linkedList2.add(Long.valueOf(j2));
                    Long l = map.get(Long.valueOf(j2));
                    map.put(Long.valueOf(j2), Long.valueOf(l != null ? Math.max(l.longValue(), syncMessageId.getTimetamp()) : syncMessageId.getTimetamp()));
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return new MmsSmsDatabase.TimestampReadResult(linkedList, linkedList2);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Pair<RecipientId, Long> getOldestUnreadMentionDetails(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{"address", "date_received"}, "thread_id = ? AND read = 0 AND mentions_self = 1", SqlUtil.buildArgs(j), null, null, "date_received ASC", SubscriptionLevels.BOOST_LEVEL);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    Pair<RecipientId, Long> pair = new Pair<>(RecipientId.from(CursorUtil.requireString(query, "address")), Long.valueOf(CursorUtil.requireLong(query, "date_received")));
                    query.close();
                    return pair;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query == null) {
            return null;
        }
        query.close();
        return null;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int getUnreadMentionCount(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{"COUNT(*)"}, "thread_id = ? AND read = 0 AND mentions_self = 1", SqlUtil.buildArgs(j), null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    public void trimEntriesForExpiredMessages() {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.delete(GroupReceiptDatabase.TABLE_NAME, GroupReceiptDatabase.MMS_ID + " NOT IN (SELECT _id FROM mms)", (String[]) null);
        String[] strArr = {"_id", AttachmentDatabase.UNIQUE_ID};
        Cursor query = signalWritableDatabase.query(AttachmentDatabase.TABLE_NAME, strArr, AttachmentDatabase.MMS_ID + " NOT IN (SELECT _id FROM mms)", null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                SignalDatabase.attachments().deleteAttachment(new AttachmentId(query.getLong(0), query.getLong(1)));
            } finally {
            }
        }
        if (query != null) {
            query.close();
        }
        SignalDatabase.mentions().deleteAbandonedMentions();
        query = signalWritableDatabase.query(ThreadDatabase.TABLE_NAME, new String[]{"_id"}, "expires_in > 0", null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                SignalDatabase.threads().setLastScrolled(query.getLong(0), 0);
                SignalDatabase.threads().update(query.getLong(0), false);
            } finally {
            }
        }
        if (query != null) {
            query.close();
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Optional<MessageDatabase.MmsNotificationInfo> getNotification(long j) {
        Cursor cursor = null;
        try {
            cursor = rawQuery(RAW_ID_WHERE, new String[]{String.valueOf(j)});
            if (cursor == null || !cursor.moveToNext()) {
                return Optional.empty();
            }
            Optional<MessageDatabase.MmsNotificationInfo> of = Optional.of(new MessageDatabase.MmsNotificationInfo(RecipientId.from(cursor.getLong(cursor.getColumnIndexOrThrow("address"))), cursor.getString(cursor.getColumnIndexOrThrow(CONTENT_LOCATION)), cursor.getString(cursor.getColumnIndexOrThrow(TRANSACTION_ID)), cursor.getInt(cursor.getColumnIndexOrThrow(MmsSmsColumns.SUBSCRIPTION_ID))));
            cursor.close();
            return of;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x0207 A[Catch: IOException -> 0x029c, all -> 0x0299, TryCatch #0 {all -> 0x0299, blocks: (B:5:0x001e, B:7:0x0024, B:11:0x0073, B:15:0x00f0, B:19:0x01ab, B:21:0x01b1, B:23:0x01b7, B:25:0x01cb, B:27:0x01d1, B:30:0x01e1, B:32:0x01e8, B:34:0x01ee, B:37:0x01fe, B:40:0x0207, B:42:0x020d, B:44:0x0213, B:47:0x0239, B:49:0x023f, B:53:0x024c, B:55:0x0252, B:57:0x025f, B:59:0x0275, B:64:0x0282, B:65:0x0298), top: B:80:0x001e }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x023f A[Catch: IOException -> 0x029c, all -> 0x0299, TRY_LEAVE, TryCatch #0 {all -> 0x0299, blocks: (B:5:0x001e, B:7:0x0024, B:11:0x0073, B:15:0x00f0, B:19:0x01ab, B:21:0x01b1, B:23:0x01b7, B:25:0x01cb, B:27:0x01d1, B:30:0x01e1, B:32:0x01e8, B:34:0x01ee, B:37:0x01fe, B:40:0x0207, B:42:0x020d, B:44:0x0213, B:47:0x0239, B:49:0x023f, B:53:0x024c, B:55:0x0252, B:57:0x025f, B:59:0x0275, B:64:0x0282, B:65:0x0298), top: B:80:0x001e }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x024a  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x02ad  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01ee A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.thoughtcrime.securesms.mms.OutgoingMediaMessage getOutgoingMessage(long r40) throws org.thoughtcrime.securesms.mms.MmsException, org.thoughtcrime.securesms.database.NoSuchMessageException {
        /*
        // Method dump skipped, instructions count: 689
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.MmsDatabase.getOutgoingMessage(long):org.thoughtcrime.securesms.mms.OutgoingMediaMessage");
    }

    public static /* synthetic */ boolean lambda$getOutgoingMessage$2(LinkPreview linkPreview) {
        return linkPreview.getThumbnail().isPresent();
    }

    public static /* synthetic */ Attachment lambda$getOutgoingMessage$3(LinkPreview linkPreview) {
        return linkPreview.getThumbnail().get();
    }

    public static List<Contact> getSharedContacts(Cursor cursor, List<DatabaseAttachment> list) {
        String string = cursor.getString(cursor.getColumnIndexOrThrow(SHARED_CONTACTS));
        if (TextUtils.isEmpty(string)) {
            return Collections.emptyList();
        }
        HashMap hashMap = new HashMap();
        for (DatabaseAttachment databaseAttachment : list) {
            hashMap.put(databaseAttachment.getAttachmentId(), databaseAttachment);
        }
        try {
            LinkedList linkedList = new LinkedList();
            JSONArray jSONArray = new JSONArray(string);
            for (int i = 0; i < jSONArray.length(); i++) {
                Contact deserialize = Contact.deserialize(jSONArray.getJSONObject(i).toString());
                if (deserialize.getAvatar() == null || deserialize.getAvatar().getAttachmentId() == null) {
                    linkedList.add(deserialize);
                } else {
                    linkedList.add(new Contact(deserialize, new Contact.Avatar(deserialize.getAvatar().getAttachmentId(), (DatabaseAttachment) hashMap.get(deserialize.getAvatar().getAttachmentId()), deserialize.getAvatar().isProfile())));
                }
            }
            return linkedList;
        } catch (IOException | JSONException e) {
            Log.w(TAG, "Failed to parse shared contacts.", e);
            return Collections.emptyList();
        }
    }

    public static List<LinkPreview> getLinkPreviews(Cursor cursor, List<DatabaseAttachment> list) {
        String string = cursor.getString(cursor.getColumnIndexOrThrow(LINK_PREVIEWS));
        if (TextUtils.isEmpty(string)) {
            return Collections.emptyList();
        }
        HashMap hashMap = new HashMap();
        for (DatabaseAttachment databaseAttachment : list) {
            hashMap.put(databaseAttachment.getAttachmentId(), databaseAttachment);
        }
        try {
            LinkedList linkedList = new LinkedList();
            JSONArray jSONArray = new JSONArray(string);
            for (int i = 0; i < jSONArray.length(); i++) {
                LinkPreview deserialize = LinkPreview.deserialize(jSONArray.getJSONObject(i).toString());
                if (deserialize.getAttachmentId() != null) {
                    DatabaseAttachment databaseAttachment2 = (DatabaseAttachment) hashMap.get(deserialize.getAttachmentId());
                    if (databaseAttachment2 != null) {
                        linkedList.add(new LinkPreview(deserialize.getUrl(), deserialize.getTitle(), deserialize.getDescription(), deserialize.getDate(), databaseAttachment2));
                    } else {
                        linkedList.add(deserialize);
                    }
                } else {
                    linkedList.add(deserialize);
                }
            }
            return linkedList;
        } catch (IOException | JSONException e) {
            Log.w(TAG, "Failed to parse shared contacts.", e);
            return Collections.emptyList();
        }
    }

    private Optional<MessageDatabase.InsertResult> insertMessageInbox(IncomingMediaMessage incomingMediaMessage, String str, long j, long j2) throws MmsException {
        long j3;
        if (j == -1 || incomingMediaMessage.isGroupMessage()) {
            j3 = getThreadIdFor(incomingMediaMessage);
        } else {
            j3 = j;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", Long.valueOf(incomingMediaMessage.getSentTimeMillis()));
        contentValues.put(MmsSmsColumns.DATE_SERVER, Long.valueOf(incomingMediaMessage.getServerTimeMillis()));
        contentValues.put("address", incomingMediaMessage.getFrom().serialize());
        contentValues.put(MESSAGE_BOX, Long.valueOf(j2));
        contentValues.put(MESSAGE_TYPE, (Integer) 132);
        contentValues.put("thread_id", Long.valueOf(j3));
        contentValues.put(CONTENT_LOCATION, str);
        contentValues.put(STATUS, (Integer) 1);
        boolean isPushMessage = incomingMediaMessage.isPushMessage();
        long receivedTimeMillis = incomingMediaMessage.getReceivedTimeMillis();
        if (!isPushMessage) {
            receivedTimeMillis = generatePduCompatTimestamp(receivedTimeMillis);
        }
        contentValues.put("date_received", Long.valueOf(receivedTimeMillis));
        contentValues.put(PART_COUNT, Integer.valueOf(incomingMediaMessage.getAttachments().size()));
        contentValues.put(MmsSmsColumns.SUBSCRIPTION_ID, Integer.valueOf(incomingMediaMessage.getSubscriptionId()));
        contentValues.put("expires_in", Long.valueOf(incomingMediaMessage.getExpiresIn()));
        contentValues.put(VIEW_ONCE, Integer.valueOf(incomingMediaMessage.isViewOnce() ? 1 : 0));
        contentValues.put(STORY_TYPE, Integer.valueOf(incomingMediaMessage.getStoryType().getCode()));
        contentValues.put(PARENT_STORY_ID, Long.valueOf(incomingMediaMessage.getParentStoryId() != null ? incomingMediaMessage.getParentStoryId().serialize() : 0));
        contentValues.put("read", Integer.valueOf(incomingMediaMessage.isExpirationUpdate() ? 1 : 0));
        contentValues.put(MmsSmsColumns.UNIDENTIFIED, Boolean.valueOf(incomingMediaMessage.isUnidentified()));
        contentValues.put("server_guid", incomingMediaMessage.getServerGuid());
        if (!contentValues.containsKey("date")) {
            contentValues.put("date", contentValues.getAsLong("date_received"));
        }
        List<Attachment> linkedList = new LinkedList<>();
        if (incomingMediaMessage.getQuote() != null) {
            contentValues.put(QUOTE_ID, Long.valueOf(incomingMediaMessage.getQuote().getId()));
            contentValues.put(QUOTE_BODY, incomingMediaMessage.getQuote().getText().toString());
            contentValues.put(QUOTE_AUTHOR, incomingMediaMessage.getQuote().getAuthor().serialize());
            contentValues.put(QUOTE_TYPE, Integer.valueOf(incomingMediaMessage.getQuote().getType().getCode()));
            contentValues.put(QUOTE_MISSING, Integer.valueOf(incomingMediaMessage.getQuote().isOriginalMissing() ? 1 : 0));
            BodyRangeList mentionsToBodyRangeList = MentionUtil.mentionsToBodyRangeList(incomingMediaMessage.getQuote().getMentions());
            if (mentionsToBodyRangeList != null) {
                contentValues.put(QUOTE_MENTIONS, mentionsToBodyRangeList.toByteArray());
            }
            linkedList = incomingMediaMessage.getQuote().getAttachments();
        }
        if (!incomingMediaMessage.isPushMessage() || !isDuplicate(incomingMediaMessage, j3)) {
            long insertMediaMessage = insertMediaMessage(j3, incomingMediaMessage.getBody(), incomingMediaMessage.getAttachments(), linkedList, incomingMediaMessage.getSharedContacts(), incomingMediaMessage.getLinkPreviews(), incomingMediaMessage.getMentions(), incomingMediaMessage.getMessageRanges(), contentValues, null, true);
            boolean z = incomingMediaMessage.getParentStoryId() == null || !incomingMediaMessage.getParentStoryId().isGroupReply();
            if (!MmsSmsColumns.Types.isExpirationTimerUpdate(j2) && !incomingMediaMessage.getStoryType().isStory() && z) {
                SignalDatabase.threads().incrementUnread(j3, 1);
                SignalDatabase.threads().update(j3, true);
            }
            notifyConversationListeners(j3);
            return Optional.of(new MessageDatabase.InsertResult(insertMediaMessage, j3));
        }
        String str2 = TAG;
        Log.w(str2, "Ignoring duplicate media message (" + incomingMediaMessage.getSentTimeMillis() + ")");
        return Optional.empty();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Optional<MessageDatabase.InsertResult> insertMessageInbox(IncomingMediaMessage incomingMediaMessage, String str, long j) throws MmsException {
        long j2 = incomingMediaMessage.isPushMessage() ? 2097172 : 20;
        if (incomingMediaMessage.isExpirationUpdate()) {
            j2 |= 262144;
        }
        return insertMessageInbox(incomingMediaMessage, str, j, j2);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Optional<MessageDatabase.InsertResult> insertSecureDecryptedMessageInbox(IncomingMediaMessage incomingMediaMessage, long j) throws MmsException {
        long j2 = incomingMediaMessage.isPushMessage() ? 10485780 : 8388628;
        if (incomingMediaMessage.isExpirationUpdate()) {
            j2 |= 262144;
        }
        boolean z = false;
        if (incomingMediaMessage.isStoryReaction()) {
            z = true;
            j2 |= MmsSmsColumns.Types.SPECIAL_TYPE_STORY_REACTION;
        }
        if (incomingMediaMessage.getGiftBadge() != null) {
            if (!z) {
                j2 |= MmsSmsColumns.Types.SPECIAL_TYPE_GIFT_BADGE;
            } else {
                throw new MmsException("Cannot insert message with multiple special types.");
            }
        }
        return insertMessageInbox(incomingMediaMessage, "", j, j2);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Pair<Long, Long> insertMessageInbox(NotificationInd notificationInd, int i) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        long threadIdFor = getThreadIdFor(notificationInd);
        ContentValues contentValues = new ContentValues();
        ContentValuesBuilder contentValuesBuilder = new ContentValuesBuilder(contentValues);
        String str = TAG;
        Log.i(str, "Message received type: " + notificationInd.getMessageType());
        contentValuesBuilder.add(CONTENT_LOCATION, notificationInd.getContentLocation());
        contentValuesBuilder.add("date", System.currentTimeMillis());
        contentValuesBuilder.add(EXPIRY, notificationInd.getExpiry());
        contentValuesBuilder.add(MESSAGE_SIZE, notificationInd.getMessageSize());
        contentValuesBuilder.add(TRANSACTION_ID, notificationInd.getTransactionId());
        contentValuesBuilder.add(MESSAGE_TYPE, notificationInd.getMessageType());
        if (notificationInd.getFrom() != null) {
            contentValues.put("address", Recipient.external(this.context, Util.toIsoString(notificationInd.getFrom().getTextString())).getId().serialize());
        } else {
            contentValues.put("address", RecipientId.UNKNOWN.serialize());
        }
        contentValues.put(MESSAGE_BOX, (Long) 20L);
        contentValues.put("thread_id", Long.valueOf(threadIdFor));
        contentValues.put(STATUS, (Integer) 1);
        contentValues.put("date_received", Long.valueOf(generatePduCompatTimestamp(System.currentTimeMillis())));
        contentValues.put("read", Integer.valueOf(1 ^ (Util.isDefaultSmsProvider(this.context) ? 1 : 0)));
        contentValues.put(MmsSmsColumns.SUBSCRIPTION_ID, Integer.valueOf(i));
        if (!contentValues.containsKey("date")) {
            contentValues.put("date", contentValues.getAsLong("date_received"));
        }
        return new Pair<>(Long.valueOf(signalWritableDatabase.insert("mms", (String) null, contentValues)), Long.valueOf(threadIdFor));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public MessageDatabase.InsertResult insertChatSessionRefreshedMessage(RecipientId recipientId, long j, long j2) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void insertBadDecryptMessage(RecipientId recipientId, int i, long j, long j2, long j3) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markIncomingNotificationReceived(long j) {
        notifyConversationListeners(j);
        if (Util.isDefaultSmsProvider(this.context)) {
            SignalDatabase.threads().incrementUnread(j, 1);
        }
        SignalDatabase.threads().update(j, true);
        TrimThreadJob.enqueueAsync(j);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markGiftRedemptionCompleted(long j) {
        markGiftRedemptionState(j, GiftBadge.RedemptionState.REDEEMED);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markGiftRedemptionStarted(long j) {
        markGiftRedemptionState(j, GiftBadge.RedemptionState.STARTED);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void markGiftRedemptionFailed(long j) {
        markGiftRedemptionState(j, GiftBadge.RedemptionState.FAILED);
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0097 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void markGiftRedemptionState(long r16, org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge.RedemptionState r18) {
        /*
        // Method dump skipped, instructions count: 229
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.MmsDatabase.markGiftRedemptionState(long, org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge$RedemptionState):void");
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public long insertMessageOutbox(OutgoingMediaMessage outgoingMediaMessage, long j, boolean z, MessageDatabase.InsertListener insertListener) throws MmsException {
        return insertMessageOutbox(outgoingMediaMessage, j, z, 0, insertListener);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        if (r2.isJustAGroupLeave() != false) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0038, code lost:
        r0 = r0 | 131072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004d, code lost:
        if (r2.isQuit() != false) goto L_0x0038;
     */
    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long insertMessageOutbox(org.thoughtcrime.securesms.mms.OutgoingMediaMessage r26, long r27, boolean r29, int r30, org.thoughtcrime.securesms.database.MessageDatabase.InsertListener r31) throws org.thoughtcrime.securesms.mms.MmsException {
        /*
        // Method dump skipped, instructions count: 904
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.MmsDatabase.insertMessageOutbox(org.thoughtcrime.securesms.mms.OutgoingMediaMessage, long, boolean, int, org.thoughtcrime.securesms.database.MessageDatabase$InsertListener):long");
    }

    public static /* synthetic */ RecipientId lambda$insertMessageOutbox$5(UUID uuid) {
        return RecipientId.from(ServiceId.from(uuid));
    }

    private boolean hasAudioAttachment(List<Attachment> list) {
        for (Attachment attachment : list) {
            if (MediaUtil.isAudio(attachment)) {
                return true;
            }
        }
        return false;
    }

    private long insertMediaMessage(long j, String str, List<Attachment> list, List<Attachment> list2, List<Contact> list3, List<LinkPreview> list4, List<Mention> list5, BodyRangeList bodyRangeList, ContentValues contentValues, MessageDatabase.InsertListener insertListener, boolean z) throws MmsException {
        long j2;
        long j3;
        Throwable th;
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        AttachmentDatabase attachments = SignalDatabase.attachments();
        MentionDatabase mentions = SignalDatabase.mentions();
        boolean isPresent = Stream.of(list5).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.MmsDatabase$$ExternalSyntheticLambda13
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return MmsDatabase.lambda$insertMediaMessage$6((Mention) obj);
            }
        }).findFirst().isPresent();
        LinkedList linkedList = new LinkedList();
        List list6 = Stream.of(list3).map(new MmsDatabase$$ExternalSyntheticLambda2()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.MmsDatabase$$ExternalSyntheticLambda14
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return MmsDatabase.lambda$insertMediaMessage$7((Attachment) obj);
            }
        }).toList();
        List list7 = Stream.of(list4).filter(new Predicate() { // from class: org.thoughtcrime.securesms.database.MmsDatabase$$ExternalSyntheticLambda15
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return MmsDatabase.lambda$insertMediaMessage$8((LinkPreview) obj);
            }
        }).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.database.MmsDatabase$$ExternalSyntheticLambda16
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MmsDatabase.lambda$insertMediaMessage$9((LinkPreview) obj);
            }
        }).toList();
        linkedList.addAll(list);
        linkedList.addAll(list6);
        linkedList.addAll(list7);
        contentValues.put("body", str);
        contentValues.put(PART_COUNT, Integer.valueOf(linkedList.size()));
        contentValues.put(MENTIONS_SELF, Integer.valueOf(isPresent ? 1 : 0));
        if (bodyRangeList != null) {
            contentValues.put(MESSAGE_RANGES, bodyRangeList.toByteArray());
        }
        signalWritableDatabase.beginTransaction();
        try {
            long insert = signalWritableDatabase.insert("mms", (String) null, contentValues);
            try {
                mentions.insert(j, insert, list5);
                Map<Attachment, AttachmentId> insertAttachmentsForMessage = attachments.insertAttachmentsForMessage(insert, linkedList, list2);
                String serializedSharedContacts = getSerializedSharedContacts(insertAttachmentsForMessage, list3);
                String serializedLinkPreviews = getSerializedLinkPreviews(insertAttachmentsForMessage, list4);
                if (!TextUtils.isEmpty(serializedSharedContacts)) {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put(SHARED_CONTACTS, serializedSharedContacts);
                    if (this.databaseHelper.getSignalReadableDatabase().update("mms", contentValues2, "_id = ?", new String[]{String.valueOf(insert)}) <= 0) {
                        Log.w(TAG, "Failed to update message with shared contact data.");
                    }
                }
                if (!TextUtils.isEmpty(serializedLinkPreviews)) {
                    ContentValues contentValues3 = new ContentValues();
                    contentValues3.put(LINK_PREVIEWS, serializedLinkPreviews);
                    if (this.databaseHelper.getSignalReadableDatabase().update("mms", contentValues3, "_id = ?", new String[]{String.valueOf(insert)}) <= 0) {
                        Log.w(TAG, "Failed to update message with link preview data.");
                    }
                }
                signalWritableDatabase.setTransactionSuccessful();
                signalWritableDatabase.endTransaction();
                if (insertListener != null) {
                    insertListener.onComplete();
                }
                long longValue = contentValues.getAsLong("thread_id").longValue();
                if (z) {
                    SignalDatabase.threads().setLastScrolled(longValue, 0);
                    SignalDatabase.threads().update(j, true);
                }
                return insert;
            } catch (Throwable th2) {
                th = th2;
                j2 = j;
                j3 = 0;
                signalWritableDatabase.endTransaction();
                if (insertListener != null) {
                    insertListener.onComplete();
                }
                long longValue2 = contentValues.getAsLong("thread_id").longValue();
                if (z) {
                    SignalDatabase.threads().setLastScrolled(longValue2, j3);
                    SignalDatabase.threads().update(j2, true);
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            j3 = 0;
            j2 = j;
        }
    }

    public static /* synthetic */ boolean lambda$insertMediaMessage$6(Mention mention) {
        return Recipient.resolved(mention.getRecipientId()).isSelf();
    }

    public static /* synthetic */ boolean lambda$insertMediaMessage$8(LinkPreview linkPreview) {
        return linkPreview.getThumbnail().isPresent();
    }

    public static /* synthetic */ Attachment lambda$insertMediaMessage$9(LinkPreview linkPreview) {
        return linkPreview.getThumbnail().get();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean deleteMessage(long j) {
        String str = TAG;
        Log.d(str, "deleteMessage(" + j + ")");
        long threadIdForMessage = getThreadIdForMessage(j);
        SignalDatabase.attachments().deleteAttachmentsForMessage(j);
        SignalDatabase.groupReceipts().deleteRowsForMessage(j);
        SignalDatabase.mentions().deleteMentionsForMessage(j);
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.delete("mms", "_id = ?", new String[]{j + ""});
        SignalDatabase.threads().setLastScrolled(threadIdForMessage, 0);
        boolean update = SignalDatabase.threads().update(threadIdForMessage, false);
        notifyConversationListeners(threadIdForMessage);
        notifyStickerListeners();
        notifyStickerPackListeners();
        return update;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void deleteThread(long j) {
        String str = TAG;
        Log.d(str, "deleteThread(" + j + ")");
        HashSet hashSet = new HashSet();
        hashSet.add(Long.valueOf(j));
        deleteThreads(hashSet);
    }

    private String getSerializedSharedContacts(Map<Attachment, AttachmentId> map, List<Contact> list) {
        if (list.isEmpty()) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        for (Contact contact : list) {
            try {
                jSONArray.put(new JSONObject(new Contact(contact, new Contact.Avatar(contact.getAvatarAttachment() != null ? map.get(contact.getAvatarAttachment()) : null, contact.getAvatarAttachment(), contact.getAvatar() != null && contact.getAvatar().isProfile())).serialize()));
            } catch (IOException | JSONException e) {
                Log.w(TAG, "Failed to serialize shared contact. Skipping it.", e);
            }
        }
        return jSONArray.toString();
    }

    private String getSerializedLinkPreviews(Map<Attachment, AttachmentId> map, List<LinkPreview> list) {
        if (list.isEmpty()) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        for (LinkPreview linkPreview : list) {
            try {
                jSONArray.put(new JSONObject(new LinkPreview(linkPreview.getUrl(), linkPreview.getTitle(), linkPreview.getDescription(), linkPreview.getDate(), linkPreview.getThumbnail().isPresent() ? map.get(linkPreview.getThumbnail().get()) : null).serialize()));
            } catch (IOException | JSONException e) {
                Log.w(TAG, "Failed to serialize shared contact. Skipping it.", e);
            }
        }
        return jSONArray.toString();
    }

    private boolean isDuplicate(IncomingMediaMessage incomingMediaMessage, long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{SubscriptionLevels.BOOST_LEVEL}, "date = ? AND address = ? AND thread_id = ?", SqlUtil.buildArgs(Long.valueOf(incomingMediaMessage.getSentTimeMillis()), incomingMediaMessage.getFrom().serialize(), Long.valueOf(j)), null, null, null, SubscriptionLevels.BOOST_LEVEL);
        try {
            boolean moveToFirst = query.moveToFirst();
            query.close();
            return moveToFirst;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public boolean isSent(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("mms", new String[]{MESSAGE_BOX}, "_id = ?", new String[]{String.valueOf(j)}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    boolean isSentType = MmsSmsColumns.Types.isSentType(query.getLong(query.getColumnIndexOrThrow(MESSAGE_BOX)));
                    query.close();
                    return isSentType;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageRecord> getProfileChangeDetailsRecords(long j, long j2) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public Set<Long> getAllRateLimitedMessageIds() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        HashSet hashSet = new HashSet();
        Cursor query = signalReadableDatabase.query("mms", new String[]{"_id"}, "(msg_box & 68719476735 & 128) > 0", null, null, null, null);
        while (query.moveToNext()) {
            try {
                hashSet.add(Long.valueOf(CursorUtil.requireLong(query, "_id")));
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return hashSet;
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void deleteThreads(Set<Long> set) {
        Log.d(TAG, "deleteThreads(count: " + set.size() + ")");
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        String str = "";
        for (Long l : set) {
            str = str + "thread_id = '" + l.longValue() + "' OR ";
        }
        Cursor query = signalWritableDatabase.query("mms", new String[]{"_id"}, str.substring(0, str.length() - 4), null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                deleteMessage(query.getLong(0));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public int deleteMessagesInThreadBeforeDate(long j, long j2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        return signalWritableDatabase.delete("mms", "thread_id = ? AND date_received < " + j2, SqlUtil.buildArgs(j));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void deleteAbandonedMessages() {
        int delete = this.databaseHelper.getSignalWritableDatabase().delete("mms", "thread_id NOT IN (SELECT _id FROM thread)", (String[]) null);
        if (delete > 0) {
            String str = TAG;
            Log.i(str, "Deleted " + delete + " abandoned messages");
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public List<MessageRecord> getMessagesInThreadAfterInclusive(long j, long j2, long j3) {
        Reader readerFor = readerFor(rawQuery("mms.thread_id = ? AND mms." + getDateReceivedColumnName() + " >= ?", SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(j2)), false, j3));
        try {
            ArrayList arrayList = new ArrayList(readerFor.cursor.getCount());
            while (readerFor.getNext() != null) {
                arrayList.add(readerFor.getCurrent());
            }
            readerFor.close();
            return arrayList;
        } catch (Throwable th) {
            if (readerFor != null) {
                try {
                    readerFor.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void deleteAllThreads() {
        Log.d(TAG, "deleteAllThreads()");
        SignalDatabase.attachments().deleteAllAttachments();
        SignalDatabase.groupReceipts().deleteAllRows();
        SignalDatabase.mentions().deleteAllMentions();
        this.databaseHelper.getSignalWritableDatabase().delete("mms", (String) null, (String[]) null);
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public ViewOnceExpirationInfo getNearestExpiringViewOnceMessage() {
        Cursor rawQuery = this.databaseHelper.getSignalReadableDatabase().rawQuery("SELECT mms._id, reveal_duration, date_received FROM mms INNER JOIN part ON mms._id = part.mid WHERE reveal_duration > 0 AND (_data NOT NULL OR pending_push != ?)", new String[]{String.valueOf(0)});
        ViewOnceExpirationInfo viewOnceExpirationInfo = null;
        long j = Long.MAX_VALUE;
        while (rawQuery != null) {
            try {
                if (!rawQuery.moveToNext()) {
                    break;
                }
                long j2 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_id"));
                long j3 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("date_received"));
                long j4 = ViewOnceUtil.MAX_LIFESPAN + j3;
                if (viewOnceExpirationInfo == null || j4 < j) {
                    viewOnceExpirationInfo = new ViewOnceExpirationInfo(j2, j3);
                    j = j4;
                }
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        return viewOnceExpirationInfo;
    }

    public static List<Mention> parseQuoteMentions(Context context, Cursor cursor) {
        return MentionUtil.bodyRangeListToMentions(context, cursor.getBlob(cursor.getColumnIndexOrThrow(QUOTE_MENTIONS)));
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public SQLiteDatabase beginTransaction() {
        this.databaseHelper.getSignalWritableDatabase().beginTransaction();
        return this.databaseHelper.getSignalWritableDatabase();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void setTransactionSuccessful() {
        this.databaseHelper.getSignalWritableDatabase().setTransactionSuccessful();
    }

    @Override // org.thoughtcrime.securesms.database.MessageDatabase
    public void endTransaction() {
        this.databaseHelper.getSignalWritableDatabase().endTransaction();
    }

    public static Reader readerFor(Cursor cursor) {
        return new Reader(cursor);
    }

    public static OutgoingMessageReader readerFor(OutgoingMediaMessage outgoingMediaMessage, long j) {
        return new OutgoingMessageReader(outgoingMediaMessage, j);
    }

    /* loaded from: classes4.dex */
    public static class OutgoingMessageReader {
        private final Context context = ApplicationDependencies.getApplication();
        private final long id;
        private final OutgoingMediaMessage message;
        private final long threadId;

        public OutgoingMessageReader(OutgoingMediaMessage outgoingMediaMessage, long j) {
            this.message = outgoingMediaMessage;
            this.id = new SecureRandom().nextLong();
            this.threadId = j;
        }

        public MessageRecord getCurrent() {
            List<Mention> list;
            String str;
            Quote quote;
            long j;
            long j2;
            SlideDeck slideDeck = new SlideDeck(this.context, this.message.getAttachments());
            String text = this.message.getOutgoingQuote() != null ? this.message.getOutgoingQuote().getText() : null;
            List<Mention> mentions = this.message.getOutgoingQuote() != null ? this.message.getOutgoingQuote().getMentions() : Collections.emptyList();
            if (text == null || mentions.isEmpty()) {
                str = text;
                list = mentions;
            } else {
                MentionUtil.UpdatedBodyAndMentions updateBodyAndMentionsWithDisplayNames = MentionUtil.updateBodyAndMentionsWithDisplayNames(this.context, text, mentions);
                CharSequence body = updateBodyAndMentionsWithDisplayNames.getBody();
                list = updateBodyAndMentionsWithDisplayNames.getMentions();
                str = body;
            }
            long j3 = this.id;
            Recipient recipient = this.message.getRecipient();
            Recipient recipient2 = this.message.getRecipient();
            long currentTimeMillis = System.currentTimeMillis();
            long currentTimeMillis2 = System.currentTimeMillis();
            long j4 = this.threadId;
            String body2 = this.message.getBody();
            int size = slideDeck.getSlides().size();
            long outgoingEncryptedMessageType = this.message.isSecure() ? MmsSmsColumns.Types.getOutgoingEncryptedMessageType() : MmsSmsColumns.Types.getOutgoingSmsMessageType();
            Set emptySet = Collections.emptySet();
            Set emptySet2 = Collections.emptySet();
            int subscriptionId = this.message.getSubscriptionId();
            long expiresIn = this.message.getExpiresIn();
            long currentTimeMillis3 = System.currentTimeMillis();
            boolean isViewOnce = this.message.isViewOnce();
            if (this.message.getOutgoingQuote() != null) {
                j2 = j4;
                j = j3;
                quote = new Quote(this.message.getOutgoingQuote().getId(), this.message.getOutgoingQuote().getAuthor(), str, this.message.getOutgoingQuote().isOriginalMissing(), new SlideDeck(this.context, this.message.getOutgoingQuote().getAttachments()), list, this.message.getOutgoingQuote().getType());
            } else {
                j2 = j4;
                j = j3;
                quote = null;
            }
            return new MediaMmsMessageRecord(j, recipient, recipient2, 1, currentTimeMillis, currentTimeMillis2, -1, 0, j2, body2, slideDeck, size, outgoingEncryptedMessageType, emptySet, emptySet2, subscriptionId, expiresIn, currentTimeMillis3, isViewOnce, 0, quote, this.message.getSharedContacts(), this.message.getLinkPreviews(), false, Collections.emptyList(), false, false, 0, 0, -1, null, this.message.getStoryType(), this.message.getParentStoryId(), this.message.getGiftBadge());
        }
    }

    /* loaded from: classes4.dex */
    public static class Reader implements MessageDatabase.Reader {
        private final Context context = ApplicationDependencies.getApplication();
        private final Cursor cursor;

        public Reader(Cursor cursor) {
            this.cursor = cursor;
        }

        @Override // org.thoughtcrime.securesms.database.MessageDatabase.Reader
        public MessageRecord getNext() {
            Cursor cursor = this.cursor;
            if (cursor == null || !cursor.moveToNext()) {
                return null;
            }
            return getCurrent();
        }

        @Override // org.thoughtcrime.securesms.database.MessageDatabase.Reader
        public MessageRecord getCurrent() {
            Cursor cursor = this.cursor;
            if (cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.MESSAGE_TYPE)) == 130) {
                return getNotificationMmsMessageRecord(this.cursor);
            }
            return getMediaMmsMessageRecord(this.cursor);
        }

        private NotificationMmsMessageRecord getNotificationMmsMessageRecord(Cursor cursor) {
            GiftBadge giftBadge;
            long j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("date_sent"));
            long j3 = cursor.getLong(cursor.getColumnIndexOrThrow("date_received"));
            long j4 = cursor.getLong(cursor.getColumnIndexOrThrow("thread_id"));
            long j5 = cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.MESSAGE_BOX));
            long j6 = cursor.getLong(cursor.getColumnIndexOrThrow("address"));
            int i = cursor.getInt(cursor.getColumnIndexOrThrow(MmsSmsColumns.ADDRESS_DEVICE_ID));
            Recipient recipient = Recipient.live(RecipientId.from(j6)).get();
            String string = cursor.getString(cursor.getColumnIndexOrThrow(MmsDatabase.CONTENT_LOCATION));
            String string2 = cursor.getString(cursor.getColumnIndexOrThrow(MmsDatabase.TRANSACTION_ID));
            long j7 = cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.MESSAGE_SIZE));
            long j8 = cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.EXPIRY));
            int i2 = cursor.getInt(cursor.getColumnIndexOrThrow(MmsDatabase.STATUS));
            int i3 = cursor.getInt(cursor.getColumnIndexOrThrow("delivery_receipt_count"));
            int i4 = cursor.getInt(cursor.getColumnIndexOrThrow("read_receipt_count"));
            int i5 = cursor.getInt(cursor.getColumnIndexOrThrow(MmsSmsColumns.SUBSCRIPTION_ID));
            int i6 = cursor.getInt(cursor.getColumnIndexOrThrow(MmsSmsColumns.VIEWED_RECEIPT_COUNT));
            long requireLong = CursorUtil.requireLong(cursor, MmsSmsColumns.RECEIPT_TIMESTAMP);
            StoryType fromCode = StoryType.fromCode(CursorUtil.requireInt(cursor, MmsDatabase.STORY_TYPE));
            ParentStoryId deserialize = ParentStoryId.deserialize(CursorUtil.requireLong(cursor, MmsDatabase.PARENT_STORY_ID));
            String string3 = cursor.getString(cursor.getColumnIndexOrThrow("body"));
            int i7 = !TextSecurePreferences.isReadReceiptsEnabled(this.context) ? 0 : i4;
            byte[] isoBytes = !TextUtils.isEmpty(string) ? Util.toIsoBytes(string) : null;
            byte[] isoBytes2 = !TextUtils.isEmpty(string2) ? Util.toIsoBytes(string2) : null;
            SlideDeck slideDeck = new SlideDeck(this.context, new MmsNotificationAttachment(i2, j7));
            if (string3 != null && MmsSmsColumns.Types.isGiftBadge(j5)) {
                try {
                    giftBadge = GiftBadge.parseFrom(Base64.decode(string3));
                } catch (IOException e) {
                    Log.w(MmsDatabase.TAG, "Error parsing gift badge", e);
                }
                return new NotificationMmsMessageRecord(j, recipient, recipient, i, j2, j3, i3, j4, isoBytes, j7, j8, i2, isoBytes2, j5, i5, slideDeck, i7, i6, requireLong, fromCode, deserialize, giftBadge);
            }
            giftBadge = null;
            return new NotificationMmsMessageRecord(j, recipient, recipient, i, j2, j3, i3, j4, isoBytes, j7, j8, i2, isoBytes2, j5, i5, slideDeck, i7, i6, requireLong, fromCode, deserialize, giftBadge);
        }

        private MediaMmsMessageRecord getMediaMmsMessageRecord(Cursor cursor) {
            int i;
            int i2;
            BodyRangeList bodyRangeList;
            BodyRangeList parseFrom;
            GiftBadge giftBadge;
            long j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("date_sent"));
            long j3 = cursor.getLong(cursor.getColumnIndexOrThrow("date_received"));
            long j4 = cursor.getLong(cursor.getColumnIndexOrThrow(MmsSmsColumns.DATE_SERVER));
            long j5 = cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.MESSAGE_BOX));
            long j6 = cursor.getLong(cursor.getColumnIndexOrThrow("thread_id"));
            long j7 = cursor.getLong(cursor.getColumnIndexOrThrow("address"));
            int i3 = cursor.getInt(cursor.getColumnIndexOrThrow(MmsSmsColumns.ADDRESS_DEVICE_ID));
            int i4 = cursor.getInt(cursor.getColumnIndexOrThrow("delivery_receipt_count"));
            int i5 = cursor.getInt(cursor.getColumnIndexOrThrow("read_receipt_count"));
            String string = cursor.getString(cursor.getColumnIndexOrThrow("body"));
            int i6 = cursor.getInt(cursor.getColumnIndexOrThrow(MmsDatabase.PART_COUNT));
            String string2 = cursor.getString(cursor.getColumnIndexOrThrow(MmsSmsColumns.MISMATCHED_IDENTITIES));
            String string3 = cursor.getString(cursor.getColumnIndexOrThrow(MmsDatabase.NETWORK_FAILURE));
            int i7 = cursor.getInt(cursor.getColumnIndexOrThrow(MmsSmsColumns.SUBSCRIPTION_ID));
            long j8 = cursor.getLong(cursor.getColumnIndexOrThrow("expires_in"));
            long j9 = cursor.getLong(cursor.getColumnIndexOrThrow(MmsSmsColumns.EXPIRE_STARTED));
            boolean z = cursor.getInt(cursor.getColumnIndexOrThrow(MmsSmsColumns.UNIDENTIFIED)) == 1;
            boolean z2 = cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.VIEW_ONCE)) == 1;
            boolean z3 = cursor.getLong(cursor.getColumnIndexOrThrow(MmsSmsColumns.REMOTE_DELETED)) == 1;
            boolean requireBoolean = CursorUtil.requireBoolean(cursor, MmsDatabase.MENTIONS_SELF);
            long requireLong = CursorUtil.requireLong(cursor, MmsSmsColumns.NOTIFIED_TIMESTAMP);
            int i8 = cursor.getInt(cursor.getColumnIndexOrThrow(MmsSmsColumns.VIEWED_RECEIPT_COUNT));
            long requireLong2 = CursorUtil.requireLong(cursor, MmsSmsColumns.RECEIPT_TIMESTAMP);
            byte[] requireBlob = CursorUtil.requireBlob(cursor, MmsDatabase.MESSAGE_RANGES);
            StoryType fromCode = StoryType.fromCode(CursorUtil.requireInt(cursor, MmsDatabase.STORY_TYPE));
            ParentStoryId deserialize = ParentStoryId.deserialize(CursorUtil.requireLong(cursor, MmsDatabase.PARENT_STORY_ID));
            if (TextSecurePreferences.isReadReceiptsEnabled(this.context)) {
                i = i8;
                i2 = i5;
            } else if (!MmsSmsColumns.Types.isOutgoingMessageType(j5) || fromCode.isStory()) {
                i = i8;
                i2 = 0;
            } else {
                i2 = 0;
                i = 0;
            }
            Recipient recipient = Recipient.live(RecipientId.from(j7)).get();
            Set<IdentityKeyMismatch> mismatchedIdentities = getMismatchedIdentities(string2);
            Set<NetworkFailure> failures = getFailures(string3);
            List<DatabaseAttachment> attachments = SignalDatabase.attachments().getAttachments(cursor);
            List sharedContacts = MmsDatabase.getSharedContacts(cursor, attachments);
            Set set = (Set) Stream.of(sharedContacts).map(new MmsDatabase$$ExternalSyntheticLambda2()).withoutNulls().collect(Collectors.toSet());
            List linkPreviews = MmsDatabase.getLinkPreviews(cursor, attachments);
            Set set2 = (Set) Stream.of(linkPreviews).filter(new MmsDatabase$Reader$$ExternalSyntheticLambda0()).map(new MmsDatabase$Reader$$ExternalSyntheticLambda1()).collect(Collectors.toSet());
            Context context = this.context;
            Stream of = Stream.of(attachments);
            Objects.requireNonNull(set);
            Stream filterNot = of.filterNot(new MmsDatabase$$ExternalSyntheticLambda6(set));
            Objects.requireNonNull(set2);
            SlideDeck buildSlideDeck = buildSlideDeck(context, filterNot.filterNot(new MmsDatabase$$ExternalSyntheticLambda6(set2)).toList());
            Quote quote = getQuote(cursor);
            if (requireBlob != null) {
                try {
                    parseFrom = BodyRangeList.parseFrom(requireBlob);
                } catch (InvalidProtocolBufferException e) {
                    Log.w(MmsDatabase.TAG, "Error parsing message ranges", e);
                    bodyRangeList = null;
                }
            } else {
                parseFrom = null;
            }
            bodyRangeList = parseFrom;
            if (string != null && MmsSmsColumns.Types.isGiftBadge(j5)) {
                try {
                    giftBadge = GiftBadge.parseFrom(Base64.decode(string));
                } catch (IOException e2) {
                    Log.w(MmsDatabase.TAG, "Error parsing gift badge", e2);
                }
                return new MediaMmsMessageRecord(j, recipient, recipient, i3, j2, j3, j4, i4, j6, string, buildSlideDeck, i6, j5, mismatchedIdentities, failures, i7, j8, j9, z2, i2, quote, sharedContacts, linkPreviews, z, Collections.emptyList(), z3, requireBoolean, requireLong, i, requireLong2, bodyRangeList, fromCode, deserialize, giftBadge);
            }
            giftBadge = null;
            return new MediaMmsMessageRecord(j, recipient, recipient, i3, j2, j3, j4, i4, j6, string, buildSlideDeck, i6, j5, mismatchedIdentities, failures, i7, j8, j9, z2, i2, quote, sharedContacts, linkPreviews, z, Collections.emptyList(), z3, requireBoolean, requireLong, i, requireLong2, bodyRangeList, fromCode, deserialize, giftBadge);
        }

        public static /* synthetic */ boolean lambda$getMediaMmsMessageRecord$0(LinkPreview linkPreview) {
            return linkPreview.getThumbnail().isPresent();
        }

        public static /* synthetic */ Attachment lambda$getMediaMmsMessageRecord$1(LinkPreview linkPreview) {
            return linkPreview.getThumbnail().get();
        }

        private Set<IdentityKeyMismatch> getMismatchedIdentities(String str) {
            if (!TextUtils.isEmpty(str)) {
                try {
                    return ((IdentityKeyMismatchSet) JsonUtils.fromJson(str, IdentityKeyMismatchSet.class)).getItems();
                } catch (IOException e) {
                    Log.w(MmsDatabase.TAG, e);
                }
            }
            return Collections.emptySet();
        }

        private Set<NetworkFailure> getFailures(String str) {
            if (!TextUtils.isEmpty(str)) {
                try {
                    return ((NetworkFailureSet) JsonUtils.fromJson(str, NetworkFailureSet.class)).getItems();
                } catch (IOException e) {
                    Log.w(MmsDatabase.TAG, e);
                }
            }
            return Collections.emptySet();
        }

        public static SlideDeck buildSlideDeck(Context context, List<DatabaseAttachment> list) {
            return new SlideDeck(context, Stream.of(list).filterNot(new MmsDatabase$$ExternalSyntheticLambda0()).sorted(new DatabaseAttachment.DisplayOrderComparator()).toList());
        }

        private Quote getQuote(Cursor cursor) {
            String str;
            List<Mention> list;
            long j = cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.QUOTE_ID));
            long j2 = cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.QUOTE_AUTHOR));
            String string = cursor.getString(cursor.getColumnIndexOrThrow(MmsDatabase.QUOTE_BODY));
            int i = cursor.getInt(cursor.getColumnIndexOrThrow(MmsDatabase.QUOTE_TYPE));
            boolean z = cursor.getInt(cursor.getColumnIndexOrThrow(MmsDatabase.QUOTE_MISSING)) == 1;
            List<Mention> parseQuoteMentions = MmsDatabase.parseQuoteMentions(this.context, cursor);
            SlideDeck slideDeck = new SlideDeck(this.context, Stream.of(SignalDatabase.attachments().getAttachments(cursor)).filter(new MmsDatabase$$ExternalSyntheticLambda0()).toList());
            if (j <= 0 || j2 <= 0) {
                return null;
            }
            if (string == null || parseQuoteMentions.isEmpty()) {
                str = string;
                list = parseQuoteMentions;
            } else {
                MentionUtil.UpdatedBodyAndMentions updateBodyAndMentionsWithDisplayNames = MentionUtil.updateBodyAndMentionsWithDisplayNames(this.context, string, parseQuoteMentions);
                CharSequence body = updateBodyAndMentionsWithDisplayNames.getBody();
                list = updateBodyAndMentionsWithDisplayNames.getMentions();
                str = body;
            }
            return new Quote(j, RecipientId.from(j2), str, z, slideDeck, list, QuoteModel.Type.fromCode(i));
        }

        @Override // org.thoughtcrime.securesms.database.MessageDatabase.Reader, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            Cursor cursor = this.cursor;
            if (cursor != null) {
                cursor.close();
            }
        }

        @Override // java.lang.Iterable
        public Iterator<MessageRecord> iterator() {
            return new ReaderIterator();
        }

        /* loaded from: classes4.dex */
        private class ReaderIterator implements Iterator<MessageRecord> {
            private ReaderIterator() {
                Reader.this = r1;
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                return (Reader.this.cursor == null || Reader.this.cursor.getCount() == 0 || Reader.this.cursor.isLast()) ? false : true;
            }

            @Override // java.util.Iterator
            public MessageRecord next() {
                MessageRecord next = Reader.this.getNext();
                if (next != null) {
                    return next;
                }
                throw new NoSuchElementException();
            }
        }
    }

    private long generatePduCompatTimestamp(long j) {
        return j - (j % 1000);
    }
}
