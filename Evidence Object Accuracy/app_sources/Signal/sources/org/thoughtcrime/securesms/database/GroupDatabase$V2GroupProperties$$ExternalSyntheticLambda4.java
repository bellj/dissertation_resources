package org.thoughtcrime.securesms.database;

import j$.util.function.Function;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.thoughtcrime.securesms.database.GroupDatabase;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupDatabase$V2GroupProperties$$ExternalSyntheticLambda4 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        DecryptedRequestingMember decryptedRequestingMember = (DecryptedRequestingMember) obj;
        return GroupDatabase.MemberLevel.REQUESTING_MEMBER;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
