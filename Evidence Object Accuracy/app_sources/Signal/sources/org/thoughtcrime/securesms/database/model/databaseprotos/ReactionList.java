package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

@Deprecated
/* loaded from: classes4.dex */
public final class ReactionList extends GeneratedMessageLite<ReactionList, Builder> implements ReactionListOrBuilder {
    private static final ReactionList DEFAULT_INSTANCE;
    private static volatile Parser<ReactionList> PARSER;
    public static final int REACTIONS_FIELD_NUMBER;
    private Internal.ProtobufList<Reaction> reactions_ = GeneratedMessageLite.emptyProtobufList();

    /* loaded from: classes4.dex */
    public interface ReactionOrBuilder extends MessageLiteOrBuilder {
        long getAuthor();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getEmoji();

        ByteString getEmojiBytes();

        long getReceivedTime();

        long getSentTime();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private ReactionList() {
    }

    /* loaded from: classes4.dex */
    public static final class Reaction extends GeneratedMessageLite<Reaction, Builder> implements ReactionOrBuilder {
        public static final int AUTHOR_FIELD_NUMBER;
        private static final Reaction DEFAULT_INSTANCE;
        public static final int EMOJI_FIELD_NUMBER;
        private static volatile Parser<Reaction> PARSER;
        public static final int RECEIVEDTIME_FIELD_NUMBER;
        public static final int SENTTIME_FIELD_NUMBER;
        private long author_;
        private String emoji_ = "";
        private long receivedTime_;
        private long sentTime_;

        private Reaction() {
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList.ReactionOrBuilder
        public String getEmoji() {
            return this.emoji_;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList.ReactionOrBuilder
        public ByteString getEmojiBytes() {
            return ByteString.copyFromUtf8(this.emoji_);
        }

        public void setEmoji(String str) {
            str.getClass();
            this.emoji_ = str;
        }

        public void clearEmoji() {
            this.emoji_ = getDefaultInstance().getEmoji();
        }

        public void setEmojiBytes(ByteString byteString) {
            AbstractMessageLite.checkByteStringIsUtf8(byteString);
            this.emoji_ = byteString.toStringUtf8();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList.ReactionOrBuilder
        public long getAuthor() {
            return this.author_;
        }

        public void setAuthor(long j) {
            this.author_ = j;
        }

        public void clearAuthor() {
            this.author_ = 0;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList.ReactionOrBuilder
        public long getSentTime() {
            return this.sentTime_;
        }

        public void setSentTime(long j) {
            this.sentTime_ = j;
        }

        public void clearSentTime() {
            this.sentTime_ = 0;
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList.ReactionOrBuilder
        public long getReceivedTime() {
            return this.receivedTime_;
        }

        public void setReceivedTime(long j) {
            this.receivedTime_ = j;
        }

        public void clearReceivedTime() {
            this.receivedTime_ = 0;
        }

        public static Reaction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Reaction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Reaction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Reaction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Reaction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Reaction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Reaction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Reaction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Reaction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Reaction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Reaction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Reaction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Reaction parseFrom(InputStream inputStream) throws IOException {
            return (Reaction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Reaction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Reaction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Reaction parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Reaction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Reaction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Reaction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Reaction parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Reaction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Reaction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Reaction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Reaction reaction) {
            return DEFAULT_INSTANCE.createBuilder(reaction);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Reaction, Builder> implements ReactionOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Reaction.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList.ReactionOrBuilder
            public String getEmoji() {
                return ((Reaction) this.instance).getEmoji();
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList.ReactionOrBuilder
            public ByteString getEmojiBytes() {
                return ((Reaction) this.instance).getEmojiBytes();
            }

            public Builder setEmoji(String str) {
                copyOnWrite();
                ((Reaction) this.instance).setEmoji(str);
                return this;
            }

            public Builder clearEmoji() {
                copyOnWrite();
                ((Reaction) this.instance).clearEmoji();
                return this;
            }

            public Builder setEmojiBytes(ByteString byteString) {
                copyOnWrite();
                ((Reaction) this.instance).setEmojiBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList.ReactionOrBuilder
            public long getAuthor() {
                return ((Reaction) this.instance).getAuthor();
            }

            public Builder setAuthor(long j) {
                copyOnWrite();
                ((Reaction) this.instance).setAuthor(j);
                return this;
            }

            public Builder clearAuthor() {
                copyOnWrite();
                ((Reaction) this.instance).clearAuthor();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList.ReactionOrBuilder
            public long getSentTime() {
                return ((Reaction) this.instance).getSentTime();
            }

            public Builder setSentTime(long j) {
                copyOnWrite();
                ((Reaction) this.instance).setSentTime(j);
                return this;
            }

            public Builder clearSentTime() {
                copyOnWrite();
                ((Reaction) this.instance).clearSentTime();
                return this;
            }

            @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList.ReactionOrBuilder
            public long getReceivedTime() {
                return ((Reaction) this.instance).getReceivedTime();
            }

            public Builder setReceivedTime(long j) {
                copyOnWrite();
                ((Reaction) this.instance).setReceivedTime(j);
                return this;
            }

            public Builder clearReceivedTime() {
                copyOnWrite();
                ((Reaction) this.instance).clearReceivedTime();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Reaction();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001Ȉ\u0002\u0003\u0003\u0003\u0004\u0003", new Object[]{"emoji_", "author_", "sentTime_", "receivedTime_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Reaction> parser = PARSER;
                    if (parser == null) {
                        synchronized (Reaction.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Reaction reaction = new Reaction();
            DEFAULT_INSTANCE = reaction;
            GeneratedMessageLite.registerDefaultInstance(Reaction.class, reaction);
        }

        public static Reaction getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Reaction> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionListOrBuilder
    public List<Reaction> getReactionsList() {
        return this.reactions_;
    }

    public List<? extends ReactionOrBuilder> getReactionsOrBuilderList() {
        return this.reactions_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionListOrBuilder
    public int getReactionsCount() {
        return this.reactions_.size();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionListOrBuilder
    public Reaction getReactions(int i) {
        return this.reactions_.get(i);
    }

    public ReactionOrBuilder getReactionsOrBuilder(int i) {
        return this.reactions_.get(i);
    }

    private void ensureReactionsIsMutable() {
        Internal.ProtobufList<Reaction> protobufList = this.reactions_;
        if (!protobufList.isModifiable()) {
            this.reactions_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setReactions(int i, Reaction reaction) {
        reaction.getClass();
        ensureReactionsIsMutable();
        this.reactions_.set(i, reaction);
    }

    public void addReactions(Reaction reaction) {
        reaction.getClass();
        ensureReactionsIsMutable();
        this.reactions_.add(reaction);
    }

    public void addReactions(int i, Reaction reaction) {
        reaction.getClass();
        ensureReactionsIsMutable();
        this.reactions_.add(i, reaction);
    }

    public void addAllReactions(Iterable<? extends Reaction> iterable) {
        ensureReactionsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.reactions_);
    }

    public void clearReactions() {
        this.reactions_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeReactions(int i) {
        ensureReactionsIsMutable();
        this.reactions_.remove(i);
    }

    public static ReactionList parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ReactionList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ReactionList parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReactionList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ReactionList parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ReactionList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ReactionList parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReactionList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ReactionList parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ReactionList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ReactionList parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReactionList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ReactionList parseFrom(InputStream inputStream) throws IOException {
        return (ReactionList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ReactionList parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReactionList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ReactionList parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ReactionList) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ReactionList parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReactionList) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ReactionList parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ReactionList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ReactionList parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReactionList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ReactionList reactionList) {
        return DEFAULT_INSTANCE.createBuilder(reactionList);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ReactionList, Builder> implements ReactionListOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(ReactionList.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionListOrBuilder
        public List<Reaction> getReactionsList() {
            return Collections.unmodifiableList(((ReactionList) this.instance).getReactionsList());
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionListOrBuilder
        public int getReactionsCount() {
            return ((ReactionList) this.instance).getReactionsCount();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.ReactionListOrBuilder
        public Reaction getReactions(int i) {
            return ((ReactionList) this.instance).getReactions(i);
        }

        public Builder setReactions(int i, Reaction reaction) {
            copyOnWrite();
            ((ReactionList) this.instance).setReactions(i, reaction);
            return this;
        }

        public Builder setReactions(int i, Reaction.Builder builder) {
            copyOnWrite();
            ((ReactionList) this.instance).setReactions(i, builder.build());
            return this;
        }

        public Builder addReactions(Reaction reaction) {
            copyOnWrite();
            ((ReactionList) this.instance).addReactions(reaction);
            return this;
        }

        public Builder addReactions(int i, Reaction reaction) {
            copyOnWrite();
            ((ReactionList) this.instance).addReactions(i, reaction);
            return this;
        }

        public Builder addReactions(Reaction.Builder builder) {
            copyOnWrite();
            ((ReactionList) this.instance).addReactions(builder.build());
            return this;
        }

        public Builder addReactions(int i, Reaction.Builder builder) {
            copyOnWrite();
            ((ReactionList) this.instance).addReactions(i, builder.build());
            return this;
        }

        public Builder addAllReactions(Iterable<? extends Reaction> iterable) {
            copyOnWrite();
            ((ReactionList) this.instance).addAllReactions(iterable);
            return this;
        }

        public Builder clearReactions() {
            copyOnWrite();
            ((ReactionList) this.instance).clearReactions();
            return this;
        }

        public Builder removeReactions(int i) {
            copyOnWrite();
            ((ReactionList) this.instance).removeReactions(i);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ReactionList();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"reactions_", Reaction.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ReactionList> parser = PARSER;
                if (parser == null) {
                    synchronized (ReactionList.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ReactionList reactionList = new ReactionList();
        DEFAULT_INSTANCE = reactionList;
        GeneratedMessageLite.registerDefaultInstance(ReactionList.class, reactionList);
    }

    public static ReactionList getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ReactionList> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
