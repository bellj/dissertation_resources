package org.thoughtcrime.securesms.database.model;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: StoryViewState.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0001\u0018\u0000 \u00062\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0006B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/StoryViewState;", "", "(Ljava/lang/String;I)V", "NONE", "UNVIEWED", "VIEWED", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum StoryViewState {
    NONE,
    UNVIEWED,
    VIEWED;
    
    public static final Companion Companion = new Companion(null);

    @JvmStatic
    public static final Observable<StoryViewState> getForRecipientId(RecipientId recipientId) {
        return Companion.getForRecipientId(recipientId);
    }

    @JvmStatic
    private static final Observable<StoryViewState> getState(RecipientId recipientId) {
        return Companion.getState(recipientId);
    }

    /* compiled from: StoryViewState.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0007J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0003¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/database/model/StoryViewState$Companion;", "", "()V", "getForRecipientId", "Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/database/model/StoryViewState;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final Observable<StoryViewState> getForRecipientId(RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            if (!recipientId.equals(Recipient.self().getId())) {
                return getState(recipientId);
            }
            Observable<StoryViewState> flatMap = Observable.fromCallable(new StoryViewState$Companion$$ExternalSyntheticLambda0()).flatMap(new StoryViewState$Companion$$ExternalSyntheticLambda1());
            Intrinsics.checkNotNullExpressionValue(flatMap, "fromCallable {\n         …  }\n          }\n        }");
            return flatMap;
        }

        /* renamed from: getForRecipientId$lambda-0 */
        public static final List m1743getForRecipientId$lambda0() {
            return SignalDatabase.Companion.recipients().getDistributionListRecipientIds();
        }

        /* renamed from: getForRecipientId$lambda-5 */
        public static final ObservableSource m1744getForRecipientId$lambda5(List list) {
            Intrinsics.checkNotNullExpressionValue(list, "ids");
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
            Iterator it = list.iterator();
            while (it.hasNext()) {
                arrayList.add(StoryViewState.Companion.getState((RecipientId) it.next()));
            }
            return Observable.combineLatest(arrayList, new StoryViewState$Companion$$ExternalSyntheticLambda5());
        }

        /* renamed from: getForRecipientId$lambda-5$lambda-4 */
        public static final StoryViewState m1745getForRecipientId$lambda5$lambda4(Object[] objArr) {
            boolean z;
            boolean z2;
            boolean z3;
            Intrinsics.checkNotNullExpressionValue(objArr, "combined");
            boolean z4 = true;
            if (objArr.length == 0) {
                return StoryViewState.NONE;
            }
            List<StoryViewState> list = ArraysKt___ArraysKt.toList(objArr);
            boolean z5 = list instanceof Collection;
            if (!z5 || !list.isEmpty()) {
                for (StoryViewState storyViewState : list) {
                    if (storyViewState == StoryViewState.UNVIEWED) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                        continue;
                    }
                    if (z3) {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
            if (z) {
                return StoryViewState.UNVIEWED;
            }
            if (!z5 || !list.isEmpty()) {
                for (StoryViewState storyViewState2 : list) {
                    if (storyViewState2 == StoryViewState.VIEWED) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (z2) {
                        break;
                    }
                }
            }
            z4 = false;
            if (z4) {
                return StoryViewState.VIEWED;
            }
            return StoryViewState.NONE;
        }

        @JvmStatic
        public final Observable<StoryViewState> getState(RecipientId recipientId) {
            Observable<StoryViewState> observeOn = Observable.create(new StoryViewState$Companion$$ExternalSyntheticLambda2(recipientId)).observeOn(Schedulers.io());
            Intrinsics.checkNotNullExpressionValue(observeOn, "create<StoryViewState> {…bserveOn(Schedulers.io())");
            return observeOn;
        }

        /* renamed from: getState$lambda-8$refresh */
        private static final void m1749getState$lambda8$refresh(ObservableEmitter<StoryViewState> observableEmitter, RecipientId recipientId) {
            observableEmitter.onNext(SignalDatabase.Companion.mms().getStoryViewState(recipientId));
        }

        /* renamed from: getState$lambda-8 */
        public static final void m1746getState$lambda8(RecipientId recipientId, ObservableEmitter observableEmitter) {
            Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
            StoryViewState$Companion$$ExternalSyntheticLambda3 storyViewState$Companion$$ExternalSyntheticLambda3 = new StoryViewState$Companion$$ExternalSyntheticLambda3(observableEmitter, recipientId);
            ApplicationDependencies.getDatabaseObserver().registerStoryObserver(recipientId, storyViewState$Companion$$ExternalSyntheticLambda3);
            observableEmitter.setCancellable(new StoryViewState$Companion$$ExternalSyntheticLambda4(storyViewState$Companion$$ExternalSyntheticLambda3));
            m1749getState$lambda8$refresh(observableEmitter, recipientId);
        }

        /* renamed from: getState$lambda-8$lambda-6 */
        public static final void m1747getState$lambda8$lambda6(ObservableEmitter observableEmitter, RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
            m1749getState$lambda8$refresh(observableEmitter, recipientId);
        }

        /* renamed from: getState$lambda-8$lambda-7 */
        public static final void m1748getState$lambda8$lambda7(DatabaseObserver.Observer observer) {
            Intrinsics.checkNotNullParameter(observer, "$storyObserver");
            ApplicationDependencies.getDatabaseObserver().unregisterObserver(observer);
        }
    }
}
