package org.thoughtcrime.securesms.database.loaders;

import android.content.Context;
import androidx.loader.content.AsyncTaskLoader;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.thoughtcrime.securesms.registration.fragments.CountryPickerFragment;
import org.whispersystems.signalservice.api.util.PhoneNumberFormatter;

/* loaded from: classes4.dex */
public final class CountryListLoader extends AsyncTaskLoader<ArrayList<Map<String, String>>> {
    public CountryListLoader(Context context) {
        super(context);
    }

    @Override // androidx.loader.content.AsyncTaskLoader
    public ArrayList<Map<String, String>> loadInBackground() {
        Set<String> supportedRegions = PhoneNumberUtil.getInstance().getSupportedRegions();
        ArrayList<Map<String, String>> arrayList = new ArrayList<>(supportedRegions.size());
        for (String str : supportedRegions) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("country_name", PhoneNumberFormatter.getRegionDisplayNameLegacy(str));
            hashMap.put(CountryPickerFragment.KEY_COUNTRY_CODE, "+" + PhoneNumberUtil.getInstance().getCountryCodeForRegion(str));
            arrayList.add(hashMap);
        }
        Collections.sort(arrayList, new RegionComparator());
        return arrayList;
    }

    /* loaded from: classes4.dex */
    public static class RegionComparator implements Comparator<Map<String, String>> {
        private final Collator collator;

        RegionComparator() {
            Collator instance = Collator.getInstance();
            this.collator = instance;
            instance.setStrength(0);
        }

        public int compare(Map<String, String> map, Map<String, String> map2) {
            return this.collator.compare(map.get("country_name"), map2.get("country_name"));
        }
    }
}
