package org.thoughtcrime.securesms.database;

/* loaded from: classes4.dex */
public interface MmsSmsColumns {
    public static final String ADDRESS_DEVICE_ID;
    public static final String BODY;
    public static final String DATE_SERVER;
    public static final String DELIVERY_RECEIPT_COUNT;
    public static final String EXPIRES_IN;
    public static final String EXPIRE_STARTED;
    public static final String ID;
    public static final String MISMATCHED_IDENTITIES;
    public static final String NORMALIZED_DATE_RECEIVED;
    public static final String NORMALIZED_DATE_SENT;
    public static final String NORMALIZED_TYPE;
    public static final String NOTIFIED;
    public static final String NOTIFIED_TIMESTAMP;
    public static final String REACTIONS_LAST_SEEN;
    public static final String REACTIONS_UNREAD;
    public static final String READ;
    public static final String READ_RECEIPT_COUNT;
    public static final String RECEIPT_TIMESTAMP;
    public static final String RECIPIENT_ID;
    public static final String REMOTE_DELETED;
    public static final String SERVER_GUID;
    public static final String SUBSCRIPTION_ID;
    public static final String THREAD_ID;
    public static final String UNIDENTIFIED;
    public static final String UNIQUE_ROW_ID;
    public static final String VIEWED_RECEIPT_COUNT;

    /* loaded from: classes4.dex */
    public static class Types {
        protected static final long BAD_DECRYPT_TYPE;
        public static final long BASE_DRAFT_TYPE;
        protected static final long BASE_INBOX_TYPE;
        protected static final long BASE_OUTBOX_TYPE;
        protected static final long BASE_PENDING_INSECURE_SMS_FALLBACK;
        protected static final long BASE_PENDING_SECURE_SMS_FALLBACK;
        protected static final long BASE_SENDING_TYPE;
        protected static final long BASE_SENT_FAILED_TYPE;
        protected static final long BASE_SENT_TYPE;
        protected static final long BASE_TYPE_MASK;
        protected static final long BOOST_REQUEST_TYPE;
        protected static final long CHANGE_NUMBER_TYPE;
        public static final long ENCRYPTION_MASK;
        protected static final long ENCRYPTION_REMOTE_BIT;
        protected static final long ENCRYPTION_REMOTE_DUPLICATE_BIT;
        protected static final long ENCRYPTION_REMOTE_FAILED_BIT;
        protected static final long ENCRYPTION_REMOTE_LEGACY_BIT;
        protected static final long ENCRYPTION_REMOTE_NO_SESSION_BIT;
        protected static final long END_SESSION_BIT;
        protected static final long EXPIRATION_TIMER_UPDATE_BIT;
        protected static final long GROUP_CALL_TYPE;
        protected static final long GROUP_LEAVE_BIT;
        protected static final long GROUP_UPDATE_BIT;
        protected static final long GROUP_V2_BIT;
        protected static final long GROUP_V2_LEAVE_BITS;
        protected static final long GV1_MIGRATION_TYPE;
        protected static final long INCOMING_AUDIO_CALL_TYPE;
        protected static final long INCOMING_VIDEO_CALL_TYPE;
        protected static final long INVALID_MESSAGE_TYPE;
        protected static final long JOINED_TYPE;
        protected static final long KEY_EXCHANGE_BIT;
        protected static final long KEY_EXCHANGE_BUNDLE_BIT;
        protected static final long KEY_EXCHANGE_CONTENT_FORMAT;
        protected static final long KEY_EXCHANGE_CORRUPTED_BIT;
        protected static final long KEY_EXCHANGE_IDENTITY_DEFAULT_BIT;
        protected static final long KEY_EXCHANGE_IDENTITY_UPDATE_BIT;
        protected static final long KEY_EXCHANGE_IDENTITY_VERIFIED_BIT;
        protected static final long KEY_EXCHANGE_INVALID_VERSION_BIT;
        protected static final long KEY_EXCHANGE_MASK;
        protected static final long MESSAGE_ATTRIBUTE_MASK;
        protected static final long MESSAGE_FORCE_SMS_BIT;
        protected static final long MESSAGE_RATE_LIMITED_BIT;
        protected static final long MISSED_AUDIO_CALL_TYPE;
        protected static final long MISSED_VIDEO_CALL_TYPE;
        protected static final long OUTGOING_AUDIO_CALL_TYPE;
        protected static final long[] OUTGOING_MESSAGE_TYPES = {BASE_OUTBOX_TYPE, BASE_SENT_TYPE, BASE_SENDING_TYPE, BASE_SENT_FAILED_TYPE, BASE_PENDING_SECURE_SMS_FALLBACK, BASE_PENDING_INSECURE_SMS_FALLBACK, OUTGOING_AUDIO_CALL_TYPE, OUTGOING_VIDEO_CALL_TYPE};
        protected static final long OUTGOING_VIDEO_CALL_TYPE;
        protected static final long PROFILE_CHANGE_TYPE;
        protected static final long PUSH_MESSAGE_BIT;
        protected static final long SECURE_MESSAGE_BIT;
        public static final long SPECIAL_TYPES_MASK;
        public static final long SPECIAL_TYPE_GIFT_BADGE;
        public static final long SPECIAL_TYPE_STORY_REACTION;
        protected static final long TOTAL_MASK;
        protected static final long UNSUPPORTED_MESSAGE_TYPE;

        public static long getOutgoingEncryptedMessageType() {
            return 10485782;
        }

        public static long getOutgoingSmsMessageType() {
            return BASE_SENDING_TYPE;
        }

        public static boolean isBadDecryptType(long j) {
            return (j & BASE_TYPE_MASK) == BAD_DECRYPT_TYPE;
        }

        public static boolean isBoostRequest(long j) {
            return j == BOOST_REQUEST_TYPE;
        }

        public static boolean isBundleKeyExchange(long j) {
            return (j & KEY_EXCHANGE_BUNDLE_BIT) != 0;
        }

        public static boolean isChangeNumber(long j) {
            return j == CHANGE_NUMBER_TYPE;
        }

        public static boolean isChatSessionRefresh(long j) {
            return (j & ENCRYPTION_REMOTE_FAILED_BIT) != 0;
        }

        public static boolean isContentBundleKeyExchange(long j) {
            return (j & KEY_EXCHANGE_CONTENT_FORMAT) != 0;
        }

        public static boolean isCorruptedKeyExchange(long j) {
            return (j & KEY_EXCHANGE_CORRUPTED_BIT) != 0;
        }

        public static boolean isDecryptInProgressType(long j) {
            return (j & 1073741824) != 0;
        }

        public static boolean isDraftMessageType(long j) {
            return (j & BASE_TYPE_MASK) == 27;
        }

        public static boolean isDuplicateMessageType(long j) {
            return (j & ENCRYPTION_REMOTE_DUPLICATE_BIT) != 0;
        }

        public static boolean isEndSessionType(long j) {
            return (j & END_SESSION_BIT) != 0;
        }

        public static boolean isExpirationTimerUpdate(long j) {
            return (j & EXPIRATION_TIMER_UPDATE_BIT) != 0;
        }

        public static boolean isFailedMessageType(long j) {
            return (j & BASE_TYPE_MASK) == BASE_SENT_FAILED_TYPE;
        }

        public static boolean isForcedSms(long j) {
            return (j & MESSAGE_FORCE_SMS_BIT) != 0;
        }

        public static boolean isGiftBadge(long j) {
            return (j & SPECIAL_TYPES_MASK) == SPECIAL_TYPE_GIFT_BADGE;
        }

        public static boolean isGroupCall(long j) {
            return j == GROUP_CALL_TYPE;
        }

        public static boolean isGroupQuit(long j) {
            return (GROUP_LEAVE_BIT & j) != 0 && (j & GROUP_V2_BIT) == 0;
        }

        public static boolean isGroupUpdate(long j) {
            return (j & GROUP_UPDATE_BIT) != 0;
        }

        public static boolean isGroupV1MigrationEvent(long j) {
            return j == GV1_MIGRATION_TYPE;
        }

        public static boolean isGroupV2(long j) {
            return (j & GROUP_V2_BIT) != 0;
        }

        public static boolean isGroupV2LeaveOnly(long j) {
            return (j & GROUP_V2_LEAVE_BITS) == GROUP_V2_LEAVE_BITS;
        }

        public static boolean isIdentityDefault(long j) {
            return (j & KEY_EXCHANGE_IDENTITY_DEFAULT_BIT) != 0;
        }

        public static boolean isIdentityUpdate(long j) {
            return (j & KEY_EXCHANGE_IDENTITY_UPDATE_BIT) != 0;
        }

        public static boolean isIdentityVerified(long j) {
            return (j & KEY_EXCHANGE_IDENTITY_VERIFIED_BIT) != 0;
        }

        public static boolean isInboxType(long j) {
            return (j & BASE_TYPE_MASK) == BASE_INBOX_TYPE;
        }

        public static boolean isIncomingAudioCall(long j) {
            return j == 1;
        }

        public static boolean isIncomingVideoCall(long j) {
            return j == INCOMING_VIDEO_CALL_TYPE;
        }

        public static boolean isInvalidMessageType(long j) {
            return (j & BASE_TYPE_MASK) == INVALID_MESSAGE_TYPE;
        }

        public static boolean isInvalidVersionKeyExchange(long j) {
            return (j & KEY_EXCHANGE_INVALID_VERSION_BIT) != 0;
        }

        public static boolean isJoinedType(long j) {
            return (j & BASE_TYPE_MASK) == JOINED_TYPE;
        }

        public static boolean isKeyExchangeType(long j) {
            return (j & KEY_EXCHANGE_BIT) != 0;
        }

        public static boolean isLegacyType(long j) {
            return ((ENCRYPTION_REMOTE_LEGACY_BIT & j) == 0 && (j & ENCRYPTION_REMOTE_BIT) == 0) ? false : true;
        }

        public static boolean isMissedAudioCall(long j) {
            return j == MISSED_AUDIO_CALL_TYPE;
        }

        public static boolean isMissedVideoCall(long j) {
            return j == MISSED_VIDEO_CALL_TYPE;
        }

        public static boolean isNoRemoteSessionType(long j) {
            return (j & ENCRYPTION_REMOTE_NO_SESSION_BIT) != 0;
        }

        public static boolean isOutgoingAudioCall(long j) {
            return j == OUTGOING_AUDIO_CALL_TYPE;
        }

        public static boolean isOutgoingVideoCall(long j) {
            return j == OUTGOING_VIDEO_CALL_TYPE;
        }

        public static boolean isPendingInsecureSmsFallbackType(long j) {
            return (j & BASE_TYPE_MASK) == BASE_PENDING_INSECURE_SMS_FALLBACK;
        }

        public static boolean isPendingMessageType(long j) {
            long j2 = j & BASE_TYPE_MASK;
            return j2 == BASE_OUTBOX_TYPE || j2 == BASE_SENDING_TYPE;
        }

        public static boolean isPendingSecureSmsFallbackType(long j) {
            return (j & BASE_TYPE_MASK) == BASE_PENDING_SECURE_SMS_FALLBACK;
        }

        public static boolean isPendingSmsFallbackType(long j) {
            long j2 = j & BASE_TYPE_MASK;
            return j2 == BASE_PENDING_INSECURE_SMS_FALLBACK || j2 == BASE_PENDING_SECURE_SMS_FALLBACK;
        }

        public static boolean isProfileChange(long j) {
            return j == 7;
        }

        public static boolean isPushType(long j) {
            return (j & PUSH_MESSAGE_BIT) != 0;
        }

        public static boolean isRateLimited(long j) {
            return (j & MESSAGE_RATE_LIMITED_BIT) != 0;
        }

        public static boolean isSecureType(long j) {
            return (j & SECURE_MESSAGE_BIT) != 0;
        }

        public static boolean isSentType(long j) {
            return (j & BASE_TYPE_MASK) == BASE_SENT_TYPE;
        }

        public static boolean isStoryReaction(long j) {
            return (j & SPECIAL_TYPES_MASK) == SPECIAL_TYPE_STORY_REACTION;
        }

        public static boolean isUnsupportedMessageType(long j) {
            return (j & BASE_TYPE_MASK) == UNSUPPORTED_MESSAGE_TYPE;
        }

        public static long translateFromSystemBaseType(long j) {
            int i = (int) j;
            if (i == 2) {
                return BASE_SENT_TYPE;
            }
            if (i != 3) {
                return i != 4 ? i != 5 ? i != 6 ? BASE_INBOX_TYPE : BASE_OUTBOX_TYPE : BASE_SENT_FAILED_TYPE : BASE_OUTBOX_TYPE;
            }
            return 27;
        }

        public static boolean isOutgoingMessageType(long j) {
            for (long j2 : OUTGOING_MESSAGE_TYPES) {
                if ((BASE_TYPE_MASK & j) == j2) {
                    return true;
                }
            }
            return false;
        }

        public static boolean isCallLog(long j) {
            return isIncomingAudioCall(j) || isIncomingVideoCall(j) || isOutgoingAudioCall(j) || isOutgoingVideoCall(j) || isMissedAudioCall(j) || isMissedVideoCall(j) || isGroupCall(j);
        }

        public static int translateToSystemBaseType(long j) {
            if (isInboxType(j)) {
                return 1;
            }
            if (isOutgoingMessageType(j)) {
                return 2;
            }
            if (isFailedMessageType(j)) {
                return 5;
            }
            return 1;
        }
    }
}
