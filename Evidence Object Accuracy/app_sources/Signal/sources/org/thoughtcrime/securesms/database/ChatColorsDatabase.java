package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* compiled from: ChatColorsDatabase.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\rJ\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\n0\u000fJ\u0010\u0010\u0010\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nH\u0002J\b\u0010\u0011\u001a\u00020\bH\u0002J\u000e\u0010\u0012\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nJ\u0010\u0010\u0013\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nH\u0002J\f\u0010\u0014\u001a\u00020\n*\u00020\u0015H\u0002J\f\u0010\u0016\u001a\u00020\u0017*\u00020\u0015H\u0002¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/database/ChatColorsDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "deleteChatColors", "", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "getById", "chatColorsId", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "getSavedChatColors", "", "insertChatColors", "notifyListeners", "saveChatColors", "updateChatColors", "getChatColors", "Landroid/database/Cursor;", "getId", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorsDatabase extends Database {
    private static final String CHAT_COLORS;
    public static final String CREATE_TABLE = "CREATE TABLE chat_colors (\n  _id INTEGER PRIMARY KEY AUTOINCREMENT,\n  chat_colors BLOB\n)";
    public static final Companion Companion = new Companion(null);
    private static final String ID;
    private static final String TABLE_NAME;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ChatColorsDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    /* compiled from: ChatColorsDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/database/ChatColorsDatabase$Companion;", "", "()V", "CHAT_COLORS", "", "CREATE_TABLE", "ID", "TABLE_NAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final ChatColors getById(ChatColors.Id id) {
        Intrinsics.checkNotNullParameter(id, "chatColorsId");
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("chat_colors", new String[]{"_id", "chat_colors"}, "_id = ?", SqlUtil.buildArgs(id.getLongValue()), null, null, null);
        if (query != null) {
            th = null;
            try {
                if (query.moveToFirst()) {
                    return getChatColors(query);
                }
                Unit unit = Unit.INSTANCE;
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        throw new IllegalArgumentException("Could not locate chat color " + id);
    }

    public final ChatColors saveChatColors(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        ChatColors.Id id = chatColors.getId();
        if (id instanceof ChatColors.Id.Auto) {
            throw new AssertionError("Saving 'auto' does not make sense");
        } else if (id instanceof ChatColors.Id.BuiltIn) {
            return chatColors;
        } else {
            if (id instanceof ChatColors.Id.NotSet) {
                return insertChatColors(chatColors);
            }
            if (id instanceof ChatColors.Id.Custom) {
                return updateChatColors(chatColors);
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    public final List<ChatColors> getSavedChatColors() {
        ArrayList arrayList = new ArrayList();
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query("chat_colors", new String[]{"_id", "chat_colors"}, null, null, null, null, null);
        if (query != null) {
            th = null;
            while (query.moveToNext()) {
                try {
                    arrayList.add(getChatColors(query));
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        return arrayList;
    }

    private final ChatColors insertChatColors(ChatColors chatColors) {
        if (Intrinsics.areEqual(chatColors.getId(), ChatColors.Id.NotSet.INSTANCE)) {
            SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
            ContentValues contentValues = new ContentValues(1);
            contentValues.put("chat_colors", chatColors.serialize().toByteArray());
            long insert = signalWritableDatabase.insert("chat_colors", (String) null, contentValues);
            if (insert != -1) {
                notifyListeners();
                return chatColors.withId(ChatColors.Id.Companion.forLongValue(insert));
            }
            throw new IllegalStateException("Failed to insert ChatColor into database");
        }
        throw new IllegalArgumentException("Bad chat colors to insert.");
    }

    private final ChatColors updateChatColors(ChatColors chatColors) {
        if (Intrinsics.areEqual(chatColors.getId(), ChatColors.Id.NotSet.INSTANCE) || Intrinsics.areEqual(chatColors.getId(), ChatColors.Id.BuiltIn.INSTANCE)) {
            throw new IllegalArgumentException("Bad chat colors to update.");
        }
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("chat_colors", chatColors.serialize().toByteArray());
        if (signalWritableDatabase.update("chat_colors", contentValues, "_id = ?", SqlUtil.buildArgs(chatColors.getId().getLongValue())) >= 1) {
            ChatColors chatColors2 = SignalStore.chatColorsValues().getChatColors();
            if (Intrinsics.areEqual(chatColors2 != null ? chatColors2.getId() : null, chatColors.getId())) {
                SignalStore.chatColorsValues().setChatColors(chatColors);
            }
            SignalDatabase.Companion.recipients().onUpdatedChatColors(chatColors);
            notifyListeners();
            return chatColors;
        }
        throw new IllegalStateException("Failed to update ChatColor in database");
    }

    public final void deleteChatColors(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        if (Intrinsics.areEqual(chatColors.getId(), ChatColors.Id.NotSet.INSTANCE) || Intrinsics.areEqual(chatColors.getId(), ChatColors.Id.BuiltIn.INSTANCE)) {
            throw new IllegalArgumentException("Cannot delete this chat color");
        }
        this.databaseHelper.getSignalWritableDatabase().delete("chat_colors", "_id = ?", SqlUtil.buildArgs(chatColors.getId().getLongValue()));
        ChatColors chatColors2 = SignalStore.chatColorsValues().getChatColors();
        if (Intrinsics.areEqual(chatColors2 != null ? chatColors2.getId() : null, chatColors.getId())) {
            SignalStore.chatColorsValues().setChatColors(null);
        }
        SignalDatabase.Companion.recipients().onDeletedChatColors(chatColors);
        notifyListeners();
    }

    private final void notifyListeners() {
        ApplicationDependencies.getDatabaseObserver().notifyChatColorsListeners();
    }

    private final long getId(Cursor cursor) {
        return CursorUtil.requireLong(cursor, "_id");
    }

    private final ChatColors getChatColors(Cursor cursor) {
        ChatColors.Companion companion = ChatColors.Companion;
        ChatColors.Id forLongValue = ChatColors.Id.Companion.forLongValue(getId(cursor));
        ChatColor parseFrom = ChatColor.parseFrom(CursorUtil.requireBlob(cursor, "chat_colors"));
        Intrinsics.checkNotNullExpressionValue(parseFrom, "parseFrom(CursorUtil.req…eBlob(this, CHAT_COLORS))");
        return companion.forChatColor(forLongValue, parseFrom);
    }
}
