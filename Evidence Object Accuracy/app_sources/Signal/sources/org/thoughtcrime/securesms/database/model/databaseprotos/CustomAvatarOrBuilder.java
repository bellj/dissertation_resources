package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.thoughtcrime.securesms.database.model.databaseprotos.CustomAvatar;

/* loaded from: classes4.dex */
public interface CustomAvatarOrBuilder extends MessageLiteOrBuilder {
    CustomAvatar.AvatarCase getAvatarCase();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    CustomAvatar.Photo getPhoto();

    CustomAvatar.Text getText();

    CustomAvatar.Vector getVector();

    boolean hasPhoto();

    boolean hasText();

    boolean hasVector();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
