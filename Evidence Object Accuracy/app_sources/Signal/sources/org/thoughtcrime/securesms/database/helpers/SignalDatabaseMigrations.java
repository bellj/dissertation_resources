package org.thoughtcrime.securesms.database.helpers;

import android.app.Application;
import android.app.NotificationChannel;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.preference.PreferenceManager;
import com.google.protobuf.InvalidProtocolBufferException;
import java.io.File;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.CursorExtensionsKt;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.KeyValueDatabase;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.ReactionDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.ReactionList;
import org.thoughtcrime.securesms.keyvalue.AccountValues;
import org.whispersystems.signalservice.api.push.ACI;

/* compiled from: SignalDatabaseMigrations.kt */
@Metadata(d1 = {"\u0000K\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0003\b\u0001\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0001\u001a\u0005\u0018\u00010\u00012\b\u0010\u0001\u001a\u00030\u0001H\u0002J0\u0010\u0001\u001a\u00030 \u00012\b\u0010\u0001\u001a\u00030\u00012\b\u0010¡\u0001\u001a\u00030¢\u00012\u0007\u0010£\u0001\u001a\u00020\u00042\u0007\u0010¤\u0001\u001a\u00020\u0004H\u0007J\u001d\u0010¥\u0001\u001a\u00030 \u00012\b\u0010\u0001\u001a\u00030¦\u00012\u0007\u0010£\u0001\u001a\u00020\u0004H\u0007J(\u0010§\u0001\u001a\u00030 \u00012\b\u0010¡\u0001\u001a\u00030¢\u00012\b\u0010¨\u0001\u001a\u00030©\u00012\b\u0010ª\u0001\u001a\u00030«\u0001H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010'\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00101\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00104\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00105\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00106\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00107\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00108\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010:\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010;\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010>\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010?\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010C\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010D\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010E\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010F\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010G\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010H\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010I\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010J\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010K\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010L\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010M\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010N\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010O\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010P\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010Q\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010R\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010S\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010T\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010U\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010V\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010W\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010X\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010Y\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010Z\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010[\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\\\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010]\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010^\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010_\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010`\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010a\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010c\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010d\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010g\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010h\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010i\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010j\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010k\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010l\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010m\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010o\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010p\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010q\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010s\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010u\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010v\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010w\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010x\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010y\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010z\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010{\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010|\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010}\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010~\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0001\u001a\u00030\u0001X\u0004¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000f\u0010\u0001\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006¬\u0001"}, d2 = {"Lorg/thoughtcrime/securesms/database/helpers/SignalDatabaseMigrations;", "", "()V", "ABANDONED_ATTACHMENT_CLEANUP", "", "ABANDONED_MESSAGE_CLEANUP", "ABOUT", "ALLOW_STORY_REPLIES", "ATTACHMENT_CAPTIONS", "ATTACHMENT_CAPTIONS_FIX", "ATTACHMENT_CDN_NUMBER", "ATTACHMENT_CLEAR_HASHES", "ATTACHMENT_CLEAR_HASHES_2", "ATTACHMENT_DIMENSIONS", "ATTACHMENT_DISPLAY_ORDER", "ATTACHMENT_FILE_INDEX", "ATTACHMENT_HASHING", "ATTACHMENT_TRANSFORM_PROPERTIES", "ATTACHMENT_UPLOAD_TIMESTAMP", "AVATAR_COLORS", "AVATAR_LOCATION_MIGRATION", "AVATAR_PICKER", "BADGES", "BAD_IMPORT_CLEANUP", "BLUR_AVATARS", "BLUR_HASH", "BORDERLESS", "CAPABILITIES_REFACTOR", "CDS_V2", "CHAT_COLORS", "CLEANUP_SESSION_MIGRATION", "CLEAN_DELETED_DISTRIBUTION_LISTS", "CLEAN_REACTION_NOTIFICATIONS", "CLEAN_STORAGE_IDS", "CLEAN_STORAGE_IDS_WITHOUT_INFO", "CLEAN_UP_GV1_IDS", "CLEAR_MMS_STORAGE_IDS", "CLEAR_PROFILE_KEY_CREDENTIALS", "COLOR_MIGRATION", "CONVERSATION_SEARCH", "DATABASE_VERSION", "DONATION_RECEIPTS", "EMOJI_SEARCH", "EXPIRING_PROFILE_CREDENTIALS", "FULL_TEXT_SEARCH", "GROUPS_V2", "GROUPS_V2_RECIPIENT_CAPABILITY", "GROUP_CALL_RING_TABLE", "GROUP_SERVICE_ID", "GROUP_STORIES", "GROUP_STORY_NOTIFICATIONS", "GROUP_STORY_REPLY_CLEANUP", "GV1_MIGRATION", "GV1_MIGRATION_LAST_SEEN", "GV1_MIGRATION_REFACTOR", "IDENTITY_MIGRATION", "JOBMANAGER_STRIKES_BACK", "JOB_INPUT_DATA", "KEY_VALUE_STORE", "LAST_PROFILE_FETCH", "LAST_RESET_SESSION_TIME", "LAST_SCROLLED", "MEGAPHONES", "MEGAPHONE_FIRST_APPEARANCE", "MENTIONS", "MENTION_CLEANUP", "MENTION_CLEANUP_V2", "MENTION_GLOBAL_SETTING_MIGRATION", "MESSAGE_DUPE_INDEX", "MESSAGE_LOG", "MESSAGE_LOG_2", "MESSAGE_RANGES", "MIGRATE_PREKEYS_VERSION", "MIGRATE_SESSIONS_VERSION", "MMS_AUTOINCREMENT", "MMS_COUNT_INDEX", "MMS_RECIPIENT_CLEANUP", "MMS_RECIPIENT_CLEANUP_2", "MP4_GIF_SUPPORT", "MY_STORY_PRIVACY_MODE", "NOTIFICATION_CHANNELS", "NOTIFICATION_PROFILES", "NOTIFICATION_PROFILES_END_FIX", "NOTIFICATION_RECIPIENT_IDS", "NOTIFIED_TIMESTAMP", "NO_MORE_IMAGE_THUMBNAILS_VERSION", "PAYMENTS", "PINNED_CONVERSATIONS", "PNI", "PNI_CLEANUP", "PNI_STORES", "PREVIEWS", "PROFILE_DATA_MIGRATION", "PROFILE_KEY_CREDENTIALS", "PROFILE_KEY_TO_DB", "QUOTED_REPLIES", "QUOTE_CLEANUP", "QUOTE_INDEX", "QUOTE_MISSING", "QUOTE_TYPE", "REACTIONS", "REACTIONS_UNREAD_INDEX", "REACTION_BACKUP_CLEANUP", "REACTION_CLEANUP", "REACTION_REFACTOR", "REACTION_REMOTE_DELETE_CLEANUP", "REACTION_TRIGGER_FIX", "RECEIPT_TIMESTAMP", "RECIPIENT_CALL_RINGTONE_VERSION", "RECIPIENT_CLEANUP", "RECIPIENT_FORCE_SMS_SELECTION", "RECIPIENT_IDS", "RECIPIENT_SEARCH", "REMAPPED_RECORDS", "REMOTE_DELETE", "REMOTE_MEGAPHONE", "REMOVE_KNOWN_UNKNOWNS", "RESUMABLE_DOWNLOADS", "REVEALABLE_MESSAGES", "SECRET_SENDER", "SELF_ATTACHMENT_CLEANUP", "SENDER_KEY", "SENDER_KEY_SHARED_TIMESTAMP", "SENDER_KEY_UUID", "SERVER_DELIVERED_TIMESTAMP", "SERVER_GUID", "SERVER_TIMESTAMP", "SESSION_MIGRATION", "SHARED_CONTACTS", "SPLIT_PROFILE_NAMES", "SPLIT_SYSTEM_NAMES", "STICKERS", "STICKER_CONTENT_TYPE", "STICKER_CONTENT_TYPE_CLEANUP", "STICKER_EMOJI_IN_NOTIFICATIONS", "STICKER_PACK_ORDER", "STORAGE_SERVICE", "STORAGE_SERVICE_ACTIVE", "STORAGE_SERVICE_REFACTOR", "STORIES", "STORY_SENDS", "STORY_SYNCS", "STORY_TYPE_AND_DISTRIBUTION", "TAG", "", "THREAD_AUTOINCREMENT", "THREAD_CLEANUP", "THUMBNAIL_CLEANUP", "TRANSFER_FILE_CLEANUP", "UNKNOWN_STORAGE_FIELDS", "USERNAMES", "UUIDS", "VIEWED_RECEIPTS", "VIEW_ONCE_ONLY", "WALLPAPER", "getLocalAci", "Lorg/whispersystems/signalservice/api/push/ACI;", "context", "Landroid/app/Application;", "migrate", "", "db", "Lnet/zetetic/database/sqlcipher/SQLiteDatabase;", "oldVersion", "newVersion", "migratePostTransaction", "Landroid/content/Context;", "migrateReaction", "cursor", "Landroid/database/Cursor;", "isMms", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SignalDatabaseMigrations {
    private static final int ABANDONED_ATTACHMENT_CLEANUP;
    private static final int ABANDONED_MESSAGE_CLEANUP;
    private static final int ABOUT;
    private static final int ALLOW_STORY_REPLIES;
    private static final int ATTACHMENT_CAPTIONS;
    private static final int ATTACHMENT_CAPTIONS_FIX;
    private static final int ATTACHMENT_CDN_NUMBER;
    private static final int ATTACHMENT_CLEAR_HASHES;
    private static final int ATTACHMENT_CLEAR_HASHES_2;
    private static final int ATTACHMENT_DIMENSIONS;
    private static final int ATTACHMENT_DISPLAY_ORDER;
    private static final int ATTACHMENT_FILE_INDEX;
    private static final int ATTACHMENT_HASHING;
    private static final int ATTACHMENT_TRANSFORM_PROPERTIES;
    private static final int ATTACHMENT_UPLOAD_TIMESTAMP;
    private static final int AVATAR_COLORS;
    private static final int AVATAR_LOCATION_MIGRATION;
    private static final int AVATAR_PICKER;
    private static final int BADGES;
    private static final int BAD_IMPORT_CLEANUP;
    private static final int BLUR_AVATARS;
    private static final int BLUR_HASH;
    private static final int BORDERLESS;
    private static final int CAPABILITIES_REFACTOR;
    private static final int CDS_V2;
    private static final int CHAT_COLORS;
    private static final int CLEANUP_SESSION_MIGRATION;
    private static final int CLEAN_DELETED_DISTRIBUTION_LISTS;
    private static final int CLEAN_REACTION_NOTIFICATIONS;
    private static final int CLEAN_STORAGE_IDS;
    private static final int CLEAN_STORAGE_IDS_WITHOUT_INFO;
    private static final int CLEAN_UP_GV1_IDS;
    private static final int CLEAR_MMS_STORAGE_IDS;
    private static final int CLEAR_PROFILE_KEY_CREDENTIALS;
    private static final int COLOR_MIGRATION;
    private static final int CONVERSATION_SEARCH;
    public static final int DATABASE_VERSION;
    private static final int DONATION_RECEIPTS;
    private static final int EMOJI_SEARCH;
    private static final int EXPIRING_PROFILE_CREDENTIALS;
    private static final int FULL_TEXT_SEARCH;
    private static final int GROUPS_V2;
    private static final int GROUPS_V2_RECIPIENT_CAPABILITY;
    private static final int GROUP_CALL_RING_TABLE;
    private static final int GROUP_SERVICE_ID;
    private static final int GROUP_STORIES;
    private static final int GROUP_STORY_NOTIFICATIONS;
    private static final int GROUP_STORY_REPLY_CLEANUP;
    private static final int GV1_MIGRATION;
    private static final int GV1_MIGRATION_LAST_SEEN;
    private static final int GV1_MIGRATION_REFACTOR;
    private static final int IDENTITY_MIGRATION;
    public static final SignalDatabaseMigrations INSTANCE;
    private static final int JOBMANAGER_STRIKES_BACK;
    private static final int JOB_INPUT_DATA;
    private static final int KEY_VALUE_STORE;
    private static final int LAST_PROFILE_FETCH;
    private static final int LAST_RESET_SESSION_TIME;
    private static final int LAST_SCROLLED;
    private static final int MEGAPHONES;
    private static final int MEGAPHONE_FIRST_APPEARANCE;
    private static final int MENTIONS;
    private static final int MENTION_CLEANUP;
    private static final int MENTION_CLEANUP_V2;
    private static final int MENTION_GLOBAL_SETTING_MIGRATION;
    private static final int MESSAGE_DUPE_INDEX;
    private static final int MESSAGE_LOG;
    private static final int MESSAGE_LOG_2;
    private static final int MESSAGE_RANGES;
    private static final int MIGRATE_PREKEYS_VERSION;
    private static final int MIGRATE_SESSIONS_VERSION;
    private static final int MMS_AUTOINCREMENT;
    private static final int MMS_COUNT_INDEX;
    private static final int MMS_RECIPIENT_CLEANUP;
    private static final int MMS_RECIPIENT_CLEANUP_2;
    private static final int MP4_GIF_SUPPORT;
    private static final int MY_STORY_PRIVACY_MODE;
    private static final int NOTIFICATION_CHANNELS;
    private static final int NOTIFICATION_PROFILES;
    private static final int NOTIFICATION_PROFILES_END_FIX;
    private static final int NOTIFICATION_RECIPIENT_IDS;
    private static final int NOTIFIED_TIMESTAMP;
    private static final int NO_MORE_IMAGE_THUMBNAILS_VERSION;
    private static final int PAYMENTS;
    private static final int PINNED_CONVERSATIONS;
    private static final int PNI;
    private static final int PNI_CLEANUP;
    private static final int PNI_STORES;
    private static final int PREVIEWS;
    private static final int PROFILE_DATA_MIGRATION;
    private static final int PROFILE_KEY_CREDENTIALS;
    private static final int PROFILE_KEY_TO_DB;
    private static final int QUOTED_REPLIES;
    private static final int QUOTE_CLEANUP;
    private static final int QUOTE_INDEX;
    private static final int QUOTE_MISSING;
    private static final int QUOTE_TYPE;
    private static final int REACTIONS;
    private static final int REACTIONS_UNREAD_INDEX;
    private static final int REACTION_BACKUP_CLEANUP;
    private static final int REACTION_CLEANUP;
    private static final int REACTION_REFACTOR;
    private static final int REACTION_REMOTE_DELETE_CLEANUP;
    private static final int REACTION_TRIGGER_FIX;
    private static final int RECEIPT_TIMESTAMP;
    private static final int RECIPIENT_CALL_RINGTONE_VERSION;
    private static final int RECIPIENT_CLEANUP;
    private static final int RECIPIENT_FORCE_SMS_SELECTION;
    private static final int RECIPIENT_IDS;
    private static final int RECIPIENT_SEARCH;
    private static final int REMAPPED_RECORDS;
    private static final int REMOTE_DELETE;
    private static final int REMOTE_MEGAPHONE;
    private static final int REMOVE_KNOWN_UNKNOWNS;
    private static final int RESUMABLE_DOWNLOADS;
    private static final int REVEALABLE_MESSAGES;
    private static final int SECRET_SENDER;
    private static final int SELF_ATTACHMENT_CLEANUP;
    private static final int SENDER_KEY;
    private static final int SENDER_KEY_SHARED_TIMESTAMP;
    private static final int SENDER_KEY_UUID;
    private static final int SERVER_DELIVERED_TIMESTAMP;
    private static final int SERVER_GUID;
    private static final int SERVER_TIMESTAMP;
    private static final int SESSION_MIGRATION;
    private static final int SHARED_CONTACTS;
    private static final int SPLIT_PROFILE_NAMES;
    private static final int SPLIT_SYSTEM_NAMES;
    private static final int STICKERS;
    private static final int STICKER_CONTENT_TYPE;
    private static final int STICKER_CONTENT_TYPE_CLEANUP;
    private static final int STICKER_EMOJI_IN_NOTIFICATIONS;
    private static final int STICKER_PACK_ORDER;
    private static final int STORAGE_SERVICE;
    private static final int STORAGE_SERVICE_ACTIVE;
    private static final int STORAGE_SERVICE_REFACTOR;
    private static final int STORIES;
    private static final int STORY_SENDS;
    private static final int STORY_SYNCS;
    private static final int STORY_TYPE_AND_DISTRIBUTION;
    private static final String TAG;
    private static final int THREAD_AUTOINCREMENT;
    private static final int THREAD_CLEANUP;
    private static final int THUMBNAIL_CLEANUP;
    private static final int TRANSFER_FILE_CLEANUP;
    private static final int UNKNOWN_STORAGE_FIELDS;
    private static final int USERNAMES;
    private static final int UUIDS;
    private static final int VIEWED_RECEIPTS;
    private static final int VIEW_ONCE_ONLY;
    private static final int WALLPAPER;

    private SignalDatabaseMigrations() {
    }

    static {
        SignalDatabaseMigrations signalDatabaseMigrations = new SignalDatabaseMigrations();
        INSTANCE = signalDatabaseMigrations;
        String tag = Log.tag(signalDatabaseMigrations.getClass());
        Intrinsics.checkNotNullExpressionValue(tag, "tag(SignalDatabaseMigrations.javaClass)");
        TAG = tag;
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Multi-variable search result rejected for r2v76, resolved type: android.content.SharedPreferences */
    /* JADX DEBUG: Multi-variable search result rejected for r2v103, resolved type: android.content.SharedPreferences */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v9, types: [java.lang.Throwable, java.lang.String] */
    /* JADX WARN: Type inference failed for: r1v485 */
    /* JADX WARN: Type inference failed for: r1v497 */
    /* JADX WARNING: Removed duplicated region for block: B:1046:0x07f5 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:1047:0x07ea A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:1108:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x06af  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x06b6  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x06ec  */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x06f2  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x0700  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x0748  */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x08ca  */
    /* JADX WARNING: Removed duplicated region for block: B:225:0x08d3  */
    /* JADX WARNING: Removed duplicated region for block: B:226:0x090b  */
    /* JADX WARNING: Removed duplicated region for block: B:229:0x0911  */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x091a  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x0923  */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x092c  */
    /* JADX WARNING: Removed duplicated region for block: B:241:0x093a  */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x0948  */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x096a  */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x0991  */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x099f  */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x09a8  */
    /* JADX WARNING: Removed duplicated region for block: B:259:0x09b1  */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x09ba  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x09c8  */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x09d1  */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x09da  */
    /* JADX WARNING: Removed duplicated region for block: B:274:0x09e3  */
    /* JADX WARNING: Removed duplicated region for block: B:286:0x0a33  */
    /* JADX WARNING: Removed duplicated region for block: B:289:0x0a3c  */
    /* JADX WARNING: Removed duplicated region for block: B:292:0x0a49  */
    /* JADX WARNING: Removed duplicated region for block: B:305:0x0abc  */
    /* JADX WARNING: Removed duplicated region for block: B:308:0x0ac4  */
    /* JADX WARNING: Removed duplicated region for block: B:311:0x0acf  */
    /* JADX WARNING: Removed duplicated region for block: B:324:0x0b6a  */
    /* JADX WARNING: Removed duplicated region for block: B:330:0x0bae  */
    /* JADX WARNING: Removed duplicated region for block: B:336:0x0bd9  */
    /* JADX WARNING: Removed duplicated region for block: B:339:0x0bec  */
    /* JADX WARNING: Removed duplicated region for block: B:342:0x0bf5  */
    /* JADX WARNING: Removed duplicated region for block: B:345:0x0bfe  */
    /* JADX WARNING: Removed duplicated region for block: B:348:0x0c07  */
    /* JADX WARNING: Removed duplicated region for block: B:351:0x0c1f  */
    /* JADX WARNING: Removed duplicated region for block: B:354:0x0c2d  */
    /* JADX WARNING: Removed duplicated region for block: B:369:0x0c87  */
    /* JADX WARNING: Removed duplicated region for block: B:372:0x0c90  */
    /* JADX WARNING: Removed duplicated region for block: B:375:0x0c99  */
    /* JADX WARNING: Removed duplicated region for block: B:378:0x0ca2  */
    /* JADX WARNING: Removed duplicated region for block: B:395:0x0d55  */
    /* JADX WARNING: Removed duplicated region for block: B:398:0x0d5f  */
    /* JADX WARNING: Removed duplicated region for block: B:401:0x0d68  */
    /* JADX WARNING: Removed duplicated region for block: B:404:0x0d76  */
    /* JADX WARNING: Removed duplicated region for block: B:407:0x0d98  */
    /* JADX WARNING: Removed duplicated region for block: B:410:0x0da6  */
    /* JADX WARNING: Removed duplicated region for block: B:413:0x0dd3  */
    /* JADX WARNING: Removed duplicated region for block: B:416:0x0ddc  */
    /* JADX WARNING: Removed duplicated region for block: B:419:0x0de5  */
    /* JADX WARNING: Removed duplicated region for block: B:422:0x0dee  */
    /* JADX WARNING: Removed duplicated region for block: B:443:0x0e89  */
    /* JADX WARNING: Removed duplicated region for block: B:446:0x0ec1  */
    /* JADX WARNING: Removed duplicated region for block: B:474:0x0f63  */
    /* JADX WARNING: Removed duplicated region for block: B:502:0x1035  */
    /* JADX WARNING: Removed duplicated region for block: B:505:0x103f  */
    /* JADX WARNING: Removed duplicated region for block: B:508:0x105a  */
    /* JADX WARNING: Removed duplicated region for block: B:511:0x106f  */
    /* JADX WARNING: Removed duplicated region for block: B:523:0x10fb  */
    /* JADX WARNING: Removed duplicated region for block: B:526:0x1101  */
    /* JADX WARNING: Removed duplicated region for block: B:529:0x110f  */
    /* JADX WARNING: Removed duplicated region for block: B:532:0x1118  */
    /* JADX WARNING: Removed duplicated region for block: B:535:0x1121  */
    /* JADX WARNING: Removed duplicated region for block: B:582:0x12e9  */
    /* JADX WARNING: Removed duplicated region for block: B:585:0x131c  */
    /* JADX WARNING: Removed duplicated region for block: B:588:0x134d  */
    /* JADX WARNING: Removed duplicated region for block: B:591:0x1356  */
    /* JADX WARNING: Removed duplicated region for block: B:594:0x1364  */
    /* JADX WARNING: Removed duplicated region for block: B:597:0x1372  */
    /* JADX WARNING: Removed duplicated region for block: B:600:0x1385  */
    /* JADX WARNING: Removed duplicated region for block: B:603:0x139d  */
    /* JADX WARNING: Removed duplicated region for block: B:604:0x13ca  */
    /* JADX WARNING: Removed duplicated region for block: B:607:0x13d0  */
    /* JADX WARNING: Removed duplicated region for block: B:610:0x13d9  */
    /* JADX WARNING: Removed duplicated region for block: B:613:0x1439  */
    /* JADX WARNING: Removed duplicated region for block: B:616:0x1467  */
    /* JADX WARNING: Removed duplicated region for block: B:661:0x1633  */
    /* JADX WARNING: Removed duplicated region for block: B:664:0x163f  */
    /* JADX WARNING: Removed duplicated region for block: B:689:0x173c  */
    /* JADX WARNING: Removed duplicated region for block: B:692:0x1745  */
    /* JADX WARNING: Removed duplicated region for block: B:695:0x1775  */
    /* JADX WARNING: Removed duplicated region for block: B:698:0x1783  */
    /* JADX WARNING: Removed duplicated region for block: B:704:0x17f4  */
    /* JADX WARNING: Removed duplicated region for block: B:718:0x184a  */
    /* JADX WARNING: Removed duplicated region for block: B:721:0x1853  */
    /* JADX WARNING: Removed duplicated region for block: B:726:0x188c A[Catch: all -> 0x18c6, LOOP:19: B:951:0x1886->B:726:0x188c, LOOP_END, TryCatch #16 {all -> 0x18c6, blocks: (B:724:0x1886, B:726:0x188c, B:727:0x18bf), top: B:951:0x1886 }] */
    /* JADX WARNING: Removed duplicated region for block: B:737:0x18d3  */
    /* JADX WARNING: Removed duplicated region for block: B:740:0x18eb  */
    /* JADX WARNING: Removed duplicated region for block: B:743:0x1912  */
    /* JADX WARNING: Removed duplicated region for block: B:746:0x196b  */
    /* JADX WARNING: Removed duplicated region for block: B:747:0x19b1  */
    /* JADX WARNING: Removed duplicated region for block: B:750:0x19b9  */
    /* JADX WARNING: Removed duplicated region for block: B:753:0x1a0a  */
    /* JADX WARNING: Removed duplicated region for block: B:756:0x1ae2  */
    /* JADX WARNING: Removed duplicated region for block: B:757:0x1aeb  */
    /* JADX WARNING: Removed duplicated region for block: B:760:0x1af1  */
    /* JADX WARNING: Removed duplicated region for block: B:772:0x1b55  */
    /* JADX WARNING: Removed duplicated region for block: B:775:0x1b5e  */
    /* JADX WARNING: Removed duplicated region for block: B:778:0x1b6f  */
    /* JADX WARNING: Removed duplicated region for block: B:781:0x1ba9  */
    /* JADX WARNING: Removed duplicated region for block: B:784:0x1be3  */
    /* JADX WARNING: Removed duplicated region for block: B:787:0x1bf1  */
    /* JADX WARNING: Removed duplicated region for block: B:788:0x1c40  */
    /* JADX WARNING: Removed duplicated region for block: B:791:0x1c46  */
    /* JADX WARNING: Removed duplicated region for block: B:794:0x1c54  */
    /* JADX WARNING: Removed duplicated region for block: B:797:0x1c5d  */
    /* JADX WARNING: Removed duplicated region for block: B:800:0x1c97  */
    /* JADX WARNING: Removed duplicated region for block: B:803:0x1ca0  */
    /* JADX WARNING: Removed duplicated region for block: B:828:0x1d12  */
    /* JADX WARNING: Removed duplicated region for block: B:831:0x1d20  */
    /* JADX WARNING: Removed duplicated region for block: B:834:0x1d3d  */
    /* JADX WARNING: Removed duplicated region for block: B:837:0x1d46  */
    /* JADX WARNING: Removed duplicated region for block: B:840:0x1d4f  */
    /* JADX WARNING: Removed duplicated region for block: B:843:0x1d58  */
    /* JADX WARNING: Removed duplicated region for block: B:846:0x1d61  */
    /* JADX WARNING: Removed duplicated region for block: B:849:0x1d6a  */
    /* JADX WARNING: Removed duplicated region for block: B:852:0x1d7d  */
    /* JADX WARNING: Removed duplicated region for block: B:867:0x1e2b  */
    /* JADX WARNING: Removed duplicated region for block: B:870:0x1e3e  */
    /* JADX WARNING: Removed duplicated region for block: B:873:0x1ee0  */
    /* JADX WARNING: Removed duplicated region for block: B:876:0x1ee9  */
    /* JADX WARNING: Removed duplicated region for block: B:879:0x1ef2  */
    /* JADX WARNING: Removed duplicated region for block: B:882:0x1efb  */
    /* JADX WARNING: Removed duplicated region for block: B:885:0x1f09  */
    /* JADX WARNING: Removed duplicated region for block: B:888:0x1f1c  */
    /* JADX WARNING: Removed duplicated region for block: B:891:0x1f25  */
    /* JADX WARNING: Removed duplicated region for block: B:894:0x1f52  */
    /* JADX WARNING: Removed duplicated region for block: B:897:0x1f60  */
    /* JADX WARNING: Removed duplicated region for block: B:900:0x1f69  */
    /* JADX WARNING: Removed duplicated region for block: B:903:0x1f72  */
    /* JADX WARNING: Removed duplicated region for block: B:906:0x1f99  */
    /* JADX WARNING: Removed duplicated region for block: B:909:0x1fa2  */
    /* JADX WARNING: Removed duplicated region for block: B:912:0x1fab  */
    /* JADX WARNING: Removed duplicated region for block: B:915:0x1fb4  */
    /* JADX WARNING: Removed duplicated region for block: B:918:0x1fbd  */
    /* JADX WARNING: Removed duplicated region for block: B:921:0x1fe9  */
    /* JADX WARNING: Unknown variable types count: 1 */
    @kotlin.jvm.JvmStatic
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void migrate(android.app.Application r41, net.zetetic.database.sqlcipher.SQLiteDatabase r42, int r43, int r44) {
        /*
        // Method dump skipped, instructions count: 8175
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.helpers.SignalDatabaseMigrations.migrate(android.app.Application, net.zetetic.database.sqlcipher.SQLiteDatabase, int, int):void");
    }

    /* renamed from: migrate$lambda-10 */
    public static final boolean m1724migrate$lambda10(NotificationChannel notificationChannel) {
        Intrinsics.checkNotNullParameter(notificationChannel, "c");
        String id = notificationChannel.getId();
        Intrinsics.checkNotNullExpressionValue(id, "c.getId()");
        return StringsKt__StringsJVMKt.startsWith$default(id, "contact_", false, 2, null);
    }

    /* renamed from: migrate$lambda-17 */
    public static final boolean m1725migrate$lambda17(File file, String str) {
        Intrinsics.checkNotNullParameter(str, "name");
        return StringsKt__StringsJVMKt.startsWith$default(str, "transfer", false, 2, null);
    }

    /* renamed from: migrate$lambda-32$lambda-31 */
    public static final boolean m1726migrate$lambda32$lambda31(long j, ReactionList.Reaction reaction) {
        Intrinsics.checkNotNullParameter(reaction, "r");
        return reaction.getReceivedTime() > j;
    }

    /* renamed from: migrate$lambda-34$lambda-33 */
    public static final boolean m1727migrate$lambda34$lambda33(long j, ReactionList.Reaction reaction) {
        Intrinsics.checkNotNullParameter(reaction, "r");
        return reaction.getReceivedTime() > j;
    }

    @JvmStatic
    public static final void migratePostTransaction(Context context, int i) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (i < 3) {
            PreKeyMigrationHelper.cleanUpPreKeys(context);
        }
    }

    private final void migrateReaction(SQLiteDatabase sQLiteDatabase, Cursor cursor, boolean z) {
        try {
            long requireLong = CursorUtil.requireLong(cursor, "_id");
            for (ReactionList.Reaction reaction : ReactionList.parseFrom(CursorUtil.requireBlob(cursor, "reactions")).getReactionsList()) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("message_id", Long.valueOf(requireLong));
                contentValues.put("is_mms", Integer.valueOf(z ? 1 : 0));
                contentValues.put("author_id", Long.valueOf(reaction.getAuthor()));
                contentValues.put("emoji", reaction.getEmoji());
                contentValues.put("date_sent", Long.valueOf(reaction.getSentTime()));
                contentValues.put(MmsSmsColumns.NORMALIZED_DATE_RECEIVED, Long.valueOf(reaction.getReceivedTime()));
                sQLiteDatabase.insert(ReactionDatabase.TABLE_NAME, (String) null, contentValues);
            }
        } catch (InvalidProtocolBufferException unused) {
            Log.w(TAG, "Failed to parse reaction!");
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r11v1, resolved type: android.content.SharedPreferences */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v0, types: [java.lang.Throwable, java.lang.String] */
    private final ACI getLocalAci(Application application) {
        ACI aci;
        th = 0;
        if (!KeyValueDatabase.exists(application)) {
            return ACI.parseOrNull(PreferenceManager.getDefaultSharedPreferences(application).getString("pref_local_uuid", th));
        }
        Cursor query = KeyValueDatabase.getInstance(application).getReadableDatabase().query("key_value", new String[]{DraftDatabase.DRAFT_VALUE}, "key = ?", SqlUtil.buildArgs(AccountValues.KEY_ACI), null, null, null);
        try {
            if (query.moveToFirst()) {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                aci = ACI.parseOrNull(CursorExtensionsKt.requireString(query, DraftDatabase.DRAFT_VALUE));
            } else {
                aci = th;
            }
            return aci;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }
}
