package org.thoughtcrime.securesms.database;

import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import org.signal.ringrtc.CallManager;

/* compiled from: GroupCallRingDatabase.kt */
@Metadata(d1 = {"\u0000\f\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\u001a\f\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002¨\u0006\u0003"}, d2 = {"toCode", "", "Lorg/signal/ringrtc/CallManager$RingUpdate;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GroupCallRingDatabaseKt {

    /* compiled from: GroupCallRingDatabase.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[CallManager.RingUpdate.values().length];
            iArr[CallManager.RingUpdate.REQUESTED.ordinal()] = 1;
            iArr[CallManager.RingUpdate.EXPIRED_REQUEST.ordinal()] = 2;
            iArr[CallManager.RingUpdate.ACCEPTED_ON_ANOTHER_DEVICE.ordinal()] = 3;
            iArr[CallManager.RingUpdate.DECLINED_ON_ANOTHER_DEVICE.ordinal()] = 4;
            iArr[CallManager.RingUpdate.BUSY_LOCALLY.ordinal()] = 5;
            iArr[CallManager.RingUpdate.BUSY_ON_ANOTHER_DEVICE.ordinal()] = 6;
            iArr[CallManager.RingUpdate.CANCELLED_BY_RINGER.ordinal()] = 7;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public static final int toCode(CallManager.RingUpdate ringUpdate) {
        switch (WhenMappings.$EnumSwitchMapping$0[ringUpdate.ordinal()]) {
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            case 5:
                return 4;
            case 6:
                return 5;
            case 7:
                return 6;
            default:
                throw new NoWhenBranchMatchedException();
        }
    }
}
