package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import androidx.core.content.ContentValuesKt;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Optional;
import j$.util.function.Function;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysJvmKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlin.text.StringsKt__IndentKt;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import net.zetetic.database.sqlcipher.SQLiteConstraintException;
import org.signal.core.util.Bitmask;
import org.signal.core.util.CursorExtensionsKt;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.OptionalExtensionsKt;
import org.signal.core.util.SQLiteDatabaseExtensionsKt;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.UpdateBuilderPart1;
import org.signal.core.util.UpdateBuilderPart2;
import org.signal.core.util.UpdateBuilderPart3;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.color.MaterialColor;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.crypto.storage.SignalIdentityKeyStore;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.PnpIdResolver;
import org.thoughtcrime.securesms.database.PnpOperation;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.ProfileAvatarFileDetails;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList;
import org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor;
import org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTime;
import org.thoughtcrime.securesms.database.model.databaseprotos.ExpiringProfileKeyCredentialColumnData;
import org.thoughtcrime.securesms.database.model.databaseprotos.RecipientExtras;
import org.thoughtcrime.securesms.database.model.databaseprotos.Wallpaper;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.v2.ProfileKeySet;
import org.thoughtcrime.securesms.jobs.RequestGroupV2InfoJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageRecordUpdate;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.storage.StorageSyncModels;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.thoughtcrime.securesms.util.IdentityUtil;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaperFactory;
import org.thoughtcrime.securesms.wallpaper.WallpaperStorage;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.storage.SignalAccountRecord;
import org.whispersystems.signalservice.api.storage.SignalContactRecord;
import org.whispersystems.signalservice.api.storage.SignalGroupV1Record;
import org.whispersystems.signalservice.api.storage.SignalGroupV2Record;
import org.whispersystems.signalservice.api.storage.StorageId;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;
import org.whispersystems.signalservice.api.util.Preconditions;

/* compiled from: RecipientDatabase.kt */
@Metadata(bv = {}, d1 = {"\u0000Ø\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u001a\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0016\b\u0016\u0018\u0000 ¶\u00022\u00020\u0001:$·\u0002¸\u0002¹\u0002º\u0002¶\u0002»\u0002¼\u0002½\u0002¾\u0002¿\u0002À\u0002Á\u0002Â\u0002Ã\u0002Ä\u0002Å\u0002Æ\u0002Ç\u0002B\u001d\u0012\b\u0010°\u0002\u001a\u00030¯\u0002\u0012\b\u0010³\u0002\u001a\u00030²\u0002¢\u0006\u0006\b´\u0002\u0010µ\u0002J$\u0010\t\u001a\u00020\b2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002J\u0010\u0010\r\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\nH\u0002J/\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\b\u0010\u000e\u001a\u0004\u0018\u00010\u00042\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000fH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0018\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0017\u001a\u00020\u0016H\u0002J\u001a\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aH\u0002J\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u0015\u001a\u00020\fH\u0002J\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u001e2\u0006\u0010\u0015\u001a\u00020\fH\u0002J\u0010\u0010!\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\fH\u0002J(\u0010(\u001a\b\u0012\u0004\u0012\u00020'0\u00112\b\u0010#\u001a\u0004\u0018\u00010\"2\u0006\u0010$\u001a\u00020\u00062\u0006\u0010&\u001a\u00020%H\u0002J4\u0010+\u001a\b\u0012\u0004\u0012\u00020'0\u00112\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010#\u001a\u0004\u0018\u00010\"2\b\u0010*\u001a\u0004\u0018\u00010)2\u0006\u0010&\u001a\u00020%H\u0002J4\u0010,\u001a\b\u0012\u0004\u0012\u00020'0\u00112\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010#\u001a\u0004\u0018\u00010\"2\b\u0010*\u001a\u0004\u0018\u00010)2\u0006\u0010&\u001a\u00020%H\u0002J\u001e\u0010/\u001a\u00020\u00182\u0014\u0010.\u001a\u0010\u0012\u0004\u0012\u00020\f\u0012\u0006\u0012\u0004\u0018\u00010\n0-H\u0002J$\u00103\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\f2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u000201\u0012\u0004\u0012\u00020100H\u0002J\u0018\u00106\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\f2\u0006\u00105\u001a\u000204H\u0002J\u0018\u00106\u001a\u00020\u00062\u0006\u00108\u001a\u0002072\u0006\u00105\u001a\u000204H\u0002J\u001e\u0010<\u001a\b\u0012\u0004\u0012\u00020\f0;2\u0006\u00109\u001a\u00020\u00042\u0006\u0010:\u001a\u00020\u0004H\u0002J\"\u0010>\u001a\u00020=2\u0006\u00109\u001a\u00020\u00042\u0006\u0010:\u001a\u00020\u00042\b\b\u0002\u00105\u001a\u000204H\u0002J\u0018\u0010A\u001a\u00020\f2\u0006\u0010?\u001a\u00020\f2\u0006\u0010@\u001a\u00020\fH\u0002J\b\u0010B\u001a\u00020\u0018H\u0002J\u001c\u0010C\u001a\u0002042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002J&\u0010D\u001a\u0002042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010#\u001a\u0004\u0018\u00010\"2\b\u0010*\u001a\u0004\u0018\u00010)H\u0002J\u0018\u0010H\u001a\u0002042\u0006\u0010F\u001a\u00020E2\u0006\u0010G\u001a\u00020\u0006H\u0002J\u0018\u0010K\u001a\u0002042\u0006\u0010J\u001a\u00020I2\u0006\u0010G\u001a\u00020\u0006H\u0002J\u0018\u0010N\u001a\u0002042\u0006\u0010M\u001a\u00020L2\u0006\u0010G\u001a\u00020\u0006H\u0002J\u0018\u0010Q\u001a\b\u0012\u0004\u0012\u00020P0\u00112\b\u0010O\u001a\u0004\u0018\u00010\nH\u0002J\u0010\u0010U\u001a\u00020T2\u0006\u0010S\u001a\u00020RH\u0002J\u0012\u0010W\u001a\u0004\u0018\u00010V2\u0006\u0010S\u001a\u00020RH\u0002J\u0012\u0010Y\u001a\u0004\u0018\u00010X2\u0006\u0010S\u001a\u00020RH\u0002J\u0018\u0010\\\u001a\u00020\u00182\u0006\u0010Z\u001a\u0002042\u0006\u0010[\u001a\u00020\u0012H\u0002J\u0010\u0010]\u001a\u00020\u00042\u0006\u00109\u001a\u00020\u0004H\u0002J\u0018\u0010_\u001a\u00020\u0006\"\u0004\b\u0000\u0010^*\b\u0012\u0004\u0012\u00028\u00000;H\u0002J\f\u0010a\u001a\u00020`*\u00020\u0012H\u0002J\u000e\u0010b\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u0004J\u0014\u0010c\u001a\b\u0012\u0004\u0012\u00020\f0;2\u0006\u0010\u0005\u001a\u00020\u0004J\u0014\u0010e\u001a\b\u0012\u0004\u0012\u00020\f0;2\u0006\u0010d\u001a\u00020\u0004J\u0014\u0010h\u001a\b\u0012\u0004\u0012\u00020\f0;2\u0006\u0010g\u001a\u00020fJ\u0014\u0010i\u001a\b\u0012\u0004\u0012\u00020\f0;2\u0006\u0010\u0003\u001a\u00020\u0002J\u0014\u0010j\u001a\b\u0012\u0004\u0012\u00020\f0;2\u0006\u0010#\u001a\u00020\"J\u0014\u0010l\u001a\b\u0012\u0004\u0012\u00020\f0;2\u0006\u0010k\u001a\u00020\u0004J&\u0010m\u001a\u00020\f2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u0006H\u0007J&\u0010n\u001a\u00020\f2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u0006H\u0007J&\u0010o\u001a\u00020\f2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u0006H\u0007J\u0012\u0010q\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020p0-J\u000e\u0010r\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u0002J\u000e\u0010s\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u0004J\u000e\u0010t\u001a\u00020\f2\u0006\u0010d\u001a\u00020\u0004J\u001c\u0010x\u001a\u00020\f2\u0006\u0010v\u001a\u00020u2\n\b\u0002\u0010w\u001a\u0004\u0018\u00010\nH\u0007J\f\u0010y\u001a\b\u0012\u0004\u0012\u00020\f0\u0011J\u000e\u0010z\u001a\u00020\f2\u0006\u0010g\u001a\u00020fJ\u000e\u0010{\u001a\u00020\f2\u0006\u0010g\u001a\u00020fJ\u0006\u0010|\u001a\u00020\fJ\u0006\u0010}\u001a\u00020RJ\u000e\u0010\u001a\u00020~2\u0006\u0010S\u001a\u00020RJ\u0007\u0010\u0001\u001a\u00020~J\u000f\u0010\u0001\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\fJ\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0015\u001a\u00020\fJ\u0011\u0010\u0001\u001a\u0004\u0018\u00010\u00122\u0006\u0010w\u001a\u00020\nJ\u0017\u0010\u0001\u001a\u00020\u00182\u000e\u0010\u0001\u001a\t\u0012\u0004\u0012\u00020\f0\u0001J\u0017\u0010\u0001\u001a\u00020\u00182\u000e\u0010\u0001\u001a\t\u0012\u0004\u0012\u00020\f0\u0001J\u000f\u0010\u0001\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\fJ\u001d\u0010\u0001\u001a\u00020\u00182\u0014\u0010\u0001\u001a\u000f\u0012\u0004\u0012\u00020\f\u0012\u0005\u0012\u00030\u00010-J\u0010\u0010\u0001\u001a\u00020\u00182\u0007\u0010\u0001\u001a\u00020EJ\u0016\u0010\u0001\u001a\u00020\u00182\r\u00106\u001a\t\u0012\u0004\u0012\u00020E0\u0001J\u0010\u0010\u0001\u001a\u00020\u00182\u0007\u0010\u0001\u001a\u00020IJ\u0016\u0010\u0001\u001a\u00020\u00182\r\u00106\u001a\t\u0012\u0004\u0012\u00020I0\u0001J\u0010\u0010\u0001\u001a\u00020\u00182\u0007\u0010\u0001\u001a\u00020LJ\u0016\u0010\u0001\u001a\u00020\u00182\r\u00106\u001a\t\u0012\u0004\u0012\u00020L0\u0001J\u0017\u0010\u0001\u001a\u00020\u00182\u000e\u00106\u001a\n\u0012\u0005\u0012\u00030\u00010\u0001J \u0010\u0001\u001a\u00020\u00182\u0017\u0010\u0001\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040-J\u000e\u0010\u0001\u001a\t\u0012\u0005\u0012\u00030\u00010\u0011J\u0014\u0010\u0001\u001a\u000f\u0012\u0004\u0012\u00020\f\u0012\u0005\u0012\u00030\u00010-J\u001d\u0010\u0001\u001a\t\u0012\u0004\u0012\u00020\u00040\u00012\r\u0010.\u001a\t\u0012\u0004\u0012\u00020\f0\u0001J\u0015\u0010\u0001\u001a\u00070\u0001R\u00020\u00002\u0007\u0010\u0001\u001a\u00020\u0006J\u0011\u0010\u0001\u001a\u00020\u00182\b\u0010\u0001\u001a\u00030\u0001J\u0011\u0010 \u0001\u001a\u00020\u00182\b\u0010\u0001\u001a\u00030\u0001J\u0012\u0010¤\u0001\u001a\u00030£\u00012\b\u0010¢\u0001\u001a\u00030¡\u0001J\u0007\u0010¥\u0001\u001a\u00020\u0018J\u000f\u0010¦\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\fJ\u0019\u0010¨\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010§\u0001\u001a\u00030\u0001J\u0019\u0010ª\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010©\u0001\u001a\u00030£\u0001J\u0018\u0010¬\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0007\u0010«\u0001\u001a\u00020\u0006J\u0018\u0010®\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0007\u0010­\u0001\u001a\u00020\u0006J\u001a\u0010°\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\t\u0010¯\u0001\u001a\u0004\u0018\u00010\u001eJ\u001a\u0010²\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\t\u0010±\u0001\u001a\u0004\u0018\u00010\u001eJ\u0019\u0010µ\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010´\u0001\u001a\u00030³\u0001J\u0019\u0010¶\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010´\u0001\u001a\u00030³\u0001J\u0019\u0010¹\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010¸\u0001\u001a\u00030·\u0001J \u0010¹\u0001\u001a\u00020\u00182\r\u0010.\u001a\t\u0012\u0004\u0012\u00020\f0\u00012\b\u0010¸\u0001\u001a\u00030·\u0001J\u000f\u0010º\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\fJ\u000f\u0010»\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\fJ\u000f\u0010¼\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\fJ\u0019\u0010¾\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010½\u0001\u001a\u00030£\u0001J\u0019\u0010Á\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010À\u0001\u001a\u00030¿\u0001J\u0019\u0010Ä\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010Ã\u0001\u001a\u00030Â\u0001J\u0010\u0010Å\u0001\u001a\u00030Â\u00012\u0006\u0010\u0015\u001a\u00020\fJ\u001e\u0010Ç\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\r\u0010Æ\u0001\u001a\b\u0012\u0004\u0012\u00020P0\u0011J\u0019\u0010Ê\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010É\u0001\u001a\u00030È\u0001J\u0019\u0010Í\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010Ì\u0001\u001a\u00030Ë\u0001J\u0018\u0010Ï\u0001\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\f2\u0007\u0010Î\u0001\u001a\u00020pJ\u0018\u0010Ð\u0001\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\f2\u0007\u0010Î\u0001\u001a\u00020pJ\"\u0010Ó\u0001\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\f2\u0007\u0010Î\u0001\u001a\u00020p2\b\u0010Ò\u0001\u001a\u00030Ñ\u0001J\u000f\u0010Ô\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\fJ\u0018\u0010×\u0001\u001a\t\u0012\u0004\u0012\u00020\f0\u00012\b\u0010Ö\u0001\u001a\u00030Õ\u0001J\u0017\u0010Ú\u0001\u001a\b\u0012\u0004\u0012\u00020\f0\u00112\b\u0010Ù\u0001\u001a\u00030Ø\u0001J\u0018\u0010Ü\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0007\u0010Û\u0001\u001a\u00020\u0004J\u0019\u0010ß\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010Þ\u0001\u001a\u00030Ý\u0001J\u001a\u0010á\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\t\u0010à\u0001\u001a\u0004\u0018\u00010\u0004J%\u0010ä\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\t\u0010â\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010ã\u0001\u001a\u0004\u0018\u00010\u0004J\u0018\u0010å\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0007\u0010´\u0001\u001a\u00020\u0006J\u001a\u0010ç\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\t\u0010æ\u0001\u001a\u0004\u0018\u00010\u0004J\u0007\u0010è\u0001\u001a\u00020\u0018J\u001a\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\n\u0010ê\u0001\u001a\u0005\u0018\u00010é\u0001J\u0018\u0010ë\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0007\u0010´\u0001\u001a\u00020\u0006J\u0011\u0010í\u0001\u001a\u00030£\u00012\u0007\u0010ì\u0001\u001a\u00020\u001eJ\u0017\u0010î\u0001\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u0004J\u0017\u0010ï\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u0004J\u0017\u0010ð\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u0004J\u000f\u0010ñ\u0001\u001a\u00020\u00182\u0006\u0010\u0005\u001a\u00020\u0004J\u0019\u0010ò\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\b\u0010k\u001a\u0004\u0018\u00010\u0004J\u0018\u0010ô\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0007\u0010ó\u0001\u001a\u00020\u0006J\u000f\u0010õ\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\fJ\u000f\u0010ö\u0001\u001a\u00020\u00182\u0006\u0010k\u001a\u00020\u0004J\u000e\u0010÷\u0001\u001a\t\u0012\u0004\u0012\u00020\u00040\u0001J\u0017\u0010ø\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0006\u0010#\u001a\u00020\"J\u0017\u0010ù\u0001\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u0002J\u0017\u0010ú\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u0002J\u000f\u0010û\u0001\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\fJ.\u0010þ\u0001\u001a\u00020\u00182\u0015\u0010ü\u0001\u001a\u0010\u0012\u0004\u0012\u00020\f\u0012\u0006\u0012\u0004\u0018\u00010\u00020-2\u000e\u0010ý\u0001\u001a\t\u0012\u0004\u0012\u00020\f0\u0001J,\u0010ÿ\u0001\u001a\u0010\u0012\u0004\u0012\u00020\f\u0012\u0006\u0012\u0004\u0018\u00010)0-2\u0015\u0010\u0001\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010)0-J$\u0010\u0002\u001a\t\u0012\u0004\u0012\u00020\f0\u00012\u0014\u0010\u0001\u001a\u000f\u0012\u0004\u0012\u00020\u0004\u0012\u0005\u0012\u00030\u00020-J/\u0010\u0002\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010#\u001a\u0004\u0018\u00010\"2\b\u0010*\u001a\u0004\u0018\u00010)2\u0006\u0010$\u001a\u00020\u0006H\u0007J\u0013\u0010\u0002\u001a\u00020\f2\b\u0010\u0002\u001a\u00030\u0002H\u0007J0\u0010\u0002\u001a\u00030\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010#\u001a\u0004\u0018\u00010\"2\b\u0010*\u001a\u0004\u0018\u00010)2\u0006\u0010$\u001a\u00020\u0006H\u0007J\r\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\f0\u0011J\r\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\f0\u0011J\r\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\f0\u0011J\t\u0010\u0002\u001a\u00020\u0018H\u0007J\u0012\u0010\u0002\u001a\u0004\u0018\u00010R2\u0007\u0010\u0002\u001a\u00020\u0006J\u0011\u0010\u0002\u001a\u00030£\u00012\u0007\u0010\u0002\u001a\u00020\u0006J\u001f\u0010\u0002\u001a\u0004\u0018\u00010R2\u0007\u0010\u0002\u001a\u00020\u00062\u000b\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0004J\u001b\u0010\u0002\u001a\u0004\u0018\u00010R2\u0007\u0010\u0002\u001a\u00020\u00042\u0007\u0010\u0002\u001a\u00020\u0006J\t\u0010\u0002\u001a\u0004\u0018\u00010RJ\u0012\u0010\u0002\u001a\u0004\u0018\u00010R2\u0007\u0010\u0002\u001a\u00020\u0004J\u0012\u0010\u0002\u001a\u0004\u0018\u00010R2\u0007\u0010\u0002\u001a\u00020\u0006J\u001b\u0010\u0002\u001a\u0004\u0018\u00010R2\u0007\u0010\u0002\u001a\u00020\u00042\u0007\u0010\u0002\u001a\u00020\u0006J\u0012\u0010\u0002\u001a\u0004\u0018\u00010R2\u0007\u0010\u0002\u001a\u00020\u0004J,\u0010\u0002\u001a\t\u0012\u0005\u0012\u00030Ø\u00010\u00112\u0007\u0010\u0002\u001a\u00020\u00042\u0011\b\u0002\u0010\u0001\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u0011H\u0007J\u000e\u0010\u0002\u001a\t\u0012\u0005\u0012\u00030Ø\u00010\u0011J+\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\f0\u00112\b\u0010\u0002\u001a\u00030·\u00012\b\u0010\u0002\u001a\u00030·\u00012\b\u0010\u0002\u001a\u00030£\u0001J \u0010\u0002\u001a\u00020\u00182\r\u0010.\u001a\t\u0012\u0004\u0012\u00020\f0\u00012\b\u0010\u0002\u001a\u00030·\u0001J(\u0010 \u0002\u001a\u00020\u00182\u000e\u0010­\u0001\u001a\t\u0012\u0005\u0012\u00030\u00020\u00112\u000f\u0010\u0002\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\n0\u0011J\u0019\u0010¡\u0002\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\f2\b\u0010\u0015\u001a\u0004\u0018\u00010\nJ\u0011\u0010£\u0002\u001a\u00020\u00182\b\u0010¢\u0002\u001a\u00030·\u0001J\u0018\u0010¤\u0002\u001a\u00020\u00182\u000f\u0010\u0001\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\u0011J\u000f\u0010¥\u0002\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\fJ\u000f\u0010¦\u0002\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\fJ\u000f\u0010§\u0002\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\fJ\u001b\u0010¬\u0002\u001a\u00020\u00182\b\u0010©\u0002\u001a\u00030¨\u00022\b\u0010«\u0002\u001a\u00030ª\u0002J\u0013\u0010­\u0002\u001a\u00020\u00182\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\fJ\u0013\u0010®\u0002\u001a\u00020\u00182\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\fJ\u0019\u0010\u0001\u001a\u00020\u00122\b\u0010°\u0002\u001a\u00030¯\u00022\u0006\u0010S\u001a\u00020RJ\"\u0010\u0001\u001a\u00020\u00122\b\u0010°\u0002\u001a\u00030¯\u00022\u0006\u0010S\u001a\u00020R2\u0007\u0010±\u0002\u001a\u00020\u0004¨\u0006È\u0002"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "Lorg/whispersystems/signalservice/api/push/ServiceId;", "serviceId", "", CdsDatabase.E164, "", "changeSelf", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch;", "fetchRecipient", "", "storageKey", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getByStorageKeyOrThrow", "query", "", MultiselectForwardFragment.ARGS, "", "Lorg/thoughtcrime/securesms/database/model/RecipientRecord;", "getRecordForSync", "(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/database/RecipientDatabase$InsightsBannerTier;", "insightsBannerTier", "", "setInsightsBannerTier", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/Wallpaper;", RecipientDatabase.WALLPAPER, "setWallpaper", "getWallpaper", "Landroid/net/Uri;", "getWallpaperUri", "recipientId", "removePhoneNumber", "Lorg/whispersystems/signalservice/api/push/PNI;", RecipientDatabase.PNI_COLUMN, "pniVerified", "Lorg/thoughtcrime/securesms/database/PnpDataSet;", "data", "Lorg/thoughtcrime/securesms/database/PnpOperation;", "processPossibleE164PniSidMerge", "Lorg/whispersystems/signalservice/api/push/ACI;", "aci", "processPossiblePniSidAciSidMerge", "processPossibleE164AciSidMerge", "", "ids", "updateStorageIds", "j$/util/function/Function", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/RecipientExtras$Builder;", "updater", "updateExtras", "Landroid/content/ContentValues;", "contentValues", "update", "Lorg/signal/core/util/SqlUtil$Query;", "updateQuery", "column", DraftDatabase.DRAFT_VALUE, "j$/util/Optional", "getByColumn", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$GetOrInsertResult;", "getOrInsertByColumn", "byAci", "byE164", "merge", "ensureInTransaction", "buildContentValuesForNewUser", "buildContentValuesForPnpInsert", "Lorg/whispersystems/signalservice/api/storage/SignalContactRecord;", "contact", "isInsert", "getValuesForStorageContact", "Lorg/whispersystems/signalservice/api/storage/SignalGroupV1Record;", "groupV1", "getValuesForStorageGroupV1", "Lorg/whispersystems/signalservice/api/storage/SignalGroupV2Record;", "groupV2", "getValuesForStorageGroupV2", "serializedBadgeList", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "parseBadgeList", "Landroid/database/Cursor;", "cursor", "Lorg/thoughtcrime/securesms/database/model/RecipientRecord$SyncExtras;", "getSyncExtras", "Lorg/thoughtcrime/securesms/recipients/Recipient$Extras;", "getExtras", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/RecipientExtras;", "getRecipientExtras", "values", "record", "updateProfileValuesForMerge", "orderByPreferringAlphaOverNumeric", "T", "isAbsent", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientLogDetails;", "toLogDetails", "containsPhoneOrUuid", "getByE164", RecipientDatabase.EMAIL, "getByEmail", "Lorg/thoughtcrime/securesms/groups/GroupId;", "groupId", "getByGroupId", "getByServiceId", "getByPni", RecipientDatabase.USERNAME, "getByUsername", "getAndPossiblyMerge", "getAndPossiblyMergeLegacy", "getAndPossiblyMergePnp", "Lorg/signal/libsignal/zkgroup/profiles/ProfileKey;", "getAllServiceIdProfileKeyPairs", "getOrInsertFromServiceId", "getOrInsertFromE164", "getOrInsertFromEmail", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "distributionListId", "storageId", "getOrInsertFromDistributionListId", "getDistributionListRecipientIds", "getOrInsertFromGroupId", "getOrInsertFromPossiblyMigratedGroupId", "insertReleaseChannelRecipient", "getBlocked", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientReader;", "readerForBlocked", "getRecipientsWithNotificationChannels", "getRecord", "getByStorageId", "", "recipientIds", "markNeedsSyncWithoutRefresh", "markNeedsSync", "Lorg/whispersystems/signalservice/api/storage/StorageId;", "storageIds", "applyStorageIdUpdates", "insert", "applyStorageSyncContactInsert", "Lorg/thoughtcrime/securesms/storage/StorageRecordUpdate;", "applyStorageSyncContactUpdate", "applyStorageSyncGroupV1Insert", "applyStorageSyncGroupV1Update", "applyStorageSyncGroupV2Insert", "applyStorageSyncGroupV2Update", "Lorg/whispersystems/signalservice/api/storage/SignalAccountRecord;", "applyStorageSyncAccountUpdate", "mapping", "updatePhoneNumbers", "getContactStorageSyncIds", "getContactStorageSyncIdsMap", "", "getE164sForIds", "clearInfoForMissingContacts", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$BulkOperationsHandle;", "beginBulkSystemContactUpdate", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "chatColors", "onUpdatedChatColors", "onDeletedChatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "chatColorsId", "", "getColorUsageCount", "clearAllColors", "clearColor", "color", "setColor", "defaultSubscriptionId", "setDefaultSubscriptionId", "forceSmsSelection", "setForceSmsSelection", RecipientDatabase.BLOCKED, "setBlocked", "notification", "setMessageRingtone", "ringtone", "setCallRingtone", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "setMessageVibrate", "setCallVibrate", "", "until", "setMuted", "setSeenFirstInviteReminder", "setSeenSecondInviteReminder", "setHasSentInvite", "expiration", "setExpireMessages", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$UnidentifiedAccessMode;", "unidentifiedAccessMode", "setUnidentifiedAccessMode", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/DeviceLastResetTime;", "lastResetTime", "setLastSessionResetTime", "getLastSessionResetTimes", RecipientDatabase.BADGES, "setBadges", "Lorg/whispersystems/signalservice/api/profiles/SignalServiceProfile$Capabilities;", RecipientDatabase.CAPABILITIES, "setCapabilities", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;", "mentionSetting", "setMentionSetting", "profileKey", "setProfileKey", "setProfileKeyIfAbsent", "Lorg/signal/libsignal/zkgroup/profiles/ExpiringProfileKeyCredential;", "expiringProfileKeyCredential", "setProfileKeyCredential", "clearProfileKeyCredential", "Lorg/thoughtcrime/securesms/groups/v2/ProfileKeySet;", "profileKeySet", "persistProfileKeySet", "Lorg/thoughtcrime/securesms/recipients/Recipient;", RecipientDatabase.TABLE_NAME, "getSimilarRecipientIds", "systemContactName", "setSystemContactName", "Lorg/thoughtcrime/securesms/profiles/ProfileName;", "profileName", "setProfileName", "profileAvatar", "setProfileAvatar", RecipientDatabase.ABOUT, "emoji", "setAbout", "setProfileSharing", "notificationChannel", "setNotificationChannel", "resetAllWallpaper", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "chatWallpaper", "setDimWallpaperInDarkTheme", "uri", "getWallpaperUriUsageCount", "setPhoneNumber", "setPhoneNumberOrThrow", "setPhoneNumberOrThrowSilent", "updateSelfPhone", "setUsername", "hideStory", "setHideStory", "updateLastStoryViewTimestamp", "clearUsernameIfExists", "getAllE164s", "setPni", "markRegistered", "markRegisteredOrThrow", "markUnregistered", RecipientDatabase.REGISTERED, "unregistered", "bulkUpdatedRegisteredStatus", "bulkProcessCdsResult", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$CdsV2Result;", "bulkProcessCdsV2Result", "processPnpTuple", "Lorg/thoughtcrime/securesms/database/PnpChangeSet;", "changeSet", "writePnpChangeSetToDisk", "processPnpTupleToChangeSet", "getUninvitedRecipientsForInsights", "getRegistered", "getSystemContacts", "updateSystemContactColors", "includeSelf", "getSignalContacts", "getSignalContactsCount", "orderBy", "inputQuery", "querySignalContacts", "getNonSignalContacts", "queryNonSignalContacts", "getNonGroupContacts", "queryNonGroupContacts", "queryAllContacts", "queryRecipientsForMentions", "getRecipientsForMultiDeviceSync", "lastInteractionThreshold", "lastProfileFetchThreshold", "limit", "getRecipientsForRoutineProfileFetch", "time", "markProfilesFetched", "Lorg/whispersystems/signalservice/api/push/SignalServiceAddress;", "groupIds", "applyBlockedUpdate", "updateStorageId", "messageRequestEnableTime", "markPreMessageRequestRecipientsAsProfileSharingEnabled", "setHasGroupsInCommon", "manuallyShowAvatar", "rotateStorageId", "setStorageIdIfNotSet", "Lorg/thoughtcrime/securesms/groups/GroupId$V1;", "v1Id", "Lorg/thoughtcrime/securesms/groups/GroupId$V2;", "v2Id", "updateGroupId", "debugClearServiceIds", "debugClearProfileData", "Landroid/content/Context;", "context", "idColumnName", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "databaseHelper", "<init>", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "Companion", "BulkOperationsHandle", "Capabilities", "CdsV2Result", "ColorUpdater", "ContactSearchSelection", "GetOrInsertResult", "GroupType", "InsightsBannerTier", "LogBundle", "MentionSetting", "MissingRecipientException", "RecipientFetch", "RecipientLogDetails", "RecipientReader", "RegisteredState", "UnidentifiedAccessMode", "VibrateState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes.dex */
public class RecipientDatabase extends Database {
    public static final String ABOUT;
    public static final String ABOUT_EMOJI;
    private static final String AVATAR_COLOR;
    private static final String BADGES;
    public static final String BLOCKED;
    private static final String CALL_RINGTONE;
    private static final String CALL_VIBRATE;
    private static final String CAPABILITIES;
    private static final String CHAT_COLORS;
    private static final String[] CREATE_INDEXS = {"CREATE INDEX IF NOT EXISTS recipient_group_type_index ON recipient (group_type);", "CREATE UNIQUE INDEX IF NOT EXISTS recipient_pni_index ON recipient (pni)", "CREATE INDEX IF NOT EXISTS recipient_service_id_profile_key ON recipient (uuid, profile_key) WHERE uuid NOT NULL AND profile_key NOT NULL"};
    public static final String CREATE_TABLE;
    private static final String CUSTOM_CHAT_COLORS_ID;
    public static final Companion Companion = new Companion(null);
    private static final String DEFAULT_SUBSCRIPTION_ID;
    public static final String DISTRIBUTION_LIST_ID;
    public static final String EMAIL;
    public static final String EXPIRING_PROFILE_KEY_CREDENTIAL;
    private static final String EXTRAS;
    public static final String FORCE_SMS_SELECTION;
    private static final String GROUPS_IN_COMMON;
    public static final String GROUP_ID;
    public static final String GROUP_TYPE;
    public static final String ID;
    private static final String IDENTITY_KEY;
    private static final String IDENTITY_STATUS;
    private static final String[] ID_PROJECTION = {"_id"};
    private static final String INSIGHTS_INVITEE_LIST = ("\n      SELECT recipient._id\n      FROM recipient INNER JOIN thread ON recipient._id = thread.thread_recipient_id\n      WHERE \n        recipient.group_id IS NULL AND\n        recipient.registered = " + RegisteredState.NOT_REGISTERED.getId() + " AND\n        recipient.seen_invite_reminder < " + InsightsBannerTier.TIER_TWO.getId() + " AND\n        thread.has_sent AND\n        thread.date > ?\n      ORDER BY thread.date DESC LIMIT 50\n      ");
    private static final String LAST_PROFILE_FETCH;
    private static final String LAST_SESSION_RESET;
    private static final String[] MENTION_SEARCH_PROJECTION = {"_id", "REPLACE(\n  COALESCE(\n    NULLIF(system_display_name, ''), \n    NULLIF(system_given_name, ''), \n    NULLIF(profile_joined_name, ''), \n    NULLIF(signal_profile_name, ''),\n    NULLIF(username, ''),\n    NULLIF(phone, '')\n  ),\n  ' ',\n  ''\n) AS sort_name"};
    private static final String MENTION_SETTING;
    private static final String MESSAGE_EXPIRATION_TIME;
    private static final String MESSAGE_RINGTONE;
    private static final String MESSAGE_VIBRATE;
    private static final String MUTE_UNTIL;
    private static final String NOTIFICATION_CHANNEL;
    public static final String PHONE;
    public static final String PNI_COLUMN;
    private static final String PROFILE_FAMILY_NAME;
    private static final String PROFILE_GIVEN_NAME;
    private static final String PROFILE_JOINED_NAME;
    private static final String PROFILE_KEY;
    public static final String PROFILE_SHARING;
    private static final String[] RECIPIENT_PROJECTION;
    public static final String REGISTERED;
    public static final String SEARCH_PROFILE_NAME;
    private static final String[] SEARCH_PROJECTION = {"_id", SYSTEM_JOINED_NAME, PHONE, EMAIL, SYSTEM_PHONE_LABEL, SYSTEM_PHONE_TYPE, REGISTERED, ABOUT, ABOUT_EMOJI, EXTRAS, GROUPS_IN_COMMON, "COALESCE(NULLIF(profile_joined_name, ''), NULLIF(signal_profile_name, '')) AS search_signal_profile", "LOWER(\n  COALESCE(\n    NULLIF(system_display_name, ''),\n    NULLIF(system_given_name, ''),\n    NULLIF(profile_joined_name, ''),\n    NULLIF(signal_profile_name, ''),\n    NULLIF(username, '')\n  )\n) AS sort_name"};
    public static final String[] SEARCH_PROJECTION_NAMES = {"_id", SYSTEM_JOINED_NAME, PHONE, EMAIL, SYSTEM_PHONE_LABEL, SYSTEM_PHONE_TYPE, REGISTERED, ABOUT, ABOUT_EMOJI, EXTRAS, GROUPS_IN_COMMON, SEARCH_PROFILE_NAME, SORT_NAME};
    private static final String SEEN_INVITE_REMINDER;
    public static final String SERVICE_ID;
    private static final String SIGNAL_PROFILE_AVATAR;
    private static final String SORT_NAME;
    private static final String STORAGE_PROTO;
    public static final String STORAGE_SERVICE_ID;
    private static final String SYSTEM_CONTACT_URI;
    public static final String SYSTEM_FAMILY_NAME;
    public static final String SYSTEM_GIVEN_NAME;
    private static final String SYSTEM_INFO_PENDING;
    public static final String SYSTEM_JOINED_NAME;
    public static final String SYSTEM_PHONE_LABEL;
    public static final String SYSTEM_PHONE_TYPE;
    private static final String SYSTEM_PHOTO_URI;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(RecipientDatabase.class);
    private static final String[] TYPED_RECIPIENT_PROJECTION;
    public static final String[] TYPED_RECIPIENT_PROJECTION_NO_ID;
    private static final String UNIDENTIFIED_ACCESS_MODE;
    private static final String USERNAME;
    private static final String WALLPAPER;
    private static final String WALLPAPER_URI;

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H&¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$ColorUpdater;", "", "update", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "name", "", "materialColor", "Lorg/thoughtcrime/securesms/color/MaterialColor;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface ColorUpdater {
        ChatColors update(String str, MaterialColor materialColor);
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[GroupType.values().length];
            iArr[GroupType.NONE.ordinal()] = 1;
            iArr[GroupType.SIGNAL_V1.ordinal()] = 2;
            iArr[GroupType.DISTRIBUTION_LIST.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public final RecipientId getAndPossiblyMerge(ServiceId serviceId, String str) {
        return getAndPossiblyMerge$default(this, serviceId, str, false, 4, null);
    }

    public final RecipientId getOrInsertFromDistributionListId(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        return getOrInsertFromDistributionListId$default(this, distributionListId, null, 2, null);
    }

    public final List<Recipient> queryRecipientsForMentions(String str) {
        Intrinsics.checkNotNullParameter(str, "inputQuery");
        return queryRecipientsForMentions$default(this, str, null, 2, null);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RecipientDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u0011\n\u0002\b@\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0019\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00040\u000e¢\u0006\n\n\u0002\u0010\u0011\u001a\u0004\b\u000f\u0010\u0010R\u0010\u0010\u0012\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00040\u000eX\u0004¢\u0006\u0004\n\u0002\u0010\u0011R\u000e\u0010!\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00040\u000eX\u0004¢\u0006\u0004\n\u0002\u0010\u0011R\u000e\u0010%\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010'\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00101\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u00102\u001a\b\u0012\u0004\u0012\u00020\u00040\u000eX\u0004¢\u0006\u0004\n\u0002\u0010\u0011R\u000e\u00103\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00104\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u00105\u001a\b\u0012\u0004\u0012\u00020\u00040\u000eX\u0004¢\u0006\u0004\n\u0002\u0010\u0011R\u0018\u00106\u001a\b\u0012\u0004\u0012\u00020\u00040\u000e8\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0011R\u000e\u00107\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00108\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010:\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010;\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010>\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010?\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010C\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010D\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010E\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010F\u001a\n G*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010H\u001a\b\u0012\u0004\u0012\u00020\u00040\u000eX\u0004¢\u0006\u0004\n\u0002\u0010\u0011R\u0018\u0010I\u001a\b\u0012\u0004\u0012\u00020\u00040\u000e8\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0011R\u000e\u0010J\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010K\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010L\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010M\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006N"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$Companion;", "", "()V", "ABOUT", "", "ABOUT_EMOJI", "AVATAR_COLOR", "BADGES", "BLOCKED", "CALL_RINGTONE", "CALL_VIBRATE", "CAPABILITIES", "CHAT_COLORS", "CREATE_INDEXS", "", "getCREATE_INDEXS", "()[Ljava/lang/String;", "[Ljava/lang/String;", "CREATE_TABLE", "CUSTOM_CHAT_COLORS_ID", "DEFAULT_SUBSCRIPTION_ID", "DISTRIBUTION_LIST_ID", "EMAIL", "EXPIRING_PROFILE_KEY_CREDENTIAL", "EXTRAS", "FORCE_SMS_SELECTION", "GROUPS_IN_COMMON", "GROUP_ID", "GROUP_TYPE", "ID", "IDENTITY_KEY", "IDENTITY_STATUS", "ID_PROJECTION", "INSIGHTS_INVITEE_LIST", "LAST_PROFILE_FETCH", "LAST_SESSION_RESET", "MENTION_SEARCH_PROJECTION", "MENTION_SETTING", "MESSAGE_EXPIRATION_TIME", "MESSAGE_RINGTONE", "MESSAGE_VIBRATE", "MUTE_UNTIL", "NOTIFICATION_CHANNEL", "PHONE", "PNI_COLUMN", "PROFILE_FAMILY_NAME", "PROFILE_GIVEN_NAME", "PROFILE_JOINED_NAME", "PROFILE_KEY", "PROFILE_SHARING", "RECIPIENT_PROJECTION", "REGISTERED", "SEARCH_PROFILE_NAME", "SEARCH_PROJECTION", "SEARCH_PROJECTION_NAMES", "SEEN_INVITE_REMINDER", "SERVICE_ID", "SIGNAL_PROFILE_AVATAR", "SORT_NAME", "STORAGE_PROTO", "STORAGE_SERVICE_ID", "SYSTEM_CONTACT_URI", "SYSTEM_FAMILY_NAME", "SYSTEM_GIVEN_NAME", "SYSTEM_INFO_PENDING", "SYSTEM_JOINED_NAME", "SYSTEM_PHONE_LABEL", "SYSTEM_PHONE_TYPE", "SYSTEM_PHOTO_URI", "TABLE_NAME", "TAG", "kotlin.jvm.PlatformType", "TYPED_RECIPIENT_PROJECTION", "TYPED_RECIPIENT_PROJECTION_NO_ID", "UNIDENTIFIED_ACCESS_MODE", "USERNAME", "WALLPAPER", "WALLPAPER_URI", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final String[] getCREATE_INDEXS() {
            return RecipientDatabase.CREATE_INDEXS;
        }
    }

    static {
        Companion = new Companion(null);
        TAG = Log.tag(RecipientDatabase.class);
        StringBuilder sb = new StringBuilder();
        sb.append("\n      CREATE TABLE recipient (\n        _id INTEGER PRIMARY KEY AUTOINCREMENT,\n        uuid TEXT UNIQUE DEFAULT NULL,\n        username TEXT UNIQUE DEFAULT NULL,\n        phone TEXT UNIQUE DEFAULT NULL,\n        email TEXT UNIQUE DEFAULT NULL,\n        group_id TEXT UNIQUE DEFAULT NULL,\n        group_type INTEGER DEFAULT ");
        sb.append(GroupType.NONE.getId());
        sb.append(",\n        blocked INTEGER DEFAULT 0,\n        message_ringtone TEXT DEFAULT NULL, \n        message_vibrate INTEGER DEFAULT ");
        VibrateState vibrateState = VibrateState.DEFAULT;
        sb.append(vibrateState.getId());
        sb.append(", \n        call_ringtone TEXT DEFAULT NULL, \n        call_vibrate INTEGER DEFAULT ");
        sb.append(vibrateState.getId());
        sb.append(", \n        notification_channel TEXT DEFAULT NULL, \n        mute_until INTEGER DEFAULT 0, \n        color TEXT DEFAULT NULL, \n        seen_invite_reminder INTEGER DEFAULT ");
        sb.append(InsightsBannerTier.NO_TIER.getId());
        sb.append(",\n        default_subscription_id INTEGER DEFAULT -1,\n        message_expiration_time INTEGER DEFAULT 0,\n        registered INTEGER DEFAULT ");
        sb.append(RegisteredState.UNKNOWN.getId());
        sb.append(",\n        system_given_name TEXT DEFAULT NULL, \n        system_family_name TEXT DEFAULT NULL, \n        system_display_name TEXT DEFAULT NULL, \n        system_photo_uri TEXT DEFAULT NULL, \n        system_phone_label TEXT DEFAULT NULL, \n        system_phone_type INTEGER DEFAULT -1, \n        system_contact_uri TEXT DEFAULT NULL, \n        system_info_pending INTEGER DEFAULT 0, \n        profile_key TEXT DEFAULT NULL, \n        profile_key_credential TEXT DEFAULT NULL, \n        signal_profile_name TEXT DEFAULT NULL, \n        profile_family_name TEXT DEFAULT NULL, \n        profile_joined_name TEXT DEFAULT NULL, \n        signal_profile_avatar TEXT DEFAULT NULL, \n        profile_sharing INTEGER DEFAULT 0, \n        last_profile_fetch INTEGER DEFAULT 0, \n        unidentified_access_mode INTEGER DEFAULT 0, \n        force_sms_selection INTEGER DEFAULT 0, \n        storage_service_key TEXT UNIQUE DEFAULT NULL, \n        mention_setting INTEGER DEFAULT ");
        sb.append(MentionSetting.ALWAYS_NOTIFY.getId());
        sb.append(", \n        storage_proto TEXT DEFAULT NULL,\n        capabilities INTEGER DEFAULT 0,\n        last_session_reset BLOB DEFAULT NULL,\n        wallpaper BLOB DEFAULT NULL,\n        wallpaper_file TEXT DEFAULT NULL,\n        about TEXT DEFAULT NULL,\n        about_emoji TEXT DEFAULT NULL,\n        extras BLOB DEFAULT NULL,\n        groups_in_common INTEGER DEFAULT 0,\n        chat_colors BLOB DEFAULT NULL,\n        custom_chat_colors_id INTEGER DEFAULT 0,\n        badges BLOB DEFAULT NULL,\n        pni TEXT DEFAULT NULL,\n        distribution_list_id INTEGER DEFAULT NULL\n      )\n      ");
        CREATE_TABLE = StringsKt__IndentKt.trimIndent(sb.toString());
        CREATE_INDEXS = new String[]{"CREATE INDEX IF NOT EXISTS recipient_group_type_index ON recipient (group_type);", "CREATE UNIQUE INDEX IF NOT EXISTS recipient_pni_index ON recipient (pni)", "CREATE INDEX IF NOT EXISTS recipient_service_id_profile_key ON recipient (uuid, profile_key) WHERE uuid NOT NULL AND profile_key NOT NULL"};
        String[] strArr = {"_id", SERVICE_ID, PNI_COLUMN, USERNAME, PHONE, EMAIL, "group_id", GROUP_TYPE, BLOCKED, MESSAGE_RINGTONE, CALL_RINGTONE, MESSAGE_VIBRATE, CALL_VIBRATE, MUTE_UNTIL, "color", SEEN_INVITE_REMINDER, DEFAULT_SUBSCRIPTION_ID, MESSAGE_EXPIRATION_TIME, REGISTERED, PROFILE_KEY, EXPIRING_PROFILE_KEY_CREDENTIAL, SYSTEM_JOINED_NAME, SYSTEM_GIVEN_NAME, SYSTEM_FAMILY_NAME, SYSTEM_PHOTO_URI, SYSTEM_PHONE_LABEL, SYSTEM_PHONE_TYPE, SYSTEM_CONTACT_URI, PROFILE_GIVEN_NAME, PROFILE_FAMILY_NAME, SIGNAL_PROFILE_AVATAR, PROFILE_SHARING, LAST_PROFILE_FETCH, NOTIFICATION_CHANNEL, UNIDENTIFIED_ACCESS_MODE, FORCE_SMS_SELECTION, CAPABILITIES, STORAGE_SERVICE_ID, MENTION_SETTING, WALLPAPER, WALLPAPER_URI, MENTION_SETTING, ABOUT, ABOUT_EMOJI, EXTRAS, GROUPS_IN_COMMON, CHAT_COLORS, CUSTOM_CHAT_COLORS_ID, BADGES, DISTRIBUTION_LIST_ID};
        RECIPIENT_PROJECTION = strArr;
        ID_PROJECTION = new String[]{"_id"};
        SEARCH_PROJECTION = new String[]{"_id", SYSTEM_JOINED_NAME, PHONE, EMAIL, SYSTEM_PHONE_LABEL, SYSTEM_PHONE_TYPE, REGISTERED, ABOUT, ABOUT_EMOJI, EXTRAS, GROUPS_IN_COMMON, "COALESCE(NULLIF(profile_joined_name, ''), NULLIF(signal_profile_name, '')) AS search_signal_profile", "LOWER(\n  COALESCE(\n    NULLIF(system_display_name, ''),\n    NULLIF(system_given_name, ''),\n    NULLIF(profile_joined_name, ''),\n    NULLIF(signal_profile_name, ''),\n    NULLIF(username, '')\n  )\n) AS sort_name"};
        SEARCH_PROJECTION_NAMES = new String[]{"_id", SYSTEM_JOINED_NAME, PHONE, EMAIL, SYSTEM_PHONE_LABEL, SYSTEM_PHONE_TYPE, REGISTERED, ABOUT, ABOUT_EMOJI, EXTRAS, GROUPS_IN_COMMON, SEARCH_PROFILE_NAME, SORT_NAME};
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String str : strArr) {
            arrayList.add("recipient." + str);
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            String[] strArr2 = (String[]) array;
            TYPED_RECIPIENT_PROJECTION = strArr2;
            TYPED_RECIPIENT_PROJECTION_NO_ID = (String[]) ArraysKt___ArraysJvmKt.copyOfRange(strArr2, 1, strArr2.length);
            MENTION_SEARCH_PROJECTION = new String[]{"_id", "REPLACE(\n  COALESCE(\n    NULLIF(system_display_name, ''), \n    NULLIF(system_given_name, ''), \n    NULLIF(profile_joined_name, ''), \n    NULLIF(signal_profile_name, ''),\n    NULLIF(username, ''),\n    NULLIF(phone, '')\n  ),\n  ' ',\n  ''\n) AS sort_name"};
            INSIGHTS_INVITEE_LIST = "\n      SELECT recipient._id\n      FROM recipient INNER JOIN thread ON recipient._id = thread.thread_recipient_id\n      WHERE \n        recipient.group_id IS NULL AND\n        recipient.registered = " + RegisteredState.NOT_REGISTERED.getId() + " AND\n        recipient.seen_invite_reminder < " + InsightsBannerTier.TIER_TWO.getId() + " AND\n        thread.has_sent AND\n        thread.date > ?\n      ORDER BY thread.date DESC LIMIT 50\n      ";
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }

    public final boolean containsPhoneOrUuid(String str) {
        Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
        boolean z = false;
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"_id"}, "uuid = ? OR phone = ?", new String[]{str, str}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    z = true;
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        th = null;
        return z;
    }

    public final Optional<RecipientId> getByE164(String str) {
        Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
        return getByColumn(PHONE, str);
    }

    public final Optional<RecipientId> getByEmail(String str) {
        Intrinsics.checkNotNullParameter(str, EMAIL);
        return getByColumn(EMAIL, str);
    }

    public final Optional<RecipientId> getByGroupId(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        String groupId2 = groupId.toString();
        Intrinsics.checkNotNullExpressionValue(groupId2, "groupId.toString()");
        return getByColumn("group_id", groupId2);
    }

    public final Optional<RecipientId> getByServiceId(ServiceId serviceId) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        String serviceId2 = serviceId.toString();
        Intrinsics.checkNotNullExpressionValue(serviceId2, "serviceId.toString()");
        return getByColumn(SERVICE_ID, serviceId2);
    }

    public final Optional<RecipientId> getByPni(PNI pni) {
        Intrinsics.checkNotNullParameter(pni, PNI_COLUMN);
        String serviceId = pni.toString();
        Intrinsics.checkNotNullExpressionValue(serviceId, "pni.toString()");
        return getByColumn(PNI_COLUMN, serviceId);
    }

    public final Optional<RecipientId> getByUsername(String str) {
        Intrinsics.checkNotNullParameter(str, USERNAME);
        return getByColumn(USERNAME, str);
    }

    public static /* synthetic */ RecipientId getAndPossiblyMerge$default(RecipientDatabase recipientDatabase, ServiceId serviceId, String str, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 4) != 0) {
                z = false;
            }
            return recipientDatabase.getAndPossiblyMerge(serviceId, str, z);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getAndPossiblyMerge");
    }

    public final RecipientId getAndPossiblyMerge(ServiceId serviceId, String str, boolean z) {
        if (FeatureFlags.phoneNumberPrivacy()) {
            return getAndPossiblyMergePnp(serviceId, str, z);
        }
        return getAndPossiblyMergeLegacy(serviceId, str, z);
    }

    public static /* synthetic */ RecipientId getAndPossiblyMergeLegacy$default(RecipientDatabase recipientDatabase, ServiceId serviceId, String str, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 4) != 0) {
                z = false;
            }
            return recipientDatabase.getAndPossiblyMergeLegacy(serviceId, str, z);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getAndPossiblyMergeLegacy");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:91:? */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:20:0x0067 */
    /* JADX DEBUG: Multi-variable search result rejected for r9v3, resolved type: org.thoughtcrime.securesms.recipients.RecipientId */
    /* JADX DEBUG: Multi-variable search result rejected for r9v4, resolved type: org.thoughtcrime.securesms.recipients.RecipientId */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r9v7, types: [org.thoughtcrime.securesms.recipients.RecipientId] */
    /* JADX WARN: Type inference failed for: r9v8 */
    /* JADX WARN: Type inference failed for: r9v9, types: [boolean] */
    /* JADX WARN: Type inference failed for: r9v30 */
    /* JADX WARN: Type inference failed for: r9v58 */
    /* JADX WARN: Type inference failed for: r9v60 */
    /* JADX WARN: Type inference failed for: r9v61 */
    /* JADX WARN: Type inference failed for: r9v62 */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01f8  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0215  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.thoughtcrime.securesms.recipients.RecipientId getAndPossiblyMergeLegacy(org.whispersystems.signalservice.api.push.ServiceId r8, java.lang.String r9, boolean r10) {
        /*
        // Method dump skipped, instructions count: 655
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.RecipientDatabase.getAndPossiblyMergeLegacy(org.whispersystems.signalservice.api.push.ServiceId, java.lang.String, boolean):org.thoughtcrime.securesms.recipients.RecipientId");
    }

    public static /* synthetic */ RecipientId getAndPossiblyMergePnp$default(RecipientDatabase recipientDatabase, ServiceId serviceId, String str, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 4) != 0) {
                z = false;
            }
            return recipientDatabase.getAndPossiblyMergePnp(serviceId, str, z);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getAndPossiblyMergePnp");
    }

    public final RecipientId getAndPossiblyMergePnp(ServiceId serviceId, String str, boolean z) {
        if ((serviceId == null && str == null) ? false : true) {
            return (RecipientId) SQLiteDatabaseExtensionsKt.withinTransaction(getWritableDatabase(), new Function1<SQLiteDatabase, RecipientId>(serviceId, this, str) { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$getAndPossiblyMergePnp$2
                final /* synthetic */ String $e164;
                final /* synthetic */ ServiceId $serviceId;
                final /* synthetic */ RecipientDatabase this$0;

                /* access modifiers changed from: package-private */
                {
                    this.$serviceId = r1;
                    this.this$0 = r2;
                    this.$e164 = r3;
                }

                public final RecipientId invoke(SQLiteDatabase sQLiteDatabase) {
                    ServiceId serviceId2 = this.$serviceId;
                    if (serviceId2 instanceof PNI) {
                        return this.this$0.processPnpTuple(this.$e164, (PNI) serviceId2, null, false);
                    }
                    if (serviceId2 instanceof ACI) {
                        return this.this$0.processPnpTuple(this.$e164, null, (ACI) serviceId2, false);
                    }
                    if (serviceId2 == null) {
                        return this.this$0.processPnpTuple(this.$e164, null, null, false);
                    }
                    RecipientDatabase recipientDatabase = this.this$0;
                    PNI from = PNI.from(serviceId2.uuid());
                    Intrinsics.checkNotNullExpressionValue(from, "from(serviceId.uuid())");
                    if (recipientDatabase.getByPni(from).isPresent()) {
                        return this.this$0.processPnpTuple(this.$e164, PNI.from(this.$serviceId.uuid()), null, false);
                    }
                    return this.this$0.processPnpTuple(this.$e164, null, ACI.fromNullable(this.$serviceId), false);
                }
            });
        }
        throw new IllegalArgumentException("Must provide an ACI or E164!".toString());
    }

    public final Map<ServiceId, ProfileKey> getAllServiceIdProfileKeyPairs() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Intrinsics.checkNotNullExpressionValue(readableDatabase, "readableDatabase");
        Cursor run = SQLiteDatabaseExtensionsKt.select(readableDatabase, SERVICE_ID, PROFILE_KEY).from(TABLE_NAME).where("uuid NOT NULL AND profile_key NOT NULL", new Object[0]).run();
        while (run.moveToNext()) {
            try {
                ServiceId parseOrNull = ServiceId.parseOrNull(CursorExtensionsKt.requireString(run, SERVICE_ID));
                ProfileKey profileKeyOrNull = ProfileKeyUtil.profileKeyOrNull(CursorExtensionsKt.requireString(run, PROFILE_KEY));
                if (!(parseOrNull == null || profileKeyOrNull == null)) {
                    linkedHashMap.put(parseOrNull, profileKeyOrNull);
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return linkedHashMap;
    }

    private final RecipientFetch fetchRecipient(ServiceId serviceId, String str, boolean z) {
        Optional<RecipientId> optional;
        Optional<RecipientId> optional2;
        if (str == null || (optional = getByE164(str)) == null) {
            optional = Optional.empty();
            Intrinsics.checkNotNullExpressionValue(optional, "empty()");
        }
        if (serviceId == null || (optional2 = getByServiceId(serviceId)) == null) {
            optional2 = Optional.empty();
            Intrinsics.checkNotNullExpressionValue(optional2, "empty()");
        }
        RecipientId recipientId = null;
        LogBundle logBundle = new LogBundle("L0", null, null, (RecipientLogDetails) optional2.map(new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1694fetchRecipient$lambda7((RecipientId) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null), (RecipientLogDetails) optional.map(new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1695fetchRecipient$lambda8((RecipientId) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null), 6, null);
        if (optional2.isPresent() && optional.isPresent() && Intrinsics.areEqual(optional2.get(), optional.get())) {
            RecipientId recipientId2 = optional2.get();
            Intrinsics.checkNotNullExpressionValue(recipientId2, "byAci.get()");
            return new RecipientFetch.Match(recipientId2, logBundle.label("L0"));
        } else if (optional2.isPresent() && isAbsent(optional)) {
            RecipientId recipientId3 = optional2.get();
            Intrinsics.checkNotNullExpressionValue(recipientId3, "byAci.get()");
            RecipientRecord record = getRecord(recipientId3);
            LogBundle copy$default = LogBundle.copy$default(logBundle, null, null, null, toLogDetails(record), null, 23, null);
            if (str != null && (z || !Intrinsics.areEqual(serviceId, SignalStore.account().getAci()))) {
                if (record.getE164() != null && !Intrinsics.areEqual(record.getE164(), str)) {
                    recipientId = record.getId();
                }
                RecipientId recipientId4 = optional2.get();
                Intrinsics.checkNotNullExpressionValue(recipientId4, "byAci.get()");
                return new RecipientFetch.MatchAndUpdateE164(recipientId4, str, recipientId, copy$default.label("L1"));
            } else if (str == null) {
                RecipientId recipientId5 = optional2.get();
                Intrinsics.checkNotNullExpressionValue(recipientId5, "byAci.get()");
                return new RecipientFetch.Match(recipientId5, copy$default.label("L2"));
            } else {
                RecipientId recipientId6 = optional2.get();
                Intrinsics.checkNotNullExpressionValue(recipientId6, "byAci.get()");
                return new RecipientFetch.Match(recipientId6, copy$default.label("L3"));
            }
        } else if (isAbsent(optional2) && optional.isPresent()) {
            RecipientId recipientId7 = optional.get();
            Intrinsics.checkNotNullExpressionValue(recipientId7, "byE164.get()");
            RecipientRecord record2 = getRecord(recipientId7);
            LogBundle copy$default2 = LogBundle.copy$default(logBundle, null, null, null, null, toLogDetails(record2), 15, null);
            if (serviceId != null && record2.getServiceId() == null) {
                RecipientId recipientId8 = optional.get();
                Intrinsics.checkNotNullExpressionValue(recipientId8, "byE164.get()");
                return new RecipientFetch.MatchAndUpdateAci(recipientId8, serviceId, copy$default2.label("L4"));
            } else if (serviceId != null && !Intrinsics.areEqual(record2.getServiceId(), SignalStore.account().getAci())) {
                RecipientId recipientId9 = optional.get();
                Intrinsics.checkNotNullExpressionValue(recipientId9, "byE164.get()");
                return new RecipientFetch.InsertAndReassignE164(serviceId, str, recipientId9, copy$default2.label("L5"));
            } else if (serviceId != null) {
                return new RecipientFetch.Insert(serviceId, null, copy$default2.label("L6"));
            } else {
                RecipientId recipientId10 = optional.get();
                Intrinsics.checkNotNullExpressionValue(recipientId10, "byE164.get()");
                return new RecipientFetch.Match(recipientId10, copy$default2.label("L7"));
            }
        } else if (isAbsent(optional2) && isAbsent(optional)) {
            return new RecipientFetch.Insert(serviceId, str, logBundle.label("L8"));
        } else {
            if (optional2.isPresent() && optional.isPresent() && !Intrinsics.areEqual(optional2.get(), optional.get())) {
                RecipientId recipientId11 = optional2.get();
                Intrinsics.checkNotNullExpressionValue(recipientId11, "byAci.get()");
                RecipientRecord record3 = getRecord(recipientId11);
                RecipientId recipientId12 = optional.get();
                Intrinsics.checkNotNullExpressionValue(recipientId12, "byE164.get()");
                RecipientRecord record4 = getRecord(recipientId12);
                LogBundle copy$default3 = LogBundle.copy$default(logBundle, null, null, null, toLogDetails(record3), toLogDetails(record4), 7, null);
                if (record4.getServiceId() == null) {
                    if (record3.getE164() != null) {
                        recipientId = record3.getId();
                    }
                    RecipientId recipientId13 = optional2.get();
                    Intrinsics.checkNotNullExpressionValue(recipientId13, "byAci.get()");
                    RecipientId recipientId14 = optional.get();
                    Intrinsics.checkNotNullExpressionValue(recipientId14, "byE164.get()");
                    return new RecipientFetch.MatchAndMerge(recipientId13, recipientId14, recipientId, copy$default3.label("L9"));
                } else if (!Intrinsics.areEqual(record4.getServiceId(), SignalStore.account().getAci())) {
                    if (record3.getE164() != null) {
                        recipientId = record3.getId();
                    }
                    RecipientId recipientId15 = optional2.get();
                    Intrinsics.checkNotNullExpressionValue(recipientId15, "byAci.get()");
                    RecipientId recipientId16 = recipientId15;
                    RecipientId recipientId17 = optional.get();
                    Intrinsics.checkNotNullExpressionValue(recipientId17, "byE164.get()");
                    Intrinsics.checkNotNull(str);
                    return new RecipientFetch.MatchAndReassignE164(recipientId16, recipientId17, str, recipientId, copy$default3.label("L10"));
                } else {
                    RecipientId recipientId18 = optional2.get();
                    Intrinsics.checkNotNullExpressionValue(recipientId18, "byAci.get()");
                    return new RecipientFetch.Match(recipientId18, copy$default3.label("L11"));
                }
            } else {
                throw new IllegalArgumentException("Assumed conditions at this point.".toString());
            }
        }
    }

    /* renamed from: fetchRecipient$lambda-7 */
    public static final RecipientLogDetails m1694fetchRecipient$lambda7(RecipientId recipientId) {
        Intrinsics.checkNotNullExpressionValue(recipientId, ContactRepository.ID_COLUMN);
        return new RecipientLogDetails(recipientId, null, null, 6, null);
    }

    /* renamed from: fetchRecipient$lambda-8 */
    public static final RecipientLogDetails m1695fetchRecipient$lambda8(RecipientId recipientId) {
        Intrinsics.checkNotNullExpressionValue(recipientId, ContactRepository.ID_COLUMN);
        return new RecipientLogDetails(recipientId, null, null, 6, null);
    }

    public final RecipientId getOrInsertFromServiceId(ServiceId serviceId) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        String serviceId2 = serviceId.toString();
        Intrinsics.checkNotNullExpressionValue(serviceId2, "serviceId.toString()");
        return getOrInsertByColumn$default(this, SERVICE_ID, serviceId2, null, 4, null).getRecipientId();
    }

    public final RecipientId getOrInsertFromE164(String str) {
        Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
        return getOrInsertByColumn$default(this, PHONE, str, null, 4, null).getRecipientId();
    }

    public final RecipientId getOrInsertFromEmail(String str) {
        Intrinsics.checkNotNullParameter(str, EMAIL);
        return getOrInsertByColumn$default(this, EMAIL, str, null, 4, null).getRecipientId();
    }

    public static /* synthetic */ RecipientId getOrInsertFromDistributionListId$default(RecipientDatabase recipientDatabase, DistributionListId distributionListId, byte[] bArr, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                bArr = null;
            }
            return recipientDatabase.getOrInsertFromDistributionListId(distributionListId, bArr);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getOrInsertFromDistributionListId");
    }

    public final RecipientId getOrInsertFromDistributionListId(DistributionListId distributionListId, byte[] bArr) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        String serialize = distributionListId.serialize();
        Intrinsics.checkNotNullExpressionValue(serialize, "distributionListId.serialize()");
        ContentValues contentValues = new ContentValues();
        contentValues.put(GROUP_TYPE, Integer.valueOf(GroupType.DISTRIBUTION_LIST.getId()));
        contentValues.put(DISTRIBUTION_LIST_ID, distributionListId.serialize());
        if (bArr == null) {
            bArr = StorageSyncHelper.generateKey();
            Intrinsics.checkNotNullExpressionValue(bArr, "generateKey()");
        }
        contentValues.put(STORAGE_SERVICE_ID, Base64.encodeBytes(bArr));
        contentValues.put(PROFILE_SHARING, (Integer) 1);
        Unit unit = Unit.INSTANCE;
        return getOrInsertByColumn(DISTRIBUTION_LIST_ID, serialize, contentValues).getRecipientId();
    }

    public final List<RecipientId> getDistributionListRecipientIds() {
        ArrayList arrayList = new ArrayList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"_id"}, "distribution_list_id is not NULL", null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                RecipientId from = RecipientId.from(CursorUtil.requireLong(query, "_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(CursorUtil.requireLong(cursor, ID))");
                arrayList.add(from);
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return arrayList;
    }

    public final RecipientId getOrInsertFromGroupId(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Optional<RecipientId> byGroupId = getByGroupId(groupId);
        if (byGroupId.isPresent()) {
            RecipientId recipientId = byGroupId.get();
            Intrinsics.checkNotNullExpressionValue(recipientId, "existing.get()");
            return recipientId;
        } else if (groupId.isV1() && SignalDatabase.Companion.groups().groupExists(groupId.requireV1().deriveV2MigrationGroupId())) {
            throw new GroupDatabase.LegacyGroupInsertException(groupId);
        } else if (!groupId.isV2() || !SignalDatabase.Companion.groups().getGroupV1ByExpectedV2(groupId.requireV2()).isPresent()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("group_id", groupId.toString());
            contentValues.put("color", AvatarColor.random().serialize());
            long insert = getWritableDatabase().insert(TABLE_NAME, (String) null, contentValues);
            if (insert < 0) {
                String groupId2 = groupId.toString();
                Intrinsics.checkNotNullExpressionValue(groupId2, "groupId.toString()");
                Optional<RecipientId> byColumn = getByColumn("group_id", groupId2);
                if (byColumn.isPresent()) {
                    RecipientId recipientId2 = byColumn.get();
                    Intrinsics.checkNotNullExpressionValue(recipientId2, "existing.get()");
                    return recipientId2;
                } else if (groupId.isV1() && SignalDatabase.Companion.groups().groupExists(groupId.requireV1().deriveV2MigrationGroupId())) {
                    throw new GroupDatabase.LegacyGroupInsertException(groupId);
                } else if (!groupId.isV2() || !SignalDatabase.Companion.groups().getGroupV1ByExpectedV2(groupId.requireV2()).isPresent()) {
                    throw new AssertionError("Failed to insert recipient!");
                } else {
                    throw new GroupDatabase.MissedGroupMigrationInsertException(groupId);
                }
            } else {
                ContentValues contentValues2 = new ContentValues();
                if (groupId.isMms()) {
                    contentValues2.put(GROUP_TYPE, Integer.valueOf(GroupType.MMS.getId()));
                } else {
                    if (groupId.isV2()) {
                        contentValues2.put(GROUP_TYPE, Integer.valueOf(GroupType.SIGNAL_V2.getId()));
                    } else {
                        contentValues2.put(GROUP_TYPE, Integer.valueOf(GroupType.SIGNAL_V1.getId()));
                    }
                    contentValues2.put(STORAGE_SERVICE_ID, Base64.encodeBytes(StorageSyncHelper.generateKey()));
                }
                RecipientId from = RecipientId.from(insert);
                Intrinsics.checkNotNullExpressionValue(from, "recipientId");
                update(from, contentValues2);
                return from;
            }
        } else {
            throw new GroupDatabase.MissedGroupMigrationInsertException(groupId);
        }
    }

    public final RecipientId getOrInsertFromPossiblyMigratedGroupId(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            String groupId2 = groupId.toString();
            Intrinsics.checkNotNullExpressionValue(groupId2, "groupId.toString()");
            Optional<RecipientId> byColumn = getByColumn("group_id", groupId2);
            if (byColumn.isPresent()) {
                writableDatabase.setTransactionSuccessful();
                RecipientId recipientId = byColumn.get();
                Intrinsics.checkNotNullExpressionValue(recipientId, "existing.get()");
                return recipientId;
            }
            if (groupId.isV1()) {
                GroupId.V2 deriveV2MigrationGroupId = groupId.requireV1().deriveV2MigrationGroupId();
                Intrinsics.checkNotNullExpressionValue(deriveV2MigrationGroupId, "groupId.requireV1().deriveV2MigrationGroupId()");
                Optional<RecipientId> byGroupId = getByGroupId(deriveV2MigrationGroupId);
                if (byGroupId.isPresent()) {
                    writableDatabase.setTransactionSuccessful();
                    RecipientId recipientId2 = byGroupId.get();
                    Intrinsics.checkNotNullExpressionValue(recipientId2, "v2.get()");
                    return recipientId2;
                }
            }
            if (groupId.isV2()) {
                Optional<GroupDatabase.GroupRecord> groupV1ByExpectedV2 = SignalDatabase.Companion.groups().getGroupV1ByExpectedV2(groupId.requireV2());
                if (groupV1ByExpectedV2.isPresent()) {
                    writableDatabase.setTransactionSuccessful();
                    RecipientId recipientId3 = groupV1ByExpectedV2.get().getRecipientId();
                    Intrinsics.checkNotNullExpressionValue(recipientId3, "v1.get().recipientId");
                    return recipientId3;
                }
            }
            RecipientId orInsertFromGroupId = getOrInsertFromGroupId(groupId);
            writableDatabase.setTransactionSuccessful();
            return orInsertFromGroupId;
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public final RecipientId insertReleaseChannelRecipient() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("color", AvatarColor.random().serialize());
        long insert = getWritableDatabase().insert(TABLE_NAME, (String) null, contentValues);
        if (insert >= 0) {
            RecipientId from = RecipientId.from(insert);
            Intrinsics.checkNotNullExpressionValue(from, "from(id)");
            return new GetOrInsertResult(from, true).getRecipientId();
        }
        throw new AssertionError("Failed to insert recipient!");
    }

    /* JADX INFO: finally extract failed */
    public final void applyBlockedUpdate(List<? extends SignalServiceAddress> list, List<byte[]> list2) {
        Intrinsics.checkNotNullParameter(list, BLOCKED);
        Intrinsics.checkNotNullParameter(list2, "groupIds");
        ArrayList<SignalServiceAddress> arrayList = new ArrayList();
        for (Object obj : list) {
            if (((SignalServiceAddress) obj).getNumber().isPresent()) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        for (SignalServiceAddress signalServiceAddress : arrayList) {
            arrayList2.add(signalServiceAddress.getNumber().get());
        }
        List list3 = CollectionsKt___CollectionsKt.toList(arrayList2);
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (SignalServiceAddress signalServiceAddress2 : list) {
            String serviceId = signalServiceAddress2.getServiceId().toString();
            Intrinsics.checkNotNullExpressionValue(serviceId, "b.serviceId.toString()");
            String lowerCase = serviceId.toLowerCase(Locale.ROOT);
            Intrinsics.checkNotNullExpressionValue(lowerCase, "this as java.lang.String).toLowerCase(Locale.ROOT)");
            arrayList3.add(lowerCase);
        }
        List list4 = CollectionsKt___CollectionsKt.toList(arrayList3);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(BLOCKED, (Integer) 0);
            writableDatabase.update(TABLE_NAME, contentValues, null, null);
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put(BLOCKED, (Integer) 1);
            contentValues2.put(PROFILE_SHARING, (Integer) 0);
            Iterator it = list3.iterator();
            while (it.hasNext()) {
                writableDatabase.update(TABLE_NAME, contentValues2, "phone = ?", new String[]{(String) it.next()});
            }
            Iterator it2 = list4.iterator();
            while (it2.hasNext()) {
                writableDatabase.update(TABLE_NAME, contentValues2, "uuid = ?", new String[]{(String) it2.next()});
            }
            ArrayList arrayList4 = new ArrayList(list2.size());
            for (byte[] bArr : list2) {
                try {
                    GroupId.V1 v1 = GroupId.v1(bArr);
                    Intrinsics.checkNotNullExpressionValue(v1, "v1(raw)");
                    arrayList4.add(v1);
                } catch (BadGroupIdException unused) {
                    Log.w(TAG, "[applyBlockedUpdate] Bad GV1 ID!");
                }
            }
            Iterator it3 = arrayList4.iterator();
            while (it3.hasNext()) {
                writableDatabase.update(TABLE_NAME, contentValues2, "group_id = ?", new String[]{((GroupId.V1) it3.next()).toString()});
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            ApplicationDependencies.getRecipientCache().clear();
        } catch (Throwable th) {
            writableDatabase.endTransaction();
            throw th;
        }
    }

    public final Cursor getBlocked() {
        Cursor query = getReadableDatabase().query(TABLE_NAME, ID_PROJECTION, "blocked = 1", null, null, null, null);
        Intrinsics.checkNotNullExpressionValue(query, "readableDatabase.query(T…, null, null, null, null)");
        return query;
    }

    public final RecipientReader readerForBlocked(Cursor cursor) {
        Intrinsics.checkNotNullParameter(cursor, "cursor");
        return new RecipientReader(cursor);
    }

    public final RecipientReader getRecipientsWithNotificationChannels() {
        Cursor query = getReadableDatabase().query(TABLE_NAME, ID_PROJECTION, "notification_channel NOT NULL", null, null, null, null);
        Intrinsics.checkNotNullExpressionValue(query, "cursor");
        return new RecipientReader(query);
    }

    public final RecipientRecord getRecord(RecipientId recipientId) {
        RecipientRecord record;
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Cursor query = getReadableDatabase().query(TABLE_NAME, RECIPIENT_PROJECTION, "_id = ?", new String[]{recipientId.serialize()}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    Context context = this.context;
                    Intrinsics.checkNotNullExpressionValue(context, "context");
                    record = getRecord(context, query);
                    th = null;
                    return record;
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Optional<RecipientId> recipient = RemappedRecords.getInstance().getRecipient(recipientId);
        Intrinsics.checkNotNullExpressionValue(recipient, "getInstance().getRecipient(id)");
        if (recipient.isPresent()) {
            String str = TAG;
            Log.w(str, "Missing recipient for " + recipientId + ", but found it in the remapped records as " + recipient.get());
            RecipientId recipientId2 = recipient.get();
            Intrinsics.checkNotNullExpressionValue(recipientId2, "remapped.get()");
            record = getRecord(recipientId2);
            th = null;
            return record;
        }
        throw new MissingRecipientException(recipientId);
    }

    public final RecipientRecord getRecordForSync(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        List<RecipientRecord> recordForSync = getRecordForSync("recipient._id = ?", new String[]{recipientId.serialize()});
        if (recordForSync.isEmpty()) {
            return null;
        }
        if (recordForSync.size() <= 1) {
            return recordForSync.get(0);
        }
        throw new AssertionError();
    }

    public final RecipientRecord getByStorageId(byte[] bArr) {
        Intrinsics.checkNotNullParameter(bArr, "storageId");
        String encodeBytes = Base64.encodeBytes(bArr);
        Intrinsics.checkNotNullExpressionValue(encodeBytes, "encodeBytes(storageId)");
        List<RecipientRecord> recordForSync = getRecordForSync("recipient.storage_service_key = ?", new String[]{encodeBytes});
        if (true ^ recordForSync.isEmpty()) {
            return recordForSync.get(0);
        }
        return null;
    }

    public final void markNeedsSyncWithoutRefresh(Collection<? extends RecipientId> collection) {
        Intrinsics.checkNotNullParameter(collection, "recipientIds");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (RecipientId recipientId : collection) {
                rotateStorageId(recipientId);
            }
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public final void markNeedsSync(Collection<? extends RecipientId> collection) {
        Intrinsics.checkNotNullParameter(collection, "recipientIds");
        SQLiteDatabaseExtensionsKt.withinTransaction(getWritableDatabase(), new Function1<SQLiteDatabase, Unit>(collection, this) { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$markNeedsSync$1
            final /* synthetic */ Collection<RecipientId> $recipientIds;
            final /* synthetic */ RecipientDatabase this$0;

            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.Collection<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$recipientIds = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(SQLiteDatabase sQLiteDatabase) {
                invoke(sQLiteDatabase);
                return Unit.INSTANCE;
            }

            public final void invoke(SQLiteDatabase sQLiteDatabase) {
                for (RecipientId recipientId : this.$recipientIds) {
                    this.this$0.markNeedsSync(recipientId);
                }
            }
        });
    }

    public final void markNeedsSync(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        rotateStorageId(recipientId);
        ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
    }

    /* JADX INFO: finally extract failed */
    public final void applyStorageIdUpdates(Map<RecipientId, ? extends StorageId> map) {
        Intrinsics.checkNotNullParameter(map, "storageIds");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (Map.Entry<RecipientId, ? extends StorageId> entry : map.entrySet()) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(STORAGE_SERVICE_ID, Base64.encodeBytes(((StorageId) entry.getValue()).getRaw()));
                writableDatabase.update(TABLE_NAME, contentValues, "_id = ?", new String[]{entry.getKey().serialize()});
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            for (RecipientId recipientId : map.keySet()) {
                ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
            }
        } catch (Throwable th) {
            writableDatabase.endTransaction();
            throw th;
        }
    }

    public final void applyStorageSyncContactInsert(SignalContactRecord signalContactRecord) {
        RecipientId recipientId;
        Intrinsics.checkNotNullParameter(signalContactRecord, "insert");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        ThreadDatabase threads = companion.threads();
        ContentValues valuesForStorageContact = getValuesForStorageContact(signalContactRecord, true);
        long insertWithOnConflict = writableDatabase.insertWithOnConflict(TABLE_NAME, null, valuesForStorageContact, 4);
        if (insertWithOnConflict < 0) {
            Log.w(TAG, "[applyStorageSyncContactInsert] Failed to insert. Possibly merging.");
            recipientId = getAndPossiblyMerge$default(this, signalContactRecord.getAddress().hasValidServiceId() ? signalContactRecord.getAddress().getServiceId() : null, signalContactRecord.getAddress().getNumber().orElse(null), false, 4, null);
            writableDatabase.update(TABLE_NAME, valuesForStorageContact, "_id = ?", SqlUtil.buildArgs(recipientId));
        } else {
            recipientId = RecipientId.from(insertWithOnConflict);
            Intrinsics.checkNotNullExpressionValue(recipientId, "from(id)");
        }
        if (signalContactRecord.getIdentityKey().isPresent() && signalContactRecord.getAddress().hasValidServiceId()) {
            try {
                companion.identities().updateIdentityAfterSync(signalContactRecord.getAddress().getIdentifier(), recipientId, new IdentityKey(signalContactRecord.getIdentityKey().get(), 0), StorageSyncModels.remoteToLocalIdentityStatus(signalContactRecord.getIdentityState()));
            } catch (InvalidKeyException e) {
                Log.w(TAG, "Failed to process identity key during insert! Skipping.", e);
            }
        }
        updateExtras(recipientId, new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda8
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1690applyStorageSyncContactInsert$lambda17(SignalContactRecord.this, (RecipientExtras.Builder) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        threads.applyStorageSyncUpdate(recipientId, signalContactRecord);
    }

    /* renamed from: applyStorageSyncContactInsert$lambda-17 */
    public static final RecipientExtras.Builder m1690applyStorageSyncContactInsert$lambda17(SignalContactRecord signalContactRecord, RecipientExtras.Builder builder) {
        Intrinsics.checkNotNullParameter(signalContactRecord, "$insert");
        Intrinsics.checkNotNullParameter(builder, "it");
        return builder.setHideStory(signalContactRecord.shouldHideStory());
    }

    public final void applyStorageSyncContactUpdate(StorageRecordUpdate<SignalContactRecord> storageRecordUpdate) {
        IdentityDatabase.VerifiedStatus verifiedStatus;
        IdentityDatabase.VerifiedStatus verifiedStatus2;
        Intrinsics.checkNotNullParameter(storageRecordUpdate, "update");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        SignalIdentityKeyStore identities = ApplicationDependencies.getProtocolStore().aci().identities();
        Intrinsics.checkNotNullExpressionValue(identities, "getProtocolStore().aci().identities()");
        SignalContactRecord signalContactRecord = storageRecordUpdate.getNew();
        Intrinsics.checkNotNullExpressionValue(signalContactRecord, "update.new");
        ContentValues valuesForStorageContact = getValuesForStorageContact(signalContactRecord, false);
        try {
        } catch (SQLiteConstraintException unused) {
            String str = TAG;
            Log.w(str, "[applyStorageSyncContactUpdate] Failed to update a user by storageId.");
            String encodeBytes = Base64.encodeBytes(storageRecordUpdate.getOld().getId().getRaw());
            Intrinsics.checkNotNullExpressionValue(encodeBytes, "encodeBytes(update.old.id.raw)");
            RecipientId recipientId = getByColumn(STORAGE_SERVICE_ID, encodeBytes).get();
            Intrinsics.checkNotNullExpressionValue(recipientId, "getByColumn(STORAGE_SERV…update.old.id.raw)).get()");
            Log.w(str, "[applyStorageSyncContactUpdate] Found user " + recipientId + ". Possibly merging.");
            RecipientId andPossiblyMerge$default = getAndPossiblyMerge$default(this, storageRecordUpdate.getNew().getAddress().hasValidServiceId() ? storageRecordUpdate.getNew().getAddress().getServiceId() : null, storageRecordUpdate.getNew().getAddress().getNumber().orElse(null), false, 4, null);
            Log.w(str, "[applyStorageSyncContactUpdate] Merged into " + andPossiblyMerge$default);
            writableDatabase.update(TABLE_NAME, valuesForStorageContact, "_id = ?", SqlUtil.buildArgs(andPossiblyMerge$default));
        }
        if (writableDatabase.update(TABLE_NAME, valuesForStorageContact, "storage_service_key = ?", new String[]{Base64.encodeBytes(storageRecordUpdate.getOld().getId().getRaw())}) >= 1) {
            byte[] raw = storageRecordUpdate.getNew().getId().getRaw();
            Intrinsics.checkNotNullExpressionValue(raw, "update.new.id.raw");
            RecipientId byStorageKeyOrThrow = getByStorageKeyOrThrow(raw);
            if (StorageSyncHelper.profileKeyChanged(storageRecordUpdate)) {
                ContentValues contentValues = new ContentValues(1);
                contentValues.putNull(EXPIRING_PROFILE_KEY_CREDENTIAL);
                writableDatabase.update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(byStorageKeyOrThrow));
            }
            try {
                Optional<IdentityRecord> identityRecord = identities.getIdentityRecord(byStorageKeyOrThrow);
                Intrinsics.checkNotNullExpressionValue(identityRecord, "identityStore.getIdentityRecord(recipientId)");
                if (storageRecordUpdate.getNew().getIdentityKey().isPresent() && storageRecordUpdate.getNew().getAddress().hasValidServiceId()) {
                    SignalDatabase.Companion.identities().updateIdentityAfterSync(storageRecordUpdate.getNew().getAddress().getIdentifier(), byStorageKeyOrThrow, new IdentityKey(storageRecordUpdate.getNew().getIdentityKey().get(), 0), StorageSyncModels.remoteToLocalIdentityStatus(storageRecordUpdate.getNew().getIdentityState()));
                }
                Optional<IdentityRecord> identityRecord2 = identities.getIdentityRecord(byStorageKeyOrThrow);
                Intrinsics.checkNotNullExpressionValue(identityRecord2, "identityStore.getIdentityRecord(recipientId)");
                if (identityRecord2.isPresent() && identityRecord2.get().getVerifiedStatus() == (verifiedStatus2 = IdentityDatabase.VerifiedStatus.VERIFIED) && (!identityRecord.isPresent() || identityRecord.get().getVerifiedStatus() != verifiedStatus2)) {
                    IdentityUtil.markIdentityVerified(this.context, Recipient.resolved(byStorageKeyOrThrow), true, true);
                } else if (identityRecord2.isPresent() && identityRecord2.get().getVerifiedStatus() != (verifiedStatus = IdentityDatabase.VerifiedStatus.VERIFIED) && identityRecord.isPresent() && identityRecord.get().getVerifiedStatus() == verifiedStatus) {
                    IdentityUtil.markIdentityVerified(this.context, Recipient.resolved(byStorageKeyOrThrow), false, true);
                }
            } catch (InvalidKeyException e) {
                Log.w(TAG, "Failed to process identity key during update! Skipping.", e);
            }
            updateExtras(byStorageKeyOrThrow, new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda9
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return RecipientDatabase.m1691applyStorageSyncContactUpdate$lambda19(StorageRecordUpdate.this, (RecipientExtras.Builder) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            });
            SignalDatabase.Companion.threads().applyStorageSyncUpdate(byStorageKeyOrThrow, storageRecordUpdate.getNew());
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(byStorageKeyOrThrow);
            return;
        }
        throw new AssertionError("Had an update, but it didn't match any rows!");
    }

    /* renamed from: applyStorageSyncContactUpdate$lambda-19 */
    public static final RecipientExtras.Builder m1691applyStorageSyncContactUpdate$lambda19(StorageRecordUpdate storageRecordUpdate, RecipientExtras.Builder builder) {
        Intrinsics.checkNotNullParameter(storageRecordUpdate, "$update");
        Intrinsics.checkNotNullParameter(builder, "it");
        return builder.setHideStory(((SignalContactRecord) storageRecordUpdate.getNew()).shouldHideStory());
    }

    public final void applyStorageSyncGroupV1Insert(SignalGroupV1Record signalGroupV1Record) {
        Intrinsics.checkNotNullParameter(signalGroupV1Record, "insert");
        RecipientId from = RecipientId.from(getWritableDatabase().insertOrThrow(TABLE_NAME, null, getValuesForStorageGroupV1(signalGroupV1Record, true)));
        SignalDatabase.Companion.threads().applyStorageSyncUpdate(from, signalGroupV1Record);
        ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(from);
    }

    public final void applyStorageSyncGroupV1Update(StorageRecordUpdate<SignalGroupV1Record> storageRecordUpdate) {
        Intrinsics.checkNotNullParameter(storageRecordUpdate, "update");
        SignalGroupV1Record signalGroupV1Record = storageRecordUpdate.getNew();
        Intrinsics.checkNotNullExpressionValue(signalGroupV1Record, "update.new");
        if (getWritableDatabase().update(TABLE_NAME, getValuesForStorageGroupV1(signalGroupV1Record, false), "storage_service_key = ?", new String[]{Base64.encodeBytes(storageRecordUpdate.getOld().getId().getRaw())}) >= 1) {
            Recipient externalGroupExact = Recipient.externalGroupExact(GroupId.v1orThrow(storageRecordUpdate.getOld().getGroupId()));
            Intrinsics.checkNotNullExpressionValue(externalGroupExact, "externalGroupExact(Group…hrow(update.old.groupId))");
            SignalDatabase.Companion.threads().applyStorageSyncUpdate(externalGroupExact.getId(), storageRecordUpdate.getNew());
            externalGroupExact.live().refresh();
            return;
        }
        throw new AssertionError("Had an update, but it didn't match any rows!");
    }

    public final void applyStorageSyncGroupV2Insert(SignalGroupV2Record signalGroupV2Record) {
        Intrinsics.checkNotNullParameter(signalGroupV2Record, "insert");
        GroupMasterKey masterKeyOrThrow = signalGroupV2Record.getMasterKeyOrThrow();
        GroupId.V2 v2 = GroupId.v2(masterKeyOrThrow);
        getWritableDatabase().insertOrThrow(TABLE_NAME, null, getValuesForStorageGroupV2(signalGroupV2Record, true));
        Recipient externalGroupExact = Recipient.externalGroupExact(v2);
        Intrinsics.checkNotNullExpressionValue(externalGroupExact, "externalGroupExact(groupId)");
        String str = TAG;
        Log.i(str, "Creating restore placeholder for " + v2);
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        companion.groups().create(masterKeyOrThrow, DecryptedGroup.newBuilder().setRevision(-2).build());
        RecipientId id = externalGroupExact.getId();
        Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
        updateExtras(id, new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda14
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1692applyStorageSyncGroupV2Insert$lambda20(SignalGroupV2Record.this, (RecipientExtras.Builder) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        Log.i(str, "Scheduling request for latest group info for " + v2);
        ApplicationDependencies.getJobManager().add(new RequestGroupV2InfoJob(v2));
        companion.threads().applyStorageSyncUpdate(externalGroupExact.getId(), signalGroupV2Record);
        externalGroupExact.live().refresh();
    }

    /* renamed from: applyStorageSyncGroupV2Insert$lambda-20 */
    public static final RecipientExtras.Builder m1692applyStorageSyncGroupV2Insert$lambda20(SignalGroupV2Record signalGroupV2Record, RecipientExtras.Builder builder) {
        Intrinsics.checkNotNullParameter(signalGroupV2Record, "$insert");
        Intrinsics.checkNotNullParameter(builder, "it");
        return builder.setHideStory(signalGroupV2Record.shouldHideStory());
    }

    public final void applyStorageSyncGroupV2Update(StorageRecordUpdate<SignalGroupV2Record> storageRecordUpdate) {
        Intrinsics.checkNotNullParameter(storageRecordUpdate, "update");
        SignalGroupV2Record signalGroupV2Record = storageRecordUpdate.getNew();
        Intrinsics.checkNotNullExpressionValue(signalGroupV2Record, "update.new");
        if (getWritableDatabase().update(TABLE_NAME, getValuesForStorageGroupV2(signalGroupV2Record, false), "storage_service_key = ?", new String[]{Base64.encodeBytes(storageRecordUpdate.getOld().getId().getRaw())}) >= 1) {
            Recipient externalGroupExact = Recipient.externalGroupExact(GroupId.v2(storageRecordUpdate.getOld().getMasterKeyOrThrow()));
            Intrinsics.checkNotNullExpressionValue(externalGroupExact, "externalGroupExact(GroupId.v2(masterKey))");
            RecipientId id = externalGroupExact.getId();
            Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
            updateExtras(id, new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda22
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return RecipientDatabase.m1693applyStorageSyncGroupV2Update$lambda21(StorageRecordUpdate.this, (RecipientExtras.Builder) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            });
            SignalDatabase.Companion.threads().applyStorageSyncUpdate(externalGroupExact.getId(), storageRecordUpdate.getNew());
            externalGroupExact.live().refresh();
            return;
        }
        throw new AssertionError("Had an update, but it didn't match any rows!");
    }

    /* renamed from: applyStorageSyncGroupV2Update$lambda-21 */
    public static final RecipientExtras.Builder m1693applyStorageSyncGroupV2Update$lambda21(StorageRecordUpdate storageRecordUpdate, RecipientExtras.Builder builder) {
        Intrinsics.checkNotNullParameter(storageRecordUpdate, "$update");
        Intrinsics.checkNotNullParameter(builder, "it");
        return builder.setHideStory(((SignalGroupV2Record) storageRecordUpdate.getNew()).shouldHideStory());
    }

    public final void applyStorageSyncAccountUpdate(StorageRecordUpdate<SignalAccountRecord> storageRecordUpdate) {
        Intrinsics.checkNotNullParameter(storageRecordUpdate, "update");
        ProfileName fromParts = ProfileName.fromParts(storageRecordUpdate.getNew().getGivenName().orElse(null), storageRecordUpdate.getNew().getFamilyName().orElse(null));
        Intrinsics.checkNotNullExpressionValue(fromParts, "fromParts(update.new.giv….familyName.orElse(null))");
        Optional<ProfileKey> profileKeyOptional = ProfileKeyUtil.profileKeyOptional(storageRecordUpdate.getOld().getProfileKey().orElse(null));
        Intrinsics.checkNotNullExpressionValue(profileKeyOptional, "profileKeyOptional(updat….profileKey.orElse(null))");
        Optional<ProfileKey> profileKeyOptional2 = ProfileKeyUtil.profileKeyOptional(storageRecordUpdate.getNew().getProfileKey().orElse(null));
        Intrinsics.checkNotNullExpressionValue(profileKeyOptional2, "profileKeyOptional(updat….profileKey.orElse(null))");
        String str = (String) OptionalExtensionsKt.or(profileKeyOptional2, profileKeyOptional).map(new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda11
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1687applyStorageSyncAccountUpdate$lambda22((ProfileKey) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda12
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1688applyStorageSyncAccountUpdate$lambda23((byte[]) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null);
        if (!profileKeyOptional2.isPresent()) {
            Log.w(TAG, "Got an empty profile key while applying an account record update!");
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(PROFILE_GIVEN_NAME, fromParts.getGivenName());
        contentValues.put(PROFILE_FAMILY_NAME, fromParts.getFamilyName());
        contentValues.put(PROFILE_JOINED_NAME, fromParts.toString());
        contentValues.put(PROFILE_KEY, str);
        contentValues.put(STORAGE_SERVICE_ID, Base64.encodeBytes(storageRecordUpdate.getNew().getId().getRaw()));
        if (storageRecordUpdate.getNew().hasUnknownFields()) {
            byte[] serializeUnknownFields = storageRecordUpdate.getNew().serializeUnknownFields();
            Objects.requireNonNull(serializeUnknownFields);
            contentValues.put(STORAGE_PROTO, Base64.encodeBytes(serializeUnknownFields));
        } else {
            contentValues.putNull(STORAGE_PROTO);
        }
        if (getWritableDatabase().update(TABLE_NAME, contentValues, "storage_service_key = ?", new String[]{Base64.encodeBytes(storageRecordUpdate.getOld().getId().getRaw())}) >= 1) {
            if (!Intrinsics.areEqual(profileKeyOptional2, profileKeyOptional)) {
                Log.i(TAG, "Our own profile key was changed during a storage sync.", new Throwable());
                SignalDatabase.Companion.runPostSuccessfulTransaction(new Runnable() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda13
                    @Override // java.lang.Runnable
                    public final void run() {
                        RecipientDatabase.m1689applyStorageSyncAccountUpdate$lambda25();
                    }
                });
            }
            SignalDatabase.Companion.threads().applyStorageSyncUpdate(Recipient.self().getId(), storageRecordUpdate.getNew());
            Recipient.self().live().refresh();
            return;
        }
        throw new AssertionError("Account update didn't match any rows!");
    }

    /* renamed from: applyStorageSyncAccountUpdate$lambda-22 */
    public static final byte[] m1687applyStorageSyncAccountUpdate$lambda22(ProfileKey profileKey) {
        Intrinsics.checkNotNullParameter(profileKey, "obj");
        return profileKey.serialize();
    }

    /* renamed from: applyStorageSyncAccountUpdate$lambda-23 */
    public static final String m1688applyStorageSyncAccountUpdate$lambda23(byte[] bArr) {
        Intrinsics.checkNotNull(bArr);
        return Base64.encodeBytes(bArr);
    }

    /* renamed from: applyStorageSyncAccountUpdate$lambda-25 */
    public static final void m1689applyStorageSyncAccountUpdate$lambda25() {
        ProfileUtil.handleSelfProfileKeyChange();
    }

    public final void updatePhoneNumbers(Map<String, String> map) {
        Intrinsics.checkNotNullParameter(map, "mapping");
        if (!map.isEmpty()) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            writableDatabase.beginTransaction();
            try {
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(PHONE, entry.getValue());
                    writableDatabase.updateWithOnConflict(TABLE_NAME, contentValues, "phone = ?", new String[]{entry.getKey()}, 4);
                }
                writableDatabase.setTransactionSuccessful();
            } finally {
                writableDatabase.endTransaction();
            }
        }
    }

    private final RecipientId getByStorageKeyOrThrow(byte[] bArr) {
        Cursor query = getReadableDatabase().query(TABLE_NAME, ID_PROJECTION, "storage_service_key = ?", new String[]{Base64.encodeBytes(bArr)}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    RecipientId from = RecipientId.from(query.getLong(query.getColumnIndexOrThrow("_id")));
                    Intrinsics.checkNotNullExpressionValue(from, "{\n        val id = curso…ipientId.from(id)\n      }");
                    th = null;
                    return from;
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        throw new AssertionError("No recipient with that storage key!");
    }

    private final List<RecipientRecord> getRecordForSync(String str, String[] strArr) {
        ArrayList arrayList = new ArrayList();
        Cursor query = getReadableDatabase().query("recipient LEFT OUTER JOIN identities ON recipient.uuid = identities.address \n            LEFT OUTER JOIN groups ON recipient.group_id = groups.group_id \n            LEFT OUTER JOIN thread ON recipient._id = thread.thread_recipient_id", (String[]) ArraysKt___ArraysJvmKt.plus((Object[]) TYPED_RECIPIENT_PROJECTION, (Object[]) new String[]{"recipient.storage_proto", "groups.master_key", "thread.archived", "thread.read", "identities.verified AS identity_status", "identities.identity_key AS identity_key"}), str, strArr, "recipient._id", null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                Context context = this.context;
                Intrinsics.checkNotNullExpressionValue(context, "context");
                arrayList.add(getRecord(context, query));
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return arrayList;
    }

    public final List<StorageId> getContactStorageSyncIds() {
        return new ArrayList(getContactStorageSyncIdsMap().values());
    }

    /* JADX INFO: finally extract failed */
    public final Map<RecipientId, StorageId> getContactStorageSyncIdsMap() {
        Pair pair;
        if (!FeatureFlags.stories() || Recipient.self().getStoriesCapability() != Recipient.Capability.SUPPORTED) {
            pair = TuplesKt.to("(?)", SqlUtil.buildArgs(Integer.valueOf(GroupType.NONE.getId()), Recipient.self().getId(), Integer.valueOf(GroupType.SIGNAL_V1.getId())));
        } else {
            pair = TuplesKt.to("(?, ?)", SqlUtil.buildArgs(Integer.valueOf(GroupType.NONE.getId()), Recipient.self().getId(), Integer.valueOf(GroupType.SIGNAL_V1.getId()), Integer.valueOf(GroupType.DISTRIBUTION_LIST.getId())));
        }
        String[] strArr = (String[]) pair.component2();
        String str = StringsKt__IndentKt.trimIndent("\n      storage_service_key NOT NULL AND (\n        (group_type = ? AND uuid NOT NULL AND _id != ?)\n        OR\n        group_type IN " + ((String) pair.component1()) + "\n      )\n    ");
        HashMap hashMap = new HashMap();
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"_id", STORAGE_SERVICE_ID, GROUP_TYPE}, str, strArr, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                RecipientId from = RecipientId.from(CursorExtensionsKt.requireLong(query, "_id"));
                String requireNonNullString = CursorExtensionsKt.requireNonNullString(query, STORAGE_SERVICE_ID);
                GroupType fromId = GroupType.Companion.fromId(CursorExtensionsKt.requireInt(query, GROUP_TYPE));
                byte[] decodeOrThrow = Base64.decodeOrThrow(requireNonNullString);
                Intrinsics.checkNotNullExpressionValue(decodeOrThrow, "decodeOrThrow(encodedKey)");
                int i = WhenMappings.$EnumSwitchMapping$0[fromId.ordinal()];
                if (i == 1) {
                    Intrinsics.checkNotNullExpressionValue(from, ContactRepository.ID_COLUMN);
                    StorageId forContact = StorageId.forContact(decodeOrThrow);
                    Intrinsics.checkNotNullExpressionValue(forContact, "forContact(key)");
                    hashMap.put(from, forContact);
                } else if (i == 2) {
                    Intrinsics.checkNotNullExpressionValue(from, ContactRepository.ID_COLUMN);
                    StorageId forGroupV1 = StorageId.forGroupV1(decodeOrThrow);
                    Intrinsics.checkNotNullExpressionValue(forGroupV1, "forGroupV1(key)");
                    hashMap.put(from, forGroupV1);
                } else if (i == 3) {
                    Intrinsics.checkNotNullExpressionValue(from, ContactRepository.ID_COLUMN);
                    StorageId forStoryDistributionList = StorageId.forStoryDistributionList(decodeOrThrow);
                    Intrinsics.checkNotNullExpressionValue(forStoryDistributionList, "forStoryDistributionList(key)");
                    hashMap.put(from, forStoryDistributionList);
                } else {
                    throw new AssertionError();
                }
            } finally {
                try {
                    throw th;
                } catch (Throwable th) {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        CloseableKt.closeFinally(query, null);
        for (GroupId.V2 v2 : SignalDatabase.Companion.groups().getAllGroupV2Ids()) {
            Intrinsics.checkNotNull(v2);
            Recipient externalGroupExact = Recipient.externalGroupExact(v2);
            Intrinsics.checkNotNullExpressionValue(externalGroupExact, "externalGroupExact(id!!)");
            RecipientId id = externalGroupExact.getId();
            Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
            RecipientRecord recordForSync = getRecordForSync(id);
            if (recordForSync != null) {
                byte[] storageId = recordForSync.getStorageId();
                if (storageId != null) {
                    StorageId forGroupV2 = StorageId.forGroupV2(storageId);
                    Intrinsics.checkNotNullExpressionValue(forGroupV2, "forGroupV2(key)");
                    hashMap.put(id, forGroupV2);
                } else {
                    throw new AssertionError();
                }
            } else {
                throw new AssertionError();
            }
        }
        return hashMap;
    }

    public final BulkOperationsHandle beginBulkSystemContactUpdate(boolean z) {
        getWritableDatabase().beginTransaction();
        if (z) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
            UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME).values(TuplesKt.to(SYSTEM_INFO_PENDING, 1)).where("system_contact_uri NOT NULL", new Object[0]), 0, 1, null);
        }
        SQLiteDatabase writableDatabase2 = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase2, "writableDatabase");
        return new BulkOperationsHandle(this, writableDatabase2);
    }

    /* JADX INFO: finally extract failed */
    public final void onUpdatedChatColors(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        String[] buildArgs = SqlUtil.buildArgs(chatColors.getId().getLongValue());
        LinkedList<RecipientId> linkedList = new LinkedList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, SqlUtil.buildArgs("_id"), "custom_chat_colors_id = ?", buildArgs, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                RecipientId from = RecipientId.from(CursorExtensionsKt.requireLong(query, "_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(cursor.requireLong(ID))");
                linkedList.add(from);
            } catch (Throwable th) {
                try {
                    throw th;
                } catch (Throwable th2) {
                    CloseableKt.closeFinally(query, th);
                    throw th2;
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        CloseableKt.closeFinally(query, null);
        if (linkedList.isEmpty()) {
            Log.d(TAG, "No recipients utilizing updated chat color.");
            return;
        }
        ContentValues contentValues = new ContentValues(2);
        contentValues.put(CHAT_COLORS, chatColors.serialize().toByteArray());
        contentValues.put(CUSTOM_CHAT_COLORS_ID, Long.valueOf(chatColors.getId().getLongValue()));
        getWritableDatabase().update(TABLE_NAME, contentValues, "custom_chat_colors_id = ?", buildArgs);
        for (RecipientId recipientId : linkedList) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    /* JADX INFO: finally extract failed */
    public final void onDeletedChatColors(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        String[] buildArgs = SqlUtil.buildArgs(chatColors.getId().getLongValue());
        LinkedList<RecipientId> linkedList = new LinkedList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, SqlUtil.buildArgs("_id"), "custom_chat_colors_id = ?", buildArgs, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                RecipientId from = RecipientId.from(CursorExtensionsKt.requireLong(query, "_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(cursor.requireLong(ID))");
                linkedList.add(from);
            } catch (Throwable th) {
                try {
                    throw th;
                } catch (Throwable th2) {
                    CloseableKt.closeFinally(query, th);
                    throw th2;
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        CloseableKt.closeFinally(query, null);
        if (linkedList.isEmpty()) {
            Log.d(TAG, "No recipients utilizing deleted chat color.");
            return;
        }
        ContentValues contentValues = new ContentValues(2);
        contentValues.put(CHAT_COLORS, (byte[]) null);
        contentValues.put(CUSTOM_CHAT_COLORS_ID, Long.valueOf(ChatColors.Id.NotSet.INSTANCE.getLongValue()));
        getWritableDatabase().update(TABLE_NAME, contentValues, "custom_chat_colors_id = ?", buildArgs);
        for (RecipientId recipientId : linkedList) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final int getColorUsageCount(ChatColors.Id id) {
        Intrinsics.checkNotNullParameter(id, "chatColorsId");
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"COUNT(*)"}, "custom_chat_colors_id = ?", SqlUtil.buildArgs(id.getLongValue()), null, null, null);
        try {
            int i = 0;
            if (query.moveToFirst()) {
                i = query.getInt(0);
            }
            th = null;
            return i;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public final void clearAllColors() {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        String[] buildArgs = SqlUtil.buildArgs(ChatColors.Id.NotSet.INSTANCE.getLongValue());
        LinkedList<RecipientId> linkedList = new LinkedList();
        Cursor query = writableDatabase.query(TABLE_NAME, SqlUtil.buildArgs("_id"), "custom_chat_colors_id != ?", buildArgs, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                RecipientId from = RecipientId.from(CursorExtensionsKt.requireLong(query, "_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(cursor.requireLong(ID))");
                linkedList.add(from);
            } finally {
                try {
                    throw th;
                } catch (Throwable th) {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        CloseableKt.closeFinally(query, null);
        if (!linkedList.isEmpty()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(CHAT_COLORS, (byte[]) null);
            contentValues.put(CUSTOM_CHAT_COLORS_ID, Long.valueOf(ChatColors.Id.NotSet.INSTANCE.getLongValue()));
            writableDatabase.update(TABLE_NAME, contentValues, "custom_chat_colors_id != ?", buildArgs);
            for (RecipientId recipientId : linkedList) {
                ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
            }
        }
    }

    public final void clearColor(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues();
        contentValues.put(CHAT_COLORS, (byte[]) null);
        contentValues.put(CUSTOM_CHAT_COLORS_ID, Long.valueOf(ChatColors.Id.NotSet.INSTANCE.getLongValue()));
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setColor(RecipientId recipientId, ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(chatColors, "color");
        ContentValues contentValues = new ContentValues();
        contentValues.put(CHAT_COLORS, chatColors.serialize().toByteArray());
        contentValues.put(CUSTOM_CHAT_COLORS_ID, Long.valueOf(chatColors.getId().getLongValue()));
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setDefaultSubscriptionId(RecipientId recipientId, int i) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues();
        contentValues.put(DEFAULT_SUBSCRIPTION_ID, Integer.valueOf(i));
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setForceSmsSelection(RecipientId recipientId, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(FORCE_SMS_SELECTION, Integer.valueOf(z ? 1 : 0));
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setBlocked(RecipientId recipientId, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues();
        contentValues.put(BLOCKED, Integer.valueOf(z ? 1 : 0));
        if (update(recipientId, contentValues)) {
            rotateStorageId(recipientId);
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setMessageRingtone(RecipientId recipientId, Uri uri) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues();
        contentValues.put(MESSAGE_RINGTONE, uri != null ? uri.toString() : null);
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setCallRingtone(RecipientId recipientId, Uri uri) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues();
        contentValues.put(CALL_RINGTONE, uri != null ? uri.toString() : null);
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setMessageVibrate(RecipientId recipientId, VibrateState vibrateState) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(vibrateState, NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED);
        ContentValues contentValues = new ContentValues();
        contentValues.put(MESSAGE_VIBRATE, Integer.valueOf(vibrateState.getId()));
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setCallVibrate(RecipientId recipientId, VibrateState vibrateState) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(vibrateState, NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED);
        ContentValues contentValues = new ContentValues();
        contentValues.put(CALL_VIBRATE, Integer.valueOf(vibrateState.getId()));
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setMuted(RecipientId recipientId, long j) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues();
        contentValues.put(MUTE_UNTIL, Long.valueOf(j));
        if (update(recipientId, contentValues)) {
            rotateStorageId(recipientId);
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
        StorageSyncHelper.scheduleSyncForDataChange();
    }

    /* JADX INFO: finally extract failed */
    public final void setMuted(Collection<? extends RecipientId> collection, long j) {
        Intrinsics.checkNotNullParameter(collection, "ids");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            SqlUtil.Query buildSingleCollectionQuery = SqlUtil.buildSingleCollectionQuery("_id", collection);
            ContentValues contentValues = new ContentValues();
            contentValues.put(MUTE_UNTIL, Long.valueOf(j));
            writableDatabase.update(TABLE_NAME, contentValues, buildSingleCollectionQuery.getWhere(), buildSingleCollectionQuery.getWhereArgs());
            for (RecipientId recipientId : collection) {
                rotateStorageId(recipientId);
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            for (RecipientId recipientId2 : collection) {
                ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId2);
            }
            StorageSyncHelper.scheduleSyncForDataChange();
        } catch (Throwable th) {
            writableDatabase.endTransaction();
            throw th;
        }
    }

    public final void setSeenFirstInviteReminder(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        setInsightsBannerTier(recipientId, InsightsBannerTier.TIER_ONE);
    }

    public final void setSeenSecondInviteReminder(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        setInsightsBannerTier(recipientId, InsightsBannerTier.TIER_TWO);
    }

    public final void setHasSentInvite(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        setSeenSecondInviteReminder(recipientId);
    }

    private final void setInsightsBannerTier(RecipientId recipientId, InsightsBannerTier insightsBannerTier) {
        String[] strArr = {recipientId.serialize(), insightsBannerTier.toString()};
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(SEEN_INVITE_REMINDER, Integer.valueOf(insightsBannerTier.getId()));
        getWritableDatabase().update(TABLE_NAME, contentValues, "_id = ? AND seen_invite_reminder < ?", strArr);
        ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
    }

    public final void setExpireMessages(RecipientId recipientId, int i) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(MESSAGE_EXPIRATION_TIME, Integer.valueOf(i));
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setUnidentifiedAccessMode(RecipientId recipientId, UnidentifiedAccessMode unidentifiedAccessMode) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(unidentifiedAccessMode, "unidentifiedAccessMode");
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(UNIDENTIFIED_ACCESS_MODE, Integer.valueOf(unidentifiedAccessMode.getMode()));
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setLastSessionResetTime(RecipientId recipientId, DeviceLastResetTime deviceLastResetTime) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(deviceLastResetTime, "lastResetTime");
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(LAST_SESSION_RESET, deviceLastResetTime.toByteArray());
        update(recipientId, contentValues);
    }

    public final DeviceLastResetTime getLastSessionResetTimes(RecipientId recipientId) {
        DeviceLastResetTime deviceLastResetTime;
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{LAST_SESSION_RESET}, "_id = ?", SqlUtil.buildArgs(recipientId), null, null, null);
        try {
            th = null;
            if (query.moveToFirst()) {
                try {
                    Intrinsics.checkNotNullExpressionValue(query, "cursor");
                    byte[] requireBlob = CursorExtensionsKt.requireBlob(query, LAST_SESSION_RESET);
                    if (requireBlob != null) {
                        deviceLastResetTime = DeviceLastResetTime.parseFrom(requireBlob);
                    } else {
                        deviceLastResetTime = DeviceLastResetTime.newBuilder().build();
                    }
                    Intrinsics.checkNotNullExpressionValue(deviceLastResetTime, "{\n          val serializ…d()\n          }\n        }");
                } catch (InvalidProtocolBufferException e) {
                    Log.w(TAG, e);
                    DeviceLastResetTime build = DeviceLastResetTime.newBuilder().build();
                    Intrinsics.checkNotNullExpressionValue(build, "{\n          Log.w(TAG, e…ilder().build()\n        }");
                    deviceLastResetTime = build;
                }
                return deviceLastResetTime;
            }
            Unit unit = Unit.INSTANCE;
            CloseableKt.closeFinally(query, th);
            DeviceLastResetTime build2 = DeviceLastResetTime.newBuilder().build();
            Intrinsics.checkNotNullExpressionValue(build2, "newBuilder().build()");
            return build2;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final void setBadges(RecipientId recipientId, List<Badge> list) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(list, BADGES);
        BadgeList.Builder newBuilder = BadgeList.newBuilder();
        for (Badge badge : list) {
            newBuilder.addBadges(Badges.toDatabaseBadge(badge));
        }
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(BADGES, newBuilder.build().toByteArray());
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setCapabilities(RecipientId recipientId, SignalServiceProfile.Capabilities capabilities) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(capabilities, CAPABILITIES);
        long update = Bitmask.update(Bitmask.update(Bitmask.update(Bitmask.update(Bitmask.update(Bitmask.update(0, 1, 2, (long) Recipient.Capability.fromBoolean(capabilities.isGv1Migration()).serialize()), 2, 2, (long) Recipient.Capability.fromBoolean(capabilities.isSenderKey()).serialize()), 3, 2, (long) Recipient.Capability.fromBoolean(capabilities.isAnnouncementGroup()).serialize()), 4, 2, (long) Recipient.Capability.fromBoolean(capabilities.isChangeNumber()).serialize()), 5, 2, (long) Recipient.Capability.fromBoolean(capabilities.isStories()).serialize()), 6, 2, (long) Recipient.Capability.fromBoolean(capabilities.isGiftBadges()).serialize());
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(CAPABILITIES, Long.valueOf(update));
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setMentionSetting(RecipientId recipientId, MentionSetting mentionSetting) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(mentionSetting, "mentionSetting");
        ContentValues contentValues = new ContentValues();
        contentValues.put(MENTION_SETTING, Integer.valueOf(mentionSetting.getId()));
        if (update(recipientId, contentValues)) {
            rotateStorageId(recipientId);
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
            StorageSyncHelper.scheduleSyncForDataChange();
        }
    }

    public final Set<String> getE164sForIds(Collection<? extends RecipientId> collection) {
        Intrinsics.checkNotNullParameter(collection, "ids");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(collection, 10));
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            arrayList.add(new String[]{((RecipientId) it.next()).serialize()});
        }
        List<SqlUtil.Query> buildCustomCollectionQuery = SqlUtil.buildCustomCollectionQuery("_id = ?", CollectionsKt___CollectionsKt.toList(arrayList));
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (SqlUtil.Query query : buildCustomCollectionQuery) {
            Cursor query2 = getReadableDatabase().query(TABLE_NAME, new String[]{PHONE}, query.getWhere(), query.getWhereArgs(), null, null, null);
            th = null;
            while (query2.moveToNext()) {
                try {
                    Intrinsics.checkNotNullExpressionValue(query2, "cursor");
                    String requireString = CursorExtensionsKt.requireString(query2, PHONE);
                    if (requireString != null) {
                        linkedHashSet.add(requireString);
                    }
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        return linkedHashSet;
    }

    public final boolean setProfileKey(RecipientId recipientId, ProfileKey profileKey) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(profileKey, "profileKey");
        String[] strArr = {recipientId.serialize()};
        String encodeBytes = Base64.encodeBytes(profileKey.serialize());
        Intrinsics.checkNotNullExpressionValue(encodeBytes, "encodeBytes(profileKey.serialize())");
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(PROFILE_KEY, encodeBytes);
        ContentValues contentValues2 = new ContentValues(3);
        contentValues2.put(PROFILE_KEY, encodeBytes);
        contentValues2.putNull(EXPIRING_PROFILE_KEY_CREDENTIAL);
        contentValues2.put(UNIDENTIFIED_ACCESS_MODE, Integer.valueOf(UnidentifiedAccessMode.UNKNOWN.getMode()));
        if (!update(SqlUtil.buildTrueUpdateQuery("_id = ?", strArr, contentValues), contentValues2)) {
            return false;
        }
        rotateStorageId(recipientId);
        ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        StorageSyncHelper.scheduleSyncForDataChange();
        if (Intrinsics.areEqual(recipientId, Recipient.self().getId())) {
            Log.i(TAG, "Our own profile key was changed.", new Throwable());
            SignalDatabase.Companion.runPostSuccessfulTransaction(new Runnable() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda10
                @Override // java.lang.Runnable
                public final void run() {
                    RecipientDatabase.m1708setProfileKey$lambda60();
                }
            });
        }
        return true;
    }

    /* renamed from: setProfileKey$lambda-60 */
    public static final void m1708setProfileKey$lambda60() {
        ProfileUtil.handleSelfProfileKeyChange();
    }

    public final boolean setProfileKeyIfAbsent(RecipientId recipientId, ProfileKey profileKey) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(profileKey, "profileKey");
        String[] strArr = {recipientId.serialize()};
        ContentValues contentValues = new ContentValues(3);
        contentValues.put(PROFILE_KEY, Base64.encodeBytes(profileKey.serialize()));
        contentValues.putNull(EXPIRING_PROFILE_KEY_CREDENTIAL);
        contentValues.put(UNIDENTIFIED_ACCESS_MODE, Integer.valueOf(UnidentifiedAccessMode.UNKNOWN.getMode()));
        if (getWritableDatabase().update(TABLE_NAME, contentValues, "_id = ? AND profile_key is NULL", strArr) <= 0) {
            return false;
        }
        rotateStorageId(recipientId);
        ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        return true;
    }

    public final boolean setProfileKeyCredential(RecipientId recipientId, ProfileKey profileKey, ExpiringProfileKeyCredential expiringProfileKeyCredential) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(profileKey, "profileKey");
        Intrinsics.checkNotNullParameter(expiringProfileKeyCredential, "expiringProfileKeyCredential");
        String[] strArr = {recipientId.serialize(), Base64.encodeBytes(profileKey.serialize())};
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(EXPIRING_PROFILE_KEY_CREDENTIAL, Base64.encodeBytes(ExpiringProfileKeyCredentialColumnData.newBuilder().setProfileKey(ByteString.copyFrom(profileKey.serialize())).setExpiringProfileKeyCredential(ByteString.copyFrom(expiringProfileKeyCredential.serialize())).build().toByteArray()));
        boolean update = update(SqlUtil.buildTrueUpdateQuery("_id = ? AND profile_key = ?", strArr, contentValues), contentValues);
        if (update) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
        return update;
    }

    public final void clearProfileKeyCredential(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues(1);
        contentValues.putNull(EXPIRING_PROFILE_KEY_CREDENTIAL);
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final Set<RecipientId> persistProfileKeySet(ProfileKeySet profileKeySet) {
        Intrinsics.checkNotNullParameter(profileKeySet, "profileKeySet");
        Map<ServiceId, ProfileKey> profileKeys = profileKeySet.getProfileKeys();
        Map<ServiceId, ProfileKey> authoritativeProfileKeys = profileKeySet.getAuthoritativeProfileKeys();
        int size = profileKeys.size() + authoritativeProfileKeys.size();
        if (size == 0) {
            return SetsKt__SetsKt.emptySet();
        }
        String str = TAG;
        Log.i(str, "Persisting " + size + " Profile keys, " + authoritativeProfileKeys.size() + " of which are authoritative");
        HashSet hashSet = new HashSet(size);
        RecipientId id = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id, "self().id");
        Intrinsics.checkNotNullExpressionValue(profileKeys, "profileKeys");
        for (Map.Entry<ServiceId, ProfileKey> entry : profileKeys.entrySet()) {
            ServiceId key = entry.getKey();
            ProfileKey value = entry.getValue();
            Intrinsics.checkNotNullExpressionValue(key, "key");
            RecipientId orInsertFromServiceId = getOrInsertFromServiceId(key);
            Intrinsics.checkNotNullExpressionValue(value, DraftDatabase.DRAFT_VALUE);
            if (setProfileKeyIfAbsent(orInsertFromServiceId, value)) {
                Log.i(TAG, "Learned new profile key");
                hashSet.add(orInsertFromServiceId);
            }
        }
        Intrinsics.checkNotNullExpressionValue(authoritativeProfileKeys, "authoritativeProfileKeys");
        for (Map.Entry<ServiceId, ProfileKey> entry2 : authoritativeProfileKeys.entrySet()) {
            ServiceId key2 = entry2.getKey();
            ProfileKey value2 = entry2.getValue();
            Intrinsics.checkNotNullExpressionValue(key2, "key");
            RecipientId orInsertFromServiceId2 = getOrInsertFromServiceId(key2);
            if (Intrinsics.areEqual(id, orInsertFromServiceId2)) {
                String str2 = TAG;
                Log.i(str2, "Seen authoritative update for self");
                if (!Intrinsics.areEqual(value2, ProfileKeyUtil.getSelfProfileKey())) {
                    Log.w(str2, "Seen authoritative update for self that didn't match local, scheduling storage sync");
                    StorageSyncHelper.scheduleSyncForDataChange();
                }
            } else {
                String str3 = TAG;
                Log.i(str3, "Profile key from owner " + orInsertFromServiceId2);
                Intrinsics.checkNotNullExpressionValue(value2, DraftDatabase.DRAFT_VALUE);
                if (setProfileKey(orInsertFromServiceId2, value2)) {
                    Log.i(str3, "Learned new profile key from owner");
                    hashSet.add(orInsertFromServiceId2);
                }
            }
        }
        return hashSet;
    }

    public final List<RecipientId> getSimilarRecipientIds(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, TABLE_NAME);
        Cursor query = getReadableDatabase().query(TABLE_NAME, SqlUtil.buildArgs("_id", "COALESCE(NULLIF(system_display_name, ''), NULLIF(profile_joined_name, '')) AS checked_name"), "checked_name = ?", SqlUtil.buildArgs(recipient.getProfileName().toString()), null, null, null);
        th = null;
        if (query != null) {
            try {
                if (query.getCount() != 0) {
                    ArrayList arrayList = new ArrayList(query.getCount());
                    while (query.moveToNext()) {
                        RecipientId from = RecipientId.from(CursorExtensionsKt.requireLong(query, "_id"));
                        Intrinsics.checkNotNullExpressionValue(from, "from(cursor.requireLong(ID))");
                        arrayList.add(from);
                    }
                    return arrayList;
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        return CollectionsKt__CollectionsKt.emptyList();
    }

    public final void setSystemContactName(RecipientId recipientId, String str) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(str, "systemContactName");
        ContentValues contentValues = new ContentValues();
        contentValues.put(SYSTEM_JOINED_NAME, str);
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setProfileName(RecipientId recipientId, ProfileName profileName) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(profileName, "profileName");
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(PROFILE_GIVEN_NAME, profileName.getGivenName());
        contentValues.put(PROFILE_FAMILY_NAME, profileName.getFamilyName());
        contentValues.put(PROFILE_JOINED_NAME, profileName.toString());
        if (update(recipientId, contentValues)) {
            rotateStorageId(recipientId);
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
            StorageSyncHelper.scheduleSyncForDataChange();
        }
    }

    public final void setProfileAvatar(RecipientId recipientId, String str) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(SIGNAL_PROFILE_AVATAR, str);
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
            if (Intrinsics.areEqual(recipientId, Recipient.self().getId())) {
                rotateStorageId(recipientId);
                StorageSyncHelper.scheduleSyncForDataChange();
            }
        }
    }

    public final void setAbout(RecipientId recipientId, String str, String str2) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues();
        contentValues.put(ABOUT, str);
        contentValues.put(ABOUT_EMOJI, str2);
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void setProfileSharing(RecipientId recipientId, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(PROFILE_SHARING, Integer.valueOf(z ? 1 : 0));
        boolean update = update(recipientId, contentValues);
        if (update && z) {
            Optional<GroupDatabase.GroupRecord> group = SignalDatabase.Companion.groups().getGroup(recipientId);
            if (group.isPresent()) {
                List<RecipientId> members = group.get().getMembers();
                Intrinsics.checkNotNullExpressionValue(members, "group.get().members");
                setHasGroupsInCommon(members);
            }
        }
        if (update) {
            rotateStorageId(recipientId);
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
            StorageSyncHelper.scheduleSyncForDataChange();
        }
    }

    public final void setNotificationChannel(RecipientId recipientId, String str) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(NOTIFICATION_CHANNEL, str);
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void resetAllWallpaper() {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        String[] buildArgs = SqlUtil.buildArgs("_id", WALLPAPER_URI);
        LinkedList<Pair> linkedList = new LinkedList();
        writableDatabase.beginTransaction();
        try {
            Cursor query = writableDatabase.query(TABLE_NAME, buildArgs, "wallpaper IS NOT NULL", null, null, null, null);
            while (query != null && query.moveToNext()) {
                linkedList.add(new Pair(RecipientId.from((long) CursorExtensionsKt.requireInt(query, "_id")), CursorExtensionsKt.optionalString(query, WALLPAPER_URI).orElse(null)));
            }
            Unit unit = Unit.INSTANCE;
            CloseableKt.closeFinally(query, null);
            if (!linkedList.isEmpty()) {
                ContentValues contentValues = new ContentValues(2);
                contentValues.putNull(WALLPAPER_URI);
                contentValues.putNull(WALLPAPER);
                int update = writableDatabase.update(TABLE_NAME, contentValues, "wallpaper IS NOT NULL", null);
                if (update == linkedList.size()) {
                    for (Pair pair : linkedList) {
                        ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged((RecipientId) pair.getFirst());
                        if (pair.getSecond() != null) {
                            WallpaperStorage.onWallpaperDeselected(this.context, Uri.parse((String) pair.getSecond()));
                        }
                    }
                    return;
                }
                throw new AssertionError("expected " + linkedList.size() + " but got " + update);
            }
        } finally {
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
        }
    }

    public final void setWallpaper(RecipientId recipientId, ChatWallpaper chatWallpaper) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        setWallpaper(recipientId, chatWallpaper != null ? chatWallpaper.serialize() : null);
    }

    private final void setWallpaper(RecipientId recipientId, Wallpaper wallpaper) {
        Uri wallpaperUri = getWallpaperUri(recipientId);
        ContentValues contentValues = new ContentValues();
        contentValues.put(WALLPAPER, wallpaper != null ? wallpaper.toByteArray() : null);
        if (wallpaper == null || !wallpaper.hasFile()) {
            contentValues.putNull(WALLPAPER_URI);
        } else {
            contentValues.put(WALLPAPER_URI, wallpaper.getFile().getUri());
        }
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
        if (wallpaperUri != null) {
            WallpaperStorage.onWallpaperDeselected(this.context, wallpaperUri);
        }
    }

    public final void setDimWallpaperInDarkTheme(RecipientId recipientId, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Wallpaper wallpaper = getWallpaper(recipientId);
        if (wallpaper != null) {
            setWallpaper(recipientId, wallpaper.toBuilder().setDimLevelInDarkTheme(z ? 0.2f : 0.0f).build());
            return;
        }
        throw new IllegalStateException("No wallpaper set for " + recipientId);
    }

    /* JADX WARN: Type inference failed for: r1v4, types: [java.lang.Throwable, org.thoughtcrime.securesms.database.model.databaseprotos.Wallpaper] */
    private final Wallpaper getWallpaper(RecipientId recipientId) {
        Wallpaper parseFrom;
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{WALLPAPER}, "_id = ?", SqlUtil.buildArgs(recipientId), null, null, null);
        try {
            th = 0;
            if (query.moveToFirst()) {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                byte[] requireBlob = CursorExtensionsKt.requireBlob(query, WALLPAPER);
                if (requireBlob != null) {
                    try {
                        parseFrom = Wallpaper.parseFrom(requireBlob);
                    } catch (InvalidProtocolBufferException unused) {
                    }
                    return parseFrom;
                }
                parseFrom = th;
                return parseFrom;
            }
            Unit unit = Unit.INSTANCE;
            return th;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    private final Uri getWallpaperUri(RecipientId recipientId) {
        Wallpaper wallpaper = getWallpaper(recipientId);
        if (wallpaper == null || !wallpaper.hasFile()) {
            return null;
        }
        return Uri.parse(wallpaper.getFile().getUri());
    }

    public final int getWallpaperUriUsageCount(Uri uri) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{"COUNT(*)"}, "wallpaper_file = ?", SqlUtil.buildArgs(uri), null, null, null);
        try {
            th = null;
            if (query.moveToFirst()) {
                return query.getInt(0);
            }
            Unit unit = Unit.INSTANCE;
            return 0;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final boolean setPhoneNumber(RecipientId recipientId, String str) {
        boolean z;
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            try {
                setPhoneNumberOrThrow(recipientId, str);
                writableDatabase.setTransactionSuccessful();
                z = false;
            } catch (SQLiteConstraintException unused) {
                String str2 = TAG;
                Log.w(str2, "[setPhoneNumber] Hit a conflict when trying to update " + recipientId + ". Possibly merging.");
                RecipientRecord record = getRecord(recipientId);
                RecipientId andPossiblyMerge$default = getAndPossiblyMerge$default(this, record.getServiceId(), str, false, 4, null);
                Log.w(str2, "[setPhoneNumber] Resulting id: " + andPossiblyMerge$default);
                writableDatabase.setTransactionSuccessful();
                z = !Intrinsics.areEqual(andPossiblyMerge$default, record.getId());
            }
            return z;
        } finally {
            writableDatabase.endTransaction();
        }
    }

    private final void removePhoneNumber(RecipientId recipientId) {
        ContentValues contentValues = new ContentValues();
        contentValues.putNull(PHONE);
        contentValues.putNull(PNI_COLUMN);
        if (update(recipientId, contentValues)) {
            rotateStorageId(recipientId);
        }
    }

    public final void setPhoneNumberOrThrow(RecipientId recipientId, String str) throws SQLiteConstraintException {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(PHONE, str);
        if (update(recipientId, contentValues)) {
            rotateStorageId(recipientId);
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
            StorageSyncHelper.scheduleSyncForDataChange();
        }
    }

    public final void setPhoneNumberOrThrowSilent(RecipientId recipientId, String str) throws SQLiteConstraintException {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(PHONE, str);
        if (update(recipientId, contentValues)) {
            rotateStorageId(recipientId);
        }
    }

    public final void updateSelfPhone(String str) {
        Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            RecipientId id = Recipient.self().getId();
            Intrinsics.checkNotNullExpressionValue(id, "self().id");
            RecipientId andPossiblyMerge = getAndPossiblyMerge(SignalStore.account().requireAci(), str, true);
            if (Intrinsics.areEqual(id, andPossiblyMerge)) {
                Log.i(TAG, "[updateSelfPhone] Phone updated for self");
                writableDatabase.setTransactionSuccessful();
                return;
            }
            throw new AssertionError("[updateSelfPhone] Self recipient id changed when updating phone. old: " + id + " new: " + andPossiblyMerge);
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public final void setUsername(RecipientId recipientId, String str) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        if (str != null) {
            Optional<RecipientId> byUsername = getByUsername(str);
            if (byUsername.isPresent() && !Intrinsics.areEqual(recipientId, byUsername.get())) {
                String str2 = TAG;
                Log.i(str2, "Username was previously thought to be owned by " + byUsername.get() + ". Clearing their username.");
                RecipientId recipientId2 = byUsername.get();
                Intrinsics.checkNotNullExpressionValue(recipientId2, "existingUsername.get()");
                setUsername(recipientId2, null);
            }
        }
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(USERNAME, str);
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
            StorageSyncHelper.scheduleSyncForDataChange();
        }
    }

    /* renamed from: setHideStory$lambda-79 */
    public static final RecipientExtras.Builder m1707setHideStory$lambda79(boolean z, RecipientExtras.Builder builder) {
        Intrinsics.checkNotNullParameter(builder, "it");
        return builder.setHideStory(z);
    }

    public final void setHideStory(RecipientId recipientId, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        updateExtras(recipientId, new Function(z) { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1707setHideStory$lambda79(this.f$0, (RecipientExtras.Builder) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        StorageSyncHelper.scheduleSyncForDataChange();
    }

    /* renamed from: updateLastStoryViewTimestamp$lambda-80 */
    public static final RecipientExtras.Builder m1709updateLastStoryViewTimestamp$lambda80(RecipientExtras.Builder builder) {
        Intrinsics.checkNotNullParameter(builder, "it");
        return builder.setLastStoryView(System.currentTimeMillis());
    }

    public final void updateLastStoryViewTimestamp(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        updateExtras(recipientId, new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1709updateLastStoryViewTimestamp$lambda80((RecipientExtras.Builder) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public final void clearUsernameIfExists(String str) {
        Intrinsics.checkNotNullParameter(str, USERNAME);
        Optional<RecipientId> byUsername = getByUsername(str);
        if (byUsername.isPresent()) {
            RecipientId recipientId = byUsername.get();
            Intrinsics.checkNotNullExpressionValue(recipientId, "existingUsername.get()");
            setUsername(recipientId, null);
        }
    }

    public final Set<String> getAllE164s() {
        HashSet hashSet = new HashSet();
        Cursor query = getReadableDatabase().query(TABLE_NAME, new String[]{PHONE}, null, null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                String string = query.getString(query.getColumnIndexOrThrow(PHONE));
                if (!TextUtils.isEmpty(string)) {
                    Intrinsics.checkNotNullExpressionValue(string, "number");
                    hashSet.add(string);
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return hashSet;
    }

    public final void setPni(RecipientId recipientId, PNI pni) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(pni, PNI_COLUMN);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME).values(TuplesKt.to(SERVICE_ID, pni.toString())).where("_id = ? AND (uuid IS NULL OR uuid = pni)", recipientId), 0, 1, null);
        SQLiteDatabase writableDatabase2 = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase2, "writableDatabase");
        UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase2, TABLE_NAME).values(TuplesKt.to(PNI_COLUMN, pni.toString())).where("_id = ?", recipientId), 0, 1, null);
    }

    public final boolean markRegistered(RecipientId recipientId, ServiceId serviceId) {
        boolean z;
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            try {
                markRegisteredOrThrow(recipientId, serviceId);
                writableDatabase.setTransactionSuccessful();
                z = false;
            } catch (SQLiteConstraintException unused) {
                String str = TAG;
                Log.w(str, "[markRegistered] Hit a conflict when trying to update " + recipientId + ". Possibly merging.");
                RecipientRecord record = getRecord(recipientId);
                RecipientId andPossiblyMerge$default = getAndPossiblyMerge$default(this, serviceId, record.getE164(), false, 4, null);
                Log.w(str, "[markRegistered] Merged into " + andPossiblyMerge$default);
                writableDatabase.setTransactionSuccessful();
                z = !Intrinsics.areEqual(andPossiblyMerge$default, record.getId());
            }
            return z;
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public final void markRegisteredOrThrow(RecipientId recipientId, ServiceId serviceId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        ContentValues contentValues = new ContentValues(2);
        contentValues.put(REGISTERED, Integer.valueOf(RegisteredState.REGISTERED.getId()));
        String serviceId2 = serviceId.toString();
        Intrinsics.checkNotNullExpressionValue(serviceId2, "serviceId.toString()");
        String lowerCase = serviceId2.toLowerCase(Locale.ROOT);
        Intrinsics.checkNotNullExpressionValue(lowerCase, "this as java.lang.String).toLowerCase(Locale.ROOT)");
        contentValues.put(SERVICE_ID, lowerCase);
        if (update(recipientId, contentValues)) {
            setStorageIdIfNotSet(recipientId);
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void markUnregistered(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        ContentValues contentValues = new ContentValues(2);
        contentValues.put(REGISTERED, Integer.valueOf(RegisteredState.NOT_REGISTERED.getId()));
        contentValues.putNull(STORAGE_SERVICE_ID);
        if (update(recipientId, contentValues)) {
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        }
    }

    public final void bulkUpdatedRegisteredStatus(Map<RecipientId, ? extends ServiceId> map, Collection<? extends RecipientId> collection) {
        Intrinsics.checkNotNullParameter(map, REGISTERED);
        Intrinsics.checkNotNullParameter(collection, "unregistered");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (Map.Entry<RecipientId, ? extends ServiceId> entry : map.entrySet()) {
                RecipientId key = entry.getKey();
                ServiceId serviceId = (ServiceId) entry.getValue();
                ContentValues contentValues = new ContentValues(2);
                contentValues.put(REGISTERED, Integer.valueOf(RegisteredState.REGISTERED.getId()));
                if (serviceId != null) {
                    String serviceId2 = serviceId.toString();
                    Intrinsics.checkNotNullExpressionValue(serviceId2, "serviceId.toString()");
                    String lowerCase = serviceId2.toLowerCase(Locale.ROOT);
                    Intrinsics.checkNotNullExpressionValue(lowerCase, "this as java.lang.String).toLowerCase(Locale.ROOT)");
                    contentValues.put(SERVICE_ID, lowerCase);
                }
                try {
                    if (update(key, contentValues)) {
                        setStorageIdIfNotSet(key);
                        ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(key);
                    }
                } catch (SQLiteConstraintException unused) {
                    String str = TAG;
                    Log.w(str, "[bulkUpdateRegisteredStatus] Hit a conflict when trying to update " + key + ". Possibly merging.");
                    RecipientId andPossiblyMerge$default = getAndPossiblyMerge$default(this, serviceId, getRecord(key).getE164(), false, 4, null);
                    Log.w(str, "[bulkUpdateRegisteredStatus] Merged into " + andPossiblyMerge$default);
                }
            }
            for (RecipientId recipientId : collection) {
                ContentValues contentValues2 = new ContentValues(2);
                contentValues2.put(REGISTERED, Integer.valueOf(RegisteredState.NOT_REGISTERED.getId()));
                contentValues2.putNull(STORAGE_SERVICE_ID);
                if (update(recipientId, contentValues2)) {
                    ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
                }
            }
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public final Map<RecipientId, ACI> bulkProcessCdsResult(Map<String, ACI> map) {
        Optional<RecipientId> optional;
        Intrinsics.checkNotNullParameter(map, "mapping");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        writableDatabase.beginTransaction();
        try {
            for (Map.Entry<String, ACI> entry : map.entrySet()) {
                String key = entry.getKey();
                ACI value = entry.getValue();
                if (value != null) {
                    optional = getByServiceId(value);
                } else {
                    optional = Optional.empty();
                    Intrinsics.checkNotNullExpressionValue(optional, "empty()");
                }
                if (optional.isPresent()) {
                    RecipientId recipientId = optional.get();
                    Intrinsics.checkNotNullExpressionValue(recipientId, "aciEntry.get()");
                    if (setPhoneNumber(recipientId, key)) {
                        Intrinsics.checkNotNull(value);
                        optional = getByServiceId(value);
                    }
                }
                RecipientId orInsertFromE164 = optional.isPresent() ? optional.get() : getOrInsertFromE164(key);
                Intrinsics.checkNotNullExpressionValue(orInsertFromE164, "if (aciEntry.isPresent) …getOrInsertFromE164(e164)");
                linkedHashMap.put(orInsertFromE164, value);
            }
            writableDatabase.setTransactionSuccessful();
            return linkedHashMap;
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public final Set<RecipientId> bulkProcessCdsV2Result(Map<String, CdsV2Result> map) {
        Intrinsics.checkNotNullParameter(map, "mapping");
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (Map.Entry<String, CdsV2Result> entry : map.entrySet()) {
                CdsV2Result value = entry.getValue();
                linkedHashSet.add(processPnpTuple(entry.getKey(), value.getPni(), value.getAci(), false));
            }
            writableDatabase.setTransactionSuccessful();
            return linkedHashSet;
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public final RecipientId processPnpTuple(String str, PNI pni, ACI aci, boolean z) {
        return writePnpChangeSetToDisk(processPnpTupleToChangeSet(str, pni, aci, z));
    }

    public final RecipientId writePnpChangeSetToDisk(PnpChangeSet pnpChangeSet) {
        Iterator<PnpOperation> it;
        Intrinsics.checkNotNullParameter(pnpChangeSet, "changeSet");
        Iterator<PnpOperation> it2 = pnpChangeSet.getOperations().iterator();
        while (it2.hasNext()) {
            PnpOperation next = it2.next();
            if (next instanceof PnpOperation.Update) {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
                UpdateBuilderPart1 update = SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME);
                Pair<String, ? extends Object>[] pairArr = new Pair[3];
                PnpOperation.Update update2 = (PnpOperation.Update) next;
                pairArr[0] = TuplesKt.to(PHONE, update2.getE164());
                Object aci = update2.getAci();
                if (aci == null) {
                    aci = update2.getPni();
                }
                pairArr[1] = TuplesKt.to(SERVICE_ID, String.valueOf(aci));
                pairArr[2] = TuplesKt.to(PNI_COLUMN, String.valueOf(update2.getPni()));
                UpdateBuilderPart3.run$default(update.values(pairArr).where("_id = ?", update2.getRecipientId()), 0, 1, null);
            } else if (next instanceof PnpOperation.RemoveE164) {
                SQLiteDatabase writableDatabase2 = getWritableDatabase();
                Intrinsics.checkNotNullExpressionValue(writableDatabase2, "writableDatabase");
                UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase2, TABLE_NAME).values(TuplesKt.to(PHONE, null)).where("_id = ?", ((PnpOperation.RemoveE164) next).getRecipientId()), 0, 1, null);
            } else if (next instanceof PnpOperation.RemovePni) {
                SQLiteDatabase writableDatabase3 = getWritableDatabase();
                Intrinsics.checkNotNullExpressionValue(writableDatabase3, "writableDatabase");
                PnpOperation.RemovePni removePni = (PnpOperation.RemovePni) next;
                UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase3, TABLE_NAME).values(TuplesKt.to(SERVICE_ID, null)).where("_id = ? AND uuid NOT NULL AND uuid = pni", removePni.getRecipientId()), 0, 1, null);
                SQLiteDatabase writableDatabase4 = getWritableDatabase();
                Intrinsics.checkNotNullExpressionValue(writableDatabase4, "writableDatabase");
                UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase4, TABLE_NAME).values(TuplesKt.to(PNI_COLUMN, null)).where("_id = ?", removePni.getRecipientId()), 0, 1, null);
            } else if (next instanceof PnpOperation.SetAci) {
                SQLiteDatabase writableDatabase5 = getWritableDatabase();
                Intrinsics.checkNotNullExpressionValue(writableDatabase5, "writableDatabase");
                PnpOperation.SetAci setAci = (PnpOperation.SetAci) next;
                UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase5, TABLE_NAME).values(TuplesKt.to(SERVICE_ID, setAci.getAci().toString()), TuplesKt.to(REGISTERED, Integer.valueOf(RegisteredState.REGISTERED.getId()))).where("_id = ?", setAci.getRecipientId()), 0, 1, null);
            } else if (next instanceof PnpOperation.SetE164) {
                SQLiteDatabase writableDatabase6 = getWritableDatabase();
                Intrinsics.checkNotNullExpressionValue(writableDatabase6, "writableDatabase");
                PnpOperation.SetE164 setE164 = (PnpOperation.SetE164) next;
                UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase6, TABLE_NAME).values(TuplesKt.to(PHONE, setE164.getE164())).where("_id = ?", setE164.getRecipientId()), 0, 1, null);
            } else if (next instanceof PnpOperation.SetPni) {
                SQLiteDatabase writableDatabase7 = getWritableDatabase();
                Intrinsics.checkNotNullExpressionValue(writableDatabase7, "writableDatabase");
                PnpOperation.SetPni setPni = (PnpOperation.SetPni) next;
                UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase7, TABLE_NAME).values(TuplesKt.to(SERVICE_ID, setPni.getPni().toString())).where("_id = ? AND (uuid IS NULL OR uuid = pni)", setPni.getRecipientId()), 0, 1, null);
                SQLiteDatabase writableDatabase8 = getWritableDatabase();
                Intrinsics.checkNotNullExpressionValue(writableDatabase8, "writableDatabase");
                UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(writableDatabase8, TABLE_NAME).values(TuplesKt.to(PNI_COLUMN, setPni.getPni().toString()), TuplesKt.to(REGISTERED, Integer.valueOf(RegisteredState.REGISTERED.getId()))).where("_id = ?", setPni.getRecipientId()), 0, 1, null);
            } else {
                if (next instanceof PnpOperation.Merge) {
                    Log.w(TAG, "WARNING: Performing a PNP merge! This operation currently only has a basic implementation only suitable for basic testing!");
                    PnpOperation.Merge merge = (PnpOperation.Merge) next;
                    RecipientRecord record = getRecord(merge.getPrimaryId());
                    RecipientRecord record2 = getRecord(merge.getSecondaryId());
                    SQLiteDatabase writableDatabase9 = getWritableDatabase();
                    Intrinsics.checkNotNullExpressionValue(writableDatabase9, "writableDatabase");
                    it = it2;
                    SQLiteDatabaseExtensionsKt.delete(writableDatabase9, TABLE_NAME).where("_id = ?", merge.getSecondaryId()).run();
                    SQLiteDatabase writableDatabase10 = getWritableDatabase();
                    Intrinsics.checkNotNullExpressionValue(writableDatabase10, "writableDatabase");
                    UpdateBuilderPart1 update3 = SQLiteDatabaseExtensionsKt.update(writableDatabase10, TABLE_NAME);
                    Pair<String, ? extends Object>[] pairArr2 = new Pair[4];
                    String e164 = record.getE164();
                    if (e164 == null) {
                        e164 = record2.getE164();
                    }
                    pairArr2[0] = TuplesKt.to(PHONE, e164);
                    PNI pni = record.getPni();
                    if (pni == null) {
                        pni = record2.getPni();
                    }
                    pairArr2[1] = TuplesKt.to(PNI_COLUMN, pni != null ? pni.toString() : null);
                    ServiceId serviceId = record.getServiceId();
                    if (serviceId == null) {
                        serviceId = record2.getServiceId();
                    }
                    pairArr2[2] = TuplesKt.to(SERVICE_ID, serviceId != null ? serviceId.toString() : null);
                    pairArr2[3] = TuplesKt.to(REGISTERED, Integer.valueOf(RegisteredState.REGISTERED.getId()));
                    UpdateBuilderPart3.run$default(update3.values(pairArr2).where("_id = ?", merge.getPrimaryId()), 0, 1, null);
                } else {
                    it = it2;
                    if (next instanceof PnpOperation.SessionSwitchoverInsert) {
                        Log.w(TAG, "Session switchover events aren't implemented yet!");
                    } else if (next instanceof PnpOperation.ChangeNumberInsert) {
                        Log.w(TAG, "Change number inserts aren't implemented yet!");
                    }
                }
                it2 = it;
            }
        }
        PnpIdResolver id = pnpChangeSet.getId();
        if (id instanceof PnpIdResolver.PnpNoopId) {
            return ((PnpIdResolver.PnpNoopId) pnpChangeSet.getId()).getRecipientId();
        }
        if (id instanceof PnpIdResolver.PnpInsert) {
            RecipientId from = RecipientId.from(getWritableDatabase().insert(TABLE_NAME, (String) null, buildContentValuesForPnpInsert(((PnpIdResolver.PnpInsert) pnpChangeSet.getId()).getE164(), ((PnpIdResolver.PnpInsert) pnpChangeSet.getId()).getPni(), ((PnpIdResolver.PnpInsert) pnpChangeSet.getId()).getAci())));
            Intrinsics.checkNotNullExpressionValue(from, "{\n        val id: Long =…ipientId.from(id)\n      }");
            return from;
        }
        throw new NoWhenBranchMatchedException();
    }

    public final PnpChangeSet processPnpTupleToChangeSet(String str, PNI pni, ACI aci, boolean z) {
        List<RecipientId> list;
        boolean z2;
        boolean z3;
        ServiceId serviceId;
        ACI aci2;
        PNI pni2;
        boolean z4;
        Preconditions.checkArgument((str == null && pni == null && aci == null) ? false : true, "Must provide at least one field!");
        Preconditions.checkArgument(pni == null || str != null, "If a PNI is provided, you must also provide an E164!");
        PnpDataSet pnpDataSet = new PnpDataSet(str, pni, aci, str != null ? getByE164(str).orElse(null) : null, pni != null ? getByServiceId(pni).orElse(null) : null, pni != null ? getByPni(pni).orElse(null) : null, aci != null ? getByServiceId(aci).orElse(null) : null, null, null, null, 896, null);
        if (aci != null) {
            list = CollectionsKt__CollectionsKt.listOf((Object[]) new RecipientId[]{pnpDataSet.getByE164(), pnpDataSet.getByAciSid(), pnpDataSet.getByPniOnly()});
        } else {
            list = CollectionsKt__CollectionsKt.listOf((Object[]) new RecipientId[]{pnpDataSet.getByE164(), pnpDataSet.getByPniSid(), pnpDataSet.getByPniOnly()});
        }
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (RecipientId recipientId : list) {
                if (recipientId != null) {
                    z4 = true;
                    continue;
                } else {
                    z4 = false;
                    continue;
                }
                if (!z4) {
                    z2 = false;
                    break;
                }
            }
        }
        z2 = true;
        if (pnpDataSet.getCommonId() != null && z2) {
            return new PnpChangeSet(new PnpIdResolver.PnpNoopId(pnpDataSet.getCommonId()), null, 2, null);
        }
        if (pnpDataSet.getCommonId() != null && !z2) {
            RecipientRecord record = getRecord(pnpDataSet.getCommonId());
            ArrayList arrayList = new ArrayList();
            if (aci == null || Intrinsics.areEqual(aci, record.getServiceId()) || record.getServiceId() == null || record.sidIsPni()) {
                if (str != null && !Intrinsics.areEqual(record.getE164(), str)) {
                    arrayList.add(new PnpOperation.SetE164(pnpDataSet.getCommonId(), str));
                }
                if (pni != null && !Intrinsics.areEqual(record.getPni(), pni)) {
                    arrayList.add(new PnpOperation.SetPni(pnpDataSet.getCommonId(), pni));
                }
                if (aci != null && !Intrinsics.areEqual(record.getServiceId(), aci)) {
                    arrayList.add(new PnpOperation.SetAci(pnpDataSet.getCommonId(), aci));
                }
                if (!(str == null || record.getE164() == null || Intrinsics.areEqual(record.getE164(), str))) {
                    arrayList.add(new PnpOperation.ChangeNumberInsert(pnpDataSet.getCommonId(), record.getE164(), str));
                }
                if (aci != null) {
                    z3 = z;
                    serviceId = aci;
                } else if (pni != null) {
                    z3 = z;
                    serviceId = pni;
                } else {
                    serviceId = record.getServiceId();
                    z3 = z;
                }
                if (!z3 && record.getServiceId() != null && !Intrinsics.areEqual(record.getServiceId(), serviceId)) {
                    SessionDatabase sessions = SignalDatabase.Companion.sessions();
                    String serviceId2 = record.getServiceId().toString();
                    Intrinsics.checkNotNullExpressionValue(serviceId2, "record.serviceId.toString()");
                    if (sessions.hasAnySessionFor(serviceId2)) {
                        arrayList.add(new PnpOperation.SessionSwitchoverInsert(pnpDataSet.getCommonId()));
                    }
                }
                return new PnpChangeSet(new PnpIdResolver.PnpNoopId(pnpDataSet.getCommonId()), arrayList);
            }
            if (Intrinsics.areEqual(record.getE164(), str)) {
                arrayList.add(new PnpOperation.RemoveE164(record.getId()));
                arrayList.add(new PnpOperation.RemovePni(record.getId()));
                pni2 = pni;
                aci2 = aci;
            } else {
                pni2 = pni;
                aci2 = aci;
                if (Intrinsics.areEqual(record.getPni(), pni2)) {
                    arrayList.add(new PnpOperation.RemovePni(record.getId()));
                }
            }
            return new PnpChangeSet(new PnpIdResolver.PnpInsert(str, pni2, aci2), arrayList);
        } else if (pnpDataSet.getByE164() == null && pnpDataSet.getByPniSid() == null && pnpDataSet.getByAciSid() == null) {
            return new PnpChangeSet(new PnpIdResolver.PnpInsert(str, pni, aci), null, 2, null);
        } else {
            RecipientId byE164 = pnpDataSet.getByE164();
            RecipientRecord record2 = byE164 != null ? getRecord(byE164) : null;
            RecipientId byPniSid = pnpDataSet.getByPniSid();
            RecipientRecord record3 = byPniSid != null ? getRecord(byPniSid) : null;
            RecipientId byAciSid = pnpDataSet.getByAciSid();
            PnpDataSet pnpDataSet2 = pnpDataSet.copy((r22 & 1) != 0 ? pnpDataSet.e164 : null, (r22 & 2) != 0 ? pnpDataSet.pni : null, (r22 & 4) != 0 ? pnpDataSet.aci : null, (r22 & 8) != 0 ? pnpDataSet.byE164 : null, (r22 & 16) != 0 ? pnpDataSet.byPniSid : null, (r22 & 32) != 0 ? pnpDataSet.byPniOnly : null, (r22 & 64) != 0 ? pnpDataSet.byAciSid : null, (r22 & 128) != 0 ? pnpDataSet.e164Record : record2, (r22 & 256) != 0 ? pnpDataSet.pniSidRecord : record3, (r22 & 512) != 0 ? pnpDataSet.aciSidRecord : byAciSid != null ? getRecord(byAciSid) : null);
            Preconditions.checkState(pnpDataSet2.getCommonId() == null);
            Preconditions.checkState(CollectionsKt__CollectionsKt.listOfNotNull((Object[]) new RecipientId[]{pnpDataSet2.getByE164(), pnpDataSet2.getByPniSid(), pnpDataSet2.getByPniOnly(), pnpDataSet2.getByAciSid()}).size() >= 2);
            ArrayList arrayList2 = new ArrayList();
            boolean unused = CollectionsKt__MutableCollectionsKt.addAll(arrayList2, processPossibleE164PniSidMerge(pni, z, pnpDataSet2));
            boolean unused2 = CollectionsKt__MutableCollectionsKt.addAll(arrayList2, processPossiblePniSidAciSidMerge(str, pni, aci, pnpDataSet2.perform(arrayList2)));
            boolean unused3 = CollectionsKt__MutableCollectionsKt.addAll(arrayList2, processPossibleE164AciSidMerge(str, pni, aci, pnpDataSet2.perform(arrayList2)));
            PnpDataSet perform = pnpDataSet2.perform(arrayList2);
            RecipientId recipientId2 = (RecipientId) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) CollectionsKt__CollectionsKt.listOfNotNull((Object[]) new RecipientId[]{perform.getByAciSid(), perform.getByE164(), perform.getByPniSid()})));
            if (perform.getByAciSid() == null && aci != null) {
                arrayList2.add(new PnpOperation.SetAci(recipientId2, aci));
            }
            if (perform.getByE164() == null && str != null) {
                arrayList2.add(new PnpOperation.SetE164(recipientId2, str));
            }
            if (perform.getByPniSid() == null && perform.getByPniOnly() == null && pni != null) {
                arrayList2.add(new PnpOperation.SetPni(recipientId2, pni));
            }
            return new PnpChangeSet(new PnpIdResolver.PnpNoopId(recipientId2), arrayList2);
        }
    }

    private final List<PnpOperation> processPossibleE164PniSidMerge(PNI pni, boolean z, PnpDataSet pnpDataSet) {
        if (pni == null || pnpDataSet.getByE164() == null || pnpDataSet.getByPniSid() == null || pnpDataSet.getE164Record() == null || pnpDataSet.getPniSidRecord() == null || Intrinsics.areEqual(pnpDataSet.getE164Record().getId(), pnpDataSet.getPniSidRecord().getId())) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        if (pnpDataSet.getPniSidRecord().sidOnly(pni)) {
            if (pnpDataSet.getE164Record().getPni() != null) {
                arrayList.add(new PnpOperation.RemovePni(pnpDataSet.getByE164()));
            }
            arrayList.add(new PnpOperation.Merge(pnpDataSet.getByE164(), pnpDataSet.getByPniSid()));
        } else {
            boolean z2 = true;
            Preconditions.checkState(!pnpDataSet.getPniSidRecord().pniAndAci());
            if (pnpDataSet.getPniSidRecord().getE164() == null) {
                z2 = false;
            }
            Preconditions.checkState(z2);
            arrayList.add(new PnpOperation.RemovePni(pnpDataSet.getByPniSid()));
            arrayList.add(new PnpOperation.SetPni(pnpDataSet.getByE164(), pni));
            if (!z && SignalDatabase.Companion.sessions().hasAnySessionFor(String.valueOf(pnpDataSet.getPniSidRecord().getServiceId()))) {
                arrayList.add(new PnpOperation.SessionSwitchoverInsert(pnpDataSet.getByPniSid()));
            }
            if (!z && pnpDataSet.getE164Record().getServiceId() != null && pnpDataSet.getE164Record().sidIsPni()) {
                SessionDatabase sessions = SignalDatabase.Companion.sessions();
                String serviceId = pnpDataSet.getE164Record().getServiceId().toString();
                Intrinsics.checkNotNullExpressionValue(serviceId, "data.e164Record.serviceId.toString()");
                if (sessions.hasAnySessionFor(serviceId)) {
                    arrayList.add(new PnpOperation.SessionSwitchoverInsert(pnpDataSet.getByE164()));
                }
            }
        }
        return arrayList;
    }

    private final List<PnpOperation> processPossiblePniSidAciSidMerge(String str, PNI pni, ACI aci, PnpDataSet pnpDataSet) {
        if (pni == null || aci == null || pnpDataSet.getByPniSid() == null || pnpDataSet.getByAciSid() == null || pnpDataSet.getPniSidRecord() == null || pnpDataSet.getAciSidRecord() == null || Intrinsics.areEqual(pnpDataSet.getPniSidRecord().getId(), pnpDataSet.getAciSidRecord().getId())) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        if (pnpDataSet.getPniSidRecord().sidOnly(pni)) {
            if (pnpDataSet.getAciSidRecord().getPni() != null) {
                arrayList.add(new PnpOperation.RemovePni(pnpDataSet.getByAciSid()));
            }
            arrayList.add(new PnpOperation.Merge(pnpDataSet.getByAciSid(), pnpDataSet.getByPniSid()));
        } else if (Intrinsics.areEqual(pnpDataSet.getPniSidRecord().getE164(), str)) {
            if (pnpDataSet.getAciSidRecord().getPni() != null) {
                arrayList.add(new PnpOperation.RemovePni(pnpDataSet.getByAciSid()));
            }
            if (pnpDataSet.getAciSidRecord().getE164() != null && !Intrinsics.areEqual(pnpDataSet.getAciSidRecord().getE164(), str)) {
                arrayList.add(new PnpOperation.RemoveE164(pnpDataSet.getByAciSid()));
            }
            arrayList.add(new PnpOperation.Merge(pnpDataSet.getByAciSid(), pnpDataSet.getByPniSid()));
            if (pnpDataSet.getAciSidRecord().getE164() != null && !Intrinsics.areEqual(pnpDataSet.getAciSidRecord().getE164(), str)) {
                RecipientId byAciSid = pnpDataSet.getByAciSid();
                String e164 = pnpDataSet.getAciSidRecord().getE164();
                Intrinsics.checkNotNull(str);
                arrayList.add(new PnpOperation.ChangeNumberInsert(byAciSid, e164, str));
            }
        } else {
            Preconditions.checkState(pnpDataSet.getPniSidRecord().getE164() != null && !Intrinsics.areEqual(pnpDataSet.getPniSidRecord().getE164(), str));
            arrayList.add(new PnpOperation.RemovePni(pnpDataSet.getByPniSid()));
            arrayList.add(new PnpOperation.Update(pnpDataSet.getByAciSid(), str, pni, ACI.from(pnpDataSet.getAciSidRecord().getServiceId())));
        }
        return arrayList;
    }

    private final List<PnpOperation> processPossibleE164AciSidMerge(String str, PNI pni, ACI aci, PnpDataSet pnpDataSet) {
        if (str == null || aci == null || pnpDataSet.getByE164() == null || pnpDataSet.getByAciSid() == null || pnpDataSet.getE164Record() == null || pnpDataSet.getAciSidRecord() == null || Intrinsics.areEqual(pnpDataSet.getE164Record().getId(), pnpDataSet.getAciSidRecord().getId())) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        if (pnpDataSet.getE164Record().e164Only()) {
            if (pnpDataSet.getAciSidRecord().getE164() != null && !Intrinsics.areEqual(pnpDataSet.getAciSidRecord().getE164(), str)) {
                arrayList.add(new PnpOperation.RemoveE164(pnpDataSet.getByAciSid()));
            }
            arrayList.add(new PnpOperation.Merge(pnpDataSet.getByAciSid(), pnpDataSet.getByE164()));
            if (pnpDataSet.getAciSidRecord().getE164() != null && !Intrinsics.areEqual(pnpDataSet.getAciSidRecord().getE164(), str)) {
                arrayList.add(new PnpOperation.ChangeNumberInsert(pnpDataSet.getByAciSid(), pnpDataSet.getAciSidRecord().getE164(), str));
            }
        } else if (pnpDataSet.getE164Record().getPni() == null || !Intrinsics.areEqual(pnpDataSet.getE164Record().getPni(), pni)) {
            arrayList.add(new PnpOperation.RemoveE164(pnpDataSet.getByE164()));
            arrayList.add(new PnpOperation.SetE164(pnpDataSet.getByAciSid(), str));
            if (pnpDataSet.getAciSidRecord().getE164() != null && !Intrinsics.areEqual(pnpDataSet.getAciSidRecord().getE164(), str)) {
                arrayList.add(new PnpOperation.ChangeNumberInsert(pnpDataSet.getByAciSid(), pnpDataSet.getAciSidRecord().getE164(), str));
            }
        } else {
            if (pnpDataSet.getAciSidRecord().getPni() != null) {
                arrayList.add(new PnpOperation.RemovePni(pnpDataSet.getByAciSid()));
            }
            if (pnpDataSet.getAciSidRecord().getE164() != null && !Intrinsics.areEqual(pnpDataSet.getAciSidRecord().getE164(), str)) {
                arrayList.add(new PnpOperation.RemoveE164(pnpDataSet.getByAciSid()));
            }
            arrayList.add(new PnpOperation.Merge(pnpDataSet.getByAciSid(), pnpDataSet.getByE164()));
            if (pnpDataSet.getAciSidRecord().getE164() != null && !Intrinsics.areEqual(pnpDataSet.getAciSidRecord().getE164(), str)) {
                arrayList.add(new PnpOperation.ChangeNumberInsert(pnpDataSet.getByAciSid(), pnpDataSet.getAciSidRecord().getE164(), str));
            }
        }
        return arrayList;
    }

    public final List<RecipientId> getUninvitedRecipientsForInsights() {
        LinkedList linkedList = new LinkedList();
        Cursor rawQuery = getReadableDatabase().rawQuery(INSIGHTS_INVITEE_LIST, new String[]{String.valueOf(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(31))});
        while (rawQuery != null) {
            try {
                if (!rawQuery.moveToNext()) {
                    break;
                }
                RecipientId from = RecipientId.from(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_id")));
                Intrinsics.checkNotNullExpressionValue(from, "from(cursor.getLong(curs…tColumnIndexOrThrow(ID)))");
                linkedList.add(from);
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return linkedList;
    }

    public final List<RecipientId> getRegistered() {
        LinkedList linkedList = new LinkedList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, ID_PROJECTION, "registered = ?", new String[]{SubscriptionLevels.BOOST_LEVEL}, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                RecipientId from = RecipientId.from(query.getLong(query.getColumnIndexOrThrow("_id")));
                Intrinsics.checkNotNullExpressionValue(from, "from(cursor.getLong(curs…tColumnIndexOrThrow(ID)))");
                linkedList.add(from);
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return linkedList;
    }

    public final List<RecipientId> getSystemContacts() {
        LinkedList linkedList = new LinkedList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, ID_PROJECTION, "system_display_name IS NOT NULL AND system_display_name != \"\"", null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                RecipientId from = RecipientId.from(query.getLong(query.getColumnIndexOrThrow("_id")));
                Intrinsics.checkNotNullExpressionValue(from, "from(cursor.getLong(curs…tColumnIndexOrThrow(ID)))");
                linkedList.add(from);
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return linkedList;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v4, resolved type: T */
    /* JADX DEBUG: Multi-variable search result rejected for r5v9, resolved type: T */
    /* JADX DEBUG: Multi-variable search result rejected for r5v10, resolved type: T */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v6, types: [T, org.thoughtcrime.securesms.conversation.colors.ChatColors] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0065 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void updateSystemContactColors() {
        /*
        // Method dump skipped, instructions count: 426
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.RecipientDatabase.updateSystemContactColors():void");
    }

    public final Cursor getSignalContacts(boolean z) {
        return getSignalContacts(z, "sort_name, system_display_name, search_signal_profile, username, phone");
    }

    public final int getSignalContactsCount(boolean z) {
        Cursor signalContacts = getSignalContacts(z);
        if (signalContacts != null) {
            return signalContacts.getCount();
        }
        return 0;
    }

    public static /* synthetic */ Cursor getSignalContacts$default(RecipientDatabase recipientDatabase, boolean z, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                str = null;
            }
            return recipientDatabase.getSignalContacts(z, str);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getSignalContacts");
    }

    public final Cursor getSignalContacts(boolean z, String str) {
        RecipientId recipientId;
        ContactSearchSelection.Builder withGroups = new ContactSearchSelection.Builder().withRegistered(true).withGroups(false);
        if (z) {
            recipientId = null;
        } else {
            recipientId = Recipient.self().getId();
        }
        ContactSearchSelection build = withGroups.excludeId(recipientId).build();
        return getReadableDatabase().query(TABLE_NAME, SEARCH_PROJECTION, build.getWhere(), build.getArgs(), null, null, str);
    }

    public final Cursor querySignalContacts(String str, boolean z) {
        RecipientId recipientId;
        Intrinsics.checkNotNullParameter(str, "inputQuery");
        String buildCaseInsensitiveGlobPattern = SqlUtil.buildCaseInsensitiveGlobPattern(str);
        ContactSearchSelection.Builder withGroups = new ContactSearchSelection.Builder().withRegistered(true).withGroups(false);
        if (z) {
            recipientId = null;
        } else {
            recipientId = Recipient.self().getId();
        }
        ContactSearchSelection build = withGroups.excludeId(recipientId).withSearchQuery(buildCaseInsensitiveGlobPattern).build();
        return getReadableDatabase().query(TABLE_NAME, SEARCH_PROJECTION, build.getWhere(), build.getArgs(), null, null, "sort_name, system_display_name, search_signal_profile, phone");
    }

    public final Cursor getNonSignalContacts() {
        ContactSearchSelection build = new ContactSearchSelection.Builder().withNonRegistered(true).withGroups(false).build();
        return getReadableDatabase().query(TABLE_NAME, SEARCH_PROJECTION, build.getWhere(), build.getArgs(), null, null, "system_display_name, phone");
    }

    public final Cursor queryNonSignalContacts(String str) {
        Intrinsics.checkNotNullParameter(str, "inputQuery");
        ContactSearchSelection build = new ContactSearchSelection.Builder().withNonRegistered(true).withGroups(false).withSearchQuery(SqlUtil.buildCaseInsensitiveGlobPattern(str)).build();
        return getReadableDatabase().query(TABLE_NAME, SEARCH_PROJECTION, build.getWhere(), build.getArgs(), null, null, "system_display_name, phone");
    }

    public final Cursor getNonGroupContacts(boolean z) {
        RecipientId recipientId;
        ContactSearchSelection.Builder withGroups = new ContactSearchSelection.Builder().withRegistered(true).withNonRegistered(true).withGroups(false);
        if (z) {
            recipientId = null;
        } else {
            recipientId = Recipient.self().getId();
        }
        ContactSearchSelection build = withGroups.excludeId(recipientId).build();
        return getReadableDatabase().query(TABLE_NAME, SEARCH_PROJECTION, build.getWhere(), build.getArgs(), null, null, orderByPreferringAlphaOverNumeric(SORT_NAME) + ", phone");
    }

    public final Cursor queryNonGroupContacts(String str, boolean z) {
        RecipientId recipientId;
        Intrinsics.checkNotNullParameter(str, "inputQuery");
        String buildCaseInsensitiveGlobPattern = SqlUtil.buildCaseInsensitiveGlobPattern(str);
        ContactSearchSelection.Builder withGroups = new ContactSearchSelection.Builder().withRegistered(true).withNonRegistered(true).withGroups(false);
        if (z) {
            recipientId = null;
        } else {
            recipientId = Recipient.self().getId();
        }
        ContactSearchSelection build = withGroups.excludeId(recipientId).withSearchQuery(buildCaseInsensitiveGlobPattern).build();
        String where = build.getWhere();
        String[] args = build.getArgs();
        return getReadableDatabase().query(TABLE_NAME, SEARCH_PROJECTION, where, args, null, null, orderByPreferringAlphaOverNumeric(SORT_NAME) + ", phone");
    }

    public final Cursor queryAllContacts(String str) {
        Intrinsics.checkNotNullParameter(str, "inputQuery");
        String buildCaseInsensitiveGlobPattern = SqlUtil.buildCaseInsensitiveGlobPattern(str);
        return getReadableDatabase().query(TABLE_NAME, SEARCH_PROJECTION, "blocked = ? AND \n(\n  sort_name GLOB ? OR \n  username GLOB ? OR \n  phone GLOB ? OR \n  email GLOB ?\n)", SqlUtil.buildArgs("0", buildCaseInsensitiveGlobPattern, buildCaseInsensitiveGlobPattern, buildCaseInsensitiveGlobPattern, buildCaseInsensitiveGlobPattern), null, null, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.database.RecipientDatabase */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ List queryRecipientsForMentions$default(RecipientDatabase recipientDatabase, String str, List list, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                list = null;
            }
            return recipientDatabase.queryRecipientsForMentions(str, list);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: queryRecipientsForMentions");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r12v1, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r12v9, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0039, code lost:
        if (r12 == null) goto L_0x003b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<org.thoughtcrime.securesms.recipients.Recipient> queryRecipientsForMentions(java.lang.String r11, java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> r12) {
        /*
            r10 = this;
            java.lang.String r0 = "inputQuery"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r11, r0)
            java.lang.String r11 = org.signal.core.util.SqlUtil.buildCaseInsensitiveGlobPattern(r11)
            boolean r0 = org.thoughtcrime.securesms.util.Util.hasItems(r12)
            r1 = 0
            if (r0 == 0) goto L_0x0046
            if (r12 == 0) goto L_0x003b
            java.util.ArrayList r0 = new java.util.ArrayList
            r2 = 10
            int r2 = kotlin.collections.CollectionsKt.collectionSizeOrDefault(r12, r2)
            r0.<init>(r2)
            java.util.Iterator r12 = r12.iterator()
        L_0x0021:
            boolean r2 = r12.hasNext()
            if (r2 == 0) goto L_0x0035
            java.lang.Object r2 = r12.next()
            org.thoughtcrime.securesms.recipients.RecipientId r2 = (org.thoughtcrime.securesms.recipients.RecipientId) r2
            java.lang.String r2 = r2.serialize()
            r0.add(r2)
            goto L_0x0021
        L_0x0035:
            java.util.List r12 = kotlin.collections.CollectionsKt.toList(r0)
            if (r12 != 0) goto L_0x003f
        L_0x003b:
            java.util.List r12 = kotlin.collections.CollectionsKt.emptyList()
        L_0x003f:
            java.lang.String r0 = ","
            java.lang.String r12 = android.text.TextUtils.join(r0, r12)
            goto L_0x0047
        L_0x0046:
            r12 = r1
        L_0x0047:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "blocked = 0 AND "
            r0.append(r2)
            if (r12 == 0) goto L_0x006a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "_id IN ("
            r2.append(r3)
            r2.append(r12)
            java.lang.String r12 = ") AND "
            r2.append(r12)
            java.lang.String r12 = r2.toString()
            goto L_0x006c
        L_0x006a:
            java.lang.String r12 = ""
        L_0x006c:
            r0.append(r12)
            java.lang.String r12 = "sort_name GLOB ?"
            r0.append(r12)
            java.lang.String r5 = r0.toString()
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
            org.thoughtcrime.securesms.database.RecipientDatabase$RecipientReader r0 = new org.thoughtcrime.securesms.database.RecipientDatabase$RecipientReader
            org.thoughtcrime.securesms.database.SQLiteDatabase r2 = r10.getReadableDatabase()
            java.lang.String[] r4 = org.thoughtcrime.securesms.database.RecipientDatabase.MENTION_SEARCH_PROJECTION
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r6 = 0
            r3[r6] = r11
            java.lang.String[] r6 = org.signal.core.util.SqlUtil.buildArgs(r3)
            r7 = 0
            r8 = 0
            java.lang.String r3 = "recipient"
            java.lang.String r9 = "sort_name"
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)
            java.lang.String r2 = "readableDatabase.query(T…), null, null, SORT_NAME)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r11, r2)
            r0.<init>(r11)
            org.thoughtcrime.securesms.recipients.Recipient r11 = r0.getNext()     // Catch: all -> 0x00b9
        L_0x00a9:
            if (r11 == 0) goto L_0x00b3
            r12.add(r11)     // Catch: all -> 0x00b9
            org.thoughtcrime.securesms.recipients.Recipient r11 = r0.getNext()     // Catch: all -> 0x00b9
            goto L_0x00a9
        L_0x00b3:
            kotlin.Unit r11 = kotlin.Unit.INSTANCE     // Catch: all -> 0x00b9
            kotlin.io.CloseableKt.closeFinally(r0, r1)
            return r12
        L_0x00b9:
            r11 = move-exception
            throw r11     // Catch: all -> 0x00bb
        L_0x00bb:
            r12 = move-exception
            kotlin.io.CloseableKt.closeFinally(r0, r11)
            goto L_0x00c1
        L_0x00c0:
            throw r12
        L_0x00c1:
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.RecipientDatabase.queryRecipientsForMentions(java.lang.String, java.util.List):java.util.List");
    }

    public final List<Recipient> getRecipientsForMultiDeviceSync() {
        String serialize = Recipient.self().getId().serialize();
        Intrinsics.checkNotNullExpressionValue(serialize, "self().id.serialize()");
        String[] strArr = {String.valueOf(RegisteredState.REGISTERED.getId()), serialize};
        ArrayList arrayList = new ArrayList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, ID_PROJECTION, "registered = ? AND group_id IS NULL AND _id != ? AND (system_contact_uri NOT NULL OR _id IN (SELECT thread.thread_recipient_id FROM thread))", strArr, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                Recipient resolved = Recipient.resolved(RecipientId.from(query.getLong(query.getColumnIndexOrThrow("_id"))));
                Intrinsics.checkNotNullExpressionValue(resolved, "resolved(RecipientId.fro…ColumnIndexOrThrow(ID))))");
                arrayList.add(resolved);
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return arrayList;
    }

    /* JADX INFO: finally extract failed */
    public final List<RecipientId> getRecipientsForRoutineProfileFetch(long j, long j2, int i) {
        ThreadDatabase threads = SignalDatabase.Companion.threads();
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        ThreadDatabase.Reader readerFor = threads.readerFor(threads.getRecentPushConversationList(-1, false));
        try {
            ThreadRecord next = readerFor.getNext();
            while (next != null && next.getDate() > j) {
                Recipient resolved = Recipient.resolved(next.getRecipient().getId());
                Intrinsics.checkNotNullExpressionValue(resolved, "resolved(record.recipient.id)");
                if (resolved.isGroup()) {
                    List<RecipientId> participantIds = resolved.getParticipantIds();
                    Intrinsics.checkNotNullExpressionValue(participantIds, "recipient.participantIds");
                    linkedHashSet.addAll(participantIds);
                } else {
                    RecipientId id = resolved.getId();
                    Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
                    linkedHashSet.add(id);
                }
                next = readerFor.getNext();
            }
            Unit unit = Unit.INSTANCE;
            CloseableKt.closeFinally(readerFor, null);
            List<Recipient> resolvedList = Recipient.resolvedList(linkedHashSet);
            Intrinsics.checkNotNullExpressionValue(resolvedList, "resolvedList(recipientsWithinInteractionThreshold)");
            return SequencesKt___SequencesKt.toMutableList(SequencesKt___SequencesKt.map(SequencesKt___SequencesKt.take(SequencesKt___SequencesKt.filter(SequencesKt___SequencesKt.filterNot(CollectionsKt___CollectionsKt.asSequence(resolvedList), RecipientDatabase$getRecipientsForRoutineProfileFetch$2.INSTANCE), new Function1<Recipient, Boolean>(j2) { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$getRecipientsForRoutineProfileFetch$3
                final /* synthetic */ long $lastProfileFetchThreshold;

                /* access modifiers changed from: package-private */
                {
                    this.$lastProfileFetchThreshold = r1;
                }

                public final Boolean invoke(Recipient recipient) {
                    return Boolean.valueOf(recipient.getLastProfileFetchTime() < this.$lastProfileFetchThreshold);
                }
            }), i), RecipientDatabase$getRecipientsForRoutineProfileFetch$4.INSTANCE));
        } finally {
            try {
                throw th;
            } catch (Throwable th) {
            }
        }
    }

    public final void markProfilesFetched(Collection<? extends RecipientId> collection, long j) {
        Intrinsics.checkNotNullParameter(collection, "ids");
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues(1);
            contentValues.put(LAST_PROFILE_FETCH, Long.valueOf(j));
            Iterator<? extends RecipientId> it = collection.iterator();
            while (it.hasNext()) {
                writableDatabase.update(TABLE_NAME, contentValues, "_id = ?", new String[]{((RecipientId) it.next()).serialize()});
            }
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public final void updateStorageId(RecipientId recipientId, byte[] bArr) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Map<RecipientId, byte[]> singletonMap = Collections.singletonMap(recipientId, bArr);
        Intrinsics.checkNotNullExpressionValue(singletonMap, "singletonMap(recipientId, id)");
        updateStorageIds(singletonMap);
    }

    /* JADX INFO: finally extract failed */
    private final void updateStorageIds(Map<RecipientId, byte[]> map) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (Map.Entry<RecipientId, byte[]> entry : map.entrySet()) {
                byte[] value = entry.getValue();
                ContentValues contentValues = new ContentValues();
                Intrinsics.checkNotNull(value);
                contentValues.put(STORAGE_SERVICE_ID, Base64.encodeBytes(value));
                writableDatabase.update(TABLE_NAME, contentValues, "_id = ?", new String[]{entry.getKey().serialize()});
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            for (RecipientId recipientId : map.keySet()) {
                ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
            }
        } catch (Throwable th) {
            writableDatabase.endTransaction();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public final void markPreMessageRequestRecipientsAsProfileSharingEnabled(long j) {
        String[] buildArgs = SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(j));
        ArrayList<Number> arrayList = new ArrayList();
        Cursor rawQuery = getReadableDatabase().rawQuery("SELECT r._id FROM recipient AS r \nINNER JOIN thread AS t ON t.thread_recipient_id = r._id\nWHERE\n  r.profile_sharing = 0 AND (\n    EXISTS(SELECT 1 FROM sms WHERE thread_id = t._id AND date < ?) OR\n    EXISTS(SELECT 1 FROM mms WHERE thread_id = t._id AND date_received < ?)\n  )", buildArgs);
        while (rawQuery.moveToNext()) {
            try {
                Intrinsics.checkNotNullExpressionValue(rawQuery, "cursor");
                arrayList.add(Long.valueOf(CursorExtensionsKt.requireLong(rawQuery, "_id")));
            } catch (Throwable th) {
                try {
                    throw th;
                } catch (Throwable th2) {
                    CloseableKt.closeFinally(rawQuery, th);
                    throw th2;
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        CloseableKt.closeFinally(rawQuery, null);
        if (Util.hasItems(arrayList)) {
            SqlUtil.Query buildSingleCollectionQuery = SqlUtil.buildSingleCollectionQuery("_id", arrayList);
            ContentValues contentValues = new ContentValues(1);
            contentValues.put(PROFILE_SHARING, (Integer) 1);
            getWritableDatabase().update(TABLE_NAME, contentValues, buildSingleCollectionQuery.getWhere(), buildSingleCollectionQuery.getWhereArgs());
            for (Number number : arrayList) {
                ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(RecipientId.from(number.longValue()));
            }
        }
    }

    public final void setHasGroupsInCommon(List<? extends RecipientId> list) {
        Intrinsics.checkNotNullParameter(list, "recipientIds");
        if (!list.isEmpty()) {
            SqlUtil.Query buildSingleCollectionQuery = SqlUtil.buildSingleCollectionQuery("_id", list);
            SQLiteDatabase writableDatabase = getWritableDatabase();
            Cursor query = writableDatabase.query(TABLE_NAME, new String[]{"_id"}, buildSingleCollectionQuery.getWhere() + " AND groups_in_common = 0", buildSingleCollectionQuery.getWhereArgs(), null, null, null);
            th = null;
            try {
                ArrayList<Number> arrayList = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    Intrinsics.checkNotNullExpressionValue(query, "cursor");
                    arrayList.add(Long.valueOf(CursorExtensionsKt.requireLong(query, "_id")));
                }
                if (Util.hasItems(arrayList)) {
                    SqlUtil.Query buildSingleCollectionQuery2 = SqlUtil.buildSingleCollectionQuery("_id", arrayList);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(GROUPS_IN_COMMON, (Integer) 1);
                    if (writableDatabase.update(TABLE_NAME, contentValues, buildSingleCollectionQuery2.getWhere(), buildSingleCollectionQuery2.getWhereArgs()) > 0) {
                        for (Number number : arrayList) {
                            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(RecipientId.from(number.longValue()));
                        }
                    }
                }
                Unit unit = Unit.INSTANCE;
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
    }

    /* renamed from: manuallyShowAvatar$lambda-115 */
    public static final RecipientExtras.Builder m1702manuallyShowAvatar$lambda115(RecipientExtras.Builder builder) {
        Intrinsics.checkNotNullParameter(builder, "b");
        return builder.setManuallyShownAvatar(true);
    }

    public final void manuallyShowAvatar(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        updateExtras(recipientId, new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda15
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1702manuallyShowAvatar$lambda115((RecipientExtras.Builder) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    /* JADX INFO: finally extract failed */
    private final void updateExtras(RecipientId recipientId, Function<RecipientExtras.Builder, RecipientExtras.Builder> function) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            Cursor query = writableDatabase.query(TABLE_NAME, new String[]{"_id", EXTRAS}, "_id = ?", SqlUtil.buildArgs(recipientId), null, null, null);
            if (query.moveToNext()) {
                Intrinsics.checkNotNullExpressionValue(query, "cursor");
                RecipientExtras recipientExtras = getRecipientExtras(query);
                byte[] byteArray = function.apply(recipientExtras != null ? recipientExtras.toBuilder() : RecipientExtras.newBuilder()).build().toByteArray();
                ContentValues contentValues = new ContentValues(1);
                contentValues.put(EXTRAS, byteArray);
                writableDatabase.update(TABLE_NAME, contentValues, "_id = ?", SqlUtil.buildArgs(CursorExtensionsKt.requireLong(query, "_id")));
            }
            Unit unit = Unit.INSTANCE;
            CloseableKt.closeFinally(query, null);
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
        } catch (Throwable th) {
            writableDatabase.endTransaction();
            throw th;
        }
    }

    public final void rotateStorageId(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(STORAGE_SERVICE_ID, Base64.encodeBytes(StorageSyncHelper.generateKey()));
        getWritableDatabase().update(TABLE_NAME, contentValues, "_id = ? AND (group_type IN (?, ?, ?) OR registered = ?)", SqlUtil.buildArgs(recipientId, Integer.valueOf(GroupType.SIGNAL_V1.getId()), Integer.valueOf(GroupType.SIGNAL_V2.getId()), Integer.valueOf(GroupType.DISTRIBUTION_LIST.getId()), Integer.valueOf(RegisteredState.REGISTERED.getId())));
    }

    public final void setStorageIdIfNotSet(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(STORAGE_SERVICE_ID, Base64.encodeBytes(StorageSyncHelper.generateKey()));
        getWritableDatabase().update(TABLE_NAME, contentValues, "_id = ? AND storage_service_key IS NULL", SqlUtil.buildArgs(recipientId));
    }

    public final void updateGroupId(GroupId.V1 v1, GroupId.V2 v2) {
        Intrinsics.checkNotNullParameter(v1, "v1Id");
        Intrinsics.checkNotNullParameter(v2, "v2Id");
        ContentValues contentValues = new ContentValues();
        contentValues.put("group_id", v2.toString());
        contentValues.put(GROUP_TYPE, Integer.valueOf(GroupType.SIGNAL_V2.getId()));
        if (update(SqlUtil.buildTrueUpdateQuery("group_id = ?", SqlUtil.buildArgs(v1), contentValues), contentValues)) {
            RecipientId recipientId = getByGroupId(v2).get();
            Intrinsics.checkNotNullExpressionValue(recipientId, "getByGroupId(v2Id).get()");
            RecipientId recipientId2 = recipientId;
            rotateStorageId(recipientId2);
            ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId2);
        }
    }

    public final boolean update(RecipientId recipientId, ContentValues contentValues) {
        return update(SqlUtil.buildTrueUpdateQuery("_id = ?", SqlUtil.buildArgs(recipientId), contentValues), contentValues);
    }

    private final boolean update(SqlUtil.Query query, ContentValues contentValues) {
        return getWritableDatabase().update(TABLE_NAME, contentValues, query.getWhere(), query.getWhereArgs()) > 0;
    }

    private final Optional<RecipientId> getByColumn(String str, String str2) {
        Optional<RecipientId> of;
        Cursor query = getReadableDatabase().query(TABLE_NAME, ID_PROJECTION, str + " = ?", new String[]{str2}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    of = Optional.of(RecipientId.from(query.getLong(query.getColumnIndexOrThrow("_id"))));
                    Intrinsics.checkNotNullExpressionValue(of, "{\n        Optional.of(Re…dexOrThrow(ID))))\n      }");
                    th = null;
                    return of;
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        of = Optional.empty();
        Intrinsics.checkNotNullExpressionValue(of, "{\n        Optional.empty()\n      }");
        th = null;
        return of;
    }

    static /* synthetic */ GetOrInsertResult getOrInsertByColumn$default(RecipientDatabase recipientDatabase, String str, String str2, ContentValues contentValues, int i, Object obj) {
        if (obj == null) {
            if ((i & 4) != 0) {
                contentValues = ContentValuesKt.contentValuesOf(TuplesKt.to(str, str2));
            }
            return recipientDatabase.getOrInsertByColumn(str, str2, contentValues);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getOrInsertByColumn");
    }

    private final GetOrInsertResult getOrInsertByColumn(String str, String str2, ContentValues contentValues) {
        if (!TextUtils.isEmpty(str2)) {
            Optional<RecipientId> byColumn = getByColumn(str, str2);
            if (byColumn.isPresent()) {
                RecipientId recipientId = byColumn.get();
                Intrinsics.checkNotNullExpressionValue(recipientId, "existing.get()");
                return new GetOrInsertResult(recipientId, false);
            }
            long insert = getWritableDatabase().insert(TABLE_NAME, (String) null, contentValues);
            if (insert < 0) {
                Optional<RecipientId> byColumn2 = getByColumn(str, str2);
                if (byColumn2.isPresent()) {
                    RecipientId recipientId2 = byColumn2.get();
                    Intrinsics.checkNotNullExpressionValue(recipientId2, "existing.get()");
                    return new GetOrInsertResult(recipientId2, false);
                }
                throw new AssertionError("Failed to insert recipient!");
            }
            RecipientId from = RecipientId.from(insert);
            Intrinsics.checkNotNullExpressionValue(from, "from(id)");
            return new GetOrInsertResult(from, true);
        }
        throw new AssertionError(str + " cannot be empty.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x02cb  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x02d0  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x031c  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0321  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0338  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x034a  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x034f  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x03f8  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x03fd  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x049a  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x049f  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x04b7  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x04bd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final org.thoughtcrime.securesms.recipients.RecipientId merge(org.thoughtcrime.securesms.recipients.RecipientId r19, org.thoughtcrime.securesms.recipients.RecipientId r20) {
        /*
        // Method dump skipped, instructions count: 1238
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.RecipientDatabase.merge(org.thoughtcrime.securesms.recipients.RecipientId, org.thoughtcrime.securesms.recipients.RecipientId):org.thoughtcrime.securesms.recipients.RecipientId");
    }

    /* renamed from: merge$lambda-134$lambda-130 */
    public static final String m1703merge$lambda134$lambda130(Uri uri) {
        return String.valueOf(uri);
    }

    /* renamed from: merge$lambda-134$lambda-131 */
    public static final String m1704merge$lambda134$lambda131(Uri uri) {
        return String.valueOf(uri);
    }

    /* renamed from: merge$lambda-134$lambda-132 */
    public static final byte[] m1705merge$lambda134$lambda132(ChatColors chatColors) {
        Intrinsics.checkNotNull(chatColors);
        return chatColors.serialize().toByteArray();
    }

    /* renamed from: merge$lambda-134$lambda-133 */
    public static final Long m1706merge$lambda134$lambda133(ChatColors chatColors) {
        Intrinsics.checkNotNull(chatColors);
        return Long.valueOf(chatColors.getId().getLongValue());
    }

    private final void ensureInTransaction() {
        if (!getWritableDatabase().inTransaction()) {
            throw new IllegalStateException("Must be in a transaction!".toString());
        }
    }

    private final ContentValues buildContentValuesForNewUser(String str, ServiceId serviceId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PHONE, str);
        if (serviceId != null) {
            String serviceId2 = serviceId.toString();
            Intrinsics.checkNotNullExpressionValue(serviceId2, "serviceId.toString()");
            String lowerCase = serviceId2.toLowerCase(Locale.ROOT);
            Intrinsics.checkNotNullExpressionValue(lowerCase, "this as java.lang.String).toLowerCase(Locale.ROOT)");
            contentValues.put(SERVICE_ID, lowerCase);
            contentValues.put(REGISTERED, Integer.valueOf(RegisteredState.REGISTERED.getId()));
            contentValues.put(STORAGE_SERVICE_ID, Base64.encodeBytes(StorageSyncHelper.generateKey()));
            contentValues.put("color", AvatarColor.random().serialize());
        }
        return contentValues;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: org.whispersystems.signalservice.api.push.ACI */
    /* JADX WARN: Multi-variable type inference failed */
    private final ContentValues buildContentValuesForPnpInsert(String str, PNI pni, ACI aci) {
        if ((str == null && pni == null && aci == 0) ? false : true) {
            Pair[] pairArr = new Pair[5];
            pairArr[0] = TuplesKt.to(PHONE, str);
            PNI pni2 = aci != 0 ? aci : pni;
            String str2 = null;
            pairArr[1] = TuplesKt.to(SERVICE_ID, pni2 != null ? pni2.toString() : null);
            if (pni != null) {
                str2 = pni.toString();
            }
            pairArr[2] = TuplesKt.to(PNI_COLUMN, str2);
            pairArr[3] = TuplesKt.to(STORAGE_SERVICE_ID, Base64.encodeBytes(StorageSyncHelper.generateKey()));
            pairArr[4] = TuplesKt.to("color", AvatarColor.random().serialize());
            ContentValues contentValuesOf = ContentValuesKt.contentValuesOf(pairArr);
            if (!(pni == null && aci == 0)) {
                contentValuesOf.put(REGISTERED, Integer.valueOf(RegisteredState.REGISTERED.getId()));
            }
            return contentValuesOf;
        }
        throw new IllegalStateException("Must provide some sort of identifier!".toString());
    }

    private final ContentValues getValuesForStorageContact(SignalContactRecord signalContactRecord, boolean z) {
        ContentValues contentValues = new ContentValues();
        String str = null;
        ProfileName fromParts = ProfileName.fromParts(signalContactRecord.getGivenName().orElse(null), signalContactRecord.getFamilyName().orElse(null));
        Intrinsics.checkNotNullExpressionValue(fromParts, "fromParts(contact.givenN….familyName.orElse(null))");
        String orElse = signalContactRecord.getUsername().orElse(null);
        if (signalContactRecord.getAddress().hasValidServiceId()) {
            contentValues.put(SERVICE_ID, signalContactRecord.getAddress().getServiceId().toString());
        }
        contentValues.put(PHONE, signalContactRecord.getAddress().getNumber().orElse(null));
        contentValues.put(PROFILE_GIVEN_NAME, fromParts.getGivenName());
        contentValues.put(PROFILE_FAMILY_NAME, fromParts.getFamilyName());
        contentValues.put(PROFILE_JOINED_NAME, fromParts.toString());
        contentValues.put(PROFILE_KEY, (String) signalContactRecord.getProfileKey().map(new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda21
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1701getValuesForStorageContact$lambda138$lambda137((byte[]) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null));
        if (!TextUtils.isEmpty(orElse)) {
            str = orElse;
        }
        contentValues.put(USERNAME, str);
        boolean isProfileSharingEnabled = signalContactRecord.isProfileSharingEnabled();
        String str2 = SubscriptionLevels.BOOST_LEVEL;
        contentValues.put(PROFILE_SHARING, isProfileSharingEnabled ? str2 : "0");
        if (!signalContactRecord.isBlocked()) {
            str2 = "0";
        }
        contentValues.put(BLOCKED, str2);
        contentValues.put(MUTE_UNTIL, Long.valueOf(signalContactRecord.getMuteUntil()));
        contentValues.put(STORAGE_SERVICE_ID, Base64.encodeBytes(signalContactRecord.getId().getRaw()));
        if (signalContactRecord.hasUnknownFields()) {
            byte[] serializeUnknownFields = signalContactRecord.serializeUnknownFields();
            Objects.requireNonNull(serializeUnknownFields);
            contentValues.put(STORAGE_PROTO, Base64.encodeBytes(serializeUnknownFields));
        } else {
            contentValues.putNull(STORAGE_PROTO);
        }
        if (z) {
            contentValues.put("color", AvatarColor.random().serialize());
        }
        return contentValues;
    }

    /* renamed from: getValuesForStorageContact$lambda-138$lambda-137 */
    public static final String m1701getValuesForStorageContact$lambda138$lambda137(byte[] bArr) {
        return Base64.encodeBytes(bArr);
    }

    private final ContentValues getValuesForStorageGroupV1(SignalGroupV1Record signalGroupV1Record, boolean z) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("group_id", GroupId.v1orThrow(signalGroupV1Record.getGroupId()).toString());
        contentValues.put(GROUP_TYPE, Integer.valueOf(GroupType.SIGNAL_V1.getId()));
        boolean isProfileSharingEnabled = signalGroupV1Record.isProfileSharingEnabled();
        String str = SubscriptionLevels.BOOST_LEVEL;
        contentValues.put(PROFILE_SHARING, isProfileSharingEnabled ? str : "0");
        if (!signalGroupV1Record.isBlocked()) {
            str = "0";
        }
        contentValues.put(BLOCKED, str);
        contentValues.put(MUTE_UNTIL, Long.valueOf(signalGroupV1Record.getMuteUntil()));
        contentValues.put(STORAGE_SERVICE_ID, Base64.encodeBytes(signalGroupV1Record.getId().getRaw()));
        if (signalGroupV1Record.hasUnknownFields()) {
            contentValues.put(STORAGE_PROTO, Base64.encodeBytes(signalGroupV1Record.serializeUnknownFields()));
        } else {
            contentValues.putNull(STORAGE_PROTO);
        }
        if (z) {
            contentValues.put("color", AvatarColor.random().serialize());
        }
        return contentValues;
    }

    private final ContentValues getValuesForStorageGroupV2(SignalGroupV2Record signalGroupV2Record, boolean z) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("group_id", GroupId.v2(signalGroupV2Record.getMasterKeyOrThrow()).toString());
        contentValues.put(GROUP_TYPE, Integer.valueOf(GroupType.SIGNAL_V2.getId()));
        boolean isProfileSharingEnabled = signalGroupV2Record.isProfileSharingEnabled();
        String str = SubscriptionLevels.BOOST_LEVEL;
        contentValues.put(PROFILE_SHARING, isProfileSharingEnabled ? str : "0");
        if (!signalGroupV2Record.isBlocked()) {
            str = "0";
        }
        contentValues.put(BLOCKED, str);
        contentValues.put(MUTE_UNTIL, Long.valueOf(signalGroupV2Record.getMuteUntil()));
        contentValues.put(STORAGE_SERVICE_ID, Base64.encodeBytes(signalGroupV2Record.getId().getRaw()));
        contentValues.put(MENTION_SETTING, Integer.valueOf((signalGroupV2Record.notifyForMentionsWhenMuted() ? MentionSetting.ALWAYS_NOTIFY : MentionSetting.DO_NOT_NOTIFY).getId()));
        if (signalGroupV2Record.hasUnknownFields()) {
            contentValues.put(STORAGE_PROTO, Base64.encodeBytes(signalGroupV2Record.serializeUnknownFields()));
        } else {
            contentValues.putNull(STORAGE_PROTO);
        }
        if (z) {
            contentValues.put("color", AvatarColor.random().serialize());
        }
        return contentValues;
    }

    public static /* synthetic */ void debugClearServiceIds$default(RecipientDatabase recipientDatabase, RecipientId recipientId, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                recipientId = null;
            }
            recipientDatabase.debugClearServiceIds(recipientId);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: debugClearServiceIds");
    }

    public final void debugClearServiceIds(RecipientId recipientId) {
        UpdateBuilderPart3 updateBuilderPart3;
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        UpdateBuilderPart2 values = SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME).values(TuplesKt.to(SERVICE_ID, null), TuplesKt.to(PNI_COLUMN, null));
        if (recipientId == null) {
            RecipientId id = Recipient.self().getId();
            Intrinsics.checkNotNullExpressionValue(id, "self().id");
            updateBuilderPart3 = values.where("_id != ?", id);
        } else {
            updateBuilderPart3 = values.where("_id = ?", recipientId);
        }
        UpdateBuilderPart3.run$default(updateBuilderPart3, 0, 1, null);
        ApplicationDependencies.getRecipientCache().clear();
        RecipientId.clearCache();
    }

    public static /* synthetic */ void debugClearProfileData$default(RecipientDatabase recipientDatabase, RecipientId recipientId, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                recipientId = null;
            }
            recipientDatabase.debugClearProfileData(recipientId);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: debugClearProfileData");
    }

    public final void debugClearProfileData(RecipientId recipientId) {
        UpdateBuilderPart3 updateBuilderPart3;
        SQLiteDatabase writableDatabase = getWritableDatabase();
        Intrinsics.checkNotNullExpressionValue(writableDatabase, "writableDatabase");
        UpdateBuilderPart2 values = SQLiteDatabaseExtensionsKt.update(writableDatabase, TABLE_NAME).values(TuplesKt.to(PROFILE_KEY, null), TuplesKt.to(EXPIRING_PROFILE_KEY_CREDENTIAL, null), TuplesKt.to(PROFILE_GIVEN_NAME, null), TuplesKt.to(PROFILE_FAMILY_NAME, null), TuplesKt.to(PROFILE_JOINED_NAME, null), TuplesKt.to(LAST_PROFILE_FETCH, 0), TuplesKt.to(SIGNAL_PROFILE_AVATAR, null));
        if (recipientId == null) {
            RecipientId id = Recipient.self().getId();
            Intrinsics.checkNotNullExpressionValue(id, "self().id");
            updateBuilderPart3 = values.where("_id != ?", id);
        } else {
            updateBuilderPart3 = values.where("_id = ?", recipientId);
        }
        UpdateBuilderPart3.run$default(updateBuilderPart3, 0, 1, null);
        ApplicationDependencies.getRecipientCache().clear();
        RecipientId.clearCache();
    }

    public final RecipientRecord getRecord(Context context, Cursor cursor) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(cursor, "cursor");
        return getRecord(context, cursor, "_id");
    }

    public final RecipientRecord getRecord(Context context, Cursor cursor, String str) {
        byte[] bArr;
        ExpiringProfileKeyCredentialColumnData parseFrom;
        ExpiringProfileKeyCredential expiringProfileKeyCredential;
        byte[] bArr2;
        ChatWallpaper chatWallpaper;
        ChatWallpaper chatWallpaper2;
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(cursor, "cursor");
        Intrinsics.checkNotNullParameter(str, "idColumnName");
        String requireString = CursorExtensionsKt.requireString(cursor, PROFILE_KEY);
        String requireString2 = CursorExtensionsKt.requireString(cursor, EXPIRING_PROFILE_KEY_CREDENTIAL);
        ChatColors chatColors = null;
        if (requireString != null) {
            try {
                bArr = Base64.decode(requireString);
            } catch (IOException e) {
                Log.w(TAG, e);
                bArr = null;
            }
            if (requireString2 != null) {
                try {
                    byte[] decode = Base64.decode(requireString2);
                    Intrinsics.checkNotNullExpressionValue(decode, "decode(expiringProfileKeyCredentialString)");
                    parseFrom = ExpiringProfileKeyCredentialColumnData.parseFrom(decode);
                } catch (IOException e2) {
                    Log.w(TAG, "Profile key credential column data could not be read", e2);
                } catch (InvalidInputException e3) {
                    Log.w(TAG, "Profile key credential column data could not be read", e3);
                }
                if (Arrays.equals(parseFrom.getProfileKey().toByteArray(), bArr)) {
                    expiringProfileKeyCredential = new ExpiringProfileKeyCredential(parseFrom.getExpiringProfileKeyCredential().toByteArray());
                    bArr2 = bArr;
                } else {
                    Log.i(TAG, "Out of date profile key credential data ignored on read");
                }
            }
            expiringProfileKeyCredential = null;
            bArr2 = bArr;
        } else {
            bArr2 = null;
            expiringProfileKeyCredential = null;
        }
        byte[] requireBlob = CursorExtensionsKt.requireBlob(cursor, WALLPAPER);
        if (requireBlob != null) {
            try {
                chatWallpaper = ChatWallpaperFactory.create(Wallpaper.parseFrom(requireBlob));
            } catch (InvalidProtocolBufferException e4) {
                Log.w(TAG, "Failed to parse wallpaper.", e4);
                chatWallpaper = null;
            }
            chatWallpaper2 = chatWallpaper;
        } else {
            chatWallpaper2 = null;
        }
        long requireLong = CursorExtensionsKt.requireLong(cursor, CUSTOM_CHAT_COLORS_ID);
        byte[] requireBlob2 = CursorExtensionsKt.requireBlob(cursor, CHAT_COLORS);
        if (requireBlob2 != null) {
            try {
                ChatColors.Companion companion = ChatColors.Companion;
                ChatColors.Id forLongValue = ChatColors.Id.Companion.forLongValue(requireLong);
                ChatColor parseFrom2 = ChatColor.parseFrom(requireBlob2);
                Intrinsics.checkNotNullExpressionValue(parseFrom2, "parseFrom(serializedChatColors)");
                chatColors = companion.forChatColor(forLongValue, parseFrom2);
            } catch (InvalidProtocolBufferException e5) {
                Log.w(TAG, "Failed to parse chat colors.", e5);
            }
        }
        RecipientId from = RecipientId.from(CursorExtensionsKt.requireLong(cursor, str));
        long requireLong2 = CursorExtensionsKt.requireLong(cursor, CAPABILITIES);
        Intrinsics.checkNotNullExpressionValue(from, "recipientId");
        ServiceId parseOrNull = ServiceId.parseOrNull(CursorExtensionsKt.requireString(cursor, SERVICE_ID));
        PNI parseOrNull2 = PNI.parseOrNull(CursorExtensionsKt.requireString(cursor, PNI_COLUMN));
        String requireString3 = CursorExtensionsKt.requireString(cursor, USERNAME);
        String requireString4 = CursorExtensionsKt.requireString(cursor, PHONE);
        String requireString5 = CursorExtensionsKt.requireString(cursor, EMAIL);
        GroupId parseNullableOrThrow = GroupId.parseNullableOrThrow(CursorExtensionsKt.requireString(cursor, "group_id"));
        DistributionListId fromNullable = DistributionListId.fromNullable(CursorExtensionsKt.requireLong(cursor, DISTRIBUTION_LIST_ID));
        GroupType fromId = GroupType.Companion.fromId(CursorExtensionsKt.requireInt(cursor, GROUP_TYPE));
        boolean requireBoolean = CursorExtensionsKt.requireBoolean(cursor, BLOCKED);
        long requireLong3 = CursorExtensionsKt.requireLong(cursor, MUTE_UNTIL);
        VibrateState.Companion companion2 = VibrateState.Companion;
        VibrateState fromId2 = companion2.fromId(CursorExtensionsKt.requireInt(cursor, MESSAGE_VIBRATE));
        VibrateState fromId3 = companion2.fromId(CursorExtensionsKt.requireInt(cursor, CALL_VIBRATE));
        Uri uri = Util.uri(CursorExtensionsKt.requireString(cursor, MESSAGE_RINGTONE));
        Uri uri2 = Util.uri(CursorExtensionsKt.requireString(cursor, CALL_RINGTONE));
        int requireInt = CursorExtensionsKt.requireInt(cursor, DEFAULT_SUBSCRIPTION_ID);
        int requireInt2 = CursorExtensionsKt.requireInt(cursor, MESSAGE_EXPIRATION_TIME);
        RegisteredState fromId4 = RegisteredState.Companion.fromId(CursorExtensionsKt.requireInt(cursor, REGISTERED));
        ProfileName fromParts = ProfileName.fromParts(CursorExtensionsKt.requireString(cursor, SYSTEM_GIVEN_NAME), CursorExtensionsKt.requireString(cursor, SYSTEM_FAMILY_NAME));
        Intrinsics.checkNotNullExpressionValue(fromParts, "fromParts(cursor.require…ring(SYSTEM_FAMILY_NAME))");
        String requireString6 = CursorExtensionsKt.requireString(cursor, SYSTEM_JOINED_NAME);
        String requireString7 = CursorExtensionsKt.requireString(cursor, SYSTEM_PHOTO_URI);
        String requireString8 = CursorExtensionsKt.requireString(cursor, SYSTEM_PHONE_LABEL);
        String requireString9 = CursorExtensionsKt.requireString(cursor, SYSTEM_CONTACT_URI);
        ProfileName fromParts2 = ProfileName.fromParts(CursorExtensionsKt.requireString(cursor, PROFILE_GIVEN_NAME), CursorExtensionsKt.requireString(cursor, PROFILE_FAMILY_NAME));
        Intrinsics.checkNotNullExpressionValue(fromParts2, "fromParts(cursor.require…ing(PROFILE_FAMILY_NAME))");
        String requireString10 = CursorExtensionsKt.requireString(cursor, SIGNAL_PROFILE_AVATAR);
        ProfileAvatarFileDetails avatarFileDetails = AvatarHelper.getAvatarFileDetails(context, from);
        Intrinsics.checkNotNullExpressionValue(avatarFileDetails, "getAvatarFileDetails(context, recipientId)");
        boolean requireBoolean2 = CursorExtensionsKt.requireBoolean(cursor, PROFILE_SHARING);
        long requireLong4 = CursorExtensionsKt.requireLong(cursor, LAST_PROFILE_FETCH);
        String requireString11 = CursorExtensionsKt.requireString(cursor, NOTIFICATION_CHANNEL);
        UnidentifiedAccessMode fromMode = UnidentifiedAccessMode.Companion.fromMode(CursorExtensionsKt.requireInt(cursor, UNIDENTIFIED_ACCESS_MODE));
        boolean requireBoolean3 = CursorExtensionsKt.requireBoolean(cursor, FORCE_SMS_SELECTION);
        Recipient.Capability deserialize = Recipient.Capability.deserialize((int) Bitmask.read(requireLong2, 1, 2));
        Intrinsics.checkNotNullExpressionValue(deserialize, "deserialize(Bitmask.read…ties.BIT_LENGTH).toInt())");
        Recipient.Capability deserialize2 = Recipient.Capability.deserialize((int) Bitmask.read(requireLong2, 2, 2));
        Intrinsics.checkNotNullExpressionValue(deserialize2, "deserialize(Bitmask.read…ties.BIT_LENGTH).toInt())");
        Recipient.Capability deserialize3 = Recipient.Capability.deserialize((int) Bitmask.read(requireLong2, 3, 2));
        Intrinsics.checkNotNullExpressionValue(deserialize3, "deserialize(Bitmask.read…ties.BIT_LENGTH).toInt())");
        Recipient.Capability deserialize4 = Recipient.Capability.deserialize((int) Bitmask.read(requireLong2, 4, 2));
        Intrinsics.checkNotNullExpressionValue(deserialize4, "deserialize(Bitmask.read…ties.BIT_LENGTH).toInt())");
        Recipient.Capability deserialize5 = Recipient.Capability.deserialize((int) Bitmask.read(requireLong2, 5, 2));
        Intrinsics.checkNotNullExpressionValue(deserialize5, "deserialize(Bitmask.read…ties.BIT_LENGTH).toInt())");
        Recipient.Capability deserialize6 = Recipient.Capability.deserialize((int) Bitmask.read(requireLong2, 6, 2));
        Intrinsics.checkNotNullExpressionValue(deserialize6, "deserialize(Bitmask.read…ties.BIT_LENGTH).toInt())");
        InsightsBannerTier fromId5 = InsightsBannerTier.Companion.fromId(CursorExtensionsKt.requireInt(cursor, SEEN_INVITE_REMINDER));
        byte[] decodeNullableOrThrow = Base64.decodeNullableOrThrow(CursorExtensionsKt.requireString(cursor, STORAGE_SERVICE_ID));
        MentionSetting fromId6 = MentionSetting.Companion.fromId(CursorExtensionsKt.requireInt(cursor, MENTION_SETTING));
        AvatarColor deserialize7 = AvatarColor.deserialize(CursorExtensionsKt.requireString(cursor, "color"));
        Intrinsics.checkNotNullExpressionValue(deserialize7, "deserialize(cursor.requireString(AVATAR_COLOR))");
        return new RecipientRecord(from, parseOrNull, parseOrNull2, requireString3, requireString4, requireString5, parseNullableOrThrow, fromNullable, fromId, requireBoolean, requireLong3, fromId2, fromId3, uri, uri2, requireInt, requireInt2, fromId4, bArr2, expiringProfileKeyCredential, fromParts, requireString6, requireString7, requireString8, requireString9, fromParts2, requireString10, avatarFileDetails, requireBoolean2, requireLong4, requireString11, fromMode, requireBoolean3, requireLong2, deserialize, deserialize2, deserialize3, deserialize4, deserialize5, deserialize6, fromId5, decodeNullableOrThrow, fromId6, chatWallpaper2, chatColors, deserialize7, CursorExtensionsKt.requireString(cursor, ABOUT), CursorExtensionsKt.requireString(cursor, ABOUT_EMOJI), getSyncExtras(cursor), getExtras(cursor), CursorExtensionsKt.requireBoolean(cursor, GROUPS_IN_COMMON), parseBadgeList(CursorExtensionsKt.requireBlob(cursor, BADGES)));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0010  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.util.List<org.thoughtcrime.securesms.badges.models.Badge> parseBadgeList(byte[] r4) {
        /*
            r3 = this;
            if (r4 == 0) goto L_0x000d
            org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList r4 = org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.parseFrom(r4)     // Catch: InvalidProtocolBufferException -> 0x0007
            goto L_0x000e
        L_0x0007:
            r4 = move-exception
            java.lang.String r0 = org.thoughtcrime.securesms.database.RecipientDatabase.TAG
            org.signal.core.util.logging.Log.w(r0, r4)
        L_0x000d:
            r4 = 0
        L_0x000e:
            if (r4 == 0) goto L_0x003b
            java.util.List r4 = r4.getBadgesList()
            java.util.ArrayList r0 = new java.util.ArrayList
            int r1 = r4.size()
            r0.<init>(r1)
            java.util.Iterator r4 = r4.iterator()
        L_0x0021:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x003f
            java.lang.Object r1 = r4.next()
            org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList$Badge r1 = (org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList.Badge) r1
            java.lang.String r2 = "protoBadge"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
            org.thoughtcrime.securesms.badges.models.Badge r1 = org.thoughtcrime.securesms.badges.Badges.fromDatabaseBadge(r1)
            r0.add(r1)
            goto L_0x0021
        L_0x003b:
            java.util.List r0 = kotlin.collections.CollectionsKt.emptyList()
        L_0x003f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.RecipientDatabase.parseBadgeList(byte[]):java.util.List");
    }

    private final RecipientRecord.SyncExtras getSyncExtras(Cursor cursor) {
        String orElse = CursorExtensionsKt.optionalString(cursor, STORAGE_PROTO).orElse(null);
        byte[] decodeOrThrow = orElse != null ? Base64.decodeOrThrow(orElse) : null;
        Optional<Boolean> optionalBoolean = CursorExtensionsKt.optionalBoolean(cursor, ThreadDatabase.ARCHIVED);
        Boolean bool = Boolean.FALSE;
        Boolean orElse2 = optionalBoolean.orElse(bool);
        Boolean bool2 = (Boolean) CursorExtensionsKt.optionalInt(cursor, "read").map(new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1697getSyncExtras$lambda143(((Integer) obj).intValue());
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(bool);
        IdentityDatabase.VerifiedStatus verifiedStatus = (IdentityDatabase.VerifiedStatus) CursorExtensionsKt.optionalInt(cursor, IDENTITY_STATUS).map(new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda7
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1700getSyncExtras$lambda146((Integer) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(IdentityDatabase.VerifiedStatus.DEFAULT);
        Intrinsics.checkNotNullExpressionValue(verifiedStatus, "identityStatus");
        Intrinsics.checkNotNullExpressionValue(orElse2, ThreadDatabase.ARCHIVED);
        boolean booleanValue = orElse2.booleanValue();
        Intrinsics.checkNotNullExpressionValue(bool2, "forcedUnread");
        return new RecipientRecord.SyncExtras(decodeOrThrow, (GroupMasterKey) CursorExtensionsKt.optionalBlob(cursor, GroupDatabase.V2_MASTER_KEY).map(new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda5
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1698getSyncExtras$lambda144((byte[]) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null), (byte[]) CursorExtensionsKt.optionalString(cursor, IDENTITY_KEY).map(new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda6
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1699getSyncExtras$lambda145((String) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null), verifiedStatus, booleanValue, bool2.booleanValue());
    }

    /* renamed from: getSyncExtras$lambda-143 */
    public static final Boolean m1697getSyncExtras$lambda143(int i) {
        return Boolean.valueOf(i == ThreadDatabase.ReadStatus.FORCED_UNREAD.serialize());
    }

    /* renamed from: getSyncExtras$lambda-144 */
    public static final GroupMasterKey m1698getSyncExtras$lambda144(byte[] bArr) {
        return GroupUtil.requireMasterKey(bArr);
    }

    /* renamed from: getSyncExtras$lambda-145 */
    public static final byte[] m1699getSyncExtras$lambda145(String str) {
        return Base64.decodeOrThrow(str);
    }

    /* renamed from: getSyncExtras$lambda-146 */
    public static final IdentityDatabase.VerifiedStatus m1700getSyncExtras$lambda146(Integer num) {
        Intrinsics.checkNotNullExpressionValue(num, "it");
        return IdentityDatabase.VerifiedStatus.forState(num.intValue());
    }

    private final Recipient.Extras getExtras(Cursor cursor) {
        return Recipient.Extras.from(getRecipientExtras(cursor));
    }

    private final RecipientExtras getRecipientExtras(Cursor cursor) {
        return (RecipientExtras) CursorExtensionsKt.optionalBlob(cursor, EXTRAS).map(new Function() { // from class: org.thoughtcrime.securesms.database.RecipientDatabase$$ExternalSyntheticLambda20
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.m1696getRecipientExtras$lambda147((byte[]) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null);
    }

    /* renamed from: getRecipientExtras$lambda-147 */
    public static final RecipientExtras m1696getRecipientExtras$lambda147(byte[] bArr) {
        try {
            return RecipientExtras.parseFrom(bArr);
        } catch (InvalidProtocolBufferException e) {
            Log.w(TAG, e);
            throw new AssertionError(e);
        }
    }

    private final void updateProfileValuesForMerge(ContentValues contentValues, RecipientRecord recipientRecord) {
        contentValues.put(PROFILE_KEY, recipientRecord.getProfileKey() != null ? Base64.encodeBytes(recipientRecord.getProfileKey()) : null);
        contentValues.putNull(EXPIRING_PROFILE_KEY_CREDENTIAL);
        contentValues.put(SIGNAL_PROFILE_AVATAR, recipientRecord.getProfileAvatar());
        contentValues.put(PROFILE_GIVEN_NAME, recipientRecord.getProfileName().getGivenName());
        contentValues.put(PROFILE_FAMILY_NAME, recipientRecord.getProfileName().getFamilyName());
        contentValues.put(PROFILE_JOINED_NAME, recipientRecord.getProfileName().toString());
    }

    private final String orderByPreferringAlphaOverNumeric(String str) {
        return "CASE WHEN " + str + " GLOB '[0-9]*' THEN 1 ELSE 0 END, " + str;
    }

    private final <T> boolean isAbsent(Optional<T> optional) {
        return !optional.isPresent();
    }

    private final RecipientLogDetails toLogDetails(RecipientRecord recipientRecord) {
        return new RecipientLogDetails(recipientRecord.getId(), recipientRecord.getServiceId(), recipientRecord.getE164());
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\b\u001a\u00020\tH\u0002J\u0006\u0010\n\u001a\u00020\tJ\b\u0010\u000b\u001a\u00020\tH\u0002JF\u0010\f\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\b\u0010\u0013\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0011R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$BulkOperationsHandle;", "", "database", "Lorg/thoughtcrime/securesms/database/SQLiteDatabase;", "(Lorg/thoughtcrime/securesms/database/RecipientDatabase;Lorg/thoughtcrime/securesms/database/SQLiteDatabase;)V", "pendingRecipients", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "clearSystemDataForPendingInfo", "", "finish", "markAllRelevantEntriesDirty", "setSystemContactInfo", ContactRepository.ID_COLUMN, "systemProfileName", "Lorg/thoughtcrime/securesms/profiles/ProfileName;", "systemDisplayName", "", "photoUri", "systemPhoneLabel", "systemPhoneType", "", "systemContactUri", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class BulkOperationsHandle {
        private final SQLiteDatabase database;
        private final Set<RecipientId> pendingRecipients = new LinkedHashSet();
        final /* synthetic */ RecipientDatabase this$0;

        public BulkOperationsHandle(RecipientDatabase recipientDatabase, SQLiteDatabase sQLiteDatabase) {
            Intrinsics.checkNotNullParameter(sQLiteDatabase, "database");
            this.this$0 = recipientDatabase;
            this.database = sQLiteDatabase;
        }

        public final void setSystemContactInfo(RecipientId recipientId, ProfileName profileName, String str, String str2, String str3, int i, String str4) {
            Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
            Intrinsics.checkNotNullParameter(profileName, "systemProfileName");
            Object firstNonNull = Util.firstNonNull(str, profileName.toString());
            Intrinsics.checkNotNullExpressionValue(firstNonNull, "firstNonNull(systemDispl…emProfileName.toString())");
            ContentValues contentValues = new ContentValues();
            contentValues.put(RecipientDatabase.SYSTEM_GIVEN_NAME, profileName.getGivenName());
            contentValues.put(RecipientDatabase.SYSTEM_FAMILY_NAME, profileName.getFamilyName());
            contentValues.put(RecipientDatabase.SYSTEM_JOINED_NAME, (String) firstNonNull);
            contentValues.put(RecipientDatabase.SYSTEM_PHOTO_URI, str2);
            contentValues.put(RecipientDatabase.SYSTEM_PHONE_LABEL, str3);
            contentValues.put(RecipientDatabase.SYSTEM_PHONE_TYPE, Integer.valueOf(i));
            contentValues.put(RecipientDatabase.SYSTEM_CONTACT_URI, str4);
            if (this.this$0.update(recipientId, contentValues)) {
                this.pendingRecipients.add(recipientId);
            }
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put(RecipientDatabase.SYSTEM_INFO_PENDING, (Integer) 0);
            this.this$0.update(recipientId, contentValues2);
        }

        public final void finish() {
            markAllRelevantEntriesDirty();
            clearSystemDataForPendingInfo();
            this.database.setTransactionSuccessful();
            this.database.endTransaction();
            for (RecipientId recipientId : this.pendingRecipients) {
                ApplicationDependencies.getDatabaseObserver().notifyRecipientChanged(recipientId);
            }
        }

        private final void markAllRelevantEntriesDirty() {
            Cursor query = this.database.query(RecipientDatabase.TABLE_NAME, RecipientDatabase.ID_PROJECTION, "system_info_pending = ? AND storage_service_key NOT NULL", SqlUtil.buildArgs(SubscriptionLevels.BOOST_LEVEL), null, null, null);
            RecipientDatabase recipientDatabase = this.this$0;
            while (query.moveToNext()) {
                try {
                    Intrinsics.checkNotNullExpressionValue(query, "cursor");
                    RecipientId from = RecipientId.from(CursorExtensionsKt.requireNonNullString(query, "_id"));
                    Intrinsics.checkNotNullExpressionValue(from, ContactRepository.ID_COLUMN);
                    recipientDatabase.rotateStorageId(from);
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
            th = null;
        }

        private final void clearSystemDataForPendingInfo() {
            UpdateBuilderPart3.run$default(SQLiteDatabaseExtensionsKt.update(this.database, RecipientDatabase.TABLE_NAME).values(TuplesKt.to(RecipientDatabase.SYSTEM_INFO_PENDING, 0), TuplesKt.to(RecipientDatabase.SYSTEM_GIVEN_NAME, null), TuplesKt.to(RecipientDatabase.SYSTEM_FAMILY_NAME, null), TuplesKt.to(RecipientDatabase.SYSTEM_JOINED_NAME, null), TuplesKt.to(RecipientDatabase.SYSTEM_PHOTO_URI, null), TuplesKt.to(RecipientDatabase.SYSTEM_PHONE_LABEL, null), TuplesKt.to(RecipientDatabase.SYSTEM_CONTACT_URI, null)).where("system_info_pending = ?", 1), 0, 1, null);
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\t\u001a\u00020\nH\u0016J\u0006\u0010\u000b\u001a\u00020\fJ\b\u0010\r\u001a\u0004\u0018\u00010\fR\u0011\u0010\u0005\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientReader;", "Ljava/io/Closeable;", "cursor", "Landroid/database/Cursor;", "(Landroid/database/Cursor;)V", NewHtcHomeBadger.COUNT, "", "getCount", "()I", "close", "", "getCurrent", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getNext", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RecipientReader implements Closeable {
        private final Cursor cursor;

        public RecipientReader(Cursor cursor) {
            Intrinsics.checkNotNullParameter(cursor, "cursor");
            this.cursor = cursor;
        }

        public final Recipient getCurrent() {
            Recipient resolved = Recipient.resolved(RecipientId.from(CursorExtensionsKt.requireLong(this.cursor, "_id")));
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(id)");
            return resolved;
        }

        public final Recipient getNext() {
            if (this.cursor.moveToNext()) {
                return getCurrent();
            }
            return null;
        }

        public final int getCount() {
            return this.cursor.getCount();
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.cursor.close();
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00060\u0001j\u0002`\u0002B\u000f\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$MissingRecipientException;", "Ljava/lang/IllegalStateException;", "Lkotlin/IllegalStateException;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MissingRecipientException extends IllegalStateException {
        public MissingRecipientException(RecipientId recipientId) {
            super("Failed to find recipient with ID: " + recipientId);
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$GetOrInsertResult;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "neededInsert", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Z)V", "getNeededInsert", "()Z", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GetOrInsertResult {
        private final boolean neededInsert;
        private final RecipientId recipientId;

        public GetOrInsertResult(RecipientId recipientId, boolean z) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
            this.neededInsert = z;
        }

        public final boolean getNeededInsert() {
            return this.neededInsert;
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\t\b\u0001\u0018\u0000 \r2\u00020\u0001:\u0002\f\rB\u001d\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\u0002\u0010\u0006R\u0019\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\n\n\u0002\u0010\t\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$ContactSearchSelection;", "", "where", "", MultiselectForwardFragment.ARGS, "", "(Ljava/lang/String;[Ljava/lang/String;)V", "getArgs", "()[Ljava/lang/String;", "[Ljava/lang/String;", "getWhere", "()Ljava/lang/String;", "Builder", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ContactSearchSelection {
        public static final Companion Companion = new Companion(null);
        public static final String FILTER_BLOCKED;
        public static final String FILTER_GROUPS;
        public static final String FILTER_ID;
        public static final String NON_SIGNAL_CONTACT;
        public static final String QUERY_NON_SIGNAL_CONTACT;
        public static final String QUERY_SIGNAL_CONTACT;
        public static final String SIGNAL_CONTACT;
        private final String[] args;
        private final String where;

        public /* synthetic */ ContactSearchSelection(String str, String[] strArr, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, strArr);
        }

        private ContactSearchSelection(String str, String[] strArr) {
            this.where = str;
            this.args = strArr;
        }

        public final String[] getArgs() {
            return this.args;
        }

        public final String getWhere() {
            return this.where;
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0001\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\u000b\u001a\u00020\fJ\u0010\u0010\u0005\u001a\u00020\u00002\b\u0010\r\u001a\u0004\u0018\u00010\u0006J\u000e\u0010\u000e\u001a\u00020\u00002\u0006\u0010\u000f\u001a\u00020\u0004J\u000e\u0010\u0010\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0004J\u000e\u0010\u0011\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\u0004J\u000e\u0010\u0012\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$ContactSearchSelection$Builder;", "", "()V", "excludeGroups", "", "excludeId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "includeNonRegistered", "includeRegistered", "searchQuery", "", "build", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$ContactSearchSelection;", "recipientId", "withGroups", "includeGroups", "withNonRegistered", "withRegistered", "withSearchQuery", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Builder {
            private boolean excludeGroups;
            private RecipientId excludeId;
            private boolean includeNonRegistered;
            private boolean includeRegistered;
            private String searchQuery;

            public final Builder withRegistered(boolean z) {
                this.includeRegistered = z;
                return this;
            }

            public final Builder withNonRegistered(boolean z) {
                this.includeNonRegistered = z;
                return this;
            }

            public final Builder excludeId(RecipientId recipientId) {
                this.excludeId = recipientId;
                return this;
            }

            public final Builder withGroups(boolean z) {
                this.excludeGroups = !z;
                return this;
            }

            public final Builder withSearchQuery(String str) {
                Intrinsics.checkNotNullParameter(str, "searchQuery");
                this.searchQuery = str;
                return this;
            }

            public final ContactSearchSelection build() {
                if (this.includeRegistered || this.includeNonRegistered) {
                    StringBuilder sb = new StringBuilder("(");
                    LinkedList<Object> linkedList = new LinkedList();
                    if (this.includeRegistered) {
                        sb.append("(");
                        linkedList.add(Integer.valueOf(RegisteredState.REGISTERED.getId()));
                        linkedList.add(1);
                        if (Util.isEmpty(this.searchQuery)) {
                            sb.append(ContactSearchSelection.SIGNAL_CONTACT);
                        } else {
                            sb.append(ContactSearchSelection.QUERY_SIGNAL_CONTACT);
                            linkedList.add(this.searchQuery);
                            linkedList.add(this.searchQuery);
                            linkedList.add(this.searchQuery);
                        }
                        sb.append(")");
                    }
                    if (this.includeRegistered && this.includeNonRegistered) {
                        sb.append(" OR ");
                    }
                    if (this.includeNonRegistered) {
                        sb.append("(");
                        linkedList.add(Integer.valueOf(RegisteredState.REGISTERED.getId()));
                        if (Util.isEmpty(this.searchQuery)) {
                            sb.append(ContactSearchSelection.NON_SIGNAL_CONTACT);
                        } else {
                            sb.append(ContactSearchSelection.QUERY_NON_SIGNAL_CONTACT);
                            linkedList.add(this.searchQuery);
                            linkedList.add(this.searchQuery);
                            linkedList.add(this.searchQuery);
                        }
                        sb.append(")");
                    }
                    sb.append(")");
                    sb.append(ContactSearchSelection.FILTER_BLOCKED);
                    linkedList.add(0);
                    if (this.excludeGroups) {
                        sb.append(ContactSearchSelection.FILTER_GROUPS);
                    }
                    if (this.excludeId != null) {
                        sb.append(ContactSearchSelection.FILTER_ID);
                        RecipientId recipientId = this.excludeId;
                        Intrinsics.checkNotNull(recipientId);
                        linkedList.add(recipientId.serialize());
                    }
                    String sb2 = sb.toString();
                    Intrinsics.checkNotNullExpressionValue(sb2, "stringBuilder.toString()");
                    ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(linkedList, 10));
                    for (Object obj : linkedList) {
                        arrayList.add(String.valueOf(obj));
                    }
                    Object[] array = arrayList.toArray(new String[0]);
                    if (array != null) {
                        return new ContactSearchSelection(sb2, (String[]) array, null);
                    }
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                }
                throw new IllegalStateException("Must include either registered or non-registered recipients in search".toString());
            }
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$ContactSearchSelection$Companion;", "", "()V", "FILTER_BLOCKED", "", "FILTER_GROUPS", "FILTER_ID", "NON_SIGNAL_CONTACT", "QUERY_NON_SIGNAL_CONTACT", "QUERY_SIGNAL_CONTACT", "SIGNAL_CONTACT", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\bÀ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$Capabilities;", "", "()V", "ANNOUNCEMENT_GROUPS", "", "BIT_LENGTH", "CHANGE_NUMBER", "GIFT_BADGES", "GROUPS_V1_MIGRATION", "SENDER_KEY", "STORIES", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Capabilities {
        public static final int ANNOUNCEMENT_GROUPS;
        public static final int BIT_LENGTH;
        public static final int CHANGE_NUMBER;
        public static final int GIFT_BADGES;
        public static final int GROUPS_V1_MIGRATION;
        public static final Capabilities INSTANCE = new Capabilities();
        public static final int SENDER_KEY;
        public static final int STORIES;

        private Capabilities() {
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\b\b\u0001\u0018\u0000 \n2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\nB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;", "", ContactRepository.ID_COLUMN, "", "(Ljava/lang/String;II)V", "getId", "()I", "DEFAULT", "ENABLED", "DISABLED", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum VibrateState {
        DEFAULT(0),
        ENABLED(1),
        DISABLED(2);
        
        public static final Companion Companion = new Companion(null);
        private final int id;

        VibrateState(int i) {
            this.id = i;
        }

        public final int getId() {
            return this.id;
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\t¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState$Companion;", "", "()V", "fromBoolean", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "", "fromId", ContactRepository.ID_COLUMN, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final VibrateState fromId(int i) {
                return VibrateState.values()[i];
            }

            public final VibrateState fromBoolean(boolean z) {
                return z ? VibrateState.ENABLED : VibrateState.DISABLED;
            }
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\b\b\u0001\u0018\u0000 \n2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\nB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RegisteredState;", "", ContactRepository.ID_COLUMN, "", "(Ljava/lang/String;II)V", "getId", "()I", "UNKNOWN", "REGISTERED", "NOT_REGISTERED", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum RegisteredState {
        UNKNOWN(0),
        REGISTERED(1),
        NOT_REGISTERED(2);
        
        public static final Companion Companion = new Companion(null);
        private final int id;

        RegisteredState(int i) {
            this.id = i;
        }

        public final int getId() {
            return this.id;
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RegisteredState$Companion;", "", "()V", "fromId", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RegisteredState;", ContactRepository.ID_COLUMN, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final RegisteredState fromId(int i) {
                return RegisteredState.values()[i];
            }
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\t\b\u0001\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000bB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$UnidentifiedAccessMode;", "", "mode", "", "(Ljava/lang/String;II)V", "getMode", "()I", "UNKNOWN", "DISABLED", "ENABLED", "UNRESTRICTED", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum UnidentifiedAccessMode {
        UNKNOWN(0),
        DISABLED(1),
        ENABLED(2),
        UNRESTRICTED(3);
        
        public static final Companion Companion = new Companion(null);
        private final int mode;

        UnidentifiedAccessMode(int i) {
            this.mode = i;
        }

        public final int getMode() {
            return this.mode;
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$UnidentifiedAccessMode$Companion;", "", "()V", "fromMode", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$UnidentifiedAccessMode;", "mode", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final UnidentifiedAccessMode fromMode(int i) {
                return UnidentifiedAccessMode.values()[i];
            }
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0001\u0018\u0000 \r2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\rB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\nj\u0002\b\u000bj\u0002\b\f¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$InsightsBannerTier;", "", ContactRepository.ID_COLUMN, "", "(Ljava/lang/String;II)V", "getId", "()I", "seen", "", "tier", "NO_TIER", "TIER_ONE", "TIER_TWO", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum InsightsBannerTier {
        NO_TIER(0),
        TIER_ONE(1),
        TIER_TWO(2);
        
        public static final Companion Companion = new Companion(null);
        private final int id;

        InsightsBannerTier(int i) {
            this.id = i;
        }

        public final int getId() {
            return this.id;
        }

        public final boolean seen(InsightsBannerTier insightsBannerTier) {
            Intrinsics.checkNotNullParameter(insightsBannerTier, "tier");
            return insightsBannerTier.id <= this.id;
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$InsightsBannerTier$Companion;", "", "()V", "fromId", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$InsightsBannerTier;", ContactRepository.ID_COLUMN, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final InsightsBannerTier fromId(int i) {
                return InsightsBannerTier.values()[i];
            }
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\n\b\u0001\u0018\u0000 \f2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\fB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$GroupType;", "", ContactRepository.ID_COLUMN, "", "(Ljava/lang/String;II)V", "getId", "()I", "NONE", "MMS", "SIGNAL_V1", "SIGNAL_V2", "DISTRIBUTION_LIST", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum GroupType {
        NONE(0),
        MMS(1),
        SIGNAL_V1(2),
        SIGNAL_V2(3),
        DISTRIBUTION_LIST(4);
        
        public static final Companion Companion = new Companion(null);
        private final int id;

        GroupType(int i) {
            this.id = i;
        }

        public final int getId() {
            return this.id;
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$GroupType$Companion;", "", "()V", "fromId", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$GroupType;", ContactRepository.ID_COLUMN, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final GroupType fromId(int i) {
                return GroupType.values()[i];
            }
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\tB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;", "", ContactRepository.ID_COLUMN, "", "(Ljava/lang/String;II)V", "getId", "()I", "ALWAYS_NOTIFY", "DO_NOT_NOTIFY", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum MentionSetting {
        ALWAYS_NOTIFY(0),
        DO_NOT_NOTIFY(1);
        
        public static final Companion Companion = new Companion(null);
        private final int id;

        MentionSetting(int i) {
            this.id = i;
        }

        public final int getId() {
            return this.id;
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting$Companion;", "", "()V", "fromId", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;", ContactRepository.ID_COLUMN, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final MentionSetting fromId(int i) {
                return MentionSetting.values()[i];
            }
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\b\u0007\b\t\n\u000b\f\r\u000eB\u0011\b\u0004\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\b\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch;", "", "logBundle", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "(Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;)V", "getLogBundle", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "Insert", "InsertAndReassignE164", "Match", "MatchAndInsertAci", "MatchAndMerge", "MatchAndReassignE164", "MatchAndUpdateAci", "MatchAndUpdateE164", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$Match;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$MatchAndUpdateE164;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$MatchAndReassignE164;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$MatchAndUpdateAci;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$MatchAndInsertAci;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$MatchAndMerge;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$Insert;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$InsertAndReassignE164;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class RecipientFetch {
        private final LogBundle logBundle;

        public /* synthetic */ RecipientFetch(LogBundle logBundle, DefaultConstructorMarker defaultConstructorMarker) {
            this(logBundle);
        }

        private RecipientFetch(LogBundle logBundle) {
            this.logBundle = logBundle;
        }

        public final LogBundle getLogBundle() {
            return this.logBundle;
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u001f\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$Match;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "bundle", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;)V", "getBundle", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "getId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Match extends RecipientFetch {
            private final LogBundle bundle;
            private final RecipientId id;

            public static /* synthetic */ Match copy$default(Match match, RecipientId recipientId, LogBundle logBundle, int i, Object obj) {
                if ((i & 1) != 0) {
                    recipientId = match.id;
                }
                if ((i & 2) != 0) {
                    logBundle = match.bundle;
                }
                return match.copy(recipientId, logBundle);
            }

            public final RecipientId component1() {
                return this.id;
            }

            public final LogBundle component2() {
                return this.bundle;
            }

            public final Match copy(RecipientId recipientId, LogBundle logBundle) {
                Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                return new Match(recipientId, logBundle);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Match)) {
                    return false;
                }
                Match match = (Match) obj;
                return Intrinsics.areEqual(this.id, match.id) && Intrinsics.areEqual(this.bundle, match.bundle);
            }

            public int hashCode() {
                int hashCode = this.id.hashCode() * 31;
                LogBundle logBundle = this.bundle;
                return hashCode + (logBundle == null ? 0 : logBundle.hashCode());
            }

            public String toString() {
                return "Match(id=" + this.id + ", bundle=" + this.bundle + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Match(RecipientId recipientId, LogBundle logBundle) {
                super(logBundle, null);
                Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                this.id = recipientId;
                this.bundle = logBundle;
            }

            public final LogBundle getBundle() {
                return this.bundle;
            }

            public final RecipientId getId() {
                return this.id;
            }
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\bHÆ\u0003J3\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\t\u0010\u001c\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\r¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$MatchAndUpdateE164;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", CdsDatabase.E164, "", "changedNumber", "bundle", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Ljava/lang/String;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;)V", "getBundle", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "getChangedNumber", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getE164", "()Ljava/lang/String;", "getId", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class MatchAndUpdateE164 extends RecipientFetch {
            private final LogBundle bundle;
            private final RecipientId changedNumber;
            private final String e164;
            private final RecipientId id;

            public static /* synthetic */ MatchAndUpdateE164 copy$default(MatchAndUpdateE164 matchAndUpdateE164, RecipientId recipientId, String str, RecipientId recipientId2, LogBundle logBundle, int i, Object obj) {
                if ((i & 1) != 0) {
                    recipientId = matchAndUpdateE164.id;
                }
                if ((i & 2) != 0) {
                    str = matchAndUpdateE164.e164;
                }
                if ((i & 4) != 0) {
                    recipientId2 = matchAndUpdateE164.changedNumber;
                }
                if ((i & 8) != 0) {
                    logBundle = matchAndUpdateE164.bundle;
                }
                return matchAndUpdateE164.copy(recipientId, str, recipientId2, logBundle);
            }

            public final RecipientId component1() {
                return this.id;
            }

            public final String component2() {
                return this.e164;
            }

            public final RecipientId component3() {
                return this.changedNumber;
            }

            public final LogBundle component4() {
                return this.bundle;
            }

            public final MatchAndUpdateE164 copy(RecipientId recipientId, String str, RecipientId recipientId2, LogBundle logBundle) {
                Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                return new MatchAndUpdateE164(recipientId, str, recipientId2, logBundle);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof MatchAndUpdateE164)) {
                    return false;
                }
                MatchAndUpdateE164 matchAndUpdateE164 = (MatchAndUpdateE164) obj;
                return Intrinsics.areEqual(this.id, matchAndUpdateE164.id) && Intrinsics.areEqual(this.e164, matchAndUpdateE164.e164) && Intrinsics.areEqual(this.changedNumber, matchAndUpdateE164.changedNumber) && Intrinsics.areEqual(this.bundle, matchAndUpdateE164.bundle);
            }

            public int hashCode() {
                int hashCode = ((this.id.hashCode() * 31) + this.e164.hashCode()) * 31;
                RecipientId recipientId = this.changedNumber;
                return ((hashCode + (recipientId == null ? 0 : recipientId.hashCode())) * 31) + this.bundle.hashCode();
            }

            public String toString() {
                return "MatchAndUpdateE164(id=" + this.id + ", e164=" + this.e164 + ", changedNumber=" + this.changedNumber + ", bundle=" + this.bundle + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public MatchAndUpdateE164(RecipientId recipientId, String str, RecipientId recipientId2, LogBundle logBundle) {
                super(logBundle, null);
                Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                this.id = recipientId;
                this.e164 = str;
                this.changedNumber = recipientId2;
                this.bundle = logBundle;
            }

            public final LogBundle getBundle() {
                return this.bundle;
            }

            public final RecipientId getChangedNumber() {
                return this.changedNumber;
            }

            public final String getE164() {
                return this.e164;
            }

            public final RecipientId getId() {
                return this.id;
            }
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0006HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\tHÆ\u0003J=\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\b\u001a\u00020\tHÆ\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cHÖ\u0003J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001J\t\u0010\u001f\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000e¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$MatchAndReassignE164;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "e164Id", CdsDatabase.E164, "", "changedNumber", "bundle", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/recipients/RecipientId;Ljava/lang/String;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;)V", "getBundle", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "getChangedNumber", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getE164", "()Ljava/lang/String;", "getE164Id", "getId", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class MatchAndReassignE164 extends RecipientFetch {
            private final LogBundle bundle;
            private final RecipientId changedNumber;
            private final String e164;
            private final RecipientId e164Id;
            private final RecipientId id;

            public static /* synthetic */ MatchAndReassignE164 copy$default(MatchAndReassignE164 matchAndReassignE164, RecipientId recipientId, RecipientId recipientId2, String str, RecipientId recipientId3, LogBundle logBundle, int i, Object obj) {
                if ((i & 1) != 0) {
                    recipientId = matchAndReassignE164.id;
                }
                if ((i & 2) != 0) {
                    recipientId2 = matchAndReassignE164.e164Id;
                }
                if ((i & 4) != 0) {
                    str = matchAndReassignE164.e164;
                }
                if ((i & 8) != 0) {
                    recipientId3 = matchAndReassignE164.changedNumber;
                }
                if ((i & 16) != 0) {
                    logBundle = matchAndReassignE164.bundle;
                }
                return matchAndReassignE164.copy(recipientId, recipientId2, str, recipientId3, logBundle);
            }

            public final RecipientId component1() {
                return this.id;
            }

            public final RecipientId component2() {
                return this.e164Id;
            }

            public final String component3() {
                return this.e164;
            }

            public final RecipientId component4() {
                return this.changedNumber;
            }

            public final LogBundle component5() {
                return this.bundle;
            }

            public final MatchAndReassignE164 copy(RecipientId recipientId, RecipientId recipientId2, String str, RecipientId recipientId3, LogBundle logBundle) {
                Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                Intrinsics.checkNotNullParameter(recipientId2, "e164Id");
                Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                return new MatchAndReassignE164(recipientId, recipientId2, str, recipientId3, logBundle);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof MatchAndReassignE164)) {
                    return false;
                }
                MatchAndReassignE164 matchAndReassignE164 = (MatchAndReassignE164) obj;
                return Intrinsics.areEqual(this.id, matchAndReassignE164.id) && Intrinsics.areEqual(this.e164Id, matchAndReassignE164.e164Id) && Intrinsics.areEqual(this.e164, matchAndReassignE164.e164) && Intrinsics.areEqual(this.changedNumber, matchAndReassignE164.changedNumber) && Intrinsics.areEqual(this.bundle, matchAndReassignE164.bundle);
            }

            public int hashCode() {
                int hashCode = ((((this.id.hashCode() * 31) + this.e164Id.hashCode()) * 31) + this.e164.hashCode()) * 31;
                RecipientId recipientId = this.changedNumber;
                return ((hashCode + (recipientId == null ? 0 : recipientId.hashCode())) * 31) + this.bundle.hashCode();
            }

            public String toString() {
                return "MatchAndReassignE164(id=" + this.id + ", e164Id=" + this.e164Id + ", e164=" + this.e164 + ", changedNumber=" + this.changedNumber + ", bundle=" + this.bundle + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public MatchAndReassignE164(RecipientId recipientId, RecipientId recipientId2, String str, RecipientId recipientId3, LogBundle logBundle) {
                super(logBundle, null);
                Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                Intrinsics.checkNotNullParameter(recipientId2, "e164Id");
                Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                this.id = recipientId;
                this.e164Id = recipientId2;
                this.e164 = str;
                this.changedNumber = recipientId3;
                this.bundle = logBundle;
            }

            public final LogBundle getBundle() {
                return this.bundle;
            }

            public final RecipientId getChangedNumber() {
                return this.changedNumber;
            }

            public final String getE164() {
                return this.e164;
            }

            public final RecipientId getE164Id() {
                return this.e164Id;
            }

            public final RecipientId getId() {
                return this.id;
            }
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$MatchAndUpdateAci;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "serviceId", "Lorg/whispersystems/signalservice/api/push/ServiceId;", "bundle", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/whispersystems/signalservice/api/push/ServiceId;Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;)V", "getBundle", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "getId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getServiceId", "()Lorg/whispersystems/signalservice/api/push/ServiceId;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class MatchAndUpdateAci extends RecipientFetch {
            private final LogBundle bundle;
            private final RecipientId id;
            private final ServiceId serviceId;

            public static /* synthetic */ MatchAndUpdateAci copy$default(MatchAndUpdateAci matchAndUpdateAci, RecipientId recipientId, ServiceId serviceId, LogBundle logBundle, int i, Object obj) {
                if ((i & 1) != 0) {
                    recipientId = matchAndUpdateAci.id;
                }
                if ((i & 2) != 0) {
                    serviceId = matchAndUpdateAci.serviceId;
                }
                if ((i & 4) != 0) {
                    logBundle = matchAndUpdateAci.bundle;
                }
                return matchAndUpdateAci.copy(recipientId, serviceId, logBundle);
            }

            public final RecipientId component1() {
                return this.id;
            }

            public final ServiceId component2() {
                return this.serviceId;
            }

            public final LogBundle component3() {
                return this.bundle;
            }

            public final MatchAndUpdateAci copy(RecipientId recipientId, ServiceId serviceId, LogBundle logBundle) {
                Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                Intrinsics.checkNotNullParameter(serviceId, "serviceId");
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                return new MatchAndUpdateAci(recipientId, serviceId, logBundle);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof MatchAndUpdateAci)) {
                    return false;
                }
                MatchAndUpdateAci matchAndUpdateAci = (MatchAndUpdateAci) obj;
                return Intrinsics.areEqual(this.id, matchAndUpdateAci.id) && Intrinsics.areEqual(this.serviceId, matchAndUpdateAci.serviceId) && Intrinsics.areEqual(this.bundle, matchAndUpdateAci.bundle);
            }

            public int hashCode() {
                return (((this.id.hashCode() * 31) + this.serviceId.hashCode()) * 31) + this.bundle.hashCode();
            }

            public String toString() {
                return "MatchAndUpdateAci(id=" + this.id + ", serviceId=" + this.serviceId + ", bundle=" + this.bundle + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public MatchAndUpdateAci(RecipientId recipientId, ServiceId serviceId, LogBundle logBundle) {
                super(logBundle, null);
                Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                Intrinsics.checkNotNullParameter(serviceId, "serviceId");
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                this.id = recipientId;
                this.serviceId = serviceId;
                this.bundle = logBundle;
            }

            public final LogBundle getBundle() {
                return this.bundle;
            }

            public final RecipientId getId() {
                return this.id;
            }

            public final ServiceId getServiceId() {
                return this.serviceId;
            }
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$MatchAndInsertAci;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "serviceId", "Lorg/whispersystems/signalservice/api/push/ServiceId;", "bundle", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/whispersystems/signalservice/api/push/ServiceId;Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;)V", "getBundle", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "getId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getServiceId", "()Lorg/whispersystems/signalservice/api/push/ServiceId;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class MatchAndInsertAci extends RecipientFetch {
            private final LogBundle bundle;
            private final RecipientId id;
            private final ServiceId serviceId;

            public static /* synthetic */ MatchAndInsertAci copy$default(MatchAndInsertAci matchAndInsertAci, RecipientId recipientId, ServiceId serviceId, LogBundle logBundle, int i, Object obj) {
                if ((i & 1) != 0) {
                    recipientId = matchAndInsertAci.id;
                }
                if ((i & 2) != 0) {
                    serviceId = matchAndInsertAci.serviceId;
                }
                if ((i & 4) != 0) {
                    logBundle = matchAndInsertAci.bundle;
                }
                return matchAndInsertAci.copy(recipientId, serviceId, logBundle);
            }

            public final RecipientId component1() {
                return this.id;
            }

            public final ServiceId component2() {
                return this.serviceId;
            }

            public final LogBundle component3() {
                return this.bundle;
            }

            public final MatchAndInsertAci copy(RecipientId recipientId, ServiceId serviceId, LogBundle logBundle) {
                Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                Intrinsics.checkNotNullParameter(serviceId, "serviceId");
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                return new MatchAndInsertAci(recipientId, serviceId, logBundle);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof MatchAndInsertAci)) {
                    return false;
                }
                MatchAndInsertAci matchAndInsertAci = (MatchAndInsertAci) obj;
                return Intrinsics.areEqual(this.id, matchAndInsertAci.id) && Intrinsics.areEqual(this.serviceId, matchAndInsertAci.serviceId) && Intrinsics.areEqual(this.bundle, matchAndInsertAci.bundle);
            }

            public int hashCode() {
                return (((this.id.hashCode() * 31) + this.serviceId.hashCode()) * 31) + this.bundle.hashCode();
            }

            public String toString() {
                return "MatchAndInsertAci(id=" + this.id + ", serviceId=" + this.serviceId + ", bundle=" + this.bundle + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public MatchAndInsertAci(RecipientId recipientId, ServiceId serviceId, LogBundle logBundle) {
                super(logBundle, null);
                Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                Intrinsics.checkNotNullParameter(serviceId, "serviceId");
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                this.id = recipientId;
                this.serviceId = serviceId;
                this.bundle = logBundle;
            }

            public final LogBundle getBundle() {
                return this.bundle;
            }

            public final RecipientId getId() {
                return this.id;
            }

            public final ServiceId getServiceId() {
                return this.serviceId;
            }
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0007HÆ\u0003J3\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$MatchAndMerge;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch;", "sidId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "e164Id", "changedNumber", "bundle", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;)V", "getBundle", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "getChangedNumber", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getE164Id", "getSidId", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class MatchAndMerge extends RecipientFetch {
            private final LogBundle bundle;
            private final RecipientId changedNumber;
            private final RecipientId e164Id;
            private final RecipientId sidId;

            public static /* synthetic */ MatchAndMerge copy$default(MatchAndMerge matchAndMerge, RecipientId recipientId, RecipientId recipientId2, RecipientId recipientId3, LogBundle logBundle, int i, Object obj) {
                if ((i & 1) != 0) {
                    recipientId = matchAndMerge.sidId;
                }
                if ((i & 2) != 0) {
                    recipientId2 = matchAndMerge.e164Id;
                }
                if ((i & 4) != 0) {
                    recipientId3 = matchAndMerge.changedNumber;
                }
                if ((i & 8) != 0) {
                    logBundle = matchAndMerge.bundle;
                }
                return matchAndMerge.copy(recipientId, recipientId2, recipientId3, logBundle);
            }

            public final RecipientId component1() {
                return this.sidId;
            }

            public final RecipientId component2() {
                return this.e164Id;
            }

            public final RecipientId component3() {
                return this.changedNumber;
            }

            public final LogBundle component4() {
                return this.bundle;
            }

            public final MatchAndMerge copy(RecipientId recipientId, RecipientId recipientId2, RecipientId recipientId3, LogBundle logBundle) {
                Intrinsics.checkNotNullParameter(recipientId, "sidId");
                Intrinsics.checkNotNullParameter(recipientId2, "e164Id");
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                return new MatchAndMerge(recipientId, recipientId2, recipientId3, logBundle);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof MatchAndMerge)) {
                    return false;
                }
                MatchAndMerge matchAndMerge = (MatchAndMerge) obj;
                return Intrinsics.areEqual(this.sidId, matchAndMerge.sidId) && Intrinsics.areEqual(this.e164Id, matchAndMerge.e164Id) && Intrinsics.areEqual(this.changedNumber, matchAndMerge.changedNumber) && Intrinsics.areEqual(this.bundle, matchAndMerge.bundle);
            }

            public int hashCode() {
                int hashCode = ((this.sidId.hashCode() * 31) + this.e164Id.hashCode()) * 31;
                RecipientId recipientId = this.changedNumber;
                return ((hashCode + (recipientId == null ? 0 : recipientId.hashCode())) * 31) + this.bundle.hashCode();
            }

            public String toString() {
                return "MatchAndMerge(sidId=" + this.sidId + ", e164Id=" + this.e164Id + ", changedNumber=" + this.changedNumber + ", bundle=" + this.bundle + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public MatchAndMerge(RecipientId recipientId, RecipientId recipientId2, RecipientId recipientId3, LogBundle logBundle) {
                super(logBundle, null);
                Intrinsics.checkNotNullParameter(recipientId, "sidId");
                Intrinsics.checkNotNullParameter(recipientId2, "e164Id");
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                this.sidId = recipientId;
                this.e164Id = recipientId2;
                this.changedNumber = recipientId3;
                this.bundle = logBundle;
            }

            public final LogBundle getBundle() {
                return this.bundle;
            }

            public final RecipientId getChangedNumber() {
                return this.changedNumber;
            }

            public final RecipientId getE164Id() {
                return this.e164Id;
            }

            public final RecipientId getSidId() {
                return this.sidId;
            }
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B!\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J+\u0010\u0012\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$Insert;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch;", "serviceId", "Lorg/whispersystems/signalservice/api/push/ServiceId;", CdsDatabase.E164, "", "bundle", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "(Lorg/whispersystems/signalservice/api/push/ServiceId;Ljava/lang/String;Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;)V", "getBundle", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "getE164", "()Ljava/lang/String;", "getServiceId", "()Lorg/whispersystems/signalservice/api/push/ServiceId;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Insert extends RecipientFetch {
            private final LogBundle bundle;
            private final String e164;
            private final ServiceId serviceId;

            public static /* synthetic */ Insert copy$default(Insert insert, ServiceId serviceId, String str, LogBundle logBundle, int i, Object obj) {
                if ((i & 1) != 0) {
                    serviceId = insert.serviceId;
                }
                if ((i & 2) != 0) {
                    str = insert.e164;
                }
                if ((i & 4) != 0) {
                    logBundle = insert.bundle;
                }
                return insert.copy(serviceId, str, logBundle);
            }

            public final ServiceId component1() {
                return this.serviceId;
            }

            public final String component2() {
                return this.e164;
            }

            public final LogBundle component3() {
                return this.bundle;
            }

            public final Insert copy(ServiceId serviceId, String str, LogBundle logBundle) {
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                return new Insert(serviceId, str, logBundle);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Insert)) {
                    return false;
                }
                Insert insert = (Insert) obj;
                return Intrinsics.areEqual(this.serviceId, insert.serviceId) && Intrinsics.areEqual(this.e164, insert.e164) && Intrinsics.areEqual(this.bundle, insert.bundle);
            }

            public int hashCode() {
                ServiceId serviceId = this.serviceId;
                int i = 0;
                int hashCode = (serviceId == null ? 0 : serviceId.hashCode()) * 31;
                String str = this.e164;
                if (str != null) {
                    i = str.hashCode();
                }
                return ((hashCode + i) * 31) + this.bundle.hashCode();
            }

            public String toString() {
                return "Insert(serviceId=" + this.serviceId + ", e164=" + this.e164 + ", bundle=" + this.bundle + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Insert(ServiceId serviceId, String str, LogBundle logBundle) {
                super(logBundle, null);
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                this.serviceId = serviceId;
                this.e164 = str;
                this.bundle = logBundle;
            }

            public final LogBundle getBundle() {
                return this.bundle;
            }

            public final String getE164() {
                return this.e164;
            }

            public final ServiceId getServiceId() {
                return this.serviceId;
            }
        }

        /* compiled from: RecipientDatabase.kt */
        @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B)\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0016\u001a\u00020\tHÆ\u0003J5\u0010\u0017\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\tHÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bHÖ\u0003J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch$InsertAndReassignE164;", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientFetch;", "serviceId", "Lorg/whispersystems/signalservice/api/push/ServiceId;", CdsDatabase.E164, "", "e164Id", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "bundle", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "(Lorg/whispersystems/signalservice/api/push/ServiceId;Ljava/lang/String;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;)V", "getBundle", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "getE164", "()Ljava/lang/String;", "getE164Id", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getServiceId", "()Lorg/whispersystems/signalservice/api/push/ServiceId;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class InsertAndReassignE164 extends RecipientFetch {
            private final LogBundle bundle;
            private final String e164;
            private final RecipientId e164Id;
            private final ServiceId serviceId;

            public static /* synthetic */ InsertAndReassignE164 copy$default(InsertAndReassignE164 insertAndReassignE164, ServiceId serviceId, String str, RecipientId recipientId, LogBundle logBundle, int i, Object obj) {
                if ((i & 1) != 0) {
                    serviceId = insertAndReassignE164.serviceId;
                }
                if ((i & 2) != 0) {
                    str = insertAndReassignE164.e164;
                }
                if ((i & 4) != 0) {
                    recipientId = insertAndReassignE164.e164Id;
                }
                if ((i & 8) != 0) {
                    logBundle = insertAndReassignE164.bundle;
                }
                return insertAndReassignE164.copy(serviceId, str, recipientId, logBundle);
            }

            public final ServiceId component1() {
                return this.serviceId;
            }

            public final String component2() {
                return this.e164;
            }

            public final RecipientId component3() {
                return this.e164Id;
            }

            public final LogBundle component4() {
                return this.bundle;
            }

            public final InsertAndReassignE164 copy(ServiceId serviceId, String str, RecipientId recipientId, LogBundle logBundle) {
                Intrinsics.checkNotNullParameter(recipientId, "e164Id");
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                return new InsertAndReassignE164(serviceId, str, recipientId, logBundle);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof InsertAndReassignE164)) {
                    return false;
                }
                InsertAndReassignE164 insertAndReassignE164 = (InsertAndReassignE164) obj;
                return Intrinsics.areEqual(this.serviceId, insertAndReassignE164.serviceId) && Intrinsics.areEqual(this.e164, insertAndReassignE164.e164) && Intrinsics.areEqual(this.e164Id, insertAndReassignE164.e164Id) && Intrinsics.areEqual(this.bundle, insertAndReassignE164.bundle);
            }

            public int hashCode() {
                ServiceId serviceId = this.serviceId;
                int i = 0;
                int hashCode = (serviceId == null ? 0 : serviceId.hashCode()) * 31;
                String str = this.e164;
                if (str != null) {
                    i = str.hashCode();
                }
                return ((((hashCode + i) * 31) + this.e164Id.hashCode()) * 31) + this.bundle.hashCode();
            }

            public String toString() {
                return "InsertAndReassignE164(serviceId=" + this.serviceId + ", e164=" + this.e164 + ", e164Id=" + this.e164Id + ", bundle=" + this.bundle + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public InsertAndReassignE164(ServiceId serviceId, String str, RecipientId recipientId, LogBundle logBundle) {
                super(logBundle, null);
                Intrinsics.checkNotNullParameter(recipientId, "e164Id");
                Intrinsics.checkNotNullParameter(logBundle, "bundle");
                this.serviceId = serviceId;
                this.e164 = str;
                this.e164Id = recipientId;
                this.bundle = logBundle;
            }

            public final LogBundle getBundle() {
                return this.bundle;
            }

            public final String getE164() {
                return this.e164;
            }

            public final RecipientId getE164Id() {
                return this.e164Id;
            }

            public final ServiceId getServiceId() {
                return this.serviceId;
            }
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\bHÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\bHÆ\u0003JC\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\u000e\u0010\u0002\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0003J\t\u0010\u001e\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\t\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$LogBundle;", "", EmojiSearchDatabase.LABEL, "", "serviceId", "Lorg/whispersystems/signalservice/api/push/ServiceId;", CdsDatabase.E164, "bySid", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientLogDetails;", "byE164", "(Ljava/lang/String;Lorg/whispersystems/signalservice/api/push/ServiceId;Ljava/lang/String;Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientLogDetails;Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientLogDetails;)V", "getByE164", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientLogDetails;", "getBySid", "getE164", "()Ljava/lang/String;", "getLabel", "getServiceId", "()Lorg/whispersystems/signalservice/api/push/ServiceId;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LogBundle {
        private final RecipientLogDetails byE164;
        private final RecipientLogDetails bySid;
        private final String e164;
        private final String label;
        private final ServiceId serviceId;

        public static /* synthetic */ LogBundle copy$default(LogBundle logBundle, String str, ServiceId serviceId, String str2, RecipientLogDetails recipientLogDetails, RecipientLogDetails recipientLogDetails2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = logBundle.label;
            }
            if ((i & 2) != 0) {
                serviceId = logBundle.serviceId;
            }
            if ((i & 4) != 0) {
                str2 = logBundle.e164;
            }
            if ((i & 8) != 0) {
                recipientLogDetails = logBundle.bySid;
            }
            if ((i & 16) != 0) {
                recipientLogDetails2 = logBundle.byE164;
            }
            return logBundle.copy(str, serviceId, str2, recipientLogDetails, recipientLogDetails2);
        }

        public final String component1() {
            return this.label;
        }

        public final ServiceId component2() {
            return this.serviceId;
        }

        public final String component3() {
            return this.e164;
        }

        public final RecipientLogDetails component4() {
            return this.bySid;
        }

        public final RecipientLogDetails component5() {
            return this.byE164;
        }

        public final LogBundle copy(String str, ServiceId serviceId, String str2, RecipientLogDetails recipientLogDetails, RecipientLogDetails recipientLogDetails2) {
            Intrinsics.checkNotNullParameter(str, EmojiSearchDatabase.LABEL);
            return new LogBundle(str, serviceId, str2, recipientLogDetails, recipientLogDetails2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof LogBundle)) {
                return false;
            }
            LogBundle logBundle = (LogBundle) obj;
            return Intrinsics.areEqual(this.label, logBundle.label) && Intrinsics.areEqual(this.serviceId, logBundle.serviceId) && Intrinsics.areEqual(this.e164, logBundle.e164) && Intrinsics.areEqual(this.bySid, logBundle.bySid) && Intrinsics.areEqual(this.byE164, logBundle.byE164);
        }

        public int hashCode() {
            int hashCode = this.label.hashCode() * 31;
            ServiceId serviceId = this.serviceId;
            int i = 0;
            int hashCode2 = (hashCode + (serviceId == null ? 0 : serviceId.hashCode())) * 31;
            String str = this.e164;
            int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
            RecipientLogDetails recipientLogDetails = this.bySid;
            int hashCode4 = (hashCode3 + (recipientLogDetails == null ? 0 : recipientLogDetails.hashCode())) * 31;
            RecipientLogDetails recipientLogDetails2 = this.byE164;
            if (recipientLogDetails2 != null) {
                i = recipientLogDetails2.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            return "LogBundle(label=" + this.label + ", serviceId=" + this.serviceId + ", e164=" + this.e164 + ", bySid=" + this.bySid + ", byE164=" + this.byE164 + ')';
        }

        public LogBundle(String str, ServiceId serviceId, String str2, RecipientLogDetails recipientLogDetails, RecipientLogDetails recipientLogDetails2) {
            Intrinsics.checkNotNullParameter(str, EmojiSearchDatabase.LABEL);
            this.label = str;
            this.serviceId = serviceId;
            this.e164 = str2;
            this.bySid = recipientLogDetails;
            this.byE164 = recipientLogDetails2;
        }

        public /* synthetic */ LogBundle(String str, ServiceId serviceId, String str2, RecipientLogDetails recipientLogDetails, RecipientLogDetails recipientLogDetails2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, (i & 2) != 0 ? null : serviceId, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : recipientLogDetails, (i & 16) != 0 ? null : recipientLogDetails2);
        }

        public final String getLabel() {
            return this.label;
        }

        public final ServiceId getServiceId() {
            return this.serviceId;
        }

        public final String getE164() {
            return this.e164;
        }

        public final RecipientLogDetails getBySid() {
            return this.bySid;
        }

        public final RecipientLogDetails getByE164() {
            return this.byE164;
        }

        public final LogBundle label(String str) {
            Intrinsics.checkNotNullParameter(str, EmojiSearchDatabase.LABEL);
            return copy$default(this, str, null, null, null, null, 30, null);
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0007HÆ\u0003J+\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0007HÖ\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$RecipientLogDetails;", "", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "serviceId", "Lorg/whispersystems/signalservice/api/push/ServiceId;", CdsDatabase.E164, "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/whispersystems/signalservice/api/push/ServiceId;Ljava/lang/String;)V", "getE164", "()Ljava/lang/String;", "getId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getServiceId", "()Lorg/whispersystems/signalservice/api/push/ServiceId;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RecipientLogDetails {
        private final String e164;
        private final RecipientId id;
        private final ServiceId serviceId;

        public static /* synthetic */ RecipientLogDetails copy$default(RecipientLogDetails recipientLogDetails, RecipientId recipientId, ServiceId serviceId, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = recipientLogDetails.id;
            }
            if ((i & 2) != 0) {
                serviceId = recipientLogDetails.serviceId;
            }
            if ((i & 4) != 0) {
                str = recipientLogDetails.e164;
            }
            return recipientLogDetails.copy(recipientId, serviceId, str);
        }

        public final RecipientId component1() {
            return this.id;
        }

        public final ServiceId component2() {
            return this.serviceId;
        }

        public final String component3() {
            return this.e164;
        }

        public final RecipientLogDetails copy(RecipientId recipientId, ServiceId serviceId, String str) {
            Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
            return new RecipientLogDetails(recipientId, serviceId, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof RecipientLogDetails)) {
                return false;
            }
            RecipientLogDetails recipientLogDetails = (RecipientLogDetails) obj;
            return Intrinsics.areEqual(this.id, recipientLogDetails.id) && Intrinsics.areEqual(this.serviceId, recipientLogDetails.serviceId) && Intrinsics.areEqual(this.e164, recipientLogDetails.e164);
        }

        public int hashCode() {
            int hashCode = this.id.hashCode() * 31;
            ServiceId serviceId = this.serviceId;
            int i = 0;
            int hashCode2 = (hashCode + (serviceId == null ? 0 : serviceId.hashCode())) * 31;
            String str = this.e164;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            return "RecipientLogDetails(id=" + this.id + ", serviceId=" + this.serviceId + ", e164=" + this.e164 + ')';
        }

        public RecipientLogDetails(RecipientId recipientId, ServiceId serviceId, String str) {
            Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
            this.id = recipientId;
            this.serviceId = serviceId;
            this.e164 = str;
        }

        public /* synthetic */ RecipientLogDetails(RecipientId recipientId, ServiceId serviceId, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(recipientId, (i & 2) != 0 ? null : serviceId, (i & 4) != 0 ? null : str);
        }

        public final RecipientId getId() {
            return this.id;
        }

        public final ServiceId getServiceId() {
            return this.serviceId;
        }

        public final String getE164() {
            return this.e164;
        }
    }

    /* compiled from: RecipientDatabase.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u000b\u001a\u00020\fJ\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u000e\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u001f\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/database/RecipientDatabase$CdsV2Result;", "", RecipientDatabase.PNI_COLUMN, "Lorg/whispersystems/signalservice/api/push/PNI;", "aci", "Lorg/whispersystems/signalservice/api/push/ACI;", "(Lorg/whispersystems/signalservice/api/push/PNI;Lorg/whispersystems/signalservice/api/push/ACI;)V", "getAci", "()Lorg/whispersystems/signalservice/api/push/ACI;", "getPni", "()Lorg/whispersystems/signalservice/api/push/PNI;", "bestServiceId", "Lorg/whispersystems/signalservice/api/push/ServiceId;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CdsV2Result {
        private final ACI aci;
        private final PNI pni;

        public static /* synthetic */ CdsV2Result copy$default(CdsV2Result cdsV2Result, PNI pni, ACI aci, int i, Object obj) {
            if ((i & 1) != 0) {
                pni = cdsV2Result.pni;
            }
            if ((i & 2) != 0) {
                aci = cdsV2Result.aci;
            }
            return cdsV2Result.copy(pni, aci);
        }

        public final PNI component1() {
            return this.pni;
        }

        public final ACI component2() {
            return this.aci;
        }

        public final CdsV2Result copy(PNI pni, ACI aci) {
            Intrinsics.checkNotNullParameter(pni, RecipientDatabase.PNI_COLUMN);
            return new CdsV2Result(pni, aci);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CdsV2Result)) {
                return false;
            }
            CdsV2Result cdsV2Result = (CdsV2Result) obj;
            return Intrinsics.areEqual(this.pni, cdsV2Result.pni) && Intrinsics.areEqual(this.aci, cdsV2Result.aci);
        }

        public int hashCode() {
            int hashCode = this.pni.hashCode() * 31;
            ACI aci = this.aci;
            return hashCode + (aci == null ? 0 : aci.hashCode());
        }

        public String toString() {
            return "CdsV2Result(pni=" + this.pni + ", aci=" + this.aci + ')';
        }

        public CdsV2Result(PNI pni, ACI aci) {
            Intrinsics.checkNotNullParameter(pni, RecipientDatabase.PNI_COLUMN);
            this.pni = pni;
            this.aci = aci;
        }

        public final PNI getPni() {
            return this.pni;
        }

        public final ACI getAci() {
            return this.aci;
        }

        public final ServiceId bestServiceId() {
            ACI aci = this.aci;
            return aci != null ? aci : this.pni;
        }
    }
}
