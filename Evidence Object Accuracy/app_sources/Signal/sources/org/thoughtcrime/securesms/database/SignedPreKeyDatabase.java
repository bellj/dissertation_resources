package org.thoughtcrime.securesms.database;

import android.content.Context;
import android.database.Cursor;
import androidx.core.content.ContentValuesKt;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.CursorExtensionsKt;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPrivateKey;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.state.SignedPreKeyRecord;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.push.ServiceId;

/* compiled from: SignedPreKeyDatabase.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0004\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0018\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00102\u0006\u0010\t\u001a\u00020\nJ\u001e\u0010\u0011\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\u000e¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/database/SignedPreKeyDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "delete", "", "serviceId", "Lorg/whispersystems/signalservice/api/push/ServiceId;", "keyId", "", "get", "Lorg/signal/libsignal/protocol/state/SignedPreKeyRecord;", "getAll", "", "insert", "record", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SignedPreKeyDatabase extends Database {
    public static final String ACCOUNT_ID;
    public static final String CREATE_TABLE;
    public static final Companion Companion = new Companion(null);
    public static final String ID;
    public static final String KEY_ID;
    public static final String PRIVATE_KEY;
    public static final String PUBLIC_KEY;
    public static final String SIGNATURE;
    public static final String TABLE_NAME;
    private static final String TAG = Log.tag(SignedPreKeyDatabase.class);
    public static final String TIMESTAMP;

    /* compiled from: SignedPreKeyDatabase.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\n \r*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/database/SignedPreKeyDatabase$Companion;", "", "()V", "ACCOUNT_ID", "", "CREATE_TABLE", "ID", "KEY_ID", "PRIVATE_KEY", "PUBLIC_KEY", "SIGNATURE", "TABLE_NAME", "TAG", "kotlin.jvm.PlatformType", "TIMESTAMP", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SignedPreKeyDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.Throwable, org.signal.libsignal.protocol.state.SignedPreKeyRecord] */
    public final SignedPreKeyRecord get(ServiceId serviceId, int i) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Cursor query = getReadableDatabase().query(TABLE_NAME, null, "account_id = ? AND key_id = ?", SqlUtil.buildArgs(serviceId, Integer.valueOf(i)), null, null, null);
        try {
            th = 0;
            if (query.moveToFirst()) {
                try {
                    Intrinsics.checkNotNullExpressionValue(query, "cursor");
                    ECPublicKey decodePoint = Curve.decodePoint(Base64.decode(CursorExtensionsKt.requireNonNullString(query, "public_key")), 0);
                    ECPrivateKey decodePrivatePoint = Curve.decodePrivatePoint(Base64.decode(CursorExtensionsKt.requireNonNullString(query, "private_key")));
                    byte[] decode = Base64.decode(CursorExtensionsKt.requireNonNullString(query, SIGNATURE));
                    Intrinsics.checkNotNullExpressionValue(decode, "decode(cursor.requireNonNullString(SIGNATURE))");
                    return new SignedPreKeyRecord(i, CursorExtensionsKt.requireLong(query, "timestamp"), new ECKeyPair(decodePoint, decodePrivatePoint), decode);
                } catch (IOException e) {
                    Log.w(TAG, e);
                } catch (InvalidKeyException e2) {
                    Log.w(TAG, e2);
                }
            }
            Unit unit = Unit.INSTANCE;
            return th;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final List<SignedPreKeyRecord> getAll(ServiceId serviceId) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        LinkedList linkedList = new LinkedList();
        Cursor query = getReadableDatabase().query(TABLE_NAME, null, "account_id = ?", SqlUtil.buildArgs(serviceId), null, null, null);
        while (query.moveToNext()) {
            try {
                try {
                    Intrinsics.checkNotNullExpressionValue(query, "cursor");
                    int requireInt = CursorExtensionsKt.requireInt(query, "key_id");
                    ECPublicKey decodePoint = Curve.decodePoint(Base64.decode(CursorExtensionsKt.requireNonNullString(query, "public_key")), 0);
                    ECPrivateKey decodePrivatePoint = Curve.decodePrivatePoint(Base64.decode(CursorExtensionsKt.requireNonNullString(query, "private_key")));
                    byte[] decode = Base64.decode(CursorExtensionsKt.requireNonNullString(query, SIGNATURE));
                    Intrinsics.checkNotNullExpressionValue(decode, "decode(cursor.requireNonNullString(SIGNATURE))");
                    linkedList.add(new SignedPreKeyRecord(requireInt, CursorExtensionsKt.requireLong(query, "timestamp"), new ECKeyPair(decodePoint, decodePrivatePoint), decode));
                } catch (IOException e) {
                    Log.w(TAG, e);
                } catch (InvalidKeyException e2) {
                    Log.w(TAG, e2);
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return linkedList;
    }

    public final void insert(ServiceId serviceId, int i, SignedPreKeyRecord signedPreKeyRecord) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        Intrinsics.checkNotNullParameter(signedPreKeyRecord, "record");
        getWritableDatabase().replace(TABLE_NAME, null, ContentValuesKt.contentValuesOf(TuplesKt.to("account_id", serviceId.toString()), TuplesKt.to("key_id", Integer.valueOf(i)), TuplesKt.to("public_key", Base64.encodeBytes(signedPreKeyRecord.getKeyPair().getPublicKey().serialize())), TuplesKt.to("private_key", Base64.encodeBytes(signedPreKeyRecord.getKeyPair().getPrivateKey().serialize())), TuplesKt.to(SIGNATURE, Base64.encodeBytes(signedPreKeyRecord.getSignature())), TuplesKt.to("timestamp", Long.valueOf(signedPreKeyRecord.getTimestamp()))));
    }

    public final void delete(ServiceId serviceId, int i) {
        Intrinsics.checkNotNullParameter(serviceId, "serviceId");
        getWritableDatabase().delete(TABLE_NAME, "account_id = ? AND key_id = ?", SqlUtil.buildArgs(serviceId, Integer.valueOf(i)));
    }
}
