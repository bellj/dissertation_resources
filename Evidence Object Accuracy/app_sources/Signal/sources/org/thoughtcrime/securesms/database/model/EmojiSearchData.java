package org.thoughtcrime.securesms.database.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/* loaded from: classes.dex */
public final class EmojiSearchData {
    @JsonProperty
    private String emoji;
    @JsonProperty
    private List<String> tags;

    public String getEmoji() {
        return this.emoji;
    }

    public List<String> getTags() {
        return this.tags;
    }
}
