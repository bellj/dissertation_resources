package org.thoughtcrime.securesms.database.documents;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashSet;
import java.util.Set;

/* loaded from: classes.dex */
public class NetworkFailureSet implements Document<NetworkFailure> {
    @JsonProperty("l")
    private Set<NetworkFailure> failures;

    public NetworkFailureSet() {
        this.failures = new HashSet();
    }

    public NetworkFailureSet(Set<NetworkFailure> set) {
        this.failures = set;
    }

    @Override // org.thoughtcrime.securesms.database.documents.Document
    public int size() {
        Set<NetworkFailure> set = this.failures;
        if (set == null) {
            return 0;
        }
        return set.size();
    }

    @Override // org.thoughtcrime.securesms.database.documents.Document
    @JsonIgnore
    public Set<NetworkFailure> getItems() {
        return this.failures;
    }
}
