package org.thoughtcrime.securesms.database;

import java.util.HashMap;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.LRUCache;

/* loaded from: classes4.dex */
public class EarlyReceiptCache {
    private static final String TAG = Log.tag(EarlyReceiptCache.class);
    private final LRUCache<Long, Map<RecipientId, Receipt>> cache = new LRUCache<>(100);
    private final String name;

    public EarlyReceiptCache(String str) {
        this.name = str;
    }

    public synchronized void increment(long j, RecipientId recipientId, long j2) {
        Map<RecipientId, Receipt> map = this.cache.get(Long.valueOf(j));
        if (map == null) {
            map = new HashMap<>();
        }
        Receipt receipt = map.get(recipientId);
        if (receipt != null) {
            Receipt.access$008(receipt);
            receipt.timestamp = j2;
        } else {
            receipt = new Receipt(1, j2);
        }
        map.put(recipientId, receipt);
        this.cache.put(Long.valueOf(j), map);
    }

    public synchronized Map<RecipientId, Receipt> remove(long j) {
        Map<RecipientId, Receipt> remove;
        remove = this.cache.remove(Long.valueOf(j));
        if (remove == null) {
            remove = new HashMap<>();
        }
        return remove;
    }

    /* loaded from: classes4.dex */
    public class Receipt {
        private long count;
        private long timestamp;

        static /* synthetic */ long access$008(Receipt receipt) {
            long j = receipt.count;
            receipt.count = 1 + j;
            return j;
        }

        private Receipt(long j, long j2) {
            EarlyReceiptCache.this = r1;
            this.count = j;
            this.timestamp = j2;
        }

        public long getCount() {
            return this.count;
        }

        public long getTimestamp() {
            return this.timestamp;
        }
    }
}
