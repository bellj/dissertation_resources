package org.thoughtcrime.securesms.database.model;

import org.thoughtcrime.securesms.megaphone.Megaphones;

/* loaded from: classes4.dex */
public class MegaphoneRecord {
    private final Megaphones.Event event;
    private final boolean finished;
    private final long firstVisible;
    private final long lastSeen;
    private final int seenCount;

    public MegaphoneRecord(Megaphones.Event event, int i, long j, long j2, boolean z) {
        this.event = event;
        this.seenCount = i;
        this.lastSeen = j;
        this.firstVisible = j2;
        this.finished = z;
    }

    public Megaphones.Event getEvent() {
        return this.event;
    }

    public int getSeenCount() {
        return this.seenCount;
    }

    public long getLastSeen() {
        return this.lastSeen;
    }

    public long getFirstVisible() {
        return this.firstVisible;
    }

    public boolean isFinished() {
        return this.finished;
    }
}
