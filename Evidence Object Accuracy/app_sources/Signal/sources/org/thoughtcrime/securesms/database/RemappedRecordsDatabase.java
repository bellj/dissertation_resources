package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.signal.core.util.CursorUtil;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class RemappedRecordsDatabase extends Database {
    public static final String[] CREATE_TABLE = {"CREATE TABLE remapped_recipients (_id INTEGER PRIMARY KEY AUTOINCREMENT, old_id INTEGER UNIQUE, new_id INTEGER)", "CREATE TABLE remapped_threads (_id INTEGER PRIMARY KEY AUTOINCREMENT, old_id INTEGER UNIQUE, new_id INTEGER)"};

    /* loaded from: classes4.dex */
    private static class SharedColumns {
        protected static final String ID;
        protected static final String NEW_ID;
        protected static final String OLD_ID;

        private SharedColumns() {
        }
    }

    /* loaded from: classes4.dex */
    private static final class Recipients extends SharedColumns {
        private static final String CREATE_TABLE;
        private static final String TABLE_NAME;

        private Recipients() {
            super();
        }
    }

    /* loaded from: classes4.dex */
    private static final class Threads extends SharedColumns {
        private static final String CREATE_TABLE;
        private static final String TABLE_NAME;

        private Threads() {
            super();
        }
    }

    public RemappedRecordsDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public Map<RecipientId, RecipientId> getAllRecipientMappings() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        HashMap hashMap = new HashMap();
        signalReadableDatabase.beginTransaction();
        try {
            for (Mapping mapping : getAllMappings("remapped_recipients")) {
                hashMap.put(RecipientId.from(mapping.getOldId()), RecipientId.from(mapping.getNewId()));
            }
            signalReadableDatabase.setTransactionSuccessful();
            return hashMap;
        } finally {
            signalReadableDatabase.endTransaction();
        }
    }

    public Map<Long, Long> getAllThreadMappings() {
        SQLiteDatabase signalReadableDatabase = this.databaseHelper.getSignalReadableDatabase();
        HashMap hashMap = new HashMap();
        signalReadableDatabase.beginTransaction();
        try {
            for (Mapping mapping : getAllMappings("remapped_threads")) {
                hashMap.put(Long.valueOf(mapping.getOldId()), Long.valueOf(mapping.getNewId()));
            }
            signalReadableDatabase.setTransactionSuccessful();
            return hashMap;
        } finally {
            signalReadableDatabase.endTransaction();
        }
    }

    public void addRecipientMapping(RecipientId recipientId, RecipientId recipientId2) {
        addMapping("remapped_recipients", new Mapping(recipientId.toLong(), recipientId2.toLong()));
    }

    public void addThreadMapping(long j, long j2) {
        addMapping("remapped_threads", new Mapping(j, j2));
    }

    public Cursor getAllRecipients() {
        return this.databaseHelper.getSignalReadableDatabase().query("remapped_recipients", null, null, null, null, null, null);
    }

    public Cursor getAllThreads() {
        return this.databaseHelper.getSignalReadableDatabase().query("remapped_threads", null, null, null, null, null, null);
    }

    private List<Mapping> getAllMappings(String str) {
        LinkedList linkedList = new LinkedList();
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(str, null, null, null, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                linkedList.add(new Mapping(CursorUtil.requireLong(query, "old_id"), CursorUtil.requireLong(query, "new_id")));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return linkedList;
    }

    private void addMapping(String str, Mapping mapping) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("old_id", Long.valueOf(mapping.getOldId()));
        contentValues.put("new_id", Long.valueOf(mapping.getNewId()));
        this.databaseHelper.getSignalWritableDatabase().insert(str, (String) null, contentValues);
    }

    /* loaded from: classes4.dex */
    public static final class Mapping {
        private final long newId;
        private final long oldId;

        public Mapping(long j, long j2) {
            this.oldId = j;
            this.newId = j2;
        }

        public long getOldId() {
            return this.oldId;
        }

        public long getNewId() {
            return this.newId;
        }
    }
}
