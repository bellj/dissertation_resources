package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import com.google.android.mms.pdu_alt.NotificationInd;
import j$.util.Optional;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import net.zetetic.database.sqlcipher.SQLiteStatement;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.documents.Document;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatchSet;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.SmsMessageRecord;
import org.thoughtcrime.securesms.database.model.StoryResult;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.groups.GroupMigrationMembershipChange;
import org.thoughtcrime.securesms.insights.InsightsConstants;
import org.thoughtcrime.securesms.mms.IncomingMediaMessage;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.revealable.ViewOnceExpirationInfo;
import org.thoughtcrime.securesms.sms.IncomingTextMessage;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* loaded from: classes4.dex */
public abstract class MessageDatabase extends Database implements MmsSmsColumns {
    private static final String TAG = Log.tag(MessageDatabase.class);
    protected static final String[] THREAD_ID_PROJECTION = {"thread_id"};
    protected static final String THREAD_ID_WHERE;

    /* loaded from: classes4.dex */
    public interface InsertListener {
        void onComplete();
    }

    /* loaded from: classes4.dex */
    public interface Reader extends Closeable, Iterable<MessageRecord> {
        @Override // java.io.Closeable, java.lang.AutoCloseable
        void close();

        @Deprecated
        MessageRecord getCurrent();

        @Deprecated
        MessageRecord getNext();
    }

    public abstract void addFailures(long j, List<NetworkFailure> list);

    public abstract SQLiteDatabase beginTransaction();

    public abstract void clearRateLimitStatus(Collection<Long> collection);

    public abstract boolean containsStories(long j);

    public abstract SQLiteStatement createInsertStatement(SQLiteDatabase sQLiteDatabase);

    abstract void deleteAbandonedMessages();

    abstract void deleteAllThreads();

    public abstract void deleteGroupStoryReplies(long j);

    public abstract boolean deleteMessage(long j);

    abstract int deleteMessagesInThreadBeforeDate(long j, long j2);

    public abstract int deleteStoriesOlderThan(long j, boolean z);

    abstract void deleteThread(long j);

    abstract void deleteThreads(Set<Long> set);

    public abstract void endTransaction();

    public abstract void endTransaction(SQLiteDatabase sQLiteDatabase);

    public abstract void ensureMigration();

    public abstract Reader getAllOutgoingStories(boolean z, int i);

    public abstract Reader getAllOutgoingStoriesAt(long j);

    public abstract Set<Long> getAllRateLimitedMessageIds();

    public abstract Reader getAllStoriesFor(RecipientId recipientId, int i);

    protected abstract String getDateReceivedColumnName();

    protected abstract String getDateSentColumnName();

    public abstract Cursor getExpirationStartedMessages();

    public abstract long getLatestGroupQuitTimestamp(long j, long j2);

    public abstract int getMessageCountForThread(long j);

    public abstract int getMessageCountForThread(long j, long j2);

    public abstract Cursor getMessageCursor(long j);

    public abstract MessageRecord getMessageRecord(long j) throws NoSuchMessageException;

    public abstract MessageRecord getMessageRecordOrNull(long j);

    public abstract Reader getMessages(Collection<Long> collection);

    public abstract List<MessageRecord> getMessagesInThreadAfterInclusive(long j, long j2, long j3);

    public abstract ViewOnceExpirationInfo getNearestExpiringViewOnceMessage();

    public abstract Optional<MmsNotificationInfo> getNotification(long j);

    public abstract int getNumberOfStoryReplies(long j);

    public abstract RecipientId getOldestGroupUpdateSender(long j, long j2);

    public abstract Long getOldestStorySendTimestamp(boolean z);

    public abstract Pair<RecipientId, Long> getOldestUnreadMentionDetails(long j);

    public abstract List<StoryResult> getOrderedStoryRecipientsAndIds(boolean z);

    public abstract OutgoingMediaMessage getOutgoingMessage(long j) throws MmsException, NoSuchMessageException;

    public abstract Reader getOutgoingStoriesTo(RecipientId recipientId);

    public abstract ParentStoryId.GroupReply getParentStoryIdForGroupReply(long j);

    public abstract List<MessageRecord> getProfileChangeDetailsRecords(long j, long j2);

    public abstract SmsMessageRecord getSmsMessage(long j) throws NoSuchMessageException;

    public abstract MessageId getStoryId(RecipientId recipientId, long j) throws NoSuchMessageException;

    public abstract Cursor getStoryReplies(long j);

    public abstract StoryViewState getStoryViewState(RecipientId recipientId);

    protected abstract String getTableName();

    public abstract long getThreadIdForMessage(long j);

    protected abstract String getTypeField();

    public abstract int getUnreadMentionCount(long j);

    public abstract Reader getUnreadStories(RecipientId recipientId, int i);

    public abstract List<RecipientId> getUnreadStoryThreadRecipientIds();

    public abstract List<MarkedMessageInfo> getViewedIncomingMessages(long j);

    public abstract boolean hasMeaningfulMessage(long j);

    public abstract boolean hasReceivedAnyCallsSince(long j, long j2);

    public abstract boolean hasSelfReplyInGroupStory(long j);

    public abstract boolean hasSelfReplyInStory(long j);

    public abstract Set<MessageUpdate> incrementReceiptCount(SyncMessageId syncMessageId, long j, ReceiptType receiptType, boolean z);

    public abstract void insertBadDecryptMessage(RecipientId recipientId, int i, long j, long j2, long j3);

    public abstract void insertBoostRequestMessage(RecipientId recipientId, long j);

    public abstract InsertResult insertChatSessionRefreshedMessage(RecipientId recipientId, long j, long j2);

    public abstract void insertGroupV1MigrationEvents(RecipientId recipientId, long j, GroupMigrationMembershipChange groupMigrationMembershipChange);

    public abstract Optional<InsertResult> insertMessageInbox(IncomingMediaMessage incomingMediaMessage, String str, long j) throws MmsException;

    public abstract Optional<InsertResult> insertMessageInbox(IncomingTextMessage incomingTextMessage);

    public abstract Optional<InsertResult> insertMessageInbox(IncomingTextMessage incomingTextMessage, long j);

    public abstract Pair<Long, Long> insertMessageInbox(NotificationInd notificationInd, int i);

    public abstract long insertMessageOutbox(long j, OutgoingTextMessage outgoingTextMessage, boolean z, long j2, InsertListener insertListener);

    public abstract long insertMessageOutbox(OutgoingMediaMessage outgoingMediaMessage, long j, boolean z, int i, InsertListener insertListener) throws MmsException;

    public abstract long insertMessageOutbox(OutgoingMediaMessage outgoingMediaMessage, long j, boolean z, InsertListener insertListener) throws MmsException;

    public abstract Pair<Long, Long> insertMissedCall(RecipientId recipientId, long j, boolean z);

    public abstract void insertNumberChangeMessages(Recipient recipient);

    public abstract void insertOrUpdateGroupCall(RecipientId recipientId, RecipientId recipientId2, long j, String str);

    public abstract void insertOrUpdateGroupCall(RecipientId recipientId, RecipientId recipientId2, long j, String str, Collection<UUID> collection, boolean z);

    public abstract Pair<Long, Long> insertOutgoingCall(RecipientId recipientId, boolean z);

    public abstract void insertProfileNameChangeMessages(Recipient recipient, String str, String str2);

    public abstract Pair<Long, Long> insertReceivedCall(RecipientId recipientId, boolean z);

    public abstract Optional<InsertResult> insertSecureDecryptedMessageInbox(IncomingMediaMessage incomingMediaMessage, long j) throws MmsException;

    public abstract boolean isGroupQuitMessage(long j);

    public abstract boolean isOutgoingStoryAlreadyInDatabase(RecipientId recipientId, long j);

    public abstract boolean isSent(long j);

    public abstract boolean isStory(long j);

    public abstract void markAsDecryptFailed(long j);

    public abstract void markAsEndSession(long j);

    public abstract void markAsForcedSms(long j);

    public abstract void markAsInsecure(long j);

    public abstract void markAsInvalidMessage(long j);

    public abstract void markAsInvalidVersionKeyExchange(long j);

    public abstract void markAsLegacyVersion(long j);

    public abstract void markAsMissedCall(long j, boolean z);

    public abstract void markAsNoSession(long j);

    public abstract void markAsNotified(long j);

    public abstract void markAsOutbox(long j);

    public abstract void markAsPendingInsecureSmsFallback(long j);

    public abstract void markAsPush(long j);

    public abstract void markAsRateLimited(long j);

    public abstract void markAsRemoteDelete(long j);

    public abstract void markAsSecure(long j);

    public abstract void markAsSending(long j);

    public abstract void markAsSent(long j, boolean z);

    public abstract void markAsSentFailed(long j);

    public abstract void markAsUnsupportedProtocolVersion(long j);

    public abstract void markDownloadState(long j, long j2);

    public abstract void markExpireStarted(long j);

    public abstract void markExpireStarted(long j, long j2);

    public abstract void markExpireStarted(Collection<Long> collection, long j);

    public abstract void markGiftRedemptionCompleted(long j);

    public abstract void markGiftRedemptionFailed(long j);

    public abstract void markGiftRedemptionStarted(long j);

    public abstract void markIncomingNotificationReceived(long j);

    public abstract void markSmsStatus(long j, int i);

    public abstract void markUnidentified(long j, boolean z);

    public abstract List<MarkedMessageInfo> setAllMessagesRead();

    public abstract List<MarkedMessageInfo> setEntireThreadRead(long j);

    public abstract List<MarkedMessageInfo> setGroupStoryMessagesReadSince(long j, long j2, long j3);

    public abstract MarkedMessageInfo setIncomingMessageViewed(long j);

    public abstract List<MarkedMessageInfo> setIncomingMessagesViewed(List<Long> list);

    public abstract List<MarkedMessageInfo> setMessagesReadSince(long j, long j2);

    public abstract void setNetworkFailures(long j, Set<NetworkFailure> set);

    public abstract List<MarkedMessageInfo> setOutgoingGiftsRevealed(List<Long> list);

    abstract MmsSmsDatabase.TimestampReadResult setTimestampRead(SyncMessageId syncMessageId, long j, Map<Long, Long> map);

    public abstract void setTransactionSuccessful();

    public abstract InsertResult updateBundleMessageBody(long j, String str);

    public abstract boolean updatePreviousGroupCall(long j, String str, Collection<UUID> collection, boolean z);

    public abstract void updateViewedStories(Set<SyncMessageId> set);

    public MessageDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
    }

    public final String getOutgoingTypeClause() {
        long[] jArr = MmsSmsColumns.Types.OUTGOING_MESSAGE_TYPES;
        ArrayList arrayList = new ArrayList(jArr.length);
        for (long j : jArr) {
            arrayList.add("(" + getTypeField() + " & 31 = " + j + ")");
        }
        return Util.join((Collection) arrayList, " OR ");
    }

    public final int getInsecureMessagesSentForThread(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(getTableName(), new String[]{"COUNT(*)"}, "thread_id = ? AND " + getOutgoingInsecureMessageClause() + " AND " + getDateSentColumnName() + " > ?", new String[]{String.valueOf(j), String.valueOf(System.currentTimeMillis() - InsightsConstants.PERIOD_IN_MILLIS)}, null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    public final int getInsecureMessageCountForInsights() {
        return getMessageCountForRecipientsAndType(getOutgoingInsecureMessageClause());
    }

    public final int getSecureMessageCountForInsights() {
        return getMessageCountForRecipientsAndType(getOutgoingSecureMessageClause());
    }

    public final int getSecureMessageCount(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(getTableName(), new String[]{"COUNT(*)"}, getSecureMessageClause() + "AND thread_id = ?", new String[]{String.valueOf(j)}, null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    public final int getOutgoingSecureMessageCount(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(getTableName(), new String[]{"COUNT(*)"}, getOutgoingSecureMessageClause() + "AND thread_id = ? AND (" + getTypeField() + " & 131072 = 0 OR " + getTypeField() + " & 524288 = 524288)", new String[]{String.valueOf(j)}, null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    private int getMessageCountForRecipientsAndType(String str) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(getTableName(), new String[]{"COUNT(*)"}, str + " AND " + getDateSentColumnName() + " > ?", new String[]{String.valueOf(System.currentTimeMillis() - InsightsConstants.PERIOD_IN_MILLIS)}, null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    query.close();
                    return i;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return 0;
    }

    private String getOutgoingInsecureMessageClause() {
        return "(" + getTypeField() + " & 31) = 23 AND NOT (" + getTypeField() + " & 8388608)";
    }

    private String getOutgoingSecureMessageClause() {
        return "(" + getTypeField() + " & 31) = 23 AND (" + getTypeField() + " & 10485760)";
    }

    private String getSecureMessageClause() {
        return String.format(Locale.ENGLISH, "(%s OR %s) AND %s", "(" + getTypeField() + " & 31) = 23", "(" + getTypeField() + " & 31) = 20", "(" + getTypeField() + " & 10485760)");
    }

    public void setReactionsSeen(long j, long j2) {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] strArr = {String.valueOf(j), SubscriptionLevels.BOOST_LEVEL};
        String str = "thread_id = ? AND reactions_unread = ?";
        if (j2 > -1) {
            str = str + " AND " + getDateReceivedColumnName() + " <= " + j2;
        }
        contentValues.put(MmsSmsColumns.REACTIONS_UNREAD, (Integer) 0);
        contentValues.put(MmsSmsColumns.REACTIONS_LAST_SEEN, Long.valueOf(System.currentTimeMillis()));
        signalWritableDatabase.update(getTableName(), contentValues, str, strArr);
    }

    public void setAllReactionsSeen() {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MmsSmsColumns.REACTIONS_UNREAD, (Integer) 0);
        contentValues.put(MmsSmsColumns.REACTIONS_LAST_SEEN, Long.valueOf(System.currentTimeMillis()));
        signalWritableDatabase.update(getTableName(), contentValues, "reactions_unread != ?", new String[]{"0"});
    }

    public void setNotifiedTimestamp(long j, List<Long> list) {
        if (!list.isEmpty()) {
            SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
            SqlUtil.Query buildSingleCollectionQuery = SqlUtil.buildSingleCollectionQuery("_id", list);
            ContentValues contentValues = new ContentValues();
            contentValues.put(MmsSmsColumns.NOTIFIED_TIMESTAMP, Long.valueOf(j));
            signalWritableDatabase.update(getTableName(), contentValues, buildSingleCollectionQuery.getWhere(), buildSingleCollectionQuery.getWhereArgs());
        }
    }

    public void addMismatchedIdentity(long j, RecipientId recipientId, IdentityKey identityKey) {
        try {
            addToDocument(j, MmsSmsColumns.MISMATCHED_IDENTITIES, (String) new IdentityKeyMismatch(recipientId, identityKey), IdentityKeyMismatchSet.class);
        } catch (IOException e) {
            Log.w(TAG, e);
        }
    }

    public void removeMismatchedIdentity(long j, RecipientId recipientId, IdentityKey identityKey) {
        try {
            removeFromDocument(j, MmsSmsColumns.MISMATCHED_IDENTITIES, new IdentityKeyMismatch(recipientId, identityKey), IdentityKeyMismatchSet.class);
        } catch (IOException e) {
            Log.w(TAG, e);
        }
    }

    public void setMismatchedIdentities(long j, Set<IdentityKeyMismatch> set) {
        try {
            setDocument(this.databaseHelper.getSignalWritableDatabase(), j, MmsSmsColumns.MISMATCHED_IDENTITIES, new IdentityKeyMismatchSet(set));
        } catch (IOException e) {
            Log.w(TAG, e);
        }
    }

    public List<ReportSpamData> getReportSpamMessageServerGuids(long j, long j2) {
        String[] buildArgs = SqlUtil.buildArgs(Long.valueOf(j), Long.valueOf(j2));
        ArrayList arrayList = new ArrayList();
        String tableName = getTableName();
        String[] strArr = {"address", "server_guid", getDateReceivedColumnName()};
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(tableName, strArr, "thread_id = ? AND " + getDateReceivedColumnName() + " <= ?", buildArgs, null, null, getDateReceivedColumnName() + " DESC", "3");
        while (query.moveToNext()) {
            try {
                RecipientId from = RecipientId.from(CursorUtil.requireLong(query, "address"));
                String requireString = CursorUtil.requireString(query, "server_guid");
                long requireLong = CursorUtil.requireLong(query, getDateReceivedColumnName());
                if (!Util.isEmpty(requireString)) {
                    arrayList.add(new ReportSpamData(from, requireString, requireLong));
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        query.close();
        return arrayList;
    }

    public void updateReactionsUnread(SQLiteDatabase sQLiteDatabase, long j, boolean z, boolean z2) {
        try {
            boolean isOutgoing = getMessageRecord(j).isOutgoing();
            ContentValues contentValues = new ContentValues();
            if (!z) {
                contentValues.put(MmsSmsColumns.REACTIONS_UNREAD, (Integer) 0);
            } else if (!z2) {
                contentValues.put(MmsSmsColumns.REACTIONS_UNREAD, (Integer) 1);
            }
            if (isOutgoing && z) {
                contentValues.put(MmsSmsColumns.NOTIFIED, (Integer) 0);
            }
            if (contentValues.size() > 0) {
                sQLiteDatabase.update(getTableName(), contentValues, "_id = ?", SqlUtil.buildArgs(j));
            }
        } catch (NoSuchMessageException unused) {
            String str = TAG;
            Log.w(str, "Failed to find message " + j);
        }
    }

    protected <D extends Document<I>, I> void removeFromDocument(long j, String str, I i, Class<D> cls) throws IOException {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            Document document = getDocument(signalWritableDatabase, j, str, cls);
            Iterator it = document.getItems().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().equals(i)) {
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
            setDocument(signalWritableDatabase, j, str, document);
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    protected <T extends Document<I>, I> void addToDocument(long j, String str, I i, Class<T> cls) throws IOException {
        addToDocument(j, str, (List) new ArrayList<I>(i) { // from class: org.thoughtcrime.securesms.database.MessageDatabase.1
            final /* synthetic */ Object val$object;

            {
                this.val$object = r2;
                add(r2);
            }
        }, (Class) cls);
    }

    public <T extends Document<I>, I> void addToDocument(long j, String str, List<I> list, Class<T> cls) throws IOException {
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        signalWritableDatabase.beginTransaction();
        try {
            Document document = getDocument(signalWritableDatabase, j, str, cls);
            document.getItems().addAll(list);
            setDocument(signalWritableDatabase, j, str, document);
            signalWritableDatabase.setTransactionSuccessful();
        } finally {
            signalWritableDatabase.endTransaction();
        }
    }

    public void setDocument(SQLiteDatabase sQLiteDatabase, long j, String str, Document document) throws IOException {
        ContentValues contentValues = new ContentValues();
        if (document == null || document.size() == 0) {
            contentValues.put(str, (String) null);
        } else {
            contentValues.put(str, JsonUtils.toJson(document));
        }
        sQLiteDatabase.update(getTableName(), contentValues, "_id = ?", new String[]{String.valueOf(j)});
    }

    private <D extends Document> D getDocument(SQLiteDatabase sQLiteDatabase, long j, String str, Class<D> cls) {
        Cursor cursor = null;
        try {
            cursor = sQLiteDatabase.query(getTableName(), new String[]{str}, "_id = ?", new String[]{String.valueOf(j)}, null, null, null);
            if (cursor != null && cursor.moveToNext()) {
                String string = cursor.getString(cursor.getColumnIndexOrThrow(str));
                try {
                    if (!TextUtils.isEmpty(string)) {
                        D d = (D) ((Document) JsonUtils.fromJson(string, cls));
                        cursor.close();
                        return d;
                    }
                } catch (IOException e) {
                    Log.w(TAG, e);
                }
            }
            try {
                try {
                    return cls.newInstance();
                } catch (IllegalAccessException e2) {
                    throw new AssertionError(e2);
                }
            } catch (InstantiationException e3) {
                throw new AssertionError(e3);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private long getThreadId(SQLiteDatabase sQLiteDatabase, long j) {
        Cursor query = sQLiteDatabase.query(getTableName(), new String[]{"thread_id"}, "_id = ?", new String[]{String.valueOf(j)}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    long j2 = query.getLong(query.getColumnIndexOrThrow("thread_id"));
                    query.close();
                    return j2;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query == null) {
            return -1;
        }
        query.close();
        return -1;
    }

    /* access modifiers changed from: protected */
    /* loaded from: classes4.dex */
    public enum ReceiptType {
        READ("read_receipt_count", 2),
        DELIVERY("delivery_receipt_count", 1),
        VIEWED(MmsSmsColumns.VIEWED_RECEIPT_COUNT, 3);
        
        private final String columnName;
        private final int groupStatus;

        ReceiptType(String str, int i) {
            this.columnName = str;
            this.groupStatus = i;
        }

        public String getColumnName() {
            return this.columnName;
        }

        public int getGroupStatus() {
            return this.groupStatus;
        }
    }

    /* loaded from: classes4.dex */
    public static class SyncMessageId {
        private final RecipientId recipientId;
        private final long timetamp;

        public SyncMessageId(RecipientId recipientId, long j) {
            this.recipientId = recipientId;
            this.timetamp = j;
        }

        public RecipientId getRecipientId() {
            return this.recipientId;
        }

        public long getTimetamp() {
            return this.timetamp;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            SyncMessageId syncMessageId = (SyncMessageId) obj;
            if (this.timetamp != syncMessageId.timetamp || !Objects.equals(this.recipientId, syncMessageId.recipientId)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return Objects.hash(this.recipientId, Long.valueOf(this.timetamp));
        }
    }

    /* loaded from: classes4.dex */
    public static class ExpirationInfo {
        private final long expireStarted;
        private final long expiresIn;
        private final long id;
        private final boolean mms;

        public ExpirationInfo(long j, long j2, long j3, boolean z) {
            this.id = j;
            this.expiresIn = j2;
            this.expireStarted = j3;
            this.mms = z;
        }

        public long getId() {
            return this.id;
        }

        public long getExpiresIn() {
            return this.expiresIn;
        }

        public long getExpireStarted() {
            return this.expireStarted;
        }

        public boolean isMms() {
            return this.mms;
        }
    }

    /* loaded from: classes4.dex */
    public static class MarkedMessageInfo {
        private final ExpirationInfo expirationInfo;
        private final MessageId messageId;
        private final SyncMessageId syncMessageId;
        private final long threadId;

        public MarkedMessageInfo(long j, SyncMessageId syncMessageId, MessageId messageId, ExpirationInfo expirationInfo) {
            this.threadId = j;
            this.syncMessageId = syncMessageId;
            this.messageId = messageId;
            this.expirationInfo = expirationInfo;
        }

        public long getThreadId() {
            return this.threadId;
        }

        public SyncMessageId getSyncMessageId() {
            return this.syncMessageId;
        }

        public MessageId getMessageId() {
            return this.messageId;
        }

        public ExpirationInfo getExpirationInfo() {
            return this.expirationInfo;
        }
    }

    /* loaded from: classes4.dex */
    public static class InsertResult {
        private final long messageId;
        private final long threadId;

        public InsertResult(long j, long j2) {
            this.messageId = j;
            this.threadId = j2;
        }

        public long getMessageId() {
            return this.messageId;
        }

        public long getThreadId() {
            return this.threadId;
        }
    }

    /* loaded from: classes4.dex */
    public static class MmsNotificationInfo {
        private final String contentLocation;
        private final RecipientId from;
        private final int subscriptionId;
        private final String transactionId;

        public MmsNotificationInfo(RecipientId recipientId, String str, String str2, int i) {
            this.from = recipientId;
            this.contentLocation = str;
            this.transactionId = str2;
            this.subscriptionId = i;
        }

        public String getContentLocation() {
            return this.contentLocation;
        }

        public String getTransactionId() {
            return this.transactionId;
        }

        public int getSubscriptionId() {
            return this.subscriptionId;
        }

        public RecipientId getFrom() {
            return this.from;
        }
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public static class MessageUpdate {
        private final MessageId messageId;
        private final long threadId;

        public MessageUpdate(long j, MessageId messageId) {
            this.threadId = j;
            this.messageId = messageId;
        }

        public long getThreadId() {
            return this.threadId;
        }

        public MessageId getMessageId() {
            return this.messageId;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            MessageUpdate messageUpdate = (MessageUpdate) obj;
            if (this.threadId != messageUpdate.threadId || !this.messageId.equals(messageUpdate.messageId)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return Objects.hash(Long.valueOf(this.threadId), this.messageId);
        }
    }

    /* loaded from: classes4.dex */
    public static class ReportSpamData {
        private final long dateReceived;
        private final RecipientId recipientId;
        private final String serverGuid;

        public ReportSpamData(RecipientId recipientId, String str, long j) {
            this.recipientId = recipientId;
            this.serverGuid = str;
            this.dateReceived = j;
        }

        public RecipientId getRecipientId() {
            return this.recipientId;
        }

        public String getServerGuid() {
            return this.serverGuid;
        }

        public long getDateReceived() {
            return this.dateReceived;
        }
    }
}
