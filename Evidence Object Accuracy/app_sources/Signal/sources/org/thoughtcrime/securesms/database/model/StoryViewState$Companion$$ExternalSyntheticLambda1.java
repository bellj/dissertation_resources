package org.thoughtcrime.securesms.database.model;

import io.reactivex.rxjava3.functions.Function;
import java.util.List;
import org.thoughtcrime.securesms.database.model.StoryViewState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoryViewState$Companion$$ExternalSyntheticLambda1 implements Function {
    @Override // io.reactivex.rxjava3.functions.Function
    public final Object apply(Object obj) {
        return StoryViewState.Companion.m1744getForRecipientId$lambda5((List) obj);
    }
}
