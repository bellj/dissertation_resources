package org.thoughtcrime.securesms.database;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import net.zetetic.database.sqlcipher.SQLiteOpenHelper;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.DatabaseSecret;
import org.thoughtcrime.securesms.crypto.DatabaseSecretProvider;
import org.thoughtcrime.securesms.jobmanager.persistence.ConstraintSpec;
import org.thoughtcrime.securesms.jobmanager.persistence.DependencySpec;
import org.thoughtcrime.securesms.jobmanager.persistence.FullSpec;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;

/* loaded from: classes4.dex */
public class JobDatabase extends SQLiteOpenHelper implements SignalDatabaseOpenHelper {
    private static final String DATABASE_NAME;
    private static final int DATABASE_VERSION;
    private static final String TAG = Log.tag(JobDatabase.class);
    private static volatile JobDatabase instance;
    private final Application application;

    /* loaded from: classes4.dex */
    private static final class Jobs {
        private static final String CREATE_TABLE;
        private static final String CREATE_TIME;
        private static final String FACTORY_KEY;
        private static final String ID;
        private static final String IS_RUNNING;
        private static final String JOB_SPEC_ID;
        private static final String LIFESPAN;
        private static final String MAX_ATTEMPTS;
        private static final String NEXT_RUN_ATTEMPT_TIME;
        private static final String QUEUE_KEY;
        private static final String RUN_ATTEMPT;
        private static final String SERIALIZED_DATA;
        private static final String SERIALIZED_INPUT_DATA;
        private static final String TABLE_NAME;

        private Jobs() {
        }
    }

    /* loaded from: classes4.dex */
    private static final class Constraints {
        private static final String CREATE_TABLE;
        private static final String FACTORY_KEY;
        private static final String ID;
        private static final String JOB_SPEC_ID;
        private static final String TABLE_NAME;

        private Constraints() {
        }
    }

    /* loaded from: classes4.dex */
    private static final class Dependencies {
        private static final String CREATE_TABLE;
        private static final String DEPENDS_ON_JOB_SPEC_ID;
        private static final String ID;
        private static final String JOB_SPEC_ID;
        private static final String TABLE_NAME;

        private Dependencies() {
        }
    }

    public static JobDatabase getInstance(Application application) {
        if (instance == null) {
            synchronized (JobDatabase.class) {
                if (instance == null) {
                    SqlCipherLibraryLoader.load();
                    instance = new JobDatabase(application, DatabaseSecretProvider.getOrCreateDatabaseSecret(application));
                    instance.setWriteAheadLoggingEnabled(true);
                }
            }
        }
        return instance;
    }

    public JobDatabase(Application application, DatabaseSecret databaseSecret) {
        super(application, DATABASE_NAME, databaseSecret.asString(), (SQLiteDatabase.CursorFactory) null, 1, 0, new SqlCipherErrorHandler(DATABASE_NAME), new SqlCipherDatabaseHook());
        this.application = application;
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        String str = TAG;
        Log.i(str, "onCreate()");
        sQLiteDatabase.execSQL("CREATE TABLE job_spec(_id INTEGER PRIMARY KEY AUTOINCREMENT, job_spec_id TEXT UNIQUE, factory_key TEXT, queue_key TEXT, create_time INTEGER, next_run_attempt_time INTEGER, run_attempt INTEGER, max_attempts INTEGER, lifespan INTEGER, serialized_data TEXT, serialized_input_data TEXT DEFAULT NULL, is_running INTEGER)");
        sQLiteDatabase.execSQL("CREATE TABLE constraint_spec(_id INTEGER PRIMARY KEY AUTOINCREMENT, job_spec_id TEXT, factory_key TEXT, UNIQUE(job_spec_id, factory_key))");
        sQLiteDatabase.execSQL("CREATE TABLE dependency_spec(_id INTEGER PRIMARY KEY AUTOINCREMENT, job_spec_id TEXT, depends_on_job_spec_id TEXT, UNIQUE(job_spec_id, depends_on_job_spec_id))");
        if (SignalDatabase.hasTable("job_spec")) {
            Log.i(str, "Found old job_spec table. Migrating data.");
            migrateJobSpecsFromPreviousDatabase(SignalDatabase.getRawDatabase(), sQLiteDatabase);
        }
        if (SignalDatabase.hasTable("constraint_spec")) {
            Log.i(str, "Found old constraint_spec table. Migrating data.");
            migrateConstraintSpecsFromPreviousDatabase(SignalDatabase.getRawDatabase(), sQLiteDatabase);
        }
        if (SignalDatabase.hasTable("dependency_spec")) {
            Log.i(str, "Found old dependency_spec table. Migrating data.");
            migrateDependencySpecsFromPreviousDatabase(SignalDatabase.getRawDatabase(), sQLiteDatabase);
        }
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        String str = TAG;
        Log.i(str, "onUpgrade(" + i + ", " + i2 + ")");
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        Log.i(TAG, "onOpen()");
        sQLiteDatabase.setForeignKeyConstraintsEnabled(true);
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.database.JobDatabase$$ExternalSyntheticLambda7
            @Override // java.lang.Runnable
            public final void run() {
                JobDatabase.m1672$r8$lambda$mPCnH21FLj07HGuBBTTkoK77eM(JobDatabase.this);
            }
        });
    }

    public /* synthetic */ void lambda$onOpen$0() {
        dropTableIfPresent("job_spec");
        dropTableIfPresent("constraint_spec");
        dropTableIfPresent("dependency_spec");
    }

    public synchronized void insertJobs(List<FullSpec> list) {
        if (!Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.database.JobDatabase$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((FullSpec) obj).getJobSpec();
            }
        }).allMatch(new JobDatabase$$ExternalSyntheticLambda3())) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            writableDatabase.beginTransaction();
            for (FullSpec fullSpec : list) {
                insertJobSpec(writableDatabase, fullSpec.getJobSpec());
                insertConstraintSpecs(writableDatabase, fullSpec.getConstraintSpecs());
                insertDependencySpecs(writableDatabase, fullSpec.getDependencySpecs());
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
        }
    }

    public synchronized List<JobSpec> getAllJobSpecs() {
        LinkedList linkedList;
        linkedList = new LinkedList();
        Cursor query = getReadableDatabase().query("job_spec", null, null, null, null, null, "create_time, _id ASC");
        while (query != null && query.moveToNext()) {
            linkedList.add(jobSpecFromCursor(query));
        }
        if (query != null) {
            query.close();
        }
        return linkedList;
    }

    public synchronized void updateJobRunningState(String str, boolean z) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("is_running", Integer.valueOf(z ? 1 : 0));
        getWritableDatabase().update("job_spec", contentValues, "job_spec_id = ?", new String[]{str});
    }

    public synchronized void updateJobAfterRetry(String str, boolean z, int i, long j, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("is_running", Integer.valueOf(z ? 1 : 0));
        contentValues.put("run_attempt", Integer.valueOf(i));
        contentValues.put("next_run_attempt_time", Long.valueOf(j));
        contentValues.put("serialized_data", str2);
        getWritableDatabase().update("job_spec", contentValues, "job_spec_id = ?", new String[]{str});
    }

    public synchronized void updateAllJobsToBePending() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("is_running", (Integer) 0);
        getWritableDatabase().update("job_spec", contentValues, null, null);
    }

    public synchronized void updateJobs(List<JobSpec> list) {
        if (!Stream.of(list).allMatch(new JobDatabase$$ExternalSyntheticLambda3())) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            writableDatabase.beginTransaction();
            Stream.of(list).filterNot(new JobDatabase$$ExternalSyntheticLambda3()).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.database.JobDatabase$$ExternalSyntheticLambda6
                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    JobDatabase.$r8$lambda$BsPVVGZod2nqQD1HmywOGTfvp6Y(SQLiteDatabase.this, (JobSpec) obj);
                }
            });
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
        }
    }

    public static /* synthetic */ void lambda$updateJobs$1(SQLiteDatabase sQLiteDatabase, JobSpec jobSpec) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("job_spec_id", jobSpec.getId());
        contentValues.put("factory_key", jobSpec.getFactoryKey());
        contentValues.put("queue_key", jobSpec.getQueueKey());
        contentValues.put("create_time", Long.valueOf(jobSpec.getCreateTime()));
        contentValues.put("next_run_attempt_time", Long.valueOf(jobSpec.getNextRunAttemptTime()));
        contentValues.put("run_attempt", Integer.valueOf(jobSpec.getRunAttempt()));
        contentValues.put("max_attempts", Integer.valueOf(jobSpec.getMaxAttempts()));
        contentValues.put("lifespan", Long.valueOf(jobSpec.getLifespan()));
        contentValues.put("serialized_data", jobSpec.getSerializedData());
        contentValues.put("serialized_input_data", jobSpec.getSerializedInputData());
        contentValues.put("is_running", Integer.valueOf(jobSpec.isRunning() ? 1 : 0));
        sQLiteDatabase.update("job_spec", contentValues, "job_spec_id = ?", new String[]{jobSpec.getId()});
    }

    public synchronized void deleteJobs(List<String> list) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            String[] strArr = {it.next()};
            writableDatabase.delete("job_spec", "job_spec_id = ?", strArr);
            writableDatabase.delete("constraint_spec", "job_spec_id = ?", strArr);
            writableDatabase.delete("dependency_spec", "job_spec_id = ?", strArr);
            writableDatabase.delete("dependency_spec", "depends_on_job_spec_id = ?", strArr);
        }
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
    }

    public synchronized List<ConstraintSpec> getAllConstraintSpecs() {
        LinkedList linkedList;
        linkedList = new LinkedList();
        Cursor query = getReadableDatabase().query("constraint_spec", null, null, null, null, null, null);
        while (query != null && query.moveToNext()) {
            linkedList.add(constraintSpecFromCursor(query));
        }
        if (query != null) {
            query.close();
        }
        return linkedList;
    }

    public synchronized List<DependencySpec> getAllDependencySpecs() {
        LinkedList linkedList;
        linkedList = new LinkedList();
        Cursor query = getReadableDatabase().query("dependency_spec", null, null, null, null, null, null);
        while (query != null && query.moveToNext()) {
            linkedList.add(dependencySpecFromCursor(query));
        }
        if (query != null) {
            query.close();
        }
        return linkedList;
    }

    private void insertJobSpec(SQLiteDatabase sQLiteDatabase, JobSpec jobSpec) {
        if (!jobSpec.isMemoryOnly()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("job_spec_id", jobSpec.getId());
            contentValues.put("factory_key", jobSpec.getFactoryKey());
            contentValues.put("queue_key", jobSpec.getQueueKey());
            contentValues.put("create_time", Long.valueOf(jobSpec.getCreateTime()));
            contentValues.put("next_run_attempt_time", Long.valueOf(jobSpec.getNextRunAttemptTime()));
            contentValues.put("run_attempt", Integer.valueOf(jobSpec.getRunAttempt()));
            contentValues.put("max_attempts", Integer.valueOf(jobSpec.getMaxAttempts()));
            contentValues.put("lifespan", Long.valueOf(jobSpec.getLifespan()));
            contentValues.put("serialized_data", jobSpec.getSerializedData());
            contentValues.put("serialized_input_data", jobSpec.getSerializedInputData());
            contentValues.put("is_running", Integer.valueOf(jobSpec.isRunning() ? 1 : 0));
            sQLiteDatabase.insertWithOnConflict("job_spec", null, contentValues, 4);
        }
    }

    private void insertConstraintSpecs(SQLiteDatabase sQLiteDatabase, List<ConstraintSpec> list) {
        Stream.of(list).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.database.JobDatabase$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((ConstraintSpec) obj).isMemoryOnly();
            }
        }).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.database.JobDatabase$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                JobDatabase.$r8$lambda$LlTWVaEVaZDbMICho_jDOtCOPf8(SQLiteDatabase.this, (ConstraintSpec) obj);
            }
        });
    }

    public static /* synthetic */ void lambda$insertConstraintSpecs$2(SQLiteDatabase sQLiteDatabase, ConstraintSpec constraintSpec) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("job_spec_id", constraintSpec.getJobSpecId());
        contentValues.put("factory_key", constraintSpec.getFactoryKey());
        sQLiteDatabase.insertWithOnConflict("constraint_spec", null, contentValues, 4);
    }

    private void insertDependencySpecs(SQLiteDatabase sQLiteDatabase, List<DependencySpec> list) {
        Stream.of(list).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.database.JobDatabase$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((DependencySpec) obj).isMemoryOnly();
            }
        }).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.database.JobDatabase$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                JobDatabase.$r8$lambda$SY1Zrt2qASn5UxgOSFfDtu2aKo8(SQLiteDatabase.this, (DependencySpec) obj);
            }
        });
    }

    public static /* synthetic */ void lambda$insertDependencySpecs$3(SQLiteDatabase sQLiteDatabase, DependencySpec dependencySpec) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("job_spec_id", dependencySpec.getJobId());
        contentValues.put("depends_on_job_spec_id", dependencySpec.getDependsOnJobId());
        sQLiteDatabase.insertWithOnConflict("dependency_spec", null, contentValues, 4);
    }

    private JobSpec jobSpecFromCursor(Cursor cursor) {
        String string = cursor.getString(cursor.getColumnIndexOrThrow("job_spec_id"));
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow("factory_key"));
        String string3 = cursor.getString(cursor.getColumnIndexOrThrow("queue_key"));
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("create_time"));
        long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("next_run_attempt_time"));
        int i = cursor.getInt(cursor.getColumnIndexOrThrow("run_attempt"));
        int i2 = cursor.getInt(cursor.getColumnIndexOrThrow("max_attempts"));
        long j3 = cursor.getLong(cursor.getColumnIndexOrThrow("lifespan"));
        String string4 = cursor.getString(cursor.getColumnIndexOrThrow("serialized_data"));
        String string5 = cursor.getString(cursor.getColumnIndexOrThrow("serialized_input_data"));
        boolean z = true;
        if (cursor.getInt(cursor.getColumnIndexOrThrow("is_running")) != 1) {
            z = false;
        }
        return new JobSpec(string, string2, string3, j, j2, i, i2, j3, string4, string5, z, false);
    }

    private ConstraintSpec constraintSpecFromCursor(Cursor cursor) {
        return new ConstraintSpec(cursor.getString(cursor.getColumnIndexOrThrow("job_spec_id")), cursor.getString(cursor.getColumnIndexOrThrow("factory_key")), false);
    }

    private DependencySpec dependencySpecFromCursor(Cursor cursor) {
        return new DependencySpec(cursor.getString(cursor.getColumnIndexOrThrow("job_spec_id")), cursor.getString(cursor.getColumnIndexOrThrow("depends_on_job_spec_id")), false);
    }

    @Override // org.thoughtcrime.securesms.database.SignalDatabaseOpenHelper
    public SQLiteDatabase getSqlCipherDatabase() {
        return getWritableDatabase();
    }

    private void dropTableIfPresent(String str) {
        if (SignalDatabase.hasTable(str)) {
            String str2 = TAG;
            Log.i(str2, "Dropping original " + str + " table from the main database.");
            SQLiteDatabase rawDatabase = SignalDatabase.getRawDatabase();
            rawDatabase.execSQL("DROP TABLE " + str);
        }
    }

    private static void migrateJobSpecsFromPreviousDatabase(SQLiteDatabase sQLiteDatabase, SQLiteDatabase sQLiteDatabase2) {
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM job_spec", (String[]) null);
        while (rawQuery.moveToNext()) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("job_spec_id", CursorUtil.requireString(rawQuery, "job_spec_id"));
                contentValues.put("factory_key", CursorUtil.requireString(rawQuery, "factory_key"));
                contentValues.put("queue_key", CursorUtil.requireString(rawQuery, "queue_key"));
                contentValues.put("create_time", Long.valueOf(CursorUtil.requireLong(rawQuery, "create_time")));
                contentValues.put("next_run_attempt_time", Long.valueOf(CursorUtil.requireLong(rawQuery, "next_run_attempt_time")));
                contentValues.put("run_attempt", Integer.valueOf(CursorUtil.requireInt(rawQuery, "run_attempt")));
                contentValues.put("max_attempts", Integer.valueOf(CursorUtil.requireInt(rawQuery, "max_attempts")));
                contentValues.put("lifespan", Long.valueOf(CursorUtil.requireLong(rawQuery, "lifespan")));
                contentValues.put("serialized_data", CursorUtil.requireString(rawQuery, "serialized_data"));
                contentValues.put("serialized_input_data", CursorUtil.requireString(rawQuery, "serialized_input_data"));
                contentValues.put("is_running", Integer.valueOf(CursorUtil.requireInt(rawQuery, "is_running")));
                sQLiteDatabase2.insert("job_spec", (String) null, contentValues);
            } catch (Throwable th) {
                if (rawQuery != null) {
                    try {
                        rawQuery.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        rawQuery.close();
    }

    private static void migrateConstraintSpecsFromPreviousDatabase(SQLiteDatabase sQLiteDatabase, SQLiteDatabase sQLiteDatabase2) {
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM constraint_spec", (String[]) null);
        while (rawQuery.moveToNext()) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("job_spec_id", CursorUtil.requireString(rawQuery, "job_spec_id"));
                contentValues.put("factory_key", CursorUtil.requireString(rawQuery, "factory_key"));
                sQLiteDatabase2.insert("constraint_spec", (String) null, contentValues);
            } catch (Throwable th) {
                if (rawQuery != null) {
                    try {
                        rawQuery.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        rawQuery.close();
    }

    private static void migrateDependencySpecsFromPreviousDatabase(SQLiteDatabase sQLiteDatabase, SQLiteDatabase sQLiteDatabase2) {
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM dependency_spec", (String[]) null);
        while (rawQuery.moveToNext()) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("job_spec_id", CursorUtil.requireString(rawQuery, "job_spec_id"));
                contentValues.put("depends_on_job_spec_id", CursorUtil.requireString(rawQuery, "depends_on_job_spec_id"));
                sQLiteDatabase2.insert("dependency_spec", (String) null, contentValues);
            } catch (Throwable th) {
                if (rawQuery != null) {
                    try {
                        rawQuery.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        rawQuery.close();
    }
}
