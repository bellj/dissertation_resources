package org.thoughtcrime.securesms.database.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;
import org.signal.core.util.DatabaseId;

/* loaded from: classes4.dex */
public final class DistributionListId implements DatabaseId, Parcelable {
    public static final Parcelable.Creator<DistributionListId> CREATOR = new Parcelable.Creator<DistributionListId>() { // from class: org.thoughtcrime.securesms.database.model.DistributionListId.1
        @Override // android.os.Parcelable.Creator
        public DistributionListId createFromParcel(Parcel parcel) {
            return new DistributionListId(parcel.readLong());
        }

        @Override // android.os.Parcelable.Creator
        public DistributionListId[] newArray(int i) {
            return new DistributionListId[i];
        }
    };
    public static final DistributionListId MY_STORY = from(1);
    public static final long MY_STORY_ID;
    private final long id;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public static DistributionListId from(long j) {
        if (j > 0) {
            return new DistributionListId(j);
        }
        throw new IllegalArgumentException("Invalid ID! " + j);
    }

    public static DistributionListId fromNullable(long j) {
        if (j > 0) {
            return new DistributionListId(j);
        }
        return null;
    }

    public static DistributionListId from(String str) {
        try {
            return from(Long.parseLong(str));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private DistributionListId(long j) {
        this.id = j;
    }

    public boolean isMyStory() {
        return equals(MY_STORY);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
    }

    @Override // org.signal.core.util.DatabaseId
    public String serialize() {
        return String.valueOf(this.id);
    }

    @Override // java.lang.Object
    public String toString() {
        return "DistributionListId::" + this.id;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && DistributionListId.class == obj.getClass() && this.id == ((DistributionListId) obj).id) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Objects.hash(Long.valueOf(this.id));
    }
}
