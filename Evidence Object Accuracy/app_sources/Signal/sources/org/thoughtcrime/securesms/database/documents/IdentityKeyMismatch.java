package org.thoughtcrime.securesms.database.documents;

import android.content.Context;
import android.text.TextUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Base64;

/* loaded from: classes.dex */
public class IdentityKeyMismatch {
    private static final String TAG = Log.tag(IdentityKeyMismatch.class);
    @JsonProperty("a")
    private String address;
    @JsonProperty("k")
    @JsonDeserialize(using = IdentityKeyDeserializer.class)
    @JsonSerialize(using = IdentityKeySerializer.class)
    private IdentityKey identityKey;
    @JsonProperty("r")
    private String recipientId;

    public IdentityKeyMismatch() {
    }

    public IdentityKeyMismatch(RecipientId recipientId, IdentityKey identityKey) {
        this.recipientId = recipientId.serialize();
        this.address = "";
        this.identityKey = identityKey;
    }

    @JsonIgnore
    public RecipientId getRecipientId(Context context) {
        if (!TextUtils.isEmpty(this.recipientId)) {
            return RecipientId.from(this.recipientId);
        }
        return Recipient.external(context, this.address).getId();
    }

    public IdentityKey getIdentityKey() {
        return this.identityKey;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        IdentityKeyMismatch identityKeyMismatch = (IdentityKeyMismatch) obj;
        if (!Objects.equals(this.address, identityKeyMismatch.address) || !Objects.equals(this.recipientId, identityKeyMismatch.recipientId) || !Objects.equals(this.identityKey, identityKeyMismatch.identityKey)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.address, this.recipientId, this.identityKey);
    }

    /* loaded from: classes4.dex */
    private static class IdentityKeySerializer extends JsonSerializer<IdentityKey> {
        private IdentityKeySerializer() {
        }

        public void serialize(IdentityKey identityKey, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(Base64.encodeBytes(identityKey.serialize()));
        }
    }

    /* loaded from: classes4.dex */
    private static class IdentityKeyDeserializer extends JsonDeserializer<IdentityKey> {
        private IdentityKeyDeserializer() {
        }

        public IdentityKey deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            try {
                return new IdentityKey(Base64.decode(jsonParser.getValueAsString()), 0);
            } catch (InvalidKeyException e) {
                Log.w(IdentityKeyMismatch.TAG, e);
                throw new IOException(e);
            }
        }
    }
}
