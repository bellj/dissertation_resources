package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.thoughtcrime.securesms.database.model.databaseprotos.Wallpaper;

/* loaded from: classes4.dex */
public interface WallpaperOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    float getDimLevelInDarkTheme();

    Wallpaper.File getFile();

    Wallpaper.LinearGradient getLinearGradient();

    Wallpaper.SingleColor getSingleColor();

    Wallpaper.WallpaperCase getWallpaperCase();

    boolean hasFile();

    boolean hasLinearGradient();

    boolean hasSingleColor();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
