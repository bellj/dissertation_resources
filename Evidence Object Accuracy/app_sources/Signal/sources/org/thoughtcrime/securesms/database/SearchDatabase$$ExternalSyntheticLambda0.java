package org.thoughtcrime.securesms.database;

import com.annimon.stream.function.Function;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SearchDatabase$$ExternalSyntheticLambda0 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((String) obj).trim();
    }
}
