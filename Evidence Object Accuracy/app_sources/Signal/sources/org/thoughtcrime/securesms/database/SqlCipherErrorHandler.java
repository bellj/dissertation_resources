package org.thoughtcrime.securesms.database;

import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsKt;
import net.zetetic.database.DatabaseErrorHandler;
import net.zetetic.database.sqlcipher.SQLiteConnection;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import net.zetetic.database.sqlcipher.SQLiteDatabaseHook;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.ExceptionUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.DatabaseSecretProvider;
import org.thoughtcrime.securesms.database.SqlCipherErrorHandler;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* compiled from: SqlCipherErrorHandler.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 \u00122\u00020\u0001:\b\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u001c\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\n2\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0007\u001a\u00020\bH\u0002J$\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u00032\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\nH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler;", "Lnet/zetetic/database/DatabaseErrorHandler;", "databaseName", "", "(Ljava/lang/String;)V", "onCorruption", "", "db", "Lnet/zetetic/database/sqlcipher/SQLiteDatabase;", "queryDatabase", "Lkotlin/Function1;", "runDiagnostics", "Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DiagnosticResults;", "context", "Landroid/content/Context;", "runDiagnosticsMethod", "descriptor", "query", "Companion", "CustomTraceError", "DatabaseCorruptedError_BothChecksFail", "DatabaseCorruptedError_BothChecksPass", "DatabaseCorruptedError_FailedToRunChecks", "DatabaseCorruptedError_NormalCheckFailsCipherCheckPasses", "DatabaseCorruptedError_NormalCheckPassesCipherCheckFails", "DiagnosticResults", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SqlCipherErrorHandler implements DatabaseErrorHandler {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(SqlCipherErrorHandler.class);
    private final String databaseName;

    public SqlCipherErrorHandler(String str) {
        Intrinsics.checkNotNullParameter(str, "databaseName");
        this.databaseName = str;
    }

    @Override // net.zetetic.database.DatabaseErrorHandler
    public void onCorruption(SQLiteDatabase sQLiteDatabase) {
        Intrinsics.checkNotNullParameter(sQLiteDatabase, "db");
        StringBuilder sb = new StringBuilder();
        sb.append("Database '");
        sb.append(this.databaseName);
        sb.append("' corrupted! Going to try to run some diagnostics.\n");
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        DiagnosticResults runDiagnostics = runDiagnostics(application, sQLiteDatabase);
        List list = StringsKt__StringsKt.split$default((CharSequence) runDiagnostics.getLogs(), new String[]{"\n"}, false, 0, 6, (Object) null);
        List list2 = CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf("Database '" + this.databaseName + "' corrupted!. Diagnostics results:\n"), (Iterable) list);
        String str = TAG;
        Log.e(str, "Database '" + this.databaseName + "' corrupted!. Diagnostics results:\n " + runDiagnostics.getLogs());
        if (runDiagnostics instanceof DiagnosticResults.Success) {
            DiagnosticResults.Success success = (DiagnosticResults.Success) runDiagnostics;
            if (success.getPragma1Passes() && success.getPragma2Passes()) {
                throw new DatabaseCorruptedError_BothChecksPass(list2);
            } else if (!success.getPragma1Passes() && success.getPragma2Passes()) {
                throw new DatabaseCorruptedError_NormalCheckFailsCipherCheckPasses(list2);
            } else if (!success.getPragma1Passes() || success.getPragma2Passes()) {
                throw new DatabaseCorruptedError_BothChecksFail(list2);
            } else {
                throw new DatabaseCorruptedError_NormalCheckPassesCipherCheckFails(list2);
            }
        } else {
            throw new DatabaseCorruptedError_FailedToRunChecks(list2);
        }
    }

    private final DiagnosticResults runDiagnostics(Context context, SQLiteDatabase sQLiteDatabase) {
        DiagnosticResults runDiagnosticsMethod = runDiagnosticsMethod("same-connection", queryDatabase(sQLiteDatabase));
        if (runDiagnosticsMethod instanceof DiagnosticResults.Success) {
            return runDiagnosticsMethod;
        }
        AtomicReference atomicReference = new AtomicReference();
        File databasePath = context.getDatabasePath(this.databaseName);
        CountDownLatch countDownLatch = new CountDownLatch(1);
        try {
            SQLiteDatabase.openOrCreateDatabase(databasePath.getAbsolutePath(), DatabaseSecretProvider.getOrCreateDatabaseSecret(context).asString(), (SQLiteDatabase.CursorFactory) null, (DatabaseErrorHandler) null, new SQLiteDatabaseHook(countDownLatch, this, atomicReference) { // from class: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1
                final /* synthetic */ AtomicReference<SqlCipherErrorHandler.DiagnosticResults> $differentConnectionResult;
                final /* synthetic */ CountDownLatch $latch;
                final /* synthetic */ SqlCipherErrorHandler this$0;

                @Override // net.zetetic.database.sqlcipher.SQLiteDatabaseHook
                public void preKey(SQLiteConnection sQLiteConnection) {
                    Intrinsics.checkNotNullParameter(sQLiteConnection, "connection");
                }

                /* access modifiers changed from: package-private */
                {
                    this.$latch = r1;
                    this.this$0 = r2;
                    this.$differentConnectionResult = r3;
                }

                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0020: INVOKE  
                      (wrap: java.util.concurrent.atomic.AtomicReference<org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults> : 0x001e: IGET  (r0v4 java.util.concurrent.atomic.AtomicReference<org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults> A[REMOVE]) = (r5v0 'this' org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1.$differentConnectionResult java.util.concurrent.atomic.AtomicReference)
                      (wrap: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults : 0x001a: ONE_ARG  (r6v2 org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults A[REMOVE]) = 
                      (wrap: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults : 0x0000: INVOKE  
                      (wrap: org.thoughtcrime.securesms.database.SqlCipherErrorHandler : 0x0011: IGET  (r0v3 org.thoughtcrime.securesms.database.SqlCipherErrorHandler A[REMOVE]) = (r5v0 'this' org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1.this$0 org.thoughtcrime.securesms.database.SqlCipherErrorHandler)
                      ("different-connection")
                      (wrap: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1 : 0x0015: CONSTRUCTOR  (r1v0 org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1 A[REMOVE]) = (r6v0 'sQLiteConnection' net.zetetic.database.sqlcipher.SQLiteConnection) call: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1.<init>(net.zetetic.database.sqlcipher.SQLiteConnection):void type: CONSTRUCTOR)
                     type: DIRECT call: org.thoughtcrime.securesms.database.SqlCipherErrorHandler.runDiagnosticsMethod(java.lang.String, kotlin.jvm.functions.Function1):org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults)
                    )
                     type: VIRTUAL call: java.util.concurrent.atomic.AtomicReference.set(java.lang.Object):void in method: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1.postKey(net.zetetic.database.sqlcipher.SQLiteConnection):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                    	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                    	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001a: ONE_ARG  (r6v2 org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults A[REMOVE]) = 
                      (wrap: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults : 0x0000: INVOKE  
                      (wrap: org.thoughtcrime.securesms.database.SqlCipherErrorHandler : 0x0011: IGET  (r0v3 org.thoughtcrime.securesms.database.SqlCipherErrorHandler A[REMOVE]) = (r5v0 'this' org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1.this$0 org.thoughtcrime.securesms.database.SqlCipherErrorHandler)
                      ("different-connection")
                      (wrap: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1 : 0x0015: CONSTRUCTOR  (r1v0 org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1 A[REMOVE]) = (r6v0 'sQLiteConnection' net.zetetic.database.sqlcipher.SQLiteConnection) call: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1.<init>(net.zetetic.database.sqlcipher.SQLiteConnection):void type: CONSTRUCTOR)
                     type: DIRECT call: org.thoughtcrime.securesms.database.SqlCipherErrorHandler.runDiagnosticsMethod(java.lang.String, kotlin.jvm.functions.Function1):org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults)
                     in method: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1.postKey(net.zetetic.database.sqlcipher.SQLiteConnection):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 18 more
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0000: INVOKE  
                      (wrap: org.thoughtcrime.securesms.database.SqlCipherErrorHandler : 0x0011: IGET  (r0v3 org.thoughtcrime.securesms.database.SqlCipherErrorHandler A[REMOVE]) = (r5v0 'this' org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1.this$0 org.thoughtcrime.securesms.database.SqlCipherErrorHandler)
                      ("different-connection")
                      (wrap: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1 : 0x0015: CONSTRUCTOR  (r1v0 org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1 A[REMOVE]) = (r6v0 'sQLiteConnection' net.zetetic.database.sqlcipher.SQLiteConnection) call: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1.<init>(net.zetetic.database.sqlcipher.SQLiteConnection):void type: CONSTRUCTOR)
                     type: DIRECT call: org.thoughtcrime.securesms.database.SqlCipherErrorHandler.runDiagnosticsMethod(java.lang.String, kotlin.jvm.functions.Function1):org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults in method: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1.postKey(net.zetetic.database.sqlcipher.SQLiteConnection):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:100)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:512)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 24 more
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0015: CONSTRUCTOR  (r1v0 org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1 A[REMOVE]) = (r6v0 'sQLiteConnection' net.zetetic.database.sqlcipher.SQLiteConnection) call: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1.<init>(net.zetetic.database.sqlcipher.SQLiteConnection):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1.postKey(net.zetetic.database.sqlcipher.SQLiteConnection):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 29 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 35 more
                    */
                @Override // net.zetetic.database.sqlcipher.SQLiteDatabaseHook
                public void postKey(net.zetetic.database.sqlcipher.SQLiteConnection r6) {
                    /*
                        r5 = this;
                        java.lang.String r0 = "connection"
                        kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r6, r0)
                        java.util.concurrent.CountDownLatch r0 = r5.$latch
                        long r0 = r0.getCount()
                        r2 = 0
                        int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                        if (r4 <= 0) goto L_0x0028
                        org.thoughtcrime.securesms.database.SqlCipherErrorHandler r0 = r5.this$0
                        org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1 r1 = new org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1$postKey$result$1
                        r1.<init>(r6)
                        java.lang.String r6 = "different-connection"
                        org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults r6 = org.thoughtcrime.securesms.database.SqlCipherErrorHandler.access$runDiagnosticsMethod(r0, r6, r1)
                        java.util.concurrent.atomic.AtomicReference<org.thoughtcrime.securesms.database.SqlCipherErrorHandler$DiagnosticResults> r0 = r5.$differentConnectionResult
                        r0.set(r6)
                        java.util.concurrent.CountDownLatch r6 = r5.$latch
                        r6.countDown()
                    L_0x0028:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$runDiagnostics$1.postKey(net.zetetic.database.sqlcipher.SQLiteConnection):void");
                }
            });
            countDownLatch.await();
            Object obj = atomicReference.get();
            Intrinsics.checkNotNull(obj);
            return (DiagnosticResults) obj;
        } catch (Throwable th) {
            String convertThrowableToString = ExceptionUtil.convertThrowableToString(th);
            Intrinsics.checkNotNullExpressionValue(convertThrowableToString, "convertThrowableToString(t)");
            return new DiagnosticResults.Failure(convertThrowableToString);
        }
    }

    public final DiagnosticResults runDiagnosticsMethod(String str, Function1<? super String, String> function1) {
        StringBuilder sb = new StringBuilder();
        sb.append(" ===== PRAGMA integrity_check (" + str + ") =====\n");
        try {
            String invoke = function1.invoke("PRAGMA integrity_check");
            sb.append(invoke);
            String lowerCase = invoke.toLowerCase(Locale.ROOT);
            Intrinsics.checkNotNullExpressionValue(lowerCase, "this as java.lang.String).toLowerCase(Locale.ROOT)");
            boolean z = false;
            boolean z2 = StringsKt__StringsKt.contains$default((CharSequence) lowerCase, (CharSequence) "ok", false, 2, (Object) null);
            sb.append("\n");
            sb.append("===== PRAGMA cipher_integrity_check (" + str + ") =====\n");
            try {
                String invoke2 = function1.invoke("PRAGMA cipher_integrity_check");
                sb.append(invoke2);
                if (StringsKt__StringsKt.trim(invoke2).toString().length() > 0) {
                    z = true;
                }
                String sb2 = sb.toString();
                Intrinsics.checkNotNullExpressionValue(sb2, "output.toString()");
                return new DiagnosticResults.Success(z2, !z, sb2);
            } catch (Throwable th) {
                sb.append("Failed to do cipher_integrity_check!\n");
                sb.append(ExceptionUtil.convertThrowableToString(th));
                String sb3 = sb.toString();
                Intrinsics.checkNotNullExpressionValue(sb3, "output.toString()");
                return new DiagnosticResults.Failure(sb3);
            }
        } catch (Throwable th2) {
            sb.append("Failed to do integrity_check!\n");
            sb.append(ExceptionUtil.convertThrowableToString(th2));
            String sb4 = sb.toString();
            Intrinsics.checkNotNullExpressionValue(sb4, "output.toString()");
            return new DiagnosticResults.Failure(sb4);
        }
    }

    private final Function1<String, String> queryDatabase(SQLiteDatabase sQLiteDatabase) {
        return new Function1<String, String>(sQLiteDatabase) { // from class: org.thoughtcrime.securesms.database.SqlCipherErrorHandler$queryDatabase$1
            final /* synthetic */ SQLiteDatabase $db;

            /* access modifiers changed from: package-private */
            {
                this.$db = r1;
            }

            /* JADX INFO: finally extract failed */
            public final String invoke(String str) {
                Intrinsics.checkNotNullParameter(str, "query");
                StringBuilder sb = new StringBuilder();
                Cursor rawQuery = this.$db.rawQuery(str, (String[]) null);
                while (rawQuery.moveToNext()) {
                    try {
                        sb.append(CursorUtil.readRowAsString(rawQuery));
                        sb.append("\n");
                    } finally {
                        try {
                            throw th;
                        } catch (Throwable th) {
                        }
                    }
                }
                Unit unit = Unit.INSTANCE;
                CloseableKt.closeFinally(rawQuery, null);
                String sb2 = sb.toString();
                Intrinsics.checkNotNullExpressionValue(sb2, "output.toString()");
                return sb2;
            }
        };
    }

    /* compiled from: SqlCipherErrorHandler.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\u0002\t\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DiagnosticResults;", "", "logs", "", "(Ljava/lang/String;)V", "getLogs", "()Ljava/lang/String;", "Failure", "Success", "Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DiagnosticResults$Success;", "Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DiagnosticResults$Failure;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class DiagnosticResults {
        private final String logs;

        public /* synthetic */ DiagnosticResults(String str, DefaultConstructorMarker defaultConstructorMarker) {
            this(str);
        }

        private DiagnosticResults(String str) {
            this.logs = str;
        }

        public final String getLogs() {
            return this.logs;
        }

        /* compiled from: SqlCipherErrorHandler.kt */
        @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\t¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DiagnosticResults$Success;", "Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DiagnosticResults;", "pragma1Passes", "", "pragma2Passes", "logs", "", "(ZZLjava/lang/String;)V", "getPragma1Passes", "()Z", "getPragma2Passes", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Success extends DiagnosticResults {
            private final boolean pragma1Passes;
            private final boolean pragma2Passes;

            public final boolean getPragma1Passes() {
                return this.pragma1Passes;
            }

            public final boolean getPragma2Passes() {
                return this.pragma2Passes;
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Success(boolean z, boolean z2, String str) {
                super(str, null);
                Intrinsics.checkNotNullParameter(str, "logs");
                this.pragma1Passes = z;
                this.pragma2Passes = z2;
            }
        }

        /* compiled from: SqlCipherErrorHandler.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DiagnosticResults$Failure;", "Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DiagnosticResults;", "logs", "", "(Ljava/lang/String;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Failure extends DiagnosticResults {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Failure(String str) {
                super(str, null);
                Intrinsics.checkNotNullParameter(str, "logs");
            }
        }
    }

    /* compiled from: SqlCipherErrorHandler.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0012\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0002\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$CustomTraceError;", "Ljava/lang/Error;", "Lkotlin/Error;", "lines", "", "", "(Ljava/util/List;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static class CustomTraceError extends Error {
        public CustomTraceError(List<String> list) {
            Intrinsics.checkNotNullParameter(list, "lines");
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
            for (String str : list) {
                arrayList.add(new StackTraceElement(str, "", "", 0));
            }
            Object[] array = arrayList.toArray(new StackTraceElement[0]);
            if (array != null) {
                setStackTrace(ExceptionUtil.joinStackTrace(getStackTrace(), (StackTraceElement[]) array));
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
        }
    }

    /* compiled from: SqlCipherErrorHandler.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DatabaseCorruptedError_BothChecksPass;", "Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$CustomTraceError;", "lines", "", "", "(Ljava/util/List;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class DatabaseCorruptedError_BothChecksPass extends CustomTraceError {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public DatabaseCorruptedError_BothChecksPass(List<String> list) {
            super(list);
            Intrinsics.checkNotNullParameter(list, "lines");
        }
    }

    /* compiled from: SqlCipherErrorHandler.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DatabaseCorruptedError_BothChecksFail;", "Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$CustomTraceError;", "lines", "", "", "(Ljava/util/List;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class DatabaseCorruptedError_BothChecksFail extends CustomTraceError {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public DatabaseCorruptedError_BothChecksFail(List<String> list) {
            super(list);
            Intrinsics.checkNotNullParameter(list, "lines");
        }
    }

    /* compiled from: SqlCipherErrorHandler.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DatabaseCorruptedError_NormalCheckFailsCipherCheckPasses;", "Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$CustomTraceError;", "lines", "", "", "(Ljava/util/List;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class DatabaseCorruptedError_NormalCheckFailsCipherCheckPasses extends CustomTraceError {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public DatabaseCorruptedError_NormalCheckFailsCipherCheckPasses(List<String> list) {
            super(list);
            Intrinsics.checkNotNullParameter(list, "lines");
        }
    }

    /* compiled from: SqlCipherErrorHandler.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DatabaseCorruptedError_NormalCheckPassesCipherCheckFails;", "Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$CustomTraceError;", "lines", "", "", "(Ljava/util/List;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class DatabaseCorruptedError_NormalCheckPassesCipherCheckFails extends CustomTraceError {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public DatabaseCorruptedError_NormalCheckPassesCipherCheckFails(List<String> list) {
            super(list);
            Intrinsics.checkNotNullParameter(list, "lines");
        }
    }

    /* compiled from: SqlCipherErrorHandler.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$DatabaseCorruptedError_FailedToRunChecks;", "Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$CustomTraceError;", "lines", "", "", "(Ljava/util/List;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class DatabaseCorruptedError_FailedToRunChecks extends CustomTraceError {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public DatabaseCorruptedError_FailedToRunChecks(List<String> list) {
            super(list);
            Intrinsics.checkNotNullParameter(list, "lines");
        }
    }

    /* compiled from: SqlCipherErrorHandler.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/database/SqlCipherErrorHandler$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
