package org.thoughtcrime.securesms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SqlUtil;
import org.signal.ringrtc.CallManager;

/* compiled from: GroupCallRingDatabase.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u001e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\rJ\u001e\u0010\u000e\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\rJ\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u0011\u001a\u00020\b¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/database/GroupCallRingDatabase;", "Lorg/thoughtcrime/securesms/database/Database;", "context", "Landroid/content/Context;", "databaseHelper", "Lorg/thoughtcrime/securesms/database/SignalDatabase;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/database/SignalDatabase;)V", "insertGroupRing", "", "ringId", "", "dateReceived", "ringState", "Lorg/signal/ringrtc/CallManager$RingUpdate;", "insertOrUpdateGroupRing", "isCancelled", "", "removeOldRings", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GroupCallRingDatabase extends Database {
    public static final String[] CREATE_INDEXES = {"CREATE INDEX date_received_index on group_call_ring (date_received)"};
    public static final String CREATE_TABLE = "CREATE TABLE group_call_ring (\n  _id INTEGER PRIMARY KEY,\n  ring_id INTEGER UNIQUE,\n  date_received INTEGER,\n  ring_state INTEGER\n)";
    public static final Companion Companion = new Companion(null);
    private static final String DATE_RECEIVED;
    private static final String ID;
    private static final String RING_ID;
    private static final String RING_STATE;
    private static final String TABLE_NAME;
    private static final long VALID_RING_DURATION = TimeUnit.MINUTES.toMillis(30);

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public GroupCallRingDatabase(Context context, SignalDatabase signalDatabase) {
        super(context, signalDatabase);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(signalDatabase, "databaseHelper");
    }

    /* compiled from: GroupCallRingDatabase.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0006R\u0010\u0010\u0007\u001a\u00020\u00058\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/database/GroupCallRingDatabase$Companion;", "", "()V", "CREATE_INDEXES", "", "", "[Ljava/lang/String;", "CREATE_TABLE", "DATE_RECEIVED", "ID", "RING_ID", "RING_STATE", "TABLE_NAME", "VALID_RING_DURATION", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final boolean isCancelled(long j) {
        Cursor query = this.databaseHelper.getSignalReadableDatabase().query(TABLE_NAME, null, "ring_id = ?", SqlUtil.buildArgs(j), null, null, null);
        try {
            boolean z = false;
            th = null;
            if (query.moveToFirst()) {
                if (CursorUtil.requireInt(query, RING_STATE) != 0) {
                    z = true;
                }
                return z;
            }
            Unit unit = Unit.INSTANCE;
            return false;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final void insertGroupRing(long j, long j2, CallManager.RingUpdate ringUpdate) {
        Intrinsics.checkNotNullParameter(ringUpdate, "ringState");
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(RING_ID, Long.valueOf(j));
        contentValues.put("date_received", Long.valueOf(j2));
        contentValues.put(RING_STATE, Integer.valueOf(GroupCallRingDatabaseKt.toCode(ringUpdate)));
        signalWritableDatabase.insert(TABLE_NAME, (String) null, contentValues);
        removeOldRings();
    }

    public final void insertOrUpdateGroupRing(long j, long j2, CallManager.RingUpdate ringUpdate) {
        Intrinsics.checkNotNullParameter(ringUpdate, "ringState");
        SQLiteDatabase signalWritableDatabase = this.databaseHelper.getSignalWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(RING_ID, Long.valueOf(j));
        contentValues.put("date_received", Long.valueOf(j2));
        contentValues.put(RING_STATE, Integer.valueOf(GroupCallRingDatabaseKt.toCode(ringUpdate)));
        signalWritableDatabase.insertWithOnConflict(TABLE_NAME, null, contentValues, 5);
        removeOldRings();
    }

    public final void removeOldRings() {
        this.databaseHelper.getSignalWritableDatabase().delete(TABLE_NAME, "date_received < ?", SqlUtil.buildArgs(System.currentTimeMillis() - VALID_RING_DURATION));
    }
}
