package org.thoughtcrime.securesms.database.model.databaseprotos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public final class SignalStoreList extends GeneratedMessageLite<SignalStoreList, Builder> implements SignalStoreListOrBuilder {
    public static final int CONTENTS_FIELD_NUMBER;
    private static final SignalStoreList DEFAULT_INSTANCE;
    private static volatile Parser<SignalStoreList> PARSER;
    private Internal.ProtobufList<String> contents_ = GeneratedMessageLite.emptyProtobufList();

    private SignalStoreList() {
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.SignalStoreListOrBuilder
    public List<String> getContentsList() {
        return this.contents_;
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.SignalStoreListOrBuilder
    public int getContentsCount() {
        return this.contents_.size();
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.SignalStoreListOrBuilder
    public String getContents(int i) {
        return this.contents_.get(i);
    }

    @Override // org.thoughtcrime.securesms.database.model.databaseprotos.SignalStoreListOrBuilder
    public ByteString getContentsBytes(int i) {
        return ByteString.copyFromUtf8(this.contents_.get(i));
    }

    private void ensureContentsIsMutable() {
        Internal.ProtobufList<String> protobufList = this.contents_;
        if (!protobufList.isModifiable()) {
            this.contents_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setContents(int i, String str) {
        str.getClass();
        ensureContentsIsMutable();
        this.contents_.set(i, str);
    }

    public void addContents(String str) {
        str.getClass();
        ensureContentsIsMutable();
        this.contents_.add(str);
    }

    public void addAllContents(Iterable<String> iterable) {
        ensureContentsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.contents_);
    }

    public void clearContents() {
        this.contents_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void addContentsBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        ensureContentsIsMutable();
        this.contents_.add(byteString.toStringUtf8());
    }

    public static SignalStoreList parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (SignalStoreList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static SignalStoreList parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (SignalStoreList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static SignalStoreList parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (SignalStoreList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static SignalStoreList parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (SignalStoreList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static SignalStoreList parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (SignalStoreList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static SignalStoreList parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (SignalStoreList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static SignalStoreList parseFrom(InputStream inputStream) throws IOException {
        return (SignalStoreList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static SignalStoreList parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (SignalStoreList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static SignalStoreList parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (SignalStoreList) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static SignalStoreList parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (SignalStoreList) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static SignalStoreList parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (SignalStoreList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static SignalStoreList parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (SignalStoreList) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(SignalStoreList signalStoreList) {
        return DEFAULT_INSTANCE.createBuilder(signalStoreList);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<SignalStoreList, Builder> implements SignalStoreListOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(SignalStoreList.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.SignalStoreListOrBuilder
        public List<String> getContentsList() {
            return Collections.unmodifiableList(((SignalStoreList) this.instance).getContentsList());
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.SignalStoreListOrBuilder
        public int getContentsCount() {
            return ((SignalStoreList) this.instance).getContentsCount();
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.SignalStoreListOrBuilder
        public String getContents(int i) {
            return ((SignalStoreList) this.instance).getContents(i);
        }

        @Override // org.thoughtcrime.securesms.database.model.databaseprotos.SignalStoreListOrBuilder
        public ByteString getContentsBytes(int i) {
            return ((SignalStoreList) this.instance).getContentsBytes(i);
        }

        public Builder setContents(int i, String str) {
            copyOnWrite();
            ((SignalStoreList) this.instance).setContents(i, str);
            return this;
        }

        public Builder addContents(String str) {
            copyOnWrite();
            ((SignalStoreList) this.instance).addContents(str);
            return this;
        }

        public Builder addAllContents(Iterable<String> iterable) {
            copyOnWrite();
            ((SignalStoreList) this.instance).addAllContents(iterable);
            return this;
        }

        public Builder clearContents() {
            copyOnWrite();
            ((SignalStoreList) this.instance).clearContents();
            return this;
        }

        public Builder addContentsBytes(ByteString byteString) {
            copyOnWrite();
            ((SignalStoreList) this.instance).addContentsBytes(byteString);
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.database.model.databaseprotos.SignalStoreList$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new SignalStoreList();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001Ț", new Object[]{"contents_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<SignalStoreList> parser = PARSER;
                if (parser == null) {
                    synchronized (SignalStoreList.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        SignalStoreList signalStoreList = new SignalStoreList();
        DEFAULT_INSTANCE = signalStoreList;
        GeneratedMessageLite.registerDefaultInstance(SignalStoreList.class, signalStoreList);
    }

    public static SignalStoreList getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<SignalStoreList> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
