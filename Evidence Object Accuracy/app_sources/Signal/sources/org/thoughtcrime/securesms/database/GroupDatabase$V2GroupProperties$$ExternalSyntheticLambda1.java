package org.thoughtcrime.securesms.database;

import j$.util.function.Predicate;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupDatabase$V2GroupProperties$$ExternalSyntheticLambda1 implements Predicate {
    public final /* synthetic */ GroupDatabase.V2GroupProperties f$0;

    public /* synthetic */ GroupDatabase$V2GroupProperties$$ExternalSyntheticLambda1(GroupDatabase.V2GroupProperties v2GroupProperties) {
        this.f$0 = v2GroupProperties;
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate and(Predicate predicate) {
        return Predicate.CC.$default$and(this, predicate);
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate negate() {
        return Predicate.CC.$default$negate(this);
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate or(Predicate predicate) {
        return Predicate.CC.$default$or(this, predicate);
    }

    @Override // j$.util.function.Predicate
    public final boolean test(Object obj) {
        return this.f$0.isAdmin((Recipient) obj);
    }
}
