package org.thoughtcrime.securesms.database;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import net.zetetic.database.sqlcipher.SQLiteOpenHelper;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.DatabaseSecret;
import org.thoughtcrime.securesms.crypto.DatabaseSecretProvider;
import org.thoughtcrime.securesms.database.model.MegaphoneRecord;
import org.thoughtcrime.securesms.megaphone.Megaphones;

/* loaded from: classes4.dex */
public class MegaphoneDatabase extends SQLiteOpenHelper implements SignalDatabaseOpenHelper {
    public static final String CREATE_TABLE;
    private static final String DATABASE_NAME;
    private static final int DATABASE_VERSION;
    private static final String EVENT;
    private static final String FINISHED;
    private static final String FIRST_VISIBLE;
    private static final String ID;
    private static final String LAST_SEEN;
    private static final String SEEN_COUNT;
    private static final String TABLE_NAME;
    private static final String TAG = Log.tag(MegaphoneDatabase.class);
    private static volatile MegaphoneDatabase instance;
    private final Application application;

    public static MegaphoneDatabase getInstance(Application application) {
        if (instance == null) {
            synchronized (MegaphoneDatabase.class) {
                if (instance == null) {
                    SqlCipherLibraryLoader.load();
                    instance = new MegaphoneDatabase(application, DatabaseSecretProvider.getOrCreateDatabaseSecret(application));
                    instance.setWriteAheadLoggingEnabled(true);
                }
            }
        }
        return instance;
    }

    public MegaphoneDatabase(Application application, DatabaseSecret databaseSecret) {
        super(application, DATABASE_NAME, databaseSecret.asString(), (SQLiteDatabase.CursorFactory) null, 1, 0, new SqlCipherErrorHandler(DATABASE_NAME), new SqlCipherDatabaseHook());
        this.application = application;
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        String str = TAG;
        Log.i(str, "onCreate()");
        sQLiteDatabase.execSQL(CREATE_TABLE);
        if (SignalDatabase.hasTable(TABLE_NAME)) {
            Log.i(str, "Found old megaphone table. Migrating data.");
            migrateDataFromPreviousDatabase(SignalDatabase.getRawDatabase(), sQLiteDatabase);
        }
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        String str = TAG;
        Log.i(str, "onUpgrade(" + i + ", " + i2 + ")");
    }

    @Override // net.zetetic.database.sqlcipher.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        Log.i(TAG, "onOpen()");
        sQLiteDatabase.setForeignKeyConstraintsEnabled(true);
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.database.MegaphoneDatabase$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                MegaphoneDatabase.$r8$lambda$oaKdGZ6vz5nb61QGUswa0LtZuAY();
            }
        });
    }

    public static /* synthetic */ void lambda$onOpen$0() {
        if (SignalDatabase.hasTable(TABLE_NAME)) {
            Log.i(TAG, "Dropping original megaphone table from the main database.");
            SignalDatabase.getRawDatabase().execSQL("DROP TABLE megaphone");
        }
    }

    public void insert(Collection<Megaphones.Event> collection) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (Megaphones.Event event : collection) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(EVENT, event.getKey());
                writableDatabase.insertWithOnConflict(TABLE_NAME, null, contentValues, 4);
            }
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public List<MegaphoneRecord> getAllAndDeleteMissing() {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ArrayList arrayList = new ArrayList();
        writableDatabase.beginTransaction();
        try {
            HashSet hashSet = new HashSet();
            Cursor query = writableDatabase.query(TABLE_NAME, null, null, null, null, null, null);
            while (query != null && query.moveToNext()) {
                String string = query.getString(query.getColumnIndexOrThrow(EVENT));
                int i = query.getInt(query.getColumnIndexOrThrow(SEEN_COUNT));
                long j = query.getLong(query.getColumnIndexOrThrow("last_seen"));
                long j2 = query.getLong(query.getColumnIndexOrThrow(FIRST_VISIBLE));
                boolean z = query.getInt(query.getColumnIndexOrThrow(FINISHED)) == 1;
                if (Megaphones.Event.hasKey(string)) {
                    arrayList.add(new MegaphoneRecord(Megaphones.Event.fromKey(string), i, j, j2, z));
                } else {
                    String str = TAG;
                    Log.w(str, "No in-app handing for event '" + string + "'! Deleting it from the database.");
                    hashSet.add(string);
                }
            }
            if (query != null) {
                query.close();
            }
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                writableDatabase.delete(TABLE_NAME, "event = ?", new String[]{(String) it.next()});
            }
            writableDatabase.setTransactionSuccessful();
            return arrayList;
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public void markFirstVisible(Megaphones.Event event, long j) {
        String[] strArr = {event.getKey()};
        ContentValues contentValues = new ContentValues();
        contentValues.put(FIRST_VISIBLE, Long.valueOf(j));
        getWritableDatabase().update(TABLE_NAME, contentValues, "event = ?", strArr);
    }

    public void markSeen(Megaphones.Event event, int i, long j) {
        String[] strArr = {event.getKey()};
        ContentValues contentValues = new ContentValues();
        contentValues.put(SEEN_COUNT, Integer.valueOf(i));
        contentValues.put("last_seen", Long.valueOf(j));
        getWritableDatabase().update(TABLE_NAME, contentValues, "event = ?", strArr);
    }

    public void markFinished(Megaphones.Event event) {
        String[] strArr = {event.getKey()};
        ContentValues contentValues = new ContentValues();
        contentValues.put(FINISHED, (Integer) 1);
        getWritableDatabase().update(TABLE_NAME, contentValues, "event = ?", strArr);
    }

    public void delete(Megaphones.Event event) {
        getWritableDatabase().delete(TABLE_NAME, "event = ?", new String[]{event.getKey()});
    }

    @Override // org.thoughtcrime.securesms.database.SignalDatabaseOpenHelper
    public SQLiteDatabase getSqlCipherDatabase() {
        return getWritableDatabase();
    }

    private static void migrateDataFromPreviousDatabase(SQLiteDatabase sQLiteDatabase, SQLiteDatabase sQLiteDatabase2) {
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM megaphone", (String[]) null);
        while (rawQuery.moveToNext()) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put(EVENT, CursorUtil.requireString(rawQuery, EVENT));
                contentValues.put(SEEN_COUNT, Integer.valueOf(CursorUtil.requireInt(rawQuery, SEEN_COUNT)));
                contentValues.put("last_seen", Long.valueOf(CursorUtil.requireLong(rawQuery, "last_seen")));
                contentValues.put(FIRST_VISIBLE, Long.valueOf(CursorUtil.requireLong(rawQuery, FIRST_VISIBLE)));
                contentValues.put(FINISHED, Integer.valueOf(CursorUtil.requireInt(rawQuery, FINISHED)));
                sQLiteDatabase2.insert(TABLE_NAME, (String) null, contentValues);
            } catch (Throwable th) {
                if (rawQuery != null) {
                    try {
                        rawQuery.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        rawQuery.close();
    }
}
