package org.thoughtcrime.securesms;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import java.util.List;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;
import org.thoughtcrime.securesms.groups.ui.RecipientClickListener;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.BottomSheetUtil;

/* loaded from: classes.dex */
public final class GroupMembersDialog {
    private final FragmentActivity fragmentActivity;
    private final Recipient groupRecipient;

    public GroupMembersDialog(FragmentActivity fragmentActivity, Recipient recipient) {
        this.fragmentActivity = fragmentActivity;
        this.groupRecipient = recipient;
    }

    public void display() {
        AlertDialog show = new AlertDialog.Builder(this.fragmentActivity).setTitle(R.string.ConversationActivity_group_members).setIcon(R.drawable.ic_group_24).setCancelable(true).setView(R.layout.dialog_group_members).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
        GroupMemberListView groupMemberListView = (GroupMemberListView) show.findViewById(R.id.list_members);
        groupMemberListView.initializeAdapter(this.fragmentActivity);
        LiveData<List<GroupMemberEntry.FullMember>> fullMembers = new LiveGroup(this.groupRecipient.requireGroupId()).getFullMembers();
        fullMembers.observe(this.fragmentActivity, new GroupMembersDialog$$ExternalSyntheticLambda0(groupMemberListView));
        show.setOnDismissListener(new DialogInterface.OnDismissListener(fullMembers) { // from class: org.thoughtcrime.securesms.GroupMembersDialog$$ExternalSyntheticLambda1
            public final /* synthetic */ LiveData f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                GroupMembersDialog.$r8$lambda$bKETBG2UJnBduS7VMiMGxBL8Wgc(GroupMembersDialog.this, this.f$1, dialogInterface);
            }
        });
        groupMemberListView.setRecipientClickListener(new RecipientClickListener(show) { // from class: org.thoughtcrime.securesms.GroupMembersDialog$$ExternalSyntheticLambda2
            public final /* synthetic */ AlertDialog f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.groups.ui.RecipientClickListener
            public final void onClick(Recipient recipient) {
                GroupMembersDialog.$r8$lambda$_Ov42GE7L6IedFV_wDVzWvzDKq8(GroupMembersDialog.this, this.f$1, recipient);
            }
        });
    }

    public /* synthetic */ void lambda$display$0(LiveData liveData, DialogInterface dialogInterface) {
        liveData.removeObservers(this.fragmentActivity);
    }

    public /* synthetic */ void lambda$display$1(AlertDialog alertDialog, Recipient recipient) {
        alertDialog.dismiss();
        contactClick(recipient);
    }

    private void contactClick(Recipient recipient) {
        RecipientBottomSheetDialogFragment.create(recipient.getId(), this.groupRecipient.requireGroupId()).show(this.fragmentActivity.getSupportFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }
}
