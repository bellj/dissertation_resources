package org.thoughtcrime.securesms.stories.settings.my;

import io.reactivex.rxjava3.functions.Action;
import org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment$getConfiguration$1;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class MyStorySettingsFragment$getConfiguration$1$3$$ExternalSyntheticLambda0 implements Action {
    public final /* synthetic */ MyStorySettingsFragment f$0;

    public /* synthetic */ MyStorySettingsFragment$getConfiguration$1$3$$ExternalSyntheticLambda0(MyStorySettingsFragment myStorySettingsFragment) {
        this.f$0 = myStorySettingsFragment;
    }

    @Override // io.reactivex.rxjava3.functions.Action
    public final void run() {
        MyStorySettingsFragment$getConfiguration$1.AnonymousClass3.m2905invoke$lambda0(this.f$0);
    }
}
