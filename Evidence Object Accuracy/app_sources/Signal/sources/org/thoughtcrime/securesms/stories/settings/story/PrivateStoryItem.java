package org.thoughtcrime.securesms.stories.settings.story;

import android.view.View;
import android.widget.TextView;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListPartialRecord;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: PrivateStoryItem.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001:\n\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "AddViewerModel", "AddViewerViewHolder", "Model", "NewModel", "NewViewHolder", "PartialModel", "PartialViewHolder", "RecipientModel", "RecipientViewHolder", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class PrivateStoryItem {
    public static final PrivateStoryItem INSTANCE = new PrivateStoryItem();

    private PrivateStoryItem() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(NewModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new PrivateStoryItem.NewViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_private_story_new_item));
        mappingAdapter.registerFactory(AddViewerModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new PrivateStoryItem.AddViewerViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_private_story_add_viewer_item));
        mappingAdapter.registerFactory(RecipientModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new PrivateStoryItem.RecipientViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_private_story_recipient_item));
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new PrivateStoryItem.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_private_story_item));
        mappingAdapter.registerFactory(PartialModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new PrivateStoryItem.PartialViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_private_story_item));
    }

    /* compiled from: PrivateStoryItem.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0000H\u0016R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$NewModel;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "onClick", "Lkotlin/Function0;", "", "(Lkotlin/jvm/functions/Function0;)V", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "areItemsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class NewModel extends PreferenceModel<NewModel> {
        private final Function0<Unit> onClick;

        public boolean areItemsTheSame(NewModel newModel) {
            Intrinsics.checkNotNullParameter(newModel, "newItem");
            return true;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public NewModel(Function0<Unit> function0) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(function0, "onClick");
            this.onClick = function0;
        }
    }

    /* compiled from: PrivateStoryItem.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0000H\u0016R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$AddViewerModel;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "onClick", "Lkotlin/Function0;", "", "(Lkotlin/jvm/functions/Function0;)V", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "areItemsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class AddViewerModel extends PreferenceModel<AddViewerModel> {
        private final Function0<Unit> onClick;

        public boolean areItemsTheSame(AddViewerModel addViewerModel) {
            Intrinsics.checkNotNullParameter(addViewerModel, "newItem");
            return true;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public AddViewerModel(Function0<Unit> function0) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(function0, "onClick");
            this.onClick = function0;
        }
    }

    /* compiled from: PrivateStoryItem.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016J\u0010\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$RecipientModel;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "onClick", "Lkotlin/Function1;", "", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lkotlin/jvm/functions/Function1;)V", "getOnClick", "()Lkotlin/jvm/functions/Function1;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class RecipientModel extends PreferenceModel<RecipientModel> {
        private final Function1<RecipientModel, Unit> onClick;
        private final Recipient recipient;

        public final Recipient getRecipient() {
            return this.recipient;
        }

        public final Function1<RecipientModel, Unit> getOnClick() {
            return this.onClick;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r11v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$RecipientModel, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public RecipientModel(Recipient recipient, Function1<? super RecipientModel, Unit> function1) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            Intrinsics.checkNotNullParameter(function1, "onClick");
            this.recipient = recipient;
            this.onClick = function1;
        }

        public boolean areItemsTheSame(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "newItem");
            return Intrinsics.areEqual(recipientModel.recipient, this.recipient);
        }

        public boolean areContentsTheSame(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "newItem");
            return recipientModel.recipient.hasSameContent(this.recipient) && super.areContentsTheSame(recipientModel);
        }
    }

    /* compiled from: PrivateStoryItem.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016J\u0010\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "privateStoryItemData", "Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "onClick", "Lkotlin/Function1;", "", "(Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;Lkotlin/jvm/functions/Function1;)V", "getOnClick", "()Lkotlin/jvm/functions/Function1;", "getPrivateStoryItemData", "()Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final Function1<Model, Unit> onClick;
        private final DistributionListRecord privateStoryItemData;

        public final DistributionListRecord getPrivateStoryItemData() {
            return this.privateStoryItemData;
        }

        public final Function1<Model, Unit> getOnClick() {
            return this.onClick;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r11v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$Model, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(DistributionListRecord distributionListRecord, Function1<? super Model, Unit> function1) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(distributionListRecord, "privateStoryItemData");
            Intrinsics.checkNotNullParameter(function1, "onClick");
            this.privateStoryItemData = distributionListRecord;
            this.onClick = function1;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(model.privateStoryItemData.getId(), this.privateStoryItemData.getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(model.privateStoryItemData, this.privateStoryItemData) && super.areContentsTheSame(model);
        }
    }

    /* compiled from: PrivateStoryItem.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016J\u0010\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$PartialModel;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "privateStoryItemData", "Lorg/thoughtcrime/securesms/database/model/DistributionListPartialRecord;", "onClick", "Lkotlin/Function1;", "", "(Lorg/thoughtcrime/securesms/database/model/DistributionListPartialRecord;Lkotlin/jvm/functions/Function1;)V", "getOnClick", "()Lkotlin/jvm/functions/Function1;", "getPrivateStoryItemData", "()Lorg/thoughtcrime/securesms/database/model/DistributionListPartialRecord;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class PartialModel extends PreferenceModel<PartialModel> {
        private final Function1<PartialModel, Unit> onClick;
        private final DistributionListPartialRecord privateStoryItemData;

        public final DistributionListPartialRecord getPrivateStoryItemData() {
            return this.privateStoryItemData;
        }

        public final Function1<PartialModel, Unit> getOnClick() {
            return this.onClick;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r11v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$PartialModel, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public PartialModel(DistributionListPartialRecord distributionListPartialRecord, Function1<? super PartialModel, Unit> function1) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(distributionListPartialRecord, "privateStoryItemData");
            Intrinsics.checkNotNullParameter(function1, "onClick");
            this.privateStoryItemData = distributionListPartialRecord;
            this.onClick = function1;
        }

        public boolean areItemsTheSame(PartialModel partialModel) {
            Intrinsics.checkNotNullParameter(partialModel, "newItem");
            return Intrinsics.areEqual(partialModel.privateStoryItemData.getId(), this.privateStoryItemData.getId());
        }

        public boolean areContentsTheSame(PartialModel partialModel) {
            Intrinsics.checkNotNullParameter(partialModel, "newItem");
            return Intrinsics.areEqual(partialModel.privateStoryItemData, this.privateStoryItemData) && super.areContentsTheSame(partialModel);
        }
    }

    /* compiled from: PrivateStoryItem.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$RecipientViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$RecipientModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "avatar", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "name", "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class RecipientViewHolder extends MappingViewHolder<RecipientModel> {
        private final AvatarImageView avatar;
        private final TextView name;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public RecipientViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.label);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.label)");
            this.name = (TextView) findViewById;
            View findViewById2 = view.findViewById(R.id.avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.avatar)");
            this.avatar = (AvatarImageView) findViewById2;
        }

        /* renamed from: bind$lambda-0 */
        public static final void m2958bind$lambda0(RecipientModel recipientModel, View view) {
            Intrinsics.checkNotNullParameter(recipientModel, "$model");
            recipientModel.getOnClick().invoke(recipientModel);
        }

        public void bind(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "model");
            this.itemView.setOnClickListener(new PrivateStoryItem$RecipientViewHolder$$ExternalSyntheticLambda0(recipientModel));
            this.avatar.setRecipient(recipientModel.getRecipient());
            if (recipientModel.getRecipient().isSelf()) {
                this.name.setText(R.string.MyStorySettingsFragment__my_story);
            } else {
                this.name.setText(recipientModel.getRecipient().getDisplayName(this.context));
            }
        }
    }

    /* compiled from: PrivateStoryItem.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$NewViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$NewModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class NewViewHolder extends MappingViewHolder<NewModel> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public NewViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }

        /* renamed from: bind$lambda-0 */
        public static final void m2956bind$lambda0(NewModel newModel, View view) {
            Intrinsics.checkNotNullParameter(newModel, "$model");
            newModel.getOnClick().invoke();
        }

        public void bind(NewModel newModel) {
            Intrinsics.checkNotNullParameter(newModel, "model");
            this.itemView.setOnClickListener(new PrivateStoryItem$NewViewHolder$$ExternalSyntheticLambda0(newModel));
        }
    }

    /* compiled from: PrivateStoryItem.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$AddViewerViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$AddViewerModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class AddViewerViewHolder extends MappingViewHolder<AddViewerModel> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public AddViewerViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }

        /* renamed from: bind$lambda-0 */
        public static final void m2955bind$lambda0(AddViewerModel addViewerModel, View view) {
            Intrinsics.checkNotNullParameter(addViewerModel, "$model");
            addViewerModel.getOnClick().invoke();
        }

        public void bind(AddViewerModel addViewerModel) {
            Intrinsics.checkNotNullParameter(addViewerModel, "model");
            this.itemView.setOnClickListener(new PrivateStoryItem$AddViewerViewHolder$$ExternalSyntheticLambda0(addViewerModel));
        }
    }

    /* compiled from: PrivateStoryItem.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", EmojiSearchDatabase.LABEL, "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final TextView label;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.label);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.label)");
            this.label = (TextView) findViewById;
        }

        /* renamed from: bind$lambda-0 */
        public static final void m2960bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClick().invoke(model);
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.itemView.setOnClickListener(new PrivateStoryItem$ViewHolder$$ExternalSyntheticLambda0(model));
            this.label.setText(model.getPrivateStoryItemData().getName());
        }
    }

    /* compiled from: PrivateStoryItem.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$PartialViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/settings/story/PrivateStoryItem$PartialModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", EmojiSearchDatabase.LABEL, "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class PartialViewHolder extends MappingViewHolder<PartialModel> {
        private final TextView label;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public PartialViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.label);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.label)");
            this.label = (TextView) findViewById;
        }

        /* renamed from: bind$lambda-0 */
        public static final void m2957bind$lambda0(PartialModel partialModel, View view) {
            Intrinsics.checkNotNullParameter(partialModel, "$model");
            partialModel.getOnClick().invoke(partialModel);
        }

        public void bind(PartialModel partialModel) {
            Intrinsics.checkNotNullParameter(partialModel, "model");
            this.itemView.setOnClickListener(new PrivateStoryItem$PartialViewHolder$$ExternalSyntheticLambda0(partialModel));
            this.label.setText(partialModel.getPrivateStoryItemData().isUnknown() ? this.context.getString(R.string.MessageRecord_unknown) : partialModel.getPrivateStoryItemData().getName());
        }
    }
}
