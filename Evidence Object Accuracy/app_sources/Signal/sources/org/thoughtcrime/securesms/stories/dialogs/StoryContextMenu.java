package org.thoughtcrime.securesms.stories.dialogs;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.core.app.ShareCompat$IntentBuilder;
import androidx.fragment.app.Fragment;
import io.reactivex.rxjava3.core.Single;
import java.util.ArrayList;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NotImplementedError;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.SignalContextMenu;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu;
import org.thoughtcrime.securesms.stories.landing.StoriesLandingItem;
import org.thoughtcrime.securesms.stories.viewer.page.StoryPost;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState;
import org.thoughtcrime.securesms.util.DeleteDialog;
import org.thoughtcrime.securesms.util.SaveAttachmentTask;

/* compiled from: StoryContextMenu.kt */
@Metadata(d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001:\u00010B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\"\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fJ\u0016\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\rJ\u0016\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0010\u001a\u00020\u0014J4\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u001a2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u000f0\u001cJJ\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u001d\u001a\u00020\b2\u0006\u0010\u001e\u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\b2\b\b\u0002\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\b2\u0006\u0010#\u001a\u00020$H\u0002J¸\u0001\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010%\u001a\u00020&2\u0012\u0010'\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u000f0(2\u0012\u0010*\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u000f0(2\u0012\u0010+\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u000f0(2\u0012\u0010,\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u000f0(2\u0012\u0010-\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u000f0(2\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u000f0(2\u0012\u0010/\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u000f0(2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u000f0\u001cR\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u00061"}, d2 = {"Lorg/thoughtcrime/securesms/stories/dialogs/StoryContextMenu;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "delete", "Lio/reactivex/rxjava3/core/Single;", "", "context", "Landroid/content/Context;", "records", "", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "save", "", "messageRecord", "share", "fragment", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/database/model/MediaMmsMessageRecord;", "show", "anchorView", "Landroid/view/View;", "previewView", "model", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItem$Model;", "onDismiss", "Lkotlin/Function0;", "isFromSelf", "isToGroup", "isFromReleaseChannel", "rootView", "Landroid/view/ViewGroup;", "canHide", "callbacks", "Lorg/thoughtcrime/securesms/stories/dialogs/StoryContextMenu$Callbacks;", "storyViewerPageState", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState;", "onHide", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost;", "onForward", "onShare", "onGoToChat", "onSave", "onDelete", "onInfo", "Callbacks", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryContextMenu {
    public static final StoryContextMenu INSTANCE = new StoryContextMenu();
    private static final String TAG = Log.tag(StoryContextMenu.class);

    /* compiled from: StoryContextMenu.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\t\bb\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\b\u0010\u0005\u001a\u00020\u0003H&J\b\u0010\u0006\u001a\u00020\u0003H&J\b\u0010\u0007\u001a\u00020\u0003H&J\b\u0010\b\u001a\u00020\u0003H&J\b\u0010\t\u001a\u00020\u0003H&J\b\u0010\n\u001a\u00020\u0003H&J\b\u0010\u000b\u001a\u00020\u0003H&¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/dialogs/StoryContextMenu$Callbacks;", "", "onDelete", "", "onDismissed", "onForward", "onGoToChat", "onHide", "onInfo", "onSave", "onShare", "onUnhide", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callbacks {
        void onDelete();

        void onDismissed();

        void onForward();

        void onGoToChat();

        void onHide();

        void onInfo();

        void onSave();

        void onShare();

        void onUnhide();
    }

    private StoryContextMenu() {
    }

    public final Single<Boolean> delete(Context context, Set<? extends MessageRecord> set) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(set, "records");
        DeleteDialog deleteDialog = DeleteDialog.INSTANCE;
        String string = context.getString(R.string.MyStories__delete_story);
        Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri….MyStories__delete_story)");
        return deleteDialog.show(context, set, string, context.getString(R.string.MyStories__this_story_will_be_deleted), true);
    }

    public final void save(Context context, MessageRecord messageRecord) {
        SlideDeck slideDeck;
        Slide firstSlide;
        SlideDeck slideDeck2;
        Slide firstSlide2;
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
        String str = null;
        MediaMmsMessageRecord mediaMmsMessageRecord = messageRecord instanceof MediaMmsMessageRecord ? (MediaMmsMessageRecord) messageRecord : null;
        Uri uri = (mediaMmsMessageRecord == null || (slideDeck2 = mediaMmsMessageRecord.getSlideDeck()) == null || (firstSlide2 = slideDeck2.getFirstSlide()) == null) ? null : firstSlide2.getUri();
        if (!(mediaMmsMessageRecord == null || (slideDeck = mediaMmsMessageRecord.getSlideDeck()) == null || (firstSlide = slideDeck.getFirstSlide()) == null)) {
            str = firstSlide.getContentType();
        }
        if (uri == null || str == null) {
            String str2 = TAG;
            Log.w(str2, "Unable to save story media uri: " + uri + " contentType: " + str);
            Toast.makeText(context, (int) R.string.MyStories__unable_to_save, 0).show();
            return;
        }
        new SaveAttachmentTask(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SaveAttachmentTask.Attachment(uri, str, mediaMmsMessageRecord.getDateSent(), null));
    }

    public final void share(Fragment fragment, MediaMmsMessageRecord mediaMmsMessageRecord) {
        Intrinsics.checkNotNullParameter(fragment, "fragment");
        Intrinsics.checkNotNullParameter(mediaMmsMessageRecord, "messageRecord");
        Slide firstSlide = mediaMmsMessageRecord.getSlideDeck().getFirstSlide();
        Intrinsics.checkNotNull(firstSlide);
        Attachment asAttachment = firstSlide.asAttachment();
        Intrinsics.checkNotNullExpressionValue(asAttachment, "messageRecord.slideDeck.…rstSlide!!.asAttachment()");
        Intent addFlags = new ShareCompat$IntentBuilder(fragment.requireContext()).setStream(asAttachment.getPublicUri()).setType(asAttachment.getContentType()).createChooserIntent().addFlags(1);
        Intrinsics.checkNotNullExpressionValue(addFlags, "IntentBuilder(fragment.r…RANT_READ_URI_PERMISSION)");
        try {
            fragment.startActivity(addFlags);
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, "No activity existed to share the media.", e);
            Toast.makeText(fragment.requireContext(), (int) R.string.MediaPreviewActivity_cant_find_an_app_able_to_share_this_media, 1).show();
        }
    }

    public final void show(Context context, View view, View view2, StoriesLandingItem.Model model, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(view, "anchorView");
        Intrinsics.checkNotNullParameter(view2, "previewView");
        Intrinsics.checkNotNullParameter(model, "model");
        Intrinsics.checkNotNullParameter(function0, "onDismiss");
        show$default(this, context, view, model.getData().getPrimaryStory().getMessageRecord().isOutgoing(), model.getData().getStoryRecipient().isGroup(), model.getData().getStoryRecipient().isReleaseNotes(), null, !model.getData().isHidden(), new Callbacks(model, function0, view2) { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$show$1
            final /* synthetic */ StoriesLandingItem.Model $model;
            final /* synthetic */ Function0<Unit> $onDismiss;
            final /* synthetic */ View $previewView;

            /* access modifiers changed from: package-private */
            {
                this.$model = r1;
                this.$onDismiss = r2;
                this.$previewView = r3;
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onHide() {
                this.$model.getOnHideStory().invoke(this.$model);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onUnhide() {
                this.$model.getOnHideStory().invoke(this.$model);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onForward() {
                this.$model.getOnForwardStory().invoke(this.$model);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onShare() {
                this.$model.getOnShareStory().invoke(this.$model);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onGoToChat() {
                this.$model.getOnGoToChat().invoke(this.$model);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onDismissed() {
                this.$onDismiss.invoke();
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onDelete() {
                this.$model.getOnDeleteStory().invoke(this.$model);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onSave() {
                this.$model.getOnSave().invoke(this.$model);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onInfo() {
                this.$model.getOnInfo().invoke(this.$model, this.$previewView);
            }
        }, 32, null);
    }

    public final void show(Context context, View view, StoryViewerPageState storyViewerPageState, Function1<? super StoryPost, Unit> function1, Function1<? super StoryPost, Unit> function12, Function1<? super StoryPost, Unit> function13, Function1<? super StoryPost, Unit> function14, Function1<? super StoryPost, Unit> function15, Function1<? super StoryPost, Unit> function16, Function1<? super StoryPost, Unit> function17, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(view, "anchorView");
        Intrinsics.checkNotNullParameter(storyViewerPageState, "storyViewerPageState");
        Intrinsics.checkNotNullParameter(function1, "onHide");
        Intrinsics.checkNotNullParameter(function12, "onForward");
        Intrinsics.checkNotNullParameter(function13, "onShare");
        Intrinsics.checkNotNullParameter(function14, "onGoToChat");
        Intrinsics.checkNotNullParameter(function15, "onSave");
        Intrinsics.checkNotNullParameter(function16, "onDelete");
        Intrinsics.checkNotNullParameter(function17, "onInfo");
        Intrinsics.checkNotNullParameter(function0, "onDismiss");
        StoryPost storyPost = storyViewerPageState.getPosts().get(storyViewerPageState.getSelectedPostIndex());
        show$default(this, context, view, storyPost.getSender().isSelf(), storyPost.getGroup() != null, storyPost.getSender().isReleaseNotes(), null, true, new Callbacks(function1, storyPost, function12, function13, function14, function0, function15, function16, function17) { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$show$2
            final /* synthetic */ Function1<StoryPost, Unit> $onDelete;
            final /* synthetic */ Function0<Unit> $onDismiss;
            final /* synthetic */ Function1<StoryPost, Unit> $onForward;
            final /* synthetic */ Function1<StoryPost, Unit> $onGoToChat;
            final /* synthetic */ Function1<StoryPost, Unit> $onHide;
            final /* synthetic */ Function1<StoryPost, Unit> $onInfo;
            final /* synthetic */ Function1<StoryPost, Unit> $onSave;
            final /* synthetic */ Function1<StoryPost, Unit> $onShare;
            final /* synthetic */ StoryPost $selectedStory;

            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.viewer.page.StoryPost, kotlin.Unit> */
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.viewer.page.StoryPost, kotlin.Unit> */
            /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.viewer.page.StoryPost, kotlin.Unit> */
            /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.viewer.page.StoryPost, kotlin.Unit> */
            /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.viewer.page.StoryPost, kotlin.Unit> */
            /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.viewer.page.StoryPost, kotlin.Unit> */
            /* JADX DEBUG: Multi-variable search result rejected for r9v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.viewer.page.StoryPost, kotlin.Unit> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$onHide = r1;
                this.$selectedStory = r2;
                this.$onForward = r3;
                this.$onShare = r4;
                this.$onGoToChat = r5;
                this.$onDismiss = r6;
                this.$onSave = r7;
                this.$onDelete = r8;
                this.$onInfo = r9;
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onHide() {
                this.$onHide.invoke(this.$selectedStory);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public Void onUnhide() {
                throw new NotImplementedError(null, 1, null);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onForward() {
                this.$onForward.invoke(this.$selectedStory);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onShare() {
                this.$onShare.invoke(this.$selectedStory);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onGoToChat() {
                this.$onGoToChat.invoke(this.$selectedStory);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onDismissed() {
                this.$onDismiss.invoke();
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onSave() {
                this.$onSave.invoke(this.$selectedStory);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onDelete() {
                this.$onDelete.invoke(this.$selectedStory);
            }

            @Override // org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu.Callbacks
            public void onInfo() {
                this.$onInfo.invoke(this.$selectedStory);
            }
        }, 32, null);
    }

    static /* synthetic */ void show$default(StoryContextMenu storyContextMenu, Context context, View view, boolean z, boolean z2, boolean z3, ViewGroup viewGroup, boolean z4, Callbacks callbacks, int i, Object obj) {
        ViewGroup viewGroup2;
        if ((i & 32) != 0) {
            View rootView = view.getRootView();
            if (rootView != null) {
                viewGroup2 = (ViewGroup) rootView;
            } else {
                throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
            }
        } else {
            viewGroup2 = viewGroup;
        }
        storyContextMenu.show(context, view, z, z2, z3, viewGroup2, z4, callbacks);
    }

    private final void show(Context context, View view, boolean z, boolean z2, boolean z3, ViewGroup viewGroup, boolean z4, Callbacks callbacks) {
        ArrayList arrayList = new ArrayList();
        if (!z || z2) {
            if (z4) {
                String string = context.getString(R.string.StoriesLandingItem__hide_story);
                Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…sLandingItem__hide_story)");
                arrayList.add(new ActionItem(R.drawable.ic_circle_x_24_tinted, string, 0, new Runnable() { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$$ExternalSyntheticLambda0
                    @Override // java.lang.Runnable
                    public final void run() {
                        StoryContextMenu.m2774$r8$lambda$eDfsMxcuZxvO0PnE0J6T4aX18(StoryContextMenu.Callbacks.this);
                    }
                }, 4, null));
            } else {
                String string2 = context.getString(R.string.StoriesLandingItem__unhide_story);
                Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.stri…andingItem__unhide_story)");
                arrayList.add(new ActionItem(R.drawable.ic_check_circle_24, string2, 0, new Runnable() { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$$ExternalSyntheticLambda1
                    @Override // java.lang.Runnable
                    public final void run() {
                        StoryContextMenu.$r8$lambda$t88NTmLbeXDDBQyVAcVny2krSjE(StoryContextMenu.Callbacks.this);
                    }
                }, 4, null));
            }
        }
        if (z) {
            String string3 = context.getString(R.string.StoriesLandingItem__forward);
            Intrinsics.checkNotNullExpressionValue(string3, "context.getString(R.stri…riesLandingItem__forward)");
            arrayList.add(new ActionItem(R.drawable.ic_forward_24_tinted, string3, 0, new Runnable() { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$$ExternalSyntheticLambda2
                @Override // java.lang.Runnable
                public final void run() {
                    StoryContextMenu.$r8$lambda$fwWinPe37IjjzpWxzmMWGJ6zn0c(StoryContextMenu.Callbacks.this);
                }
            }, 4, null));
            String string4 = context.getString(R.string.StoriesLandingItem__share);
            Intrinsics.checkNotNullExpressionValue(string4, "context.getString(R.stri…toriesLandingItem__share)");
            arrayList.add(new ActionItem(R.drawable.ic_share_24_tinted, string4, 0, new Runnable() { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    StoryContextMenu.m2772$r8$lambda$KDSeKX64NIAruQaQ5McnaP2Mrc(StoryContextMenu.Callbacks.this);
                }
            }, 4, null));
            String string5 = context.getString(R.string.delete);
            Intrinsics.checkNotNullExpressionValue(string5, "context.getString(R.string.delete)");
            arrayList.add(new ActionItem(R.drawable.ic_delete_24_tinted, string5, 0, new Runnable() { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$$ExternalSyntheticLambda4
                @Override // java.lang.Runnable
                public final void run() {
                    StoryContextMenu.$r8$lambda$aFZnBFR8toyRw6eMT9zC_S_THlo(StoryContextMenu.Callbacks.this);
                }
            }, 4, null));
            String string6 = context.getString(R.string.save);
            Intrinsics.checkNotNullExpressionValue(string6, "context.getString(R.string.save)");
            arrayList.add(new ActionItem(R.drawable.ic_download_24_tinted, string6, 0, new Runnable() { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$$ExternalSyntheticLambda5
                @Override // java.lang.Runnable
                public final void run() {
                    StoryContextMenu.m2773$r8$lambda$YQxJCiWo59OzdrDqldt_WMlBUQ(StoryContextMenu.Callbacks.this);
                }
            }, 4, null));
        }
        if ((z2 || !z) && !z3) {
            String string7 = context.getString(R.string.StoriesLandingItem__go_to_chat);
            Intrinsics.checkNotNullExpressionValue(string7, "context.getString(R.stri…sLandingItem__go_to_chat)");
            arrayList.add(new ActionItem(R.drawable.ic_open_24_tinted, string7, 0, new Runnable() { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$$ExternalSyntheticLambda6
                @Override // java.lang.Runnable
                public final void run() {
                    StoryContextMenu.$r8$lambda$hLY0ViYY27vKcGV20ifNm6Ukd88(StoryContextMenu.Callbacks.this);
                }
            }, 4, null));
        }
        String string8 = context.getString(R.string.StoriesLandingItem__info);
        Intrinsics.checkNotNullExpressionValue(string8, "context.getString(R.stri…StoriesLandingItem__info)");
        arrayList.add(new ActionItem(R.drawable.ic_info_outline_message_details_24, string8, 0, new Runnable() { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$$ExternalSyntheticLambda7
            @Override // java.lang.Runnable
            public final void run() {
                StoryContextMenu.$r8$lambda$mNJTCns2vREo2cdiRzO53ZCkP7c(StoryContextMenu.Callbacks.this);
            }
        }, 4, null));
        SignalContextMenu.Builder onDismiss = new SignalContextMenu.Builder(view, viewGroup).preferredHorizontalPosition(SignalContextMenu.HorizontalPosition.START).onDismiss(new Runnable() { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu$$ExternalSyntheticLambda8
            @Override // java.lang.Runnable
            public final void run() {
                StoryContextMenu.$r8$lambda$tGW8qfQhSLkLuq81nJoyqnaaxko(StoryContextMenu.Callbacks.this);
            }
        });
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        onDismiss.offsetY((int) dimensionUnit.toPixels(12.0f)).offsetX((int) dimensionUnit.toPixels(16.0f)).show(arrayList);
    }

    /* renamed from: show$lambda-8$lambda-0 */
    public static final void m2775show$lambda8$lambda0(Callbacks callbacks) {
        Intrinsics.checkNotNullParameter(callbacks, "$callbacks");
        callbacks.onHide();
    }

    /* renamed from: show$lambda-8$lambda-1 */
    public static final void m2776show$lambda8$lambda1(Callbacks callbacks) {
        Intrinsics.checkNotNullParameter(callbacks, "$callbacks");
        callbacks.onUnhide();
    }

    /* renamed from: show$lambda-8$lambda-2 */
    public static final void m2777show$lambda8$lambda2(Callbacks callbacks) {
        Intrinsics.checkNotNullParameter(callbacks, "$callbacks");
        callbacks.onForward();
    }

    /* renamed from: show$lambda-8$lambda-3 */
    public static final void m2778show$lambda8$lambda3(Callbacks callbacks) {
        Intrinsics.checkNotNullParameter(callbacks, "$callbacks");
        callbacks.onShare();
    }

    /* renamed from: show$lambda-8$lambda-4 */
    public static final void m2779show$lambda8$lambda4(Callbacks callbacks) {
        Intrinsics.checkNotNullParameter(callbacks, "$callbacks");
        callbacks.onDelete();
    }

    /* renamed from: show$lambda-8$lambda-5 */
    public static final void m2780show$lambda8$lambda5(Callbacks callbacks) {
        Intrinsics.checkNotNullParameter(callbacks, "$callbacks");
        callbacks.onSave();
    }

    /* renamed from: show$lambda-8$lambda-6 */
    public static final void m2781show$lambda8$lambda6(Callbacks callbacks) {
        Intrinsics.checkNotNullParameter(callbacks, "$callbacks");
        callbacks.onGoToChat();
    }

    /* renamed from: show$lambda-8$lambda-7 */
    public static final void m2782show$lambda8$lambda7(Callbacks callbacks) {
        Intrinsics.checkNotNullParameter(callbacks, "$callbacks");
        callbacks.onInfo();
    }

    /* renamed from: show$lambda-9 */
    public static final void m2783show$lambda9(Callbacks callbacks) {
        Intrinsics.checkNotNullParameter(callbacks, "$callbacks");
        callbacks.onDismissed();
    }
}
