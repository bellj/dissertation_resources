package org.thoughtcrime.securesms.stories;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.IntIterator;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt___RangesKt;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* compiled from: StoryTextView.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0014J\u0010\u0010\u0011\u001a\u00020\u000e2\b\b\u0001\u0010\u0012\u001a\u00020\u0013R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryTextView;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiTextView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "canvasBounds", "Landroid/graphics/Rect;", "textBounds", "Landroid/graphics/RectF;", "wrappedBackgroundPaint", "Landroid/graphics/Paint;", "onDraw", "", "canvas", "Landroid/graphics/Canvas;", "setWrappedBackgroundColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryTextView extends EmojiTextView {
    private final Rect canvasBounds;
    private final RectF textBounds;
    private final Paint wrappedBackgroundPaint;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryTextView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ StoryTextView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        this.canvasBounds = new Rect();
        this.textBounds = new RectF();
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setColor(0);
        this.wrappedBackgroundPaint = paint;
        if (isInEditMode()) {
            paint.setColor(-65536);
        }
    }

    public final void setWrappedBackgroundColor(int i) {
        this.wrappedBackgroundPaint.setColor(i);
        invalidate();
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiTextView, android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        if (!(this.wrappedBackgroundPaint.getColor() == 0 || getLayout() == null)) {
            canvas.getClipBounds(this.canvasBounds);
            this.textBounds.set(this.canvasBounds);
            IntRange intRange = RangesKt___RangesKt.until(0, getLayout().getLineCount());
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(intRange, 10));
            Iterator<Integer> it = intRange.iterator();
            while (it.hasNext()) {
                arrayList.add(Float.valueOf(getLayout().getLineWidth(((IntIterator) it).nextInt())));
            }
            Float maxOrNull = CollectionsKt.maxOrNull((Iterable) arrayList);
            if (maxOrNull != null) {
                this.textBounds.inset((((((float) getWidth()) - maxOrNull.floatValue()) - ((float) getPaddingStart())) - ((float) getPaddingEnd())) / 2.0f, 0.0f);
                RectF rectF = this.textBounds;
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                canvas.drawRoundRect(rectF, dimensionUnit.toPixels(18.0f), dimensionUnit.toPixels(18.0f), this.wrappedBackgroundPaint);
            }
        }
        super.onDraw(canvas);
    }
}
