package org.thoughtcrime.securesms.stories.viewer.text;

import android.graphics.Typeface;
import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryTextPostViewModel$2$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ Typeface f$0;

    public /* synthetic */ StoryTextPostViewModel$2$$ExternalSyntheticLambda0(Typeface typeface) {
        this.f$0 = typeface;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return StoryTextPostViewModel.AnonymousClass2.m3200invoke$lambda0(this.f$0, (StoryTextPostState) obj);
    }
}
