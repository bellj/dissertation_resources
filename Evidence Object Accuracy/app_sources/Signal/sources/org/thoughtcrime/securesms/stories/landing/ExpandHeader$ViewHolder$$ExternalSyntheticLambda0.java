package org.thoughtcrime.securesms.stories.landing;

import android.view.View;
import org.thoughtcrime.securesms.stories.landing.ExpandHeader;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class ExpandHeader$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ExpandHeader.Model f$0;

    public /* synthetic */ ExpandHeader$ViewHolder$$ExternalSyntheticLambda0(ExpandHeader.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ExpandHeader.ViewHolder.m2789bind$lambda0(this.f$0, view);
    }
}
