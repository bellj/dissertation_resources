package org.thoughtcrime.securesms.stories.landing;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import j$.util.function.Consumer;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.stories.landing.StoriesLandingItem;

/* compiled from: StoriesLandingFragment.kt */
/* access modifiers changed from: package-private */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItem$Model;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoriesLandingFragment$createStoryLandingItem$4 extends Lambda implements Function1<StoriesLandingItem.Model, Unit> {
    final /* synthetic */ StoriesLandingFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoriesLandingFragment$createStoryLandingItem$4(StoriesLandingFragment storiesLandingFragment) {
        super(1);
        this.this$0 = storiesLandingFragment;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoriesLandingItem.Model model) {
        invoke(model);
        return Unit.INSTANCE;
    }

    public final void invoke(StoriesLandingItem.Model model) {
        Intrinsics.checkNotNullParameter(model, "it");
        MultiselectForwardFragmentArgs.Companion companion = MultiselectForwardFragmentArgs.Companion;
        Context requireContext = this.this$0.requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        companion.create(requireContext, model.getData().getPrimaryStory().getMultiselectCollection().toSet(), new Consumer() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$createStoryLandingItem$4$$ExternalSyntheticLambda0
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                StoriesLandingFragment$createStoryLandingItem$4.m2804invoke$lambda0(StoriesLandingFragment.this, (MultiselectForwardFragmentArgs) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m2804invoke$lambda0(StoriesLandingFragment storiesLandingFragment, MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
        Intrinsics.checkNotNullParameter(storiesLandingFragment, "this$0");
        Intrinsics.checkNotNullParameter(multiselectForwardFragmentArgs, MultiselectForwardFragment.ARGS);
        MultiselectForwardFragment.Companion companion = MultiselectForwardFragment.Companion;
        FragmentManager childFragmentManager = storiesLandingFragment.getChildFragmentManager();
        Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        companion.showBottomSheet(childFragmentManager, multiselectForwardFragmentArgs);
    }
}
