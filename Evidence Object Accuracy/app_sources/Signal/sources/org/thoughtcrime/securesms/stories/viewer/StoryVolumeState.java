package org.thoughtcrime.securesms.stories.viewer;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;

/* compiled from: StoryVolumeState.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000b\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\r\u001a\u00020\u00032\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryVolumeState;", "", "isMuted", "", "level", "", "(ZI)V", "()Z", "getLevel", "()I", "component1", "component2", "copy", "equals", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryVolumeState {
    private final boolean isMuted;
    private final int level;

    public StoryVolumeState() {
        this(false, 0, 3, null);
    }

    public static /* synthetic */ StoryVolumeState copy$default(StoryVolumeState storyVolumeState, boolean z, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            z = storyVolumeState.isMuted;
        }
        if ((i2 & 2) != 0) {
            i = storyVolumeState.level;
        }
        return storyVolumeState.copy(z, i);
    }

    public final boolean component1() {
        return this.isMuted;
    }

    public final int component2() {
        return this.level;
    }

    public final StoryVolumeState copy(boolean z, int i) {
        return new StoryVolumeState(z, i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryVolumeState)) {
            return false;
        }
        StoryVolumeState storyVolumeState = (StoryVolumeState) obj;
        return this.isMuted == storyVolumeState.isMuted && this.level == storyVolumeState.level;
    }

    public int hashCode() {
        boolean z = this.isMuted;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return (i * 31) + this.level;
    }

    public String toString() {
        return "StoryVolumeState(isMuted=" + this.isMuted + ", level=" + this.level + ')';
    }

    public StoryVolumeState(boolean z, int i) {
        this.isMuted = z;
        this.level = i;
    }

    public /* synthetic */ StoryVolumeState(boolean z, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? true : z, (i2 & 2) != 0 ? -1 : i);
    }

    public final boolean isMuted() {
        return this.isMuted;
    }

    public final int getLevel() {
        return this.level;
    }
}
