package org.thoughtcrime.securesms.stories.viewer.info;

import androidx.core.os.BundleKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.stories.viewer.info.StoryInfoHeader;
import org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRecipientRow;
import org.thoughtcrime.securesms.stories.viewer.info.StoryInfoViewModel;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: StoryInfoBottomSheetDialogFragment.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u001e2\u00020\u0001:\u0002\u001e\u001fB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u001c\u001a\u00020\u001dH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u0006XD¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001b\u0010\r\u001a\u00020\u000e8BX\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "peekHeightPercentage", "", "getPeekHeightPercentage", "()F", "storyId", "", "getStoryId", "()J", "viewModel", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoState;", "onDismiss", "dialog", "Landroid/content/DialogInterface;", "Companion", "OnInfoSheetDismissedListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryInfoBottomSheetDialogFragment extends DSLSettingsBottomSheetFragment {
    public static final Companion Companion = new Companion(null);
    private static final String STORY_ID;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final float peekHeightPercentage = 0.5f;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryInfoViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment$viewModel$2
        final /* synthetic */ StoryInfoBottomSheetDialogFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new StoryInfoViewModel.Factory(StoryInfoBottomSheetDialogFragment.access$getStoryId(this.this$0));
        }
    });

    /* compiled from: StoryInfoBottomSheetDialogFragment.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoBottomSheetDialogFragment$OnInfoSheetDismissedListener;", "", "onInfoSheetDismissed", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface OnInfoSheetDismissedListener {
        void onInfoSheetDismissed();
    }

    public StoryInfoBottomSheetDialogFragment() {
        super(0, null, 0.0f, 7, null);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment, org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    protected float getPeekHeightPercentage() {
        return this.peekHeightPercentage;
    }

    /* compiled from: StoryInfoBottomSheetDialogFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoBottomSheetDialogFragment$Companion;", "", "()V", "STORY_ID", "", "create", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoBottomSheetDialogFragment;", "storyId", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final StoryInfoBottomSheetDialogFragment create(long j) {
            StoryInfoBottomSheetDialogFragment storyInfoBottomSheetDialogFragment = new StoryInfoBottomSheetDialogFragment();
            storyInfoBottomSheetDialogFragment.setArguments(BundleKt.bundleOf(TuplesKt.to(StoryInfoBottomSheetDialogFragment.STORY_ID, Long.valueOf(j))));
            return storyInfoBottomSheetDialogFragment;
        }
    }

    public final long getStoryId() {
        return requireArguments().getLong(STORY_ID);
    }

    private final StoryInfoViewModel getViewModel() {
        return (StoryInfoViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        StoryInfoHeader.INSTANCE.register(dSLSettingsAdapter);
        StoryInfoRecipientRow.INSTANCE.register(dSLSettingsAdapter);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getState().subscribe(new Consumer(this) { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ StoryInfoBottomSheetDialogFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryInfoBottomSheetDialogFragment.$r8$lambda$4NpaZZ4M0L1TXNymRCQox3Tje8Y(DSLSettingsAdapter.this, this.f$1, (StoryInfoState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.state.subscrib…odelList())\n      }\n    }");
        lifecycleDisposable2.plusAssign(subscribe);
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m3017bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, StoryInfoBottomSheetDialogFragment storyInfoBottomSheetDialogFragment, StoryInfoState storyInfoState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(storyInfoBottomSheetDialogFragment, "this$0");
        if (storyInfoState.isLoaded()) {
            Intrinsics.checkNotNullExpressionValue(storyInfoState, "state");
            dSLSettingsAdapter.submitList(storyInfoBottomSheetDialogFragment.getConfiguration(storyInfoState).toMappingModelList());
        }
    }

    private final DSLConfiguration getConfiguration(StoryInfoState storyInfoState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(storyInfoState) { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment$getConfiguration$1
            final /* synthetic */ StoryInfoState $state;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.customPref(new StoryInfoHeader.Model(this.$state.getSentMillis(), this.$state.getReceivedMillis(), this.$state.getSize()));
                dSLConfiguration.sectionHeaderPref(this.$state.isOutgoing() ? R.string.StoryInfoBottomSheetDialogFragment__sent_to : R.string.StoryInfoBottomSheetDialogFragment__sent_from);
                for (StoryInfoRecipientRow.Model model : this.$state.getRecipients()) {
                    dSLConfiguration.customPref(model);
                }
            }
        });
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:15:0x0023 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v2, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r2v5, types: [org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment$OnInfoSheetDismissedListener] */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARN: Type inference failed for: r2v11 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDismiss(android.content.DialogInterface r2) {
        /*
            r1 = this;
            java.lang.String r0 = "dialog"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r2, r0)
            super.onDismiss(r2)
            androidx.fragment.app.Fragment r2 = r1.getParentFragment()
        L_0x000c:
            if (r2 == 0) goto L_0x0018
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment.OnInfoSheetDismissedListener
            if (r0 == 0) goto L_0x0013
            goto L_0x0023
        L_0x0013:
            androidx.fragment.app.Fragment r2 = r2.getParentFragment()
            goto L_0x000c
        L_0x0018:
            androidx.fragment.app.FragmentActivity r2 = r1.requireActivity()
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment.OnInfoSheetDismissedListener
            if (r0 != 0) goto L_0x0021
            r2 = 0
        L_0x0021:
            org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment$OnInfoSheetDismissedListener r2 = (org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment.OnInfoSheetDismissedListener) r2
        L_0x0023:
            org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment$OnInfoSheetDismissedListener r2 = (org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment.OnInfoSheetDismissedListener) r2
            if (r2 == 0) goto L_0x002a
            r2.onInfoSheetDismissed()
        L_0x002a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment.onDismiss(android.content.DialogInterface):void");
    }
}
