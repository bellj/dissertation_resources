package org.thoughtcrime.securesms.stories.viewer.reply.group;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ReplyBody.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0016\u0017\u0018B\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0000H\u0016R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012\u0001\u0003\u0019\u001a\u001b¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;", "", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "(Lorg/thoughtcrime/securesms/database/model/MessageRecord;)V", "key", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "getKey", "()Lorg/thoughtcrime/securesms/database/model/MessageId;", "getMessageRecord", "()Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "sender", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getSender", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "sentAtMillis", "", "getSentAtMillis", "()J", "hasSameContent", "", "other", "Reaction", "RemoteDelete", "Text", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$Text;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$Reaction;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$RemoteDelete;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public abstract class ReplyBody {
    private final MessageId key;
    private final MessageRecord messageRecord;
    private final Recipient sender;
    private final long sentAtMillis;

    public /* synthetic */ ReplyBody(MessageRecord messageRecord, DefaultConstructorMarker defaultConstructorMarker) {
        this(messageRecord);
    }

    private ReplyBody(MessageRecord messageRecord) {
        String str;
        Recipient recipient;
        this.messageRecord = messageRecord;
        this.key = new MessageId(messageRecord.getId(), true);
        if (messageRecord.isOutgoing()) {
            recipient = Recipient.self();
            str = "self()";
        } else {
            recipient = messageRecord.getIndividualRecipient().resolve();
            str = "messageRecord.individualRecipient.resolve()";
        }
        Intrinsics.checkNotNullExpressionValue(recipient, str);
        this.sender = recipient;
        this.sentAtMillis = messageRecord.getDateSent();
    }

    public final MessageRecord getMessageRecord() {
        return this.messageRecord;
    }

    public final MessageId getKey() {
        return this.key;
    }

    public final Recipient getSender() {
        return this.sender;
    }

    public final long getSentAtMillis() {
        return this.sentAtMillis;
    }

    public boolean hasSameContent(ReplyBody replyBody) {
        Intrinsics.checkNotNullParameter(replyBody, "other");
        return Intrinsics.areEqual(this.key, replyBody.key) && this.sender.hasSameContent(replyBody.sender) && this.sentAtMillis == replyBody.sentAtMillis;
    }

    /* compiled from: ReplyBody.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0001H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$Text;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;", "message", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "(Lorg/thoughtcrime/securesms/conversation/ConversationMessage;)V", "getMessage", "()Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "hasSameContent", "", "other", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Text extends ReplyBody {
        private final ConversationMessage message;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public Text(org.thoughtcrime.securesms.conversation.ConversationMessage r3) {
            /*
                r2 = this;
                java.lang.String r0 = "message"
                kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r0)
                org.thoughtcrime.securesms.database.model.MessageRecord r0 = r3.getMessageRecord()
                java.lang.String r1 = "message.messageRecord"
                kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.message = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody.Text.<init>(org.thoughtcrime.securesms.conversation.ConversationMessage):void");
        }

        public final ConversationMessage getMessage() {
            return this.message;
        }

        @Override // org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody
        public boolean hasSameContent(ReplyBody replyBody) {
            Intrinsics.checkNotNullParameter(replyBody, "other");
            if (!ReplyBody.super.hasSameContent(replyBody)) {
                return false;
            }
            if ((replyBody instanceof Text ? (Text) replyBody : null) != null ? Intrinsics.areEqual(getMessageRecord().getBody(), replyBody.getMessageRecord().getBody()) : false) {
                return true;
            }
            return false;
        }
    }

    /* compiled from: ReplyBody.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0001H\u0016R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$Reaction;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "(Lorg/thoughtcrime/securesms/database/model/MessageRecord;)V", "emoji", "", "getEmoji", "()Ljava/lang/CharSequence;", "hasSameContent", "", "other", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Reaction extends ReplyBody {
        private final CharSequence emoji;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Reaction(MessageRecord messageRecord) {
            super(messageRecord, null);
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            String body = messageRecord.getBody();
            Intrinsics.checkNotNullExpressionValue(body, "messageRecord.body");
            this.emoji = body;
        }

        public final CharSequence getEmoji() {
            return this.emoji;
        }

        @Override // org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody
        public boolean hasSameContent(ReplyBody replyBody) {
            Intrinsics.checkNotNullParameter(replyBody, "other");
            if (!ReplyBody.super.hasSameContent(replyBody)) {
                return false;
            }
            if ((replyBody instanceof Reaction ? (Reaction) replyBody : null) != null ? Intrinsics.areEqual(this.emoji, ((Reaction) replyBody).emoji) : false) {
                return true;
            }
            return false;
        }
    }

    /* compiled from: ReplyBody.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$RemoteDelete;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "(Lorg/thoughtcrime/securesms/database/model/MessageRecord;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class RemoteDelete extends ReplyBody {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public RemoteDelete(MessageRecord messageRecord) {
            super(messageRecord, null);
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
        }
    }
}
