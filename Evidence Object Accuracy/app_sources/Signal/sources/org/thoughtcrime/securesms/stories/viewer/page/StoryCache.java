package org.thoughtcrime.securesms.stories.viewer.page;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import kotlin.sequences.SequencesKt___SequencesKt;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.stories.viewer.page.StoryDisplay;

/* compiled from: StoryCache.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0003\u0013\u0014\u0015B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u000b\u001a\u00020\fJ\u0010\u0010\r\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000e\u001a\u00020\tJ\u0014\u0010\u000f\u001a\u00020\f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache;", "", "glideRequests", "Lorg/thoughtcrime/securesms/mms/GlideRequests;", "storySize", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryDisplay$Size;", "(Lorg/thoughtcrime/securesms/mms/GlideRequests;Lorg/thoughtcrime/securesms/stories/viewer/page/StoryDisplay$Size;)V", "cache", "", "Landroid/net/Uri;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$StoryCacheValue;", "clear", "", "getFromCache", "uri", "prefetch", "attachments", "", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "Listener", "StoryCacheTarget", "StoryCacheValue", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryCache {
    private final Map<Uri, StoryCacheValue> cache = new LinkedHashMap();
    private final GlideRequests glideRequests;
    private final StoryDisplay.Size storySize;

    /* compiled from: StoryCache.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$Listener;", "", "onLoadFailed", "", "onResourceReady", "resource", "Landroid/graphics/drawable/Drawable;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Listener {
        void onLoadFailed();

        void onResourceReady(Drawable drawable);
    }

    public StoryCache(GlideRequests glideRequests, StoryDisplay.Size size) {
        Intrinsics.checkNotNullParameter(glideRequests, "glideRequests");
        Intrinsics.checkNotNullParameter(size, "storySize");
        this.glideRequests = glideRequests;
        this.storySize = size;
    }

    public final void prefetch(List<? extends Attachment> list) {
        StoryCacheTarget storyCacheTarget;
        Intrinsics.checkNotNullParameter(list, "attachments");
        List list2 = SequencesKt___SequencesKt.toList(SequencesKt___SequencesKt.filter(SequencesKt___SequencesKt.filter(SequencesKt___SequencesKt.filter(CollectionsKt___CollectionsKt.asSequence(list), new Function1<Attachment, Boolean>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryCache$prefetch$prefetchableAttachments$1
            final /* synthetic */ StoryCache this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            public final Boolean invoke(Attachment attachment) {
                Intrinsics.checkNotNullParameter(attachment, "it");
                return Boolean.valueOf(attachment.getUri() != null && !StoryCache.access$getCache$p(this.this$0).containsKey(attachment.getUri()));
            }
        }), StoryCache$prefetch$prefetchableAttachments$2.INSTANCE), StoryCache$prefetch$prefetchableAttachments$3.INSTANCE));
        LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(list2, 10)), 16));
        for (Object obj : list2) {
            Attachment attachment = (Attachment) obj;
            GlideRequests glideRequests = this.glideRequests;
            Uri uri = attachment.getUri();
            Intrinsics.checkNotNull(uri);
            GlideRequest<Drawable> load = glideRequests.load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri));
            Priority priority = Priority.HIGH;
            GlideRequest<Drawable> priority2 = load.priority(priority);
            Uri uri2 = attachment.getUri();
            Intrinsics.checkNotNull(uri2);
            Target into = priority2.into((GlideRequest<Drawable>) new StoryCacheTarget(this, uri2, this.storySize));
            Intrinsics.checkNotNullExpressionValue(into, "glideRequests\n        .l…chment.uri!!, storySize))");
            StoryCacheTarget storyCacheTarget2 = (StoryCacheTarget) into;
            if (attachment.getBlurHash() != null) {
                GlideRequest<Drawable> priority3 = this.glideRequests.load((Object) attachment.getBlurHash()).priority(priority);
                Uri uri3 = attachment.getUri();
                Intrinsics.checkNotNull(uri3);
                storyCacheTarget = (StoryCacheTarget) priority3.into((GlideRequest<Drawable>) new StoryCacheTarget(this, uri3, this.storySize));
            } else {
                storyCacheTarget = null;
            }
            linkedHashMap.put(obj, new StoryCacheValue(storyCacheTarget2, storyCacheTarget));
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(MapsKt__MapsJVMKt.mapCapacity(linkedHashMap.size()));
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            Uri uri4 = ((Attachment) entry.getKey()).getUri();
            Intrinsics.checkNotNull(uri4);
            linkedHashMap2.put(uri4, entry.getValue());
        }
        this.cache.putAll(linkedHashMap2);
    }

    public final void clear() {
        for (StoryCacheValue storyCacheValue : new ArrayList(this.cache.values())) {
            this.glideRequests.clear(storyCacheValue.getImageTarget());
            StoryCacheTarget blurTarget = storyCacheValue.getBlurTarget();
            if (blurTarget != null) {
                this.glideRequests.clear(blurTarget);
            }
        }
        this.cache.clear();
    }

    public final StoryCacheValue getFromCache(Uri uri) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        return this.cache.get(uri);
    }

    /* compiled from: StoryCache.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001f\u0012\n\u0010\u0002\u001a\u00060\u0003R\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0018\u00010\u0003R\u00020\u0004¢\u0006\u0002\u0010\u0006J\r\u0010\n\u001a\u00060\u0003R\u00020\u0004HÆ\u0003J\u000f\u0010\u000b\u001a\b\u0018\u00010\u0003R\u00020\u0004HÆ\u0003J'\u0010\f\u001a\u00020\u00002\f\b\u0002\u0010\u0002\u001a\u00060\u0003R\u00020\u00042\u000e\b\u0002\u0010\u0005\u001a\b\u0018\u00010\u0003R\u00020\u0004HÆ\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0017\u0010\u0005\u001a\b\u0018\u00010\u0003R\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0015\u0010\u0002\u001a\u00060\u0003R\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\b¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$StoryCacheValue;", "", "imageTarget", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$StoryCacheTarget;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache;", "blurTarget", "(Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$StoryCacheTarget;Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$StoryCacheTarget;)V", "getBlurTarget", "()Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$StoryCacheTarget;", "getImageTarget", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class StoryCacheValue {
        private final StoryCacheTarget blurTarget;
        private final StoryCacheTarget imageTarget;

        public static /* synthetic */ StoryCacheValue copy$default(StoryCacheValue storyCacheValue, StoryCacheTarget storyCacheTarget, StoryCacheTarget storyCacheTarget2, int i, Object obj) {
            if ((i & 1) != 0) {
                storyCacheTarget = storyCacheValue.imageTarget;
            }
            if ((i & 2) != 0) {
                storyCacheTarget2 = storyCacheValue.blurTarget;
            }
            return storyCacheValue.copy(storyCacheTarget, storyCacheTarget2);
        }

        public final StoryCacheTarget component1() {
            return this.imageTarget;
        }

        public final StoryCacheTarget component2() {
            return this.blurTarget;
        }

        public final StoryCacheValue copy(StoryCacheTarget storyCacheTarget, StoryCacheTarget storyCacheTarget2) {
            Intrinsics.checkNotNullParameter(storyCacheTarget, "imageTarget");
            return new StoryCacheValue(storyCacheTarget, storyCacheTarget2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoryCacheValue)) {
                return false;
            }
            StoryCacheValue storyCacheValue = (StoryCacheValue) obj;
            return Intrinsics.areEqual(this.imageTarget, storyCacheValue.imageTarget) && Intrinsics.areEqual(this.blurTarget, storyCacheValue.blurTarget);
        }

        public int hashCode() {
            int hashCode = this.imageTarget.hashCode() * 31;
            StoryCacheTarget storyCacheTarget = this.blurTarget;
            return hashCode + (storyCacheTarget == null ? 0 : storyCacheTarget.hashCode());
        }

        public String toString() {
            return "StoryCacheValue(imageTarget=" + this.imageTarget + ", blurTarget=" + this.blurTarget + ')';
        }

        public StoryCacheValue(StoryCacheTarget storyCacheTarget, StoryCacheTarget storyCacheTarget2) {
            Intrinsics.checkNotNullParameter(storyCacheTarget, "imageTarget");
            this.imageTarget = storyCacheTarget;
            this.blurTarget = storyCacheTarget2;
        }

        public final StoryCacheTarget getBlurTarget() {
            return this.blurTarget;
        }

        public final StoryCacheTarget getImageTarget() {
            return this.imageTarget;
        }
    }

    /* compiled from: StoryCache.kt */
    @Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\fJ\u0012\u0010\u0013\u001a\u00020\u00112\b\u0010\u0014\u001a\u0004\u0018\u00010\u0002H\u0016J\u0012\u0010\u0015\u001a\u00020\u00112\b\u0010\u0016\u001a\u0004\u0018\u00010\u0002H\u0016J\"\u0010\u0017\u001a\u00020\u00112\u0006\u0010\r\u001a\u00020\u00022\u0010\u0010\u0018\u001a\f\u0012\u0006\b\u0000\u0012\u00020\u0002\u0018\u00010\u0019H\u0016J\u000e\u0010\u001a\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\fR\u000e\u0010\b\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u0002X\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$StoryCacheTarget;", "Lcom/bumptech/glide/request/target/CustomTarget;", "Landroid/graphics/drawable/Drawable;", "uri", "Landroid/net/Uri;", MediaPreviewActivity.SIZE_EXTRA, "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryDisplay$Size;", "(Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache;Landroid/net/Uri;Lorg/thoughtcrime/securesms/stories/viewer/page/StoryDisplay$Size;)V", "isFailed", "", "listeners", "", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$Listener;", "resource", "getUri", "()Landroid/net/Uri;", "addListener", "", "listener", "onLoadCleared", "placeholder", "onLoadFailed", "errorDrawable", "onResourceReady", "transition", "Lcom/bumptech/glide/request/transition/Transition;", "removeListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public final class StoryCacheTarget extends CustomTarget<Drawable> {
        private boolean isFailed;
        private final Set<Listener> listeners = new LinkedHashSet();
        private Drawable resource;
        final /* synthetic */ StoryCache this$0;
        private final Uri uri;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public StoryCacheTarget(StoryCache storyCache, Uri uri, StoryDisplay.Size size) {
            super(size.getWidth(), size.getHeight());
            Intrinsics.checkNotNullParameter(uri, "uri");
            Intrinsics.checkNotNullParameter(size, MediaPreviewActivity.SIZE_EXTRA);
            this.this$0 = storyCache;
            this.uri = uri;
        }

        public final Uri getUri() {
            return this.uri;
        }

        @Override // com.bumptech.glide.request.target.Target
        public /* bridge */ /* synthetic */ void onResourceReady(Object obj, Transition transition) {
            onResourceReady((Drawable) obj, (Transition<? super Drawable>) transition);
        }

        public final void addListener(Listener listener) {
            Intrinsics.checkNotNullParameter(listener, "listener");
            this.listeners.add(listener);
            Drawable drawable = this.resource;
            if (drawable != null) {
                listener.onResourceReady(drawable);
            }
            if (this.isFailed) {
                listener.onLoadFailed();
            }
        }

        public final void removeListener(Listener listener) {
            Intrinsics.checkNotNullParameter(listener, "listener");
            this.listeners.remove(listener);
        }

        public void onResourceReady(Drawable drawable, Transition<? super Drawable> transition) {
            Intrinsics.checkNotNullParameter(drawable, "resource");
            this.resource = drawable;
            for (Listener listener : this.listeners) {
                listener.onResourceReady(drawable);
            }
        }

        @Override // com.bumptech.glide.request.target.CustomTarget, com.bumptech.glide.request.target.Target
        public void onLoadFailed(Drawable drawable) {
            super.onLoadFailed(drawable);
            this.isFailed = true;
            for (Listener listener : this.listeners) {
                listener.onLoadFailed();
            }
        }

        @Override // com.bumptech.glide.request.target.Target
        public void onLoadCleared(Drawable drawable) {
            this.resource = null;
            this.isFailed = false;
            this.this$0.cache.remove(this.uri);
        }
    }
}
