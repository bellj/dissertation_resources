package org.thoughtcrime.securesms.stories.viewer.page;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: StoryViewerDialog.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0007\b\t\nB\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\u0003\u000b\f\r¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog;", "", "type", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog$Type;", "(Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog$Type;)V", "getType", "()Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog$Type;", "Delete", "Forward", "GroupDirectReply", "Type", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog$GroupDirectReply;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog$Forward;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog$Delete;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public abstract class StoryViewerDialog {
    private final Type type;

    /* compiled from: StoryViewerDialog.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog$Type;", "", "(Ljava/lang/String;I)V", "DIRECT_REPLY", "FORWARD", "DELETE", "CONTEXT_MENU", "VIEWS_AND_REPLIES", "INFO", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum Type {
        DIRECT_REPLY,
        FORWARD,
        DELETE,
        CONTEXT_MENU,
        VIEWS_AND_REPLIES,
        INFO
    }

    public /* synthetic */ StoryViewerDialog(Type type, DefaultConstructorMarker defaultConstructorMarker) {
        this(type);
    }

    private StoryViewerDialog(Type type) {
        this.type = type;
    }

    public final Type getType() {
        return this.type;
    }

    /* compiled from: StoryViewerDialog.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog$GroupDirectReply;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "storyId", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;J)V", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getStoryId", "()J", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class GroupDirectReply extends StoryViewerDialog {
        private final RecipientId recipientId;
        private final long storyId;

        public static /* synthetic */ GroupDirectReply copy$default(GroupDirectReply groupDirectReply, RecipientId recipientId, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = groupDirectReply.recipientId;
            }
            if ((i & 2) != 0) {
                j = groupDirectReply.storyId;
            }
            return groupDirectReply.copy(recipientId, j);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final long component2() {
            return this.storyId;
        }

        public final GroupDirectReply copy(RecipientId recipientId, long j) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return new GroupDirectReply(recipientId, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof GroupDirectReply)) {
                return false;
            }
            GroupDirectReply groupDirectReply = (GroupDirectReply) obj;
            return Intrinsics.areEqual(this.recipientId, groupDirectReply.recipientId) && this.storyId == groupDirectReply.storyId;
        }

        public int hashCode() {
            return (this.recipientId.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.storyId);
        }

        public String toString() {
            return "GroupDirectReply(recipientId=" + this.recipientId + ", storyId=" + this.storyId + ')';
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final long getStoryId() {
            return this.storyId;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public GroupDirectReply(RecipientId recipientId, long j) {
            super(Type.DIRECT_REPLY, null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
            this.storyId = j;
        }
    }

    /* compiled from: StoryViewerDialog.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog$Forward;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Forward extends StoryViewerDialog {
        public static final Forward INSTANCE = new Forward();

        private Forward() {
            super(Type.FORWARD, null);
        }
    }

    /* compiled from: StoryViewerDialog.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog$Delete;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Delete extends StoryViewerDialog {
        public static final Delete INSTANCE = new Delete();

        private Delete() {
            super(Type.DELETE, null);
        }
    }
}
