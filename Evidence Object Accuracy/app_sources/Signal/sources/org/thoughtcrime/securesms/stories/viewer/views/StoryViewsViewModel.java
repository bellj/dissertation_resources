package org.thoughtcrime.securesms.stories.viewer.views;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.stories.viewer.views.StoryViewsState;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: StoryViewsViewModel.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0013B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0010\u001a\u00020\u0011H\u0014J\u0006\u0010\u0012\u001a\u00020\u0011R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsViewModel;", "Landroidx/lifecycle/ViewModel;", "storyId", "", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsRepository;", "(JLorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "refresh", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewsViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final StoryViewsRepository repository;
    private final LiveData<StoryViewsState> state;
    private final Store<StoryViewsState> store;
    private final long storyId;

    public StoryViewsViewModel(long j, StoryViewsRepository storyViewsRepository) {
        Intrinsics.checkNotNullParameter(storyViewsRepository, "repository");
        this.storyId = j;
        this.repository = storyViewsRepository;
        Store<StoryViewsState> store = new Store<>(new StoryViewsState(StoryViewsState.LoadState.INIT, null, 2, null));
        this.store = store;
        LiveData<StoryViewsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<StoryViewsState> getState() {
        return this.state;
    }

    public final void refresh() {
        if (this.repository.isReadReceiptsEnabled()) {
            CompositeDisposable compositeDisposable = this.disposables;
            Disposable subscribe = this.repository.getViews(this.storyId).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsViewModel$$ExternalSyntheticLambda1
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    StoryViewsViewModel.m3212refresh$lambda1(StoryViewsViewModel.this, (List) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getViews(stor…      )\n        }\n      }");
            DisposableKt.plusAssign(compositeDisposable, subscribe);
            return;
        }
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewsViewModel.m3214refresh$lambda2((StoryViewsState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-1 */
    public static final void m3212refresh$lambda1(StoryViewsViewModel storyViewsViewModel, List list) {
        Intrinsics.checkNotNullParameter(storyViewsViewModel, "this$0");
        storyViewsViewModel.store.update(new Function(list) { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewsViewModel.m3213refresh$lambda1$lambda0(this.f$0, (StoryViewsState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-1$lambda-0 */
    public static final StoryViewsState m3213refresh$lambda1$lambda0(List list, StoryViewsState storyViewsState) {
        StoryViewsState.LoadState loadState = StoryViewsState.LoadState.READY;
        Intrinsics.checkNotNullExpressionValue(list, "data");
        return storyViewsState.copy(loadState, list);
    }

    /* renamed from: refresh$lambda-2 */
    public static final StoryViewsState m3214refresh$lambda2(StoryViewsState storyViewsState) {
        Intrinsics.checkNotNullExpressionValue(storyViewsState, "it");
        return StoryViewsState.copy$default(storyViewsState, StoryViewsState.LoadState.DISABLED, null, 2, null);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    /* compiled from: StoryViewsViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "storyId", "", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsRepository;", "(JLorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final StoryViewsRepository repository;
        private final long storyId;

        public Factory(long j, StoryViewsRepository storyViewsRepository) {
            Intrinsics.checkNotNullParameter(storyViewsRepository, "repository");
            this.storyId = j;
            this.repository = storyViewsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StoryViewsViewModel(this.storyId, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.viewer.views.StoryViewsViewModel.Factory.create");
        }
    }
}
