package org.thoughtcrime.securesms.stories.viewer.views;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: StoryViewsState.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u0016B\u001f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsState;", "", "loadState", "Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsState$LoadState;", "views", "", "Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewItemData;", "(Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsState$LoadState;Ljava/util/List;)V", "getLoadState", "()Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsState$LoadState;", "getViews", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "LoadState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewsState {
    private final LoadState loadState;
    private final List<StoryViewItemData> views;

    /* compiled from: StoryViewsState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsState$LoadState;", "", "(Ljava/lang/String;I)V", "INIT", "READY", "DISABLED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum LoadState {
        INIT,
        READY,
        DISABLED
    }

    public StoryViewsState() {
        this(null, null, 3, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ StoryViewsState copy$default(StoryViewsState storyViewsState, LoadState loadState, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            loadState = storyViewsState.loadState;
        }
        if ((i & 2) != 0) {
            list = storyViewsState.views;
        }
        return storyViewsState.copy(loadState, list);
    }

    public final LoadState component1() {
        return this.loadState;
    }

    public final List<StoryViewItemData> component2() {
        return this.views;
    }

    public final StoryViewsState copy(LoadState loadState, List<StoryViewItemData> list) {
        Intrinsics.checkNotNullParameter(loadState, "loadState");
        Intrinsics.checkNotNullParameter(list, "views");
        return new StoryViewsState(loadState, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryViewsState)) {
            return false;
        }
        StoryViewsState storyViewsState = (StoryViewsState) obj;
        return this.loadState == storyViewsState.loadState && Intrinsics.areEqual(this.views, storyViewsState.views);
    }

    public int hashCode() {
        return (this.loadState.hashCode() * 31) + this.views.hashCode();
    }

    public String toString() {
        return "StoryViewsState(loadState=" + this.loadState + ", views=" + this.views + ')';
    }

    public StoryViewsState(LoadState loadState, List<StoryViewItemData> list) {
        Intrinsics.checkNotNullParameter(loadState, "loadState");
        Intrinsics.checkNotNullParameter(list, "views");
        this.loadState = loadState;
        this.views = list;
    }

    public /* synthetic */ StoryViewsState(LoadState loadState, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? LoadState.INIT : loadState, (i & 2) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list);
    }

    public final LoadState getLoadState() {
        return this.loadState;
    }

    public final List<StoryViewItemData> getViews() {
        return this.views;
    }
}
