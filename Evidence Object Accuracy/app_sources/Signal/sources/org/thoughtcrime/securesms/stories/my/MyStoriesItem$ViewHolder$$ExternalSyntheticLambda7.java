package org.thoughtcrime.securesms.stories.my;

import android.view.View;
import org.thoughtcrime.securesms.stories.my.MyStoriesItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class MyStoriesItem$ViewHolder$$ExternalSyntheticLambda7 implements View.OnClickListener {
    public final /* synthetic */ MyStoriesItem.Model f$0;

    public /* synthetic */ MyStoriesItem$ViewHolder$$ExternalSyntheticLambda7(MyStoriesItem.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        MyStoriesItem.ViewHolder.m2842bind$lambda4(this.f$0, view);
    }
}
