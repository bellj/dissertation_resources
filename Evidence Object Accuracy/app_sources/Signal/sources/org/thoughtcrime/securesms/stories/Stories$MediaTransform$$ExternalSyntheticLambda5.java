package org.thoughtcrime.securesms.stories;

import kotlin.jvm.internal.Ref$ObjectRef;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class Stories$MediaTransform$$ExternalSyntheticLambda5 implements Runnable {
    public final /* synthetic */ Ref$ObjectRef f$0;

    public /* synthetic */ Stories$MediaTransform$$ExternalSyntheticLambda5(Ref$ObjectRef ref$ObjectRef) {
        this.f$0 = ref$ObjectRef;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Stories.MediaTransform.m2765getVideoDuration$lambda5(this.f$0);
    }
}
