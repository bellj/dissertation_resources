package org.thoughtcrime.securesms.stories.viewer.text;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import java.util.ArrayList;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment;
import org.thoughtcrime.securesms.stories.StoryTextPostView;
import org.thoughtcrime.securesms.stories.viewer.page.StoryPost;
import org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostState;
import org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.FragmentDialogs;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: StoryTextPostPreviewFragment.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00132\u00020\u0001:\u0002\u0012\u0013B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u0018\u0010\u000f\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0003R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostPreviewFragment;", "Landroidx/fragment/app/Fragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "showLinkPreviewTooltip", "linkPreview", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;", "Callback", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryTextPostPreviewFragment extends Fragment {
    public static final Companion Companion = new Companion(null);
    private static final String STORY_ID;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryTextPostViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment$viewModel$2
        final /* synthetic */ StoryTextPostPreviewFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new StoryTextPostViewModel.Factory(this.this$0.requireArguments().getLong("STORY_ID"), new StoryTextPostRepository());
        }
    });

    /* compiled from: StoryTextPostPreviewFragment.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostPreviewFragment$Callback;", "", "setIsDisplayingLinkPreviewTooltip", "", "isDisplayingLinkPreviewTooltip", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void setIsDisplayingLinkPreviewTooltip(boolean z);
    }

    /* compiled from: StoryTextPostPreviewFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[StoryTextPostState.LoadState.values().length];
            iArr[StoryTextPostState.LoadState.INIT.ordinal()] = 1;
            iArr[StoryTextPostState.LoadState.LOADED.ordinal()] = 2;
            iArr[StoryTextPostState.LoadState.FAILED.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public StoryTextPostPreviewFragment() {
        super(R.layout.stories_text_post_preview_fragment);
    }

    /* compiled from: StoryTextPostPreviewFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostPreviewFragment$Companion;", "", "()V", StoryTextPostPreviewFragment.STORY_ID, "", "create", "Landroidx/fragment/app/Fragment;", "content", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content$TextContent;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Fragment create(StoryPost.Content.TextContent textContent) {
            Intrinsics.checkNotNullParameter(textContent, "content");
            StoryTextPostPreviewFragment storyTextPostPreviewFragment = new StoryTextPostPreviewFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(MediaPreviewFragment.DATA_URI, textContent.getUri());
            bundle.putLong(StoryTextPostPreviewFragment.STORY_ID, textContent.getRecordId());
            storyTextPostPreviewFragment.setArguments(bundle);
            return storyTextPostPreviewFragment;
        }
    }

    private final void showLinkPreviewTooltip(View view, LinkPreview linkPreview) {
        Callback callback;
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        callback = (Callback) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment.Callback");
                    }
                } else if (fragment instanceof Callback) {
                    callback = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            callback.setIsDisplayingLinkPreviewTooltip(true);
            View inflate = LayoutInflater.from(requireContext()).inflate(R.layout.stories_link_popup, (ViewGroup) null, false);
            ((TextView) inflate.findViewById(R.id.url)).setText(linkPreview.getUrl());
            inflate.setOnClickListener(new View.OnClickListener(linkPreview) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment$$ExternalSyntheticLambda2
                public final /* synthetic */ LinkPreview f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    StoryTextPostPreviewFragment.m3193showLinkPreviewTooltip$lambda2(StoryTextPostPreviewFragment.this, this.f$1, view2);
                }
            });
            inflate.measure(View.MeasureSpec.makeMeasureSpec((int) DimensionUnit.DP.toPixels(275.0f), 1073741824), 0);
            inflate.layout(0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
            FragmentDialogs fragmentDialogs = FragmentDialogs.INSTANCE;
            Intrinsics.checkNotNullExpressionValue(inflate, "contentView");
            FragmentDialogs.displayInDialogAboveAnchor$default(fragmentDialogs, this, view, inflate, 0.0f, (Function2) null, 8, (Object) null);
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    private final StoryTextPostViewModel getViewModel() {
        return (StoryTextPostViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.story_text_post);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.story_text_post)");
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ StoryTextPostPreviewFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StoryTextPostPreviewFragment.m3191onViewCreated$lambda1(StoryTextPostView.this, this.f$1, (StoryTextPostState) obj);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m3191onViewCreated$lambda1(StoryTextPostView storyTextPostView, StoryTextPostPreviewFragment storyTextPostPreviewFragment, StoryTextPostState storyTextPostState) {
        MediaPreviewFragment.Events events;
        MediaPreviewFragment.Events events2;
        Intrinsics.checkNotNullParameter(storyTextPostView, "$storyTextPostView");
        Intrinsics.checkNotNullParameter(storyTextPostPreviewFragment, "this$0");
        int i = WhenMappings.$EnumSwitchMapping$0[storyTextPostState.getLoadState().ordinal()];
        if (i == 2) {
            StoryTextPost storyTextPost = storyTextPostState.getStoryTextPost();
            Intrinsics.checkNotNull(storyTextPost);
            storyTextPostView.bindFromStoryTextPost(storyTextPost);
            storyTextPostView.bindLinkPreview(storyTextPostState.getLinkPreview());
            if (storyTextPostState.getLinkPreview() != null) {
                storyTextPostView.setLinkPreviewClickListener(new View.OnClickListener(storyTextPostState) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment$$ExternalSyntheticLambda1
                    public final /* synthetic */ StoryTextPostState f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        StoryTextPostPreviewFragment.m3192onViewCreated$lambda1$lambda0(StoryTextPostPreviewFragment.this, this.f$1, view);
                    }
                });
            } else {
                storyTextPostView.setLinkPreviewClickListener(null);
            }
        } else if (i == 3) {
            ArrayList arrayList = new ArrayList();
            try {
                Fragment fragment = storyTextPostPreviewFragment.getParentFragment();
                while (true) {
                    if (fragment == null) {
                        FragmentActivity requireActivity = storyTextPostPreviewFragment.requireActivity();
                        if (requireActivity != null) {
                            events2 = (MediaPreviewFragment.Events) requireActivity;
                        } else {
                            throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment.Events");
                        }
                    } else if (fragment instanceof MediaPreviewFragment.Events) {
                        events2 = fragment;
                        break;
                    } else {
                        String name = fragment.getClass().getName();
                        Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                        arrayList.add(name);
                        fragment = fragment.getParentFragment();
                    }
                }
                events2.mediaNotAvailable();
            } catch (ClassCastException e) {
                String name2 = storyTextPostPreviewFragment.requireActivity().getClass().getName();
                Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
                arrayList.add(name2);
                throw new ListenerNotFoundException(arrayList, e);
            }
        }
        if (storyTextPostState.getTypeface() != null) {
            storyTextPostView.setTypeface(storyTextPostState.getTypeface());
        }
        if (storyTextPostState.getTypeface() != null && storyTextPostState.getLoadState() == StoryTextPostState.LoadState.LOADED) {
            ArrayList arrayList2 = new ArrayList();
            try {
                Fragment fragment2 = storyTextPostPreviewFragment.getParentFragment();
                while (true) {
                    if (fragment2 == null) {
                        FragmentActivity requireActivity2 = storyTextPostPreviewFragment.requireActivity();
                        if (requireActivity2 != null) {
                            events = (MediaPreviewFragment.Events) requireActivity2;
                        } else {
                            throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment.Events");
                        }
                    } else if (fragment2 instanceof MediaPreviewFragment.Events) {
                        events = fragment2;
                        break;
                    } else {
                        String name3 = fragment2.getClass().getName();
                        Intrinsics.checkNotNullExpressionValue(name3, "parent::class.java.name");
                        arrayList2.add(name3);
                        fragment2 = fragment2.getParentFragment();
                    }
                }
                events.onMediaReady();
            } catch (ClassCastException e2) {
                String name4 = storyTextPostPreviewFragment.requireActivity().getClass().getName();
                Intrinsics.checkNotNullExpressionValue(name4, "requireActivity()::class.java.name");
                arrayList2.add(name4);
                throw new ListenerNotFoundException(arrayList2, e2);
            }
        }
    }

    /* renamed from: onViewCreated$lambda-1$lambda-0 */
    public static final void m3192onViewCreated$lambda1$lambda0(StoryTextPostPreviewFragment storyTextPostPreviewFragment, StoryTextPostState storyTextPostState, View view) {
        Intrinsics.checkNotNullParameter(storyTextPostPreviewFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        storyTextPostPreviewFragment.showLinkPreviewTooltip(view, storyTextPostState.getLinkPreview());
    }

    /* renamed from: showLinkPreviewTooltip$lambda-2 */
    public static final void m3193showLinkPreviewTooltip$lambda2(StoryTextPostPreviewFragment storyTextPostPreviewFragment, LinkPreview linkPreview, View view) {
        Intrinsics.checkNotNullParameter(storyTextPostPreviewFragment, "this$0");
        Intrinsics.checkNotNullParameter(linkPreview, "$linkPreview");
        CommunicationActions.openBrowserLink(storyTextPostPreviewFragment.requireContext(), linkPreview.getUrl());
    }
}
