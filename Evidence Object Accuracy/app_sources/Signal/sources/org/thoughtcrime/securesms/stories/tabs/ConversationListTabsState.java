package org.thoughtcrime.securesms.stories.tabs;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;

/* compiled from: ConversationListTabsState.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u001dB-\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0014\u001a\u00020\bHÆ\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsState;", "", "tab", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTab;", "unreadChatsCount", "", "unreadStoriesCount", "visibilityState", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsState$VisibilityState;", "(Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTab;JJLorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsState$VisibilityState;)V", "getTab", "()Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTab;", "getUnreadChatsCount", "()J", "getUnreadStoriesCount", "getVisibilityState", "()Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsState$VisibilityState;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "", "VisibilityState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ConversationListTabsState {
    private final ConversationListTab tab;
    private final long unreadChatsCount;
    private final long unreadStoriesCount;
    private final VisibilityState visibilityState;

    public ConversationListTabsState() {
        this(null, 0, 0, null, 15, null);
    }

    public static /* synthetic */ ConversationListTabsState copy$default(ConversationListTabsState conversationListTabsState, ConversationListTab conversationListTab, long j, long j2, VisibilityState visibilityState, int i, Object obj) {
        if ((i & 1) != 0) {
            conversationListTab = conversationListTabsState.tab;
        }
        if ((i & 2) != 0) {
            j = conversationListTabsState.unreadChatsCount;
        }
        if ((i & 4) != 0) {
            j2 = conversationListTabsState.unreadStoriesCount;
        }
        if ((i & 8) != 0) {
            visibilityState = conversationListTabsState.visibilityState;
        }
        return conversationListTabsState.copy(conversationListTab, j, j2, visibilityState);
    }

    public final ConversationListTab component1() {
        return this.tab;
    }

    public final long component2() {
        return this.unreadChatsCount;
    }

    public final long component3() {
        return this.unreadStoriesCount;
    }

    public final VisibilityState component4() {
        return this.visibilityState;
    }

    public final ConversationListTabsState copy(ConversationListTab conversationListTab, long j, long j2, VisibilityState visibilityState) {
        Intrinsics.checkNotNullParameter(conversationListTab, "tab");
        Intrinsics.checkNotNullParameter(visibilityState, "visibilityState");
        return new ConversationListTabsState(conversationListTab, j, j2, visibilityState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConversationListTabsState)) {
            return false;
        }
        ConversationListTabsState conversationListTabsState = (ConversationListTabsState) obj;
        return this.tab == conversationListTabsState.tab && this.unreadChatsCount == conversationListTabsState.unreadChatsCount && this.unreadStoriesCount == conversationListTabsState.unreadStoriesCount && Intrinsics.areEqual(this.visibilityState, conversationListTabsState.visibilityState);
    }

    public int hashCode() {
        return (((((this.tab.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.unreadChatsCount)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.unreadStoriesCount)) * 31) + this.visibilityState.hashCode();
    }

    public String toString() {
        return "ConversationListTabsState(tab=" + this.tab + ", unreadChatsCount=" + this.unreadChatsCount + ", unreadStoriesCount=" + this.unreadStoriesCount + ", visibilityState=" + this.visibilityState + ')';
    }

    public ConversationListTabsState(ConversationListTab conversationListTab, long j, long j2, VisibilityState visibilityState) {
        Intrinsics.checkNotNullParameter(conversationListTab, "tab");
        Intrinsics.checkNotNullParameter(visibilityState, "visibilityState");
        this.tab = conversationListTab;
        this.unreadChatsCount = j;
        this.unreadStoriesCount = j2;
        this.visibilityState = visibilityState;
    }

    public /* synthetic */ ConversationListTabsState(ConversationListTab conversationListTab, long j, long j2, VisibilityState visibilityState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? ConversationListTab.CHATS : conversationListTab, (i & 2) != 0 ? 0 : j, (i & 4) == 0 ? j2 : 0, (i & 8) != 0 ? new VisibilityState(false, false, false, 7, null) : visibilityState);
    }

    public final ConversationListTab getTab() {
        return this.tab;
    }

    public final long getUnreadChatsCount() {
        return this.unreadChatsCount;
    }

    public final long getUnreadStoriesCount() {
        return this.unreadStoriesCount;
    }

    public final VisibilityState getVisibilityState() {
        return this.visibilityState;
    }

    /* compiled from: ConversationListTabsState.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B#\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\t\u0010\b\u001a\u00020\u0003HÆ\u0003J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J'\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\u00032\b\u0010\r\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\u0006\u0010\u0010\u001a\u00020\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0007R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0007¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsState$VisibilityState;", "", "isSearchOpen", "", "isMultiSelectOpen", "isShowingArchived", "(ZZZ)V", "()Z", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "isVisible", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class VisibilityState {
        private final boolean isMultiSelectOpen;
        private final boolean isSearchOpen;
        private final boolean isShowingArchived;

        public VisibilityState() {
            this(false, false, false, 7, null);
        }

        public static /* synthetic */ VisibilityState copy$default(VisibilityState visibilityState, boolean z, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                z = visibilityState.isSearchOpen;
            }
            if ((i & 2) != 0) {
                z2 = visibilityState.isMultiSelectOpen;
            }
            if ((i & 4) != 0) {
                z3 = visibilityState.isShowingArchived;
            }
            return visibilityState.copy(z, z2, z3);
        }

        public final boolean component1() {
            return this.isSearchOpen;
        }

        public final boolean component2() {
            return this.isMultiSelectOpen;
        }

        public final boolean component3() {
            return this.isShowingArchived;
        }

        public final VisibilityState copy(boolean z, boolean z2, boolean z3) {
            return new VisibilityState(z, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VisibilityState)) {
                return false;
            }
            VisibilityState visibilityState = (VisibilityState) obj;
            return this.isSearchOpen == visibilityState.isSearchOpen && this.isMultiSelectOpen == visibilityState.isMultiSelectOpen && this.isShowingArchived == visibilityState.isShowingArchived;
        }

        public int hashCode() {
            boolean z = this.isSearchOpen;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = i2 * 31;
            boolean z2 = this.isMultiSelectOpen;
            if (z2) {
                z2 = true;
            }
            int i6 = z2 ? 1 : 0;
            int i7 = z2 ? 1 : 0;
            int i8 = z2 ? 1 : 0;
            int i9 = (i5 + i6) * 31;
            boolean z3 = this.isShowingArchived;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            return i9 + i;
        }

        public String toString() {
            return "VisibilityState(isSearchOpen=" + this.isSearchOpen + ", isMultiSelectOpen=" + this.isMultiSelectOpen + ", isShowingArchived=" + this.isShowingArchived + ')';
        }

        public VisibilityState(boolean z, boolean z2, boolean z3) {
            this.isSearchOpen = z;
            this.isMultiSelectOpen = z2;
            this.isShowingArchived = z3;
        }

        public /* synthetic */ VisibilityState(boolean z, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? false : z3);
        }

        public final boolean isSearchOpen() {
            return this.isSearchOpen;
        }

        public final boolean isMultiSelectOpen() {
            return this.isMultiSelectOpen;
        }

        public final boolean isShowingArchived() {
            return this.isShowingArchived;
        }

        public final boolean isVisible() {
            return !this.isSearchOpen && !this.isMultiSelectOpen && !this.isShowingArchived;
        }
    }
}
