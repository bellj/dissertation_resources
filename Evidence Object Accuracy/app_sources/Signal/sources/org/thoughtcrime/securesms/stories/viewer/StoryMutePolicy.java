package org.thoughtcrime.securesms.stories.viewer;

import kotlin.Metadata;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.AppForegroundObserver;

/* compiled from: StoryMutePolicy.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\b\u001a\u00020\tJ\b\u0010\n\u001a\u00020\tH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0003\u0010\u0005\"\u0004\b\u0006\u0010\u0007¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryMutePolicy;", "Lorg/thoughtcrime/securesms/util/AppForegroundObserver$Listener;", "()V", "isContentMuted", "", "()Z", "setContentMuted", "(Z)V", "initialize", "", "onBackground", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryMutePolicy implements AppForegroundObserver.Listener {
    public static final StoryMutePolicy INSTANCE = new StoryMutePolicy();
    private static boolean isContentMuted = true;

    @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
    public /* synthetic */ void onForeground() {
        AppForegroundObserver.Listener.CC.$default$onForeground(this);
    }

    private StoryMutePolicy() {
    }

    public final boolean isContentMuted() {
        return isContentMuted;
    }

    public final void setContentMuted(boolean z) {
        isContentMuted = z;
    }

    public final void initialize() {
        ApplicationDependencies.getAppForegroundObserver().addListener(this);
    }

    @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
    public void onBackground() {
        isContentMuted = true;
    }
}
