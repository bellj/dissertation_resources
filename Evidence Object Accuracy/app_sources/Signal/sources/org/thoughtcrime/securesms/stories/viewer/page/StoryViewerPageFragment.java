package org.thoughtcrime.securesms.stories.viewer.page;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.RenderEffect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.os.BundleKt;
import androidx.core.view.GestureDetectorCompat;
import androidx.core.view.animation.PathInterpolatorCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentResultListener;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import com.google.android.material.button.MaterialButton;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.segmentedprogressbar.SegmentedProgressBar;
import org.thoughtcrime.securesms.components.segmentedprogressbar.SegmentedProgressBarListener;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.FallbackPhoto20dp;
import org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment;
import org.thoughtcrime.securesms.mediapreview.VideoControlsDelegate;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.StoryFirstTimeNavigationView;
import org.thoughtcrime.securesms.stories.StorySlateView;
import org.thoughtcrime.securesms.stories.StoryVolumeOverlayView;
import org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerState;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel;
import org.thoughtcrime.securesms.stories.viewer.StoryVolumeState;
import org.thoughtcrime.securesms.stories.viewer.StoryVolumeViewModel;
import org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment;
import org.thoughtcrime.securesms.stories.viewer.page.StoryDisplay;
import org.thoughtcrime.securesms.stories.viewer.page.StoryPost;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerDialog;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel;
import org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyBottomSheetDialogFragment;
import org.thoughtcrime.securesms.stories.viewer.reply.reaction.OnReactionSentView;
import org.thoughtcrime.securesms.stories.viewer.reply.tabs.StoryViewsAndRepliesDialogFragment;
import org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment;
import org.thoughtcrime.securesms.stories.viewer.views.StoryViewsBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.AvatarUtil;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.Debouncer;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;
import org.thoughtcrime.securesms.util.views.TouchInterceptingFrameLayout;

/* compiled from: StoryViewerPageFragment.kt */
@Metadata(d1 = {"\u0000\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000f\u0018\u0000 \u00012\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u00062\u00020\u0007:\n\u0001\u0001\u0001 \u0001¡\u0001B\u0005¢\u0006\u0002\u0010\bJ \u0010G\u001a\u00020H2\u0006\u0010@\u001a\u00020\u000f2\u0006\u0010I\u001a\u00020\u000f2\u0006\u0010J\u001a\u00020KH\u0002J\u0010\u0010L\u001a\u00020H2\u0006\u0010M\u001a\u00020NH\u0002J\u0010\u0010O\u001a\u00020\u00152\u0006\u0010P\u001a\u00020QH\u0002J\u0010\u0010R\u001a\u00020\u00012\u0006\u0010S\u001a\u00020TH\u0002J\u0010\u0010U\u001a\u00020\u00012\u0006\u0010V\u001a\u00020WH\u0002J\u0010\u0010X\u001a\u00020H2\u0006\u0010Y\u001a\u00020\u000fH\u0002J\u0010\u0010Z\u001a\u00020H2\u0006\u0010[\u001a\u00020\u000fH\u0002J\b\u0010\\\u001a\u00020:H\u0016J\u0010\u0010]\u001a\u00020\u00152\u0006\u0010^\u001a\u00020_H\u0002J\u0010\u0010`\u001a\u00020N2\u0006\u0010^\u001a\u00020_H\u0002J\b\u0010a\u001a\u00020bH\u0002J\b\u0010c\u001a\u00020HH\u0002J\b\u0010d\u001a\u00020HH\u0002J\b\u0010e\u001a\u00020HH\u0002J\b\u0010f\u001a\u00020HH\u0016J\b\u0010g\u001a\u00020HH\u0016J\b\u0010h\u001a\u00020HH\u0016J\b\u0010i\u001a\u00020HH\u0016J\b\u0010j\u001a\u00020HH\u0016J \u0010k\u001a\u00020H2\u0006\u0010l\u001a\u00020m2\u0006\u0010n\u001a\u00020m2\u0006\u0010o\u001a\u00020\u000fH\u0002J\b\u0010p\u001a\u00020HH\u0016J\b\u0010q\u001a\u00020HH\u0016J\b\u0010r\u001a\u00020HH\u0016J\b\u0010s\u001a\u00020HH\u0016J \u0010t\u001a\u00020H2\u0006\u0010l\u001a\u00020m2\u0006\u0010n\u001a\u00020m2\u0006\u0010o\u001a\u00020\u000fH\u0002J\u0018\u0010u\u001a\u00020H2\u0006\u0010v\u001a\u00020\u00152\u0006\u0010w\u001a\u00020.H\u0002J\u0018\u0010x\u001a\u00020H2\u0006\u0010y\u001a\u00020z2\u0006\u0010{\u001a\u00020\u0015H\u0016J\u001a\u0010|\u001a\u00020H2\u0006\u0010[\u001a\u00020\u000f2\b\u0010}\u001a\u0004\u0018\u00010~H\u0017J\b\u0010\u001a\u00020HH\u0002J\u0011\u0010\u0001\u001a\u00020H2\u0006\u0010V\u001a\u00020WH\u0002J)\u0010\u0001\u001a\u00020H2\u0006\u0010l\u001a\u00020m2\u0006\u0010n\u001a\u00020m2\u0006\u0010o\u001a\u00020\u000f2\u0006\u0010V\u001a\u00020WH\u0003J\u001a\u0010\u0001\u001a\u00020H2\u0007\u0010\u0001\u001a\u00020m2\u0006\u0010V\u001a\u00020WH\u0002J\u001a\u0010\u0001\u001a\u00020H2\u0007\u0010\u0001\u001a\u00020m2\u0006\u0010V\u001a\u00020WH\u0002J\u001a\u0010\u0001\u001a\u00020H2\u0007\u0010\u0001\u001a\u00020m2\u0006\u0010V\u001a\u00020WH\u0002J\u001c\u0010\u0001\u001a\u00020H2\b\u0010\u0001\u001a\u00030\u00012\u0007\u0010\u0001\u001a\u00020WH\u0002J\u001c\u0010\u0001\u001a\u00020H2\b\u0010\u0001\u001a\u00030\u00012\u0007\u0010\u0001\u001a\u00020WH\u0002J\u0012\u0010\u0001\u001a\u00020H2\u0007\u0010\u0001\u001a\u00020WH\u0002J\u001b\u0010\u0001\u001a\u00020H2\u0007\u0010\u0001\u001a\u00020W2\u0007\u0010\u0001\u001a\u00020\u0011H\u0002J%\u0010\u0001\u001a\u00020H2\u0007\u0010\u0001\u001a\u00020W2\b\u0010\u0001\u001a\u00030\u00012\u0007\u0010\u0001\u001a\u00020\u0019H\u0002J\t\u0010\u0001\u001a\u00020HH\u0002J\u0012\u0010\u0001\u001a\u00020H2\u0007\u0010\u0001\u001a\u00020\u0019H\u0016J\t\u0010\u0001\u001a\u00020HH\u0002J\u0011\u0010\u0001\u001a\u00020H2\u0006\u0010V\u001a\u00020WH\u0002J\t\u0010\u0001\u001a\u00020\u0019H\u0016J\u001d\u0010\u0001\u001a\u00020H2\b\b\u0002\u0010\u001b\u001a\u00020\u00192\b\b\u0002\u0010\u0010\u001a\u00020\u0011H\u0002J\t\u0010\u0001\u001a\u00020\u0019H\u0016R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eX.¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00020\u00118BX\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\u00158BX\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u00198BX\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u00198BX\u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001aR\u0014\u0010\u001c\u001a\u00020\u00198BX\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u001aR\u0014\u0010\u001d\u001a\u00020\u00198BX\u0004¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u001aR\u000e\u0010\u001e\u001a\u00020\u001fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X.¢\u0006\u0002\n\u0000R\u001b\u0010\"\u001a\u00020#8BX\u0002¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b$\u0010%R\u000e\u0010(\u001a\u00020)X.¢\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020)X.¢\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020,X.¢\u0006\u0002\n\u0000R\u0014\u0010-\u001a\u00020.8BX\u0004¢\u0006\u0006\u001a\u0004\b/\u00100R\u000e\u00101\u001a\u000202X.¢\u0006\u0002\n\u0000R\u001b\u00103\u001a\u0002048BX\u0002¢\u0006\f\n\u0004\b7\u0010'\u001a\u0004\b5\u00106R\u000e\u00108\u001a\u00020\u001fX\u0004¢\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020:X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010;\u001a\u00020<8BX\u0002¢\u0006\f\n\u0004\b?\u0010'\u001a\u0004\b=\u0010>R\u000e\u0010@\u001a\u00020AX.¢\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020CX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010D\u001a\u0004\u0018\u00010EX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010F\u001a\u0004\u0018\u00010EX\u000e¢\u0006\u0002\n\u0000¨\u0006¢\u0001"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/mediapreview/MediaPreviewFragment$Events;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardBottomSheet$Callback;", "Lorg/thoughtcrime/securesms/stories/StorySlateView$Callback;", "Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostPreviewFragment$Callback;", "Lorg/thoughtcrime/securesms/stories/StoryFirstTimeNavigationView$Callback;", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoBottomSheetDialogFragment$OnInfoSheetDismissedListener;", "()V", "animatorSet", "Landroid/animation/AnimatorSet;", "callback", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageFragment$Callback;", "chrome", "", "Landroid/view/View;", "groupReplyStartPosition", "", "getGroupReplyStartPosition", "()I", "initialStoryId", "", "getInitialStoryId", "()J", "isFromInfoContextMenuAction", "", "()Z", "isFromNotification", "isOutgoingOnly", "isUnviewedOnly", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "progressBar", "Lorg/thoughtcrime/securesms/components/segmentedprogressbar/SegmentedProgressBar;", "sharedViewModel", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerViewModel;", "getSharedViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerViewModel;", "sharedViewModel$delegate", "Lkotlin/Lazy;", "storyCaptionContainer", "Landroid/widget/FrameLayout;", "storyContentContainer", "storyFirstTimeNavigationViewStub", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryFirstNavigationStub;", "storyRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getStoryRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "storySlate", "Lorg/thoughtcrime/securesms/stories/StorySlateView;", "storyVolumeViewModel", "Lorg/thoughtcrime/securesms/stories/viewer/StoryVolumeViewModel;", "getStoryVolumeViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/StoryVolumeViewModel;", "storyVolumeViewModel$delegate", "timeoutDisposable", "videoControlsDelegate", "Lorg/thoughtcrime/securesms/mediapreview/VideoControlsDelegate;", "viewModel", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "viewModel$delegate", "viewsAndReplies", "Lcom/google/android/material/button/MaterialButton;", "volumeDebouncer", "Lorg/thoughtcrime/securesms/util/Debouncer;", "volumeInAnimator", "Landroid/animation/Animator;", "volumeOutAnimator", "adjustConstraintsForScreenDimensions", "", "cardWrapper", "card", "Landroidx/cardview/widget/CardView;", "animateChrome", "alphaTarget", "", "calculateDurationForText", "textContent", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content$TextContent;", "createFragmentForAttachmentContent", "attachmentContent", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content$AttachmentContent;", "createFragmentForPost", "storyPost", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost;", "displayMoreContextMenu", "anchor", "displayStoryVolumeOverlayForTimeout", "view", "getVideoControlsDelegate", "getVideoPlaybackDuration", "playerState", "Lorg/thoughtcrime/securesms/mediapreview/VideoControlsDelegate$PlayerState;", "getVideoPlaybackPosition", "getViewsAndRepliesDialogStartPage", "Lorg/thoughtcrime/securesms/stories/viewer/reply/tabs/StoryViewsAndRepliesDialogFragment$StartPage;", "hideChrome", "hideChromeImmediate", "markViewedIfAble", "mediaNotAvailable", "onDestroyView", "onDismissForwardSheet", "onFinishForwardAction", "onGotItClicked", "onHideCaptionOverlay", MediaPreviewActivity.CAPTION_EXTRA, "Landroid/widget/TextView;", "largeCaption", "largeCaptionOverlay", "onInfoSheetDismissed", "onMediaReady", "onPause", "onResume", "onShowCaptionOverlay", "onStartDirectReply", "storyId", "recipientId", "onStateChanged", "state", "Lorg/thoughtcrime/securesms/stories/StorySlateView$State;", "postId", "onViewCreated", "savedInstanceState", "Landroid/os/Bundle;", "pauseProgress", "presentBlur", "presentCaption", "presentDate", "date", "presentDistributionList", "distributionList", "presentFrom", "from", "presentGroupAvatar", "groupAvatar", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "post", "presentSenderAvatar", "senderAvatar", "presentSlate", "presentStory", "index", "presentViewsAndReplies", "replyState", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState$ReplyState;", "isReceiptsEnabled", "resumeProgress", "setIsDisplayingLinkPreviewTooltip", "isDisplayingLinkPreviewTooltip", "showChrome", "showInfo", "singleTapOnMedia", "startReply", "userHasSeenFirstNavigationView", "Callback", "Companion", "FallbackPhotoProvider", "FixedSizeGeneratedContactPhoto", "StoryGestureListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerPageFragment extends Fragment implements MediaPreviewFragment.Events, MultiselectForwardBottomSheet.Callback, StorySlateView.Callback, StoryTextPostPreviewFragment.Callback, StoryFirstTimeNavigationView.Callback, StoryInfoBottomSheetDialogFragment.OnInfoSheetDismissedListener {
    private static final String ARG_GROUP_REPLY_START_POSITION;
    private static final String ARG_IS_FROM_INFO_CONTEXT_MENU_ACTION;
    private static final String ARG_IS_FROM_NOTIFICATION;
    private static final String ARG_IS_OUTGOING_ONLY;
    private static final String ARG_IS_UNVIEWED_ONLY;
    private static final String ARG_STORY_ID;
    private static final String ARG_STORY_RECIPIENT_ID;
    private static final long CHARACTERS_PER_SECOND = 15;
    public static final Companion Companion = new Companion(null);
    private static final long DEFAULT_DURATION;
    private static final long MAX_VIDEO_PLAYBACK_DURATION;
    private static final long MIN_GIF_LOOPS = 3;
    private static final long MIN_GIF_PLAYBACK_DURATION;
    private static final long MIN_TEXT_STORY_PLAYBACK;
    private static final String TAG = Log.tag(StoryViewerPageFragment.class);
    private AnimatorSet animatorSet;
    private Callback callback;
    private List<? extends View> chrome;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private SegmentedProgressBar progressBar;
    private final Lazy sharedViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryViewerViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$sharedViewModel$2
        final /* synthetic */ StoryViewerPageFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            Fragment requireParentFragment = this.this$0.requireParentFragment();
            Intrinsics.checkNotNullExpressionValue(requireParentFragment, "requireParentFragment()");
            return requireParentFragment;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$special$$inlined$viewModels$default$4
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private FrameLayout storyCaptionContainer;
    private FrameLayout storyContentContainer;
    private StoryFirstNavigationStub storyFirstTimeNavigationViewStub;
    private StorySlateView storySlate;
    private final Lazy storyVolumeViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryVolumeViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$storyVolumeViewModel$2
        final /* synthetic */ StoryViewerPageFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private final LifecycleDisposable timeoutDisposable = new LifecycleDisposable();
    private final VideoControlsDelegate videoControlsDelegate = new VideoControlsDelegate();
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryViewerPageViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$special$$inlined$viewModels$default$3
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$viewModel$2
        final /* synthetic */ StoryViewerPageFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            RecipientId recipientId = this.this$0.getStoryRecipientId();
            long j = this.this$0.getInitialStoryId();
            boolean z = this.this$0.isUnviewedOnly();
            boolean z2 = this.this$0.isOutgoingOnly();
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            StoryViewerPageRepository storyViewerPageRepository = new StoryViewerPageRepository(requireContext);
            GlideRequests with = GlideApp.with(this.this$0.requireActivity());
            Intrinsics.checkNotNullExpressionValue(with, "with(requireActivity())");
            StoryDisplay.Companion companion = StoryDisplay.Companion;
            Resources resources = this.this$0.getResources();
            Intrinsics.checkNotNullExpressionValue(resources, "resources");
            return new StoryViewerPageViewModel.Factory(recipientId, j, z, z2, storyViewerPageRepository, new StoryCache(with, companion.getStorySize(resources)));
        }
    });
    private MaterialButton viewsAndReplies;
    private Debouncer volumeDebouncer = new Debouncer(3, TimeUnit.SECONDS);
    private Animator volumeInAnimator;
    private Animator volumeOutAnimator;

    /* compiled from: StoryViewerPageFragment.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageFragment$Callback;", "", "onFinishedPosts", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "onGoToPreviousStory", "onStoryHidden", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onFinishedPosts(RecipientId recipientId);

        void onGoToPreviousStory(RecipientId recipientId);

        void onStoryHidden(RecipientId recipientId);
    }

    /* compiled from: StoryViewerPageFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            int[] iArr = new int[StoryDisplay.values().length];
            iArr[StoryDisplay.LARGE.ordinal()] = 1;
            iArr[StoryDisplay.MEDIUM.ordinal()] = 2;
            iArr[StoryDisplay.SMALL.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
            int[] iArr2 = new int[StoryViewerPageState.ReplyState.values().length];
            iArr2[StoryViewerPageState.ReplyState.NONE.ordinal()] = 1;
            iArr2[StoryViewerPageState.ReplyState.SELF.ordinal()] = 2;
            iArr2[StoryViewerPageState.ReplyState.GROUP.ordinal()] = 3;
            iArr2[StoryViewerPageState.ReplyState.PRIVATE.ordinal()] = 4;
            iArr2[StoryViewerPageState.ReplyState.GROUP_SELF.ordinal()] = 5;
            $EnumSwitchMapping$1 = iArr2;
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback
    public void onFinishForwardAction() {
    }

    @Override // org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment.Events
    public boolean singleTapOnMedia() {
        return false;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Fragment fragment;
        Intrinsics.checkNotNullParameter(view, "view");
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment2 = getParentFragment();
            while (true) {
                if (fragment2 == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        fragment = (Callback) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment.Callback");
                    }
                } else if (fragment2 instanceof Callback) {
                    fragment = fragment2;
                    break;
                } else {
                    String name = fragment2.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment2 = fragment2.getParentFragment();
                }
            }
            this.callback = fragment;
            if (getStoryVolumeViewModel().getSnapshot().isMuted()) {
                this.videoControlsDelegate.mute();
            } else {
                this.videoControlsDelegate.unmute();
            }
            View findViewById = view.findViewById(R.id.close);
            Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.close)");
            View findViewById2 = view.findViewById(R.id.sender_avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.sender_avatar)");
            AvatarImageView avatarImageView = (AvatarImageView) findViewById2;
            View findViewById3 = view.findViewById(R.id.group_avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.group_avatar)");
            AvatarImageView avatarImageView2 = (AvatarImageView) findViewById3;
            View findViewById4 = view.findViewById(R.id.from);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.from)");
            TextView textView = (TextView) findViewById4;
            View findViewById5 = view.findViewById(R.id.date);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.date)");
            TextView textView2 = (TextView) findViewById5;
            View findViewById6 = view.findViewById(R.id.more);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.more)");
            View findViewById7 = view.findViewById(R.id.distribution_list);
            Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.distribution_list)");
            TextView textView3 = (TextView) findViewById7;
            View findViewById8 = view.findViewById(R.id.story_content_card_touch_interceptor);
            Intrinsics.checkNotNullExpressionValue(findViewById8, "view.findViewById(R.id.s…t_card_touch_interceptor)");
            TouchInterceptingFrameLayout touchInterceptingFrameLayout = (TouchInterceptingFrameLayout) findViewById8;
            View findViewById9 = view.findViewById(R.id.story_content_card);
            Intrinsics.checkNotNullExpressionValue(findViewById9, "view.findViewById(R.id.story_content_card)");
            CardView cardView = (CardView) findViewById9;
            View findViewById10 = view.findViewById(R.id.story_caption);
            Intrinsics.checkNotNullExpressionValue(findViewById10, "view.findViewById(R.id.story_caption)");
            TextView textView4 = (TextView) findViewById10;
            View findViewById11 = view.findViewById(R.id.story_large_caption);
            Intrinsics.checkNotNullExpressionValue(findViewById11, "view.findViewById(R.id.story_large_caption)");
            TextView textView5 = (TextView) findViewById11;
            View findViewById12 = view.findViewById(R.id.story_large_caption_overlay);
            Intrinsics.checkNotNullExpressionValue(findViewById12, "view.findViewById(R.id.s…ry_large_caption_overlay)");
            View findViewById13 = view.findViewById(R.id.on_reaction_sent_view);
            Intrinsics.checkNotNullExpressionValue(findViewById13, "view.findViewById(R.id.on_reaction_sent_view)");
            OnReactionSentView onReactionSentView = (OnReactionSentView) findViewById13;
            View findViewById14 = view.findViewById(R.id.story_gradient_top);
            Intrinsics.checkNotNullExpressionValue(findViewById14, "view.findViewById(R.id.story_gradient_top)");
            View findViewById15 = view.findViewById(R.id.story_gradient_bottom);
            Intrinsics.checkNotNullExpressionValue(findViewById15, "view.findViewById(R.id.story_gradient_bottom)");
            View findViewById16 = view.findViewById(R.id.story_volume_overlay);
            Intrinsics.checkNotNullExpressionValue(findViewById16, "view.findViewById(R.id.story_volume_overlay)");
            StoryVolumeOverlayView storyVolumeOverlayView = (StoryVolumeOverlayView) findViewById16;
            View findViewById17 = view.findViewById(R.id.story_content_container);
            Intrinsics.checkNotNullExpressionValue(findViewById17, "view.findViewById(R.id.story_content_container)");
            this.storyContentContainer = (FrameLayout) findViewById17;
            View findViewById18 = view.findViewById(R.id.story_caption_container);
            Intrinsics.checkNotNullExpressionValue(findViewById18, "view.findViewById(R.id.story_caption_container)");
            this.storyCaptionContainer = (FrameLayout) findViewById18;
            View findViewById19 = view.findViewById(R.id.story_slate);
            Intrinsics.checkNotNullExpressionValue(findViewById19, "view.findViewById(R.id.story_slate)");
            this.storySlate = (StorySlateView) findViewById19;
            View findViewById20 = view.findViewById(R.id.progress);
            Intrinsics.checkNotNullExpressionValue(findViewById20, "view.findViewById(R.id.progress)");
            this.progressBar = (SegmentedProgressBar) findViewById20;
            View findViewById21 = view.findViewById(R.id.views_and_replies_bar);
            Intrinsics.checkNotNullExpressionValue(findViewById21, "view.findViewById(R.id.views_and_replies_bar)");
            this.viewsAndReplies = (MaterialButton) findViewById21;
            View findViewById22 = view.findViewById(R.id.story_first_time_nav_stub);
            Intrinsics.checkNotNullExpressionValue(findViewById22, "view.findViewById(R.id.story_first_time_nav_stub)");
            this.storyFirstTimeNavigationViewStub = new StoryFirstNavigationStub((ViewStub) findViewById22);
            StorySlateView storySlateView = this.storySlate;
            if (storySlateView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storySlate");
                storySlateView = null;
            }
            storySlateView.setCallback(this);
            StoryFirstNavigationStub storyFirstNavigationStub = this.storyFirstTimeNavigationViewStub;
            if (storyFirstNavigationStub == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyFirstTimeNavigationViewStub");
                storyFirstNavigationStub = null;
            }
            storyFirstNavigationStub.setCallback(this);
            View[] viewArr = new View[11];
            viewArr[0] = findViewById;
            viewArr[1] = avatarImageView;
            viewArr[2] = avatarImageView2;
            viewArr[3] = textView;
            viewArr[4] = textView2;
            viewArr[5] = findViewById6;
            viewArr[6] = textView3;
            MaterialButton materialButton = this.viewsAndReplies;
            if (materialButton == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton = null;
            }
            viewArr[7] = materialButton;
            SegmentedProgressBar segmentedProgressBar = this.progressBar;
            if (segmentedProgressBar == null) {
                Intrinsics.throwUninitializedPropertyAccessException("progressBar");
                segmentedProgressBar = null;
            }
            viewArr[8] = segmentedProgressBar;
            viewArr[9] = findViewById14;
            viewArr[10] = findViewById15;
            this.chrome = CollectionsKt__CollectionsKt.listOf((Object[]) viewArr);
            avatarImageView.setFallbackPhotoProvider(new FallbackPhotoProvider());
            avatarImageView2.setFallbackPhotoProvider(new FallbackPhotoProvider());
            findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    StoryViewerPageFragment.m3039onViewCreated$lambda0(StoryViewerPageFragment.this, view2);
                }
            });
            GestureDetectorCompat gestureDetectorCompat = new GestureDetectorCompat(requireContext(), new StoryGestureListener(touchInterceptingFrameLayout, new Function0<Unit>(getViewModel()) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$onViewCreated$gestureDetector$1
                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    ((StoryViewerPageViewModel) this.receiver).goToNextPost();
                }
            }, new Function0<Unit>(getViewModel()) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$onViewCreated$gestureDetector$2
                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    ((StoryViewerPageViewModel) this.receiver).goToPreviousPost();
                }
            }, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$onViewCreated$gestureDetector$3
                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    StoryViewerPageFragment.startReply$default((StoryViewerPageFragment) this.receiver, false, 0, 3, null);
                }
            }, null, getSharedViewModel(), 16, null));
            touchInterceptingFrameLayout.setOnInterceptTouchEventListener(new TouchInterceptingFrameLayout.OnInterceptTouchEventListener() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda5
                @Override // org.thoughtcrime.securesms.util.views.TouchInterceptingFrameLayout.OnInterceptTouchEventListener
                public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
                    return StoryViewerPageFragment.m3040onViewCreated$lambda1(StoryViewerPageFragment.this, motionEvent);
                }
            });
            touchInterceptingFrameLayout.setOnTouchListener(new View.OnTouchListener(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda6
                public final /* synthetic */ StoryViewerPageFragment f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnTouchListener
                public final boolean onTouch(View view2, MotionEvent motionEvent) {
                    return StoryViewerPageFragment.m3044onViewCreated$lambda2(GestureDetectorCompat.this, this.f$1, view2, motionEvent);
                }
            });
            MaterialButton materialButton2 = this.viewsAndReplies;
            if (materialButton2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton2 = null;
            }
            materialButton2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda7
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    StoryViewerPageFragment.m3045onViewCreated$lambda3(StoryViewerPageFragment.this, view2);
                }
            });
            findViewById6.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda8
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    StoryViewerPageFragment.this.displayMoreContextMenu(view2);
                }
            });
            SegmentedProgressBar segmentedProgressBar2 = this.progressBar;
            if (segmentedProgressBar2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("progressBar");
                segmentedProgressBar2 = null;
            }
            segmentedProgressBar2.setListener(new SegmentedProgressBarListener(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$onViewCreated$6
                final /* synthetic */ StoryViewerPageFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // org.thoughtcrime.securesms.components.segmentedprogressbar.SegmentedProgressBarListener
                public void onPage(int i, int i2) {
                    if (i != i2 && this.this$0.getContext() != null) {
                        this.this$0.getViewModel().setSelectedPostIndex(i2);
                    }
                }

                @Override // org.thoughtcrime.securesms.components.segmentedprogressbar.SegmentedProgressBarListener
                public void onFinished() {
                    this.this$0.getViewModel().goToNextPost();
                }

                @Override // org.thoughtcrime.securesms.components.segmentedprogressbar.SegmentedProgressBarListener
                public Float onRequestSegmentProgressPercentage() {
                    VideoControlsDelegate.PlayerState playerState;
                    StoryPost post = this.this$0.getViewModel().getPost();
                    if (post == null) {
                        return null;
                    }
                    Uri uri = post.getContent().isVideo() ? post.getContent().getUri() : null;
                    if (uri == null || (playerState = this.this$0.videoControlsDelegate.getPlayerState(uri)) == null) {
                        return null;
                    }
                    return Float.valueOf(this.this$0.getVideoPlaybackPosition(playerState) / ((float) this.this$0.getVideoPlaybackDuration(playerState)));
                }
            });
            onReactionSentView.setCallback(new OnReactionSentView.Callback(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$onViewCreated$7
                final /* synthetic */ StoryViewerPageFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // org.thoughtcrime.securesms.stories.viewer.reply.reaction.OnReactionSentView.Callback
                public void onFinished() {
                    this.this$0.getViewModel().setIsDisplayingReactionAnimation(false);
                }
            });
            getSharedViewModel().isScrolling().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda9
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    StoryViewerPageFragment.m3046onViewCreated$lambda4(StoryViewerPageFragment.this, (Boolean) obj);
                }
            });
            LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
            Disposable subscribe = getSharedViewModel().isChildScrolling().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda10
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    StoryViewerPageFragment.m3047onViewCreated$lambda5(StoryViewerPageFragment.this, (Boolean) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe, "sharedViewModel.isChildS…rScrollingChild(it)\n    }");
            lifecycleDisposable.plusAssign(subscribe);
            LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
            Disposable subscribe2 = getStoryVolumeViewModel().getState().distinctUntilChanged().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(storyVolumeOverlayView) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda11
                public final /* synthetic */ StoryVolumeOverlayView f$1;

                {
                    this.f$1 = r2;
                }

                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    StoryViewerPageFragment.m3048onViewCreated$lambda6(StoryViewerPageFragment.this, this.f$1, (StoryVolumeState) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe2, "storyVolumeViewModel.sta…verlayView)\n      }\n    }");
            lifecycleDisposable2.plusAssign(subscribe2);
            LifecycleDisposable lifecycleDisposable3 = this.lifecycleDisposable;
            Disposable subscribe3 = getSharedViewModel().getState().distinctUntilChanged().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda12
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    StoryViewerPageFragment.m3049onViewCreated$lambda7(StoryViewerPageFragment.this, (StoryViewerState) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe3, "sharedViewModel.state.di…Page(false)\n      }\n    }");
            lifecycleDisposable3.plusAssign(subscribe3);
            LifecycleDisposable lifecycleDisposable4 = this.lifecycleDisposable;
            Disposable subscribe4 = getViewModel().getState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(avatarImageView, avatarImageView2, textView, textView2, textView3, textView4, textView5, findViewById12) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda13
                public final /* synthetic */ AvatarImageView f$1;
                public final /* synthetic */ AvatarImageView f$2;
                public final /* synthetic */ TextView f$3;
                public final /* synthetic */ TextView f$4;
                public final /* synthetic */ TextView f$5;
                public final /* synthetic */ TextView f$6;
                public final /* synthetic */ TextView f$7;
                public final /* synthetic */ View f$8;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                    this.f$5 = r6;
                    this.f$6 = r7;
                    this.f$7 = r8;
                    this.f$8 = r9;
                }

                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    StoryViewerPageFragment.m3050onViewCreated$lambda9(StoryViewerPageFragment.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, (StoryViewerPageState) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe4, "viewModel.state.observeO…)\n        }\n      }\n    }");
            lifecycleDisposable4.plusAssign(subscribe4);
            getViewModel().getStoryViewerPlaybackState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda2
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    StoryViewerPageFragment.m3041onViewCreated$lambda10(StoryViewerPageFragment.this, (StoryViewerPlaybackState) obj);
                }
            });
            LifecycleDisposable lifecycleDisposable5 = this.timeoutDisposable;
            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
            Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
            lifecycleDisposable5.bindTo(viewLifecycleOwner);
            LifecycleDisposable lifecycleDisposable6 = this.lifecycleDisposable;
            LifecycleOwner viewLifecycleOwner2 = getViewLifecycleOwner();
            Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner2, "viewLifecycleOwner");
            lifecycleDisposable6.bindTo(viewLifecycleOwner2);
            LifecycleDisposable lifecycleDisposable7 = this.lifecycleDisposable;
            Disposable subscribe5 = getViewModel().getGroupDirectReplyObservable().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda3
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    StoryViewerPageFragment.m3042onViewCreated$lambda11(StoryViewerPageFragment.this, (Optional) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe5, "viewModel.groupDirectRep…}\n        }\n      }\n    }");
            lifecycleDisposable7.plusAssign(subscribe5);
            View view2 = this.viewsAndReplies;
            if (view2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                view2 = null;
            }
            adjustConstraintsForScreenDimensions(view2, touchInterceptingFrameLayout, cardView);
            getChildFragmentManager().setFragmentResultListener(StoryDirectReplyDialogFragment.REQUEST_EMOJI, getViewLifecycleOwner(), new FragmentResultListener(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda4
                public final /* synthetic */ StoryViewerPageFragment f$1;

                {
                    this.f$1 = r2;
                }

                @Override // androidx.fragment.app.FragmentResultListener
                public final void onFragmentResult(String str, Bundle bundle2) {
                    StoryViewerPageFragment.m3043onViewCreated$lambda12(OnReactionSentView.this, this.f$1, str, bundle2);
                }
            });
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback
    public Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        return MultiselectForwardBottomSheet.Callback.DefaultImpls.getStorySendRequirements(this);
    }

    public StoryViewerPageFragment() {
        super(R.layout.stories_viewer_fragment_page);
    }

    private final StoryVolumeViewModel getStoryVolumeViewModel() {
        return (StoryVolumeViewModel) this.storyVolumeViewModel$delegate.getValue();
    }

    public final StoryViewerPageViewModel getViewModel() {
        return (StoryViewerPageViewModel) this.viewModel$delegate.getValue();
    }

    private final StoryViewerViewModel getSharedViewModel() {
        return (StoryViewerViewModel) this.sharedViewModel$delegate.getValue();
    }

    public final RecipientId getStoryRecipientId() {
        Parcelable parcelable = requireArguments().getParcelable(ARG_STORY_RECIPIENT_ID);
        Intrinsics.checkNotNull(parcelable);
        return (RecipientId) parcelable;
    }

    public final long getInitialStoryId() {
        return requireArguments().getLong(ARG_STORY_ID, -1);
    }

    private final boolean isFromNotification() {
        return requireArguments().getBoolean(ARG_IS_FROM_NOTIFICATION, false);
    }

    private final int getGroupReplyStartPosition() {
        return requireArguments().getInt(ARG_GROUP_REPLY_START_POSITION, -1);
    }

    public final boolean isUnviewedOnly() {
        return requireArguments().getBoolean(ARG_IS_UNVIEWED_ONLY, false);
    }

    public final boolean isOutgoingOnly() {
        return requireArguments().getBoolean(ARG_IS_OUTGOING_ONLY, false);
    }

    private final boolean isFromInfoContextMenuAction() {
        return requireArguments().getBoolean(ARG_IS_FROM_INFO_CONTEXT_MENU_ACTION, false);
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m3039onViewCreated$lambda0(StoryViewerPageFragment storyViewerPageFragment, View view) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        storyViewerPageFragment.requireActivity().onBackPressed();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final boolean m3040onViewCreated$lambda1(StoryViewerPageFragment storyViewerPageFragment, MotionEvent motionEvent) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        StorySlateView storySlateView = storyViewerPageFragment.storySlate;
        if (storySlateView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storySlate");
            storySlateView = null;
        }
        return !storySlateView.getState().getHasClickableContent() && !(storyViewerPageFragment.getChildFragmentManager().findFragmentById(R.id.story_content_container) instanceof StoryTextPostPreviewFragment);
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final boolean m3044onViewCreated$lambda2(GestureDetectorCompat gestureDetectorCompat, StoryViewerPageFragment storyViewerPageFragment, View view, MotionEvent motionEvent) {
        Intrinsics.checkNotNullParameter(gestureDetectorCompat, "$gestureDetector");
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        boolean onTouchEvent = gestureDetectorCompat.onTouchEvent(motionEvent);
        if (motionEvent.getActionMasked() == 0) {
            storyViewerPageFragment.getViewModel().setIsUserTouching(true);
        } else if (motionEvent.getActionMasked() == 1 || motionEvent.getActionMasked() == 3) {
            storyViewerPageFragment.getViewModel().setIsUserTouching(false);
            float translationX = storyViewerPageFragment.requireView().getTranslationX();
            DimensionUnit dimensionUnit = DimensionUnit.DP;
            boolean z = translationX > dimensionUnit.toPixels(56.0f);
            boolean z2 = storyViewerPageFragment.requireView().getTranslationY() > dimensionUnit.toPixels(56.0f) || storyViewerPageFragment.requireView().getTranslationY() < (-dimensionUnit.toPixels(56.0f));
            if ((z || z2) && motionEvent.getActionMasked() == 1) {
                storyViewerPageFragment.requireActivity().onBackPressed();
            } else {
                storyViewerPageFragment.getSharedViewModel().setIsChildScrolling(false);
                storyViewerPageFragment.requireView().animate().setInterpolator(StoryGestureListener.Companion.getINTERPOLATOR()).setDuration(100).translationX(0.0f).translationY(0.0f);
            }
        }
        return onTouchEvent;
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m3045onViewCreated$lambda3(StoryViewerPageFragment storyViewerPageFragment, View view) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        startReply$default(storyViewerPageFragment, false, 0, 3, null);
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m3046onViewCreated$lambda4(StoryViewerPageFragment storyViewerPageFragment, Boolean bool) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        StoryViewerPageViewModel viewModel = storyViewerPageFragment.getViewModel();
        Intrinsics.checkNotNullExpressionValue(bool, "isScrolling");
        viewModel.setIsUserScrollingParent(bool.booleanValue());
    }

    /* renamed from: onViewCreated$lambda-5 */
    public static final void m3047onViewCreated$lambda5(StoryViewerPageFragment storyViewerPageFragment, Boolean bool) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        StoryViewerPageViewModel viewModel = storyViewerPageFragment.getViewModel();
        Intrinsics.checkNotNullExpressionValue(bool, "it");
        viewModel.setIsUserScrollingChild(bool.booleanValue());
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m3048onViewCreated$lambda6(StoryViewerPageFragment storyViewerPageFragment, StoryVolumeOverlayView storyVolumeOverlayView, StoryVolumeState storyVolumeState) {
        StoryPost.Content content;
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(storyVolumeOverlayView, "$storyVolumeOverlayView");
        if (storyVolumeState.isMuted()) {
            storyViewerPageFragment.videoControlsDelegate.mute();
        } else if (storyViewerPageFragment.getViewModel().hasPost()) {
            StoryPost post = storyViewerPageFragment.getViewModel().getPost();
            if (((post == null || (content = post.getContent()) == null || !content.isVideo()) ? false : true) && storyVolumeState.getLevel() >= 0) {
                if (!storyVolumeState.isMuted()) {
                    storyViewerPageFragment.videoControlsDelegate.unmute();
                }
                AudioManager audioManager = ServiceUtil.getAudioManager(storyViewerPageFragment.requireContext());
                if (audioManager.getStreamVolume(3) != storyVolumeState.getLevel()) {
                    audioManager.setStreamVolume(3, storyVolumeState.getLevel(), 0);
                    storyVolumeOverlayView.setVolumeLevel(storyVolumeState.getLevel());
                    storyVolumeOverlayView.setVideoHaNoAudio(!storyViewerPageFragment.videoControlsDelegate.hasAudioStream());
                    storyViewerPageFragment.displayStoryVolumeOverlayForTimeout(storyVolumeOverlayView);
                }
            }
        }
    }

    /* renamed from: onViewCreated$lambda-7 */
    public static final void m3049onViewCreated$lambda7(StoryViewerPageFragment storyViewerPageFragment, StoryViewerState storyViewerState) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        storyViewerPageFragment.getViewModel().setIsRunningSharedElementAnimation(!storyViewerState.getLoadState().isCrossfaderReady());
        FrameLayout frameLayout = storyViewerPageFragment.storyContentContainer;
        SegmentedProgressBar segmentedProgressBar = null;
        if (frameLayout == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyContentContainer");
            frameLayout = null;
        }
        ViewExtensionsKt.setVisible(frameLayout, storyViewerState.getLoadState().isCrossfaderReady());
        boolean z = false;
        if (storyViewerState.getPages().size() <= storyViewerState.getPage()) {
            storyViewerPageFragment.getViewModel().setIsSelectedPage(false);
        } else if (Intrinsics.areEqual(storyViewerPageFragment.getStoryRecipientId(), storyViewerState.getPages().get(storyViewerState.getPage()))) {
            SegmentedProgressBar segmentedProgressBar2 = storyViewerPageFragment.progressBar;
            if (segmentedProgressBar2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("progressBar");
                segmentedProgressBar2 = null;
            }
            if (segmentedProgressBar2.getSegmentCount() != 0) {
                SegmentedProgressBar segmentedProgressBar3 = storyViewerPageFragment.progressBar;
                if (segmentedProgressBar3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("progressBar");
                    segmentedProgressBar3 = null;
                }
                segmentedProgressBar3.reset();
                SegmentedProgressBar segmentedProgressBar4 = storyViewerPageFragment.progressBar;
                if (segmentedProgressBar4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("progressBar");
                } else {
                    segmentedProgressBar = segmentedProgressBar4;
                }
                segmentedProgressBar.setPosition(storyViewerPageFragment.getViewModel().getRestartIndex());
                storyViewerPageFragment.videoControlsDelegate.restart();
            }
            StoryViewerPageViewModel viewModel = storyViewerPageFragment.getViewModel();
            if (storyViewerState.getPage() == 0) {
                z = true;
            }
            viewModel.setIsFirstPage(z);
            storyViewerPageFragment.getViewModel().setIsSelectedPage(true);
        } else {
            storyViewerPageFragment.getViewModel().setIsSelectedPage(false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0118, code lost:
        if (kotlin.jvm.internal.Intrinsics.areEqual(r8.getSegmentDurations(), r7) == false) goto L_0x011a;
     */
    /* renamed from: onViewCreated$lambda-9 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m3050onViewCreated$lambda9(org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment r6, org.thoughtcrime.securesms.components.AvatarImageView r7, org.thoughtcrime.securesms.components.AvatarImageView r8, android.widget.TextView r9, android.widget.TextView r10, android.widget.TextView r11, android.widget.TextView r12, android.widget.TextView r13, android.view.View r14, org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState r15) {
        /*
        // Method dump skipped, instructions count: 467
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment.m3050onViewCreated$lambda9(org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment, org.thoughtcrime.securesms.components.AvatarImageView, org.thoughtcrime.securesms.components.AvatarImageView, android.widget.TextView, android.widget.TextView, android.widget.TextView, android.widget.TextView, android.widget.TextView, android.view.View, org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState):void");
    }

    /* renamed from: onViewCreated$lambda-10 */
    public static final void m3041onViewCreated$lambda10(StoryViewerPageFragment storyViewerPageFragment, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        if (storyViewerPlaybackState.isPaused()) {
            storyViewerPageFragment.pauseProgress();
        } else {
            storyViewerPageFragment.resumeProgress();
        }
        StoryFirstNavigationStub storyFirstNavigationStub = storyViewerPageFragment.storyFirstTimeNavigationViewStub;
        StoryFirstNavigationStub storyFirstNavigationStub2 = null;
        if (storyFirstNavigationStub == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyFirstTimeNavigationViewStub");
            storyFirstNavigationStub = null;
        }
        boolean isVisible = storyFirstNavigationStub.isVisible();
        if (storyViewerPlaybackState.getHideChromeImmediate()) {
            storyViewerPageFragment.hideChromeImmediate();
            FrameLayout frameLayout = storyViewerPageFragment.storyCaptionContainer;
            if (frameLayout == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyCaptionContainer");
                frameLayout = null;
            }
            ViewExtensionsKt.setVisible(frameLayout, false);
            StoryFirstNavigationStub storyFirstNavigationStub3 = storyViewerPageFragment.storyFirstTimeNavigationViewStub;
            if (storyFirstNavigationStub3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyFirstTimeNavigationViewStub");
                storyFirstNavigationStub3 = null;
            }
            storyFirstNavigationStub3.hide();
        } else if (storyViewerPlaybackState.getHideChrome()) {
            storyViewerPageFragment.hideChrome();
            FrameLayout frameLayout2 = storyViewerPageFragment.storyCaptionContainer;
            if (frameLayout2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyCaptionContainer");
                frameLayout2 = null;
            }
            ViewExtensionsKt.setVisible(frameLayout2, true);
            StoryFirstNavigationStub storyFirstNavigationStub4 = storyViewerPageFragment.storyFirstTimeNavigationViewStub;
            if (storyFirstNavigationStub4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyFirstTimeNavigationViewStub");
                storyFirstNavigationStub4 = null;
            }
            storyFirstNavigationStub4.showIfAble(true ^ SignalStore.storyValues().getUserHasSeenFirstNavView());
        } else {
            storyViewerPageFragment.showChrome();
            FrameLayout frameLayout3 = storyViewerPageFragment.storyCaptionContainer;
            if (frameLayout3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyCaptionContainer");
                frameLayout3 = null;
            }
            ViewExtensionsKt.setVisible(frameLayout3, true);
            StoryFirstNavigationStub storyFirstNavigationStub5 = storyViewerPageFragment.storyFirstTimeNavigationViewStub;
            if (storyFirstNavigationStub5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyFirstTimeNavigationViewStub");
                storyFirstNavigationStub5 = null;
            }
            storyFirstNavigationStub5.showIfAble(true ^ SignalStore.storyValues().getUserHasSeenFirstNavView());
        }
        StoryFirstNavigationStub storyFirstNavigationStub6 = storyViewerPageFragment.storyFirstTimeNavigationViewStub;
        if (storyFirstNavigationStub6 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyFirstTimeNavigationViewStub");
            storyFirstNavigationStub6 = null;
        }
        boolean isVisible2 = storyFirstNavigationStub6.isVisible();
        if (isVisible2 && Build.VERSION.SDK_INT >= 31) {
            storyViewerPageFragment.hideChromeImmediate();
            FrameLayout frameLayout4 = storyViewerPageFragment.storyContentContainer;
            if (frameLayout4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyContentContainer");
                frameLayout4 = null;
            }
            frameLayout4.setRenderEffect(RenderEffect.createBlurEffect(100.0f, 100.0f, Shader.TileMode.CLAMP));
        } else if (Build.VERSION.SDK_INT >= 31) {
            FrameLayout frameLayout5 = storyViewerPageFragment.storyContentContainer;
            if (frameLayout5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyContentContainer");
                frameLayout5 = null;
            }
            frameLayout5.setRenderEffect(null);
        }
        if (isVisible2 ^ isVisible) {
            StoryViewerPageViewModel viewModel = storyViewerPageFragment.getViewModel();
            StoryFirstNavigationStub storyFirstNavigationStub7 = storyViewerPageFragment.storyFirstTimeNavigationViewStub;
            if (storyFirstNavigationStub7 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyFirstTimeNavigationViewStub");
            } else {
                storyFirstNavigationStub2 = storyFirstNavigationStub7;
            }
            viewModel.setIsDisplayingFirstTimeNavigation(storyFirstNavigationStub2.isVisible());
        }
    }

    /* renamed from: onViewCreated$lambda-11 */
    public static final void m3042onViewCreated$lambda11(StoryViewerPageFragment storyViewerPageFragment, Optional optional) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        if (optional.isPresent()) {
            Object obj = optional.get();
            Intrinsics.checkNotNullExpressionValue(obj, "opt.get()");
            StoryViewerDialog storyViewerDialog = (StoryViewerDialog) obj;
            if (storyViewerDialog instanceof StoryViewerDialog.GroupDirectReply) {
                StoryViewerDialog.GroupDirectReply groupDirectReply = (StoryViewerDialog.GroupDirectReply) storyViewerDialog;
                storyViewerPageFragment.onStartDirectReply(groupDirectReply.getStoryId(), groupDirectReply.getRecipientId());
            }
        }
    }

    /* renamed from: onViewCreated$lambda-12 */
    public static final void m3043onViewCreated$lambda12(OnReactionSentView onReactionSentView, StoryViewerPageFragment storyViewerPageFragment, String str, Bundle bundle) {
        Intrinsics.checkNotNullParameter(onReactionSentView, "$reactionAnimationView");
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(str, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        String string = bundle.getString(StoryDirectReplyDialogFragment.REQUEST_EMOJI);
        if (string != null) {
            onReactionSentView.playForEmoji(string);
            storyViewerPageFragment.getViewModel().setIsDisplayingReactionAnimation(true);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().setIsFragmentResumed(true);
        getViewModel().checkReadReceiptState();
        markViewedIfAble();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        getViewModel().setIsFragmentResumed(false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        Intrinsics.checkNotNullExpressionValue(fragments, "childFragmentManager.fragments");
        for (Fragment fragment : fragments) {
            if (fragment instanceof MediaPreviewFragment) {
                ((MediaPreviewFragment) fragment).cleanUp();
            }
        }
        this.volumeDebouncer.clear();
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback
    public void onDismissForwardSheet() {
        getViewModel().setIsDisplayingForwardDialog(false);
    }

    private final long calculateDurationForText(StoryPost.Content.TextContent textContent) {
        return TimeUnit.SECONDS.toMillis(((long) textContent.getLength()) / CHARACTERS_PER_SECOND) + MIN_TEXT_STORY_PLAYBACK;
    }

    public final float getVideoPlaybackPosition(VideoControlsDelegate.PlayerState playerState) {
        if (playerState.isGif()) {
            return ((float) playerState.getPosition()) + ((float) (playerState.getDuration() * ((long) playerState.getLoopCount())));
        }
        return (float) playerState.getPosition();
    }

    public final long getVideoPlaybackDuration(VideoControlsDelegate.PlayerState playerState) {
        if (!playerState.isGif()) {
            return Math.min(playerState.getDuration(), MAX_VIDEO_PLAYBACK_DURATION);
        }
        return Math.max(MIN_GIF_PLAYBACK_DURATION, playerState.getDuration() * MIN_GIF_LOOPS);
    }

    private final void displayStoryVolumeOverlayForTimeout(View view) {
        Animator animator = this.volumeInAnimator;
        if (!(animator != null && animator.isRunning())) {
            Animator animator2 = this.volumeOutAnimator;
            if (animator2 != null) {
                animator2.cancel();
            }
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, View.ALPHA, 1.0f);
            ofFloat.setDuration(200L);
            ofFloat.start();
            this.volumeInAnimator = ofFloat;
        }
        this.volumeDebouncer.publish(new Runnable(view) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda14
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StoryViewerPageFragment.m3036displayStoryVolumeOverlayForTimeout$lambda16(StoryViewerPageFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: displayStoryVolumeOverlayForTimeout$lambda-16 */
    public static final void m3036displayStoryVolumeOverlayForTimeout$lambda16(StoryViewerPageFragment storyViewerPageFragment, View view) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(view, "$view");
        Animator animator = storyViewerPageFragment.volumeOutAnimator;
        if (!(animator != null && animator.isRunning())) {
            Animator animator2 = storyViewerPageFragment.volumeInAnimator;
            if (animator2 != null) {
                animator2.cancel();
            }
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, View.ALPHA, 0.0f);
            ofFloat.setDuration(200L);
            ofFloat.start();
            storyViewerPageFragment.volumeOutAnimator = ofFloat;
        }
    }

    private final void hideChromeImmediate() {
        AnimatorSet animatorSet = this.animatorSet;
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        List<? extends View> list = this.chrome;
        if (list == null) {
            Intrinsics.throwUninitializedPropertyAccessException("chrome");
            list = null;
        }
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (View view : list) {
            view.setAlpha(0.0f);
            arrayList.add(Unit.INSTANCE);
        }
    }

    private final void hideChrome() {
        animateChrome(0.0f);
    }

    private final void showChrome() {
        animateChrome(1.0f);
    }

    private final void animateChrome(float f) {
        AnimatorSet animatorSet = this.animatorSet;
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.setDuration(100L);
        animatorSet2.setInterpolator(StoryGestureListener.Companion.getINTERPOLATOR());
        List<? extends View> list = this.chrome;
        if (list == null) {
            Intrinsics.throwUninitializedPropertyAccessException("chrome");
            list = null;
        }
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (View view : list) {
            arrayList.add(ObjectAnimator.ofFloat(view, View.ALPHA, f));
        }
        animatorSet2.playTogether(arrayList);
        animatorSet2.start();
        this.animatorSet = animatorSet2;
    }

    private final void adjustConstraintsForScreenDimensions(View view, View view2, CardView cardView) {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone((ConstraintLayout) requireView());
        int i = WhenMappings.$EnumSwitchMapping$0[StoryDisplay.Companion.getStoryDisplay((float) getResources().getDisplayMetrics().widthPixels, (float) getResources().getDisplayMetrics().heightPixels).ordinal()];
        if (i == 1) {
            constraintSet.setDimensionRatio(view2.getId(), "9:16");
            constraintSet.connect(view.getId(), 3, view2.getId(), 4);
            constraintSet.connect(view.getId(), 4, 0, 4);
            cardView.setRadius(DimensionUnit.DP.toPixels(18.0f));
        } else if (i == 2) {
            constraintSet.setDimensionRatio(view2.getId(), "9:16");
            constraintSet.clear(view.getId(), 3);
            constraintSet.connect(view.getId(), 4, view2.getId(), 4);
            cardView.setRadius(DimensionUnit.DP.toPixels(18.0f));
        } else if (i == 3) {
            constraintSet.setDimensionRatio(view2.getId(), null);
            constraintSet.clear(view.getId(), 3);
            constraintSet.connect(view.getId(), 4, view2.getId(), 4);
            cardView.setRadius(DimensionUnit.DP.toPixels(0.0f));
        }
        constraintSet.applyTo((ConstraintLayout) requireView());
    }

    private final void resumeProgress() {
        Uri uri;
        StoryPost post = getViewModel().getPost();
        if (post != null) {
            SegmentedProgressBar segmentedProgressBar = this.progressBar;
            SegmentedProgressBar segmentedProgressBar2 = null;
            if (segmentedProgressBar == null) {
                Intrinsics.throwUninitializedPropertyAccessException("progressBar");
                segmentedProgressBar = null;
            }
            if (segmentedProgressBar.getSegmentCount() != 0 && (uri = post.getContent().getUri()) != null) {
                SegmentedProgressBar segmentedProgressBar3 = this.progressBar;
                if (segmentedProgressBar3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("progressBar");
                } else {
                    segmentedProgressBar2 = segmentedProgressBar3;
                }
                segmentedProgressBar2.start();
                this.videoControlsDelegate.resume(uri);
            }
        }
    }

    private final void pauseProgress() {
        SegmentedProgressBar segmentedProgressBar = this.progressBar;
        if (segmentedProgressBar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("progressBar");
            segmentedProgressBar = null;
        }
        segmentedProgressBar.pause();
        this.videoControlsDelegate.pause();
    }

    public static /* synthetic */ void startReply$default(StoryViewerPageFragment storyViewerPageFragment, boolean z, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            z = false;
        }
        if ((i2 & 2) != 0) {
            i = -1;
        }
        storyViewerPageFragment.startReply(z, i);
    }

    private final void startReply(boolean z, int i) {
        DialogFragment dialogFragment;
        StoryPost post = getViewModel().getPost();
        if (post != null) {
            long id = post.getId();
            int i2 = WhenMappings.$EnumSwitchMapping$1[getViewModel().getSwipeToReplyState().ordinal()];
            if (i2 != 1) {
                if (i2 == 2) {
                    dialogFragment = StoryViewsBottomSheetDialogFragment.Companion.create(id);
                } else if (i2 == 3) {
                    StoryGroupReplyBottomSheetDialogFragment.Companion companion = StoryGroupReplyBottomSheetDialogFragment.Companion;
                    Recipient group = post.getGroup();
                    Intrinsics.checkNotNull(group);
                    RecipientId id2 = group.getId();
                    Intrinsics.checkNotNullExpressionValue(id2, "storyPost.group!!.id");
                    dialogFragment = companion.create(id, id2, z, i);
                } else if (i2 == 4) {
                    dialogFragment = StoryDirectReplyDialogFragment.Companion.create$default(StoryDirectReplyDialogFragment.Companion, id, null, 2, null);
                } else if (i2 == 5) {
                    StoryViewsAndRepliesDialogFragment.Companion companion2 = StoryViewsAndRepliesDialogFragment.Companion;
                    Recipient group2 = post.getGroup();
                    Intrinsics.checkNotNull(group2);
                    RecipientId id3 = group2.getId();
                    Intrinsics.checkNotNullExpressionValue(id3, "storyPost.group!!.id");
                    dialogFragment = companion2.create(id, id3, z ? StoryViewsAndRepliesDialogFragment.StartPage.REPLIES : getViewsAndRepliesDialogStartPage(), z, i);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                if (getViewModel().getSwipeToReplyState() == StoryViewerPageState.ReplyState.PRIVATE) {
                    getViewModel().setIsDisplayingDirectReplyDialog(true);
                } else {
                    getViewModel().setIsDisplayingViewsAndRepliesDialog(true);
                }
                dialogFragment.showNow(getChildFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
            }
        }
    }

    public final void showInfo(StoryPost storyPost) {
        getViewModel().setIsDisplayingInfoDialog(true);
        StoryInfoBottomSheetDialogFragment.Companion.create(storyPost.getId()).show(getChildFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    private final void markViewedIfAble() {
        StoryPost post = getViewModel().getPost();
        if (post != null && post.getContent().getTransferState() == 0 && isResumed()) {
            getViewModel().markViewed(post);
        }
    }

    private final void onStartDirectReply(long j, RecipientId recipientId) {
        getViewModel().setIsDisplayingDirectReplyDialog(true);
        StoryDirectReplyDialogFragment.Companion.create(j, recipientId).show(getChildFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    private final StoryViewsAndRepliesDialogFragment.StartPage getViewsAndRepliesDialogStartPage() {
        if (getViewModel().requirePost().getReplyCount() > 0) {
            return StoryViewsAndRepliesDialogFragment.StartPage.REPLIES;
        }
        return StoryViewsAndRepliesDialogFragment.StartPage.VIEWS;
    }

    private final void presentStory(StoryPost storyPost, int i) {
        Fragment findFragmentById = getChildFragmentManager().findFragmentById(R.id.story_content_container);
        StorySlateView storySlateView = null;
        SegmentedProgressBar segmentedProgressBar = null;
        SegmentedProgressBar segmentedProgressBar2 = null;
        if (findFragmentById == null || !Intrinsics.areEqual(findFragmentById.requireArguments().getParcelable(MediaPreviewFragment.DATA_URI), storyPost.getContent().getUri())) {
            if (findFragmentById instanceof MediaPreviewFragment) {
                ((MediaPreviewFragment) findFragmentById).cleanUp();
            }
            if (storyPost.getContent().getUri() == null) {
                SegmentedProgressBar segmentedProgressBar3 = this.progressBar;
                if (segmentedProgressBar3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("progressBar");
                    segmentedProgressBar3 = null;
                }
                segmentedProgressBar3.setPosition(i);
                SegmentedProgressBar segmentedProgressBar4 = this.progressBar;
                if (segmentedProgressBar4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("progressBar");
                } else {
                    segmentedProgressBar2 = segmentedProgressBar4;
                }
                segmentedProgressBar2.invalidate();
                return;
            }
            SegmentedProgressBar segmentedProgressBar5 = this.progressBar;
            if (segmentedProgressBar5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("progressBar");
                segmentedProgressBar5 = null;
            }
            segmentedProgressBar5.setPosition(i);
            StorySlateView storySlateView2 = this.storySlate;
            if (storySlateView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storySlate");
            } else {
                storySlateView = storySlateView2;
            }
            storySlateView.moveToState(StorySlateView.State.HIDDEN, storyPost.getId());
            getViewModel().setIsDisplayingSlate(false);
            getChildFragmentManager().beginTransaction().replace(R.id.story_content_container, createFragmentForPost(storyPost)).commitNow();
            return;
        }
        SegmentedProgressBar segmentedProgressBar6 = this.progressBar;
        if (segmentedProgressBar6 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("progressBar");
        } else {
            segmentedProgressBar = segmentedProgressBar6;
        }
        segmentedProgressBar.setPosition(i);
    }

    private final void presentSlate(StoryPost storyPost) {
        SlideDeck slideDeck;
        Slide thumbnailSlide;
        StorySlateView storySlateView = this.storySlate;
        StorySlateView storySlateView2 = null;
        if (storySlateView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storySlate");
            storySlateView = null;
        }
        MessageRecord messageRecord = storyPost.getConversationMessage().getMessageRecord();
        MediaMmsMessageRecord mediaMmsMessageRecord = messageRecord instanceof MediaMmsMessageRecord ? (MediaMmsMessageRecord) messageRecord : null;
        storySlateView.setBackground((mediaMmsMessageRecord == null || (slideDeck = mediaMmsMessageRecord.getSlideDeck()) == null || (thumbnailSlide = slideDeck.getThumbnailSlide()) == null) ? null : thumbnailSlide.getPlaceholderBlur());
        int transferState = storyPost.getContent().getTransferState();
        if (transferState == 0) {
            StorySlateView storySlateView3 = this.storySlate;
            if (storySlateView3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storySlate");
            } else {
                storySlateView2 = storySlateView3;
            }
            storySlateView2.moveToState(StorySlateView.State.HIDDEN, storyPost.getId());
            getViewModel().setIsDisplayingSlate(false);
            markViewedIfAble();
        } else if (transferState == 1) {
            StorySlateView storySlateView4 = this.storySlate;
            if (storySlateView4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storySlate");
            } else {
                storySlateView2 = storySlateView4;
            }
            storySlateView2.moveToState(StorySlateView.State.LOADING, storyPost.getId());
            getSharedViewModel().setContentIsReady();
            getViewModel().setIsDisplayingSlate(true);
        } else if (transferState == 2) {
            StorySlateView storySlateView5 = this.storySlate;
            if (storySlateView5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storySlate");
            } else {
                storySlateView2 = storySlateView5;
            }
            storySlateView2.moveToState(StorySlateView.State.LOADING, storyPost.getId());
            getSharedViewModel().setContentIsReady();
            getViewModel().setIsDisplayingSlate(true);
        } else if (transferState == 3) {
            StorySlateView storySlateView6 = this.storySlate;
            if (storySlateView6 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storySlate");
            } else {
                storySlateView2 = storySlateView6;
            }
            storySlateView2.moveToState(StorySlateView.State.ERROR, storyPost.getId());
            getSharedViewModel().setContentIsReady();
            getViewModel().setIsDisplayingSlate(true);
        }
    }

    @Override // org.thoughtcrime.securesms.stories.StorySlateView.Callback
    public void onStateChanged(StorySlateView.State state, long j) {
        Intrinsics.checkNotNullParameter(state, "state");
        if (state == StorySlateView.State.LOADING || state == StorySlateView.State.RETRY) {
            this.timeoutDisposable.getDisposables().clear();
            LifecycleDisposable lifecycleDisposable = this.timeoutDisposable;
            Disposable subscribe = Observable.interval(10, TimeUnit.SECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(j) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda0
                public final /* synthetic */ long f$1;

                {
                    this.f$1 = r2;
                }

                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    StoryViewerPageFragment.m3038onStateChanged$lambda20(StoryViewerPageFragment.this, this.f$1, (Long) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe, "interval(10, TimeUnit.SE….ERROR, postId)\n        }");
            lifecycleDisposable.plusAssign(subscribe);
            getViewModel().forceDownloadSelectedPost();
        } else {
            this.timeoutDisposable.getDisposables().clear();
        }
        MaterialButton materialButton = this.viewsAndReplies;
        if (materialButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
            materialButton = null;
        }
        ViewExtensionsKt.setVisible(materialButton, state == StorySlateView.State.HIDDEN);
    }

    /* renamed from: onStateChanged$lambda-20 */
    public static final void m3038onStateChanged$lambda20(StoryViewerPageFragment storyViewerPageFragment, long j, Long l) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        StorySlateView storySlateView = storyViewerPageFragment.storySlate;
        if (storySlateView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storySlate");
            storySlateView = null;
        }
        storySlateView.moveToState(StorySlateView.State.ERROR, j);
    }

    private final void presentDistributionList(TextView textView, StoryPost storyPost) {
        Recipient distributionList = storyPost.getDistributionList();
        textView.setText(distributionList != null ? distributionList.getDisplayName(requireContext()) : null);
        ViewExtensionsKt.setVisible(textView, storyPost.getDistributionList() != null && !storyPost.getDistributionList().isMyStory());
    }

    private final void presentBlur(StoryPost storyPost) {
        SlideDeck slideDeck;
        Slide thumbnailSlide;
        MessageRecord messageRecord = storyPost.getConversationMessage().getMessageRecord();
        StoryFirstNavigationStub storyFirstNavigationStub = null;
        MediaMmsMessageRecord mediaMmsMessageRecord = messageRecord instanceof MediaMmsMessageRecord ? (MediaMmsMessageRecord) messageRecord : null;
        BlurHash placeholderBlur = (mediaMmsMessageRecord == null || (slideDeck = mediaMmsMessageRecord.getSlideDeck()) == null || (thumbnailSlide = slideDeck.getThumbnailSlide()) == null) ? null : thumbnailSlide.getPlaceholderBlur();
        StoryFirstNavigationStub storyFirstNavigationStub2 = this.storyFirstTimeNavigationViewStub;
        if (storyFirstNavigationStub2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyFirstTimeNavigationViewStub");
        } else {
            storyFirstNavigationStub = storyFirstNavigationStub2;
        }
        storyFirstNavigationStub.setBlurHash(placeholderBlur);
    }

    private final void presentCaption(TextView textView, TextView textView2, View view, StoryPost storyPost) {
        String caption;
        String str = "";
        if ((storyPost.getContent() instanceof StoryPost.Content.AttachmentContent) && (caption = ((StoryPost.Content.AttachmentContent) storyPost.getContent()).getAttachment().getCaption()) != null) {
            str = caption;
        }
        textView.setText(str);
        textView2.setText(str);
        ViewExtensionsKt.setVisible(textView, str.length() > 0);
        textView.requestLayout();
        textView.addOnLayoutChangeListener(new View.OnLayoutChangeListener(str, textView, this, textView2, view) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1
            final /* synthetic */ TextView $caption$inlined;
            final /* synthetic */ String $displayBody$inlined;
            final /* synthetic */ TextView $largeCaption$inlined;
            final /* synthetic */ View $largeCaptionOverlay$inlined;
            final /* synthetic */ StoryViewerPageFragment this$0;

            {
                this.$displayBody$inlined = r1;
                this.$caption$inlined = r2;
                this.this$0 = r3;
                this.$largeCaption$inlined = r4;
                this.$largeCaptionOverlay$inlined = r5;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00b6: INVOKE  
                  (r1v7 'textView4' android.widget.TextView)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$1$1 : 0x00b3: CONSTRUCTOR  (r2v3 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$1$1 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment : 0x00ad: IGET  (r3v3 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment A[REMOVE]) = 
                  (r0v0 'this' org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1 A[IMMUTABLE_TYPE, THIS])
                 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1.this$0 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment)
                  (r1v7 'textView4' android.widget.TextView)
                  (wrap: android.widget.TextView : 0x00af: IGET  (r4v1 android.widget.TextView A[REMOVE]) = 
                  (r0v0 'this' org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1 A[IMMUTABLE_TYPE, THIS])
                 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1.$largeCaption$inlined android.widget.TextView)
                  (wrap: android.view.View : 0x00b1: IGET  (r5v1 android.view.View A[REMOVE]) = 
                  (r0v0 'this' org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1 A[IMMUTABLE_TYPE, THIS])
                 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1.$largeCaptionOverlay$inlined android.view.View)
                 call: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$1$1.<init>(org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment, android.widget.TextView, android.widget.TextView, android.view.View):void type: CONSTRUCTOR)
                 type: VIRTUAL call: android.widget.TextView.setOnClickListener(android.view.View$OnClickListener):void in method: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1.onLayoutChange(android.view.View, int, int, int, int, int, int, int, int):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00b3: CONSTRUCTOR  (r2v3 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$1$1 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment : 0x00ad: IGET  (r3v3 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment A[REMOVE]) = 
                  (r0v0 'this' org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1 A[IMMUTABLE_TYPE, THIS])
                 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1.this$0 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment)
                  (r1v7 'textView4' android.widget.TextView)
                  (wrap: android.widget.TextView : 0x00af: IGET  (r4v1 android.widget.TextView A[REMOVE]) = 
                  (r0v0 'this' org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1 A[IMMUTABLE_TYPE, THIS])
                 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1.$largeCaption$inlined android.widget.TextView)
                  (wrap: android.view.View : 0x00b1: IGET  (r5v1 android.view.View A[REMOVE]) = 
                  (r0v0 'this' org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1 A[IMMUTABLE_TYPE, THIS])
                 org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1.$largeCaptionOverlay$inlined android.view.View)
                 call: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$1$1.<init>(org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment, android.widget.TextView, android.widget.TextView, android.view.View):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1.onLayoutChange(android.view.View, int, int, int, int, int, int, int, int):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 14 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$1$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 20 more
                */
            @Override // android.view.View.OnLayoutChangeListener
            public void onLayoutChange(android.view.View r1, int r2, int r3, int r4, int r5, int r6, int r7, int r8, int r9) {
                /*
                    r0 = this;
                    java.lang.String r2 = "view"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r1, r2)
                    r1.removeOnLayoutChangeListener(r0)
                    java.lang.String r1 = r0.$displayBody$inlined
                    int r1 = r1.length()
                    r2 = 0
                    if (r1 <= 0) goto L_0x0013
                    r1 = 1
                    goto L_0x0014
                L_0x0013:
                    r1 = 0
                L_0x0014:
                    if (r1 == 0) goto L_0x008b
                    android.widget.TextView r1 = r0.$caption$inlined
                    int r1 = r1.getLineCount()
                    r3 = 5
                    if (r1 <= r3) goto L_0x008b
                    android.widget.TextView r1 = r0.$caption$inlined
                    android.text.Layout r1 = r1.getLayout()
                    r4 = 4
                    int r1 = r1.getLineVisibleEnd(r4)
                    android.widget.TextView r4 = r0.$caption$inlined
                    r4.setMaxLines(r3)
                    org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment r3 = r0.this$0
                    r4 = 2131953945(0x7f130919, float:1.9544375E38)
                    java.lang.String r3 = r3.getString(r4)
                    java.lang.String r4 = "getString(R.string.Story…erPageFragment__see_more)"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r3, r4)
                    android.widget.TextView r4 = r0.$caption$inlined
                    android.text.TextPaint r4 = r4.getPaint()
                    float r4 = r4.measureText(r3)
                    int r5 = r3.length()
                L_0x004b:
                    int r6 = r1 - r5
                    if (r6 >= 0) goto L_0x0050
                    goto L_0x0068
                L_0x0050:
                    android.widget.TextView r7 = r0.$caption$inlined
                    android.text.TextPaint r7 = r7.getPaint()
                    java.lang.String r8 = r0.$displayBody$inlined
                    java.lang.CharSequence r8 = r8.subSequence(r6, r1)
                    java.lang.String r8 = r8.toString()
                    float r7 = r7.measureText(r8)
                    int r7 = (r7 > r4 ? 1 : (r7 == r4 ? 0 : -1))
                    if (r7 <= 0) goto L_0x0088
                L_0x0068:
                    android.widget.TextView r1 = r0.$caption$inlined
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = r0.$displayBody$inlined
                    java.lang.String r5 = r5.substring(r2, r6)
                    java.lang.String r6 = "this as java.lang.String…ing(startIndex, endIndex)"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r5, r6)
                    r4.append(r5)
                    r4.append(r3)
                    java.lang.String r3 = r4.toString()
                    r1.setText(r3)
                    goto L_0x008b
                L_0x0088:
                    int r5 = r5 + 1
                    goto L_0x004b
                L_0x008b:
                    android.widget.TextView r1 = r0.$caption$inlined
                    java.lang.CharSequence r1 = r1.getText()
                    int r1 = r1.length()
                    java.lang.String r3 = r0.$displayBody$inlined
                    int r3 = r3.length()
                    if (r1 != r3) goto L_0x00a9
                    android.widget.TextView r1 = r0.$caption$inlined
                    r3 = 0
                    r1.setOnClickListener(r3)
                    android.widget.TextView r1 = r0.$caption$inlined
                    r1.setClickable(r2)
                    goto L_0x00b9
                L_0x00a9:
                    android.widget.TextView r1 = r0.$caption$inlined
                    org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$1$1 r2 = new org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$1$1
                    org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment r3 = r0.this$0
                    android.widget.TextView r4 = r0.$largeCaption$inlined
                    android.view.View r5 = r0.$largeCaptionOverlay$inlined
                    r2.<init>(r3, r1, r4, r5)
                    r1.setOnClickListener(r2)
                L_0x00b9:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$presentCaption$$inlined$doOnNextLayout$1.onLayoutChange(android.view.View, int, int, int, int, int, int, int, int):void");
            }
        });
    }

    public final void onShowCaptionOverlay(TextView textView, TextView textView2, View view) {
        ViewExtensionsKt.setVisible(textView, false);
        ViewExtensionsKt.setVisible(textView2, true);
        ViewExtensionsKt.setVisible(view, true);
        view.setOnClickListener(new View.OnClickListener(textView, textView2, view) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$$ExternalSyntheticLambda15
            public final /* synthetic */ TextView f$1;
            public final /* synthetic */ TextView f$2;
            public final /* synthetic */ View f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                StoryViewerPageFragment.m3037onShowCaptionOverlay$lambda22(StoryViewerPageFragment.this, this.f$1, this.f$2, this.f$3, view2);
            }
        });
        getViewModel().setIsDisplayingCaptionOverlay(true);
    }

    /* renamed from: onShowCaptionOverlay$lambda-22 */
    public static final void m3037onShowCaptionOverlay$lambda22(StoryViewerPageFragment storyViewerPageFragment, TextView textView, TextView textView2, View view, View view2) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(textView, "$caption");
        Intrinsics.checkNotNullParameter(textView2, "$largeCaption");
        Intrinsics.checkNotNullParameter(view, "$largeCaptionOverlay");
        storyViewerPageFragment.onHideCaptionOverlay(textView, textView2, view);
    }

    private final void onHideCaptionOverlay(TextView textView, TextView textView2, View view) {
        ViewExtensionsKt.setVisible(textView, true);
        ViewExtensionsKt.setVisible(textView2, false);
        ViewExtensionsKt.setVisible(view, false);
        view.setOnClickListener(null);
        getViewModel().setIsDisplayingCaptionOverlay(false);
    }

    private final void presentFrom(TextView textView, StoryPost storyPost) {
        String str;
        if (storyPost.getSender().isSelf()) {
            str = getString(R.string.StoryViewerPageFragment__you);
        } else {
            str = storyPost.getSender().getDisplayName(requireContext());
        }
        Intrinsics.checkNotNullExpressionValue(str, "if (storyPost.sender.isS…e(requireContext())\n    }");
        if (storyPost.getGroup() != null) {
            textView.setText(getString(R.string.StoryViewerPageFragment__s_to_s, str, storyPost.getGroup().getDisplayName(requireContext())));
        } else {
            textView.setText(str);
        }
    }

    private final void presentDate(TextView textView, StoryPost storyPost) {
        textView.setText(DateUtils.getBriefRelativeTimeSpanString(getContext(), Locale.getDefault(), storyPost.getDateInMilliseconds()));
    }

    private final void presentSenderAvatar(AvatarImageView avatarImageView, StoryPost storyPost) {
        AvatarUtil.loadIconIntoImageView(storyPost.getSender(), avatarImageView, (int) DimensionUnit.DP.toPixels(32.0f));
    }

    private final void presentGroupAvatar(AvatarImageView avatarImageView, StoryPost storyPost) {
        if (storyPost.getGroup() != null) {
            avatarImageView.setRecipient(storyPost.getGroup());
            ViewExtensionsKt.setVisible(avatarImageView, true);
            return;
        }
        ViewExtensionsKt.setVisible(avatarImageView, false);
    }

    private final void presentViewsAndReplies(StoryPost storyPost, StoryViewerPageState.ReplyState replyState, boolean z) {
        MaterialButton materialButton = null;
        if (replyState == StoryViewerPageState.ReplyState.NONE) {
            MaterialButton materialButton2 = this.viewsAndReplies;
            if (materialButton2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
            } else {
                materialButton = materialButton2;
            }
            ViewExtensionsKt.setVisible(materialButton, false);
            return;
        }
        MaterialButton materialButton3 = this.viewsAndReplies;
        if (materialButton3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
            materialButton3 = null;
        }
        ViewExtensionsKt.setVisible(materialButton3, true);
        String quantityString = getResources().getQuantityString(R.plurals.StoryViewerFragment__d_views, storyPost.getViewCount(), Integer.valueOf(storyPost.getViewCount()));
        Intrinsics.checkNotNullExpressionValue(quantityString, "resources.getQuantityStr…iewCount, post.viewCount)");
        String quantityString2 = getResources().getQuantityString(R.plurals.StoryViewerFragment__d_replies, storyPost.getReplyCount(), Integer.valueOf(storyPost.getReplyCount()));
        Intrinsics.checkNotNullExpressionValue(quantityString2, "resources.getQuantityStr…lyCount, post.replyCount)");
        if (Intrinsics.areEqual(Recipient.self(), storyPost.getSender())) {
            if (z) {
                if (storyPost.getReplyCount() == 0) {
                    MaterialButton materialButton4 = this.viewsAndReplies;
                    if (materialButton4 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                        materialButton4 = null;
                    }
                    materialButton4.setIconResource(R.drawable.ic_chevron_end_bold_16);
                    MaterialButton materialButton5 = this.viewsAndReplies;
                    if (materialButton5 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                        materialButton5 = null;
                    }
                    materialButton5.setIconSize((int) DimensionUnit.DP.toPixels(16.0f));
                    MaterialButton materialButton6 = this.viewsAndReplies;
                    if (materialButton6 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                        materialButton6 = null;
                    }
                    materialButton6.setIconGravity(4);
                    MaterialButton materialButton7 = this.viewsAndReplies;
                    if (materialButton7 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                    } else {
                        materialButton = materialButton7;
                    }
                    materialButton.setText(quantityString);
                    return;
                }
                MaterialButton materialButton8 = this.viewsAndReplies;
                if (materialButton8 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                    materialButton8 = null;
                }
                materialButton8.setIconResource(R.drawable.ic_chevron_end_bold_16);
                MaterialButton materialButton9 = this.viewsAndReplies;
                if (materialButton9 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                    materialButton9 = null;
                }
                materialButton9.setIconSize((int) DimensionUnit.DP.toPixels(16.0f));
                MaterialButton materialButton10 = this.viewsAndReplies;
                if (materialButton10 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                    materialButton10 = null;
                }
                materialButton10.setIconGravity(4);
                MaterialButton materialButton11 = this.viewsAndReplies;
                if (materialButton11 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                } else {
                    materialButton = materialButton11;
                }
                materialButton.setText(getString(R.string.StoryViewerFragment__s_s, quantityString, quantityString2));
            } else if (storyPost.getReplyCount() == 0) {
                MaterialButton materialButton12 = this.viewsAndReplies;
                if (materialButton12 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                    materialButton12 = null;
                }
                materialButton12.setIcon(null);
                MaterialButton materialButton13 = this.viewsAndReplies;
                if (materialButton13 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                } else {
                    materialButton = materialButton13;
                }
                materialButton.setText(R.string.StoryViewerPageFragment__views_off);
            } else {
                MaterialButton materialButton14 = this.viewsAndReplies;
                if (materialButton14 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                    materialButton14 = null;
                }
                materialButton14.setIconResource(R.drawable.ic_chevron_end_bold_16);
                MaterialButton materialButton15 = this.viewsAndReplies;
                if (materialButton15 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                    materialButton15 = null;
                }
                materialButton15.setIconSize((int) DimensionUnit.DP.toPixels(16.0f));
                MaterialButton materialButton16 = this.viewsAndReplies;
                if (materialButton16 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                    materialButton16 = null;
                }
                materialButton16.setIconGravity(4);
                MaterialButton materialButton17 = this.viewsAndReplies;
                if (materialButton17 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                } else {
                    materialButton = materialButton17;
                }
                materialButton.setText(quantityString2);
            }
        } else if (storyPost.getReplyCount() > 0) {
            MaterialButton materialButton18 = this.viewsAndReplies;
            if (materialButton18 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton18 = null;
            }
            materialButton18.setIconResource(R.drawable.ic_chevron_end_bold_16);
            MaterialButton materialButton19 = this.viewsAndReplies;
            if (materialButton19 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton19 = null;
            }
            materialButton19.setIconSize((int) DimensionUnit.DP.toPixels(16.0f));
            MaterialButton materialButton20 = this.viewsAndReplies;
            if (materialButton20 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton20 = null;
            }
            materialButton20.setIconGravity(4);
            MaterialButton materialButton21 = this.viewsAndReplies;
            if (materialButton21 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
            } else {
                materialButton = materialButton21;
            }
            materialButton.setText(quantityString2);
        } else if (storyPost.getGroup() != null) {
            MaterialButton materialButton22 = this.viewsAndReplies;
            if (materialButton22 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton22 = null;
            }
            materialButton22.setIconResource(R.drawable.ic_reply_24_outline);
            MaterialButton materialButton23 = this.viewsAndReplies;
            if (materialButton23 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton23 = null;
            }
            materialButton23.setIconSize((int) DimensionUnit.DP.toPixels(24.0f));
            MaterialButton materialButton24 = this.viewsAndReplies;
            if (materialButton24 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton24 = null;
            }
            materialButton24.setIconGravity(2);
            MaterialButton materialButton25 = this.viewsAndReplies;
            if (materialButton25 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
            } else {
                materialButton = materialButton25;
            }
            materialButton.setText(R.string.StoryViewerPageFragment__reply_to_group);
        } else {
            MaterialButton materialButton26 = this.viewsAndReplies;
            if (materialButton26 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton26 = null;
            }
            materialButton26.setIconResource(R.drawable.ic_reply_24_outline);
            MaterialButton materialButton27 = this.viewsAndReplies;
            if (materialButton27 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton27 = null;
            }
            materialButton27.setIconSize((int) DimensionUnit.DP.toPixels(24.0f));
            MaterialButton materialButton28 = this.viewsAndReplies;
            if (materialButton28 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
                materialButton28 = null;
            }
            materialButton28.setIconGravity(2);
            MaterialButton materialButton29 = this.viewsAndReplies;
            if (materialButton29 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewsAndReplies");
            } else {
                materialButton = materialButton29;
            }
            materialButton.setText(R.string.StoryViewerPageFragment__reply);
        }
    }

    private final Fragment createFragmentForPost(StoryPost storyPost) {
        StoryPost.Content content = storyPost.getContent();
        if (content instanceof StoryPost.Content.AttachmentContent) {
            return createFragmentForAttachmentContent((StoryPost.Content.AttachmentContent) storyPost.getContent());
        }
        if (content instanceof StoryPost.Content.TextContent) {
            return StoryTextPostPreviewFragment.Companion.create((StoryPost.Content.TextContent) storyPost.getContent());
        }
        throw new NoWhenBranchMatchedException();
    }

    private final Fragment createFragmentForAttachmentContent(StoryPost.Content.AttachmentContent attachmentContent) {
        if (!attachmentContent.isVideo()) {
            return StoryImageContentFragment.Companion.create(attachmentContent.getAttachment());
        }
        MediaPreviewFragment newInstance = MediaPreviewFragment.newInstance(attachmentContent.getAttachment(), false);
        Intrinsics.checkNotNullExpressionValue(newInstance, "{\n      MediaPreviewFrag….attachment, false)\n    }");
        return newInstance;
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostPreviewFragment.Callback
    public void setIsDisplayingLinkPreviewTooltip(boolean z) {
        getViewModel().setIsDisplayingLinkPreviewTooltip(z);
    }

    @Override // org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment.Events
    public VideoControlsDelegate getVideoControlsDelegate() {
        return this.videoControlsDelegate;
    }

    public final void displayMoreContextMenu(View view) {
        getViewModel().setIsDisplayingContextMenu(true);
        StoryContextMenu storyContextMenu = StoryContextMenu.INSTANCE;
        Context requireContext = requireContext();
        StoryViewerPageState stateSnapshot = getViewModel().getStateSnapshot();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        storyContextMenu.show(requireContext, view, stateSnapshot, new StoryViewerPageFragment$displayMoreContextMenu$1(this), new StoryViewerPageFragment$displayMoreContextMenu$2(this), new Function1<StoryPost, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$displayMoreContextMenu$3
            final /* synthetic */ StoryViewerPageFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(StoryPost storyPost) {
                invoke(storyPost);
                return Unit.INSTANCE;
            }

            public final void invoke(StoryPost storyPost) {
                Intrinsics.checkNotNullParameter(storyPost, "it");
                StoryContextMenu.INSTANCE.share(this.this$0, (MediaMmsMessageRecord) storyPost.getConversationMessage().getMessageRecord());
            }
        }, new Function1<StoryPost, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$displayMoreContextMenu$4
            final /* synthetic */ StoryViewerPageFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(StoryPost storyPost) {
                invoke(storyPost);
                return Unit.INSTANCE;
            }

            public final void invoke(StoryPost storyPost) {
                Intrinsics.checkNotNullParameter(storyPost, "it");
                StoryViewerPageFragment storyViewerPageFragment = this.this$0;
                storyViewerPageFragment.startActivity(ConversationIntents.createBuilder(storyViewerPageFragment.requireContext(), this.this$0.getStoryRecipientId(), -1).build());
            }
        }, new Function1<StoryPost, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$displayMoreContextMenu$5
            final /* synthetic */ StoryViewerPageFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(StoryPost storyPost) {
                invoke(storyPost);
                return Unit.INSTANCE;
            }

            public final void invoke(StoryPost storyPost) {
                Intrinsics.checkNotNullParameter(storyPost, "it");
                StoryContextMenu storyContextMenu2 = StoryContextMenu.INSTANCE;
                Context requireContext2 = this.this$0.requireContext();
                Intrinsics.checkNotNullExpressionValue(requireContext2, "requireContext()");
                MessageRecord messageRecord = storyPost.getConversationMessage().getMessageRecord();
                Intrinsics.checkNotNullExpressionValue(messageRecord, "it.conversationMessage.messageRecord");
                storyContextMenu2.save(requireContext2, messageRecord);
            }
        }, new StoryViewerPageFragment$displayMoreContextMenu$6(this), new Function1<StoryPost, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$displayMoreContextMenu$7
            final /* synthetic */ StoryViewerPageFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(StoryPost storyPost) {
                invoke(storyPost);
                return Unit.INSTANCE;
            }

            public final void invoke(StoryPost storyPost) {
                Intrinsics.checkNotNullParameter(storyPost, "it");
                this.this$0.showInfo(storyPost);
            }
        }, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$displayMoreContextMenu$8
            final /* synthetic */ StoryViewerPageFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                this.this$0.getViewModel().setIsDisplayingContextMenu(false);
            }
        });
    }

    /* compiled from: StoryViewerPageFragment.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J>\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\f2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u001e\u001a\u00020\u001a2\u0006\u0010\u001f\u001a\u00020\u001aR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fXD¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\fXD¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\n \u0013*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageFragment$Companion;", "", "()V", "ARG_GROUP_REPLY_START_POSITION", "", "ARG_IS_FROM_INFO_CONTEXT_MENU_ACTION", "ARG_IS_FROM_NOTIFICATION", "ARG_IS_OUTGOING_ONLY", "ARG_IS_UNVIEWED_ONLY", "ARG_STORY_ID", "ARG_STORY_RECIPIENT_ID", "CHARACTERS_PER_SECOND", "", "DEFAULT_DURATION", "MAX_VIDEO_PLAYBACK_DURATION", "MIN_GIF_LOOPS", "MIN_GIF_PLAYBACK_DURATION", "MIN_TEXT_STORY_PLAYBACK", "TAG", "kotlin.jvm.PlatformType", "create", "Landroidx/fragment/app/Fragment;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "initialStoryId", "isFromNotification", "", "groupReplyStartPosition", "", "isUnviewedOnly", "isOutgoingOnly", "isFromInfoContextMenuAction", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Fragment create(RecipientId recipientId, long j, boolean z, int i, boolean z2, boolean z3, boolean z4) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            StoryViewerPageFragment storyViewerPageFragment = new StoryViewerPageFragment();
            storyViewerPageFragment.setArguments(BundleKt.bundleOf(TuplesKt.to(StoryViewerPageFragment.ARG_STORY_RECIPIENT_ID, recipientId), TuplesKt.to(StoryViewerPageFragment.ARG_STORY_ID, Long.valueOf(j)), TuplesKt.to(StoryViewerPageFragment.ARG_IS_FROM_NOTIFICATION, Boolean.valueOf(z)), TuplesKt.to(StoryViewerPageFragment.ARG_GROUP_REPLY_START_POSITION, Integer.valueOf(i)), TuplesKt.to(StoryViewerPageFragment.ARG_IS_UNVIEWED_ONLY, Boolean.valueOf(z2)), TuplesKt.to(StoryViewerPageFragment.ARG_IS_OUTGOING_ONLY, Boolean.valueOf(z3)), TuplesKt.to(StoryViewerPageFragment.ARG_IS_FROM_INFO_CONTEXT_MENU_ACTION, Boolean.valueOf(z4))));
            return storyViewerPageFragment;
        }
    }

    static {
        Companion = new Companion(null);
        TAG = Log.tag(StoryViewerPageFragment.class);
        TimeUnit timeUnit = TimeUnit.SECONDS;
        MAX_VIDEO_PLAYBACK_DURATION = timeUnit.toMillis(30);
        MIN_GIF_LOOPS = 3;
        MIN_GIF_PLAYBACK_DURATION = timeUnit.toMillis(5);
        MIN_TEXT_STORY_PLAYBACK = timeUnit.toMillis(3);
        CHARACTERS_PER_SECOND = 15;
        DEFAULT_DURATION = timeUnit.toMillis(5);
    }

    /* compiled from: StoryViewerPageFragment.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\b\u0002\u0018\u0000  2\u00020\u0001:\u0001 BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\b\u0002\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\b\u0010\u000f\u001a\u00020\u000eH\u0002J\b\u0010\u0010\u001a\u00020\u000eH\u0002J\u0012\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016J,\u0010\u0015\u001a\u00020\u00122\b\u0010\u0016\u001a\u0004\u0018\u00010\u00142\b\u0010\u0017\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u0019\u001a\u00020\u000eH\u0016J(\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u000e2\u0006\u0010\u001c\u001a\u00020\u000eH\u0016J\u0010\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\b\u0010\u001e\u001a\u00020\u0006H\u0002J\b\u0010\u001f\u001a\u00020\u0006H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageFragment$StoryGestureListener;", "Landroid/view/GestureDetector$SimpleOnGestureListener;", "container", "Landroid/view/View;", "onGoToNext", "Lkotlin/Function0;", "", "onGoToPrevious", "onReplyToPost", "viewToTranslate", "sharedViewModel", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerViewModel;", "(Landroid/view/View;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Landroid/view/View;Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerViewModel;)V", "maxSlide", "", "getLeftBoundary", "getRightBoundary", "onDown", "", "e", "Landroid/view/MotionEvent;", "onFling", "e1", "e2", "velocityX", "velocityY", "onScroll", "distanceX", "distanceY", "onSingleTapUp", "performLeftAction", "performRightAction", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class StoryGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final float BOUNDARY_NEXT;
        private static final float BOUNDARY_PREV;
        public static final Companion Companion = new Companion(null);
        private static final Interpolator INTERPOLATOR;
        private final View container;
        private final float maxSlide;
        private final Function0<Unit> onGoToNext;
        private final Function0<Unit> onGoToPrevious;
        private final Function0<Unit> onReplyToPost;
        private final StoryViewerViewModel sharedViewModel;
        private final View viewToTranslate;

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ StoryGestureListener(android.view.View r8, kotlin.jvm.functions.Function0 r9, kotlin.jvm.functions.Function0 r10, kotlin.jvm.functions.Function0 r11, android.view.View r12, org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel r13, int r14, kotlin.jvm.internal.DefaultConstructorMarker r15) {
            /*
                r7 = this;
                r14 = r14 & 16
                if (r14 == 0) goto L_0x0015
                android.view.ViewParent r12 = r8.getParent()
                if (r12 == 0) goto L_0x000d
                android.view.View r12 = (android.view.View) r12
                goto L_0x0015
            L_0x000d:
                java.lang.NullPointerException r8 = new java.lang.NullPointerException
                java.lang.String r9 = "null cannot be cast to non-null type android.view.View"
                r8.<init>(r9)
                throw r8
            L_0x0015:
                r5 = r12
                r0 = r7
                r1 = r8
                r2 = r9
                r3 = r10
                r4 = r11
                r6 = r13
                r0.<init>(r1, r2, r3, r4, r5, r6)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment.StoryGestureListener.<init>(android.view.View, kotlin.jvm.functions.Function0, kotlin.jvm.functions.Function0, kotlin.jvm.functions.Function0, android.view.View, org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        public StoryGestureListener(View view, Function0<Unit> function0, Function0<Unit> function02, Function0<Unit> function03, View view2, StoryViewerViewModel storyViewerViewModel) {
            Intrinsics.checkNotNullParameter(view, "container");
            Intrinsics.checkNotNullParameter(function0, "onGoToNext");
            Intrinsics.checkNotNullParameter(function02, "onGoToPrevious");
            Intrinsics.checkNotNullParameter(function03, "onReplyToPost");
            Intrinsics.checkNotNullParameter(view2, "viewToTranslate");
            Intrinsics.checkNotNullParameter(storyViewerViewModel, "sharedViewModel");
            this.container = view;
            this.onGoToNext = function0;
            this.onGoToPrevious = function02;
            this.onReplyToPost = function03;
            this.viewToTranslate = view2;
            this.sharedViewModel = storyViewerViewModel;
            this.maxSlide = DimensionUnit.DP.toPixels(112.0f);
        }

        /* compiled from: StoryViewerPageFragment.kt */
        @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageFragment$StoryGestureListener$Companion;", "", "()V", "BOUNDARY_NEXT", "", "BOUNDARY_PREV", "INTERPOLATOR", "Landroid/view/animation/Interpolator;", "getINTERPOLATOR", "()Landroid/view/animation/Interpolator;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final Interpolator getINTERPOLATOR() {
                return StoryGestureListener.INTERPOLATOR;
            }
        }

        static {
            Companion = new Companion(null);
            Interpolator create = PathInterpolatorCompat.create(0.4f, 0.0f, 0.2f, 1.0f);
            Intrinsics.checkNotNullExpressionValue(create, "create(0.4f, 0f, 0.2f, 1f)");
            INTERPOLATOR = create;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            Intrinsics.checkNotNullParameter(motionEvent, "e1");
            Intrinsics.checkNotNullParameter(motionEvent2, "e2");
            boolean z = false;
            boolean z2 = this.sharedViewModel.getStateSnapshot().getPage() == 0;
            boolean z3 = CollectionsKt__CollectionsKt.getLastIndex(this.sharedViewModel.getStateSnapshot().getPages()) == this.sharedViewModel.getStateSnapshot().getPage();
            boolean z4 = Math.abs(f) > Math.abs(f2) || this.viewToTranslate.getTranslationX() > 0.0f;
            boolean z5 = z2 && (this.viewToTranslate.getTranslationY() > 0.0f || f2 < 0.0f);
            boolean z6 = z3 && (this.viewToTranslate.getTranslationY() < 0.0f || f2 > 0.0f);
            StoryViewerViewModel storyViewerViewModel = this.sharedViewModel;
            if (z4 || z5 || z6) {
                z = true;
            }
            storyViewerViewModel.setIsChildScrolling(z);
            if (z2) {
                float interpolation = this.maxSlide * INTERPOLATOR.getInterpolation((Math.max(0.0f, motionEvent2.getRawY() - motionEvent.getRawY()) / 3.0f) / this.maxSlide);
                this.viewToTranslate.animate().cancel();
                this.viewToTranslate.setTranslationY(interpolation);
            }
            if (z3) {
                float f3 = this.maxSlide * (-INTERPOLATOR.getInterpolation((Math.max(0.0f, motionEvent.getRawY() - motionEvent2.getRawY()) / 3.0f) / this.maxSlide));
                this.viewToTranslate.animate().cancel();
                this.viewToTranslate.setTranslationY(f3);
            }
            float interpolation2 = this.maxSlide * INTERPOLATOR.getInterpolation((Math.max(0.0f, motionEvent2.getRawX() - motionEvent.getRawX()) / 3.0f) / this.maxSlide);
            this.viewToTranslate.animate().cancel();
            this.viewToTranslate.setTranslationX(interpolation2);
            return true;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            if (!(Math.abs(f) > Math.abs(f2))) {
                return false;
            }
            if (this.viewToTranslate.getTranslationX() == 0.0f) {
                if (this.viewToTranslate.getTranslationY() == 0.0f) {
                    if (ViewUtil.isLtr(this.container)) {
                        if (f < 0.0f) {
                            this.onReplyToPost.invoke();
                        }
                    } else if (f > 0.0f) {
                        this.onReplyToPost.invoke();
                    }
                    return true;
                }
            }
            return false;
        }

        private final float getLeftBoundary() {
            return this.container.getLayoutDirection() == 0 ? BOUNDARY_PREV : BOUNDARY_NEXT;
        }

        private final float getRightBoundary() {
            return this.container.getLayoutDirection() == 0 ? BOUNDARY_NEXT : BOUNDARY_PREV;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            Intrinsics.checkNotNullParameter(motionEvent, "e");
            if (motionEvent.getX() < ((float) this.container.getMeasuredWidth()) * getLeftBoundary()) {
                performLeftAction();
                return true;
            } else if (motionEvent.getX() <= ((float) this.container.getMeasuredWidth()) - (((float) this.container.getMeasuredWidth()) * getRightBoundary())) {
                return false;
            } else {
                performRightAction();
                return true;
            }
        }

        private final void performLeftAction() {
            if (this.container.getLayoutDirection() == 1) {
                this.onGoToNext.invoke();
            } else {
                this.onGoToPrevious.invoke();
            }
        }

        private final void performRightAction() {
            if (this.container.getLayoutDirection() == 1) {
                this.onGoToPrevious.invoke();
            } else {
                this.onGoToNext.invoke();
            }
        }
    }

    /* compiled from: StoryViewerPageFragment.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0004H\u0016J\u0018\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\u0004H\u0016J\b\u0010\f\u001a\u00020\u0004H\u0016¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageFragment$FallbackPhotoProvider;", "Lorg/thoughtcrime/securesms/recipients/Recipient$FallbackPhotoProvider;", "()V", "getPhotoForGroup", "Lorg/thoughtcrime/securesms/contacts/avatars/FallbackContactPhoto;", "getPhotoForLocalNumber", "getPhotoForRecipientWithName", "name", "", "targetSize", "", "getPhotoForRecipientWithoutName", "getPhotoForResolvingRecipient", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    private static final class FallbackPhotoProvider extends Recipient.FallbackPhotoProvider {
        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForGroup() {
            return new FallbackPhoto20dp(R.drawable.ic_group_outline_20);
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForResolvingRecipient() {
            throw new UnsupportedOperationException("This provider does not support resolving recipients");
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForLocalNumber() {
            throw new UnsupportedOperationException("This provider does not support local number");
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForRecipientWithName(String str, int i) {
            Intrinsics.checkNotNullParameter(str, "name");
            return new FixedSizeGeneratedContactPhoto(str, R.drawable.ic_profile_outline_20);
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForRecipientWithoutName() {
            return new FallbackPhoto20dp(R.drawable.ic_profile_outline_20);
        }
    }

    /* compiled from: StoryViewerPageFragment.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J \u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0014¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageFragment$FixedSizeGeneratedContactPhoto;", "Lorg/thoughtcrime/securesms/contacts/avatars/GeneratedContactPhoto;", "name", "", "fallbackResId", "", "(Ljava/lang/String;I)V", "newFallbackDrawable", "Landroid/graphics/drawable/Drawable;", "context", "Landroid/content/Context;", NotificationProfileDatabase.NotificationProfileTable.COLOR, "Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "inverted", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    private static final class FixedSizeGeneratedContactPhoto extends GeneratedContactPhoto {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public FixedSizeGeneratedContactPhoto(String str, int i) {
            super(str, i);
            Intrinsics.checkNotNullParameter(str, "name");
        }

        @Override // org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto
        protected Drawable newFallbackDrawable(Context context, AvatarColor avatarColor, boolean z) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(avatarColor, NotificationProfileDatabase.NotificationProfileTable.COLOR);
            Drawable asDrawable = new FallbackPhoto20dp(getFallbackResId()).asDrawable(context, avatarColor, z);
            Intrinsics.checkNotNullExpressionValue(asDrawable, "FallbackPhoto20dp(fallba…context, color, inverted)");
            return asDrawable;
        }
    }

    @Override // org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment.Events
    public void onMediaReady() {
        getSharedViewModel().setContentIsReady();
    }

    @Override // org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment.Events
    public void mediaNotAvailable() {
        getSharedViewModel().setContentIsReady();
    }

    @Override // org.thoughtcrime.securesms.stories.StoryFirstTimeNavigationView.Callback
    public boolean userHasSeenFirstNavigationView() {
        return SignalStore.storyValues().getUserHasSeenFirstNavView();
    }

    @Override // org.thoughtcrime.securesms.stories.StoryFirstTimeNavigationView.Callback
    public void onGotItClicked() {
        SignalStore.storyValues().setUserHasSeenFirstNavView(true);
        getViewModel().setIsDisplayingFirstTimeNavigation(false);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.info.StoryInfoBottomSheetDialogFragment.OnInfoSheetDismissedListener
    public void onInfoSheetDismissed() {
        getViewModel().setIsDisplayingInfoDialog(false);
    }
}
