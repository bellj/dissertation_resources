package org.thoughtcrime.securesms.stories;

import android.net.Uri;
import java.util.concurrent.CountDownLatch;
import kotlin.jvm.internal.Ref$LongRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class Stories$MediaTransform$$ExternalSyntheticLambda4 implements Runnable {
    public final /* synthetic */ CountDownLatch f$0;
    public final /* synthetic */ Uri f$1;
    public final /* synthetic */ Ref$ObjectRef f$2;
    public final /* synthetic */ Ref$LongRef f$3;

    public /* synthetic */ Stories$MediaTransform$$ExternalSyntheticLambda4(CountDownLatch countDownLatch, Uri uri, Ref$ObjectRef ref$ObjectRef, Ref$LongRef ref$LongRef) {
        this.f$0 = countDownLatch;
        this.f$1 = uri;
        this.f$2 = ref$ObjectRef;
        this.f$3 = ref$LongRef;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Stories.MediaTransform.m2764getVideoDuration$lambda4(this.f$0, this.f$1, this.f$2, this.f$3);
    }
}
