package org.thoughtcrime.securesms.stories.my;

import org.thoughtcrime.securesms.stories.my.MyStoriesItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class MyStoriesItem$ViewHolder$$ExternalSyntheticLambda4 implements Runnable {
    public final /* synthetic */ MyStoriesItem.Model f$0;
    public final /* synthetic */ MyStoriesItem.ViewHolder f$1;

    public /* synthetic */ MyStoriesItem$ViewHolder$$ExternalSyntheticLambda4(MyStoriesItem.Model model, MyStoriesItem.ViewHolder viewHolder) {
        this.f$0 = model;
        this.f$1 = viewHolder;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MyStoriesItem.ViewHolder.m2844showContextMenu$lambda10(this.f$0, this.f$1);
    }
}
