package org.thoughtcrime.securesms.stories;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import j$.util.function.Function;
import j$.util.function.Supplier;
import java.text.SimpleDateFormat;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.components.OutlinedThumbnailView;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewUtil;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.ImageSlide;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;
import org.thoughtcrime.securesms.util.views.Stub;

/* compiled from: StoryLinkPreviewView.kt */
@Metadata(d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J \u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\b\u0010\u0016\u001a\u0004\u0018\u00010\u00172\b\b\u0002\u0010\u0018\u001a\u00020\u0019J\u0018\u0010\u0013\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\b\b\u0002\u0010\u0018\u001a\u00020\u0019J\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010!\u001a\u00020\u001a2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u000e\u0010\"\u001a\u00020\u001a2\u0006\u0010#\u001a\u00020\u0015J\u0010\u0010$\u001a\u00020\u001a2\b\u0010%\u001a\u0004\u0018\u00010&R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006'"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryLinkPreviewView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "close", "Landroid/view/View;", "description", "Landroid/widget/TextView;", "fallbackIcon", "Landroid/widget/ImageView;", DraftDatabase.Draft.IMAGE, "Lorg/thoughtcrime/securesms/components/OutlinedThumbnailView;", "loadingSpinner", "Lorg/thoughtcrime/securesms/util/views/Stub;", MultiselectForwardFragment.DIALOG_TITLE, "url", "bind", "Lorg/thoughtcrime/securesms/util/concurrent/ListenableFuture;", "", "linkPreview", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;", "hiddenVisibility", "", "", "linkPreviewState", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreviewViewModel$LinkPreviewState;", "formatDate", "", "date", "", "formatUrl", "setCanClose", "canClose", "setOnCloseClickListener", "onClickListener", "Landroid/view/View$OnClickListener;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryLinkPreviewView extends ConstraintLayout {
    private final View close;
    private final TextView description;
    private final ImageView fallbackIcon;
    private final OutlinedThumbnailView image;
    private final Stub<View> loadingSpinner;
    private final TextView title;
    private final TextView url;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryLinkPreviewView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ StoryLinkPreviewView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryLinkPreviewView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        ViewGroup.inflate(context, R.layout.stories_text_post_link_preview, this);
        View findViewById = findViewById(R.id.link_preview_close);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.link_preview_close)");
        this.close = findViewById;
        View findViewById2 = findViewById(R.id.link_preview_image);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.link_preview_image)");
        this.image = (OutlinedThumbnailView) findViewById2;
        View findViewById3 = findViewById(R.id.link_preview_title);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.link_preview_title)");
        this.title = (TextView) findViewById3;
        View findViewById4 = findViewById(R.id.link_preview_url);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.link_preview_url)");
        this.url = (TextView) findViewById4;
        View findViewById5 = findViewById(R.id.link_preview_description);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "findViewById(R.id.link_preview_description)");
        this.description = (TextView) findViewById5;
        View findViewById6 = findViewById(R.id.link_preview_fallback_icon);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "findViewById(R.id.link_preview_fallback_icon)");
        this.fallbackIcon = (ImageView) findViewById6;
        this.loadingSpinner = new Stub<>((ViewStub) findViewById(R.id.loading_spinner));
    }

    public static /* synthetic */ ListenableFuture bind$default(StoryLinkPreviewView storyLinkPreviewView, LinkPreview linkPreview, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 4;
        }
        return storyLinkPreviewView.bind(linkPreview, i);
    }

    public final ListenableFuture<Boolean> bind(LinkPreview linkPreview, int i) {
        ListenableFuture<Boolean> listenableFuture = null;
        boolean z = false;
        if (linkPreview != null) {
            setVisibility(0);
            setClickable(true);
            int pixels = (int) DimensionUnit.DP.toPixels(18.0f);
            this.image.setCorners(pixels, pixels, pixels, pixels);
            ImageSlide imageSlide = (ImageSlide) linkPreview.getThumbnail().map(new Function() { // from class: org.thoughtcrime.securesms.stories.StoryLinkPreviewView$$ExternalSyntheticLambda1
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return StoryLinkPreviewView.m2769bind$lambda0(StoryLinkPreviewView.this, (Attachment) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).orElse(null);
            if (imageSlide != null) {
                OutlinedThumbnailView outlinedThumbnailView = this.image;
                listenableFuture = outlinedThumbnailView.setImageResource(GlideApp.with(outlinedThumbnailView), (Slide) imageSlide, false, false);
                ViewExtensionsKt.setVisible(this.image, true);
                ViewExtensionsKt.setVisible(this.fallbackIcon, false);
            } else {
                ViewExtensionsKt.setVisible(this.image, false);
                ViewExtensionsKt.setVisible(this.fallbackIcon, true);
            }
            this.title.setText(linkPreview.getTitle());
            this.description.setText(linkPreview.getDescription());
            TextView textView = this.description;
            String description = linkPreview.getDescription();
            Intrinsics.checkNotNullExpressionValue(description, "linkPreview.description");
            if (description.length() > 0) {
                z = true;
            }
            ViewExtensionsKt.setVisible(textView, z);
            formatUrl(linkPreview);
        } else {
            setVisibility(i);
            setClickable(false);
        }
        return listenableFuture == null ? new SettableFuture(Boolean.FALSE) : listenableFuture;
    }

    /* renamed from: bind$lambda-0 */
    public static final ImageSlide m2769bind$lambda0(StoryLinkPreviewView storyLinkPreviewView, Attachment attachment) {
        Intrinsics.checkNotNullParameter(storyLinkPreviewView, "this$0");
        return new ImageSlide(storyLinkPreviewView.image.getContext(), attachment);
    }

    public static /* synthetic */ void bind$default(StoryLinkPreviewView storyLinkPreviewView, LinkPreviewViewModel.LinkPreviewState linkPreviewState, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 4;
        }
        storyLinkPreviewView.bind(linkPreviewState, i);
    }

    public final void bind(LinkPreviewViewModel.LinkPreviewState linkPreviewState, int i) {
        Intrinsics.checkNotNullParameter(linkPreviewState, "linkPreviewState");
        bind(linkPreviewState.getLinkPreview().orElseGet(new Supplier() { // from class: org.thoughtcrime.securesms.stories.StoryLinkPreviewView$$ExternalSyntheticLambda0
            @Override // j$.util.function.Supplier
            public final Object get() {
                return StoryLinkPreviewView.m2770bind$lambda2(LinkPreviewViewModel.LinkPreviewState.this);
            }
        }), i);
        View view = this.loadingSpinner.get();
        Intrinsics.checkNotNullExpressionValue(view, "loadingSpinner.get()");
        ViewExtensionsKt.setVisible(view, linkPreviewState.isLoading());
        if (linkPreviewState.isLoading()) {
            ViewExtensionsKt.setVisible(this, true);
        }
    }

    /* renamed from: bind$lambda-2 */
    public static final LinkPreview m2770bind$lambda2(LinkPreviewViewModel.LinkPreviewState linkPreviewState) {
        Intrinsics.checkNotNullParameter(linkPreviewState, "$linkPreviewState");
        String activeUrlForError = linkPreviewState.getActiveUrlForError();
        if (activeUrlForError == null) {
            return null;
        }
        String topLevelDomain = LinkPreviewUtil.getTopLevelDomain(activeUrlForError);
        return new LinkPreview(activeUrlForError, topLevelDomain == null ? activeUrlForError : topLevelDomain, (String) null, -1, (AttachmentId) null);
    }

    private final void formatUrl(LinkPreview linkPreview) {
        String topLevelDomain = LinkPreviewUtil.getTopLevelDomain(linkPreview.getUrl());
        if (Intrinsics.areEqual(linkPreview.getTitle(), topLevelDomain)) {
            this.url.setVisibility(8);
        } else if (topLevelDomain != null && linkPreview.getDate() > 0) {
            TextView textView = this.url;
            textView.setText(textView.getContext().getString(R.string.LinkPreviewView_domain_date, topLevelDomain, formatDate(linkPreview.getDate())));
            this.url.setVisibility(0);
        } else if (topLevelDomain != null) {
            this.url.setText(topLevelDomain);
            this.url.setVisibility(0);
        } else if (linkPreview.getDate() > 0) {
            this.url.setText(formatDate(linkPreview.getDate()));
            this.url.setVisibility(0);
        } else {
            this.url.setVisibility(8);
        }
    }

    public final void setOnCloseClickListener(View.OnClickListener onClickListener) {
        this.close.setOnClickListener(onClickListener);
    }

    public final void setCanClose(boolean z) {
        ViewExtensionsKt.setVisible(this.close, z);
    }

    private final String formatDate(long j) {
        return new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()).format(Long.valueOf(j));
    }
}
