package org.thoughtcrime.securesms.stories.viewer.info;

import android.view.View;
import android.widget.TextView;
import j$.util.function.Function;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.stories.viewer.info.StoryInfoHeader;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: StoryInfoHeader.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoHeader;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryInfoHeader {
    public static final StoryInfoHeader INSTANCE = new StoryInfoHeader();

    private StoryInfoHeader() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoHeader$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new StoryInfoHeader.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.story_info_header));
    }

    /* compiled from: StoryInfoHeader.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016J\u0010\u0010\u000e\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoHeader$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "sentMillis", "", "receivedMillis", MediaPreviewActivity.SIZE_EXTRA, "(JJJ)V", "getReceivedMillis", "()J", "getSentMillis", "getSize", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model implements MappingModel<Model> {
        private final long receivedMillis;
        private final long sentMillis;
        private final long size;

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return true;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        public Model(long j, long j2, long j3) {
            this.sentMillis = j;
            this.receivedMillis = j2;
            this.size = j3;
        }

        public final long getReceivedMillis() {
            return this.receivedMillis;
        }

        public final long getSentMillis() {
            return this.sentMillis;
        }

        public final long getSize() {
            return this.size;
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return model.sentMillis == this.sentMillis && model.receivedMillis == this.receivedMillis && model.size == this.size;
        }
    }

    /* compiled from: StoryInfoHeader.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoHeader$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoHeader$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "recvView", "Landroid/widget/TextView;", "sentView", "sizeView", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final TextView recvView;
        private final TextView sentView;
        private final TextView sizeView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.story_info_view_sent_label);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.…ory_info_view_sent_label)");
            this.sentView = (TextView) findViewById;
            View findViewById2 = view.findViewById(R.id.story_info_view_received_label);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.…info_view_received_label)");
            this.recvView = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.story_info_view_file_size_label);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.…nfo_view_file_size_label)");
            this.sizeView = (TextView) findViewById3;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            if (model.getSentMillis() > 0) {
                ViewExtensionsKt.setVisible(this.sentView, true);
                this.sentView.setText(DateUtils.getTimeString(this.context, Locale.getDefault(), model.getSentMillis()));
            } else {
                ViewExtensionsKt.setVisible(this.sentView, false);
            }
            if (model.getReceivedMillis() > 0) {
                ViewExtensionsKt.setVisible(this.recvView, true);
                this.recvView.setText(DateUtils.getTimeString(this.context, Locale.getDefault(), model.getReceivedMillis()));
            } else {
                ViewExtensionsKt.setVisible(this.recvView, false);
            }
            if (model.getSize() > 0) {
                ViewExtensionsKt.setVisible(this.sizeView, true);
                this.sizeView.setText(Util.getPrettyFileSize(model.getSize()));
                return;
            }
            ViewExtensionsKt.setVisible(this.sizeView, false);
        }
    }
}
