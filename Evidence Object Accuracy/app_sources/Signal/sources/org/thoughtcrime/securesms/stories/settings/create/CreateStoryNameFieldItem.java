package org.thoughtcrime.securesms.stories.settings.create;

import android.view.View;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputLayout;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.EditTextUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: CreateStoryNameFieldItem.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00040\b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryNameFieldItem;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "onTextChanged", "Lkotlin/Function1;", "", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class CreateStoryNameFieldItem {
    public static final CreateStoryNameFieldItem INSTANCE = new CreateStoryNameFieldItem();

    private CreateStoryNameFieldItem() {
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m2859register$lambda0(Function1 function1, View view) {
        Intrinsics.checkNotNullParameter(function1, "$onTextChanged");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view, function1);
    }

    public final void register(MappingAdapter mappingAdapter, Function1<? super CharSequence, Unit> function1) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        Intrinsics.checkNotNullParameter(function1, "onTextChanged");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryNameFieldItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return CreateStoryNameFieldItem.m2858$r8$lambda$PMqM4HJkXwy2IkBiy7xIvLcZSk(Function1.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_create_story_name_field_item));
    }

    /* compiled from: CreateStoryNameFieldItem.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0005J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0000H\u0016J\u0010\u0010\f\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryNameFieldItem$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "body", "", "error", "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V", "getBody", "()Ljava/lang/CharSequence;", "getError", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final CharSequence body;
        private final CharSequence error;

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return true;
        }

        public final CharSequence getBody() {
            return this.body;
        }

        public final CharSequence getError() {
            return this.error;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(CharSequence charSequence, CharSequence charSequence2) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(charSequence, "body");
            this.body = charSequence;
            this.error = charSequence2;
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && Intrinsics.areEqual(this.body, model.body) && Intrinsics.areEqual(this.error, model.error);
        }
    }

    /* compiled from: CreateStoryNameFieldItem.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B!\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0002\u0010\tJ\u0010\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryNameFieldItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryNameFieldItem$Model;", "itemView", "Landroid/view/View;", "onTextChanged", "Lkotlin/Function1;", "", "", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "editText", "Landroid/widget/EditText;", "editTextWrapper", "Lcom/google/android/material/textfield/TextInputLayout;", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final EditText editText;
        private final TextInputLayout editTextWrapper;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view, Function1<? super CharSequence, Unit> function1) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function1, "onTextChanged");
            View findViewById = view.findViewById(R.id.edit_text_wrapper);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.edit_text_wrapper)");
            this.editTextWrapper = (TextInputLayout) findViewById;
            View findViewById2 = view.findViewById(R.id.edit_text);
            EditText editText = (EditText) findViewById2;
            EditTextUtil.addGraphemeClusterLimitFilter(editText, 23);
            Intrinsics.checkNotNullExpressionValue(editText, "");
            editText.addTextChangedListener(new CreateStoryNameFieldItem$ViewHolder$editText$lambda1$$inlined$doAfterTextChanged$1(function1));
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById<Ed…)\n        }\n      }\n    }");
            this.editText = editText;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            if (!Intrinsics.areEqual(model.getBody(), this.editText.getText())) {
                this.editText.setText(model.getBody());
            }
            this.editTextWrapper.setError(model.getError());
        }
    }
}
