package org.thoughtcrime.securesms.stories.settings.my;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode;

/* compiled from: MyStoryPrivacyState.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001f\u0010\r\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/my/MyStoryPrivacyState;", "", "privacyMode", "Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "connectionCount", "", "(Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;I)V", "getConnectionCount", "()I", "getPrivacyMode", "()Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStoryPrivacyState {
    private final int connectionCount;
    private final DistributionListPrivacyMode privacyMode;

    public MyStoryPrivacyState() {
        this(null, 0, 3, null);
    }

    public static /* synthetic */ MyStoryPrivacyState copy$default(MyStoryPrivacyState myStoryPrivacyState, DistributionListPrivacyMode distributionListPrivacyMode, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            distributionListPrivacyMode = myStoryPrivacyState.privacyMode;
        }
        if ((i2 & 2) != 0) {
            i = myStoryPrivacyState.connectionCount;
        }
        return myStoryPrivacyState.copy(distributionListPrivacyMode, i);
    }

    public final DistributionListPrivacyMode component1() {
        return this.privacyMode;
    }

    public final int component2() {
        return this.connectionCount;
    }

    public final MyStoryPrivacyState copy(DistributionListPrivacyMode distributionListPrivacyMode, int i) {
        return new MyStoryPrivacyState(distributionListPrivacyMode, i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MyStoryPrivacyState)) {
            return false;
        }
        MyStoryPrivacyState myStoryPrivacyState = (MyStoryPrivacyState) obj;
        return this.privacyMode == myStoryPrivacyState.privacyMode && this.connectionCount == myStoryPrivacyState.connectionCount;
    }

    public int hashCode() {
        DistributionListPrivacyMode distributionListPrivacyMode = this.privacyMode;
        return ((distributionListPrivacyMode == null ? 0 : distributionListPrivacyMode.hashCode()) * 31) + this.connectionCount;
    }

    public String toString() {
        return "MyStoryPrivacyState(privacyMode=" + this.privacyMode + ", connectionCount=" + this.connectionCount + ')';
    }

    public MyStoryPrivacyState(DistributionListPrivacyMode distributionListPrivacyMode, int i) {
        this.privacyMode = distributionListPrivacyMode;
        this.connectionCount = i;
    }

    public /* synthetic */ MyStoryPrivacyState(DistributionListPrivacyMode distributionListPrivacyMode, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? null : distributionListPrivacyMode, (i2 & 2) != 0 ? 0 : i);
    }

    public final int getConnectionCount() {
        return this.connectionCount;
    }

    public final DistributionListPrivacyMode getPrivacyMode() {
        return this.privacyMode;
    }
}
