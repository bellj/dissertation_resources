package org.thoughtcrime.securesms.stories.viewer.views;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerChild;
import org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent;
import org.thoughtcrime.securesms.stories.viewer.views.StoryViewItem;
import org.thoughtcrime.securesms.stories.viewer.views.StoryViewsState;
import org.thoughtcrime.securesms.stories.viewer.views.StoryViewsViewModel;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: StoryViewsFragment.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u001a2\u00020\u00012\u00020\u0002:\u0001\u001aB\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\b\u0010\u0019\u001a\u00020\u000fH\u0016R\u0014\u0010\u0004\u001a\u00020\u00058BX\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u001b\u0010\b\u001a\u00020\t8BX\u0002¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000b¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerChild;", "()V", "storyId", "", "getStoryId", "()J", "viewModel", "Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsState;", "onPageSelected", "child", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent$Child;", "onResume", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewsFragment extends DSLSettingsFragment implements StoryViewsAndRepliesPagerChild {
    private static final String ARG_STORY_ID;
    public static final Companion Companion = new Companion(null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryViewsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsFragment$viewModel$2
        final /* synthetic */ StoryViewsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new StoryViewsViewModel.Factory(this.this$0.getStoryId(), new StoryViewsRepository());
        }
    });

    public StoryViewsFragment() {
        super(0, 0, R.layout.stories_views_fragment, null, 11, null);
    }

    private final StoryViewsViewModel getViewModel() {
        return (StoryViewsViewModel) this.viewModel$delegate.getValue();
    }

    public final long getStoryId() {
        return requireArguments().getLong(ARG_STORY_ID);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:19:0x005d */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v5, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r2v8, types: [org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent] */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARN: Type inference failed for: r2v18 */
    /* JADX WARN: Type inference failed for: r2v19 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void bindAdapter(org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter r6) {
        /*
            r5 = this;
            java.lang.String r0 = "adapter"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r6, r0)
            org.thoughtcrime.securesms.stories.viewer.views.StoryViewItem r0 = org.thoughtcrime.securesms.stories.viewer.views.StoryViewItem.INSTANCE
            r0.register(r6)
            android.view.View r0 = r5.requireView()
            r1 = 2131363057(0x7f0a04f1, float:1.8345912E38)
            android.view.View r0 = r0.findViewById(r1)
            java.lang.String r1 = "requireView().findViewById(R.id.empty_notice)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            android.view.View r1 = r5.requireView()
            r2 = 2131362889(0x7f0a0449, float:1.8345571E38)
            android.view.View r1 = r1.findViewById(r2)
            java.lang.String r2 = "requireView().findViewById(R.id.disabled_notice)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
            android.view.View r2 = r5.requireView()
            r3 = 2131362887(0x7f0a0447, float:1.8345567E38)
            android.view.View r2 = r2.findViewById(r3)
            java.lang.String r3 = "requireView().findViewById(R.id.disabled_button)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r3)
            org.thoughtcrime.securesms.stories.viewer.views.StoryViewsFragment$$ExternalSyntheticLambda0 r3 = new org.thoughtcrime.securesms.stories.viewer.views.StoryViewsFragment$$ExternalSyntheticLambda0
            r3.<init>()
            r2.setOnClickListener(r3)
            androidx.fragment.app.Fragment r2 = r5.getParentFragment()
        L_0x0046:
            if (r2 == 0) goto L_0x0052
            boolean r3 = r2 instanceof org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent
            if (r3 == 0) goto L_0x004d
            goto L_0x005d
        L_0x004d:
            androidx.fragment.app.Fragment r2 = r2.getParentFragment()
            goto L_0x0046
        L_0x0052:
            androidx.fragment.app.FragmentActivity r2 = r5.requireActivity()
            boolean r3 = r2 instanceof org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent
            if (r3 != 0) goto L_0x005b
            r2 = 0
        L_0x005b:
            org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent r2 = (org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent) r2
        L_0x005d:
            org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent r2 = (org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent) r2
            if (r2 == 0) goto L_0x0067
            org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent$Child r2 = r2.getSelectedChild()
            if (r2 != 0) goto L_0x0069
        L_0x0067:
            org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent$Child r2 = org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent.Child.VIEWS
        L_0x0069:
            r5.onPageSelected(r2)
            org.thoughtcrime.securesms.stories.viewer.views.StoryViewsViewModel r2 = r5.getViewModel()
            androidx.lifecycle.LiveData r2 = r2.getState()
            androidx.lifecycle.LifecycleOwner r3 = r5.getViewLifecycleOwner()
            org.thoughtcrime.securesms.stories.viewer.views.StoryViewsFragment$$ExternalSyntheticLambda1 r4 = new org.thoughtcrime.securesms.stories.viewer.views.StoryViewsFragment$$ExternalSyntheticLambda1
            r4.<init>(r0, r1, r5, r6)
            r2.observe(r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsFragment.bindAdapter(org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter):void");
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m3204bindAdapter$lambda0(StoryViewsFragment storyViewsFragment, View view) {
        Intrinsics.checkNotNullParameter(storyViewsFragment, "this$0");
        AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
        Context requireContext = storyViewsFragment.requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        storyViewsFragment.startActivity(companion.privacy(requireContext));
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m3205bindAdapter$lambda1(View view, View view2, StoryViewsFragment storyViewsFragment, DSLSettingsAdapter dSLSettingsAdapter, StoryViewsState storyViewsState) {
        Intrinsics.checkNotNullParameter(view, "$emptyNotice");
        Intrinsics.checkNotNullParameter(view2, "$disabledNotice");
        Intrinsics.checkNotNullParameter(storyViewsFragment, "this$0");
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        StoryViewsState.LoadState loadState = storyViewsState.getLoadState();
        StoryViewsState.LoadState loadState2 = StoryViewsState.LoadState.READY;
        boolean z = true;
        ViewExtensionsKt.setVisible(view, loadState == loadState2 && storyViewsState.getViews().isEmpty());
        ViewExtensionsKt.setVisible(view2, storyViewsState.getLoadState() == StoryViewsState.LoadState.DISABLED);
        RecyclerView recyclerView = storyViewsFragment.getRecyclerView();
        if (recyclerView != null) {
            if (storyViewsState.getLoadState() != loadState2) {
                z = false;
            }
            ViewExtensionsKt.setVisible(recyclerView, z);
        }
        Intrinsics.checkNotNullExpressionValue(storyViewsState, "it");
        dSLSettingsAdapter.submitList(storyViewsFragment.getConfiguration(storyViewsState).toMappingModelList());
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().refresh();
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerChild
    public void onPageSelected(StoryViewsAndRepliesPagerParent.Child child) {
        Intrinsics.checkNotNullParameter(child, "child");
        RecyclerView recyclerView = getRecyclerView();
        if (recyclerView != null) {
            recyclerView.setNestedScrollingEnabled(child == StoryViewsAndRepliesPagerParent.Child.VIEWS);
        }
    }

    private final DSLConfiguration getConfiguration(StoryViewsState storyViewsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(storyViewsState) { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsFragment$getConfiguration$1
            final /* synthetic */ StoryViewsState $state;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                for (StoryViewItemData storyViewItemData : this.$state.getViews()) {
                    dSLConfiguration.customPref(new StoryViewItem.Model(storyViewItemData));
                }
            }
        });
    }

    /* compiled from: StoryViewsFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsFragment$Companion;", "", "()V", "ARG_STORY_ID", "", "create", "Landroidx/fragment/app/Fragment;", "storyId", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Fragment create(long j) {
            StoryViewsFragment storyViewsFragment = new StoryViewsFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(StoryViewsFragment.ARG_STORY_ID, j);
            storyViewsFragment.setArguments(bundle);
            return storyViewsFragment;
        }
    }
}
