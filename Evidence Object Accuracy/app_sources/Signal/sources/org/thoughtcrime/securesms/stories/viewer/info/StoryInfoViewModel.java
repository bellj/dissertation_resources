package org.thoughtcrime.securesms.stories.viewer.info;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRecipientRow;
import org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRepository;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: StoryInfoViewModel.kt */
@Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0017B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0016\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0014R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoViewModel;", "Landroidx/lifecycle/ViewModel;", "storyId", "", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRepository;", "(JLorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoState;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "buildRecipients", "", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRecipientRow$Model;", "storyInfo", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRepository$StoryInfo;", "onCleared", "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryInfoViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final Flowable<StoryInfoState> state;
    private final RxStore<StoryInfoState> store;

    public StoryInfoViewModel(long j, StoryInfoRepository storyInfoRepository) {
        Intrinsics.checkNotNullParameter(storyInfoRepository, "repository");
        RxStore<StoryInfoState> rxStore = new RxStore<>(new StoryInfoState(0, 0, 0, false, null, false, 63, null), null, 2, null);
        this.store = rxStore;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        Flowable<StoryInfoState> observeOn = rxStore.getStateFlowable().observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "store.stateFlowable.obse…dSchedulers.mainThread())");
        this.state = observeOn;
        Flowable<StoryInfoRepository.StoryInfo> flowable = storyInfoRepository.getStoryInfo(j).toFlowable(BackpressureStrategy.LATEST);
        Intrinsics.checkNotNullExpressionValue(flowable, "repository.getStoryInfo(…kpressureStrategy.LATEST)");
        DisposableKt.plusAssign(compositeDisposable, rxStore.update(flowable, new Function2<StoryInfoRepository.StoryInfo, StoryInfoState, StoryInfoState>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoViewModel.1
            final /* synthetic */ StoryInfoViewModel this$0;

            {
                this.this$0 = r1;
            }

            public final StoryInfoState invoke(StoryInfoRepository.StoryInfo storyInfo, StoryInfoState storyInfoState) {
                long j2;
                Intrinsics.checkNotNullParameter(storyInfoState, "storyInfoState");
                long dateSent = storyInfo.getMessageRecord().getDateSent();
                long dateReceived = storyInfo.getMessageRecord().getDateReceived();
                MessageRecord messageRecord = storyInfo.getMessageRecord();
                Long l = null;
                MmsMessageRecord mmsMessageRecord = messageRecord instanceof MmsMessageRecord ? (MmsMessageRecord) messageRecord : null;
                if (mmsMessageRecord != null) {
                    Slide firstSlide = mmsMessageRecord.getSlideDeck().getFirstSlide();
                    if (firstSlide != null) {
                        l = Long.valueOf(firstSlide.getFileSize());
                    }
                    if (l != null) {
                        j2 = l.longValue();
                        boolean isOutgoing = storyInfo.getMessageRecord().isOutgoing();
                        StoryInfoViewModel storyInfoViewModel = this.this$0;
                        Intrinsics.checkNotNullExpressionValue(storyInfo, "storyInfo");
                        return storyInfoState.copy(dateSent, dateReceived, j2, isOutgoing, storyInfoViewModel.buildRecipients(storyInfo), true);
                    }
                }
                j2 = -1;
                boolean isOutgoing = storyInfo.getMessageRecord().isOutgoing();
                StoryInfoViewModel storyInfoViewModel = this.this$0;
                Intrinsics.checkNotNullExpressionValue(storyInfo, "storyInfo");
                return storyInfoState.copy(dateSent, dateReceived, j2, isOutgoing, storyInfoViewModel.buildRecipients(storyInfo), true);
            }
        }));
    }

    public /* synthetic */ StoryInfoViewModel(long j, StoryInfoRepository storyInfoRepository, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, (i & 2) != 0 ? new StoryInfoRepository() : storyInfoRepository);
    }

    public final Flowable<StoryInfoState> getState() {
        return this.state;
    }

    public final List<StoryInfoRecipientRow.Model> buildRecipients(StoryInfoRepository.StoryInfo storyInfo) {
        if (storyInfo.getMessageRecord().isOutgoing()) {
            List<GroupReceiptDatabase.GroupReceiptInfo> receiptInfo = storyInfo.getReceiptInfo();
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(receiptInfo, 10));
            for (GroupReceiptDatabase.GroupReceiptInfo groupReceiptInfo : receiptInfo) {
                Recipient resolved = Recipient.resolved(groupReceiptInfo.getRecipientId());
                Intrinsics.checkNotNullExpressionValue(resolved, "resolved(it.recipientId)");
                arrayList.add(new StoryInfoRecipientRow.Model(resolved, groupReceiptInfo.getTimestamp(), groupReceiptInfo.getStatus()));
            }
            return arrayList;
        }
        Recipient individualRecipient = storyInfo.getMessageRecord().getIndividualRecipient();
        Intrinsics.checkNotNullExpressionValue(individualRecipient, "storyInfo.messageRecord.individualRecipient");
        return CollectionsKt__CollectionsJVMKt.listOf(new StoryInfoRecipientRow.Model(individualRecipient, storyInfo.getMessageRecord().getDateSent(), -1));
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    /* compiled from: StoryInfoViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "storyId", "", "(J)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final long storyId;

        public Factory(long j) {
            this.storyId = j;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StoryInfoViewModel(this.storyId, null, 2, null));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.viewer.info.StoryInfoViewModel.Factory.create");
        }
    }
}
