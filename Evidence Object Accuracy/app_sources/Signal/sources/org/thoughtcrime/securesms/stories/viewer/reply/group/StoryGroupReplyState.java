package org.thoughtcrime.securesms.stories.viewer.reply.group;

import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.conversation.colors.NameColor;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: StoryGroupReplyState.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001%B?\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0014\b\u0002\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\u0015\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bHÆ\u0003J\t\u0010\u001d\u001a\u00020\fHÆ\u0003JC\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u0014\b\u0002\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b2\b\b\u0002\u0010\u000b\u001a\u00020\fHÆ\u0001J\u0013\u0010\u001f\u001a\u00020\u00132\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010!\u001a\u00020\"HÖ\u0001J\t\u0010#\u001a\u00020$HÖ\u0001R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u001d\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyState;", "", "threadId", "", "replies", "", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;", "nameColors", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lorg/thoughtcrime/securesms/conversation/colors/NameColor;", "loadState", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyState$LoadState;", "(JLjava/util/List;Ljava/util/Map;Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyState$LoadState;)V", "getLoadState", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyState$LoadState;", "getNameColors", "()Ljava/util/Map;", "noReplies", "", "getNoReplies", "()Z", "getReplies", "()Ljava/util/List;", "getThreadId", "()J", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "toString", "", "LoadState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryGroupReplyState {
    private final LoadState loadState;
    private final Map<RecipientId, NameColor> nameColors;
    private final boolean noReplies;
    private final List<ReplyBody> replies;
    private final long threadId;

    /* compiled from: StoryGroupReplyState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyState$LoadState;", "", "(Ljava/lang/String;I)V", "INIT", "READY", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum LoadState {
        INIT,
        READY
    }

    public StoryGroupReplyState() {
        this(0, null, null, null, 15, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ StoryGroupReplyState copy$default(StoryGroupReplyState storyGroupReplyState, long j, List list, Map map, LoadState loadState, int i, Object obj) {
        if ((i & 1) != 0) {
            j = storyGroupReplyState.threadId;
        }
        if ((i & 2) != 0) {
            list = storyGroupReplyState.replies;
        }
        if ((i & 4) != 0) {
            map = storyGroupReplyState.nameColors;
        }
        if ((i & 8) != 0) {
            loadState = storyGroupReplyState.loadState;
        }
        return storyGroupReplyState.copy(j, list, map, loadState);
    }

    public final long component1() {
        return this.threadId;
    }

    public final List<ReplyBody> component2() {
        return this.replies;
    }

    public final Map<RecipientId, NameColor> component3() {
        return this.nameColors;
    }

    public final LoadState component4() {
        return this.loadState;
    }

    public final StoryGroupReplyState copy(long j, List<? extends ReplyBody> list, Map<RecipientId, NameColor> map, LoadState loadState) {
        Intrinsics.checkNotNullParameter(list, "replies");
        Intrinsics.checkNotNullParameter(map, "nameColors");
        Intrinsics.checkNotNullParameter(loadState, "loadState");
        return new StoryGroupReplyState(j, list, map, loadState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryGroupReplyState)) {
            return false;
        }
        StoryGroupReplyState storyGroupReplyState = (StoryGroupReplyState) obj;
        return this.threadId == storyGroupReplyState.threadId && Intrinsics.areEqual(this.replies, storyGroupReplyState.replies) && Intrinsics.areEqual(this.nameColors, storyGroupReplyState.nameColors) && this.loadState == storyGroupReplyState.loadState;
    }

    public int hashCode() {
        return (((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.threadId) * 31) + this.replies.hashCode()) * 31) + this.nameColors.hashCode()) * 31) + this.loadState.hashCode();
    }

    public String toString() {
        return "StoryGroupReplyState(threadId=" + this.threadId + ", replies=" + this.replies + ", nameColors=" + this.nameColors + ", loadState=" + this.loadState + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody> */
    /* JADX WARN: Multi-variable type inference failed */
    public StoryGroupReplyState(long j, List<? extends ReplyBody> list, Map<RecipientId, NameColor> map, LoadState loadState) {
        Intrinsics.checkNotNullParameter(list, "replies");
        Intrinsics.checkNotNullParameter(map, "nameColors");
        Intrinsics.checkNotNullParameter(loadState, "loadState");
        this.threadId = j;
        this.replies = list;
        this.nameColors = map;
        this.loadState = loadState;
        this.noReplies = list.isEmpty();
    }

    public final long getThreadId() {
        return this.threadId;
    }

    public /* synthetic */ StoryGroupReplyState(long j, List list, Map map, LoadState loadState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? 0 : j, (i & 2) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i & 4) != 0 ? MapsKt__MapsKt.emptyMap() : map, (i & 8) != 0 ? LoadState.INIT : loadState);
    }

    public final List<ReplyBody> getReplies() {
        return this.replies;
    }

    public final Map<RecipientId, NameColor> getNameColors() {
        return this.nameColors;
    }

    public final LoadState getLoadState() {
        return this.loadState;
    }

    public final boolean getNoReplies() {
        return this.noReplies;
    }
}
