package org.thoughtcrime.securesms.stories.viewer.reply.group;

import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ StoryGroupReplyItem.Model f$0;

    public /* synthetic */ StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda1(StoryGroupReplyItem.Model model) {
        this.f$0 = model;
    }

    @Override // java.lang.Runnable
    public final void run() {
        StoryGroupReplyItem.BaseViewHolder.m3151displayContextMenu$lambda4(this.f$0);
    }
}
