package org.thoughtcrime.securesms.stories.viewer.reply.group;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.ThreadUtil;
import org.signal.paging.ObservablePagedData;
import org.signal.paging.PagedData;
import org.signal.paging.PagingConfig;
import org.signal.paging.PagingController;
import org.thoughtcrime.securesms.conversation.colors.NameColor;
import org.thoughtcrime.securesms.conversation.colors.NameColors;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: StoryGroupReplyRepository.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J:\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u00042\u0006\u0010\b\u001a\u00020\t2\u0018\u0010\n\u001a\u0014\u0012\u0004\u0012\u00020\f\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u000bJ \u0010\u000f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u00100\u00042\u0006\u0010\u0013\u001a\u00020\tJ\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\t0\u00152\u0006\u0010\b\u001a\u00020\t¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyRepository;", "", "()V", "getNameColorsMap", "Lio/reactivex/rxjava3/core/Observable;", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lorg/thoughtcrime/securesms/conversation/colors/NameColor;", "storyId", "", "sessionMemberCache", "", "Lorg/thoughtcrime/securesms/groups/GroupId;", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getPagedReplies", "Lorg/signal/paging/ObservablePagedData;", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;", "parentStoryId", "getThreadId", "Lio/reactivex/rxjava3/core/Single;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryGroupReplyRepository {
    public final Single<Long> getThreadId(long j) {
        Single<Long> subscribeOn = Single.fromCallable(new Callable(j) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda13
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return StoryGroupReplyRepository.$r8$lambda$narq7o5Z6uzUfT41a5WZ7TDMN3I(this.f$0);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      Sig…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getThreadId$lambda-0 */
    public static final Long m3174getThreadId$lambda0(long j) {
        return Long.valueOf(SignalDatabase.Companion.mms().getThreadIdForMessage(j));
    }

    /* JADX DEBUG: Type inference failed for r3v1. Raw type applied. Possible types: io.reactivex.rxjava3.core.Observable<R>, java.lang.Object, io.reactivex.rxjava3.core.Observable<org.signal.paging.ObservablePagedData<org.thoughtcrime.securesms.database.model.MessageId, org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody>> */
    public final Observable<ObservablePagedData<MessageId, ReplyBody>> getPagedReplies(long j) {
        Observable flatMap = getThreadId(j).toObservable().flatMap(new Function(j) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda12
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryGroupReplyRepository.m3156$r8$lambda$Znq2alg82QJD56Mq9iGDqJK7o8(this.f$0, (Long) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMap, "getThreadId(parentStoryI…(Schedulers.io())\n      }");
        return flatMap;
    }

    /* renamed from: getPagedReplies$lambda-6 */
    public static final ObservableSource m3168getPagedReplies$lambda6(long j, Long l) {
        return Observable.create(new ObservableOnSubscribe(j, l) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda11
            public final /* synthetic */ long f$0;
            public final /* synthetic */ Long f$1;

            {
                this.f$0 = r1;
                this.f$1 = r3;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                StoryGroupReplyRepository.m3158$r8$lambda$jBd9ncWDRB_c1VwUC85X4YxTO8(this.f$0, this.f$1, observableEmitter);
            }
        }).subscribeOn(Schedulers.io());
    }

    /* renamed from: getPagedReplies$lambda-6$lambda-5 */
    public static final void m3169getPagedReplies$lambda6$lambda5(long j, Long l, ObservableEmitter observableEmitter) {
        ObservablePagedData createForObservable = PagedData.createForObservable(new StoryGroupReplyDataSource(j), new PagingConfig.Builder().build());
        Intrinsics.checkNotNullExpressionValue(createForObservable, "createForObservable(Stor…Config.Builder().build())");
        PagingController<Key> controller = createForObservable.getController();
        Intrinsics.checkNotNullExpressionValue(controller, "pagedData.controller");
        StoryGroupReplyRepository$$ExternalSyntheticLambda2 storyGroupReplyRepository$$ExternalSyntheticLambda2 = new DatabaseObserver.MessageObserver() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.MessageObserver
            public final void onMessageChanged(MessageId messageId) {
                StoryGroupReplyRepository.$r8$lambda$8TQkUsEaDLiHsNfWVzk3hXIXDjE(PagingController.this, messageId);
            }
        };
        StoryGroupReplyRepository$$ExternalSyntheticLambda3 storyGroupReplyRepository$$ExternalSyntheticLambda3 = new DatabaseObserver.MessageObserver() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.MessageObserver
            public final void onMessageChanged(MessageId messageId) {
                StoryGroupReplyRepository.m3153$r8$lambda$0izQVFHeLgUbpqiL6n3A6QPxAU(PagingController.this, messageId);
            }
        };
        StoryGroupReplyRepository$$ExternalSyntheticLambda4 storyGroupReplyRepository$$ExternalSyntheticLambda4 = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda4
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                StoryGroupReplyRepository.m3160$r8$lambda$zUO3LqSDQen7k9qT1NzpG5WKoU(PagingController.this);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerMessageUpdateObserver(storyGroupReplyRepository$$ExternalSyntheticLambda2);
        DatabaseObserver databaseObserver = ApplicationDependencies.getDatabaseObserver();
        Intrinsics.checkNotNullExpressionValue(l, "threadId");
        databaseObserver.registerMessageInsertObserver(l.longValue(), storyGroupReplyRepository$$ExternalSyntheticLambda3);
        ApplicationDependencies.getDatabaseObserver().registerConversationObserver(l.longValue(), storyGroupReplyRepository$$ExternalSyntheticLambda4);
        observableEmitter.setCancellable(new Cancellable(storyGroupReplyRepository$$ExternalSyntheticLambda3, storyGroupReplyRepository$$ExternalSyntheticLambda4) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ DatabaseObserver.MessageObserver f$1;
            public final /* synthetic */ DatabaseObserver.Observer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                StoryGroupReplyRepository.m3154$r8$lambda$8FEkdfAbigswxx1FDpkCQYwEOY(DatabaseObserver.MessageObserver.this, this.f$1, this.f$2);
            }
        });
        observableEmitter.onNext(createForObservable);
    }

    /* renamed from: getPagedReplies$lambda-6$lambda-5$lambda-1 */
    public static final void m3170getPagedReplies$lambda6$lambda5$lambda1(PagingController pagingController, MessageId messageId) {
        Intrinsics.checkNotNullParameter(pagingController, "$controller");
        Intrinsics.checkNotNullParameter(messageId, "it");
        pagingController.onDataItemChanged(messageId);
    }

    /* renamed from: getPagedReplies$lambda-6$lambda-5$lambda-2 */
    public static final void m3171getPagedReplies$lambda6$lambda5$lambda2(PagingController pagingController, MessageId messageId) {
        Intrinsics.checkNotNullParameter(pagingController, "$controller");
        Intrinsics.checkNotNullParameter(messageId, "it");
        pagingController.onDataItemInserted(messageId, -1);
    }

    /* renamed from: getPagedReplies$lambda-6$lambda-5$lambda-3 */
    public static final void m3172getPagedReplies$lambda6$lambda5$lambda3(PagingController pagingController) {
        Intrinsics.checkNotNullParameter(pagingController, "$controller");
        pagingController.onDataInvalidated();
    }

    /* renamed from: getPagedReplies$lambda-6$lambda-5$lambda-4 */
    public static final void m3173getPagedReplies$lambda6$lambda5$lambda4(DatabaseObserver.MessageObserver messageObserver, DatabaseObserver.MessageObserver messageObserver2, DatabaseObserver.Observer observer) {
        Intrinsics.checkNotNullParameter(messageObserver, "$updateObserver");
        Intrinsics.checkNotNullParameter(messageObserver2, "$insertObserver");
        Intrinsics.checkNotNullParameter(observer, "$conversationObserver");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(messageObserver);
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(messageObserver2);
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(observer);
    }

    /* renamed from: getNameColorsMap$lambda-7 */
    public static final RecipientId m3167getNameColorsMap$lambda7(long j) {
        return SignalDatabase.Companion.mms().getMessageRecord(j).getIndividualRecipient().getId();
    }

    public final Observable<Map<RecipientId, NameColor>> getNameColorsMap(long j, Map<GroupId, Set<Recipient>> map) {
        Intrinsics.checkNotNullParameter(map, "sessionMemberCache");
        Observable<Map<RecipientId, NameColor>> flatMapObservable = Single.fromCallable(new Callable(j) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return StoryGroupReplyRepository.$r8$lambda$jXWLlCzPLFqADOVMvNNlnhtvSLI(this.f$0);
            }
        }).subscribeOn(Schedulers.io()).flatMapObservable(new Function(map) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ Map f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryGroupReplyRepository.m3157$r8$lambda$gXsi7blXO4WUqnIKI4oaD_d5vU(this.f$0, (RecipientId) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMapObservable, "fromCallable { SignalDat…(Schedulers.io())\n      }");
        return flatMapObservable;
    }

    /* renamed from: getNameColorsMap$lambda-13 */
    public static final ObservableSource m3161getNameColorsMap$lambda13(Map map, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(map, "$sessionMemberCache");
        return Observable.create(new ObservableOnSubscribe(map) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda10
            public final /* synthetic */ Map f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                StoryGroupReplyRepository.m3159$r8$lambda$oVPqcegrDNjE_ITJMrl94PQ7qI(RecipientId.this, this.f$1, observableEmitter);
            }
        }).subscribeOn(Schedulers.io());
    }

    /* renamed from: getNameColorsMap$lambda-13$lambda-12 */
    public static final void m3162getNameColorsMap$lambda13$lambda12(RecipientId recipientId, Map map, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(map, "$sessionMemberCache");
        LiveData<Map<RecipientId, NameColor>> nameColorsMapLiveData = NameColors.INSTANCE.getNameColorsMapLiveData(new MutableLiveData(recipientId), map);
        StoryGroupReplyRepository$$ExternalSyntheticLambda6 storyGroupReplyRepository$$ExternalSyntheticLambda6 = new Observer() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StoryGroupReplyRepository.$r8$lambda$xTYTg5iLK0NgB29R5bEOApQc_8g(ObservableEmitter.this, (Map) obj);
            }
        };
        ThreadUtil.postToMain(new Runnable(storyGroupReplyRepository$$ExternalSyntheticLambda6) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda7
            public final /* synthetic */ Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StoryGroupReplyRepository.$r8$lambda$i_Sl022vvjQsizbMvSLOvi4D82c(LiveData.this, this.f$1);
            }
        });
        observableEmitter.setCancellable(new Cancellable(storyGroupReplyRepository$$ExternalSyntheticLambda6) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                StoryGroupReplyRepository.$r8$lambda$nJCq_mTpQY2rhojOWs8tCqbDIFk(LiveData.this, this.f$1);
            }
        });
    }

    /* renamed from: getNameColorsMap$lambda-13$lambda-12$lambda-8 */
    public static final void m3165getNameColorsMap$lambda13$lambda12$lambda8(ObservableEmitter observableEmitter, Map map) {
        observableEmitter.onNext(map);
    }

    /* renamed from: getNameColorsMap$lambda-13$lambda-12$lambda-9 */
    public static final void m3166getNameColorsMap$lambda13$lambda12$lambda9(LiveData liveData, Observer observer) {
        Intrinsics.checkNotNullParameter(liveData, "$nameColorsMapLiveData");
        Intrinsics.checkNotNullParameter(observer, "$observer");
        liveData.observeForever(observer);
    }

    /* renamed from: getNameColorsMap$lambda-13$lambda-12$lambda-11 */
    public static final void m3163getNameColorsMap$lambda13$lambda12$lambda11(LiveData liveData, Observer observer) {
        Intrinsics.checkNotNullParameter(liveData, "$nameColorsMapLiveData");
        Intrinsics.checkNotNullParameter(observer, "$observer");
        ThreadUtil.postToMain(new Runnable(observer) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyRepository$$ExternalSyntheticLambda9
            public final /* synthetic */ Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StoryGroupReplyRepository.m3155$r8$lambda$AtlOp1xzWdghgodExIkjFzn_8I(LiveData.this, this.f$1);
            }
        });
    }

    /* renamed from: getNameColorsMap$lambda-13$lambda-12$lambda-11$lambda-10 */
    public static final void m3164getNameColorsMap$lambda13$lambda12$lambda11$lambda10(LiveData liveData, Observer observer) {
        Intrinsics.checkNotNullParameter(liveData, "$nameColorsMapLiveData");
        Intrinsics.checkNotNullParameter(observer, "$observer");
        liveData.removeObserver(observer);
    }
}
