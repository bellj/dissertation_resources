package org.thoughtcrime.securesms.stories.settings.my;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: MyStorySettingsViewModel.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u000e\u001a\u00020\u000fH\u0014J\u0006\u0010\u0010\u001a\u00020\u000fJ\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u0017R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsRepository;", "(Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "refresh", "setMyStoryPrivacyMode", "Lio/reactivex/rxjava3/core/Completable;", "privacyMode", "Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "setRepliesAndReactionsEnabled", "repliesAndReactionsEnabled", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStorySettingsViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final MyStorySettingsRepository repository;
    private final LiveData<MyStorySettingsState> state;
    private final Store<MyStorySettingsState> store;

    public MyStorySettingsViewModel() {
        this(null, 1, null);
    }

    public MyStorySettingsViewModel(MyStorySettingsRepository myStorySettingsRepository) {
        Intrinsics.checkNotNullParameter(myStorySettingsRepository, "repository");
        this.repository = myStorySettingsRepository;
        Store<MyStorySettingsState> store = new Store<>(new MyStorySettingsState(null, false, 3, null));
        this.store = store;
        this.disposables = new CompositeDisposable();
        LiveData<MyStorySettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public /* synthetic */ MyStorySettingsViewModel(MyStorySettingsRepository myStorySettingsRepository, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? new MyStorySettingsRepository() : myStorySettingsRepository);
    }

    public final LiveData<MyStorySettingsState> getState() {
        return this.state;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void refresh() {
        this.disposables.clear();
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.repository.getPrivacyState().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                MyStorySettingsViewModel.m2920refresh$lambda1(MyStorySettingsViewModel.this, (MyStoryPrivacyState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getPrivacySta… myStoryPrivacyState) } }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
        CompositeDisposable compositeDisposable2 = this.disposables;
        Disposable subscribe2 = this.repository.getRepliesAndReactionsEnabled().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                MyStorySettingsViewModel.m2922refresh$lambda3(MyStorySettingsViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "repository.getRepliesAnd…sAndReactionsEnabled) } }");
        DisposableKt.plusAssign(compositeDisposable2, subscribe2);
    }

    /* renamed from: refresh$lambda-1 */
    public static final void m2920refresh$lambda1(MyStorySettingsViewModel myStorySettingsViewModel, MyStoryPrivacyState myStoryPrivacyState) {
        Intrinsics.checkNotNullParameter(myStorySettingsViewModel, "this$0");
        myStorySettingsViewModel.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsViewModel$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MyStorySettingsViewModel.m2921refresh$lambda1$lambda0(MyStoryPrivacyState.this, (MyStorySettingsState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-1$lambda-0 */
    public static final MyStorySettingsState m2921refresh$lambda1$lambda0(MyStoryPrivacyState myStoryPrivacyState, MyStorySettingsState myStorySettingsState) {
        Intrinsics.checkNotNullExpressionValue(myStorySettingsState, "it");
        Intrinsics.checkNotNullExpressionValue(myStoryPrivacyState, "myStoryPrivacyState");
        return MyStorySettingsState.copy$default(myStorySettingsState, myStoryPrivacyState, false, 2, null);
    }

    /* renamed from: refresh$lambda-3 */
    public static final void m2922refresh$lambda3(MyStorySettingsViewModel myStorySettingsViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(myStorySettingsViewModel, "this$0");
        myStorySettingsViewModel.store.update(new Function(bool) { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ Boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MyStorySettingsViewModel.m2923refresh$lambda3$lambda2(this.f$0, (MyStorySettingsState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-3$lambda-2 */
    public static final MyStorySettingsState m2923refresh$lambda3$lambda2(Boolean bool, MyStorySettingsState myStorySettingsState) {
        Intrinsics.checkNotNullExpressionValue(myStorySettingsState, "it");
        Intrinsics.checkNotNullExpressionValue(bool, "repliesAndReactionsEnabled");
        return MyStorySettingsState.copy$default(myStorySettingsState, null, bool.booleanValue(), 1, null);
    }

    public final void setRepliesAndReactionsEnabled(boolean z) {
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.repository.setRepliesAndReactionsEnabled(z).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                MyStorySettingsViewModel.m2925setRepliesAndReactionsEnabled$lambda4(MyStorySettingsViewModel.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.setRepliesAnd… .subscribe { refresh() }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* renamed from: setRepliesAndReactionsEnabled$lambda-4 */
    public static final void m2925setRepliesAndReactionsEnabled$lambda4(MyStorySettingsViewModel myStorySettingsViewModel) {
        Intrinsics.checkNotNullParameter(myStorySettingsViewModel, "this$0");
        myStorySettingsViewModel.refresh();
    }

    public final Completable setMyStoryPrivacyMode(DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        MyStorySettingsState value = this.state.getValue();
        Intrinsics.checkNotNull(value);
        if (distributionListPrivacyMode == value.getMyStoryPrivacyState().getPrivacyMode()) {
            Completable complete = Completable.complete();
            Intrinsics.checkNotNullExpressionValue(complete, "{\n      Completable.complete()\n    }");
            return complete;
        }
        Completable doOnComplete = this.repository.setPrivacyMode(distributionListPrivacyMode).observeOn(AndroidSchedulers.mainThread()).doOnComplete(new Action() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                MyStorySettingsViewModel.m2924setMyStoryPrivacyMode$lambda5(MyStorySettingsViewModel.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(doOnComplete, "{\n      repository.setPr…plete { refresh() }\n    }");
        return doOnComplete;
    }

    /* renamed from: setMyStoryPrivacyMode$lambda-5 */
    public static final void m2924setMyStoryPrivacyMode$lambda5(MyStorySettingsViewModel myStorySettingsViewModel) {
        Intrinsics.checkNotNullParameter(myStorySettingsViewModel, "this$0");
        myStorySettingsViewModel.refresh();
    }
}
