package org.thoughtcrime.securesms.stories.my;

import android.view.View;
import org.thoughtcrime.securesms.stories.my.MyStoriesItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class MyStoriesItem$ViewHolder$$ExternalSyntheticLambda8 implements View.OnClickListener {
    public final /* synthetic */ MyStoriesItem.ViewHolder f$0;
    public final /* synthetic */ MyStoriesItem.Model f$1;

    public /* synthetic */ MyStoriesItem$ViewHolder$$ExternalSyntheticLambda8(MyStoriesItem.ViewHolder viewHolder, MyStoriesItem.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        MyStoriesItem.ViewHolder.m2843bind$lambda5(this.f$0, this.f$1, view);
    }
}
