package org.thoughtcrime.securesms.stories.settings.custom.name;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableEmitter;
import io.reactivex.rxjava3.core.CompletableOnSubscribe;
import io.reactivex.rxjava3.schedulers.Schedulers;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: EditStoryNameRepository.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/name/EditStoryNameRepository;", "", "()V", "save", "Lio/reactivex/rxjava3/core/Completable;", "privateStoryId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "name", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class EditStoryNameRepository {
    public final Completable save(DistributionListId distributionListId, CharSequence charSequence) {
        Intrinsics.checkNotNullParameter(distributionListId, "privateStoryId");
        Intrinsics.checkNotNullParameter(charSequence, "name");
        Completable subscribeOn = Completable.create(new CompletableOnSubscribe(charSequence) { // from class: org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ CharSequence f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.core.CompletableOnSubscribe
            public final void subscribe(CompletableEmitter completableEmitter) {
                EditStoryNameRepository.$r8$lambda$R2A2EmJUv8_RY4QqHSF27FIpfq0(DistributionListId.this, this.f$1, completableEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create {\n      if (priva…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: save$lambda-0 */
    public static final void m2901save$lambda0(DistributionListId distributionListId, CharSequence charSequence, CompletableEmitter completableEmitter) {
        Intrinsics.checkNotNullParameter(distributionListId, "$privateStoryId");
        Intrinsics.checkNotNullParameter(charSequence, "$name");
        if (Intrinsics.areEqual(distributionListId, DistributionListId.MY_STORY)) {
            throw new IllegalStateException("Cannot set name for My Story".toString());
        } else if (SignalDatabase.Companion.distributionLists().setName(distributionListId, charSequence.toString())) {
            Stories.INSTANCE.onStorySettingsChanged(distributionListId);
            completableEmitter.onComplete();
        } else {
            completableEmitter.onError(new Exception("Could not update story name."));
        }
    }
}
