package org.thoughtcrime.securesms.stories.viewer.text;

import android.graphics.Typeface;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostState;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: StoryTextPostViewModel.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 \u00122\u00020\u0001:\u0002\u0012\u0013B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0010\u001a\u00020\u0011H\u0014R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostViewModel;", "Landroidx/lifecycle/ViewModel;", "recordId", "", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostRepository;", "(JLorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryTextPostViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(StoryTextPostViewModel.class);
    private final CompositeDisposable disposables;
    private final LiveData<StoryTextPostState> state;
    private final Store<StoryTextPostState> store;

    public StoryTextPostViewModel(long j, StoryTextPostRepository storyTextPostRepository) {
        Intrinsics.checkNotNullParameter(storyTextPostRepository, "repository");
        Store<StoryTextPostState> store = new Store<>(new StoryTextPostState(null, null, null, null, 15, null));
        this.store = store;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        LiveData<StoryTextPostState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy(storyTextPostRepository.getTypeface(j), new Function1<Throwable, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel.1
            final /* synthetic */ StoryTextPostViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Intrinsics.checkNotNullParameter(th, "error");
                Log.w(StoryTextPostViewModel.TAG, "Failed to get typeface. Rendering with default.", th);
                this.this$0.store.update(new StoryTextPostViewModel$1$$ExternalSyntheticLambda0());
            }

            /* renamed from: invoke$lambda-0 */
            public static final StoryTextPostState m3198invoke$lambda0(StoryTextPostState storyTextPostState) {
                Intrinsics.checkNotNullExpressionValue(storyTextPostState, "it");
                return StoryTextPostState.copy$default(storyTextPostState, null, null, null, Typeface.DEFAULT, 7, null);
            }
        }, new Function1<Typeface, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel.2
            final /* synthetic */ StoryTextPostViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Typeface typeface) {
                invoke(typeface);
                return Unit.INSTANCE;
            }

            public final void invoke(Typeface typeface) {
                Intrinsics.checkNotNullParameter(typeface, "typeface");
                this.this$0.store.update(new StoryTextPostViewModel$2$$ExternalSyntheticLambda0(typeface));
            }

            /* renamed from: invoke$lambda-0 */
            public static final StoryTextPostState m3200invoke$lambda0(Typeface typeface, StoryTextPostState storyTextPostState) {
                Intrinsics.checkNotNullParameter(typeface, "$typeface");
                Intrinsics.checkNotNullExpressionValue(storyTextPostState, "it");
                return StoryTextPostState.copy$default(storyTextPostState, null, null, null, typeface, 7, null);
            }
        }));
        Single<R> map = storyTextPostRepository.getRecord(j).map(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryTextPostViewModel.m3197_init_$lambda0((MmsMessageRecord) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "repository.getRecord(rec…mpty.\")\n        }\n      }");
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy(map, new Function1<Throwable, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel.4
            final /* synthetic */ StoryTextPostViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Intrinsics.checkNotNullParameter(th, "it");
                this.this$0.store.update(new StoryTextPostViewModel$4$$ExternalSyntheticLambda0());
            }

            /* renamed from: invoke$lambda-0 */
            public static final StoryTextPostState m3201invoke$lambda0(StoryTextPostState storyTextPostState) {
                Intrinsics.checkNotNullExpressionValue(storyTextPostState, "state");
                return StoryTextPostState.copy$default(storyTextPostState, null, null, StoryTextPostState.LoadState.FAILED, null, 11, null);
            }
        }, new Function1<Pair<? extends StoryTextPost, ? extends LinkPreview>, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel.5
            final /* synthetic */ StoryTextPostViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends StoryTextPost, ? extends LinkPreview> pair) {
                invoke((Pair<StoryTextPost, ? extends LinkPreview>) pair);
                return Unit.INSTANCE;
            }

            public final void invoke(Pair<StoryTextPost, ? extends LinkPreview> pair) {
                this.this$0.store.update(new StoryTextPostViewModel$5$$ExternalSyntheticLambda0(pair.component1(), (LinkPreview) pair.component2()));
            }

            /* renamed from: invoke$lambda-0 */
            public static final StoryTextPostState m3202invoke$lambda0(StoryTextPost storyTextPost, LinkPreview linkPreview, StoryTextPostState storyTextPostState) {
                Intrinsics.checkNotNullExpressionValue(storyTextPostState, "state");
                return StoryTextPostState.copy$default(storyTextPostState, storyTextPost, linkPreview, StoryTextPostState.LoadState.LOADED, null, 8, null);
            }
        }));
    }

    /* compiled from: StoryTextPostViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostViewModel$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final LiveData<StoryTextPostState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final Pair m3197_init_$lambda0(MmsMessageRecord mmsMessageRecord) {
        String body = mmsMessageRecord.getBody();
        Intrinsics.checkNotNullExpressionValue(body, "it.body");
        if (body.length() > 0) {
            StoryTextPost parseFrom = StoryTextPost.parseFrom(Base64.decode(mmsMessageRecord.getBody()));
            List<LinkPreview> linkPreviews = mmsMessageRecord.getLinkPreviews();
            Intrinsics.checkNotNullExpressionValue(linkPreviews, "it.linkPreviews");
            return TuplesKt.to(parseFrom, CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) linkPreviews)));
        }
        throw new Exception("Text post message body is empty.");
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    /* compiled from: StoryTextPostViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "recordId", "", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostRepository;", "(JLorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final long recordId;
        private final StoryTextPostRepository repository;

        public Factory(long j, StoryTextPostRepository storyTextPostRepository) {
            Intrinsics.checkNotNullParameter(storyTextPostRepository, "repository");
            this.recordId = j;
            this.repository = storyTextPostRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StoryTextPostViewModel(this.recordId, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel.Factory.create");
        }
    }
}
