package org.thoughtcrime.securesms.stories.settings.custom.name;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;

/* loaded from: classes3.dex */
public class EditStoryNameFragmentArgs {
    private final HashMap arguments;

    private EditStoryNameFragmentArgs() {
        this.arguments = new HashMap();
    }

    private EditStoryNameFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static EditStoryNameFragmentArgs fromBundle(Bundle bundle) {
        EditStoryNameFragmentArgs editStoryNameFragmentArgs = new EditStoryNameFragmentArgs();
        bundle.setClassLoader(EditStoryNameFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
            throw new IllegalArgumentException("Required argument \"distribution_list_id\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(DistributionListId.class) || Serializable.class.isAssignableFrom(DistributionListId.class)) {
            DistributionListId distributionListId = (DistributionListId) bundle.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
            if (distributionListId != null) {
                editStoryNameFragmentArgs.arguments.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                if (bundle.containsKey("name")) {
                    String string = bundle.getString("name");
                    if (string != null) {
                        editStoryNameFragmentArgs.arguments.put("name", string);
                        return editStoryNameFragmentArgs;
                    }
                    throw new IllegalArgumentException("Argument \"name\" is marked as non-null but was passed a null value.");
                }
                throw new IllegalArgumentException("Required argument \"name\" is missing and does not have an android:defaultValue");
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(DistributionListId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public DistributionListId getDistributionListId() {
        return (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
    }

    public String getName() {
        return (String) this.arguments.get("name");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
            DistributionListId distributionListId = (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
            if (Parcelable.class.isAssignableFrom(DistributionListId.class) || distributionListId == null) {
                bundle.putParcelable(RecipientDatabase.DISTRIBUTION_LIST_ID, (Parcelable) Parcelable.class.cast(distributionListId));
            } else if (Serializable.class.isAssignableFrom(DistributionListId.class)) {
                bundle.putSerializable(RecipientDatabase.DISTRIBUTION_LIST_ID, (Serializable) Serializable.class.cast(distributionListId));
            } else {
                throw new UnsupportedOperationException(DistributionListId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        if (this.arguments.containsKey("name")) {
            bundle.putString("name", (String) this.arguments.get("name"));
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        EditStoryNameFragmentArgs editStoryNameFragmentArgs = (EditStoryNameFragmentArgs) obj;
        if (this.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID) != editStoryNameFragmentArgs.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
            return false;
        }
        if (getDistributionListId() == null ? editStoryNameFragmentArgs.getDistributionListId() != null : !getDistributionListId().equals(editStoryNameFragmentArgs.getDistributionListId())) {
            return false;
        }
        if (this.arguments.containsKey("name") != editStoryNameFragmentArgs.arguments.containsKey("name")) {
            return false;
        }
        return getName() == null ? editStoryNameFragmentArgs.getName() == null : getName().equals(editStoryNameFragmentArgs.getName());
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((getDistributionListId() != null ? getDistributionListId().hashCode() : 0) + 31) * 31;
        if (getName() != null) {
            i = getName().hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "EditStoryNameFragmentArgs{distributionListId=" + getDistributionListId() + ", name=" + getName() + "}";
    }

    /* loaded from: classes3.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(EditStoryNameFragmentArgs editStoryNameFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(editStoryNameFragmentArgs.arguments);
        }

        public Builder(DistributionListId distributionListId, String str) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (distributionListId != null) {
                hashMap.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                if (str != null) {
                    hashMap.put("name", str);
                    return;
                }
                throw new IllegalArgumentException("Argument \"name\" is marked as non-null but was passed a null value.");
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        }

        public EditStoryNameFragmentArgs build() {
            return new EditStoryNameFragmentArgs(this.arguments);
        }

        public Builder setDistributionListId(DistributionListId distributionListId) {
            if (distributionListId != null) {
                this.arguments.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                return this;
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        }

        public Builder setName(String str) {
            if (str != null) {
                this.arguments.put("name", str);
                return this;
            }
            throw new IllegalArgumentException("Argument \"name\" is marked as non-null but was passed a null value.");
        }

        public DistributionListId getDistributionListId() {
            return (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
        }

        public String getName() {
            return (String) this.arguments.get("name");
        }
    }
}
