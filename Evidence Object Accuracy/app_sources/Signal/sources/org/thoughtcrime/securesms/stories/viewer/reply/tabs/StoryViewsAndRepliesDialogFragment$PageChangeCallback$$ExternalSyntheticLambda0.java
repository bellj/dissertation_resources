package org.thoughtcrime.securesms.stories.viewer.reply.tabs;

import org.thoughtcrime.securesms.stories.viewer.reply.tabs.StoryViewsAndRepliesDialogFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryViewsAndRepliesDialogFragment$PageChangeCallback$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ StoryViewsAndRepliesDialogFragment f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ StoryViewsAndRepliesDialogFragment$PageChangeCallback$$ExternalSyntheticLambda0(StoryViewsAndRepliesDialogFragment storyViewsAndRepliesDialogFragment, int i) {
        this.f$0 = storyViewsAndRepliesDialogFragment;
        this.f$1 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        StoryViewsAndRepliesDialogFragment.PageChangeCallback.m3188onPageSelected$lambda1(this.f$0, this.f$1);
    }
}
