package org.thoughtcrime.securesms.stories.viewer.reply.group;

import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryGroupReplyFragment$GroupDataObserver$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ StoryGroupReplyFragment f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ StoryGroupReplyFragment$GroupDataObserver$$ExternalSyntheticLambda0(StoryGroupReplyFragment storyGroupReplyFragment, int i) {
        this.f$0 = storyGroupReplyFragment;
        this.f$1 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        StoryGroupReplyFragment.GroupDataObserver.m3140onItemRangeInserted$lambda0(this.f$0, this.f$1);
    }
}
