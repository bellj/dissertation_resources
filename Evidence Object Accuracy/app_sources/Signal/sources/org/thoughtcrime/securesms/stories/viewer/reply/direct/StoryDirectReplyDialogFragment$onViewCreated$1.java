package org.thoughtcrime.securesms.stories.viewer.reply.direct;

import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.MediaKeyboard;
import org.thoughtcrime.securesms.keyboard.KeyboardPage;
import org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer;
import org.thoughtcrime.securesms.util.FragmentDialogs;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: StoryDirectReplyDialogFragment.kt */
@Metadata(d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\b\u0010\u0006\u001a\u00020\u0003H\u0016J\b\u0010\u0007\u001a\u00020\u0003H\u0016¨\u0006\b"}, d2 = {"org/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyDialogFragment$onViewCreated$1", "Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer$Callback;", "onInitializeEmojiDrawer", "", "mediaKeyboard", "Lorg/thoughtcrime/securesms/components/emoji/MediaKeyboard;", "onPickReactionClicked", "onSendActionClicked", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryDirectReplyDialogFragment$onViewCreated$1 implements StoryReplyComposer.Callback {
    final /* synthetic */ StoryDirectReplyDialogFragment this$0;

    public StoryDirectReplyDialogFragment$onViewCreated$1(StoryDirectReplyDialogFragment storyDirectReplyDialogFragment) {
        this.this$0 = storyDirectReplyDialogFragment;
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer.Callback
    public void onHideEmojiKeyboard() {
        StoryReplyComposer.Callback.DefaultImpls.onHideEmojiKeyboard(this);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer.Callback
    public void onShowEmojiKeyboard() {
        StoryReplyComposer.Callback.DefaultImpls.onShowEmojiKeyboard(this);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer.Callback
    public void onSendActionClicked() {
        LifecycleDisposable lifecycleDisposable = this.this$0.lifecycleDisposable;
        StoryDirectReplyViewModel storyDirectReplyViewModel = this.this$0.getViewModel();
        StoryReplyComposer storyReplyComposer = this.this$0.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        Disposable subscribe = storyDirectReplyViewModel.sendReply(storyReplyComposer.consumeInput().getFirst()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                StoryDirectReplyDialogFragment$onViewCreated$1.m3117onSendActionClicked$lambda0(StoryDirectReplyDialogFragment.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.sendReply(comp…ngStateLoss()\n          }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* renamed from: onSendActionClicked$lambda-0 */
    public static final void m3117onSendActionClicked$lambda0(StoryDirectReplyDialogFragment storyDirectReplyDialogFragment) {
        Intrinsics.checkNotNullParameter(storyDirectReplyDialogFragment, "this$0");
        Toast.makeText(storyDirectReplyDialogFragment.requireContext(), (int) R.string.StoryDirectReplyDialogFragment__sending_reply, 1).show();
        storyDirectReplyDialogFragment.dismissAllowingStateLoss();
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer.Callback
    public void onPickReactionClicked() {
        FragmentDialogs fragmentDialogs = FragmentDialogs.INSTANCE;
        StoryDirectReplyDialogFragment storyDirectReplyDialogFragment = this.this$0;
        StoryReplyComposer storyReplyComposer = storyDirectReplyDialogFragment.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        FragmentDialogs.displayInDialogAboveAnchor$default(fragmentDialogs, storyDirectReplyDialogFragment, storyReplyComposer.getReactionButton(), (int) R.layout.stories_reaction_bar_layout, 0.0f, new Function2<DialogInterface, View, Unit>(this.this$0) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1
            final /* synthetic */ StoryDirectReplyDialogFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((DialogInterface) obj, (View) obj2);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001a: INVOKE  
                  (r4v2 'storyReactionBar' org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1$1$1 : 0x0017: CONSTRUCTOR  (r1v0 org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1$1$1 A[REMOVE]) = 
                  (r3v0 'dialogInterface' android.content.DialogInterface)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment : 0x0011: IGET  (r0v3 org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment A[REMOVE]) = 
                  (r2v0 'this' org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1 A[IMMUTABLE_TYPE, THIS])
                 org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1.this$0 org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment)
                 call: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1$1$1.<init>(android.content.DialogInterface, org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment):void type: CONSTRUCTOR)
                 type: VIRTUAL call: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar.setCallback(org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar$Callback):void in method: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1.invoke(android.content.DialogInterface, android.view.View):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0017: CONSTRUCTOR  (r1v0 org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1$1$1 A[REMOVE]) = 
                  (r3v0 'dialogInterface' android.content.DialogInterface)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment : 0x0011: IGET  (r0v3 org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment A[REMOVE]) = 
                  (r2v0 'this' org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1 A[IMMUTABLE_TYPE, THIS])
                 org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1.this$0 org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment)
                 call: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1$1$1.<init>(android.content.DialogInterface, org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1.invoke(android.content.DialogInterface, android.view.View):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 10 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1$1$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 16 more
                */
            public final void invoke(android.content.DialogInterface r3, android.view.View r4) {
                /*
                    r2 = this;
                    java.lang.String r0 = "dialog"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r0)
                    java.lang.String r0 = "view"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r4, r0)
                    r0 = 2131364205(0x7f0a096d, float:1.834824E38)
                    android.view.View r4 = r4.findViewById(r0)
                    org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment r0 = r2.this$0
                    org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar r4 = (org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar) r4
                    org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1$1$1 r1 = new org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1$1$1
                    r1.<init>(r3, r0)
                    r4.setCallback(r1)
                    r4.animateIn()
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$onViewCreated$1$onPickReactionClicked$1.invoke(android.content.DialogInterface, android.view.View):void");
            }
        }, 4, (Object) null);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer.Callback
    public void onInitializeEmojiDrawer(MediaKeyboard mediaKeyboard) {
        Intrinsics.checkNotNullParameter(mediaKeyboard, "mediaKeyboard");
        this.this$0.getKeyboardPagerViewModel().setOnlyPage(KeyboardPage.EMOJI);
        mediaKeyboard.setFragmentManager(this.this$0.getChildFragmentManager());
    }
}
