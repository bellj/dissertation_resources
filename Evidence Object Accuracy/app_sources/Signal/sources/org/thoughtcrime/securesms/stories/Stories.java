package org.thoughtcrime.securesms.stories;

import android.content.Context;
import android.net.Uri;
import androidx.fragment.app.FragmentManager;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableEmitter;
import io.reactivex.rxjava3.core.CompletableOnSubscribe;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.LongIterator;
import kotlin.io.CloseableKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref$LongRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.ranges.LongRange;
import kotlin.ranges.RangesKt___RangesKt;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.contacts.HeaderAction;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.AttachmentDownloadJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.mms.OutgoingSecureMediaMessage;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.notifications.v2.MessageNotifierV2;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* compiled from: Stories.kt */
@Metadata(d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001*B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0003J\"\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u00142\b\b\u0002\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u0004H\u0007J\u000e\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aJ&\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001c2\u0006\u0010\u001e\u001a\u00020\u00062\u0006\u0010\u001f\u001a\u00020\u00062\u0006\u0010 \u001a\u00020\u000fH\u0007J\b\u0010!\u001a\u00020\u000fH\u0007J\b\u0010\"\u001a\u00020\u000fH\u0007J\u0010\u0010#\u001a\u00020\u00112\u0006\u0010$\u001a\u00020%H\u0007J\u0010\u0010#\u001a\u00020\u00112\u0006\u0010&\u001a\u00020\u0014H\u0007J\u0014\u0010'\u001a\u00020\u000b2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020)0\u001cR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0004¢\u0006\u0002\n\u0000¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/stories/Stories;", "", "()V", "MAX_BODY_SIZE", "", "MAX_VIDEO_DURATION_MILLIS", "", "TAG", "", "kotlin.jvm.PlatformType", "enqueueAttachmentsFromStoryForDownload", "Lio/reactivex/rxjava3/core/Completable;", "record", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "ignoreAutoDownloadConstraints", "", "enqueueAttachmentsFromStoryForDownloadSync", "", "enqueueNextStoriesForDownload", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "force", "limit", "getHeaderAction", "Lorg/thoughtcrime/securesms/contacts/HeaderAction;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "getRecipientsToSendTo", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "messageId", "sentTimestamp", "allowsReplies", "isFeatureAvailable", "isFeatureEnabled", "onStorySettingsChanged", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "storyRecipientId", "sendTextStories", MessageNotifierV2.NOTIFICATION_GROUP, "Lorg/thoughtcrime/securesms/mms/OutgoingSecureMediaMessage;", "MediaTransform", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class Stories {
    public static final Stories INSTANCE = new Stories();
    public static final int MAX_BODY_SIZE;
    public static final long MAX_VIDEO_DURATION_MILLIS = TimeUnit.SECONDS.toMillis(30);
    private static final String TAG = Log.tag(Stories.class);

    private Stories() {
    }

    @JvmStatic
    public static final boolean isFeatureAvailable() {
        return SignalStore.account().isRegistered() && FeatureFlags.stories() && Recipient.self().getStoriesCapability() == Recipient.Capability.SUPPORTED;
    }

    @JvmStatic
    public static final boolean isFeatureEnabled() {
        return isFeatureAvailable() && !SignalStore.storyValues().isFeatureDisabled();
    }

    public final HeaderAction getHeaderAction(FragmentManager fragmentManager) {
        Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
        return new HeaderAction(R.string.ContactsCursorLoader_new_story, R.drawable.ic_plus_20, new Runnable() { // from class: org.thoughtcrime.securesms.stories.Stories$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                Stories.m2757getHeaderAction$lambda0(FragmentManager.this);
            }
        });
    }

    /* renamed from: getHeaderAction$lambda-0 */
    public static final void m2757getHeaderAction$lambda0(FragmentManager fragmentManager) {
        Intrinsics.checkNotNullParameter(fragmentManager, "$fragmentManager");
        new ChooseStoryTypeBottomSheet().show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    public final Completable sendTextStories(List<? extends OutgoingSecureMediaMessage> list) {
        Intrinsics.checkNotNullParameter(list, MessageNotifierV2.NOTIFICATION_GROUP);
        Completable create = Completable.create(new CompletableOnSubscribe(list) { // from class: org.thoughtcrime.securesms.stories.Stories$$ExternalSyntheticLambda2
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.core.CompletableOnSubscribe
            public final void subscribe(CompletableEmitter completableEmitter) {
                Stories.m2758sendTextStories$lambda1(this.f$0, completableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create { emitter ->\n    …mitter.onComplete()\n    }");
        return create;
    }

    /* renamed from: sendTextStories$lambda-1 */
    public static final void m2758sendTextStories$lambda1(List list, CompletableEmitter completableEmitter) {
        Intrinsics.checkNotNullParameter(list, "$messages");
        MessageSender.sendStories(ApplicationDependencies.getApplication(), list, null, null);
        completableEmitter.onComplete();
    }

    @JvmStatic
    public static final List<Recipient> getRecipientsToSendTo(long j, long j2, boolean z) {
        List<RecipientId> recipientsToSendTo = SignalDatabase.Companion.storySends().getRecipientsToSendTo(j, j2, z);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(recipientsToSendTo, 10));
        for (RecipientId recipientId : recipientsToSendTo) {
            arrayList.add(Recipient.resolved(recipientId));
        }
        List<Recipient> eligibleForSending = RecipientUtil.getEligibleForSending(arrayList);
        Intrinsics.checkNotNullExpressionValue(eligibleForSending, "getEligibleForSending(re…map(Recipient::resolved))");
        return eligibleForSending;
    }

    public final void onStorySettingsChanged(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        RecipientId recipientId = SignalDatabase.Companion.distributionLists().getRecipientId(distributionListId);
        if (recipientId != null) {
            onStorySettingsChanged(recipientId);
            return;
        }
        throw new IllegalStateException("Cannot find recipient id for distribution list.".toString());
    }

    public final void onStorySettingsChanged(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "storyRecipientId");
        SignalDatabase.Companion.recipients().markNeedsSync(recipientId);
        StorageSyncHelper.scheduleSyncForDataChange();
    }

    public static /* synthetic */ void enqueueNextStoriesForDownload$default(RecipientId recipientId, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        enqueueNextStoriesForDownload(recipientId, z, i);
    }

    @JvmStatic
    public static final void enqueueNextStoriesForDownload(RecipientId recipientId, boolean z, int i) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        if (z || resolved.isSelf() || (!resolved.shouldHideStory() && resolved.hasViewedStory())) {
            String str = TAG;
            Log.d(str, "Enqueuing downloads for up to " + i + " stories for " + recipientId + " (force: " + z + ')');
            MessageDatabase.Reader unreadStories = SignalDatabase.Companion.mms().getUnreadStories(recipientId, i);
            th = null;
            try {
                Intrinsics.checkNotNullExpressionValue(unreadStories, "reader");
                for (MessageRecord messageRecord : unreadStories) {
                    Stories stories = INSTANCE;
                    if (messageRecord != null) {
                        stories.enqueueAttachmentsFromStoryForDownloadSync((MmsMessageRecord) messageRecord, false);
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.model.MmsMessageRecord");
                    }
                }
                Unit unit = Unit.INSTANCE;
            } catch (Throwable th) {
                try {
                    throw th;
                } finally {
                    CloseableKt.closeFinally(unreadStories, th);
                }
            }
        }
    }

    public final Completable enqueueAttachmentsFromStoryForDownload(MmsMessageRecord mmsMessageRecord, boolean z) {
        Intrinsics.checkNotNullParameter(mmsMessageRecord, "record");
        Completable subscribeOn = Completable.fromAction(new Action(z) { // from class: org.thoughtcrime.securesms.stories.Stories$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                Stories.m2756enqueueAttachmentsFromStoryForDownload$lambda4(MmsMessageRecord.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      enque…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: enqueueAttachmentsFromStoryForDownload$lambda-4 */
    public static final void m2756enqueueAttachmentsFromStoryForDownload$lambda4(MmsMessageRecord mmsMessageRecord, boolean z) {
        Intrinsics.checkNotNullParameter(mmsMessageRecord, "$record");
        INSTANCE.enqueueAttachmentsFromStoryForDownloadSync(mmsMessageRecord, z);
    }

    private final void enqueueAttachmentsFromStoryForDownloadSync(MmsMessageRecord mmsMessageRecord, boolean z) {
        List<DatabaseAttachment> attachmentsForMessage = SignalDatabase.Companion.attachments().getAttachmentsForMessage(mmsMessageRecord.getId());
        Intrinsics.checkNotNullExpressionValue(attachmentsForMessage, "SignalDatabase.attachmen…entsForMessage(record.id)");
        ArrayList<DatabaseAttachment> arrayList = new ArrayList();
        for (Object obj : attachmentsForMessage) {
            if (!((DatabaseAttachment) obj).isSticker()) {
                arrayList.add(obj);
            }
        }
        for (DatabaseAttachment databaseAttachment : arrayList) {
            ApplicationDependencies.getJobManager().add(new AttachmentDownloadJob(mmsMessageRecord.getId(), databaseAttachment.getAttachmentId(), z));
        }
        if (MessageRecordUtil.hasLinkPreview(mmsMessageRecord)) {
            ApplicationDependencies.getJobManager().add(new AttachmentDownloadJob(mmsMessageRecord.getId(), mmsMessageRecord.getLinkPreviews().get(0).getAttachmentId(), true));
        }
    }

    /* compiled from: Stories.kt */
    @Metadata(d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002 !B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0002J\u0010\u0010\n\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0007J\u0016\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\t0\f2\u0006\u0010\b\u001a\u00020\tH\u0007J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\b\u001a\u00020\tH\u0002J\u0016\u0010\u000f\u001a\u00020\u00102\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\fH\u0007J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\b\u001a\u00020\tH\u0007J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0007J\u000e\u0010\u0015\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tJ\u0018\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\b\u001a\u00020\tH\u0007J\u0018\u0010\u001a\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0018\u0010\u001d\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u00172\u0006\u0010\u001f\u001a\u00020\u0012H\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "canClipMedia", "", "media", "Lorg/thoughtcrime/securesms/mediasend/Media;", "canPreUploadMedia", "clipMediaToStoryDuration", "", "getContentDuration", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult;", "getSendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "getVideoDuration", "", "uri", "Landroid/net/Uri;", "hasHighQualityTransform", "mediaToVideoSlide", "Lorg/thoughtcrime/securesms/mms/VideoSlide;", "context", "Landroid/content/Context;", "transformMedia", "transformProperties", "Lorg/thoughtcrime/securesms/database/AttachmentDatabase$TransformProperties;", "videoSlideToMedia", "videoSlide", "duration", "DurationResult", "SendRequirements", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class MediaTransform {
        public static final MediaTransform INSTANCE = new MediaTransform();
        private static final String TAG = Log.tag(MediaTransform.class);

        /* compiled from: Stories.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "", "(Ljava/lang/String;I)V", "VALID_DURATION", "REQUIRES_CLIP", "CAN_NOT_SEND", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public enum SendRequirements {
            VALID_DURATION,
            REQUIRES_CLIP,
            CAN_NOT_SEND
        }

        private MediaTransform() {
        }

        /* compiled from: Stories.kt */
        @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0004\u0007\b\t\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult;", "", "()V", "CanNotGetDuration", "InvalidDuration", "None", "ValidDuration", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult$ValidDuration;", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult$InvalidDuration;", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult$CanNotGetDuration;", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult$None;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static abstract class DurationResult {
            public /* synthetic */ DurationResult(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private DurationResult() {
            }

            /* compiled from: Stories.kt */
            @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult$ValidDuration;", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult;", "duration", "", "(J)V", "getDuration", "()J", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
            /* loaded from: classes3.dex */
            public static final class ValidDuration extends DurationResult {
                private final long duration;

                public static /* synthetic */ ValidDuration copy$default(ValidDuration validDuration, long j, int i, Object obj) {
                    if ((i & 1) != 0) {
                        j = validDuration.duration;
                    }
                    return validDuration.copy(j);
                }

                public final long component1() {
                    return this.duration;
                }

                public final ValidDuration copy(long j) {
                    return new ValidDuration(j);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    return (obj instanceof ValidDuration) && this.duration == ((ValidDuration) obj).duration;
                }

                public int hashCode() {
                    return SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.duration);
                }

                public String toString() {
                    return "ValidDuration(duration=" + this.duration + ')';
                }

                public ValidDuration(long j) {
                    super(null);
                    this.duration = j;
                }

                public final long getDuration() {
                    return this.duration;
                }
            }

            /* compiled from: Stories.kt */
            @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult$InvalidDuration;", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult;", "duration", "", "(J)V", "getDuration", "()J", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
            /* loaded from: classes3.dex */
            public static final class InvalidDuration extends DurationResult {
                private final long duration;

                public static /* synthetic */ InvalidDuration copy$default(InvalidDuration invalidDuration, long j, int i, Object obj) {
                    if ((i & 1) != 0) {
                        j = invalidDuration.duration;
                    }
                    return invalidDuration.copy(j);
                }

                public final long component1() {
                    return this.duration;
                }

                public final InvalidDuration copy(long j) {
                    return new InvalidDuration(j);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    return (obj instanceof InvalidDuration) && this.duration == ((InvalidDuration) obj).duration;
                }

                public int hashCode() {
                    return SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.duration);
                }

                public String toString() {
                    return "InvalidDuration(duration=" + this.duration + ')';
                }

                public InvalidDuration(long j) {
                    super(null);
                    this.duration = j;
                }

                public final long getDuration() {
                    return this.duration;
                }
            }

            /* compiled from: Stories.kt */
            @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult$CanNotGetDuration;", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
            /* loaded from: classes3.dex */
            public static final class CanNotGetDuration extends DurationResult {
                public static final CanNotGetDuration INSTANCE = new CanNotGetDuration();

                private CanNotGetDuration() {
                    super(null);
                }
            }

            /* compiled from: Stories.kt */
            @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult$None;", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$DurationResult;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
            /* loaded from: classes3.dex */
            public static final class None extends DurationResult {
                public static final None INSTANCE = new None();

                private None() {
                    super(null);
                }
            }
        }

        @JvmStatic
        public static final boolean canPreUploadMedia(Media media) {
            Intrinsics.checkNotNullParameter(media, "media");
            if (MediaUtil.isVideo(media.getMimeType())) {
                if (getSendRequirements(media) != SendRequirements.REQUIRES_CLIP) {
                    return true;
                }
            } else if (!INSTANCE.hasHighQualityTransform(media)) {
                return true;
            }
            return false;
        }

        /* renamed from: hasHighQualityTransform$lambda-0 */
        public static final Boolean m2766hasHighQualityTransform$lambda0(AttachmentDatabase.TransformProperties transformProperties) {
            return Boolean.valueOf(transformProperties.getSentMediaQuality() == SentMediaQuality.HIGH.getCode());
        }

        public final boolean hasHighQualityTransform(Media media) {
            Intrinsics.checkNotNullParameter(media, "media");
            if (MediaUtil.isImageType(media.getMimeType())) {
                Object orElse = media.getTransformProperties().map(new Stories$MediaTransform$$ExternalSyntheticLambda1()).orElse(Boolean.FALSE);
                Intrinsics.checkNotNullExpressionValue(orElse, "media.transformPropertie…HIGH.code }.orElse(false)");
                if (((Boolean) orElse).booleanValue()) {
                    return true;
                }
            }
            return false;
        }

        @JvmStatic
        public static final SendRequirements getSendRequirements(Media media) {
            Intrinsics.checkNotNullParameter(media, "media");
            MediaTransform mediaTransform = INSTANCE;
            DurationResult contentDuration = mediaTransform.getContentDuration(media);
            if (contentDuration instanceof DurationResult.ValidDuration) {
                return SendRequirements.VALID_DURATION;
            }
            if (contentDuration instanceof DurationResult.InvalidDuration) {
                if (mediaTransform.canClipMedia(media)) {
                    return SendRequirements.REQUIRES_CLIP;
                }
                return SendRequirements.CAN_NOT_SEND;
            } else if (contentDuration instanceof DurationResult.CanNotGetDuration) {
                return SendRequirements.CAN_NOT_SEND;
            } else {
                if (contentDuration instanceof DurationResult.None) {
                    return SendRequirements.VALID_DURATION;
                }
                throw new NoWhenBranchMatchedException();
            }
        }

        private final boolean canClipMedia(Media media) {
            return MediaUtil.isVideo(media.getMimeType()) && MediaConstraints.isVideoTranscodeAvailable();
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x007d  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0080  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private final org.thoughtcrime.securesms.stories.Stories.MediaTransform.DurationResult getContentDuration(org.thoughtcrime.securesms.mediasend.Media r9) {
            /*
                r8 = this;
                java.lang.String r0 = r9.getMimeType()
                boolean r0 = org.thoughtcrime.securesms.util.MediaUtil.isVideo(r0)
                if (r0 == 0) goto L_0x0092
                long r0 = r9.getDuration()
                r2 = 0
                int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r4 != 0) goto L_0x0031
                j$.util.Optional r0 = r9.getTransformProperties()
                java.lang.String r1 = "media.transformProperties"
                kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
                boolean r0 = org.signal.core.util.OptionalExtensionsKt.isAbsent(r0)
                if (r0 == 0) goto L_0x0031
                android.net.Uri r9 = r9.getUri()
                java.lang.String r0 = "media.uri"
                kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r9, r0)
                long r0 = getVideoDuration(r9)
                goto L_0x0079
            L_0x0031:
                j$.util.Optional r0 = r9.getTransformProperties()
                org.thoughtcrime.securesms.stories.Stories$MediaTransform$$ExternalSyntheticLambda0 r1 = new org.thoughtcrime.securesms.stories.Stories$MediaTransform$$ExternalSyntheticLambda0
                r1.<init>()
                j$.util.Optional r0 = r0.map(r1)
                java.lang.Boolean r1 = java.lang.Boolean.FALSE
                java.lang.Object r0 = r0.orElse(r1)
                java.lang.String r1 = "media.transformPropertie…VideoTrim }.orElse(false)"
                kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
                java.lang.Boolean r0 = (java.lang.Boolean) r0
                boolean r0 = r0.booleanValue()
                if (r0 == 0) goto L_0x0075
                java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.MICROSECONDS
                j$.util.Optional r1 = r9.getTransformProperties()
                java.lang.Object r1 = r1.get()
                org.thoughtcrime.securesms.database.AttachmentDatabase$TransformProperties r1 = (org.thoughtcrime.securesms.database.AttachmentDatabase.TransformProperties) r1
                long r4 = r1.getVideoTrimEndTimeUs()
                j$.util.Optional r9 = r9.getTransformProperties()
                java.lang.Object r9 = r9.get()
                org.thoughtcrime.securesms.database.AttachmentDatabase$TransformProperties r9 = (org.thoughtcrime.securesms.database.AttachmentDatabase.TransformProperties) r9
                long r6 = r9.getVideoTrimStartTimeUs()
                long r4 = r4 - r6
                long r0 = r0.toMillis(r4)
                goto L_0x0079
            L_0x0075:
                long r0 = r9.getDuration()
            L_0x0079:
                int r9 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r9 > 0) goto L_0x0080
                org.thoughtcrime.securesms.stories.Stories$MediaTransform$DurationResult$CanNotGetDuration r9 = org.thoughtcrime.securesms.stories.Stories.MediaTransform.DurationResult.CanNotGetDuration.INSTANCE
                goto L_0x0091
            L_0x0080:
                long r2 = org.thoughtcrime.securesms.stories.Stories.MAX_VIDEO_DURATION_MILLIS
                int r9 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r9 <= 0) goto L_0x008c
                org.thoughtcrime.securesms.stories.Stories$MediaTransform$DurationResult$InvalidDuration r9 = new org.thoughtcrime.securesms.stories.Stories$MediaTransform$DurationResult$InvalidDuration
                r9.<init>(r0)
                goto L_0x0091
            L_0x008c:
                org.thoughtcrime.securesms.stories.Stories$MediaTransform$DurationResult$ValidDuration r9 = new org.thoughtcrime.securesms.stories.Stories$MediaTransform$DurationResult$ValidDuration
                r9.<init>(r0)
            L_0x0091:
                return r9
            L_0x0092:
                org.thoughtcrime.securesms.stories.Stories$MediaTransform$DurationResult$None r9 = org.thoughtcrime.securesms.stories.Stories.MediaTransform.DurationResult.None.INSTANCE
                return r9
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.Stories.MediaTransform.getContentDuration(org.thoughtcrime.securesms.mediasend.Media):org.thoughtcrime.securesms.stories.Stories$MediaTransform$DurationResult");
        }

        /* renamed from: getContentDuration$lambda-3 */
        public static final Boolean m2763getContentDuration$lambda3(AttachmentDatabase.TransformProperties transformProperties) {
            return Boolean.valueOf(transformProperties.isVideoTrim());
        }

        @JvmStatic
        public static final long getVideoDuration(Uri uri) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            Ref$LongRef ref$LongRef = new Ref$LongRef();
            Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            CountDownLatch countDownLatch = new CountDownLatch(1);
            ThreadUtil.runOnMainSync(new Stories$MediaTransform$$ExternalSyntheticLambda4(countDownLatch, uri, ref$ObjectRef, ref$LongRef));
            countDownLatch.await();
            ThreadUtil.runOnMainSync(new Stories$MediaTransform$$ExternalSyntheticLambda5(ref$ObjectRef));
            return Math.max(ref$LongRef.element, 0L);
        }

        /* JADX WARN: Type inference failed for: r0v6, types: [T, com.google.android.exoplayer2.BasePlayer, com.google.android.exoplayer2.SimpleExoPlayer] */
        /* renamed from: getVideoDuration$lambda-4 */
        public static final void m2764getVideoDuration$lambda4(CountDownLatch countDownLatch, Uri uri, Ref$ObjectRef ref$ObjectRef, Ref$LongRef ref$LongRef) {
            Intrinsics.checkNotNullParameter(countDownLatch, "$countDownLatch");
            Intrinsics.checkNotNullParameter(uri, "$uri");
            Intrinsics.checkNotNullParameter(ref$ObjectRef, "$player");
            Intrinsics.checkNotNullParameter(ref$LongRef, "$duration");
            SimpleExoPlayer simpleExoPlayer = ApplicationDependencies.getExoPlayerPool().get("stories_duration_check");
            if (simpleExoPlayer == 0) {
                Log.w(TAG, "Could not get a player from the pool, so we cannot get the length of the video.");
                countDownLatch.countDown();
                return;
            }
            simpleExoPlayer.addListener((Player.Listener) new Stories$MediaTransform$getVideoDuration$1$1(ref$LongRef, simpleExoPlayer, countDownLatch));
            simpleExoPlayer.setMediaItem(MediaItem.fromUri(uri));
            simpleExoPlayer.prepare();
            ref$ObjectRef.element = simpleExoPlayer;
        }

        /* renamed from: getVideoDuration$lambda-5 */
        public static final void m2765getVideoDuration$lambda5(Ref$ObjectRef ref$ObjectRef) {
            Intrinsics.checkNotNullParameter(ref$ObjectRef, "$player");
            SimpleExoPlayer simpleExoPlayer = (SimpleExoPlayer) ref$ObjectRef.element;
            if (simpleExoPlayer != null) {
                ApplicationDependencies.getExoPlayerPool().pool(simpleExoPlayer);
            }
        }

        @JvmStatic
        public static final List<Media> clipMediaToStoryDuration(Media media) {
            Intrinsics.checkNotNullParameter(media, "media");
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            long micros = timeUnit.toMicros(Stories.MAX_VIDEO_DURATION_MILLIS);
            long j = 0;
            Long l = (Long) media.getTransformProperties().map(new Stories$MediaTransform$$ExternalSyntheticLambda2()).orElse(0L);
            Optional<U> map = media.getTransformProperties().map(new Stories$MediaTransform$$ExternalSyntheticLambda3());
            Uri uri = media.getUri();
            Intrinsics.checkNotNullExpressionValue(uri, "media.uri");
            Long l2 = (Long) map.orElse(Long.valueOf(timeUnit.toMicros(getVideoDuration(uri))));
            long longValue = l2.longValue();
            Intrinsics.checkNotNullExpressionValue(l, "startOffsetUs");
            long longValue2 = longValue - l.longValue();
            if (longValue2 <= 0) {
                return CollectionsKt__CollectionsKt.emptyList();
            }
            long j2 = longValue2 / micros;
            long j3 = longValue2 % micros;
            if (j3 + ((((j3 ^ micros) & ((-j3) | j3)) >> 63) & micros) != 0) {
                j = 1;
            }
            LongRange longRange = RangesKt___RangesKt.until(0, j2 + j);
            ArrayList<AttachmentDatabase.TransformProperties> arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(longRange, 10));
            Iterator<Long> it = longRange.iterator();
            while (it.hasNext()) {
                long nextLong = ((LongIterator) it).nextLong();
                long longValue3 = (nextLong * micros) + l.longValue();
                Intrinsics.checkNotNullExpressionValue(l2, "endOffsetUs");
                long min = Math.min(longValue3 + micros, l2.longValue());
                if (longValue3 <= min) {
                    arrayList.add(new AttachmentDatabase.TransformProperties(false, true, longValue3, min, SentMediaQuality.STANDARD.getCode()));
                } else {
                    throw new IllegalStateException(("Illegal clip: " + longValue3 + " > " + min + " for clip " + nextLong).toString());
                }
            }
            ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
            for (AttachmentDatabase.TransformProperties transformProperties : arrayList) {
                arrayList2.add(INSTANCE.transformMedia(media, transformProperties));
            }
            return arrayList2;
        }

        /* renamed from: clipMediaToStoryDuration$lambda-6 */
        public static final Long m2761clipMediaToStoryDuration$lambda6(AttachmentDatabase.TransformProperties transformProperties) {
            return Long.valueOf(transformProperties.getVideoTrimStartTimeUs());
        }

        /* renamed from: clipMediaToStoryDuration$lambda-7 */
        public static final Long m2762clipMediaToStoryDuration$lambda7(AttachmentDatabase.TransformProperties transformProperties) {
            return Long.valueOf(transformProperties.getVideoTrimEndTimeUs());
        }

        private final Media transformMedia(Media media, AttachmentDatabase.TransformProperties transformProperties) {
            return new Media(media.getUri(), media.getMimeType(), media.getDate(), media.getWidth(), media.getHeight(), media.getSize(), media.getDuration(), media.isBorderless(), media.isVideoGif(), media.getBucketId(), media.getCaption(), Optional.of(transformProperties));
        }

        @JvmStatic
        public static final VideoSlide mediaToVideoSlide(Context context, Media media) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(media, "media");
            return new VideoSlide(context, media.getUri(), media.getSize(), media.isVideoGif(), media.getWidth(), media.getHeight(), media.getCaption().orElse(null), media.getTransformProperties().orElse(null));
        }

        @JvmStatic
        public static final Media videoSlideToMedia(VideoSlide videoSlide, long j) {
            Intrinsics.checkNotNullParameter(videoSlide, "videoSlide");
            Uri uri = videoSlide.getUri();
            Intrinsics.checkNotNull(uri);
            return new Media(uri, videoSlide.getContentType(), System.currentTimeMillis(), 0, 0, videoSlide.getFileSize(), j, videoSlide.isBorderless(), videoSlide.isVideoGif(), Optional.empty(), videoSlide.getCaption(), Optional.empty());
        }

        @JvmStatic
        public static final SendRequirements getSendRequirements(List<? extends Media> list) {
            Intrinsics.checkNotNullParameter(list, "media");
            ArrayList<SendRequirements> arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
            for (Media media : list) {
                arrayList.add(getSendRequirements(media));
            }
            SendRequirements sendRequirements = SendRequirements.VALID_DURATION;
            for (SendRequirements sendRequirements2 : arrayList) {
                SendRequirements sendRequirements3 = SendRequirements.CAN_NOT_SEND;
                sendRequirements = (sendRequirements == sendRequirements3 || sendRequirements2 == sendRequirements3 || sendRequirements == (sendRequirements3 = SendRequirements.REQUIRES_CLIP) || sendRequirements2 == sendRequirements3) ? sendRequirements3 : SendRequirements.VALID_DURATION;
            }
            return sendRequirements;
        }
    }
}
