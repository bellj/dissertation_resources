package org.thoughtcrime.securesms.stories.viewer;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleEmitter;
import io.reactivex.rxjava3.core.SingleOnSubscribe;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.StoryResult;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: StoryViewerRepository.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010 \n\u0002\b\u0004\b\u0016\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J$\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bJ*\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\r0\u00042\u0006\u0010\u000e\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\tJ\"\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\r*\b\u0012\u0004\u0012\u00020\u00070\r2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0002¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerRepository;", "", "()V", "getFirstStory", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "unviewedOnly", "", "storyId", "", "getStories", "", "hiddenStories", "isOutgoingOnly", "floatToTop", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public class StoryViewerRepository {
    public final Single<MmsMessageRecord> getFirstStory(RecipientId recipientId, boolean z, long j) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        if (j > 0) {
            Single<MmsMessageRecord> fromCallable = Single.fromCallable(new Callable(j) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerRepository$$ExternalSyntheticLambda0
                public final /* synthetic */ long f$0;

                {
                    this.f$0 = r1;
                }

                @Override // java.util.concurrent.Callable
                public final Object call() {
                    return StoryViewerRepository.$r8$lambda$Rj_ao_GD042FLuEhOL6IMJ4yFbc(this.f$0);
                }
            });
            Intrinsics.checkNotNullExpressionValue(fromCallable, "{\n      Single.fromCalla…ssageRecord\n      }\n    }");
            return fromCallable;
        }
        Single<MmsMessageRecord> fromCallable2 = Single.fromCallable(new Callable(z) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return StoryViewerRepository.$r8$lambda$Ke1RIKIwBMCqKCVCE6RMJXF2xjQ(RecipientId.this, this.f$1);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable2, "{\n      Single.fromCalla…ssageRecord\n      }\n    }");
        return fromCallable2;
    }

    /* renamed from: getFirstStory$lambda-0 */
    public static final MmsMessageRecord m3004getFirstStory$lambda0(long j) {
        MessageRecord messageRecord = SignalDatabase.Companion.mms().getMessageRecord(j);
        if (messageRecord != null) {
            return (MmsMessageRecord) messageRecord;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.model.MmsMessageRecord");
    }

    /* renamed from: getFirstStory$lambda-2 */
    public static final MmsMessageRecord m3005getFirstStory$lambda2(RecipientId recipientId, boolean z) {
        MessageDatabase.Reader reader;
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        if (resolved.isMyStory() || resolved.isSelf()) {
            reader = SignalDatabase.Companion.mms().getAllOutgoingStories(false, 1);
            Intrinsics.checkNotNullExpressionValue(reader, "{\n          SignalDataba…ories(false, 1)\n        }");
        } else if (z) {
            reader = SignalDatabase.Companion.mms().getUnreadStories(recipientId, 1);
            Intrinsics.checkNotNullExpressionValue(reader, "{\n          SignalDataba…recipientId, 1)\n        }");
        } else {
            reader = SignalDatabase.Companion.mms().getAllStoriesFor(recipientId, 1);
            Intrinsics.checkNotNullExpressionValue(reader, "{\n          SignalDataba…recipientId, 1)\n        }");
        }
        th = null;
        try {
            MessageRecord next = reader.getNext();
            if (next != null) {
                return (MmsMessageRecord) next;
            }
            throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.model.MmsMessageRecord");
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final Single<List<RecipientId>> getStories(boolean z, boolean z2, boolean z3) {
        Single<List<RecipientId>> subscribeOn = Single.create(new SingleOnSubscribe(z3, this, z, z2) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ boolean f$0;
            public final /* synthetic */ StoryViewerRepository f$1;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // io.reactivex.rxjava3.core.SingleOnSubscribe
            public final void subscribe(SingleEmitter singleEmitter) {
                StoryViewerRepository.m3003$r8$lambda$ErMyrzK3szJkz_eDqrjIacEGPI(this.f$0, this.f$1, this.f$2, this.f$3, singleEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create<List<RecipientId>…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getStories$lambda-7 */
    public static final void m3006getStories$lambda7(boolean z, StoryViewerRepository storyViewerRepository, boolean z2, boolean z3, SingleEmitter singleEmitter) {
        Intrinsics.checkNotNullParameter(storyViewerRepository, "this$0");
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        RecipientDatabase recipients = companion.recipients();
        DistributionListId distributionListId = DistributionListId.MY_STORY;
        Intrinsics.checkNotNullExpressionValue(distributionListId, "MY_STORY");
        RecipientId orInsertFromDistributionListId$default = RecipientDatabase.getOrInsertFromDistributionListId$default(recipients, distributionListId, null, 2, null);
        Recipient resolved = Recipient.resolved(orInsertFromDistributionListId$default);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(myStoriesId)");
        RecipientId releaseChannelRecipientId = SignalStore.releaseChannelValues().getReleaseChannelRecipientId();
        List<StoryResult> orderedStoryRecipientsAndIds = companion.mms().getOrderedStoryRecipientsAndIds(z);
        Intrinsics.checkNotNullExpressionValue(orderedStoryRecipientsAndIds, "SignalDatabase.mms.getOr…ntsAndIds(isOutgoingOnly)");
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : orderedStoryRecipientsAndIds) {
            Recipient resolved2 = Recipient.resolved(((StoryResult) obj).getRecipientId());
            Intrinsics.checkNotNullExpressionValue(resolved2, "resolved(it.recipientId)");
            if (resolved2.isDistributionList()) {
                resolved2 = resolved;
            }
            Object obj2 = linkedHashMap.get(resolved2);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(resolved2, obj2);
            }
            ((List) obj2).add(obj);
        }
        Set keySet = linkedHashMap.keySet();
        ArrayList arrayList = new ArrayList();
        Iterator it = keySet.iterator();
        while (true) {
            boolean z4 = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            Recipient recipient = (Recipient) next;
            if (z2) {
                z4 = recipient.shouldHideStory();
            } else if (recipient.shouldHideStory()) {
                z4 = false;
            }
            if (z4) {
                arrayList.add(next);
            }
        }
        ArrayList<Recipient> arrayList2 = new ArrayList();
        for (Object obj3 : arrayList) {
            Recipient recipient2 = (Recipient) obj3;
            if (!z3 || (!recipient2.isSelf() && !recipient2.isMyStory() && SignalDatabase.Companion.mms().getStoryViewState(recipient2.getId()) == StoryViewState.UNVIEWED)) {
                arrayList2.add(obj3);
            }
        }
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList2, 10));
        for (Recipient recipient3 : arrayList2) {
            arrayList3.add(recipient3.getId());
        }
        singleEmitter.onSuccess(storyViewerRepository.floatToTop(storyViewerRepository.floatToTop(arrayList3, releaseChannelRecipientId), orInsertFromDistributionListId$default));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
    /* JADX WARN: Multi-variable type inference failed */
    private final List<RecipientId> floatToTop(List<? extends RecipientId> list, RecipientId recipientId) {
        return (recipientId == null || !list.contains(recipientId)) ? list : CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf(recipientId), (Iterable) CollectionsKt___CollectionsKt.minus(list, recipientId));
    }
}
