package org.thoughtcrime.securesms.stories.settings.my;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes3.dex */
public class MyStorySettingsFragmentDirections {
    private MyStorySettingsFragmentDirections() {
    }

    public static NavDirections actionMyStorySettingsToSignalConnectionsBottomSheet() {
        return new ActionOnlyNavDirections(R.id.action_myStorySettings_to_signalConnectionsBottomSheet);
    }

    public static NavDirections actionMyStorySettingsToAllExceptFragment() {
        return new ActionOnlyNavDirections(R.id.action_myStorySettings_to_allExceptFragment);
    }

    public static NavDirections actionMyStorySettingsToOnlyShareWithFragment() {
        return new ActionOnlyNavDirections(R.id.action_myStorySettings_to_onlyShareWithFragment);
    }
}
