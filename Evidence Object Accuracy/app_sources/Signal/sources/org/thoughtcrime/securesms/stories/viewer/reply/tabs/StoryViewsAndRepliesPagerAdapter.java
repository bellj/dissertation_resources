package org.thoughtcrime.securesms.stories.viewer.reply.tabs;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment;
import org.thoughtcrime.securesms.stories.viewer.views.StoryViewsFragment;

/* compiled from: StoryViewsAndRepliesPagerAdapter.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000bH\u0016J\b\u0010\u000f\u001a\u00020\u000bH\u0016J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/tabs/StoryViewsAndRepliesPagerAdapter;", "Landroidx/viewpager2/adapter/FragmentStateAdapter;", "fragment", "Landroidx/fragment/app/Fragment;", "storyId", "", "groupRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "isFromNotification", "", "groupReplyStartPosition", "", "(Landroidx/fragment/app/Fragment;JLorg/thoughtcrime/securesms/recipients/RecipientId;ZI)V", "createFragment", "position", "getItemCount", "onAttachedToRecyclerView", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewsAndRepliesPagerAdapter extends FragmentStateAdapter {
    private final RecipientId groupRecipientId;
    private final int groupReplyStartPosition;
    private final boolean isFromNotification;
    private final long storyId;

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return 2;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryViewsAndRepliesPagerAdapter(Fragment fragment, long j, RecipientId recipientId, boolean z, int i) {
        super(fragment);
        Intrinsics.checkNotNullParameter(fragment, "fragment");
        Intrinsics.checkNotNullParameter(recipientId, "groupRecipientId");
        this.storyId = j;
        this.groupRecipientId = recipientId;
        this.isFromNotification = z;
        this.groupReplyStartPosition = i;
    }

    @Override // androidx.viewpager2.adapter.FragmentStateAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        super.onAttachedToRecyclerView(recyclerView);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override // androidx.viewpager2.adapter.FragmentStateAdapter
    public Fragment createFragment(int i) {
        if (i == 0) {
            return StoryViewsFragment.Companion.create(this.storyId);
        }
        if (i == 1) {
            return StoryGroupReplyFragment.Companion.create(this.storyId, this.groupRecipientId, this.isFromNotification, this.groupReplyStartPosition);
        }
        throw new IndexOutOfBoundsException();
    }
}
