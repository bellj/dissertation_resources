package org.thoughtcrime.securesms.stories.settings.story;

import android.view.View;
import org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class PrivateStoryItem$NewViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ PrivateStoryItem.NewModel f$0;

    public /* synthetic */ PrivateStoryItem$NewViewHolder$$ExternalSyntheticLambda0(PrivateStoryItem.NewModel newModel) {
        this.f$0 = newModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        PrivateStoryItem.NewViewHolder.m2956bind$lambda0(this.f$0, view);
    }
}
