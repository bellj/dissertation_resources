package org.thoughtcrime.securesms.stories.settings.custom.name;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.DistributionListId;

/* compiled from: EditStoryNameViewModel.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/name/EditStoryNameViewModel;", "Landroidx/lifecycle/ViewModel;", "privateStoryId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/custom/name/EditStoryNameRepository;", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;Lorg/thoughtcrime/securesms/stories/settings/custom/name/EditStoryNameRepository;)V", "save", "Lio/reactivex/rxjava3/core/Completable;", "name", "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class EditStoryNameViewModel extends ViewModel {
    private final DistributionListId privateStoryId;
    private final EditStoryNameRepository repository;

    public EditStoryNameViewModel(DistributionListId distributionListId, EditStoryNameRepository editStoryNameRepository) {
        Intrinsics.checkNotNullParameter(distributionListId, "privateStoryId");
        Intrinsics.checkNotNullParameter(editStoryNameRepository, "repository");
        this.privateStoryId = distributionListId;
        this.repository = editStoryNameRepository;
    }

    public final Completable save(CharSequence charSequence) {
        Intrinsics.checkNotNullParameter(charSequence, "name");
        Completable observeOn = this.repository.save(this.privateStoryId, charSequence).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.save(privateS…dSchedulers.mainThread())");
        return observeOn;
    }

    /* compiled from: EditStoryNameViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/name/EditStoryNameViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "privateStoryId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/custom/name/EditStoryNameRepository;", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;Lorg/thoughtcrime/securesms/stories/settings/custom/name/EditStoryNameRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final DistributionListId privateStoryId;
        private final EditStoryNameRepository repository;

        public Factory(DistributionListId distributionListId, EditStoryNameRepository editStoryNameRepository) {
            Intrinsics.checkNotNullParameter(distributionListId, "privateStoryId");
            Intrinsics.checkNotNullParameter(editStoryNameRepository, "repository");
            this.privateStoryId = distributionListId;
            this.repository = editStoryNameRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new EditStoryNameViewModel(this.privateStoryId, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameViewModel.Factory.create");
        }
    }
}
