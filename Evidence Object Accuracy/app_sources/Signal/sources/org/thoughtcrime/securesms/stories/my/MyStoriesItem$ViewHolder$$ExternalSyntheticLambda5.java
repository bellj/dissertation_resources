package org.thoughtcrime.securesms.stories.my;

import android.view.View;
import org.thoughtcrime.securesms.stories.my.MyStoriesItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class MyStoriesItem$ViewHolder$$ExternalSyntheticLambda5 implements View.OnClickListener {
    public final /* synthetic */ MyStoriesItem.Model f$0;
    public final /* synthetic */ MyStoriesItem.ViewHolder f$1;

    public /* synthetic */ MyStoriesItem$ViewHolder$$ExternalSyntheticLambda5(MyStoriesItem.Model model, MyStoriesItem.ViewHolder viewHolder) {
        this.f$0 = model;
        this.f$1 = viewHolder;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        MyStoriesItem.ViewHolder.m2840bind$lambda2(this.f$0, this.f$1, view);
    }
}
