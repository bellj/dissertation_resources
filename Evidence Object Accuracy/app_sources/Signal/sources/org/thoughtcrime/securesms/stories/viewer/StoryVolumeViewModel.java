package org.thoughtcrime.securesms.stories.viewer;

import androidx.lifecycle.ViewModel;
import io.reactivex.rxjava3.core.Flowable;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: StoryVolumeViewModel.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011J\u000e\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011J\u0006\u0010\u0013\u001a\u00020\u000eR\u0011\u0010\u0003\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryVolumeViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "snapshot", "Lorg/thoughtcrime/securesms/stories/viewer/StoryVolumeState;", "getSnapshot", "()Lorg/thoughtcrime/securesms/stories/viewer/StoryVolumeState;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "mute", "", "onVolumeDown", "level", "", "onVolumeUp", "unmute", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryVolumeViewModel extends ViewModel {
    private final Flowable<StoryVolumeState> state;
    private final RxStore<StoryVolumeState> store;

    public StoryVolumeViewModel() {
        RxStore<StoryVolumeState> rxStore = new RxStore<>(new StoryVolumeState(false, 0, 3, null), null, 2, null);
        this.store = rxStore;
        this.state = rxStore.getStateFlowable();
    }

    public final Flowable<StoryVolumeState> getState() {
        return this.state;
    }

    public final StoryVolumeState getSnapshot() {
        return this.store.getState();
    }

    public final void mute() {
        this.store.update(StoryVolumeViewModel$mute$1.INSTANCE);
    }

    public final void unmute() {
        this.store.update(StoryVolumeViewModel$unmute$1.INSTANCE);
    }

    public final void onVolumeDown(int i) {
        this.store.update(new Function1<StoryVolumeState, StoryVolumeState>(i) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryVolumeViewModel$onVolumeDown$1
            final /* synthetic */ int $level;

            /* access modifiers changed from: package-private */
            {
                this.$level = r1;
            }

            public final StoryVolumeState invoke(StoryVolumeState storyVolumeState) {
                Intrinsics.checkNotNullParameter(storyVolumeState, "it");
                return StoryVolumeState.copy$default(storyVolumeState, false, this.$level, 1, null);
            }
        });
    }

    public final void onVolumeUp(int i) {
        this.store.update(new Function1<StoryVolumeState, StoryVolumeState>(i) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryVolumeViewModel$onVolumeUp$1
            final /* synthetic */ int $level;

            /* access modifiers changed from: package-private */
            {
                this.$level = r1;
            }

            public final StoryVolumeState invoke(StoryVolumeState storyVolumeState) {
                Intrinsics.checkNotNullParameter(storyVolumeState, "it");
                return storyVolumeState.copy(false, this.$level);
            }
        });
    }
}
