package org.thoughtcrime.securesms.stories.settings.privacy;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.WrapperDialogFragment;

/* compiled from: ChangeMyStoryMembershipFragment.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u0000 \u00072\u00020\u0001:\u0002\u0007\bB\u0005¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/privacy/OnlyShareWithFragment;", "Lorg/thoughtcrime/securesms/stories/settings/privacy/ChangeMyStoryMembershipFragment;", "()V", "toolbarTitleId", "", "getToolbarTitleId", "()I", "Companion", "Dialog", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class OnlyShareWithFragment extends ChangeMyStoryMembershipFragment {
    public static final Companion Companion = new Companion(null);
    private final int toolbarTitleId = R.string.ChangeMyStoryMembershipFragment__only_share_with;

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment
    protected int getToolbarTitleId() {
        return this.toolbarTitleId;
    }

    /* compiled from: ChangeMyStoryMembershipFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/privacy/OnlyShareWithFragment$Dialog;", "Lorg/thoughtcrime/securesms/components/WrapperDialogFragment;", "()V", "getWrappedFragment", "Landroidx/fragment/app/Fragment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Dialog extends WrapperDialogFragment {
        @Override // org.thoughtcrime.securesms.components.WrapperDialogFragment
        public Fragment getWrappedFragment() {
            return new OnlyShareWithFragment();
        }
    }

    /* compiled from: ChangeMyStoryMembershipFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/privacy/OnlyShareWithFragment$Companion;", "", "()V", "createAsDialog", "Landroidx/fragment/app/DialogFragment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final DialogFragment createAsDialog() {
            return new Dialog();
        }
    }
}
