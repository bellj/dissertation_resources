package org.thoughtcrime.securesms.stories.settings.privacy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import com.google.android.material.radiobutton.MaterialRadioButton;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;
import org.thoughtcrime.securesms.components.WrapperDialogFragment;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: ChooseInitialMyStoryMembershipBottomSheetDialogFragment.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 %2\u00020\u00012\u00020\u00022\u00020\u0003:\u0002$%B\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u0018\u001a\u00020\u0019H\u0016J&\u0010\u001a\u001a\u0004\u0018\u00010\n2\u0006\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\u001a\u0010!\u001a\u00020\u00192\u0006\u0010\"\u001a\u00020\n2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\b\u0010#\u001a\u00020\u0019H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0012\u001a\u00020\u00138BX\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0014\u0010\u0015¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/components/FixedRoundedCornerBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/components/WrapperDialogFragment$WrapperDialogFragmentCallback;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionFragment$Callback;", "()V", "allExceptCount", "Landroid/widget/TextView;", "allExceptRadio", "Lcom/google/android/material/radiobutton/MaterialRadioButton;", "allExceptRow", "Landroid/view/View;", "allRadio", "allRow", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "onlyWitRadio", "onlyWitRow", "onlyWithCount", "viewModel", "Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "exitFlow", "", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "onWrapperDialogFragmentDismissed", "Callback", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ChooseInitialMyStoryMembershipBottomSheetDialogFragment extends FixedRoundedCornerBottomSheetDialogFragment implements WrapperDialogFragment.WrapperDialogFragmentCallback, BaseStoryRecipientSelectionFragment.Callback {
    public static final Companion Companion = new Companion(null);
    private static final String SELECTION_FRAGMENT;
    private TextView allExceptCount;
    private MaterialRadioButton allExceptRadio;
    private View allExceptRow;
    private MaterialRadioButton allRadio;
    private View allRow;
    private LifecycleDisposable lifecycleDisposable;
    private MaterialRadioButton onlyWitRadio;
    private View onlyWitRow;
    private TextView onlyWithCount;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(ChooseInitialMyStoryMembershipViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    /* compiled from: ChooseInitialMyStoryMembershipBottomSheetDialogFragment.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipBottomSheetDialogFragment$Callback;", "", "onMyStoryConfigured", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onMyStoryConfigured(RecipientId recipientId);
    }

    /* compiled from: ChooseInitialMyStoryMembershipBottomSheetDialogFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[DistributionListPrivacyMode.values().length];
            iArr[DistributionListPrivacyMode.ALL_EXCEPT.ordinal()] = 1;
            iArr[DistributionListPrivacyMode.ONLY_WITH.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    @Override // org.thoughtcrime.securesms.components.WrapperDialogFragment.WrapperDialogFragmentCallback
    public void onWrapperDialogFragmentDismissed() {
    }

    public final ChooseInitialMyStoryMembershipViewModel getViewModel() {
        return (ChooseInitialMyStoryMembershipViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.choose_initial_my_story_membership_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.choose_initial_my_story_all_signal_connnections_row);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.c…_signal_connnections_row)");
        this.allRow = findViewById;
        View findViewById2 = view.findViewById(R.id.choose_initial_my_story_all_signal_connnections_except_row);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.c…_connnections_except_row)");
        this.allExceptRow = findViewById2;
        View findViewById3 = view.findViewById(R.id.choose_initial_my_story_only_share_with_row);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.c…tory_only_share_with_row)");
        this.onlyWitRow = findViewById3;
        View findViewById4 = view.findViewById(R.id.choose_initial_my_story_all_signal_connnections_radio);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.c…ignal_connnections_radio)");
        this.allRadio = (MaterialRadioButton) findViewById4;
        View findViewById5 = view.findViewById(R.id.choose_initial_my_story_all_signal_connnections_except_radio);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.c…onnnections_except_radio)");
        this.allExceptRadio = (MaterialRadioButton) findViewById5;
        View findViewById6 = view.findViewById(R.id.choose_initial_my_story_only_share_with_radio);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.c…ry_only_share_with_radio)");
        this.onlyWitRadio = (MaterialRadioButton) findViewById6;
        View findViewById7 = view.findViewById(R.id.choose_initial_my_story_all_signal_connnections_except_count);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.c…onnnections_except_count)");
        this.allExceptCount = (TextView) findViewById7;
        View findViewById8 = view.findViewById(R.id.choose_initial_my_story_only_share_with_count);
        Intrinsics.checkNotNullExpressionValue(findViewById8, "view.findViewById(R.id.c…ry_only_share_with_count)");
        this.onlyWithCount = (TextView) findViewById8;
        View findViewById9 = view.findViewById(R.id.choose_initial_my_story_save);
        findViewById9.setEnabled(false);
        LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        this.lifecycleDisposable = lifecycleDisposable;
        Disposable subscribe = getViewModel().getState().subscribe(new Consumer(findViewById9) { // from class: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ChooseInitialMyStoryMembershipBottomSheetDialogFragment.m2928onViewCreated$lambda2(ChooseInitialMyStoryMembershipBottomSheetDialogFragment.this, this.f$1, (ChooseInitialMyStoryMembershipState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.state\n      .s…cipientId != null\n      }");
        lifecycleDisposable.plusAssign(subscribe);
        ChooseInitialMyStoryMembershipBottomSheetDialogFragment$onViewCreated$clickListener$1 chooseInitialMyStoryMembershipBottomSheetDialogFragment$onViewCreated$clickListener$1 = new ChooseInitialMyStoryMembershipBottomSheetDialogFragment$onViewCreated$clickListener$1(this);
        View[] viewArr = new View[3];
        View view2 = this.allRow;
        View view3 = null;
        if (view2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("allRow");
            view2 = null;
        }
        viewArr[0] = view2;
        View view4 = this.allExceptRow;
        if (view4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("allExceptRow");
            view4 = null;
        }
        viewArr[1] = view4;
        View view5 = this.onlyWitRow;
        if (view5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("onlyWitRow");
        } else {
            view3 = view5;
        }
        viewArr[2] = view3;
        for (View view6 : CollectionsKt__CollectionsKt.listOf((Object[]) viewArr)) {
            view6.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment$$ExternalSyntheticLambda2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view7) {
                    ChooseInitialMyStoryMembershipBottomSheetDialogFragment.m2929onViewCreated$lambda4$lambda3(Function1.this, view7);
                }
            });
        }
        findViewById9.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view7) {
                ChooseInitialMyStoryMembershipBottomSheetDialogFragment.m2930onViewCreated$lambda6(ChooseInitialMyStoryMembershipBottomSheetDialogFragment.this, view7);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m2928onViewCreated$lambda2(ChooseInitialMyStoryMembershipBottomSheetDialogFragment chooseInitialMyStoryMembershipBottomSheetDialogFragment, View view, ChooseInitialMyStoryMembershipState chooseInitialMyStoryMembershipState) {
        Intrinsics.checkNotNullParameter(chooseInitialMyStoryMembershipBottomSheetDialogFragment, "this$0");
        MaterialRadioButton materialRadioButton = chooseInitialMyStoryMembershipBottomSheetDialogFragment.allRadio;
        TextView textView = null;
        if (materialRadioButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("allRadio");
            materialRadioButton = null;
        }
        boolean z = false;
        materialRadioButton.setChecked(chooseInitialMyStoryMembershipState.getPrivacyState().getPrivacyMode() == DistributionListPrivacyMode.ALL);
        MaterialRadioButton materialRadioButton2 = chooseInitialMyStoryMembershipBottomSheetDialogFragment.allExceptRadio;
        if (materialRadioButton2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("allExceptRadio");
            materialRadioButton2 = null;
        }
        materialRadioButton2.setChecked(chooseInitialMyStoryMembershipState.getPrivacyState().getPrivacyMode() == DistributionListPrivacyMode.ALL_EXCEPT);
        MaterialRadioButton materialRadioButton3 = chooseInitialMyStoryMembershipBottomSheetDialogFragment.onlyWitRadio;
        if (materialRadioButton3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("onlyWitRadio");
            materialRadioButton3 = null;
        }
        materialRadioButton3.setChecked(chooseInitialMyStoryMembershipState.getPrivacyState().getPrivacyMode() == DistributionListPrivacyMode.ONLY_WITH);
        TextView textView2 = chooseInitialMyStoryMembershipBottomSheetDialogFragment.allExceptCount;
        if (textView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("allExceptCount");
            textView2 = null;
        }
        MaterialRadioButton materialRadioButton4 = chooseInitialMyStoryMembershipBottomSheetDialogFragment.allExceptRadio;
        if (materialRadioButton4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("allExceptRadio");
            materialRadioButton4 = null;
        }
        ViewExtensionsKt.setVisible(textView2, materialRadioButton4.isChecked());
        TextView textView3 = chooseInitialMyStoryMembershipBottomSheetDialogFragment.onlyWithCount;
        if (textView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("onlyWithCount");
            textView3 = null;
        }
        MaterialRadioButton materialRadioButton5 = chooseInitialMyStoryMembershipBottomSheetDialogFragment.onlyWitRadio;
        if (materialRadioButton5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("onlyWitRadio");
            materialRadioButton5 = null;
        }
        ViewExtensionsKt.setVisible(textView3, materialRadioButton5.isChecked());
        DistributionListPrivacyMode privacyMode = chooseInitialMyStoryMembershipState.getPrivacyState().getPrivacyMode();
        int i = privacyMode == null ? -1 : WhenMappings.$EnumSwitchMapping$0[privacyMode.ordinal()];
        if (i == 1) {
            TextView textView4 = chooseInitialMyStoryMembershipBottomSheetDialogFragment.allExceptCount;
            if (textView4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("allExceptCount");
            } else {
                textView = textView4;
            }
            textView.setText(chooseInitialMyStoryMembershipBottomSheetDialogFragment.getResources().getQuantityString(R.plurals.MyStorySettingsFragment__d_people_excluded, chooseInitialMyStoryMembershipState.getPrivacyState().getConnectionCount(), Integer.valueOf(chooseInitialMyStoryMembershipState.getPrivacyState().getConnectionCount())));
        } else if (i == 2) {
            TextView textView5 = chooseInitialMyStoryMembershipBottomSheetDialogFragment.onlyWithCount;
            if (textView5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("onlyWithCount");
            } else {
                textView = textView5;
            }
            textView.setText(chooseInitialMyStoryMembershipBottomSheetDialogFragment.getResources().getQuantityString(R.plurals.MyStorySettingsFragment__d_people, chooseInitialMyStoryMembershipState.getPrivacyState().getConnectionCount(), Integer.valueOf(chooseInitialMyStoryMembershipState.getPrivacyState().getConnectionCount())));
        }
        if (chooseInitialMyStoryMembershipState.getRecipientId() != null) {
            z = true;
        }
        view.setEnabled(z);
    }

    /* renamed from: onViewCreated$lambda-4$lambda-3 */
    public static final void m2929onViewCreated$lambda4$lambda3(Function1 function1, View view) {
        Intrinsics.checkNotNullParameter(function1, "$clickListener");
        Intrinsics.checkNotNullExpressionValue(view, "v");
        function1.invoke(view);
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m2930onViewCreated$lambda6(ChooseInitialMyStoryMembershipBottomSheetDialogFragment chooseInitialMyStoryMembershipBottomSheetDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(chooseInitialMyStoryMembershipBottomSheetDialogFragment, "this$0");
        LifecycleDisposable lifecycleDisposable = chooseInitialMyStoryMembershipBottomSheetDialogFragment.lifecycleDisposable;
        if (lifecycleDisposable == null) {
            Intrinsics.throwUninitializedPropertyAccessException("lifecycleDisposable");
            lifecycleDisposable = null;
        }
        Disposable subscribe = chooseInitialMyStoryMembershipBottomSheetDialogFragment.getViewModel().save().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ChooseInitialMyStoryMembershipBottomSheetDialogFragment.m2931onViewCreated$lambda6$lambda5(ChooseInitialMyStoryMembershipBottomSheetDialogFragment.this, (RecipientId) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel\n        .save(…ed(recipientId)\n        }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:15:0x0024 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v5, types: [org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment$Callback] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: onViewCreated$lambda-6$lambda-5 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m2931onViewCreated$lambda6$lambda5(org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment r2, org.thoughtcrime.securesms.recipients.RecipientId r3) {
        /*
            java.lang.String r0 = "this$0"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r2, r0)
            r2.dismissAllowingStateLoss()
            androidx.fragment.app.Fragment r0 = r2.getParentFragment()
        L_0x000c:
            if (r0 == 0) goto L_0x0018
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment.Callback
            if (r1 == 0) goto L_0x0013
            goto L_0x0024
        L_0x0013:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x000c
        L_0x0018:
            androidx.fragment.app.FragmentActivity r2 = r2.requireActivity()
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment.Callback
            if (r0 != 0) goto L_0x0021
            r2 = 0
        L_0x0021:
            r0 = r2
            org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment$Callback r0 = (org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment.Callback) r0
        L_0x0024:
            org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment$Callback r0 = (org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment.Callback) r0
            if (r0 == 0) goto L_0x0030
            java.lang.String r2 = "recipientId"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r3, r2)
            r0.onMyStoryConfigured(r3)
        L_0x0030:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment.m2931onViewCreated$lambda6$lambda5(org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment, org.thoughtcrime.securesms.recipients.RecipientId):void");
    }

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment.Callback
    public void exitFlow() {
        Fragment findFragmentByTag = getChildFragmentManager().findFragmentByTag(SELECTION_FRAGMENT);
        DialogFragment dialogFragment = findFragmentByTag instanceof DialogFragment ? (DialogFragment) findFragmentByTag : null;
        if (dialogFragment != null) {
            dialogFragment.dismissAllowingStateLoss();
        }
    }

    /* compiled from: ChooseInitialMyStoryMembershipBottomSheetDialogFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipBottomSheetDialogFragment$Companion;", "", "()V", "SELECTION_FRAGMENT", "", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final void show(FragmentManager fragmentManager) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            new ChooseInitialMyStoryMembershipBottomSheetDialogFragment().show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        }
    }
}
