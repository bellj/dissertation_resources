package org.thoughtcrime.securesms.stories;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.view.ViewCompat;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* compiled from: ScaleTransition.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u0007\b\u0016¢\u0006\u0002\u0010\u0002B\u0017\b\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\r\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0002J&\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u000b2\b\u0010\u0013\u001a\u0004\u0018\u00010\u000bH\u0016J\u0013\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015H\u0016¢\u0006\u0002\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/stories/ScaleTransition;", "Landroid/transition/Transition;", "()V", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "captureEndValues", "", "transitionValues", "Landroid/transition/TransitionValues;", "captureStartValues", "captureValues", "createAnimator", "Landroid/animation/Animator;", "sceneRoot", "Landroid/view/ViewGroup;", NotificationProfileDatabase.NotificationProfileScheduleTable.START, NotificationProfileDatabase.NotificationProfileScheduleTable.END, "getTransitionProperties", "", "", "()[Ljava/lang/String;", "resetValues", "view", "Landroid/view/View;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ScaleTransition extends Transition {
    public static final Companion Companion = new Companion(null);
    private static final String LAYOUT_HEIGHT;
    private static final String LAYOUT_WIDTH;
    private static final String POSITION_X;
    private static final String POSITION_Y;
    private static final String[] PROPERTIES = {LAYOUT_WIDTH, LAYOUT_HEIGHT, POSITION_X, POSITION_Y, SCALE_X, SCALE_Y};
    private static final String SCALE_X;
    private static final String SCALE_Y;

    /* compiled from: ScaleTransition.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\tX\u0004¢\u0006\u0004\n\u0002\u0010\nR\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/ScaleTransition$Companion;", "", "()V", "LAYOUT_HEIGHT", "", "LAYOUT_WIDTH", "POSITION_X", "POSITION_Y", "PROPERTIES", "", "[Ljava/lang/String;", "SCALE_X", "SCALE_Y", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public ScaleTransition() {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ScaleTransition(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(attributeSet, "attrs");
    }

    @Override // android.transition.Transition
    public String[] getTransitionProperties() {
        return PROPERTIES;
    }

    @Override // android.transition.Transition
    public void captureStartValues(TransitionValues transitionValues) {
        Intrinsics.checkNotNullParameter(transitionValues, "transitionValues");
        if (Intrinsics.areEqual(ViewCompat.getTransitionName(transitionValues.view), "story")) {
            captureValues(transitionValues);
        }
    }

    @Override // android.transition.Transition
    public void captureEndValues(TransitionValues transitionValues) {
        Intrinsics.checkNotNullParameter(transitionValues, "transitionValues");
        if (Intrinsics.areEqual(ViewCompat.getTransitionName(transitionValues.view), "story")) {
            View view = transitionValues.view;
            Intrinsics.checkNotNullExpressionValue(view, "transitionValues.view");
            resetValues(view);
            captureValues(transitionValues);
        }
    }

    private final void captureValues(TransitionValues transitionValues) {
        View view = transitionValues.view;
        Map map = transitionValues.values;
        Intrinsics.checkNotNullExpressionValue(map, "transitionValues.values");
        map.put(LAYOUT_WIDTH, Float.valueOf((float) view.getWidth()));
        Map map2 = transitionValues.values;
        Intrinsics.checkNotNullExpressionValue(map2, "transitionValues.values");
        map2.put(LAYOUT_HEIGHT, Float.valueOf((float) view.getHeight()));
        Map map3 = transitionValues.values;
        Intrinsics.checkNotNullExpressionValue(map3, "transitionValues.values");
        map3.put(POSITION_X, Float.valueOf(view.getX()));
        Map map4 = transitionValues.values;
        Intrinsics.checkNotNullExpressionValue(map4, "transitionValues.values");
        map4.put(POSITION_Y, Float.valueOf(view.getY()));
        Map map5 = transitionValues.values;
        Intrinsics.checkNotNullExpressionValue(map5, "transitionValues.values");
        map5.put(SCALE_X, Float.valueOf(view.getScaleX()));
        Map map6 = transitionValues.values;
        Intrinsics.checkNotNullExpressionValue(map6, "transitionValues.values");
        map6.put(SCALE_Y, Float.valueOf(view.getScaleY()));
    }

    public final void resetValues(View view) {
        view.setTranslationX(0.0f);
        view.setTranslationY(0.0f);
        view.setScaleX(1.0f);
        view.setScaleY(1.0f);
    }

    @Override // android.transition.Transition
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        Intrinsics.checkNotNullParameter(viewGroup, "sceneRoot");
        if (transitionValues == null || transitionValues2 == null) {
            return null;
        }
        Object obj = transitionValues.values.get(LAYOUT_WIDTH);
        if (obj != null) {
            float floatValue = ((Float) obj).floatValue();
            Object obj2 = transitionValues2.values.get(LAYOUT_WIDTH);
            if (obj2 != null) {
                float floatValue2 = ((Float) obj2).floatValue();
                Object obj3 = transitionValues.values.get(LAYOUT_HEIGHT);
                if (obj3 != null) {
                    float floatValue3 = ((Float) obj3).floatValue();
                    Object obj4 = transitionValues2.values.get(LAYOUT_HEIGHT);
                    if (obj4 != null) {
                        float floatValue4 = ((Float) obj4).floatValue();
                        Object obj5 = transitionValues.values.get(POSITION_X);
                        if (obj5 != null) {
                            float floatValue5 = ((Float) obj5).floatValue();
                            Object obj6 = transitionValues2.values.get(POSITION_X);
                            if (obj6 != null) {
                                float floatValue6 = ((Float) obj6).floatValue();
                                Object obj7 = transitionValues.values.get(POSITION_Y);
                                if (obj7 != null) {
                                    float floatValue7 = ((Float) obj7).floatValue();
                                    Object obj8 = transitionValues2.values.get(POSITION_Y);
                                    if (obj8 != null) {
                                        float floatValue8 = ((Float) obj8).floatValue();
                                        Object obj9 = transitionValues.values.get(SCALE_X);
                                        if (obj9 != null) {
                                            float floatValue9 = ((Float) obj9).floatValue();
                                            Object obj10 = transitionValues.values.get(SCALE_Y);
                                            if (obj10 != null) {
                                                float floatValue10 = ((Float) obj10).floatValue();
                                                float f = (float) 2;
                                                transitionValues2.view.setTranslationX((floatValue5 - floatValue6) - ((floatValue2 - floatValue) / f));
                                                transitionValues2.view.setTranslationY((floatValue7 - floatValue8) - ((floatValue4 - floatValue3) / f));
                                                transitionValues2.view.setScaleX((floatValue / floatValue2) * floatValue9);
                                                transitionValues2.view.setScaleY((floatValue3 / floatValue4) * floatValue10);
                                                ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(transitionValues2.view, PropertyValuesHolder.ofFloat(View.TRANSLATION_X, 0.0f), PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0.0f), PropertyValuesHolder.ofFloat(View.SCALE_X, 1.0f), PropertyValuesHolder.ofFloat(View.SCALE_Y, 1.0f));
                                                ofPropertyValuesHolder.addListener(new AnimatorListenerAdapter(this, transitionValues, transitionValues2) { // from class: org.thoughtcrime.securesms.stories.ScaleTransition$createAnimator$1$1
                                                    final /* synthetic */ TransitionValues $end;
                                                    final /* synthetic */ TransitionValues $start;
                                                    final /* synthetic */ ScaleTransition this$0;

                                                    /* access modifiers changed from: package-private */
                                                    {
                                                        this.this$0 = r1;
                                                        this.$start = r2;
                                                        this.$end = r3;
                                                    }

                                                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                                                    public void onAnimationEnd(Animator animator) {
                                                        ScaleTransition scaleTransition = this.this$0;
                                                        View view = this.$start.view;
                                                        Intrinsics.checkNotNullExpressionValue(view, "start.view");
                                                        ScaleTransition.access$resetValues(scaleTransition, view);
                                                        ScaleTransition scaleTransition2 = this.this$0;
                                                        View view2 = this.$end.view;
                                                        Intrinsics.checkNotNullExpressionValue(view2, "end.view");
                                                        ScaleTransition.access$resetValues(scaleTransition2, view2);
                                                    }
                                                });
                                                return ofPropertyValuesHolder;
                                            }
                                            throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
                                        }
                                        throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
                                    }
                                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
                                }
                                throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
                            }
                            throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
                        }
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
                    }
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
                }
                throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
            }
            throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
    }
}
