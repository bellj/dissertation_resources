package org.thoughtcrime.securesms.stories.viewer;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerState;

/* compiled from: StoryViewerViewModel.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState;", "it", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerViewModel$setContentIsReady$1 extends Lambda implements Function1<StoryViewerState, StoryViewerState> {
    public static final StoryViewerViewModel$setContentIsReady$1 INSTANCE = new StoryViewerViewModel$setContentIsReady$1();

    StoryViewerViewModel$setContentIsReady$1() {
        super(1);
    }

    public final StoryViewerState invoke(StoryViewerState storyViewerState) {
        Intrinsics.checkNotNullParameter(storyViewerState, "it");
        return StoryViewerState.copy$default(storyViewerState, null, 0, 0, null, null, StoryViewerState.LoadState.copy$default(storyViewerState.getLoadState(), true, false, 2, null), false, 95, null);
    }
}
