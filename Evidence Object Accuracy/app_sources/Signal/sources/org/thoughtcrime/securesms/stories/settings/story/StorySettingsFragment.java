package org.thoughtcrime.securesms.stories.settings.story;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;

/* compiled from: StorySettingsFragment.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\b\u0010\u0011\u001a\u00020\nH\u0016R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsState;", "onResume", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StorySettingsFragment extends DSLSettingsFragment {
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StorySettingsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, StorySettingsFragment$viewModel$2.INSTANCE);

    public StorySettingsFragment() {
        super(R.string.StorySettingsFragment__story_settings, 0, 0, null, 14, null);
    }

    private final StorySettingsViewModel getViewModel() {
        return (StorySettingsViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().refresh();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        PrivateStoryItem.INSTANCE.register(dSLSettingsAdapter);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ StorySettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StorySettingsFragment.$r8$lambda$e1SHD6MxMRmCsjZqQ0SMPuM95dI(DSLSettingsAdapter.this, this.f$1, (StorySettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m2961bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, StorySettingsFragment storySettingsFragment, StorySettingsState storySettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(storySettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(storySettingsState, "state");
        dSLSettingsAdapter.submitList(storySettingsFragment.getConfiguration(storySettingsState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(StorySettingsState storySettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(storySettingsState, this) { // from class: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1
            final /* synthetic */ StorySettingsState $state;
            final /* synthetic */ StorySettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0057: INVOKE  
                  (r6v0 'dSLConfiguration' org.thoughtcrime.securesms.components.settings.DSLConfiguration)
                  (wrap: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$PartialModel : 0x0054: CONSTRUCTOR  (r3v1 org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$PartialModel A[REMOVE]) = 
                  (r2v8 'distributionListPartialRecord' org.thoughtcrime.securesms.database.model.DistributionListPartialRecord A[REMOVE])
                  (wrap: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1 : 0x0051: CONSTRUCTOR  (r4v0 org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1 A[REMOVE]) = (r1v2 'storySettingsFragment3' org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment) call: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1.<init>(org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment):void type: CONSTRUCTOR)
                 call: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem.PartialModel.<init>(org.thoughtcrime.securesms.database.model.DistributionListPartialRecord, kotlin.jvm.functions.Function1):void type: CONSTRUCTOR)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLConfiguration.customPref(org.thoughtcrime.securesms.util.adapter.mapping.MappingModel):void in method: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:221)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0054: CONSTRUCTOR  (r3v1 org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$PartialModel A[REMOVE]) = 
                  (r2v8 'distributionListPartialRecord' org.thoughtcrime.securesms.database.model.DistributionListPartialRecord A[REMOVE])
                  (wrap: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1 : 0x0051: CONSTRUCTOR  (r4v0 org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1 A[REMOVE]) = (r1v2 'storySettingsFragment3' org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment) call: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1.<init>(org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment):void type: CONSTRUCTOR)
                 call: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem.PartialModel.<init>(org.thoughtcrime.securesms.database.model.DistributionListPartialRecord, kotlin.jvm.functions.Function1):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 16 more
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0051: CONSTRUCTOR  (r4v0 org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1 A[REMOVE]) = (r1v2 'storySettingsFragment3' org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment) call: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1.<init>(org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 22 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 28 more
                */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r6) {
                /*
                    r5 = this;
                    java.lang.String r0 = "$this$configure"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r6, r0)
                    org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$RecipientModel r0 = new org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$RecipientModel
                    org.thoughtcrime.securesms.recipients.Recipient r1 = org.thoughtcrime.securesms.recipients.Recipient.self()
                    java.lang.String r2 = "self()"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
                    org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$1 r2 = new org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$1
                    org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment r3 = r5.this$0
                    r2.<init>(r3)
                    r0.<init>(r1, r2)
                    r6.customPref(r0)
                    r6.dividerPref()
                    r0 = 2131953935(0x7f13090f, float:1.9544355E38)
                    r6.sectionHeaderPref(r0)
                    org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$NewModel r0 = new org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$NewModel
                    org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$2 r1 = new org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$2
                    org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment r2 = r5.this$0
                    r1.<init>(r2)
                    r0.<init>(r1)
                    r6.customPref(r0)
                    org.thoughtcrime.securesms.stories.settings.story.StorySettingsState r0 = r5.$state
                    java.util.List r0 = r0.getPrivateStories()
                    org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment r1 = r5.this$0
                    java.util.Iterator r0 = r0.iterator()
                L_0x0041:
                    boolean r2 = r0.hasNext()
                    if (r2 == 0) goto L_0x005b
                    java.lang.Object r2 = r0.next()
                    org.thoughtcrime.securesms.database.model.DistributionListPartialRecord r2 = (org.thoughtcrime.securesms.database.model.DistributionListPartialRecord) r2
                    org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$PartialModel r3 = new org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$PartialModel
                    org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1 r4 = new org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1$3$1
                    r4.<init>(r1)
                    r3.<init>(r2, r4)
                    r6.customPref(r3)
                    goto L_0x0041
                L_0x005b:
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion r0 = org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion
                    r1 = 2131953936(0x7f130910, float:1.9544357E38)
                    r2 = 0
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r2 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r2]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r0 = r0.from(r1, r2)
                    r1 = 1
                    r2 = 0
                    org.thoughtcrime.securesms.components.settings.DSLConfiguration.textPref$default(r6, r2, r0, r1, r2)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.settings.story.StorySettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }
}
