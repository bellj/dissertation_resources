package org.thoughtcrime.securesms.stories.viewer.views;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: StoryViewItemData.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewItemData;", "", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "timeViewedInMillis", "", "(Lorg/thoughtcrime/securesms/recipients/Recipient;J)V", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getTimeViewedInMillis", "()J", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewItemData {
    private final Recipient recipient;
    private final long timeViewedInMillis;

    public static /* synthetic */ StoryViewItemData copy$default(StoryViewItemData storyViewItemData, Recipient recipient, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            recipient = storyViewItemData.recipient;
        }
        if ((i & 2) != 0) {
            j = storyViewItemData.timeViewedInMillis;
        }
        return storyViewItemData.copy(recipient, j);
    }

    public final Recipient component1() {
        return this.recipient;
    }

    public final long component2() {
        return this.timeViewedInMillis;
    }

    public final StoryViewItemData copy(Recipient recipient, long j) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        return new StoryViewItemData(recipient, j);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryViewItemData)) {
            return false;
        }
        StoryViewItemData storyViewItemData = (StoryViewItemData) obj;
        return Intrinsics.areEqual(this.recipient, storyViewItemData.recipient) && this.timeViewedInMillis == storyViewItemData.timeViewedInMillis;
    }

    public int hashCode() {
        return (this.recipient.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.timeViewedInMillis);
    }

    public String toString() {
        return "StoryViewItemData(recipient=" + this.recipient + ", timeViewedInMillis=" + this.timeViewedInMillis + ')';
    }

    public StoryViewItemData(Recipient recipient, long j) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        this.recipient = recipient;
        this.timeViewedInMillis = j;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }

    public final long getTimeViewedInMillis() {
        return this.timeViewedInMillis;
    }
}
