package org.thoughtcrime.securesms.stories.settings.custom;

import android.content.DialogInterface;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import androidx.navigation.fragment.NavHostFragment;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.functions.Action;
import j$.util.function.Function;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.WrapperDialogFragment;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragmentArgs;
import org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragmentDirections;
import org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsViewModel;
import org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel;
import org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder;

/* compiled from: PrivateStorySettingsFragment.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u001f2\u00020\u0001:\u0003\u001f !B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u000eH\u0002J\u0010\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u000eH\u0016J\b\u0010\u001e\u001a\u00020\u000eH\u0016R\u0014\u0010\u0003\u001a\u00020\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\u0007\u001a\u00020\b8BX\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\n¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getDistributionListId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "viewModel", "Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsState;", "handleDeletePrivateStory", "handleRemoveRecipient", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onResume", "onToolbarNavigationClicked", "Companion", "Dialog", "RecipientEventListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class PrivateStorySettingsFragment extends DSLSettingsFragment {
    public static final Companion Companion = new Companion(null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(PrivateStorySettingsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$viewModel$2
        final /* synthetic */ PrivateStorySettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            DistributionListId distributionListId = PrivateStorySettingsFragmentArgs.fromBundle(this.this$0.requireArguments()).getDistributionListId();
            Intrinsics.checkNotNullExpressionValue(distributionListId, "fromBundle(requireArguments()).distributionListId");
            return new PrivateStorySettingsViewModel.Factory(distributionListId, new PrivateStorySettingsRepository());
        }
    });

    /* renamed from: handleDeletePrivateStory$lambda-4 */
    public static final void m2876handleDeletePrivateStory$lambda4(DialogInterface dialogInterface, int i) {
    }

    /* renamed from: handleRemoveRecipient$lambda-3 */
    public static final void m2880handleRemoveRecipient$lambda3(DialogInterface dialogInterface, int i) {
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void onToolbarNavigationClicked() {
        Unit unit;
        WrapperDialogFragment parentFragment = getParentFragment();
        while (true) {
            unit = null;
            if (parentFragment == null) {
                FragmentActivity requireActivity = requireActivity();
                if (!(requireActivity instanceof WrapperDialogFragment)) {
                    requireActivity = null;
                }
                parentFragment = (WrapperDialogFragment) requireActivity;
            } else if (parentFragment instanceof WrapperDialogFragment) {
                break;
            } else {
                parentFragment = parentFragment.getParentFragment();
            }
        }
        WrapperDialogFragment wrapperDialogFragment = parentFragment;
        if (wrapperDialogFragment != null) {
            wrapperDialogFragment.dismiss();
            unit = Unit.INSTANCE;
        }
        if (unit == null) {
            super.onToolbarNavigationClicked();
        }
    }

    public PrivateStorySettingsFragment() {
        super(0, R.menu.story_private_menu, 0, null, 13, null);
    }

    public final PrivateStorySettingsViewModel getViewModel() {
        return (PrivateStorySettingsViewModel) this.viewModel$delegate.getValue();
    }

    public final DistributionListId getDistributionListId() {
        DistributionListId distributionListId = PrivateStorySettingsFragmentArgs.fromBundle(requireArguments()).getDistributionListId();
        Intrinsics.checkNotNullExpressionValue(distributionListId, "fromBundle(requireArguments()).distributionListId");
        return distributionListId;
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().refresh();
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final MappingViewHolder m2874bindAdapter$lambda0(PrivateStorySettingsFragment privateStorySettingsFragment, View view) {
        Intrinsics.checkNotNullParameter(privateStorySettingsFragment, "this$0");
        return new RecipientViewHolder(view, new RecipientEventListener());
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        dSLSettingsAdapter.registerFactory(RecipientMappingModel.RecipientIdMappingModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PrivateStorySettingsFragment.m2874bindAdapter$lambda0(PrivateStorySettingsFragment.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_recipient_item));
        PrivateStoryItem.INSTANCE.register(dSLSettingsAdapter);
        View findViewById = requireView().findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewById(R.id.toolbar)");
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(dSLSettingsAdapter, this) { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ DSLSettingsAdapter f$1;
            public final /* synthetic */ PrivateStorySettingsFragment f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PrivateStorySettingsFragment.m2875bindAdapter$lambda1(Toolbar.this, this.f$1, this.f$2, (PrivateStorySettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m2875bindAdapter$lambda1(Toolbar toolbar, DSLSettingsAdapter dSLSettingsAdapter, PrivateStorySettingsFragment privateStorySettingsFragment, PrivateStorySettingsState privateStorySettingsState) {
        Intrinsics.checkNotNullParameter(toolbar, "$toolbar");
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(privateStorySettingsFragment, "this$0");
        DistributionListRecord privateStory = privateStorySettingsState.getPrivateStory();
        toolbar.setTitle(privateStory != null ? privateStory.getName() : null);
        Intrinsics.checkNotNullExpressionValue(privateStorySettingsState, "state");
        dSLSettingsAdapter.submitList(privateStorySettingsFragment.getConfiguration(privateStorySettingsState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(PrivateStorySettingsState privateStorySettingsState) {
        if (privateStorySettingsState.getPrivateStory() == null) {
            return DslKt.configure(PrivateStorySettingsFragment$getConfiguration$1.INSTANCE);
        }
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(privateStorySettingsState, this) { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$getConfiguration$2
            final /* synthetic */ PrivateStorySettingsState $state;
            final /* synthetic */ PrivateStorySettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.sectionHeaderPref(R.string.MyStorySettingsFragment__who_can_see_this_story);
                final PrivateStorySettingsFragment privateStorySettingsFragment = this.this$0;
                dSLConfiguration.customPref(new PrivateStoryItem.AddViewerModel(new Function0<Unit>() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$getConfiguration$2.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = FragmentKt.findNavController(privateStorySettingsFragment);
                        PrivateStorySettingsFragmentDirections.ActionPrivateStorySettingsToEditStoryViewers actionPrivateStorySettingsToEditStoryViewers = PrivateStorySettingsFragmentDirections.actionPrivateStorySettingsToEditStoryViewers(privateStorySettingsFragment.getDistributionListId());
                        Intrinsics.checkNotNullExpressionValue(actionPrivateStorySettingsToEditStoryViewers, "actionPrivateStorySettin…ewers(distributionListId)");
                        SafeNavigation.safeNavigate(findNavController, actionPrivateStorySettingsToEditStoryViewers);
                    }
                }));
                for (RecipientId recipientId : this.$state.getPrivateStory().getMembers()) {
                    dSLConfiguration.customPref(new RecipientMappingModel.RecipientIdMappingModel(recipientId));
                }
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.MyStorySettingsFragment__replies_amp_reactions);
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.MyStorySettingsFragment__allow_replies_amp_reactions, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from2 = companion.from(R.string.MyStorySettingsFragment__let_people_who_can_view_your_story_react_and_reply, new DSLSettingsText.Modifier[0]);
                boolean areRepliesAndReactionsEnabled = this.$state.getAreRepliesAndReactionsEnabled();
                final PrivateStorySettingsFragment privateStorySettingsFragment2 = this.this$0;
                final PrivateStorySettingsState privateStorySettingsState2 = this.$state;
                dSLConfiguration.switchPref(from, (r16 & 2) != 0 ? null : from2, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, areRepliesAndReactionsEnabled, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$getConfiguration$2.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        privateStorySettingsFragment2.getViewModel().setRepliesAndReactionsEnabled(!privateStorySettingsState2.getAreRepliesAndReactionsEnabled());
                    }
                });
                dSLConfiguration.dividerPref();
                DSLSettingsText from3 = companion.from(R.string.PrivateStorySettingsFragment__delete_private_story, new DSLSettingsText.ColorModifier(ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_alert_primary)));
                final PrivateStorySettingsFragment privateStorySettingsFragment3 = this.this$0;
                dSLConfiguration.clickPref(from3, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$getConfiguration$2.4
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        privateStorySettingsFragment3.handleDeletePrivateStory();
                    }
                }, (r18 & 64) != 0 ? null : null);
            }
        });
    }

    @Override // androidx.fragment.app.Fragment
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intrinsics.checkNotNullParameter(menuItem, "item");
        if (menuItem.getItemId() != R.id.action_edit) {
            return false;
        }
        PrivateStorySettingsFragmentDirections.ActionPrivateStorySettingsToEditStoryNameFragment actionPrivateStorySettingsToEditStoryNameFragment = PrivateStorySettingsFragmentDirections.actionPrivateStorySettingsToEditStoryNameFragment(getDistributionListId(), getViewModel().getName());
        Intrinsics.checkNotNullExpressionValue(actionPrivateStorySettingsToEditStoryNameFragment, "actionPrivateStorySettin…tId, viewModel.getName())");
        FragmentKt.findNavController(this).navigate(actionPrivateStorySettingsToEditStoryNameFragment);
        return true;
    }

    public final void handleRemoveRecipient(Recipient recipient) {
        new MaterialAlertDialogBuilder(requireContext()).setTitle((CharSequence) getString(R.string.PrivateStorySettingsFragment__remove_s, recipient.getDisplayName(requireContext()))).setMessage(R.string.PrivateStorySettingsFragment__this_person_will_no_longer).setPositiveButton(R.string.PrivateStorySettingsFragment__remove, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(recipient) { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$$ExternalSyntheticLambda5
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PrivateStorySettingsFragment.m2879handleRemoveRecipient$lambda2(PrivateStorySettingsFragment.this, this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$$ExternalSyntheticLambda6
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PrivateStorySettingsFragment.m2880handleRemoveRecipient$lambda3(dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: handleRemoveRecipient$lambda-2 */
    public static final void m2879handleRemoveRecipient$lambda2(PrivateStorySettingsFragment privateStorySettingsFragment, Recipient recipient, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(privateStorySettingsFragment, "this$0");
        Intrinsics.checkNotNullParameter(recipient, "$recipient");
        privateStorySettingsFragment.getViewModel().remove(recipient);
    }

    public final void handleDeletePrivateStory() {
        new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.PrivateStorySettingsFragment__are_you_sure).setMessage(R.string.PrivateStorySettingsFragment__this_action_cannot).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$$ExternalSyntheticLambda3
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PrivateStorySettingsFragment.m2876handleDeletePrivateStory$lambda4(dialogInterface, i);
            }
        }).setPositiveButton(R.string.delete, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$$ExternalSyntheticLambda4
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PrivateStorySettingsFragment.m2877handleDeletePrivateStory$lambda6(PrivateStorySettingsFragment.this, dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: handleDeletePrivateStory$lambda-6 */
    public static final void m2877handleDeletePrivateStory$lambda6(PrivateStorySettingsFragment privateStorySettingsFragment, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(privateStorySettingsFragment, "this$0");
        privateStorySettingsFragment.getViewModel().delete().subscribe(new Action() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                PrivateStorySettingsFragment.m2878handleDeletePrivateStory$lambda6$lambda5(PrivateStorySettingsFragment.this);
            }
        });
    }

    /* renamed from: handleDeletePrivateStory$lambda-6$lambda-5 */
    public static final void m2878handleDeletePrivateStory$lambda6$lambda5(PrivateStorySettingsFragment privateStorySettingsFragment) {
        Intrinsics.checkNotNullParameter(privateStorySettingsFragment, "this$0");
        FragmentKt.findNavController(privateStorySettingsFragment).popBackStack();
    }

    /* compiled from: PrivateStorySettingsFragment.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsFragment$RecipientEventListener;", "Lorg/thoughtcrime/securesms/util/viewholders/RecipientViewHolder$EventListener;", "Lorg/thoughtcrime/securesms/util/viewholders/RecipientMappingModel$RecipientIdMappingModel;", "(Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsFragment;)V", "onClick", "", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public final class RecipientEventListener implements RecipientViewHolder.EventListener<RecipientMappingModel.RecipientIdMappingModel> {
        @Override // org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder.EventListener
        public /* synthetic */ void onModelClick(RecipientMappingModel.RecipientIdMappingModel recipientIdMappingModel) {
            RecipientViewHolder.EventListener.CC.$default$onModelClick(this, recipientIdMappingModel);
        }

        public RecipientEventListener() {
            PrivateStorySettingsFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder.EventListener
        public void onClick(Recipient recipient) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            PrivateStorySettingsFragment.this.handleRemoveRecipient(recipient);
        }
    }

    /* compiled from: PrivateStorySettingsFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsFragment$Dialog;", "Lorg/thoughtcrime/securesms/components/WrapperDialogFragment;", "()V", "getWrappedFragment", "Landroidx/fragment/app/Fragment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Dialog extends WrapperDialogFragment {
        @Override // org.thoughtcrime.securesms.components.WrapperDialogFragment
        public Fragment getWrappedFragment() {
            NavHostFragment create = NavHostFragment.create(R.navigation.private_story_settings, requireArguments());
            Intrinsics.checkNotNullExpressionValue(create, "create(R.navigation.priv…ings, requireArguments())");
            return create;
        }
    }

    /* compiled from: PrivateStorySettingsFragment.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsFragment$Companion;", "", "()V", "createAsDialog", "Landroidx/fragment/app/DialogFragment;", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final DialogFragment createAsDialog(DistributionListId distributionListId) {
            Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
            Dialog dialog = new Dialog();
            dialog.setArguments(new PrivateStorySettingsFragmentArgs.Builder(distributionListId).build().toBundle());
            return dialog;
        }
    }
}
