package org.thoughtcrime.securesms.stories.viewer.page;

import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.MediaUtil;

/* compiled from: StoryPost.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b#\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u00016Ba\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0002\u0010\u0013J\t\u0010%\u001a\u00020\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u0011HÆ\u0003J\t\u0010'\u001a\u00020\u0011HÆ\u0003J\t\u0010(\u001a\u00020\u0005HÆ\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010*\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010+\u001a\u00020\tHÆ\u0003J\t\u0010,\u001a\u00020\tHÆ\u0003J\t\u0010-\u001a\u00020\u0003HÆ\u0003J\t\u0010.\u001a\u00020\rHÆ\u0003J\t\u0010/\u001a\u00020\u000fHÆ\u0003J{\u00100\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u0011HÆ\u0001J\u0013\u00101\u001a\u00020\u00112\b\u00102\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00103\u001a\u00020\tHÖ\u0001J\t\u00104\u001a\u000205HÖ\u0001R\u0011\u0010\u0010\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001dR\u0011\u0010\u0012\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0015R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u001bR\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001dR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\"¨\u00067"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost;", "", ContactRepository.ID_COLUMN, "", "sender", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "group", "distributionList", "viewCount", "", "replyCount", "dateInMilliseconds", "content", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content;", "conversationMessage", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "allowsReplies", "", "hasSelfViewed", "(JLorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/recipients/Recipient;IIJLorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content;Lorg/thoughtcrime/securesms/conversation/ConversationMessage;ZZ)V", "getAllowsReplies", "()Z", "getContent", "()Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content;", "getConversationMessage", "()Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "getDateInMilliseconds", "()J", "getDistributionList", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getGroup", "getHasSelfViewed", "getId", "getReplyCount", "()I", "getSender", "getViewCount", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "", "Content", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryPost {
    private final boolean allowsReplies;
    private final Content content;
    private final ConversationMessage conversationMessage;
    private final long dateInMilliseconds;
    private final Recipient distributionList;
    private final Recipient group;
    private final boolean hasSelfViewed;
    private final long id;
    private final int replyCount;
    private final Recipient sender;
    private final int viewCount;

    public final long component1() {
        return this.id;
    }

    public final boolean component10() {
        return this.allowsReplies;
    }

    public final boolean component11() {
        return this.hasSelfViewed;
    }

    public final Recipient component2() {
        return this.sender;
    }

    public final Recipient component3() {
        return this.group;
    }

    public final Recipient component4() {
        return this.distributionList;
    }

    public final int component5() {
        return this.viewCount;
    }

    public final int component6() {
        return this.replyCount;
    }

    public final long component7() {
        return this.dateInMilliseconds;
    }

    public final Content component8() {
        return this.content;
    }

    public final ConversationMessage component9() {
        return this.conversationMessage;
    }

    public final StoryPost copy(long j, Recipient recipient, Recipient recipient2, Recipient recipient3, int i, int i2, long j2, Content content, ConversationMessage conversationMessage, boolean z, boolean z2) {
        Intrinsics.checkNotNullParameter(recipient, "sender");
        Intrinsics.checkNotNullParameter(content, "content");
        Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
        return new StoryPost(j, recipient, recipient2, recipient3, i, i2, j2, content, conversationMessage, z, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryPost)) {
            return false;
        }
        StoryPost storyPost = (StoryPost) obj;
        return this.id == storyPost.id && Intrinsics.areEqual(this.sender, storyPost.sender) && Intrinsics.areEqual(this.group, storyPost.group) && Intrinsics.areEqual(this.distributionList, storyPost.distributionList) && this.viewCount == storyPost.viewCount && this.replyCount == storyPost.replyCount && this.dateInMilliseconds == storyPost.dateInMilliseconds && Intrinsics.areEqual(this.content, storyPost.content) && Intrinsics.areEqual(this.conversationMessage, storyPost.conversationMessage) && this.allowsReplies == storyPost.allowsReplies && this.hasSelfViewed == storyPost.hasSelfViewed;
    }

    public int hashCode() {
        int m = ((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id) * 31) + this.sender.hashCode()) * 31;
        Recipient recipient = this.group;
        int i = 0;
        int hashCode = (m + (recipient == null ? 0 : recipient.hashCode())) * 31;
        Recipient recipient2 = this.distributionList;
        if (recipient2 != null) {
            i = recipient2.hashCode();
        }
        int m2 = (((((((((((hashCode + i) * 31) + this.viewCount) * 31) + this.replyCount) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.dateInMilliseconds)) * 31) + this.content.hashCode()) * 31) + this.conversationMessage.hashCode()) * 31;
        boolean z = this.allowsReplies;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = (m2 + i3) * 31;
        boolean z2 = this.hasSelfViewed;
        if (!z2) {
            i2 = z2 ? 1 : 0;
        }
        return i6 + i2;
    }

    public String toString() {
        return "StoryPost(id=" + this.id + ", sender=" + this.sender + ", group=" + this.group + ", distributionList=" + this.distributionList + ", viewCount=" + this.viewCount + ", replyCount=" + this.replyCount + ", dateInMilliseconds=" + this.dateInMilliseconds + ", content=" + this.content + ", conversationMessage=" + this.conversationMessage + ", allowsReplies=" + this.allowsReplies + ", hasSelfViewed=" + this.hasSelfViewed + ')';
    }

    public StoryPost(long j, Recipient recipient, Recipient recipient2, Recipient recipient3, int i, int i2, long j2, Content content, ConversationMessage conversationMessage, boolean z, boolean z2) {
        Intrinsics.checkNotNullParameter(recipient, "sender");
        Intrinsics.checkNotNullParameter(content, "content");
        Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
        this.id = j;
        this.sender = recipient;
        this.group = recipient2;
        this.distributionList = recipient3;
        this.viewCount = i;
        this.replyCount = i2;
        this.dateInMilliseconds = j2;
        this.content = content;
        this.conversationMessage = conversationMessage;
        this.allowsReplies = z;
        this.hasSelfViewed = z2;
    }

    public final long getId() {
        return this.id;
    }

    public final Recipient getSender() {
        return this.sender;
    }

    public final Recipient getGroup() {
        return this.group;
    }

    public final Recipient getDistributionList() {
        return this.distributionList;
    }

    public final int getViewCount() {
        return this.viewCount;
    }

    public final int getReplyCount() {
        return this.replyCount;
    }

    public final long getDateInMilliseconds() {
        return this.dateInMilliseconds;
    }

    public final Content getContent() {
        return this.content;
    }

    public final ConversationMessage getConversationMessage() {
        return this.conversationMessage;
    }

    public final boolean getAllowsReplies() {
        return this.allowsReplies;
    }

    public final boolean getHasSelfViewed() {
        return this.hasSelfViewed;
    }

    /* compiled from: StoryPost.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\r\u000eB\u0011\b\u0004\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u000b\u001a\u00020\fH&R\u0012\u0010\u0005\u001a\u00020\u0006X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u0001\u0002\u000f\u0010¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content;", "", "uri", "Landroid/net/Uri;", "(Landroid/net/Uri;)V", "transferState", "", "getTransferState", "()I", "getUri", "()Landroid/net/Uri;", "isVideo", "", "AttachmentContent", "TextContent", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content$AttachmentContent;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content$TextContent;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static abstract class Content {
        private final Uri uri;

        public /* synthetic */ Content(Uri uri, DefaultConstructorMarker defaultConstructorMarker) {
            this(uri);
        }

        public abstract int getTransferState();

        public abstract boolean isVideo();

        /* compiled from: StoryPost.kt */
        @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u000b\u001a\u00020\fH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content$AttachmentContent;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content;", "attachment", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "(Lorg/thoughtcrime/securesms/attachments/Attachment;)V", "getAttachment", "()Lorg/thoughtcrime/securesms/attachments/Attachment;", "transferState", "", "getTransferState", "()I", "isVideo", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class AttachmentContent extends Content {
            private final Attachment attachment;
            private final int transferState;

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public AttachmentContent(Attachment attachment) {
                super(attachment.getUri(), null);
                Intrinsics.checkNotNullParameter(attachment, "attachment");
                this.attachment = attachment;
                this.transferState = attachment.getTransferState();
            }

            public final Attachment getAttachment() {
                return this.attachment;
            }

            @Override // org.thoughtcrime.securesms.stories.viewer.page.StoryPost.Content
            public int getTransferState() {
                return this.transferState;
            }

            @Override // org.thoughtcrime.securesms.stories.viewer.page.StoryPost.Content
            public boolean isVideo() {
                return MediaUtil.isVideo(this.attachment);
            }
        }

        private Content(Uri uri) {
            this.uri = uri;
        }

        public final Uri getUri() {
            return this.uri;
        }

        /* compiled from: StoryPost.kt */
        @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\t\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\b\u0010\u0011\u001a\u00020\u0007H\u0016R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\f¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content$TextContent;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content;", "uri", "Landroid/net/Uri;", "recordId", "", "hasBody", "", "length", "", "(Landroid/net/Uri;JZI)V", "getLength", "()I", "getRecordId", "()J", "transferState", "getTransferState", "isVideo", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class TextContent extends Content {
            private final int length;
            private final long recordId;
            private final int transferState;

            @Override // org.thoughtcrime.securesms.stories.viewer.page.StoryPost.Content
            public boolean isVideo() {
                return false;
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public TextContent(Uri uri, long j, boolean z, int i) {
                super(uri, null);
                Intrinsics.checkNotNullParameter(uri, "uri");
                this.recordId = j;
                this.length = i;
                this.transferState = z ? 0 : 3;
            }

            public final int getLength() {
                return this.length;
            }

            public final long getRecordId() {
                return this.recordId;
            }

            @Override // org.thoughtcrime.securesms.stories.viewer.page.StoryPost.Content
            public int getTransferState() {
                return this.transferState;
            }
        }
    }
}
