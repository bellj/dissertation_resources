package org.thoughtcrime.securesms.stories.settings.custom;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: PrivateStorySettingsRepository.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0005\u001a\u00020\u0006J\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\b2\u0006\u0010\u0005\u001a\u00020\u0006J\u0016\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00020\u000fJ\u0016\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u000b¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsRepository;", "", "()V", "delete", "Lio/reactivex/rxjava3/core/Completable;", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getRecord", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "getRepliesAndReactionsEnabled", "", "removeMember", "distributionListRecord", "member", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "setRepliesAndReactionsEnabled", "repliesAndReactionsEnabled", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class PrivateStorySettingsRepository {
    public final Single<DistributionListRecord> getRecord(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        Single<DistributionListRecord> subscribeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsRepository$$ExternalSyntheticLambda0
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return PrivateStorySettingsRepository.m2884$r8$lambda$zDQj7Ork3_5FQn77BVNz1WAb5Q(DistributionListId.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      Sig…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getRecord$lambda-0 */
    public static final DistributionListRecord m2886getRecord$lambda0(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "$distributionListId");
        DistributionListRecord list = SignalDatabase.Companion.distributionLists().getList(distributionListId);
        if (list != null) {
            return list;
        }
        throw new IllegalStateException("Record does not exist.".toString());
    }

    public final Completable removeMember(DistributionListRecord distributionListRecord, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(distributionListRecord, "distributionListRecord");
        Intrinsics.checkNotNullParameter(recipientId, "member");
        Completable subscribeOn = Completable.fromAction(new Action(recipientId) { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                PrivateStorySettingsRepository.$r8$lambda$L4VdDMbmoA7AJu7TuWWBa7epWbI(DistributionListRecord.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Signa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: removeMember$lambda-1 */
    public static final void m2888removeMember$lambda1(DistributionListRecord distributionListRecord, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(distributionListRecord, "$distributionListRecord");
        Intrinsics.checkNotNullParameter(recipientId, "$member");
        SignalDatabase.Companion.distributionLists().removeMemberFromList(distributionListRecord.getId(), distributionListRecord.getPrivacyMode(), recipientId);
        Stories.INSTANCE.onStorySettingsChanged(distributionListRecord.getId());
    }

    public final Completable delete(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        Completable subscribeOn = Completable.fromAction(new Action() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsRepository$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                PrivateStorySettingsRepository.m2883$r8$lambda$v1lMmikyYg01GNovzxzBdtKbiQ(DistributionListId.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Signa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: delete$lambda-2 */
    public static final void m2885delete$lambda2(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "$distributionListId");
        DistributionListDatabase.deleteList$default(SignalDatabase.Companion.distributionLists(), distributionListId, 0, 2, null);
        Stories.INSTANCE.onStorySettingsChanged(distributionListId);
    }

    public final Single<Boolean> getRepliesAndReactionsEnabled(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        Single<Boolean> subscribeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsRepository$$ExternalSyntheticLambda3
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return PrivateStorySettingsRepository.m2882$r8$lambda$sp1Iu8LNcqwED5RI5mzRq2yGQs(DistributionListId.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      Sig…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getRepliesAndReactionsEnabled$lambda-3 */
    public static final Boolean m2887getRepliesAndReactionsEnabled$lambda3(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "$distributionListId");
        return Boolean.valueOf(SignalDatabase.Companion.distributionLists().getStoryType(distributionListId).isStoryWithReplies());
    }

    public final Completable setRepliesAndReactionsEnabled(DistributionListId distributionListId, boolean z) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        Completable subscribeOn = Completable.fromAction(new Action(z) { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                PrivateStorySettingsRepository.m2881$r8$lambda$iB3MyBvU_tjSRyLOOJDM0Ds2F0(DistributionListId.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Signa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: setRepliesAndReactionsEnabled$lambda-4 */
    public static final void m2889setRepliesAndReactionsEnabled$lambda4(DistributionListId distributionListId, boolean z) {
        Intrinsics.checkNotNullParameter(distributionListId, "$distributionListId");
        SignalDatabase.Companion.distributionLists().setAllowsReplies(distributionListId, z);
        Stories.INSTANCE.onStorySettingsChanged(distributionListId);
    }
}
