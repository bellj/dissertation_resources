package org.thoughtcrime.securesms.stories.my;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: MyStoriesViewModel.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u000e\u001a\u00020\u000fH\u0014J\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/stories/my/MyStoriesRepository;", "(Lorg/thoughtcrime/securesms/stories/my/MyStoriesRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/stories/my/MyStoriesState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "resend", "Lio/reactivex/rxjava3/core/Completable;", "story", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStoriesViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final MyStoriesRepository repository;
    private final LiveData<MyStoriesState> state;
    private final Store<MyStoriesState> store;

    public MyStoriesViewModel(MyStoriesRepository myStoriesRepository) {
        Intrinsics.checkNotNullParameter(myStoriesRepository, "repository");
        this.repository = myStoriesRepository;
        Store<MyStoriesState> store = new Store<>(new MyStoriesState(null, 1, null));
        this.store = store;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        LiveData<MyStoriesState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        Disposable subscribe = myStoriesRepository.getMyStories().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                MyStoriesViewModel.m2856_init_$lambda1(MyStoriesViewModel.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getMyStories(…distributionSets) }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final LiveData<MyStoriesState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m2856_init_$lambda1(MyStoriesViewModel myStoriesViewModel, List list) {
        Intrinsics.checkNotNullParameter(myStoriesViewModel, "this$0");
        myStoriesViewModel.store.update(new Function(list) { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MyStoriesViewModel.m2857lambda1$lambda0(this.f$0, (MyStoriesState) obj);
            }
        });
    }

    /* renamed from: lambda-1$lambda-0 */
    public static final MyStoriesState m2857lambda1$lambda0(List list, MyStoriesState myStoriesState) {
        Intrinsics.checkNotNullExpressionValue(list, "distributionSets");
        return myStoriesState.copy(list);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final Completable resend(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "story");
        return this.repository.resend(messageRecord);
    }

    /* compiled from: MyStoriesViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/stories/my/MyStoriesRepository;", "(Lorg/thoughtcrime/securesms/stories/my/MyStoriesRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final MyStoriesRepository repository;

        public Factory(MyStoriesRepository myStoriesRepository) {
            Intrinsics.checkNotNullParameter(myStoriesRepository, "repository");
            this.repository = myStoriesRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new MyStoriesViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.my.MyStoriesViewModel.Factory.create");
        }
    }
}
