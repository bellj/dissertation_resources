package org.thoughtcrime.securesms.stories.settings.create;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersState;

/* compiled from: CreateStoryWithViewersViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class CreateStoryWithViewersViewModel$create$3 extends Lambda implements Function1<Throwable, Unit> {
    final /* synthetic */ CreateStoryWithViewersViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CreateStoryWithViewersViewModel$create$3(CreateStoryWithViewersViewModel createStoryWithViewersViewModel) {
        super(1);
        this.this$0 = createStoryWithViewersViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        invoke(th);
        return Unit.INSTANCE;
    }

    public final void invoke(Throwable th) {
        Intrinsics.checkNotNullParameter(th, "it");
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersViewModel$create$3$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CreateStoryWithViewersViewModel$create$3.m2871invoke$lambda0((CreateStoryWithViewersState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final CreateStoryWithViewersState m2871invoke$lambda0(CreateStoryWithViewersState createStoryWithViewersState) {
        CreateStoryWithViewersState.SaveState.Init init = CreateStoryWithViewersState.SaveState.Init.INSTANCE;
        CreateStoryWithViewersState.NameError nameError = CreateStoryWithViewersState.NameError.DUPLICATE_LABEL;
        Intrinsics.checkNotNullExpressionValue(createStoryWithViewersState, "it");
        return CreateStoryWithViewersState.copy$default(createStoryWithViewersState, null, nameError, init, 1, null);
    }
}
