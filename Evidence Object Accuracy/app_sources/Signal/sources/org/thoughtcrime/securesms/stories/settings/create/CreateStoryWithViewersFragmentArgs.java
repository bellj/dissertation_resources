package org.thoughtcrime.securesms.stories.settings.create;

import android.os.Bundle;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes3.dex */
public class CreateStoryWithViewersFragmentArgs {
    private final HashMap arguments;

    private CreateStoryWithViewersFragmentArgs() {
        this.arguments = new HashMap();
    }

    private CreateStoryWithViewersFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static CreateStoryWithViewersFragmentArgs fromBundle(Bundle bundle) {
        RecipientId[] recipientIdArr;
        CreateStoryWithViewersFragmentArgs createStoryWithViewersFragmentArgs = new CreateStoryWithViewersFragmentArgs();
        bundle.setClassLoader(CreateStoryWithViewersFragmentArgs.class.getClassLoader());
        if (bundle.containsKey(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS)) {
            Parcelable[] parcelableArray = bundle.getParcelableArray(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
            if (parcelableArray != null) {
                recipientIdArr = new RecipientId[parcelableArray.length];
                System.arraycopy(parcelableArray, 0, recipientIdArr, 0, parcelableArray.length);
            } else {
                recipientIdArr = null;
            }
            if (recipientIdArr != null) {
                createStoryWithViewersFragmentArgs.arguments.put(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, recipientIdArr);
                return createStoryWithViewersFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"recipients\" is marked as non-null but was passed a null value.");
        }
        throw new IllegalArgumentException("Required argument \"recipients\" is missing and does not have an android:defaultValue");
    }

    public RecipientId[] getRecipients() {
        return (RecipientId[]) this.arguments.get(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS)) {
            bundle.putParcelableArray(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, (RecipientId[]) this.arguments.get(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS));
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CreateStoryWithViewersFragmentArgs createStoryWithViewersFragmentArgs = (CreateStoryWithViewersFragmentArgs) obj;
        if (this.arguments.containsKey(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS) != createStoryWithViewersFragmentArgs.arguments.containsKey(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS)) {
            return false;
        }
        return getRecipients() == null ? createStoryWithViewersFragmentArgs.getRecipients() == null : getRecipients().equals(createStoryWithViewersFragmentArgs.getRecipients());
    }

    public int hashCode() {
        return 31 + Arrays.hashCode(getRecipients());
    }

    public String toString() {
        return "CreateStoryWithViewersFragmentArgs{recipients=" + getRecipients() + "}";
    }

    /* loaded from: classes3.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(CreateStoryWithViewersFragmentArgs createStoryWithViewersFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(createStoryWithViewersFragmentArgs.arguments);
        }

        public Builder(RecipientId[] recipientIdArr) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (recipientIdArr != null) {
                hashMap.put(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, recipientIdArr);
                return;
            }
            throw new IllegalArgumentException("Argument \"recipients\" is marked as non-null but was passed a null value.");
        }

        public CreateStoryWithViewersFragmentArgs build() {
            return new CreateStoryWithViewersFragmentArgs(this.arguments);
        }

        public Builder setRecipients(RecipientId[] recipientIdArr) {
            if (recipientIdArr != null) {
                this.arguments.put(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, recipientIdArr);
                return this;
            }
            throw new IllegalArgumentException("Argument \"recipients\" is marked as non-null but was passed a null value.");
        }

        public RecipientId[] getRecipients() {
            return (RecipientId[]) this.arguments.get(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        }
    }
}
