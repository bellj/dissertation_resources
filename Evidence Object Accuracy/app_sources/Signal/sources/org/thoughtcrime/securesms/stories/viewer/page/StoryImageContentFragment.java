package org.thoughtcrime.securesms.stories.viewer.page;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import java.util.ArrayList;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.stories.viewer.page.StoryCache;
import org.thoughtcrime.securesms.stories.viewer.page.StoryDisplay;
import org.thoughtcrime.securesms.stories.viewer.page.StoryImageContentFragment;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: StoryImageContentFragment.kt */
@Metadata(d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004*\u0002\u0006\u000b\u0018\u0000 $2\u00020\u0001:\u0003$%&B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u001a\u0010\u0019\u001a\u00020\u00162\b\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\b\u0010\u001e\u001a\u00020\u0016H\u0002J\u001a\u0010\u001f\u001a\u00020\u00162\u0006\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0007R\u000e\u0010\b\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0004\n\u0002\u0010\fR\u000e\u0010\r\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012¨\u0006'"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment;", "Landroidx/fragment/app/Fragment;", "()V", "blur", "Landroid/widget/ImageView;", "blurListener", "org/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment$blurListener$1", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment$blurListener$1;", "blurState", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment$LoadState;", "imageListener", "org/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment$imageListener$1", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment$imageListener$1;", "imageState", "imageView", "parentViewModel", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "getParentViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "parentViewModel$delegate", "Lkotlin/Lazy;", "loadViaCache", "", "cacheValue", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$StoryCacheValue;", "loadViaGlide", "blurHash", "Lorg/thoughtcrime/securesms/blurhash/BlurHash;", "storySize", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryDisplay$Size;", "notifyListeners", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "LoadState", "OnDestroy", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryImageContentFragment extends Fragment {
    private static final String BLUR;
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(StoryImageContentFragment.class);
    private static final String URI;
    private ImageView blur;
    private final StoryImageContentFragment$blurListener$1 blurListener = new StoryImageContentFragment$blurListener$1(this);
    private LoadState blurState;
    private final StoryImageContentFragment$imageListener$1 imageListener = new StoryImageContentFragment$imageListener$1(this);
    private LoadState imageState;
    private ImageView imageView;
    private final Lazy parentViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryViewerPageViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryImageContentFragment$parentViewModel$2
        final /* synthetic */ StoryImageContentFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            Fragment requireParentFragment = this.this$0.requireParentFragment();
            Intrinsics.checkNotNullExpressionValue(requireParentFragment, "requireParentFragment()");
            return requireParentFragment;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryImageContentFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    /* compiled from: StoryImageContentFragment.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment$LoadState;", "", "(Ljava/lang/String;I)V", "INIT", "READY", "FAILED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum LoadState {
        INIT,
        READY,
        FAILED
    }

    public StoryImageContentFragment() {
        super(R.layout.stories_image_content_fragment);
        LoadState loadState = LoadState.INIT;
        this.blurState = loadState;
        this.imageState = loadState;
    }

    private final StoryViewerPageViewModel getParentViewModel() {
        return (StoryViewerPageViewModel) this.parentViewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.image);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.image)");
        this.imageView = (ImageView) findViewById;
        View findViewById2 = view.findViewById(R.id.blur);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.blur)");
        this.blur = (ImageView) findViewById2;
        StoryDisplay.Companion companion = StoryDisplay.Companion;
        Resources resources = getResources();
        Intrinsics.checkNotNullExpressionValue(resources, "resources");
        StoryDisplay.Size storySize = companion.getStorySize(resources);
        BlurHash blurHash = (BlurHash) requireArguments().getParcelable(BLUR);
        Parcelable parcelable = requireArguments().getParcelable("DATA_URI");
        Intrinsics.checkNotNull(parcelable);
        StoryCache.StoryCacheValue fromCache = getParentViewModel().getStoryCache().getFromCache((Uri) parcelable);
        if (fromCache != null) {
            loadViaCache(fromCache);
        } else {
            loadViaGlide(blurHash, storySize);
        }
    }

    private final void loadViaCache(StoryCache.StoryCacheValue storyCacheValue) {
        Log.d(TAG, "Attachment in cache. Loading via cache...");
        StoryCache.StoryCacheTarget blurTarget = storyCacheValue.getBlurTarget();
        if (blurTarget != null) {
            blurTarget.addListener(this.blurListener);
            getViewLifecycleOwner().getLifecycle().addObserver(new OnDestroy(this, new Function0<Unit>(blurTarget, this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryImageContentFragment$loadViaCache$1
                final /* synthetic */ StoryCache.StoryCacheTarget $blurTarget;
                final /* synthetic */ StoryImageContentFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.$blurTarget = r1;
                    this.this$0 = r2;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    this.$blurTarget.removeListener(StoryImageContentFragment.access$getBlurListener$p(this.this$0));
                }
            }));
        } else {
            this.blurState = LoadState.FAILED;
            notifyListeners();
        }
        StoryCache.StoryCacheTarget imageTarget = storyCacheValue.getImageTarget();
        imageTarget.addListener(this.imageListener);
        getViewLifecycleOwner().getLifecycle().addObserver(new OnDestroy(this, new Function0<Unit>(imageTarget, this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryImageContentFragment$loadViaCache$2
            final /* synthetic */ StoryCache.StoryCacheTarget $imageTarget;
            final /* synthetic */ StoryImageContentFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$imageTarget = r1;
                this.this$0 = r2;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                this.$imageTarget.removeListener(StoryImageContentFragment.access$getBlurListener$p(this.this$0));
            }
        }));
    }

    private final void loadViaGlide(BlurHash blurHash, StoryDisplay.Size size) {
        Log.d(TAG, "Attachment not in cache. Loading via glide...");
        ImageView imageView = null;
        if (blurHash != null) {
            ImageView imageView2 = this.blur;
            if (imageView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("blur");
                imageView2 = null;
            }
            GlideRequest<Drawable> addListener = GlideApp.with(imageView2).load((Object) blurHash).override(size.getWidth(), size.getHeight()).addListener((RequestListener<Drawable>) new RequestListener<Drawable>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryImageContentFragment$loadViaGlide$1
                final /* synthetic */ StoryImageContentFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // com.bumptech.glide.request.RequestListener
                public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, DataSource dataSource, boolean z) {
                    return onResourceReady((Drawable) obj, obj2, (Target<Drawable>) target, dataSource, z);
                }

                @Override // com.bumptech.glide.request.RequestListener
                public boolean onLoadFailed(GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                    StoryImageContentFragment.access$setBlurState$p(this.this$0, StoryImageContentFragment.LoadState.FAILED);
                    StoryImageContentFragment.access$notifyListeners(this.this$0);
                    return false;
                }

                public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                    StoryImageContentFragment.access$setBlurState$p(this.this$0, StoryImageContentFragment.LoadState.READY);
                    StoryImageContentFragment.access$notifyListeners(this.this$0);
                    return false;
                }
            });
            ImageView imageView3 = this.blur;
            if (imageView3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("blur");
                imageView3 = null;
            }
            addListener.into(imageView3);
        } else {
            this.blurState = LoadState.FAILED;
            notifyListeners();
        }
        ImageView imageView4 = this.imageView;
        if (imageView4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("imageView");
            imageView4 = null;
        }
        GlideRequests with = GlideApp.with(imageView4);
        Parcelable parcelable = requireArguments().getParcelable("DATA_URI");
        Intrinsics.checkNotNull(parcelable);
        GlideRequest<Drawable> addListener2 = with.load((Object) new DecryptableStreamUriLoader.DecryptableUri((Uri) parcelable)).override(size.getWidth(), size.getHeight()).addListener((RequestListener<Drawable>) new RequestListener<Drawable>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryImageContentFragment$loadViaGlide$2
            final /* synthetic */ StoryImageContentFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // com.bumptech.glide.request.RequestListener
            public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, DataSource dataSource, boolean z) {
                return onResourceReady((Drawable) obj, obj2, (Target<Drawable>) target, dataSource, z);
            }

            @Override // com.bumptech.glide.request.RequestListener
            public boolean onLoadFailed(GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                StoryImageContentFragment.access$setImageState$p(this.this$0, StoryImageContentFragment.LoadState.FAILED);
                StoryImageContentFragment.access$notifyListeners(this.this$0);
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                StoryImageContentFragment.access$setImageState$p(this.this$0, StoryImageContentFragment.LoadState.READY);
                StoryImageContentFragment.access$notifyListeners(this.this$0);
                return false;
            }
        });
        ImageView imageView5 = this.imageView;
        if (imageView5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("imageView");
        } else {
            imageView = imageView5;
        }
        addListener2.into(imageView);
    }

    public final void notifyListeners() {
        LoadState loadState;
        MediaPreviewFragment.Events events;
        MediaPreviewFragment.Events events2;
        if (isDetached()) {
            Log.w(TAG, "Fragment is detached, dropping notify call.");
            return;
        }
        LoadState loadState2 = this.blurState;
        LoadState loadState3 = LoadState.INIT;
        if (loadState2 != loadState3 && (loadState = this.imageState) != loadState3) {
            if (loadState == LoadState.FAILED) {
                ArrayList arrayList = new ArrayList();
                try {
                    Fragment fragment = getParentFragment();
                    while (true) {
                        if (fragment == null) {
                            FragmentActivity requireActivity = requireActivity();
                            if (requireActivity != null) {
                                events2 = (MediaPreviewFragment.Events) requireActivity;
                            } else {
                                throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment.Events");
                            }
                        } else if (fragment instanceof MediaPreviewFragment.Events) {
                            events2 = fragment;
                            break;
                        } else {
                            String name = fragment.getClass().getName();
                            Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                            arrayList.add(name);
                            fragment = fragment.getParentFragment();
                        }
                    }
                    events2.mediaNotAvailable();
                } catch (ClassCastException e) {
                    String name2 = requireActivity().getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
                    arrayList.add(name2);
                    throw new ListenerNotFoundException(arrayList, e);
                }
            } else {
                ArrayList arrayList2 = new ArrayList();
                try {
                    Fragment fragment2 = getParentFragment();
                    while (true) {
                        if (fragment2 == null) {
                            FragmentActivity requireActivity2 = requireActivity();
                            if (requireActivity2 != null) {
                                events = (MediaPreviewFragment.Events) requireActivity2;
                            } else {
                                throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment.Events");
                            }
                        } else if (fragment2 instanceof MediaPreviewFragment.Events) {
                            events = fragment2;
                            break;
                        } else {
                            String name3 = fragment2.getClass().getName();
                            Intrinsics.checkNotNullExpressionValue(name3, "parent::class.java.name");
                            arrayList2.add(name3);
                            fragment2 = fragment2.getParentFragment();
                        }
                    }
                    events.onMediaReady();
                } catch (ClassCastException e2) {
                    String name4 = requireActivity().getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name4, "requireActivity()::class.java.name");
                    arrayList2.add(name4);
                    throw new ListenerNotFoundException(arrayList2, e2);
                }
            }
        }
    }

    /* compiled from: StoryImageContentFragment.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0002\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment$OnDestroy;", "Landroidx/lifecycle/DefaultLifecycleObserver;", "onDestroy", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment;Lkotlin/jvm/functions/Function0;)V", "owner", "Landroidx/lifecycle/LifecycleOwner;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public final class OnDestroy implements DefaultLifecycleObserver {
        private final Function0<Unit> onDestroy;
        final /* synthetic */ StoryImageContentFragment this$0;

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onPause(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner);
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner);
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
        }

        public OnDestroy(StoryImageContentFragment storyImageContentFragment, Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter(function0, "onDestroy");
            this.this$0 = storyImageContentFragment;
            this.onDestroy = function0;
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public void onDestroy(LifecycleOwner lifecycleOwner) {
            Intrinsics.checkNotNullParameter(lifecycleOwner, "owner");
            this.onDestroy.invoke();
        }
    }

    /* compiled from: StoryImageContentFragment.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment$Companion;", "", "()V", "BLUR", "", "TAG", "kotlin.jvm.PlatformType", "URI", "create", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment;", "attachment", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final StoryImageContentFragment create(Attachment attachment) {
            Intrinsics.checkNotNullParameter(attachment, "attachment");
            StoryImageContentFragment storyImageContentFragment = new StoryImageContentFragment();
            Bundle bundle = new Bundle();
            Uri uri = attachment.getUri();
            Intrinsics.checkNotNull(uri);
            bundle.putParcelable("DATA_URI", uri);
            bundle.putParcelable(StoryImageContentFragment.BLUR, attachment.getBlurHash());
            storyImageContentFragment.setArguments(bundle);
            return storyImageContentFragment;
        }
    }
}
