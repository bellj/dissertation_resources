package org.thoughtcrime.securesms.stories.settings.my;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyData;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipState;

/* compiled from: MyStorySettingsRepository.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004J\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0004J\b\u0010\b\u001a\u00020\u0005H\u0003J\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nJ\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u0007¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsRepository;", "", "()V", "getPrivacyState", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/stories/settings/my/MyStoryPrivacyState;", "getRepliesAndReactionsEnabled", "", "getStoryPrivacyState", "observeChooseInitialPrivacy", "Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipState;", "setPrivacyMode", "Lio/reactivex/rxjava3/core/Completable;", "privacyMode", "Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "setRepliesAndReactionsEnabled", "repliesAndReactionsEnabled", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStorySettingsRepository {
    public final Single<MyStoryPrivacyState> getPrivacyState() {
        Single<MyStoryPrivacyState> subscribeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsRepository$$ExternalSyntheticLambda3
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return MyStorySettingsRepository.m2906$r8$lambda$GCuEcUgmhB9AibK6gQYLFU0uw0(MyStorySettingsRepository.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      get…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getPrivacyState$lambda-0 */
    public static final MyStoryPrivacyState m2909getPrivacyState$lambda0(MyStorySettingsRepository myStorySettingsRepository) {
        Intrinsics.checkNotNullParameter(myStorySettingsRepository, "this$0");
        return myStorySettingsRepository.getStoryPrivacyState();
    }

    /* renamed from: observeChooseInitialPrivacy$lambda-1 */
    public static final RecipientId m2911observeChooseInitialPrivacy$lambda1() {
        DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
        DistributionListId distributionListId = DistributionListId.MY_STORY;
        Intrinsics.checkNotNullExpressionValue(distributionListId, "MY_STORY");
        RecipientId recipientId = distributionLists.getRecipientId(distributionListId);
        Intrinsics.checkNotNull(recipientId);
        return recipientId;
    }

    public final Observable<ChooseInitialMyStoryMembershipState> observeChooseInitialPrivacy() {
        Observable<ChooseInitialMyStoryMembershipState> flatMapObservable = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsRepository$$ExternalSyntheticLambda4
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return MyStorySettingsRepository.$r8$lambda$HMvHIp2UrY4V3LLdO66iBhzTy0Y();
            }
        }).subscribeOn(Schedulers.io()).flatMapObservable(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsRepository$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return MyStorySettingsRepository.$r8$lambda$KXfZq3OchwizNKvm1N0JfomtsaI(MyStorySettingsRepository.this, (RecipientId) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMapObservable, "fromCallable { SignalDat…rivacyState())) }\n      }");
        return flatMapObservable;
    }

    /* renamed from: observeChooseInitialPrivacy$lambda-3 */
    public static final ObservableSource m2912observeChooseInitialPrivacy$lambda3(MyStorySettingsRepository myStorySettingsRepository, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(myStorySettingsRepository, "this$0");
        return Recipient.observable(recipientId).flatMap(new Function(myStorySettingsRepository) { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ MyStorySettingsRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return MyStorySettingsRepository.$r8$lambda$KbiUmzLGMiTMYs5HIa08iEgxR2o(RecipientId.this, this.f$1, (Recipient) obj);
            }
        });
    }

    /* renamed from: observeChooseInitialPrivacy$lambda-3$lambda-2 */
    public static final ObservableSource m2913observeChooseInitialPrivacy$lambda3$lambda2(RecipientId recipientId, MyStorySettingsRepository myStorySettingsRepository, Recipient recipient) {
        Intrinsics.checkNotNullParameter(myStorySettingsRepository, "this$0");
        return Observable.just(new ChooseInitialMyStoryMembershipState(recipientId, myStorySettingsRepository.getStoryPrivacyState()));
    }

    public final Completable setPrivacyMode(DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "privacyMode");
        Completable subscribeOn = Completable.fromAction(new Action() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsRepository$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                MyStorySettingsRepository.m2907$r8$lambda$ak1isVtygrKb_XmSxasYZKZiCU(DistributionListPrivacyMode.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Signa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: setPrivacyMode$lambda-4 */
    public static final void m2914setPrivacyMode$lambda4(DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "$privacyMode");
        DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
        DistributionListId distributionListId = DistributionListId.MY_STORY;
        Intrinsics.checkNotNullExpressionValue(distributionListId, "MY_STORY");
        distributionLists.setPrivacyMode(distributionListId, distributionListPrivacyMode);
        Stories stories = Stories.INSTANCE;
        Intrinsics.checkNotNullExpressionValue(distributionListId, "MY_STORY");
        stories.onStorySettingsChanged(distributionListId);
    }

    public final Single<Boolean> getRepliesAndReactionsEnabled() {
        Single<Boolean> subscribeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsRepository$$ExternalSyntheticLambda6
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return MyStorySettingsRepository.m2908$r8$lambda$u0pJcCM20dP5MPYdIGtpSW27SM();
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      Sig…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getRepliesAndReactionsEnabled$lambda-5 */
    public static final Boolean m2910getRepliesAndReactionsEnabled$lambda5() {
        DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
        DistributionListId distributionListId = DistributionListId.MY_STORY;
        Intrinsics.checkNotNullExpressionValue(distributionListId, "MY_STORY");
        return Boolean.valueOf(distributionLists.getStoryType(distributionListId).isStoryWithReplies());
    }

    public final Completable setRepliesAndReactionsEnabled(boolean z) {
        Completable subscribeOn = Completable.fromAction(new Action(z) { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                MyStorySettingsRepository.$r8$lambda$4ojfFRBKzeemSZa5irix63XLfPA(this.f$0);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Signa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: setRepliesAndReactionsEnabled$lambda-6 */
    public static final void m2915setRepliesAndReactionsEnabled$lambda6(boolean z) {
        DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
        DistributionListId distributionListId = DistributionListId.MY_STORY;
        Intrinsics.checkNotNullExpressionValue(distributionListId, "MY_STORY");
        distributionLists.setAllowsReplies(distributionListId, z);
        Stories stories = Stories.INSTANCE;
        Intrinsics.checkNotNullExpressionValue(distributionListId, "MY_STORY");
        stories.onStorySettingsChanged(distributionListId);
    }

    private final MyStoryPrivacyState getStoryPrivacyState() {
        DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
        DistributionListId distributionListId = DistributionListId.MY_STORY;
        Intrinsics.checkNotNullExpressionValue(distributionListId, "MY_STORY");
        DistributionListPrivacyData privacyData = distributionLists.getPrivacyData(distributionListId);
        return new MyStoryPrivacyState(privacyData.getPrivacyMode(), privacyData.getPrivacyMode() == DistributionListPrivacyMode.ALL_EXCEPT ? privacyData.getRawMemberCount() : privacyData.getMemberCount());
    }
}
