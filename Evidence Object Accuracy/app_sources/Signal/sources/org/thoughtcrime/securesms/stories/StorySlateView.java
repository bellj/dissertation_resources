package org.thoughtcrime.securesms.stories;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: StorySlateView.kt */
@Metadata(d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 *2\u00020\u0001:\u0003)*+B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u001d\u001a\u00020\u001eH\u0002J\b\u0010\u001f\u001a\u00020\u001eH\u0002J\b\u0010 \u001a\u00020\u001eH\u0002J\u0010\u0010!\u001a\u00020\u001e2\u0006\u0010\u0019\u001a\u00020\u0018H\u0002J\u0016\u0010\"\u001a\u00020\u001e2\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\u0016J\u0012\u0010#\u001a\u00020\u001e2\b\u0010\u0019\u001a\u0004\u0018\u00010$H\u0014J\b\u0010%\u001a\u00020$H\u0014J\u0010\u0010&\u001a\u00020\u001e2\b\u0010'\u001a\u0004\u0018\u00010(R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0017\u001a\u00020\u0018@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u000e\u0010\u001c\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000¨\u0006,"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StorySlateView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "background", "Landroid/widget/ImageView;", "callback", "Lorg/thoughtcrime/securesms/stories/StorySlateView$Callback;", "getCallback", "()Lorg/thoughtcrime/securesms/stories/StorySlateView$Callback;", "setCallback", "(Lorg/thoughtcrime/securesms/stories/StorySlateView$Callback;)V", "errorBackground", "Landroid/view/View;", "errorCircle", "errorText", "Landroid/widget/TextView;", "loadingSpinner", "postId", "", "<set-?>", "Lorg/thoughtcrime/securesms/stories/StorySlateView$State;", "state", "getState", "()Lorg/thoughtcrime/securesms/stories/StorySlateView$State;", "unavailableText", "moveToErrorState", "", "moveToHiddenState", "moveToNotFoundState", "moveToProgressState", "moveToState", "onRestoreInstanceState", "Landroid/os/Parcelable;", "onSaveInstanceState", "setBackground", "blur", "Lorg/thoughtcrime/securesms/blurhash/BlurHash;", "Callback", "Companion", "State", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StorySlateView extends FrameLayout {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(StorySlateView.class);
    private final ImageView background;
    private Callback callback;
    private final View errorBackground;
    private final View errorCircle;
    private final TextView errorText;
    private final View loadingSpinner;
    private long postId;
    private State state;
    private final View unavailableText;

    /* compiled from: StorySlateView.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StorySlateView$Callback;", "", "onStateChanged", "", "state", "Lorg/thoughtcrime/securesms/stories/StorySlateView$State;", "postId", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onStateChanged(State state, long j);
    }

    /* compiled from: StorySlateView.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[State.values().length];
            iArr[State.LOADING.ordinal()] = 1;
            iArr[State.ERROR.ordinal()] = 2;
            iArr[State.RETRY.ordinal()] = 3;
            iArr[State.NOT_FOUND.ordinal()] = 4;
            iArr[State.HIDDEN.ordinal()] = 5;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StorySlateView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ StorySlateView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StorySlateView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        this.state = State.HIDDEN;
        FrameLayout.inflate(context, R.layout.stories_slate_view, this);
        View findViewById = findViewById(R.id.background);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.background)");
        this.background = (ImageView) findViewById;
        View findViewById2 = findViewById(R.id.loading_spinner);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.loading_spinner)");
        this.loadingSpinner = findViewById2;
        View findViewById3 = findViewById(R.id.error_circle);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.error_circle)");
        this.errorCircle = findViewById3;
        View findViewById4 = findViewById(R.id.stories_error_background);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.stories_error_background)");
        this.errorBackground = findViewById4;
        View findViewById5 = findViewById(R.id.unavailable);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "findViewById(R.id.unavailable)");
        this.unavailableText = findViewById5;
        View findViewById6 = findViewById(R.id.error_text);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "findViewById(R.id.error_text)");
        this.errorText = (TextView) findViewById6;
        findViewById3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.StorySlateView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StorySlateView.$r8$lambda$EgInotMbKXZ2lnfC6J4Rlh6kwgk(StorySlateView.this, view);
            }
        });
    }

    /* compiled from: StorySlateView.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StorySlateView$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final Callback getCallback() {
        return this.callback;
    }

    public final void setCallback(Callback callback) {
        this.callback = callback;
    }

    public final State getState() {
        return this.state;
    }

    public final void moveToState(State state, long j) {
        Intrinsics.checkNotNullParameter(state, "state");
        if (this.state != state || this.postId != j) {
            if (this.postId != j) {
                this.postId = j;
                moveToHiddenState();
                Callback callback = this.callback;
                if (callback != null) {
                    callback.onStateChanged(State.HIDDEN, j);
                }
            }
            int i = WhenMappings.$EnumSwitchMapping$0[state.ordinal()];
            if (i == 1) {
                moveToProgressState(State.LOADING);
            } else if (i == 2) {
                moveToErrorState();
            } else if (i == 3) {
                moveToProgressState(State.RETRY);
            } else if (i == 4) {
                moveToNotFoundState();
            } else if (i == 5) {
                moveToHiddenState();
            }
            Callback callback2 = this.callback;
            if (callback2 != null) {
                callback2.onStateChanged(state, j);
            }
        }
    }

    public final void setBackground(BlurHash blurHash) {
        if (blurHash != null) {
            GlideApp.with(this.background).load((Object) blurHash).into(this.background);
        } else {
            GlideApp.with(this.background).clear(this.background);
        }
    }

    private final void moveToProgressState(State state) {
        this.state = state;
        ViewExtensionsKt.setVisible(this, true);
        ViewExtensionsKt.setVisible(this.background, true);
        ViewExtensionsKt.setVisible(this.loadingSpinner, true);
        ViewExtensionsKt.setVisible(this.errorCircle, false);
        ViewExtensionsKt.setVisible(this.errorBackground, false);
        ViewExtensionsKt.setVisible(this.unavailableText, false);
        ViewExtensionsKt.setVisible(this.errorText, false);
    }

    private final void moveToErrorState() {
        this.state = State.ERROR;
        ViewExtensionsKt.setVisible(this, true);
        ViewExtensionsKt.setVisible(this.background, true);
        ViewExtensionsKt.setVisible(this.loadingSpinner, false);
        ViewExtensionsKt.setVisible(this.errorCircle, true);
        ViewExtensionsKt.setVisible(this.errorBackground, true);
        ViewExtensionsKt.setVisible(this.unavailableText, false);
        ViewExtensionsKt.setVisible(this.errorText, true);
        if (NetworkConstraint.isMet(ApplicationDependencies.getApplication())) {
            this.errorText.setText(R.string.StorySlateView__couldnt_load_content);
        } else {
            this.errorText.setText(R.string.StorySlateView__no_internet_connection);
        }
    }

    private final void moveToNotFoundState() {
        this.state = State.NOT_FOUND;
        ViewExtensionsKt.setVisible(this, true);
        ViewExtensionsKt.setVisible(this.background, true);
        ViewExtensionsKt.setVisible(this.loadingSpinner, false);
        ViewExtensionsKt.setVisible(this.errorCircle, false);
        ViewExtensionsKt.setVisible(this.errorBackground, false);
        ViewExtensionsKt.setVisible(this.unavailableText, true);
        ViewExtensionsKt.setVisible(this.errorText, false);
    }

    private final void moveToHiddenState() {
        this.state = State.HIDDEN;
        ViewExtensionsKt.setVisible(this, false);
    }

    @Override // android.view.View
    protected Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable("ROOT", onSaveInstanceState);
        bundle.putInt("STATE", this.state.getCode());
        bundle.putLong("ID", this.postId);
        return bundle;
    }

    @Override // android.view.View
    protected void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            Parcelable parcelable2 = bundle.getParcelable("ROOT");
            this.state = State.Companion.fromCode(bundle.getInt("STATE", State.HIDDEN.getCode()));
            this.postId = bundle.getLong("ID");
            super.onRestoreInstanceState(parcelable2);
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m2771_init_$lambda1(StorySlateView storySlateView, View view) {
        Intrinsics.checkNotNullParameter(storySlateView, "this$0");
        storySlateView.moveToState(State.RETRY, storySlateView.postId);
    }

    /* compiled from: StorySlateView.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0001\u0018\u0000 \u00102\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0010B\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000f¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StorySlateView$State;", "", "code", "", "hasClickableContent", "", "(Ljava/lang/String;IIZ)V", "getCode", "()I", "getHasClickableContent", "()Z", "LOADING", "ERROR", "RETRY", "NOT_FOUND", "HIDDEN", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum State {
        LOADING(0, false),
        ERROR(1, true),
        RETRY(2, true),
        NOT_FOUND(3, false),
        HIDDEN(4, false);
        
        public static final Companion Companion = new Companion(null);
        private final int code;
        private final boolean hasClickableContent;

        State(int i, boolean z) {
            this.code = i;
            this.hasClickableContent = z;
        }

        public final int getCode() {
            return this.code;
        }

        public final boolean getHasClickableContent() {
            return this.hasClickableContent;
        }

        /* compiled from: StorySlateView.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StorySlateView$State$Companion;", "", "()V", "fromCode", "Lorg/thoughtcrime/securesms/stories/StorySlateView$State;", "code", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final State fromCode(int i) {
                State state;
                State[] values = State.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        state = null;
                        break;
                    }
                    state = values[i2];
                    if (state.getCode() == i) {
                        break;
                    }
                    i2++;
                }
                return state == null ? State.HIDDEN : state;
            }
        }
    }
}
