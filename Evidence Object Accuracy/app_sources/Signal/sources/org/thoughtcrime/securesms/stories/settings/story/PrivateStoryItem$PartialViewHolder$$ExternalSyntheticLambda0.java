package org.thoughtcrime.securesms.stories.settings.story;

import android.view.View;
import org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class PrivateStoryItem$PartialViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ PrivateStoryItem.PartialModel f$0;

    public /* synthetic */ PrivateStoryItem$PartialViewHolder$$ExternalSyntheticLambda0(PrivateStoryItem.PartialModel partialModel) {
        this.f$0 = partialModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        PrivateStoryItem.PartialViewHolder.m2957bind$lambda0(this.f$0, view);
    }
}
