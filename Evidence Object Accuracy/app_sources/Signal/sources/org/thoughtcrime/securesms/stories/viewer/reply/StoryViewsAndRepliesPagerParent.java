package org.thoughtcrime.securesms.stories.viewer.reply;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;

/* compiled from: StoryViewsAndRepliesPagerParent.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001:\u0001\u0006R\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent;", "", "selectedChild", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent$Child;", "getSelectedChild", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent$Child;", "Child", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public interface StoryViewsAndRepliesPagerParent {
    Child getSelectedChild();

    /* compiled from: StoryViewsAndRepliesPagerParent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u0000 \u00052\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0005B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent$Child;", "", "(Ljava/lang/String;I)V", "VIEWS", "REPLIES", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum Child {
        VIEWS,
        REPLIES;
        
        public static final Companion Companion = new Companion(null);

        /* compiled from: StoryViewsAndRepliesPagerParent.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent$Child$Companion;", "", "()V", "forIndex", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent$Child;", "index", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final Child forIndex(int i) {
                if (i == 0) {
                    return Child.VIEWS;
                }
                if (i == 1) {
                    return Child.REPLIES;
                }
                throw new IllegalArgumentException();
            }
        }
    }
}
