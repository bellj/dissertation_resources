package org.thoughtcrime.securesms.stories.tabs;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.core.view.animation.PathInterpolatorCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.value.LottieFrameInfo;
import com.airbnb.lottie.value.SimpleLottieValueCallback;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: ConversationListTabsFragment.kt */
@Metadata(d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u001a\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J!\u0010\u001e\u001a\u00020\u00192\u0012\u0010\u001f\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00040 \"\u00020\u0004H\u0002¢\u0006\u0002\u0010!J)\u0010\"\u001a\u00020\u00192\u0006\u0010#\u001a\u00020\u00172\u0012\u0010\u001f\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060 \"\u00020\u0006H\u0002¢\u0006\u0002\u0010$J\u0018\u0010%\u001a\u00020\u00192\u0006\u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00020)H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0002¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0010\u0010\u0011¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "chatsIcon", "Lcom/airbnb/lottie/LottieAnimationView;", "chatsPill", "Landroid/widget/ImageView;", "chatsUnreadIndicator", "Landroid/widget/TextView;", "pillAnimator", "Landroid/animation/Animator;", "storiesIcon", "storiesPill", "storiesUnreadIndicator", "viewModel", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "formatCount", "", NewHtcHomeBadger.COUNT, "", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "runLottieAnimations", "toAnimate", "", "([Lcom/airbnb/lottie/LottieAnimationView;)V", "runPillAnimation", "duration", "(J[Landroid/widget/ImageView;)V", "update", "state", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsState;", "immediate", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ConversationListTabsFragment extends Fragment {
    private LottieAnimationView chatsIcon;
    private ImageView chatsPill;
    private TextView chatsUnreadIndicator;
    private Animator pillAnimator;
    private LottieAnimationView storiesIcon;
    private ImageView storiesPill;
    private TextView storiesUnreadIndicator;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(ConversationListTabsViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsFragment$viewModel$2
        final /* synthetic */ ConversationListTabsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    public ConversationListTabsFragment() {
        super(R.layout.conversation_list_tabs);
    }

    private final ConversationListTabsViewModel getViewModel() {
        return (ConversationListTabsViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.chats_unread_indicator);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.chats_unread_indicator)");
        this.chatsUnreadIndicator = (TextView) findViewById;
        View findViewById2 = view.findViewById(R.id.stories_unread_indicator);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.stories_unread_indicator)");
        this.storiesUnreadIndicator = (TextView) findViewById2;
        View findViewById3 = view.findViewById(R.id.chats_tab_icon);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.chats_tab_icon)");
        this.chatsIcon = (LottieAnimationView) findViewById3;
        View findViewById4 = view.findViewById(R.id.stories_tab_icon);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.stories_tab_icon)");
        this.storiesIcon = (LottieAnimationView) findViewById4;
        View findViewById5 = view.findViewById(R.id.chats_pill);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.chats_pill)");
        this.chatsPill = (ImageView) findViewById5;
        View findViewById6 = view.findViewById(R.id.stories_pill);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.stories_pill)");
        this.storiesPill = (ImageView) findViewById6;
        int color = ContextCompat.getColor(requireContext(), R.color.signal_colorOnSecondaryContainer);
        LottieAnimationView lottieAnimationView = this.chatsIcon;
        LottieAnimationView lottieAnimationView2 = null;
        if (lottieAnimationView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("chatsIcon");
            lottieAnimationView = null;
        }
        KeyPath keyPath = new KeyPath("**");
        Integer num = LottieProperty.COLOR;
        lottieAnimationView.addValueCallback(keyPath, (KeyPath) num, (SimpleLottieValueCallback<KeyPath>) new SimpleLottieValueCallback(color) { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.airbnb.lottie.value.SimpleLottieValueCallback
            public final Object getValue(LottieFrameInfo lottieFrameInfo) {
                return ConversationListTabsFragment.$r8$lambda$NbvVn2WMV_am8mo6ZKUD1ugJbUE(this.f$0, lottieFrameInfo);
            }
        });
        LottieAnimationView lottieAnimationView3 = this.storiesIcon;
        if (lottieAnimationView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storiesIcon");
        } else {
            lottieAnimationView2 = lottieAnimationView3;
        }
        lottieAnimationView2.addValueCallback(new KeyPath("**"), (KeyPath) num, (SimpleLottieValueCallback<KeyPath>) new SimpleLottieValueCallback(color) { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.airbnb.lottie.value.SimpleLottieValueCallback
            public final Object getValue(LottieFrameInfo lottieFrameInfo) {
                return ConversationListTabsFragment.$r8$lambda$mfxd8mDNGLGyGcroptgykMbMo84(this.f$0, lottieFrameInfo);
            }
        });
        view.findViewById(R.id.chats_tab_touch_point).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationListTabsFragment.$r8$lambda$Li3B8VkjG8tBdvXlucDVSTzD9tU(ConversationListTabsFragment.this, view2);
            }
        });
        view.findViewById(R.id.stories_tab_touch_point).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationListTabsFragment.m2978$r8$lambda$JTUleCCwp8MaW1ksXu2jIqWOJE(ConversationListTabsFragment.this, view2);
            }
        });
        update(getViewModel().getStateSnapshot(), true);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsFragment$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListTabsFragment.$r8$lambda$Ou9LbVDW6CR27b36_JLJgI_zYZI(ConversationListTabsFragment.this, (ConversationListTabsState) obj);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final Integer m2979onViewCreated$lambda0(int i, LottieFrameInfo lottieFrameInfo) {
        return Integer.valueOf(i);
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final Integer m2980onViewCreated$lambda1(int i, LottieFrameInfo lottieFrameInfo) {
        return Integer.valueOf(i);
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m2981onViewCreated$lambda2(ConversationListTabsFragment conversationListTabsFragment, View view) {
        Intrinsics.checkNotNullParameter(conversationListTabsFragment, "this$0");
        conversationListTabsFragment.getViewModel().onChatsSelected();
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2982onViewCreated$lambda3(ConversationListTabsFragment conversationListTabsFragment, View view) {
        Intrinsics.checkNotNullParameter(conversationListTabsFragment, "this$0");
        conversationListTabsFragment.getViewModel().onStoriesSelected();
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m2983onViewCreated$lambda4(ConversationListTabsFragment conversationListTabsFragment, ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullParameter(conversationListTabsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "it");
        conversationListTabsFragment.update(conversationListTabsState, false);
    }

    private final void update(ConversationListTabsState conversationListTabsState, boolean z) {
        LottieAnimationView lottieAnimationView = this.chatsIcon;
        TextView textView = null;
        if (lottieAnimationView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("chatsIcon");
            lottieAnimationView = null;
        }
        boolean isSelected = lottieAnimationView.isSelected();
        LottieAnimationView lottieAnimationView2 = this.chatsIcon;
        if (lottieAnimationView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("chatsIcon");
            lottieAnimationView2 = null;
        }
        boolean z2 = true;
        lottieAnimationView2.setSelected(conversationListTabsState.getTab() == ConversationListTab.CHATS);
        LottieAnimationView lottieAnimationView3 = this.storiesIcon;
        if (lottieAnimationView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storiesIcon");
            lottieAnimationView3 = null;
        }
        lottieAnimationView3.setSelected(conversationListTabsState.getTab() == ConversationListTab.STORIES);
        ImageView imageView = this.chatsPill;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("chatsPill");
            imageView = null;
        }
        LottieAnimationView lottieAnimationView4 = this.chatsIcon;
        if (lottieAnimationView4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("chatsIcon");
            lottieAnimationView4 = null;
        }
        imageView.setSelected(lottieAnimationView4.isSelected());
        ImageView imageView2 = this.storiesPill;
        if (imageView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storiesPill");
            imageView2 = null;
        }
        LottieAnimationView lottieAnimationView5 = this.storiesIcon;
        if (lottieAnimationView5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storiesIcon");
            lottieAnimationView5 = null;
        }
        imageView2.setSelected(lottieAnimationView5.isSelected());
        LottieAnimationView lottieAnimationView6 = this.chatsIcon;
        if (lottieAnimationView6 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("chatsIcon");
            lottieAnimationView6 = null;
        }
        boolean isSelected2 = isSelected ^ lottieAnimationView6.isSelected();
        if (z) {
            LottieAnimationView lottieAnimationView7 = this.chatsIcon;
            if (lottieAnimationView7 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("chatsIcon");
                lottieAnimationView7 = null;
            }
            lottieAnimationView7.pauseAnimation();
            LottieAnimationView lottieAnimationView8 = this.storiesIcon;
            if (lottieAnimationView8 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storiesIcon");
                lottieAnimationView8 = null;
            }
            lottieAnimationView8.pauseAnimation();
            LottieAnimationView lottieAnimationView9 = this.chatsIcon;
            if (lottieAnimationView9 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("chatsIcon");
                lottieAnimationView9 = null;
            }
            LottieAnimationView lottieAnimationView10 = this.chatsIcon;
            if (lottieAnimationView10 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("chatsIcon");
                lottieAnimationView10 = null;
            }
            float f = 1.0f;
            lottieAnimationView9.setProgress(lottieAnimationView10.isSelected() ? 1.0f : 0.0f);
            LottieAnimationView lottieAnimationView11 = this.storiesIcon;
            if (lottieAnimationView11 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storiesIcon");
                lottieAnimationView11 = null;
            }
            LottieAnimationView lottieAnimationView12 = this.storiesIcon;
            if (lottieAnimationView12 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storiesIcon");
                lottieAnimationView12 = null;
            }
            if (!lottieAnimationView12.isSelected()) {
                f = 0.0f;
            }
            lottieAnimationView11.setProgress(f);
            ImageView[] imageViewArr = new ImageView[2];
            ImageView imageView3 = this.chatsPill;
            if (imageView3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("chatsPill");
                imageView3 = null;
            }
            imageViewArr[0] = imageView3;
            ImageView imageView4 = this.storiesPill;
            if (imageView4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storiesPill");
                imageView4 = null;
            }
            imageViewArr[1] = imageView4;
            runPillAnimation(0, imageViewArr);
        } else if (isSelected2) {
            LottieAnimationView[] lottieAnimationViewArr = new LottieAnimationView[2];
            LottieAnimationView lottieAnimationView13 = this.chatsIcon;
            if (lottieAnimationView13 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("chatsIcon");
                lottieAnimationView13 = null;
            }
            lottieAnimationViewArr[0] = lottieAnimationView13;
            LottieAnimationView lottieAnimationView14 = this.storiesIcon;
            if (lottieAnimationView14 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storiesIcon");
                lottieAnimationView14 = null;
            }
            lottieAnimationViewArr[1] = lottieAnimationView14;
            runLottieAnimations(lottieAnimationViewArr);
            ImageView[] imageViewArr2 = new ImageView[2];
            ImageView imageView5 = this.chatsPill;
            if (imageView5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("chatsPill");
                imageView5 = null;
            }
            imageViewArr2[0] = imageView5;
            ImageView imageView6 = this.storiesPill;
            if (imageView6 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storiesPill");
                imageView6 = null;
            }
            imageViewArr2[1] = imageView6;
            runPillAnimation(150, imageViewArr2);
        }
        TextView textView2 = this.chatsUnreadIndicator;
        if (textView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("chatsUnreadIndicator");
            textView2 = null;
        }
        ViewExtensionsKt.setVisible(textView2, conversationListTabsState.getUnreadChatsCount() > 0);
        TextView textView3 = this.chatsUnreadIndicator;
        if (textView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("chatsUnreadIndicator");
            textView3 = null;
        }
        textView3.setText(formatCount(conversationListTabsState.getUnreadChatsCount()));
        TextView textView4 = this.storiesUnreadIndicator;
        if (textView4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storiesUnreadIndicator");
            textView4 = null;
        }
        if (conversationListTabsState.getUnreadStoriesCount() <= 0) {
            z2 = false;
        }
        ViewExtensionsKt.setVisible(textView4, z2);
        TextView textView5 = this.storiesUnreadIndicator;
        if (textView5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storiesUnreadIndicator");
        } else {
            textView = textView5;
        }
        textView.setText(formatCount(conversationListTabsState.getUnreadStoriesCount()));
        View requireView = requireView();
        Intrinsics.checkNotNullExpressionValue(requireView, "requireView()");
        ViewExtensionsKt.setVisible(requireView, conversationListTabsState.getVisibilityState().isVisible());
    }

    /* renamed from: runPillAnimation$lambda-10$lambda-9$lambda-8$lambda-7 */
    public static final void m2984runPillAnimation$lambda10$lambda9$lambda8$lambda7(ImageView imageView, ValueAnimator valueAnimator) {
        Intrinsics.checkNotNullParameter(imageView, "$view");
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            int intValue = ((Integer) animatedValue).intValue();
            Object animatedValue2 = valueAnimator.getAnimatedValue();
            if (animatedValue2 != null) {
                imageView.setPadding(intValue, 0, ((Integer) animatedValue2).intValue(), 0);
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type kotlin.Int");
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Int");
    }

    private final String formatCount(long j) {
        if (j > 99) {
            String string = getString(R.string.ConversationListTabs__99p);
            Intrinsics.checkNotNullExpressionValue(string, "getString(R.string.ConversationListTabs__99p)");
            return string;
        }
        String format = NumberFormat.getInstance().format(j);
        Intrinsics.checkNotNullExpressionValue(format, "getInstance().format(count)");
        return format;
    }

    private final void runLottieAnimations(LottieAnimationView... lottieAnimationViewArr) {
        for (LottieAnimationView lottieAnimationView : lottieAnimationViewArr) {
            if (lottieAnimationView.isSelected()) {
                lottieAnimationView.resumeAnimation();
            } else {
                if (lottieAnimationView.isAnimating()) {
                    lottieAnimationView.pauseAnimation();
                }
                lottieAnimationView.setProgress(0.0f);
            }
        }
    }

    private final void runPillAnimation(long j, ImageView... imageViewArr) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (ImageView imageView : imageViewArr) {
            if (imageView.isSelected()) {
                arrayList.add(imageView);
            } else {
                arrayList2.add(imageView);
            }
        }
        Pair pair = new Pair(arrayList, arrayList2);
        List<ImageView> list = (List) pair.component1();
        List<ImageView> list2 = (List) pair.component2();
        Animator animator = this.pillAnimator;
        if (animator != null) {
            animator.cancel();
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(j);
        animatorSet.setInterpolator(PathInterpolatorCompat.create(0.17f, 0.17f, 0.0f, 1.0f));
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (ImageView imageView2 : list) {
            imageView2.setVisibility(0);
            ValueAnimator ofInt = ValueAnimator.ofInt(imageView2.getPaddingLeft(), 0);
            ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(imageView2) { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsFragment$$ExternalSyntheticLambda0
                public final /* synthetic */ ImageView f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    ConversationListTabsFragment.$r8$lambda$alCiUEAYPmV6HdlTM0QyJFNNIbE(this.f$0, valueAnimator);
                }
            });
            arrayList3.add(ofInt);
        }
        animatorSet.playTogether(arrayList3);
        animatorSet.start();
        this.pillAnimator = animatorSet;
        for (ImageView imageView3 : list2) {
            int pixels = (int) DimensionUnit.DP.toPixels(16.0f);
            imageView3.setPadding(pixels, 0, pixels, 0);
            imageView3.setVisibility(4);
        }
    }
}
