package org.thoughtcrime.securesms.stories.viewer.reply.direct;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: StoryDirectReplyState.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0003J!\u0010\r\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyState;", "", "groupDirectReplyRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "storyRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/database/model/MessageRecord;)V", "getGroupDirectReplyRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getStoryRecord", "()Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryDirectReplyState {
    private final Recipient groupDirectReplyRecipient;
    private final MessageRecord storyRecord;

    public StoryDirectReplyState() {
        this(null, null, 3, null);
    }

    public static /* synthetic */ StoryDirectReplyState copy$default(StoryDirectReplyState storyDirectReplyState, Recipient recipient, MessageRecord messageRecord, int i, Object obj) {
        if ((i & 1) != 0) {
            recipient = storyDirectReplyState.groupDirectReplyRecipient;
        }
        if ((i & 2) != 0) {
            messageRecord = storyDirectReplyState.storyRecord;
        }
        return storyDirectReplyState.copy(recipient, messageRecord);
    }

    public final Recipient component1() {
        return this.groupDirectReplyRecipient;
    }

    public final MessageRecord component2() {
        return this.storyRecord;
    }

    public final StoryDirectReplyState copy(Recipient recipient, MessageRecord messageRecord) {
        return new StoryDirectReplyState(recipient, messageRecord);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryDirectReplyState)) {
            return false;
        }
        StoryDirectReplyState storyDirectReplyState = (StoryDirectReplyState) obj;
        return Intrinsics.areEqual(this.groupDirectReplyRecipient, storyDirectReplyState.groupDirectReplyRecipient) && Intrinsics.areEqual(this.storyRecord, storyDirectReplyState.storyRecord);
    }

    public int hashCode() {
        Recipient recipient = this.groupDirectReplyRecipient;
        int i = 0;
        int hashCode = (recipient == null ? 0 : recipient.hashCode()) * 31;
        MessageRecord messageRecord = this.storyRecord;
        if (messageRecord != null) {
            i = messageRecord.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "StoryDirectReplyState(groupDirectReplyRecipient=" + this.groupDirectReplyRecipient + ", storyRecord=" + this.storyRecord + ')';
    }

    public StoryDirectReplyState(Recipient recipient, MessageRecord messageRecord) {
        this.groupDirectReplyRecipient = recipient;
        this.storyRecord = messageRecord;
    }

    public /* synthetic */ StoryDirectReplyState(Recipient recipient, MessageRecord messageRecord, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : recipient, (i & 2) != 0 ? null : messageRecord);
    }

    public final Recipient getGroupDirectReplyRecipient() {
        return this.groupDirectReplyRecipient;
    }

    public final MessageRecord getStoryRecord() {
        return this.storyRecord;
    }
}
