package org.thoughtcrime.securesms.stories;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: StoryViewerArgs.kt */
@Metadata(d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b%\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0001AB\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r\u0012\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00030\u000f\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0012\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0005¢\u0006\u0002\u0010\u0017J\t\u0010'\u001a\u00020\u0003HÆ\u0003J\t\u0010(\u001a\u00020\u0005HÆ\u0003J\t\u0010)\u001a\u00020\u0005HÆ\u0003J\t\u0010*\u001a\u00020\u0005HÆ\u0003J\t\u0010+\u001a\u00020\u0005HÆ\u0003J\t\u0010,\u001a\u00020\u0005HÆ\u0003J\t\u0010-\u001a\u00020\u0007HÆ\u0003J\u000b\u0010.\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010/\u001a\u0004\u0018\u00010\u000bHÆ\u0003J\u000b\u00100\u001a\u0004\u0018\u00010\rHÆ\u0003J\u000f\u00101\u001a\b\u0012\u0004\u0012\u00020\u00030\u000fHÆ\u0003J\t\u00102\u001a\u00020\u0005HÆ\u0003J\t\u00103\u001a\u00020\u0012HÆ\u0003J\u0001\u00104\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r2\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00030\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u00052\b\b\u0002\u0010\u0014\u001a\u00020\u00052\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\u0005HÆ\u0001J\t\u00105\u001a\u00020\u0012HÖ\u0001J\u0013\u00106\u001a\u00020\u00052\b\u00107\u001a\u0004\u0018\u000108HÖ\u0003J\t\u00109\u001a\u00020\u0012HÖ\u0001J\t\u0010:\u001a\u00020;HÖ\u0001J\u0019\u0010<\u001a\u00020=2\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u0014\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u001aR\u0011\u0010\u0016\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u001aR\u0011\u0010\u0010\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u001aR\u0011\u0010\u0015\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u001aR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u001aR\u0011\u0010\u0013\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u001aR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0017\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00030\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0013\u0010\f\u001a\u0004\u0018\u00010\r¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&¨\u0006B"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryViewerArgs;", "Landroid/os/Parcelable;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "isInHiddenStoryMode", "", "storyId", "", "storyThumbTextModel", "Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;", "storyThumbUri", "Landroid/net/Uri;", "storyThumbBlur", "Lorg/thoughtcrime/securesms/blurhash/BlurHash;", "recipientIds", "", "isFromNotification", "groupReplyStartPosition", "", "isUnviewedOnly", "isFromInfoContextMenuAction", "isFromQuote", "isFromMyStories", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;ZJLorg/thoughtcrime/securesms/stories/StoryTextPostModel;Landroid/net/Uri;Lorg/thoughtcrime/securesms/blurhash/BlurHash;Ljava/util/List;ZIZZZZ)V", "getGroupReplyStartPosition", "()I", "()Z", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getRecipientIds", "()Ljava/util/List;", "getStoryId", "()J", "getStoryThumbBlur", "()Lorg/thoughtcrime/securesms/blurhash/BlurHash;", "getStoryThumbTextModel", "()Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;", "getStoryThumbUri", "()Landroid/net/Uri;", "component1", "component10", "component11", "component12", "component13", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "describeContents", "equals", "other", "", "hashCode", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Builder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerArgs implements Parcelable {
    public static final Parcelable.Creator<StoryViewerArgs> CREATOR = new Creator();
    private final int groupReplyStartPosition;
    private final boolean isFromInfoContextMenuAction;
    private final boolean isFromMyStories;
    private final boolean isFromNotification;
    private final boolean isFromQuote;
    private final boolean isInHiddenStoryMode;
    private final boolean isUnviewedOnly;
    private final RecipientId recipientId;
    private final List<RecipientId> recipientIds;
    private final long storyId;
    private final BlurHash storyThumbBlur;
    private final StoryTextPostModel storyThumbTextModel;
    private final Uri storyThumbUri;

    /* compiled from: StoryViewerArgs.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Creator implements Parcelable.Creator<StoryViewerArgs> {
        @Override // android.os.Parcelable.Creator
        public final StoryViewerArgs createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            RecipientId recipientId = (RecipientId) parcel.readParcelable(StoryViewerArgs.class.getClassLoader());
            boolean z = parcel.readInt() != 0;
            long readLong = parcel.readLong();
            StoryTextPostModel storyTextPostModel = (StoryTextPostModel) parcel.readParcelable(StoryViewerArgs.class.getClassLoader());
            Uri uri = (Uri) parcel.readParcelable(StoryViewerArgs.class.getClassLoader());
            BlurHash blurHash = (BlurHash) parcel.readParcelable(StoryViewerArgs.class.getClassLoader());
            int readInt = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt);
            for (int i = 0; i != readInt; i++) {
                arrayList.add(parcel.readParcelable(StoryViewerArgs.class.getClassLoader()));
            }
            return new StoryViewerArgs(recipientId, z, readLong, storyTextPostModel, uri, blurHash, arrayList, parcel.readInt() != 0, parcel.readInt(), parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0);
        }

        @Override // android.os.Parcelable.Creator
        public final StoryViewerArgs[] newArray(int i) {
            return new StoryViewerArgs[i];
        }
    }

    public final RecipientId component1() {
        return this.recipientId;
    }

    public final boolean component10() {
        return this.isUnviewedOnly;
    }

    public final boolean component11() {
        return this.isFromInfoContextMenuAction;
    }

    public final boolean component12() {
        return this.isFromQuote;
    }

    public final boolean component13() {
        return this.isFromMyStories;
    }

    public final boolean component2() {
        return this.isInHiddenStoryMode;
    }

    public final long component3() {
        return this.storyId;
    }

    public final StoryTextPostModel component4() {
        return this.storyThumbTextModel;
    }

    public final Uri component5() {
        return this.storyThumbUri;
    }

    public final BlurHash component6() {
        return this.storyThumbBlur;
    }

    public final List<RecipientId> component7() {
        return this.recipientIds;
    }

    public final boolean component8() {
        return this.isFromNotification;
    }

    public final int component9() {
        return this.groupReplyStartPosition;
    }

    public final StoryViewerArgs copy(RecipientId recipientId, boolean z, long j, StoryTextPostModel storyTextPostModel, Uri uri, BlurHash blurHash, List<? extends RecipientId> list, boolean z2, int i, boolean z3, boolean z4, boolean z5, boolean z6) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(list, "recipientIds");
        return new StoryViewerArgs(recipientId, z, j, storyTextPostModel, uri, blurHash, list, z2, i, z3, z4, z5, z6);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryViewerArgs)) {
            return false;
        }
        StoryViewerArgs storyViewerArgs = (StoryViewerArgs) obj;
        return Intrinsics.areEqual(this.recipientId, storyViewerArgs.recipientId) && this.isInHiddenStoryMode == storyViewerArgs.isInHiddenStoryMode && this.storyId == storyViewerArgs.storyId && Intrinsics.areEqual(this.storyThumbTextModel, storyViewerArgs.storyThumbTextModel) && Intrinsics.areEqual(this.storyThumbUri, storyViewerArgs.storyThumbUri) && Intrinsics.areEqual(this.storyThumbBlur, storyViewerArgs.storyThumbBlur) && Intrinsics.areEqual(this.recipientIds, storyViewerArgs.recipientIds) && this.isFromNotification == storyViewerArgs.isFromNotification && this.groupReplyStartPosition == storyViewerArgs.groupReplyStartPosition && this.isUnviewedOnly == storyViewerArgs.isUnviewedOnly && this.isFromInfoContextMenuAction == storyViewerArgs.isFromInfoContextMenuAction && this.isFromQuote == storyViewerArgs.isFromQuote && this.isFromMyStories == storyViewerArgs.isFromMyStories;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int hashCode = this.recipientId.hashCode() * 31;
        boolean z = this.isInHiddenStoryMode;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int m = (((hashCode + i2) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.storyId)) * 31;
        StoryTextPostModel storyTextPostModel = this.storyThumbTextModel;
        int i5 = 0;
        int hashCode2 = (m + (storyTextPostModel == null ? 0 : storyTextPostModel.hashCode())) * 31;
        Uri uri = this.storyThumbUri;
        int hashCode3 = (hashCode2 + (uri == null ? 0 : uri.hashCode())) * 31;
        BlurHash blurHash = this.storyThumbBlur;
        if (blurHash != null) {
            i5 = blurHash.hashCode();
        }
        int hashCode4 = (((hashCode3 + i5) * 31) + this.recipientIds.hashCode()) * 31;
        boolean z2 = this.isFromNotification;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (((hashCode4 + i6) * 31) + this.groupReplyStartPosition) * 31;
        boolean z3 = this.isUnviewedOnly;
        if (z3) {
            z3 = true;
        }
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = (i9 + i10) * 31;
        boolean z4 = this.isFromInfoContextMenuAction;
        if (z4) {
            z4 = true;
        }
        int i14 = z4 ? 1 : 0;
        int i15 = z4 ? 1 : 0;
        int i16 = z4 ? 1 : 0;
        int i17 = (i13 + i14) * 31;
        boolean z5 = this.isFromQuote;
        if (z5) {
            z5 = true;
        }
        int i18 = z5 ? 1 : 0;
        int i19 = z5 ? 1 : 0;
        int i20 = z5 ? 1 : 0;
        int i21 = (i17 + i18) * 31;
        boolean z6 = this.isFromMyStories;
        if (!z6) {
            i = z6 ? 1 : 0;
        }
        return i21 + i;
    }

    @Override // java.lang.Object
    public String toString() {
        return "StoryViewerArgs(recipientId=" + this.recipientId + ", isInHiddenStoryMode=" + this.isInHiddenStoryMode + ", storyId=" + this.storyId + ", storyThumbTextModel=" + this.storyThumbTextModel + ", storyThumbUri=" + this.storyThumbUri + ", storyThumbBlur=" + this.storyThumbBlur + ", recipientIds=" + this.recipientIds + ", isFromNotification=" + this.isFromNotification + ", groupReplyStartPosition=" + this.groupReplyStartPosition + ", isUnviewedOnly=" + this.isUnviewedOnly + ", isFromInfoContextMenuAction=" + this.isFromInfoContextMenuAction + ", isFromQuote=" + this.isFromQuote + ", isFromMyStories=" + this.isFromMyStories + ')';
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "out");
        parcel.writeParcelable(this.recipientId, i);
        parcel.writeInt(this.isInHiddenStoryMode ? 1 : 0);
        parcel.writeLong(this.storyId);
        parcel.writeParcelable(this.storyThumbTextModel, i);
        parcel.writeParcelable(this.storyThumbUri, i);
        parcel.writeParcelable(this.storyThumbBlur, i);
        List<RecipientId> list = this.recipientIds;
        parcel.writeInt(list.size());
        for (RecipientId recipientId : list) {
            parcel.writeParcelable(recipientId, i);
        }
        parcel.writeInt(this.isFromNotification ? 1 : 0);
        parcel.writeInt(this.groupReplyStartPosition);
        parcel.writeInt(this.isUnviewedOnly ? 1 : 0);
        parcel.writeInt(this.isFromInfoContextMenuAction ? 1 : 0);
        parcel.writeInt(this.isFromQuote ? 1 : 0);
        parcel.writeInt(this.isFromMyStories ? 1 : 0);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r9v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
    /* JADX WARN: Multi-variable type inference failed */
    public StoryViewerArgs(RecipientId recipientId, boolean z, long j, StoryTextPostModel storyTextPostModel, Uri uri, BlurHash blurHash, List<? extends RecipientId> list, boolean z2, int i, boolean z3, boolean z4, boolean z5, boolean z6) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(list, "recipientIds");
        this.recipientId = recipientId;
        this.isInHiddenStoryMode = z;
        this.storyId = j;
        this.storyThumbTextModel = storyTextPostModel;
        this.storyThumbUri = uri;
        this.storyThumbBlur = blurHash;
        this.recipientIds = list;
        this.isFromNotification = z2;
        this.groupReplyStartPosition = i;
        this.isUnviewedOnly = z3;
        this.isFromInfoContextMenuAction = z4;
        this.isFromQuote = z5;
        this.isFromMyStories = z6;
    }

    public final RecipientId getRecipientId() {
        return this.recipientId;
    }

    public final boolean isInHiddenStoryMode() {
        return this.isInHiddenStoryMode;
    }

    public final long getStoryId() {
        return this.storyId;
    }

    public final StoryTextPostModel getStoryThumbTextModel() {
        return this.storyThumbTextModel;
    }

    public final Uri getStoryThumbUri() {
        return this.storyThumbUri;
    }

    public final BlurHash getStoryThumbBlur() {
        return this.storyThumbBlur;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ StoryViewerArgs(org.thoughtcrime.securesms.recipients.RecipientId r19, boolean r20, long r21, org.thoughtcrime.securesms.stories.StoryTextPostModel r23, android.net.Uri r24, org.thoughtcrime.securesms.blurhash.BlurHash r25, java.util.List r26, boolean r27, int r28, boolean r29, boolean r30, boolean r31, boolean r32, int r33, kotlin.jvm.internal.DefaultConstructorMarker r34) {
        /*
            r18 = this;
            r0 = r33
            r1 = r0 & 4
            if (r1 == 0) goto L_0x000a
            r1 = -1
            r6 = r1
            goto L_0x000c
        L_0x000a:
            r6 = r21
        L_0x000c:
            r1 = r0 & 8
            r2 = 0
            if (r1 == 0) goto L_0x0013
            r8 = r2
            goto L_0x0015
        L_0x0013:
            r8 = r23
        L_0x0015:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x001b
            r9 = r2
            goto L_0x001d
        L_0x001b:
            r9 = r24
        L_0x001d:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0023
            r10 = r2
            goto L_0x0025
        L_0x0023:
            r10 = r25
        L_0x0025:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x002f
            java.util.List r1 = kotlin.collections.CollectionsKt.emptyList()
            r11 = r1
            goto L_0x0031
        L_0x002f:
            r11 = r26
        L_0x0031:
            r1 = r0 & 128(0x80, float:1.794E-43)
            r2 = 0
            if (r1 == 0) goto L_0x0038
            r12 = 0
            goto L_0x003a
        L_0x0038:
            r12 = r27
        L_0x003a:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x0041
            r1 = -1
            r13 = -1
            goto L_0x0043
        L_0x0041:
            r13 = r28
        L_0x0043:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L_0x0049
            r14 = 0
            goto L_0x004b
        L_0x0049:
            r14 = r29
        L_0x004b:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x0051
            r15 = 0
            goto L_0x0053
        L_0x0051:
            r15 = r30
        L_0x0053:
            r1 = r0 & 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x005a
            r16 = 0
            goto L_0x005c
        L_0x005a:
            r16 = r31
        L_0x005c:
            r0 = r0 & 4096(0x1000, float:5.74E-42)
            if (r0 == 0) goto L_0x0063
            r17 = 0
            goto L_0x0065
        L_0x0063:
            r17 = r32
        L_0x0065:
            r3 = r18
            r4 = r19
            r5 = r20
            r3.<init>(r4, r5, r6, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.StoryViewerArgs.<init>(org.thoughtcrime.securesms.recipients.RecipientId, boolean, long, org.thoughtcrime.securesms.stories.StoryTextPostModel, android.net.Uri, org.thoughtcrime.securesms.blurhash.BlurHash, java.util.List, boolean, int, boolean, boolean, boolean, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final List<RecipientId> getRecipientIds() {
        return this.recipientIds;
    }

    public final boolean isFromNotification() {
        return this.isFromNotification;
    }

    public final int getGroupReplyStartPosition() {
        return this.groupReplyStartPosition;
    }

    public final boolean isUnviewedOnly() {
        return this.isUnviewedOnly;
    }

    public final boolean isFromInfoContextMenuAction() {
        return this.isFromInfoContextMenuAction;
    }

    public final boolean isFromQuote() {
        return this.isFromQuote;
    }

    public final boolean isFromMyStories() {
        return this.isFromMyStories;
    }

    /* compiled from: StoryViewerArgs.kt */
    @Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010 \n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0017\u001a\u00020\u0018J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u0005J\u000e\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0005J\u000e\u0010\f\u001a\u00020\u00002\u0006\u0010\f\u001a\u00020\u0005J\u000e\u0010\u0019\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\bJ\u0014\u0010\u001a\u001a\u00020\u00002\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u000eJ\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u000f\u001a\u00020\u0010J\u0010\u0010\u001c\u001a\u00020\u00002\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u0010\u0010\u001d\u001a\u00020\u00002\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014J\u0010\u0010\u001e\u001a\u00020\u00002\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u000eX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u000e¢\u0006\u0002\n\u0000¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryViewerArgs$Builder;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "isInHiddenStoryMode", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Z)V", "groupReplyStartPosition", "", "isFromInfoContextMenuAction", "isFromNotification", "isFromQuote", "isUnviewedOnly", "recipientIds", "", "storyId", "", "storyThumbBlur", "Lorg/thoughtcrime/securesms/blurhash/BlurHash;", "storyThumbTextModel", "Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;", "storyThumbUri", "Landroid/net/Uri;", "build", "Lorg/thoughtcrime/securesms/stories/StoryViewerArgs;", "withGroupReplyStartPosition", "withRecipientIds", "withStoryId", "withStoryThumbBlur", "withStoryThumbTextModel", "withStoryThumbUri", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Builder {
        private int groupReplyStartPosition = -1;
        private boolean isFromInfoContextMenuAction;
        private boolean isFromNotification;
        private boolean isFromQuote;
        private final boolean isInHiddenStoryMode;
        private boolean isUnviewedOnly;
        private final RecipientId recipientId;
        private List<? extends RecipientId> recipientIds = CollectionsKt__CollectionsKt.emptyList();
        private long storyId = -1;
        private BlurHash storyThumbBlur;
        private StoryTextPostModel storyThumbTextModel;
        private Uri storyThumbUri;

        public Builder(RecipientId recipientId, boolean z) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
            this.isInHiddenStoryMode = z;
        }

        public final Builder withStoryId(long j) {
            this.storyId = j;
            return this;
        }

        public final Builder withStoryThumbTextModel(StoryTextPostModel storyTextPostModel) {
            this.storyThumbTextModel = storyTextPostModel;
            return this;
        }

        public final Builder withStoryThumbUri(Uri uri) {
            this.storyThumbUri = uri;
            return this;
        }

        public final Builder withStoryThumbBlur(BlurHash blurHash) {
            this.storyThumbBlur = blurHash;
            return this;
        }

        public final Builder withRecipientIds(List<? extends RecipientId> list) {
            Intrinsics.checkNotNullParameter(list, "recipientIds");
            this.recipientIds = list;
            return this;
        }

        public final Builder isFromNotification(boolean z) {
            this.isFromNotification = z;
            return this;
        }

        public final Builder withGroupReplyStartPosition(int i) {
            this.groupReplyStartPosition = i;
            return this;
        }

        public final Builder isUnviewedOnly(boolean z) {
            this.isUnviewedOnly = z;
            return this;
        }

        public final Builder isFromQuote(boolean z) {
            this.isFromQuote = z;
            return this;
        }

        public final StoryViewerArgs build() {
            return new StoryViewerArgs(this.recipientId, this.isInHiddenStoryMode, this.storyId, this.storyThumbTextModel, this.storyThumbUri, this.storyThumbBlur, this.recipientIds, this.isFromNotification, this.groupReplyStartPosition, this.isUnviewedOnly, this.isFromInfoContextMenuAction, this.isFromQuote, false, RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT, null);
        }
    }
}
