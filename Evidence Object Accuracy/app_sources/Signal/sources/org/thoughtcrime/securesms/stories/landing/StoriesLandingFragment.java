package org.thoughtcrime.securesms.stories.landing;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import androidx.activity.OnBackPressedCallback;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.app.SharedElementCallback;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Predicate;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.main.Material3OnScrollHelperBinder;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.stories.StoryTextPostModel;
import org.thoughtcrime.securesms.stories.StoryViewerArgs;
import org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu;
import org.thoughtcrime.securesms.stories.dialogs.StoryDialogs;
import org.thoughtcrime.securesms.stories.landing.ExpandHeader;
import org.thoughtcrime.securesms.stories.landing.MyStoriesItem;
import org.thoughtcrime.securesms.stories.landing.StoriesLandingItem;
import org.thoughtcrime.securesms.stories.landing.StoriesLandingState;
import org.thoughtcrime.securesms.stories.landing.StoriesLandingViewModel;
import org.thoughtcrime.securesms.stories.my.MyStoriesActivity;
import org.thoughtcrime.securesms.stories.settings.StorySettingsActivity;
import org.thoughtcrime.securesms.stories.tabs.ConversationListTab;
import org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerActivity;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: StoriesLandingFragment.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 @2\u00020\u0001:\u0001@B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0010\u0010 \u001a\u00020\u00172\u0006\u0010!\u001a\u00020\u0019H\u0002J\u0010\u0010\"\u001a\u00020\u00172\u0006\u0010!\u001a\u00020\u0019H\u0002J\u0012\u0010#\u001a\u00020\u00172\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\u0018\u0010&\u001a\u00020\u00172\u0006\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0016J\u0010\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0016J-\u0010/\u001a\u00020\u00172\u0006\u00100\u001a\u0002012\u000e\u00102\u001a\n\u0012\u0006\b\u0001\u0012\u000204032\u0006\u00105\u001a\u000206H\u0016¢\u0006\u0002\u00107J\b\u00108\u001a\u00020\u0017H\u0016J \u00109\u001a\u00020\u00172\u0006\u0010!\u001a\u00020\u00192\u0006\u0010:\u001a\u00020\b2\u0006\u0010;\u001a\u00020,H\u0002J\u001c\u0010<\u001a\u00020\u00172\u0006\u0010=\u001a\u00020>2\n\b\u0002\u0010?\u001a\u0004\u0018\u00010%H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000eR\u001b\u0010\u0011\u001a\u00020\u00128BX\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0010\u001a\u0004\b\u0013\u0010\u0014¨\u0006A"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "cameraFab", "Lcom/google/android/material/floatingactionbutton/FloatingActionButton;", "emptyNotice", "Landroid/view/View;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "tabsViewModel", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsViewModel;", "getTabsViewModel", "()Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsViewModel;", "tabsViewModel$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingViewModel;", "viewModel$delegate", "bindAdapter", "", "createStoryLandingItem", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItem$Model;", "data", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItemData;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingState;", "handleDeleteStory", "model", "handleHideStory", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onRequestPermissionsResult", "requestCode", "", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "openStoryViewer", "preview", "isFromInfoContextMenuAction", "startActivityIfAble", "intent", "Landroid/content/Intent;", "options", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoriesLandingFragment extends DSLSettingsFragment {
    public static final Companion Companion = new Companion(null);
    private static final int LIST_SMOOTH_SCROLL_TO_TOP_THRESHOLD;
    private DSLSettingsAdapter adapter;
    private FloatingActionButton cameraFab;
    private View emptyNotice;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy tabsViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(ConversationListTabsViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$tabsViewModel$2
        final /* synthetic */ StoriesLandingFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$special$$inlined$viewModels$default$3
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoriesLandingViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$viewModel$2
        final /* synthetic */ StoriesLandingFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            return new StoriesLandingViewModel.Factory(new StoriesLandingRepository(requireContext));
        }
    });

    /* renamed from: handleHideStory$lambda-7 */
    public static final void m2803handleHideStory$lambda7(DialogInterface dialogInterface, int i) {
    }

    public StoriesLandingFragment() {
        super(0, 0, R.layout.stories_landing_fragment, null, 11, null);
    }

    /* compiled from: StoriesLandingFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingFragment$Companion;", "", "()V", "LIST_SMOOTH_SCROLL_TO_TOP_THRESHOLD", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final StoriesLandingViewModel getViewModel() {
        return (StoriesLandingViewModel) this.viewModel$delegate.getValue();
    }

    public final ConversationListTabsViewModel getTabsViewModel() {
        return (ConversationListTabsViewModel) this.tabsViewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        Intrinsics.checkNotNullParameter(menu, "menu");
        Intrinsics.checkNotNullParameter(menuInflater, "inflater");
        menu.clear();
        menuInflater.inflate(R.menu.story_landing_menu, menu);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().setTransitioningToAnotherScreen(false);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Material3OnScrollHelperBinder material3OnScrollHelperBinder;
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        this.adapter = dSLSettingsAdapter;
        StoriesLandingItem.INSTANCE.register(dSLSettingsAdapter);
        MyStoriesItem.INSTANCE.register(dSLSettingsAdapter);
        ExpandHeader.INSTANCE.register(dSLSettingsAdapter);
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        material3OnScrollHelperBinder = (Material3OnScrollHelperBinder) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.main.Material3OnScrollHelperBinder");
                    }
                } else if (fragment instanceof Material3OnScrollHelperBinder) {
                    material3OnScrollHelperBinder = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            RecyclerView recyclerView = getRecyclerView();
            Intrinsics.checkNotNull(recyclerView);
            material3OnScrollHelperBinder.bindScrollHelper(recyclerView);
            LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
            Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
            lifecycleDisposable.bindTo(viewLifecycleOwner);
            View findViewById = requireView().findViewById(R.id.empty_notice);
            Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewById(R.id.empty_notice)");
            this.emptyNotice = findViewById;
            View findViewById2 = requireView().findViewById(R.id.camera_fab);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "requireView().findViewById(R.id.camera_fab)");
            this.cameraFab = (FloatingActionButton) findViewById2;
            setSharedElementEnterTransition(TransitionInflater.from(requireContext()).inflateTransition(R.transition.change_transform_fabs));
            setEnterSharedElementCallback(new SharedElementCallback(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1
                final /* synthetic */ StoriesLandingFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x004a: INVOKE  
                      (r4v5 'access$getLifecycleDisposable$p' org.thoughtcrime.securesms.util.LifecycleDisposable)
                      (wrap: io.reactivex.rxjava3.disposables.Disposable : 0x0046: INVOKE  (r5v4 io.reactivex.rxjava3.disposables.Disposable A[REMOVE]) = 
                      (r0v3 'observeOn' io.reactivex.rxjava3.core.Single<java.lang.Long>)
                      (wrap: kotlin.jvm.functions.Function1 : ?: CAST (kotlin.jvm.functions.Function1) (null kotlin.jvm.functions.Function1))
                      (wrap: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1 : 0x0043: CONSTRUCTOR  (r1v2 org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1 A[REMOVE]) = 
                      (wrap: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment : 0x0041: IGET  (r2v1 org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment A[REMOVE]) = (r3v0 'this' org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1.this$0 org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment)
                     call: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1.<init>(org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment):void type: CONSTRUCTOR)
                      (1 int)
                      (wrap: java.lang.Object : ?: CAST (java.lang.Object) (null java.lang.Object))
                     type: STATIC call: io.reactivex.rxjava3.kotlin.SubscribersKt.subscribeBy$default(io.reactivex.rxjava3.core.Single, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, int, java.lang.Object):io.reactivex.rxjava3.disposables.Disposable)
                     type: VIRTUAL call: org.thoughtcrime.securesms.util.LifecycleDisposable.plusAssign(io.reactivex.rxjava3.disposables.Disposable):void in method: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1.onSharedElementStart(java.util.List<java.lang.String>, java.util.List<android.view.View>, java.util.List<android.view.View>):void, file: classes3.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                    	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                    	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0046: INVOKE  (r5v4 io.reactivex.rxjava3.disposables.Disposable A[REMOVE]) = 
                      (r0v3 'observeOn' io.reactivex.rxjava3.core.Single<java.lang.Long>)
                      (wrap: kotlin.jvm.functions.Function1 : ?: CAST (kotlin.jvm.functions.Function1) (null kotlin.jvm.functions.Function1))
                      (wrap: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1 : 0x0043: CONSTRUCTOR  (r1v2 org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1 A[REMOVE]) = 
                      (wrap: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment : 0x0041: IGET  (r2v1 org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment A[REMOVE]) = (r3v0 'this' org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1.this$0 org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment)
                     call: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1.<init>(org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment):void type: CONSTRUCTOR)
                      (1 int)
                      (wrap: java.lang.Object : ?: CAST (java.lang.Object) (null java.lang.Object))
                     type: STATIC call: io.reactivex.rxjava3.kotlin.SubscribersKt.subscribeBy$default(io.reactivex.rxjava3.core.Single, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, int, java.lang.Object):io.reactivex.rxjava3.disposables.Disposable in method: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1.onSharedElementStart(java.util.List<java.lang.String>, java.util.List<android.view.View>, java.util.List<android.view.View>):void, file: classes3.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 18 more
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0043: CONSTRUCTOR  (r1v2 org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1 A[REMOVE]) = 
                      (wrap: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment : 0x0041: IGET  (r2v1 org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment A[REMOVE]) = (r3v0 'this' org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1.this$0 org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment)
                     call: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1.<init>(org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1.onSharedElementStart(java.util.List<java.lang.String>, java.util.List<android.view.View>, java.util.List<android.view.View>):void, file: classes3.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 24 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 30 more
                    */
                @Override // androidx.core.app.SharedElementCallback
                public void onSharedElementStart(java.util.List<java.lang.String> r4, java.util.List<android.view.View> r5, java.util.List<android.view.View> r6) {
                    /*
                        r3 = this;
                        r5 = 0
                        r6 = 1
                        if (r4 == 0) goto L_0x000d
                        java.lang.String r0 = "camera_fab"
                        boolean r4 = r4.contains(r0)
                        if (r4 != r6) goto L_0x000d
                        r5 = 1
                    L_0x000d:
                        if (r5 == 0) goto L_0x004d
                        org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment r4 = r3.this$0
                        com.google.android.material.floatingactionbutton.FloatingActionButton r4 = org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment.access$getCameraFab$p(r4)
                        r5 = 0
                        if (r4 != 0) goto L_0x001e
                        java.lang.String r4 = "cameraFab"
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r4)
                        r4 = r5
                    L_0x001e:
                        r0 = 2131231185(0x7f0801d1, float:1.8078444E38)
                        r4.setImageResource(r0)
                        org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment r4 = r3.this$0
                        org.thoughtcrime.securesms.util.LifecycleDisposable r4 = org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment.access$getLifecycleDisposable$p(r4)
                        r0 = 200(0xc8, double:9.9E-322)
                        java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS
                        io.reactivex.rxjava3.core.Single r0 = io.reactivex.rxjava3.core.Single.timer(r0, r2)
                        io.reactivex.rxjava3.core.Scheduler r1 = io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread()
                        io.reactivex.rxjava3.core.Single r0 = r0.observeOn(r1)
                        java.lang.String r1 = "timer(200, TimeUnit.MILL…dSchedulers.mainThread())"
                        kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
                        org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1 r1 = new org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1$onSharedElementStart$1
                        org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment r2 = r3.this$0
                        r1.<init>(r2)
                        io.reactivex.rxjava3.disposables.Disposable r5 = io.reactivex.rxjava3.kotlin.SubscribersKt.subscribeBy$default(r0, r5, r1, r6, r5)
                        r4.plusAssign(r5)
                    L_0x004d:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$1.onSharedElementStart(java.util.List, java.util.List, java.util.List):void");
                }
            });
            FloatingActionButton floatingActionButton = this.cameraFab;
            if (floatingActionButton == null) {
                Intrinsics.throwUninitializedPropertyAccessException("cameraFab");
                floatingActionButton = null;
            }
            floatingActionButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$$ExternalSyntheticLambda0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    StoriesLandingFragment.m2793$r8$lambda$4M73Sk6l4X42o5AYjXNyXb6pyQ(StoriesLandingFragment.this, view);
                }
            });
            getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$$ExternalSyntheticLambda1
                public final /* synthetic */ StoriesLandingFragment f$1;

                {
                    this.f$1 = r2;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    StoriesLandingFragment.$r8$lambda$kaoWv1KbwapEYu9oT_MR8mYgdwI(DSLSettingsAdapter.this, this.f$1, (StoriesLandingState) obj);
                }
            });
            requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$4
                final /* synthetic */ StoriesLandingFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // androidx.activity.OnBackPressedCallback
                public void handleOnBackPressed() {
                    StoriesLandingFragment.access$getTabsViewModel(this.this$0).onChatsSelected();
                }
            });
            LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
            Observable<ConversationListTab> filter = getTabsViewModel().getTabClickEvents().filter(new Predicate() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$$ExternalSyntheticLambda2
                @Override // io.reactivex.rxjava3.functions.Predicate
                public final boolean test(Object obj) {
                    return StoriesLandingFragment.$r8$lambda$ffrtuh91ra8YZ9pbnuWgRzvwYoU((ConversationListTab) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(filter, "tabsViewModel.tabClickEv…ersationListTab.STORIES }");
            lifecycleDisposable2.plusAssign(SubscribersKt.subscribeBy$default(filter, (Function1) null, (Function0) null, new Function1<ConversationListTab, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$bindAdapter$6
                final /* synthetic */ StoriesLandingFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(ConversationListTab conversationListTab) {
                    invoke(conversationListTab);
                    return Unit.INSTANCE;
                }

                public final void invoke(ConversationListTab conversationListTab) {
                    RecyclerView access$getRecyclerView = StoriesLandingFragment.access$getRecyclerView(this.this$0);
                    LinearLayoutManager linearLayoutManager = null;
                    RecyclerView.LayoutManager layoutManager = access$getRecyclerView != null ? access$getRecyclerView.getLayoutManager() : null;
                    if (layoutManager instanceof LinearLayoutManager) {
                        linearLayoutManager = (LinearLayoutManager) layoutManager;
                    }
                    if (linearLayoutManager != null) {
                        if (linearLayoutManager.findFirstVisibleItemPosition() <= 25) {
                            RecyclerView access$getRecyclerView2 = StoriesLandingFragment.access$getRecyclerView(this.this$0);
                            if (access$getRecyclerView2 != null) {
                                access$getRecyclerView2.smoothScrollToPosition(0);
                                return;
                            }
                            return;
                        }
                        RecyclerView access$getRecyclerView3 = StoriesLandingFragment.access$getRecyclerView(this.this$0);
                        if (access$getRecyclerView3 != null) {
                            access$getRecyclerView3.scrollToPosition(0);
                        }
                    }
                }
            }, 3, (Object) null));
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    /* renamed from: bindAdapter$lambda-2 */
    public static final void m2796bindAdapter$lambda2(StoriesLandingFragment storiesLandingFragment, View view) {
        Intrinsics.checkNotNullParameter(storiesLandingFragment, "this$0");
        Permissions.with(storiesLandingFragment).request("android.permission.CAMERA").ifNecessary().withRationaleDialog(storiesLandingFragment.getString(R.string.ConversationActivity_to_capture_photos_and_video_allow_signal_access_to_the_camera), R.drawable.ic_camera_24).withPermanentDenialDialog(storiesLandingFragment.getString(R.string.ConversationActivity_signal_needs_the_camera_permission_to_take_photos_or_video)).onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                StoriesLandingFragment.$r8$lambda$4egArM5SuRt6GW71BGlKu2MUZxY(StoriesLandingFragment.this);
            }
        }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                StoriesLandingFragment.$r8$lambda$ekIEXjoaaSF6p_B12w4nR1umq7M(StoriesLandingFragment.this);
            }
        }).execute();
    }

    /* renamed from: bindAdapter$lambda-2$lambda-0 */
    public static final void m2797bindAdapter$lambda2$lambda0(StoriesLandingFragment storiesLandingFragment) {
        Intrinsics.checkNotNullParameter(storiesLandingFragment, "this$0");
        MediaSelectionActivity.Companion companion = MediaSelectionActivity.Companion;
        Context requireContext = storiesLandingFragment.requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        startActivityIfAble$default(storiesLandingFragment, companion.camera(requireContext, true), null, 2, null);
    }

    /* renamed from: bindAdapter$lambda-2$lambda-1 */
    public static final void m2798bindAdapter$lambda2$lambda1(StoriesLandingFragment storiesLandingFragment) {
        Intrinsics.checkNotNullParameter(storiesLandingFragment, "this$0");
        Toast.makeText(storiesLandingFragment.requireContext(), (int) R.string.ConversationActivity_signal_needs_camera_permissions_to_take_photos_or_video, 1).show();
    }

    /* renamed from: bindAdapter$lambda-3 */
    public static final void m2799bindAdapter$lambda3(DSLSettingsAdapter dSLSettingsAdapter, StoriesLandingFragment storiesLandingFragment, StoriesLandingState storiesLandingState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(storiesLandingFragment, "this$0");
        if (storiesLandingState.getLoadingState() == StoriesLandingState.LoadingState.LOADED) {
            Intrinsics.checkNotNullExpressionValue(storiesLandingState, "it");
            dSLSettingsAdapter.submitList(storiesLandingFragment.getConfiguration(storiesLandingState).toMappingModelList());
            View view = storiesLandingFragment.emptyNotice;
            if (view == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emptyNotice");
                view = null;
            }
            ViewExtensionsKt.setVisible(view, storiesLandingState.getHasNoStories());
        }
    }

    /* renamed from: bindAdapter$lambda-4 */
    public static final boolean m2800bindAdapter$lambda4(ConversationListTab conversationListTab) {
        return conversationListTab == ConversationListTab.STORIES;
    }

    private final DSLConfiguration getConfiguration(StoriesLandingState storiesLandingState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(storiesLandingState, this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$getConfiguration$1
            final /* synthetic */ StoriesLandingState $state;
            final /* synthetic */ StoriesLandingFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                List<StoriesLandingItemData> storiesLandingItems = this.$state.getStoriesLandingItems();
                StoriesLandingFragment storiesLandingFragment = this.this$0;
                ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(storiesLandingItems, 10));
                for (StoriesLandingItemData storiesLandingItemData : storiesLandingItems) {
                    arrayList.add(StoriesLandingFragment.access$createStoryLandingItem(storiesLandingFragment, storiesLandingItemData));
                }
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                for (Object obj : arrayList) {
                    if (!((StoriesLandingItem.Model) obj).getData().isHidden()) {
                        arrayList2.add(obj);
                    } else {
                        arrayList3.add(obj);
                    }
                }
                Pair pair = new Pair(arrayList2, arrayList3);
                List<StoriesLandingItem.Model> list = (List) pair.component1();
                List<StoriesLandingItem.Model> list2 = (List) pair.component2();
                if (this.$state.getDisplayMyStoryItem()) {
                    final StoriesLandingFragment storiesLandingFragment2 = this.this$0;
                    AnonymousClass3 r3 = new Function0<Unit>() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$getConfiguration$1.3
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            StoriesLandingFragment.startActivityIfAble$default(storiesLandingFragment2, new Intent(storiesLandingFragment2.requireContext(), MyStoriesActivity.class), null, 2, null);
                        }
                    };
                    final StoriesLandingFragment storiesLandingFragment3 = this.this$0;
                    dSLConfiguration.customPref(new MyStoriesItem.Model(r3, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$getConfiguration$1.4
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            FloatingActionButton access$getCameraFab$p = StoriesLandingFragment.access$getCameraFab$p(storiesLandingFragment3);
                            if (access$getCameraFab$p == null) {
                                Intrinsics.throwUninitializedPropertyAccessException("cameraFab");
                                access$getCameraFab$p = null;
                            }
                            access$getCameraFab$p.performClick();
                        }
                    }));
                }
                for (StoriesLandingItem.Model model : list) {
                    dSLConfiguration.customPref(model);
                }
                if (!list2.isEmpty()) {
                    DSLSettingsText from = DSLSettingsText.Companion.from(R.string.StoriesLandingFragment__hidden_stories, new DSLSettingsText.Modifier[0]);
                    boolean isHiddenContentVisible = this.$state.isHiddenContentVisible();
                    final StoriesLandingFragment storiesLandingFragment4 = this.this$0;
                    dSLConfiguration.customPref(new ExpandHeader.Model(from, isHiddenContentVisible, new Function1<Boolean, Unit>() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$getConfiguration$1.6
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
                            invoke(bool.booleanValue());
                            return Unit.INSTANCE;
                        }

                        public final void invoke(boolean z) {
                            StoriesLandingFragment.access$getViewModel(storiesLandingFragment4).setHiddenContentVisible(z);
                        }
                    }));
                }
                if (this.$state.isHiddenContentVisible()) {
                    for (StoriesLandingItem.Model model2 : list2) {
                        dSLConfiguration.customPref(model2);
                    }
                }
            }
        });
    }

    public final StoriesLandingItem.Model createStoryLandingItem(StoriesLandingItemData storiesLandingItemData) {
        return new StoriesLandingItem.Model(storiesLandingItemData, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$createStoryLandingItem$1
            final /* synthetic */ StoriesLandingFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                FloatingActionButton access$getCameraFab$p = StoriesLandingFragment.access$getCameraFab$p(this.this$0);
                if (access$getCameraFab$p == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("cameraFab");
                    access$getCameraFab$p = null;
                }
                access$getCameraFab$p.performClick();
            }
        }, new Function2<StoriesLandingItem.Model, View, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$createStoryLandingItem$2
            final /* synthetic */ StoriesLandingFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((StoriesLandingItem.Model) obj, (View) obj2);
                return Unit.INSTANCE;
            }

            public final void invoke(StoriesLandingItem.Model model, View view) {
                Intrinsics.checkNotNullParameter(model, "model");
                Intrinsics.checkNotNullParameter(view, "preview");
                StoriesLandingFragment.access$openStoryViewer(this.this$0, model, view, false);
            }
        }, new Function1<StoriesLandingItem.Model, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$createStoryLandingItem$3
            final /* synthetic */ StoriesLandingFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(StoriesLandingItem.Model model) {
                invoke(model);
                return Unit.INSTANCE;
            }

            public final void invoke(StoriesLandingItem.Model model) {
                Intrinsics.checkNotNullParameter(model, "it");
                if (!model.getData().isHidden()) {
                    StoriesLandingFragment.access$handleHideStory(this.this$0, model);
                    return;
                }
                LifecycleDisposable access$getLifecycleDisposable$p = StoriesLandingFragment.access$getLifecycleDisposable$p(this.this$0);
                Disposable subscribe = StoriesLandingFragment.access$getViewModel(this.this$0).setHideStory(model.getData().getStoryRecipient(), !model.getData().isHidden()).subscribe();
                Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.setHideStory(i…ata.isHidden).subscribe()");
                access$getLifecycleDisposable$p.plusAssign(subscribe);
            }
        }, new StoriesLandingFragment$createStoryLandingItem$4(this), new Function1<StoriesLandingItem.Model, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$createStoryLandingItem$5
            final /* synthetic */ StoriesLandingFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(StoriesLandingItem.Model model) {
                invoke(model);
                return Unit.INSTANCE;
            }

            public final void invoke(StoriesLandingItem.Model model) {
                Intrinsics.checkNotNullParameter(model, "it");
                StoryContextMenu.INSTANCE.share(this.this$0, (MediaMmsMessageRecord) model.getData().getPrimaryStory().getMessageRecord());
            }
        }, new Function1<StoriesLandingItem.Model, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$createStoryLandingItem$6
            final /* synthetic */ StoriesLandingFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(StoriesLandingItem.Model model) {
                invoke(model);
                return Unit.INSTANCE;
            }

            public final void invoke(StoriesLandingItem.Model model) {
                Intrinsics.checkNotNullParameter(model, "it");
                StoriesLandingFragment storiesLandingFragment = this.this$0;
                Intent build = ConversationIntents.createBuilder(storiesLandingFragment.requireContext(), model.getData().getStoryRecipient().getId(), -1).build();
                Intrinsics.checkNotNullExpressionValue(build, "createBuilder(requireCon…ecipient.id, -1L).build()");
                StoriesLandingFragment.startActivityIfAble$default(storiesLandingFragment, build, null, 2, null);
            }
        }, new Function1<StoriesLandingItem.Model, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$createStoryLandingItem$7
            final /* synthetic */ StoriesLandingFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(StoriesLandingItem.Model model) {
                invoke(model);
                return Unit.INSTANCE;
            }

            public final void invoke(StoriesLandingItem.Model model) {
                Intrinsics.checkNotNullParameter(model, "it");
                StoryContextMenu storyContextMenu = StoryContextMenu.INSTANCE;
                Context requireContext = this.this$0.requireContext();
                Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                MessageRecord messageRecord = model.getData().getPrimaryStory().getMessageRecord();
                Intrinsics.checkNotNullExpressionValue(messageRecord, "it.data.primaryStory.messageRecord");
                storyContextMenu.save(requireContext, messageRecord);
            }
        }, new Function1<StoriesLandingItem.Model, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$createStoryLandingItem$8
            final /* synthetic */ StoriesLandingFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(StoriesLandingItem.Model model) {
                invoke(model);
                return Unit.INSTANCE;
            }

            public final void invoke(StoriesLandingItem.Model model) {
                Intrinsics.checkNotNullParameter(model, "it");
                StoriesLandingFragment.access$handleDeleteStory(this.this$0, model);
            }
        }, new Function2<StoriesLandingItem.Model, View, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$createStoryLandingItem$9
            final /* synthetic */ StoriesLandingFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((StoriesLandingItem.Model) obj, (View) obj2);
                return Unit.INSTANCE;
            }

            public final void invoke(StoriesLandingItem.Model model, View view) {
                Intrinsics.checkNotNullParameter(model, "model");
                Intrinsics.checkNotNullParameter(view, "preview");
                StoriesLandingFragment.access$openStoryViewer(this.this$0, model, view, true);
            }
        });
    }

    public final void openStoryViewer(StoriesLandingItem.Model model, View view, boolean z) {
        Pair pair;
        if (model.getData().getStoryRecipient().isMyStory()) {
            startActivityIfAble$default(this, new Intent(requireContext(), MyStoriesActivity.class), null, 2, null);
        } else if (!model.getData().getPrimaryStory().getMessageRecord().isOutgoing() || !model.getData().getPrimaryStory().getMessageRecord().isFailed()) {
            FragmentActivity requireActivity = requireActivity();
            String transitionName = ViewCompat.getTransitionName(view);
            if (transitionName == null) {
                transitionName = "";
            }
            ActivityOptionsCompat makeSceneTransitionAnimation = ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity, view, transitionName);
            Intrinsics.checkNotNullExpressionValue(makeSceneTransitionAnimation, "makeSceneTransitionAnima…itionName(preview) ?: \"\")");
            MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) model.getData().getPrimaryStory().getMessageRecord();
            Slide thumbnailSlide = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
            BlurHash placeholderBlur = thumbnailSlide != null ? thumbnailSlide.getPlaceholderBlur() : null;
            if (mmsMessageRecord.getStoryType().isTextStory()) {
                pair = TuplesKt.to(StoryTextPostModel.CREATOR.parseFrom(mmsMessageRecord), null);
            } else {
                Slide thumbnailSlide2 = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
                pair = TuplesKt.to(null, thumbnailSlide2 != null ? thumbnailSlide2.getUri() : null);
            }
            StoryTextPostModel storyTextPostModel = (StoryTextPostModel) pair.component1();
            Uri uri = (Uri) pair.component2();
            StoryViewerActivity.Companion companion = StoryViewerActivity.Companion;
            Context requireContext = requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            RecipientId id = model.getData().getStoryRecipient().getId();
            boolean isHidden = model.getData().isHidden();
            StoriesLandingViewModel viewModel = getViewModel();
            boolean isHidden2 = model.getData().isHidden();
            StoryViewState storyViewState = model.getData().getStoryViewState();
            StoryViewState storyViewState2 = StoryViewState.UNVIEWED;
            List<RecipientId> recipientIds = viewModel.getRecipientIds(isHidden2, storyViewState == storyViewState2);
            boolean z2 = model.getData().getStoryViewState() == storyViewState2;
            Intrinsics.checkNotNullExpressionValue(id, ContactRepository.ID_COLUMN);
            startActivityIfAble(companion.createIntent(requireContext, new StoryViewerArgs(id, isHidden, -1, storyTextPostModel, uri, placeholderBlur, recipientIds, false, 0, z2, z, false, false, 6528, null)), makeSceneTransitionAnimation.toBundle());
        } else if (model.getData().getPrimaryStory().getMessageRecord().isIdentityMismatchFailure()) {
            Context requireContext2 = requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext2, "requireContext()");
            MessageRecord messageRecord = model.getData().getPrimaryStory().getMessageRecord();
            Intrinsics.checkNotNullExpressionValue(messageRecord, "model.data.primaryStory.messageRecord");
            SafetyNumberBottomSheet.Factory forMessageRecord = SafetyNumberBottomSheet.forMessageRecord(requireContext2, messageRecord);
            FragmentManager childFragmentManager = getChildFragmentManager();
            Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
            forMessageRecord.show(childFragmentManager);
        } else {
            StoryDialogs storyDialogs = StoryDialogs.INSTANCE;
            Context requireContext3 = requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext3, "requireContext()");
            storyDialogs.resendStory(requireContext3, new Function0<Unit>(this, model) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$openStoryViewer$1
                final /* synthetic */ StoriesLandingItem.Model $model;
                final /* synthetic */ StoriesLandingFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                    this.$model = r2;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    LifecycleDisposable access$getLifecycleDisposable$p = StoriesLandingFragment.access$getLifecycleDisposable$p(this.this$0);
                    StoriesLandingViewModel access$getViewModel = StoriesLandingFragment.access$getViewModel(this.this$0);
                    MessageRecord messageRecord2 = this.$model.getData().getPrimaryStory().getMessageRecord();
                    Intrinsics.checkNotNullExpressionValue(messageRecord2, "model.data.primaryStory.messageRecord");
                    Disposable subscribe = access$getViewModel.resend(messageRecord2).subscribe();
                    Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.resend(model.d…essageRecord).subscribe()");
                    access$getLifecycleDisposable$p.plusAssign(subscribe);
                }
            });
        }
    }

    public final void handleDeleteStory(StoriesLandingItem.Model model) {
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        StoryContextMenu storyContextMenu = StoryContextMenu.INSTANCE;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        Disposable subscribe = storyContextMenu.delete(requireContext, SetsKt__SetsJVMKt.setOf(model.getData().getPrimaryStory().getMessageRecord())).subscribe();
        Intrinsics.checkNotNullExpressionValue(subscribe, "StoryContextMenu.delete(…ssageRecord)).subscribe()");
        lifecycleDisposable.plusAssign(subscribe);
    }

    public final void handleHideStory(StoriesLandingItem.Model model) {
        new MaterialAlertDialogBuilder(requireContext(), R.style.ThemeOverlay_Signal_MaterialAlertDialog).setTitle(R.string.StoriesLandingFragment__hide_story).setMessage((CharSequence) getString(R.string.StoriesLandingFragment__new_story_updates, model.getData().getStoryRecipient().getShortDisplayName(requireContext()))).setPositiveButton(R.string.StoriesLandingFragment__hide, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(model) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$$ExternalSyntheticLambda6
            public final /* synthetic */ StoriesLandingItem.Model f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                StoriesLandingFragment.m2795$r8$lambda$rv3virjtIYbhUiDnmOvzXuwDg(StoriesLandingFragment.this, this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$$ExternalSyntheticLambda7
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                StoriesLandingFragment.$r8$lambda$cIheTBbmWU6Wi1JRMSw0q9JriV4(dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: handleHideStory$lambda-6 */
    public static final void m2801handleHideStory$lambda6(StoriesLandingFragment storiesLandingFragment, StoriesLandingItem.Model model, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(storiesLandingFragment, "this$0");
        Intrinsics.checkNotNullParameter(model, "$model");
        storiesLandingFragment.getViewModel().setHideStory(model.getData().getStoryRecipient(), true).subscribe(new Action() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingFragment$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                StoriesLandingFragment.m2794$r8$lambda$_KOL_nn53nymqMazMTNJbUqRE(StoriesLandingFragment.this);
            }
        });
    }

    /* renamed from: handleHideStory$lambda-6$lambda-5 */
    public static final void m2802handleHideStory$lambda6$lambda5(StoriesLandingFragment storiesLandingFragment) {
        Intrinsics.checkNotNullParameter(storiesLandingFragment, "this$0");
        FloatingActionButton floatingActionButton = storiesLandingFragment.cameraFab;
        FloatingActionButton floatingActionButton2 = null;
        if (floatingActionButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraFab");
            floatingActionButton = null;
        }
        Snackbar make = Snackbar.make(floatingActionButton, (int) R.string.StoriesLandingFragment__story_hidden, -1);
        FloatingActionButton floatingActionButton3 = storiesLandingFragment.cameraFab;
        if (floatingActionButton3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraFab");
        } else {
            floatingActionButton2 = floatingActionButton3;
        }
        make.setAnchorView(floatingActionButton2).setAnimationMode(1).show();
    }

    @Override // androidx.fragment.app.Fragment
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intrinsics.checkNotNullParameter(menuItem, "item");
        if (menuItem.getItemId() != R.id.action_settings) {
            return false;
        }
        StorySettingsActivity.Companion companion = StorySettingsActivity.Companion;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        startActivityIfAble$default(this, companion.getIntent(requireContext), null, 2, null);
        return true;
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Intrinsics.checkNotNullParameter(strArr, "permissions");
        Intrinsics.checkNotNullParameter(iArr, "grantResults");
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    public static /* synthetic */ void startActivityIfAble$default(StoriesLandingFragment storiesLandingFragment, Intent intent, Bundle bundle, int i, Object obj) {
        if ((i & 2) != 0) {
            bundle = null;
        }
        storiesLandingFragment.startActivityIfAble(intent, bundle);
    }

    private final void startActivityIfAble(Intent intent, Bundle bundle) {
        if (!getViewModel().isTransitioningToAnotherScreen()) {
            getViewModel().setTransitioningToAnotherScreen(true);
            startActivity(intent, bundle);
        }
    }
}
