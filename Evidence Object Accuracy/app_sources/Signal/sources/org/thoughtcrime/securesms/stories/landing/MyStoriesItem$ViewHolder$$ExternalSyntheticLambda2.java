package org.thoughtcrime.securesms.stories.landing;

import android.view.View;
import org.thoughtcrime.securesms.stories.landing.MyStoriesItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class MyStoriesItem$ViewHolder$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ MyStoriesItem.Model f$0;

    public /* synthetic */ MyStoriesItem$ViewHolder$$ExternalSyntheticLambda2(MyStoriesItem.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        MyStoriesItem.ViewHolder.m2792bind$lambda2(this.f$0, view);
    }
}
