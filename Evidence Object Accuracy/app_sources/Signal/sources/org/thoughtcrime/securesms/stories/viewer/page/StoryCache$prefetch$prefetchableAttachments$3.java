package org.thoughtcrime.securesms.stories.viewer.page;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.attachments.Attachment;

/* compiled from: StoryCache.kt */
/* access modifiers changed from: package-private */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "invoke", "(Lorg/thoughtcrime/securesms/attachments/Attachment;)Ljava/lang/Boolean;"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryCache$prefetch$prefetchableAttachments$3 extends Lambda implements Function1<Attachment, Boolean> {
    public static final StoryCache$prefetch$prefetchableAttachments$3 INSTANCE = new StoryCache$prefetch$prefetchableAttachments$3();

    StoryCache$prefetch$prefetchableAttachments$3() {
        super(1);
    }

    public final Boolean invoke(Attachment attachment) {
        Intrinsics.checkNotNullParameter(attachment, "it");
        return Boolean.valueOf(attachment.getTransferState() == 0);
    }
}
