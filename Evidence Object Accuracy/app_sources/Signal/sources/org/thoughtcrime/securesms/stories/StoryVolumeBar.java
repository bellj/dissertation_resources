package org.thoughtcrime.securesms.stories;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.AudioManager;
import android.util.AttributeSet;
import android.view.View;
import androidx.core.content.ContextCompat;
import androidx.media.AudioManagerCompat;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* compiled from: StoryVolumeBar.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0014J0\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00072\u0006\u0010\u001f\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u00072\u0006\u0010!\u001a\u00020\u0007H\u0014J\u000e\u0010\"\u001a\u00020\u00182\u0006\u0010\u0014\u001a\u00020\u0007R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryVolumeBar;", "Landroid/view/View;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "backgroundPaint", "Landroid/graphics/Paint;", "bounds", "Landroid/graphics/Rect;", "clipBoundsF", "Landroid/graphics/RectF;", "clipPath", "Landroid/graphics/Path;", "clipRadius", "", "foregroundPaint", "level", "maximum", "minimum", "onDraw", "", "canvas", "Landroid/graphics/Canvas;", "onLayout", "changed", "", "left", "top", "right", "bottom", "setLevel", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryVolumeBar extends View {
    private final Paint backgroundPaint;
    private final Rect bounds;
    private final RectF clipBoundsF;
    private final Path clipPath;
    private final float clipRadius;
    private final Paint foregroundPaint;
    private int level;
    private final int maximum;
    private final int minimum;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryVolumeBar(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryVolumeBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ StoryVolumeBar(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryVolumeBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        this.bounds = new Rect();
        this.clipBoundsF = new RectF();
        this.clipPath = new Path();
        this.clipRadius = DimensionUnit.DP.toPixels(4.0f);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(ContextCompat.getColor(context, R.color.transparent_black_40));
        paint.setStyle(Paint.Style.STROKE);
        this.backgroundPaint = paint;
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setColor(ContextCompat.getColor(context, R.color.core_white));
        paint2.setStyle(Paint.Style.STROKE);
        this.foregroundPaint = paint2;
        AudioManager audioManager = ServiceUtil.getAudioManager(context);
        if (isInEditMode()) {
            this.minimum = 0;
            this.maximum = 100;
            this.level = 50;
            return;
        }
        this.minimum = AudioManagerCompat.getStreamMinVolume(audioManager, 3);
        this.maximum = AudioManagerCompat.getStreamMaxVolume(audioManager, 3);
        this.level = audioManager.getStreamVolume(3);
    }

    public final void setLevel(int i) {
        this.level = i;
        invalidate();
    }

    @Override // android.view.View
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.backgroundPaint.setStrokeWidth((float) ((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight()));
        this.foregroundPaint.setStrokeWidth(this.backgroundPaint.getStrokeWidth());
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        canvas.getClipBounds(this.bounds);
        this.clipBoundsF.set(this.bounds);
        this.clipPath.reset();
        Path path = this.clipPath;
        RectF rectF = this.clipBoundsF;
        float f = this.clipRadius;
        path.addRoundRect(rectF, f, f, Path.Direction.CW);
        Path path2 = this.clipPath;
        int save = canvas.save();
        canvas.clipPath(path2);
        try {
            float exactCenterX = this.bounds.exactCenterX();
            Rect rect = this.bounds;
            canvas.drawLine(exactCenterX, (float) rect.top, rect.exactCenterX(), (float) this.bounds.bottom, this.backgroundPaint);
            int i = this.level;
            int i2 = this.minimum;
            float height = ((float) this.bounds.height()) * (((float) (i - i2)) / (((float) this.maximum) - ((float) i2)));
            float exactCenterX2 = this.bounds.exactCenterX();
            Rect rect2 = this.bounds;
            canvas.drawLine(exactCenterX2, ((float) rect2.bottom) - height, rect2.exactCenterX(), (float) this.bounds.bottom, this.foregroundPaint);
        } finally {
            canvas.restoreToCount(save);
        }
    }
}
