package org.thoughtcrime.securesms.stories.settings.custom;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;

/* compiled from: PrivateStorySettingsFragment.kt */
@Metadata(d1 = {"\u0000\f\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class PrivateStorySettingsFragment$getConfiguration$1 extends Lambda implements Function1<DSLConfiguration, Unit> {
    public static final PrivateStorySettingsFragment$getConfiguration$1 INSTANCE = new PrivateStorySettingsFragment$getConfiguration$1();

    PrivateStorySettingsFragment$getConfiguration$1() {
        super(1);
    }

    public final void invoke(DSLConfiguration dSLConfiguration) {
        Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
        invoke(dSLConfiguration);
        return Unit.INSTANCE;
    }
}
