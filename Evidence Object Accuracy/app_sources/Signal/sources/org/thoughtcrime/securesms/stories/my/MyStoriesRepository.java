package org.thoughtcrime.securesms.stories.my;

import android.content.Context;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.stories.my.MyStoriesState;

/* compiled from: MyStoriesRepository.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH\u0002J\u0012\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u000b0\u000eJ\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\fR\u0016\u0010\u0002\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "kotlin.jvm.PlatformType", "createDistributionSet", "Lorg/thoughtcrime/securesms/stories/my/MyStoriesState$DistributionSet;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "messageRecords", "", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "getMyStories", "Lio/reactivex/rxjava3/core/Observable;", "resend", "Lio/reactivex/rxjava3/core/Completable;", "story", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStoriesRepository {
    private final Context context;

    public MyStoriesRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context.getApplicationContext();
    }

    public final Completable resend(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "story");
        Completable subscribeOn = Completable.fromAction(new Action(messageRecord) { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ MessageRecord f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                MyStoriesRepository.$r8$lambda$lWD5Clf1JVGIacPI2o54CbBBc_U(MyStoriesRepository.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Messa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: resend$lambda-0 */
    public static final void m2855resend$lambda0(MyStoriesRepository myStoriesRepository, MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(myStoriesRepository, "this$0");
        Intrinsics.checkNotNullParameter(messageRecord, "$story");
        MessageSender.resend(myStoriesRepository.context, messageRecord);
    }

    public final Observable<List<MyStoriesState.DistributionSet>> getMyStories() {
        Observable<List<MyStoriesState.DistributionSet>> create = Observable.create(new ObservableOnSubscribe() { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesRepository$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                MyStoriesRepository.$r8$lambda$oOZs63SPH0tJnpsGq5sdv9wga0I(MyStoriesRepository.this, observableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create { emitter ->\n    … }\n\n      refresh()\n    }");
        return create;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: getMyStories$lambda-5$refresh */
    private static final void m2854getMyStories$lambda5$refresh(ObservableEmitter<List<MyStoriesState.DistributionSet>> observableEmitter, MyStoriesRepository myStoriesRepository) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        MessageDatabase.Reader allOutgoingStories = SignalDatabase.Companion.mms().getAllOutgoingStories(true, -1);
        while (allOutgoingStories.getNext() != null) {
            try {
                MessageRecord current = allOutgoingStories.getCurrent();
                List list = (List) linkedHashMap.get(current.getRecipient());
                if (list == null) {
                    list = CollectionsKt__CollectionsKt.emptyList();
                }
                Recipient recipient = current.getRecipient();
                Intrinsics.checkNotNullExpressionValue(recipient, "messageRecord.recipient");
                linkedHashMap.put(recipient, CollectionsKt___CollectionsKt.plus((Collection<? extends MessageRecord>) ((Collection<? extends Object>) list), current));
            } finally {
                try {
                    throw th;
                } catch (Throwable th) {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        CloseableKt.closeFinally(allOutgoingStories, null);
        ArrayList arrayList = new ArrayList(linkedHashMap.size());
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            arrayList.add(myStoriesRepository.createDistributionSet((Recipient) entry.getKey(), (List) entry.getValue()));
        }
        observableEmitter.onNext(arrayList);
    }

    /* renamed from: getMyStories$lambda-5 */
    public static final void m2851getMyStories$lambda5(MyStoriesRepository myStoriesRepository, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(myStoriesRepository, "this$0");
        MyStoriesRepository$$ExternalSyntheticLambda1 myStoriesRepository$$ExternalSyntheticLambda1 = new DatabaseObserver.Observer(myStoriesRepository) { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ MyStoriesRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                MyStoriesRepository.m2850$r8$lambda$FE9GkydSJuivbpJPQslQDGbDM(ObservableEmitter.this, this.f$1);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerConversationListObserver(myStoriesRepository$$ExternalSyntheticLambda1);
        observableEmitter.setCancellable(new Cancellable() { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesRepository$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                MyStoriesRepository.m2849$r8$lambda$0R_8yzY4O8MKmw0GqUE6GsZpo(DatabaseObserver.Observer.this);
            }
        });
        m2854getMyStories$lambda5$refresh(observableEmitter, myStoriesRepository);
    }

    /* renamed from: getMyStories$lambda-5$lambda-3 */
    public static final void m2852getMyStories$lambda5$lambda3(ObservableEmitter observableEmitter, MyStoriesRepository myStoriesRepository) {
        Intrinsics.checkNotNullParameter(myStoriesRepository, "this$0");
        m2854getMyStories$lambda5$refresh(observableEmitter, myStoriesRepository);
    }

    /* renamed from: getMyStories$lambda-5$lambda-4 */
    public static final void m2853getMyStories$lambda5$lambda4(DatabaseObserver.Observer observer) {
        Intrinsics.checkNotNullParameter(observer, "$observer");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(observer);
    }

    private final MyStoriesState.DistributionSet createDistributionSet(Recipient recipient, List<? extends MessageRecord> list) {
        String displayName = recipient.getDisplayName(this.context);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (MessageRecord messageRecord : list) {
            ConversationMessage createWithUnresolvedData = ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(this.context, messageRecord);
            Intrinsics.checkNotNullExpressionValue(createWithUnresolvedData, "createWithUnresolvedData(context, it)");
            arrayList.add(createWithUnresolvedData);
        }
        return new MyStoriesState.DistributionSet(displayName, arrayList);
    }
}
