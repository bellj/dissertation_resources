package org.thoughtcrime.securesms.stories.viewer.text;

import android.app.Application;
import android.graphics.Typeface;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.fonts.SupportedScript;
import org.thoughtcrime.securesms.fonts.TextFont;
import org.thoughtcrime.securesms.fonts.TextToScript;
import org.thoughtcrime.securesms.fonts.TypefaceCache;
import org.thoughtcrime.securesms.util.Base64;

/* compiled from: StoryTextPostRepository.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostRepository;", "", "()V", "getRecord", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "recordId", "", "getTypeface", "Landroid/graphics/Typeface;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryTextPostRepository {
    public final Single<MmsMessageRecord> getRecord(long j) {
        Single<MmsMessageRecord> subscribeOn = Single.fromCallable(new Callable(j) { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return StoryTextPostRepository.$r8$lambda$JQw3P4JTMuzNDjLtVuAmpXO4cuQ(this.f$0);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      Sig…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getRecord$lambda-0 */
    public static final MmsMessageRecord m3194getRecord$lambda0(long j) {
        MessageRecord messageRecord = SignalDatabase.Companion.mms().getMessageRecord(j);
        if (messageRecord != null) {
            return (MmsMessageRecord) messageRecord;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.model.MmsMessageRecord");
    }

    /* JADX DEBUG: Type inference failed for r1v2. Raw type applied. Possible types: io.reactivex.rxjava3.core.Single<R>, java.lang.Object, io.reactivex.rxjava3.core.Single<android.graphics.Typeface> */
    public final Single<Typeface> getTypeface(long j) {
        Single flatMap = getRecord(j).flatMap(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryTextPostRepository.$r8$lambda$LiluqBiZ49Q7K_RHIc4cKUG8Tok((MmsMessageRecord) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMap, "getRecord(recordId).flat…, textFont, script)\n    }");
        return flatMap;
    }

    /* renamed from: getTypeface$lambda-1 */
    public static final SingleSource m3195getTypeface$lambda1(MmsMessageRecord mmsMessageRecord) {
        StoryTextPost parseFrom = StoryTextPost.parseFrom(Base64.decode(mmsMessageRecord.getBody()));
        TextFont.Companion companion = TextFont.Companion;
        StoryTextPost.Style style = parseFrom.getStyle();
        Intrinsics.checkNotNullExpressionValue(style, "model.style");
        TextFont fromStyle = companion.fromStyle(style);
        TextToScript textToScript = TextToScript.INSTANCE;
        String body = parseFrom.getBody();
        Intrinsics.checkNotNullExpressionValue(body, "model.body");
        SupportedScript guessScript = textToScript.guessScript(body);
        TypefaceCache typefaceCache = TypefaceCache.INSTANCE;
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        return typefaceCache.get(application, fromStyle, guessScript);
    }
}
