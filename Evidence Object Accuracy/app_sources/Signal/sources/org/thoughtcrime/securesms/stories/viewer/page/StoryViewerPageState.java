package org.thoughtcrime.securesms.stories.viewer.page;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: StoryViewerPageState.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0017\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001#BO\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n\u0012\b\b\u0002\u0010\f\u001a\u00020\n\u0012\u0006\u0010\r\u001a\u00020\n¢\u0006\u0002\u0010\u000eJ\u000f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0018\u001a\u00020\bHÆ\u0003J\t\u0010\u0019\u001a\u00020\nHÆ\u0003J\t\u0010\u001a\u001a\u00020\nHÆ\u0003J\t\u0010\u001b\u001a\u00020\nHÆ\u0003J\t\u0010\u001c\u001a\u00020\nHÆ\u0003JU\u0010\u001d\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\n2\b\b\u0002\u0010\f\u001a\u00020\n2\b\b\u0002\u0010\r\u001a\u00020\nHÆ\u0001J\u0013\u0010\u001e\u001a\u00020\n2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010 \u001a\u00020\u0006HÖ\u0001J\t\u0010!\u001a\u00020\"HÖ\u0001R\u0011\u0010\u000b\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\u000fR\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u000fR\u0011\u0010\f\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000fR\u0011\u0010\r\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000fR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState;", "", "posts", "", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost;", "selectedPostIndex", "", "replyState", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState$ReplyState;", "isFirstPage", "", "isDisplayingInitialState", "isReady", "isReceiptsEnabled", "(Ljava/util/List;ILorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState$ReplyState;ZZZZ)V", "()Z", "getPosts", "()Ljava/util/List;", "getReplyState", "()Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState$ReplyState;", "getSelectedPostIndex", "()I", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "hashCode", "toString", "", "ReplyState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerPageState {
    private final boolean isDisplayingInitialState;
    private final boolean isFirstPage;
    private final boolean isReady;
    private final boolean isReceiptsEnabled;
    private final List<StoryPost> posts;
    private final ReplyState replyState;
    private final int selectedPostIndex;

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ StoryViewerPageState copy$default(StoryViewerPageState storyViewerPageState, List list, int i, ReplyState replyState, boolean z, boolean z2, boolean z3, boolean z4, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            list = storyViewerPageState.posts;
        }
        if ((i2 & 2) != 0) {
            i = storyViewerPageState.selectedPostIndex;
        }
        if ((i2 & 4) != 0) {
            replyState = storyViewerPageState.replyState;
        }
        if ((i2 & 8) != 0) {
            z = storyViewerPageState.isFirstPage;
        }
        if ((i2 & 16) != 0) {
            z2 = storyViewerPageState.isDisplayingInitialState;
        }
        if ((i2 & 32) != 0) {
            z3 = storyViewerPageState.isReady;
        }
        if ((i2 & 64) != 0) {
            z4 = storyViewerPageState.isReceiptsEnabled;
        }
        return storyViewerPageState.copy(list, i, replyState, z, z2, z3, z4);
    }

    public final List<StoryPost> component1() {
        return this.posts;
    }

    public final int component2() {
        return this.selectedPostIndex;
    }

    public final ReplyState component3() {
        return this.replyState;
    }

    public final boolean component4() {
        return this.isFirstPage;
    }

    public final boolean component5() {
        return this.isDisplayingInitialState;
    }

    public final boolean component6() {
        return this.isReady;
    }

    public final boolean component7() {
        return this.isReceiptsEnabled;
    }

    public final StoryViewerPageState copy(List<StoryPost> list, int i, ReplyState replyState, boolean z, boolean z2, boolean z3, boolean z4) {
        Intrinsics.checkNotNullParameter(list, "posts");
        Intrinsics.checkNotNullParameter(replyState, "replyState");
        return new StoryViewerPageState(list, i, replyState, z, z2, z3, z4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryViewerPageState)) {
            return false;
        }
        StoryViewerPageState storyViewerPageState = (StoryViewerPageState) obj;
        return Intrinsics.areEqual(this.posts, storyViewerPageState.posts) && this.selectedPostIndex == storyViewerPageState.selectedPostIndex && this.replyState == storyViewerPageState.replyState && this.isFirstPage == storyViewerPageState.isFirstPage && this.isDisplayingInitialState == storyViewerPageState.isDisplayingInitialState && this.isReady == storyViewerPageState.isReady && this.isReceiptsEnabled == storyViewerPageState.isReceiptsEnabled;
    }

    public int hashCode() {
        int hashCode = ((((this.posts.hashCode() * 31) + this.selectedPostIndex) * 31) + this.replyState.hashCode()) * 31;
        boolean z = this.isFirstPage;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode + i2) * 31;
        boolean z2 = this.isDisplayingInitialState;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.isReady;
        if (z3) {
            z3 = true;
        }
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = (i9 + i10) * 31;
        boolean z4 = this.isReceiptsEnabled;
        if (!z4) {
            i = z4 ? 1 : 0;
        }
        return i13 + i;
    }

    public String toString() {
        return "StoryViewerPageState(posts=" + this.posts + ", selectedPostIndex=" + this.selectedPostIndex + ", replyState=" + this.replyState + ", isFirstPage=" + this.isFirstPage + ", isDisplayingInitialState=" + this.isDisplayingInitialState + ", isReady=" + this.isReady + ", isReceiptsEnabled=" + this.isReceiptsEnabled + ')';
    }

    public StoryViewerPageState(List<StoryPost> list, int i, ReplyState replyState, boolean z, boolean z2, boolean z3, boolean z4) {
        Intrinsics.checkNotNullParameter(list, "posts");
        Intrinsics.checkNotNullParameter(replyState, "replyState");
        this.posts = list;
        this.selectedPostIndex = i;
        this.replyState = replyState;
        this.isFirstPage = z;
        this.isDisplayingInitialState = z2;
        this.isReady = z3;
        this.isReceiptsEnabled = z4;
    }

    public /* synthetic */ StoryViewerPageState(List list, int i, ReplyState replyState, boolean z, boolean z2, boolean z3, boolean z4, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i2 & 2) != 0 ? 0 : i, (i2 & 4) != 0 ? ReplyState.NONE : replyState, (i2 & 8) != 0 ? false : z, (i2 & 16) != 0 ? false : z2, (i2 & 32) != 0 ? false : z3, z4);
    }

    public final List<StoryPost> getPosts() {
        return this.posts;
    }

    public final int getSelectedPostIndex() {
        return this.selectedPostIndex;
    }

    public final ReplyState getReplyState() {
        return this.replyState;
    }

    public final boolean isFirstPage() {
        return this.isFirstPage;
    }

    public final boolean isDisplayingInitialState() {
        return this.isDisplayingInitialState;
    }

    public final boolean isReady() {
        return this.isReady;
    }

    public final boolean isReceiptsEnabled() {
        return this.isReceiptsEnabled;
    }

    /* compiled from: StoryViewerPageState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0001\u0018\u0000 \b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState$ReplyState;", "", "(Ljava/lang/String;I)V", "NONE", "SELF", "GROUP", "PRIVATE", "GROUP_SELF", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum ReplyState {
        NONE,
        SELF,
        GROUP,
        PRIVATE,
        GROUP_SELF;
        
        public static final Companion Companion = new Companion(null);

        /* compiled from: StoryViewerPageState.kt */
        @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState$ReplyState$Companion;", "", "()V", "resolve", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState$ReplyState;", "isFromSelf", "", "isToGroup", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final ReplyState resolve(boolean z, boolean z2) {
                if (z && z2) {
                    return ReplyState.GROUP_SELF;
                }
                if (z && !z2) {
                    return ReplyState.SELF;
                }
                if (!z && z2) {
                    return ReplyState.GROUP;
                }
                if (z || z2) {
                    return ReplyState.NONE;
                }
                return ReplyState.PRIVATE;
            }
        }
    }
}
