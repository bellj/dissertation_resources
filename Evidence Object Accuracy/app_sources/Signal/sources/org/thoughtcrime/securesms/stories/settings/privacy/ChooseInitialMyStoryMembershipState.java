package org.thoughtcrime.securesms.stories.settings.privacy;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.settings.my.MyStoryPrivacyState;

/* compiled from: ChooseInitialMyStoryMembershipState.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001f\u0010\r\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipState;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "privacyState", "Lorg/thoughtcrime/securesms/stories/settings/my/MyStoryPrivacyState;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/stories/settings/my/MyStoryPrivacyState;)V", "getPrivacyState", "()Lorg/thoughtcrime/securesms/stories/settings/my/MyStoryPrivacyState;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ChooseInitialMyStoryMembershipState {
    private final MyStoryPrivacyState privacyState;
    private final RecipientId recipientId;

    public ChooseInitialMyStoryMembershipState() {
        this(null, null, 3, null);
    }

    public static /* synthetic */ ChooseInitialMyStoryMembershipState copy$default(ChooseInitialMyStoryMembershipState chooseInitialMyStoryMembershipState, RecipientId recipientId, MyStoryPrivacyState myStoryPrivacyState, int i, Object obj) {
        if ((i & 1) != 0) {
            recipientId = chooseInitialMyStoryMembershipState.recipientId;
        }
        if ((i & 2) != 0) {
            myStoryPrivacyState = chooseInitialMyStoryMembershipState.privacyState;
        }
        return chooseInitialMyStoryMembershipState.copy(recipientId, myStoryPrivacyState);
    }

    public final RecipientId component1() {
        return this.recipientId;
    }

    public final MyStoryPrivacyState component2() {
        return this.privacyState;
    }

    public final ChooseInitialMyStoryMembershipState copy(RecipientId recipientId, MyStoryPrivacyState myStoryPrivacyState) {
        Intrinsics.checkNotNullParameter(myStoryPrivacyState, "privacyState");
        return new ChooseInitialMyStoryMembershipState(recipientId, myStoryPrivacyState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChooseInitialMyStoryMembershipState)) {
            return false;
        }
        ChooseInitialMyStoryMembershipState chooseInitialMyStoryMembershipState = (ChooseInitialMyStoryMembershipState) obj;
        return Intrinsics.areEqual(this.recipientId, chooseInitialMyStoryMembershipState.recipientId) && Intrinsics.areEqual(this.privacyState, chooseInitialMyStoryMembershipState.privacyState);
    }

    public int hashCode() {
        RecipientId recipientId = this.recipientId;
        return ((recipientId == null ? 0 : recipientId.hashCode()) * 31) + this.privacyState.hashCode();
    }

    public String toString() {
        return "ChooseInitialMyStoryMembershipState(recipientId=" + this.recipientId + ", privacyState=" + this.privacyState + ')';
    }

    public ChooseInitialMyStoryMembershipState(RecipientId recipientId, MyStoryPrivacyState myStoryPrivacyState) {
        Intrinsics.checkNotNullParameter(myStoryPrivacyState, "privacyState");
        this.recipientId = recipientId;
        this.privacyState = myStoryPrivacyState;
    }

    public /* synthetic */ ChooseInitialMyStoryMembershipState(RecipientId recipientId, MyStoryPrivacyState myStoryPrivacyState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : recipientId, (i & 2) != 0 ? new MyStoryPrivacyState(null, 0, 3, null) : myStoryPrivacyState);
    }

    public final MyStoryPrivacyState getPrivacyState() {
        return this.privacyState;
    }

    public final RecipientId getRecipientId() {
        return this.recipientId;
    }
}
