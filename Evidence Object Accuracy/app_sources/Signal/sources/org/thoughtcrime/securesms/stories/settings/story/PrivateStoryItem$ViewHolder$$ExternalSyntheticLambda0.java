package org.thoughtcrime.securesms.stories.settings.story;

import android.view.View;
import org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class PrivateStoryItem$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ PrivateStoryItem.Model f$0;

    public /* synthetic */ PrivateStoryItem$ViewHolder$$ExternalSyntheticLambda0(PrivateStoryItem.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        PrivateStoryItem.ViewHolder.m2960bind$lambda0(this.f$0, view);
    }
}
