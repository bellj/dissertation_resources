package org.thoughtcrime.securesms.stories.viewer.page;

import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: StoryViewerPageFragment.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerPageFragment$displayMoreContextMenu$1 extends Lambda implements Function1<StoryPost, Unit> {
    final /* synthetic */ StoryViewerPageFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryViewerPageFragment$displayMoreContextMenu$1(StoryViewerPageFragment storyViewerPageFragment) {
        super(1);
        this.this$0 = storyViewerPageFragment;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoryPost storyPost) {
        invoke(storyPost);
        return Unit.INSTANCE;
    }

    public final void invoke(StoryPost storyPost) {
        Intrinsics.checkNotNullParameter(storyPost, "it");
        LifecycleDisposable lifecycleDisposable = this.this$0.lifecycleDisposable;
        Disposable subscribe = this.this$0.getViewModel().hideStory().subscribe(new Action() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$displayMoreContextMenu$1$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                StoryViewerPageFragment$displayMoreContextMenu$1.m3051invoke$lambda0(StoryViewerPageFragment.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.hideStory().su…oryRecipientId)\n        }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m3051invoke$lambda0(StoryViewerPageFragment storyViewerPageFragment) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        StoryViewerPageFragment.Callback callback = storyViewerPageFragment.callback;
        if (callback == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
            callback = null;
        }
        callback.onStoryHidden(storyViewerPageFragment.getStoryRecipientId());
    }
}
