package org.thoughtcrime.securesms.stories.settings.custom;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: PrivateStorySettingsViewModel.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u001dB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0010\u001a\u00020\u0011J\u0006\u0010\u0012\u001a\u00020\u0013J\b\u0010\u0014\u001a\u00020\u0015H\u0014J\u0006\u0010\u0016\u001a\u00020\u0015J\u000e\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u0019J\u000e\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u001cR\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsRepository;", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "delete", "Lio/reactivex/rxjava3/core/Completable;", "getName", "", "onCleared", "", "refresh", "remove", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "setRepliesAndReactionsEnabled", "repliesAndReactionsEnabled", "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class PrivateStorySettingsViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final DistributionListId distributionListId;
    private final PrivateStorySettingsRepository repository;
    private final LiveData<PrivateStorySettingsState> state;
    private final Store<PrivateStorySettingsState> store;

    public PrivateStorySettingsViewModel(DistributionListId distributionListId, PrivateStorySettingsRepository privateStorySettingsRepository) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        Intrinsics.checkNotNullParameter(privateStorySettingsRepository, "repository");
        this.distributionListId = distributionListId;
        this.repository = privateStorySettingsRepository;
        Store<PrivateStorySettingsState> store = new Store<>(new PrivateStorySettingsState(null, false, 3, null));
        this.store = store;
        LiveData<PrivateStorySettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<PrivateStorySettingsState> getState() {
        return this.state;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void refresh() {
        this.disposables.clear();
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.repository.getRecord(this.distributionListId).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                PrivateStorySettingsViewModel.m2891refresh$lambda1(PrivateStorySettingsViewModel.this, (DistributionListRecord) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getRecord(dis…Story = record) }\n      }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
        CompositeDisposable compositeDisposable2 = this.disposables;
        Disposable subscribe2 = this.repository.getRepliesAndReactionsEnabled(this.distributionListId).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                PrivateStorySettingsViewModel.m2893refresh$lambda3(PrivateStorySettingsViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "repository.getRepliesAnd…actionsEnabled) }\n      }");
        DisposableKt.plusAssign(compositeDisposable2, subscribe2);
    }

    /* renamed from: refresh$lambda-1 */
    public static final void m2891refresh$lambda1(PrivateStorySettingsViewModel privateStorySettingsViewModel, DistributionListRecord distributionListRecord) {
        Intrinsics.checkNotNullParameter(privateStorySettingsViewModel, "this$0");
        privateStorySettingsViewModel.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PrivateStorySettingsViewModel.m2892refresh$lambda1$lambda0(DistributionListRecord.this, (PrivateStorySettingsState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-1$lambda-0 */
    public static final PrivateStorySettingsState m2892refresh$lambda1$lambda0(DistributionListRecord distributionListRecord, PrivateStorySettingsState privateStorySettingsState) {
        Intrinsics.checkNotNullExpressionValue(privateStorySettingsState, "it");
        return PrivateStorySettingsState.copy$default(privateStorySettingsState, distributionListRecord, false, 2, null);
    }

    /* renamed from: refresh$lambda-3 */
    public static final void m2893refresh$lambda3(PrivateStorySettingsViewModel privateStorySettingsViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(privateStorySettingsViewModel, "this$0");
        privateStorySettingsViewModel.store.update(new Function(bool) { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ Boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PrivateStorySettingsViewModel.m2894refresh$lambda3$lambda2(this.f$0, (PrivateStorySettingsState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-3$lambda-2 */
    public static final PrivateStorySettingsState m2894refresh$lambda3$lambda2(Boolean bool, PrivateStorySettingsState privateStorySettingsState) {
        Intrinsics.checkNotNullExpressionValue(privateStorySettingsState, "it");
        Intrinsics.checkNotNullExpressionValue(bool, "repliesAndReactionsEnabled");
        return PrivateStorySettingsState.copy$default(privateStorySettingsState, null, bool.booleanValue(), 1, null);
    }

    public final String getName() {
        String name;
        DistributionListRecord privateStory = this.store.getState().getPrivateStory();
        return (privateStory == null || (name = privateStory.getName()) == null) ? "" : name;
    }

    public final void remove(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        CompositeDisposable compositeDisposable = this.disposables;
        PrivateStorySettingsRepository privateStorySettingsRepository = this.repository;
        DistributionListRecord privateStory = this.store.getState().getPrivateStory();
        Intrinsics.checkNotNull(privateStory);
        RecipientId id = recipient.getId();
        Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
        Disposable subscribe = privateStorySettingsRepository.removeMember(privateStory, id).subscribe(new Action() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                PrivateStorySettingsViewModel.m2895remove$lambda4(PrivateStorySettingsViewModel.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.removeMember(…        refresh()\n      }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* renamed from: remove$lambda-4 */
    public static final void m2895remove$lambda4(PrivateStorySettingsViewModel privateStorySettingsViewModel) {
        Intrinsics.checkNotNullParameter(privateStorySettingsViewModel, "this$0");
        privateStorySettingsViewModel.refresh();
    }

    public final void setRepliesAndReactionsEnabled(boolean z) {
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.repository.setRepliesAndReactionsEnabled(this.distributionListId, z).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action() { // from class: org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsViewModel$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                PrivateStorySettingsViewModel.m2896setRepliesAndReactionsEnabled$lambda5(PrivateStorySettingsViewModel.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.setRepliesAnd… .subscribe { refresh() }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* renamed from: setRepliesAndReactionsEnabled$lambda-5 */
    public static final void m2896setRepliesAndReactionsEnabled$lambda5(PrivateStorySettingsViewModel privateStorySettingsViewModel) {
        Intrinsics.checkNotNullParameter(privateStorySettingsViewModel, "this$0");
        privateStorySettingsViewModel.refresh();
    }

    public final Completable delete() {
        Completable observeOn = this.repository.delete(this.distributionListId).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.delete(distri…dSchedulers.mainThread())");
        return observeOn;
    }

    /* compiled from: PrivateStorySettingsViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "privateStoryItemData", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsRepository;", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final DistributionListId privateStoryItemData;
        private final PrivateStorySettingsRepository repository;

        public Factory(DistributionListId distributionListId, PrivateStorySettingsRepository privateStorySettingsRepository) {
            Intrinsics.checkNotNullParameter(distributionListId, "privateStoryItemData");
            Intrinsics.checkNotNullParameter(privateStorySettingsRepository, "repository");
            this.privateStoryItemData = distributionListId;
            this.repository = privateStorySettingsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new PrivateStorySettingsViewModel(this.privateStoryItemData, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsViewModel.Factory.create");
        }
    }
}
