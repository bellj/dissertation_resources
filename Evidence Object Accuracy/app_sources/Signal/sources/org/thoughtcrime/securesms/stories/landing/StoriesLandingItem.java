package org.thoughtcrime.securesms.stories.landing;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import j$.util.function.Function;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.view.AvatarView;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.stories.StoryTextPostModel;
import org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu;
import org.thoughtcrime.securesms.stories.landing.StoriesLandingItem;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: StoriesLandingItem.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\t\nB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItem;", "", "()V", "STATUS_CHANGE", "", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoriesLandingItem {
    public static final StoriesLandingItem INSTANCE = new StoriesLandingItem();
    private static final int STATUS_CHANGE;

    private StoriesLandingItem() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new StoriesLandingItem.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_landing_item));
    }

    /* compiled from: StoriesLandingItem.kt */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001BÇ\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00060\b\u0012\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b\u0012\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b\u0012\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b\u0012\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b\u0012\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b\u0012\u0018\u0010\u0011\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00060\b¢\u0006\u0002\u0010\u0012J\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0000H\u0016J\u0010\u0010$\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0000H\u0016J\u0012\u0010%\u001a\u0004\u0018\u00010&2\u0006\u0010#\u001a\u00020\u0000H\u0016J\u0010\u0010'\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0000H\u0002J\u0010\u0010(\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0000H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u001d\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u001d\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0018R\u001d\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0018R\u001d\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0018R#\u0010\u0011\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00060\b¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR#\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00060\b¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001dR\u001d\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0018R\u001d\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00060\u000b¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0018¨\u0006)"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItem$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "data", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItemData;", "onAvatarClick", "Lkotlin/Function0;", "", "onRowClick", "Lkotlin/Function2;", "Landroid/view/View;", "onHideStory", "Lkotlin/Function1;", "onForwardStory", "onShareStory", "onGoToChat", "onSave", "onDeleteStory", "onInfo", "(Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItemData;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V", "getData", "()Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItemData;", "getOnAvatarClick", "()Lkotlin/jvm/functions/Function0;", "getOnDeleteStory", "()Lkotlin/jvm/functions/Function1;", "getOnForwardStory", "getOnGoToChat", "getOnHideStory", "getOnInfo", "()Lkotlin/jvm/functions/Function2;", "getOnRowClick", "getOnSave", "getOnShareStory", "areContentsTheSame", "", "newItem", "areItemsTheSame", "getChangePayload", "", "hasStatusChange", "isSameRecord", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model implements MappingModel<Model> {
        private final StoriesLandingItemData data;
        private final Function0<Unit> onAvatarClick;
        private final Function1<Model, Unit> onDeleteStory;
        private final Function1<Model, Unit> onForwardStory;
        private final Function1<Model, Unit> onGoToChat;
        private final Function1<Model, Unit> onHideStory;
        private final Function2<Model, View, Unit> onInfo;
        private final Function2<Model, View, Unit> onRowClick;
        private final Function1<Model, Unit> onSave;
        private final Function1<Model, Unit> onShareStory;

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function2<? super org.thoughtcrime.securesms.stories.landing.StoriesLandingItem$Model, ? super android.view.View, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.landing.StoriesLandingItem$Model, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.landing.StoriesLandingItem$Model, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.landing.StoriesLandingItem$Model, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.landing.StoriesLandingItem$Model, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r9v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.landing.StoriesLandingItem$Model, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r10v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.landing.StoriesLandingItem$Model, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r11v0, resolved type: kotlin.jvm.functions.Function2<? super org.thoughtcrime.securesms.stories.landing.StoriesLandingItem$Model, ? super android.view.View, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        public Model(StoriesLandingItemData storiesLandingItemData, Function0<Unit> function0, Function2<? super Model, ? super View, Unit> function2, Function1<? super Model, Unit> function1, Function1<? super Model, Unit> function12, Function1<? super Model, Unit> function13, Function1<? super Model, Unit> function14, Function1<? super Model, Unit> function15, Function1<? super Model, Unit> function16, Function2<? super Model, ? super View, Unit> function22) {
            Intrinsics.checkNotNullParameter(storiesLandingItemData, "data");
            Intrinsics.checkNotNullParameter(function0, "onAvatarClick");
            Intrinsics.checkNotNullParameter(function2, "onRowClick");
            Intrinsics.checkNotNullParameter(function1, "onHideStory");
            Intrinsics.checkNotNullParameter(function12, "onForwardStory");
            Intrinsics.checkNotNullParameter(function13, "onShareStory");
            Intrinsics.checkNotNullParameter(function14, "onGoToChat");
            Intrinsics.checkNotNullParameter(function15, "onSave");
            Intrinsics.checkNotNullParameter(function16, "onDeleteStory");
            Intrinsics.checkNotNullParameter(function22, "onInfo");
            this.data = storiesLandingItemData;
            this.onAvatarClick = function0;
            this.onRowClick = function2;
            this.onHideStory = function1;
            this.onForwardStory = function12;
            this.onShareStory = function13;
            this.onGoToChat = function14;
            this.onSave = function15;
            this.onDeleteStory = function16;
            this.onInfo = function22;
        }

        public final StoriesLandingItemData getData() {
            return this.data;
        }

        public final Function0<Unit> getOnAvatarClick() {
            return this.onAvatarClick;
        }

        public final Function2<Model, View, Unit> getOnRowClick() {
            return this.onRowClick;
        }

        public final Function1<Model, Unit> getOnHideStory() {
            return this.onHideStory;
        }

        public final Function1<Model, Unit> getOnForwardStory() {
            return this.onForwardStory;
        }

        public final Function1<Model, Unit> getOnShareStory() {
            return this.onShareStory;
        }

        public final Function1<Model, Unit> getOnGoToChat() {
            return this.onGoToChat;
        }

        public final Function1<Model, Unit> getOnSave() {
            return this.onSave;
        }

        public final Function1<Model, Unit> getOnDeleteStory() {
            return this.onDeleteStory;
        }

        public final Function2<Model, View, Unit> getOnInfo() {
            return this.onInfo;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.data.getStoryRecipient().getId(), model.data.getStoryRecipient().getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return this.data.getStoryRecipient().hasSameContent(model.data.getStoryRecipient()) && Intrinsics.areEqual(this.data, model.data) && !hasStatusChange(model) && this.data.getSendingCount() == model.data.getSendingCount() && this.data.getFailureCount() == model.data.getFailureCount() && this.data.getStoryViewState() == model.data.getStoryViewState();
        }

        public Object getChangePayload(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return (!isSameRecord(model) || !hasStatusChange(model)) ? null : 0;
        }

        private final boolean isSameRecord(Model model) {
            return this.data.getPrimaryStory().getMessageRecord().getId() == model.data.getPrimaryStory().getMessageRecord().getId();
        }

        private final boolean hasStatusChange(Model model) {
            MessageRecord messageRecord = this.data.getPrimaryStory().getMessageRecord();
            Intrinsics.checkNotNullExpressionValue(messageRecord, "data.primaryStory.messageRecord");
            MessageRecord messageRecord2 = model.data.getPrimaryStory().getMessageRecord();
            Intrinsics.checkNotNullExpressionValue(messageRecord2, "newItem.data.primaryStory.messageRecord");
            return messageRecord.isOutgoing() && messageRecord2.isOutgoing() && !(messageRecord.isPending() == messageRecord2.isPending() && messageRecord.isSent() == messageRecord2.isSent() && messageRecord.isFailed() == messageRecord2.isFailed());
        }
    }

    /* compiled from: StoriesLandingItem.kt */
    @Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001 B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0016J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0002J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0017\u001a\u00020\u0002H\u0002J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0017\u001a\u00020\u0002H\u0002J\u0010\u0010\u001e\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0002J\u0010\u0010\u001f\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0002R\u000e\u0010\u0006\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItem$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "addToStoriesView", "avatarView", "Lorg/thoughtcrime/securesms/avatar/view/AvatarView;", "badgeView", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "date", "Landroid/widget/TextView;", "errorIndicator", "icon", "Landroid/widget/ImageView;", "sender", "storyBlur", "storyMulti", "storyOutline", "storyPreview", "bind", "", "model", "clearGlide", "displayContext", "getGroupPresentation", "", "getReleaseNotesPresentation", "", "presentDateOrStatus", "setUpClickListeners", "HideBlurAfterLoadListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final View addToStoriesView;
        private final AvatarView avatarView;
        private final BadgeImageView badgeView;
        private final TextView date;
        private final View errorIndicator;
        private final ImageView icon;
        private final TextView sender;
        private final ImageView storyBlur;
        private final ImageView storyMulti;
        private final ImageView storyOutline;
        private final ImageView storyPreview;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.avatar)");
            this.avatarView = (AvatarView) findViewById;
            View findViewById2 = view.findViewById(R.id.badge);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.badge)");
            this.badgeView = (BadgeImageView) findViewById2;
            View findViewById3 = view.findViewById(R.id.story);
            ImageView imageView = (ImageView) findViewById3;
            imageView.setClickable(false);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById<Im…isClickable = false\n    }");
            this.storyPreview = imageView;
            View findViewById4 = view.findViewById(R.id.story_blur);
            ImageView imageView2 = (ImageView) findViewById4;
            imageView2.setClickable(false);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "itemView.findViewById<Im…isClickable = false\n    }");
            this.storyBlur = imageView2;
            View findViewById5 = view.findViewById(R.id.story_outline);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "itemView.findViewById(R.id.story_outline)");
            this.storyOutline = (ImageView) findViewById5;
            View findViewById6 = view.findViewById(R.id.story_multi);
            ImageView imageView3 = (ImageView) findViewById6;
            imageView3.setClickable(false);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "itemView.findViewById<Im…isClickable = false\n    }");
            this.storyMulti = imageView3;
            View findViewById7 = view.findViewById(R.id.sender);
            Intrinsics.checkNotNullExpressionValue(findViewById7, "itemView.findViewById(R.id.sender)");
            this.sender = (TextView) findViewById7;
            View findViewById8 = view.findViewById(R.id.date);
            Intrinsics.checkNotNullExpressionValue(findViewById8, "itemView.findViewById(R.id.date)");
            this.date = (TextView) findViewById8;
            View findViewById9 = view.findViewById(R.id.icon);
            Intrinsics.checkNotNullExpressionValue(findViewById9, "itemView.findViewById(R.id.icon)");
            this.icon = (ImageView) findViewById9;
            View findViewById10 = view.findViewById(R.id.error_indicator);
            Intrinsics.checkNotNullExpressionValue(findViewById10, "itemView.findViewById(R.id.error_indicator)");
            this.errorIndicator = findViewById10;
            View findViewById11 = view.findViewById(R.id.add_to_story);
            Intrinsics.checkNotNullExpressionValue(findViewById11, "itemView.findViewById(R.id.add_to_story)");
            this.addToStoriesView = findViewById11;
        }

        public void bind(Model model) {
            CharSequence charSequence;
            Intrinsics.checkNotNullParameter(model, "model");
            presentDateOrStatus(model);
            setUpClickListeners(model);
            if (!this.payload.contains(0)) {
                Uri uri = null;
                if (model.getData().getStoryRecipient().isMyStory()) {
                    AvatarView avatarView = this.avatarView;
                    Recipient self = Recipient.self();
                    Intrinsics.checkNotNullExpressionValue(self, "self()");
                    avatarView.displayProfileAvatar(self);
                    this.badgeView.setBadgeFromRecipient(null);
                } else {
                    this.avatarView.displayProfileAvatar(model.getData().getStoryRecipient());
                    this.badgeView.setBadgeFromRecipient(model.getData().getStoryRecipient());
                }
                MediaMmsMessageRecord mediaMmsMessageRecord = (MediaMmsMessageRecord) model.getData().getPrimaryStory().getMessageRecord();
                this.avatarView.setStoryRingFromState(model.getData().getStoryViewState());
                Slide thumbnailSlide = mediaMmsMessageRecord.getSlideDeck().getThumbnailSlide();
                Uri uri2 = thumbnailSlide != null ? thumbnailSlide.getUri() : null;
                Slide thumbnailSlide2 = mediaMmsMessageRecord.getSlideDeck().getThumbnailSlide();
                BlurHash placeholderBlur = thumbnailSlide2 != null ? thumbnailSlide2.getPlaceholderBlur() : null;
                clearGlide();
                ViewExtensionsKt.setVisible(this.storyBlur, placeholderBlur != null);
                if (placeholderBlur != null) {
                    GlideApp.with(this.storyBlur).load((Object) placeholderBlur).into(this.storyBlur);
                }
                if (mediaMmsMessageRecord.getStoryType().isTextStory()) {
                    ViewExtensionsKt.setVisible(this.storyBlur, false);
                    StoryTextPostModel parseFrom = StoryTextPostModel.CREATOR.parseFrom(mediaMmsMessageRecord);
                    GlideApp.with(this.storyPreview).load((Object) parseFrom).placeholder(parseFrom.getPlaceholder()).centerCrop().dontAnimate().into(this.storyPreview);
                } else if (uri2 != null) {
                    ViewExtensionsKt.setVisible(this.storyBlur, placeholderBlur != null);
                    GlideApp.with(this.storyPreview).load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri2)).addListener((RequestListener<Drawable>) new HideBlurAfterLoadListener()).centerCrop().dontAnimate().into(this.storyPreview);
                }
                if (model.getData().getSecondaryStory() != null) {
                    MediaMmsMessageRecord mediaMmsMessageRecord2 = (MediaMmsMessageRecord) model.getData().getSecondaryStory().getMessageRecord();
                    Slide thumbnailSlide3 = mediaMmsMessageRecord2.getSlideDeck().getThumbnailSlide();
                    if (thumbnailSlide3 != null) {
                        uri = thumbnailSlide3.getUri();
                    }
                    this.storyOutline.setBackgroundColor(ContextCompat.getColor(this.context, R.color.signal_background_primary));
                    if (mediaMmsMessageRecord2.getStoryType().isTextStory()) {
                        StoryTextPostModel parseFrom2 = StoryTextPostModel.CREATOR.parseFrom(mediaMmsMessageRecord2);
                        GlideApp.with(this.storyMulti).load((Object) parseFrom2).placeholder(parseFrom2.getPlaceholder()).centerCrop().dontAnimate().into(this.storyMulti);
                        ViewExtensionsKt.setVisible(this.storyMulti, true);
                    } else if (uri != null) {
                        GlideApp.with(this.storyMulti).load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).centerCrop().dontAnimate().into(this.storyMulti);
                        ViewExtensionsKt.setVisible(this.storyMulti, true);
                    } else {
                        this.storyOutline.setBackgroundColor(0);
                        GlideApp.with(this.storyMulti).clear(this.storyMulti);
                        ViewExtensionsKt.setVisible(this.storyMulti, false);
                    }
                } else {
                    this.storyOutline.setBackgroundColor(0);
                    GlideApp.with(this.storyMulti).clear(this.storyMulti);
                    ViewExtensionsKt.setVisible(this.storyMulti, false);
                }
                TextView textView = this.sender;
                if (model.getData().getStoryRecipient().isMyStory()) {
                    charSequence = this.context.getText(R.string.StoriesLandingFragment__my_stories);
                } else if (model.getData().getStoryRecipient().isGroup()) {
                    charSequence = getGroupPresentation(model);
                } else if (model.getData().getStoryRecipient().isReleaseNotes()) {
                    charSequence = getReleaseNotesPresentation(model);
                } else {
                    charSequence = model.getData().getStoryRecipient().getDisplayName(this.context);
                }
                textView.setText(charSequence);
                ViewExtensionsKt.setVisible(this.icon, model.getData().getHasReplies() || model.getData().getHasRepliesFromSelf());
                this.icon.setImageResource(model.getData().getHasReplies() ? R.drawable.ic_messages_solid_20 : R.drawable.ic_reply_24_solid_tinted);
                for (View view : CollectionsKt__CollectionsKt.listOf((Object[]) new View[]{this.avatarView, this.storyPreview, this.storyMulti, this.sender, this.date, this.icon})) {
                    view.setAlpha(model.getData().isHidden() ? 0.5f : 1.0f);
                }
            }
        }

        private final void presentDateOrStatus(Model model) {
            if (model.getData().getSendingCount() > 0 || (model.getData().getPrimaryStory().getMessageRecord().isOutgoing() && (model.getData().getPrimaryStory().getMessageRecord().isPending() || model.getData().getPrimaryStory().getMessageRecord().isMediaPending()))) {
                ViewExtensionsKt.setVisible(this.errorIndicator, model.getData().getFailureCount() > 0);
                if (model.getData().getSendingCount() > 1) {
                    this.date.setText(this.context.getString(R.string.StoriesLandingItem__sending_d, Long.valueOf(model.getData().getSendingCount())));
                } else {
                    this.date.setText(R.string.StoriesLandingItem__sending);
                }
            } else if (model.getData().getFailureCount() > 0 || (model.getData().getPrimaryStory().getMessageRecord().isOutgoing() && model.getData().getPrimaryStory().getMessageRecord().isFailed())) {
                ViewExtensionsKt.setVisible(this.errorIndicator, true);
                this.date.setText(SpanUtil.color(ContextCompat.getColor(this.context, R.color.signal_alert_primary), this.context.getString(model.getData().getPrimaryStory().getMessageRecord().isIdentityMismatchFailure() ? R.string.StoriesLandingItem__partially_sent : R.string.StoriesLandingItem__send_failed)));
            } else {
                ViewExtensionsKt.setVisible(this.errorIndicator, false);
                this.date.setText(DateUtils.getBriefRelativeTimeSpanString(this.context, Locale.getDefault(), model.getData().getDateInMilliseconds()));
            }
        }

        private final void setUpClickListeners(Model model) {
            this.itemView.setOnClickListener(new StoriesLandingItem$ViewHolder$$ExternalSyntheticLambda0(model, this));
            if (model.getData().getStoryRecipient().isMyStory()) {
                this.itemView.setOnLongClickListener(null);
                this.avatarView.setOnClickListener(new StoriesLandingItem$ViewHolder$$ExternalSyntheticLambda1(model));
                ViewExtensionsKt.setVisible(this.addToStoriesView, true);
                return;
            }
            this.itemView.setOnLongClickListener(new StoriesLandingItem$ViewHolder$$ExternalSyntheticLambda2(this, model));
            this.avatarView.setOnClickListener(null);
            this.avatarView.setClickable(false);
            ViewExtensionsKt.setVisible(this.addToStoriesView, false);
        }

        /* renamed from: setUpClickListeners$lambda-4 */
        public static final void m2807setUpClickListeners$lambda4(Model model, ViewHolder viewHolder, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            model.getOnRowClick().invoke(model, viewHolder.storyPreview);
        }

        /* renamed from: setUpClickListeners$lambda-5 */
        public static final void m2808setUpClickListeners$lambda5(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnAvatarClick().invoke();
        }

        /* renamed from: setUpClickListeners$lambda-6 */
        public static final boolean m2809setUpClickListeners$lambda6(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            viewHolder.displayContext(model);
            return true;
        }

        private final String getGroupPresentation(Model model) {
            String displayName = model.getData().getStoryRecipient().getDisplayName(this.context);
            Intrinsics.checkNotNullExpressionValue(displayName, "model.data.storyRecipient.getDisplayName(context)");
            return displayName;
        }

        private final CharSequence getReleaseNotesPresentation(Model model) {
            Drawable requireDrawable = ContextUtil.requireDrawable(this.context, R.drawable.ic_official_20);
            Intrinsics.checkNotNullExpressionValue(requireDrawable, "requireDrawable(context,….drawable.ic_official_20)");
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(model.getData().getStoryRecipient().getDisplayName(this.context));
            SpanUtil.appendCenteredImageSpan(spannableStringBuilder, requireDrawable, 20, 20);
            return spannableStringBuilder;
        }

        private final void displayContext(Model model) {
            this.itemView.setSelected(true);
            StoryContextMenu storyContextMenu = StoryContextMenu.INSTANCE;
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            View view = this.itemView;
            Intrinsics.checkNotNullExpressionValue(view, "itemView");
            storyContextMenu.show(context, view, this.storyPreview, model, new StoriesLandingItem$ViewHolder$displayContext$1(this));
        }

        private final void clearGlide() {
            GlideApp.with(this.storyPreview).clear(this.storyPreview);
            GlideApp.with(this.storyBlur).clear(this.storyBlur);
        }

        /* compiled from: StoriesLandingItem.kt */
        @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J4\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\u0010\b\u001a\u0004\u0018\u00010\t2\u000e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\u0005H\u0016J>\u0010\r\u001a\u00020\u00052\b\u0010\u000e\u001a\u0004\u0018\u00010\u00022\b\u0010\b\u001a\u0004\u0018\u00010\t2\u000e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\f\u001a\u00020\u0005H\u0016¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItem$ViewHolder$HideBlurAfterLoadListener;", "Lcom/bumptech/glide/request/RequestListener;", "Landroid/graphics/drawable/Drawable;", "(Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItem$ViewHolder;)V", "onLoadFailed", "", "e", "Lcom/bumptech/glide/load/engine/GlideException;", "model", "", "target", "Lcom/bumptech/glide/request/target/Target;", "isFirstResource", "onResourceReady", "resource", "dataSource", "Lcom/bumptech/glide/load/DataSource;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public final class HideBlurAfterLoadListener implements RequestListener<Drawable> {
            @Override // com.bumptech.glide.request.RequestListener
            public boolean onLoadFailed(GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                return false;
            }

            public HideBlurAfterLoadListener() {
                ViewHolder.this = r1;
            }

            @Override // com.bumptech.glide.request.RequestListener
            public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, DataSource dataSource, boolean z) {
                return onResourceReady((Drawable) obj, obj2, (Target<Drawable>) target, dataSource, z);
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                ViewExtensionsKt.setVisible(ViewHolder.this.storyBlur, false);
                return false;
            }
        }
    }
}
