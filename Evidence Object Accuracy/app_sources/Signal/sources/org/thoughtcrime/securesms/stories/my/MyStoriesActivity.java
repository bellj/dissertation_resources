package org.thoughtcrime.securesms.stories.my;

import androidx.fragment.app.Fragment;
import kotlin.Metadata;
import org.thoughtcrime.securesms.components.FragmentWrapperActivity;

/* compiled from: MyStoriesActivity.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesActivity;", "Lorg/thoughtcrime/securesms/components/FragmentWrapperActivity;", "()V", "getFragment", "Landroidx/fragment/app/Fragment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStoriesActivity extends FragmentWrapperActivity {
    @Override // org.thoughtcrime.securesms.components.FragmentWrapperActivity
    public Fragment getFragment() {
        return new MyStoriesFragment();
    }
}
