package org.thoughtcrime.securesms.stories.landing;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: StoriesLandingState.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u001eB3\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0006\u0012\b\b\u0002\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u000f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0016\u001a\u00020\tHÆ\u0003J7\u0010\u0017\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\tHÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00062\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\fR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingState;", "", "storiesLandingItems", "", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItemData;", "displayMyStoryItem", "", "isHiddenContentVisible", "loadingState", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingState$LoadingState;", "(Ljava/util/List;ZZLorg/thoughtcrime/securesms/stories/landing/StoriesLandingState$LoadingState;)V", "getDisplayMyStoryItem", "()Z", "hasNoStories", "getHasNoStories", "getLoadingState", "()Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingState$LoadingState;", "getStoriesLandingItems", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "toString", "", "LoadingState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoriesLandingState {
    private final boolean displayMyStoryItem;
    private final boolean hasNoStories;
    private final boolean isHiddenContentVisible;
    private final LoadingState loadingState;
    private final List<StoriesLandingItemData> storiesLandingItems;

    /* compiled from: StoriesLandingState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingState$LoadingState;", "", "(Ljava/lang/String;I)V", "INIT", "LOADED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum LoadingState {
        INIT,
        LOADED
    }

    public StoriesLandingState() {
        this(null, false, false, null, 15, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.stories.landing.StoriesLandingState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ StoriesLandingState copy$default(StoriesLandingState storiesLandingState, List list, boolean z, boolean z2, LoadingState loadingState, int i, Object obj) {
        if ((i & 1) != 0) {
            list = storiesLandingState.storiesLandingItems;
        }
        if ((i & 2) != 0) {
            z = storiesLandingState.displayMyStoryItem;
        }
        if ((i & 4) != 0) {
            z2 = storiesLandingState.isHiddenContentVisible;
        }
        if ((i & 8) != 0) {
            loadingState = storiesLandingState.loadingState;
        }
        return storiesLandingState.copy(list, z, z2, loadingState);
    }

    public final List<StoriesLandingItemData> component1() {
        return this.storiesLandingItems;
    }

    public final boolean component2() {
        return this.displayMyStoryItem;
    }

    public final boolean component3() {
        return this.isHiddenContentVisible;
    }

    public final LoadingState component4() {
        return this.loadingState;
    }

    public final StoriesLandingState copy(List<StoriesLandingItemData> list, boolean z, boolean z2, LoadingState loadingState) {
        Intrinsics.checkNotNullParameter(list, "storiesLandingItems");
        Intrinsics.checkNotNullParameter(loadingState, "loadingState");
        return new StoriesLandingState(list, z, z2, loadingState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoriesLandingState)) {
            return false;
        }
        StoriesLandingState storiesLandingState = (StoriesLandingState) obj;
        return Intrinsics.areEqual(this.storiesLandingItems, storiesLandingState.storiesLandingItems) && this.displayMyStoryItem == storiesLandingState.displayMyStoryItem && this.isHiddenContentVisible == storiesLandingState.isHiddenContentVisible && this.loadingState == storiesLandingState.loadingState;
    }

    public int hashCode() {
        int hashCode = this.storiesLandingItems.hashCode() * 31;
        boolean z = this.displayMyStoryItem;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode + i2) * 31;
        boolean z2 = this.isHiddenContentVisible;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        return ((i5 + i) * 31) + this.loadingState.hashCode();
    }

    public String toString() {
        return "StoriesLandingState(storiesLandingItems=" + this.storiesLandingItems + ", displayMyStoryItem=" + this.displayMyStoryItem + ", isHiddenContentVisible=" + this.isHiddenContentVisible + ", loadingState=" + this.loadingState + ')';
    }

    public StoriesLandingState(List<StoriesLandingItemData> list, boolean z, boolean z2, LoadingState loadingState) {
        Intrinsics.checkNotNullParameter(list, "storiesLandingItems");
        Intrinsics.checkNotNullParameter(loadingState, "loadingState");
        this.storiesLandingItems = list;
        this.displayMyStoryItem = z;
        this.isHiddenContentVisible = z2;
        this.loadingState = loadingState;
        this.hasNoStories = loadingState == LoadingState.LOADED && list.isEmpty();
    }

    public /* synthetic */ StoriesLandingState(List list, boolean z, boolean z2, LoadingState loadingState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i & 2) != 0 ? false : z, (i & 4) != 0 ? false : z2, (i & 8) != 0 ? LoadingState.INIT : loadingState);
    }

    public final List<StoriesLandingItemData> getStoriesLandingItems() {
        return this.storiesLandingItems;
    }

    public final boolean getDisplayMyStoryItem() {
        return this.displayMyStoryItem;
    }

    public final boolean isHiddenContentVisible() {
        return this.isHiddenContentVisible;
    }

    public final LoadingState getLoadingState() {
        return this.loadingState;
    }

    public final boolean getHasNoStories() {
        return this.hasNoStories;
    }
}
