package org.thoughtcrime.securesms.stories.settings.story;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.DistributionListPartialRecord;

/* compiled from: StorySettingsState.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsState;", "", "privateStories", "", "Lorg/thoughtcrime/securesms/database/model/DistributionListPartialRecord;", "(Ljava/util/List;)V", "getPrivateStories", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StorySettingsState {
    private final List<DistributionListPartialRecord> privateStories;

    public StorySettingsState() {
        this(null, 1, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.stories.settings.story.StorySettingsState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ StorySettingsState copy$default(StorySettingsState storySettingsState, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = storySettingsState.privateStories;
        }
        return storySettingsState.copy(list);
    }

    public final List<DistributionListPartialRecord> component1() {
        return this.privateStories;
    }

    public final StorySettingsState copy(List<DistributionListPartialRecord> list) {
        Intrinsics.checkNotNullParameter(list, "privateStories");
        return new StorySettingsState(list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof StorySettingsState) && Intrinsics.areEqual(this.privateStories, ((StorySettingsState) obj).privateStories);
    }

    public int hashCode() {
        return this.privateStories.hashCode();
    }

    public String toString() {
        return "StorySettingsState(privateStories=" + this.privateStories + ')';
    }

    public StorySettingsState(List<DistributionListPartialRecord> list) {
        Intrinsics.checkNotNullParameter(list, "privateStories");
        this.privateStories = list;
    }

    public /* synthetic */ StorySettingsState(List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list);
    }

    public final List<DistributionListPartialRecord> getPrivateStories() {
        return this.privateStories;
    }
}
