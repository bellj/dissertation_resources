package org.thoughtcrime.securesms.stories.settings.custom;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;

/* loaded from: classes3.dex */
public class PrivateStorySettingsFragmentDirections {
    private PrivateStorySettingsFragmentDirections() {
    }

    public static ActionPrivateStorySettingsToEditStoryNameFragment actionPrivateStorySettingsToEditStoryNameFragment(DistributionListId distributionListId, String str) {
        return new ActionPrivateStorySettingsToEditStoryNameFragment(distributionListId, str);
    }

    public static ActionPrivateStorySettingsToEditStoryViewers actionPrivateStorySettingsToEditStoryViewers(DistributionListId distributionListId) {
        return new ActionPrivateStorySettingsToEditStoryViewers(distributionListId);
    }

    /* loaded from: classes3.dex */
    public static class ActionPrivateStorySettingsToEditStoryNameFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_privateStorySettings_to_editStoryNameFragment;
        }

        private ActionPrivateStorySettingsToEditStoryNameFragment(DistributionListId distributionListId, String str) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (distributionListId != null) {
                hashMap.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                if (str != null) {
                    hashMap.put("name", str);
                    return;
                }
                throw new IllegalArgumentException("Argument \"name\" is marked as non-null but was passed a null value.");
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        }

        public ActionPrivateStorySettingsToEditStoryNameFragment setDistributionListId(DistributionListId distributionListId) {
            if (distributionListId != null) {
                this.arguments.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                return this;
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        }

        public ActionPrivateStorySettingsToEditStoryNameFragment setName(String str) {
            if (str != null) {
                this.arguments.put("name", str);
                return this;
            }
            throw new IllegalArgumentException("Argument \"name\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
                DistributionListId distributionListId = (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
                if (Parcelable.class.isAssignableFrom(DistributionListId.class) || distributionListId == null) {
                    bundle.putParcelable(RecipientDatabase.DISTRIBUTION_LIST_ID, (Parcelable) Parcelable.class.cast(distributionListId));
                } else if (Serializable.class.isAssignableFrom(DistributionListId.class)) {
                    bundle.putSerializable(RecipientDatabase.DISTRIBUTION_LIST_ID, (Serializable) Serializable.class.cast(distributionListId));
                } else {
                    throw new UnsupportedOperationException(DistributionListId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("name")) {
                bundle.putString("name", (String) this.arguments.get("name"));
            }
            return bundle;
        }

        public DistributionListId getDistributionListId() {
            return (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
        }

        public String getName() {
            return (String) this.arguments.get("name");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionPrivateStorySettingsToEditStoryNameFragment actionPrivateStorySettingsToEditStoryNameFragment = (ActionPrivateStorySettingsToEditStoryNameFragment) obj;
            if (this.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID) != actionPrivateStorySettingsToEditStoryNameFragment.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
                return false;
            }
            if (getDistributionListId() == null ? actionPrivateStorySettingsToEditStoryNameFragment.getDistributionListId() != null : !getDistributionListId().equals(actionPrivateStorySettingsToEditStoryNameFragment.getDistributionListId())) {
                return false;
            }
            if (this.arguments.containsKey("name") != actionPrivateStorySettingsToEditStoryNameFragment.arguments.containsKey("name")) {
                return false;
            }
            if (getName() == null ? actionPrivateStorySettingsToEditStoryNameFragment.getName() == null : getName().equals(actionPrivateStorySettingsToEditStoryNameFragment.getName())) {
                return getActionId() == actionPrivateStorySettingsToEditStoryNameFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((getDistributionListId() != null ? getDistributionListId().hashCode() : 0) + 31) * 31;
            if (getName() != null) {
                i = getName().hashCode();
            }
            return ((hashCode + i) * 31) + getActionId();
        }

        public String toString() {
            return "ActionPrivateStorySettingsToEditStoryNameFragment(actionId=" + getActionId() + "){distributionListId=" + getDistributionListId() + ", name=" + getName() + "}";
        }
    }

    /* loaded from: classes3.dex */
    public static class ActionPrivateStorySettingsToEditStoryViewers implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_privateStorySettings_to_editStoryViewers;
        }

        private ActionPrivateStorySettingsToEditStoryViewers(DistributionListId distributionListId) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (distributionListId != null) {
                hashMap.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                return;
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        }

        public ActionPrivateStorySettingsToEditStoryViewers setDistributionListId(DistributionListId distributionListId) {
            if (distributionListId != null) {
                this.arguments.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                return this;
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
                DistributionListId distributionListId = (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
                if (Parcelable.class.isAssignableFrom(DistributionListId.class) || distributionListId == null) {
                    bundle.putParcelable(RecipientDatabase.DISTRIBUTION_LIST_ID, (Parcelable) Parcelable.class.cast(distributionListId));
                } else if (Serializable.class.isAssignableFrom(DistributionListId.class)) {
                    bundle.putSerializable(RecipientDatabase.DISTRIBUTION_LIST_ID, (Serializable) Serializable.class.cast(distributionListId));
                } else {
                    throw new UnsupportedOperationException(DistributionListId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public DistributionListId getDistributionListId() {
            return (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionPrivateStorySettingsToEditStoryViewers actionPrivateStorySettingsToEditStoryViewers = (ActionPrivateStorySettingsToEditStoryViewers) obj;
            if (this.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID) != actionPrivateStorySettingsToEditStoryViewers.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
                return false;
            }
            if (getDistributionListId() == null ? actionPrivateStorySettingsToEditStoryViewers.getDistributionListId() == null : getDistributionListId().equals(actionPrivateStorySettingsToEditStoryViewers.getDistributionListId())) {
                return getActionId() == actionPrivateStorySettingsToEditStoryViewers.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getDistributionListId() != null ? getDistributionListId().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionPrivateStorySettingsToEditStoryViewers(actionId=" + getActionId() + "){distributionListId=" + getDistributionListId() + "}";
        }
    }
}
