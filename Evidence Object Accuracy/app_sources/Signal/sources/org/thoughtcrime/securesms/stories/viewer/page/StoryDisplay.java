package org.thoughtcrime.securesms.stories.viewer.page;

import android.content.res.Resources;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: StoryDisplay.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0001\u0018\u0000 \u00062\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0002\u0006\u0007B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryDisplay;", "", "(Ljava/lang/String;I)V", "LARGE", "MEDIUM", "SMALL", "Companion", "Size", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public enum StoryDisplay {
    LARGE,
    MEDIUM,
    SMALL;
    
    public static final Companion Companion = new Companion(null);
    private static final float LANDSCAPE;
    private static final float LARGE_AR;
    private static final float SMALL_AR;

    /* compiled from: StoryDisplay.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u0004J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryDisplay$Companion;", "", "()V", "LANDSCAPE", "", "LARGE_AR", "SMALL_AR", "getStoryDisplay", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryDisplay;", "screenWidth", "screenHeight", "getStorySize", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryDisplay$Size;", "resources", "Landroid/content/res/Resources;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {

        /* compiled from: StoryDisplay.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[StoryDisplay.values().length];
                iArr[StoryDisplay.LARGE.ordinal()] = 1;
                iArr[StoryDisplay.MEDIUM.ordinal()] = 2;
                iArr[StoryDisplay.SMALL.ordinal()] = 3;
                $EnumSwitchMapping$0 = iArr;
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final StoryDisplay getStoryDisplay(float f, float f2) {
            float f3 = f / f2;
            if (f3 >= StoryDisplay.LANDSCAPE) {
                return StoryDisplay.MEDIUM;
            }
            if (f3 <= StoryDisplay.LARGE_AR) {
                return StoryDisplay.LARGE;
            }
            if (f3 >= 0.5625f) {
                return StoryDisplay.SMALL;
            }
            return StoryDisplay.MEDIUM;
        }

        public final Size getStorySize(Resources resources) {
            Pair pair;
            Intrinsics.checkNotNullParameter(resources, "resources");
            float f = (float) resources.getDisplayMetrics().widthPixels;
            float f2 = (float) resources.getDisplayMetrics().heightPixels;
            int i = WhenMappings.$EnumSwitchMapping$0[getStoryDisplay(f, f2).ordinal()];
            if (i == 1) {
                pair = TuplesKt.to(Float.valueOf(f), Float.valueOf((f * ((float) 16)) / ((float) 9)));
            } else if (i == 2) {
                pair = TuplesKt.to(Float.valueOf(f), Float.valueOf((f * ((float) 16)) / ((float) 9)));
            } else if (i == 3) {
                pair = TuplesKt.to(Float.valueOf(f), Float.valueOf(f2));
            } else {
                throw new NoWhenBranchMatchedException();
            }
            return new Size((int) ((Number) pair.component1()).floatValue(), (int) ((Number) pair.component2()).floatValue());
        }
    }

    /* compiled from: StoryDisplay.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryDisplay$Size;", "", "width", "", "height", "(II)V", "getHeight", "()I", "getWidth", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Size {
        private final int height;
        private final int width;

        public static /* synthetic */ Size copy$default(Size size, int i, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = size.width;
            }
            if ((i3 & 2) != 0) {
                i2 = size.height;
            }
            return size.copy(i, i2);
        }

        public final int component1() {
            return this.width;
        }

        public final int component2() {
            return this.height;
        }

        public final Size copy(int i, int i2) {
            return new Size(i, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Size)) {
                return false;
            }
            Size size = (Size) obj;
            return this.width == size.width && this.height == size.height;
        }

        public int hashCode() {
            return (this.width * 31) + this.height;
        }

        public String toString() {
            return "Size(width=" + this.width + ", height=" + this.height + ')';
        }

        public Size(int i, int i2) {
            this.width = i;
            this.height = i2;
        }

        public final int getHeight() {
            return this.height;
        }

        public final int getWidth() {
            return this.width;
        }
    }
}
