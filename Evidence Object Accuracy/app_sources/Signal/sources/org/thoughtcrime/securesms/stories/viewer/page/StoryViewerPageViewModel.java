package org.thoughtcrime.securesms.stories.viewer.page;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.viewer.page.StoryPost;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerDialog;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: StoryViewerPageViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b/\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001yB7\u0012\u0006\u0010\"\u001a\u00020!\u0012\u0006\u0010L\u001a\u00020\u001f\u0012\u0006\u0010N\u001a\u00020\u001a\u0012\u0006\u0010P\u001a\u00020\u001a\u0012\u0006\u0010R\u001a\u00020Q\u0012\u0006\u0010U\u001a\u00020T¢\u0006\u0004\bw\u0010xJ\u0018\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002J\u0018\u0010\u000b\u001a\u0004\u0018\u00010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002J\u0006\u0010\f\u001a\u00020\u0002J\u0006\u0010\u000e\u001a\u00020\rJ\u0006\u0010\u000f\u001a\u00020\rJ\b\u0010\u0010\u001a\u00020\rH\u0014J\u0006\u0010\u0012\u001a\u00020\u0011J\u000e\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0013\u001a\u00020\tJ\u000e\u0010\u0015\u001a\u00020\r2\u0006\u0010\u0005\u001a\u00020\u0004J\u0006\u0010\u0016\u001a\u00020\rJ\u0006\u0010\u0017\u001a\u00020\rJ\u0006\u0010\u0018\u001a\u00020\u0004J\u0006\u0010\u0019\u001a\u00020\u0006J\u0006\u0010\u001b\u001a\u00020\u001aJ\b\u0010\u001c\u001a\u0004\u0018\u00010\tJ\u0006\u0010\u001d\u001a\u00020\tJ\u0006\u0010\u001e\u001a\u00020\rJ\u0016\u0010#\u001a\u00020\r2\u0006\u0010 \u001a\u00020\u001f2\u0006\u0010\"\u001a\u00020!J\u000e\u0010%\u001a\u00020\r2\u0006\u0010$\u001a\u00020\u001aJ\u000e\u0010'\u001a\u00020\r2\u0006\u0010&\u001a\u00020\u001aJ\u000e\u0010)\u001a\u00020\r2\u0006\u0010(\u001a\u00020\u001aJ\u000e\u0010+\u001a\u00020\r2\u0006\u0010*\u001a\u00020\u001aJ\u000e\u0010-\u001a\u00020\r2\u0006\u0010,\u001a\u00020\u001aJ\u000e\u0010/\u001a\u00020\r2\u0006\u0010.\u001a\u00020\u001aJ\u000e\u00101\u001a\u00020\r2\u0006\u00100\u001a\u00020\u001aJ\u000e\u00103\u001a\u00020\r2\u0006\u00102\u001a\u00020\u001aJ\u000e\u00105\u001a\u00020\r2\u0006\u00104\u001a\u00020\u001aJ\u000e\u00107\u001a\u00020\r2\u0006\u00106\u001a\u00020\u001aJ\u000e\u00109\u001a\u00020\r2\u0006\u00108\u001a\u00020\u001aJ\u000e\u0010;\u001a\u00020\r2\u0006\u0010:\u001a\u00020\u001aJ\u000e\u0010=\u001a\u00020\r2\u0006\u0010<\u001a\u00020\u001aJ\u000e\u0010?\u001a\u00020\r2\u0006\u0010>\u001a\u00020\u001aJ\u000e\u0010A\u001a\u00020\r2\u0006\u0010@\u001a\u00020\u001aJ\u000e\u0010C\u001a\u00020\r2\u0006\u0010B\u001a\u00020\u001aJ\u000e\u0010E\u001a\u00020\r2\u0006\u0010D\u001a\u00020\u001aJ\u000e\u0010G\u001a\u00020\r2\u0006\u0010F\u001a\u00020\u001aJ\u000e\u0010I\u001a\u00020\r2\u0006\u0010H\u001a\u00020\u001aJ\u0010\u0010J\u001a\u0004\u0018\u00010\t2\u0006\u0010\u0005\u001a\u00020\u0004R\u0014\u0010\"\u001a\u00020!8\u0002X\u0004¢\u0006\u0006\n\u0004\b\"\u0010KR\u0014\u0010L\u001a\u00020\u001f8\u0002X\u0004¢\u0006\u0006\n\u0004\bL\u0010MR\u0014\u0010N\u001a\u00020\u001a8\u0002X\u0004¢\u0006\u0006\n\u0004\bN\u0010OR\u0014\u0010P\u001a\u00020\u001a8\u0002X\u0004¢\u0006\u0006\n\u0004\bP\u0010OR\u0014\u0010R\u001a\u00020Q8\u0002X\u0004¢\u0006\u0006\n\u0004\bR\u0010SR\u0017\u0010U\u001a\u00020T8\u0006¢\u0006\f\n\u0004\bU\u0010V\u001a\u0004\bW\u0010XR\u001a\u0010Z\u001a\b\u0012\u0004\u0012\u00020\u00020Y8\u0002X\u0004¢\u0006\u0006\n\u0004\bZ\u0010[R\u0014\u0010]\u001a\u00020\\8\u0002X\u0004¢\u0006\u0006\n\u0004\b]\u0010^R \u0010b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020a0`0_8\u0002X\u0004¢\u0006\u0006\n\u0004\bb\u0010cR\u001a\u0010d\u001a\b\u0012\u0004\u0012\u00020\u001a0_8\u0002X\u0004¢\u0006\u0006\n\u0004\bd\u0010cR\u001a\u0010g\u001a\b\u0012\u0004\u0012\u00020f0e8\u0002X\u0004¢\u0006\u0006\n\u0004\bg\u0010hR\u001d\u0010j\u001a\b\u0012\u0004\u0012\u00020f0i8\u0006¢\u0006\f\n\u0004\bj\u0010k\u001a\u0004\bl\u0010mR#\u0010o\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020a0`0n8\u0006¢\u0006\f\n\u0004\bo\u0010p\u001a\u0004\bq\u0010rR\u001d\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020s8\u0006¢\u0006\f\n\u0004\b\u0003\u0010t\u001a\u0004\bu\u0010v¨\u0006z"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "Landroidx/lifecycle/ViewModel;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState;", "state", "", "index", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageState$ReplyState;", "resolveSwipeToReplyState", "", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost;", "list", "getNextUnreadPost", "getStateSnapshot", "", "checkReadReceiptState", "refresh", "onCleared", "Lio/reactivex/rxjava3/core/Completable;", "hideStory", "storyPost", "markViewed", "setSelectedPostIndex", "goToNextPost", "goToPreviousPost", "getRestartIndex", "getSwipeToReplyState", "", "hasPost", "getPost", "requirePost", "forceDownloadSelectedPost", "", "storyId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "recipientId", "startDirectReply", "isFragmentResumed", "setIsFragmentResumed", "isUserScrollingParent", "setIsUserScrollingParent", "isUserScrollingChild", "setIsUserScrollingChild", "isDisplayingSlate", "setIsDisplayingSlate", "isFirstPage", "setIsFirstPage", "isSelectedPage", "setIsSelectedPage", "isDisplayingReactionAnimation", "setIsDisplayingReactionAnimation", "isDisplayingContextMenu", "setIsDisplayingContextMenu", "isDisplayingForwardDialog", "setIsDisplayingForwardDialog", "isDisplayingDeleteDialog", "setIsDisplayingDeleteDialog", "isDisplayingViewsAndRepliesDialog", "setIsDisplayingViewsAndRepliesDialog", "isDisplayingDirectReplyDialog", "setIsDisplayingDirectReplyDialog", "isDisplayingCaptionOverlay", "setIsDisplayingCaptionOverlay", "isUserTouching", "setIsUserTouching", "areSegmentsInitialized", "setAreSegmentsInitialized", "isDisplayingLinkPreviewTooltip", "setIsDisplayingLinkPreviewTooltip", "isRunningSharedElementAnimation", "setIsRunningSharedElementAnimation", "isDisplayingFirstTimeNavigation", "setIsDisplayingFirstTimeNavigation", "isDisplayingInfoDialog", "setIsDisplayingInfoDialog", "getPostAt", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "initialStoryId", "J", "isUnviewedOnly", "Z", "isOutgoingOnly", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageRepository;", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageRepository;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache;", "storyCache", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache;", "getStoryCache", "()Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache;", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "Lio/reactivex/rxjava3/subjects/Subject;", "j$/util/Optional", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerDialog;", "storyViewerDialogSubject", "Lio/reactivex/rxjava3/subjects/Subject;", "storyLongPressSubject", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPlaybackState;", "storyViewerPlaybackStore", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Landroidx/lifecycle/LiveData;", "storyViewerPlaybackState", "Landroidx/lifecycle/LiveData;", "getStoryViewerPlaybackState", "()Landroidx/lifecycle/LiveData;", "Lio/reactivex/rxjava3/core/Observable;", "groupDirectReplyObservable", "Lio/reactivex/rxjava3/core/Observable;", "getGroupDirectReplyObservable", "()Lio/reactivex/rxjava3/core/Observable;", "Lio/reactivex/rxjava3/core/Flowable;", "Lio/reactivex/rxjava3/core/Flowable;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "<init>", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;JZZLorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageRepository;Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache;)V", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class StoryViewerPageViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final Observable<Optional<StoryViewerDialog>> groupDirectReplyObservable;
    private final long initialStoryId;
    private final boolean isOutgoingOnly;
    private final boolean isUnviewedOnly;
    private final RecipientId recipientId;
    private final StoryViewerPageRepository repository;
    private final Flowable<StoryViewerPageState> state;
    private final RxStore<StoryViewerPageState> store;
    private final StoryCache storyCache;
    private final Subject<Boolean> storyLongPressSubject;
    private final Subject<Optional<StoryViewerDialog>> storyViewerDialogSubject;
    private final LiveData<StoryViewerPlaybackState> storyViewerPlaybackState;
    private final Store<StoryViewerPlaybackState> storyViewerPlaybackStore;

    public final StoryCache getStoryCache() {
        return this.storyCache;
    }

    public StoryViewerPageViewModel(RecipientId recipientId, long j, boolean z, boolean z2, StoryViewerPageRepository storyViewerPageRepository, StoryCache storyCache) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(storyViewerPageRepository, "repository");
        Intrinsics.checkNotNullParameter(storyCache, "storyCache");
        this.recipientId = recipientId;
        this.initialStoryId = j;
        this.isUnviewedOnly = z;
        this.isOutgoingOnly = z2;
        this.repository = storyViewerPageRepository;
        this.storyCache = storyCache;
        RxStore<StoryViewerPageState> rxStore = new RxStore<>(new StoryViewerPageState(null, 0, null, false, false, false, storyViewerPageRepository.isReadReceiptsEnabled(), 63, null), null, 2, null);
        this.store = rxStore;
        PublishSubject create = PublishSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.storyViewerDialogSubject = create;
        PublishSubject create2 = PublishSubject.create();
        Intrinsics.checkNotNullExpressionValue(create2, "create()");
        this.storyLongPressSubject = create2;
        Store<StoryViewerPlaybackState> store = new Store<>(new StoryViewerPlaybackState(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 524287, null));
        this.storyViewerPlaybackStore = store;
        LiveData<StoryViewerPlaybackState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "storyViewerPlaybackStore.stateLiveData");
        this.storyViewerPlaybackState = stateLiveData;
        this.groupDirectReplyObservable = create;
        this.state = rxStore.getStateFlowable();
        refresh();
    }

    public final LiveData<StoryViewerPlaybackState> getStoryViewerPlaybackState() {
        return this.storyViewerPlaybackState;
    }

    public final Observable<Optional<StoryViewerDialog>> getGroupDirectReplyObservable() {
        return this.groupDirectReplyObservable;
    }

    public final Flowable<StoryViewerPageState> getState() {
        return this.state;
    }

    public final StoryViewerPageState getStateSnapshot() {
        return this.store.getState();
    }

    public final void checkReadReceiptState() {
        boolean isReceiptsEnabled = getStateSnapshot().isReceiptsEnabled();
        boolean isReadReceiptsEnabled = this.repository.isReadReceiptsEnabled();
        if (isReceiptsEnabled ^ isReadReceiptsEnabled) {
            this.store.update(new Function1<StoryViewerPageState, StoryViewerPageState>(isReadReceiptsEnabled) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$checkReadReceiptState$1
                final /* synthetic */ boolean $isReceiptsEnabledInRepository;

                /* access modifiers changed from: package-private */
                {
                    this.$isReceiptsEnabledInRepository = r1;
                }

                public final StoryViewerPageState invoke(StoryViewerPageState storyViewerPageState) {
                    Intrinsics.checkNotNullParameter(storyViewerPageState, "it");
                    return StoryViewerPageState.copy$default(storyViewerPageState, null, 0, null, false, false, false, this.$isReceiptsEnabledInRepository, 63, null);
                }
            });
        }
    }

    public final void refresh() {
        this.disposables.clear();
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.repository.getStoryPostsFor(this.recipientId, this.isUnviewedOnly, this.isOutgoingOnly).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda12
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryViewerPageViewModel.m3079refresh$lambda2(StoryViewerPageViewModel.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getStoryPosts…ttachment }\n      )\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
        CompositeDisposable compositeDisposable2 = this.disposables;
        Disposable subscribe2 = this.storyLongPressSubject.debounce(150, TimeUnit.MILLISECONDS).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda13
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryViewerPageViewModel.m3080refresh$lambda4(StoryViewerPageViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "storyLongPressSubject.de…ng = isLongPress) }\n    }");
        DisposableKt.plusAssign(compositeDisposable2, subscribe2);
    }

    /* renamed from: refresh$lambda-2 */
    public static final void m3079refresh$lambda2(StoryViewerPageViewModel storyViewerPageViewModel, List list) {
        Intrinsics.checkNotNullParameter(storyViewerPageViewModel, "this$0");
        storyViewerPageViewModel.store.update(new Function1<StoryViewerPageState, StoryViewerPageState>(list, storyViewerPageViewModel) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$refresh$1$1
            final /* synthetic */ List<StoryPost> $posts;
            final /* synthetic */ StoryViewerPageViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.$posts = r1;
                this.this$0 = r2;
            }

            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState invoke(org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState r15) {
                /*
                    r14 = this;
                    java.lang.String r0 = "state"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r15, r0)
                    java.util.List r0 = r15.getPosts()
                    boolean r0 = r0.isEmpty()
                    java.lang.String r1 = "posts"
                    r2 = 0
                    r3 = 1
                    if (r0 == 0) goto L_0x0021
                    java.util.List<org.thoughtcrime.securesms.stories.viewer.page.StoryPost> r0 = r14.$posts
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
                    boolean r0 = r0.isEmpty()
                    r0 = r0 ^ r3
                    if (r0 == 0) goto L_0x0021
                    r9 = 1
                    goto L_0x0022
                L_0x0021:
                    r9 = 0
                L_0x0022:
                    java.util.List r0 = r15.getPosts()
                    boolean r0 = r0.isEmpty()
                    r4 = 0
                    r5 = -1
                    if (r0 == 0) goto L_0x0082
                    org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel r0 = r14.this$0
                    long r6 = org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel.access$getInitialStoryId$p(r0)
                    r10 = 0
                    int r0 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
                    if (r0 <= 0) goto L_0x0082
                    java.util.List<org.thoughtcrime.securesms.stories.viewer.page.StoryPost> r0 = r14.$posts
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
                    org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel r6 = r14.this$0
                    java.util.Iterator r0 = r0.iterator()
                    r7 = 0
                L_0x0046:
                    boolean r8 = r0.hasNext()
                    if (r8 == 0) goto L_0x0067
                    java.lang.Object r8 = r0.next()
                    org.thoughtcrime.securesms.stories.viewer.page.StoryPost r8 = (org.thoughtcrime.securesms.stories.viewer.page.StoryPost) r8
                    long r10 = r8.getId()
                    long r12 = org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel.access$getInitialStoryId$p(r6)
                    int r8 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
                    if (r8 != 0) goto L_0x0060
                    r8 = 1
                    goto L_0x0061
                L_0x0060:
                    r8 = 0
                L_0x0061:
                    if (r8 == 0) goto L_0x0064
                    goto L_0x0068
                L_0x0064:
                    int r7 = r7 + 1
                    goto L_0x0046
                L_0x0067:
                    r7 = -1
                L_0x0068:
                    java.lang.Integer r0 = java.lang.Integer.valueOf(r7)
                    int r6 = r0.intValue()
                    if (r6 <= r5) goto L_0x0073
                    r2 = 1
                L_0x0073:
                    if (r2 == 0) goto L_0x0076
                    r4 = r0
                L_0x0076:
                    if (r4 == 0) goto L_0x007d
                    int r0 = r4.intValue()
                    goto L_0x00bf
                L_0x007d:
                    int r0 = r15.getSelectedPostIndex()
                    goto L_0x00bf
                L_0x0082:
                    java.util.List r0 = r15.getPosts()
                    boolean r0 = r0.isEmpty()
                    if (r0 == 0) goto L_0x00bb
                    org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel r0 = r14.this$0
                    java.util.List<org.thoughtcrime.securesms.stories.viewer.page.StoryPost> r6 = r14.$posts
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r6, r1)
                    org.thoughtcrime.securesms.stories.viewer.page.StoryPost r0 = org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel.access$getNextUnreadPost(r0, r6)
                    if (r0 == 0) goto L_0x00a0
                    java.util.List<org.thoughtcrime.securesms.stories.viewer.page.StoryPost> r6 = r14.$posts
                    int r0 = r6.indexOf(r0)
                    goto L_0x00a1
                L_0x00a0:
                    r0 = -1
                L_0x00a1:
                    java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                    int r6 = r0.intValue()
                    if (r6 <= r5) goto L_0x00ac
                    r2 = 1
                L_0x00ac:
                    if (r2 == 0) goto L_0x00af
                    r4 = r0
                L_0x00af:
                    if (r4 == 0) goto L_0x00b6
                    int r0 = r4.intValue()
                    goto L_0x00bf
                L_0x00b6:
                    int r0 = r15.getSelectedPostIndex()
                    goto L_0x00bf
                L_0x00bb:
                    int r0 = r15.getSelectedPostIndex()
                L_0x00bf:
                    r6 = r0
                    org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel r0 = r14.this$0
                    org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState$ReplyState r7 = org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel.access$resolveSwipeToReplyState(r0, r15, r6)
                    java.util.List<org.thoughtcrime.securesms.stories.viewer.page.StoryPost> r5 = r14.$posts
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r5, r1)
                    r8 = 0
                    r10 = 1
                    r11 = 0
                    r12 = 72
                    r13 = 0
                    r4 = r15
                    org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState r15 = org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState.copy$default(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
                    return r15
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$refresh$1$1.invoke(org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState):org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageState");
            }
        });
        StoryCache storyCache = storyViewerPageViewModel.storyCache;
        Intrinsics.checkNotNullExpressionValue(list, "posts");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((StoryPost) it.next()).getContent());
        }
        ArrayList<StoryPost.Content.AttachmentContent> arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (obj instanceof StoryPost.Content.AttachmentContent) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList2, 10));
        for (StoryPost.Content.AttachmentContent attachmentContent : arrayList2) {
            arrayList3.add(attachmentContent.getAttachment());
        }
        storyCache.prefetch(arrayList3);
    }

    /* renamed from: refresh$lambda-4 */
    public static final void m3080refresh$lambda4(StoryViewerPageViewModel storyViewerPageViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(storyViewerPageViewModel, "this$0");
        storyViewerPageViewModel.storyViewerPlaybackStore.update(new Function(bool) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda16
            public final /* synthetic */ Boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3081refresh$lambda4$lambda3(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-4$lambda-3 */
    public static final StoryViewerPlaybackState m3081refresh$lambda4$lambda3(Boolean bool, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        Intrinsics.checkNotNullExpressionValue(bool, "isLongPress");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : bool.booleanValue(), (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
        this.storyCache.clear();
    }

    public final Completable hideStory() {
        return this.repository.hideStory(this.recipientId);
    }

    public final void markViewed(StoryPost storyPost) {
        Intrinsics.checkNotNullParameter(storyPost, "storyPost");
        this.repository.markViewed(storyPost);
    }

    public final void setSelectedPostIndex(int i) {
        StoryPost postAt = getPostAt(i);
        if (!(postAt == null || postAt.getContent().getTransferState() == 0)) {
            CompositeDisposable compositeDisposable = this.disposables;
            Disposable subscribe = this.repository.forceDownload(postAt).subscribe();
            Intrinsics.checkNotNullExpressionValue(subscribe, "repository.forceDownload(selectedPost).subscribe()");
            DisposableKt.plusAssign(compositeDisposable, subscribe);
        }
        this.store.update(new Function1<StoryViewerPageState, StoryViewerPageState>(i, this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$setSelectedPostIndex$1
            final /* synthetic */ int $index;
            final /* synthetic */ StoryViewerPageViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.$index = r1;
                this.this$0 = r2;
            }

            public final StoryViewerPageState invoke(StoryViewerPageState storyViewerPageState) {
                Intrinsics.checkNotNullParameter(storyViewerPageState, "it");
                int i2 = this.$index;
                return StoryViewerPageState.copy$default(storyViewerPageState, null, i2, this.this$0.resolveSwipeToReplyState(storyViewerPageState, i2), false, false, false, false, 121, null);
            }
        });
    }

    public final void goToNextPost() {
        if (!this.store.getState().getPosts().isEmpty()) {
            int selectedPostIndex = this.store.getState().getSelectedPostIndex() + 1;
            StoryPost nextUnreadPost = getNextUnreadPost(CollectionsKt___CollectionsKt.drop(this.store.getState().getPosts(), selectedPostIndex));
            if (nextUnreadPost == null) {
                setSelectedPostIndex(selectedPostIndex);
            } else {
                setSelectedPostIndex(this.store.getState().getPosts().indexOf(nextUnreadPost));
            }
        }
    }

    public final void goToPreviousPost() {
        if (!this.store.getState().getPosts().isEmpty()) {
            setSelectedPostIndex(Math.max(this.store.getState().isFirstPage() ? 0 : -1, this.store.getState().getSelectedPostIndex() - 1));
        }
    }

    public final int getRestartIndex() {
        return Math.min(this.store.getState().getSelectedPostIndex(), CollectionsKt__CollectionsKt.getLastIndex(this.store.getState().getPosts()));
    }

    public final StoryViewerPageState.ReplyState getSwipeToReplyState() {
        return this.store.getState().getReplyState();
    }

    public final boolean hasPost() {
        int size = this.store.getState().getPosts().size();
        int selectedPostIndex = this.store.getState().getSelectedPostIndex();
        return selectedPostIndex >= 0 && selectedPostIndex < size;
    }

    public final StoryPost getPost() {
        if (hasPost()) {
            return this.store.getState().getPosts().get(this.store.getState().getSelectedPostIndex());
        }
        return null;
    }

    public final StoryPost requirePost() {
        StoryPost post = getPost();
        Intrinsics.checkNotNull(post);
        return post;
    }

    public final void forceDownloadSelectedPost() {
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.repository.forceDownload(requirePost()).subscribe();
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.forceDownload…equirePost()).subscribe()");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final void startDirectReply(long j, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.storyViewerDialogSubject.onNext(Optional.of(new StoryViewerDialog.GroupDirectReply(recipientId, j)));
    }

    /* renamed from: setIsFragmentResumed$lambda-5 */
    public static final StoryViewerPlaybackState m3094setIsFragmentResumed$lambda5(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : z, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsFragmentResumed(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda19
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3094setIsFragmentResumed$lambda5(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsUserScrollingParent$lambda-6 */
    public static final StoryViewerPlaybackState m3098setIsUserScrollingParent$lambda6(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : z, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsUserScrollingParent(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda11
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3098setIsUserScrollingParent$lambda6(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsUserScrollingChild$lambda-7 */
    public static final StoryViewerPlaybackState m3097setIsUserScrollingChild$lambda7(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : z);
    }

    public final void setIsUserScrollingChild(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda7
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3097setIsUserScrollingChild$lambda7(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingSlate$lambda-8 */
    public static final StoryViewerPlaybackState m3092setIsDisplayingSlate$lambda8(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : z, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingSlate(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda20
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3092setIsDisplayingSlate$lambda8(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    public final void setIsFirstPage(boolean z) {
        this.store.update(new Function1<StoryViewerPageState, StoryViewerPageState>(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$setIsFirstPage$1
            final /* synthetic */ boolean $isFirstPage;

            /* access modifiers changed from: package-private */
            {
                this.$isFirstPage = r1;
            }

            public final StoryViewerPageState invoke(StoryViewerPageState storyViewerPageState) {
                Intrinsics.checkNotNullParameter(storyViewerPageState, "it");
                return StoryViewerPageState.copy$default(storyViewerPageState, null, 0, null, this.$isFirstPage, false, false, false, 119, null);
            }
        });
    }

    /* renamed from: setIsSelectedPage$lambda-9 */
    public static final StoryViewerPlaybackState m3096setIsSelectedPage$lambda9(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : z, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsSelectedPage(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda4
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3096setIsSelectedPage$lambda9(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingReactionAnimation$lambda-10 */
    public static final StoryViewerPlaybackState m3091setIsDisplayingReactionAnimation$lambda10(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : z, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingReactionAnimation(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda9
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3091setIsDisplayingReactionAnimation$lambda10(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingContextMenu$lambda-11 */
    public static final StoryViewerPlaybackState m3084setIsDisplayingContextMenu$lambda11(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : z, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingContextMenu(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3084setIsDisplayingContextMenu$lambda11(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingForwardDialog$lambda-12 */
    public static final StoryViewerPlaybackState m3088setIsDisplayingForwardDialog$lambda12(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : z, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingForwardDialog(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3088setIsDisplayingForwardDialog$lambda12(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingDeleteDialog$lambda-13 */
    public static final StoryViewerPlaybackState m3085setIsDisplayingDeleteDialog$lambda13(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : z, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingDeleteDialog(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda18
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3085setIsDisplayingDeleteDialog$lambda13(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingViewsAndRepliesDialog$lambda-14 */
    public static final StoryViewerPlaybackState m3093setIsDisplayingViewsAndRepliesDialog$lambda14(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : z, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingViewsAndRepliesDialog(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3093setIsDisplayingViewsAndRepliesDialog$lambda14(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingDirectReplyDialog$lambda-15 */
    public static final StoryViewerPlaybackState m3086setIsDisplayingDirectReplyDialog$lambda15(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : z, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingDirectReplyDialog(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda8
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3086setIsDisplayingDirectReplyDialog$lambda15(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingCaptionOverlay$lambda-16 */
    public static final StoryViewerPlaybackState m3083setIsDisplayingCaptionOverlay$lambda16(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : z, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingCaptionOverlay(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda10
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3083setIsDisplayingCaptionOverlay$lambda16(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsUserTouching$lambda-17 */
    public static final StoryViewerPlaybackState m3099setIsUserTouching$lambda17(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : z, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsUserTouching(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda6
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3099setIsUserTouching$lambda17(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
        this.storyLongPressSubject.onNext(Boolean.valueOf(z));
    }

    /* renamed from: setAreSegmentsInitialized$lambda-18 */
    public static final StoryViewerPlaybackState m3082setAreSegmentsInitialized$lambda18(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : z, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setAreSegmentsInitialized(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda14
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3082setAreSegmentsInitialized$lambda18(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingLinkPreviewTooltip$lambda-19 */
    public static final StoryViewerPlaybackState m3090setIsDisplayingLinkPreviewTooltip$lambda19(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : z, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingLinkPreviewTooltip(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3090setIsDisplayingLinkPreviewTooltip$lambda19(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsRunningSharedElementAnimation$lambda-20 */
    public static final StoryViewerPlaybackState m3095setIsRunningSharedElementAnimation$lambda20(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : z, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsRunningSharedElementAnimation(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda15
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3095setIsRunningSharedElementAnimation$lambda20(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingFirstTimeNavigation$lambda-21 */
    public static final StoryViewerPlaybackState m3087setIsDisplayingFirstTimeNavigation$lambda21(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : z, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : false, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingFirstTimeNavigation(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3087setIsDisplayingFirstTimeNavigation$lambda21(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    /* renamed from: setIsDisplayingInfoDialog$lambda-22 */
    public static final StoryViewerPlaybackState m3089setIsDisplayingInfoDialog$lambda22(boolean z, StoryViewerPlaybackState storyViewerPlaybackState) {
        Intrinsics.checkNotNullExpressionValue(storyViewerPlaybackState, "it");
        return storyViewerPlaybackState.copy((r37 & 1) != 0 ? storyViewerPlaybackState.areSegmentsInitialized : false, (r37 & 2) != 0 ? storyViewerPlaybackState.isUserTouching : false, (r37 & 4) != 0 ? storyViewerPlaybackState.isDisplayingForwardDialog : false, (r37 & 8) != 0 ? storyViewerPlaybackState.isDisplayingDeleteDialog : false, (r37 & 16) != 0 ? storyViewerPlaybackState.isDisplayingContextMenu : false, (r37 & 32) != 0 ? storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog : false, (r37 & 64) != 0 ? storyViewerPlaybackState.isDisplayingDirectReplyDialog : false, (r37 & 128) != 0 ? storyViewerPlaybackState.isDisplayingCaptionOverlay : false, (r37 & 256) != 0 ? storyViewerPlaybackState.isUserScrollingParent : false, (r37 & 512) != 0 ? storyViewerPlaybackState.isSelectedPage : false, (r37 & 1024) != 0 ? storyViewerPlaybackState.isDisplayingSlate : false, (r37 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? storyViewerPlaybackState.isFragmentResumed : false, (r37 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? storyViewerPlaybackState.isDisplayingLinkPreviewTooltip : false, (r37 & 8192) != 0 ? storyViewerPlaybackState.isDisplayingReactionAnimation : false, (r37 & 16384) != 0 ? storyViewerPlaybackState.isRunningSharedElementAnimation : false, (r37 & 32768) != 0 ? storyViewerPlaybackState.isDisplayingFirstTimeNavigation : false, (r37 & 65536) != 0 ? storyViewerPlaybackState.isDisplayingInfoDialog : z, (r37 & 131072) != 0 ? storyViewerPlaybackState.isUserLongTouching : false, (r37 & 262144) != 0 ? storyViewerPlaybackState.isUserScrollingChild : false);
    }

    public final void setIsDisplayingInfoDialog(boolean z) {
        this.storyViewerPlaybackStore.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel$$ExternalSyntheticLambda17
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryViewerPageViewModel.m3089setIsDisplayingInfoDialog$lambda22(this.f$0, (StoryViewerPlaybackState) obj);
            }
        });
    }

    public final StoryViewerPageState.ReplyState resolveSwipeToReplyState(StoryViewerPageState storyViewerPageState, int i) {
        boolean z = true;
        if (!(i >= 0 && i < storyViewerPageState.getPosts().size())) {
            return StoryViewerPageState.ReplyState.NONE;
        }
        StoryPost storyPost = storyViewerPageState.getPosts().get(i);
        boolean isSelf = storyPost.getSender().isSelf();
        if (storyPost.getGroup() == null) {
            z = false;
        }
        if (storyPost.getAllowsReplies()) {
            return StoryViewerPageState.ReplyState.Companion.resolve(isSelf, z);
        }
        if (isSelf) {
            return StoryViewerPageState.ReplyState.SELF;
        }
        return StoryViewerPageState.ReplyState.NONE;
    }

    public final StoryPost getPostAt(int i) {
        return (StoryPost) CollectionsKt___CollectionsKt.getOrNull(this.store.getState().getPosts(), i);
    }

    /* compiled from: StoryViewerPageViewModel.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ%\u0010\u000e\u001a\u0002H\u000f\"\b\b\u0000\u0010\u000f*\u00020\u00102\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u0002H\u000f0\u0012H\u0016¢\u0006\u0002\u0010\u0013R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "initialStoryId", "", "isUnviewedOnly", "", "isOutgoingOnly", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageRepository;", "storyCache", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;JZZLorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageRepository;Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final long initialStoryId;
        private final boolean isOutgoingOnly;
        private final boolean isUnviewedOnly;
        private final RecipientId recipientId;
        private final StoryViewerPageRepository repository;
        private final StoryCache storyCache;

        public Factory(RecipientId recipientId, long j, boolean z, boolean z2, StoryViewerPageRepository storyViewerPageRepository, StoryCache storyCache) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(storyViewerPageRepository, "repository");
            Intrinsics.checkNotNullParameter(storyCache, "storyCache");
            this.recipientId = recipientId;
            this.initialStoryId = j;
            this.isUnviewedOnly = z;
            this.isOutgoingOnly = z2;
            this.repository = storyViewerPageRepository;
            this.storyCache = storyCache;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StoryViewerPageViewModel(this.recipientId, this.initialStoryId, this.isUnviewedOnly, this.isOutgoingOnly, this.repository, this.storyCache));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel.Factory.create");
        }
    }

    public final StoryPost getNextUnreadPost(List<StoryPost> list) {
        Object obj;
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (!((StoryPost) obj).getHasSelfViewed()) {
                break;
            }
        }
        return (StoryPost) obj;
    }
}
