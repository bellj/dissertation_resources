package org.thoughtcrime.securesms.stories.settings.custom;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;

/* loaded from: classes3.dex */
public class PrivateStorySettingsFragmentArgs {
    private final HashMap arguments;

    private PrivateStorySettingsFragmentArgs() {
        this.arguments = new HashMap();
    }

    private PrivateStorySettingsFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static PrivateStorySettingsFragmentArgs fromBundle(Bundle bundle) {
        PrivateStorySettingsFragmentArgs privateStorySettingsFragmentArgs = new PrivateStorySettingsFragmentArgs();
        bundle.setClassLoader(PrivateStorySettingsFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
            throw new IllegalArgumentException("Required argument \"distribution_list_id\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(DistributionListId.class) || Serializable.class.isAssignableFrom(DistributionListId.class)) {
            DistributionListId distributionListId = (DistributionListId) bundle.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
            if (distributionListId != null) {
                privateStorySettingsFragmentArgs.arguments.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                return privateStorySettingsFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(DistributionListId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public DistributionListId getDistributionListId() {
        return (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
            DistributionListId distributionListId = (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
            if (Parcelable.class.isAssignableFrom(DistributionListId.class) || distributionListId == null) {
                bundle.putParcelable(RecipientDatabase.DISTRIBUTION_LIST_ID, (Parcelable) Parcelable.class.cast(distributionListId));
            } else if (Serializable.class.isAssignableFrom(DistributionListId.class)) {
                bundle.putSerializable(RecipientDatabase.DISTRIBUTION_LIST_ID, (Serializable) Serializable.class.cast(distributionListId));
            } else {
                throw new UnsupportedOperationException(DistributionListId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PrivateStorySettingsFragmentArgs privateStorySettingsFragmentArgs = (PrivateStorySettingsFragmentArgs) obj;
        if (this.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID) != privateStorySettingsFragmentArgs.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
            return false;
        }
        return getDistributionListId() == null ? privateStorySettingsFragmentArgs.getDistributionListId() == null : getDistributionListId().equals(privateStorySettingsFragmentArgs.getDistributionListId());
    }

    public int hashCode() {
        return 31 + (getDistributionListId() != null ? getDistributionListId().hashCode() : 0);
    }

    public String toString() {
        return "PrivateStorySettingsFragmentArgs{distributionListId=" + getDistributionListId() + "}";
    }

    /* loaded from: classes3.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(PrivateStorySettingsFragmentArgs privateStorySettingsFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(privateStorySettingsFragmentArgs.arguments);
        }

        public Builder(DistributionListId distributionListId) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (distributionListId != null) {
                hashMap.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                return;
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        }

        public PrivateStorySettingsFragmentArgs build() {
            return new PrivateStorySettingsFragmentArgs(this.arguments);
        }

        public Builder setDistributionListId(DistributionListId distributionListId) {
            if (distributionListId != null) {
                this.arguments.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                return this;
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        }

        public DistributionListId getDistributionListId() {
            return (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
        }
    }
}
