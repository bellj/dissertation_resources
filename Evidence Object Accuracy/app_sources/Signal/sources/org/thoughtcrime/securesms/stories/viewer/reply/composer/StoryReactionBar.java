package org.thoughtcrime.securesms.stories.viewer.reply.composer;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiImageView;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: StoryReactionBar.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0018B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0012\u001a\u00020\u0013H\u0007J\u0010\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0013H\u0002R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReactionBar;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "animatorSet", "Landroid/animation/AnimatorSet;", "callback", "Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReactionBar$Callback;", "getCallback", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReactionBar$Callback;", "setCallback", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReactionBar$Callback;)V", "emojiViews", "", "Lorg/thoughtcrime/securesms/components/emoji/EmojiImageView;", "animateIn", "", "onEmojiSelected", "emoji", "", "onOpenReactionPicker", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryReactionBar extends ConstraintLayout {
    private AnimatorSet animatorSet;
    private Callback callback;
    private final List<EmojiImageView> emojiViews;

    /* compiled from: StoryReactionBar.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\b\u0010\u0007\u001a\u00020\u0003H&¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReactionBar$Callback;", "", "onOpenReactionPicker", "", "onReactionSelected", "emoji", "", "onTouchOutsideOfReactionBar", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onOpenReactionPicker();

        void onReactionSelected(String str);

        void onTouchOutsideOfReactionBar();
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryReactionBar(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ StoryReactionBar(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryReactionBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        ViewGroup.inflate(context, R.layout.stories_reaction_bar, this);
        setAlpha(0.0f);
        setBackgroundResource(R.drawable.conversation_reaction_overlay_background);
        int i = 0;
        List<EmojiImageView> list = CollectionsKt__CollectionsKt.listOf((Object[]) new EmojiImageView[]{(EmojiImageView) findViewById(R.id.reaction_1), (EmojiImageView) findViewById(R.id.reaction_2), (EmojiImageView) findViewById(R.id.reaction_3), (EmojiImageView) findViewById(R.id.reaction_4), (EmojiImageView) findViewById(R.id.reaction_5), (EmojiImageView) findViewById(R.id.reaction_6), (EmojiImageView) findViewById(R.id.reaction_7)});
        this.emojiViews = list;
        if (!isInEditMode()) {
            List<String> reactions = SignalStore.emojiValues().getReactions();
            Intrinsics.checkNotNullExpressionValue(reactions, "emojiValues().reactions");
            for (Object obj : list) {
                int i2 = i + 1;
                if (i < 0) {
                    CollectionsKt__CollectionsKt.throwIndexOverflow();
                }
                EmojiImageView emojiImageView = (EmojiImageView) obj;
                if (i == CollectionsKt__CollectionsKt.getLastIndex(this.emojiViews)) {
                    emojiImageView.setImageResource(R.drawable.ic_any_emoji_32);
                    emojiImageView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar$$ExternalSyntheticLambda0
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            StoryReactionBar.m3100$r8$lambda$5mGf6lntsT5AbEC2nWXOjwfZGw(StoryReactionBar.this, view);
                        }
                    });
                } else {
                    String preferredVariation = SignalStore.emojiValues().getPreferredVariation(reactions.get(i));
                    Intrinsics.checkNotNullExpressionValue(preferredVariation, "emojiValues().getPreferredVariation(emojis[index])");
                    emojiImageView.setImageEmoji(preferredVariation);
                    emojiImageView.setOnClickListener(new View.OnClickListener(preferredVariation) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar$$ExternalSyntheticLambda1
                        public final /* synthetic */ String f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            StoryReactionBar.$r8$lambda$1y8ufTcDWZCUusZIPBTdOrS6KPI(StoryReactionBar.this, this.f$1, view);
                        }
                    });
                }
                i = i2;
            }
        }
        setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoryReactionBar.$r8$lambda$nVnNW7tdgjN9LpDt7ZVd_pUdLpE(StoryReactionBar.this, view);
            }
        });
    }

    public final Callback getCallback() {
        return this.callback;
    }

    public final void setCallback(Callback callback) {
        this.callback = callback;
    }

    /* renamed from: lambda-2$lambda-0 */
    public static final void m3102lambda2$lambda0(StoryReactionBar storyReactionBar, View view) {
        Intrinsics.checkNotNullParameter(storyReactionBar, "this$0");
        storyReactionBar.onOpenReactionPicker();
    }

    /* renamed from: lambda-2$lambda-1 */
    public static final void m3103lambda2$lambda1(StoryReactionBar storyReactionBar, String str, View view) {
        Intrinsics.checkNotNullParameter(storyReactionBar, "this$0");
        Intrinsics.checkNotNullParameter(str, "$emoji");
        storyReactionBar.onEmojiSelected(str);
    }

    /* renamed from: _init_$lambda-3 */
    public static final void m3101_init_$lambda3(StoryReactionBar storyReactionBar, View view) {
        Intrinsics.checkNotNullParameter(storyReactionBar, "this$0");
        Callback callback = storyReactionBar.callback;
        if (callback != null) {
            callback.onTouchOutsideOfReactionBar();
        }
    }

    public final void animateIn() {
        ViewExtensionsKt.setVisible(this, true);
        AnimatorSet animatorSet = this.animatorSet;
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        AnimatorSet animatorSet2 = new AnimatorSet();
        List<EmojiImageView> list = this.emojiViews;
        ArrayList arrayList = new ArrayList();
        for (EmojiImageView emojiImageView : list) {
            boolean unused = CollectionsKt__MutableCollectionsKt.addAll(arrayList, CollectionsKt__CollectionsKt.listOf((Object[]) new ObjectAnimator[]{ObjectAnimator.ofFloat(emojiImageView, View.ALPHA, 1.0f), ObjectAnimator.ofFloat(emojiImageView, View.TRANSLATION_Y, 0.0f)}));
        }
        animatorSet2.playTogether(CollectionsKt___CollectionsKt.plus((Collection<? extends ObjectAnimator>) ((Collection<? extends Object>) arrayList), ObjectAnimator.ofFloat(this, View.ALPHA, 1.0f)));
        animatorSet2.start();
        this.animatorSet = animatorSet2;
    }

    private final void onEmojiSelected(String str) {
        Callback callback = this.callback;
        if (callback != null) {
            callback.onReactionSelected(str);
        }
    }

    private final void onOpenReactionPicker() {
        Callback callback = this.callback;
        if (callback != null) {
            callback.onOpenReactionPicker();
        }
    }
}
