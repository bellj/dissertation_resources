package org.thoughtcrime.securesms.stories.viewer.reply.direct;

import android.content.Context;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableEmitter;
import io.reactivex.rxjava3.core.CompletableOnSubscribe;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.MessageSender;

/* compiled from: StoryDirectReplyRepository.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\t\u001a\u00020\nJ(\u0010\u000b\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\n2\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012R\u0016\u0010\u0002\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "kotlin.jvm.PlatformType", "getStoryPost", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "storyId", "", "send", "Lio/reactivex/rxjava3/core/Completable;", "groupDirectReplyRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "charSequence", "", "isReaction", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryDirectReplyRepository {
    private final Context context;

    public StoryDirectReplyRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context.getApplicationContext();
    }

    public final Single<MessageRecord> getStoryPost(long j) {
        Single<MessageRecord> subscribeOn = Single.fromCallable(new Callable(j) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return StoryDirectReplyRepository.m3118$r8$lambda$9afNbY4ryuFbAUz7ybnnbQNjpc(this.f$0);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      Sig…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getStoryPost$lambda-0 */
    public static final MessageRecord m3120getStoryPost$lambda0(long j) {
        return SignalDatabase.Companion.mms().getMessageRecord(j);
    }

    public final Completable send(long j, RecipientId recipientId, CharSequence charSequence, boolean z) {
        Intrinsics.checkNotNullParameter(charSequence, "charSequence");
        Completable subscribeOn = Completable.create(new CompletableOnSubscribe(j, recipientId, this, charSequence, z) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ long f$0;
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ StoryDirectReplyRepository f$2;
            public final /* synthetic */ CharSequence f$3;
            public final /* synthetic */ boolean f$4;

            {
                this.f$0 = r1;
                this.f$1 = r3;
                this.f$2 = r4;
                this.f$3 = r5;
                this.f$4 = r6;
            }

            @Override // io.reactivex.rxjava3.core.CompletableOnSubscribe
            public final void subscribe(CompletableEmitter completableEmitter) {
                StoryDirectReplyRepository.$r8$lambda$M_aijRZgj6tc_cogGX51_WNSgKs(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, completableEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create { emitter ->\n    …scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: send$lambda-2 */
    public static final void m3121send$lambda2(long j, RecipientId recipientId, StoryDirectReplyRepository storyDirectReplyRepository, CharSequence charSequence, boolean z, CompletableEmitter completableEmitter) {
        Pair pair;
        Recipient recipient;
        Intrinsics.checkNotNullParameter(storyDirectReplyRepository, "this$0");
        Intrinsics.checkNotNullParameter(charSequence, "$charSequence");
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        MessageRecord messageRecord = companion.mms().getMessageRecord(j);
        if (messageRecord != null) {
            MediaMmsMessageRecord mediaMmsMessageRecord = (MediaMmsMessageRecord) messageRecord;
            if (recipientId == null) {
                pair = TuplesKt.to(mediaMmsMessageRecord.getRecipient(), Long.valueOf(mediaMmsMessageRecord.getThreadId()));
            } else {
                Recipient resolved = Recipient.resolved(recipientId);
                Intrinsics.checkNotNullExpressionValue(resolved, "resolved(groupDirectReplyRecipientId)");
                pair = TuplesKt.to(resolved, Long.valueOf(companion.threads().getOrCreateThreadIdFor(resolved)));
            }
            Recipient recipient2 = (Recipient) pair.component1();
            long longValue = ((Number) pair.component2()).longValue();
            if (mediaMmsMessageRecord.isOutgoing()) {
                recipient = Recipient.self();
                Intrinsics.checkNotNullExpressionValue(recipient, "self()");
            } else {
                recipient = mediaMmsMessageRecord.getIndividualRecipient();
                Intrinsics.checkNotNullExpressionValue(recipient, "message.individualRecipient");
            }
            MessageSender.send(storyDirectReplyRepository.context, new OutgoingMediaMessage(recipient2, charSequence.toString(), CollectionsKt__CollectionsKt.emptyList(), System.currentTimeMillis(), 0, TimeUnit.SECONDS.toMillis((long) recipient2.getExpiresInSeconds()), false, 0, StoryType.NONE, new ParentStoryId.DirectReply(j), z, new QuoteModel(mediaMmsMessageRecord.getDateSent(), recipient.getId(), mediaMmsMessageRecord.getBody(), false, mediaMmsMessageRecord.getSlideDeck().asAttachments(), null, QuoteModel.Type.NORMAL), CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList(), SetsKt__SetsKt.emptySet(), SetsKt__SetsKt.emptySet(), null), longValue, false, (String) null, (MessageDatabase.InsertListener) new MessageDatabase.InsertListener() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyRepository$$ExternalSyntheticLambda2
                @Override // org.thoughtcrime.securesms.database.MessageDatabase.InsertListener
                public final void onComplete() {
                    StoryDirectReplyRepository.m3119$r8$lambda$PihjHu2lh6KS46Vp_eeQkDKEg(CompletableEmitter.this);
                }
            });
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord");
    }

    /* renamed from: send$lambda-2$lambda-1 */
    public static final void m3122send$lambda2$lambda1(CompletableEmitter completableEmitter) {
        completableEmitter.onComplete();
    }
}
