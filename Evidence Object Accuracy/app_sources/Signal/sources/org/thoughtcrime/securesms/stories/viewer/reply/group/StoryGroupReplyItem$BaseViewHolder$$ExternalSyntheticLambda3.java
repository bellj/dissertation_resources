package org.thoughtcrime.securesms.stories.viewer.reply.group;

import android.view.View;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda3 implements View.OnLongClickListener {
    public final /* synthetic */ StoryGroupReplyItem.BaseViewHolder f$0;
    public final /* synthetic */ StoryGroupReplyItem.Model f$1;

    public /* synthetic */ StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda3(StoryGroupReplyItem.BaseViewHolder baseViewHolder, StoryGroupReplyItem.Model model) {
        this.f$0 = baseViewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return StoryGroupReplyItem.BaseViewHolder.m3147bind$lambda0(this.f$0, this.f$1, view);
    }
}
