package org.thoughtcrime.securesms.stories.landing;

import android.view.View;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.view.AvatarView;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.stories.landing.MyStoriesItem;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: MyStoriesItem.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/MyStoriesItem;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStoriesItem {
    public static final MyStoriesItem INSTANCE = new MyStoriesItem();

    private MyStoriesItem() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.landing.MyStoriesItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new MyStoriesItem.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_landing_item_my_stories));
    }

    /* compiled from: MyStoriesItem.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B!\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0006J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0000H\u0016R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\b¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/MyStoriesItem$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "onClick", "Lkotlin/Function0;", "", "onClickThumbnail", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "getOnClickThumbnail", "areItemsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final Function0<Unit> onClick;
        private final Function0<Unit> onClickThumbnail;

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return true;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        public final Function0<Unit> getOnClickThumbnail() {
            return this.onClickThumbnail;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(Function0<Unit> function0, Function0<Unit> function02) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(function0, "onClick");
            Intrinsics.checkNotNullParameter(function02, "onClickThumbnail");
            this.onClick = function0;
            this.onClickThumbnail = function02;
        }
    }

    /* compiled from: MyStoriesItem.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/MyStoriesItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/landing/MyStoriesItem$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "avatarView", "Lorg/thoughtcrime/securesms/avatar/view/AvatarView;", "thumbnail", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final AvatarView avatarView;
        private final View thumbnail;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.story);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.story)");
            this.thumbnail = findViewById;
            View findViewById2 = view.findViewById(R.id.avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.avatar)");
            this.avatarView = (AvatarView) findViewById2;
        }

        /* renamed from: bind$lambda-0 */
        public static final void m2790bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClick().invoke();
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.itemView.setOnClickListener(new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda0(model));
            this.thumbnail.setOnClickListener(new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda1(model));
            this.avatarView.setOnClickListener(new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda2(model));
            AvatarView avatarView = this.avatarView;
            Recipient self = Recipient.self();
            Intrinsics.checkNotNullExpressionValue(self, "self()");
            avatarView.displayProfileAvatar(self);
        }

        /* renamed from: bind$lambda-1 */
        public static final void m2791bind$lambda1(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClickThumbnail().invoke();
        }

        /* renamed from: bind$lambda-2 */
        public static final void m2792bind$lambda2(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClickThumbnail().invoke();
        }
    }
}
