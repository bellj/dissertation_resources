package org.thoughtcrime.securesms.stories.viewer;

import android.net.Uri;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.StoryTextPostModel;

/* compiled from: StoryViewerState.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u001a\n\u0002\u0010\u000e\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001:\u0003+,-BQ\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u000f¢\u0006\u0002\u0010\u0010J\u000f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0006HÆ\u0003J\t\u0010 \u001a\u00020\u0006HÆ\u0003J\t\u0010!\u001a\u00020\tHÆ\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\u000bHÆ\u0003J\t\u0010#\u001a\u00020\rHÆ\u0003J\t\u0010$\u001a\u00020\u000fHÆ\u0003JW\u0010%\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000fHÆ\u0001J\u0013\u0010&\u001a\u00020\u000f2\b\u0010'\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010(\u001a\u00020\u0006HÖ\u0001J\t\u0010)\u001a\u00020*HÖ\u0001R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0018R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001d¨\u0006."}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState;", "", "pages", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "previousPage", "", "page", "crossfadeSource", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource;", "crossfadeTarget", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeTarget;", "loadState", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$LoadState;", "skipCrossfade", "", "(Ljava/util/List;IILorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource;Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeTarget;Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$LoadState;Z)V", "getCrossfadeSource", "()Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource;", "getCrossfadeTarget", "()Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeTarget;", "getLoadState", "()Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$LoadState;", "getPage", "()I", "getPages", "()Ljava/util/List;", "getPreviousPage", "getSkipCrossfade", "()Z", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "hashCode", "toString", "", "CrossfadeSource", "CrossfadeTarget", "LoadState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerState {
    private final CrossfadeSource crossfadeSource;
    private final CrossfadeTarget crossfadeTarget;
    private final LoadState loadState;
    private final int page;
    private final List<RecipientId> pages;
    private final int previousPage;
    private final boolean skipCrossfade;

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: org.thoughtcrime.securesms.stories.viewer.StoryViewerState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ StoryViewerState copy$default(StoryViewerState storyViewerState, List list, int i, int i2, CrossfadeSource crossfadeSource, CrossfadeTarget crossfadeTarget, LoadState loadState, boolean z, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            list = storyViewerState.pages;
        }
        if ((i3 & 2) != 0) {
            i = storyViewerState.previousPage;
        }
        if ((i3 & 4) != 0) {
            i2 = storyViewerState.page;
        }
        if ((i3 & 8) != 0) {
            crossfadeSource = storyViewerState.crossfadeSource;
        }
        if ((i3 & 16) != 0) {
            crossfadeTarget = storyViewerState.crossfadeTarget;
        }
        if ((i3 & 32) != 0) {
            loadState = storyViewerState.loadState;
        }
        if ((i3 & 64) != 0) {
            z = storyViewerState.skipCrossfade;
        }
        return storyViewerState.copy(list, i, i2, crossfadeSource, crossfadeTarget, loadState, z);
    }

    public final List<RecipientId> component1() {
        return this.pages;
    }

    public final int component2() {
        return this.previousPage;
    }

    public final int component3() {
        return this.page;
    }

    public final CrossfadeSource component4() {
        return this.crossfadeSource;
    }

    public final CrossfadeTarget component5() {
        return this.crossfadeTarget;
    }

    public final LoadState component6() {
        return this.loadState;
    }

    public final boolean component7() {
        return this.skipCrossfade;
    }

    public final StoryViewerState copy(List<? extends RecipientId> list, int i, int i2, CrossfadeSource crossfadeSource, CrossfadeTarget crossfadeTarget, LoadState loadState, boolean z) {
        Intrinsics.checkNotNullParameter(list, "pages");
        Intrinsics.checkNotNullParameter(crossfadeSource, "crossfadeSource");
        Intrinsics.checkNotNullParameter(loadState, "loadState");
        return new StoryViewerState(list, i, i2, crossfadeSource, crossfadeTarget, loadState, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryViewerState)) {
            return false;
        }
        StoryViewerState storyViewerState = (StoryViewerState) obj;
        return Intrinsics.areEqual(this.pages, storyViewerState.pages) && this.previousPage == storyViewerState.previousPage && this.page == storyViewerState.page && Intrinsics.areEqual(this.crossfadeSource, storyViewerState.crossfadeSource) && Intrinsics.areEqual(this.crossfadeTarget, storyViewerState.crossfadeTarget) && Intrinsics.areEqual(this.loadState, storyViewerState.loadState) && this.skipCrossfade == storyViewerState.skipCrossfade;
    }

    public int hashCode() {
        int hashCode = ((((((this.pages.hashCode() * 31) + this.previousPage) * 31) + this.page) * 31) + this.crossfadeSource.hashCode()) * 31;
        CrossfadeTarget crossfadeTarget = this.crossfadeTarget;
        int hashCode2 = (((hashCode + (crossfadeTarget == null ? 0 : crossfadeTarget.hashCode())) * 31) + this.loadState.hashCode()) * 31;
        boolean z = this.skipCrossfade;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode2 + i;
    }

    public String toString() {
        return "StoryViewerState(pages=" + this.pages + ", previousPage=" + this.previousPage + ", page=" + this.page + ", crossfadeSource=" + this.crossfadeSource + ", crossfadeTarget=" + this.crossfadeTarget + ", loadState=" + this.loadState + ", skipCrossfade=" + this.skipCrossfade + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
    /* JADX WARN: Multi-variable type inference failed */
    public StoryViewerState(List<? extends RecipientId> list, int i, int i2, CrossfadeSource crossfadeSource, CrossfadeTarget crossfadeTarget, LoadState loadState, boolean z) {
        Intrinsics.checkNotNullParameter(list, "pages");
        Intrinsics.checkNotNullParameter(crossfadeSource, "crossfadeSource");
        Intrinsics.checkNotNullParameter(loadState, "loadState");
        this.pages = list;
        this.previousPage = i;
        this.page = i2;
        this.crossfadeSource = crossfadeSource;
        this.crossfadeTarget = crossfadeTarget;
        this.loadState = loadState;
        this.skipCrossfade = z;
    }

    public /* synthetic */ StoryViewerState(List list, int i, int i2, CrossfadeSource crossfadeSource, CrossfadeTarget crossfadeTarget, LoadState loadState, boolean z, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this((i3 & 1) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i3 & 2) != 0 ? -1 : i, (i3 & 4) != 0 ? -1 : i2, crossfadeSource, (i3 & 16) != 0 ? null : crossfadeTarget, (i3 & 32) != 0 ? new LoadState(false, false, 3, null) : loadState, (i3 & 64) != 0 ? false : z);
    }

    public final List<RecipientId> getPages() {
        return this.pages;
    }

    public final int getPreviousPage() {
        return this.previousPage;
    }

    public final int getPage() {
        return this.page;
    }

    public final CrossfadeSource getCrossfadeSource() {
        return this.crossfadeSource;
    }

    public final CrossfadeTarget getCrossfadeTarget() {
        return this.crossfadeTarget;
    }

    public final LoadState getLoadState() {
        return this.loadState;
    }

    public final boolean getSkipCrossfade() {
        return this.skipCrossfade;
    }

    /* compiled from: StoryViewerState.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource;", "", "()V", "ImageUri", "None", "TextModel", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource$None;", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource$ImageUri;", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource$TextModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static abstract class CrossfadeSource {
        public /* synthetic */ CrossfadeSource(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: StoryViewerState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource$None;", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class None extends CrossfadeSource {
            public static final None INSTANCE = new None();

            private None() {
                super(null);
            }
        }

        private CrossfadeSource() {
        }

        /* compiled from: StoryViewerState.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource$ImageUri;", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource;", "imageUri", "Landroid/net/Uri;", "imageBlur", "Lorg/thoughtcrime/securesms/blurhash/BlurHash;", "(Landroid/net/Uri;Lorg/thoughtcrime/securesms/blurhash/BlurHash;)V", "getImageBlur", "()Lorg/thoughtcrime/securesms/blurhash/BlurHash;", "getImageUri", "()Landroid/net/Uri;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class ImageUri extends CrossfadeSource {
            private final BlurHash imageBlur;
            private final Uri imageUri;

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public ImageUri(Uri uri, BlurHash blurHash) {
                super(null);
                Intrinsics.checkNotNullParameter(uri, "imageUri");
                this.imageUri = uri;
                this.imageBlur = blurHash;
            }

            public final BlurHash getImageBlur() {
                return this.imageBlur;
            }

            public final Uri getImageUri() {
                return this.imageUri;
            }
        }

        /* compiled from: StoryViewerState.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource$TextModel;", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeSource;", "storyTextPostModel", "Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;", "(Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;)V", "getStoryTextPostModel", "()Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class TextModel extends CrossfadeSource {
            private final StoryTextPostModel storyTextPostModel;

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public TextModel(StoryTextPostModel storyTextPostModel) {
                super(null);
                Intrinsics.checkNotNullParameter(storyTextPostModel, "storyTextPostModel");
                this.storyTextPostModel = storyTextPostModel;
            }

            public final StoryTextPostModel getStoryTextPostModel() {
                return this.storyTextPostModel;
            }
        }
    }

    /* compiled from: StoryViewerState.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeTarget;", "", "()V", "None", "Record", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeTarget$None;", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeTarget$Record;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static abstract class CrossfadeTarget {
        public /* synthetic */ CrossfadeTarget(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: StoryViewerState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeTarget$None;", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeTarget;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class None extends CrossfadeTarget {
            public static final None INSTANCE = new None();

            private None() {
                super(null);
            }
        }

        private CrossfadeTarget() {
        }

        /* compiled from: StoryViewerState.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeTarget$Record;", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$CrossfadeTarget;", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "(Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;)V", "getMessageRecord", "()Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Record extends CrossfadeTarget {
            private final MmsMessageRecord messageRecord;

            public static /* synthetic */ Record copy$default(Record record, MmsMessageRecord mmsMessageRecord, int i, Object obj) {
                if ((i & 1) != 0) {
                    mmsMessageRecord = record.messageRecord;
                }
                return record.copy(mmsMessageRecord);
            }

            public final MmsMessageRecord component1() {
                return this.messageRecord;
            }

            public final Record copy(MmsMessageRecord mmsMessageRecord) {
                Intrinsics.checkNotNullParameter(mmsMessageRecord, "messageRecord");
                return new Record(mmsMessageRecord);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Record) && Intrinsics.areEqual(this.messageRecord, ((Record) obj).messageRecord);
            }

            public int hashCode() {
                return this.messageRecord.hashCode();
            }

            public String toString() {
                return "Record(messageRecord=" + this.messageRecord + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Record(MmsMessageRecord mmsMessageRecord) {
                super(null);
                Intrinsics.checkNotNullParameter(mmsMessageRecord, "messageRecord");
                this.messageRecord = mmsMessageRecord;
            }

            public final MmsMessageRecord getMessageRecord() {
                return this.messageRecord;
            }
        }
    }

    /* compiled from: StoryViewerState.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\t\u0010\b\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u00032\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\u0006\u0010\u000e\u001a\u00020\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState$LoadState;", "", "isContentReady", "", "isCrossfaderReady", "(ZZ)V", "()Z", "component1", "component2", "copy", "equals", "other", "hashCode", "", "isReady", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class LoadState {
        private final boolean isContentReady;
        private final boolean isCrossfaderReady;

        public LoadState() {
            this(false, false, 3, null);
        }

        public static /* synthetic */ LoadState copy$default(LoadState loadState, boolean z, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                z = loadState.isContentReady;
            }
            if ((i & 2) != 0) {
                z2 = loadState.isCrossfaderReady;
            }
            return loadState.copy(z, z2);
        }

        public final boolean component1() {
            return this.isContentReady;
        }

        public final boolean component2() {
            return this.isCrossfaderReady;
        }

        public final LoadState copy(boolean z, boolean z2) {
            return new LoadState(z, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof LoadState)) {
                return false;
            }
            LoadState loadState = (LoadState) obj;
            return this.isContentReady == loadState.isContentReady && this.isCrossfaderReady == loadState.isCrossfaderReady;
        }

        public int hashCode() {
            boolean z = this.isContentReady;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = i2 * 31;
            boolean z2 = this.isCrossfaderReady;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            return i5 + i;
        }

        public String toString() {
            return "LoadState(isContentReady=" + this.isContentReady + ", isCrossfaderReady=" + this.isCrossfaderReady + ')';
        }

        public LoadState(boolean z, boolean z2) {
            this.isContentReady = z;
            this.isCrossfaderReady = z2;
        }

        public /* synthetic */ LoadState(boolean z, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2);
        }

        public final boolean isContentReady() {
            return this.isContentReady;
        }

        public final boolean isCrossfaderReady() {
            return this.isCrossfaderReady;
        }

        public final boolean isReady() {
            return this.isContentReady && this.isCrossfaderReady;
        }
    }
}
