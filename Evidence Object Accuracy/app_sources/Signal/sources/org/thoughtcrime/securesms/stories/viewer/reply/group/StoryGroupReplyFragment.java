package org.thoughtcrime.securesms.stories.viewer.reply.group;

import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Annotation;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetBehaviorHack;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ComposeText;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;
import org.thoughtcrime.securesms.components.emoji.EmojiPageView;
import org.thoughtcrime.securesms.components.emoji.MediaKeyboard;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.components.mention.MentionValidatorWatcher;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.MarkReadHelper;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerFragment;
import org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerViewModel;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.keyboard.KeyboardPage;
import org.thoughtcrime.securesms.keyboard.KeyboardPagerViewModel;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardCallback;
import org.thoughtcrime.securesms.mediasend.v2.UntrustedRecords;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerChild;
import org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent;
import org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyViewModel;
import org.thoughtcrime.securesms.util.DeleteDialog;
import org.thoughtcrime.securesms.util.FragmentDialogs;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.PagingMappingAdapter;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: StoryGroupReplyFragment.kt */
@Metadata(d1 = {"\u0000ô\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 v2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006:\u0004uvwxB\u0005¢\u0006\u0002\u0010\u0007J\b\u0010A\u001a\u00020BH\u0016J\b\u0010C\u001a\u00020BH\u0002J\u0016\u0010D\u001a\u00020E2\f\u0010F\u001a\b\u0012\u0004\u0012\u00020G04H\u0002J\b\u0010H\u001a\u00020BH\u0002J\b\u0010I\u001a\u00020BH\u0016J\u0010\u0010J\u001a\u00020B2\u0006\u0010K\u001a\u000202H\u0002J\u0010\u0010L\u001a\u00020B2\u0006\u0010M\u001a\u00020NH\u0002J\b\u0010O\u001a\u00020BH\u0016J\u0012\u0010P\u001a\u00020B2\b\u0010Q\u001a\u0004\u0018\u000107H\u0016J\b\u0010R\u001a\u00020BH\u0016J\u0010\u0010S\u001a\u00020B2\u0006\u0010T\u001a\u00020UH\u0016J\u0012\u0010V\u001a\u00020B2\b\u0010W\u001a\u0004\u0018\u00010XH\u0016J\b\u0010Y\u001a\u00020BH\u0016J\u0010\u0010Z\u001a\u00020B2\u0006\u0010[\u001a\u00020\u0010H\u0016J\b\u0010\\\u001a\u00020BH\u0016J\b\u0010]\u001a\u00020BH\u0016J\b\u0010^\u001a\u00020BH\u0016J\u0010\u0010_\u001a\u00020B2\u0006\u0010Q\u001a\u000207H\u0016J\b\u0010`\u001a\u00020BH\u0016J\b\u0010a\u001a\u00020BH\u0016J\b\u0010b\u001a\u00020BH\u0016J\u0010\u0010c\u001a\u00020B2\u0006\u0010M\u001a\u00020NH\u0002J\u001a\u0010d\u001a\u00020B2\u0006\u0010e\u001a\u00020f2\b\u0010g\u001a\u0004\u0018\u00010hH\u0016J\b\u0010i\u001a\u00020BH\u0016J\u001e\u0010j\u001a\u00020B2\u0006\u0010k\u001a\u0002022\f\u0010l\u001a\b\u0012\u0004\u0012\u00020504H\u0002J\b\u0010m\u001a\u00020BH\u0002J\u0016\u0010n\u001a\u00020B2\f\u0010o\u001a\b\u0012\u0004\u0012\u00020p04H\u0016J\u0010\u0010q\u001a\u00020B2\u0006\u0010Q\u001a\u000207H\u0002J\b\u0010r\u001a\u00020BH\u0002J\f\u0010s\u001a\u00020\u0018*\u00020tH\u0003R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X.¢\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u00020\u00148BX\u0004¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u00188BX\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u001c8BX\u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001dR\u001b\u0010\u001e\u001a\u00020\u001f8BX\u0002¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b \u0010!R\u000e\u0010$\u001a\u00020%X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010&\u001a\u0004\u0018\u00010'X\u000e¢\u0006\u0002\n\u0000R\u001b\u0010(\u001a\u00020)8BX\u0002¢\u0006\f\n\u0004\b,\u0010#\u001a\u0004\b*\u0010+R\u000e\u0010-\u001a\u00020.X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u000200X.¢\u0006\u0002\n\u0000R\u0010\u00101\u001a\u0004\u0018\u000102X\u000e¢\u0006\u0002\n\u0000R\u0014\u00103\u001a\b\u0012\u0004\u0012\u00020504X\u000e¢\u0006\u0002\n\u0000R\u0010\u00106\u001a\u0004\u0018\u000107X\u000e¢\u0006\u0002\n\u0000R\u0014\u00108\u001a\u0002098BX\u0004¢\u0006\u0006\u001a\u0004\b:\u0010;R\u001b\u0010<\u001a\u00020=8BX\u0002¢\u0006\f\n\u0004\b@\u0010#\u001a\u0004\b>\u0010?¨\u0006y"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerChild;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer$Callback;", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardCallback;", "Lorg/thoughtcrime/securesms/reactions/any/ReactWithAnyEmojiBottomSheetDialogFragment$Callback;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheet$Callbacks;", "()V", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/PagingMappingAdapter;", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "colorizer", "Lorg/thoughtcrime/securesms/conversation/colors/Colorizer;", "composer", "Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer;", "currentChild", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent$Child;", "dataObserver", "Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;", "groupRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getGroupRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "groupReplyStartPosition", "", "getGroupReplyStartPosition", "()I", "isFromNotification", "", "()Z", "keyboardPagerViewModel", "Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerViewModel;", "getKeyboardPagerViewModel", "()Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerViewModel;", "keyboardPagerViewModel$delegate", "Lkotlin/Lazy;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "markReadHelper", "Lorg/thoughtcrime/securesms/conversation/MarkReadHelper;", "mentionsViewModel", "Lorg/thoughtcrime/securesms/conversation/ui/mentions/MentionsPickerViewModel;", "getMentionsViewModel", "()Lorg/thoughtcrime/securesms/conversation/ui/mentions/MentionsPickerViewModel;", "mentionsViewModel$delegate", "recyclerListener", "Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "resendBody", "", "resendMentions", "", "Lorg/thoughtcrime/securesms/database/model/Mention;", "resendReaction", "", "storyId", "", "getStoryId", "()J", "viewModel", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyViewModel;", "viewModel$delegate", "closeEmojiSearch", "", "ensureMentionsContainerFilled", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "pageData", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;", "initializeMentions", "onCanceled", "onCopyClick", "textToCopy", "onDeleteClick", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "onDestroyView", "onEmojiSelected", "emoji", "onHideEmojiKeyboard", "onInitializeEmojiDrawer", "mediaKeyboard", "Lorg/thoughtcrime/securesms/components/emoji/MediaKeyboard;", "onKeyEvent", "keyEvent", "Landroid/view/KeyEvent;", "onMessageResentAfterSafetyNumberChangeInBottomSheet", "onPageSelected", "child", "onPause", "onPickReactionClicked", "onReactWithAnyEmojiDialogDismissed", "onReactWithAnyEmojiSelected", "onResume", "onSendActionClicked", "onShowEmojiKeyboard", "onTapForDetailsClick", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "openEmojiSearch", "performSend", "body", "mentions", "postMarkAsReadRequest", "sendAnywayAfterSafetyNumberChangedInBottomSheet", "destinations", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "sendReaction", "updateNestedScrolling", "getStoryGroupReplyColor", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "Callback", "Companion", "GroupDataObserver", "GroupReplyScrollObserver", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryGroupReplyFragment extends Fragment implements StoryViewsAndRepliesPagerChild, StoryReplyComposer.Callback, EmojiKeyboardCallback, ReactWithAnyEmojiBottomSheetDialogFragment.Callback, SafetyNumberBottomSheet.Callbacks {
    private static final String ARG_GROUP_RECIPIENT_ID;
    private static final String ARG_GROUP_REPLY_START_POSITION;
    private static final String ARG_IS_FROM_NOTIFICATION;
    private static final String ARG_STORY_ID;
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(StoryGroupReplyFragment.class);
    private PagingMappingAdapter<MessageId> adapter;
    private final Colorizer colorizer;
    private StoryReplyComposer composer;
    private StoryViewsAndRepliesPagerParent.Child currentChild;
    private RecyclerView.AdapterDataObserver dataObserver;
    private final Lazy keyboardPagerViewModel$delegate;
    private final LifecycleDisposable lifecycleDisposable;
    private MarkReadHelper markReadHelper;
    private final Lazy mentionsViewModel$delegate;
    private final RecyclerView.OnItemTouchListener recyclerListener;
    private RecyclerView recyclerView;
    private CharSequence resendBody;
    private List<? extends Mention> resendMentions;
    private String resendReaction;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryGroupReplyViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$viewModel$2
        final /* synthetic */ StoryGroupReplyFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new StoryGroupReplyViewModel.Factory(this.this$0.getStoryId(), new StoryGroupReplyRepository());
        }
    });

    /* compiled from: StoryGroupReplyFragment.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyFragment$Callback;", "", "onReactionEmojiSelected", "", "emoji", "", "onStartDirectReply", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "requestFullScreen", "fullscreen", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onReactionEmojiSelected(String str);

        void onStartDirectReply(RecipientId recipientId);

        void requestFullScreen(boolean z);
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onKeyEvent(KeyEvent keyEvent) {
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiDialogDismissed() {
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer.Callback
    public void onShowEmojiKeyboard() {
        Callback callback;
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        callback = (Callback) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.Callback");
                    }
                } else if (fragment instanceof Callback) {
                    callback = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            callback.requestFullScreen(true);
            RecyclerView recyclerView = this.recyclerView;
            StoryReplyComposer storyReplyComposer = null;
            if (recyclerView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
                recyclerView = null;
            }
            recyclerView.addOnItemTouchListener(this.recyclerListener);
            StoryReplyComposer storyReplyComposer2 = this.composer;
            if (storyReplyComposer2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("composer");
            } else {
                storyReplyComposer = storyReplyComposer2;
            }
            EmojiPageView emojiPageView = storyReplyComposer.getEmojiPageView();
            if (emojiPageView != null) {
                emojiPageView.addOnItemTouchListener(this.recyclerListener);
            }
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    public StoryGroupReplyFragment() {
        super(R.layout.stories_group_replies_fragment);
        StoryGroupReplyFragment$mentionsViewModel$2 storyGroupReplyFragment$mentionsViewModel$2 = new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$mentionsViewModel$2
            final /* synthetic */ StoryGroupReplyFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStoreOwner invoke() {
                FragmentActivity requireActivity = this.this$0.requireActivity();
                Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
                return requireActivity;
            }
        };
        this.mentionsViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MentionsPickerViewModel.class), new Function0<ViewModelStore>(storyGroupReplyFragment$mentionsViewModel$2) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$special$$inlined$viewModels$1
            final /* synthetic */ Function0 $ownerProducer;

            {
                this.$ownerProducer = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStore invoke() {
                ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
                Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
                return viewModelStore;
            }
        }, StoryGroupReplyFragment$mentionsViewModel$3.INSTANCE);
        this.keyboardPagerViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(KeyboardPagerViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$keyboardPagerViewModel$2
            final /* synthetic */ StoryGroupReplyFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStoreOwner invoke() {
                FragmentActivity requireActivity = this.this$0.requireActivity();
                Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
                return requireActivity;
            }
        }) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$special$$inlined$viewModels$default$3
            final /* synthetic */ Function0 $ownerProducer;

            {
                this.$ownerProducer = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStore invoke() {
                ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
                Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
                return viewModelStore;
            }
        }, null);
        this.recyclerListener = new RecyclerView.SimpleOnItemTouchListener(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$recyclerListener$1
            final /* synthetic */ StoryGroupReplyFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // androidx.recyclerview.widget.RecyclerView.SimpleOnItemTouchListener, androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                Intrinsics.checkNotNullParameter(recyclerView, "view");
                Intrinsics.checkNotNullParameter(motionEvent, "e");
                RecyclerView recyclerView2 = this.this$0.recyclerView;
                StoryReplyComposer storyReplyComposer = null;
                if (recyclerView2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
                    recyclerView2 = null;
                }
                RecyclerView recyclerView3 = this.this$0.recyclerView;
                if (recyclerView3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
                    recyclerView3 = null;
                }
                recyclerView2.setNestedScrollingEnabled(Intrinsics.areEqual(recyclerView, recyclerView3));
                StoryReplyComposer storyReplyComposer2 = this.this$0.composer;
                if (storyReplyComposer2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("composer");
                    storyReplyComposer2 = null;
                }
                EmojiPageView emojiPageView = storyReplyComposer2.getEmojiPageView();
                if (emojiPageView != null) {
                    StoryReplyComposer storyReplyComposer3 = this.this$0.composer;
                    if (storyReplyComposer3 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("composer");
                    } else {
                        storyReplyComposer = storyReplyComposer3;
                    }
                    emojiPageView.setNestedScrollingEnabled(Intrinsics.areEqual(recyclerView, storyReplyComposer.getEmojiPageView()));
                }
                Fragment parentFragment = this.this$0.getParentFragment();
                if (parentFragment != null) {
                    Dialog dialog = ((FixedRoundedCornerBottomSheetDialogFragment) parentFragment).getDialog();
                    if (dialog != null) {
                        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialog;
                        BottomSheetBehaviorHack bottomSheetBehaviorHack = BottomSheetBehaviorHack.INSTANCE;
                        BottomSheetBehavior<FrameLayout> behavior = bottomSheetDialog.getBehavior();
                        Intrinsics.checkNotNullExpressionValue(behavior, "dialog.behavior");
                        bottomSheetBehaviorHack.setNestedScrollingChild(behavior, recyclerView);
                        View findViewById = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
                        if (findViewById == null) {
                            return false;
                        }
                        findViewById.invalidate();
                        return false;
                    }
                    throw new NullPointerException("null cannot be cast to non-null type com.google.android.material.bottomsheet.BottomSheetDialog");
                }
                throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment");
            }
        };
        this.colorizer = new Colorizer();
        this.lifecycleDisposable = new LifecycleDisposable();
        this.resendMentions = CollectionsKt__CollectionsKt.emptyList();
    }

    /* compiled from: StoryGroupReplyFragment.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\n \t*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyFragment$Companion;", "", "()V", "ARG_GROUP_RECIPIENT_ID", "", "ARG_GROUP_REPLY_START_POSITION", "ARG_IS_FROM_NOTIFICATION", "ARG_STORY_ID", "TAG", "kotlin.jvm.PlatformType", "create", "Landroidx/fragment/app/Fragment;", "storyId", "", "groupRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "isFromNotification", "", "groupReplyStartPosition", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Fragment create(long j, RecipientId recipientId, boolean z, int i) {
            Intrinsics.checkNotNullParameter(recipientId, "groupRecipientId");
            StoryGroupReplyFragment storyGroupReplyFragment = new StoryGroupReplyFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(StoryGroupReplyFragment.ARG_STORY_ID, j);
            bundle.putParcelable(StoryGroupReplyFragment.ARG_GROUP_RECIPIENT_ID, recipientId);
            bundle.putBoolean(StoryGroupReplyFragment.ARG_IS_FROM_NOTIFICATION, z);
            bundle.putInt(StoryGroupReplyFragment.ARG_GROUP_REPLY_START_POSITION, i);
            storyGroupReplyFragment.setArguments(bundle);
            return storyGroupReplyFragment;
        }
    }

    private final StoryGroupReplyViewModel getViewModel() {
        return (StoryGroupReplyViewModel) this.viewModel$delegate.getValue();
    }

    private final MentionsPickerViewModel getMentionsViewModel() {
        return (MentionsPickerViewModel) this.mentionsViewModel$delegate.getValue();
    }

    private final KeyboardPagerViewModel getKeyboardPagerViewModel() {
        return (KeyboardPagerViewModel) this.keyboardPagerViewModel$delegate.getValue();
    }

    public final long getStoryId() {
        return requireArguments().getLong(ARG_STORY_ID);
    }

    public final RecipientId getGroupRecipientId() {
        Parcelable parcelable = requireArguments().getParcelable(ARG_GROUP_RECIPIENT_ID);
        Intrinsics.checkNotNull(parcelable);
        return (RecipientId) parcelable;
    }

    public final boolean isFromNotification() {
        return requireArguments().getBoolean(ARG_IS_FROM_NOTIFICATION, false);
    }

    public final int getGroupReplyStartPosition() {
        return requireArguments().getInt(ARG_GROUP_REPLY_START_POSITION, -1);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:53:0x00cd */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v20, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v23, types: [org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent] */
    /* JADX WARN: Type inference failed for: r0v25 */
    /* JADX WARN: Type inference failed for: r0v39 */
    /* JADX WARN: Type inference failed for: r0v40 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // androidx.fragment.app.Fragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onViewCreated(android.view.View r14, android.os.Bundle r15) {
        /*
        // Method dump skipped, instructions count: 322
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.onViewCreated(android.view.View, android.os.Bundle):void");
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m3139onViewCreated$lambda0(StoryGroupReplyFragment storyGroupReplyFragment) {
        Intrinsics.checkNotNullParameter(storyGroupReplyFragment, "this$0");
        RetrieveProfileJob.enqueue(storyGroupReplyFragment.getGroupRecipientId());
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        long threadId = getViewModel().getStateSnapshot().getThreadId();
        if (threadId != 0) {
            ApplicationDependencies.getMessageNotifier().setVisibleThread(new ConversationId(threadId, Long.valueOf(getStoryId())));
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        ApplicationDependencies.getMessageNotifier().setVisibleThread(null);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        storyReplyComposer.getInput().setMentionQueryChangedListener(null);
        StoryReplyComposer storyReplyComposer2 = this.composer;
        if (storyReplyComposer2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer2 = null;
        }
        storyReplyComposer2.getInput().setMentionValidator(null);
    }

    public final void postMarkAsReadRequest() {
        MarkReadHelper markReadHelper;
        PagingMappingAdapter<MessageId> pagingMappingAdapter = this.adapter;
        PagingMappingAdapter<MessageId> pagingMappingAdapter2 = null;
        if (pagingMappingAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("adapter");
            pagingMappingAdapter = null;
        }
        if (pagingMappingAdapter.getItemCount() != 0 && this.markReadHelper != null) {
            RecyclerView recyclerView = this.recyclerView;
            if (recyclerView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
                recyclerView = null;
            }
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (layoutManager != null) {
                int findLastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                PagingMappingAdapter<MessageId> pagingMappingAdapter3 = this.adapter;
                if (pagingMappingAdapter3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("adapter");
                } else {
                    pagingMappingAdapter2 = pagingMappingAdapter3;
                }
                MappingModel<?> item = pagingMappingAdapter2.getItem(findLastVisibleItemPosition);
                if (item != null && (item instanceof StoryGroupReplyItem.Model) && (markReadHelper = this.markReadHelper) != null) {
                    markReadHelper.onViewsRevealed(((StoryGroupReplyItem.Model) item).getReplyBody().getSentAtMillis());
                    return;
                }
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    public final DSLConfiguration getConfiguration(List<? extends ReplyBody> list) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(list, this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1
            final /* synthetic */ List<ReplyBody> $pageData;
            final /* synthetic */ StoryGroupReplyFragment this$0;

            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$pageData = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0042: INVOKE  
                  (r12v0 'dSLConfiguration' org.thoughtcrime.securesms.components.settings.DSLConfiguration)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$TextModel : 0x003f: CONSTRUCTOR  (r3v2 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$TextModel A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$Text : 0x0028: CHECK_CAST (r5v1 org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$Text A[REMOVE]) = (org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$Text) (r2v2 'replyBody' org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody))
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1 : 0x002c: CONSTRUCTOR  (r6v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1 A[REMOVE]) = (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment) call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR)
                  (wrap: int : 0x0021: ONE_ARG  (r7v0 int A[REMOVE]) = 
                  (wrap: int : 0x0000: INVOKE  
                  (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment)
                  (wrap: org.thoughtcrime.securesms.recipients.Recipient : 0x001d: INVOKE  (r3v1 org.thoughtcrime.securesms.recipients.Recipient A[REMOVE]) = (r2v2 'replyBody' org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody) type: VIRTUAL call: org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody.getSender():org.thoughtcrime.securesms.recipients.Recipient)
                 type: DIRECT call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.getStoryGroupReplyColor(org.thoughtcrime.securesms.recipients.Recipient):int)
                )
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$2 : 0x0031: CONSTRUCTOR  (r8v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$2 A[REMOVE]) = (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment) call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$2.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$3 : 0x0036: CONSTRUCTOR  (r9v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$3 A[REMOVE]) = (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment) call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$3.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$4 : 0x003b: CONSTRUCTOR  (r10v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$4 A[REMOVE]) = (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment) call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$4.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR)
                 call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem.TextModel.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$Text, kotlin.jvm.functions.Function1, int, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1):void type: CONSTRUCTOR)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLConfiguration.customPref(org.thoughtcrime.securesms.util.adapter.mapping.MappingModel):void in method: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:221)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003f: CONSTRUCTOR  (r3v2 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$TextModel A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$Text : 0x0028: CHECK_CAST (r5v1 org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$Text A[REMOVE]) = (org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$Text) (r2v2 'replyBody' org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody))
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1 : 0x002c: CONSTRUCTOR  (r6v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1 A[REMOVE]) = (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment) call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR)
                  (wrap: int : 0x0021: ONE_ARG  (r7v0 int A[REMOVE]) = 
                  (wrap: int : 0x0000: INVOKE  
                  (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment)
                  (wrap: org.thoughtcrime.securesms.recipients.Recipient : 0x001d: INVOKE  (r3v1 org.thoughtcrime.securesms.recipients.Recipient A[REMOVE]) = (r2v2 'replyBody' org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody) type: VIRTUAL call: org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody.getSender():org.thoughtcrime.securesms.recipients.Recipient)
                 type: DIRECT call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.getStoryGroupReplyColor(org.thoughtcrime.securesms.recipients.Recipient):int)
                )
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$2 : 0x0031: CONSTRUCTOR  (r8v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$2 A[REMOVE]) = (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment) call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$2.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$3 : 0x0036: CONSTRUCTOR  (r9v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$3 A[REMOVE]) = (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment) call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$3.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$4 : 0x003b: CONSTRUCTOR  (r10v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$4 A[REMOVE]) = (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment) call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$4.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR)
                 call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem.TextModel.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$Text, kotlin.jvm.functions.Function1, int, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 22 more
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x002c: CONSTRUCTOR  (r6v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1 A[REMOVE]) = (r1v0 'storyGroupReplyFragment' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment) call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1.<init>(org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 28 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 34 more
                */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r12) {
                /*
                    r11 = this;
                    java.lang.String r0 = "$this$configure"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r12, r0)
                    java.util.List<org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody> r0 = r11.$pageData
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment r1 = r11.this$0
                    java.util.Iterator r0 = r0.iterator()
                L_0x000d:
                    boolean r2 = r0.hasNext()
                    if (r2 == 0) goto L_0x0091
                    java.lang.Object r2 = r0.next()
                    org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody r2 = (org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody) r2
                    boolean r3 = r2 instanceof org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody.Text
                    if (r3 == 0) goto L_0x0046
                    org.thoughtcrime.securesms.recipients.Recipient r3 = r2.getSender()
                    int r7 = org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.access$getStoryGroupReplyColor(r1, r3)
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$TextModel r3 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$TextModel
                    r5 = r2
                    org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$Text r5 = (org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody.Text) r5
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1 r6 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$1
                    r6.<init>(r1)
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$2 r8 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$2
                    r8.<init>(r1)
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$3 r9 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$3
                    r9.<init>(r1)
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$4 r10 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$4
                    r10.<init>(r1)
                    r4 = r3
                    r4.<init>(r5, r6, r7, r8, r9, r10)
                    r12.customPref(r3)
                    goto L_0x000d
                L_0x0046:
                    boolean r3 = r2 instanceof org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody.Reaction
                    if (r3 == 0) goto L_0x006e
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$ReactionModel r3 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$ReactionModel
                    r5 = r2
                    org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$Reaction r5 = (org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody.Reaction) r5
                    org.thoughtcrime.securesms.recipients.Recipient r2 = r2.getSender()
                    int r6 = org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.access$getStoryGroupReplyColor(r1, r2)
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$5 r7 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$5
                    r7.<init>(r1)
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$6 r8 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$6
                    r8.<init>(r1)
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$7 r9 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$7
                    r9.<init>(r1)
                    r4 = r3
                    r4.<init>(r5, r6, r7, r8, r9)
                    r12.customPref(r3)
                    goto L_0x000d
                L_0x006e:
                    boolean r3 = r2 instanceof org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody.RemoteDelete
                    if (r3 == 0) goto L_0x000d
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$RemoteDeleteModel r3 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$RemoteDeleteModel
                    r4 = r2
                    org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody$RemoteDelete r4 = (org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody.RemoteDelete) r4
                    org.thoughtcrime.securesms.recipients.Recipient r2 = r2.getSender()
                    int r2 = org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.access$getStoryGroupReplyColor(r1, r2)
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$8 r5 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$8
                    r5.<init>(r1)
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$9 r6 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1$1$9
                    r6.<init>(r1)
                    r3.<init>(r4, r2, r5, r6)
                    r12.customPref(r3)
                    goto L_0x000d
                L_0x0091:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    public final void onCopyClick(CharSequence charSequence) {
        ServiceUtil.getClipboardManager(requireContext()).setPrimaryClip(ClipData.newPlainText(requireContext().getString(R.string.app_name), charSequence));
        Toast.makeText(requireContext(), (int) R.string.StoryGroupReplyFragment__copied_to_clipboard, 0).show();
    }

    public final void onDeleteClick(MessageRecord messageRecord) {
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        DeleteDialog deleteDialog = DeleteDialog.INSTANCE;
        FragmentActivity requireActivity = requireActivity();
        Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        Disposable subscribe = DeleteDialog.show$default(deleteDialog, requireActivity, SetsKt__SetsJVMKt.setOf(messageRecord), null, null, false, 28, null).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryGroupReplyFragment.m3136onDeleteClick$lambda2((Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "DeleteDialog.show(requir…ike this.\")\n      }\n    }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* renamed from: onDeleteClick$lambda-2 */
    public static final void m3136onDeleteClick$lambda2(Boolean bool) {
        Intrinsics.checkNotNullExpressionValue(bool, "didDeleteThread");
        if (bool.booleanValue()) {
            throw new AssertionError("We should never end up deleting a Group Thread like this.");
        }
    }

    public final void onTapForDetailsClick(MessageRecord messageRecord) {
        if (!messageRecord.isRemoteDelete()) {
            if (messageRecord.isIdentityMismatchFailure()) {
                Context requireContext = requireContext();
                Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                SafetyNumberBottomSheet.Factory forMessageRecord = SafetyNumberBottomSheet.forMessageRecord(requireContext, messageRecord);
                FragmentManager childFragmentManager = getChildFragmentManager();
                Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                forMessageRecord.show(childFragmentManager);
            } else if (messageRecord.hasFailedWithNetworkFailures()) {
                new MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.conversation_activity__message_could_not_be_sent).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(R.string.conversation_activity__send, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(messageRecord) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$$ExternalSyntheticLambda4
                    public final /* synthetic */ MessageRecord f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        StoryGroupReplyFragment.m3137onTapForDetailsClick$lambda4(StoryGroupReplyFragment.this, this.f$1, dialogInterface, i);
                    }
                }).show();
            }
        }
    }

    /* renamed from: onTapForDetailsClick$lambda-4 */
    public static final void m3137onTapForDetailsClick$lambda4(StoryGroupReplyFragment storyGroupReplyFragment, MessageRecord messageRecord, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(storyGroupReplyFragment, "this$0");
        Intrinsics.checkNotNullParameter(messageRecord, "$messageRecord");
        SignalExecutors.BOUNDED.execute(new Runnable(messageRecord) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ MessageRecord f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StoryGroupReplyFragment.m3138onTapForDetailsClick$lambda4$lambda3(StoryGroupReplyFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: onTapForDetailsClick$lambda-4$lambda-3 */
    public static final void m3138onTapForDetailsClick$lambda4$lambda3(StoryGroupReplyFragment storyGroupReplyFragment, MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(storyGroupReplyFragment, "this$0");
        Intrinsics.checkNotNullParameter(messageRecord, "$messageRecord");
        MessageSender.resend(storyGroupReplyFragment.requireContext(), messageRecord);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerChild
    public void onPageSelected(StoryViewsAndRepliesPagerParent.Child child) {
        Intrinsics.checkNotNullParameter(child, "child");
        this.currentChild = child;
        updateNestedScrolling();
    }

    private final void updateNestedScrolling() {
        boolean z;
        RecyclerView recyclerView = this.recyclerView;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
            recyclerView = null;
        }
        if (this.currentChild == StoryViewsAndRepliesPagerParent.Child.REPLIES) {
            Boolean value = getMentionsViewModel().isShowing().getValue();
            if (value == null) {
                value = Boolean.FALSE;
            }
            if (!value.booleanValue()) {
                z = true;
                recyclerView.setNestedScrollingEnabled(z);
            }
        }
        z = false;
        recyclerView.setNestedScrollingEnabled(z);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer.Callback
    public void onSendActionClicked() {
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        Pair<CharSequence, List<Mention>> consumeInput = storyReplyComposer.consumeInput();
        performSend(consumeInput.component1(), consumeInput.component2());
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer.Callback
    public void onPickReactionClicked() {
        FragmentDialogs fragmentDialogs = FragmentDialogs.INSTANCE;
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        FragmentDialogs.displayInDialogAboveAnchor$default(fragmentDialogs, this, storyReplyComposer.getReactionButton(), (int) R.layout.stories_reaction_bar_layout, 0.0f, new Function2<DialogInterface, View, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1
            final /* synthetic */ StoryGroupReplyFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((DialogInterface) obj, (View) obj2);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001a: INVOKE  
                  (r4v2 'storyReactionBar' org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1$1$1 : 0x0017: CONSTRUCTOR  (r1v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1$1$1 A[REMOVE]) = 
                  (r3v0 'dialogInterface' android.content.DialogInterface)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment : 0x0011: IGET  (r0v3 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment A[REMOVE]) = 
                  (r2v0 'this' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1 A[IMMUTABLE_TYPE, THIS])
                 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1.this$0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment)
                 call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1$1$1.<init>(android.content.DialogInterface, org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR)
                 type: VIRTUAL call: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar.setCallback(org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar$Callback):void in method: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1.invoke(android.content.DialogInterface, android.view.View):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0017: CONSTRUCTOR  (r1v0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1$1$1 A[REMOVE]) = 
                  (r3v0 'dialogInterface' android.content.DialogInterface)
                  (wrap: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment : 0x0011: IGET  (r0v3 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment A[REMOVE]) = 
                  (r2v0 'this' org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1 A[IMMUTABLE_TYPE, THIS])
                 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1.this$0 org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment)
                 call: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1$1$1.<init>(android.content.DialogInterface, org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1.invoke(android.content.DialogInterface, android.view.View):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 10 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1$1$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 16 more
                */
            public final void invoke(android.content.DialogInterface r3, android.view.View r4) {
                /*
                    r2 = this;
                    java.lang.String r0 = "dialog"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r0)
                    java.lang.String r0 = "view"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r4, r0)
                    r0 = 2131364205(0x7f0a096d, float:1.834824E38)
                    android.view.View r4 = r4.findViewById(r0)
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment r0 = r2.this$0
                    org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar r4 = (org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReactionBar) r4
                    org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1$1$1 r1 = new org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1$1$1
                    r1.<init>(r3, r0)
                    r4.setCallback(r1)
                    r4.animateIn()
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onPickReactionClicked$1.invoke(android.content.DialogInterface, android.view.View):void");
            }
        }, 4, (Object) null);
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onEmojiSelected(String str) {
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        storyReplyComposer.onEmojiSelected(str);
    }

    public final void sendReaction(String str) {
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        StoryGroupReplySender storyGroupReplySender = StoryGroupReplySender.INSTANCE;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        Completable observeOn = storyGroupReplySender.sendReaction(requireContext, getStoryId(), str).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "StoryGroupReplySender.se…dSchedulers.mainThread())");
        lifecycleDisposable.plusAssign(SubscribersKt.subscribeBy$default(observeOn, new Function1<Throwable, Unit>(this, str) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$sendReaction$1
            final /* synthetic */ String $emoji;
            final /* synthetic */ StoryGroupReplyFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$emoji = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Intrinsics.checkNotNullParameter(th, "error");
                if (th instanceof UntrustedRecords.UntrustedRecordsException) {
                    this.this$0.resendReaction = this.$emoji;
                    SafetyNumberBottomSheet.Factory forIdentityRecordsAndDestination = SafetyNumberBottomSheet.forIdentityRecordsAndDestination(((UntrustedRecords.UntrustedRecordsException) th).getUntrustedRecords(), new ContactSearchKey.RecipientSearchKey.Story(this.this$0.getGroupRecipientId()));
                    FragmentManager childFragmentManager = this.this$0.getChildFragmentManager();
                    Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                    forIdentityRecordsAndDestination.show(childFragmentManager);
                    return;
                }
                Log.w(StoryGroupReplyFragment.TAG, "Failed to send reply", th);
                Context context = this.this$0.getContext();
                if (context != null) {
                    Toast.makeText(context, (int) R.string.message_details_recipient__failed_to_send, 0).show();
                }
            }
        }, (Function0) null, 2, (Object) null));
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer.Callback
    public void onInitializeEmojiDrawer(MediaKeyboard mediaKeyboard) {
        Intrinsics.checkNotNullParameter(mediaKeyboard, "mediaKeyboard");
        getKeyboardPagerViewModel().setOnlyPage(KeyboardPage.EMOJI);
        mediaKeyboard.setFragmentManager(getChildFragmentManager());
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer.Callback
    public void onHideEmojiKeyboard() {
        Callback callback;
        RecyclerView recyclerView = this.recyclerView;
        StoryReplyComposer storyReplyComposer = null;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
            recyclerView = null;
        }
        recyclerView.removeOnItemTouchListener(this.recyclerListener);
        StoryReplyComposer storyReplyComposer2 = this.composer;
        if (storyReplyComposer2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
        } else {
            storyReplyComposer = storyReplyComposer2;
        }
        EmojiPageView emojiPageView = storyReplyComposer.getEmojiPageView();
        if (emojiPageView != null) {
            emojiPageView.removeOnItemTouchListener(this.recyclerListener);
        }
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        callback = (Callback) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.Callback");
                    }
                } else if (fragment instanceof Callback) {
                    callback = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            callback.requestFullScreen(false);
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment.Callback
    public void openEmojiSearch() {
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        storyReplyComposer.openEmojiSearch();
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment.Callback
    public void closeEmojiSearch() {
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        storyReplyComposer.closeEmojiSearch();
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiSelected(String str) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        sendReaction(str);
    }

    private final void initializeMentions() {
        Recipient.live(getGroupRecipientId()).observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StoryGroupReplyFragment.m3133initializeMentions$lambda9(StoryGroupReplyFragment.this, (Recipient) obj);
            }
        });
        getMentionsViewModel().getSelectedRecipient().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$$ExternalSyntheticLambda7
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StoryGroupReplyFragment.m3131initializeMentions$lambda10(StoryGroupReplyFragment.this, (Recipient) obj);
            }
        });
        getMentionsViewModel().isShowing().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$$ExternalSyntheticLambda8
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StoryGroupReplyFragment.m3132initializeMentions$lambda11(StoryGroupReplyFragment.this, (Boolean) obj);
            }
        });
    }

    /* renamed from: initializeMentions$lambda-9 */
    public static final void m3133initializeMentions$lambda9(StoryGroupReplyFragment storyGroupReplyFragment, Recipient recipient) {
        Intrinsics.checkNotNullParameter(storyGroupReplyFragment, "this$0");
        storyGroupReplyFragment.getMentionsViewModel().onRecipientChange(recipient);
        StoryReplyComposer storyReplyComposer = storyGroupReplyFragment.composer;
        StoryReplyComposer storyReplyComposer2 = null;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        storyReplyComposer.getInput().setMentionQueryChangedListener(new ComposeText.MentionQueryChangedListener(storyGroupReplyFragment) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ StoryGroupReplyFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.components.ComposeText.MentionQueryChangedListener
            public final void onQueryChanged(String str) {
                StoryGroupReplyFragment.m3134initializeMentions$lambda9$lambda5(Recipient.this, this.f$1, str);
            }
        });
        StoryReplyComposer storyReplyComposer3 = storyGroupReplyFragment.composer;
        if (storyReplyComposer3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
        } else {
            storyReplyComposer2 = storyReplyComposer3;
        }
        storyReplyComposer2.getInput().setMentionValidator(new MentionValidatorWatcher.MentionValidator() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.components.mention.MentionValidatorWatcher.MentionValidator
            public final List getInvalidMentionAnnotations(List list) {
                return StoryGroupReplyFragment.m3135initializeMentions$lambda9$lambda8(Recipient.this, list);
            }
        });
    }

    /* renamed from: initializeMentions$lambda-9$lambda-5 */
    public static final void m3134initializeMentions$lambda9$lambda5(Recipient recipient, StoryGroupReplyFragment storyGroupReplyFragment, String str) {
        Intrinsics.checkNotNullParameter(storyGroupReplyFragment, "this$0");
        if (recipient.isPushV2Group()) {
            storyGroupReplyFragment.ensureMentionsContainerFilled();
            storyGroupReplyFragment.getMentionsViewModel().onQueryChange(str);
        }
    }

    /* renamed from: initializeMentions$lambda-9$lambda-8 */
    public static final List m3135initializeMentions$lambda9$lambda8(Recipient recipient, List list) {
        if (!recipient.isPushV2Group()) {
            return list;
        }
        List<RecipientId> participantIds = recipient.getParticipantIds();
        Intrinsics.checkNotNullExpressionValue(participantIds, "recipient.participantIds");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(participantIds, 10));
        for (RecipientId recipientId : participantIds) {
            arrayList.add(MentionAnnotation.idToMentionAnnotationValue(recipientId));
        }
        Set set = CollectionsKt___CollectionsKt.toSet(arrayList);
        Intrinsics.checkNotNullExpressionValue(list, "annotations");
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : list) {
            if (!set.contains(((Annotation) obj).getValue())) {
                arrayList2.add(obj);
            }
        }
        return CollectionsKt___CollectionsKt.toList(arrayList2);
    }

    /* renamed from: initializeMentions$lambda-10 */
    public static final void m3131initializeMentions$lambda10(StoryGroupReplyFragment storyGroupReplyFragment, Recipient recipient) {
        Intrinsics.checkNotNullParameter(storyGroupReplyFragment, "this$0");
        StoryReplyComposer storyReplyComposer = storyGroupReplyFragment.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        storyReplyComposer.getInput().replaceTextWithMention(recipient.getDisplayName(storyGroupReplyFragment.requireContext()), recipient.getId());
    }

    /* renamed from: initializeMentions$lambda-11 */
    public static final void m3132initializeMentions$lambda11(StoryGroupReplyFragment storyGroupReplyFragment, Boolean bool) {
        Intrinsics.checkNotNullParameter(storyGroupReplyFragment, "this$0");
        storyGroupReplyFragment.updateNestedScrolling();
    }

    private final void ensureMentionsContainerFilled() {
        if (getChildFragmentManager().findFragmentById(R.id.mentions_picker_container) == null) {
            getChildFragmentManager().beginTransaction().replace(R.id.mentions_picker_container, new MentionsPickerFragment()).commitNowAllowingStateLoss();
        }
    }

    private final void performSend(CharSequence charSequence, List<? extends Mention> list) {
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        StoryGroupReplySender storyGroupReplySender = StoryGroupReplySender.INSTANCE;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        Completable observeOn = storyGroupReplySender.sendReply(requireContext, getStoryId(), charSequence, list).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "StoryGroupReplySender.se…dSchedulers.mainThread())");
        lifecycleDisposable.plusAssign(SubscribersKt.subscribeBy$default(observeOn, new Function1<Throwable, Unit>(this, charSequence, list) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$performSend$1
            final /* synthetic */ CharSequence $body;
            final /* synthetic */ List<Mention> $mentions;
            final /* synthetic */ StoryGroupReplyFragment this$0;

            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.database.model.Mention> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$body = r2;
                this.$mentions = r3;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Intrinsics.checkNotNullParameter(th, "throwable");
                if (th instanceof UntrustedRecords.UntrustedRecordsException) {
                    this.this$0.resendBody = this.$body;
                    this.this$0.resendMentions = this.$mentions;
                    SafetyNumberBottomSheet.Factory forIdentityRecordsAndDestination = SafetyNumberBottomSheet.forIdentityRecordsAndDestination(((UntrustedRecords.UntrustedRecordsException) th).getUntrustedRecords(), new ContactSearchKey.RecipientSearchKey.Story(this.this$0.getGroupRecipientId()));
                    FragmentManager childFragmentManager = this.this$0.getChildFragmentManager();
                    Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                    forIdentityRecordsAndDestination.show(childFragmentManager);
                    return;
                }
                Log.w(StoryGroupReplyFragment.TAG, "Failed to send reply", th);
                Context context = this.this$0.getContext();
                if (context != null) {
                    Toast.makeText(context, (int) R.string.message_details_recipient__failed_to_send, 0).show();
                }
            }
        }, (Function0) null, 2, (Object) null));
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void sendAnywayAfterSafetyNumberChangedInBottomSheet(List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Intrinsics.checkNotNullParameter(list, "destinations");
        CharSequence charSequence = this.resendBody;
        String str = this.resendReaction;
        if (charSequence != null) {
            performSend(charSequence, this.resendMentions);
        } else if (str != null) {
            sendReaction(str);
        }
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void onMessageResentAfterSafetyNumberChangeInBottomSheet() {
        Log.i(TAG, "Message resent");
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void onCanceled() {
        this.resendBody = null;
        this.resendMentions = CollectionsKt__CollectionsKt.emptyList();
        this.resendReaction = null;
    }

    public final int getStoryGroupReplyColor(Recipient recipient) {
        Colorizer colorizer = this.colorizer;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        return colorizer.getIncomingGroupSenderColor(requireContext, recipient);
    }

    /* compiled from: StoryGroupReplyFragment.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J \u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\bH\u0016¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyFragment$GroupReplyScrollObserver;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyFragment;)V", "onScrollStateChanged", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "newState", "", "onScrolled", "dx", "dy", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    private final class GroupReplyScrollObserver extends RecyclerView.OnScrollListener {
        public GroupReplyScrollObserver() {
            StoryGroupReplyFragment.this = r1;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
            StoryGroupReplyFragment.this.postMarkAsReadRequest();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
            StoryGroupReplyFragment.this.postMarkAsReadRequest();
        }
    }

    /* compiled from: StoryGroupReplyFragment.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyFragment$GroupDataObserver;", "Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyFragment;)V", "onItemRangeInserted", "", "positionStart", "", "itemCount", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public final class GroupDataObserver extends RecyclerView.AdapterDataObserver {
        public GroupDataObserver() {
            StoryGroupReplyFragment.this = r1;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void onItemRangeInserted(int i, int i2) {
            if (i2 != 0) {
                PagingMappingAdapter pagingMappingAdapter = StoryGroupReplyFragment.this.adapter;
                RecyclerView recyclerView = null;
                if (pagingMappingAdapter == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("adapter");
                    pagingMappingAdapter = null;
                }
                MappingModel<?> item = pagingMappingAdapter.getItem(i);
                PagingMappingAdapter pagingMappingAdapter2 = StoryGroupReplyFragment.this.adapter;
                if (pagingMappingAdapter2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("adapter");
                    pagingMappingAdapter2 = null;
                }
                if (i == pagingMappingAdapter2.getItemCount() - 1 && (item instanceof StoryGroupReplyItem.Model)) {
                    boolean areEqual = Intrinsics.areEqual(((StoryGroupReplyItem.Model) item).getReplyBody().getSender(), Recipient.self());
                    if (!areEqual) {
                        if (!areEqual) {
                            RecyclerView recyclerView2 = StoryGroupReplyFragment.this.recyclerView;
                            if (recyclerView2 == null) {
                                Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
                                recyclerView2 = null;
                            }
                            if (recyclerView2.canScrollVertically(1)) {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                    RecyclerView recyclerView3 = StoryGroupReplyFragment.this.recyclerView;
                    if (recyclerView3 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
                    } else {
                        recyclerView = recyclerView3;
                    }
                    recyclerView.post(new StoryGroupReplyFragment$GroupDataObserver$$ExternalSyntheticLambda0(StoryGroupReplyFragment.this, i));
                }
            }
        }

        /* renamed from: onItemRangeInserted$lambda-0 */
        public static final void m3140onItemRangeInserted$lambda0(StoryGroupReplyFragment storyGroupReplyFragment, int i) {
            Intrinsics.checkNotNullParameter(storyGroupReplyFragment, "this$0");
            RecyclerView recyclerView = storyGroupReplyFragment.recyclerView;
            if (recyclerView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
                recyclerView = null;
            }
            recyclerView.scrollToPosition(i);
        }
    }
}
