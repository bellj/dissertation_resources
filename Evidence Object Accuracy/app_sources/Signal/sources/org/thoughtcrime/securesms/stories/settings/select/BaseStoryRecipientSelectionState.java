package org.thoughtcrime.securesms.stories.settings.select;

import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: BaseStoryRecipientSelectionState.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B+\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionState;", "", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "privateStory", "Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "selection", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;Ljava/util/Set;)V", "getDistributionListId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getPrivateStory", "()Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "getSelection", "()Ljava/util/Set;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BaseStoryRecipientSelectionState {
    private final DistributionListId distributionListId;
    private final DistributionListRecord privateStory;
    private final Set<RecipientId> selection;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ BaseStoryRecipientSelectionState copy$default(BaseStoryRecipientSelectionState baseStoryRecipientSelectionState, DistributionListId distributionListId, DistributionListRecord distributionListRecord, Set set, int i, Object obj) {
        if ((i & 1) != 0) {
            distributionListId = baseStoryRecipientSelectionState.distributionListId;
        }
        if ((i & 2) != 0) {
            distributionListRecord = baseStoryRecipientSelectionState.privateStory;
        }
        if ((i & 4) != 0) {
            set = baseStoryRecipientSelectionState.selection;
        }
        return baseStoryRecipientSelectionState.copy(distributionListId, distributionListRecord, set);
    }

    public final DistributionListId component1() {
        return this.distributionListId;
    }

    public final DistributionListRecord component2() {
        return this.privateStory;
    }

    public final Set<RecipientId> component3() {
        return this.selection;
    }

    public final BaseStoryRecipientSelectionState copy(DistributionListId distributionListId, DistributionListRecord distributionListRecord, Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(set, "selection");
        return new BaseStoryRecipientSelectionState(distributionListId, distributionListRecord, set);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BaseStoryRecipientSelectionState)) {
            return false;
        }
        BaseStoryRecipientSelectionState baseStoryRecipientSelectionState = (BaseStoryRecipientSelectionState) obj;
        return Intrinsics.areEqual(this.distributionListId, baseStoryRecipientSelectionState.distributionListId) && Intrinsics.areEqual(this.privateStory, baseStoryRecipientSelectionState.privateStory) && Intrinsics.areEqual(this.selection, baseStoryRecipientSelectionState.selection);
    }

    public int hashCode() {
        DistributionListId distributionListId = this.distributionListId;
        int i = 0;
        int hashCode = (distributionListId == null ? 0 : distributionListId.hashCode()) * 31;
        DistributionListRecord distributionListRecord = this.privateStory;
        if (distributionListRecord != null) {
            i = distributionListRecord.hashCode();
        }
        return ((hashCode + i) * 31) + this.selection.hashCode();
    }

    public String toString() {
        return "BaseStoryRecipientSelectionState(distributionListId=" + this.distributionListId + ", privateStory=" + this.privateStory + ", selection=" + this.selection + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
    /* JADX WARN: Multi-variable type inference failed */
    public BaseStoryRecipientSelectionState(DistributionListId distributionListId, DistributionListRecord distributionListRecord, Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(set, "selection");
        this.distributionListId = distributionListId;
        this.privateStory = distributionListRecord;
        this.selection = set;
    }

    public final DistributionListId getDistributionListId() {
        return this.distributionListId;
    }

    public final DistributionListRecord getPrivateStory() {
        return this.privateStory;
    }

    public /* synthetic */ BaseStoryRecipientSelectionState(DistributionListId distributionListId, DistributionListRecord distributionListRecord, Set set, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(distributionListId, (i & 2) != 0 ? null : distributionListRecord, (i & 4) != 0 ? SetsKt__SetsKt.emptySet() : set);
    }

    public final Set<RecipientId> getSelection() {
        return this.selection;
    }
}
