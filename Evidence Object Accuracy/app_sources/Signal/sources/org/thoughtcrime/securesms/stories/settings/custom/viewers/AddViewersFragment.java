package org.thoughtcrime.securesms.stories.settings.custom.viewers;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment;

/* compiled from: AddViewersFragment.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\b8VX\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/viewers/AddViewersFragment;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionFragment;", "()V", "actionButtonLabel", "", "getActionButtonLabel", "()I", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getDistributionListId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class AddViewersFragment extends BaseStoryRecipientSelectionFragment {
    private final int actionButtonLabel = R.string.HideStoryFromFragment__done;

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment
    public int getActionButtonLabel() {
        return this.actionButtonLabel;
    }

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment
    public DistributionListId getDistributionListId() {
        DistributionListId distributionListId = AddViewersFragmentArgs.fromBundle(requireArguments()).getDistributionListId();
        Intrinsics.checkNotNullExpressionValue(distributionListId, "fromBundle(requireArguments()).distributionListId");
        return distributionListId;
    }
}
