package org.thoughtcrime.securesms.stories;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import androidx.core.view.ViewKt;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.SimpleResource;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Charsets;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.PushDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.fonts.TextFont;
import org.thoughtcrime.securesms.fonts.TextToScript;
import org.thoughtcrime.securesms.fonts.TypefaceCache;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.ParcelUtil;

/* compiled from: StoryTextPostModel.kt */
@Metadata(d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 $2\u00020\u00012\u00020\u0002:\u0002$%B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u000e\u001a\u00020\u0004HÂ\u0003J\t\u0010\u000f\u001a\u00020\u0006HÂ\u0003J\t\u0010\u0010\u001a\u00020\bHÂ\u0003J'\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\u0006\u0010\u0018\u001a\u00020\u0019J\t\u0010\u001a\u001a\u00020\u0013HÖ\u0001J\t\u0010\u001b\u001a\u00020\u000bHÖ\u0001J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0018\u0010 \u001a\u00020\u001d2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0013H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;", "Lcom/bumptech/glide/load/Key;", "Landroid/os/Parcelable;", "storyTextPost", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/StoryTextPost;", "storySentAtMillis", "", "storyAuthor", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/database/model/databaseprotos/StoryTextPost;JLorg/thoughtcrime/securesms/recipients/RecipientId;)V", DraftDatabase.Draft.TEXT, "", "getText", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "getPlaceholder", "Landroid/graphics/drawable/Drawable;", "hashCode", "toString", "updateDiskCacheKey", "", "messageDigest", "Ljava/security/MessageDigest;", "writeToParcel", "parcel", "Landroid/os/Parcel;", "flags", "CREATOR", "Decoder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryTextPostModel implements Key, Parcelable {
    public static final CREATOR CREATOR = new CREATOR(null);
    private final RecipientId storyAuthor;
    private final long storySentAtMillis;
    private final StoryTextPost storyTextPost;
    private final String text;

    private final StoryTextPost component1() {
        return this.storyTextPost;
    }

    private final long component2() {
        return this.storySentAtMillis;
    }

    private final RecipientId component3() {
        return this.storyAuthor;
    }

    public static /* synthetic */ StoryTextPostModel copy$default(StoryTextPostModel storyTextPostModel, StoryTextPost storyTextPost, long j, RecipientId recipientId, int i, Object obj) {
        if ((i & 1) != 0) {
            storyTextPost = storyTextPostModel.storyTextPost;
        }
        if ((i & 2) != 0) {
            j = storyTextPostModel.storySentAtMillis;
        }
        if ((i & 4) != 0) {
            recipientId = storyTextPostModel.storyAuthor;
        }
        return storyTextPostModel.copy(storyTextPost, j, recipientId);
    }

    @JvmStatic
    public static final StoryTextPostModel parseFrom(String str, long j, RecipientId recipientId) throws IOException {
        return CREATOR.parseFrom(str, j, recipientId);
    }

    public final StoryTextPostModel copy(StoryTextPost storyTextPost, long j, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(storyTextPost, "storyTextPost");
        Intrinsics.checkNotNullParameter(recipientId, "storyAuthor");
        return new StoryTextPostModel(storyTextPost, j, recipientId);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // com.bumptech.glide.load.Key
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryTextPostModel)) {
            return false;
        }
        StoryTextPostModel storyTextPostModel = (StoryTextPostModel) obj;
        return Intrinsics.areEqual(this.storyTextPost, storyTextPostModel.storyTextPost) && this.storySentAtMillis == storyTextPostModel.storySentAtMillis && Intrinsics.areEqual(this.storyAuthor, storyTextPostModel.storyAuthor);
    }

    @Override // com.bumptech.glide.load.Key
    public int hashCode() {
        return (((this.storyTextPost.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.storySentAtMillis)) * 31) + this.storyAuthor.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        return "StoryTextPostModel(storyTextPost=" + this.storyTextPost + ", storySentAtMillis=" + this.storySentAtMillis + ", storyAuthor=" + this.storyAuthor + ')';
    }

    public StoryTextPostModel(StoryTextPost storyTextPost, long j, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(storyTextPost, "storyTextPost");
        Intrinsics.checkNotNullParameter(recipientId, "storyAuthor");
        this.storyTextPost = storyTextPost;
        this.storySentAtMillis = j;
        this.storyAuthor = recipientId;
        String body = storyTextPost.getBody();
        Intrinsics.checkNotNullExpressionValue(body, "storyTextPost.body");
        this.text = body;
    }

    @Override // com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        Intrinsics.checkNotNullParameter(messageDigest, "messageDigest");
        messageDigest.update(this.storyTextPost.toByteArray());
        String valueOf = String.valueOf(this.storySentAtMillis);
        Charset charset = Charsets.UTF_8;
        byte[] bytes = valueOf.getBytes(charset);
        Intrinsics.checkNotNullExpressionValue(bytes, "this as java.lang.String).getBytes(charset)");
        messageDigest.update(bytes);
        String serialize = this.storyAuthor.serialize();
        Intrinsics.checkNotNullExpressionValue(serialize, "storyAuthor.serialize()");
        byte[] bytes2 = serialize.getBytes(charset);
        Intrinsics.checkNotNullExpressionValue(bytes2, "this as java.lang.String).getBytes(charset)");
        messageDigest.update(bytes2);
    }

    public final String getText() {
        return this.text;
    }

    public final Drawable getPlaceholder() {
        if (!this.storyTextPost.hasBackground()) {
            return new ColorDrawable(0);
        }
        ChatColors.Companion companion = ChatColors.Companion;
        ChatColors.Id.NotSet notSet = ChatColors.Id.NotSet.INSTANCE;
        ChatColor background = this.storyTextPost.getBackground();
        Intrinsics.checkNotNullExpressionValue(background, "storyTextPost.background");
        return companion.forChatColor(notSet, background).getChatBubbleMask();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "parcel");
        ParcelUtil.writeByteArray(parcel, this.storyTextPost.toByteArray());
        parcel.writeLong(this.storySentAtMillis);
        parcel.writeParcelable(this.storyAuthor, i);
    }

    /* compiled from: StoryTextPostModel.kt */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001d\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016¢\u0006\u0002\u0010\u000bJ \u0010\f\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0007J\u000e\u0010\f\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryTextPostModel$CREATOR;", "Landroid/os/Parcelable$Creator;", "Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;", "()V", "createFromParcel", "parcel", "Landroid/os/Parcel;", "newArray", "", MediaPreviewActivity.SIZE_EXTRA, "", "(I)[Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;", "parseFrom", "body", "", "storySentAtMillis", "", "storyAuthor", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class CREATOR implements Parcelable.Creator<StoryTextPostModel> {
        public /* synthetic */ CREATOR(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private CREATOR() {
        }

        @Override // android.os.Parcelable.Creator
        public StoryTextPostModel createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            StoryTextPost parseFrom = StoryTextPost.parseFrom(ParcelUtil.readByteArray(parcel));
            Intrinsics.checkNotNullExpressionValue(parseFrom, "parseFrom(storyTextPostArray)");
            long readLong = parcel.readLong();
            Parcelable readParcelable = parcel.readParcelable(RecipientId.class.getClassLoader());
            Intrinsics.checkNotNull(readParcelable);
            return new StoryTextPostModel(parseFrom, readLong, (RecipientId) readParcelable);
        }

        @Override // android.os.Parcelable.Creator
        public StoryTextPostModel[] newArray(int i) {
            return new StoryTextPostModel[i];
        }

        public final StoryTextPostModel parseFrom(MessageRecord messageRecord) {
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            String body = messageRecord.getBody();
            Intrinsics.checkNotNullExpressionValue(body, "messageRecord.body");
            long timestamp = messageRecord.getTimestamp();
            RecipientId id = (messageRecord.isOutgoing() ? Recipient.self() : messageRecord.getIndividualRecipient()).getId();
            Intrinsics.checkNotNullExpressionValue(id, "if (messageRecord.isOutg…rd.individualRecipient.id");
            return parseFrom(body, timestamp, id);
        }

        @JvmStatic
        public final StoryTextPostModel parseFrom(String str, long j, RecipientId recipientId) throws IOException {
            Intrinsics.checkNotNullParameter(str, "body");
            Intrinsics.checkNotNullParameter(recipientId, "storyAuthor");
            StoryTextPost parseFrom = StoryTextPost.parseFrom(Base64.decode(str));
            Intrinsics.checkNotNullExpressionValue(parseFrom, "parseFrom(Base64.decode(body))");
            return new StoryTextPostModel(parseFrom, j, recipientId);
        }
    }

    /* compiled from: StoryTextPostModel.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u0000 \u000f2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\u000fB\u0005¢\u0006\u0002\u0010\u0004J.\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u00062\u0006\u0010\u0007\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\fH\u0016¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryTextPostModel$Decoder;", "Lcom/bumptech/glide/load/ResourceDecoder;", "Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;", "Landroid/graphics/Bitmap;", "()V", "decode", "Lcom/bumptech/glide/load/engine/Resource;", PushDatabase.SOURCE_E164, "width", "", "height", "options", "Lcom/bumptech/glide/load/Options;", "handles", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Decoder implements ResourceDecoder<StoryTextPostModel, Bitmap> {
        public static final Companion Companion = new Companion(null);
        private static final float RENDER_HW_AR;

        public boolean handles(StoryTextPostModel storyTextPostModel, Options options) {
            Intrinsics.checkNotNullParameter(storyTextPostModel, PushDatabase.SOURCE_E164);
            Intrinsics.checkNotNullParameter(options, "options");
            return true;
        }

        /* compiled from: StoryTextPostModel.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryTextPostModel$Decoder$Companion;", "", "()V", "RENDER_HW_AR", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }

        public Resource<Bitmap> decode(StoryTextPostModel storyTextPostModel, int i, int i2, Options options) {
            List<LinkPreview> linkPreviews;
            Intrinsics.checkNotNullParameter(storyTextPostModel, PushDatabase.SOURCE_E164);
            Intrinsics.checkNotNullParameter(options, "options");
            MessageRecord messageFor = SignalDatabase.Companion.mmsSms().getMessageFor(storyTextPostModel.storySentAtMillis, storyTextPostModel.storyAuthor);
            Application application = ApplicationDependencies.getApplication();
            Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
            StoryTextPostView storyTextPostView = new StoryTextPostView(application, null, 0, 6, null);
            TypefaceCache typefaceCache = TypefaceCache.INSTANCE;
            Application application2 = ApplicationDependencies.getApplication();
            Intrinsics.checkNotNullExpressionValue(application2, "getApplication()");
            TextFont.Companion companion = TextFont.Companion;
            StoryTextPost.Style style = storyTextPostModel.storyTextPost.getStyle();
            Intrinsics.checkNotNullExpressionValue(style, "source.storyTextPost.style");
            TextFont fromStyle = companion.fromStyle(style);
            TextToScript textToScript = TextToScript.INSTANCE;
            String body = storyTextPostModel.storyTextPost.getBody();
            Intrinsics.checkNotNullExpressionValue(body, "source.storyTextPost.body");
            Typeface blockingGet = typefaceCache.get(application2, fromStyle, textToScript.guessScript(body)).blockingGet();
            int i3 = ApplicationDependencies.getApplication().getResources().getDisplayMetrics().widthPixels;
            int i4 = (int) (((float) i3) * RENDER_HW_AR);
            Intrinsics.checkNotNullExpressionValue(blockingGet, "typeface");
            storyTextPostView.setTypeface(blockingGet);
            storyTextPostView.bindFromStoryTextPost(storyTextPostModel.storyTextPost);
            MmsMessageRecord mmsMessageRecord = messageFor instanceof MmsMessageRecord ? (MmsMessageRecord) messageFor : null;
            storyTextPostView.bindLinkPreview((mmsMessageRecord == null || (linkPreviews = mmsMessageRecord.getLinkPreviews()) == null) ? null : (LinkPreview) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) linkPreviews)));
            storyTextPostView.invalidate();
            storyTextPostView.measure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), View.MeasureSpec.makeMeasureSpec(i4, 1073741824));
            storyTextPostView.layout(0, 0, storyTextPostView.getMeasuredWidth(), storyTextPostView.getMeasuredHeight());
            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(ViewKt.drawToBitmap$default(storyTextPostView, null, 1, null), i, i2, true);
            Intrinsics.checkNotNullExpressionValue(createScaledBitmap, "Bitmap.createScaledBitma…s, width, height, filter)");
            return new SimpleResource(createScaledBitmap);
        }
    }
}
