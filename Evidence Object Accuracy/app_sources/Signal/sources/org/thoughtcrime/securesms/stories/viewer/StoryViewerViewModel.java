package org.thoughtcrime.securesms.stories.viewer;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.functions.Predicate;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.StoryViewerArgs;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerState;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: StoryViewerViewModel.kt */
@Metadata(d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001>B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010$\u001a\u00020%J\u0014\u0010&\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020)0(0'H\u0002J\b\u0010*\u001a\u00020%H\u0014J\u000e\u0010+\u001a\u00020%2\u0006\u0010,\u001a\u00020)J\u000e\u0010-\u001a\u00020%2\u0006\u0010,\u001a\u00020)J\u0006\u0010.\u001a\u00020%J\b\u0010/\u001a\u00020%H\u0002J\u001e\u00100\u001a\u0002012\u0006\u00102\u001a\u0002012\f\u00103\u001a\b\u0012\u0004\u0012\u00020)0(H\u0002J\u0006\u00104\u001a\u00020%J\u000e\u00105\u001a\u00020%2\u0006\u00106\u001a\u000207J\u000e\u00108\u001a\u00020%2\u0006\u00109\u001a\u00020\tJ\u000e\u0010:\u001a\u00020%2\u0006\u0010\u0014\u001a\u00020\tJ\u000e\u0010;\u001a\u00020%2\u0006\u0010\u0015\u001a\u00020\tJ\u000e\u0010<\u001a\u00020%2\u0006\u00102\u001a\u000201J\u0018\u0010=\u001a\u00020\u001c2\u0006\u0010\u001a\u001a\u00020\u001c2\u0006\u00102\u001a\u000201H\u0002R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\t@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000bR\u0017\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\t0\u0016¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0017R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\u0019X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u001f\u001a\u00020\u001c8F¢\u0006\u0006\u001a\u0004\b \u0010!R\u0014\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u001c0#X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006?"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerViewModel;", "Landroidx/lifecycle/ViewModel;", "storyViewerArgs", "Lorg/thoughtcrime/securesms/stories/StoryViewerArgs;", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerRepository;", "(Lorg/thoughtcrime/securesms/stories/StoryViewerArgs;Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerRepository;)V", "allowParentScrolling", "Lio/reactivex/rxjava3/core/Observable;", "", "getAllowParentScrolling", "()Lio/reactivex/rxjava3/core/Observable;", "childScrollStatePublisher", "Lio/reactivex/rxjava3/subjects/BehaviorSubject;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "<set-?>", "hasConsumedInitialState", "getHasConsumedInitialState", "()Z", "isChildScrolling", "isScrolling", "Landroidx/lifecycle/LiveData;", "()Landroidx/lifecycle/LiveData;", "scrollStatePublisher", "Landroidx/lifecycle/MutableLiveData;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "stateSnapshot", "getStateSnapshot", "()Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerState;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "consumeInitialState", "", "getStories", "Lio/reactivex/rxjava3/core/Single;", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "onCleared", "onGoToNext", "recipientId", "onGoToPrevious", "onRecipientHidden", "refresh", "resolvePage", "", "page", "recipientIds", "setContentIsReady", "setCrossfadeTarget", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "setCrossfaderIsReady", "isReady", "setIsChildScrolling", "setIsScrolling", "setSelectedPage", "updatePages", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerViewModel extends ViewModel {
    private final Observable<Boolean> allowParentScrolling;
    private final BehaviorSubject<Boolean> childScrollStatePublisher;
    private final CompositeDisposable disposables;
    private boolean hasConsumedInitialState;
    private final Observable<Boolean> isChildScrolling;
    private final LiveData<Boolean> isScrolling;
    private final StoryViewerRepository repository;
    private final MutableLiveData<Boolean> scrollStatePublisher;
    private final Flowable<StoryViewerState> state;
    private final RxStore<StoryViewerState> store;
    private final StoryViewerArgs storyViewerArgs;

    public StoryViewerViewModel(StoryViewerArgs storyViewerArgs, StoryViewerRepository storyViewerRepository) {
        StoryViewerState.CrossfadeSource crossfadeSource;
        Intrinsics.checkNotNullParameter(storyViewerArgs, "storyViewerArgs");
        Intrinsics.checkNotNullParameter(storyViewerRepository, "repository");
        this.storyViewerArgs = storyViewerArgs;
        this.repository = storyViewerRepository;
        if (storyViewerArgs.getStoryThumbTextModel() != null) {
            crossfadeSource = new StoryViewerState.CrossfadeSource.TextModel(storyViewerArgs.getStoryThumbTextModel());
        } else if (storyViewerArgs.getStoryThumbUri() != null) {
            crossfadeSource = new StoryViewerState.CrossfadeSource.ImageUri(storyViewerArgs.getStoryThumbUri(), storyViewerArgs.getStoryThumbBlur());
        } else {
            crossfadeSource = StoryViewerState.CrossfadeSource.None.INSTANCE;
        }
        RxStore<StoryViewerState> rxStore = new RxStore<>(new StoryViewerState(null, 0, 0, crossfadeSource, null, null, storyViewerArgs.isFromNotification() || storyViewerArgs.isFromQuote(), 55, null), null, 2, null);
        this.store = rxStore;
        this.disposables = new CompositeDisposable();
        Flowable<StoryViewerState> stateFlowable = rxStore.getStateFlowable();
        this.state = stateFlowable;
        Boolean bool = Boolean.FALSE;
        MutableLiveData<Boolean> mutableLiveData = new MutableLiveData<>(bool);
        this.scrollStatePublisher = mutableLiveData;
        this.isScrolling = mutableLiveData;
        BehaviorSubject<Boolean> createDefault = BehaviorSubject.createDefault(bool);
        Intrinsics.checkNotNullExpressionValue(createDefault, "createDefault(false)");
        this.childScrollStatePublisher = createDefault;
        Observable<Boolean> combineLatest = Observable.combineLatest(createDefault.distinctUntilChanged(), stateFlowable.toObservable().map(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryViewerViewModel.m3010allowParentScrolling$lambda0((StoryViewerState) obj);
            }
        }).distinctUntilChanged(), new BiFunction() { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return StoryViewerViewModel.m3011allowParentScrolling$lambda1((Boolean) obj, (Boolean) obj2);
            }
        });
        Intrinsics.checkNotNullExpressionValue(combineLatest, "combineLatest(\n    child…)\n  ) { a, b -> !a && b }");
        this.allowParentScrolling = combineLatest;
        Observable<Boolean> distinctUntilChanged = createDefault.distinctUntilChanged();
        Intrinsics.checkNotNullExpressionValue(distinctUntilChanged, "childScrollStatePublisher.distinctUntilChanged()");
        this.isChildScrolling = distinctUntilChanged;
        refresh();
    }

    public final StoryViewerState getStateSnapshot() {
        return this.store.getState();
    }

    public final Flowable<StoryViewerState> getState() {
        return this.state;
    }

    public final LiveData<Boolean> isScrolling() {
        return this.isScrolling;
    }

    public final Observable<Boolean> getAllowParentScrolling() {
        return this.allowParentScrolling;
    }

    /* renamed from: allowParentScrolling$lambda-0 */
    public static final Boolean m3010allowParentScrolling$lambda0(StoryViewerState storyViewerState) {
        return Boolean.valueOf(storyViewerState.getLoadState().isReady());
    }

    /* renamed from: allowParentScrolling$lambda-1 */
    public static final Boolean m3011allowParentScrolling$lambda1(Boolean bool, Boolean bool2) {
        boolean z;
        if (!bool.booleanValue()) {
            Intrinsics.checkNotNullExpressionValue(bool2, "b");
            if (bool2.booleanValue()) {
                z = true;
                return Boolean.valueOf(z);
            }
        }
        z = false;
        return Boolean.valueOf(z);
    }

    public final boolean getHasConsumedInitialState() {
        return this.hasConsumedInitialState;
    }

    public final Observable<Boolean> isChildScrolling() {
        return this.isChildScrolling;
    }

    public final void setCrossfadeTarget(MmsMessageRecord mmsMessageRecord) {
        Intrinsics.checkNotNullParameter(mmsMessageRecord, "messageRecord");
        this.store.update(new Function1<StoryViewerState, StoryViewerState>(mmsMessageRecord) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$setCrossfadeTarget$1
            final /* synthetic */ MmsMessageRecord $messageRecord;

            /* access modifiers changed from: package-private */
            {
                this.$messageRecord = r1;
            }

            public final StoryViewerState invoke(StoryViewerState storyViewerState) {
                Intrinsics.checkNotNullParameter(storyViewerState, "it");
                return StoryViewerState.copy$default(storyViewerState, null, 0, 0, null, new StoryViewerState.CrossfadeTarget.Record(this.$messageRecord), null, false, 111, null);
            }
        });
    }

    public final void consumeInitialState() {
        this.hasConsumedInitialState = true;
    }

    public final void setContentIsReady() {
        this.store.update(StoryViewerViewModel$setContentIsReady$1.INSTANCE);
    }

    public final void setCrossfaderIsReady(boolean z) {
        this.store.update(new Function1<StoryViewerState, StoryViewerState>(z) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$setCrossfaderIsReady$1
            final /* synthetic */ boolean $isReady;

            /* access modifiers changed from: package-private */
            {
                this.$isReady = r1;
            }

            public final StoryViewerState invoke(StoryViewerState storyViewerState) {
                Intrinsics.checkNotNullParameter(storyViewerState, "it");
                return StoryViewerState.copy$default(storyViewerState, null, 0, 0, null, null, StoryViewerState.LoadState.copy$default(storyViewerState.getLoadState(), false, this.$isReady, 1, null), false, 95, null);
            }
        });
    }

    public final void setIsScrolling(boolean z) {
        this.scrollStatePublisher.setValue(Boolean.valueOf(z));
    }

    private final Single<List<RecipientId>> getStories() {
        if (!(!this.storyViewerArgs.getRecipientIds().isEmpty())) {
            return this.repository.getStories(this.storyViewerArgs.isInHiddenStoryMode(), this.storyViewerArgs.isUnviewedOnly(), this.storyViewerArgs.isFromMyStories());
        }
        Single<List<RecipientId>> just = Single.just(this.storyViewerArgs.getRecipientIds());
        Intrinsics.checkNotNullExpressionValue(just, "{\n      Single.just(stor…rArgs.recipientIds)\n    }");
        return just;
    }

    private final void refresh() {
        this.disposables.clear();
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.repository.getFirstStory(this.storyViewerArgs.getRecipientId(), this.storyViewerArgs.isUnviewedOnly(), this.storyViewerArgs.getStoryId()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryViewerViewModel.m3012refresh$lambda2(StoryViewerViewModel.this, (MmsMessageRecord) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getFirstStory…)\n        )\n      }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
        CompositeDisposable compositeDisposable2 = this.disposables;
        Disposable subscribe2 = getStories().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryViewerViewModel.m3013refresh$lambda3(StoryViewerViewModel.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "getStories().subscribe {…Ids), page)\n      }\n    }");
        DisposableKt.plusAssign(compositeDisposable2, subscribe2);
        CompositeDisposable compositeDisposable3 = this.disposables;
        Disposable subscribe3 = this.state.map(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryViewerViewModel.m3014refresh$lambda4((StoryViewerState) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Predicate
            public final boolean test(Object obj) {
                return StoryViewerViewModel.m3015refresh$lambda5((RecipientId) obj);
            }
        }).distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryViewerViewModel.m3016refresh$lambda6((RecipientId) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe3, "state\n      .map {\n     …ownloadMaximum())\n      }");
        DisposableKt.plusAssign(compositeDisposable3, subscribe3);
    }

    /* renamed from: refresh$lambda-2 */
    public static final void m3012refresh$lambda2(StoryViewerViewModel storyViewerViewModel, MmsMessageRecord mmsMessageRecord) {
        Intrinsics.checkNotNullParameter(storyViewerViewModel, "this$0");
        storyViewerViewModel.store.update(new Function1<StoryViewerState, StoryViewerState>(mmsMessageRecord) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$refresh$1$1
            final /* synthetic */ MmsMessageRecord $record;

            /* access modifiers changed from: package-private */
            {
                this.$record = r1;
            }

            public final StoryViewerState invoke(StoryViewerState storyViewerState) {
                Intrinsics.checkNotNullParameter(storyViewerState, "it");
                MmsMessageRecord mmsMessageRecord2 = this.$record;
                Intrinsics.checkNotNullExpressionValue(mmsMessageRecord2, "record");
                return StoryViewerState.copy$default(storyViewerState, null, 0, 0, null, new StoryViewerState.CrossfadeTarget.Record(mmsMessageRecord2), null, false, 111, null);
            }
        });
    }

    /* renamed from: refresh$lambda-3 */
    public static final void m3013refresh$lambda3(StoryViewerViewModel storyViewerViewModel, List list) {
        Intrinsics.checkNotNullParameter(storyViewerViewModel, "this$0");
        storyViewerViewModel.store.update(new Function1<StoryViewerState, StoryViewerState>(list, storyViewerViewModel) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$refresh$2$1
            final /* synthetic */ List<RecipientId> $recipientIds;
            final /* synthetic */ StoryViewerViewModel this$0;

            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$recipientIds = r1;
                this.this$0 = r2;
            }

            public final StoryViewerState invoke(StoryViewerState storyViewerState) {
                int i;
                Intrinsics.checkNotNullParameter(storyViewerState, "it");
                if (!storyViewerState.getPages().isEmpty()) {
                    int page = storyViewerState.getPage();
                    i = this.$recipientIds.indexOf(storyViewerState.getPages().get(page));
                    if (i == -1) {
                        i = storyViewerState.getPage();
                    }
                } else {
                    i = storyViewerState.getPage();
                }
                StoryViewerViewModel storyViewerViewModel2 = this.this$0;
                List<RecipientId> list2 = this.$recipientIds;
                Intrinsics.checkNotNullExpressionValue(list2, "recipientIds");
                return storyViewerViewModel2.updatePages(StoryViewerState.copy$default(storyViewerState, list2, 0, 0, null, null, null, false, 126, null), i);
            }
        });
    }

    /* renamed from: refresh$lambda-4 */
    public static final RecipientId m3014refresh$lambda4(StoryViewerState storyViewerState) {
        int size = storyViewerState.getPages().size();
        int page = storyViewerState.getPage() + 1;
        boolean z = false;
        if (page >= 0 && page < size) {
            z = true;
        }
        if (z) {
            return storyViewerState.getPages().get(storyViewerState.getPage() + 1);
        }
        return RecipientId.UNKNOWN;
    }

    /* renamed from: refresh$lambda-5 */
    public static final boolean m3015refresh$lambda5(RecipientId recipientId) {
        return !Intrinsics.areEqual(recipientId, RecipientId.UNKNOWN);
    }

    /* renamed from: refresh$lambda-6 */
    public static final void m3016refresh$lambda6(RecipientId recipientId) {
        Intrinsics.checkNotNullExpressionValue(recipientId, "it");
        Stories.enqueueNextStoriesForDownload(recipientId, true, FeatureFlags.storiesAutoDownloadMaximum());
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void setSelectedPage(int i) {
        this.store.update(new Function1<StoryViewerState, StoryViewerState>(this, i) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$setSelectedPage$1
            final /* synthetic */ int $page;
            final /* synthetic */ StoryViewerViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$page = r2;
            }

            public final StoryViewerState invoke(StoryViewerState storyViewerState) {
                Intrinsics.checkNotNullParameter(storyViewerState, "it");
                return this.this$0.updatePages(storyViewerState, this.$page);
            }
        });
    }

    public final void onGoToNext(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.store.update(new Function1<StoryViewerState, StoryViewerState>(recipientId, this) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$onGoToNext$1
            final /* synthetic */ RecipientId $recipientId;
            final /* synthetic */ StoryViewerViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.$recipientId = r1;
                this.this$0 = r2;
            }

            public final StoryViewerState invoke(StoryViewerState storyViewerState) {
                Intrinsics.checkNotNullParameter(storyViewerState, "it");
                int size = storyViewerState.getPages().size();
                int page = storyViewerState.getPage();
                boolean z = false;
                if (page >= 0 && page < size) {
                    z = true;
                }
                return (!z || !Intrinsics.areEqual(storyViewerState.getPages().get(storyViewerState.getPage()), this.$recipientId)) ? storyViewerState : this.this$0.updatePages(storyViewerState, storyViewerState.getPage() + 1);
            }
        });
    }

    public final void onGoToPrevious(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.store.update(new Function1<StoryViewerState, StoryViewerState>(recipientId, this) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel$onGoToPrevious$1
            final /* synthetic */ RecipientId $recipientId;
            final /* synthetic */ StoryViewerViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.$recipientId = r1;
                this.this$0 = r2;
            }

            public final StoryViewerState invoke(StoryViewerState storyViewerState) {
                Intrinsics.checkNotNullParameter(storyViewerState, "it");
                int size = storyViewerState.getPages().size();
                int page = storyViewerState.getPage();
                return (!(page >= 0 && page < size) || !Intrinsics.areEqual(storyViewerState.getPages().get(storyViewerState.getPage()), this.$recipientId)) ? storyViewerState : this.this$0.updatePages(storyViewerState, Math.max(0, storyViewerState.getPage() - 1));
            }
        });
    }

    public final void onRecipientHidden() {
        refresh();
    }

    public final StoryViewerState updatePages(StoryViewerState storyViewerState, int i) {
        int i2;
        int resolvePage = resolvePage(i, storyViewerState.getPages());
        if (resolvePage == storyViewerState.getPage()) {
            i2 = storyViewerState.getPreviousPage();
        } else {
            i2 = storyViewerState.getPage();
        }
        return StoryViewerState.copy$default(storyViewerState, null, i2, resolvePage, null, null, null, false, 121, null);
    }

    private final int resolvePage(int i, List<? extends RecipientId> list) {
        if (i > -1) {
            return i;
        }
        int indexOf = list.indexOf(this.storyViewerArgs.getRecipientId());
        if (indexOf == -1) {
            return 0;
        }
        return indexOf;
    }

    public final void setIsChildScrolling(boolean z) {
        this.childScrollStatePublisher.onNext(Boolean.valueOf(z));
    }

    /* compiled from: StoryViewerViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "storyViewerArgs", "Lorg/thoughtcrime/securesms/stories/StoryViewerArgs;", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerRepository;", "(Lorg/thoughtcrime/securesms/stories/StoryViewerArgs;Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final StoryViewerRepository repository;
        private final StoryViewerArgs storyViewerArgs;

        public Factory(StoryViewerArgs storyViewerArgs, StoryViewerRepository storyViewerRepository) {
            Intrinsics.checkNotNullParameter(storyViewerArgs, "storyViewerArgs");
            Intrinsics.checkNotNullParameter(storyViewerRepository, "repository");
            this.storyViewerArgs = storyViewerArgs;
            this.repository = storyViewerRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StoryViewerViewModel(this.storyViewerArgs, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel.Factory.create");
        }
    }
}
