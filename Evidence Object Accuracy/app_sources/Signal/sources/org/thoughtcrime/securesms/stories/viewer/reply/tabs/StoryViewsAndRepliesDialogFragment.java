package org.thoughtcrime.securesms.stories.viewer.reply.tabs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.math.MathKt__MathJVMKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel;
import org.thoughtcrime.securesms.stories.viewer.reply.BottomSheetBehaviorDelegate;
import org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerChild;
import org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment;
import org.thoughtcrime.securesms.stories.viewer.reply.reaction.OnReactionSentView;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: StoryViewsAndRepliesDialogFragment.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u000b\u0018\u0000 G2\u00020\u00012\u00020\u00022\u00020\u0003:\u0003GHIB\u0005¢\u0006\u0002\u0010\u0004J&\u00100\u001a\u0004\u0018\u0001012\u0006\u00102\u001a\u0002032\b\u00104\u001a\u0004\u0018\u0001052\b\u00106\u001a\u0004\u0018\u000107H\u0016J\u0010\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020;H\u0016J\b\u0010<\u001a\u000209H\u0016J\u0010\u0010=\u001a\u0002092\u0006\u0010>\u001a\u00020?H\u0016J\b\u0010@\u001a\u000209H\u0016J\u0010\u0010A\u001a\u0002092\u0006\u0010B\u001a\u00020\u0006H\u0016J\u001a\u0010C\u001a\u0002092\u0006\u0010D\u001a\u0002012\b\u00106\u001a\u0004\u0018\u000107H\u0016J\u0010\u0010E\u001a\u0002092\u0006\u0010F\u001a\u00020\u000fH\u0016R\u0014\u0010\u0005\u001a\u00020\u00068BX\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u000f8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u00060\u0014R\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X.¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u00020\u0018XD¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX.¢\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\u00020\u001e8VX\u0004¢\u0006\u0006\u001a\u0004\b\u001f\u0010 R\u000e\u0010!\u001a\u00020\u000fX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\"\u001a\u00020\n8BX\u0004¢\u0006\u0006\u001a\u0004\b#\u0010\fR\u0014\u0010$\u001a\u00020%8BX\u0004¢\u0006\u0006\u001a\u0004\b&\u0010'R\u001b\u0010(\u001a\u00020)8BX\u0002¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b*\u0010+R\u0014\u0010.\u001a\u00020\n8TX\u0004¢\u0006\u0006\u001a\u0004\b/\u0010\f¨\u0006J"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/tabs/StoryViewsAndRepliesDialogFragment;", "Lorg/thoughtcrime/securesms/components/FixedRoundedCornerBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyFragment$Callback;", "()V", "groupRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getGroupRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "groupReplyStartPosition", "", "getGroupReplyStartPosition", "()I", "initialParentHeight", "isFromNotification", "", "()Z", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "onPageChangeCallback", "Lorg/thoughtcrime/securesms/stories/viewer/reply/tabs/StoryViewsAndRepliesDialogFragment$PageChangeCallback;", "pager", "Landroidx/viewpager2/widget/ViewPager2;", "peekHeightPercentage", "", "getPeekHeightPercentage", "()F", "reactionView", "Lorg/thoughtcrime/securesms/stories/viewer/reply/reaction/OnReactionSentView;", "selectedChild", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent$Child;", "getSelectedChild", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/StoryViewsAndRepliesPagerParent$Child;", "shouldShowFullScreen", "startPageIndex", "getStartPageIndex", "storyId", "", "getStoryId", "()J", "storyViewerPageViewModel", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "getStoryViewerPageViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "storyViewerPageViewModel$delegate", "Lkotlin/Lazy;", "themeResId", "getThemeResId", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDismiss", "", "dialog", "Landroid/content/DialogInterface;", "onPause", "onReactionEmojiSelected", "emoji", "", "onResume", "onStartDirectReply", "recipientId", "onViewCreated", "view", "requestFullScreen", "fullscreen", "Companion", "PageChangeCallback", "StartPage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewsAndRepliesDialogFragment extends FixedRoundedCornerBottomSheetDialogFragment implements StoryViewsAndRepliesPagerParent, StoryGroupReplyFragment.Callback {
    private static final String ARG_GROUP_RECIPIENT_ID;
    private static final String ARG_GROUP_REPLY_START_POSITION;
    private static final String ARG_IS_FROM_NOTIFICATION;
    private static final String ARG_START_PAGE;
    private static final String ARG_STORY_ID;
    public static final Companion Companion = new Companion(null);
    private int initialParentHeight;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final PageChangeCallback onPageChangeCallback = new PageChangeCallback();
    private ViewPager2 pager;
    private final float peekHeightPercentage = 1.0f;
    private OnReactionSentView reactionView;
    private boolean shouldShowFullScreen;
    private final Lazy storyViewerPageViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryViewerPageViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.tabs.StoryViewsAndRepliesDialogFragment$storyViewerPageViewModel$2
        final /* synthetic */ StoryViewsAndRepliesDialogFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            Fragment requireParentFragment = this.this$0.requireParentFragment();
            Intrinsics.checkNotNullExpressionValue(requireParentFragment, "requireParentFragment()");
            return requireParentFragment;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.tabs.StoryViewsAndRepliesDialogFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    public int getThemeResId() {
        return R.style.Widget_Signal_FixedRoundedCorners_Stories;
    }

    private final long getStoryId() {
        return requireArguments().getLong(ARG_STORY_ID);
    }

    private final RecipientId getGroupRecipientId() {
        Parcelable parcelable = requireArguments().getParcelable(ARG_GROUP_RECIPIENT_ID);
        Intrinsics.checkNotNull(parcelable);
        return (RecipientId) parcelable;
    }

    private final int getStartPageIndex() {
        return requireArguments().getInt(ARG_START_PAGE);
    }

    private final boolean isFromNotification() {
        return requireArguments().getBoolean(ARG_IS_FROM_NOTIFICATION, false);
    }

    private final int getGroupReplyStartPosition() {
        return requireArguments().getInt(ARG_GROUP_REPLY_START_POSITION, -1);
    }

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    protected float getPeekHeightPercentage() {
        return this.peekHeightPercentage;
    }

    private final StoryViewerPageViewModel getStoryViewerPageViewModel() {
        return (StoryViewerPageViewModel) this.storyViewerPageViewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.StoryViewsAndRepliesPagerParent
    public StoryViewsAndRepliesPagerParent.Child getSelectedChild() {
        StoryViewsAndRepliesPagerParent.Child.Companion companion = StoryViewsAndRepliesPagerParent.Child.Companion;
        ViewPager2 viewPager2 = this.pager;
        if (viewPager2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("pager");
            viewPager2 = null;
        }
        return companion.forIndex(viewPager2.getCurrentItem());
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.stories_views_and_replies_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.pager);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.pager)");
        this.pager = (ViewPager2) findViewById;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        ViewPager2 viewPager2 = null;
        this.reactionView = new OnReactionSentView(requireContext, null, 2, null);
        ViewPager2 viewPager22 = this.pager;
        if (viewPager22 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("pager");
            viewPager22 = null;
        }
        FrameLayout frameLayout = (FrameLayout) viewPager22.getRootView().findViewById(R.id.container);
        OnReactionSentView onReactionSentView = this.reactionView;
        if (onReactionSentView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("reactionView");
            onReactionSentView = null;
        }
        frameLayout.addView(onReactionSentView);
        BottomSheetBehavior<FrameLayout> behavior = ((BottomSheetDialog) requireDialog()).getBehavior();
        Intrinsics.checkNotNullExpressionValue(behavior, "requireDialog() as BottomSheetDialog).behavior");
        behavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.tabs.StoryViewsAndRepliesDialogFragment$onViewCreated$1
            final /* synthetic */ StoryViewsAndRepliesDialogFragment this$0;

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onStateChanged(View view2, int i) {
                Intrinsics.checkNotNullParameter(view2, "bottomSheet");
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onSlide(View view2, float f) {
                Intrinsics.checkNotNullParameter(view2, "bottomSheet");
                List<Fragment> fragments = this.this$0.getChildFragmentManager().getFragments();
                Intrinsics.checkNotNullExpressionValue(fragments, "childFragmentManager.fragments");
                for (Fragment fragment : fragments) {
                    if (fragment instanceof BottomSheetBehaviorDelegate) {
                        ((BottomSheetBehaviorDelegate) fragment).onSlide(view2);
                    }
                }
            }
        });
        View findViewById2 = view.findViewById(R.id.tab_layout);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.tab_layout)");
        TabLayout tabLayout = (TabLayout) findViewById2;
        ViewCompat.setNestedScrollingEnabled(tabLayout, false);
        ViewPager2 viewPager23 = this.pager;
        if (viewPager23 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("pager");
            viewPager23 = null;
        }
        viewPager23.setAdapter(new StoryViewsAndRepliesPagerAdapter(this, getStoryId(), getGroupRecipientId(), isFromNotification(), getGroupReplyStartPosition()));
        ViewPager2 viewPager24 = this.pager;
        if (viewPager24 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("pager");
            viewPager24 = null;
        }
        viewPager24.setCurrentItem(getStartPageIndex(), false);
        ViewPager2 viewPager25 = this.pager;
        if (viewPager25 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("pager");
        } else {
            viewPager2 = viewPager25;
        }
        new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.tabs.StoryViewsAndRepliesDialogFragment$$ExternalSyntheticLambda0
            @Override // com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
            public final void onConfigureTab(TabLayout.Tab tab, int i) {
                StoryViewsAndRepliesDialogFragment.m3185onViewCreated$lambda0(tab, i);
            }
        }).attach();
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(view) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.tabs.StoryViewsAndRepliesDialogFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                StoryViewsAndRepliesDialogFragment.m3186onViewCreated$lambda2(StoryViewsAndRepliesDialogFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m3185onViewCreated$lambda0(TabLayout.Tab tab, int i) {
        Intrinsics.checkNotNullParameter(tab, "tab");
        if (i == 0) {
            tab.setText(R.string.StoryViewsAndRepliesDialogFragment__views);
        } else if (i == 1) {
            tab.setText(R.string.StoryViewsAndRepliesDialogFragment__replies);
        }
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m3186onViewCreated$lambda2(StoryViewsAndRepliesDialogFragment storyViewsAndRepliesDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(storyViewsAndRepliesDialogFragment, "this$0");
        Intrinsics.checkNotNullParameter(view, "$view");
        int height = BottomSheetUtil.INSTANCE.requireCoordinatorLayout(storyViewsAndRepliesDialogFragment).getHeight();
        int i = MathKt__MathJVMKt.roundToInt(((float) storyViewsAndRepliesDialogFragment.getResources().getDisplayMetrics().heightPixels) * 0.6f);
        if (storyViewsAndRepliesDialogFragment.initialParentHeight == 0) {
            storyViewsAndRepliesDialogFragment.initialParentHeight = height;
        }
        if (height == 0) {
            height = i;
        } else if (!storyViewsAndRepliesDialogFragment.shouldShowFullScreen && height == storyViewsAndRepliesDialogFragment.initialParentHeight) {
            height = Math.min(height, i);
        }
        if (view.getHeight() != height) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams != null) {
                layoutParams.height = height;
                view.setLayoutParams(layoutParams);
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup.LayoutParams");
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ViewPager2 viewPager2 = this.pager;
        if (viewPager2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("pager");
            viewPager2 = null;
        }
        viewPager2.registerOnPageChangeCallback(this.onPageChangeCallback);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        ViewPager2 viewPager2 = this.pager;
        if (viewPager2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("pager");
            viewPager2 = null;
        }
        viewPager2.unregisterOnPageChangeCallback(this.onPageChangeCallback);
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        getStoryViewerPageViewModel().setIsDisplayingViewsAndRepliesDialog(false);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.Callback
    public void onStartDirectReply(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        dismiss();
        getStoryViewerPageViewModel().startDirectReply(getStoryId(), recipientId);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.Callback
    public void requestFullScreen(boolean z) {
        this.shouldShowFullScreen = z;
        requireView().invalidate();
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.Callback
    public void onReactionEmojiSelected(String str) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        OnReactionSentView onReactionSentView = this.reactionView;
        if (onReactionSentView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("reactionView");
            onReactionSentView = null;
        }
        onReactionSentView.playForEmoji(str);
    }

    /* compiled from: StoryViewsAndRepliesDialogFragment.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0006H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/tabs/StoryViewsAndRepliesDialogFragment$PageChangeCallback;", "Landroidx/viewpager2/widget/ViewPager2$OnPageChangeCallback;", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/tabs/StoryViewsAndRepliesDialogFragment;)V", "onPageScrollStateChanged", "", "state", "", "onPageSelected", "position", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public final class PageChangeCallback extends ViewPager2.OnPageChangeCallback {
        public PageChangeCallback() {
            StoryViewsAndRepliesDialogFragment.this = r1;
        }

        @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
        public void onPageScrollStateChanged(int i) {
            if (i == 0) {
                ViewPager2 viewPager2 = StoryViewsAndRepliesDialogFragment.this.pager;
                if (viewPager2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("pager");
                    viewPager2 = null;
                }
                viewPager2.requestLayout();
            }
        }

        @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
        public void onPageSelected(int i) {
            ViewPager2 viewPager2 = StoryViewsAndRepliesDialogFragment.this.pager;
            if (viewPager2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("pager");
                viewPager2 = null;
            }
            viewPager2.post(new StoryViewsAndRepliesDialogFragment$PageChangeCallback$$ExternalSyntheticLambda0(StoryViewsAndRepliesDialogFragment.this, i));
        }

        /* renamed from: onPageSelected$lambda-1 */
        public static final void m3188onPageSelected$lambda1(StoryViewsAndRepliesDialogFragment storyViewsAndRepliesDialogFragment, int i) {
            Intrinsics.checkNotNullParameter(storyViewsAndRepliesDialogFragment, "this$0");
            List<Fragment> fragments = storyViewsAndRepliesDialogFragment.getChildFragmentManager().getFragments();
            Intrinsics.checkNotNullExpressionValue(fragments, "childFragmentManager.fragments");
            for (Fragment fragment : fragments) {
                if (fragment instanceof StoryViewsAndRepliesPagerChild) {
                    ((StoryViewsAndRepliesPagerChild) fragment).onPageSelected(StoryViewsAndRepliesPagerParent.Child.Companion.forIndex(i));
                }
                if (fragment instanceof BottomSheetBehaviorDelegate) {
                    BottomSheetBehaviorDelegate bottomSheetBehaviorDelegate = (BottomSheetBehaviorDelegate) fragment;
                    ViewParent parent = storyViewsAndRepliesDialogFragment.requireView().getParent();
                    if (parent != null) {
                        bottomSheetBehaviorDelegate.onSlide((View) parent);
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type android.view.View");
                    }
                }
            }
        }
    }

    /* compiled from: StoryViewsAndRepliesDialogFragment.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J.\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/tabs/StoryViewsAndRepliesDialogFragment$Companion;", "", "()V", "ARG_GROUP_RECIPIENT_ID", "", "ARG_GROUP_REPLY_START_POSITION", "ARG_IS_FROM_NOTIFICATION", "ARG_START_PAGE", "ARG_STORY_ID", "create", "Landroidx/fragment/app/DialogFragment;", "storyId", "", "groupRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "startPage", "Lorg/thoughtcrime/securesms/stories/viewer/reply/tabs/StoryViewsAndRepliesDialogFragment$StartPage;", "isFromNotification", "", "groupReplyStartPosition", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final DialogFragment create(long j, RecipientId recipientId, StartPage startPage, boolean z, int i) {
            Intrinsics.checkNotNullParameter(recipientId, "groupRecipientId");
            Intrinsics.checkNotNullParameter(startPage, "startPage");
            StoryViewsAndRepliesDialogFragment storyViewsAndRepliesDialogFragment = new StoryViewsAndRepliesDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(StoryViewsAndRepliesDialogFragment.ARG_STORY_ID, j);
            bundle.putInt(StoryViewsAndRepliesDialogFragment.ARG_START_PAGE, startPage.getIndex());
            bundle.putParcelable(StoryViewsAndRepliesDialogFragment.ARG_GROUP_RECIPIENT_ID, recipientId);
            bundle.putBoolean(StoryViewsAndRepliesDialogFragment.ARG_IS_FROM_NOTIFICATION, z);
            bundle.putInt(StoryViewsAndRepliesDialogFragment.ARG_GROUP_REPLY_START_POSITION, i);
            storyViewsAndRepliesDialogFragment.setArguments(bundle);
            return storyViewsAndRepliesDialogFragment;
        }
    }

    /* compiled from: StoryViewsAndRepliesDialogFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/tabs/StoryViewsAndRepliesDialogFragment$StartPage;", "", "index", "", "(Ljava/lang/String;II)V", "getIndex", "()I", "VIEWS", "REPLIES", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum StartPage {
        VIEWS(0),
        REPLIES(1);
        
        private final int index;

        StartPage(int i) {
            this.index = i;
        }

        public final int getIndex() {
            return this.index;
        }
    }
}
