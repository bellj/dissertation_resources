package org.thoughtcrime.securesms.stories.viewer.info;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* compiled from: StoryInfoRepository.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u000e2\u00020\u0001:\u0002\u000e\u000fB\u0005¢\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u0007\u001a\u00020\bJ\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\n2\u0006\u0010\u0007\u001a\u00020\bH\u0002¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRepository;", "", "()V", "getReceiptInfo", "Lio/reactivex/rxjava3/core/Single;", "", "Lorg/thoughtcrime/securesms/database/GroupReceiptDatabase$GroupReceiptInfo;", "storyId", "", "getStoryInfo", "Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRepository$StoryInfo;", "observeMessageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "Companion", "StoryInfo", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryInfoRepository {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(StoryInfoRepository.class);

    /* compiled from: StoryInfoRepository.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRepository$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final Observable<StoryInfo> getStoryInfo(long j) {
        Observable<StoryInfo> subscribeOn = observeMessageRecord(j).switchMap(new Function(j) { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryInfoRepository.$r8$lambda$0SfPgsntVstJ7U4uevdFRvK9tIY(StoryInfoRepository.this, this.f$1, (MessageRecord) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "observeMessageRecord(sto…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getStoryInfo$lambda-1 */
    public static final ObservableSource m3023getStoryInfo$lambda1(StoryInfoRepository storyInfoRepository, long j, MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(storyInfoRepository, "this$0");
        return storyInfoRepository.getReceiptInfo(j).map(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRepository$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryInfoRepository.m3021$r8$lambda$j94zZMOf8tQk18C_qo1Jtus1jg(MessageRecord.this, (List) obj);
            }
        }).toObservable();
    }

    /* renamed from: getStoryInfo$lambda-1$lambda-0 */
    public static final StoryInfo m3024getStoryInfo$lambda1$lambda0(MessageRecord messageRecord, List list) {
        Intrinsics.checkNotNullExpressionValue(messageRecord, "record");
        Intrinsics.checkNotNullExpressionValue(list, "receiptInfo");
        return new StoryInfo(messageRecord, list);
    }

    private final Observable<MessageRecord> observeMessageRecord(long j) {
        Observable<MessageRecord> create = Observable.create(new ObservableOnSubscribe(j) { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                StoryInfoRepository.m3020$r8$lambda$O1yqDwBFVtEq0PN9sTMKcX4DA(this.f$0, observableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create { emitter ->\n    … }\n\n      refresh()\n    }");
        return create;
    }

    /* renamed from: observeMessageRecord$lambda-4$refresh */
    private static final void m3028observeMessageRecord$lambda4$refresh(ObservableEmitter<MessageRecord> observableEmitter, long j) {
        try {
            observableEmitter.onNext(SignalDatabase.Companion.mms().getMessageRecord(j));
        } catch (NoSuchMessageException unused) {
            Log.w(TAG, "The story message disappeared. Terminating emission.");
            observableEmitter.onComplete();
        }
    }

    /* renamed from: observeMessageRecord$lambda-4 */
    public static final void m3025observeMessageRecord$lambda4(long j, ObservableEmitter observableEmitter) {
        StoryInfoRepository$$ExternalSyntheticLambda0 storyInfoRepository$$ExternalSyntheticLambda0 = new DatabaseObserver.MessageObserver(j, observableEmitter) { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;
            public final /* synthetic */ ObservableEmitter f$1;

            {
                this.f$0 = r1;
                this.f$1 = r3;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.MessageObserver
            public final void onMessageChanged(MessageId messageId) {
                StoryInfoRepository.m3018$r8$lambda$KcLdp6eKnjOotLeOpb2y2yxoF4(this.f$0, this.f$1, messageId);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerMessageUpdateObserver(storyInfoRepository$$ExternalSyntheticLambda0);
        observableEmitter.setCancellable(new Cancellable() { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                StoryInfoRepository.m3019$r8$lambda$7vIDOgG2ObP1776lLLXZ6amjw(DatabaseObserver.MessageObserver.this);
            }
        });
        m3028observeMessageRecord$lambda4$refresh(observableEmitter, j);
    }

    /* renamed from: observeMessageRecord$lambda-4$lambda-2 */
    public static final void m3026observeMessageRecord$lambda4$lambda2(long j, ObservableEmitter observableEmitter, MessageId messageId) {
        Intrinsics.checkNotNullParameter(messageId, "it");
        if (messageId.isMms() && messageId.getId() == j) {
            m3028observeMessageRecord$lambda4$refresh(observableEmitter, j);
        }
    }

    /* renamed from: observeMessageRecord$lambda-4$lambda-3 */
    public static final void m3027observeMessageRecord$lambda4$lambda3(DatabaseObserver.MessageObserver messageObserver) {
        Intrinsics.checkNotNullParameter(messageObserver, "$observer");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(messageObserver);
    }

    private final Single<List<GroupReceiptDatabase.GroupReceiptInfo>> getReceiptInfo(long j) {
        Single<List<GroupReceiptDatabase.GroupReceiptInfo>> fromCallable = Single.fromCallable(new Callable(j) { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return StoryInfoRepository.$r8$lambda$vcAwD6IVNVMDTqhFfz59uX2SuZE(this.f$0);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n      Sig…eceiptInfo(storyId)\n    }");
        return fromCallable;
    }

    /* renamed from: getReceiptInfo$lambda-5 */
    public static final List m3022getReceiptInfo$lambda5(long j) {
        return SignalDatabase.Companion.groupReceipts().getGroupReceiptInfo(j);
    }

    /* compiled from: StoryInfoRepository.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRepository$StoryInfo;", "", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "receiptInfo", "", "Lorg/thoughtcrime/securesms/database/GroupReceiptDatabase$GroupReceiptInfo;", "(Lorg/thoughtcrime/securesms/database/model/MessageRecord;Ljava/util/List;)V", "getMessageRecord", "()Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "getReceiptInfo", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class StoryInfo {
        private final MessageRecord messageRecord;
        private final List<GroupReceiptDatabase.GroupReceiptInfo> receiptInfo;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRepository$StoryInfo */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoryInfo copy$default(StoryInfo storyInfo, MessageRecord messageRecord, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                messageRecord = storyInfo.messageRecord;
            }
            if ((i & 2) != 0) {
                list = storyInfo.receiptInfo;
            }
            return storyInfo.copy(messageRecord, list);
        }

        public final MessageRecord component1() {
            return this.messageRecord;
        }

        public final List<GroupReceiptDatabase.GroupReceiptInfo> component2() {
            return this.receiptInfo;
        }

        public final StoryInfo copy(MessageRecord messageRecord, List<? extends GroupReceiptDatabase.GroupReceiptInfo> list) {
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            Intrinsics.checkNotNullParameter(list, "receiptInfo");
            return new StoryInfo(messageRecord, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoryInfo)) {
                return false;
            }
            StoryInfo storyInfo = (StoryInfo) obj;
            return Intrinsics.areEqual(this.messageRecord, storyInfo.messageRecord) && Intrinsics.areEqual(this.receiptInfo, storyInfo.receiptInfo);
        }

        public int hashCode() {
            return (this.messageRecord.hashCode() * 31) + this.receiptInfo.hashCode();
        }

        public String toString() {
            return "StoryInfo(messageRecord=" + this.messageRecord + ", receiptInfo=" + this.receiptInfo + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.database.GroupReceiptDatabase$GroupReceiptInfo> */
        /* JADX WARN: Multi-variable type inference failed */
        public StoryInfo(MessageRecord messageRecord, List<? extends GroupReceiptDatabase.GroupReceiptInfo> list) {
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            Intrinsics.checkNotNullParameter(list, "receiptInfo");
            this.messageRecord = messageRecord;
            this.receiptInfo = list;
        }

        public final MessageRecord getMessageRecord() {
            return this.messageRecord;
        }

        public final List<GroupReceiptDatabase.GroupReceiptInfo> getReceiptInfo() {
            return this.receiptInfo;
        }
    }
}
