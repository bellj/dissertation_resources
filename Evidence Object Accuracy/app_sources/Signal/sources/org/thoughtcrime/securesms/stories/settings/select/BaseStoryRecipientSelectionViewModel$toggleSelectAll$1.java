package org.thoughtcrime.securesms.stories.settings.select;

import com.annimon.stream.function.Function;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: BaseStoryRecipientSelectionViewModel.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "allSignalRecipients", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BaseStoryRecipientSelectionViewModel$toggleSelectAll$1 extends Lambda implements Function1<Set<? extends RecipientId>, Unit> {
    final /* synthetic */ BaseStoryRecipientSelectionViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BaseStoryRecipientSelectionViewModel$toggleSelectAll$1(BaseStoryRecipientSelectionViewModel baseStoryRecipientSelectionViewModel) {
        super(1);
        this.this$0 = baseStoryRecipientSelectionViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Set<? extends RecipientId> set) {
        invoke(set);
        return Unit.INSTANCE;
    }

    /* renamed from: invoke$lambda-0 */
    public static final BaseStoryRecipientSelectionState m2953invoke$lambda0(Set set, BaseStoryRecipientSelectionState baseStoryRecipientSelectionState) {
        Intrinsics.checkNotNullParameter(set, "$allSignalRecipients");
        Intrinsics.checkNotNullExpressionValue(baseStoryRecipientSelectionState, "it");
        return BaseStoryRecipientSelectionState.copy$default(baseStoryRecipientSelectionState, null, null, set, 3, null);
    }

    public final void invoke(Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(set, "allSignalRecipients");
        this.this$0.store.update(new Function(set) { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionViewModel$toggleSelectAll$1$$ExternalSyntheticLambda0
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BaseStoryRecipientSelectionViewModel$toggleSelectAll$1.m2953invoke$lambda0(this.f$0, (BaseStoryRecipientSelectionState) obj);
            }
        });
    }
}
