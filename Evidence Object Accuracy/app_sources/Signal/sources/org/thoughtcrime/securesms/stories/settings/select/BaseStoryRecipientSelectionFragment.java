package org.thoughtcrime.securesms.stories.settings.select;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import com.google.android.material.button.MaterialButton;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.HeaderAction;
import org.thoughtcrime.securesms.contacts.SelectedContact;
import org.thoughtcrime.securesms.contacts.selection.ContactSelectionArguments;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sharing.ShareContact;
import org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionViewModel;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: BaseStoryRecipientSelectionFragment.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b&\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0001>B\u0007¢\u0006\u0004\b<\u0010=J\b\u0010\u0005\u001a\u00020\u0004H\u0002J\b\u0010\u0007\u001a\u00020\u0006H\u0002J\b\u0010\b\u001a\u00020\u0006H\u0002J\u001a\u0010\r\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0016J\u0018\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0010H\u0014J\u0016\u0010\u0016\u001a\u00020\u00062\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013H\u0014J.\u0010\u001e\u001a\u00020\u00062\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00140\u00172\b\u0010\u001a\u001a\u0004\u0018\u00010\u00192\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001bH\u0016J \u0010\u001f\u001a\u00020\u00062\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00140\u00172\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019H\u0016J\b\u0010 \u001a\u00020\u0006H\u0016J\b\u0010\"\u001a\u00020!H\u0016R\u001b\u0010(\u001a\u00020#8BX\u0002¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u0014\u0010*\u001a\u00020)8\u0002X\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u001a\u0010,\u001a\u00020\u00108\u0014XD¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R\u001a\u00100\u001a\u00020\u00108\u0014XD¢\u0006\f\n\u0004\b0\u0010-\u001a\u0004\b1\u0010/R\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X.¢\u0006\u0006\n\u0004\b\u000f\u00102R\u0016\u00104\u001a\u0002038\u0002@\u0002X.¢\u0006\u0006\n\u0004\b4\u00105R\u0014\u00107\u001a\u00020\u00108&X¦\u0004¢\u0006\u0006\u001a\u0004\b6\u0010/R\u0016\u0010;\u001a\u0004\u0018\u0001088&X¦\u0004¢\u0006\u0006\u001a\u0004\b9\u0010:¨\u0006?"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/ContactSelectionListFragment$OnContactSelectedListener;", "Lorg/thoughtcrime/securesms/ContactSelectionListFragment$HeaderActionProvider;", "Lorg/thoughtcrime/securesms/ContactSelectionListFragment;", "getAttachedContactSelectionFragment", "", "exitFlow", "initializeContactSelectionFragment", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "Landroidx/appcompat/widget/Toolbar;", "toolbar", "", MediaPreviewActivity.SIZE_EXTRA, "presentTitle", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "goToNextScreen", "j$/util/Optional", "recipientId", "", "number", "j$/util/function/Consumer", "", "callback", "onBeforeContactSelected", "onContactDeselected", "onSelectionChanged", "Lorg/thoughtcrime/securesms/contacts/HeaderAction;", "getHeaderAction", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel;", "viewModel", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "toolbarTitleId", "I", "getToolbarTitleId", "()I", "checkboxResource", "getCheckboxResource", "Landroidx/appcompat/widget/Toolbar;", "Landroid/widget/EditText;", "searchField", "Landroid/widget/EditText;", "getActionButtonLabel", "actionButtonLabel", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getDistributionListId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "distributionListId", "<init>", "()V", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public abstract class BaseStoryRecipientSelectionFragment extends Fragment implements ContactSelectionListFragment.OnContactSelectedListener, ContactSelectionListFragment.HeaderActionProvider {
    private final int checkboxResource = R.drawable.contact_selection_checkbox;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private EditText searchField;
    private Toolbar toolbar;
    private final int toolbarTitleId = R.string.CreateStoryViewerSelectionFragment__choose_viewers;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(BaseStoryRecipientSelectionViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$viewModel$2
        final /* synthetic */ BaseStoryRecipientSelectionFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new BaseStoryRecipientSelectionViewModel.Factory(this.this$0.getDistributionListId(), new BaseStoryRecipientSelectionRepository());
        }
    });

    /* compiled from: BaseStoryRecipientSelectionFragment.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionFragment$Callback;", "", "exitFlow", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void exitFlow();
    }

    public abstract int getActionButtonLabel();

    public abstract DistributionListId getDistributionListId();

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onSelectionChanged() {
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:17:0x001b */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v4, types: [org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$Callback] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARN: Type inference failed for: r0v11 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void exitFlow() {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.getParentFragment()
        L_0x0004:
            if (r0 == 0) goto L_0x0010
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment.Callback
            if (r1 == 0) goto L_0x000b
            goto L_0x001b
        L_0x000b:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x0004
        L_0x0010:
            androidx.fragment.app.FragmentActivity r0 = r2.requireActivity()
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment.Callback
            if (r1 != 0) goto L_0x0019
            r0 = 0
        L_0x0019:
            org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$Callback r0 = (org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment.Callback) r0
        L_0x001b:
            org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$Callback r0 = (org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment.Callback) r0
            if (r0 != 0) goto L_0x0027
            androidx.navigation.NavController r0 = androidx.navigation.fragment.FragmentKt.findNavController(r2)
            r0.popBackStack()
            goto L_0x002a
        L_0x0027:
            r0.exitFlow()
        L_0x002a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment.exitFlow():void");
    }

    public BaseStoryRecipientSelectionFragment() {
        super(R.layout.stories_base_recipient_selection_fragment);
    }

    private final BaseStoryRecipientSelectionViewModel getViewModel() {
        return (BaseStoryRecipientSelectionViewModel) this.viewModel$delegate.getValue();
    }

    protected int getToolbarTitleId() {
        return this.toolbarTitleId;
    }

    protected int getCheckboxResource() {
        return this.checkboxResource;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.action_button);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.action_button)");
        MaterialButton materialButton = (MaterialButton) findViewById;
        View findViewById2 = view.findViewById(R.id.search_field);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.search_field)");
        this.searchField = (EditText) findViewById2;
        View findViewById3 = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.toolbar)");
        Toolbar toolbar = (Toolbar) findViewById3;
        this.toolbar = toolbar;
        EditText editText = null;
        if (toolbar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("toolbar");
            toolbar = null;
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseStoryRecipientSelectionFragment.m2938onViewCreated$lambda0(BaseStoryRecipientSelectionFragment.this, view2);
            }
        });
        Toolbar toolbar2 = this.toolbar;
        if (toolbar2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("toolbar");
            toolbar2 = null;
        }
        toolbar2.setTitle(getToolbarTitleId());
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        materialButton.setText(getActionButtonLabel());
        EditText editText2 = this.searchField;
        if (editText2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("searchField");
        } else {
            editText = editText2;
        }
        editText.addTextChangedListener(new TextWatcher(this) { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$onViewCreated$$inlined$doAfterTextChanged$1
            final /* synthetic */ BaseStoryRecipientSelectionFragment this$0;

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            {
                this.this$0 = r1;
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                ContactSelectionListFragment contactSelectionListFragment = this.this$0.getAttachedContactSelectionFragment();
                if (editable == null || editable.length() == 0) {
                    contactSelectionListFragment.resetQueryFilter();
                } else {
                    contactSelectionListFragment.setQueryFilter(editable.toString());
                }
            }
        });
        materialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseStoryRecipientSelectionFragment.m2939onViewCreated$lambda2(BaseStoryRecipientSelectionFragment.this, view2);
            }
        });
        if (getChildFragmentManager().findFragmentById(R.id.fragment_container) == null) {
            initializeContactSelectionFragment();
        }
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BaseStoryRecipientSelectionFragment.m2940onViewCreated$lambda3(BaseStoryRecipientSelectionFragment.this, (BaseStoryRecipientSelectionState) obj);
            }
        });
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getActionObservable().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                BaseStoryRecipientSelectionFragment.m2941onViewCreated$lambda5(BaseStoryRecipientSelectionFragment.this, (BaseStoryRecipientSelectionViewModel.Action) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.actionObservab…)\n        )\n      }\n    }");
        lifecycleDisposable2.plusAssign(subscribe);
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2938onViewCreated$lambda0(BaseStoryRecipientSelectionFragment baseStoryRecipientSelectionFragment, View view) {
        Intrinsics.checkNotNullParameter(baseStoryRecipientSelectionFragment, "this$0");
        baseStoryRecipientSelectionFragment.exitFlow();
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m2939onViewCreated$lambda2(BaseStoryRecipientSelectionFragment baseStoryRecipientSelectionFragment, View view) {
        Intrinsics.checkNotNullParameter(baseStoryRecipientSelectionFragment, "this$0");
        baseStoryRecipientSelectionFragment.getViewModel().onAction();
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2940onViewCreated$lambda3(BaseStoryRecipientSelectionFragment baseStoryRecipientSelectionFragment, BaseStoryRecipientSelectionState baseStoryRecipientSelectionState) {
        Intrinsics.checkNotNullParameter(baseStoryRecipientSelectionFragment, "this$0");
        if (baseStoryRecipientSelectionState.getDistributionListId() == null || baseStoryRecipientSelectionState.getPrivateStory() != null) {
            ContactSelectionListFragment attachedContactSelectionFragment = baseStoryRecipientSelectionFragment.getAttachedContactSelectionFragment();
            Set<RecipientId> selection = baseStoryRecipientSelectionState.getSelection();
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(selection, 10));
            for (RecipientId recipientId : selection) {
                arrayList.add(new ShareContact(recipientId));
            }
            attachedContactSelectionFragment.markSelected(CollectionsKt___CollectionsKt.toSet(arrayList));
            Toolbar toolbar = baseStoryRecipientSelectionFragment.toolbar;
            if (toolbar == null) {
                Intrinsics.throwUninitializedPropertyAccessException("toolbar");
                toolbar = null;
            }
            baseStoryRecipientSelectionFragment.presentTitle(toolbar, baseStoryRecipientSelectionState.getSelection().size());
        }
    }

    /* renamed from: onViewCreated$lambda-5 */
    public static final void m2941onViewCreated$lambda5(BaseStoryRecipientSelectionFragment baseStoryRecipientSelectionFragment, BaseStoryRecipientSelectionViewModel.Action action) {
        Intrinsics.checkNotNullParameter(baseStoryRecipientSelectionFragment, "this$0");
        if (action instanceof BaseStoryRecipientSelectionViewModel.Action.ExitFlow) {
            baseStoryRecipientSelectionFragment.exitFlow();
        } else if (action instanceof BaseStoryRecipientSelectionViewModel.Action.GoToNextScreen) {
            List<SelectedContact> selectedContacts = baseStoryRecipientSelectionFragment.getAttachedContactSelectionFragment().getSelectedContacts();
            Intrinsics.checkNotNullExpressionValue(selectedContacts, "getAttachedContactSelect…agment().selectedContacts");
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(selectedContacts, 10));
            for (SelectedContact selectedContact : selectedContacts) {
                arrayList.add(selectedContact.getOrCreateRecipientId(baseStoryRecipientSelectionFragment.requireContext()));
            }
            baseStoryRecipientSelectionFragment.goToNextScreen(CollectionsKt___CollectionsKt.toSet(arrayList));
        }
    }

    protected void presentTitle(Toolbar toolbar, int i) {
        Intrinsics.checkNotNullParameter(toolbar, "toolbar");
        if (i == 0) {
            toolbar.setTitle(R.string.CreateStoryViewerSelectionFragment__choose_viewers);
        } else {
            toolbar.setTitle(getResources().getQuantityString(R.plurals.SelectViewersFragment__d_viewers, i, Integer.valueOf(i)));
        }
    }

    public final ContactSelectionListFragment getAttachedContactSelectionFragment() {
        Fragment findFragmentById = getChildFragmentManager().findFragmentById(R.id.contacts_container);
        if (findFragmentById != null) {
            return (ContactSelectionListFragment) findFragmentById;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.ContactSelectionListFragment");
    }

    protected void goToNextScreen(Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(set, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onBeforeContactSelected(Optional<RecipientId> optional, String str, j$.util.function.Consumer<Boolean> consumer) {
        Intrinsics.checkNotNullParameter(optional, "recipientId");
        Intrinsics.checkNotNullParameter(consumer, "callback");
        BaseStoryRecipientSelectionViewModel viewModel = getViewModel();
        RecipientId recipientId = optional.get();
        Intrinsics.checkNotNullExpressionValue(recipientId, "recipientId.get()");
        viewModel.addRecipient(recipientId);
        EditText editText = this.searchField;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("searchField");
            editText = null;
        }
        editText.setText("");
        consumer.accept(Boolean.TRUE);
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onContactDeselected(Optional<RecipientId> optional, String str) {
        Intrinsics.checkNotNullParameter(optional, "recipientId");
        BaseStoryRecipientSelectionViewModel viewModel = getViewModel();
        RecipientId recipientId = optional.get();
        Intrinsics.checkNotNullExpressionValue(recipientId, "recipientId.get()");
        viewModel.removeRecipient(recipientId);
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.HeaderActionProvider
    public HeaderAction getHeaderAction() {
        return new HeaderAction(R.string.BaseStoryRecipientSelectionFragment__select_all, new Runnable() { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                BaseStoryRecipientSelectionFragment.m2937getHeaderAction$lambda6(BaseStoryRecipientSelectionFragment.this);
            }
        });
    }

    /* renamed from: getHeaderAction$lambda-6 */
    public static final void m2937getHeaderAction$lambda6(BaseStoryRecipientSelectionFragment baseStoryRecipientSelectionFragment) {
        Intrinsics.checkNotNullParameter(baseStoryRecipientSelectionFragment, "this$0");
        baseStoryRecipientSelectionFragment.getViewModel().toggleSelectAll();
    }

    private final void initializeContactSelectionFragment() {
        ContactSelectionListFragment contactSelectionListFragment = new ContactSelectionListFragment();
        contactSelectionListFragment.setArguments(new ContactSelectionArguments(65, false, false, SelectionLimits.NO_LIMITS, CollectionsKt__CollectionsKt.emptyList(), false, false, false, 0, false, getCheckboxResource(), 768, null).toArgumentBundle());
        getChildFragmentManager().beginTransaction().replace(R.id.contacts_container, contactSelectionListFragment).commitNowAllowingStateLoss();
    }
}
