package org.thoughtcrime.securesms.stories.my;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;

/* compiled from: MyStoriesState.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u0011B\u0015\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesState;", "", "distributionSets", "", "Lorg/thoughtcrime/securesms/stories/my/MyStoriesState$DistributionSet;", "(Ljava/util/List;)V", "getDistributionSets", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "DistributionSet", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStoriesState {
    private final List<DistributionSet> distributionSets;

    public MyStoriesState() {
        this(null, 1, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.stories.my.MyStoriesState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ MyStoriesState copy$default(MyStoriesState myStoriesState, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = myStoriesState.distributionSets;
        }
        return myStoriesState.copy(list);
    }

    public final List<DistributionSet> component1() {
        return this.distributionSets;
    }

    public final MyStoriesState copy(List<DistributionSet> list) {
        Intrinsics.checkNotNullParameter(list, "distributionSets");
        return new MyStoriesState(list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof MyStoriesState) && Intrinsics.areEqual(this.distributionSets, ((MyStoriesState) obj).distributionSets);
    }

    public int hashCode() {
        return this.distributionSets.hashCode();
    }

    public String toString() {
        return "MyStoriesState(distributionSets=" + this.distributionSets + ')';
    }

    public MyStoriesState(List<DistributionSet> list) {
        Intrinsics.checkNotNullParameter(list, "distributionSets");
        this.distributionSets = list;
    }

    public /* synthetic */ MyStoriesState(List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list);
    }

    public final List<DistributionSet> getDistributionSets() {
        return this.distributionSets;
    }

    /* compiled from: MyStoriesState.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J%\u0010\u000e\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesState$DistributionSet;", "", EmojiSearchDatabase.LABEL, "", "stories", "", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "(Ljava/lang/String;Ljava/util/List;)V", "getLabel", "()Ljava/lang/String;", "getStories", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class DistributionSet {
        private final String label;
        private final List<ConversationMessage> stories;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.stories.my.MyStoriesState$DistributionSet */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ DistributionSet copy$default(DistributionSet distributionSet, String str, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                str = distributionSet.label;
            }
            if ((i & 2) != 0) {
                list = distributionSet.stories;
            }
            return distributionSet.copy(str, list);
        }

        public final String component1() {
            return this.label;
        }

        public final List<ConversationMessage> component2() {
            return this.stories;
        }

        public final DistributionSet copy(String str, List<? extends ConversationMessage> list) {
            Intrinsics.checkNotNullParameter(list, "stories");
            return new DistributionSet(str, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DistributionSet)) {
                return false;
            }
            DistributionSet distributionSet = (DistributionSet) obj;
            return Intrinsics.areEqual(this.label, distributionSet.label) && Intrinsics.areEqual(this.stories, distributionSet.stories);
        }

        public int hashCode() {
            String str = this.label;
            return ((str == null ? 0 : str.hashCode()) * 31) + this.stories.hashCode();
        }

        public String toString() {
            return "DistributionSet(label=" + this.label + ", stories=" + this.stories + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.conversation.ConversationMessage> */
        /* JADX WARN: Multi-variable type inference failed */
        public DistributionSet(String str, List<? extends ConversationMessage> list) {
            Intrinsics.checkNotNullParameter(list, "stories");
            this.label = str;
            this.stories = list;
        }

        public final String getLabel() {
            return this.label;
        }

        public final List<ConversationMessage> getStories() {
            return this.stories;
        }
    }
}
