package org.thoughtcrime.securesms.stories.viewer.reply.group;

import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ StoryGroupReplyItem.Model f$0;
    public final /* synthetic */ StoryGroupReplyItem.BaseViewHolder f$1;

    public /* synthetic */ StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda0(StoryGroupReplyItem.Model model, StoryGroupReplyItem.BaseViewHolder baseViewHolder) {
        this.f$0 = model;
        this.f$1 = baseViewHolder;
    }

    @Override // java.lang.Runnable
    public final void run() {
        StoryGroupReplyItem.BaseViewHolder.m3150displayContextMenu$lambda3(this.f$0, this.f$1);
    }
}
