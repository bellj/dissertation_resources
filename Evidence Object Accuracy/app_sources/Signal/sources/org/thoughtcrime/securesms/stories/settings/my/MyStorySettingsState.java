package org.thoughtcrime.securesms.stories.settings.my;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: MyStorySettingsState.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u00052\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsState;", "", "myStoryPrivacyState", "Lorg/thoughtcrime/securesms/stories/settings/my/MyStoryPrivacyState;", "areRepliesAndReactionsEnabled", "", "(Lorg/thoughtcrime/securesms/stories/settings/my/MyStoryPrivacyState;Z)V", "getAreRepliesAndReactionsEnabled", "()Z", "getMyStoryPrivacyState", "()Lorg/thoughtcrime/securesms/stories/settings/my/MyStoryPrivacyState;", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStorySettingsState {
    private final boolean areRepliesAndReactionsEnabled;
    private final MyStoryPrivacyState myStoryPrivacyState;

    public MyStorySettingsState() {
        this(null, false, 3, null);
    }

    public static /* synthetic */ MyStorySettingsState copy$default(MyStorySettingsState myStorySettingsState, MyStoryPrivacyState myStoryPrivacyState, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            myStoryPrivacyState = myStorySettingsState.myStoryPrivacyState;
        }
        if ((i & 2) != 0) {
            z = myStorySettingsState.areRepliesAndReactionsEnabled;
        }
        return myStorySettingsState.copy(myStoryPrivacyState, z);
    }

    public final MyStoryPrivacyState component1() {
        return this.myStoryPrivacyState;
    }

    public final boolean component2() {
        return this.areRepliesAndReactionsEnabled;
    }

    public final MyStorySettingsState copy(MyStoryPrivacyState myStoryPrivacyState, boolean z) {
        Intrinsics.checkNotNullParameter(myStoryPrivacyState, "myStoryPrivacyState");
        return new MyStorySettingsState(myStoryPrivacyState, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MyStorySettingsState)) {
            return false;
        }
        MyStorySettingsState myStorySettingsState = (MyStorySettingsState) obj;
        return Intrinsics.areEqual(this.myStoryPrivacyState, myStorySettingsState.myStoryPrivacyState) && this.areRepliesAndReactionsEnabled == myStorySettingsState.areRepliesAndReactionsEnabled;
    }

    public int hashCode() {
        int hashCode = this.myStoryPrivacyState.hashCode() * 31;
        boolean z = this.areRepliesAndReactionsEnabled;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        return "MyStorySettingsState(myStoryPrivacyState=" + this.myStoryPrivacyState + ", areRepliesAndReactionsEnabled=" + this.areRepliesAndReactionsEnabled + ')';
    }

    public MyStorySettingsState(MyStoryPrivacyState myStoryPrivacyState, boolean z) {
        Intrinsics.checkNotNullParameter(myStoryPrivacyState, "myStoryPrivacyState");
        this.myStoryPrivacyState = myStoryPrivacyState;
        this.areRepliesAndReactionsEnabled = z;
    }

    public /* synthetic */ MyStorySettingsState(MyStoryPrivacyState myStoryPrivacyState, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? new MyStoryPrivacyState(null, 0, 3, null) : myStoryPrivacyState, (i & 2) != 0 ? false : z);
    }

    public final MyStoryPrivacyState getMyStoryPrivacyState() {
        return this.myStoryPrivacyState;
    }

    public final boolean getAreRepliesAndReactionsEnabled() {
        return this.areRepliesAndReactionsEnabled;
    }
}
