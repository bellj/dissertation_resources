package org.thoughtcrime.securesms.stories.viewer.reply.group;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import org.signal.paging.PagedDataSource;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody;

/* compiled from: StoryGroupReplyDataSource.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010!\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u0003H\u0016J&\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0002H\u0016J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\b\u0010\u0014\u001a\u00020\fH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyDataSource;", "Lorg/signal/paging/PagedDataSource;", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;", "parentStoryId", "", "(J)V", "getKey", "data", "load", "", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "", "length", "cancellationSignal", "Lorg/signal/paging/PagedDataSource$CancellationSignal;", "key", "readRowFromRecord", "record", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", MediaPreviewActivity.SIZE_EXTRA, "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryGroupReplyDataSource implements PagedDataSource<MessageId, ReplyBody> {
    private final long parentStoryId;

    public StoryGroupReplyDataSource(long j) {
        this.parentStoryId = j;
    }

    @Override // org.signal.paging.PagedDataSource
    public int size() {
        return SignalDatabase.Companion.mms().getNumberOfStoryReplies(this.parentStoryId);
    }

    @Override // org.signal.paging.PagedDataSource
    public List<ReplyBody> load(int i, int i2, PagedDataSource.CancellationSignal cancellationSignal) {
        Intrinsics.checkNotNullParameter(cancellationSignal, "cancellationSignal");
        ArrayList arrayList = new ArrayList(i2);
        Cursor storyReplies = SignalDatabase.Companion.mms().getStoryReplies(this.parentStoryId);
        try {
            storyReplies.moveToPosition(i - 1);
            MmsDatabase.Reader reader = new MmsDatabase.Reader(storyReplies);
            while (storyReplies.moveToNext() && storyReplies.getPosition() < i + i2) {
                MessageRecord current = reader.getCurrent();
                if (current != null) {
                    arrayList.add(readRowFromRecord((MmsMessageRecord) current));
                } else {
                    throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.model.MmsMessageRecord");
                }
            }
            Unit unit = Unit.INSTANCE;
            th = null;
            return arrayList;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public ReplyBody load(MessageId messageId) {
        Intrinsics.checkNotNullParameter(messageId, "key");
        MessageRecord messageRecord = SignalDatabase.Companion.mms().getMessageRecord(messageId.getId());
        if (messageRecord != null) {
            return readRowFromRecord((MmsMessageRecord) messageRecord);
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.model.MmsMessageRecord");
    }

    public MessageId getKey(ReplyBody replyBody) {
        Intrinsics.checkNotNullParameter(replyBody, "data");
        return replyBody.getKey();
    }

    private final ReplyBody readRowFromRecord(MmsMessageRecord mmsMessageRecord) {
        if (mmsMessageRecord.isRemoteDelete()) {
            return new ReplyBody.RemoteDelete(mmsMessageRecord);
        }
        if (MmsSmsColumns.Types.isStoryReaction(mmsMessageRecord.getType())) {
            return new ReplyBody.Reaction(mmsMessageRecord);
        }
        ConversationMessage createWithUnresolvedData = ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(ApplicationDependencies.getApplication(), mmsMessageRecord);
        Intrinsics.checkNotNullExpressionValue(createWithUnresolvedData, "createWithUnresolvedData…getApplication(), record)");
        return new ReplyBody.Text(createWithUnresolvedData);
    }
}
