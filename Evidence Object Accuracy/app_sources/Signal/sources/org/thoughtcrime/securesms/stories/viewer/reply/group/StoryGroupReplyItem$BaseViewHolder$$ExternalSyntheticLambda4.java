package org.thoughtcrime.securesms.stories.viewer.reply.group;

import android.view.View;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda4 implements View.OnClickListener {
    public final /* synthetic */ StoryGroupReplyItem.Model f$0;

    public /* synthetic */ StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda4(StoryGroupReplyItem.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        StoryGroupReplyItem.BaseViewHolder.m3148bind$lambda1(this.f$0, view);
    }
}
