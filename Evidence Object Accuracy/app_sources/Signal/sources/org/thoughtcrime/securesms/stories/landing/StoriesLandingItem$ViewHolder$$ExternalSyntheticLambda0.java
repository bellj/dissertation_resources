package org.thoughtcrime.securesms.stories.landing;

import android.view.View;
import org.thoughtcrime.securesms.stories.landing.StoriesLandingItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoriesLandingItem$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ StoriesLandingItem.Model f$0;
    public final /* synthetic */ StoriesLandingItem.ViewHolder f$1;

    public /* synthetic */ StoriesLandingItem$ViewHolder$$ExternalSyntheticLambda0(StoriesLandingItem.Model model, StoriesLandingItem.ViewHolder viewHolder) {
        this.f$0 = model;
        this.f$1 = viewHolder;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        StoriesLandingItem.ViewHolder.m2807setUpClickListeners$lambda4(this.f$0, this.f$1, view);
    }
}
