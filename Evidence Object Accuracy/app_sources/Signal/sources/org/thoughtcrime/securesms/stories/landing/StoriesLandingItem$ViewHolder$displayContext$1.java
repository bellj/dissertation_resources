package org.thoughtcrime.securesms.stories.landing;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.stories.landing.StoriesLandingItem;

/* compiled from: StoriesLandingItem.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoriesLandingItem$ViewHolder$displayContext$1 extends Lambda implements Function0<Unit> {
    final /* synthetic */ StoriesLandingItem.ViewHolder this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoriesLandingItem$ViewHolder$displayContext$1(StoriesLandingItem.ViewHolder viewHolder) {
        super(0);
        this.this$0 = viewHolder;
    }

    @Override // kotlin.jvm.functions.Function0
    public final void invoke() {
        this.this$0.itemView.setSelected(false);
    }
}
