package org.thoughtcrime.securesms.stories.my;

import android.view.View;
import org.thoughtcrime.securesms.stories.my.MyStoriesItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class MyStoriesItem$ViewHolder$$ExternalSyntheticLambda6 implements View.OnLongClickListener {
    public final /* synthetic */ MyStoriesItem.Model f$0;

    public /* synthetic */ MyStoriesItem$ViewHolder$$ExternalSyntheticLambda6(MyStoriesItem.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return MyStoriesItem.ViewHolder.m2841bind$lambda3(this.f$0, view);
    }
}
