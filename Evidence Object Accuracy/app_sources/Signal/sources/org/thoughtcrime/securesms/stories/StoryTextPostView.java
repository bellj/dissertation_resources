package org.thoughtcrime.securesms.stories;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.graphics.ColorUtils;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.fonts.TextFont;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel;
import org.thoughtcrime.securesms.mediasend.v2.text.TextAlignment;
import org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationState;
import org.thoughtcrime.securesms.mediasend.v2.text.TextStoryScale;
import org.thoughtcrime.securesms.mediasend.v2.text.TextStoryTextWatcher;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;

/* compiled from: StoryTextPostView.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u0013\u001a\u00020\u0014H\u0002J\u000e\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u001aJ\u0016\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\f0\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eJ\u0016\u0010\u001f\u001a\u00020\u00142\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u0007J\b\u0010#\u001a\u00020\fH\u0002J\u0006\u0010$\u001a\u00020\u0014J\u0006\u0010%\u001a\u00020\u0014J\u0006\u0010&\u001a\u00020\u0014J\u0010\u0010'\u001a\u00020\u00142\b\u0010(\u001a\u0004\u0018\u00010)J\u000e\u0010*\u001a\u00020\u00142\u0006\u0010(\u001a\u00020)J\u0010\u0010+\u001a\u00020\u00142\u0006\u0010,\u001a\u00020-H\u0002J\u0018\u0010.\u001a\u00020\u00142\u0006\u0010/\u001a\u0002002\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u0012\u00101\u001a\u00020\u00142\b\b\u0001\u00102\u001a\u00020\u0007H\u0002J\u001a\u00103\u001a\u00020\u00142\b\b\u0001\u00102\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u0010\u00104\u001a\u00020\u00142\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u00105\u001a\u00020\u00142\u0006\u00106\u001a\u00020\u0007H\u0002J\u0012\u00107\u001a\u00020\u00142\b\b\u0001\u00108\u001a\u000209H\u0002J\u000e\u0010:\u001a\u00020\u00142\u0006\u0010(\u001a\u00020)J\u0010\u0010;\u001a\u00020\u00142\u0006\u0010<\u001a\u00020\fH\u0002J\u000e\u0010=\u001a\u00020\u00142\u0006\u0010>\u001a\u00020?J\u0006\u0010@\u001a\u00020\u0014J\u0006\u0010A\u001a\u00020\u0014R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000¨\u0006B"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryTextPostView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "backgroundView", "Landroid/widget/ImageView;", "isPlaceholder", "", "linkPreviewView", "Lorg/thoughtcrime/securesms/stories/StoryLinkPreviewView;", "textAlignment", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextAlignment;", "textView", "Lorg/thoughtcrime/securesms/stories/StoryTextView;", "adjustLinkPreviewTranslationY", "", "bindFromCreationState", "state", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationState;", "bindFromStoryTextPost", "storyTextPost", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/StoryTextPost;", "bindLinkPreview", "Lorg/thoughtcrime/securesms/util/concurrent/ListenableFuture;", "linkPreview", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;", "bindLinkPreviewState", "linkPreviewState", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreviewViewModel$LinkPreviewState;", "hiddenVisibility", "canDisplayText", "hideCloseButton", "hidePostContent", "postAdjustLinkPreviewTranslationY", "setLinkPreviewClickListener", "onClickListener", "Landroid/view/View$OnClickListener;", "setLinkPreviewCloseListener", "setPostBackground", "drawable", "Landroid/graphics/drawable/Drawable;", "setText", DraftDatabase.Draft.TEXT, "", "setTextBackgroundColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "setTextColor", "setTextGravity", "setTextScale", "scalePercent", "setTextSize", "textSize", "", "setTextViewClickListener", "setTextVisible", "visible", "setTypeface", "typeface", "Landroid/graphics/Typeface;", "showCloseButton", "showPostContent", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryTextPostView extends ConstraintLayout {
    private final ImageView backgroundView;
    private boolean isPlaceholder;
    private final StoryLinkPreviewView linkPreviewView;
    private TextAlignment textAlignment;
    private final StoryTextView textView;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryTextPostView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryTextPostView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ StoryTextPostView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryTextPostView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        ViewGroup.inflate(context, R.layout.stories_text_post_view, this);
        View findViewById = findViewById(R.id.text_story_post_background);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.text_story_post_background)");
        this.backgroundView = (ImageView) findViewById;
        View findViewById2 = findViewById(R.id.text_story_post_text);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.text_story_post_text)");
        StoryTextView storyTextView = (StoryTextView) findViewById2;
        this.textView = storyTextView;
        View findViewById3 = findViewById(R.id.text_story_post_link_preview);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.text_story_post_link_preview)");
        this.linkPreviewView = (StoryLinkPreviewView) findViewById3;
        this.isPlaceholder = true;
        TextStoryTextWatcher.Companion.install(storyTextView);
    }

    public final void showCloseButton() {
        this.linkPreviewView.setCanClose(true);
    }

    public final void hideCloseButton() {
        this.linkPreviewView.setCanClose(false);
    }

    public final void setTypeface(Typeface typeface) {
        Intrinsics.checkNotNullParameter(typeface, "typeface");
        this.textView.setTypeface(typeface);
    }

    private final void setPostBackground(Drawable drawable) {
        this.backgroundView.setImageDrawable(drawable);
    }

    private final void setTextColor(int i, boolean z) {
        if (z) {
            this.textView.setTextColor(ColorUtils.setAlphaComponent(i, 153));
        } else {
            this.textView.setTextColor(i);
        }
    }

    private final void setText(CharSequence charSequence, boolean z) {
        this.isPlaceholder = z;
        this.textView.setText(charSequence);
    }

    private final void setTextSize(float f) {
        this.textView.setTextSize(0, f);
    }

    private final void setTextGravity(TextAlignment textAlignment) {
        this.textView.setGravity(textAlignment.getGravity());
    }

    private final void setTextScale(int i) {
        float convertToScale = TextStoryScale.INSTANCE.convertToScale(i);
        this.textView.setScaleX(convertToScale);
        this.textView.setScaleY(convertToScale);
    }

    private final void setTextVisible(boolean z) {
        ViewExtensionsKt.setVisible(this.textView, z);
    }

    private final void setTextBackgroundColor(int i) {
        this.textView.setWrappedBackgroundColor(i);
    }

    public final void bindFromCreationState(TextStoryPostCreationState textStoryPostCreationState) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationState, "state");
        this.textAlignment = textStoryPostCreationState.getTextAlignment();
        setPostBackground(textStoryPostCreationState.getBackgroundColor().getChatBubbleMask());
        String body = textStoryPostCreationState.getBody();
        boolean z = true;
        if (body.length() == 0) {
            body = getContext().getString(R.string.TextStoryPostCreationFragment__tap_to_add_text);
            Intrinsics.checkNotNullExpressionValue(body, "context.getString(R.stri…ragment__tap_to_add_text)");
        }
        if (textStoryPostCreationState.getTextFont().isAllCaps()) {
            String obj = body.toString();
            Locale locale = Locale.getDefault();
            Intrinsics.checkNotNullExpressionValue(locale, "getDefault()");
            body = obj.toUpperCase(locale);
            Intrinsics.checkNotNullExpressionValue(body, "this as java.lang.String).toUpperCase(locale)");
        }
        setText(body, textStoryPostCreationState.getBody().length() == 0);
        int textForegroundColor = textStoryPostCreationState.getTextForegroundColor();
        if (textStoryPostCreationState.getBody().length() != 0) {
            z = false;
        }
        setTextColor(textForegroundColor, z);
        setTextBackgroundColor(textStoryPostCreationState.getTextBackgroundColor());
        setTextGravity(textStoryPostCreationState.getTextAlignment());
        setTextScale(textStoryPostCreationState.getTextScale());
        postAdjustLinkPreviewTranslationY();
    }

    public final void bindFromStoryTextPost(StoryTextPost storyTextPost) {
        Intrinsics.checkNotNullParameter(storyTextPost, "storyTextPost");
        ViewExtensionsKt.setVisible(this, true);
        ViewExtensionsKt.setVisible(this.linkPreviewView, false);
        TextAlignment textAlignment = TextAlignment.CENTER;
        this.textAlignment = textAlignment;
        TextFont.Companion companion = TextFont.Companion;
        StoryTextPost.Style style = storyTextPost.getStyle();
        Intrinsics.checkNotNullExpressionValue(style, "storyTextPost.style");
        TextFont fromStyle = companion.fromStyle(style);
        ChatColors.Companion companion2 = ChatColors.Companion;
        ChatColors.Id.NotSet notSet = ChatColors.Id.NotSet.INSTANCE;
        ChatColor background = storyTextPost.getBackground();
        Intrinsics.checkNotNullExpressionValue(background, "storyTextPost.background");
        setPostBackground(companion2.forChatColor(notSet, background).getChatBubbleMask());
        if (fromStyle.isAllCaps()) {
            String body = storyTextPost.getBody();
            Intrinsics.checkNotNullExpressionValue(body, "storyTextPost.body");
            Locale locale = Locale.getDefault();
            Intrinsics.checkNotNullExpressionValue(locale, "getDefault()");
            String upperCase = body.toUpperCase(locale);
            Intrinsics.checkNotNullExpressionValue(upperCase, "this as java.lang.String).toUpperCase(locale)");
            setText(upperCase, false);
        } else {
            String body2 = storyTextPost.getBody();
            Intrinsics.checkNotNullExpressionValue(body2, "storyTextPost.body");
            setText(body2, false);
        }
        setTextColor(storyTextPost.getTextForegroundColor(), false);
        setTextBackgroundColor(storyTextPost.getTextBackgroundColor());
        setTextGravity(textAlignment);
        hideCloseButton();
        postAdjustLinkPreviewTranslationY();
    }

    public final ListenableFuture<Boolean> bindLinkPreview(LinkPreview linkPreview) {
        return this.linkPreviewView.bind(linkPreview, 8);
    }

    public final void bindLinkPreviewState(LinkPreviewViewModel.LinkPreviewState linkPreviewState, int i) {
        Intrinsics.checkNotNullParameter(linkPreviewState, "linkPreviewState");
        this.linkPreviewView.bind(linkPreviewState, i);
    }

    public final void postAdjustLinkPreviewTranslationY() {
        setTextVisible(canDisplayText());
        addOnLayoutChangeListener(new View.OnLayoutChangeListener(this) { // from class: org.thoughtcrime.securesms.stories.StoryTextPostView$postAdjustLinkPreviewTranslationY$$inlined$doOnNextLayout$1
            final /* synthetic */ StoryTextPostView this$0;

            {
                this.this$0 = r1;
            }

            @Override // android.view.View.OnLayoutChangeListener
            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                Intrinsics.checkNotNullParameter(view, "view");
                view.removeOnLayoutChangeListener(this);
                this.this$0.adjustLinkPreviewTranslationY();
            }
        });
    }

    public final void setTextViewClickListener(View.OnClickListener onClickListener) {
        Intrinsics.checkNotNullParameter(onClickListener, "onClickListener");
        setOnClickListener(onClickListener);
    }

    public final void setLinkPreviewCloseListener(View.OnClickListener onClickListener) {
        Intrinsics.checkNotNullParameter(onClickListener, "onClickListener");
        this.linkPreviewView.setOnCloseClickListener(onClickListener);
    }

    public final void setLinkPreviewClickListener(View.OnClickListener onClickListener) {
        this.linkPreviewView.setOnClickListener(onClickListener);
    }

    public final void showPostContent() {
        this.textView.setAlpha(1.0f);
        this.linkPreviewView.setAlpha(1.0f);
    }

    public final void hidePostContent() {
        this.textView.setAlpha(0.0f);
        this.linkPreviewView.setAlpha(0.0f);
    }

    private final boolean canDisplayText() {
        return !(this.linkPreviewView.getVisibility() == 0) || !this.isPlaceholder;
    }

    public final void adjustLinkPreviewTranslationY() {
        int measuredHeight = this.backgroundView.getMeasuredHeight();
        float measuredHeight2 = canDisplayText() ? ((float) this.textView.getMeasuredHeight()) * this.textView.getScaleY() : 0.0f;
        float f = (float) measuredHeight;
        float measuredHeight3 = (float) (ViewExtensionsKt.getVisible(this.linkPreviewView) ? this.linkPreviewView.getMeasuredHeight() : 0);
        if (f - measuredHeight2 >= measuredHeight3) {
            float f2 = (f - (measuredHeight3 + measuredHeight2)) / 2.0f;
            this.linkPreviewView.setTranslationY(-f2);
            this.textView.setTranslationY(((measuredHeight2 / 2.0f) + f2) - (((float) this.textView.getMeasuredHeight()) / 2.0f));
            return;
        }
        this.linkPreviewView.setTranslationY(0.0f);
        this.textView.setTranslationY((f / 2.0f) - (((float) this.textView.getMeasuredHeight()) / 2.0f));
    }
}
