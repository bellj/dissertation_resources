package org.thoughtcrime.securesms.stories.settings.select;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: BaseStoryRecipientSelectionViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001:\u0002,-B\u0019\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b*\u0010+J\b\u0010\u0003\u001a\u00020\u0002H\u0014J\u0006\u0010\u0004\u001a\u00020\u0002J\u000e\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005J\u000e\u0010\b\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005J\u0006\u0010\t\u001a\u00020\u0002R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\n8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0014\u0010\u000e\u001a\u00020\r8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00108\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R8\u0010\u0017\u001a&\u0012\f\u0012\n \u0016*\u0004\u0018\u00010\u00150\u0015 \u0016*\u0012\u0012\f\u0012\n \u0016*\u0004\u0018\u00010\u00150\u0015\u0018\u00010\u00140\u00148\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0014\u0010\u001a\u001a\u00020\u00198\u0002X\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR(\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00150\u001c8\u0006@\u0006X\u000e¢\u0006\u0012\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R(\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00110#8\u0006@\u0006X\u000e¢\u0006\u0012\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)¨\u0006."}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel;", "Landroidx/lifecycle/ViewModel;", "", "onCleared", "toggleSelectAll", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "recipientId", "addRecipient", "removeRecipient", "onAction", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionRepository;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionRepository;", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionState;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel$Action;", "kotlin.jvm.PlatformType", SmsDatabase.SUBJECT, "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "disposable", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "Lio/reactivex/rxjava3/core/Observable;", "actionObservable", "Lio/reactivex/rxjava3/core/Observable;", "getActionObservable", "()Lio/reactivex/rxjava3/core/Observable;", "setActionObservable", "(Lio/reactivex/rxjava3/core/Observable;)V", "Landroidx/lifecycle/LiveData;", "state", "Landroidx/lifecycle/LiveData;", "getState", "()Landroidx/lifecycle/LiveData;", "setState", "(Landroidx/lifecycle/LiveData;)V", "<init>", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionRepository;)V", "Action", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class BaseStoryRecipientSelectionViewModel extends ViewModel {
    private Observable<Action> actionObservable;
    private final CompositeDisposable disposable;
    private final DistributionListId distributionListId;
    private final BaseStoryRecipientSelectionRepository repository;
    private LiveData<BaseStoryRecipientSelectionState> state;
    private final Store<BaseStoryRecipientSelectionState> store;
    private final PublishSubject<Action> subject;

    public BaseStoryRecipientSelectionViewModel(DistributionListId distributionListId, BaseStoryRecipientSelectionRepository baseStoryRecipientSelectionRepository) {
        Intrinsics.checkNotNullParameter(baseStoryRecipientSelectionRepository, "repository");
        this.distributionListId = distributionListId;
        this.repository = baseStoryRecipientSelectionRepository;
        Store<BaseStoryRecipientSelectionState> store = new Store<>(new BaseStoryRecipientSelectionState(distributionListId, null, null, 6, null));
        this.store = store;
        PublishSubject<Action> create = PublishSubject.create();
        this.subject = create;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposable = compositeDisposable;
        Intrinsics.checkNotNullExpressionValue(create, SmsDatabase.SUBJECT);
        this.actionObservable = create;
        LiveData<BaseStoryRecipientSelectionState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        if (distributionListId != null) {
            Disposable subscribe = baseStoryRecipientSelectionRepository.getRecord(distributionListId).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionViewModel$$ExternalSyntheticLambda1
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    BaseStoryRecipientSelectionViewModel.m2949_init_$lambda1(BaseStoryRecipientSelectionViewModel.this, (DistributionListRecord) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getRecord(dis…ingSelection) }\n        }");
            DisposableKt.plusAssign(compositeDisposable, subscribe);
        }
    }

    public final Observable<Action> getActionObservable() {
        return this.actionObservable;
    }

    public final void setActionObservable(Observable<Action> observable) {
        Intrinsics.checkNotNullParameter(observable, "<set-?>");
        this.actionObservable = observable;
    }

    public final LiveData<BaseStoryRecipientSelectionState> getState() {
        return this.state;
    }

    public final void setState(LiveData<BaseStoryRecipientSelectionState> liveData) {
        Intrinsics.checkNotNullParameter(liveData, "<set-?>");
        this.state = liveData;
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m2949_init_$lambda1(BaseStoryRecipientSelectionViewModel baseStoryRecipientSelectionViewModel, DistributionListRecord distributionListRecord) {
        Intrinsics.checkNotNullParameter(baseStoryRecipientSelectionViewModel, "this$0");
        baseStoryRecipientSelectionViewModel.store.update(new Function(distributionListRecord.getPrivacyMode() == DistributionListPrivacyMode.ALL_EXCEPT ? distributionListRecord.getRawMembers() : distributionListRecord.getMembers()) { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BaseStoryRecipientSelectionViewModel.m2951lambda1$lambda0(DistributionListRecord.this, this.f$1, (BaseStoryRecipientSelectionState) obj);
            }
        });
    }

    /* renamed from: lambda-1$lambda-0 */
    public static final BaseStoryRecipientSelectionState m2951lambda1$lambda0(DistributionListRecord distributionListRecord, List list, BaseStoryRecipientSelectionState baseStoryRecipientSelectionState) {
        Intrinsics.checkNotNullParameter(list, "$startingSelection");
        Intrinsics.checkNotNullExpressionValue(baseStoryRecipientSelectionState, "it");
        return BaseStoryRecipientSelectionState.copy$default(baseStoryRecipientSelectionState, null, distributionListRecord, SetsKt___SetsKt.plus((Set) baseStoryRecipientSelectionState.getSelection(), (Iterable) list), 1, null);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposable.clear();
    }

    public final void toggleSelectAll() {
        DisposableKt.plusAssign(this.disposable, SubscribersKt.subscribeBy$default(this.repository.getAllSignalContacts(), (Function1) null, new BaseStoryRecipientSelectionViewModel$toggleSelectAll$1(this), 1, (Object) null));
    }

    /* renamed from: addRecipient$lambda-2 */
    public static final BaseStoryRecipientSelectionState m2950addRecipient$lambda2(RecipientId recipientId, BaseStoryRecipientSelectionState baseStoryRecipientSelectionState) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullExpressionValue(baseStoryRecipientSelectionState, "it");
        return BaseStoryRecipientSelectionState.copy$default(baseStoryRecipientSelectionState, null, null, SetsKt___SetsKt.plus(baseStoryRecipientSelectionState.getSelection(), recipientId), 3, null);
    }

    public final void addRecipient(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionViewModel$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BaseStoryRecipientSelectionViewModel.m2950addRecipient$lambda2(RecipientId.this, (BaseStoryRecipientSelectionState) obj);
            }
        });
    }

    /* renamed from: removeRecipient$lambda-3 */
    public static final BaseStoryRecipientSelectionState m2952removeRecipient$lambda3(RecipientId recipientId, BaseStoryRecipientSelectionState baseStoryRecipientSelectionState) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullExpressionValue(baseStoryRecipientSelectionState, "it");
        return BaseStoryRecipientSelectionState.copy$default(baseStoryRecipientSelectionState, null, null, SetsKt___SetsKt.minus(baseStoryRecipientSelectionState.getSelection(), recipientId), 3, null);
    }

    public final void removeRecipient(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BaseStoryRecipientSelectionViewModel.m2952removeRecipient$lambda3(RecipientId.this, (BaseStoryRecipientSelectionState) obj);
            }
        });
    }

    public final void onAction() {
        if (this.distributionListId != null) {
            BaseStoryRecipientSelectionRepository baseStoryRecipientSelectionRepository = this.repository;
            DistributionListRecord privateStory = this.store.getState().getPrivateStory();
            Intrinsics.checkNotNull(privateStory);
            baseStoryRecipientSelectionRepository.updateDistributionListMembership(privateStory, this.store.getState().getSelection());
            this.subject.onNext(Action.ExitFlow.INSTANCE);
            return;
        }
        this.subject.onNext(new Action.GoToNextScreen(this.store.getState().getSelection()));
    }

    /* compiled from: BaseStoryRecipientSelectionViewModel.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel$Action;", "", "()V", "ExitFlow", "GoToNextScreen", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel$Action$GoToNextScreen;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel$Action$ExitFlow;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static abstract class Action {
        public /* synthetic */ Action(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: BaseStoryRecipientSelectionViewModel.kt */
        @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel$Action$GoToNextScreen;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel$Action;", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Ljava/util/Set;)V", "getRecipients", "()Ljava/util/Set;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class GoToNextScreen extends Action {
            private final Set<RecipientId> recipients;

            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionViewModel$Action$GoToNextScreen */
            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ GoToNextScreen copy$default(GoToNextScreen goToNextScreen, Set set, int i, Object obj) {
                if ((i & 1) != 0) {
                    set = goToNextScreen.recipients;
                }
                return goToNextScreen.copy(set);
            }

            public final Set<RecipientId> component1() {
                return this.recipients;
            }

            public final GoToNextScreen copy(Set<? extends RecipientId> set) {
                Intrinsics.checkNotNullParameter(set, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
                return new GoToNextScreen(set);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof GoToNextScreen) && Intrinsics.areEqual(this.recipients, ((GoToNextScreen) obj).recipients);
            }

            public int hashCode() {
                return this.recipients.hashCode();
            }

            public String toString() {
                return "GoToNextScreen(recipients=" + this.recipients + ')';
            }

            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public GoToNextScreen(Set<? extends RecipientId> set) {
                super(null);
                Intrinsics.checkNotNullParameter(set, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
                this.recipients = set;
            }

            public final Set<RecipientId> getRecipients() {
                return this.recipients;
            }
        }

        private Action() {
        }

        /* compiled from: BaseStoryRecipientSelectionViewModel.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel$Action$ExitFlow;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel$Action;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class ExitFlow extends Action {
            public static final ExitFlow INSTANCE = new ExitFlow();

            private ExitFlow() {
                super(null);
            }
        }
    }

    /* compiled from: BaseStoryRecipientSelectionViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionRepository;", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final DistributionListId distributionListId;
        private final BaseStoryRecipientSelectionRepository repository;

        public Factory(DistributionListId distributionListId, BaseStoryRecipientSelectionRepository baseStoryRecipientSelectionRepository) {
            Intrinsics.checkNotNullParameter(baseStoryRecipientSelectionRepository, "repository");
            this.distributionListId = distributionListId;
            this.repository = baseStoryRecipientSelectionRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new BaseStoryRecipientSelectionViewModel(this.distributionListId, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionViewModel.Factory.create");
        }
    }
}
