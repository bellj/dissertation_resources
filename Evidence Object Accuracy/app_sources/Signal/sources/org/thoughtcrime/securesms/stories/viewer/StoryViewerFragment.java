package org.thoughtcrime.securesms.stories.viewer;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.viewpager2.widget.ViewPager2;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.StoryViewerArgs;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerState;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerViewModel;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment;
import org.thoughtcrime.securesms.stories.viewer.reply.StoriesSharedElementCrossFaderView;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: StoryViewerFragment.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 *2\u00020\u00012\u00020\u00022\u00020\u0003:\u0002*+B\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u001a\u001a\u00020\u001bH\u0016J\b\u0010\u001c\u001a\u00020\u001bH\u0016J\u0010\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0010\u0010 \u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\b\u0010!\u001a\u00020\u001bH\u0016J\b\u0010\"\u001a\u00020\u001bH\u0016J\b\u0010#\u001a\u00020\u001bH\u0016J\u0010\u0010$\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u001a\u0010%\u001a\u00020\u001b2\u0006\u0010&\u001a\u00020'2\b\u0010(\u001a\u0004\u0018\u00010)H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u00060\bR\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u0014\u001a\u0004\b\u0017\u0010\u0018¨\u0006,"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageFragment$Callback;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView$Callback;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "onPageChanged", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerFragment$OnPageChanged;", "pagerOnPageSelectedLock", "", "storyCrossfader", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView;", "storyPager", "Landroidx/viewpager2/widget/ViewPager2;", "storyViewerArgs", "Lorg/thoughtcrime/securesms/stories/StoryViewerArgs;", "getStoryViewerArgs", "()Lorg/thoughtcrime/securesms/stories/StoryViewerArgs;", "storyViewerArgs$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerViewModel;", "viewModel$delegate", "onAnimationFinished", "", "onAnimationStarted", "onFinishedPosts", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "onGoToPreviousStory", "onPause", "onReadyToAnimate", "onResume", "onStoryHidden", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "OnPageChanged", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerFragment extends Fragment implements StoryViewerPageFragment.Callback, StoriesSharedElementCrossFaderView.Callback {
    private static final String ARGS;
    public static final Companion Companion = new Companion(null);
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final OnPageChanged onPageChanged = new OnPageChanged();
    private boolean pagerOnPageSelectedLock;
    private StoriesSharedElementCrossFaderView storyCrossfader;
    private ViewPager2 storyPager;
    private final Lazy storyViewerArgs$delegate = LazyKt__LazyJVMKt.lazy(new Function0<StoryViewerArgs>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerFragment$storyViewerArgs$2
        final /* synthetic */ StoryViewerFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final StoryViewerArgs invoke() {
            Parcelable parcelable = this.this$0.requireArguments().getParcelable(MultiselectForwardFragment.ARGS);
            Intrinsics.checkNotNull(parcelable);
            return (StoryViewerArgs) parcelable;
        }
    });
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryViewerViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerFragment$viewModel$2
        final /* synthetic */ StoryViewerFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new StoryViewerViewModel.Factory(this.this$0.getStoryViewerArgs(), new StoryViewerRepository());
        }
    });

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.StoriesSharedElementCrossFaderView.Callback
    public void onReadyToAnimate() {
    }

    public StoryViewerFragment() {
        super(R.layout.stories_viewer_fragment);
    }

    public final StoryViewerViewModel getViewModel() {
        return (StoryViewerViewModel) this.viewModel$delegate.getValue();
    }

    public final StoryViewerArgs getStoryViewerArgs() {
        return (StoryViewerArgs) this.storyViewerArgs$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.story_content_crossfader);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.story_content_crossfader)");
        this.storyCrossfader = (StoriesSharedElementCrossFaderView) findViewById;
        View findViewById2 = view.findViewById(R.id.story_item_pager);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.story_item_pager)");
        this.storyPager = (ViewPager2) findViewById2;
        StoriesSharedElementCrossFaderView storiesSharedElementCrossFaderView = this.storyCrossfader;
        ViewPager2 viewPager2 = null;
        if (storiesSharedElementCrossFaderView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyCrossfader");
            storiesSharedElementCrossFaderView = null;
        }
        storiesSharedElementCrossFaderView.setCallback(this);
        StoryViewerPagerAdapter storyViewerPagerAdapter = new StoryViewerPagerAdapter(this, getStoryViewerArgs().getStoryId(), getStoryViewerArgs().isFromNotification(), getStoryViewerArgs().getGroupReplyStartPosition(), getStoryViewerArgs().isUnviewedOnly(), getStoryViewerArgs().isFromMyStories(), getStoryViewerArgs().isFromInfoContextMenuAction());
        ViewPager2 viewPager22 = this.storyPager;
        if (viewPager22 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyPager");
            viewPager22 = null;
        }
        viewPager22.setAdapter(storyViewerPagerAdapter);
        ViewPager2 viewPager23 = this.storyPager;
        if (viewPager23 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyPager");
            viewPager23 = null;
        }
        viewPager23.setOverScrollMode(2);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getAllowParentScrolling().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerFragment$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryViewerFragment.m3001onViewCreated$lambda0(StoryViewerFragment.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.allowParentScr…erInputEnabled = it\n    }");
        lifecycleDisposable.plusAssign(subscribe);
        ViewPager2 viewPager24 = this.storyPager;
        if (viewPager24 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyPager");
        } else {
            viewPager2 = viewPager24;
        }
        viewPager2.setOffscreenPageLimit(1);
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable2.bindTo(viewLifecycleOwner);
        LifecycleDisposable lifecycleDisposable3 = this.lifecycleDisposable;
        Disposable subscribe2 = getViewModel().getState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(this) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ StoryViewerFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryViewerFragment.m3002onViewCreated$lambda1(StoryViewerPagerAdapter.this, this.f$1, (StoryViewerState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "viewModel.state.observeO….alpha = 0f\n      }\n    }");
        lifecycleDisposable3.plusAssign(subscribe2);
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m3001onViewCreated$lambda0(StoryViewerFragment storyViewerFragment, Boolean bool) {
        Intrinsics.checkNotNullParameter(storyViewerFragment, "this$0");
        ViewPager2 viewPager2 = storyViewerFragment.storyPager;
        if (viewPager2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyPager");
            viewPager2 = null;
        }
        Intrinsics.checkNotNullExpressionValue(bool, "it");
        viewPager2.setUserInputEnabled(bool.booleanValue());
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m3002onViewCreated$lambda1(StoryViewerPagerAdapter storyViewerPagerAdapter, StoryViewerFragment storyViewerFragment, StoryViewerState storyViewerState) {
        Intrinsics.checkNotNullParameter(storyViewerPagerAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(storyViewerFragment, "this$0");
        storyViewerPagerAdapter.setPages(storyViewerState.getPages());
        StoriesSharedElementCrossFaderView storiesSharedElementCrossFaderView = null;
        if (!storyViewerState.getPages().isEmpty()) {
            ViewPager2 viewPager2 = storyViewerFragment.storyPager;
            if (viewPager2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyPager");
                viewPager2 = null;
            }
            if (viewPager2.getCurrentItem() != storyViewerState.getPage()) {
                storyViewerFragment.pagerOnPageSelectedLock = true;
                ViewPager2 viewPager22 = storyViewerFragment.storyPager;
                if (viewPager22 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("storyPager");
                    viewPager22 = null;
                }
                viewPager22.setUserInputEnabled(false);
                ViewPager2 viewPager23 = storyViewerFragment.storyPager;
                if (viewPager23 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("storyPager");
                    viewPager23 = null;
                }
                viewPager23.setCurrentItem(storyViewerState.getPage(), storyViewerState.getPreviousPage() > -1);
                storyViewerFragment.pagerOnPageSelectedLock = false;
                if (storyViewerState.getPage() >= storyViewerState.getPages().size()) {
                    storyViewerFragment.requireActivity().onBackPressed();
                }
            }
        }
        StoryViewerState.CrossfadeSource crossfadeSource = storyViewerState.getCrossfadeSource();
        if (crossfadeSource instanceof StoryViewerState.CrossfadeSource.TextModel) {
            StoriesSharedElementCrossFaderView storiesSharedElementCrossFaderView2 = storyViewerFragment.storyCrossfader;
            if (storiesSharedElementCrossFaderView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyCrossfader");
                storiesSharedElementCrossFaderView2 = null;
            }
            storiesSharedElementCrossFaderView2.setSourceView(((StoryViewerState.CrossfadeSource.TextModel) storyViewerState.getCrossfadeSource()).getStoryTextPostModel());
        } else if (crossfadeSource instanceof StoryViewerState.CrossfadeSource.ImageUri) {
            StoriesSharedElementCrossFaderView storiesSharedElementCrossFaderView3 = storyViewerFragment.storyCrossfader;
            if (storiesSharedElementCrossFaderView3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyCrossfader");
                storiesSharedElementCrossFaderView3 = null;
            }
            storiesSharedElementCrossFaderView3.setSourceView(((StoryViewerState.CrossfadeSource.ImageUri) storyViewerState.getCrossfadeSource()).getImageUri(), ((StoryViewerState.CrossfadeSource.ImageUri) storyViewerState.getCrossfadeSource()).getImageBlur());
        }
        if (storyViewerState.getCrossfadeTarget() instanceof StoryViewerState.CrossfadeTarget.Record) {
            StoriesSharedElementCrossFaderView storiesSharedElementCrossFaderView4 = storyViewerFragment.storyCrossfader;
            if (storiesSharedElementCrossFaderView4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyCrossfader");
                storiesSharedElementCrossFaderView4 = null;
            }
            storiesSharedElementCrossFaderView4.setTargetView(((StoryViewerState.CrossfadeTarget.Record) storyViewerState.getCrossfadeTarget()).getMessageRecord());
            storyViewerFragment.requireActivity().supportStartPostponedEnterTransition();
        }
        if (storyViewerState.getSkipCrossfade()) {
            storyViewerFragment.getViewModel().setCrossfaderIsReady(true);
        }
        if (storyViewerState.getLoadState().isReady()) {
            StoriesSharedElementCrossFaderView storiesSharedElementCrossFaderView5 = storyViewerFragment.storyCrossfader;
            if (storiesSharedElementCrossFaderView5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyCrossfader");
            } else {
                storiesSharedElementCrossFaderView = storiesSharedElementCrossFaderView5;
            }
            storiesSharedElementCrossFaderView.setAlpha(0.0f);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().setIsScrolling(false);
        ViewPager2 viewPager2 = this.storyPager;
        if (viewPager2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyPager");
            viewPager2 = null;
        }
        viewPager2.registerOnPageChangeCallback(this.onPageChanged);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        getViewModel().setIsScrolling(false);
        ViewPager2 viewPager2 = this.storyPager;
        if (viewPager2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyPager");
            viewPager2 = null;
        }
        viewPager2.unregisterOnPageChangeCallback(this.onPageChanged);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment.Callback
    public void onGoToPreviousStory(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        getViewModel().onGoToPrevious(recipientId);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment.Callback
    public void onFinishedPosts(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        getViewModel().onGoToNext(recipientId);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment.Callback
    public void onStoryHidden(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        getViewModel().onRecipientHidden();
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.StoriesSharedElementCrossFaderView.Callback
    public void onAnimationStarted() {
        getViewModel().setCrossfaderIsReady(false);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.StoriesSharedElementCrossFaderView.Callback
    public void onAnimationFinished() {
        getViewModel().setCrossfaderIsReady(true);
    }

    /* compiled from: StoryViewerFragment.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0006H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerFragment$OnPageChanged;", "Landroidx/viewpager2/widget/ViewPager2$OnPageChangeCallback;", "(Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerFragment;)V", "onPageScrollStateChanged", "", "state", "", "onPageSelected", "position", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public final class OnPageChanged extends ViewPager2.OnPageChangeCallback {
        public OnPageChanged() {
            StoryViewerFragment.this = r1;
        }

        @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
        public void onPageSelected(int i) {
            if (!StoryViewerFragment.this.pagerOnPageSelectedLock) {
                StoryViewerFragment.this.getViewModel().setSelectedPage(i);
            }
        }

        @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
        public void onPageScrollStateChanged(int i) {
            StoryViewerFragment.this.getViewModel().setIsScrolling(i == 1);
            if (i == 0) {
                ViewPager2 viewPager2 = StoryViewerFragment.this.storyPager;
                if (viewPager2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("storyPager");
                    viewPager2 = null;
                }
                viewPager2.setUserInputEnabled(true);
            }
        }
    }

    /* compiled from: StoryViewerFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerFragment$Companion;", "", "()V", "ARGS", "", "create", "Landroidx/fragment/app/Fragment;", "storyViewerArgs", "Lorg/thoughtcrime/securesms/stories/StoryViewerArgs;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Fragment create(StoryViewerArgs storyViewerArgs) {
            Intrinsics.checkNotNullParameter(storyViewerArgs, "storyViewerArgs");
            StoryViewerFragment storyViewerFragment = new StoryViewerFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("args", storyViewerArgs);
            storyViewerFragment.setArguments(bundle);
            return storyViewerFragment;
        }
    }
}
