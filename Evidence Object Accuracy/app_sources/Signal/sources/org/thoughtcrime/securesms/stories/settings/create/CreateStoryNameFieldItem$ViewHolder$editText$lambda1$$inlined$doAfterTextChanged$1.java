package org.thoughtcrime.securesms.stories.settings.create;

import android.text.Editable;
import android.text.TextWatcher;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* compiled from: TextView.kt */
@Metadata(bv = {}, d1 = {"\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0016J*\u0010\f\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\bH\u0016J*\u0010\u000e\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\bH\u0016¨\u0006\u000f"}, d2 = {"androidx/core/widget/TextViewKt$addTextChangedListener$textWatcher$1", "Landroid/text/TextWatcher;", "Landroid/text/Editable;", "s", "", "afterTextChanged", "", DraftDatabase.Draft.TEXT, "", NotificationProfileDatabase.NotificationProfileScheduleTable.START, NewHtcHomeBadger.COUNT, "after", "beforeTextChanged", "before", "onTextChanged", "core-ktx_release"}, k = 1, mv = {1, 6, 0})
/* renamed from: org.thoughtcrime.securesms.stories.settings.create.CreateStoryNameFieldItem$ViewHolder$editText$lambda-1$$inlined$doAfterTextChanged$1 */
/* loaded from: classes3.dex */
public final class CreateStoryNameFieldItem$ViewHolder$editText$lambda1$$inlined$doAfterTextChanged$1 implements TextWatcher {
    final /* synthetic */ Function1 $onTextChanged$inlined;

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public CreateStoryNameFieldItem$ViewHolder$editText$lambda1$$inlined$doAfterTextChanged$1(Function1 function1) {
        this.$onTextChanged$inlined = function1;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        if (editable != null) {
            this.$onTextChanged$inlined.invoke(editable);
        }
    }
}
