package org.thoughtcrime.securesms.stories.settings.create;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleEmitter;
import io.reactivex.rxjava3.core.SingleOnSubscribe;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: CreateStoryWithViewersRepository.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\"\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\t¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersRepository;", "", "()V", "createList", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "name", "", "members", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class CreateStoryWithViewersRepository {
    public final Single<RecipientId> createList(CharSequence charSequence, Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(charSequence, "name");
        Intrinsics.checkNotNullParameter(set, "members");
        Single<RecipientId> subscribeOn = Single.create(new SingleOnSubscribe(charSequence, set) { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ CharSequence f$0;
            public final /* synthetic */ Set f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.core.SingleOnSubscribe
            public final void subscribe(SingleEmitter singleEmitter) {
                CreateStoryWithViewersRepository.m2864createList$lambda0(this.f$0, this.f$1, singleEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create<RecipientId> {\n  …scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: createList$lambda-0 */
    public static final void m2864createList$lambda0(CharSequence charSequence, Set set, SingleEmitter singleEmitter) {
        Intrinsics.checkNotNullParameter(charSequence, "$name");
        Intrinsics.checkNotNullParameter(set, "$members");
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        DistributionListId createList$default = DistributionListDatabase.createList$default(companion.distributionLists(), charSequence.toString(), CollectionsKt___CollectionsKt.toList(set), null, false, 0, null, false, null, 252, null);
        if (createList$default == null) {
            singleEmitter.onError(new Exception("Null result, due to a duplicated name."));
            return;
        }
        Stories.INSTANCE.onStorySettingsChanged(createList$default);
        singleEmitter.onSuccess(RecipientDatabase.getOrInsertFromDistributionListId$default(companion.recipients(), createList$default, null, 2, null));
    }
}
