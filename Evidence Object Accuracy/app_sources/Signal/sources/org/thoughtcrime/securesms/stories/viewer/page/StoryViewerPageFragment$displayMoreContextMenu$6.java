package org.thoughtcrime.securesms.stories.viewer.page;

import android.content.Context;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: StoryViewerPageFragment.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerPageFragment$displayMoreContextMenu$6 extends Lambda implements Function1<StoryPost, Unit> {
    final /* synthetic */ StoryViewerPageFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryViewerPageFragment$displayMoreContextMenu$6(StoryViewerPageFragment storyViewerPageFragment) {
        super(1);
        this.this$0 = storyViewerPageFragment;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoryPost storyPost) {
        invoke(storyPost);
        return Unit.INSTANCE;
    }

    public final void invoke(StoryPost storyPost) {
        Intrinsics.checkNotNullParameter(storyPost, "it");
        this.this$0.getViewModel().setIsDisplayingDeleteDialog(true);
        LifecycleDisposable lifecycleDisposable = this.this$0.lifecycleDisposable;
        StoryContextMenu storyContextMenu = StoryContextMenu.INSTANCE;
        Context requireContext = this.this$0.requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        Disposable subscribe = storyContextMenu.delete(requireContext, SetsKt__SetsJVMKt.setOf(storyPost.getConversationMessage().getMessageRecord())).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageFragment$displayMoreContextMenu$6$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryViewerPageFragment$displayMoreContextMenu$6.m3054invoke$lambda0(StoryViewerPageFragment.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "StoryContextMenu.delete(…Model.refresh()\n        }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m3054invoke$lambda0(StoryViewerPageFragment storyViewerPageFragment, Boolean bool) {
        Intrinsics.checkNotNullParameter(storyViewerPageFragment, "this$0");
        storyViewerPageFragment.getViewModel().setIsDisplayingDeleteDialog(false);
        storyViewerPageFragment.getViewModel().refresh();
    }
}
