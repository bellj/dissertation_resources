package org.thoughtcrime.securesms.stories.viewer;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.KeyEvent;
import androidx.activity.ComponentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelLazy;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.media.AudioManagerCompat;
import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaController;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaControllerOwner;
import org.thoughtcrime.securesms.stories.StoryViewerArgs;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerFragment;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* compiled from: StoryViewerActivity.kt */
@Metadata(d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 %2\u00020\u00012\u00020\u0002:\u0001%B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0014J\u001a\u0010\u0014\u001a\u00020\u00112\b\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0014J\b\u0010\u0019\u001a\u00020\u0011H\u0014J\b\u0010\u001a\u001a\u00020\u0011H\u0016J\u001a\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\u0012\u0010 \u001a\u00020\u00112\b\u0010!\u001a\u0004\u0018\u00010\"H\u0014J\b\u0010#\u001a\u00020\u0011H\u0014J\b\u0010$\u001a\u00020\u0011H\u0002R\u001b\u0010\u0004\u001a\u00020\u00058BX\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u001a\u0010\n\u001a\u00020\u000bX.¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerActivity;", "Lorg/thoughtcrime/securesms/PassphraseRequiredActivity;", "Lorg/thoughtcrime/securesms/components/voice/VoiceNoteMediaControllerOwner;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/stories/viewer/StoryVolumeViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/StoryVolumeViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "voiceNoteMediaController", "Lorg/thoughtcrime/securesms/components/voice/VoiceNoteMediaController;", "getVoiceNoteMediaController", "()Lorg/thoughtcrime/securesms/components/voice/VoiceNoteMediaController;", "setVoiceNoteMediaController", "(Lorg/thoughtcrime/securesms/components/voice/VoiceNoteMediaController;)V", "attachBaseContext", "", "newBase", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "ready", "", "onDestroy", "onEnterAnimationComplete", "onKeyDown", "keyCode", "", "event", "Landroid/view/KeyEvent;", "onNewIntent", "intent", "Landroid/content/Intent;", "onResume", "replaceStoryViewerFragment", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerActivity extends PassphraseRequiredActivity implements VoiceNoteMediaControllerOwner {
    private static final String ARGS;
    public static final Companion Companion = new Companion(null);
    private final Lazy viewModel$delegate = new ViewModelLazy(Reflection.getOrCreateKotlinClass(StoryVolumeViewModel.class), new Function0<ViewModelStore>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerActivity$special$$inlined$viewModels$default$2
        final /* synthetic */ ComponentActivity $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = this.$this_viewModels.getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.StoryViewerActivity$special$$inlined$viewModels$default$1
        final /* synthetic */ ComponentActivity $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return this.$this_viewModels.getDefaultViewModelProviderFactory();
        }
    });
    public VoiceNoteMediaController voiceNoteMediaController;

    @JvmStatic
    public static final Intent createIntent(Context context, StoryViewerArgs storyViewerArgs) {
        return Companion.createIntent(context, storyViewerArgs);
    }

    private final StoryVolumeViewModel getViewModel() {
        return (StoryVolumeViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.voice.VoiceNoteMediaControllerOwner
    public VoiceNoteMediaController getVoiceNoteMediaController() {
        VoiceNoteMediaController voiceNoteMediaController = this.voiceNoteMediaController;
        if (voiceNoteMediaController != null) {
            return voiceNoteMediaController;
        }
        Intrinsics.throwUninitializedPropertyAccessException("voiceNoteMediaController");
        return null;
    }

    public void setVoiceNoteMediaController(VoiceNoteMediaController voiceNoteMediaController) {
        Intrinsics.checkNotNullParameter(voiceNoteMediaController, "<set-?>");
        this.voiceNoteMediaController = voiceNoteMediaController;
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        Intrinsics.checkNotNullParameter(context, "newBase");
        getDelegate().setLocalNightMode(2);
        super.attachBaseContext(context);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        StoryMutePolicy.INSTANCE.initialize();
        Glide.get(this).setMemoryCategory(MemoryCategory.HIGH);
        supportPostponeEnterTransition();
        super.onCreate(bundle, z);
        setContentView(R.layout.fragment_container);
        setVoiceNoteMediaController(new VoiceNoteMediaController(this));
        if (bundle == null) {
            replaceStoryViewerFragment();
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        Glide.get(this).setMemoryCategory(MemoryCategory.NORMAL);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        if (StoryMutePolicy.INSTANCE.isContentMuted()) {
            getViewModel().mute();
        } else {
            getViewModel().unmute();
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        replaceStoryViewerFragment();
    }

    @Override // android.app.Activity
    public void onEnterAnimationComplete() {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setTransitionBackgroundFadeDuration(100);
        }
    }

    private final void replaceStoryViewerFragment() {
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        StoryViewerFragment.Companion companion = StoryViewerFragment.Companion;
        Parcelable parcelableExtra = getIntent().getParcelableExtra(ARGS);
        Intrinsics.checkNotNull(parcelableExtra);
        beginTransaction.replace(R.id.fragment_container, companion.create((StoryViewerArgs) parcelableExtra)).commit();
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        AudioManager audioManager = ServiceUtil.getAudioManager(this);
        if (i == 24) {
            int streamMaxVolume = AudioManagerCompat.getStreamMaxVolume(audioManager, 3);
            StoryMutePolicy.INSTANCE.setContentMuted(false);
            getViewModel().onVolumeUp(Math.min(streamMaxVolume, audioManager.getStreamVolume(3) + 1));
            return true;
        } else if (i != 25) {
            return super.onKeyDown(i, keyEvent);
        } else {
            getViewModel().onVolumeDown(Math.max(AudioManagerCompat.getStreamMinVolume(audioManager, 3), audioManager.getStreamVolume(3) - 1));
            return true;
        }
    }

    /* compiled from: StoryViewerActivity.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/StoryViewerActivity$Companion;", "", "()V", "ARGS", "", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "storyViewerArgs", "Lorg/thoughtcrime/securesms/stories/StoryViewerArgs;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final Intent createIntent(Context context, StoryViewerArgs storyViewerArgs) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(storyViewerArgs, "storyViewerArgs");
            Intent putExtra = new Intent(context, StoryViewerActivity.class).putExtra(StoryViewerActivity.ARGS, storyViewerArgs);
            Intrinsics.checkNotNullExpressionValue(putExtra, "Intent(context, StoryVie…ra(ARGS, storyViewerArgs)");
            return putExtra;
        }
    }
}
