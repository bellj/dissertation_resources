package org.thoughtcrime.securesms.stories.viewer.reply.composer;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ComposeText;
import org.thoughtcrime.securesms.components.InputAwareLayout;
import org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout;
import org.thoughtcrime.securesms.components.QuoteView;
import org.thoughtcrime.securesms.components.emoji.EmojiPageView;
import org.thoughtcrime.securesms.components.emoji.EmojiToggle;
import org.thoughtcrime.securesms.components.emoji.MediaKeyboard;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: StoryReplyComposer.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 <2\u00020\u0001:\u0002;<B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0006\u0010)\u001a\u00020*J\u0018\u0010+\u001a\u0014\u0012\u0004\u0012\u00020-\u0012\n\u0012\b\u0012\u0004\u0012\u00020/0.0,J\u000e\u00100\u001a\u00020*2\u0006\u00101\u001a\u000202J\u0010\u00103\u001a\u00020*2\b\u00104\u001a\u0004\u0018\u000105J\b\u00106\u001a\u00020*H\u0002J\u0006\u00107\u001a\u00020*J\u000e\u00108\u001a\u00020*2\u0006\u00109\u001a\u00020:R\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u00148F¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0017\u001a\u00020\u0018¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u001f\u001a\u00020\u001e2\u0006\u0010\u001d\u001a\u00020\u001e@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u000e\u0010!\u001a\u00020\"X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010%\u001a\u00020&¢\u0006\b\n\u0000\u001a\u0004\b'\u0010(¨\u0006="}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "callback", "Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer$Callback;", "getCallback", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer$Callback;", "setCallback", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer$Callback;)V", "emojiDrawer", "Lorg/thoughtcrime/securesms/components/emoji/MediaKeyboard;", "emojiDrawerToggle", "Lorg/thoughtcrime/securesms/components/emoji/EmojiToggle;", "emojiPageView", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageView;", "getEmojiPageView", "()Lorg/thoughtcrime/securesms/components/emoji/EmojiPageView;", "input", "Lorg/thoughtcrime/securesms/components/ComposeText;", "getInput", "()Lorg/thoughtcrime/securesms/components/ComposeText;", "inputAwareLayout", "Lorg/thoughtcrime/securesms/components/InputAwareLayout;", "<set-?>", "", "isRequestingEmojiDrawer", "()Z", "privacyChrome", "Landroid/widget/TextView;", "quoteView", "Lorg/thoughtcrime/securesms/components/QuoteView;", "reactionButton", "Landroid/view/View;", "getReactionButton", "()Landroid/view/View;", "closeEmojiSearch", "", "consumeInput", "Lkotlin/Pair;", "", "", "Lorg/thoughtcrime/securesms/database/model/Mention;", "displayPrivacyChrome", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "onEmojiSelected", "emoji", "", "onEmojiToggleClicked", "openEmojiSearch", "setQuote", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MediaMmsMessageRecord;", "Callback", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryReplyComposer extends FrameLayout {
    public static final Companion Companion = new Companion(null);
    private Callback callback;
    private final MediaKeyboard emojiDrawer;
    private final EmojiToggle emojiDrawerToggle;
    private final ComposeText input;
    private final InputAwareLayout inputAwareLayout;
    private boolean isRequestingEmojiDrawer;
    private final TextView privacyChrome;
    private final QuoteView quoteView;
    private final View reactionButton;

    /* compiled from: StoryReplyComposer.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\b\u0010\u0007\u001a\u00020\u0003H&J\b\u0010\b\u001a\u00020\u0003H&J\b\u0010\t\u001a\u00020\u0003H\u0016¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer$Callback;", "", "onHideEmojiKeyboard", "", "onInitializeEmojiDrawer", "mediaKeyboard", "Lorg/thoughtcrime/securesms/components/emoji/MediaKeyboard;", "onPickReactionClicked", "onSendActionClicked", "onShowEmojiKeyboard", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {

        /* compiled from: StoryReplyComposer.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class DefaultImpls {
            public static void onHideEmojiKeyboard(Callback callback) {
            }

            public static void onShowEmojiKeyboard(Callback callback) {
            }
        }

        void onHideEmojiKeyboard();

        void onInitializeEmojiDrawer(MediaKeyboard mediaKeyboard);

        void onPickReactionClicked();

        void onSendActionClicked();

        void onShowEmojiKeyboard();
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryReplyComposer(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryReplyComposer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ StoryReplyComposer(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryReplyComposer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        FrameLayout.inflate(context, R.layout.stories_reply_to_story_composer, this);
        View findViewById = findViewById(R.id.input_aware_layout);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.input_aware_layout)");
        InputAwareLayout inputAwareLayout = (InputAwareLayout) findViewById;
        this.inputAwareLayout = inputAwareLayout;
        View findViewById2 = findViewById(R.id.emoji_toggle);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.emoji_toggle)");
        EmojiToggle emojiToggle = (EmojiToggle) findViewById2;
        this.emojiDrawerToggle = emojiToggle;
        View findViewById3 = findViewById(R.id.quote_view);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.quote_view)");
        this.quoteView = (QuoteView) findViewById3;
        View findViewById4 = findViewById(R.id.compose_text);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.compose_text)");
        ComposeText composeText = (ComposeText) findViewById4;
        this.input = composeText;
        View findViewById5 = findViewById(R.id.reaction);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "findViewById(R.id.reaction)");
        this.reactionButton = findViewById5;
        View findViewById6 = findViewById(R.id.private_reply_recipient);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "findViewById(R.id.private_reply_recipient)");
        this.privacyChrome = (TextView) findViewById6;
        View findViewById7 = findViewById(R.id.emoji_drawer);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "findViewById(R.id.emoji_drawer)");
        this.emojiDrawer = (MediaKeyboard) findViewById7;
        View findViewById8 = findViewById(R.id.reply_reaction_switch);
        Intrinsics.checkNotNullExpressionValue(findViewById8, "findViewById(R.id.reply_reaction_switch)");
        View findViewById9 = findViewById(R.id.reply);
        Intrinsics.checkNotNullExpressionValue(findViewById9, "findViewById(R.id.reply)");
        findViewById9.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoryReplyComposer.$r8$lambda$6PfPDLNzEGoOAvpTT1OwlO8Rxmg(StoryReplyComposer.this, view);
            }
        });
        composeText.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer$$ExternalSyntheticLambda1
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i2, KeyEvent keyEvent) {
                return StoryReplyComposer.$r8$lambda$OZuQ3cUYFFGbq8qPD5CGkBZRXqs(StoryReplyComposer.this, textView, i2, keyEvent);
            }
        });
        composeText.addTextChangedListener(new TextWatcher((ViewSwitcher) findViewById8) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer$special$$inlined$doAfterTextChanged$1
            final /* synthetic */ ViewSwitcher $viewSwitcher$inlined;

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            }

            {
                this.$viewSwitcher$inlined = r1;
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                if (editable == null || editable.length() == 0) {
                    this.$viewSwitcher$inlined.setDisplayedChild(0);
                } else {
                    this.$viewSwitcher$inlined.setDisplayedChild(1);
                }
            }
        });
        findViewById5.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoryReplyComposer.$r8$lambda$k2c9Ssvaf75FwxpB3DbGZWEkjGo(StoryReplyComposer.this, view);
            }
        });
        emojiToggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoryReplyComposer.$r8$lambda$xahCEEcRnjUpBfdFbFdIxqy7tD4(StoryReplyComposer.this, view);
            }
        });
        inputAwareLayout.addOnKeyboardShownListener(new KeyboardAwareLinearLayout.OnKeyboardShownListener() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer$$ExternalSyntheticLambda4
            @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardShownListener
            public final void onKeyboardShown() {
                StoryReplyComposer.m3105$r8$lambda$sX0LfWmAYys7JI3rjkIKewgPE(StoryReplyComposer.this);
            }
        });
    }

    public final View getReactionButton() {
        return this.reactionButton;
    }

    public final ComposeText getInput() {
        return this.input;
    }

    public final boolean isRequestingEmojiDrawer() {
        return this.isRequestingEmojiDrawer;
    }

    public final Callback getCallback() {
        return this.callback;
    }

    public final void setCallback(Callback callback) {
        this.callback = callback;
    }

    public final EmojiPageView getEmojiPageView() {
        return (EmojiPageView) findViewById(R.id.emoji_page_view);
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m3106_init_$lambda0(StoryReplyComposer storyReplyComposer, View view) {
        Intrinsics.checkNotNullParameter(storyReplyComposer, "this$0");
        Callback callback = storyReplyComposer.callback;
        if (callback != null) {
            callback.onSendActionClicked();
        }
    }

    /* renamed from: _init_$lambda-1 */
    public static final boolean m3107_init_$lambda1(StoryReplyComposer storyReplyComposer, TextView textView, int i, KeyEvent keyEvent) {
        Intrinsics.checkNotNullParameter(storyReplyComposer, "this$0");
        if (i != 4) {
            return false;
        }
        Callback callback = storyReplyComposer.callback;
        if (callback != null) {
            callback.onSendActionClicked();
        }
        return true;
    }

    /* renamed from: _init_$lambda-3 */
    public static final void m3108_init_$lambda3(StoryReplyComposer storyReplyComposer, View view) {
        Intrinsics.checkNotNullParameter(storyReplyComposer, "this$0");
        Callback callback = storyReplyComposer.callback;
        if (callback != null) {
            callback.onPickReactionClicked();
        }
    }

    /* renamed from: _init_$lambda-4 */
    public static final void m3109_init_$lambda4(StoryReplyComposer storyReplyComposer, View view) {
        Intrinsics.checkNotNullParameter(storyReplyComposer, "this$0");
        storyReplyComposer.onEmojiToggleClicked();
    }

    /* renamed from: _init_$lambda-5 */
    public static final void m3110_init_$lambda5(StoryReplyComposer storyReplyComposer) {
        Intrinsics.checkNotNullParameter(storyReplyComposer, "this$0");
        if (Intrinsics.areEqual(storyReplyComposer.inputAwareLayout.getCurrentInput(), storyReplyComposer.emojiDrawer) && !storyReplyComposer.emojiDrawer.isEmojiSearchMode()) {
            storyReplyComposer.onEmojiToggleClicked();
        }
    }

    public final void setQuote(MediaMmsMessageRecord mediaMmsMessageRecord) {
        Intrinsics.checkNotNullParameter(mediaMmsMessageRecord, "messageRecord");
        this.quoteView.setQuote(GlideApp.with(this), mediaMmsMessageRecord.getDateSent(), mediaMmsMessageRecord.getRecipient(), mediaMmsMessageRecord.getBody(), false, mediaMmsMessageRecord.getSlideDeck(), null, QuoteModel.Type.NORMAL);
        ViewExtensionsKt.setVisible(this.quoteView, true);
    }

    public final void displayPrivacyChrome(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        this.privacyChrome.setText(getContext().getString(R.string.StoryReplyComposer__replying_privately_to_s, recipient.getDisplayName(getContext())));
        ViewExtensionsKt.setVisible(this.privacyChrome, true);
    }

    public final Pair<CharSequence, List<Mention>> consumeInput() {
        String obj = this.input.getTextTrimmed().toString();
        List<Mention> mentions = this.input.getMentions();
        Intrinsics.checkNotNullExpressionValue(mentions, "input.mentions");
        this.input.setText("");
        return TuplesKt.to(obj, mentions);
    }

    public final void openEmojiSearch() {
        this.emojiDrawer.onOpenEmojiSearch();
    }

    public final void onEmojiSelected(String str) {
        this.input.insertEmoji(str);
    }

    public final void closeEmojiSearch() {
        this.emojiDrawer.onCloseEmojiSearch();
    }

    private final void onEmojiToggleClicked() {
        if (!this.emojiDrawer.isInitialised()) {
            Callback callback = this.callback;
            if (callback != null) {
                callback.onInitializeEmojiDrawer(this.emojiDrawer);
            }
            this.emojiDrawerToggle.attach(this.emojiDrawer);
        }
        if (Intrinsics.areEqual(this.inputAwareLayout.getCurrentInput(), this.emojiDrawer)) {
            this.isRequestingEmojiDrawer = false;
            this.inputAwareLayout.showSoftkey(this.input);
            Callback callback2 = this.callback;
            if (callback2 != null) {
                callback2.onHideEmojiKeyboard();
                return;
            }
            return;
        }
        this.isRequestingEmojiDrawer = true;
        this.inputAwareLayout.hideSoftkey(this.input, new Runnable() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer$$ExternalSyntheticLambda7
            @Override // java.lang.Runnable
            public final void run() {
                StoryReplyComposer.m3104$r8$lambda$ntIBwEDLecUgqu0JFBaQ13URME(StoryReplyComposer.this);
            }
        });
    }

    /* renamed from: onEmojiToggleClicked$lambda-8 */
    public static final void m3111onEmojiToggleClicked$lambda8(StoryReplyComposer storyReplyComposer) {
        Intrinsics.checkNotNullParameter(storyReplyComposer, "this$0");
        storyReplyComposer.inputAwareLayout.post(new Runnable() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                StoryReplyComposer.$r8$lambda$Za0WKlYYXV04q19gTgS60N_OGLw(StoryReplyComposer.this);
            }
        });
    }

    /* renamed from: onEmojiToggleClicked$lambda-8$lambda-7 */
    public static final void m3112onEmojiToggleClicked$lambda8$lambda7(StoryReplyComposer storyReplyComposer) {
        Intrinsics.checkNotNullParameter(storyReplyComposer, "this$0");
        storyReplyComposer.inputAwareLayout.show(storyReplyComposer.input, storyReplyComposer.emojiDrawer);
        storyReplyComposer.emojiDrawer.post(new Runnable() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer$$ExternalSyntheticLambda6
            @Override // java.lang.Runnable
            public final void run() {
                StoryReplyComposer.$r8$lambda$9Bx45LNTELK3XLe4Eae2ngr9QYM(StoryReplyComposer.this);
            }
        });
    }

    /* renamed from: onEmojiToggleClicked$lambda-8$lambda-7$lambda-6 */
    public static final void m3113onEmojiToggleClicked$lambda8$lambda7$lambda6(StoryReplyComposer storyReplyComposer) {
        Intrinsics.checkNotNullParameter(storyReplyComposer, "this$0");
        Callback callback = storyReplyComposer.callback;
        if (callback != null) {
            callback.onShowEmojiKeyboard();
        }
    }

    /* compiled from: StoryReplyComposer.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer$Companion;", "", "()V", "installIntoBottomSheet", "Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer;", "context", "Landroid/content/Context;", "dialog", "Landroid/app/Dialog;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final StoryReplyComposer installIntoBottomSheet(Context context, Dialog dialog) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(dialog, "dialog");
            View findViewById = dialog.findViewById(R.id.container);
            Intrinsics.checkNotNullExpressionValue(findViewById, "dialog.findViewById(R.id.container)");
            ViewGroup viewGroup = (ViewGroup) findViewById;
            StoryReplyComposer storyReplyComposer = (StoryReplyComposer) viewGroup.findViewById(R.id.input);
            if (storyReplyComposer != null) {
                return storyReplyComposer;
            }
            StoryReplyComposer storyReplyComposer2 = new StoryReplyComposer(context, null, 0, 6, null);
            storyReplyComposer2.setId(R.id.input);
            viewGroup.addView(storyReplyComposer2);
            return storyReplyComposer2;
        }
    }
}
