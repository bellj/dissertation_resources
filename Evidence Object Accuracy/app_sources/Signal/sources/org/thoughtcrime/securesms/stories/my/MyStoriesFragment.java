package org.thoughtcrime.securesms.stories.my;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import androidx.activity.OnBackPressedCallback;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import io.reactivex.rxjava3.disposables.Disposable;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.stories.StoryTextPostModel;
import org.thoughtcrime.securesms.stories.StoryViewerArgs;
import org.thoughtcrime.securesms.stories.dialogs.StoryContextMenu;
import org.thoughtcrime.securesms.stories.dialogs.StoryDialogs;
import org.thoughtcrime.securesms.stories.my.MyStoriesItem;
import org.thoughtcrime.securesms.stories.my.MyStoriesViewModel;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerActivity;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: MyStoriesFragment.kt */
@Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\f2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J \u0010\u0016\u001a\u00020\f2\u0006\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "viewModel", "Lorg/thoughtcrime/securesms/stories/my/MyStoriesViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/my/MyStoriesViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/stories/my/MyStoriesState;", "handleDeleteClick", "model", "Lorg/thoughtcrime/securesms/stories/my/MyStoriesItem$Model;", "openStoryViewer", "it", "preview", "Landroid/view/View;", "isFromInfoContextMenuAction", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStoriesFragment extends DSLSettingsFragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MyStoriesViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$viewModel$2
        final /* synthetic */ MyStoriesFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            return new MyStoriesViewModel.Factory(new MyStoriesRepository(requireContext));
        }
    });

    public MyStoriesFragment() {
        super(R.string.StoriesLandingFragment__my_stories, 0, R.layout.stories_my_stories_fragment, null, 10, null);
    }

    public final MyStoriesViewModel getViewModel() {
        return (MyStoriesViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        MyStoriesItem.INSTANCE.register(dSLSettingsAdapter);
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(this) { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$bindAdapter$1
            final /* synthetic */ MyStoriesFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // androidx.activity.OnBackPressedCallback
            public void handleOnBackPressed() {
                this.this$0.requireActivity().finish();
            }
        });
        View findViewById = requireView().findViewById(R.id.empty_notice);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this, findViewById) { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ MyStoriesFragment f$1;
            public final /* synthetic */ View f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MyStoriesFragment.m2835bindAdapter$lambda0(DSLSettingsAdapter.this, this.f$1, this.f$2, (MyStoriesState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m2835bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, MyStoriesFragment myStoriesFragment, View view, MyStoriesState myStoriesState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(myStoriesFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(myStoriesState, "it");
        dSLSettingsAdapter.submitList(myStoriesFragment.getConfiguration(myStoriesState).toMappingModelList());
        Intrinsics.checkNotNullExpressionValue(view, "emptyNotice");
        ViewExtensionsKt.setVisible(view, myStoriesState.getDistributionSets().isEmpty());
    }

    private final DSLConfiguration getConfiguration(MyStoriesState myStoriesState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(myStoriesState, this) { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1
            final /* synthetic */ MyStoriesState $state;
            final /* synthetic */ MyStoriesFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00d0: INVOKE  
                  (r21v0 'dSLConfiguration' org.thoughtcrime.securesms.components.settings.DSLConfiguration)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model : 0x00cd: CONSTRUCTOR  (r10v4 org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model A[REMOVE]) = 
                  (r12v1 'conversationMessage' org.thoughtcrime.securesms.conversation.ConversationMessage A[REMOVE])
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1 : 0x00a3: CONSTRUCTOR  (r13v0 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$2 : 0x00a8: CONSTRUCTOR  (r14v0 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$2 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$2.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$3 : 0x00ad: CONSTRUCTOR  (r15v0 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$3 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$3.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$4 : 0x00b2: CONSTRUCTOR  (r11v0 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$4 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$4.<init>(java.lang.Object):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$5 : 0x00b7: CONSTRUCTOR  (r5v3 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$5 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$5.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$6 : 0x00bc: CONSTRUCTOR  (r6v3 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$6 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$6.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$7 : 0x00c1: CONSTRUCTOR  (r0v3 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$7 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$7.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                 call: org.thoughtcrime.securesms.stories.my.MyStoriesItem.Model.<init>(org.thoughtcrime.securesms.conversation.ConversationMessage, kotlin.jvm.functions.Function2, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function2):void type: CONSTRUCTOR)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLConfiguration.customPref(org.thoughtcrime.securesms.util.adapter.mapping.MappingModel):void in method: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:221)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:221)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00cd: CONSTRUCTOR  (r10v4 org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model A[REMOVE]) = 
                  (r12v1 'conversationMessage' org.thoughtcrime.securesms.conversation.ConversationMessage A[REMOVE])
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1 : 0x00a3: CONSTRUCTOR  (r13v0 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$2 : 0x00a8: CONSTRUCTOR  (r14v0 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$2 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$2.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$3 : 0x00ad: CONSTRUCTOR  (r15v0 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$3 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$3.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$4 : 0x00b2: CONSTRUCTOR  (r11v0 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$4 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$4.<init>(java.lang.Object):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$5 : 0x00b7: CONSTRUCTOR  (r5v3 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$5 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$5.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$6 : 0x00bc: CONSTRUCTOR  (r6v3 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$6 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$6.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$7 : 0x00c1: CONSTRUCTOR  (r0v3 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$7 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$7.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR)
                 call: org.thoughtcrime.securesms.stories.my.MyStoriesItem.Model.<init>(org.thoughtcrime.securesms.conversation.ConversationMessage, kotlin.jvm.functions.Function2, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function2):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 22 more
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00a3: CONSTRUCTOR  (r13v0 org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1 A[REMOVE]) = (r2v4 'myStoriesFragment' org.thoughtcrime.securesms.stories.my.MyStoriesFragment) call: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1.<init>(org.thoughtcrime.securesms.stories.my.MyStoriesFragment):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 28 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 34 more
                */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r21) {
                /*
                    r20 = this;
                    r0 = r20
                    r1 = r21
                    java.lang.String r2 = "$this$configure"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r1, r2)
                    org.thoughtcrime.securesms.stories.my.MyStoriesState r2 = r0.$state
                    java.util.List r2 = r2.getDistributionSets()
                    java.util.ArrayList r3 = new java.util.ArrayList
                    r3.<init>()
                    java.util.Iterator r2 = r2.iterator()
                L_0x0018:
                    boolean r4 = r2.hasNext()
                    r5 = 1
                    if (r4 == 0) goto L_0x0035
                    java.lang.Object r4 = r2.next()
                    r6 = r4
                    org.thoughtcrime.securesms.stories.my.MyStoriesState$DistributionSet r6 = (org.thoughtcrime.securesms.stories.my.MyStoriesState.DistributionSet) r6
                    java.util.List r6 = r6.getStories()
                    boolean r6 = r6.isEmpty()
                    r5 = r5 ^ r6
                    if (r5 == 0) goto L_0x0018
                    r3.add(r4)
                    goto L_0x0018
                L_0x0035:
                    org.thoughtcrime.securesms.stories.my.MyStoriesFragment r2 = r0.this$0
                    java.util.Iterator r4 = r3.iterator()
                    r6 = 0
                    r7 = 0
                L_0x003d:
                    boolean r8 = r4.hasNext()
                    if (r8 == 0) goto L_0x00e8
                    java.lang.Object r8 = r4.next()
                    int r9 = r7 + 1
                    if (r7 >= 0) goto L_0x004e
                    kotlin.collections.CollectionsKt.throwIndexOverflow()
                L_0x004e:
                    org.thoughtcrime.securesms.stories.my.MyStoriesState$DistributionSet r8 = (org.thoughtcrime.securesms.stories.my.MyStoriesState.DistributionSet) r8
                    java.lang.String r10 = r8.getLabel()
                    if (r10 != 0) goto L_0x007b
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion r10 = org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion
                    r11 = 2131953209(0x7f130639, float:1.9542883E38)
                    java.lang.Object[] r12 = new java.lang.Object[r5]
                    org.thoughtcrime.securesms.recipients.Recipient r13 = org.thoughtcrime.securesms.recipients.Recipient.self()
                    android.content.Context r14 = r2.requireContext()
                    java.lang.String r13 = r13.getShortDisplayName(r14)
                    r12[r6] = r13
                    java.lang.String r11 = r2.getString(r11, r12)
                    java.lang.String r12 = "getString(R.string.MySto…ayName(requireContext()))"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r11, r12)
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r12 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r6]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r10 = r10.from(r11, r12)
                    goto L_0x0087
                L_0x007b:
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion r10 = org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion
                    java.lang.String r11 = r8.getLabel()
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r12 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r6]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r10 = r10.from(r11, r12)
                L_0x0087:
                    r1.sectionHeaderPref(r10)
                    java.util.List r8 = r8.getStories()
                    java.util.Iterator r8 = r8.iterator()
                L_0x0092:
                    boolean r10 = r8.hasNext()
                    if (r10 == 0) goto L_0x00d8
                    java.lang.Object r10 = r8.next()
                    r12 = r10
                    org.thoughtcrime.securesms.conversation.ConversationMessage r12 = (org.thoughtcrime.securesms.conversation.ConversationMessage) r12
                    org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model r10 = new org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model
                    org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1 r13 = new org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$1
                    r13.<init>(r2)
                    org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$2 r14 = new org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$2
                    r14.<init>(r2)
                    org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$3 r15 = new org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$3
                    r15.<init>(r2)
                    org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$4 r11 = new org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$4
                    r11.<init>(r2)
                    org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$5 r5 = new org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$5
                    r5.<init>(r2)
                    org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$6 r6 = new org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$6
                    r6.<init>(r2)
                    org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$7 r0 = new org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$7
                    r0.<init>(r2)
                    r16 = r11
                    r11 = r10
                    r17 = r5
                    r18 = r6
                    r19 = r0
                    r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)
                    r1.customPref(r10)
                    r0 = r20
                    r5 = 1
                    r6 = 0
                    goto L_0x0092
                L_0x00d8:
                    int r0 = kotlin.collections.CollectionsKt.getLastIndex(r3)
                    if (r7 == r0) goto L_0x00e1
                    r21.dividerPref()
                L_0x00e1:
                    r0 = r20
                    r7 = r9
                    r5 = 1
                    r6 = 0
                    goto L_0x003d
                L_0x00e8:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    public final void openStoryViewer(MyStoriesItem.Model model, View view, boolean z) {
        Recipient recipient;
        Pair pair;
        if (!model.getDistributionStory().getMessageRecord().isOutgoing() || !model.getDistributionStory().getMessageRecord().isFailed()) {
            if (model.getDistributionStory().getMessageRecord().getRecipient().isGroup()) {
                recipient = model.getDistributionStory().getMessageRecord().getRecipient();
            } else {
                recipient = Recipient.self();
            }
            MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) model.getDistributionStory().getMessageRecord();
            Slide thumbnailSlide = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
            BlurHash placeholderBlur = thumbnailSlide != null ? thumbnailSlide.getPlaceholderBlur() : null;
            if (mmsMessageRecord.getStoryType().isTextStory()) {
                pair = TuplesKt.to(StoryTextPostModel.CREATOR.parseFrom(mmsMessageRecord), null);
            } else {
                Slide thumbnailSlide2 = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
                pair = TuplesKt.to(null, thumbnailSlide2 != null ? thumbnailSlide2.getUri() : null);
            }
            StoryTextPostModel storyTextPostModel = (StoryTextPostModel) pair.component1();
            Uri uri = (Uri) pair.component2();
            FragmentActivity requireActivity = requireActivity();
            String transitionName = ViewCompat.getTransitionName(view);
            if (transitionName == null) {
                transitionName = "";
            }
            ActivityOptionsCompat makeSceneTransitionAnimation = ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity, view, transitionName);
            Intrinsics.checkNotNullExpressionValue(makeSceneTransitionAnimation, "makeSceneTransitionAnima…itionName(preview) ?: \"\")");
            StoryViewerActivity.Companion companion = StoryViewerActivity.Companion;
            Context requireContext = requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            RecipientId id = recipient.getId();
            long id2 = model.getDistributionStory().getMessageRecord().getId();
            boolean shouldHideStory = recipient.shouldHideStory();
            Intrinsics.checkNotNullExpressionValue(id, ContactRepository.ID_COLUMN);
            startActivity(companion.createIntent(requireContext, new StoryViewerArgs(id, shouldHideStory, id2, storyTextPostModel, uri, placeholderBlur, null, false, 0, false, z, false, true, 3008, null)), makeSceneTransitionAnimation.toBundle());
        } else if (model.getDistributionStory().getMessageRecord().isIdentityMismatchFailure()) {
            Context requireContext2 = requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext2, "requireContext()");
            MessageRecord messageRecord = model.getDistributionStory().getMessageRecord();
            Intrinsics.checkNotNullExpressionValue(messageRecord, "it.distributionStory.messageRecord");
            SafetyNumberBottomSheet.Factory forMessageRecord = SafetyNumberBottomSheet.forMessageRecord(requireContext2, messageRecord);
            FragmentManager childFragmentManager = getChildFragmentManager();
            Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
            forMessageRecord.show(childFragmentManager);
        } else {
            StoryDialogs storyDialogs = StoryDialogs.INSTANCE;
            Context requireContext3 = requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext3, "requireContext()");
            storyDialogs.resendStory(requireContext3, new Function0<Unit>(this, model) { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$openStoryViewer$1
                final /* synthetic */ MyStoriesItem.Model $it;
                final /* synthetic */ MyStoriesFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                    this.$it = r2;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    LifecycleDisposable lifecycleDisposable = this.this$0.lifecycleDisposable;
                    MyStoriesViewModel myStoriesViewModel = this.this$0.getViewModel();
                    MessageRecord messageRecord2 = this.$it.getDistributionStory().getMessageRecord();
                    Intrinsics.checkNotNullExpressionValue(messageRecord2, "it.distributionStory.messageRecord");
                    Disposable subscribe = myStoriesViewModel.resend(messageRecord2).subscribe();
                    Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.resend(it.dist…essageRecord).subscribe()");
                    lifecycleDisposable.plusAssign(subscribe);
                }
            });
        }
    }

    public final void handleDeleteClick(MyStoriesItem.Model model) {
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        StoryContextMenu storyContextMenu = StoryContextMenu.INSTANCE;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        Disposable subscribe = storyContextMenu.delete(requireContext, SetsKt__SetsJVMKt.setOf(model.getDistributionStory().getMessageRecord())).subscribe();
        Intrinsics.checkNotNullExpressionValue(subscribe, "StoryContextMenu.delete(…ssageRecord)).subscribe()");
        lifecycleDisposable.plusAssign(subscribe);
    }
}
