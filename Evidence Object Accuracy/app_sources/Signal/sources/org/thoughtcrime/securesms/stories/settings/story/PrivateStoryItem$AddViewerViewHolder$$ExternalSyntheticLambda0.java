package org.thoughtcrime.securesms.stories.settings.story;

import android.view.View;
import org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class PrivateStoryItem$AddViewerViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ PrivateStoryItem.AddViewerModel f$0;

    public /* synthetic */ PrivateStoryItem$AddViewerViewHolder$$ExternalSyntheticLambda0(PrivateStoryItem.AddViewerModel addViewerModel) {
        this.f$0 = addViewerModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        PrivateStoryItem.AddViewerViewHolder.m2955bind$lambda0(this.f$0, view);
    }
}
