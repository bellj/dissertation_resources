package org.thoughtcrime.securesms.stories.settings.my;

import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.FragmentKt;
import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.rxjava3.disposables.Disposable;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.WrapperDialogFragment;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: MyStorySettingsFragment.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00192\u00020\u0001:\u0002\u0019\u001aB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\b\u0010\u0013\u001a\u00020\fH\u0016J\u001a\u0010\u0014\u001a\u00020\f2\u0006\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "viewModel", "Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsState;", "onResume", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "Dialog", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStorySettingsFragment extends DSLSettingsFragment {
    public static final Companion Companion = new Companion(null);
    private LifecycleDisposable lifecycleDisposable;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MyStorySettingsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    public MyStorySettingsFragment() {
        super(R.string.MyStorySettingsFragment__my_story, 0, 0, null, 14, null);
    }

    public final MyStorySettingsViewModel getViewModel() {
        return (MyStorySettingsViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
        this.lifecycleDisposable = lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        super.onViewCreated(view, bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().refresh();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ MyStorySettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MyStorySettingsFragment.m2902bindAdapter$lambda0(DSLSettingsAdapter.this, this.f$1, (MyStorySettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m2902bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, MyStorySettingsFragment myStorySettingsFragment, MyStorySettingsState myStorySettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(myStorySettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(myStorySettingsState, "state");
        dSLSettingsAdapter.submitList(myStorySettingsFragment.getConfiguration(myStorySettingsState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(MyStorySettingsState myStorySettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(myStorySettingsState, this) { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment$getConfiguration$1
            final /* synthetic */ MyStorySettingsState $state;
            final /* synthetic */ MyStorySettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                DSLSettingsText dSLSettingsText;
                DSLSettingsText dSLSettingsText2;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.sectionHeaderPref(R.string.MyStorySettingsFragment__who_can_see_this_story);
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.MyStorySettingsFragment__all_signal_connections, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from2 = companion.from(R.string.MyStorySettingsFragment__share_with_all_connections, new DSLSettingsText.Modifier[0]);
                boolean z = true;
                boolean z2 = this.$state.getMyStoryPrivacyState().getPrivacyMode() == DistributionListPrivacyMode.ALL;
                final MyStorySettingsFragment myStorySettingsFragment = this.this$0;
                DSLConfiguration.radioPref$default(dSLConfiguration, from, from2, false, z2, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        LifecycleDisposable lifecycleDisposable = myStorySettingsFragment.lifecycleDisposable;
                        if (lifecycleDisposable == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("lifecycleDisposable");
                            lifecycleDisposable = null;
                        }
                        Disposable subscribe = myStorySettingsFragment.getViewModel().setMyStoryPrivacyMode(DistributionListPrivacyMode.ALL).subscribe();
                        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.setMyStoryPriv…\n            .subscribe()");
                        lifecycleDisposable.plusAssign(subscribe);
                    }
                }, 4, null);
                DistributionListPrivacyMode privacyMode = this.$state.getMyStoryPrivacyState().getPrivacyMode();
                DistributionListPrivacyMode distributionListPrivacyMode = DistributionListPrivacyMode.ALL_EXCEPT;
                if (privacyMode == distributionListPrivacyMode) {
                    String quantityString = this.this$0.getResources().getQuantityString(R.plurals.MyStorySettingsFragment__d_people_excluded, this.$state.getMyStoryPrivacyState().getConnectionCount(), Integer.valueOf(this.$state.getMyStoryPrivacyState().getConnectionCount()));
                    Intrinsics.checkNotNullExpressionValue(quantityString, "resources.getQuantityStr…acyState.connectionCount)");
                    dSLSettingsText = companion.from(quantityString, new DSLSettingsText.Modifier[0]);
                } else {
                    dSLSettingsText = companion.from(R.string.MyStorySettingsFragment__hide_your_story_from_specific_people, new DSLSettingsText.Modifier[0]);
                }
                DSLSettingsText from3 = companion.from(R.string.MyStorySettingsFragment__all_signal_connections_except, new DSLSettingsText.Modifier[0]);
                boolean z3 = this.$state.getMyStoryPrivacyState().getPrivacyMode() == distributionListPrivacyMode;
                final MyStorySettingsFragment myStorySettingsFragment2 = this.this$0;
                DSLConfiguration.radioPref$default(dSLConfiguration, from3, dSLSettingsText, false, z3, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        LifecycleDisposable lifecycleDisposable = myStorySettingsFragment2.lifecycleDisposable;
                        if (lifecycleDisposable == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("lifecycleDisposable");
                            lifecycleDisposable = null;
                        }
                        Disposable subscribe = myStorySettingsFragment2.getViewModel().setMyStoryPrivacyMode(DistributionListPrivacyMode.ALL_EXCEPT).subscribe(new MyStorySettingsFragment$getConfiguration$1$2$$ExternalSyntheticLambda0(myStorySettingsFragment2));
                        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.setMyStoryPriv…s_to_allExceptFragment) }");
                        lifecycleDisposable.plusAssign(subscribe);
                    }

                    /* access modifiers changed from: private */
                    /* renamed from: invoke$lambda-0  reason: not valid java name */
                    public static final void m2904invoke$lambda0(MyStorySettingsFragment myStorySettingsFragment3) {
                        Intrinsics.checkNotNullParameter(myStorySettingsFragment3, "this$0");
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(myStorySettingsFragment3), (int) R.id.action_myStorySettings_to_allExceptFragment);
                    }
                }, 4, null);
                DistributionListPrivacyMode privacyMode2 = this.$state.getMyStoryPrivacyState().getPrivacyMode();
                DistributionListPrivacyMode distributionListPrivacyMode2 = DistributionListPrivacyMode.ONLY_WITH;
                if (privacyMode2 == distributionListPrivacyMode2) {
                    String quantityString2 = this.this$0.getResources().getQuantityString(R.plurals.MyStorySettingsFragment__d_people, this.$state.getMyStoryPrivacyState().getConnectionCount(), Integer.valueOf(this.$state.getMyStoryPrivacyState().getConnectionCount()));
                    Intrinsics.checkNotNullExpressionValue(quantityString2, "resources.getQuantityStr…acyState.connectionCount)");
                    dSLSettingsText2 = companion.from(quantityString2, new DSLSettingsText.Modifier[0]);
                } else {
                    dSLSettingsText2 = companion.from(R.string.MyStorySettingsFragment__only_share_with_selected_people, new DSLSettingsText.Modifier[0]);
                }
                DSLSettingsText from4 = companion.from(R.string.MyStorySettingsFragment__only_share_with, new DSLSettingsText.Modifier[0]);
                if (this.$state.getMyStoryPrivacyState().getPrivacyMode() != distributionListPrivacyMode2) {
                    z = false;
                }
                final MyStorySettingsFragment myStorySettingsFragment3 = this.this$0;
                DSLConfiguration.radioPref$default(dSLConfiguration, from4, dSLSettingsText2, false, z, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment$getConfiguration$1.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        LifecycleDisposable lifecycleDisposable = myStorySettingsFragment3.lifecycleDisposable;
                        if (lifecycleDisposable == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("lifecycleDisposable");
                            lifecycleDisposable = null;
                        }
                        Disposable subscribe = myStorySettingsFragment3.getViewModel().setMyStoryPrivacyMode(DistributionListPrivacyMode.ONLY_WITH).subscribe(new MyStorySettingsFragment$getConfiguration$1$3$$ExternalSyntheticLambda0(myStorySettingsFragment3));
                        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.setMyStoryPriv…_onlyShareWithFragment) }");
                        lifecycleDisposable.plusAssign(subscribe);
                    }

                    /* access modifiers changed from: private */
                    /* renamed from: invoke$lambda-0  reason: not valid java name */
                    public static final void m2905invoke$lambda0(MyStorySettingsFragment myStorySettingsFragment4) {
                        Intrinsics.checkNotNullParameter(myStorySettingsFragment4, "this$0");
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(myStorySettingsFragment4), (int) R.id.action_myStorySettings_to_onlyShareWithFragment);
                    }
                }, 4, null);
                DSLSettingsText from5 = companion.from(R.string.MyStorySettingsFragment__choose_who_can_view_your_story, new DSLSettingsText.Modifier[0]);
                final MyStorySettingsFragment myStorySettingsFragment4 = this.this$0;
                DSLConfiguration.learnMoreTextPref$default(dSLConfiguration, null, from5, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment$getConfiguration$1.4
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(myStorySettingsFragment4), (int) R.id.action_myStorySettings_to_signalConnectionsBottomSheet);
                    }
                }, 1, null);
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.MyStorySettingsFragment__replies_amp_reactions);
                DSLSettingsText from6 = companion.from(R.string.MyStorySettingsFragment__allow_replies_amp_reactions, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from7 = companion.from(R.string.MyStorySettingsFragment__let_people_who_can_view_your_story_react_and_reply, new DSLSettingsText.Modifier[0]);
                boolean areRepliesAndReactionsEnabled = this.$state.getAreRepliesAndReactionsEnabled();
                final MyStorySettingsFragment myStorySettingsFragment5 = this.this$0;
                final MyStorySettingsState myStorySettingsState2 = this.$state;
                dSLConfiguration.switchPref(from6, (r16 & 2) != 0 ? null : from7, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, areRepliesAndReactionsEnabled, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment$getConfiguration$1.5
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        myStorySettingsFragment5.getViewModel().setRepliesAndReactionsEnabled(!myStorySettingsState2.getAreRepliesAndReactionsEnabled());
                    }
                });
            }
        });
    }

    /* compiled from: MyStorySettingsFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsFragment$Dialog;", "Lorg/thoughtcrime/securesms/components/WrapperDialogFragment;", "()V", "getWrappedFragment", "Landroidx/fragment/app/Fragment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Dialog extends WrapperDialogFragment {
        @Override // org.thoughtcrime.securesms.components.WrapperDialogFragment
        public Fragment getWrappedFragment() {
            NavHostFragment create = NavHostFragment.create(R.navigation.my_story_settings);
            Intrinsics.checkNotNullExpressionValue(create, "create(R.navigation.my_story_settings)");
            return create;
        }
    }

    /* compiled from: MyStorySettingsFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsFragment$Companion;", "", "()V", "createAsDialog", "Landroidx/fragment/app/DialogFragment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final DialogFragment createAsDialog() {
            return new Dialog();
        }
    }
}
