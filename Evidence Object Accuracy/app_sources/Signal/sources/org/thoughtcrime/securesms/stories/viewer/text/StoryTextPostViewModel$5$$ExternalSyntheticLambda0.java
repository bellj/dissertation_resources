package org.thoughtcrime.securesms.stories.viewer.text;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryTextPostViewModel$5$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ StoryTextPost f$0;
    public final /* synthetic */ LinkPreview f$1;

    public /* synthetic */ StoryTextPostViewModel$5$$ExternalSyntheticLambda0(StoryTextPost storyTextPost, LinkPreview linkPreview) {
        this.f$0 = storyTextPost;
        this.f$1 = linkPreview;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return StoryTextPostViewModel.AnonymousClass5.m3202invoke$lambda0(this.f$0, this.f$1, (StoryTextPostState) obj);
    }
}
