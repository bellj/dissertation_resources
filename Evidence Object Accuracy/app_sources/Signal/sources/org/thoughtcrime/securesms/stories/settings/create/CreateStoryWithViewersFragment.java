package org.thoughtcrime.securesms.stories.settings.create;

import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import j$.util.function.Function;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.settings.create.CreateStoryNameFieldItem;
import org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersState;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel;
import org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder;

/* compiled from: CreateStoryWithViewersFragment.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u0000 !2\u00020\u0001:\u0002 !B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0017\u001a\u00020\u0005H\u0002J\u0014\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0002J\u0014\u0010\u001c\u001a\u00020\u000f*\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002R\u001a\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u001b\u0010\b\u001a\u00020\t8BX\u0002¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000b¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "recipientIds", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getRecipientIds", "()[Lorg/thoughtcrime/securesms/recipients/RecipientId;", "viewModel", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState;", "onDone", "recipientId", "presentError", "", "error", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$NameError;", "setCanPress", "Landroid/view/View;", "canPress", "", "Callback", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class CreateStoryWithViewersFragment extends DSLSettingsFragment {
    public static final Companion Companion = new Companion(null);
    public static final String REQUEST_KEY;
    public static final String STORY_RECIPIENT;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(CreateStoryWithViewersViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, CreateStoryWithViewersFragment$viewModel$2.INSTANCE);

    /* compiled from: CreateStoryWithViewersFragment.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersFragment$Callback;", "", "onDone", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onDone(RecipientId recipientId);
    }

    /* compiled from: CreateStoryWithViewersFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[CreateStoryWithViewersState.NameError.values().length];
            iArr[CreateStoryWithViewersState.NameError.NO_LABEL.ordinal()] = 1;
            iArr[CreateStoryWithViewersState.NameError.DUPLICATE_LABEL.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:17:0x001b */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v4, types: [org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment$Callback] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v11 */
    /* JADX WARN: Type inference failed for: r0v12 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void onDone(org.thoughtcrime.securesms.recipients.RecipientId r3) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.getParentFragment()
        L_0x0004:
            if (r0 == 0) goto L_0x0010
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment.Callback
            if (r1 == 0) goto L_0x000b
            goto L_0x001b
        L_0x000b:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x0004
        L_0x0010:
            androidx.fragment.app.FragmentActivity r0 = r2.requireActivity()
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment.Callback
            if (r1 != 0) goto L_0x0019
            r0 = 0
        L_0x0019:
            org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment$Callback r0 = (org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment.Callback) r0
        L_0x001b:
            org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment$Callback r0 = (org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment.Callback) r0
            if (r0 == 0) goto L_0x0023
            r0.onDone(r3)
            goto L_0x003f
        L_0x0023:
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
            java.lang.String r1 = "story-recipient"
            r0.putParcelable(r1, r3)
            kotlin.Unit r3 = kotlin.Unit.INSTANCE
            java.lang.String r3 = "new-story"
            androidx.fragment.app.FragmentKt.setFragmentResult(r2, r3, r0)
            androidx.navigation.NavController r3 = androidx.navigation.fragment.FragmentKt.findNavController(r2)
            r0 = 2131362740(0x7f0a03b4, float:1.834527E38)
            r1 = 1
            r3.popBackStack(r0, r1)
        L_0x003f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment.onDone(org.thoughtcrime.securesms.recipients.RecipientId):void");
    }

    public CreateStoryWithViewersFragment() {
        super(R.string.CreateStoryWithViewersFragment__name_story, 0, R.layout.stories_create_with_recipients_fragment, null, 10, null);
    }

    /* compiled from: CreateStoryWithViewersFragment.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersFragment$Companion;", "", "()V", "REQUEST_KEY", "", "STORY_RECIPIENT", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final CreateStoryWithViewersViewModel getViewModel() {
        return (CreateStoryWithViewersViewModel) this.viewModel$delegate.getValue();
    }

    public final RecipientId[] getRecipientIds() {
        RecipientId[] recipients = CreateStoryWithViewersFragmentArgs.fromBundle(requireArguments()).getRecipients();
        Intrinsics.checkNotNullExpressionValue(recipients, "fromBundle(requireArguments()).recipients");
        return recipients;
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final MappingViewHolder m2861bindAdapter$lambda0(View view) {
        return new RecipientViewHolder(view, null);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        dSLSettingsAdapter.registerFactory(RecipientMappingModel.RecipientIdMappingModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return CreateStoryWithViewersFragment.m2861bindAdapter$lambda0((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_recipient_item));
        CreateStoryNameFieldItem.INSTANCE.register(dSLSettingsAdapter, new Function1<CharSequence, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment$bindAdapter$2
            final /* synthetic */ CreateStoryWithViewersFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(CharSequence charSequence) {
                invoke(charSequence);
                return Unit.INSTANCE;
            }

            public final void invoke(CharSequence charSequence) {
                Intrinsics.checkNotNullParameter(charSequence, "it");
                this.this$0.getViewModel().setLabel(charSequence);
            }
        });
        View findViewById = requireView().findViewById(R.id.create);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewById(R.id.create)");
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CreateStoryWithViewersFragment.m2862bindAdapter$lambda1(CreateStoryWithViewersFragment.this, view);
            }
        });
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this, findViewById) { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ CreateStoryWithViewersFragment f$1;
            public final /* synthetic */ View f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CreateStoryWithViewersFragment.m2863bindAdapter$lambda2(DSLSettingsAdapter.this, this.f$1, this.f$2, (CreateStoryWithViewersState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m2862bindAdapter$lambda1(CreateStoryWithViewersFragment createStoryWithViewersFragment, View view) {
        Intrinsics.checkNotNullParameter(createStoryWithViewersFragment, "this$0");
        createStoryWithViewersFragment.getViewModel().create(ArraysKt___ArraysKt.toSet(createStoryWithViewersFragment.getRecipientIds()));
    }

    /* renamed from: bindAdapter$lambda-2 */
    public static final void m2863bindAdapter$lambda2(DSLSettingsAdapter dSLSettingsAdapter, CreateStoryWithViewersFragment createStoryWithViewersFragment, View view, CreateStoryWithViewersState createStoryWithViewersState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(createStoryWithViewersFragment, "this$0");
        Intrinsics.checkNotNullParameter(view, "$createButton");
        Intrinsics.checkNotNullExpressionValue(createStoryWithViewersState, "state");
        dSLSettingsAdapter.submitList(createStoryWithViewersFragment.getConfiguration(createStoryWithViewersState).toMappingModelList());
        CreateStoryWithViewersState.SaveState saveState = createStoryWithViewersState.getSaveState();
        boolean z = false;
        if (Intrinsics.areEqual(saveState, CreateStoryWithViewersState.SaveState.Init.INSTANCE)) {
            if (createStoryWithViewersState.getLabel().length() > 0) {
                z = true;
            }
            createStoryWithViewersFragment.setCanPress(view, z);
        } else if (Intrinsics.areEqual(saveState, CreateStoryWithViewersState.SaveState.Saving.INSTANCE)) {
            createStoryWithViewersFragment.setCanPress(view, false);
        } else if (saveState instanceof CreateStoryWithViewersState.SaveState.Saved) {
            createStoryWithViewersFragment.onDone(((CreateStoryWithViewersState.SaveState.Saved) createStoryWithViewersState.getSaveState()).getRecipientId());
        }
    }

    private final void setCanPress(View view, boolean z) {
        view.setEnabled(z);
        view.setAlpha(z ? 1.0f : 0.5f);
    }

    private final DSLConfiguration getConfiguration(CreateStoryWithViewersState createStoryWithViewersState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(createStoryWithViewersState, this) { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment$getConfiguration$1
            final /* synthetic */ CreateStoryWithViewersState $state;
            final /* synthetic */ CreateStoryWithViewersFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.customPref(new CreateStoryNameFieldItem.Model(this.$state.getLabel(), this.this$0.presentError(this.$state.getError())));
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.CreateStoryWithViewersFragment__viewers);
                for (RecipientId recipientId : this.this$0.getRecipientIds()) {
                    dSLConfiguration.customPref(new RecipientMappingModel.RecipientIdMappingModel(recipientId));
                }
            }
        });
    }

    public final String presentError(CreateStoryWithViewersState.NameError nameError) {
        int i = nameError == null ? -1 : WhenMappings.$EnumSwitchMapping$0[nameError.ordinal()];
        if (i == 1) {
            return getString(R.string.CreateStoryWithViewersFragment__this_field_is_required);
        }
        if (i != 2) {
            return null;
        }
        return getString(R.string.CreateStoryWithViewersFragment__there_is_already_a_story_with_this_name);
    }
}
