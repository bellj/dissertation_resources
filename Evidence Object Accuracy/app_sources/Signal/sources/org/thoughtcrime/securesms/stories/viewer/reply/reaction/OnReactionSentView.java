package org.thoughtcrime.securesms.stories.viewer.reply.reaction;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.motion.widget.TransitionAdapter;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiImageView;

/* compiled from: OnReactionSentView.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0013B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/reaction/OnReactionSentView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "callback", "Lorg/thoughtcrime/securesms/stories/viewer/reply/reaction/OnReactionSentView$Callback;", "getCallback", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/reaction/OnReactionSentView$Callback;", "setCallback", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/reaction/OnReactionSentView$Callback;)V", "motionLayout", "Landroidx/constraintlayout/motion/widget/MotionLayout;", "playForEmoji", "", "emoji", "", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class OnReactionSentView extends FrameLayout {
    private Callback callback;
    private final MotionLayout motionLayout;

    /* compiled from: OnReactionSentView.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/reaction/OnReactionSentView$Callback;", "", "onFinished", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onFinished();
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public OnReactionSentView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ OnReactionSentView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public OnReactionSentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        FrameLayout.inflate(context, R.layout.on_reaction_sent_view, this);
        View findViewById = findViewById(R.id.motion_layout);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.motion_layout)");
        MotionLayout motionLayout = (MotionLayout) findViewById;
        this.motionLayout = motionLayout;
        motionLayout.addTransitionListener(new TransitionAdapter(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.reaction.OnReactionSentView.1
            final /* synthetic */ OnReactionSentView this$0;

            {
                this.this$0 = r1;
            }

            @Override // androidx.constraintlayout.motion.widget.TransitionAdapter, androidx.constraintlayout.motion.widget.MotionLayout.TransitionListener
            public void onTransitionCompleted(MotionLayout motionLayout2, int i) {
                this.this$0.motionLayout.setProgress(0.0f);
                Callback callback = this.this$0.getCallback();
                if (callback != null) {
                    callback.onFinished();
                }
            }
        });
    }

    public final Callback getCallback() {
        return this.callback;
    }

    public final void setCallback(Callback callback) {
        this.callback = callback;
    }

    public final void playForEmoji(CharSequence charSequence) {
        Intrinsics.checkNotNullParameter(charSequence, "emoji");
        this.motionLayout.setProgress(0.0f);
        for (Number number : CollectionsKt__CollectionsKt.listOf((Object[]) new Integer[]{Integer.valueOf((int) R.id.emoji_1), Integer.valueOf((int) R.id.emoji_2), Integer.valueOf((int) R.id.emoji_3), Integer.valueOf((int) R.id.emoji_4), Integer.valueOf((int) R.id.emoji_5), Integer.valueOf((int) R.id.emoji_6), Integer.valueOf((int) R.id.emoji_7), Integer.valueOf((int) R.id.emoji_8), Integer.valueOf((int) R.id.emoji_9), Integer.valueOf((int) R.id.emoji_10), Integer.valueOf((int) R.id.emoji_11)})) {
            ((EmojiImageView) findViewById(number.intValue())).setImageEmoji(charSequence);
        }
        this.motionLayout.requestLayout();
        this.motionLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.reaction.OnReactionSentView$playForEmoji$$inlined$doOnNextLayout$1
            final /* synthetic */ OnReactionSentView this$0;

            {
                this.this$0 = r1;
            }

            @Override // android.view.View.OnLayoutChangeListener
            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                Intrinsics.checkNotNullParameter(view, "view");
                view.removeOnLayoutChangeListener(this);
                this.this$0.motionLayout.transitionToEnd();
            }
        });
    }
}
