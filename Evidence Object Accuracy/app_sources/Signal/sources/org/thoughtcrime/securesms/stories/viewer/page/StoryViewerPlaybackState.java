package org.thoughtcrime.securesms.stories.viewer.page;

import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;

/* compiled from: StoryViewerPlaybackState.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b1\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001BÃ\u0001\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0002\u0010\b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\t\u001a\u00020\u0003\u0012\b\b\u0002\u0010\n\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\f\u001a\u00020\u0003\u0012\b\b\u0002\u0010\r\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u0003¢\u0006\u0002\u0010\u0016J\t\u0010\u001e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\u0003HÆ\u0003J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0003HÆ\u0003J\t\u0010%\u001a\u00020\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u0003HÆ\u0003J\t\u0010'\u001a\u00020\u0003HÆ\u0003J\t\u0010(\u001a\u00020\u0003HÆ\u0003J\t\u0010)\u001a\u00020\u0003HÆ\u0003J\t\u0010*\u001a\u00020\u0003HÆ\u0003J\t\u0010+\u001a\u00020\u0003HÆ\u0003J\t\u0010,\u001a\u00020\u0003HÆ\u0003J\t\u0010-\u001a\u00020\u0003HÆ\u0003J\t\u0010.\u001a\u00020\u0003HÆ\u0003J\t\u0010/\u001a\u00020\u0003HÆ\u0003J\t\u00100\u001a\u00020\u0003HÆ\u0003JÇ\u0001\u00101\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u00032\b\b\u0002\u0010\u000f\u001a\u00020\u00032\b\b\u0002\u0010\u0010\u001a\u00020\u00032\b\b\u0002\u0010\u0011\u001a\u00020\u00032\b\b\u0002\u0010\u0012\u001a\u00020\u00032\b\b\u0002\u0010\u0013\u001a\u00020\u00032\b\b\u0002\u0010\u0014\u001a\u00020\u00032\b\b\u0002\u0010\u0015\u001a\u00020\u0003HÆ\u0001J\u0013\u00102\u001a\u00020\u00032\b\u00103\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00104\u001a\u000205HÖ\u0001J\t\u00106\u001a\u000207HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\u0019\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0018R\u0011\u0010\u001b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0018R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0018R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0018R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0018R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0018R\u0011\u0010\u0012\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0018R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0018R\u0011\u0010\u0013\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0018R\u0011\u0010\u000f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0018R\u0011\u0010\u0010\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0018R\u0011\u0010\r\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u0018R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0018R\u0011\u0010\u000e\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0018R\u0011\u0010\u001d\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0018R\u0011\u0010\u0011\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0018R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0018R\u0011\u0010\u0014\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0018R\u0011\u0010\u0015\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0018R\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\u0018R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0018¨\u00068"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPlaybackState;", "", "areSegmentsInitialized", "", "isUserTouching", "isDisplayingForwardDialog", "isDisplayingDeleteDialog", "isDisplayingContextMenu", "isDisplayingViewsAndRepliesDialog", "isDisplayingDirectReplyDialog", "isDisplayingCaptionOverlay", "isUserScrollingParent", "isSelectedPage", "isDisplayingSlate", "isFragmentResumed", "isDisplayingLinkPreviewTooltip", "isDisplayingReactionAnimation", "isRunningSharedElementAnimation", "isDisplayingFirstTimeNavigation", "isDisplayingInfoDialog", "isUserLongTouching", "isUserScrollingChild", "(ZZZZZZZZZZZZZZZZZZZ)V", "getAreSegmentsInitialized", "()Z", "hideChrome", "getHideChrome", "hideChromeImmediate", "getHideChromeImmediate", "isPaused", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewerPlaybackState {
    private final boolean areSegmentsInitialized;
    private final boolean hideChrome;
    private final boolean hideChromeImmediate;
    private final boolean isDisplayingCaptionOverlay;
    private final boolean isDisplayingContextMenu;
    private final boolean isDisplayingDeleteDialog;
    private final boolean isDisplayingDirectReplyDialog;
    private final boolean isDisplayingFirstTimeNavigation;
    private final boolean isDisplayingForwardDialog;
    private final boolean isDisplayingInfoDialog;
    private final boolean isDisplayingLinkPreviewTooltip;
    private final boolean isDisplayingReactionAnimation;
    private final boolean isDisplayingSlate;
    private final boolean isDisplayingViewsAndRepliesDialog;
    private final boolean isFragmentResumed;
    private final boolean isPaused;
    private final boolean isRunningSharedElementAnimation;
    private final boolean isSelectedPage;
    private final boolean isUserLongTouching;
    private final boolean isUserScrollingChild;
    private final boolean isUserScrollingParent;
    private final boolean isUserTouching;

    public StoryViewerPlaybackState() {
        this(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 524287, null);
    }

    public final boolean component1() {
        return this.areSegmentsInitialized;
    }

    public final boolean component10() {
        return this.isSelectedPage;
    }

    public final boolean component11() {
        return this.isDisplayingSlate;
    }

    public final boolean component12() {
        return this.isFragmentResumed;
    }

    public final boolean component13() {
        return this.isDisplayingLinkPreviewTooltip;
    }

    public final boolean component14() {
        return this.isDisplayingReactionAnimation;
    }

    public final boolean component15() {
        return this.isRunningSharedElementAnimation;
    }

    public final boolean component16() {
        return this.isDisplayingFirstTimeNavigation;
    }

    public final boolean component17() {
        return this.isDisplayingInfoDialog;
    }

    public final boolean component18() {
        return this.isUserLongTouching;
    }

    public final boolean component19() {
        return this.isUserScrollingChild;
    }

    public final boolean component2() {
        return this.isUserTouching;
    }

    public final boolean component3() {
        return this.isDisplayingForwardDialog;
    }

    public final boolean component4() {
        return this.isDisplayingDeleteDialog;
    }

    public final boolean component5() {
        return this.isDisplayingContextMenu;
    }

    public final boolean component6() {
        return this.isDisplayingViewsAndRepliesDialog;
    }

    public final boolean component7() {
        return this.isDisplayingDirectReplyDialog;
    }

    public final boolean component8() {
        return this.isDisplayingCaptionOverlay;
    }

    public final boolean component9() {
        return this.isUserScrollingParent;
    }

    public final StoryViewerPlaybackState copy(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12, boolean z13, boolean z14, boolean z15, boolean z16, boolean z17, boolean z18, boolean z19) {
        return new StoryViewerPlaybackState(z, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, z14, z15, z16, z17, z18, z19);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryViewerPlaybackState)) {
            return false;
        }
        StoryViewerPlaybackState storyViewerPlaybackState = (StoryViewerPlaybackState) obj;
        return this.areSegmentsInitialized == storyViewerPlaybackState.areSegmentsInitialized && this.isUserTouching == storyViewerPlaybackState.isUserTouching && this.isDisplayingForwardDialog == storyViewerPlaybackState.isDisplayingForwardDialog && this.isDisplayingDeleteDialog == storyViewerPlaybackState.isDisplayingDeleteDialog && this.isDisplayingContextMenu == storyViewerPlaybackState.isDisplayingContextMenu && this.isDisplayingViewsAndRepliesDialog == storyViewerPlaybackState.isDisplayingViewsAndRepliesDialog && this.isDisplayingDirectReplyDialog == storyViewerPlaybackState.isDisplayingDirectReplyDialog && this.isDisplayingCaptionOverlay == storyViewerPlaybackState.isDisplayingCaptionOverlay && this.isUserScrollingParent == storyViewerPlaybackState.isUserScrollingParent && this.isSelectedPage == storyViewerPlaybackState.isSelectedPage && this.isDisplayingSlate == storyViewerPlaybackState.isDisplayingSlate && this.isFragmentResumed == storyViewerPlaybackState.isFragmentResumed && this.isDisplayingLinkPreviewTooltip == storyViewerPlaybackState.isDisplayingLinkPreviewTooltip && this.isDisplayingReactionAnimation == storyViewerPlaybackState.isDisplayingReactionAnimation && this.isRunningSharedElementAnimation == storyViewerPlaybackState.isRunningSharedElementAnimation && this.isDisplayingFirstTimeNavigation == storyViewerPlaybackState.isDisplayingFirstTimeNavigation && this.isDisplayingInfoDialog == storyViewerPlaybackState.isDisplayingInfoDialog && this.isUserLongTouching == storyViewerPlaybackState.isUserLongTouching && this.isUserScrollingChild == storyViewerPlaybackState.isUserScrollingChild;
    }

    public int hashCode() {
        boolean z = this.areSegmentsInitialized;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        boolean z2 = this.isUserTouching;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.isDisplayingForwardDialog;
        if (z3) {
            z3 = true;
        }
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = (i9 + i10) * 31;
        boolean z4 = this.isDisplayingDeleteDialog;
        if (z4) {
            z4 = true;
        }
        int i14 = z4 ? 1 : 0;
        int i15 = z4 ? 1 : 0;
        int i16 = z4 ? 1 : 0;
        int i17 = (i13 + i14) * 31;
        boolean z5 = this.isDisplayingContextMenu;
        if (z5) {
            z5 = true;
        }
        int i18 = z5 ? 1 : 0;
        int i19 = z5 ? 1 : 0;
        int i20 = z5 ? 1 : 0;
        int i21 = (i17 + i18) * 31;
        boolean z6 = this.isDisplayingViewsAndRepliesDialog;
        if (z6) {
            z6 = true;
        }
        int i22 = z6 ? 1 : 0;
        int i23 = z6 ? 1 : 0;
        int i24 = z6 ? 1 : 0;
        int i25 = (i21 + i22) * 31;
        boolean z7 = this.isDisplayingDirectReplyDialog;
        if (z7) {
            z7 = true;
        }
        int i26 = z7 ? 1 : 0;
        int i27 = z7 ? 1 : 0;
        int i28 = z7 ? 1 : 0;
        int i29 = (i25 + i26) * 31;
        boolean z8 = this.isDisplayingCaptionOverlay;
        if (z8) {
            z8 = true;
        }
        int i30 = z8 ? 1 : 0;
        int i31 = z8 ? 1 : 0;
        int i32 = z8 ? 1 : 0;
        int i33 = (i29 + i30) * 31;
        boolean z9 = this.isUserScrollingParent;
        if (z9) {
            z9 = true;
        }
        int i34 = z9 ? 1 : 0;
        int i35 = z9 ? 1 : 0;
        int i36 = z9 ? 1 : 0;
        int i37 = (i33 + i34) * 31;
        boolean z10 = this.isSelectedPage;
        if (z10) {
            z10 = true;
        }
        int i38 = z10 ? 1 : 0;
        int i39 = z10 ? 1 : 0;
        int i40 = z10 ? 1 : 0;
        int i41 = (i37 + i38) * 31;
        boolean z11 = this.isDisplayingSlate;
        if (z11) {
            z11 = true;
        }
        int i42 = z11 ? 1 : 0;
        int i43 = z11 ? 1 : 0;
        int i44 = z11 ? 1 : 0;
        int i45 = (i41 + i42) * 31;
        boolean z12 = this.isFragmentResumed;
        if (z12) {
            z12 = true;
        }
        int i46 = z12 ? 1 : 0;
        int i47 = z12 ? 1 : 0;
        int i48 = z12 ? 1 : 0;
        int i49 = (i45 + i46) * 31;
        boolean z13 = this.isDisplayingLinkPreviewTooltip;
        if (z13) {
            z13 = true;
        }
        int i50 = z13 ? 1 : 0;
        int i51 = z13 ? 1 : 0;
        int i52 = z13 ? 1 : 0;
        int i53 = (i49 + i50) * 31;
        boolean z14 = this.isDisplayingReactionAnimation;
        if (z14) {
            z14 = true;
        }
        int i54 = z14 ? 1 : 0;
        int i55 = z14 ? 1 : 0;
        int i56 = z14 ? 1 : 0;
        int i57 = (i53 + i54) * 31;
        boolean z15 = this.isRunningSharedElementAnimation;
        if (z15) {
            z15 = true;
        }
        int i58 = z15 ? 1 : 0;
        int i59 = z15 ? 1 : 0;
        int i60 = z15 ? 1 : 0;
        int i61 = (i57 + i58) * 31;
        boolean z16 = this.isDisplayingFirstTimeNavigation;
        if (z16) {
            z16 = true;
        }
        int i62 = z16 ? 1 : 0;
        int i63 = z16 ? 1 : 0;
        int i64 = z16 ? 1 : 0;
        int i65 = (i61 + i62) * 31;
        boolean z17 = this.isDisplayingInfoDialog;
        if (z17) {
            z17 = true;
        }
        int i66 = z17 ? 1 : 0;
        int i67 = z17 ? 1 : 0;
        int i68 = z17 ? 1 : 0;
        int i69 = (i65 + i66) * 31;
        boolean z18 = this.isUserLongTouching;
        if (z18) {
            z18 = true;
        }
        int i70 = z18 ? 1 : 0;
        int i71 = z18 ? 1 : 0;
        int i72 = z18 ? 1 : 0;
        int i73 = (i69 + i70) * 31;
        boolean z19 = this.isUserScrollingChild;
        if (!z19) {
            i = z19 ? 1 : 0;
        }
        return i73 + i;
    }

    public String toString() {
        return "StoryViewerPlaybackState(areSegmentsInitialized=" + this.areSegmentsInitialized + ", isUserTouching=" + this.isUserTouching + ", isDisplayingForwardDialog=" + this.isDisplayingForwardDialog + ", isDisplayingDeleteDialog=" + this.isDisplayingDeleteDialog + ", isDisplayingContextMenu=" + this.isDisplayingContextMenu + ", isDisplayingViewsAndRepliesDialog=" + this.isDisplayingViewsAndRepliesDialog + ", isDisplayingDirectReplyDialog=" + this.isDisplayingDirectReplyDialog + ", isDisplayingCaptionOverlay=" + this.isDisplayingCaptionOverlay + ", isUserScrollingParent=" + this.isUserScrollingParent + ", isSelectedPage=" + this.isSelectedPage + ", isDisplayingSlate=" + this.isDisplayingSlate + ", isFragmentResumed=" + this.isFragmentResumed + ", isDisplayingLinkPreviewTooltip=" + this.isDisplayingLinkPreviewTooltip + ", isDisplayingReactionAnimation=" + this.isDisplayingReactionAnimation + ", isRunningSharedElementAnimation=" + this.isRunningSharedElementAnimation + ", isDisplayingFirstTimeNavigation=" + this.isDisplayingFirstTimeNavigation + ", isDisplayingInfoDialog=" + this.isDisplayingInfoDialog + ", isUserLongTouching=" + this.isUserLongTouching + ", isUserScrollingChild=" + this.isUserScrollingChild + ')';
    }

    public StoryViewerPlaybackState(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12, boolean z13, boolean z14, boolean z15, boolean z16, boolean z17, boolean z18, boolean z19) {
        this.areSegmentsInitialized = z;
        this.isUserTouching = z2;
        this.isDisplayingForwardDialog = z3;
        this.isDisplayingDeleteDialog = z4;
        this.isDisplayingContextMenu = z5;
        this.isDisplayingViewsAndRepliesDialog = z6;
        this.isDisplayingDirectReplyDialog = z7;
        this.isDisplayingCaptionOverlay = z8;
        this.isUserScrollingParent = z9;
        this.isSelectedPage = z10;
        this.isDisplayingSlate = z11;
        this.isFragmentResumed = z12;
        this.isDisplayingLinkPreviewTooltip = z13;
        this.isDisplayingReactionAnimation = z14;
        this.isRunningSharedElementAnimation = z15;
        this.isDisplayingFirstTimeNavigation = z16;
        this.isDisplayingInfoDialog = z17;
        this.isUserLongTouching = z18;
        this.isUserScrollingChild = z19;
        this.hideChromeImmediate = z15;
        this.hideChrome = z15 || z18 || z19;
        this.isPaused = !z || z2 || z8 || z3 || z4 || z5 || z6 || z7 || z8 || z9 || !z10 || z11 || !z12 || z13 || z14 || z15 || z16 || z17;
    }

    public /* synthetic */ StoryViewerPlaybackState(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12, boolean z13, boolean z14, boolean z15, boolean z16, boolean z17, boolean z18, boolean z19, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? false : z3, (i & 8) != 0 ? false : z4, (i & 16) != 0 ? false : z5, (i & 32) != 0 ? false : z6, (i & 64) != 0 ? false : z7, (i & 128) != 0 ? false : z8, (i & 256) != 0 ? false : z9, (i & 512) != 0 ? false : z10, (i & 1024) != 0 ? false : z11, (i & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? false : z12, (i & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? false : z13, (i & 8192) != 0 ? false : z14, (i & 16384) != 0 ? false : z15, (i & 32768) != 0 ? false : z16, (i & 65536) != 0 ? false : z17, (i & 131072) != 0 ? false : z18, (i & 262144) != 0 ? false : z19);
    }

    public final boolean getAreSegmentsInitialized() {
        return this.areSegmentsInitialized;
    }

    public final boolean isUserTouching() {
        return this.isUserTouching;
    }

    public final boolean isDisplayingForwardDialog() {
        return this.isDisplayingForwardDialog;
    }

    public final boolean isDisplayingDeleteDialog() {
        return this.isDisplayingDeleteDialog;
    }

    public final boolean isDisplayingContextMenu() {
        return this.isDisplayingContextMenu;
    }

    public final boolean isDisplayingViewsAndRepliesDialog() {
        return this.isDisplayingViewsAndRepliesDialog;
    }

    public final boolean isDisplayingDirectReplyDialog() {
        return this.isDisplayingDirectReplyDialog;
    }

    public final boolean isDisplayingCaptionOverlay() {
        return this.isDisplayingCaptionOverlay;
    }

    public final boolean isUserScrollingParent() {
        return this.isUserScrollingParent;
    }

    public final boolean isSelectedPage() {
        return this.isSelectedPage;
    }

    public final boolean isDisplayingSlate() {
        return this.isDisplayingSlate;
    }

    public final boolean isFragmentResumed() {
        return this.isFragmentResumed;
    }

    public final boolean isDisplayingLinkPreviewTooltip() {
        return this.isDisplayingLinkPreviewTooltip;
    }

    public final boolean isDisplayingReactionAnimation() {
        return this.isDisplayingReactionAnimation;
    }

    public final boolean isRunningSharedElementAnimation() {
        return this.isRunningSharedElementAnimation;
    }

    public final boolean isDisplayingFirstTimeNavigation() {
        return this.isDisplayingFirstTimeNavigation;
    }

    public final boolean isDisplayingInfoDialog() {
        return this.isDisplayingInfoDialog;
    }

    public final boolean isUserLongTouching() {
        return this.isUserLongTouching;
    }

    public final boolean isUserScrollingChild() {
        return this.isUserScrollingChild;
    }

    public final boolean getHideChromeImmediate() {
        return this.hideChromeImmediate;
    }

    public final boolean getHideChrome() {
        return this.hideChrome;
    }

    public final boolean isPaused() {
        return this.isPaused;
    }
}
