package org.thoughtcrime.securesms.stories.viewer.views;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* compiled from: StoryViewsRepository.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00042\u0006\u0010\u0007\u001a\u00020\bJ\u0006\u0010\t\u001a\u00020\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewsRepository;", "", "()V", "getViews", "Lio/reactivex/rxjava3/core/Observable;", "", "Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewItemData;", "storyId", "", "isReadReceiptsEnabled", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewsRepository {
    public final boolean isReadReceiptsEnabled() {
        return TextSecurePreferences.isReadReceiptsEnabled(ApplicationDependencies.getApplication());
    }

    public final Observable<List<StoryViewItemData>> getViews(long j) {
        Observable<List<StoryViewItemData>> subscribeOn = Observable.create(new ObservableOnSubscribe(j) { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                StoryViewsRepository.$r8$lambda$4pifgO23Y9_P7D3VHzx0pAKeKQQ(this.f$0, observableEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create<List<StoryViewIte…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getViews$lambda-4$refresh */
    private static final void m3210getViews$lambda4$refresh(ObservableEmitter<List<StoryViewItemData>> observableEmitter, long j) {
        List<GroupReceiptDatabase.GroupReceiptInfo> groupReceiptInfo = SignalDatabase.Companion.groupReceipts().getGroupReceiptInfo(j);
        Intrinsics.checkNotNullExpressionValue(groupReceiptInfo, "SignalDatabase.groupRece…GroupReceiptInfo(storyId)");
        ArrayList<GroupReceiptDatabase.GroupReceiptInfo> arrayList = new ArrayList();
        for (Object obj : groupReceiptInfo) {
            if (((GroupReceiptDatabase.GroupReceiptInfo) obj).getStatus() == 3) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        for (GroupReceiptDatabase.GroupReceiptInfo groupReceiptInfo2 : arrayList) {
            Recipient resolved = Recipient.resolved(groupReceiptInfo2.getRecipientId());
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(it.recipientId)");
            arrayList2.add(new StoryViewItemData(resolved, groupReceiptInfo2.getTimestamp()));
        }
        observableEmitter.onNext(arrayList2);
    }

    /* renamed from: getViews$lambda-4 */
    public static final void m3207getViews$lambda4(long j, ObservableEmitter observableEmitter) {
        StoryViewsRepository$$ExternalSyntheticLambda0 storyViewsRepository$$ExternalSyntheticLambda0 = new DatabaseObserver.MessageObserver(j) { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.MessageObserver
            public final void onMessageChanged(MessageId messageId) {
                StoryViewsRepository.$r8$lambda$C61_HcH1j1LEubhJServE1oM8tI(ObservableEmitter.this, this.f$1, messageId);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerMessageUpdateObserver(storyViewsRepository$$ExternalSyntheticLambda0);
        observableEmitter.setCancellable(new Cancellable() { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewsRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                StoryViewsRepository.m3206$r8$lambda$hf3CqtrChtvoBUQfv72pTmeOE4(DatabaseObserver.MessageObserver.this);
            }
        });
        m3210getViews$lambda4$refresh(observableEmitter, j);
    }

    /* renamed from: getViews$lambda-4$lambda-2 */
    public static final void m3208getViews$lambda4$lambda2(ObservableEmitter observableEmitter, long j, MessageId messageId) {
        Intrinsics.checkNotNullParameter(messageId, "it");
        m3210getViews$lambda4$refresh(observableEmitter, j);
    }

    /* renamed from: getViews$lambda-4$lambda-3 */
    public static final void m3209getViews$lambda4$lambda3(DatabaseObserver.MessageObserver messageObserver) {
        Intrinsics.checkNotNullParameter(messageObserver, "$observer");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(messageObserver);
    }
}
