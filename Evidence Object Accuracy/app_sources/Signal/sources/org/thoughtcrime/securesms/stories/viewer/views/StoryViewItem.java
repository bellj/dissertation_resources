package org.thoughtcrime.securesms.stories.viewer.views;

import android.view.View;
import android.widget.TextView;
import j$.util.function.Function;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.stories.viewer.views.StoryViewItem;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: StoryViewItem.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewItem;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryViewItem {
    public static final StoryViewItem INSTANCE = new StoryViewItem();

    private StoryViewItem() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.views.StoryViewItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new StoryViewItem.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_story_view_item));
    }

    /* compiled from: StoryViewItem.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewItem$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "storyViewItemData", "Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewItemData;", "(Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewItemData;)V", "getStoryViewItemData", "()Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewItemData;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final StoryViewItemData storyViewItemData;

        public final StoryViewItemData getStoryViewItemData() {
            return this.storyViewItemData;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(StoryViewItemData storyViewItemData) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(storyViewItemData, "storyViewItemData");
            this.storyViewItemData = storyViewItemData;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.storyViewItemData.getRecipient(), model.storyViewItemData.getRecipient());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.storyViewItemData, model.storyViewItemData) && this.storyViewItemData.getRecipient().hasSameContent(model.storyViewItemData.getRecipient()) && super.areContentsTheSame(model);
        }
    }

    /* compiled from: StoryViewItem.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0002H\u0016J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/viewer/views/StoryViewItem$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "avatarView", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "nameView", "Landroid/widget/TextView;", "viewedAtView", "bind", "", "model", "formatDate", "", "dateInMilliseconds", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final AvatarImageView avatarView;
        private final TextView nameView;
        private final TextView viewedAtView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.avatar)");
            this.avatarView = (AvatarImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.name);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.name)");
            this.nameView = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.viewed_at);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.viewed_at)");
            this.viewedAtView = (TextView) findViewById3;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.avatarView.setAvatar(model.getStoryViewItemData().getRecipient());
            this.nameView.setText(model.getStoryViewItemData().getRecipient().getDisplayName(this.context));
            this.viewedAtView.setText(formatDate(model.getStoryViewItemData().getTimeViewedInMillis()));
        }

        private final String formatDate(long j) {
            String formatDateWithDayOfWeek = DateUtils.formatDateWithDayOfWeek(Locale.getDefault(), j);
            Intrinsics.checkNotNullExpressionValue(formatDateWithDayOfWeek, "formatDateWithDayOfWeek(…lt(), dateInMilliseconds)");
            return formatDateWithDayOfWeek;
        }
    }
}
