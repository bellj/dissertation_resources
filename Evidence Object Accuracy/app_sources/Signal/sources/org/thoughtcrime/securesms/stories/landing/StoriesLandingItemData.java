package org.thoughtcrime.securesms.stories.landing;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: StoriesLandingItemData.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0013\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001Bg\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\b\b\u0002\u0010\r\u001a\u00020\f\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u000f¢\u0006\u0002\u0010\u0012J\u0011\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0000H\u0002J\t\u0010%\u001a\u00020\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u000fHÆ\u0003J\t\u0010'\u001a\u00020\u000fHÆ\u0003J\t\u0010(\u001a\u00020\u0005HÆ\u0003J\t\u0010)\u001a\u00020\u0005HÆ\u0003J\t\u0010*\u001a\u00020\u0005HÆ\u0003J\t\u0010+\u001a\u00020\tHÆ\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\tHÆ\u0003J\t\u0010-\u001a\u00020\fHÆ\u0003J\t\u0010.\u001a\u00020\fHÆ\u0003J\t\u0010/\u001a\u00020\u000fHÆ\u0003Jy\u00100\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\f2\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u000f2\b\b\u0002\u0010\u0011\u001a\u00020\u000fHÆ\u0001J\u0013\u00101\u001a\u00020\u00052\b\u0010$\u001a\u0004\u0018\u000102HÖ\u0003J\t\u00103\u001a\u00020#HÖ\u0001J\t\u00104\u001a\u000205HÖ\u0001R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0011\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0017R\u0011\u0010\r\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0017R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0013\u0010\n\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001cR\u0011\u0010\u0010\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0014R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001aR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b \u0010!¨\u00066"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItemData;", "", "storyViewState", "Lorg/thoughtcrime/securesms/database/model/StoryViewState;", "hasReplies", "", "hasRepliesFromSelf", "isHidden", "primaryStory", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "secondaryStory", "storyRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "individualRecipient", "dateInMilliseconds", "", "sendingCount", "failureCount", "(Lorg/thoughtcrime/securesms/database/model/StoryViewState;ZZZLorg/thoughtcrime/securesms/conversation/ConversationMessage;Lorg/thoughtcrime/securesms/conversation/ConversationMessage;Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/recipients/Recipient;JJJ)V", "getDateInMilliseconds", "()J", "getFailureCount", "getHasReplies", "()Z", "getHasRepliesFromSelf", "getIndividualRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getPrimaryStory", "()Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "getSecondaryStory", "getSendingCount", "getStoryRecipient", "getStoryViewState", "()Lorg/thoughtcrime/securesms/database/model/StoryViewState;", "compareTo", "", "other", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoriesLandingItemData implements Comparable<StoriesLandingItemData> {
    private final long dateInMilliseconds;
    private final long failureCount;
    private final boolean hasReplies;
    private final boolean hasRepliesFromSelf;
    private final Recipient individualRecipient;
    private final boolean isHidden;
    private final ConversationMessage primaryStory;
    private final ConversationMessage secondaryStory;
    private final long sendingCount;
    private final Recipient storyRecipient;
    private final StoryViewState storyViewState;

    public final StoryViewState component1() {
        return this.storyViewState;
    }

    public final long component10() {
        return this.sendingCount;
    }

    public final long component11() {
        return this.failureCount;
    }

    public final boolean component2() {
        return this.hasReplies;
    }

    public final boolean component3() {
        return this.hasRepliesFromSelf;
    }

    public final boolean component4() {
        return this.isHidden;
    }

    public final ConversationMessage component5() {
        return this.primaryStory;
    }

    public final ConversationMessage component6() {
        return this.secondaryStory;
    }

    public final Recipient component7() {
        return this.storyRecipient;
    }

    public final Recipient component8() {
        return this.individualRecipient;
    }

    public final long component9() {
        return this.dateInMilliseconds;
    }

    public final StoriesLandingItemData copy(StoryViewState storyViewState, boolean z, boolean z2, boolean z3, ConversationMessage conversationMessage, ConversationMessage conversationMessage2, Recipient recipient, Recipient recipient2, long j, long j2, long j3) {
        Intrinsics.checkNotNullParameter(storyViewState, "storyViewState");
        Intrinsics.checkNotNullParameter(conversationMessage, "primaryStory");
        Intrinsics.checkNotNullParameter(recipient, "storyRecipient");
        Intrinsics.checkNotNullParameter(recipient2, "individualRecipient");
        return new StoriesLandingItemData(storyViewState, z, z2, z3, conversationMessage, conversationMessage2, recipient, recipient2, j, j2, j3);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoriesLandingItemData)) {
            return false;
        }
        StoriesLandingItemData storiesLandingItemData = (StoriesLandingItemData) obj;
        return this.storyViewState == storiesLandingItemData.storyViewState && this.hasReplies == storiesLandingItemData.hasReplies && this.hasRepliesFromSelf == storiesLandingItemData.hasRepliesFromSelf && this.isHidden == storiesLandingItemData.isHidden && Intrinsics.areEqual(this.primaryStory, storiesLandingItemData.primaryStory) && Intrinsics.areEqual(this.secondaryStory, storiesLandingItemData.secondaryStory) && Intrinsics.areEqual(this.storyRecipient, storiesLandingItemData.storyRecipient) && Intrinsics.areEqual(this.individualRecipient, storiesLandingItemData.individualRecipient) && this.dateInMilliseconds == storiesLandingItemData.dateInMilliseconds && this.sendingCount == storiesLandingItemData.sendingCount && this.failureCount == storiesLandingItemData.failureCount;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int hashCode = this.storyViewState.hashCode() * 31;
        boolean z = this.hasReplies;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode + i2) * 31;
        boolean z2 = this.hasRepliesFromSelf;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.isHidden;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        int hashCode2 = (((i9 + i) * 31) + this.primaryStory.hashCode()) * 31;
        ConversationMessage conversationMessage = this.secondaryStory;
        return ((((((((((hashCode2 + (conversationMessage == null ? 0 : conversationMessage.hashCode())) * 31) + this.storyRecipient.hashCode()) * 31) + this.individualRecipient.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.dateInMilliseconds)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.sendingCount)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.failureCount);
    }

    @Override // java.lang.Object
    public String toString() {
        return "StoriesLandingItemData(storyViewState=" + this.storyViewState + ", hasReplies=" + this.hasReplies + ", hasRepliesFromSelf=" + this.hasRepliesFromSelf + ", isHidden=" + this.isHidden + ", primaryStory=" + this.primaryStory + ", secondaryStory=" + this.secondaryStory + ", storyRecipient=" + this.storyRecipient + ", individualRecipient=" + this.individualRecipient + ", dateInMilliseconds=" + this.dateInMilliseconds + ", sendingCount=" + this.sendingCount + ", failureCount=" + this.failureCount + ')';
    }

    public StoriesLandingItemData(StoryViewState storyViewState, boolean z, boolean z2, boolean z3, ConversationMessage conversationMessage, ConversationMessage conversationMessage2, Recipient recipient, Recipient recipient2, long j, long j2, long j3) {
        Intrinsics.checkNotNullParameter(storyViewState, "storyViewState");
        Intrinsics.checkNotNullParameter(conversationMessage, "primaryStory");
        Intrinsics.checkNotNullParameter(recipient, "storyRecipient");
        Intrinsics.checkNotNullParameter(recipient2, "individualRecipient");
        this.storyViewState = storyViewState;
        this.hasReplies = z;
        this.hasRepliesFromSelf = z2;
        this.isHidden = z3;
        this.primaryStory = conversationMessage;
        this.secondaryStory = conversationMessage2;
        this.storyRecipient = recipient;
        this.individualRecipient = recipient2;
        this.dateInMilliseconds = j;
        this.sendingCount = j2;
        this.failureCount = j3;
    }

    public final StoryViewState getStoryViewState() {
        return this.storyViewState;
    }

    public final boolean getHasReplies() {
        return this.hasReplies;
    }

    public final boolean getHasRepliesFromSelf() {
        return this.hasRepliesFromSelf;
    }

    public final boolean isHidden() {
        return this.isHidden;
    }

    public final ConversationMessage getPrimaryStory() {
        return this.primaryStory;
    }

    public final ConversationMessage getSecondaryStory() {
        return this.secondaryStory;
    }

    public final Recipient getStoryRecipient() {
        return this.storyRecipient;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ StoriesLandingItemData(org.thoughtcrime.securesms.database.model.StoryViewState r19, boolean r20, boolean r21, boolean r22, org.thoughtcrime.securesms.conversation.ConversationMessage r23, org.thoughtcrime.securesms.conversation.ConversationMessage r24, org.thoughtcrime.securesms.recipients.Recipient r25, org.thoughtcrime.securesms.recipients.Recipient r26, long r27, long r29, long r31, int r33, kotlin.jvm.internal.DefaultConstructorMarker r34) {
        /*
            r18 = this;
            r0 = r33
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x0015
            org.thoughtcrime.securesms.database.model.MessageRecord r1 = r23.getMessageRecord()
            org.thoughtcrime.securesms.recipients.Recipient r1 = r1.getIndividualRecipient()
            java.lang.String r2 = "primaryStory.messageRecord.individualRecipient"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
            r11 = r1
            goto L_0x0017
        L_0x0015:
            r11 = r26
        L_0x0017:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x0025
            org.thoughtcrime.securesms.database.model.MessageRecord r1 = r23.getMessageRecord()
            long r1 = r1.getDateSent()
            r12 = r1
            goto L_0x0027
        L_0x0025:
            r12 = r27
        L_0x0027:
            r1 = r0 & 512(0x200, float:7.175E-43)
            r2 = 0
            if (r1 == 0) goto L_0x002f
            r14 = r2
            goto L_0x0031
        L_0x002f:
            r14 = r29
        L_0x0031:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x0038
            r16 = r2
            goto L_0x003a
        L_0x0038:
            r16 = r31
        L_0x003a:
            r3 = r18
            r4 = r19
            r5 = r20
            r6 = r21
            r7 = r22
            r8 = r23
            r9 = r24
            r10 = r25
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r14, r16)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.landing.StoriesLandingItemData.<init>(org.thoughtcrime.securesms.database.model.StoryViewState, boolean, boolean, boolean, org.thoughtcrime.securesms.conversation.ConversationMessage, org.thoughtcrime.securesms.conversation.ConversationMessage, org.thoughtcrime.securesms.recipients.Recipient, org.thoughtcrime.securesms.recipients.Recipient, long, long, long, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final Recipient getIndividualRecipient() {
        return this.individualRecipient;
    }

    public final long getDateInMilliseconds() {
        return this.dateInMilliseconds;
    }

    public final long getSendingCount() {
        return this.sendingCount;
    }

    public final long getFailureCount() {
        return this.failureCount;
    }

    public int compareTo(StoriesLandingItemData storiesLandingItemData) {
        Intrinsics.checkNotNullParameter(storiesLandingItemData, "other");
        if (!this.storyRecipient.isMyStory() || storiesLandingItemData.storyRecipient.isMyStory()) {
            if (!this.storyRecipient.isMyStory() && storiesLandingItemData.storyRecipient.isMyStory()) {
                return 1;
            }
            if (!this.storyRecipient.isReleaseNotes() || storiesLandingItemData.storyRecipient.isReleaseNotes()) {
                if (!this.storyRecipient.isReleaseNotes() && storiesLandingItemData.storyRecipient.isReleaseNotes()) {
                    return 1;
                }
                StoryViewState storyViewState = this.storyViewState;
                StoryViewState storyViewState2 = StoryViewState.UNVIEWED;
                if (storyViewState != storyViewState2 || storiesLandingItemData.storyViewState == storyViewState2) {
                    if (storyViewState == storyViewState2 || storiesLandingItemData.storyViewState != storyViewState2) {
                        return -Intrinsics.compare(this.dateInMilliseconds, storiesLandingItemData.dateInMilliseconds);
                    }
                    return 1;
                }
            }
        }
        return -1;
    }
}
