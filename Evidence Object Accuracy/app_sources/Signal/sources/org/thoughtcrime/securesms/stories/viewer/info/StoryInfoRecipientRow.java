package org.thoughtcrime.securesms.stories.viewer.info;

import android.view.View;
import android.widget.TextView;
import j$.util.function.Function;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRecipientRow;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: StoryInfoRecipientRow.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRecipientRow;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryInfoRecipientRow {
    public static final StoryInfoRecipientRow INSTANCE = new StoryInfoRecipientRow();

    private StoryInfoRecipientRow() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRecipientRow$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new StoryInfoRecipientRow.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.story_info_recipient_row));
    }

    /* compiled from: StoryInfoRecipientRow.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0000H\u0016J\u0010\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0000H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRecipientRow$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "date", "", "status", "", "(Lorg/thoughtcrime/securesms/recipients/Recipient;JI)V", "getDate", "()J", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getStatus", "()I", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model implements MappingModel<Model> {
        private final long date;
        private final Recipient recipient;
        private final int status;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        public Model(Recipient recipient, long j, int i) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.recipient = recipient;
            this.date = j;
            this.status = i;
        }

        public final Recipient getRecipient() {
            return this.recipient;
        }

        public final long getDate() {
            return this.date;
        }

        public final int getStatus() {
            return this.status;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.recipient.getId(), model.recipient.getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return this.recipient.hasSameContent(model.recipient) && this.date == model.date;
        }
    }

    /* compiled from: StoryInfoRecipientRow.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRecipientRow$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRecipientRow$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "avatarView", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "nameView", "Landroid/widget/TextView;", "timestampView", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final AvatarImageView avatarView;
        private final TextView nameView;
        private final TextView timestampView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.story_info_avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.story_info_avatar)");
            this.avatarView = (AvatarImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.story_info_display_name);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.….story_info_display_name)");
            this.nameView = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.story_info_timestamp);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.story_info_timestamp)");
            this.timestampView = (TextView) findViewById3;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.avatarView.setRecipient(model.getRecipient());
            this.nameView.setText(model.getRecipient().getDisplayName(this.context));
            this.timestampView.setText(DateUtils.getTimeString(this.context, Locale.getDefault(), model.getDate()));
        }
    }
}
