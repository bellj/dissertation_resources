package org.thoughtcrime.securesms.stories.viewer.text;

import android.graphics.Typeface;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;

/* compiled from: StoryTextPostState.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u001fB3\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\nJ\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0007HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\tHÆ\u0003J7\u0010\u0017\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\tHÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostState;", "", "storyTextPost", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/StoryTextPost;", "linkPreview", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;", "loadState", "Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostState$LoadState;", "typeface", "Landroid/graphics/Typeface;", "(Lorg/thoughtcrime/securesms/database/model/databaseprotos/StoryTextPost;Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostState$LoadState;Landroid/graphics/Typeface;)V", "getLinkPreview", "()Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;", "getLoadState", "()Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostState$LoadState;", "getStoryTextPost", "()Lorg/thoughtcrime/securesms/database/model/databaseprotos/StoryTextPost;", "getTypeface", "()Landroid/graphics/Typeface;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "", "LoadState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryTextPostState {
    private final LinkPreview linkPreview;
    private final LoadState loadState;
    private final StoryTextPost storyTextPost;
    private final Typeface typeface;

    /* compiled from: StoryTextPostState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/text/StoryTextPostState$LoadState;", "", "(Ljava/lang/String;I)V", "INIT", "LOADED", "FAILED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum LoadState {
        INIT,
        LOADED,
        FAILED
    }

    public StoryTextPostState() {
        this(null, null, null, null, 15, null);
    }

    public static /* synthetic */ StoryTextPostState copy$default(StoryTextPostState storyTextPostState, StoryTextPost storyTextPost, LinkPreview linkPreview, LoadState loadState, Typeface typeface, int i, Object obj) {
        if ((i & 1) != 0) {
            storyTextPost = storyTextPostState.storyTextPost;
        }
        if ((i & 2) != 0) {
            linkPreview = storyTextPostState.linkPreview;
        }
        if ((i & 4) != 0) {
            loadState = storyTextPostState.loadState;
        }
        if ((i & 8) != 0) {
            typeface = storyTextPostState.typeface;
        }
        return storyTextPostState.copy(storyTextPost, linkPreview, loadState, typeface);
    }

    public final StoryTextPost component1() {
        return this.storyTextPost;
    }

    public final LinkPreview component2() {
        return this.linkPreview;
    }

    public final LoadState component3() {
        return this.loadState;
    }

    public final Typeface component4() {
        return this.typeface;
    }

    public final StoryTextPostState copy(StoryTextPost storyTextPost, LinkPreview linkPreview, LoadState loadState, Typeface typeface) {
        Intrinsics.checkNotNullParameter(loadState, "loadState");
        return new StoryTextPostState(storyTextPost, linkPreview, loadState, typeface);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryTextPostState)) {
            return false;
        }
        StoryTextPostState storyTextPostState = (StoryTextPostState) obj;
        return Intrinsics.areEqual(this.storyTextPost, storyTextPostState.storyTextPost) && Intrinsics.areEqual(this.linkPreview, storyTextPostState.linkPreview) && this.loadState == storyTextPostState.loadState && Intrinsics.areEqual(this.typeface, storyTextPostState.typeface);
    }

    public int hashCode() {
        StoryTextPost storyTextPost = this.storyTextPost;
        int i = 0;
        int hashCode = (storyTextPost == null ? 0 : storyTextPost.hashCode()) * 31;
        LinkPreview linkPreview = this.linkPreview;
        int hashCode2 = (((hashCode + (linkPreview == null ? 0 : linkPreview.hashCode())) * 31) + this.loadState.hashCode()) * 31;
        Typeface typeface = this.typeface;
        if (typeface != null) {
            i = typeface.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "StoryTextPostState(storyTextPost=" + this.storyTextPost + ", linkPreview=" + this.linkPreview + ", loadState=" + this.loadState + ", typeface=" + this.typeface + ')';
    }

    public StoryTextPostState(StoryTextPost storyTextPost, LinkPreview linkPreview, LoadState loadState, Typeface typeface) {
        Intrinsics.checkNotNullParameter(loadState, "loadState");
        this.storyTextPost = storyTextPost;
        this.linkPreview = linkPreview;
        this.loadState = loadState;
        this.typeface = typeface;
    }

    public final StoryTextPost getStoryTextPost() {
        return this.storyTextPost;
    }

    public final LinkPreview getLinkPreview() {
        return this.linkPreview;
    }

    public /* synthetic */ StoryTextPostState(StoryTextPost storyTextPost, LinkPreview linkPreview, LoadState loadState, Typeface typeface, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : storyTextPost, (i & 2) != 0 ? null : linkPreview, (i & 4) != 0 ? LoadState.INIT : loadState, (i & 8) != 0 ? null : typeface);
    }

    public final LoadState getLoadState() {
        return this.loadState;
    }

    public final Typeface getTypeface() {
        return this.typeface;
    }
}
