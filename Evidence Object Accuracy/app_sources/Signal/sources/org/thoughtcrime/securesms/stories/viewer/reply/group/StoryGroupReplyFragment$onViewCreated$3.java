package org.thoughtcrime.securesms.stories.viewer.reply.group;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.Ref$BooleanRef;
import org.thoughtcrime.securesms.conversation.MarkReadHelper;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyState;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.PagingMappingAdapter;

/* compiled from: StoryGroupReplyFragment.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "state", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyState;", "kotlin.jvm.PlatformType", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
final class StoryGroupReplyFragment$onViewCreated$3 extends Lambda implements Function1<StoryGroupReplyState, Unit> {
    final /* synthetic */ View $emptyNotice;
    final /* synthetic */ Ref$BooleanRef $firstSubmit;
    final /* synthetic */ StoryGroupReplyFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryGroupReplyFragment$onViewCreated$3(StoryGroupReplyFragment storyGroupReplyFragment, View view, Ref$BooleanRef ref$BooleanRef) {
        super(1);
        this.this$0 = storyGroupReplyFragment;
        this.$emptyNotice = view;
        this.$firstSubmit = ref$BooleanRef;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoryGroupReplyState storyGroupReplyState) {
        invoke(storyGroupReplyState);
        return Unit.INSTANCE;
    }

    public final void invoke(StoryGroupReplyState storyGroupReplyState) {
        MarkReadHelper markReadHelper;
        if (this.this$0.markReadHelper == null && storyGroupReplyState.getThreadId() > 0) {
            if (this.this$0.isResumed()) {
                ApplicationDependencies.getMessageNotifier().setVisibleThread(new ConversationId(storyGroupReplyState.getThreadId(), Long.valueOf(this.this$0.getStoryId())));
            }
            this.this$0.markReadHelper = new MarkReadHelper(new ConversationId(storyGroupReplyState.getThreadId(), Long.valueOf(this.this$0.getStoryId())), this.this$0.requireContext(), this.this$0.getViewLifecycleOwner());
            if ((this.this$0.isFromNotification()) && (markReadHelper = this.this$0.markReadHelper) != null) {
                markReadHelper.onViewsRevealed(System.currentTimeMillis());
            }
        }
        ViewExtensionsKt.setVisible(this.$emptyNotice, storyGroupReplyState.getNoReplies() && storyGroupReplyState.getLoadState() == StoryGroupReplyState.LoadState.READY);
        this.this$0.colorizer.onNameColorsChanged(storyGroupReplyState.getNameColors());
        PagingMappingAdapter pagingMappingAdapter = this.this$0.adapter;
        if (pagingMappingAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("adapter");
            pagingMappingAdapter = null;
        }
        pagingMappingAdapter.submitList(this.this$0.getConfiguration(storyGroupReplyState.getReplies()).toMappingModelList(), new Runnable(this.this$0) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onViewCreated$3$$ExternalSyntheticLambda1
            public final /* synthetic */ StoryGroupReplyFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StoryGroupReplyFragment$onViewCreated$3.m3142invoke$lambda1(Ref$BooleanRef.this, this.f$1);
            }
        });
    }

    /* renamed from: invoke$lambda-1 */
    public static final void m3142invoke$lambda1(Ref$BooleanRef ref$BooleanRef, StoryGroupReplyFragment storyGroupReplyFragment) {
        Intrinsics.checkNotNullParameter(ref$BooleanRef, "$firstSubmit");
        Intrinsics.checkNotNullParameter(storyGroupReplyFragment, "this$0");
        if (ref$BooleanRef.element && storyGroupReplyFragment.getGroupReplyStartPosition() >= 0) {
            PagingMappingAdapter pagingMappingAdapter = storyGroupReplyFragment.adapter;
            RecyclerView recyclerView = null;
            if (pagingMappingAdapter == null) {
                Intrinsics.throwUninitializedPropertyAccessException("adapter");
                pagingMappingAdapter = null;
            }
            if (pagingMappingAdapter.hasItem(storyGroupReplyFragment.getGroupReplyStartPosition())) {
                ref$BooleanRef.element = false;
                RecyclerView recyclerView2 = storyGroupReplyFragment.recyclerView;
                if (recyclerView2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
                } else {
                    recyclerView = recyclerView2;
                }
                recyclerView.post(new Runnable() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment$onViewCreated$3$$ExternalSyntheticLambda0
                    @Override // java.lang.Runnable
                    public final void run() {
                        StoryGroupReplyFragment$onViewCreated$3.m3143invoke$lambda1$lambda0(StoryGroupReplyFragment.this);
                    }
                });
            }
        }
    }

    /* renamed from: invoke$lambda-1$lambda-0 */
    public static final void m3143invoke$lambda1$lambda0(StoryGroupReplyFragment storyGroupReplyFragment) {
        Intrinsics.checkNotNullParameter(storyGroupReplyFragment, "this$0");
        RecyclerView recyclerView = storyGroupReplyFragment.recyclerView;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
            recyclerView = null;
        }
        recyclerView.scrollToPosition(storyGroupReplyFragment.getGroupReplyStartPosition());
    }
}
