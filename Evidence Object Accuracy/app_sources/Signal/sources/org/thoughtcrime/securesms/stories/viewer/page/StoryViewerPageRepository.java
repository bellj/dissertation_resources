package org.thoughtcrime.securesms.stories.viewer.page;

import android.content.Context;
import android.net.Uri;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.BreakIteratorCompat;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceViewedUpdateJob;
import org.thoughtcrime.securesms.jobs.SendViewedReceiptJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.viewer.page.StoryPost;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* compiled from: StoryViewerPageRepository.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0016\u0018\u0000 #2\u00020\u0001:\u0001#B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0002J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u001e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\r0\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0010\u001a\u00020\u0016H\u0002J*\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\u00180\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u001a\u001a\u00020\u0007J,\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00180\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u001a\u001a\u00020\u0007H\u0002J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\b\u001a\u00020\tH\u0002J\u000e\u0010\u001e\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0015J\u0006\u0010\u001f\u001a\u00020\u0007J\u000e\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\rR\u0016\u0010\u0002\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "kotlin.jvm.PlatformType", "canParseToTextStory", "", "body", "", "forceDownload", "Lio/reactivex/rxjava3/core/Completable;", "post", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost;", "getContent", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryPost$Content;", "record", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "getStoryPostFromRecord", "Lio/reactivex/rxjava3/core/Observable;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "getStoryPostsFor", "", "isUnviewedOnly", "isOutgoingOnly", "getStoryRecords", "getTextStoryLength", "", "hideStory", "isReadReceiptsEnabled", "markViewed", "", "storyPost", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public class StoryViewerPageRepository {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(StoryViewerPageRepository.class);
    private final Context context;

    public StoryViewerPageRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context.getApplicationContext();
    }

    /* compiled from: StoryViewerPageRepository.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageRepository$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final boolean isReadReceiptsEnabled() {
        return TextSecurePreferences.isReadReceiptsEnabled(this.context);
    }

    private final Observable<List<MessageRecord>> getStoryRecords(RecipientId recipientId, boolean z, boolean z2) {
        Observable<List<MessageRecord>> create = Observable.create(new ObservableOnSubscribe(z, z2) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda6
            public final /* synthetic */ boolean f$1;
            public final /* synthetic */ boolean f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                StoryViewerPageRepository.m3056$r8$lambda$SO5BJdqUz0mbKTrGCyUG11DiaU(RecipientId.this, this.f$1, this.f$2, observableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create { emitter ->\n    … }\n\n      refresh()\n    }");
        return create;
    }

    /* renamed from: getStoryRecords$lambda-3 */
    public static final void m3067getStoryRecords$lambda3(RecipientId recipientId, boolean z, boolean z2, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        StoryViewerPageRepository$$ExternalSyntheticLambda7 storyViewerPageRepository$$ExternalSyntheticLambda7 = new DatabaseObserver.Observer(z, recipientId, z2, observableEmitter) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda7
            public final /* synthetic */ boolean f$1;
            public final /* synthetic */ RecipientId f$2;
            public final /* synthetic */ boolean f$3;
            public final /* synthetic */ ObservableEmitter f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                StoryViewerPageRepository.$r8$lambda$Z8zpF7AzttoXRnckr2DLMFWxN5U(Recipient.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerStoryObserver(recipientId, storyViewerPageRepository$$ExternalSyntheticLambda7);
        observableEmitter.setCancellable(new Cancellable() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda8
            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                StoryViewerPageRepository.$r8$lambda$rRkJ0y13yxytELriRJ4AlxrfXOc(DatabaseObserver.Observer.this);
            }
        });
        m3070getStoryRecords$lambda3$refresh(resolved, z, recipientId, z2, observableEmitter);
    }

    /* renamed from: getStoryRecords$lambda-3$refresh */
    private static final void m3070getStoryRecords$lambda3$refresh(Recipient recipient, boolean z, RecipientId recipientId, boolean z2, ObservableEmitter<List<MessageRecord>> observableEmitter) {
        MessageDatabase.Reader reader;
        if (recipient.isMyStory()) {
            reader = SignalDatabase.Companion.mms().getAllOutgoingStories(false, 100);
        } else if (z) {
            reader = SignalDatabase.Companion.mms().getUnreadStories(recipientId, 100);
        } else if (z2) {
            reader = SignalDatabase.Companion.mms().getOutgoingStoriesTo(recipientId);
        } else {
            reader = SignalDatabase.Companion.mms().getAllStoriesFor(recipientId, 100);
        }
        Intrinsics.checkNotNullExpressionValue(reader, "if (recipient.isMyStory)…cipientId, 100)\n        }");
        List<MessageRecord> arrayList = new ArrayList<>();
        for (Object obj : reader) {
            if (!(recipient.isMyStory() && ((MessageRecord) obj).getRecipient().isGroup())) {
                arrayList.add(obj);
            }
        }
        observableEmitter.onNext(arrayList);
    }

    /* renamed from: getStoryRecords$lambda-3$lambda-1 */
    public static final void m3068getStoryRecords$lambda3$lambda1(Recipient recipient, boolean z, RecipientId recipientId, boolean z2, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(recipient, "$recipient");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        m3070getStoryRecords$lambda3$refresh(recipient, z, recipientId, z2, observableEmitter);
    }

    /* renamed from: getStoryRecords$lambda-3$lambda-2 */
    public static final void m3069getStoryRecords$lambda3$lambda2(DatabaseObserver.Observer observer) {
        Intrinsics.checkNotNullParameter(observer, "$storyObserver");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(observer);
    }

    private final Observable<StoryPost> getStoryPostFromRecord(RecipientId recipientId, MessageRecord messageRecord) {
        Observable<StoryPost> create = Observable.create(new ObservableOnSubscribe(messageRecord, this) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda11
            public final /* synthetic */ MessageRecord f$1;
            public final /* synthetic */ StoryViewerPageRepository f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                StoryViewerPageRepository.$r8$lambda$sW0efWEE3jK5pMxlXZnCUWHroK0(RecipientId.this, this.f$1, this.f$2, observableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create { emitter ->\n    …    refresh(record)\n    }");
        return create;
    }

    /* renamed from: getStoryPostFromRecord$lambda-9$refresh-4 */
    private static final void m3064getStoryPostFromRecord$lambda9$refresh4(RecipientId recipientId, StoryViewerPageRepository storyViewerPageRepository, ObservableEmitter<StoryPost> observableEmitter, MessageRecord messageRecord) {
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        long id = messageRecord.getId();
        Recipient self = messageRecord.isOutgoing() ? Recipient.self() : messageRecord.getIndividualRecipient();
        Intrinsics.checkNotNullExpressionValue(self, "if (record.isOutgoing) R…ecord.individualRecipient");
        if (!resolved.isGroup()) {
            resolved = null;
        }
        Recipient recipient = messageRecord.getRecipient().isDistributionList() ? messageRecord.getRecipient() : null;
        int viewedReceiptCount = messageRecord.getViewedReceiptCount();
        int numberOfStoryReplies = SignalDatabase.Companion.mms().getNumberOfStoryReplies(messageRecord.getId());
        long dateSent = messageRecord.getDateSent();
        MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) messageRecord;
        StoryPost.Content content = storyViewerPageRepository.getContent(mmsMessageRecord);
        ConversationMessage createWithUnresolvedData = ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(storyViewerPageRepository.context, messageRecord);
        Intrinsics.checkNotNullExpressionValue(createWithUnresolvedData, "createWithUnresolvedData(context, record)");
        observableEmitter.onNext(new StoryPost(id, self, resolved, recipient, viewedReceiptCount, numberOfStoryReplies, dateSent, content, createWithUnresolvedData, mmsMessageRecord.getStoryType().isStoryWithReplies(), mmsMessageRecord.isOutgoing() || mmsMessageRecord.getViewedReceiptCount() > 0));
    }

    /* renamed from: getStoryPostFromRecord$lambda-9 */
    public static final void m3059getStoryPostFromRecord$lambda9(RecipientId recipientId, MessageRecord messageRecord, StoryViewerPageRepository storyViewerPageRepository, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(messageRecord, "$record");
        Intrinsics.checkNotNullParameter(storyViewerPageRepository, "this$0");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        StoryViewerPageRepository$$ExternalSyntheticLambda2 storyViewerPageRepository$$ExternalSyntheticLambda2 = new DatabaseObserver.MessageObserver(observableEmitter, recipientId, storyViewerPageRepository) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ ObservableEmitter f$1;
            public final /* synthetic */ RecipientId f$2;
            public final /* synthetic */ StoryViewerPageRepository f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.MessageObserver
            public final void onMessageChanged(MessageId messageId) {
                StoryViewerPageRepository.m3055$r8$lambda$2MTqTKSEY3WGgXAM2nBBJFmPCM(MessageRecord.this, this.f$1, this.f$2, this.f$3, messageId);
            }
        };
        StoryViewerPageRepository$$ExternalSyntheticLambda3 storyViewerPageRepository$$ExternalSyntheticLambda3 = new DatabaseObserver.Observer(recipientId, storyViewerPageRepository, observableEmitter) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ StoryViewerPageRepository f$2;
            public final /* synthetic */ ObservableEmitter f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                StoryViewerPageRepository.$r8$lambda$DzjuhlXf8v2MBw67u2LDSY0VV5w(MessageRecord.this, this.f$1, this.f$2, this.f$3);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerConversationObserver(messageRecord.getThreadId(), storyViewerPageRepository$$ExternalSyntheticLambda3);
        ApplicationDependencies.getDatabaseObserver().registerMessageUpdateObserver(storyViewerPageRepository$$ExternalSyntheticLambda2);
        StoryViewerPageRepository$$ExternalSyntheticLambda4 storyViewerPageRepository$$ExternalSyntheticLambda4 = new DatabaseObserver.MessageObserver(recipientId, storyViewerPageRepository, observableEmitter) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ StoryViewerPageRepository f$2;
            public final /* synthetic */ ObservableEmitter f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.MessageObserver
            public final void onMessageChanged(MessageId messageId) {
                StoryViewerPageRepository.m3057$r8$lambda$SQYyBKeprKVx34DJJZsGLSEDc8(MessageRecord.this, this.f$1, this.f$2, this.f$3, messageId);
            }
        };
        if (resolved.isGroup()) {
            ApplicationDependencies.getDatabaseObserver().registerMessageInsertObserver(messageRecord.getThreadId(), storyViewerPageRepository$$ExternalSyntheticLambda4);
        }
        observableEmitter.setCancellable(new Cancellable(storyViewerPageRepository$$ExternalSyntheticLambda2, resolved, storyViewerPageRepository$$ExternalSyntheticLambda4) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ DatabaseObserver.MessageObserver f$1;
            public final /* synthetic */ Recipient f$2;
            public final /* synthetic */ DatabaseObserver.MessageObserver f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                StoryViewerPageRepository.$r8$lambda$KNXcgSEbmr7P4z4PYplqruUkFPU(DatabaseObserver.Observer.this, this.f$1, this.f$2, this.f$3);
            }
        });
        m3064getStoryPostFromRecord$lambda9$refresh4(recipientId, storyViewerPageRepository, observableEmitter, messageRecord);
    }

    /* renamed from: getStoryPostFromRecord$lambda-9$lambda-5 */
    public static final void m3060getStoryPostFromRecord$lambda9$lambda5(MessageRecord messageRecord, ObservableEmitter observableEmitter, RecipientId recipientId, StoryViewerPageRepository storyViewerPageRepository, MessageId messageId) {
        Intrinsics.checkNotNullParameter(messageRecord, "$record");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(storyViewerPageRepository, "this$0");
        Intrinsics.checkNotNullParameter(messageId, "it");
        if (messageId.isMms() && messageId.getId() == messageRecord.getId()) {
            try {
                MessageRecord messageRecord2 = SignalDatabase.Companion.mms().getMessageRecord(messageRecord.getId());
                if (messageRecord2.isRemoteDelete()) {
                    observableEmitter.onComplete();
                    return;
                }
                Intrinsics.checkNotNullExpressionValue(messageRecord2, "messageRecord");
                m3064getStoryPostFromRecord$lambda9$refresh4(recipientId, storyViewerPageRepository, observableEmitter, messageRecord2);
            } catch (NoSuchMessageException unused) {
                while (true) {
                    observableEmitter.onComplete();
                    return;
                }
            }
        }
    }

    /* renamed from: getStoryPostFromRecord$lambda-9$lambda-6 */
    public static final void m3061getStoryPostFromRecord$lambda9$lambda6(MessageRecord messageRecord, RecipientId recipientId, StoryViewerPageRepository storyViewerPageRepository, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(messageRecord, "$record");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(storyViewerPageRepository, "this$0");
        try {
            MessageRecord messageRecord2 = SignalDatabase.Companion.mms().getMessageRecord(messageRecord.getId());
            Intrinsics.checkNotNullExpressionValue(messageRecord2, "SignalDatabase.mms.getMessageRecord(record.id)");
            m3064getStoryPostFromRecord$lambda9$refresh4(recipientId, storyViewerPageRepository, observableEmitter, messageRecord2);
        } catch (NoSuchMessageException e) {
            Log.w(TAG, "Message deleted during content refresh.", e);
        }
    }

    /* renamed from: getStoryPostFromRecord$lambda-9$lambda-7 */
    public static final void m3062getStoryPostFromRecord$lambda9$lambda7(MessageRecord messageRecord, RecipientId recipientId, StoryViewerPageRepository storyViewerPageRepository, ObservableEmitter observableEmitter, MessageId messageId) {
        Intrinsics.checkNotNullParameter(messageRecord, "$record");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(storyViewerPageRepository, "this$0");
        Intrinsics.checkNotNullParameter(messageId, "it");
        MessageRecord messageRecord2 = SignalDatabase.Companion.mms().getMessageRecord(messageRecord.getId());
        Intrinsics.checkNotNullExpressionValue(messageRecord2, "SignalDatabase.mms.getMessageRecord(record.id)");
        m3064getStoryPostFromRecord$lambda9$refresh4(recipientId, storyViewerPageRepository, observableEmitter, messageRecord2);
    }

    /* renamed from: getStoryPostFromRecord$lambda-9$lambda-8 */
    public static final void m3063getStoryPostFromRecord$lambda9$lambda8(DatabaseObserver.Observer observer, DatabaseObserver.MessageObserver messageObserver, Recipient recipient, DatabaseObserver.MessageObserver messageObserver2) {
        Intrinsics.checkNotNullParameter(observer, "$conversationObserver");
        Intrinsics.checkNotNullParameter(messageObserver, "$messageUpdateObserver");
        Intrinsics.checkNotNullParameter(recipient, "$recipient");
        Intrinsics.checkNotNullParameter(messageObserver2, "$messageInsertObserver");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(observer);
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(messageObserver);
        if (recipient.isGroup()) {
            ApplicationDependencies.getDatabaseObserver().unregisterObserver(messageObserver2);
        }
    }

    public final Completable forceDownload(StoryPost storyPost) {
        Intrinsics.checkNotNullParameter(storyPost, "post");
        return Stories.INSTANCE.enqueueAttachmentsFromStoryForDownload((MmsMessageRecord) storyPost.getConversationMessage().getMessageRecord(), true);
    }

    public final Observable<List<StoryPost>> getStoryPostsFor(RecipientId recipientId, boolean z, boolean z2) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Observable<List<StoryPost>> observeOn = getStoryRecords(recipientId, z, z2).switchMap(new Function(recipientId) { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda9
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryViewerPageRepository.$r8$lambda$BubSO74Z9OJA8TNXtjuHXrP3Cfk(StoryViewerPageRepository.this, this.f$1, (List) obj);
            }
        }).observeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(observeOn, "getStoryRecords(recipien…bserveOn(Schedulers.io())");
        return observeOn;
    }

    /* renamed from: getStoryPostsFor$lambda-12 */
    public static final ObservableSource m3065getStoryPostsFor$lambda12(StoryViewerPageRepository storyViewerPageRepository, RecipientId recipientId, List list) {
        Intrinsics.checkNotNullParameter(storyViewerPageRepository, "this$0");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullExpressionValue(list, "records");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(storyViewerPageRepository.getStoryPostFromRecord(recipientId, (MessageRecord) it.next()));
        }
        if (arrayList.isEmpty()) {
            return Observable.just(CollectionsKt__CollectionsKt.emptyList());
        }
        return Observable.combineLatest(arrayList, new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryViewerPageRepository.m3058$r8$lambda$hScM_q6GQq8duyeIxSgYDCJQa8((Object[]) obj);
            }
        });
    }

    /* renamed from: getStoryPostsFor$lambda-12$lambda-11 */
    public static final List m3066getStoryPostsFor$lambda12$lambda11(Object[] objArr) {
        Intrinsics.checkNotNullExpressionValue(objArr, "it");
        return ArraysKt___ArraysKt.toList(objArr);
    }

    public final Completable hideStory(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Completable subscribeOn = Completable.fromAction(new Action() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                StoryViewerPageRepository.$r8$lambda$IZLc9kJcxVFhqg2clnmkZK1KEDI(RecipientId.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Signa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: hideStory$lambda-13 */
    public static final void m3071hideStory$lambda13(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        SignalDatabase.Companion.recipients().setHideStory(recipientId, true);
    }

    public final void markViewed(StoryPost storyPost) {
        Intrinsics.checkNotNullParameter(storyPost, "storyPost");
        if (!storyPost.getConversationMessage().getMessageRecord().isOutgoing()) {
            SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageRepository$$ExternalSyntheticLambda10
                @Override // java.lang.Runnable
                public final void run() {
                    StoryViewerPageRepository.$r8$lambda$O6mAPfNtvQLX09O7ehG3s3uEnws(StoryPost.this);
                }
            });
        }
    }

    /* renamed from: markViewed$lambda-14 */
    public static final void m3072markViewed$lambda14(StoryPost storyPost) {
        RecipientId recipientId;
        Intrinsics.checkNotNullParameter(storyPost, "$storyPost");
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        MessageDatabase.MarkedMessageInfo incomingMessageViewed = companion.mms().setIncomingMessageViewed(storyPost.getId());
        if (incomingMessageViewed != null) {
            ApplicationDependencies.getDatabaseObserver().notifyConversationListListeners();
            if (storyPost.getSender().isReleaseNotes()) {
                SignalStore.storyValues().setUserHasSeenOnboardingStory(true);
                return;
            }
            ApplicationDependencies.getJobManager().add(new SendViewedReceiptJob(incomingMessageViewed.getThreadId(), storyPost.getSender().getId(), incomingMessageViewed.getSyncMessageId().getTimetamp(), new MessageId(storyPost.getId(), true)));
            MultiDeviceViewedUpdateJob.enqueue(CollectionsKt__CollectionsJVMKt.listOf(incomingMessageViewed.getSyncMessageId()));
            Recipient group = storyPost.getGroup();
            if (group == null || (recipientId = group.getId()) == null) {
                recipientId = storyPost.getSender().getId();
            }
            Intrinsics.checkNotNullExpressionValue(recipientId, "storyPost.group?.id ?: storyPost.sender.id");
            companion.recipients().updateLastStoryViewTimestamp(recipientId);
            Stories.enqueueNextStoriesForDownload(recipientId, true, 5);
        }
    }

    private final StoryPost.Content getContent(MmsMessageRecord mmsMessageRecord) {
        if (mmsMessageRecord.getStoryType().isTextStory() || mmsMessageRecord.getSlideDeck().asAttachments().isEmpty()) {
            Uri parse = Uri.parse("story_text_post://" + mmsMessageRecord.getId());
            Intrinsics.checkNotNullExpressionValue(parse, "parse(\"story_text_post://${record.id}\")");
            long id = mmsMessageRecord.getId();
            String body = mmsMessageRecord.getBody();
            Intrinsics.checkNotNullExpressionValue(body, "record.body");
            boolean canParseToTextStory = canParseToTextStory(body);
            String body2 = mmsMessageRecord.getBody();
            Intrinsics.checkNotNullExpressionValue(body2, "record.body");
            return new StoryPost.Content.TextContent(parse, id, canParseToTextStory, getTextStoryLength(body2));
        }
        List<Attachment> asAttachments = mmsMessageRecord.getSlideDeck().asAttachments();
        Intrinsics.checkNotNullExpressionValue(asAttachments, "record.slideDeck.asAttachments()");
        Object obj = CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) asAttachments));
        Intrinsics.checkNotNullExpressionValue(obj, "record.slideDeck.asAttachments().first()");
        return new StoryPost.Content.AttachmentContent((Attachment) obj);
    }

    private final int getTextStoryLength(String str) {
        if (!canParseToTextStory(str)) {
            return 0;
        }
        BreakIteratorCompat instance = BreakIteratorCompat.getInstance();
        instance.setText(StoryTextPost.parseFrom(Base64.decode(str)).getBody());
        return instance.countBreaks();
    }

    private final boolean canParseToTextStory(String str) {
        if (str.length() > 0) {
            try {
                StoryTextPost.parseFrom(Base64.decode(str));
                return true;
            } catch (Exception unused) {
            }
        }
        return false;
    }
}
