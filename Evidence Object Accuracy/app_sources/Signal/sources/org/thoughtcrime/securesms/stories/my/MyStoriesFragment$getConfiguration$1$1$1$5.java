package org.thoughtcrime.securesms.stories.my;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import j$.util.function.Consumer;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.stories.my.MyStoriesItem;

/* compiled from: MyStoriesFragment.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "item", "Lorg/thoughtcrime/securesms/stories/my/MyStoriesItem$Model;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStoriesFragment$getConfiguration$1$1$1$5 extends Lambda implements Function1<MyStoriesItem.Model, Unit> {
    final /* synthetic */ MyStoriesFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MyStoriesFragment$getConfiguration$1$1$1$5(MyStoriesFragment myStoriesFragment) {
        super(1);
        this.this$0 = myStoriesFragment;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MyStoriesItem.Model model) {
        invoke(model);
        return Unit.INSTANCE;
    }

    public final void invoke(MyStoriesItem.Model model) {
        Intrinsics.checkNotNullParameter(model, "item");
        MultiselectForwardFragmentArgs.Companion companion = MultiselectForwardFragmentArgs.Companion;
        Context requireContext = this.this$0.requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        companion.create(requireContext, model.getDistributionStory().getMultiselectCollection().toSet(), new Consumer() { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesFragment$getConfiguration$1$1$1$5$$ExternalSyntheticLambda0
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                MyStoriesFragment$getConfiguration$1$1$1$5.$r8$lambda$tloyBBwGlihGGXFKRA4rBC4kEco(MyStoriesFragment.this, (MultiselectForwardFragmentArgs) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m2836invoke$lambda0(MyStoriesFragment myStoriesFragment, MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
        Intrinsics.checkNotNullParameter(myStoriesFragment, "this$0");
        Intrinsics.checkNotNullParameter(multiselectForwardFragmentArgs, "it");
        MultiselectForwardFragment.Companion companion = MultiselectForwardFragment.Companion;
        FragmentManager childFragmentManager = myStoriesFragment.getChildFragmentManager();
        Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        companion.showBottomSheet(childFragmentManager, multiselectForwardFragmentArgs);
    }
}
