package org.thoughtcrime.securesms.stories.landing;

import android.view.View;
import org.thoughtcrime.securesms.stories.landing.StoriesLandingItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoriesLandingItem$ViewHolder$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ StoriesLandingItem.Model f$0;

    public /* synthetic */ StoriesLandingItem$ViewHolder$$ExternalSyntheticLambda1(StoriesLandingItem.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        StoriesLandingItem.ViewHolder.m2808setUpClickListeners$lambda5(this.f$0, view);
    }
}
