package org.thoughtcrime.securesms.stories.viewer.reply.group;

import android.content.Context;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableEmitter;
import io.reactivex.rxjava3.core.CompletableOnSubscribe;
import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.mediasend.v2.UntrustedRecords;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.OutgoingSecureMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.MessageSender;

/* compiled from: StoryGroupReplySender.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J6\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u001e\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\u0012J,\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplySender;", "", "()V", "sendInternal", "Lio/reactivex/rxjava3/core/Completable;", "context", "Landroid/content/Context;", "storyId", "", "body", "", "mentions", "", "Lorg/thoughtcrime/securesms/database/model/Mention;", "isReaction", "", "sendReaction", "emoji", "", "sendReply", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryGroupReplySender {
    public static final StoryGroupReplySender INSTANCE = new StoryGroupReplySender();

    private StoryGroupReplySender() {
    }

    public final Completable sendReply(Context context, long j, CharSequence charSequence, List<? extends Mention> list) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(charSequence, "body");
        Intrinsics.checkNotNullParameter(list, "mentions");
        return sendInternal(context, j, charSequence, list, false);
    }

    public final Completable sendReaction(Context context, long j, String str) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(str, "emoji");
        return sendInternal(context, j, str, CollectionsKt__CollectionsKt.emptyList(), true);
    }

    private final Completable sendInternal(Context context, long j, CharSequence charSequence, List<? extends Mention> list, boolean z) {
        Completable subscribeOn = Single.fromCallable(new Callable(j) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplySender$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return StoryGroupReplySender.m3177sendInternal$lambda0(this.f$0);
            }
        }).flatMapCompletable(new Function(context, charSequence, z, list) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplySender$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ CharSequence f$1;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ List f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryGroupReplySender.m3178sendInternal$lambda3(this.f$0, this.f$1, this.f$2, this.f$3, (Pair) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "messageAndRecipient.flat…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: sendInternal$lambda-0 */
    public static final Pair m3177sendInternal$lambda0(long j) {
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        MessageRecord messageRecord = companion.mms().getMessageRecord(j);
        Recipient recipientForThreadId = companion.threads().getRecipientForThreadId(messageRecord.getThreadId());
        Intrinsics.checkNotNull(recipientForThreadId);
        return TuplesKt.to(messageRecord, recipientForThreadId);
    }

    /* renamed from: sendInternal$lambda-3 */
    public static final CompletableSource m3178sendInternal$lambda3(Context context, CharSequence charSequence, boolean z, List list, Pair pair) {
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(charSequence, "$body");
        Intrinsics.checkNotNullParameter(list, "$mentions");
        MessageRecord messageRecord = (MessageRecord) pair.component1();
        Recipient recipient = (Recipient) pair.component2();
        UntrustedRecords untrustedRecords = UntrustedRecords.INSTANCE;
        RecipientId id = recipient.getId();
        Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
        return untrustedRecords.checkForBadIdentityRecords(SetsKt__SetsJVMKt.setOf(new ContactSearchKey.RecipientSearchKey.KnownRecipient(id))).andThen(Completable.create(new CompletableOnSubscribe(context, recipient, charSequence, messageRecord, z, list) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplySender$$ExternalSyntheticLambda2
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Recipient f$1;
            public final /* synthetic */ CharSequence f$2;
            public final /* synthetic */ MessageRecord f$3;
            public final /* synthetic */ boolean f$4;
            public final /* synthetic */ List f$5;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // io.reactivex.rxjava3.core.CompletableOnSubscribe
            public final void subscribe(CompletableEmitter completableEmitter) {
                StoryGroupReplySender.m3179sendInternal$lambda3$lambda2(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, completableEmitter);
            }
        }));
    }

    /* renamed from: sendInternal$lambda-3$lambda-2 */
    public static final void m3179sendInternal$lambda3$lambda2(Context context, Recipient recipient, CharSequence charSequence, MessageRecord messageRecord, boolean z, List list, CompletableEmitter completableEmitter) {
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(recipient, "$recipient");
        Intrinsics.checkNotNullParameter(charSequence, "$body");
        Intrinsics.checkNotNullParameter(list, "$mentions");
        MessageSender.send(context, (OutgoingMediaMessage) new OutgoingSecureMediaMessage(new OutgoingMediaMessage(recipient, charSequence.toString(), CollectionsKt__CollectionsKt.emptyList(), System.currentTimeMillis(), 0, 0, false, 0, StoryType.NONE, new ParentStoryId.GroupReply(messageRecord.getId()), z, null, CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList(), list, SetsKt__SetsKt.emptySet(), SetsKt__SetsKt.emptySet(), null)), messageRecord.getThreadId(), false, (String) null, (MessageDatabase.InsertListener) new MessageDatabase.InsertListener() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplySender$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.database.MessageDatabase.InsertListener
            public final void onComplete() {
                StoryGroupReplySender.m3180sendInternal$lambda3$lambda2$lambda1(CompletableEmitter.this);
            }
        });
    }

    /* renamed from: sendInternal$lambda-3$lambda-2$lambda-1 */
    public static final void m3180sendInternal$lambda3$lambda2$lambda1(CompletableEmitter completableEmitter) {
        completableEmitter.onComplete();
    }
}
