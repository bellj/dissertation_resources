package org.thoughtcrime.securesms.stories.settings.create;

import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.settings.create.CreateStoryViewerSelectionFragmentDirections;
import org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: CreateStoryViewerSelectionFragment.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0016\u0010\u000b\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0014R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryViewerSelectionFragment;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionFragment;", "()V", "actionButtonLabel", "", "getActionButtonLabel", "()I", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getDistributionListId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "goToNextScreen", "", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class CreateStoryViewerSelectionFragment extends BaseStoryRecipientSelectionFragment {
    private final int actionButtonLabel = R.string.CreateStoryViewerSelectionFragment__next;
    private final DistributionListId distributionListId;

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment
    public int getActionButtonLabel() {
        return this.actionButtonLabel;
    }

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment
    public DistributionListId getDistributionListId() {
        return this.distributionListId;
    }

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment
    protected void goToNextScreen(Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(set, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        NavController findNavController = FragmentKt.findNavController(this);
        Object[] array = set.toArray(new RecipientId[0]);
        if (array != null) {
            CreateStoryViewerSelectionFragmentDirections.ActionCreateStoryViewerSelectionToCreateStoryWithViewers actionCreateStoryViewerSelectionToCreateStoryWithViewers = CreateStoryViewerSelectionFragmentDirections.actionCreateStoryViewerSelectionToCreateStoryWithViewers((RecipientId[]) array);
            Intrinsics.checkNotNullExpressionValue(actionCreateStoryViewerSelectionToCreateStoryWithViewers, "actionCreateStoryViewerS…ecipients.toTypedArray())");
            SafeNavigation.safeNavigate(findNavController, actionCreateStoryViewerSelectionToCreateStoryWithViewers);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }
}
