package org.thoughtcrime.securesms.stories.settings.custom.name;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.FragmentKt;
import com.airbnb.lottie.SimpleColorFilter;
import com.google.android.material.textfield.TextInputLayout;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameViewModel;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* compiled from: EditStoryNameFragment.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0019\u001a\u00020\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u001aH\u0002J\u001a\u0010\u001c\u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016R\u0014\u0010\u0003\u001a\u00020\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\b8BX\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0013\u001a\u00020\u00148BX\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0015\u0010\u0016¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/name/EditStoryNameFragment;", "Landroidx/fragment/app/Fragment;", "()V", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getDistributionListId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "initialName", "", "getInitialName", "()Ljava/lang/String;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "saveButton", "Lorg/thoughtcrime/securesms/util/views/CircularProgressMaterialButton;", "storyName", "Landroid/widget/EditText;", "storyNameWrapper", "Lcom/google/android/material/textfield/TextInputLayout;", "viewModel", "Lorg/thoughtcrime/securesms/stories/settings/custom/name/EditStoryNameViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/settings/custom/name/EditStoryNameViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "onPause", "", "onSaveClicked", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class EditStoryNameFragment extends Fragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private CircularProgressMaterialButton saveButton;
    private EditText storyName;
    private TextInputLayout storyNameWrapper;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(EditStoryNameViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameFragment$viewModel$2
        final /* synthetic */ EditStoryNameFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new EditStoryNameViewModel.Factory(EditStoryNameFragment.access$getDistributionListId(this.this$0), new EditStoryNameRepository());
        }
    });

    public EditStoryNameFragment() {
        super(R.layout.stories_edit_story_name_fragment);
    }

    private final EditStoryNameViewModel getViewModel() {
        return (EditStoryNameViewModel) this.viewModel$delegate.getValue();
    }

    public final DistributionListId getDistributionListId() {
        DistributionListId distributionListId = EditStoryNameFragmentArgs.fromBundle(requireArguments()).getDistributionListId();
        Intrinsics.checkNotNullExpressionValue(distributionListId, "fromBundle(requireArguments()).distributionListId");
        return distributionListId;
    }

    private final String getInitialName() {
        String name = EditStoryNameFragmentArgs.fromBundle(requireArguments()).getName();
        Intrinsics.checkNotNullExpressionValue(name, "fromBundle(requireArguments()).name");
        return name;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        Toolbar toolbar = (Toolbar) findViewById;
        Drawable navigationIcon = toolbar.getNavigationIcon();
        if (navigationIcon != null) {
            navigationIcon.setColorFilter(new SimpleColorFilter(ContextCompat.getColor(requireContext(), R.color.signal_icon_tint_primary)));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditStoryNameFragment.m2897$r8$lambda$o9POASzJ0dFGWRMvEkLBXvjWJ4(EditStoryNameFragment.this, view2);
            }
        });
        View findViewById2 = view.findViewById(R.id.story_name_wrapper);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.story_name_wrapper)");
        this.storyNameWrapper = (TextInputLayout) findViewById2;
        View findViewById3 = view.findViewById(R.id.story_name);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.story_name)");
        EditText editText = (EditText) findViewById3;
        this.storyName = editText;
        CircularProgressMaterialButton circularProgressMaterialButton = null;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyName");
            editText = null;
        }
        editText.setText(getInitialName());
        EditText editText2 = this.storyName;
        if (editText2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyName");
            editText2 = null;
        }
        editText2.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameFragment$$ExternalSyntheticLambda1
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return EditStoryNameFragment.$r8$lambda$93vT7aWdJO0sDgPtFByTisF5FAo(EditStoryNameFragment.this, textView, i, keyEvent);
            }
        });
        EditText editText3 = this.storyName;
        if (editText3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyName");
            editText3 = null;
        }
        editText3.addTextChangedListener(new TextWatcher(this) { // from class: org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameFragment$onViewCreated$$inlined$doAfterTextChanged$1
            final /* synthetic */ EditStoryNameFragment this$0;

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            {
                this.this$0 = r1;
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                CircularProgressMaterialButton access$getSaveButton$p = EditStoryNameFragment.access$getSaveButton$p(this.this$0);
                if (access$getSaveButton$p == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("saveButton");
                    access$getSaveButton$p = null;
                }
                boolean z = false;
                access$getSaveButton$p.setEnabled(!(editable == null || editable.length() == 0));
                CircularProgressMaterialButton access$getSaveButton$p2 = EditStoryNameFragment.access$getSaveButton$p(this.this$0);
                if (access$getSaveButton$p2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("saveButton");
                    access$getSaveButton$p2 = null;
                }
                if (editable == null || editable.length() == 0) {
                    z = true;
                }
                access$getSaveButton$p2.setAlpha(z ? 0.5f : 1.0f);
                TextInputLayout access$getStoryNameWrapper$p = EditStoryNameFragment.access$getStoryNameWrapper$p(this.this$0);
                if (access$getStoryNameWrapper$p == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("storyNameWrapper");
                    access$getStoryNameWrapper$p = null;
                }
                access$getStoryNameWrapper$p.setError(null);
            }
        });
        View findViewById4 = view.findViewById(R.id.save);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.save)");
        CircularProgressMaterialButton circularProgressMaterialButton2 = (CircularProgressMaterialButton) findViewById4;
        this.saveButton = circularProgressMaterialButton2;
        if (circularProgressMaterialButton2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("saveButton");
        } else {
            circularProgressMaterialButton = circularProgressMaterialButton2;
        }
        circularProgressMaterialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditStoryNameFragment.$r8$lambda$tT3hon2vf7CdJBVCqROxe1eCE5A(EditStoryNameFragment.this, view2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2898onViewCreated$lambda0(EditStoryNameFragment editStoryNameFragment, View view) {
        Intrinsics.checkNotNullParameter(editStoryNameFragment, "this$0");
        FragmentKt.findNavController(editStoryNameFragment).popBackStack();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final boolean m2899onViewCreated$lambda1(EditStoryNameFragment editStoryNameFragment, TextView textView, int i, KeyEvent keyEvent) {
        Intrinsics.checkNotNullParameter(editStoryNameFragment, "this$0");
        if (i != 6) {
            return false;
        }
        editStoryNameFragment.onSaveClicked();
        return true;
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2900onViewCreated$lambda3(EditStoryNameFragment editStoryNameFragment, View view) {
        Intrinsics.checkNotNullParameter(editStoryNameFragment, "this$0");
        editStoryNameFragment.onSaveClicked();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Context requireContext = requireContext();
        EditText editText = this.storyName;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyName");
            editText = null;
        }
        ViewUtil.hideKeyboard(requireContext, editText);
    }

    private final void onSaveClicked() {
        CircularProgressMaterialButton circularProgressMaterialButton = this.saveButton;
        EditText editText = null;
        if (circularProgressMaterialButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("saveButton");
            circularProgressMaterialButton = null;
        }
        circularProgressMaterialButton.setClickable(false);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        EditStoryNameViewModel viewModel = getViewModel();
        EditText editText2 = this.storyName;
        if (editText2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyName");
        } else {
            editText = editText2;
        }
        Editable text = editText.getText();
        Intrinsics.checkNotNullExpressionValue(text, "storyName.text");
        lifecycleDisposable.plusAssign(SubscribersKt.subscribeBy(viewModel.save(text), new Function1<Throwable, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameFragment$onSaveClicked$1
            final /* synthetic */ EditStoryNameFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Intrinsics.checkNotNullParameter(th, "it");
                CircularProgressMaterialButton access$getSaveButton$p = EditStoryNameFragment.access$getSaveButton$p(this.this$0);
                TextInputLayout textInputLayout = null;
                if (access$getSaveButton$p == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("saveButton");
                    access$getSaveButton$p = null;
                }
                access$getSaveButton$p.setClickable(true);
                TextInputLayout access$getStoryNameWrapper$p = EditStoryNameFragment.access$getStoryNameWrapper$p(this.this$0);
                if (access$getStoryNameWrapper$p == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("storyNameWrapper");
                } else {
                    textInputLayout = access$getStoryNameWrapper$p;
                }
                textInputLayout.setError(this.this$0.getString(R.string.CreateStoryWithViewersFragment__there_is_already_a_story_with_this_name));
            }
        }, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.stories.settings.custom.name.EditStoryNameFragment$onSaveClicked$2
            final /* synthetic */ EditStoryNameFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                FragmentKt.findNavController(this.this$0).popBackStack();
            }
        }));
    }
}
