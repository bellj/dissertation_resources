package org.thoughtcrime.securesms.stories.viewer.text;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryTextPostViewModel$1$$ExternalSyntheticLambda0 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return StoryTextPostViewModel.AnonymousClass1.m3198invoke$lambda0((StoryTextPostState) obj);
    }
}
