package org.thoughtcrime.securesms.stories;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.components.CornerMask;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: StoryFirstTimeNavigationView.kt */
@Metadata(d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000  2\u00020\u0001:\u0002\u001f B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0014J\u0006\u0010\u0018\u001a\u00020\u0015J\b\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010\u001b\u001a\u00020\u00152\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dJ\u0006\u0010\u001e\u001a\u00020\u0015R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryFirstTimeNavigationView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "blurHashView", "Landroid/widget/ImageView;", "callback", "Lorg/thoughtcrime/securesms/stories/StoryFirstTimeNavigationView$Callback;", "getCallback", "()Lorg/thoughtcrime/securesms/stories/StoryFirstTimeNavigationView$Callback;", "setCallback", "(Lorg/thoughtcrime/securesms/stories/StoryFirstTimeNavigationView$Callback;)V", "cornerMask", "Lorg/thoughtcrime/securesms/components/CornerMask;", "gotIt", "Landroid/view/View;", "overlayView", "dispatchDraw", "", "canvas", "Landroid/graphics/Canvas;", "hide", "isRenderEffectSupported", "", "setBlurHash", "blurHash", "Lorg/thoughtcrime/securesms/blurhash/BlurHash;", "show", "Callback", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryFirstTimeNavigationView extends ConstraintLayout {
    private static final int BLUR_ALPHA;
    public static final Companion Companion = new Companion(null);
    private static final int NO_BLUR_ALPHA;
    private final ImageView blurHashView;
    private Callback callback;
    private final CornerMask cornerMask;
    private final View gotIt;
    private final ImageView overlayView;

    /* compiled from: StoryFirstTimeNavigationView.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryFirstTimeNavigationView$Callback;", "", "onGotItClicked", "", "userHasSeenFirstNavigationView", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onGotItClicked();

        boolean userHasSeenFirstNavigationView();
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryFirstTimeNavigationView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ StoryFirstTimeNavigationView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryFirstTimeNavigationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        ViewGroup.inflate(context, R.layout.story_first_time_navigation_view, this);
        View findViewById = findViewById(R.id.edu_blur_hash);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.edu_blur_hash)");
        ImageView imageView = (ImageView) findViewById;
        this.blurHashView = imageView;
        View findViewById2 = findViewById(R.id.edu_overlay);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.edu_overlay)");
        ImageView imageView2 = (ImageView) findViewById2;
        this.overlayView = imageView2;
        View findViewById3 = findViewById(R.id.edu_got_it);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.edu_got_it)");
        this.gotIt = findViewById3;
        CornerMask cornerMask = new CornerMask(this);
        cornerMask.setRadius((int) DimensionUnit.DP.toPixels(18.0f));
        this.cornerMask = cornerMask;
        if (isRenderEffectSupported()) {
            ViewExtensionsKt.setVisible(imageView, false);
            ViewExtensionsKt.setVisible(imageView2, true);
            imageView2.setImageDrawable(new ColorDrawable(Color.argb(61, 0, 0, 0)));
        }
        findViewById3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.StoryFirstTimeNavigationView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoryFirstTimeNavigationView.$r8$lambda$tDzldgj80XOk7xFzENJ42earZmo(StoryFirstTimeNavigationView.this, view);
            }
        });
    }

    /* compiled from: StoryFirstTimeNavigationView.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryFirstTimeNavigationView$Companion;", "", "()V", "BLUR_ALPHA", "", "NO_BLUR_ALPHA", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final Callback getCallback() {
        return this.callback;
    }

    public final void setCallback(Callback callback) {
        this.callback = callback;
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m2767_init_$lambda1(StoryFirstTimeNavigationView storyFirstTimeNavigationView, View view) {
        Intrinsics.checkNotNullParameter(storyFirstTimeNavigationView, "this$0");
        Callback callback = storyFirstTimeNavigationView.callback;
        if (callback != null) {
            callback.onGotItClicked();
        }
        GlideApp.with(storyFirstTimeNavigationView).clear(storyFirstTimeNavigationView.blurHashView);
        storyFirstTimeNavigationView.blurHashView.setImageDrawable(null);
        storyFirstTimeNavigationView.hide();
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        super.dispatchDraw(canvas);
        this.cornerMask.mask(canvas);
    }

    public final void setBlurHash(BlurHash blurHash) {
        if (!isRenderEffectSupported()) {
            Callback callback = this.callback;
            if (!(callback != null && callback.userHasSeenFirstNavigationView())) {
                if (blurHash == null) {
                    ViewExtensionsKt.setVisible(this.blurHashView, false);
                    ViewExtensionsKt.setVisible(this.overlayView, true);
                    this.overlayView.setImageDrawable(new ColorDrawable(Color.argb((int) NO_BLUR_ALPHA, 0, 0, 0)));
                    GlideApp.with(this).clear(this.blurHashView);
                    return;
                }
                ViewExtensionsKt.setVisible(this.blurHashView, true);
                ViewExtensionsKt.setVisible(this.overlayView, true);
                this.overlayView.setImageDrawable(new ColorDrawable(Color.argb(61, 0, 0, 0)));
                GlideApp.with(this).load((Object) blurHash).addListener((RequestListener<Drawable>) new RequestListener<Drawable>(this) { // from class: org.thoughtcrime.securesms.stories.StoryFirstTimeNavigationView$setBlurHash$1
                    final /* synthetic */ StoryFirstTimeNavigationView this$0;

                    public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                        return false;
                    }

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                    }

                    @Override // com.bumptech.glide.request.RequestListener
                    public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, DataSource dataSource, boolean z) {
                        return onResourceReady((Drawable) obj, obj2, (Target<Drawable>) target, dataSource, z);
                    }

                    @Override // com.bumptech.glide.request.RequestListener
                    public boolean onLoadFailed(GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                        this.this$0.setBlurHash(null);
                        return false;
                    }
                }).into(this.blurHashView);
            }
        }
    }

    public final void show() {
        Callback callback = this.callback;
        boolean z = false;
        if (callback != null && callback.userHasSeenFirstNavigationView()) {
            z = true;
        }
        if (!z) {
            ViewExtensionsKt.setVisible(this, true);
        }
    }

    public final void hide() {
        ViewExtensionsKt.setVisible(this, false);
    }

    private final boolean isRenderEffectSupported() {
        return Build.VERSION.SDK_INT >= 31;
    }
}
