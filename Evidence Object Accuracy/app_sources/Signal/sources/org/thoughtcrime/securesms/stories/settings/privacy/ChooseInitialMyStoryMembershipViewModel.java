package org.thoughtcrime.securesms.stories.settings.privacy;

import androidx.lifecycle.ViewModel;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsRepository;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: ChooseInitialMyStoryMembershipViewModel.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u000e\u001a\u00020\u000fH\u0014J\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011J\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\u00112\u0006\u0010\u0015\u001a\u00020\u0014R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsRepository;", "(Lorg/thoughtcrime/securesms/stories/settings/my/MyStorySettingsRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipState;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "onCleared", "", "save", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "select", "Lorg/thoughtcrime/securesms/database/model/DistributionListPrivacyMode;", "selection", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ChooseInitialMyStoryMembershipViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final MyStorySettingsRepository repository;
    private final Flowable<ChooseInitialMyStoryMembershipState> state;
    private final RxStore<ChooseInitialMyStoryMembershipState> store;

    public ChooseInitialMyStoryMembershipViewModel() {
        this(null, 1, null);
    }

    public /* synthetic */ ChooseInitialMyStoryMembershipViewModel(MyStorySettingsRepository myStorySettingsRepository, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? new MyStorySettingsRepository() : myStorySettingsRepository);
    }

    public ChooseInitialMyStoryMembershipViewModel(MyStorySettingsRepository myStorySettingsRepository) {
        Intrinsics.checkNotNullParameter(myStorySettingsRepository, "repository");
        this.repository = myStorySettingsRepository;
        RxStore<ChooseInitialMyStoryMembershipState> rxStore = new RxStore<>(new ChooseInitialMyStoryMembershipState(null, null, 3, null), null, 2, null);
        this.store = rxStore;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        Flowable<ChooseInitialMyStoryMembershipState> observeOn = rxStore.getStateFlowable().observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "store.stateFlowable.obse…dSchedulers.mainThread())");
        this.state = observeOn;
        Disposable subscribe = myStorySettingsRepository.observeChooseInitialPrivacy().distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ChooseInitialMyStoryMembershipViewModel.m2934_init_$lambda0(ChooseInitialMyStoryMembershipViewModel.this, (ChooseInitialMyStoryMembershipState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.observeChoose… store.update { state } }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final Flowable<ChooseInitialMyStoryMembershipState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m2934_init_$lambda0(ChooseInitialMyStoryMembershipViewModel chooseInitialMyStoryMembershipViewModel, ChooseInitialMyStoryMembershipState chooseInitialMyStoryMembershipState) {
        Intrinsics.checkNotNullParameter(chooseInitialMyStoryMembershipViewModel, "this$0");
        chooseInitialMyStoryMembershipViewModel.store.update(new Function1<ChooseInitialMyStoryMembershipState, ChooseInitialMyStoryMembershipState>(chooseInitialMyStoryMembershipState) { // from class: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipViewModel$1$1
            final /* synthetic */ ChooseInitialMyStoryMembershipState $state;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
            }

            public final ChooseInitialMyStoryMembershipState invoke(ChooseInitialMyStoryMembershipState chooseInitialMyStoryMembershipState2) {
                Intrinsics.checkNotNullParameter(chooseInitialMyStoryMembershipState2, "it");
                ChooseInitialMyStoryMembershipState chooseInitialMyStoryMembershipState3 = this.$state;
                Intrinsics.checkNotNullExpressionValue(chooseInitialMyStoryMembershipState3, "state");
                return chooseInitialMyStoryMembershipState3;
            }
        });
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final Single<DistributionListPrivacyMode> select(DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(distributionListPrivacyMode, "selection");
        Single<DistributionListPrivacyMode> observeOn = this.repository.setPrivacyMode(distributionListPrivacyMode).toSingleDefault(distributionListPrivacyMode).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.setPrivacyMod…dSchedulers.mainThread())");
        return observeOn;
    }

    public final Single<RecipientId> save() {
        Single<RecipientId> observeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipViewModel$$ExternalSyntheticLambda1
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ChooseInitialMyStoryMembershipViewModel.m2935save$lambda1(ChooseInitialMyStoryMembershipViewModel.this);
            }
        }).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "fromCallable<RecipientId…dSchedulers.mainThread())");
        return observeOn;
    }

    /* renamed from: save$lambda-1 */
    public static final RecipientId m2935save$lambda1(ChooseInitialMyStoryMembershipViewModel chooseInitialMyStoryMembershipViewModel) {
        Intrinsics.checkNotNullParameter(chooseInitialMyStoryMembershipViewModel, "this$0");
        SignalStore.storyValues().setUserHasBeenNotifiedAboutStories(true);
        return chooseInitialMyStoryMembershipViewModel.store.getState().getRecipientId();
    }
}
