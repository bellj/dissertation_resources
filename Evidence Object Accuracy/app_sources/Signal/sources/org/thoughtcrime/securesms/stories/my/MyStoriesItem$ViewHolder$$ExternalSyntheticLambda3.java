package org.thoughtcrime.securesms.stories.my;

import org.thoughtcrime.securesms.stories.my.MyStoriesItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class MyStoriesItem$ViewHolder$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ MyStoriesItem.Model f$0;

    public /* synthetic */ MyStoriesItem$ViewHolder$$ExternalSyntheticLambda3(MyStoriesItem.Model model) {
        this.f$0 = model;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MyStoriesItem.ViewHolder.m2848showContextMenu$lambda9(this.f$0);
    }
}
