package org.thoughtcrime.securesms.stories.settings.create;

import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment;
import org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment;

/* compiled from: CreateStoryFlowDialogFragment.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\u0012\u0010\u0007\u001a\u00020\u00062\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0016J\u0010\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\fH\u0016¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryFlowDialogFragment;", "Landroidx/fragment/app/DialogFragment;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionFragment$Callback;", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersFragment$Callback;", "()V", "exitFlow", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDone", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class CreateStoryFlowDialogFragment extends DialogFragment implements BaseStoryRecipientSelectionFragment.Callback, CreateStoryWithViewersFragment.Callback {
    public CreateStoryFlowDialogFragment() {
        super(R.layout.create_story_flow_dialog_fragment);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, R.style.Signal_DayNight_Dialog_FullScreen);
    }

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment.Callback
    public void exitFlow() {
        dismissAllowingStateLoss();
    }

    @Override // org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment.Callback
    public void onDone(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Bundle bundle = new Bundle();
        bundle.putParcelable(CreateStoryWithViewersFragment.STORY_RECIPIENT, recipientId);
        Unit unit = Unit.INSTANCE;
        FragmentKt.setFragmentResult(this, CreateStoryWithViewersFragment.REQUEST_KEY, bundle);
        dismissAllowingStateLoss();
    }
}
