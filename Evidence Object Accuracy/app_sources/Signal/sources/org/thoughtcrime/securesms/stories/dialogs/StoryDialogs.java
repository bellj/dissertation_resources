package org.thoughtcrime.securesms.stories.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import androidx.appcompat.app.AlertDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;

/* compiled from: StoryDialogs.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\bJ\u001c\u0010\n\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/dialogs/StoryDialogs;", "", "()V", "displayStoryOrProfileImage", "", "context", "Landroid/content/Context;", "onViewStory", "Lkotlin/Function0;", "onViewAvatar", "resendStory", "resend", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryDialogs {
    public static final StoryDialogs INSTANCE = new StoryDialogs();

    private StoryDialogs() {
    }

    public final void resendStory(Context context, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(function0, "resend");
        new MaterialAlertDialogBuilder(context).setMessage(R.string.StoryDialogs__story_could_not_be_sent).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(R.string.StoryDialogs__send, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryDialogs$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                StoryDialogs.$r8$lambda$vokQUT0rXLKRnAERTxb1S3TSfmI(Function0.this, dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: resendStory$lambda-0 */
    public static final void m2788resendStory$lambda0(Function0 function0, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(function0, "$resend");
        function0.invoke();
    }

    public final void displayStoryOrProfileImage(Context context, Function0<Unit> function0, Function0<Unit> function02) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(function0, "onViewStory");
        Intrinsics.checkNotNullParameter(function02, "onViewAvatar");
        AlertDialog create = new MaterialAlertDialogBuilder(context).setView(R.layout.display_story_or_profile_image_dialog).setBackground(new MaterialShapeDrawable(ShapeAppearanceModel.builder().setAllCornerSizes(DimensionUnit.DP.toPixels(28.0f)).build())).create();
        create.setOnShowListener(new DialogInterface.OnShowListener(function0, function02) { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryDialogs$$ExternalSyntheticLambda3
            public final /* synthetic */ Function0 f$1;
            public final /* synthetic */ Function0 f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                StoryDialogs.$r8$lambda$Yan0xhju2pQXK89A8wacZ2HOyjE(AlertDialog.this, this.f$1, this.f$2, dialogInterface);
            }
        });
        create.show();
    }

    /* renamed from: displayStoryOrProfileImage$lambda-4$lambda-3 */
    public static final void m2785displayStoryOrProfileImage$lambda4$lambda3(AlertDialog alertDialog, Function0 function0, Function0 function02, DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(alertDialog, "$this_apply");
        Intrinsics.checkNotNullParameter(function0, "$onViewStory");
        Intrinsics.checkNotNullParameter(function02, "$onViewAvatar");
        View findViewById = alertDialog.findViewById(R.id.view_story);
        Intrinsics.checkNotNull(findViewById);
        findViewById.setOnClickListener(new View.OnClickListener(dialogInterface, function0) { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryDialogs$$ExternalSyntheticLambda0
            public final /* synthetic */ DialogInterface f$0;
            public final /* synthetic */ Function0 f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoryDialogs.$r8$lambda$mYnioaYXkTWUMJrUE9cBh1BTNyE(this.f$0, this.f$1, view);
            }
        });
        View findViewById2 = alertDialog.findViewById(R.id.view_profile_photo);
        Intrinsics.checkNotNull(findViewById2);
        findViewById2.setOnClickListener(new View.OnClickListener(dialogInterface, function02) { // from class: org.thoughtcrime.securesms.stories.dialogs.StoryDialogs$$ExternalSyntheticLambda1
            public final /* synthetic */ DialogInterface f$0;
            public final /* synthetic */ Function0 f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoryDialogs.m2784$r8$lambda$f_rF0j_H9UvzP1aps_eKuZc_I(this.f$0, this.f$1, view);
            }
        });
    }

    /* renamed from: displayStoryOrProfileImage$lambda-4$lambda-3$lambda-1 */
    public static final void m2786displayStoryOrProfileImage$lambda4$lambda3$lambda1(DialogInterface dialogInterface, Function0 function0, View view) {
        Intrinsics.checkNotNullParameter(function0, "$onViewStory");
        dialogInterface.dismiss();
        function0.invoke();
    }

    /* renamed from: displayStoryOrProfileImage$lambda-4$lambda-3$lambda-2 */
    public static final void m2787displayStoryOrProfileImage$lambda4$lambda3$lambda2(DialogInterface dialogInterface, Function0 function0, View view) {
        Intrinsics.checkNotNullParameter(function0, "$onViewAvatar");
        dialogInterface.dismiss();
        function0.invoke();
    }
}
