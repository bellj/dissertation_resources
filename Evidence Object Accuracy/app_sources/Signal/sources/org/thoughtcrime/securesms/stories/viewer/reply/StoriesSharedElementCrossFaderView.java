package org.thoughtcrime.securesms.stories.viewer.reply;

import android.animation.FloatEvaluator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.MutablePropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KProperty;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.transitions.CrossfaderTransition;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.stories.StoryTextPostModel;

/* compiled from: StoriesSharedElementCrossFaderView.kt */
@Metadata(d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0010\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 :2\u00020\u00012\u00020\u0002:\u00049:;<B\u001b\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007J\b\u0010'\u001a\u00020(H\u0002J\u0018\u0010)\u001a\u00020(2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020\u000fH\u0016J\u0010\u0010-\u001a\u00020(2\u0006\u0010,\u001a\u00020\u000fH\u0016J\u0010\u0010.\u001a\u00020(2\u0006\u0010,\u001a\u00020\u000fH\u0016J\u0018\u0010/\u001a\u00020(2\u0006\u00100\u001a\u0002012\b\u00102\u001a\u0004\u0018\u000103J\u000e\u0010/\u001a\u00020(2\u0006\u00104\u001a\u000205J\u001a\u00106\u001a\u00020(2\u0006\u00100\u001a\u0002012\b\u00102\u001a\u0004\u0018\u000103H\u0002J\u000e\u00106\u001a\u00020\u000f2\u0006\u00107\u001a\u000208J\u0010\u00106\u001a\u00020(2\u0006\u00104\u001a\u000205H\u0002R\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR+\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u000f8B@BX\u0002¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R+\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u000f8B@BX\u0002¢\u0006\u0012\n\u0004\b\u0018\u0010\u0015\u001a\u0004\b\u0016\u0010\u0011\"\u0004\b\u0017\u0010\u0013R+\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u000f8B@BX\u0002¢\u0006\u0012\n\u0004\b\u001b\u0010\u0015\u001a\u0004\b\u0019\u0010\u0011\"\u0004\b\u001a\u0010\u0013R+\u0010\u001c\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u000f8B@BX\u0002¢\u0006\u0012\n\u0004\b\u001e\u0010\u0015\u001a\u0004\b\u001c\u0010\u0011\"\u0004\b\u001d\u0010\u0013R\u0010\u0010\u001f\u001a\u0004\u0018\u00010 X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010 X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020#X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020#X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020#X\u0004¢\u0006\u0002\n\u0000¨\u0006="}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView;", "Landroidx/cardview/widget/CardView;", "Lorg/thoughtcrime/securesms/animation/transitions/CrossfaderTransition$Crossfadeable;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "callback", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView$Callback;", "getCallback", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView$Callback;", "setCallback", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView$Callback;)V", "<set-?>", "", "isSourceBlurReady", "()Z", "setSourceBlurReady", "(Z)V", "isSourceBlurReady$delegate", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView$NotifyIfReadyDelegate;", "isSourceReady", "setSourceReady", "isSourceReady$delegate", "isTargetBlurReady", "setTargetBlurReady", "isTargetBlurReady$delegate", "isTargetReady", "setTargetReady", "isTargetReady$delegate", "latestSource", "", "latestTarget", "sourceBlurView", "Landroid/widget/ImageView;", "sourceView", "targetBlurView", "targetView", "notifyIfReady", "", "onCrossfadeAnimationUpdated", "progress", "", "reverse", "onCrossfadeFinished", "onCrossfadeStarted", "setSourceView", "uri", "Landroid/net/Uri;", "blur", "Lorg/thoughtcrime/securesms/blurhash/BlurHash;", "storyTextPostModel", "Lorg/thoughtcrime/securesms/stories/StoryTextPostModel;", "setTargetView", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "Callback", "Companion", "NotifyIfReadyDelegate", "OnReadyListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoriesSharedElementCrossFaderView extends CardView implements CrossfaderTransition.Crossfadeable {
    static final /* synthetic */ KProperty<Object>[] $$delegatedProperties = {Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoriesSharedElementCrossFaderView.class, "isSourceReady", "isSourceReady()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoriesSharedElementCrossFaderView.class, "isSourceBlurReady", "isSourceBlurReady()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoriesSharedElementCrossFaderView.class, "isTargetReady", "isTargetReady()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoriesSharedElementCrossFaderView.class, "isTargetBlurReady", "isTargetBlurReady()Z", 0))};
    private static final float CORNER_RADIUS_END;
    private static final FloatEvaluator CORNER_RADIUS_EVALUATOR = new FloatEvaluator();
    private static final float CORNER_RADIUS_START;
    public static final Companion Companion = new Companion(null);
    private Callback callback;
    private final NotifyIfReadyDelegate isSourceBlurReady$delegate;
    private final NotifyIfReadyDelegate isSourceReady$delegate;
    private final NotifyIfReadyDelegate isTargetBlurReady$delegate;
    private final NotifyIfReadyDelegate isTargetReady$delegate;
    private Object latestSource;
    private Object latestTarget;
    private final ImageView sourceBlurView;
    private final ImageView sourceView;
    private final ImageView targetBlurView;
    private final ImageView targetView;

    /* compiled from: StoriesSharedElementCrossFaderView.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\b\u0010\u0005\u001a\u00020\u0003H&¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView$Callback;", "", "onAnimationFinished", "", "onAnimationStarted", "onReadyToAnimate", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onAnimationFinished();

        void onAnimationStarted();

        void onReadyToAnimate();
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoriesSharedElementCrossFaderView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ StoriesSharedElementCrossFaderView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoriesSharedElementCrossFaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        FrameLayout.inflate(context, R.layout.stories_shared_element_crossfader, this);
        View findViewById = findViewById(R.id.source_image);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.source_image)");
        this.sourceView = (ImageView) findViewById;
        View findViewById2 = findViewById(R.id.source_image_blur);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.source_image_blur)");
        this.sourceBlurView = (ImageView) findViewById2;
        View findViewById3 = findViewById(R.id.target_image);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.target_image)");
        this.targetView = (ImageView) findViewById3;
        View findViewById4 = findViewById(R.id.target_image_blur);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.target_image_blur)");
        this.targetBlurView = (ImageView) findViewById4;
        this.isSourceReady$delegate = new NotifyIfReadyDelegate(false);
        this.isSourceBlurReady$delegate = new NotifyIfReadyDelegate(false);
        this.isTargetReady$delegate = new NotifyIfReadyDelegate(false);
        this.isTargetBlurReady$delegate = new NotifyIfReadyDelegate(false);
    }

    /* compiled from: StoriesSharedElementCrossFaderView.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView$Companion;", "", "()V", "CORNER_RADIUS_END", "", "getCORNER_RADIUS_END", "()F", "CORNER_RADIUS_EVALUATOR", "Landroid/animation/FloatEvaluator;", "getCORNER_RADIUS_EVALUATOR", "()Landroid/animation/FloatEvaluator;", "CORNER_RADIUS_START", "getCORNER_RADIUS_START", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final float getCORNER_RADIUS_START() {
            return StoriesSharedElementCrossFaderView.CORNER_RADIUS_START;
        }

        public final float getCORNER_RADIUS_END() {
            return StoriesSharedElementCrossFaderView.CORNER_RADIUS_END;
        }

        public final FloatEvaluator getCORNER_RADIUS_EVALUATOR() {
            return StoriesSharedElementCrossFaderView.CORNER_RADIUS_EVALUATOR;
        }
    }

    static {
        $$delegatedProperties = new KProperty[]{Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoriesSharedElementCrossFaderView.class, "isSourceReady", "isSourceReady()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoriesSharedElementCrossFaderView.class, "isSourceBlurReady", "isSourceBlurReady()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoriesSharedElementCrossFaderView.class, "isTargetReady", "isTargetReady()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoriesSharedElementCrossFaderView.class, "isTargetBlurReady", "isTargetBlurReady()Z", 0))};
        Companion = new Companion(null);
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        CORNER_RADIUS_START = dimensionUnit.toPixels(12.0f);
        CORNER_RADIUS_END = dimensionUnit.toPixels(18.0f);
        CORNER_RADIUS_EVALUATOR = new FloatEvaluator();
    }

    private final boolean isSourceReady() {
        return this.isSourceReady$delegate.getValue(this, $$delegatedProperties[0]);
    }

    public final void setSourceReady(boolean z) {
        this.isSourceReady$delegate.setValue(this, $$delegatedProperties[0], z);
    }

    private final boolean isSourceBlurReady() {
        return this.isSourceBlurReady$delegate.getValue(this, $$delegatedProperties[1]);
    }

    public final void setSourceBlurReady(boolean z) {
        this.isSourceBlurReady$delegate.setValue(this, $$delegatedProperties[1], z);
    }

    private final boolean isTargetReady() {
        return this.isTargetReady$delegate.getValue(this, $$delegatedProperties[2]);
    }

    public final void setTargetReady(boolean z) {
        this.isTargetReady$delegate.setValue(this, $$delegatedProperties[2], z);
    }

    private final boolean isTargetBlurReady() {
        return this.isTargetBlurReady$delegate.getValue(this, $$delegatedProperties[3]);
    }

    public final void setTargetBlurReady(boolean z) {
        this.isTargetBlurReady$delegate.setValue(this, $$delegatedProperties[3], z);
    }

    public final Callback getCallback() {
        return this.callback;
    }

    public final void setCallback(Callback callback) {
        this.callback = callback;
    }

    public final void setSourceView(StoryTextPostModel storyTextPostModel) {
        Intrinsics.checkNotNullParameter(storyTextPostModel, "storyTextPostModel");
        if (!Intrinsics.areEqual(this.latestSource, storyTextPostModel)) {
            this.latestSource = storyTextPostModel;
            GlideApp.with(this.sourceView).load((Object) storyTextPostModel).addListener((RequestListener<Drawable>) new OnReadyListener(new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.StoriesSharedElementCrossFaderView$setSourceView$1
                final /* synthetic */ StoriesSharedElementCrossFaderView this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    this.this$0.setSourceReady(true);
                }
            })).placeholder(storyTextPostModel.getPlaceholder()).dontAnimate().centerCrop().into(this.sourceView);
            GlideApp.with(this.sourceBlurView).clear(this.sourceBlurView);
            setSourceBlurReady(true);
        }
    }

    public final void setSourceView(Uri uri, BlurHash blurHash) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        if (!Intrinsics.areEqual(this.latestSource, uri)) {
            this.latestSource = uri;
            GlideApp.with(this.sourceView).load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).addListener((RequestListener<Drawable>) new OnReadyListener(new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.StoriesSharedElementCrossFaderView$setSourceView$2
                final /* synthetic */ StoriesSharedElementCrossFaderView this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    this.this$0.setSourceReady(true);
                }
            })).dontAnimate().centerCrop().into(this.sourceView);
            if (blurHash == null) {
                GlideApp.with(this.sourceBlurView).clear(this.sourceBlurView);
                setSourceBlurReady(true);
                return;
            }
            GlideApp.with(this.sourceBlurView).load((Object) blurHash).addListener((RequestListener<Drawable>) new OnReadyListener(new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.StoriesSharedElementCrossFaderView$setSourceView$3
                final /* synthetic */ StoriesSharedElementCrossFaderView this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    this.this$0.setSourceBlurReady(true);
                }
            })).dontAnimate().centerCrop().into(this.sourceBlurView);
        }
    }

    public final boolean setTargetView(MmsMessageRecord mmsMessageRecord) {
        Intrinsics.checkNotNullParameter(mmsMessageRecord, "messageRecord");
        Slide thumbnailSlide = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
        BlurHash blurHash = null;
        Uri uri = thumbnailSlide != null ? thumbnailSlide.getUri() : null;
        Slide thumbnailSlide2 = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
        if (thumbnailSlide2 != null) {
            blurHash = thumbnailSlide2.getPlaceholderBlur();
        }
        if (mmsMessageRecord.getStoryType().isTextStory()) {
            setTargetView(StoryTextPostModel.CREATOR.parseFrom(mmsMessageRecord));
            return true;
        } else if (uri == null) {
            return false;
        } else {
            setTargetView(uri, blurHash);
            return true;
        }
    }

    private final void setTargetView(StoryTextPostModel storyTextPostModel) {
        if (!Intrinsics.areEqual(this.latestTarget, storyTextPostModel)) {
            this.latestTarget = storyTextPostModel;
            GlideApp.with(this.targetView).load((Object) storyTextPostModel).addListener((RequestListener<Drawable>) new OnReadyListener(new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.StoriesSharedElementCrossFaderView$setTargetView$1
                final /* synthetic */ StoriesSharedElementCrossFaderView this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    this.this$0.setTargetReady(true);
                }
            })).dontAnimate().placeholder(storyTextPostModel.getPlaceholder()).centerCrop().into(this.targetView);
            GlideApp.with(this.targetBlurView).clear(this.targetBlurView);
            setTargetBlurReady(true);
        }
    }

    private final void setTargetView(Uri uri, BlurHash blurHash) {
        if (!Intrinsics.areEqual(this.latestTarget, uri)) {
            this.latestTarget = uri;
            GlideApp.with(this.targetView).load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).addListener((RequestListener<Drawable>) new OnReadyListener(new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.StoriesSharedElementCrossFaderView$setTargetView$2
                final /* synthetic */ StoriesSharedElementCrossFaderView this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    this.this$0.setTargetReady(true);
                }
            })).dontAnimate().fitCenter().into(this.targetView);
            if (blurHash == null) {
                GlideApp.with(this.targetBlurView).clear(this.targetBlurView);
                setTargetBlurReady(true);
                return;
            }
            GlideApp.with(this.targetBlurView).load((Object) blurHash).addListener((RequestListener<Drawable>) new OnReadyListener(new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.StoriesSharedElementCrossFaderView$setTargetView$3
                final /* synthetic */ StoriesSharedElementCrossFaderView this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    this.this$0.setTargetBlurReady(true);
                }
            })).dontAnimate().centerCrop().into(this.targetBlurView);
        }
    }

    public final void notifyIfReady() {
        Callback callback;
        if (isSourceReady() && isTargetReady() && isSourceBlurReady() && isTargetBlurReady() && (callback = this.callback) != null) {
            callback.onReadyToAnimate();
        }
    }

    /* compiled from: StoriesSharedElementCrossFaderView.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0002\u0010\u0006J4\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\u000e\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\bH\u0016J>\u0010\u0010\u001a\u00020\b2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00022\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\u000e\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u000e2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u000f\u001a\u00020\bH\u0016R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView$OnReadyListener;", "Lcom/bumptech/glide/request/RequestListener;", "Landroid/graphics/drawable/Drawable;", "onReady", "Lkotlin/Function0;", "", "(Lkotlin/jvm/functions/Function0;)V", "onLoadFailed", "", "e", "Lcom/bumptech/glide/load/engine/GlideException;", "model", "", "target", "Lcom/bumptech/glide/request/target/Target;", "isFirstResource", "onResourceReady", "resource", "dataSource", "Lcom/bumptech/glide/load/DataSource;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class OnReadyListener implements RequestListener<Drawable> {
        private final Function0<Unit> onReady;

        public OnReadyListener(Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter(function0, "onReady");
            this.onReady = function0;
        }

        @Override // com.bumptech.glide.request.RequestListener
        public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, DataSource dataSource, boolean z) {
            return onResourceReady((Drawable) obj, obj2, (Target<Drawable>) target, dataSource, z);
        }

        @Override // com.bumptech.glide.request.RequestListener
        public boolean onLoadFailed(GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
            this.onReady.invoke();
            return false;
        }

        public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
            this.onReady.invoke();
            return false;
        }
    }

    @Override // org.thoughtcrime.securesms.animation.transitions.CrossfaderTransition.Crossfadeable
    public void onCrossfadeAnimationUpdated(float f, boolean z) {
        if (z) {
            this.sourceView.setAlpha(f);
            this.sourceBlurView.setAlpha(f);
            float f2 = 1.0f - f;
            this.targetView.setAlpha(f2);
            this.targetBlurView.setAlpha(f2);
            Float evaluate = CORNER_RADIUS_EVALUATOR.evaluate(f, (Number) Float.valueOf(CORNER_RADIUS_END), (Number) Float.valueOf(CORNER_RADIUS_START));
            Intrinsics.checkNotNullExpressionValue(evaluate, "CORNER_RADIUS_EVALUATOR.…END, CORNER_RADIUS_START)");
            setRadius(evaluate.floatValue());
            return;
        }
        float f3 = 1.0f - f;
        this.sourceView.setAlpha(f3);
        this.sourceBlurView.setAlpha(f3);
        this.targetView.setAlpha(f);
        this.targetBlurView.setAlpha(f);
        Float evaluate2 = CORNER_RADIUS_EVALUATOR.evaluate(f, (Number) Float.valueOf(CORNER_RADIUS_START), (Number) Float.valueOf(CORNER_RADIUS_END));
        Intrinsics.checkNotNullExpressionValue(evaluate2, "CORNER_RADIUS_EVALUATOR.…START, CORNER_RADIUS_END)");
        setRadius(evaluate2.floatValue());
    }

    @Override // org.thoughtcrime.securesms.animation.transitions.CrossfaderTransition.Crossfadeable
    public void onCrossfadeStarted(boolean z) {
        float f = 1.0f;
        setAlpha(1.0f);
        this.sourceView.setAlpha(z ? 0.0f : 1.0f);
        this.sourceBlurView.setAlpha(z ? 0.0f : 1.0f);
        this.targetView.setAlpha(z ? 1.0f : 0.0f);
        ImageView imageView = this.targetBlurView;
        if (!z) {
            f = 0.0f;
        }
        imageView.setAlpha(f);
        setRadius(z ? CORNER_RADIUS_END : CORNER_RADIUS_START);
        Callback callback = this.callback;
        if (callback != null) {
            callback.onAnimationStarted();
        }
    }

    @Override // org.thoughtcrime.securesms.animation.transitions.CrossfaderTransition.Crossfadeable
    public void onCrossfadeFinished(boolean z) {
        Callback callback;
        if (!z && (callback = this.callback) != null) {
            callback.onAnimationFinished();
        }
    }

    /* compiled from: StoriesSharedElementCrossFaderView.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001d\u0010\u0005\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\n2\n\u0010\u000b\u001a\u0006\u0012\u0002\b\u00030\fH\u0002J%\u0010\u0007\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\n2\n\u0010\u000b\u001a\u0006\u0012\u0002\b\u00030\f2\u0006\u0010\u000e\u001a\u00020\u0003H\u0002R\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView$NotifyIfReadyDelegate;", "", DraftDatabase.DRAFT_VALUE, "", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView;Z)V", "getValue", "()Z", "setValue", "(Z)V", "storiesSharedElementCrossFaderView", "Lorg/thoughtcrime/securesms/stories/viewer/reply/StoriesSharedElementCrossFaderView;", "property", "Lkotlin/reflect/KProperty;", "", "b", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public final class NotifyIfReadyDelegate {
        private boolean value;

        public NotifyIfReadyDelegate(boolean z) {
            StoriesSharedElementCrossFaderView.this = r1;
            this.value = z;
        }

        public final boolean getValue() {
            return this.value;
        }

        public final void setValue(boolean z) {
            this.value = z;
        }

        public final boolean getValue(StoriesSharedElementCrossFaderView storiesSharedElementCrossFaderView, KProperty<?> kProperty) {
            Intrinsics.checkNotNullParameter(storiesSharedElementCrossFaderView, "storiesSharedElementCrossFaderView");
            Intrinsics.checkNotNullParameter(kProperty, "property");
            return this.value;
        }

        public final void setValue(StoriesSharedElementCrossFaderView storiesSharedElementCrossFaderView, KProperty<?> kProperty, boolean z) {
            Intrinsics.checkNotNullParameter(storiesSharedElementCrossFaderView, "storiesSharedElementCrossFaderView");
            Intrinsics.checkNotNullParameter(kProperty, "property");
            this.value = z;
            StoriesSharedElementCrossFaderView.this.notifyIfReady();
        }
    }
}
