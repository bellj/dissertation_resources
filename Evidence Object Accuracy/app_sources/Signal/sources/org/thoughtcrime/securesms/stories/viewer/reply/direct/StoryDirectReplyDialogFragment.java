package org.thoughtcrime.securesms.stories.viewer.reply.direct;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentKt;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.KeyboardEntryDialogFragment;
import org.thoughtcrime.securesms.components.emoji.EmojiEventListener;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.keyboard.KeyboardPagerViewModel;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment;
import org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel;
import org.thoughtcrime.securesms.stories.viewer.reply.composer.StoryReplyComposer;
import org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyViewModel;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: StoryDirectReplyDialogFragment.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 D2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005:\u0001DB\u0005¢\u0006\u0002\u0010\u0006J\b\u0010-\u001a\u00020.H\u0016J\u0010\u0010/\u001a\u00020.2\u0006\u00100\u001a\u000201H\u0016J\u0012\u00102\u001a\u00020.2\b\u00103\u001a\u0004\u0018\u000104H\u0016J\u0012\u00105\u001a\u00020.2\b\u00106\u001a\u0004\u0018\u000107H\u0016J\b\u00108\u001a\u00020.H\u0016J\b\u00109\u001a\u00020.H\u0016J\b\u0010:\u001a\u00020.H\u0016J\u0010\u0010;\u001a\u00020.2\u0006\u00103\u001a\u000204H\u0016J\b\u0010<\u001a\u00020.H\u0016J\u001a\u0010=\u001a\u00020.2\u0006\u0010>\u001a\u00020?2\b\u0010@\u001a\u0004\u0018\u00010AH\u0016J\b\u0010B\u001a\u00020.H\u0016J\u0010\u0010C\u001a\u00020.2\u0006\u00103\u001a\u000204H\u0002R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u001b\u0010\f\u001a\u00020\r8BX\u0002¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\u0004\u0018\u00010\u00158BX\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u00198BX\u0004¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u00020\u001d8BX\u0002¢\u0006\f\n\u0004\b \u0010\u0011\u001a\u0004\b\u001e\u0010\u001fR\u0014\u0010!\u001a\u00020\"XD¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u001b\u0010%\u001a\u00020&8BX\u0002¢\u0006\f\n\u0004\b)\u0010\u0011\u001a\u0004\b'\u0010(R\u0014\u0010*\u001a\u00020\nXD¢\u0006\b\n\u0000\u001a\u0004\b+\u0010,¨\u0006E"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyDialogFragment;", "Lorg/thoughtcrime/securesms/components/KeyboardEntryDialogFragment;", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment$Callback;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiEventListener;", "Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchFragment$Callback;", "Lorg/thoughtcrime/securesms/reactions/any/ReactWithAnyEmojiBottomSheetDialogFragment$Callback;", "()V", "composer", "Lorg/thoughtcrime/securesms/stories/viewer/reply/composer/StoryReplyComposer;", "isReactClosingAfterSend", "", "isRequestingReactWithAny", "keyboardPagerViewModel", "Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerViewModel;", "getKeyboardPagerViewModel", "()Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerViewModel;", "keyboardPagerViewModel$delegate", "Lkotlin/Lazy;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "storyId", "", "getStoryId", "()J", "storyViewerPageViewModel", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "getStoryViewerPageViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "storyViewerPageViewModel$delegate", "themeResId", "", "getThemeResId", "()I", "viewModel", "Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyViewModel;", "viewModel$delegate", "withDim", "getWithDim", "()Z", "closeEmojiSearch", "", "onDismiss", "dialog", "Landroid/content/DialogInterface;", "onEmojiSelected", "emoji", "", "onKeyEvent", "keyEvent", "Landroid/view/KeyEvent;", "onKeyboardHidden", "onPause", "onReactWithAnyEmojiDialogDismissed", "onReactWithAnyEmojiSelected", "onResume", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "openEmojiSearch", "sendReaction", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryDirectReplyDialogFragment extends KeyboardEntryDialogFragment implements EmojiKeyboardPageFragment.Callback, EmojiEventListener, EmojiSearchFragment.Callback, ReactWithAnyEmojiBottomSheetDialogFragment.Callback {
    private static final String ARG_RECIPIENT_ID;
    private static final String ARG_STORY_ID;
    public static final Companion Companion = new Companion(null);
    public static final String REQUEST_EMOJI;
    private StoryReplyComposer composer;
    private boolean isReactClosingAfterSend;
    private boolean isRequestingReactWithAny;
    private final Lazy keyboardPagerViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(KeyboardPagerViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$keyboardPagerViewModel$2
        final /* synthetic */ StoryDirectReplyDialogFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$special$$inlined$viewModels$default$3
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy storyViewerPageViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryViewerPageViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$storyViewerPageViewModel$2
        final /* synthetic */ StoryDirectReplyDialogFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            Fragment requireParentFragment = this.this$0.requireParentFragment();
            Intrinsics.checkNotNullExpressionValue(requireParentFragment, "requireParentFragment()");
            return requireParentFragment;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$special$$inlined$viewModels$default$4
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private final int themeResId = R.style.Theme_Signal_RoundedBottomSheet_Stories;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryDirectReplyViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$viewModel$2
        final /* synthetic */ StoryDirectReplyDialogFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            long j = this.this$0.getStoryId();
            RecipientId recipientId = this.this$0.getRecipientId();
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            return new StoryDirectReplyViewModel.Factory(j, recipientId, new StoryDirectReplyRepository(requireContext));
        }
    });
    private final boolean withDim = true;

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onKeyEvent(KeyEvent keyEvent) {
    }

    public StoryDirectReplyDialogFragment() {
        super(R.layout.stories_reply_to_story_fragment);
    }

    @Override // org.thoughtcrime.securesms.components.KeyboardEntryDialogFragment
    protected int getThemeResId() {
        return this.themeResId;
    }

    public final StoryDirectReplyViewModel getViewModel() {
        return (StoryDirectReplyViewModel) this.viewModel$delegate.getValue();
    }

    public final KeyboardPagerViewModel getKeyboardPagerViewModel() {
        return (KeyboardPagerViewModel) this.keyboardPagerViewModel$delegate.getValue();
    }

    private final StoryViewerPageViewModel getStoryViewerPageViewModel() {
        return (StoryViewerPageViewModel) this.storyViewerPageViewModel$delegate.getValue();
    }

    public final long getStoryId() {
        return requireArguments().getLong(ARG_STORY_ID);
    }

    public final RecipientId getRecipientId() {
        return (RecipientId) requireArguments().getParcelable(ARG_RECIPIENT_ID);
    }

    @Override // org.thoughtcrime.securesms.components.KeyboardEntryDialogFragment
    protected boolean getWithDim() {
        return this.withDim;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        View findViewById = view.findViewById(R.id.input);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.input)");
        StoryReplyComposer storyReplyComposer = (StoryReplyComposer) findViewById;
        this.composer = storyReplyComposer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        storyReplyComposer.setCallback(new StoryDirectReplyDialogFragment$onViewCreated$1(this));
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StoryDirectReplyDialogFragment.m3115onViewCreated$lambda0(StoryDirectReplyDialogFragment.this, (StoryDirectReplyState) obj);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m3115onViewCreated$lambda0(StoryDirectReplyDialogFragment storyDirectReplyDialogFragment, StoryDirectReplyState storyDirectReplyState) {
        Intrinsics.checkNotNullParameter(storyDirectReplyDialogFragment, "this$0");
        StoryReplyComposer storyReplyComposer = null;
        if (storyDirectReplyState.getGroupDirectReplyRecipient() != null) {
            StoryReplyComposer storyReplyComposer2 = storyDirectReplyDialogFragment.composer;
            if (storyReplyComposer2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("composer");
                storyReplyComposer2 = null;
            }
            storyReplyComposer2.displayPrivacyChrome(storyDirectReplyState.getGroupDirectReplyRecipient());
        } else if (storyDirectReplyState.getStoryRecord() != null) {
            StoryReplyComposer storyReplyComposer3 = storyDirectReplyDialogFragment.composer;
            if (storyReplyComposer3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("composer");
                storyReplyComposer3 = null;
            }
            Recipient recipient = storyDirectReplyState.getStoryRecord().getRecipient();
            Intrinsics.checkNotNullExpressionValue(recipient, "state.storyRecord.recipient");
            storyReplyComposer3.displayPrivacyChrome(recipient);
        }
        if (storyDirectReplyState.getStoryRecord() != null) {
            StoryReplyComposer storyReplyComposer4 = storyDirectReplyDialogFragment.composer;
            if (storyReplyComposer4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("composer");
            } else {
                storyReplyComposer = storyReplyComposer4;
            }
            MessageRecord storyRecord = storyDirectReplyState.getStoryRecord();
            if (storyRecord != null) {
                storyReplyComposer.setQuote((MediaMmsMessageRecord) storyRecord);
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord");
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        ViewUtil.focusAndShowKeyboard(storyReplyComposer.getInput());
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Context requireContext = requireContext();
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        ViewUtil.hideKeyboard(requireContext, storyReplyComposer.getInput());
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment.Callback
    public void openEmojiSearch() {
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        storyReplyComposer.openEmojiSearch();
    }

    @Override // org.thoughtcrime.securesms.components.KeyboardEntryDialogFragment, org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardHiddenListener
    public void onKeyboardHidden() {
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        if (!storyReplyComposer.isRequestingEmojiDrawer() && !this.isRequestingReactWithAny) {
            super.onKeyboardHidden();
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        getStoryViewerPageViewModel().setIsDisplayingDirectReplyDialog(false);
    }

    /* compiled from: StoryDirectReplyDialogFragment.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\fR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyDialogFragment$Companion;", "", "()V", "ARG_RECIPIENT_ID", "", "ARG_STORY_ID", "REQUEST_EMOJI", "create", "Landroidx/fragment/app/DialogFragment;", "storyId", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public static /* synthetic */ DialogFragment create$default(Companion companion, long j, RecipientId recipientId, int i, Object obj) {
            if ((i & 2) != 0) {
                recipientId = null;
            }
            return companion.create(j, recipientId);
        }

        public final DialogFragment create(long j, RecipientId recipientId) {
            StoryDirectReplyDialogFragment storyDirectReplyDialogFragment = new StoryDirectReplyDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(StoryDirectReplyDialogFragment.ARG_STORY_ID, j);
            bundle.putParcelable(StoryDirectReplyDialogFragment.ARG_RECIPIENT_ID, recipientId);
            storyDirectReplyDialogFragment.setArguments(bundle);
            return storyDirectReplyDialogFragment;
        }
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onEmojiSelected(String str) {
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        storyReplyComposer.onEmojiSelected(str);
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment.Callback
    public void closeEmojiSearch() {
        StoryReplyComposer storyReplyComposer = this.composer;
        if (storyReplyComposer == null) {
            Intrinsics.throwUninitializedPropertyAccessException("composer");
            storyReplyComposer = null;
        }
        storyReplyComposer.closeEmojiSearch();
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiDialogDismissed() {
        this.isRequestingReactWithAny = false;
        if (!this.isReactClosingAfterSend) {
            StoryReplyComposer storyReplyComposer = this.composer;
            if (storyReplyComposer == null) {
                Intrinsics.throwUninitializedPropertyAccessException("composer");
                storyReplyComposer = null;
            }
            ViewUtil.focusAndShowKeyboard(storyReplyComposer.getInput());
        }
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiSelected(String str) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        sendReaction(str);
        this.isReactClosingAfterSend = true;
    }

    public final void sendReaction(String str) {
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().sendReaction(str).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action(str) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyDialogFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                StoryDirectReplyDialogFragment.m3116sendReaction$lambda2(StoryDirectReplyDialogFragment.this, this.f$1);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.sendReaction(e…lowingStateLoss()\n      }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* renamed from: sendReaction$lambda-2 */
    public static final void m3116sendReaction$lambda2(StoryDirectReplyDialogFragment storyDirectReplyDialogFragment, String str) {
        Intrinsics.checkNotNullParameter(storyDirectReplyDialogFragment, "this$0");
        Intrinsics.checkNotNullParameter(str, "$emoji");
        Bundle bundle = new Bundle();
        bundle.putString(REQUEST_EMOJI, str);
        Unit unit = Unit.INSTANCE;
        FragmentKt.setFragmentResult(storyDirectReplyDialogFragment, REQUEST_EMOJI, bundle);
        storyDirectReplyDialogFragment.dismissAllowingStateLoss();
    }
}
