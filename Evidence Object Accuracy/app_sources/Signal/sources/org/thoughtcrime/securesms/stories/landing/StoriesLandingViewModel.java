package org.thoughtcrime.securesms.stories.landing;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: StoriesLandingViewModel.kt */
@Metadata(d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001$B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001c\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\bJ\b\u0010\u0018\u001a\u00020\u0019H\u0014J\u000e\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dJ\u000e\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020\bJ\u0016\u0010 \u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\t\"\u0004\b\n\u0010\u000bR\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006%"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingViewModel;", "Landroidx/lifecycle/ViewModel;", "storiesLandingRepository", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingRepository;", "(Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "isTransitioningToAnotherScreen", "", "()Z", "setTransitioningToAnotherScreen", "(Z)V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "getRecipientIds", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "hidden", "isUnviewed", "onCleared", "", "resend", "Lio/reactivex/rxjava3/core/Completable;", "story", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "setHiddenContentVisible", "isExpanded", "setHideStory", "sender", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "hide", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoriesLandingViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private boolean isTransitioningToAnotherScreen;
    private final LiveData<StoriesLandingState> state;
    private final Store<StoriesLandingState> store;
    private final StoriesLandingRepository storiesLandingRepository;

    public StoriesLandingViewModel(StoriesLandingRepository storiesLandingRepository) {
        Intrinsics.checkNotNullParameter(storiesLandingRepository, "storiesLandingRepository");
        this.storiesLandingRepository = storiesLandingRepository;
        Store<StoriesLandingState> store = new Store<>(new StoriesLandingState(null, false, false, null, 15, null));
        this.store = store;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        LiveData<StoriesLandingState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        Disposable subscribe = storiesLandingRepository.getStories().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoriesLandingViewModel.m2832_init_$lambda2(StoriesLandingViewModel.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "storiesLandingRepository…}\n        )\n      }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final LiveData<StoriesLandingState> getState() {
        return this.state;
    }

    public final boolean isTransitioningToAnotherScreen() {
        return this.isTransitioningToAnotherScreen;
    }

    public final void setTransitioningToAnotherScreen(boolean z) {
        this.isTransitioningToAnotherScreen = z;
    }

    /* renamed from: _init_$lambda-2 */
    public static final void m2832_init_$lambda2(StoriesLandingViewModel storiesLandingViewModel, List list) {
        Intrinsics.checkNotNullParameter(storiesLandingViewModel, "this$0");
        storiesLandingViewModel.store.update(new Function(list) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoriesLandingViewModel.m2833lambda2$lambda1(this.f$0, (StoriesLandingState) obj);
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        if (r7 != false) goto L_0x0038;
     */
    /* renamed from: lambda-2$lambda-1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final org.thoughtcrime.securesms.stories.landing.StoriesLandingState m2833lambda2$lambda1(java.util.List r7, org.thoughtcrime.securesms.stories.landing.StoriesLandingState r8) {
        /*
            org.thoughtcrime.securesms.stories.landing.StoriesLandingState$LoadingState r4 = org.thoughtcrime.securesms.stories.landing.StoriesLandingState.LoadingState.LOADED
            java.lang.String r0 = "stories"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r7, r0)
            java.util.List r1 = kotlin.collections.CollectionsKt.sorted(r7)
            boolean r0 = r7.isEmpty()
            r2 = 0
            r3 = 1
            if (r0 != 0) goto L_0x0038
            boolean r0 = r7.isEmpty()
            if (r0 == 0) goto L_0x001b
        L_0x0019:
            r7 = 1
            goto L_0x0036
        L_0x001b:
            java.util.Iterator r7 = r7.iterator()
        L_0x001f:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0019
            java.lang.Object r0 = r7.next()
            org.thoughtcrime.securesms.stories.landing.StoriesLandingItemData r0 = (org.thoughtcrime.securesms.stories.landing.StoriesLandingItemData) r0
            org.thoughtcrime.securesms.recipients.Recipient r0 = r0.getStoryRecipient()
            boolean r0 = r0.isMyStory()
            if (r0 == 0) goto L_0x001f
            r7 = 0
        L_0x0036:
            if (r7 == 0) goto L_0x0039
        L_0x0038:
            r2 = 1
        L_0x0039:
            java.lang.String r7 = "state"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r8, r7)
            r3 = 0
            r5 = 4
            r6 = 0
            r0 = r8
            org.thoughtcrime.securesms.stories.landing.StoriesLandingState r7 = org.thoughtcrime.securesms.stories.landing.StoriesLandingState.copy$default(r0, r1, r2, r3, r4, r5, r6)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.landing.StoriesLandingViewModel.m2833lambda2$lambda1(java.util.List, org.thoughtcrime.securesms.stories.landing.StoriesLandingState):org.thoughtcrime.securesms.stories.landing.StoriesLandingState");
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final Completable resend(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "story");
        return this.storiesLandingRepository.resend(messageRecord);
    }

    public final Completable setHideStory(Recipient recipient, boolean z) {
        Intrinsics.checkNotNullParameter(recipient, "sender");
        StoriesLandingRepository storiesLandingRepository = this.storiesLandingRepository;
        RecipientId id = recipient.getId();
        Intrinsics.checkNotNullExpressionValue(id, "sender.id");
        return storiesLandingRepository.setHideStory(id, z);
    }

    /* renamed from: setHiddenContentVisible$lambda-3 */
    public static final StoriesLandingState m2834setHiddenContentVisible$lambda3(boolean z, StoriesLandingState storiesLandingState) {
        Intrinsics.checkNotNullExpressionValue(storiesLandingState, "it");
        return StoriesLandingState.copy$default(storiesLandingState, null, false, z, null, 11, null);
    }

    public final void setHiddenContentVisible(boolean z) {
        this.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoriesLandingViewModel.m2834setHiddenContentVisible$lambda3(this.f$0, (StoriesLandingState) obj);
            }
        });
    }

    public final List<RecipientId> getRecipientIds(boolean z, boolean z2) {
        List<StoriesLandingItemData> storiesLandingItems = this.store.getState().getStoriesLandingItems();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = storiesLandingItems.iterator();
        while (true) {
            boolean z3 = false;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (((StoriesLandingItemData) next).isHidden() == z) {
                z3 = true;
            }
            if (z3) {
                arrayList.add(next);
            }
        }
        ArrayList<StoriesLandingItemData> arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (!z2 || ((StoriesLandingItemData) obj).getStoryViewState() == StoryViewState.UNVIEWED) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList2, 10));
        for (StoriesLandingItemData storiesLandingItemData : arrayList2) {
            arrayList3.add(storiesLandingItemData.getStoryRecipient().getId());
        }
        return arrayList3;
    }

    /* compiled from: StoriesLandingViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "storiesLandingRepository", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingRepository;", "(Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final StoriesLandingRepository storiesLandingRepository;

        public Factory(StoriesLandingRepository storiesLandingRepository) {
            Intrinsics.checkNotNullParameter(storiesLandingRepository, "storiesLandingRepository");
            this.storiesLandingRepository = storiesLandingRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StoriesLandingViewModel(this.storiesLandingRepository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.landing.StoriesLandingViewModel.Factory.create");
        }
    }
}
