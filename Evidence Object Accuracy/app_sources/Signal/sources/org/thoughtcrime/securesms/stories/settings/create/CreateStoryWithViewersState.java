package org.thoughtcrime.securesms.stories.settings.create;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: CreateStoryWithViewersState.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0002\u001a\u001bB%\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J)\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState;", "", EmojiSearchDatabase.LABEL, "", "error", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$NameError;", "saveState", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState;", "(Ljava/lang/CharSequence;Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$NameError;Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState;)V", "getError", "()Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$NameError;", "getLabel", "()Ljava/lang/CharSequence;", "getSaveState", "()Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "NameError", "SaveState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class CreateStoryWithViewersState {
    private final NameError error;
    private final CharSequence label;
    private final SaveState saveState;

    /* compiled from: CreateStoryWithViewersState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$NameError;", "", "(Ljava/lang/String;I)V", "NO_LABEL", "DUPLICATE_LABEL", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum NameError {
        NO_LABEL,
        DUPLICATE_LABEL
    }

    public CreateStoryWithViewersState() {
        this(null, null, null, 7, null);
    }

    public static /* synthetic */ CreateStoryWithViewersState copy$default(CreateStoryWithViewersState createStoryWithViewersState, CharSequence charSequence, NameError nameError, SaveState saveState, int i, Object obj) {
        if ((i & 1) != 0) {
            charSequence = createStoryWithViewersState.label;
        }
        if ((i & 2) != 0) {
            nameError = createStoryWithViewersState.error;
        }
        if ((i & 4) != 0) {
            saveState = createStoryWithViewersState.saveState;
        }
        return createStoryWithViewersState.copy(charSequence, nameError, saveState);
    }

    public final CharSequence component1() {
        return this.label;
    }

    public final NameError component2() {
        return this.error;
    }

    public final SaveState component3() {
        return this.saveState;
    }

    public final CreateStoryWithViewersState copy(CharSequence charSequence, NameError nameError, SaveState saveState) {
        Intrinsics.checkNotNullParameter(charSequence, EmojiSearchDatabase.LABEL);
        Intrinsics.checkNotNullParameter(saveState, "saveState");
        return new CreateStoryWithViewersState(charSequence, nameError, saveState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CreateStoryWithViewersState)) {
            return false;
        }
        CreateStoryWithViewersState createStoryWithViewersState = (CreateStoryWithViewersState) obj;
        return Intrinsics.areEqual(this.label, createStoryWithViewersState.label) && this.error == createStoryWithViewersState.error && Intrinsics.areEqual(this.saveState, createStoryWithViewersState.saveState);
    }

    public int hashCode() {
        int hashCode = this.label.hashCode() * 31;
        NameError nameError = this.error;
        return ((hashCode + (nameError == null ? 0 : nameError.hashCode())) * 31) + this.saveState.hashCode();
    }

    public String toString() {
        return "CreateStoryWithViewersState(label=" + ((Object) this.label) + ", error=" + this.error + ", saveState=" + this.saveState + ')';
    }

    public CreateStoryWithViewersState(CharSequence charSequence, NameError nameError, SaveState saveState) {
        Intrinsics.checkNotNullParameter(charSequence, EmojiSearchDatabase.LABEL);
        Intrinsics.checkNotNullParameter(saveState, "saveState");
        this.label = charSequence;
        this.error = nameError;
        this.saveState = saveState;
    }

    public final CharSequence getLabel() {
        return this.label;
    }

    public final NameError getError() {
        return this.error;
    }

    public /* synthetic */ CreateStoryWithViewersState(String str, NameError nameError, SaveState saveState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? null : nameError, (i & 4) != 0 ? SaveState.Init.INSTANCE : saveState);
    }

    public final SaveState getSaveState() {
        return this.saveState;
    }

    /* compiled from: CreateStoryWithViewersState.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState;", "", "()V", "Init", "Saved", "Saving", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState$Init;", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState$Saving;", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState$Saved;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static abstract class SaveState {
        public /* synthetic */ SaveState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: CreateStoryWithViewersState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState$Init;", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Init extends SaveState {
            public static final Init INSTANCE = new Init();

            private Init() {
                super(null);
            }
        }

        private SaveState() {
        }

        /* compiled from: CreateStoryWithViewersState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState$Saving;", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Saving extends SaveState {
            public static final Saving INSTANCE = new Saving();

            private Saving() {
                super(null);
            }
        }

        /* compiled from: CreateStoryWithViewersState.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState$Saved;", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState$SaveState;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Saved extends SaveState {
            private final RecipientId recipientId;

            public static /* synthetic */ Saved copy$default(Saved saved, RecipientId recipientId, int i, Object obj) {
                if ((i & 1) != 0) {
                    recipientId = saved.recipientId;
                }
                return saved.copy(recipientId);
            }

            public final RecipientId component1() {
                return this.recipientId;
            }

            public final Saved copy(RecipientId recipientId) {
                Intrinsics.checkNotNullParameter(recipientId, "recipientId");
                return new Saved(recipientId);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Saved) && Intrinsics.areEqual(this.recipientId, ((Saved) obj).recipientId);
            }

            public int hashCode() {
                return this.recipientId.hashCode();
            }

            public String toString() {
                return "Saved(recipientId=" + this.recipientId + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Saved(RecipientId recipientId) {
                super(null);
                Intrinsics.checkNotNullParameter(recipientId, "recipientId");
                this.recipientId = recipientId;
            }

            public final RecipientId getRecipientId() {
                return this.recipientId;
            }
        }
    }
}
