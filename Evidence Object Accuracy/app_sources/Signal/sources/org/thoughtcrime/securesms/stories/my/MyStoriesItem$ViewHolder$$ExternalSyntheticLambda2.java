package org.thoughtcrime.securesms.stories.my;

import org.thoughtcrime.securesms.stories.my.MyStoriesItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class MyStoriesItem$ViewHolder$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ MyStoriesItem.Model f$0;

    public /* synthetic */ MyStoriesItem$ViewHolder$$ExternalSyntheticLambda2(MyStoriesItem.Model model) {
        this.f$0 = model;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MyStoriesItem.ViewHolder.m2847showContextMenu$lambda8(this.f$0);
    }
}
