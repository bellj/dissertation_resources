package org.thoughtcrime.securesms.stories.settings.privacy;

import androidx.appcompat.widget.Toolbar;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment;

/* compiled from: ChangeMyStoryMembershipFragment.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b&\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0004H\u0014R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\b8VX\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\n¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/privacy/ChangeMyStoryMembershipFragment;", "Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionFragment;", "()V", "actionButtonLabel", "", "getActionButtonLabel", "()I", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getDistributionListId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "presentTitle", "", "toolbar", "Landroidx/appcompat/widget/Toolbar;", MediaPreviewActivity.SIZE_EXTRA, "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public abstract class ChangeMyStoryMembershipFragment extends BaseStoryRecipientSelectionFragment {
    private final int actionButtonLabel = R.string.HideStoryFromFragment__done;

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment
    protected void presentTitle(Toolbar toolbar, int i) {
        Intrinsics.checkNotNullParameter(toolbar, "toolbar");
    }

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment
    public int getActionButtonLabel() {
        return this.actionButtonLabel;
    }

    @Override // org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionFragment
    public DistributionListId getDistributionListId() {
        DistributionListId from = DistributionListId.from(1);
        Intrinsics.checkNotNullExpressionValue(from, "from(DistributionListId.MY_STORY_ID)");
        return from;
    }
}
