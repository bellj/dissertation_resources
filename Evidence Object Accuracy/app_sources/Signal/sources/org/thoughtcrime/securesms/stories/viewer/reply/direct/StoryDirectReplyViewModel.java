package org.thoughtcrime.securesms.stories.viewer.reply.direct;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: StoryDirectReplyViewModel.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u001aB\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u0012\u001a\u00020\u0013H\u0014J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u0017R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyViewModel;", "Landroidx/lifecycle/ViewModel;", "storyId", "", "groupDirectReplyRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyRepository;", "(JLorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "sendReaction", "Lio/reactivex/rxjava3/core/Completable;", "emoji", "", "sendReply", "charSequence", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryDirectReplyViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final RecipientId groupDirectReplyRecipientId;
    private final StoryDirectReplyRepository repository;
    private final LiveData<StoryDirectReplyState> state;
    private final Store<StoryDirectReplyState> store;
    private final long storyId;

    /* JADX DEBUG: Type inference failed for r7v2. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, androidx.lifecycle.LiveData<Input> */
    public StoryDirectReplyViewModel(long j, RecipientId recipientId, StoryDirectReplyRepository storyDirectReplyRepository) {
        Intrinsics.checkNotNullParameter(storyDirectReplyRepository, "repository");
        this.storyId = j;
        this.groupDirectReplyRecipientId = recipientId;
        this.repository = storyDirectReplyRepository;
        Store<StoryDirectReplyState> store = new Store<>(new StoryDirectReplyState(null, null, 3, null));
        this.store = store;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        LiveData<StoryDirectReplyState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        if (recipientId != null) {
            store.update(Recipient.live(recipientId).getLiveDataResolved(), new Store.Action() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyViewModel$$ExternalSyntheticLambda0
                @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
                public final Object apply(Object obj, Object obj2) {
                    return StoryDirectReplyViewModel.m3125_init_$lambda0((Recipient) obj, (StoryDirectReplyState) obj2);
                }
            });
        }
        Disposable subscribe = storyDirectReplyRepository.getStoryPost(j).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryDirectReplyViewModel.m3126_init_$lambda2(StoryDirectReplyViewModel.this, (MessageRecord) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getStoryPost(…yRecord = record) }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final LiveData<StoryDirectReplyState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final StoryDirectReplyState m3125_init_$lambda0(Recipient recipient, StoryDirectReplyState storyDirectReplyState) {
        Intrinsics.checkNotNullExpressionValue(storyDirectReplyState, "state");
        return StoryDirectReplyState.copy$default(storyDirectReplyState, recipient, null, 2, null);
    }

    /* renamed from: _init_$lambda-2 */
    public static final void m3126_init_$lambda2(StoryDirectReplyViewModel storyDirectReplyViewModel, MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(storyDirectReplyViewModel, "this$0");
        storyDirectReplyViewModel.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StoryDirectReplyViewModel.m3127lambda2$lambda1(MessageRecord.this, (StoryDirectReplyState) obj);
            }
        });
    }

    /* renamed from: lambda-2$lambda-1 */
    public static final StoryDirectReplyState m3127lambda2$lambda1(MessageRecord messageRecord, StoryDirectReplyState storyDirectReplyState) {
        Intrinsics.checkNotNullExpressionValue(storyDirectReplyState, "it");
        return StoryDirectReplyState.copy$default(storyDirectReplyState, null, messageRecord, 1, null);
    }

    public final Completable sendReply(CharSequence charSequence) {
        Intrinsics.checkNotNullParameter(charSequence, "charSequence");
        return this.repository.send(this.storyId, this.groupDirectReplyRecipientId, charSequence, false);
    }

    public final Completable sendReaction(CharSequence charSequence) {
        Intrinsics.checkNotNullParameter(charSequence, "emoji");
        return this.repository.send(this.storyId, this.groupDirectReplyRecipientId, charSequence, true);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        this.disposables.clear();
    }

    /* compiled from: StoryDirectReplyViewModel.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ%\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016¢\u0006\u0002\u0010\u000eR\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "storyId", "", "groupDirectReplyRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyRepository;", "(JLorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/stories/viewer/reply/direct/StoryDirectReplyRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final RecipientId groupDirectReplyRecipientId;
        private final StoryDirectReplyRepository repository;
        private final long storyId;

        public Factory(long j, RecipientId recipientId, StoryDirectReplyRepository storyDirectReplyRepository) {
            Intrinsics.checkNotNullParameter(storyDirectReplyRepository, "repository");
            this.storyId = j;
            this.groupDirectReplyRecipientId = recipientId;
            this.repository = storyDirectReplyRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StoryDirectReplyViewModel(this.storyId, this.groupDirectReplyRecipientId, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.viewer.reply.direct.StoryDirectReplyViewModel.Factory.create");
        }
    }
}
