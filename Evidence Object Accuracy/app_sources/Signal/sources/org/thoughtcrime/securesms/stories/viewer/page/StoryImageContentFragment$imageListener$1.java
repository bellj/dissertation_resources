package org.thoughtcrime.securesms.stories.viewer.page;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.stories.viewer.page.StoryCache;
import org.thoughtcrime.securesms.stories.viewer.page.StoryImageContentFragment;

/* compiled from: StoryImageContentFragment.kt */
@Metadata(d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"}, d2 = {"org/thoughtcrime/securesms/stories/viewer/page/StoryImageContentFragment$imageListener$1", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryCache$Listener;", "onLoadFailed", "", "onResourceReady", "resource", "Landroid/graphics/drawable/Drawable;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryImageContentFragment$imageListener$1 implements StoryCache.Listener {
    final /* synthetic */ StoryImageContentFragment this$0;

    public StoryImageContentFragment$imageListener$1(StoryImageContentFragment storyImageContentFragment) {
        this.this$0 = storyImageContentFragment;
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.page.StoryCache.Listener
    public void onResourceReady(Drawable drawable) {
        Intrinsics.checkNotNullParameter(drawable, "resource");
        ImageView imageView = this.this$0.imageView;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("imageView");
            imageView = null;
        }
        imageView.setImageDrawable(drawable);
        this.this$0.imageState = StoryImageContentFragment.LoadState.READY;
        this.this$0.notifyListeners();
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.page.StoryCache.Listener
    public void onLoadFailed() {
        this.this$0.imageState = StoryImageContentFragment.LoadState.FAILED;
        this.this$0.notifyListeners();
    }
}
