package org.thoughtcrime.securesms.stories.settings.story;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;

/* loaded from: classes3.dex */
public class StorySettingsFragmentDirections {
    private StorySettingsFragmentDirections() {
    }

    public static NavDirections actionStorySettingsToMyStorySettings() {
        return new ActionOnlyNavDirections(R.id.action_storySettings_to_myStorySettings);
    }

    public static ActionStorySettingsToPrivateStorySettings actionStorySettingsToPrivateStorySettings(DistributionListId distributionListId) {
        return new ActionStorySettingsToPrivateStorySettings(distributionListId);
    }

    public static NavDirections actionStorySettingsToNewStory() {
        return new ActionOnlyNavDirections(R.id.action_storySettings_to_newStory);
    }

    /* loaded from: classes3.dex */
    public static class ActionStorySettingsToPrivateStorySettings implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_storySettings_to_privateStorySettings;
        }

        private ActionStorySettingsToPrivateStorySettings(DistributionListId distributionListId) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (distributionListId != null) {
                hashMap.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                return;
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        }

        public ActionStorySettingsToPrivateStorySettings setDistributionListId(DistributionListId distributionListId) {
            if (distributionListId != null) {
                this.arguments.put(RecipientDatabase.DISTRIBUTION_LIST_ID, distributionListId);
                return this;
            }
            throw new IllegalArgumentException("Argument \"distribution_list_id\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
                DistributionListId distributionListId = (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
                if (Parcelable.class.isAssignableFrom(DistributionListId.class) || distributionListId == null) {
                    bundle.putParcelable(RecipientDatabase.DISTRIBUTION_LIST_ID, (Parcelable) Parcelable.class.cast(distributionListId));
                } else if (Serializable.class.isAssignableFrom(DistributionListId.class)) {
                    bundle.putSerializable(RecipientDatabase.DISTRIBUTION_LIST_ID, (Serializable) Serializable.class.cast(distributionListId));
                } else {
                    throw new UnsupportedOperationException(DistributionListId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public DistributionListId getDistributionListId() {
            return (DistributionListId) this.arguments.get(RecipientDatabase.DISTRIBUTION_LIST_ID);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionStorySettingsToPrivateStorySettings actionStorySettingsToPrivateStorySettings = (ActionStorySettingsToPrivateStorySettings) obj;
            if (this.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID) != actionStorySettingsToPrivateStorySettings.arguments.containsKey(RecipientDatabase.DISTRIBUTION_LIST_ID)) {
                return false;
            }
            if (getDistributionListId() == null ? actionStorySettingsToPrivateStorySettings.getDistributionListId() == null : getDistributionListId().equals(actionStorySettingsToPrivateStorySettings.getDistributionListId())) {
                return getActionId() == actionStorySettingsToPrivateStorySettings.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getDistributionListId() != null ? getDistributionListId().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionStorySettingsToPrivateStorySettings(actionId=" + getActionId() + "){distributionListId=" + getDistributionListId() + "}";
        }
    }
}
