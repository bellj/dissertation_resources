package org.thoughtcrime.securesms.stories.tabs;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ConversationListTabRepository.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0003\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0002J\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004J\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabRepository;", "", "()V", "getNumberOfUnreadConversations", "Lio/reactivex/rxjava3/core/Observable;", "", "getNumberOfUnseenStories", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ConversationListTabRepository {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ConversationListTabRepository.class);

    /* compiled from: ConversationListTabRepository.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabRepository$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final Observable<Long> getNumberOfUnreadConversations() {
        Observable<Long> subscribeOn = Observable.create(new ObservableOnSubscribe() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabRepository$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                ConversationListTabRepository.m2970getNumberOfUnreadConversations$lambda2(observableEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create<Long> {\n      fun…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getNumberOfUnreadConversations$lambda-2$refresh */
    private static final void m2973getNumberOfUnreadConversations$lambda2$refresh(ObservableEmitter<Long> observableEmitter) {
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        observableEmitter.onNext(companion.threads().getUnreadThreadCount());
        String unreadThreadIdList = companion.threads().getUnreadThreadIdList();
        String str = TAG;
        Log.d(str, "Unread threads: { " + unreadThreadIdList + " }");
    }

    /* renamed from: getNumberOfUnreadConversations$lambda-2 */
    public static final void m2970getNumberOfUnreadConversations$lambda2(ObservableEmitter observableEmitter) {
        ConversationListTabRepository$$ExternalSyntheticLambda0 conversationListTabRepository$$ExternalSyntheticLambda0 = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabRepository$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                ConversationListTabRepository.m2971getNumberOfUnreadConversations$lambda2$lambda0(ObservableEmitter.this);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerConversationListObserver(conversationListTabRepository$$ExternalSyntheticLambda0);
        observableEmitter.setCancellable(new Cancellable() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                ConversationListTabRepository.m2972getNumberOfUnreadConversations$lambda2$lambda1(DatabaseObserver.Observer.this);
            }
        });
        m2973getNumberOfUnreadConversations$lambda2$refresh(observableEmitter);
    }

    /* renamed from: getNumberOfUnreadConversations$lambda-2$lambda-0 */
    public static final void m2971getNumberOfUnreadConversations$lambda2$lambda0(ObservableEmitter observableEmitter) {
        m2973getNumberOfUnreadConversations$lambda2$refresh(observableEmitter);
    }

    /* renamed from: getNumberOfUnreadConversations$lambda-2$lambda-1 */
    public static final void m2972getNumberOfUnreadConversations$lambda2$lambda1(DatabaseObserver.Observer observer) {
        Intrinsics.checkNotNullParameter(observer, "$listener");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(observer);
    }

    public final Observable<Long> getNumberOfUnseenStories() {
        Observable<Long> subscribeOn = Observable.create(new ObservableOnSubscribe() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabRepository$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                ConversationListTabRepository.m2974getNumberOfUnseenStories$lambda8(observableEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create<Long> { emitter -…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getNumberOfUnseenStories$lambda-8$refresh-5 */
    private static final void m2977getNumberOfUnseenStories$lambda8$refresh5(ObservableEmitter<Long> observableEmitter) {
        List<RecipientId> unreadStoryThreadRecipientIds = SignalDatabase.Companion.mms().getUnreadStoryThreadRecipientIds();
        Intrinsics.checkNotNullExpressionValue(unreadStoryThreadRecipientIds, "SignalDatabase.mms.unreadStoryThreadRecipientIds");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(unreadStoryThreadRecipientIds, 10));
        for (RecipientId recipientId : unreadStoryThreadRecipientIds) {
            arrayList.add(Recipient.resolved(recipientId));
        }
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (!((Recipient) obj).shouldHideStory()) {
                arrayList2.add(obj);
            }
        }
        observableEmitter.onNext(Long.valueOf((long) arrayList2.size()));
    }

    /* renamed from: getNumberOfUnseenStories$lambda-8 */
    public static final void m2974getNumberOfUnseenStories$lambda8(ObservableEmitter observableEmitter) {
        ConversationListTabRepository$$ExternalSyntheticLambda3 conversationListTabRepository$$ExternalSyntheticLambda3 = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabRepository$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                ConversationListTabRepository.m2975getNumberOfUnseenStories$lambda8$lambda6(ObservableEmitter.this);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerConversationListObserver(conversationListTabRepository$$ExternalSyntheticLambda3);
        observableEmitter.setCancellable(new Cancellable() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabRepository$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                ConversationListTabRepository.m2976getNumberOfUnseenStories$lambda8$lambda7(DatabaseObserver.Observer.this);
            }
        });
        m2977getNumberOfUnseenStories$lambda8$refresh5(observableEmitter);
    }

    /* renamed from: getNumberOfUnseenStories$lambda-8$lambda-6 */
    public static final void m2975getNumberOfUnseenStories$lambda8$lambda6(ObservableEmitter observableEmitter) {
        m2977getNumberOfUnseenStories$lambda8$refresh5(observableEmitter);
    }

    /* renamed from: getNumberOfUnseenStories$lambda-8$lambda-7 */
    public static final void m2976getNumberOfUnseenStories$lambda8$lambda7(DatabaseObserver.Observer observer) {
        Intrinsics.checkNotNullParameter(observer, "$listener");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(observer);
    }
}
