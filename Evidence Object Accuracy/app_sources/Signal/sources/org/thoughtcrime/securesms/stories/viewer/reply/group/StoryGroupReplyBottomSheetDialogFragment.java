package org.thoughtcrime.securesms.stories.viewer.reply.group;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.math.MathKt__MathJVMKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.viewer.page.StoryViewerPageViewModel;
import org.thoughtcrime.securesms.stories.viewer.reply.BottomSheetBehaviorDelegate;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment;
import org.thoughtcrime.securesms.stories.viewer.reply.reaction.OnReactionSentView;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: StoryGroupReplyBottomSheetDialogFragment.kt */
@Metadata(d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\u0018\u0000 :2\u00020\u00012\u00020\u0002:\u0001:B\u0005¢\u0006\u0002\u0010\u0003J&\u0010%\u001a\u0004\u0018\u00010&2\u0006\u0010'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010*2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\u0010\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u000200H\u0016J\u0010\u00101\u001a\u00020.2\u0006\u00102\u001a\u000203H\u0016J\u0010\u00104\u001a\u00020.2\u0006\u00105\u001a\u00020\u0005H\u0016J\u001a\u00106\u001a\u00020.2\u0006\u00107\u001a\u00020&2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\u0010\u00108\u001a\u00020.2\u0006\u00109\u001a\u00020\u000eH\u0016R\u0014\u0010\u0004\u001a\u00020\u00058BX\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\u00020\t8BX\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u00020\u000e8BX\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u00020\u0013XD¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u000eX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u00020\u001a8BX\u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001cR\u001b\u0010\u001d\u001a\u00020\u001e8BX\u0002¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b\u001f\u0010 R\u0014\u0010#\u001a\u00020\t8TX\u0004¢\u0006\u0006\u001a\u0004\b$\u0010\u000b¨\u0006;"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/components/FixedRoundedCornerBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyFragment$Callback;", "()V", "groupRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getGroupRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "groupReplyStartPosition", "", "getGroupReplyStartPosition", "()I", "initialParentHeight", "isFromNotification", "", "()Z", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "peekHeightPercentage", "", "getPeekHeightPercentage", "()F", "reactionView", "Lorg/thoughtcrime/securesms/stories/viewer/reply/reaction/OnReactionSentView;", "shouldShowFullScreen", "storyId", "", "getStoryId", "()J", "storyViewerPageViewModel", "Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "getStoryViewerPageViewModel", "()Lorg/thoughtcrime/securesms/stories/viewer/page/StoryViewerPageViewModel;", "storyViewerPageViewModel$delegate", "Lkotlin/Lazy;", "themeResId", "getThemeResId", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDismiss", "", "dialog", "Landroid/content/DialogInterface;", "onReactionEmojiSelected", "emoji", "", "onStartDirectReply", "recipientId", "onViewCreated", "view", "requestFullScreen", "fullscreen", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryGroupReplyBottomSheetDialogFragment extends FixedRoundedCornerBottomSheetDialogFragment implements StoryGroupReplyFragment.Callback {
    private static final String ARG_GROUP_RECIPIENT_ID;
    private static final String ARG_GROUP_REPLY_START_POSITION;
    private static final String ARG_IS_FROM_NOTIFICATION;
    private static final String ARG_STORY_ID;
    public static final Companion Companion = new Companion(null);
    private int initialParentHeight;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final float peekHeightPercentage = 1.0f;
    private OnReactionSentView reactionView;
    private boolean shouldShowFullScreen;
    private final Lazy storyViewerPageViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(StoryViewerPageViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyBottomSheetDialogFragment$storyViewerPageViewModel$2
        final /* synthetic */ StoryGroupReplyBottomSheetDialogFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            Fragment requireParentFragment = this.this$0.requireParentFragment();
            Intrinsics.checkNotNullExpressionValue(requireParentFragment, "requireParentFragment()");
            return requireParentFragment;
        }
    }) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyBottomSheetDialogFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    public int getThemeResId() {
        return R.style.Widget_Signal_FixedRoundedCorners_Stories;
    }

    private final long getStoryId() {
        return requireArguments().getLong(ARG_STORY_ID);
    }

    private final RecipientId getGroupRecipientId() {
        Parcelable parcelable = requireArguments().getParcelable(ARG_GROUP_RECIPIENT_ID);
        Intrinsics.checkNotNull(parcelable);
        return (RecipientId) parcelable;
    }

    private final boolean isFromNotification() {
        return requireArguments().getBoolean(ARG_IS_FROM_NOTIFICATION, false);
    }

    private final int getGroupReplyStartPosition() {
        return requireArguments().getInt(ARG_GROUP_REPLY_START_POSITION, -1);
    }

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    protected float getPeekHeightPercentage() {
        return this.peekHeightPercentage;
    }

    private final StoryViewerPageViewModel getStoryViewerPageViewModel() {
        return (StoryViewerPageViewModel) this.storyViewerPageViewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.bottom_sheet_container, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        if (bundle == null) {
            getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, StoryGroupReplyFragment.Companion.create(getStoryId(), getGroupRecipientId(), isFromNotification(), getGroupReplyStartPosition())).commitAllowingStateLoss();
        }
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        OnReactionSentView onReactionSentView = null;
        this.reactionView = new OnReactionSentView(requireContext, null, 2, null);
        FrameLayout frameLayout = (FrameLayout) view.getRootView().findViewById(R.id.container);
        OnReactionSentView onReactionSentView2 = this.reactionView;
        if (onReactionSentView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("reactionView");
        } else {
            onReactionSentView = onReactionSentView2;
        }
        frameLayout.addView(onReactionSentView);
        BottomSheetBehavior<FrameLayout> behavior = ((BottomSheetDialog) requireDialog()).getBehavior();
        Intrinsics.checkNotNullExpressionValue(behavior, "requireDialog() as BottomSheetDialog).behavior");
        behavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyBottomSheetDialogFragment$onViewCreated$1
            final /* synthetic */ StoryGroupReplyBottomSheetDialogFragment this$0;

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onStateChanged(View view2, int i) {
                Intrinsics.checkNotNullParameter(view2, "bottomSheet");
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onSlide(View view2, float f) {
                Intrinsics.checkNotNullParameter(view2, "bottomSheet");
                List<Fragment> fragments = this.this$0.getChildFragmentManager().getFragments();
                Intrinsics.checkNotNullExpressionValue(fragments, "childFragmentManager.fragments");
                for (Fragment fragment : fragments) {
                    if (fragment instanceof BottomSheetBehaviorDelegate) {
                        ((BottomSheetBehaviorDelegate) fragment).onSlide(view2);
                    }
                }
            }
        });
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(view) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyBottomSheetDialogFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                StoryGroupReplyBottomSheetDialogFragment.m3129onViewCreated$lambda1(StoryGroupReplyBottomSheetDialogFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m3129onViewCreated$lambda1(StoryGroupReplyBottomSheetDialogFragment storyGroupReplyBottomSheetDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(storyGroupReplyBottomSheetDialogFragment, "this$0");
        Intrinsics.checkNotNullParameter(view, "$view");
        int height = BottomSheetUtil.INSTANCE.requireCoordinatorLayout(storyGroupReplyBottomSheetDialogFragment).getHeight();
        int i = MathKt__MathJVMKt.roundToInt(((float) storyGroupReplyBottomSheetDialogFragment.getResources().getDisplayMetrics().heightPixels) * 0.6f);
        if (storyGroupReplyBottomSheetDialogFragment.initialParentHeight == 0) {
            storyGroupReplyBottomSheetDialogFragment.initialParentHeight = height;
        }
        if (height == 0) {
            height = i;
        } else if (!storyGroupReplyBottomSheetDialogFragment.shouldShowFullScreen && height == storyGroupReplyBottomSheetDialogFragment.initialParentHeight) {
            height = Math.min(height, i);
        }
        if (view.getHeight() != height) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams != null) {
                layoutParams.height = height;
                view.setLayoutParams(layoutParams);
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup.LayoutParams");
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        getStoryViewerPageViewModel().setIsDisplayingViewsAndRepliesDialog(false);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.Callback
    public void onStartDirectReply(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        dismiss();
        getStoryViewerPageViewModel().startDirectReply(getStoryId(), recipientId);
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.Callback
    public void requestFullScreen(boolean z) {
        this.shouldShowFullScreen = z;
        requireView().invalidate();
    }

    @Override // org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyFragment.Callback
    public void onReactionEmojiSelected(String str) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        OnReactionSentView onReactionSentView = this.reactionView;
        if (onReactionSentView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("reactionView");
            onReactionSentView = null;
        }
        onReactionSentView.playForEmoji(str);
    }

    /* compiled from: StoryGroupReplyBottomSheetDialogFragment.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyBottomSheetDialogFragment$Companion;", "", "()V", "ARG_GROUP_RECIPIENT_ID", "", "ARG_GROUP_REPLY_START_POSITION", "ARG_IS_FROM_NOTIFICATION", "ARG_STORY_ID", "create", "Landroidx/fragment/app/DialogFragment;", "storyId", "", "groupRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "isFromNotification", "", "groupReplyStartPosition", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final DialogFragment create(long j, RecipientId recipientId, boolean z, int i) {
            Intrinsics.checkNotNullParameter(recipientId, "groupRecipientId");
            StoryGroupReplyBottomSheetDialogFragment storyGroupReplyBottomSheetDialogFragment = new StoryGroupReplyBottomSheetDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(StoryGroupReplyBottomSheetDialogFragment.ARG_STORY_ID, j);
            bundle.putParcelable(StoryGroupReplyBottomSheetDialogFragment.ARG_GROUP_RECIPIENT_ID, recipientId);
            bundle.putBoolean(StoryGroupReplyBottomSheetDialogFragment.ARG_IS_FROM_NOTIFICATION, z);
            bundle.putInt(StoryGroupReplyBottomSheetDialogFragment.ARG_GROUP_REPLY_START_POSITION, i);
            storyGroupReplyBottomSheetDialogFragment.setArguments(bundle);
            return storyGroupReplyBottomSheetDialogFragment;
        }
    }
}
