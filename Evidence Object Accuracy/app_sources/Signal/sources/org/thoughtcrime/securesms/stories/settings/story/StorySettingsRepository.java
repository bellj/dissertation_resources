package org.thoughtcrime.securesms.stories.settings.story;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListPartialRecord;

/* compiled from: StorySettingsRepository.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsRepository;", "", "()V", "getPrivateStories", "Lio/reactivex/rxjava3/core/Single;", "", "Lorg/thoughtcrime/securesms/database/model/DistributionListPartialRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StorySettingsRepository {
    public final Single<List<DistributionListPartialRecord>> getPrivateStories() {
        Single<List<DistributionListPartialRecord>> subscribeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.stories.settings.story.StorySettingsRepository$$ExternalSyntheticLambda0
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return StorySettingsRepository.m2963getPrivateStories$lambda0();
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      Sig…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getPrivateStories$lambda-0 */
    public static final List m2963getPrivateStories$lambda0() {
        return SignalDatabase.Companion.distributionLists().getCustomListsForUi();
    }
}
