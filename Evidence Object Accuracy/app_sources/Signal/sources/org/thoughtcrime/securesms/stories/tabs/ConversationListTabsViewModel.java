package org.thoughtcrime.securesms.stories.tabs;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Predicate;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.tabs.ConversationListTabsState;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: ConversationListTabsViewModel.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\u0010\u000b\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0001$B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001a\u001a\u00020\u001cJ\u0006\u0010\u001d\u001a\u00020\u001bJ\b\u0010\u001e\u001a\u00020\u001bH\u0014J\u0006\u0010\u001f\u001a\u00020\u001bJ\u0006\u0010 \u001a\u00020\u001bJ\u0006\u0010!\u001a\u00020\u001bJ\u0006\u0010\"\u001a\u00020\u001bJ\u0006\u0010#\u001a\u00020\u001bR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0015X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0017¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019¨\u0006%"}, d2 = {"Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabRepository;", "(Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "getDisposables", "()Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "internalTabClickEvents", "Lio/reactivex/rxjava3/subjects/Subject;", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTab;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsState;", "getState", "()Landroidx/lifecycle/LiveData;", "stateSnapshot", "getStateSnapshot", "()Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsState;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "tabClickEvents", "Lio/reactivex/rxjava3/core/Observable;", "getTabClickEvents", "()Lio/reactivex/rxjava3/core/Observable;", "isShowingArchived", "", "", "onChatsSelected", "onCleared", "onMultiSelectFinished", "onMultiSelectStarted", "onSearchClosed", "onSearchOpened", "onStoriesSelected", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ConversationListTabsViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final Subject<ConversationListTab> internalTabClickEvents;
    private final LiveData<ConversationListTabsState> state;
    private final ConversationListTabsState stateSnapshot;
    private final Store<ConversationListTabsState> store;
    private final Observable<ConversationListTab> tabClickEvents;

    /* JADX DEBUG: Type inference failed for r1v5. Raw type applied. Possible types: io.reactivex.rxjava3.core.Observable<T>, java.lang.Object, io.reactivex.rxjava3.core.Observable<org.thoughtcrime.securesms.stories.tabs.ConversationListTab> */
    public ConversationListTabsViewModel(ConversationListTabRepository conversationListTabRepository) {
        Intrinsics.checkNotNullParameter(conversationListTabRepository, "repository");
        Store<ConversationListTabsState> store = new Store<>(new ConversationListTabsState(null, 0, 0, null, 15, null));
        this.store = store;
        ConversationListTabsState state = store.getState();
        Intrinsics.checkNotNullExpressionValue(state, "store.state");
        this.stateSnapshot = state;
        LiveData<ConversationListTabsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        PublishSubject create = PublishSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.internalTabClickEvents = create;
        Observable filter = create.filter(new Predicate() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Predicate
            public final boolean test(Object obj) {
                return ConversationListTabsViewModel.m3000tabClickEvents$lambda0((ConversationListTab) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(filter, "internalTabClickEvents.f…ries.isFeatureEnabled() }");
        this.tabClickEvents = filter;
        Disposable subscribe = conversationListTabRepository.getNumberOfUnreadConversations().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationListTabsViewModel.m2989_init_$lambda2(ConversationListTabsViewModel.this, (Long) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getNumberOfUn…nt = unreadChats) }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
        Disposable subscribe2 = conversationListTabRepository.getNumberOfUnseenStories().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationListTabsViewModel.m2990_init_$lambda4(ConversationListTabsViewModel.this, (Long) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "repository.getNumberOfUn… = unseenStories) }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe2);
    }

    public final ConversationListTabsState getStateSnapshot() {
        return this.stateSnapshot;
    }

    public final LiveData<ConversationListTabsState> getState() {
        return this.state;
    }

    public final CompositeDisposable getDisposables() {
        return this.disposables;
    }

    /* renamed from: tabClickEvents$lambda-0 */
    public static final boolean m3000tabClickEvents$lambda0(ConversationListTab conversationListTab) {
        return Stories.isFeatureEnabled();
    }

    public final Observable<ConversationListTab> getTabClickEvents() {
        return this.tabClickEvents;
    }

    /* renamed from: _init_$lambda-2 */
    public static final void m2989_init_$lambda2(ConversationListTabsViewModel conversationListTabsViewModel, Long l) {
        Intrinsics.checkNotNullParameter(conversationListTabsViewModel, "this$0");
        conversationListTabsViewModel.store.update(new Function(l) { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda11
            public final /* synthetic */ Long f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationListTabsViewModel.m2992lambda2$lambda1(this.f$0, (ConversationListTabsState) obj);
            }
        });
    }

    /* renamed from: lambda-2$lambda-1 */
    public static final ConversationListTabsState m2992lambda2$lambda1(Long l, ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "it");
        Intrinsics.checkNotNullExpressionValue(l, "unreadChats");
        return ConversationListTabsState.copy$default(conversationListTabsState, null, l.longValue(), 0, null, 13, null);
    }

    /* renamed from: _init_$lambda-4 */
    public static final void m2990_init_$lambda4(ConversationListTabsViewModel conversationListTabsViewModel, Long l) {
        Intrinsics.checkNotNullParameter(conversationListTabsViewModel, "this$0");
        conversationListTabsViewModel.store.update(new Function(l) { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda10
            public final /* synthetic */ Long f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationListTabsViewModel.m2993lambda4$lambda3(this.f$0, (ConversationListTabsState) obj);
            }
        });
    }

    /* renamed from: lambda-4$lambda-3 */
    public static final ConversationListTabsState m2993lambda4$lambda3(Long l, ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "it");
        Intrinsics.checkNotNullExpressionValue(l, "unseenStories");
        return ConversationListTabsState.copy$default(conversationListTabsState, null, 0, l.longValue(), null, 11, null);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void onChatsSelected() {
        this.internalTabClickEvents.onNext(ConversationListTab.CHATS);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationListTabsViewModel.m2994onChatsSelected$lambda5((ConversationListTabsState) obj);
            }
        });
    }

    /* renamed from: onChatsSelected$lambda-5 */
    public static final ConversationListTabsState m2994onChatsSelected$lambda5(ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "it");
        return ConversationListTabsState.copy$default(conversationListTabsState, ConversationListTab.CHATS, 0, 0, null, 14, null);
    }

    public final void onStoriesSelected() {
        this.internalTabClickEvents.onNext(ConversationListTab.STORIES);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda9
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationListTabsViewModel.m2999onStoriesSelected$lambda6((ConversationListTabsState) obj);
            }
        });
    }

    /* renamed from: onStoriesSelected$lambda-6 */
    public static final ConversationListTabsState m2999onStoriesSelected$lambda6(ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "it");
        return ConversationListTabsState.copy$default(conversationListTabsState, ConversationListTab.STORIES, 0, 0, null, 14, null);
    }

    /* renamed from: onSearchOpened$lambda-7 */
    public static final ConversationListTabsState m2998onSearchOpened$lambda7(ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "it");
        return ConversationListTabsState.copy$default(conversationListTabsState, null, 0, 0, ConversationListTabsState.VisibilityState.copy$default(conversationListTabsState.getVisibilityState(), true, false, false, 6, null), 7, null);
    }

    public final void onSearchOpened() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationListTabsViewModel.m2998onSearchOpened$lambda7((ConversationListTabsState) obj);
            }
        });
    }

    /* renamed from: onSearchClosed$lambda-8 */
    public static final ConversationListTabsState m2997onSearchClosed$lambda8(ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "it");
        return ConversationListTabsState.copy$default(conversationListTabsState, null, 0, 0, ConversationListTabsState.VisibilityState.copy$default(conversationListTabsState.getVisibilityState(), false, false, false, 6, null), 7, null);
    }

    public final void onSearchClosed() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationListTabsViewModel.m2997onSearchClosed$lambda8((ConversationListTabsState) obj);
            }
        });
    }

    /* renamed from: onMultiSelectStarted$lambda-9 */
    public static final ConversationListTabsState m2996onMultiSelectStarted$lambda9(ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "it");
        return ConversationListTabsState.copy$default(conversationListTabsState, null, 0, 0, ConversationListTabsState.VisibilityState.copy$default(conversationListTabsState.getVisibilityState(), false, true, false, 5, null), 7, null);
    }

    public final void onMultiSelectStarted() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationListTabsViewModel.m2996onMultiSelectStarted$lambda9((ConversationListTabsState) obj);
            }
        });
    }

    /* renamed from: onMultiSelectFinished$lambda-10 */
    public static final ConversationListTabsState m2995onMultiSelectFinished$lambda10(ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "it");
        return ConversationListTabsState.copy$default(conversationListTabsState, null, 0, 0, ConversationListTabsState.VisibilityState.copy$default(conversationListTabsState.getVisibilityState(), false, false, false, 5, null), 7, null);
    }

    public final void onMultiSelectFinished() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationListTabsViewModel.m2995onMultiSelectFinished$lambda10((ConversationListTabsState) obj);
            }
        });
    }

    /* renamed from: isShowingArchived$lambda-11 */
    public static final ConversationListTabsState m2991isShowingArchived$lambda11(boolean z, ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "it");
        return ConversationListTabsState.copy$default(conversationListTabsState, null, 0, 0, ConversationListTabsState.VisibilityState.copy$default(conversationListTabsState.getVisibilityState(), false, false, z, 3, null), 7, null);
    }

    public final void isShowingArchived(boolean z) {
        this.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel$$ExternalSyntheticLambda7
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationListTabsViewModel.m2991isShowingArchived$lambda11(this.f$0, (ConversationListTabsState) obj);
            }
        });
    }

    /* compiled from: ConversationListTabsViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabRepository;", "(Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final ConversationListTabRepository repository;

        public Factory(ConversationListTabRepository conversationListTabRepository) {
            Intrinsics.checkNotNullParameter(conversationListTabRepository, "repository");
            this.repository = conversationListTabRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new ConversationListTabsViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel.Factory.create");
        }
    }
}
