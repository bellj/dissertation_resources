package org.thoughtcrime.securesms.stories.my;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import j$.util.function.Function;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.SignalContextMenu;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.stories.StoryTextPostModel;
import org.thoughtcrime.securesms.stories.my.MyStoriesItem;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: MyStoriesItem.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\t\nB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesItem;", "", "()V", "STATUS_CHANGE", "", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class MyStoriesItem {
    public static final MyStoriesItem INSTANCE = new MyStoriesItem();
    private static final int STATUS_CHANGE;

    private MyStoriesItem() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.my.MyStoriesItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new MyStoriesItem.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_my_stories_item));
    }

    /* compiled from: MyStoriesItem.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0015\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B¥\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0018\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u0012\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\n0\t\u0012\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00070\t\u0012\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00070\t\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00070\t\u0012\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00070\t\u0012\u0018\u0010\u000f\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\u0002\u0010\u0010J\u0010\u0010\u001c\u001a\u00020\n2\u0006\u0010\u001d\u001a\u00020\u0000H\u0016J\u0010\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001d\u001a\u00020\u0000H\u0016J\u0012\u0010\u001f\u001a\u0004\u0018\u00010 2\u0006\u0010\u001d\u001a\u00020\u0000H\u0016J\u0010\u0010!\u001a\u00020\n2\u0006\u0010\u001d\u001a\u00020\u0000H\u0002J\u0010\u0010\"\u001a\u00020\n2\u0006\u0010\u001d\u001a\u00020\u0000H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R#\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001d\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00070\t¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u001d\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00070\t¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0016R#\u0010\u000f\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0014R\u001d\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0016R\u001d\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00070\t¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u001d\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00070\t¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0016¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesItem$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "distributionStory", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "onClick", "Lkotlin/Function2;", "Landroid/view/View;", "", "onLongClick", "Lkotlin/Function1;", "", "onSaveClick", "onDeleteClick", "onForwardClick", "onShareClick", "onInfoClick", "(Lorg/thoughtcrime/securesms/conversation/ConversationMessage;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V", "getDistributionStory", "()Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "getOnClick", "()Lkotlin/jvm/functions/Function2;", "getOnDeleteClick", "()Lkotlin/jvm/functions/Function1;", "getOnForwardClick", "getOnInfoClick", "getOnLongClick", "getOnSaveClick", "getOnShareClick", "areContentsTheSame", "newItem", "areItemsTheSame", "getChangePayload", "", "hasStatusChange", "isSameRecord", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final ConversationMessage distributionStory;
        private final Function2<Model, View, Unit> onClick;
        private final Function1<Model, Unit> onDeleteClick;
        private final Function1<Model, Unit> onForwardClick;
        private final Function2<Model, View, Unit> onInfoClick;
        private final Function1<Model, Boolean> onLongClick;
        private final Function1<Model, Unit> onSaveClick;
        private final Function1<Model, Unit> onShareClick;

        public final ConversationMessage getDistributionStory() {
            return this.distributionStory;
        }

        public final Function2<Model, View, Unit> getOnClick() {
            return this.onClick;
        }

        public final Function1<Model, Boolean> getOnLongClick() {
            return this.onLongClick;
        }

        public final Function1<Model, Unit> getOnSaveClick() {
            return this.onSaveClick;
        }

        public final Function1<Model, Unit> getOnDeleteClick() {
            return this.onDeleteClick;
        }

        public final Function1<Model, Unit> getOnForwardClick() {
            return this.onForwardClick;
        }

        public final Function1<Model, Unit> getOnShareClick() {
            return this.onShareClick;
        }

        public final Function2<Model, View, Unit> getOnInfoClick() {
            return this.onInfoClick;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r19v0, resolved type: kotlin.jvm.functions.Function2<? super org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model, ? super android.view.View, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r20v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model, java.lang.Boolean> */
        /* JADX DEBUG: Multi-variable search result rejected for r21v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r22v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r23v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r24v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r25v0, resolved type: kotlin.jvm.functions.Function2<? super org.thoughtcrime.securesms.stories.my.MyStoriesItem$Model, ? super android.view.View, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(ConversationMessage conversationMessage, Function2<? super Model, ? super View, Unit> function2, Function1<? super Model, Boolean> function1, Function1<? super Model, Unit> function12, Function1<? super Model, Unit> function13, Function1<? super Model, Unit> function14, Function1<? super Model, Unit> function15, Function2<? super Model, ? super View, Unit> function22) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(conversationMessage, "distributionStory");
            Intrinsics.checkNotNullParameter(function2, "onClick");
            Intrinsics.checkNotNullParameter(function1, "onLongClick");
            Intrinsics.checkNotNullParameter(function12, "onSaveClick");
            Intrinsics.checkNotNullParameter(function13, "onDeleteClick");
            Intrinsics.checkNotNullParameter(function14, "onForwardClick");
            Intrinsics.checkNotNullParameter(function15, "onShareClick");
            Intrinsics.checkNotNullParameter(function22, "onInfoClick");
            this.distributionStory = conversationMessage;
            this.onClick = function2;
            this.onLongClick = function1;
            this.onSaveClick = function12;
            this.onDeleteClick = function13;
            this.onForwardClick = function14;
            this.onShareClick = function15;
            this.onInfoClick = function22;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return this.distributionStory.getMessageRecord().getId() == model.distributionStory.getMessageRecord().getId();
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.distributionStory, model.distributionStory) && !hasStatusChange(model) && this.distributionStory.getMessageRecord().getViewedReceiptCount() == model.distributionStory.getMessageRecord().getViewedReceiptCount() && super.areContentsTheSame(model);
        }

        public Object getChangePayload(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return (!isSameRecord(model) || !hasStatusChange(model)) ? null : 0;
        }

        private final boolean isSameRecord(Model model) {
            return this.distributionStory.getMessageRecord().getId() == model.distributionStory.getMessageRecord().getId();
        }

        private final boolean hasStatusChange(Model model) {
            MessageRecord messageRecord = this.distributionStory.getMessageRecord();
            Intrinsics.checkNotNullExpressionValue(messageRecord, "distributionStory.messageRecord");
            MessageRecord messageRecord2 = model.distributionStory.getMessageRecord();
            Intrinsics.checkNotNullExpressionValue(messageRecord2, "newItem.distributionStory.messageRecord");
            return messageRecord.isOutgoing() && messageRecord2.isOutgoing() && !(messageRecord.isPending() == messageRecord2.isPending() && messageRecord.isSent() == messageRecord2.isSent() && messageRecord.isFailed() == messageRecord2.isFailed());
        }
    }

    /* compiled from: MyStoriesItem.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0016J\b\u0010\u0012\u001a\u00020\u0010H\u0002J\u0010\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J\u0010\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/stories/my/MyStoriesItem$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "date", "Landroid/widget/TextView;", "downloadTarget", "errorIndicator", "moreTarget", "storyBlur", "Landroid/widget/ImageView;", "storyPreview", "viewCount", "bind", "", "model", "clearGlide", "presentDateOrStatus", "showContextMenu", "HideBlurAfterLoadListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final TextView date;
        private final View downloadTarget;
        private final View errorIndicator;
        private final View moreTarget;
        private final ImageView storyBlur;
        private final ImageView storyPreview;
        private final TextView viewCount;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.download_touch);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.download_touch)");
            this.downloadTarget = findViewById;
            View findViewById2 = view.findViewById(R.id.more_touch);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.more_touch)");
            this.moreTarget = findViewById2;
            View findViewById3 = view.findViewById(R.id.story);
            ImageView imageView = (ImageView) findViewById3;
            imageView.setClickable(false);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById<Im…isClickable = false\n    }");
            this.storyPreview = imageView;
            View findViewById4 = view.findViewById(R.id.story_blur);
            ImageView imageView2 = (ImageView) findViewById4;
            imageView2.setClickable(false);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "itemView.findViewById<Im…isClickable = false\n    }");
            this.storyBlur = imageView2;
            View findViewById5 = view.findViewById(R.id.view_count);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "itemView.findViewById(R.id.view_count)");
            this.viewCount = (TextView) findViewById5;
            View findViewById6 = view.findViewById(R.id.date);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "itemView.findViewById(R.id.date)");
            this.date = (TextView) findViewById6;
            View findViewById7 = view.findViewById(R.id.error_indicator);
            Intrinsics.checkNotNullExpressionValue(findViewById7, "itemView.findViewById(R.id.error_indicator)");
            this.errorIndicator = findViewById7;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            boolean z = false;
            this.storyPreview.setClickable(false);
            this.itemView.setOnClickListener(new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda5(model, this));
            this.itemView.setOnLongClickListener(new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda6(model));
            this.downloadTarget.setOnClickListener(new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda7(model));
            this.moreTarget.setOnClickListener(new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda8(this, model));
            presentDateOrStatus(model);
            if (model.getDistributionStory().getMessageRecord().isSent()) {
                if (TextSecurePreferences.isReadReceiptsEnabled(this.context)) {
                    this.viewCount.setText(this.context.getResources().getQuantityString(R.plurals.MyStories__d_views, model.getDistributionStory().getMessageRecord().getViewedReceiptCount(), Integer.valueOf(model.getDistributionStory().getMessageRecord().getViewedReceiptCount())));
                } else {
                    this.viewCount.setText(R.string.StoryViewerPageFragment__views_off);
                }
            }
            if (!this.payload.contains(0)) {
                MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) model.getDistributionStory().getMessageRecord();
                Slide thumbnailSlide = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
                BlurHash blurHash = null;
                Uri uri = thumbnailSlide != null ? thumbnailSlide.getUri() : null;
                Slide thumbnailSlide2 = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
                if (thumbnailSlide2 != null) {
                    blurHash = thumbnailSlide2.getPlaceholderBlur();
                }
                clearGlide();
                ViewExtensionsKt.setVisible(this.storyBlur, blurHash != null);
                if (blurHash != null) {
                    GlideApp.with(this.storyBlur).load((Object) blurHash).into(this.storyBlur);
                }
                if (mmsMessageRecord.getStoryType().isTextStory()) {
                    ViewExtensionsKt.setVisible(this.storyBlur, false);
                    StoryTextPostModel parseFrom = StoryTextPostModel.CREATOR.parseFrom(mmsMessageRecord);
                    GlideApp.with(this.storyPreview).load((Object) parseFrom).placeholder(parseFrom.getPlaceholder()).centerCrop().dontAnimate().into(this.storyPreview);
                } else if (uri != null) {
                    ImageView imageView = this.storyBlur;
                    if (blurHash != null) {
                        z = true;
                    }
                    ViewExtensionsKt.setVisible(imageView, z);
                    GlideApp.with(this.storyPreview).load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).addListener((RequestListener<Drawable>) new HideBlurAfterLoadListener()).centerCrop().dontAnimate().into(this.storyPreview);
                }
            }
        }

        /* renamed from: bind$lambda-2 */
        public static final void m2840bind$lambda2(Model model, ViewHolder viewHolder, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            model.getOnClick().invoke(model, viewHolder.storyPreview);
        }

        /* renamed from: bind$lambda-3 */
        public static final boolean m2841bind$lambda3(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            return model.getOnLongClick().invoke(model).booleanValue();
        }

        /* renamed from: bind$lambda-4 */
        public static final void m2842bind$lambda4(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnSaveClick().invoke(model);
        }

        /* renamed from: bind$lambda-5 */
        public static final void m2843bind$lambda5(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            viewHolder.showContextMenu(model);
        }

        private final void presentDateOrStatus(Model model) {
            if (model.getDistributionStory().getMessageRecord().isPending() || model.getDistributionStory().getMessageRecord().isMediaPending()) {
                ViewExtensionsKt.setVisible(this.errorIndicator, false);
                ViewExtensionsKt.setVisible(this.date, false);
                this.viewCount.setText(R.string.StoriesLandingItem__sending);
            } else if (model.getDistributionStory().getMessageRecord().isFailed()) {
                ViewExtensionsKt.setVisible(this.errorIndicator, true);
                ViewExtensionsKt.setVisible(this.date, true);
                this.viewCount.setText(R.string.StoriesLandingItem__send_failed);
                this.date.setText(R.string.StoriesLandingItem__tap_to_retry);
            } else {
                ViewExtensionsKt.setVisible(this.errorIndicator, false);
                ViewExtensionsKt.setVisible(this.date, true);
                this.date.setText(DateUtils.getBriefRelativeTimeSpanString(this.context, Locale.getDefault(), model.getDistributionStory().getMessageRecord().getDateSent()));
            }
        }

        private final void showContextMenu(Model model) {
            View view = this.itemView;
            Intrinsics.checkNotNullExpressionValue(view, "itemView");
            View rootView = this.itemView.getRootView();
            if (rootView != null) {
                SignalContextMenu.Builder preferredHorizontalPosition = new SignalContextMenu.Builder(view, (ViewGroup) rootView).preferredHorizontalPosition(SignalContextMenu.HorizontalPosition.END);
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                SignalContextMenu.Builder offsetY = preferredHorizontalPosition.offsetX((int) dimensionUnit.toPixels(16.0f)).offsetY((int) dimensionUnit.toPixels(12.0f));
                String string = this.context.getString(R.string.delete);
                Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.string.delete)");
                String string2 = this.context.getString(R.string.save);
                Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.string.save)");
                String string3 = this.context.getString(R.string.MyStories_forward);
                Intrinsics.checkNotNullExpressionValue(string3, "context.getString(R.string.MyStories_forward)");
                String string4 = this.context.getString(R.string.StoriesLandingItem__share);
                Intrinsics.checkNotNullExpressionValue(string4, "context.getString(R.stri…toriesLandingItem__share)");
                String string5 = this.context.getString(R.string.StoriesLandingItem__info);
                Intrinsics.checkNotNullExpressionValue(string5, "context.getString(R.stri…StoriesLandingItem__info)");
                offsetY.show(CollectionsKt__CollectionsKt.listOf((Object[]) new ActionItem[]{new ActionItem(R.drawable.ic_delete_24_tinted, string, 0, new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda0(model), 4, null), new ActionItem(R.drawable.ic_download_24_tinted, string2, 0, new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda1(model), 4, null), new ActionItem(R.drawable.ic_forward_24_tinted, string3, 0, new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda2(model), 4, null), new ActionItem(R.drawable.ic_share_24_tinted, string4, 0, new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda3(model), 4, null), new ActionItem(R.drawable.ic_info_outline_message_details_24, string5, 0, new MyStoriesItem$ViewHolder$$ExternalSyntheticLambda4(model, this), 4, null)}));
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
        }

        /* renamed from: showContextMenu$lambda-6 */
        public static final void m2845showContextMenu$lambda6(Model model) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnDeleteClick().invoke(model);
        }

        /* renamed from: showContextMenu$lambda-7 */
        public static final void m2846showContextMenu$lambda7(Model model) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnSaveClick().invoke(model);
        }

        /* renamed from: showContextMenu$lambda-8 */
        public static final void m2847showContextMenu$lambda8(Model model) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnForwardClick().invoke(model);
        }

        /* renamed from: showContextMenu$lambda-9 */
        public static final void m2848showContextMenu$lambda9(Model model) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnShareClick().invoke(model);
        }

        /* renamed from: showContextMenu$lambda-10 */
        public static final void m2844showContextMenu$lambda10(Model model, ViewHolder viewHolder) {
            Intrinsics.checkNotNullParameter(model, "$model");
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            model.getOnInfoClick().invoke(model, viewHolder.storyPreview);
        }

        /* compiled from: MyStoriesItem.kt */
        @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J4\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\u0010\b\u001a\u0004\u0018\u00010\t2\u000e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\u0005H\u0016J>\u0010\r\u001a\u00020\u00052\b\u0010\u000e\u001a\u0004\u0018\u00010\u00022\b\u0010\b\u001a\u0004\u0018\u00010\t2\u000e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\f\u001a\u00020\u0005H\u0016¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/stories/my/MyStoriesItem$ViewHolder$HideBlurAfterLoadListener;", "Lcom/bumptech/glide/request/RequestListener;", "Landroid/graphics/drawable/Drawable;", "(Lorg/thoughtcrime/securesms/stories/my/MyStoriesItem$ViewHolder;)V", "onLoadFailed", "", "e", "Lcom/bumptech/glide/load/engine/GlideException;", "model", "", "target", "Lcom/bumptech/glide/request/target/Target;", "isFirstResource", "onResourceReady", "resource", "dataSource", "Lcom/bumptech/glide/load/DataSource;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public final class HideBlurAfterLoadListener implements RequestListener<Drawable> {
            @Override // com.bumptech.glide.request.RequestListener
            public boolean onLoadFailed(GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                return false;
            }

            public HideBlurAfterLoadListener() {
                ViewHolder.this = r1;
            }

            @Override // com.bumptech.glide.request.RequestListener
            public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, DataSource dataSource, boolean z) {
                return onResourceReady((Drawable) obj, obj2, (Target<Drawable>) target, dataSource, z);
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                ViewExtensionsKt.setVisible(ViewHolder.this.storyBlur, false);
                return false;
            }
        }

        private final void clearGlide() {
            GlideApp.with(this.storyPreview).clear(this.storyPreview);
            GlideApp.with(this.storyBlur).clear(this.storyBlur);
        }
    }
}
