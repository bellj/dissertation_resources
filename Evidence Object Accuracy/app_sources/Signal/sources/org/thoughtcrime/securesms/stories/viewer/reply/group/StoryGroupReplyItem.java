package org.thoughtcrime.securesms.stories.viewer.reply.group;

import android.content.Context;
import android.text.Annotation;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import j$.util.function.Function;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AlertView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.DeliveryStatusView;
import org.thoughtcrime.securesms.components.FromTextView;
import org.thoughtcrime.securesms.components.emoji.EmojiImageView;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.SignalContextMenu;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.ReactionDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.viewer.reply.group.ReplyBody;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem;
import org.thoughtcrime.securesms.util.AvatarUtil;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: StoryGroupReplyItem.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001:\b\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem;", "", "()V", "NAME_COLOR_CHANGED", "", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "resolveName", "", "context", "Landroid/content/Context;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "BaseViewHolder", "Model", "ReactionModel", "ReactionViewHolder", "RemoteDeleteModel", "RemoteDeleteViewHolder", "TextModel", "TextViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryGroupReplyItem {
    public static final StoryGroupReplyItem INSTANCE = new StoryGroupReplyItem();
    private static final int NAME_COLOR_CHANGED;

    private StoryGroupReplyItem() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(TextModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new StoryGroupReplyItem.TextViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_group_text_reply_item));
        mappingAdapter.registerFactory(ReactionModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new StoryGroupReplyItem.ReactionViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_group_text_reply_item));
        mappingAdapter.registerFactory(RemoteDeleteModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new StoryGroupReplyItem.RemoteDeleteViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.stories_group_text_reply_item));
    }

    /* compiled from: StoryGroupReplyItem.kt */
    @Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003Be\b\u0004\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u001a\u0010\b\u001a\u0016\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\tj\u0004\u0018\u0001`\f\u0012\u0016\u0010\r\u001a\u0012\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000b0\tj\u0002`\u000f\u0012\u0016\u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000b0\tj\u0002`\u0011¢\u0006\u0002\u0010\u0012J\u0015\u0010&\u001a\u00020\u00142\u0006\u0010'\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010(J\u0015\u0010)\u001a\u00020\u00142\u0006\u0010'\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010(J\u0014\u0010*\u001a\u00020\u00142\n\u0010'\u001a\u0006\u0012\u0002\b\u00030\u0000H\u0002J\u0017\u0010+\u001a\u0004\u0018\u00010\u00022\u0006\u0010'\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010,R\u0011\u0010\u0013\u001a\u00020\u0014¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u0014¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0015R\u0011\u0010\u0017\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR%\u0010\b\u001a\u0016\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\tj\u0004\u0018\u0001`\f¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR!\u0010\r\u001a\u0012\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000b0\tj\u0002`\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001dR!\u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000b0\tj\u0002`\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001dR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\"\u001a\u00020#¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%\u0001\u0003-./¨\u00060"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$Model;", "T", "", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "replyBody", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;", "nameColor", "", "onCopyClick", "Lkotlin/Function1;", "", "", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnCopyClick;", "onDeleteClick", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnDeleteClick;", "onTapForDetailsClick", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnTapForDetailsClick;", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "isFailure", "", "()Z", "isPending", "messageRecord", "getMessageRecord", "()Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "getNameColor", "()I", "getOnCopyClick", "()Lkotlin/jvm/functions/Function1;", "getOnDeleteClick", "getOnTapForDetailsClick", "getReplyBody", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody;", "sentAtMillis", "", "getSentAtMillis", "()J", "areContentsTheSame", "newItem", "(Ljava/lang/Object;)Z", "areItemsTheSame", "areNonPayloadPropertiesTheSame", "getChangePayload", "(Ljava/lang/Object;)Ljava/lang/Object;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$TextModel;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$ReactionModel;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$RemoteDeleteModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static abstract class Model<T> implements MappingModel<T> {
        private final boolean isFailure;
        private final boolean isPending;
        private final MessageRecord messageRecord;
        private final int nameColor;
        private final Function1<CharSequence, Unit> onCopyClick;
        private final Function1<MessageRecord, Unit> onDeleteClick;
        private final Function1<MessageRecord, Unit> onTapForDetailsClick;
        private final ReplyBody replyBody;
        private final long sentAtMillis;

        public /* synthetic */ Model(ReplyBody replyBody, int i, Function1 function1, Function1 function12, Function1 function13, DefaultConstructorMarker defaultConstructorMarker) {
            this(replyBody, i, function1, function12, function13);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.CharSequence, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.database.model.MessageRecord, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.database.model.MessageRecord, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        private Model(ReplyBody replyBody, int i, Function1<? super CharSequence, Unit> function1, Function1<? super MessageRecord, Unit> function12, Function1<? super MessageRecord, Unit> function13) {
            this.replyBody = replyBody;
            this.nameColor = i;
            this.onCopyClick = function1;
            this.onDeleteClick = function12;
            this.onTapForDetailsClick = function13;
            MessageRecord messageRecord = replyBody.getMessageRecord();
            this.messageRecord = messageRecord;
            this.isPending = messageRecord.isPending();
            this.isFailure = messageRecord.isFailed();
            this.sentAtMillis = replyBody.getSentAtMillis();
        }

        public final ReplyBody getReplyBody() {
            return this.replyBody;
        }

        public final int getNameColor() {
            return this.nameColor;
        }

        public final Function1<CharSequence, Unit> getOnCopyClick() {
            return this.onCopyClick;
        }

        public final Function1<MessageRecord, Unit> getOnDeleteClick() {
            return this.onDeleteClick;
        }

        public final Function1<MessageRecord, Unit> getOnTapForDetailsClick() {
            return this.onTapForDetailsClick;
        }

        public final MessageRecord getMessageRecord() {
            return this.messageRecord;
        }

        public final boolean isPending() {
            return this.isPending;
        }

        public final boolean isFailure() {
            return this.isFailure;
        }

        public final long getSentAtMillis() {
            return this.sentAtMillis;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public boolean areItemsTheSame(T t) {
            Intrinsics.checkNotNullParameter(t, "newItem");
            Model model = (Model) t;
            return Intrinsics.areEqual(this.replyBody.getSender(), model.replyBody.getSender()) && this.replyBody.getSentAtMillis() == model.replyBody.getSentAtMillis();
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public boolean areContentsTheSame(T t) {
            Intrinsics.checkNotNullParameter(t, "newItem");
            Model<?> model = (Model) t;
            return areNonPayloadPropertiesTheSame(model) && this.nameColor == model.nameColor;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public Object getChangePayload(T t) {
            Intrinsics.checkNotNullParameter(t, "newItem");
            Model<?> model = (Model) t;
            return (this.nameColor == model.nameColor || !areNonPayloadPropertiesTheSame(model)) ? null : 1;
        }

        private final boolean areNonPayloadPropertiesTheSame(Model<?> model) {
            return this.replyBody.hasSameContent(model.replyBody) && this.isPending == model.isPending && this.isFailure == model.isFailure && this.sentAtMillis == model.sentAtMillis;
        }
    }

    /* compiled from: StoryGroupReplyItem.kt */
    @Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001Bs\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u0012\b\b\u0001\u0010\b\u001a\u00020\t\u0012\u0016\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\f\u0012\u0016\u0010\r\u001a\u0012\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u000f\u0012\u0016\u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0011¢\u0006\u0002\u0010\u0012R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$TextModel;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$Model;", DraftDatabase.Draft.TEXT, "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$Text;", "onMentionClick", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "", "nameColor", "", "onCopyClick", "", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnCopyClick;", "onDeleteClick", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnDeleteClick;", "onTapForDetailsClick", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnTapForDetailsClick;", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$Text;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "getOnMentionClick", "()Lkotlin/jvm/functions/Function1;", "getText", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$Text;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class TextModel extends Model<TextModel> {
        private final Function1<RecipientId, Unit> onMentionClick;
        private final ReplyBody.Text text;

        public final ReplyBody.Text getText() {
            return this.text;
        }

        public final Function1<RecipientId, Unit> getOnMentionClick() {
            return this.onMentionClick;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r10v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.recipients.RecipientId, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public TextModel(ReplyBody.Text text, Function1<? super RecipientId, Unit> function1, int i, Function1<? super CharSequence, Unit> function12, Function1<? super MessageRecord, Unit> function13, Function1<? super MessageRecord, Unit> function14) {
            super(text, i, function12, function13, function14, null);
            Intrinsics.checkNotNullParameter(text, DraftDatabase.Draft.TEXT);
            Intrinsics.checkNotNullParameter(function1, "onMentionClick");
            Intrinsics.checkNotNullParameter(function12, "onCopyClick");
            Intrinsics.checkNotNullParameter(function13, "onDeleteClick");
            Intrinsics.checkNotNullParameter(function14, "onTapForDetailsClick");
            this.text = text;
            this.onMentionClick = function1;
        }
    }

    /* compiled from: StoryGroupReplyItem.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B_\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007j\u0002`\n\u0012\u0016\u0010\u000b\u001a\u0012\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\t0\u0007j\u0002`\r\u0012\u0016\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\t0\u0007j\u0002`\u000f¢\u0006\u0002\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$ReactionModel;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$Model;", ReactionDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$Reaction;", "nameColor", "", "onCopyClick", "Lkotlin/Function1;", "", "", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnCopyClick;", "onDeleteClick", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnDeleteClick;", "onTapForDetailsClick", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnTapForDetailsClick;", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$Reaction;ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "getReaction", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$Reaction;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ReactionModel extends Model<ReactionModel> {
        private final ReplyBody.Reaction reaction;

        public final ReplyBody.Reaction getReaction() {
            return this.reaction;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ReactionModel(ReplyBody.Reaction reaction, int i, Function1<? super CharSequence, Unit> function1, Function1<? super MessageRecord, Unit> function12, Function1<? super MessageRecord, Unit> function13) {
            super(reaction, i, function1, function12, function13, null);
            Intrinsics.checkNotNullParameter(reaction, ReactionDatabase.TABLE_NAME);
            Intrinsics.checkNotNullParameter(function1, "onCopyClick");
            Intrinsics.checkNotNullParameter(function12, "onDeleteClick");
            Intrinsics.checkNotNullParameter(function13, "onTapForDetailsClick");
            this.reaction = reaction;
        }
    }

    /* compiled from: StoryGroupReplyItem.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001BG\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007j\u0002`\n\u0012\u0016\u0010\u000b\u001a\u0012\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007j\u0002`\f¢\u0006\u0002\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$RemoteDeleteModel;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$Model;", "remoteDelete", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$RemoteDelete;", "nameColor", "", "onDeleteClick", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnDeleteClick;", "onTapForDetailsClick", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/OnTapForDetailsClick;", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$RemoteDelete;ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "getRemoteDelete", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/group/ReplyBody$RemoteDelete;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class RemoteDeleteModel extends Model<RemoteDeleteModel> {
        private final ReplyBody.RemoteDelete remoteDelete;

        public final ReplyBody.RemoteDelete getRemoteDelete() {
            return this.remoteDelete;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public RemoteDeleteModel(ReplyBody.RemoteDelete remoteDelete, int i, Function1<? super MessageRecord, Unit> function1, Function1<? super MessageRecord, Unit> function12) {
            super(remoteDelete, i, null, function1, function12, null);
            Intrinsics.checkNotNullParameter(remoteDelete, "remoteDelete");
            Intrinsics.checkNotNullParameter(function1, "onDeleteClick");
            Intrinsics.checkNotNullParameter(function12, "onTapForDetailsClick");
            this.remoteDelete = remoteDelete;
        }
    }

    /* compiled from: StoryGroupReplyItem.kt */
    @Metadata(d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\"\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00028\u0000H\u0017¢\u0006\u0002\u0010+J\u0014\u0010,\u001a\u00020)2\n\u0010*\u001a\u0006\u0012\u0002\b\u00030\u0002H\u0002R\u0014\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0014\u0010\u0016\u001a\u00020\u0017X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0014\u0010\u001a\u001a\u00020\u0017X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0019R\u0014\u0010\u001c\u001a\u00020\u001dX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0014\u0010 \u001a\u00020!X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0014\u0010$\u001a\u00020%X\u0004¢\u0006\b\n\u0000\u001a\u0004\b&\u0010'¨\u0006-"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$BaseViewHolder;", "T", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "alertView", "Lorg/thoughtcrime/securesms/components/AlertView;", "getAlertView", "()Lorg/thoughtcrime/securesms/components/AlertView;", "avatar", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "getAvatar", "()Lorg/thoughtcrime/securesms/components/AvatarImageView;", "body", "Lorg/thoughtcrime/securesms/components/emoji/EmojiTextView;", "getBody", "()Lorg/thoughtcrime/securesms/components/emoji/EmojiTextView;", "bubble", "getBubble", "()Landroid/view/View;", "date", "Landroid/widget/TextView;", "getDate", "()Landroid/widget/TextView;", "dateBelow", "getDateBelow", "name", "Lorg/thoughtcrime/securesms/components/FromTextView;", "getName", "()Lorg/thoughtcrime/securesms/components/FromTextView;", ReactionDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/components/emoji/EmojiImageView;", "getReaction", "()Lorg/thoughtcrime/securesms/components/emoji/EmojiImageView;", "status", "Lorg/thoughtcrime/securesms/components/DeliveryStatusView;", "getStatus", "()Lorg/thoughtcrime/securesms/components/DeliveryStatusView;", "bind", "", "model", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$Model;)V", "displayContextMenu", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static abstract class BaseViewHolder<T extends Model<T>> extends MappingViewHolder<T> {
        private final AlertView alertView;
        private final AvatarImageView avatar;
        private final EmojiTextView body;
        private final View bubble;
        private final TextView date;
        private final TextView dateBelow;
        private final FromTextView name;
        private final EmojiImageView reaction;
        private final DeliveryStatusView status;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public BaseViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = findViewById(R.id.bubble);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.bubble)");
            this.bubble = findViewById;
            View findViewById2 = findViewById(R.id.avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.avatar)");
            this.avatar = (AvatarImageView) findViewById2;
            View findViewById3 = findViewById(R.id.name);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.name)");
            this.name = (FromTextView) findViewById3;
            View findViewById4 = findViewById(R.id.body);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.body)");
            this.body = (EmojiTextView) findViewById4;
            View findViewById5 = findViewById(R.id.viewed_at);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "findViewById(R.id.viewed_at)");
            this.date = (TextView) findViewById5;
            View findViewById6 = findViewById(R.id.viewed_at_below);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "findViewById(R.id.viewed_at_below)");
            this.dateBelow = (TextView) findViewById6;
            View findViewById7 = findViewById(R.id.delivery_status);
            Intrinsics.checkNotNullExpressionValue(findViewById7, "findViewById(R.id.delivery_status)");
            this.status = (DeliveryStatusView) findViewById7;
            View findViewById8 = findViewById(R.id.alert_view);
            Intrinsics.checkNotNullExpressionValue(findViewById8, "findViewById(R.id.alert_view)");
            this.alertView = (AlertView) findViewById8;
            View findViewById9 = view.findViewById(R.id.reaction);
            Intrinsics.checkNotNullExpressionValue(findViewById9, "itemView.findViewById(R.id.reaction)");
            this.reaction = (EmojiImageView) findViewById9;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$BaseViewHolder<T extends org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem$Model<T>> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public /* bridge */ /* synthetic */ void bind(Object obj) {
            bind((BaseViewHolder<T>) ((Model) obj));
        }

        protected final View getBubble() {
            return this.bubble;
        }

        protected final AvatarImageView getAvatar() {
            return this.avatar;
        }

        protected final FromTextView getName() {
            return this.name;
        }

        protected final EmojiTextView getBody() {
            return this.body;
        }

        protected final TextView getDate() {
            return this.date;
        }

        protected final TextView getDateBelow() {
            return this.dateBelow;
        }

        protected final DeliveryStatusView getStatus() {
            return this.status;
        }

        protected final AlertView getAlertView() {
            return this.alertView;
        }

        protected final EmojiImageView getReaction() {
            return this.reaction;
        }

        public void bind(T t) {
            Intrinsics.checkNotNullParameter(t, "model");
            this.itemView.setOnLongClickListener(new StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda3(this, t));
            this.name.setTextColor(t.getNameColor());
            if (!this.payload.contains(1)) {
                AvatarUtil.loadIconIntoImageView(t.getReplyBody().getSender(), this.avatar, (int) DimensionUnit.DP.toPixels(28.0f));
                FromTextView fromTextView = this.name;
                StoryGroupReplyItem storyGroupReplyItem = StoryGroupReplyItem.INSTANCE;
                Context context = this.context;
                Intrinsics.checkNotNullExpressionValue(context, "context");
                fromTextView.setText(storyGroupReplyItem.resolveName(context, t.getReplyBody().getSender()));
                if (t.isPending()) {
                    this.status.setPending();
                } else {
                    this.status.setNone();
                }
                if (t.isFailure()) {
                    this.alertView.setFailed();
                    this.itemView.setOnClickListener(new StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda4(t));
                    this.date.setText(R.string.ConversationItem_error_not_sent_tap_for_details);
                    this.dateBelow.setText(R.string.ConversationItem_error_not_sent_tap_for_details);
                } else {
                    this.alertView.setNone();
                    this.itemView.setOnClickListener(null);
                    String briefRelativeTimeSpanString = DateUtils.getBriefRelativeTimeSpanString(this.context, Locale.getDefault(), t.getSentAtMillis());
                    this.date.setText(briefRelativeTimeSpanString);
                    this.dateBelow.setText(briefRelativeTimeSpanString);
                }
                this.itemView.post(new StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda5(this));
            }
        }

        /* renamed from: bind$lambda-0 */
        public static final boolean m3147bind$lambda0(BaseViewHolder baseViewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(baseViewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            baseViewHolder.displayContextMenu(model);
            return true;
        }

        /* renamed from: bind$lambda-1 */
        public static final void m3148bind$lambda1(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnTapForDetailsClick().invoke(model.getMessageRecord());
        }

        /* renamed from: bind$lambda-2 */
        public static final void m3149bind$lambda2(BaseViewHolder baseViewHolder) {
            Intrinsics.checkNotNullParameter(baseViewHolder, "this$0");
            if (ViewExtensionsKt.getVisible(baseViewHolder.alertView) || baseViewHolder.body.getLastLineWidth() + baseViewHolder.date.getMeasuredWidth() > ViewUtil.dpToPx(242)) {
                ViewExtensionsKt.setVisible(baseViewHolder.date, false);
                ViewExtensionsKt.setVisible(baseViewHolder.dateBelow, true);
                return;
            }
            ViewExtensionsKt.setVisible(baseViewHolder.dateBelow, false);
            ViewExtensionsKt.setVisible(baseViewHolder.date, true);
        }

        private final void displayContextMenu(Model<?> model) {
            this.itemView.setSelected(true);
            ArrayList arrayList = new ArrayList();
            if (model.getOnCopyClick() != null) {
                String string = this.context.getString(R.string.StoryGroupReplyItem__copy);
                Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…toryGroupReplyItem__copy)");
                arrayList.add(new ActionItem(R.drawable.ic_copy_24_tinted, string, 0, new StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda0(model, this), 4, null));
            }
            String string2 = this.context.getString(R.string.StoryGroupReplyItem__delete);
            Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.stri…ryGroupReplyItem__delete)");
            arrayList.add(new ActionItem(R.drawable.ic_trash_24, string2, 0, new StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda1(model), 4, null));
            View view = this.itemView;
            Intrinsics.checkNotNullExpressionValue(view, "itemView");
            View rootView = this.itemView.getRootView();
            if (rootView != null) {
                new SignalContextMenu.Builder(view, (ViewGroup) rootView).preferredHorizontalPosition(SignalContextMenu.HorizontalPosition.START).onDismiss(new StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda2(this)).show(arrayList);
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
        }

        /* renamed from: displayContextMenu$lambda-3 */
        public static final void m3150displayContextMenu$lambda3(Model model, BaseViewHolder baseViewHolder) {
            SpannableString spannableString;
            Intrinsics.checkNotNullParameter(model, "$model");
            Intrinsics.checkNotNullParameter(baseViewHolder, "this$0");
            if (model instanceof TextModel) {
                spannableString = ((TextModel) model).getText().getMessage().getDisplayBody(baseViewHolder.context);
                Intrinsics.checkNotNullExpressionValue(spannableString, "model.text.message.getDisplayBody(context)");
            } else {
                spannableString = model.getMessageRecord().getDisplayBody(baseViewHolder.context);
                Intrinsics.checkNotNullExpressionValue(spannableString, "model.messageRecord.getDisplayBody(context)");
            }
            model.getOnCopyClick().invoke(spannableString);
        }

        /* renamed from: displayContextMenu$lambda-4 */
        public static final void m3151displayContextMenu$lambda4(Model model) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnDeleteClick().invoke(model.getMessageRecord());
        }

        /* renamed from: displayContextMenu$lambda-5 */
        public static final void m3152displayContextMenu$lambda5(BaseViewHolder baseViewHolder) {
            Intrinsics.checkNotNullParameter(baseViewHolder, "this$0");
            baseViewHolder.itemView.setSelected(false);
        }
    }

    /* compiled from: StoryGroupReplyItem.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\fB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016J\u0018\u0010\t\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u000bH\u0002¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$TextViewHolder;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$BaseViewHolder;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$TextModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "linkifyBody", "body", "Landroid/text/Spannable;", "MentionClickableSpan", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class TextViewHolder extends BaseViewHolder<TextModel> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public TextViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }

        public void bind(TextModel textModel) {
            Intrinsics.checkNotNullParameter(textModel, "model");
            super.bind((TextViewHolder) textModel);
            getBody().setMovementMethod(LinkMovementMethod.getInstance());
            EmojiTextView body = getBody();
            SpannableString displayBody = textModel.getText().getMessage().getDisplayBody(this.context);
            Intrinsics.checkNotNullExpressionValue(displayBody, "this");
            linkifyBody(textModel, displayBody);
            body.setText(displayBody);
        }

        private final void linkifyBody(TextModel textModel, Spannable spannable) {
            List<Annotation> mentionAnnotations = MentionAnnotation.getMentionAnnotations(spannable);
            Intrinsics.checkNotNullExpressionValue(mentionAnnotations, "getMentionAnnotations(body)");
            for (Annotation annotation : mentionAnnotations) {
                RecipientId from = RecipientId.from(annotation.getValue());
                Intrinsics.checkNotNullExpressionValue(from, "from(annotation.value)");
                spannable.setSpan(new MentionClickableSpan(textModel, from), spannable.getSpanStart(annotation), spannable.getSpanEnd(annotation), 33);
            }
        }

        /* compiled from: StoryGroupReplyItem.kt */
        @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\rH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$TextViewHolder$MentionClickableSpan;", "Landroid/text/style/ClickableSpan;", "model", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$TextModel;", "mentionedRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$TextModel;Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "onClick", "", "widget", "Landroid/view/View;", "updateDrawState", "ds", "Landroid/text/TextPaint;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class MentionClickableSpan extends ClickableSpan {
            private final RecipientId mentionedRecipientId;
            private final TextModel model;

            @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
            public void updateDrawState(TextPaint textPaint) {
                Intrinsics.checkNotNullParameter(textPaint, "ds");
            }

            public MentionClickableSpan(TextModel textModel, RecipientId recipientId) {
                Intrinsics.checkNotNullParameter(textModel, "model");
                Intrinsics.checkNotNullParameter(recipientId, "mentionedRecipientId");
                this.model = textModel;
                this.mentionedRecipientId = recipientId;
            }

            @Override // android.text.style.ClickableSpan
            public void onClick(View view) {
                Intrinsics.checkNotNullParameter(view, "widget");
                this.model.getOnMentionClick().invoke(this.mentionedRecipientId);
            }
        }
    }

    /* compiled from: StoryGroupReplyItem.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$ReactionViewHolder;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$BaseViewHolder;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$ReactionModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ReactionViewHolder extends BaseViewHolder<ReactionModel> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ReactionViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            ViewExtensionsKt.setVisible(getReaction(), true);
            getBubble().setVisibility(4);
            ViewExtensionsKt.padding$default(view, 0, 0, 0, 0, 7, null);
            getBody().setText(R.string.StoryGroupReactionReplyItem__reacted_to_the_story);
            EmojiTextView body = getBody();
            ViewGroup.LayoutParams layoutParams = body.getLayoutParams();
            if (layoutParams != null) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                marginLayoutParams.setMarginEnd(0);
                body.setLayoutParams(marginLayoutParams);
                ViewExtensionsKt.changeConstraints((ConstraintLayout) view, new Function1<ConstraintSet, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem.ReactionViewHolder.2
                    final /* synthetic */ ReactionViewHolder this$0;

                    {
                        this.this$0 = r1;
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(ConstraintSet constraintSet) {
                        invoke(constraintSet);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(ConstraintSet constraintSet) {
                        Intrinsics.checkNotNullParameter(constraintSet, "$this$changeConstraints");
                        constraintSet.connect(this.this$0.getAvatar().getId(), 4, this.this$0.getBody().getId(), 4);
                    }
                });
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }

        public void bind(ReactionModel reactionModel) {
            Intrinsics.checkNotNullParameter(reactionModel, "model");
            super.bind((ReactionViewHolder) reactionModel);
            getReaction().setImageEmoji(reactionModel.getReaction().getEmoji());
        }
    }

    /* compiled from: StoryGroupReplyItem.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$RemoteDeleteViewHolder;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$BaseViewHolder;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyItem$RemoteDeleteModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class RemoteDeleteViewHolder extends BaseViewHolder<RemoteDeleteModel> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public RemoteDeleteViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            getBubble().setBackgroundResource(R.drawable.rounded_outline_secondary_18);
            getBody().setText(R.string.ThreadRecord_this_message_was_deleted);
        }
    }

    public final String resolveName(Context context, Recipient recipient) {
        if (recipient.isSelf()) {
            String string = context.getString(R.string.StoryViewerPageFragment__you);
            Intrinsics.checkNotNullExpressionValue(string, "{\n      context.getStrin…rPageFragment__you)\n    }");
            return string;
        }
        String displayName = recipient.getDisplayName(context);
        Intrinsics.checkNotNullExpressionValue(displayName, "{\n      recipient.getDisplayName(context)\n    }");
        return displayName;
    }
}
