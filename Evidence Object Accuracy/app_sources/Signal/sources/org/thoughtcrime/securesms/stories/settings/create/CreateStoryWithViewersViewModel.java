package org.thoughtcrime.securesms.stories.settings.create;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersState;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: CreateStoryWithViewersViewModel.kt */
@Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0017B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\u000e\u001a\u00020\u000f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011J\b\u0010\u0013\u001a\u00020\u000fH\u0014J\u000e\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersRepository;", "(Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "create", "", "members", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "onCleared", "setLabel", EmojiSearchDatabase.LABEL, "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class CreateStoryWithViewersViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final CreateStoryWithViewersRepository repository;
    private final LiveData<CreateStoryWithViewersState> state;
    private final Store<CreateStoryWithViewersState> store;

    public CreateStoryWithViewersViewModel(CreateStoryWithViewersRepository createStoryWithViewersRepository) {
        Intrinsics.checkNotNullParameter(createStoryWithViewersRepository, "repository");
        this.repository = createStoryWithViewersRepository;
        Store<CreateStoryWithViewersState> store = new Store<>(new CreateStoryWithViewersState(null, null, null, 7, null));
        this.store = store;
        LiveData<CreateStoryWithViewersState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<CreateStoryWithViewersState> getState() {
        return this.state;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    /* renamed from: setLabel$lambda-0 */
    public static final CreateStoryWithViewersState m2869setLabel$lambda0(CharSequence charSequence, CreateStoryWithViewersState createStoryWithViewersState) {
        Intrinsics.checkNotNullParameter(charSequence, "$label");
        Intrinsics.checkNotNullExpressionValue(createStoryWithViewersState, "it");
        return CreateStoryWithViewersState.copy$default(createStoryWithViewersState, charSequence, null, null, 6, null);
    }

    public final void setLabel(CharSequence charSequence) {
        Intrinsics.checkNotNullParameter(charSequence, EmojiSearchDatabase.LABEL);
        this.store.update(new Function(charSequence) { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ CharSequence f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CreateStoryWithViewersViewModel.m2869setLabel$lambda0(this.f$0, (CreateStoryWithViewersState) obj);
            }
        });
    }

    /* renamed from: create$lambda-1 */
    public static final CreateStoryWithViewersState m2867create$lambda1(CreateStoryWithViewersState createStoryWithViewersState) {
        Intrinsics.checkNotNullExpressionValue(createStoryWithViewersState, "it");
        return CreateStoryWithViewersState.copy$default(createStoryWithViewersState, null, null, CreateStoryWithViewersState.SaveState.Saving.INSTANCE, 3, null);
    }

    public final void create(Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(set, "members");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CreateStoryWithViewersViewModel.m2867create$lambda1((CreateStoryWithViewersState) obj);
            }
        });
        CharSequence label = this.store.getState().getLabel();
        if (label.length() == 0) {
            this.store.update(new Function() { // from class: org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersViewModel$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return CreateStoryWithViewersViewModel.m2868create$lambda2((CreateStoryWithViewersState) obj);
                }
            });
        }
        DisposableKt.plusAssign(this.disposables, SubscribersKt.subscribeBy(this.repository.createList(label, set), new CreateStoryWithViewersViewModel$create$3(this), new CreateStoryWithViewersViewModel$create$4(this)));
    }

    /* renamed from: create$lambda-2 */
    public static final CreateStoryWithViewersState m2868create$lambda2(CreateStoryWithViewersState createStoryWithViewersState) {
        Intrinsics.checkNotNullExpressionValue(createStoryWithViewersState, "it");
        return CreateStoryWithViewersState.copy$default(createStoryWithViewersState, null, CreateStoryWithViewersState.NameError.NO_LABEL, CreateStoryWithViewersState.SaveState.Init.INSTANCE, 1, null);
    }

    /* compiled from: CreateStoryWithViewersViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersRepository;", "(Lorg/thoughtcrime/securesms/stories/settings/create/CreateStoryWithViewersRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final CreateStoryWithViewersRepository repository;

        public Factory(CreateStoryWithViewersRepository createStoryWithViewersRepository) {
            Intrinsics.checkNotNullParameter(createStoryWithViewersRepository, "repository");
            this.repository = createStoryWithViewersRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new CreateStoryWithViewersViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersViewModel.Factory.create");
        }
    }
}
