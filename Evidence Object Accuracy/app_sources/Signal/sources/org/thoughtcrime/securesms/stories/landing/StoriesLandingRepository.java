package org.thoughtcrime.securesms.stories.landing;

import android.content.Context;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.comparisons.ComparisonsKt__ComparisonsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.StoryResult;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientForeverObserver;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.MessageSender;

/* compiled from: StoriesLandingRepository.kt */
@Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J4\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fH\u0002J\u0012\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\f0\u0007J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\rJ\u0016\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019R\u0016\u0010\u0002\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "kotlin.jvm.PlatformType", "createStoriesLandingItemData", "Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/stories/landing/StoriesLandingItemData;", "sender", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "messageRecords", "", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "sendingCount", "", "failureCount", "getStories", "resend", "Lio/reactivex/rxjava3/core/Completable;", "story", "setHideStory", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "hideStory", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoriesLandingRepository {
    private final Context context;

    public StoriesLandingRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context.getApplicationContext();
    }

    public final Completable resend(MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(messageRecord, "story");
        Completable subscribeOn = Completable.fromAction(new Action(messageRecord) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ MessageRecord f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                StoriesLandingRepository.$r8$lambda$GpZxuDIp34u0GNHwzzPhjqfgKw0(StoriesLandingRepository.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Messa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: resend$lambda-0 */
    public static final void m2827resend$lambda0(StoriesLandingRepository storiesLandingRepository, MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(storiesLandingRepository, "this$0");
        Intrinsics.checkNotNullParameter(messageRecord, "$story");
        MessageSender.resend(storiesLandingRepository.context, messageRecord);
    }

    public final Observable<List<StoriesLandingItemData>> getStories() {
        Observable create = Observable.create(new ObservableOnSubscribe() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                StoriesLandingRepository.m2813$r8$lambda$mYXaUp6PpFhcInHgc7LOHIrlH0(observableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create { emitter ->\n    … }\n\n      refresh()\n    }");
        Observable<List<StoriesLandingItemData>> subscribeOn = create.switchMap(new Function() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda8
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoriesLandingRepository.$r8$lambda$IHKWtQ7b8LPAvAr04_V0pd54en4(StoriesLandingRepository.this, (Map) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "storyRecipients.switchMa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getStories$lambda-4$refresh */
    private static final void m2826getStories$lambda4$refresh(ObservableEmitter<Map<Recipient, List<StoryResult>>> observableEmitter) {
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        RecipientDatabase recipients = companion.recipients();
        DistributionListId distributionListId = DistributionListId.MY_STORY;
        Intrinsics.checkNotNullExpressionValue(distributionListId, "MY_STORY");
        Recipient resolved = Recipient.resolved(RecipientDatabase.getOrInsertFromDistributionListId$default(recipients, distributionListId, null, 2, null));
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(myStoriesId)");
        List<StoryResult> orderedStoryRecipientsAndIds = companion.mms().getOrderedStoryRecipientsAndIds(false);
        Intrinsics.checkNotNullExpressionValue(orderedStoryRecipientsAndIds, "SignalDatabase.mms.getOr…ryRecipientsAndIds(false)");
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (StoryResult storyResult : orderedStoryRecipientsAndIds) {
            Recipient resolved2 = Recipient.resolved(storyResult.getRecipientId());
            Intrinsics.checkNotNullExpressionValue(resolved2, "resolved(it.recipientId)");
            if (resolved2.isDistributionList() || storyResult.isOutgoing()) {
                List<StoryResult> list = linkedHashMap.get(resolved);
                if (list == null) {
                    list = CollectionsKt__CollectionsKt.emptyList();
                }
                linkedHashMap.put(resolved, CollectionsKt___CollectionsKt.plus((Collection<? extends StoryResult>) ((Collection<? extends Object>) list), storyResult));
            }
            if (!resolved2.isDistributionList()) {
                List<StoryResult> list2 = linkedHashMap.get(resolved2);
                if (list2 == null) {
                    list2 = CollectionsKt__CollectionsKt.emptyList();
                }
                linkedHashMap.put(resolved2, CollectionsKt___CollectionsKt.plus((Collection<? extends StoryResult>) ((Collection<? extends Object>) list2), storyResult));
            }
        }
        observableEmitter.onNext(linkedHashMap);
    }

    /* renamed from: getStories$lambda-4 */
    public static final void m2823getStories$lambda4(ObservableEmitter observableEmitter) {
        StoriesLandingRepository$$ExternalSyntheticLambda4 storiesLandingRepository$$ExternalSyntheticLambda4 = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda4
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                StoriesLandingRepository.$r8$lambda$MU0HpBv87RCrGAnUzuT1szEgpew(ObservableEmitter.this);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerConversationListObserver(storiesLandingRepository$$ExternalSyntheticLambda4);
        observableEmitter.setCancellable(new Cancellable() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                StoriesLandingRepository.m2811$r8$lambda$eK_wt_7U42Y1KvYtla6HMFe68c(DatabaseObserver.Observer.this);
            }
        });
        m2826getStories$lambda4$refresh(observableEmitter);
    }

    /* renamed from: getStories$lambda-4$lambda-2 */
    public static final void m2824getStories$lambda4$lambda2(ObservableEmitter observableEmitter) {
        m2826getStories$lambda4$refresh(observableEmitter);
    }

    /* renamed from: getStories$lambda-4$lambda-3 */
    public static final void m2825getStories$lambda4$lambda3(DatabaseObserver.Observer observer) {
        Intrinsics.checkNotNullParameter(observer, "$observer");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(observer);
    }

    /* renamed from: getStories$lambda-11 */
    public static final ObservableSource m2821getStories$lambda11(StoriesLandingRepository storiesLandingRepository, Map map) {
        long j;
        Intrinsics.checkNotNullParameter(storiesLandingRepository, "this$0");
        Intrinsics.checkNotNullExpressionValue(map, "map");
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry entry : map.entrySet()) {
            Recipient recipient = (Recipient) entry.getKey();
            List<StoryResult> list = (List) entry.getValue();
            List<StoryResult> list2 = CollectionsKt___CollectionsKt.take(CollectionsKt___CollectionsKt.reversed(CollectionsKt___CollectionsKt.sortedWith(list, new Comparator() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$getStories$lambda-11$lambda-9$$inlined$sortedBy$1
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    return ComparisonsKt__ComparisonsKt.compareValues(Long.valueOf(((StoryResult) t).getMessageSentTimestamp()), Long.valueOf(((StoryResult) t2).getMessageSentTimestamp()));
                }
            })), recipient.isMyStory() ? 2 : 1);
            ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list2, 10));
            for (StoryResult storyResult : list2) {
                arrayList2.add(SignalDatabase.Companion.mms().getMessageRecord(storyResult.getMessageId()));
            }
            long j2 = 0;
            if (recipient.isMyStory()) {
                MmsDatabase mms = SignalDatabase.Companion.mms();
                ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
                for (StoryResult storyResult2 : list) {
                    arrayList3.add(Long.valueOf(storyResult2.getMessageId()));
                }
                MmsDatabase.Reader messages = mms.getMessages((Collection<Long>) arrayList3);
                th = null;
                try {
                    j = 0;
                    for (MessageRecord next = messages.getNext(); next != null; next = messages.getNext()) {
                        if (next.isOutgoing() && (next.isPending() || next.isMediaPending())) {
                            j2++;
                        } else if (next.isFailed()) {
                            j++;
                        }
                    }
                    Unit unit = Unit.INSTANCE;
                } catch (Throwable th) {
                    try {
                        throw th;
                    } finally {
                        CloseableKt.closeFinally(messages, th);
                    }
                }
            } else {
                j = 0;
            }
            arrayList.add(storiesLandingRepository.createStoriesLandingItemData(recipient, arrayList2, j2, j));
        }
        if (arrayList.isEmpty()) {
            return Observable.just(CollectionsKt__CollectionsKt.emptyList());
        }
        return Observable.combineLatest(arrayList, new Function() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoriesLandingRepository.m2814$r8$lambda$z_sccyeOuLKEDktDaHcGdBLJmY((Object[]) obj);
            }
        });
    }

    /* renamed from: getStories$lambda-11$lambda-10 */
    public static final List m2822getStories$lambda11$lambda10(Object[] objArr) {
        Intrinsics.checkNotNullExpressionValue(objArr, "it");
        return ArraysKt___ArraysKt.toList(objArr);
    }

    private final Observable<StoriesLandingItemData> createStoriesLandingItemData(Recipient recipient, List<? extends MessageRecord> list, long j, long j2) {
        Observable create = Observable.create(new ObservableOnSubscribe(list, recipient, this, j, j2) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$0;
            public final /* synthetic */ Recipient f$1;
            public final /* synthetic */ StoriesLandingRepository f$2;
            public final /* synthetic */ long f$3;
            public final /* synthetic */ long f$4;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r6;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                StoriesLandingRepository.m2810$r8$lambda$FKSovqgmcX4muq03MkgYNzSc8c(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, observableEmitter);
            }
        });
        StoryViewState.Companion companion = StoryViewState.Companion;
        if (recipient.isMyStory()) {
            recipient = Recipient.self();
        }
        RecipientId id = recipient.getId();
        Intrinsics.checkNotNullExpressionValue(id, "if (sender.isMyStory) Re….self().id else sender.id");
        Observable<StoriesLandingItemData> combineLatest = Observable.combineLatest(create, companion.getForRecipientId(id), new BiFunction() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return StoriesLandingRepository.$r8$lambda$aOTPmiIgwafFkcOJEKvCbvWE9oA((StoriesLandingItemData) obj, (StoryViewState) obj2);
            }
        });
        Intrinsics.checkNotNullExpressionValue(combineLatest, "combineLatest(itemDataOb…yViewState = state)\n    }");
        return combineLatest;
    }

    /* renamed from: createStoriesLandingItemData$lambda-21 */
    public static final void m2815createStoriesLandingItemData$lambda21(List list, Recipient recipient, StoriesLandingRepository storiesLandingRepository, long j, long j2, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(list, "$messageRecords");
        Intrinsics.checkNotNullParameter(recipient, "$sender");
        Intrinsics.checkNotNullParameter(storiesLandingRepository, "this$0");
        StoriesLandingRepository$$ExternalSyntheticLambda9 storiesLandingRepository$$ExternalSyntheticLambda9 = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda9
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                StoriesLandingRepository.m2812$r8$lambda$m69nStwh2Ay5wLXiDCJ51TjNg4(Recipient.this);
            }
        };
        StoriesLandingRepository$$ExternalSyntheticLambda10 storiesLandingRepository$$ExternalSyntheticLambda10 = new RecipientForeverObserver(list, storiesLandingRepository, j, j2, observableEmitter) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda10
            public final /* synthetic */ List f$0;
            public final /* synthetic */ StoriesLandingRepository f$1;
            public final /* synthetic */ long f$2;
            public final /* synthetic */ long f$3;
            public final /* synthetic */ ObservableEmitter f$4;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r5;
                this.f$4 = r7;
            }

            @Override // org.thoughtcrime.securesms.recipients.RecipientForeverObserver
            public final void onRecipientChanged(Recipient recipient2) {
                StoriesLandingRepository.$r8$lambda$FjrCdPCIajnR_9SQRvwHzZU56vo(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, recipient2);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerConversationObserver(((MessageRecord) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) list))).getThreadId(), storiesLandingRepository$$ExternalSyntheticLambda9);
        LiveRecipient live = Recipient.live(recipient.getId());
        Intrinsics.checkNotNullExpressionValue(live, "live(sender.id)");
        live.observeForever(storiesLandingRepository$$ExternalSyntheticLambda10);
        observableEmitter.setCancellable(new Cancellable(live, storiesLandingRepository$$ExternalSyntheticLambda10) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda11
            public final /* synthetic */ LiveRecipient f$1;
            public final /* synthetic */ RecipientForeverObserver f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                StoriesLandingRepository.$r8$lambda$ZusvGcCeFUhhsZYFCgP8fktA1P0(DatabaseObserver.Observer.this, this.f$1, this.f$2);
            }
        });
        m2819createStoriesLandingItemData$lambda21$refresh17(list, storiesLandingRepository, j, j2, observableEmitter, recipient);
    }

    /* renamed from: createStoriesLandingItemData$lambda-21$lambda-18 */
    public static final void m2816createStoriesLandingItemData$lambda21$lambda18(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, "$sender");
        Recipient.live(recipient.getId()).refresh();
    }

    /* renamed from: createStoriesLandingItemData$lambda-21$lambda-19 */
    public static final void m2817createStoriesLandingItemData$lambda21$lambda19(List list, StoriesLandingRepository storiesLandingRepository, long j, long j2, ObservableEmitter observableEmitter, Recipient recipient) {
        Intrinsics.checkNotNullParameter(list, "$messageRecords");
        Intrinsics.checkNotNullParameter(storiesLandingRepository, "this$0");
        Intrinsics.checkNotNullParameter(recipient, "it");
        m2819createStoriesLandingItemData$lambda21$refresh17(list, storiesLandingRepository, j, j2, observableEmitter, recipient);
    }

    /* renamed from: createStoriesLandingItemData$lambda-21$lambda-20 */
    public static final void m2818createStoriesLandingItemData$lambda21$lambda20(DatabaseObserver.Observer observer, LiveRecipient liveRecipient, RecipientForeverObserver recipientForeverObserver) {
        Intrinsics.checkNotNullParameter(observer, "$newRepliesObserver");
        Intrinsics.checkNotNullParameter(liveRecipient, "$liveRecipient");
        Intrinsics.checkNotNullParameter(recipientForeverObserver, "$recipientChangedObserver");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(observer);
        liveRecipient.lambda$asObservable$6(recipientForeverObserver);
    }

    /* renamed from: createStoriesLandingItemData$lambda-22 */
    public static final StoriesLandingItemData m2820createStoriesLandingItemData$lambda22(StoriesLandingItemData storiesLandingItemData, StoryViewState storyViewState) {
        Intrinsics.checkNotNullExpressionValue(storiesLandingItemData, "data");
        Intrinsics.checkNotNullExpressionValue(storyViewState, "state");
        return storiesLandingItemData.copy((r31 & 1) != 0 ? storiesLandingItemData.storyViewState : storyViewState, (r31 & 2) != 0 ? storiesLandingItemData.hasReplies : false, (r31 & 4) != 0 ? storiesLandingItemData.hasRepliesFromSelf : false, (r31 & 8) != 0 ? storiesLandingItemData.isHidden : false, (r31 & 16) != 0 ? storiesLandingItemData.primaryStory : null, (r31 & 32) != 0 ? storiesLandingItemData.secondaryStory : null, (r31 & 64) != 0 ? storiesLandingItemData.storyRecipient : null, (r31 & 128) != 0 ? storiesLandingItemData.individualRecipient : null, (r31 & 256) != 0 ? storiesLandingItemData.dateInMilliseconds : 0, (r31 & 512) != 0 ? storiesLandingItemData.sendingCount : 0, (r31 & 1024) != 0 ? storiesLandingItemData.failureCount : 0);
    }

    public final Completable setHideStory(RecipientId recipientId, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Completable subscribeOn = Completable.fromAction(new Action(z) { // from class: org.thoughtcrime.securesms.stories.landing.StoriesLandingRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                StoriesLandingRepository.$r8$lambda$chirAlbNIZbdEgtlSx0vkU53q8E(RecipientId.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Signa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: setHideStory$lambda-23 */
    public static final void m2828setHideStory$lambda23(RecipientId recipientId, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        SignalDatabase.Companion.recipients().setHideStory(recipientId, z);
    }

    /* renamed from: createStoriesLandingItemData$lambda-21$refresh-17 */
    private static final void m2819createStoriesLandingItemData$lambda21$refresh17(List<? extends MessageRecord> list, StoriesLandingRepository storiesLandingRepository, long j, long j2, ObservableEmitter<StoriesLandingItemData> observableEmitter, Recipient recipient) {
        boolean z;
        boolean z2;
        MessageRecord messageRecord;
        boolean z3;
        Iterator<? extends MessageRecord> it = list.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            }
            MessageRecord messageRecord2 = (MessageRecord) it.next();
            if (!messageRecord2.isOutgoing() && messageRecord2.getViewedReceiptCount() == 0) {
                break;
            }
            i++;
        }
        Integer valueOf = Integer.valueOf(i);
        ConversationMessage conversationMessage = null;
        if (!(valueOf.intValue() > -1)) {
            valueOf = null;
        }
        int intValue = valueOf != null ? valueOf.intValue() : 0;
        StoryViewState storyViewState = StoryViewState.NONE;
        if (!list.isEmpty()) {
            for (MessageRecord messageRecord3 : list) {
                if (SignalDatabase.Companion.mms().getNumberOfStoryReplies(messageRecord3.getId()) > 0) {
                    z3 = true;
                    continue;
                } else {
                    z3 = false;
                    continue;
                }
                if (z3) {
                    z = true;
                    break;
                }
            }
        }
        z = false;
        if (!list.isEmpty()) {
            for (MessageRecord messageRecord4 : list) {
                if (SignalDatabase.Companion.mms().hasSelfReplyInStory(messageRecord4.getId())) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        boolean shouldHideStory = recipient.shouldHideStory();
        ConversationMessage createWithUnresolvedData = ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(storiesLandingRepository.context, (MessageRecord) list.get(intValue));
        if (recipient.isMyStory() && (messageRecord = (MessageRecord) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) CollectionsKt___CollectionsKt.drop(list, 1)))) != null) {
            conversationMessage = ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(storiesLandingRepository.context, messageRecord);
        }
        Intrinsics.checkNotNullExpressionValue(createWithUnresolvedData, "createWithUnresolvedData…ageRecords[primaryIndex])");
        observableEmitter.onNext(new StoriesLandingItemData(storyViewState, z, z2, shouldHideStory, createWithUnresolvedData, conversationMessage, recipient, null, 0, j, j2, 384, null));
    }
}
