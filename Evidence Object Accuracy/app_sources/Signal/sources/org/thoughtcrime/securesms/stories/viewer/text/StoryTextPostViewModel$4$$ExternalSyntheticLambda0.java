package org.thoughtcrime.securesms.stories.viewer.text;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.stories.viewer.text.StoryTextPostViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryTextPostViewModel$4$$ExternalSyntheticLambda0 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return StoryTextPostViewModel.AnonymousClass4.m3201invoke$lambda0((StoryTextPostState) obj);
    }
}
