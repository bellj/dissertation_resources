package org.thoughtcrime.securesms.stories.viewer;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: StoryVolumeViewModel.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/stories/viewer/StoryVolumeState;", "it", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryVolumeViewModel$unmute$1 extends Lambda implements Function1<StoryVolumeState, StoryVolumeState> {
    public static final StoryVolumeViewModel$unmute$1 INSTANCE = new StoryVolumeViewModel$unmute$1();

    StoryVolumeViewModel$unmute$1() {
        super(1);
    }

    public final StoryVolumeState invoke(StoryVolumeState storyVolumeState) {
        Intrinsics.checkNotNullParameter(storyVolumeState, "it");
        return StoryVolumeState.copy$default(storyVolumeState, false, 0, 2, null);
    }
}
