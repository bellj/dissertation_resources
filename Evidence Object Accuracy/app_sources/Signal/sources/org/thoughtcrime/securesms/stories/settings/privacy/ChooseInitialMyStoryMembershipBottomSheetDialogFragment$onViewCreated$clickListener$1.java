package org.thoughtcrime.securesms.stories.settings.privacy;

import android.view.View;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.database.model.DistributionListPrivacyMode;

/* compiled from: ChooseInitialMyStoryMembershipBottomSheetDialogFragment.kt */
@Metadata(bv = {}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroid/view/View;", "v", "Lio/reactivex/rxjava3/disposables/Disposable;", "kotlin.jvm.PlatformType", "invoke", "(Landroid/view/View;)Lio/reactivex/rxjava3/disposables/Disposable;", "<anonymous>"}, k = 3, mv = {1, 6, 0})
/* loaded from: classes3.dex */
final class ChooseInitialMyStoryMembershipBottomSheetDialogFragment$onViewCreated$clickListener$1 extends Lambda implements Function1<View, Disposable> {
    final /* synthetic */ ChooseInitialMyStoryMembershipBottomSheetDialogFragment this$0;

    /* compiled from: ChooseInitialMyStoryMembershipBottomSheetDialogFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[DistributionListPrivacyMode.values().length];
            iArr[DistributionListPrivacyMode.ALL_EXCEPT.ordinal()] = 1;
            iArr[DistributionListPrivacyMode.ONLY_WITH.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ChooseInitialMyStoryMembershipBottomSheetDialogFragment$onViewCreated$clickListener$1(ChooseInitialMyStoryMembershipBottomSheetDialogFragment chooseInitialMyStoryMembershipBottomSheetDialogFragment) {
        super(1);
        this.this$0 = chooseInitialMyStoryMembershipBottomSheetDialogFragment;
    }

    public final Disposable invoke(View view) {
        DistributionListPrivacyMode distributionListPrivacyMode;
        Intrinsics.checkNotNullParameter(view, "v");
        View view2 = this.this$0.allRow;
        View view3 = null;
        if (view2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("allRow");
            view2 = null;
        }
        if (Intrinsics.areEqual(view, view2)) {
            distributionListPrivacyMode = DistributionListPrivacyMode.ALL;
        } else {
            View view4 = this.this$0.allExceptRow;
            if (view4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("allExceptRow");
                view4 = null;
            }
            if (Intrinsics.areEqual(view, view4)) {
                distributionListPrivacyMode = DistributionListPrivacyMode.ALL_EXCEPT;
            } else {
                View view5 = this.this$0.onlyWitRow;
                if (view5 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("onlyWitRow");
                } else {
                    view3 = view5;
                }
                if (Intrinsics.areEqual(view, view3)) {
                    distributionListPrivacyMode = DistributionListPrivacyMode.ONLY_WITH;
                } else {
                    throw new AssertionError();
                }
            }
        }
        return this.this$0.getViewModel().select(distributionListPrivacyMode).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment$onViewCreated$clickListener$1$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ChooseInitialMyStoryMembershipBottomSheetDialogFragment$onViewCreated$clickListener$1.m2933invoke$lambda0(ChooseInitialMyStoryMembershipBottomSheetDialogFragment.this, (DistributionListPrivacyMode) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m2933invoke$lambda0(ChooseInitialMyStoryMembershipBottomSheetDialogFragment chooseInitialMyStoryMembershipBottomSheetDialogFragment, DistributionListPrivacyMode distributionListPrivacyMode) {
        Intrinsics.checkNotNullParameter(chooseInitialMyStoryMembershipBottomSheetDialogFragment, "this$0");
        int i = distributionListPrivacyMode == null ? -1 : WhenMappings.$EnumSwitchMapping$0[distributionListPrivacyMode.ordinal()];
        if (i == 1) {
            AllExceptFragment.Companion.createAsDialog().show(chooseInitialMyStoryMembershipBottomSheetDialogFragment.getChildFragmentManager(), "selection_fragment");
        } else if (i == 2) {
            OnlyShareWithFragment.Companion.createAsDialog().show(chooseInitialMyStoryMembershipBottomSheetDialogFragment.getChildFragmentManager(), "selection_fragment");
        }
    }
}
