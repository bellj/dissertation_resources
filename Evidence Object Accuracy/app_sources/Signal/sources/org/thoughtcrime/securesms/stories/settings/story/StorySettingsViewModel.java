package org.thoughtcrime.securesms.stories.settings.story;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: StorySettingsViewModel.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0011B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u000e\u001a\u00020\u000fH\u0014J\u0006\u0010\u0010\u001a\u00020\u000fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsRepository;", "(Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "refresh", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StorySettingsViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final StorySettingsRepository repository;
    private final LiveData<StorySettingsState> state;
    private final Store<StorySettingsState> store;

    public StorySettingsViewModel(StorySettingsRepository storySettingsRepository) {
        Intrinsics.checkNotNullParameter(storySettingsRepository, "repository");
        this.repository = storySettingsRepository;
        Store<StorySettingsState> store = new Store<>(new StorySettingsState(null, 1, null));
        this.store = store;
        LiveData<StorySettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<StorySettingsState> getState() {
        return this.state;
    }

    public final void refresh() {
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.repository.getPrivateStories().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.settings.story.StorySettingsViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StorySettingsViewModel.m2965refresh$lambda1(StorySettingsViewModel.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getPrivateSto…= privateStories) }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* renamed from: refresh$lambda-1 */
    public static final void m2965refresh$lambda1(StorySettingsViewModel storySettingsViewModel, List list) {
        Intrinsics.checkNotNullParameter(storySettingsViewModel, "this$0");
        storySettingsViewModel.store.update(new Function(list) { // from class: org.thoughtcrime.securesms.stories.settings.story.StorySettingsViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StorySettingsViewModel.m2966refresh$lambda1$lambda0(this.f$0, (StorySettingsState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-1$lambda-0 */
    public static final StorySettingsState m2966refresh$lambda1$lambda0(List list, StorySettingsState storySettingsState) {
        Intrinsics.checkNotNullExpressionValue(list, "privateStories");
        return storySettingsState.copy(list);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    /* compiled from: StorySettingsViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsRepository;", "(Lorg/thoughtcrime/securesms/stories/settings/story/StorySettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final StorySettingsRepository repository;

        public Factory(StorySettingsRepository storySettingsRepository) {
            Intrinsics.checkNotNullParameter(storySettingsRepository, "repository");
            this.repository = storySettingsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StorySettingsViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.settings.story.StorySettingsViewModel.Factory.create");
        }
    }
}
