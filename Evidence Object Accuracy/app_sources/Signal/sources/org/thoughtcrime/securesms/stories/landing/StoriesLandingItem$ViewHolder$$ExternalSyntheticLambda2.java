package org.thoughtcrime.securesms.stories.landing;

import android.view.View;
import org.thoughtcrime.securesms.stories.landing.StoriesLandingItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoriesLandingItem$ViewHolder$$ExternalSyntheticLambda2 implements View.OnLongClickListener {
    public final /* synthetic */ StoriesLandingItem.ViewHolder f$0;
    public final /* synthetic */ StoriesLandingItem.Model f$1;

    public /* synthetic */ StoriesLandingItem$ViewHolder$$ExternalSyntheticLambda2(StoriesLandingItem.ViewHolder viewHolder, StoriesLandingItem.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return StoriesLandingItem.ViewHolder.m2809setUpClickListeners$lambda6(this.f$0, this.f$1, view);
    }
}
