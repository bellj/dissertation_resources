package org.thoughtcrime.securesms.stories.settings.custom;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;

/* compiled from: PrivateStorySettingsState.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001f\u0010\r\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u00052\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/custom/PrivateStorySettingsState;", "", "privateStory", "Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "areRepliesAndReactionsEnabled", "", "(Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;Z)V", "getAreRepliesAndReactionsEnabled", "()Z", "getPrivateStory", "()Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class PrivateStorySettingsState {
    private final boolean areRepliesAndReactionsEnabled;
    private final DistributionListRecord privateStory;

    public PrivateStorySettingsState() {
        this(null, false, 3, null);
    }

    public static /* synthetic */ PrivateStorySettingsState copy$default(PrivateStorySettingsState privateStorySettingsState, DistributionListRecord distributionListRecord, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            distributionListRecord = privateStorySettingsState.privateStory;
        }
        if ((i & 2) != 0) {
            z = privateStorySettingsState.areRepliesAndReactionsEnabled;
        }
        return privateStorySettingsState.copy(distributionListRecord, z);
    }

    public final DistributionListRecord component1() {
        return this.privateStory;
    }

    public final boolean component2() {
        return this.areRepliesAndReactionsEnabled;
    }

    public final PrivateStorySettingsState copy(DistributionListRecord distributionListRecord, boolean z) {
        return new PrivateStorySettingsState(distributionListRecord, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PrivateStorySettingsState)) {
            return false;
        }
        PrivateStorySettingsState privateStorySettingsState = (PrivateStorySettingsState) obj;
        return Intrinsics.areEqual(this.privateStory, privateStorySettingsState.privateStory) && this.areRepliesAndReactionsEnabled == privateStorySettingsState.areRepliesAndReactionsEnabled;
    }

    public int hashCode() {
        DistributionListRecord distributionListRecord = this.privateStory;
        int hashCode = (distributionListRecord == null ? 0 : distributionListRecord.hashCode()) * 31;
        boolean z = this.areRepliesAndReactionsEnabled;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        return "PrivateStorySettingsState(privateStory=" + this.privateStory + ", areRepliesAndReactionsEnabled=" + this.areRepliesAndReactionsEnabled + ')';
    }

    public PrivateStorySettingsState(DistributionListRecord distributionListRecord, boolean z) {
        this.privateStory = distributionListRecord;
        this.areRepliesAndReactionsEnabled = z;
    }

    public /* synthetic */ PrivateStorySettingsState(DistributionListRecord distributionListRecord, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : distributionListRecord, (i & 2) != 0 ? false : z);
    }

    public final DistributionListRecord getPrivateStory() {
        return this.privateStory;
    }

    public final boolean getAreRepliesAndReactionsEnabled() {
        return this.areRepliesAndReactionsEnabled;
    }
}
