package org.thoughtcrime.securesms.stories.viewer.page;

import android.view.ViewStub;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.stories.StoryFirstTimeNavigationView;
import org.thoughtcrime.securesms.util.views.Stub;

/* compiled from: StoryFirstNavigationStub.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\n\u001a\u00020\u0002H\u0016J\u0006\u0010\u000b\u001a\u00020\fJ\u0006\u0010\r\u001a\u00020\u000eJ\u0010\u0010\u000f\u001a\u00020\f2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u000e\u0010\u0010\u001a\u00020\f2\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\u000eR\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/page/StoryFirstNavigationStub;", "Lorg/thoughtcrime/securesms/util/views/Stub;", "Lorg/thoughtcrime/securesms/stories/StoryFirstTimeNavigationView;", "viewStub", "Landroid/view/ViewStub;", "(Landroid/view/ViewStub;)V", "blurHash", "Lorg/thoughtcrime/securesms/blurhash/BlurHash;", "callback", "Lorg/thoughtcrime/securesms/stories/StoryFirstTimeNavigationView$Callback;", "get", "hide", "", "isVisible", "", "setBlurHash", "setCallback", "showIfAble", "ableToShow", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryFirstNavigationStub extends Stub<StoryFirstTimeNavigationView> {
    private BlurHash blurHash;
    private StoryFirstTimeNavigationView.Callback callback;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryFirstNavigationStub(ViewStub viewStub) {
        super(viewStub);
        Intrinsics.checkNotNullParameter(viewStub, "viewStub");
    }

    public final void setCallback(StoryFirstTimeNavigationView.Callback callback) {
        Intrinsics.checkNotNullParameter(callback, "callback");
        if (resolved()) {
            get().setCallback(callback);
        } else {
            this.callback = callback;
        }
    }

    public final void setBlurHash(BlurHash blurHash) {
        if (resolved()) {
            get().setBlurHash(blurHash);
        } else {
            this.blurHash = blurHash;
        }
    }

    public final void showIfAble(boolean z) {
        if (z) {
            get().show();
        }
    }

    public final boolean isVisible() {
        if (resolved()) {
            if (get().getVisibility() == 0) {
                return true;
            }
        }
        return false;
    }

    public final void hide() {
        if (resolved()) {
            get().hide();
        }
    }

    @Override // org.thoughtcrime.securesms.util.views.Stub
    public StoryFirstTimeNavigationView get() {
        boolean z = !resolved();
        StoryFirstTimeNavigationView storyFirstTimeNavigationView = (StoryFirstTimeNavigationView) super.get();
        if (z) {
            storyFirstTimeNavigationView.setBlurHash(this.blurHash);
            storyFirstTimeNavigationView.setCallback(this.callback);
            this.blurHash = null;
            this.callback = null;
        }
        Intrinsics.checkNotNullExpressionValue(storyFirstTimeNavigationView, "view");
        return storyFirstTimeNavigationView;
    }
}
