package org.thoughtcrime.securesms.stories;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: StoryVolumeOverlayView.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u0011R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/stories/StoryVolumeOverlayView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "videoHasNoAudioIndicator", "Landroid/view/View;", "volumeBar", "Lorg/thoughtcrime/securesms/stories/StoryVolumeBar;", "setVideoHaNoAudio", "", "videoHasNoAudio", "", "setVolumeLevel", "level", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryVolumeOverlayView extends ConstraintLayout {
    private final View videoHasNoAudioIndicator;
    private final StoryVolumeBar volumeBar;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public StoryVolumeOverlayView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ StoryVolumeOverlayView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryVolumeOverlayView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        ViewGroup.inflate(context, R.layout.story_volume_overlay_view, this);
        View findViewById = findViewById(R.id.story_no_audio_indicator);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.story_no_audio_indicator)");
        this.videoHasNoAudioIndicator = findViewById;
        View findViewById2 = findViewById(R.id.story_volume_bar);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.story_volume_bar)");
        this.volumeBar = (StoryVolumeBar) findViewById2;
    }

    public final void setVideoHaNoAudio(boolean z) {
        ViewExtensionsKt.setVisible(this.videoHasNoAudioIndicator, z);
    }

    public final void setVolumeLevel(int i) {
        this.volumeBar.setLevel(i);
    }
}
