package org.thoughtcrime.securesms.stories.settings.create;

import android.os.Bundle;
import androidx.navigation.NavDirections;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes3.dex */
public class CreateStoryViewerSelectionFragmentDirections {
    private CreateStoryViewerSelectionFragmentDirections() {
    }

    public static ActionCreateStoryViewerSelectionToCreateStoryWithViewers actionCreateStoryViewerSelectionToCreateStoryWithViewers(RecipientId[] recipientIdArr) {
        return new ActionCreateStoryViewerSelectionToCreateStoryWithViewers(recipientIdArr);
    }

    /* loaded from: classes3.dex */
    public static class ActionCreateStoryViewerSelectionToCreateStoryWithViewers implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_createStoryViewerSelection_to_createStoryWithViewers;
        }

        private ActionCreateStoryViewerSelectionToCreateStoryWithViewers(RecipientId[] recipientIdArr) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (recipientIdArr != null) {
                hashMap.put(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, recipientIdArr);
                return;
            }
            throw new IllegalArgumentException("Argument \"recipients\" is marked as non-null but was passed a null value.");
        }

        public ActionCreateStoryViewerSelectionToCreateStoryWithViewers setRecipients(RecipientId[] recipientIdArr) {
            if (recipientIdArr != null) {
                this.arguments.put(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, recipientIdArr);
                return this;
            }
            throw new IllegalArgumentException("Argument \"recipients\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS)) {
                bundle.putParcelableArray(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, (RecipientId[]) this.arguments.get(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS));
            }
            return bundle;
        }

        public RecipientId[] getRecipients() {
            return (RecipientId[]) this.arguments.get(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionCreateStoryViewerSelectionToCreateStoryWithViewers actionCreateStoryViewerSelectionToCreateStoryWithViewers = (ActionCreateStoryViewerSelectionToCreateStoryWithViewers) obj;
            if (this.arguments.containsKey(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS) != actionCreateStoryViewerSelectionToCreateStoryWithViewers.arguments.containsKey(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS)) {
                return false;
            }
            if (getRecipients() == null ? actionCreateStoryViewerSelectionToCreateStoryWithViewers.getRecipients() == null : getRecipients().equals(actionCreateStoryViewerSelectionToCreateStoryWithViewers.getRecipients())) {
                return getActionId() == actionCreateStoryViewerSelectionToCreateStoryWithViewers.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return ((Arrays.hashCode(getRecipients()) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionCreateStoryViewerSelectionToCreateStoryWithViewers(actionId=" + getActionId() + "){recipients=" + getRecipients() + "}";
        }
    }
}
