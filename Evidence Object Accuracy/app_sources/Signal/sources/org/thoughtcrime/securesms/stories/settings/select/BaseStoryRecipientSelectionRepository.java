package org.thoughtcrime.securesms.stories.settings.select;

import android.database.Cursor;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: BaseStoryRecipientSelectionRepository.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004J\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u00042\u0006\u0010\t\u001a\u00020\nJ\u001c\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/stories/settings/select/BaseStoryRecipientSelectionRepository;", "", "()V", "getAllSignalContacts", "Lio/reactivex/rxjava3/core/Single;", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getRecord", "Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "updateDistributionListMembership", "", "distributionListRecord", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BaseStoryRecipientSelectionRepository {
    public final Single<DistributionListRecord> getRecord(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        Single<DistributionListRecord> subscribeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionRepository$$ExternalSyntheticLambda0
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return BaseStoryRecipientSelectionRepository.m2943$r8$lambda$LJ4X8U0a69uOlX5AXn31od8ydI(DistributionListId.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      Sig…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getRecord$lambda-0 */
    public static final DistributionListRecord m2946getRecord$lambda0(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "$distributionListId");
        DistributionListRecord list = SignalDatabase.Companion.distributionLists().getList(distributionListId);
        if (list != null) {
            return list;
        }
        throw new IllegalStateException("Record does not exist.".toString());
    }

    public final void updateDistributionListMembership(DistributionListRecord distributionListRecord, Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(distributionListRecord, "distributionListRecord");
        Intrinsics.checkNotNullParameter(set, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        SignalExecutors.BOUNDED.execute(new Runnable(set) { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ Set f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BaseStoryRecipientSelectionRepository.m2942$r8$lambda$4rApkLEyTQi_oDh2u76Ea0zIoU(DistributionListRecord.this, this.f$1);
            }
        });
    }

    /* renamed from: updateDistributionListMembership$lambda-3 */
    public static final void m2947updateDistributionListMembership$lambda3(DistributionListRecord distributionListRecord, Set set) {
        Intrinsics.checkNotNullParameter(distributionListRecord, "$distributionListRecord");
        Intrinsics.checkNotNullParameter(set, "$recipients");
        Set set2 = CollectionsKt___CollectionsKt.toSet(SignalDatabase.Companion.distributionLists().getRawMembers(distributionListRecord.getId(), distributionListRecord.getPrivacyMode()));
        Set<RecipientId> set3 = SetsKt___SetsKt.minus(set2, (Iterable) set);
        Set<RecipientId> set4 = SetsKt___SetsKt.minus(set, (Iterable) set2);
        for (RecipientId recipientId : set3) {
            SignalDatabase.Companion.distributionLists().removeMemberFromList(distributionListRecord.getId(), distributionListRecord.getPrivacyMode(), recipientId);
        }
        for (RecipientId recipientId2 : set4) {
            SignalDatabase.Companion.distributionLists().addMemberToList(distributionListRecord.getId(), distributionListRecord.getPrivacyMode(), recipientId2);
        }
        Stories.INSTANCE.onStorySettingsChanged(distributionListRecord.getId());
    }

    public final Single<Set<RecipientId>> getAllSignalContacts() {
        Single<Set<RecipientId>> subscribeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.stories.settings.select.BaseStoryRecipientSelectionRepository$$ExternalSyntheticLambda1
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return BaseStoryRecipientSelectionRepository.m2944$r8$lambda$db822e8nTvbcojCL5s9B6ZpWCs();
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      Sig…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getAllSignalContacts$lambda-5 */
    public static final Set m2945getAllSignalContacts$lambda5() {
        Cursor signalContacts = SignalDatabase.Companion.recipients().getSignalContacts(false);
        if (signalContacts == null) {
            return SetsKt__SetsKt.emptySet();
        }
        th = null;
        try {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            while (signalContacts.moveToNext()) {
                RecipientId from = RecipientId.from(CursorUtil.requireLong(signalContacts, "_id"));
                Intrinsics.checkNotNullExpressionValue(from, "from(CursorUtil.requireL…t, RecipientDatabase.ID))");
                linkedHashSet.add(from);
            }
            return linkedHashSet;
        } catch (Throwable th) {
            try {
                throw th;
            } finally {
                CloseableKt.closeFinally(signalContacts, th);
            }
        }
    }
}
