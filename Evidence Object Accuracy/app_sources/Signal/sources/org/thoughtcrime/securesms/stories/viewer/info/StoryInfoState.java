package org.thoughtcrime.securesms.stories.viewer.info;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.stories.viewer.info.StoryInfoRecipientRow;

/* compiled from: StoryInfoState.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001BG\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0007¢\u0006\u0002\u0010\fJ\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0007HÆ\u0003J\u000f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\n0\tHÆ\u0003J\t\u0010\u0019\u001a\u00020\u0007HÆ\u0003JK\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00072\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\b\b\u0002\u0010\u000b\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u001b\u001a\u00020\u00072\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001J\t\u0010\u001f\u001a\u00020 HÖ\u0001R\u0011\u0010\u000b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000f¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoState;", "", "sentMillis", "", "receivedMillis", MediaPreviewActivity.SIZE_EXTRA, "isOutgoing", "", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "", "Lorg/thoughtcrime/securesms/stories/viewer/info/StoryInfoRecipientRow$Model;", "isLoaded", "(JJJZLjava/util/List;Z)V", "()Z", "getReceivedMillis", "()J", "getRecipients", "()Ljava/util/List;", "getSentMillis", "getSize", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryInfoState {
    private final boolean isLoaded;
    private final boolean isOutgoing;
    private final long receivedMillis;
    private final List<StoryInfoRecipientRow.Model> recipients;
    private final long sentMillis;
    private final long size;

    public StoryInfoState() {
        this(0, 0, 0, false, null, false, 63, null);
    }

    public final long component1() {
        return this.sentMillis;
    }

    public final long component2() {
        return this.receivedMillis;
    }

    public final long component3() {
        return this.size;
    }

    public final boolean component4() {
        return this.isOutgoing;
    }

    public final List<StoryInfoRecipientRow.Model> component5() {
        return this.recipients;
    }

    public final boolean component6() {
        return this.isLoaded;
    }

    public final StoryInfoState copy(long j, long j2, long j3, boolean z, List<StoryInfoRecipientRow.Model> list, boolean z2) {
        Intrinsics.checkNotNullParameter(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        return new StoryInfoState(j, j2, j3, z, list, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoryInfoState)) {
            return false;
        }
        StoryInfoState storyInfoState = (StoryInfoState) obj;
        return this.sentMillis == storyInfoState.sentMillis && this.receivedMillis == storyInfoState.receivedMillis && this.size == storyInfoState.size && this.isOutgoing == storyInfoState.isOutgoing && Intrinsics.areEqual(this.recipients, storyInfoState.recipients) && this.isLoaded == storyInfoState.isLoaded;
    }

    public int hashCode() {
        int m = ((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.sentMillis) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.receivedMillis)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.size)) * 31;
        boolean z = this.isOutgoing;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int hashCode = (((m + i2) * 31) + this.recipients.hashCode()) * 31;
        boolean z2 = this.isLoaded;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        return hashCode + i;
    }

    public String toString() {
        return "StoryInfoState(sentMillis=" + this.sentMillis + ", receivedMillis=" + this.receivedMillis + ", size=" + this.size + ", isOutgoing=" + this.isOutgoing + ", recipients=" + this.recipients + ", isLoaded=" + this.isLoaded + ')';
    }

    public StoryInfoState(long j, long j2, long j3, boolean z, List<StoryInfoRecipientRow.Model> list, boolean z2) {
        Intrinsics.checkNotNullParameter(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        this.sentMillis = j;
        this.receivedMillis = j2;
        this.size = j3;
        this.isOutgoing = z;
        this.recipients = list;
        this.isLoaded = z2;
    }

    public final long getSentMillis() {
        return this.sentMillis;
    }

    public final long getReceivedMillis() {
        return this.receivedMillis;
    }

    public final long getSize() {
        return this.size;
    }

    public final boolean isOutgoing() {
        return this.isOutgoing;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ StoryInfoState(long r11, long r13, long r15, boolean r17, java.util.List r18, boolean r19, int r20, kotlin.jvm.internal.DefaultConstructorMarker r21) {
        /*
            r10 = this;
            r0 = r20 & 1
            r1 = -1
            if (r0 == 0) goto L_0x0008
            r3 = r1
            goto L_0x0009
        L_0x0008:
            r3 = r11
        L_0x0009:
            r0 = r20 & 2
            if (r0 == 0) goto L_0x000f
            r5 = r1
            goto L_0x0010
        L_0x000f:
            r5 = r13
        L_0x0010:
            r0 = r20 & 4
            if (r0 == 0) goto L_0x0015
            goto L_0x0016
        L_0x0015:
            r1 = r15
        L_0x0016:
            r0 = r20 & 8
            r7 = 0
            if (r0 == 0) goto L_0x001d
            r0 = 0
            goto L_0x001f
        L_0x001d:
            r0 = r17
        L_0x001f:
            r8 = r20 & 16
            if (r8 == 0) goto L_0x0028
            java.util.List r8 = kotlin.collections.CollectionsKt.emptyList()
            goto L_0x002a
        L_0x0028:
            r8 = r18
        L_0x002a:
            r9 = r20 & 32
            if (r9 == 0) goto L_0x002f
            goto L_0x0031
        L_0x002f:
            r7 = r19
        L_0x0031:
            r11 = r10
            r12 = r3
            r14 = r5
            r16 = r1
            r18 = r0
            r19 = r8
            r20 = r7
            r11.<init>(r12, r14, r16, r18, r19, r20)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stories.viewer.info.StoryInfoState.<init>(long, long, long, boolean, java.util.List, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final List<StoryInfoRecipientRow.Model> getRecipients() {
        return this.recipients;
    }

    public final boolean isLoaded() {
        return this.isLoaded;
    }
}
