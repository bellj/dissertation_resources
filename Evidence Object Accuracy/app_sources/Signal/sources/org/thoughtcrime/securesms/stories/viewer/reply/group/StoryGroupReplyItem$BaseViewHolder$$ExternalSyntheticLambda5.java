package org.thoughtcrime.securesms.stories.viewer.reply.group;

import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda5 implements Runnable {
    public final /* synthetic */ StoryGroupReplyItem.BaseViewHolder f$0;

    public /* synthetic */ StoryGroupReplyItem$BaseViewHolder$$ExternalSyntheticLambda5(StoryGroupReplyItem.BaseViewHolder baseViewHolder) {
        this.f$0 = baseViewHolder;
    }

    @Override // java.lang.Runnable
    public final void run() {
        StoryGroupReplyItem.BaseViewHolder.m3149bind$lambda2(this.f$0);
    }
}
