package org.thoughtcrime.securesms.stories.settings.story;

import android.view.View;
import org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class PrivateStoryItem$RecipientViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ PrivateStoryItem.RecipientModel f$0;

    public /* synthetic */ PrivateStoryItem$RecipientViewHolder$$ExternalSyntheticLambda0(PrivateStoryItem.RecipientModel recipientModel) {
        this.f$0 = recipientModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        PrivateStoryItem.RecipientViewHolder.m2958bind$lambda0(this.f$0, view);
    }
}
