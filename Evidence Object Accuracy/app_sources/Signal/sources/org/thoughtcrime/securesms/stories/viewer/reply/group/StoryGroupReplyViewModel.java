package org.thoughtcrime.securesms.stories.viewer.reply.group;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.paging.ObservablePagedData;
import org.signal.paging.ProxyPagingController;
import org.thoughtcrime.securesms.conversation.colors.NameColor;
import org.thoughtcrime.securesms.conversation.colors.NameColors;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyState;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: StoryGroupReplyViewModel.kt */
@Metadata(d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u001fB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u001d\u001a\u00020\u001eH\u0014R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR \u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00020\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u000fX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0018\u001a\u00020\u0015¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00150\u001cX\u0004¢\u0006\u0002\n\u0000¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyViewModel;", "Landroidx/lifecycle/ViewModel;", "storyId", "", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyRepository;", "(JLorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "pagingController", "Lorg/signal/paging/ProxyPagingController;", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "getPagingController", "()Lorg/signal/paging/ProxyPagingController;", "sessionMemberCache", "", "Lorg/thoughtcrime/securesms/groups/GroupId;", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyState;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "stateSnapshot", "getStateSnapshot", "()Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyState;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "onCleared", "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StoryGroupReplyViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final ProxyPagingController<MessageId> pagingController = new ProxyPagingController<>();
    private final Map<GroupId, Set<Recipient>> sessionMemberCache;
    private final Flowable<StoryGroupReplyState> state;
    private final StoryGroupReplyState stateSnapshot;
    private final RxStore<StoryGroupReplyState> store;

    public StoryGroupReplyViewModel(long j, StoryGroupReplyRepository storyGroupReplyRepository) {
        Intrinsics.checkNotNullParameter(storyGroupReplyRepository, "repository");
        Map<GroupId, Set<Recipient>> createSessionMembersCache = NameColors.INSTANCE.createSessionMembersCache();
        this.sessionMemberCache = createSessionMembersCache;
        RxStore<StoryGroupReplyState> rxStore = new RxStore<>(new StoryGroupReplyState(0, null, null, null, 15, null), null, 2, null);
        this.store = rxStore;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        this.stateSnapshot = rxStore.getState();
        this.state = rxStore.getStateFlowable();
        Disposable subscribe = storyGroupReplyRepository.getThreadId(j).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryGroupReplyViewModel.m3182_init_$lambda0(StoryGroupReplyViewModel.this, (Long) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getThreadId(s…eadId = threadId) }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
        Observable<R> flatMap = storyGroupReplyRepository.getPagedReplies(j).doOnNext(new Consumer() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                StoryGroupReplyViewModel.m3183_init_$lambda1(StoryGroupReplyViewModel.this, (ObservablePagedData) obj);
            }
        }).flatMap(new Function() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryGroupReplyViewModel.m3184_init_$lambda2((ObservablePagedData) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMap, "repository.getPagedRepli…     .flatMap { it.data }");
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy$default(flatMap, (Function1) null, (Function0) null, new Function1<List<ReplyBody>, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyViewModel.4
            final /* synthetic */ StoryGroupReplyViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(List<ReplyBody> list) {
                invoke(list);
                return Unit.INSTANCE;
            }

            public final void invoke(final List<ReplyBody> list) {
                this.this$0.store.update(new Function1<StoryGroupReplyState, StoryGroupReplyState>() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyViewModel.4.1
                    public final StoryGroupReplyState invoke(StoryGroupReplyState storyGroupReplyState) {
                        Intrinsics.checkNotNullParameter(storyGroupReplyState, "state");
                        List<ReplyBody> list2 = list;
                        Intrinsics.checkNotNullExpressionValue(list2, "data");
                        return StoryGroupReplyState.copy$default(storyGroupReplyState, 0, list2, null, StoryGroupReplyState.LoadState.READY, 5, null);
                    }
                });
            }
        }, 3, (Object) null));
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy$default(storyGroupReplyRepository.getNameColorsMap(j, createSessionMembersCache), (Function1) null, (Function0) null, new Function1<Map<RecipientId, ? extends NameColor>, Unit>(this) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyViewModel.5
            final /* synthetic */ StoryGroupReplyViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Map<RecipientId, ? extends NameColor> map) {
                invoke((Map<RecipientId, NameColor>) map);
                return Unit.INSTANCE;
            }

            public final void invoke(final Map<RecipientId, NameColor> map) {
                Intrinsics.checkNotNullParameter(map, "nameColors");
                this.this$0.store.update(new Function1<StoryGroupReplyState, StoryGroupReplyState>() { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyViewModel.5.1
                    public final StoryGroupReplyState invoke(StoryGroupReplyState storyGroupReplyState) {
                        Intrinsics.checkNotNullParameter(storyGroupReplyState, "state");
                        return StoryGroupReplyState.copy$default(storyGroupReplyState, 0, null, map, null, 11, null);
                    }
                });
            }
        }, 3, (Object) null));
    }

    public final StoryGroupReplyState getStateSnapshot() {
        return this.stateSnapshot;
    }

    public final Flowable<StoryGroupReplyState> getState() {
        return this.state;
    }

    public final ProxyPagingController<MessageId> getPagingController() {
        return this.pagingController;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m3182_init_$lambda0(StoryGroupReplyViewModel storyGroupReplyViewModel, Long l) {
        Intrinsics.checkNotNullParameter(storyGroupReplyViewModel, "this$0");
        storyGroupReplyViewModel.store.update(new Function1<StoryGroupReplyState, StoryGroupReplyState>(l) { // from class: org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyViewModel$1$1
            final /* synthetic */ Long $threadId;

            /* access modifiers changed from: package-private */
            {
                this.$threadId = r1;
            }

            public final StoryGroupReplyState invoke(StoryGroupReplyState storyGroupReplyState) {
                Intrinsics.checkNotNullParameter(storyGroupReplyState, "it");
                Long l2 = this.$threadId;
                Intrinsics.checkNotNullExpressionValue(l2, "threadId");
                return StoryGroupReplyState.copy$default(storyGroupReplyState, l2.longValue(), null, null, null, 14, null);
            }
        });
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: org.signal.paging.ProxyPagingController<org.thoughtcrime.securesms.database.model.MessageId> */
    /* JADX WARN: Multi-variable type inference failed */
    /* renamed from: _init_$lambda-1 */
    public static final void m3183_init_$lambda1(StoryGroupReplyViewModel storyGroupReplyViewModel, ObservablePagedData observablePagedData) {
        Intrinsics.checkNotNullParameter(storyGroupReplyViewModel, "this$0");
        storyGroupReplyViewModel.pagingController.set(observablePagedData.getController());
    }

    /* renamed from: _init_$lambda-2 */
    public static final ObservableSource m3184_init_$lambda2(ObservablePagedData observablePagedData) {
        return observablePagedData.getData();
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    /* compiled from: StoryGroupReplyViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "storyId", "", "repository", "Lorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyRepository;", "(JLorg/thoughtcrime/securesms/stories/viewer/reply/group/StoryGroupReplyRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final StoryGroupReplyRepository repository;
        private final long storyId;

        public Factory(long j, StoryGroupReplyRepository storyGroupReplyRepository) {
            Intrinsics.checkNotNullParameter(storyGroupReplyRepository, "repository");
            this.storyId = j;
            this.repository = storyGroupReplyRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StoryGroupReplyViewModel(this.storyId, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.stories.viewer.reply.group.StoryGroupReplyViewModel.Factory.create");
        }
    }
}
