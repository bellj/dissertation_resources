package org.thoughtcrime.securesms.proxy;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.preferences.EditProxyViewModel;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public final class ProxyBottomSheetFragment extends BottomSheetDialogFragment {
    private static final String ARG_PROXY_LINK;
    private static final String TAG = Log.tag(ProxyBottomSheetFragment.class);
    private View cancelButton;
    private TextView proxyText;
    private CircularProgressMaterialButton useProxyButton;
    private EditProxyViewModel viewModel;

    public static void showForProxy(FragmentManager fragmentManager, String str) {
        ProxyBottomSheetFragment proxyBottomSheetFragment = new ProxyBottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_PROXY_LINK, str);
        proxyBottomSheetFragment.setArguments(bundle);
        proxyBottomSheetFragment.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, ThemeUtil.isDarkTheme(requireContext()) ? R.style.Theme_Signal_RoundedBottomSheet : R.style.Theme_Signal_RoundedBottomSheet_Light);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.proxy_bottom_sheet, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.proxyText = (TextView) view.findViewById(R.id.proxy_sheet_host);
        this.useProxyButton = (CircularProgressMaterialButton) view.findViewById(R.id.proxy_sheet_use_proxy);
        this.cancelButton = view.findViewById(R.id.proxy_sheet_cancel);
        String string = getArguments().getString(ARG_PROXY_LINK);
        this.proxyText.setText(string);
        initViewModel();
        this.useProxyButton.setOnClickListener(new View.OnClickListener(string) { // from class: org.thoughtcrime.securesms.proxy.ProxyBottomSheetFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ProxyBottomSheetFragment.$r8$lambda$GzAN0GY3HdIFKHt_aBvFLPQf0EQ(ProxyBottomSheetFragment.this, this.f$1, view2);
            }
        });
        this.cancelButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.proxy.ProxyBottomSheetFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ProxyBottomSheetFragment.m2503$r8$lambda$ftXMDQcKIDcdcrMlPw1l41fg9o(ProxyBottomSheetFragment.this, view2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(String str, View view) {
        this.viewModel.onSaveClicked(str);
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        dismiss();
    }

    private void initViewModel() {
        EditProxyViewModel editProxyViewModel = (EditProxyViewModel) ViewModelProviders.of(this).get(EditProxyViewModel.class);
        this.viewModel = editProxyViewModel;
        editProxyViewModel.getSaveState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.proxy.ProxyBottomSheetFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ProxyBottomSheetFragment.$r8$lambda$AkCFNk6hyp3wwC8L3xm5AEWJNfg(ProxyBottomSheetFragment.this, (EditProxyViewModel.SaveState) obj);
            }
        });
        this.viewModel.getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.proxy.ProxyBottomSheetFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ProxyBottomSheetFragment.$r8$lambda$8ZuRI0xFO4ahj_WjUOHXpvYdIbM(ProxyBottomSheetFragment.this, (EditProxyViewModel.Event) obj);
            }
        });
    }

    public void presentSaveState(EditProxyViewModel.SaveState saveState) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$SaveState[saveState.ordinal()];
        if (i == 1) {
            this.useProxyButton.cancelSpinning();
        } else if (i == 2) {
            this.useProxyButton.setSpinning();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.proxy.ProxyBottomSheetFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$Event;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$SaveState;

        static {
            int[] iArr = new int[EditProxyViewModel.Event.values().length];
            $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$Event = iArr;
            try {
                iArr[EditProxyViewModel.Event.PROXY_SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$Event[EditProxyViewModel.Event.PROXY_FAILURE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[EditProxyViewModel.SaveState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$SaveState = iArr2;
            try {
                iArr2[EditProxyViewModel.SaveState.IDLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$SaveState[EditProxyViewModel.SaveState.IN_PROGRESS.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public void presentEvents(EditProxyViewModel.Event event) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$Event[event.ordinal()];
        if (i == 1) {
            Toast.makeText(requireContext(), (int) R.string.ProxyBottomSheetFragment_successfully_connected_to_proxy, 1).show();
            dismiss();
        } else if (i == 2) {
            new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.preferences_failed_to_connect).setMessage(R.string.preferences_couldnt_connect_to_the_proxy).setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.proxy.ProxyBottomSheetFragment$$ExternalSyntheticLambda4
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    ProxyBottomSheetFragment.m2502$r8$lambda$cFdlyJw0lXVC5ORMUhdxLuHgEU(dialogInterface, i2);
                }
            }).show();
            dismiss();
        }
    }
}
