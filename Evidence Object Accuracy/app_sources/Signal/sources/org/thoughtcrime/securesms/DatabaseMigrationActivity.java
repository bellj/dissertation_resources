package org.thoughtcrime.securesms;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import org.thoughtcrime.securesms.database.SmsMigrator;
import org.thoughtcrime.securesms.service.ApplicationMigrationService;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes.dex */
public class DatabaseMigrationActivity extends PassphraseRequiredActivity {
    private final BroadcastReceiver completedReceiver = new NullReceiver();
    private Button importButton;
    private ApplicationMigrationService importService;
    private final ImportStateHandler importStateHandler = new ImportStateHandler();
    private boolean isVisible = false;
    private ProgressBar progress;
    private TextView progressLabel;
    private LinearLayout progressLayout;
    private LinearLayout promptLayout;
    private final ImportServiceConnection serviceConnection = new ImportServiceConnection();
    private Button skipButton;

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.database_migration_activity);
        initializeResources();
        initializeServiceBinding();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.isVisible = true;
        registerForCompletedNotification();
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        this.isVisible = false;
        unregisterForCompletedNotification();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        shutdownServiceBinding();
    }

    private void initializeServiceBinding() {
        bindService(new Intent(this, ApplicationMigrationService.class), this.serviceConnection, 1);
    }

    private void initializeResources() {
        this.promptLayout = (LinearLayout) findViewById(R.id.prompt_layout);
        this.progressLayout = (LinearLayout) findViewById(R.id.progress_layout);
        this.skipButton = (Button) findViewById(R.id.skip_button);
        this.importButton = (Button) findViewById(R.id.import_button);
        this.progress = (ProgressBar) findViewById(R.id.import_progress);
        this.progressLabel = (TextView) findViewById(R.id.import_status);
        this.progressLayout.setVisibility(8);
        this.promptLayout.setVisibility(8);
        this.importButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.DatabaseMigrationActivity.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Intent intent = new Intent(DatabaseMigrationActivity.this, ApplicationMigrationService.class);
                intent.setAction(ApplicationMigrationService.MIGRATE_DATABASE);
                intent.putExtra("master_secret", DatabaseMigrationActivity.this.getIntent().getParcelableExtra("master_secret"));
                DatabaseMigrationActivity.this.startService(intent);
                DatabaseMigrationActivity.this.promptLayout.setVisibility(8);
                DatabaseMigrationActivity.this.progressLayout.setVisibility(0);
            }
        });
        this.skipButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.DatabaseMigrationActivity.2
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                ApplicationMigrationService.setDatabaseImported(DatabaseMigrationActivity.this);
                DatabaseMigrationActivity.this.handleImportComplete();
            }
        });
    }

    private void registerForCompletedNotification() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ApplicationMigrationService.COMPLETED_ACTION);
        intentFilter.setPriority(MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH);
        registerReceiver(this.completedReceiver, intentFilter);
    }

    private void unregisterForCompletedNotification() {
        unregisterReceiver(this.completedReceiver);
    }

    private void shutdownServiceBinding() {
        unbindService(this.serviceConnection);
    }

    public void handleStateIdle() {
        this.promptLayout.setVisibility(0);
        this.progressLayout.setVisibility(8);
    }

    public void handleStateProgress(SmsMigrator.ProgressDescription progressDescription) {
        this.promptLayout.setVisibility(8);
        this.progressLayout.setVisibility(0);
        TextView textView = this.progressLabel;
        textView.setText(progressDescription.primaryComplete + "/" + progressDescription.primaryTotal);
        double max = (double) this.progress.getMax();
        double d = (double) progressDescription.primaryTotal;
        double d2 = (double) progressDescription.primaryComplete;
        double d3 = (double) progressDescription.secondaryTotal;
        double d4 = (double) progressDescription.secondaryComplete;
        ProgressBar progressBar = this.progress;
        Double.isNaN(d2);
        Double.isNaN(d);
        Double.isNaN(max);
        progressBar.setProgress((int) Math.round((d2 / d) * max));
        ProgressBar progressBar2 = this.progress;
        Double.isNaN(d4);
        Double.isNaN(d3);
        Double.isNaN(max);
        progressBar2.setSecondaryProgress((int) Math.round((d4 / d3) * max));
    }

    public void handleImportComplete() {
        if (this.isVisible) {
            if (getIntent().hasExtra("next_intent")) {
                startActivity((Intent) getIntent().getParcelableExtra("next_intent"));
            } else {
                startActivity(MainActivity.clearTop(this));
            }
        }
        finish();
    }

    /* loaded from: classes.dex */
    public class ImportStateHandler extends Handler {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ImportStateHandler() {
            super(Looper.getMainLooper());
            DatabaseMigrationActivity.this = r1;
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                DatabaseMigrationActivity.this.handleStateIdle();
            } else if (i == 2) {
                DatabaseMigrationActivity.this.handleStateProgress((SmsMigrator.ProgressDescription) message.obj);
            } else if (i == 3) {
                DatabaseMigrationActivity.this.handleImportComplete();
            }
        }
    }

    /* loaded from: classes.dex */
    public class ImportServiceConnection implements ServiceConnection {
        private ImportServiceConnection() {
            DatabaseMigrationActivity.this = r1;
        }

        @Override // android.content.ServiceConnection
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            DatabaseMigrationActivity.this.importService = ((ApplicationMigrationService.ApplicationMigrationBinder) iBinder).getService();
            DatabaseMigrationActivity.this.importService.setImportStateHandler(DatabaseMigrationActivity.this.importStateHandler);
            ApplicationMigrationService.ImportState state = DatabaseMigrationActivity.this.importService.getState();
            DatabaseMigrationActivity.this.importStateHandler.obtainMessage(state.state, state.progress).sendToTarget();
        }

        @Override // android.content.ServiceConnection
        public void onServiceDisconnected(ComponentName componentName) {
            DatabaseMigrationActivity.this.importService.setImportStateHandler(null);
        }
    }

    /* loaded from: classes.dex */
    private static class NullReceiver extends BroadcastReceiver {
        private NullReceiver() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            abortBroadcast();
        }
    }
}
