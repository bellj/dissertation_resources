package org.thoughtcrime.securesms;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.ContextThemeWrapper;
import androidx.appcompat.app.AlertDialog;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes.dex */
public final class ClearAvatarPromptActivity extends Activity {
    private static final String ARG_TITLE;

    public static Intent createForUserProfilePhoto() {
        Intent intent = new Intent(ApplicationDependencies.getApplication(), ClearAvatarPromptActivity.class);
        intent.putExtra(ARG_TITLE, R.string.ClearProfileActivity_remove_profile_photo);
        return intent;
    }

    public static Intent createForGroupProfilePhoto() {
        Intent intent = new Intent(ApplicationDependencies.getApplication(), ClearAvatarPromptActivity.class);
        intent.putExtra(ARG_TITLE, R.string.ClearProfileActivity_remove_group_photo);
        return intent;
    }

    @Override // android.app.Activity
    public void onResume() {
        super.onResume();
        new AlertDialog.Builder(new ContextThemeWrapper(this, DynamicTheme.isDarkTheme(this) ? R.style.TextSecure_DarkTheme : R.style.TextSecure_LightTheme)).setMessage(getIntent().getIntExtra(ARG_TITLE, 0)).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.ClearAvatarPromptActivity$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ClearAvatarPromptActivity.$r8$lambda$SPI9cFbe9nt3SNZzMQP_YcDbXRY(ClearAvatarPromptActivity.this, dialogInterface, i);
            }
        }).setPositiveButton(R.string.ClearProfileActivity_remove, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.ClearAvatarPromptActivity$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ClearAvatarPromptActivity.m240$r8$lambda$UhhgUuGH6X0ZYbAs0dYuLZlvII(ClearAvatarPromptActivity.this, dialogInterface, i);
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() { // from class: org.thoughtcrime.securesms.ClearAvatarPromptActivity$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                ClearAvatarPromptActivity.$r8$lambda$uOqg43hqeI6yS1S1h_X7Hv6g1vo(ClearAvatarPromptActivity.this, dialogInterface);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onResume$0(DialogInterface dialogInterface, int i) {
        finish();
    }

    public /* synthetic */ void lambda$onResume$1(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.putExtra("delete", true);
        setResult(-1, intent);
        finish();
    }

    public /* synthetic */ void lambda$onResume$2(DialogInterface dialogInterface) {
        finish();
    }
}
