package org.thoughtcrime.securesms.events;

import org.thoughtcrime.securesms.attachments.Attachment;

/* loaded from: classes4.dex */
public final class PartProgressEvent {
    public final Attachment attachment;
    public final long progress;
    public final long total;
    public final Type type;

    /* loaded from: classes4.dex */
    public enum Type {
        COMPRESSION,
        NETWORK
    }

    public PartProgressEvent(Attachment attachment, Type type, long j, long j2) {
        this.attachment = attachment;
        this.type = type;
        this.total = j;
        this.progress = j2;
    }
}
