package org.thoughtcrime.securesms.events;

import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class GroupCallPeekEvent {
    private final long deviceCount;
    private final long deviceLimit;
    private final String eraId;
    private final RecipientId groupRecipientId;

    public GroupCallPeekEvent(RecipientId recipientId, String str, Long l, Long l2) {
        this.groupRecipientId = recipientId;
        this.eraId = str;
        long j = 0;
        this.deviceCount = l != null ? l.longValue() : 0;
        this.deviceLimit = l2 != null ? l2.longValue() : j;
    }

    public RecipientId getGroupRecipientId() {
        return this.groupRecipientId;
    }

    public boolean isOngoing() {
        return this.eraId != null && this.deviceCount > 0;
    }

    public boolean callHasCapacity() {
        return isOngoing() && this.deviceCount < this.deviceLimit;
    }
}
