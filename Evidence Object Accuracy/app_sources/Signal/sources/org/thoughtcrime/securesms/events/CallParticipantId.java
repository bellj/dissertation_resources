package org.thoughtcrime.securesms.events;

import java.util.Objects;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class CallParticipantId {
    public static final long DEFAULT_ID;
    private final long demuxId;
    private final RecipientId recipientId;

    public CallParticipantId(Recipient recipient) {
        this(-1, recipient.getId());
    }

    public CallParticipantId(long j, RecipientId recipientId) {
        this.demuxId = j;
        this.recipientId = recipientId;
    }

    public long getDemuxId() {
        return this.demuxId;
    }

    public RecipientId getRecipientId() {
        return this.recipientId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || CallParticipantId.class != obj.getClass()) {
            return false;
        }
        CallParticipantId callParticipantId = (CallParticipantId) obj;
        if (this.demuxId != callParticipantId.demuxId || !this.recipientId.equals(callParticipantId.recipientId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(Long.valueOf(this.demuxId), this.recipientId);
    }
}
