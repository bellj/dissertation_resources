package org.thoughtcrime.securesms.events;

import com.annimon.stream.OptionalLong;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__IndentKt;
import org.signal.ringrtc.CallId;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.ringrtc.CameraState;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.webrtc.PeerConnection;

/* compiled from: WebRtcViewModel.kt */
@Metadata(d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0002=>B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010:\u001a\u00020\u000eJ\b\u0010;\u001a\u00020<H\u0016R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0015\u001a\u00020\u0016¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0017\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001a0\n¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\fR\u0011\u0010\u001c\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0010R\u0011\u0010\u001d\u001a\u00020\u000e8F¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u0010R\u0011\u0010\u001e\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0010R\u0011\u0010\u001f\u001a\u00020 ¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0015\u0010#\u001a\u0004\u0018\u00010\u0012¢\u0006\n\n\u0002\u0010&\u001a\u0004\b$\u0010%R\u0011\u0010'\u001a\u00020(¢\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0011\u0010+\u001a\u00020,¢\u0006\b\n\u0000\u001a\u0004\b-\u0010.R\u0017\u0010/\u001a\b\u0012\u0004\u0012\u00020 00¢\u0006\b\n\u0000\u001a\u0004\b1\u00102R\u0013\u00103\u001a\u00020\u000e8G¢\u0006\b\n\u0000\u001a\u0004\b4\u0010\u0010R\u0011\u00105\u001a\u00020(¢\u0006\b\n\u0000\u001a\u0004\b6\u0010*R\u0011\u0010\u0002\u001a\u000207¢\u0006\b\n\u0000\u001a\u0004\b8\u00109¨\u0006?"}, d2 = {"Lorg/thoughtcrime/securesms/events/WebRtcViewModel;", "", "state", "Lorg/thoughtcrime/securesms/service/webrtc/state/WebRtcServiceState;", "(Lorg/thoughtcrime/securesms/service/webrtc/state/WebRtcServiceState;)V", "activeDevice", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "getActiveDevice", "()Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "availableDevices", "", "getAvailableDevices", "()Ljava/util/Set;", "bluetoothPermissionDenied", "", "getBluetoothPermissionDenied", "()Z", "callConnectedTime", "", "getCallConnectedTime", "()J", "groupState", "Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;", "getGroupState", "()Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;", "identityChangedParticipants", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getIdentityChangedParticipants", "isCellularConnection", "isRemoteVideoEnabled", "isRemoteVideoOffer", "localParticipant", "Lorg/thoughtcrime/securesms/events/CallParticipant;", "getLocalParticipant", "()Lorg/thoughtcrime/securesms/events/CallParticipant;", "participantLimit", "getParticipantLimit", "()Ljava/lang/Long;", "Ljava/lang/Long;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "remoteDevicesCount", "Lcom/annimon/stream/OptionalLong;", "getRemoteDevicesCount", "()Lcom/annimon/stream/OptionalLong;", "remoteParticipants", "", "getRemoteParticipants", "()Ljava/util/List;", "ringGroup", "shouldRingGroup", "ringerRecipient", "getRingerRecipient", "Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;", "getState", "()Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;", "areRemoteDevicesInCall", "toString", "", "GroupCallState", "State", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class WebRtcViewModel {
    private final SignalAudioManager.AudioDevice activeDevice;
    private final Set<SignalAudioManager.AudioDevice> availableDevices;
    private final boolean bluetoothPermissionDenied;
    private final long callConnectedTime;
    private final GroupCallState groupState;
    private final Set<RecipientId> identityChangedParticipants;
    private final boolean isCellularConnection;
    private final boolean isRemoteVideoOffer;
    private final CallParticipant localParticipant;
    private final Long participantLimit;
    private final Recipient recipient;
    private final OptionalLong remoteDevicesCount;
    private final List<CallParticipant> remoteParticipants;
    private final boolean ringGroup;
    private final Recipient ringerRecipient;
    private final State state;

    /* compiled from: WebRtcViewModel.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[PeerConnection.AdapterType.values().length];
            iArr[PeerConnection.AdapterType.UNKNOWN.ordinal()] = 1;
            iArr[PeerConnection.AdapterType.ETHERNET.ordinal()] = 2;
            iArr[PeerConnection.AdapterType.WIFI.ordinal()] = 3;
            iArr[PeerConnection.AdapterType.VPN.ordinal()] = 4;
            iArr[PeerConnection.AdapterType.LOOPBACK.ordinal()] = 5;
            iArr[PeerConnection.AdapterType.ADAPTER_TYPE_ANY.ordinal()] = 6;
            iArr[PeerConnection.AdapterType.CELLULAR.ordinal()] = 7;
            iArr[PeerConnection.AdapterType.CELLULAR_2G.ordinal()] = 8;
            iArr[PeerConnection.AdapterType.CELLULAR_3G.ordinal()] = 9;
            iArr[PeerConnection.AdapterType.CELLULAR_4G.ordinal()] = 10;
            iArr[PeerConnection.AdapterType.CELLULAR_5G.ordinal()] = 11;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public WebRtcViewModel(WebRtcServiceState webRtcServiceState) {
        boolean z;
        Intrinsics.checkNotNullParameter(webRtcServiceState, "state");
        this.state = webRtcServiceState.getCallInfoState().getCallState();
        this.groupState = webRtcServiceState.getCallInfoState().getGroupCallState();
        this.recipient = webRtcServiceState.getCallInfoState().getCallRecipient();
        RemotePeer activePeer = webRtcServiceState.getCallInfoState().getActivePeer();
        CallId callId = null;
        this.isRemoteVideoOffer = webRtcServiceState.getCallSetupState(activePeer != null ? activePeer.getCallId() : null).isRemoteVideoOffer();
        this.callConnectedTime = webRtcServiceState.getCallInfoState().getCallConnectedTime();
        this.remoteParticipants = webRtcServiceState.getCallInfoState().getRemoteCallParticipants();
        this.identityChangedParticipants = webRtcServiceState.getCallInfoState().getIdentityChangedRecipients();
        this.remoteDevicesCount = webRtcServiceState.getCallInfoState().getRemoteDevicesCount();
        this.participantLimit = webRtcServiceState.getCallInfoState().getParticipantLimit();
        RemotePeer activePeer2 = webRtcServiceState.getCallInfoState().getActivePeer();
        this.ringGroup = webRtcServiceState.getCallSetupState(activePeer2 != null ? activePeer2.getCallId() : null).shouldRingGroup();
        RemotePeer activePeer3 = webRtcServiceState.getCallInfoState().getActivePeer();
        this.ringerRecipient = webRtcServiceState.getCallSetupState(activePeer3 != null ? activePeer3.getCallId() : callId).getRingerRecipient();
        this.activeDevice = webRtcServiceState.getLocalDeviceState().getActiveDevice();
        this.availableDevices = webRtcServiceState.getLocalDeviceState().getAvailableDevices();
        this.bluetoothPermissionDenied = webRtcServiceState.getLocalDeviceState().getBluetoothPermissionDenied();
        CallParticipant.Companion companion = CallParticipant.Companion;
        CameraState cameraState = webRtcServiceState.getLocalDeviceState().getCameraState();
        BroadcastVideoSink localSink = webRtcServiceState.getVideoState().getLocalSink() != null ? webRtcServiceState.getVideoState().getLocalSink() : new BroadcastVideoSink();
        Intrinsics.checkNotNull(localSink);
        this.localParticipant = companion.createLocal(cameraState, localSink, webRtcServiceState.getLocalDeviceState().isMicrophoneEnabled());
        switch (WhenMappings.$EnumSwitchMapping$0[webRtcServiceState.getLocalDeviceState().getNetworkConnectionType().ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                z = false;
                break;
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                z = true;
                break;
            default:
                throw new NoWhenBranchMatchedException();
        }
        this.isCellularConnection = z;
    }

    /* compiled from: WebRtcViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0016\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0005R\u0011\u0010\u0006\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0005R\u0011\u0010\u0007\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\u0005j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016j\u0002\b\u0017j\u0002\b\u0018j\u0002\b\u0019¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;", "", "(Ljava/lang/String;I)V", "isErrorState", "", "()Z", "isPassedPreJoin", "isPreJoinOrNetworkUnavailable", "IDLE", "CALL_PRE_JOIN", "CALL_INCOMING", "CALL_OUTGOING", "CALL_CONNECTED", "CALL_RINGING", "CALL_BUSY", "CALL_DISCONNECTED", "CALL_DISCONNECTED_GLARE", "CALL_NEEDS_PERMISSION", "CALL_RECONNECTING", "NETWORK_FAILURE", "RECIPIENT_UNAVAILABLE", "NO_SUCH_USER", "UNTRUSTED_IDENTITY", "CALL_ACCEPTED_ELSEWHERE", "CALL_DECLINED_ELSEWHERE", "CALL_ONGOING_ELSEWHERE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum State {
        IDLE,
        CALL_PRE_JOIN,
        CALL_INCOMING,
        CALL_OUTGOING,
        CALL_CONNECTED,
        CALL_RINGING,
        CALL_BUSY,
        CALL_DISCONNECTED,
        CALL_DISCONNECTED_GLARE,
        CALL_NEEDS_PERMISSION,
        CALL_RECONNECTING,
        NETWORK_FAILURE,
        RECIPIENT_UNAVAILABLE,
        NO_SUCH_USER,
        UNTRUSTED_IDENTITY,
        CALL_ACCEPTED_ELSEWHERE,
        CALL_DECLINED_ELSEWHERE,
        CALL_ONGOING_ELSEWHERE;

        public final boolean isErrorState() {
            return this == NETWORK_FAILURE || this == RECIPIENT_UNAVAILABLE || this == NO_SUCH_USER || this == UNTRUSTED_IDENTITY;
        }

        public final boolean isPreJoinOrNetworkUnavailable() {
            return this == CALL_PRE_JOIN || this == NETWORK_FAILURE;
        }

        public final boolean isPassedPreJoin() {
            return ordinal() > ordinal();
        }
    }

    /* compiled from: WebRtcViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0005R\u0011\u0010\u0006\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0005R\u0011\u0010\u0007\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\u0005R\u0011\u0010\b\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\b\u0010\u0005R\u0011\u0010\t\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\t\u0010\u0005j\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;", "", "(Ljava/lang/String;I)V", "isConnected", "", "()Z", "isIdle", "isNotIdle", "isNotIdleOrConnected", "isRinging", "IDLE", "RINGING", "DISCONNECTED", "CONNECTING", "RECONNECTING", "CONNECTED", "CONNECTED_AND_JOINING", "CONNECTED_AND_JOINED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum GroupCallState {
        IDLE,
        RINGING,
        DISCONNECTED,
        CONNECTING,
        RECONNECTING,
        CONNECTED,
        CONNECTED_AND_JOINING,
        CONNECTED_AND_JOINED;

        /* compiled from: WebRtcViewModel.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[GroupCallState.values().length];
                iArr[GroupCallState.CONNECTED.ordinal()] = 1;
                iArr[GroupCallState.CONNECTED_AND_JOINING.ordinal()] = 2;
                iArr[GroupCallState.CONNECTED_AND_JOINED.ordinal()] = 3;
                iArr[GroupCallState.DISCONNECTED.ordinal()] = 4;
                iArr[GroupCallState.CONNECTING.ordinal()] = 5;
                iArr[GroupCallState.RECONNECTING.ordinal()] = 6;
                $EnumSwitchMapping$0 = iArr;
            }
        }

        public final boolean isIdle() {
            return this == IDLE;
        }

        public final boolean isNotIdle() {
            return this != IDLE;
        }

        public final boolean isConnected() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            return i == 1 || i == 2 || i == 3;
        }

        public final boolean isNotIdleOrConnected() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            return i == 4 || i == 5 || i == 6;
        }

        public final boolean isRinging() {
            return this == RINGING;
        }
    }

    public final State getState() {
        return this.state;
    }

    public final GroupCallState getGroupState() {
        return this.groupState;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }

    public final boolean isRemoteVideoOffer() {
        return this.isRemoteVideoOffer;
    }

    public final long getCallConnectedTime() {
        return this.callConnectedTime;
    }

    public final List<CallParticipant> getRemoteParticipants() {
        return this.remoteParticipants;
    }

    public final Set<RecipientId> getIdentityChangedParticipants() {
        return this.identityChangedParticipants;
    }

    public final OptionalLong getRemoteDevicesCount() {
        return this.remoteDevicesCount;
    }

    public final Long getParticipantLimit() {
        return this.participantLimit;
    }

    public final boolean shouldRingGroup() {
        return this.ringGroup;
    }

    public final Recipient getRingerRecipient() {
        return this.ringerRecipient;
    }

    public final SignalAudioManager.AudioDevice getActiveDevice() {
        return this.activeDevice;
    }

    public final Set<SignalAudioManager.AudioDevice> getAvailableDevices() {
        return this.availableDevices;
    }

    public final boolean getBluetoothPermissionDenied() {
        return this.bluetoothPermissionDenied;
    }

    public final CallParticipant getLocalParticipant() {
        return this.localParticipant;
    }

    public final boolean isCellularConnection() {
        return this.isCellularConnection;
    }

    public final boolean isRemoteVideoEnabled() {
        boolean z;
        List<CallParticipant> list = this.remoteParticipants;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (CallParticipant callParticipant : list) {
                if (callParticipant.isVideoEnabled()) {
                    z = true;
                    break;
                }
            }
        }
        z = false;
        if (z) {
            return true;
        }
        if (!this.groupState.isNotIdle() || this.remoteParticipants.size() <= 1) {
            return false;
        }
        return true;
    }

    public final boolean areRemoteDevicesInCall() {
        return this.remoteDevicesCount.isPresent() && this.remoteDevicesCount.getAsLong() > 0;
    }

    public String toString() {
        return StringsKt__IndentKt.trimIndent("\n      WebRtcViewModel {\n       state=" + this.state + ",\n       recipient=" + this.recipient.getId() + ",\n       isRemoteVideoOffer=" + this.isRemoteVideoOffer + ",\n       callConnectedTime=" + this.callConnectedTime + ",\n       localParticipant=" + this.localParticipant + ",\n       remoteParticipants=" + this.remoteParticipants + ",\n       identityChangedRecipients=" + this.identityChangedParticipants + ",\n       remoteDevicesCount=" + this.remoteDevicesCount + ",\n       participantLimit=" + this.participantLimit + ",\n       activeDevice=" + this.activeDevice + ",\n       availableDevices=" + this.availableDevices + ",\n       bluetoothPermissionDenied=" + this.bluetoothPermissionDenied + ",\n       ringGroup=" + this.ringGroup + "\n      }\n    ");
    }
}
