package org.thoughtcrime.securesms.events;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.libsignal.protocol.IdentityKey;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.ringrtc.CameraState;

/* compiled from: CallParticipant.kt */
@Metadata(d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b!\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\n\b\b\u0018\u0000 P2\u00020\u0001:\u0003OPQB\u0001\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\r\u0012\b\b\u0002\u0010\u000f\u001a\u00020\r\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0011\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u0012\b\b\u0002\u0010\u0014\u001a\u00020\r\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0016\u001a\u00020\r\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0018¢\u0006\u0002\u0010\u0019J\t\u00101\u001a\u00020\u0003HÆ\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\u0013HÆ\u0003J\t\u00103\u001a\u00020\rHÆ\u0003J\t\u00104\u001a\u00020\u0011HÆ\u0003J\t\u00105\u001a\u00020\rHÆ\u0003J\t\u00106\u001a\u00020\u0018HÂ\u0003J\t\u00107\u001a\u00020\u0005HÆ\u0003J\u000b\u00108\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\t\u00109\u001a\u00020\tHÆ\u0003J\t\u0010:\u001a\u00020\u000bHÆ\u0003J\t\u0010;\u001a\u00020\rHÆ\u0003J\t\u0010<\u001a\u00020\rHÆ\u0003J\t\u0010=\u001a\u00020\rHÆ\u0003J\t\u0010>\u001a\u00020\u0011HÆ\u0003J\u0001\u0010?\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u000f\u001a\u00020\r2\b\b\u0002\u0010\u0010\u001a\u00020\u00112\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00132\b\b\u0002\u0010\u0014\u001a\u00020\r2\b\b\u0002\u0010\u0015\u001a\u00020\u00112\b\b\u0002\u0010\u0016\u001a\u00020\r2\b\b\u0002\u0010\u0017\u001a\u00020\u0018HÆ\u0001J\u0013\u0010@\u001a\u00020\r2\b\u0010A\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u000e\u0010B\u001a\u00020C2\u0006\u0010D\u001a\u00020EJ\u000e\u0010F\u001a\u00020C2\u0006\u0010D\u001a\u00020EJ\t\u0010G\u001a\u00020HHÖ\u0001J\t\u0010I\u001a\u00020CHÖ\u0001J\u0010\u0010J\u001a\u00020\u00002\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u000e\u0010K\u001a\u00020\u00002\u0006\u0010L\u001a\u00020\rJ\u000e\u0010M\u001a\u00020\u00002\u0006\u0010N\u001a\u00020\rR\u0011\u0010\u0015\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0013¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010 \u001a\u00020!8F¢\u0006\u0006\u001a\u0004\b\"\u0010#R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u000e\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b&\u0010'R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010(R\u0011\u0010\u0014\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010(R\u0011\u0010\u000f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010(R\u0011\u0010)\u001a\u00020\r8F¢\u0006\u0006\u001a\u0004\b)\u0010(R\u0011\u0010*\u001a\u00020\r8F¢\u0006\u0006\u001a\u0004\b*\u0010(R\u0011\u0010\u0016\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010(R\u0011\u0010+\u001a\u00020\r8F¢\u0006\u0006\u001a\u0004\b+\u0010(R\u0011\u0010\u000e\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010(R\u0011\u0010\u0010\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b,\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b-\u0010.R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b/\u00100¨\u0006R"}, d2 = {"Lorg/thoughtcrime/securesms/events/CallParticipant;", "", "callParticipantId", "Lorg/thoughtcrime/securesms/events/CallParticipantId;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "identityKey", "Lorg/signal/libsignal/protocol/IdentityKey;", "videoSink", "Lorg/thoughtcrime/securesms/components/webrtc/BroadcastVideoSink;", "cameraState", "Lorg/thoughtcrime/securesms/ringrtc/CameraState;", "isForwardingVideo", "", "isVideoEnabled", "isMicrophoneEnabled", "lastSpoke", "", "audioLevel", "Lorg/thoughtcrime/securesms/events/CallParticipant$AudioLevel;", "isMediaKeysReceived", "addedToCallTime", "isScreenSharing", "deviceOrdinal", "Lorg/thoughtcrime/securesms/events/CallParticipant$DeviceOrdinal;", "(Lorg/thoughtcrime/securesms/events/CallParticipantId;Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/signal/libsignal/protocol/IdentityKey;Lorg/thoughtcrime/securesms/components/webrtc/BroadcastVideoSink;Lorg/thoughtcrime/securesms/ringrtc/CameraState;ZZZJLorg/thoughtcrime/securesms/events/CallParticipant$AudioLevel;ZJZLorg/thoughtcrime/securesms/events/CallParticipant$DeviceOrdinal;)V", "getAddedToCallTime", "()J", "getAudioLevel", "()Lorg/thoughtcrime/securesms/events/CallParticipant$AudioLevel;", "getCallParticipantId", "()Lorg/thoughtcrime/securesms/events/CallParticipantId;", "cameraDirection", "Lorg/thoughtcrime/securesms/ringrtc/CameraState$Direction;", "getCameraDirection", "()Lorg/thoughtcrime/securesms/ringrtc/CameraState$Direction;", "getCameraState", "()Lorg/thoughtcrime/securesms/ringrtc/CameraState;", "getIdentityKey", "()Lorg/signal/libsignal/protocol/IdentityKey;", "()Z", "isMoreThanOneCameraAvailable", "isPrimary", "isSelf", "getLastSpoke", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getVideoSink", "()Lorg/thoughtcrime/securesms/components/webrtc/BroadcastVideoSink;", "component1", "component10", "component11", "component12", "component13", "component14", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "getRecipientDisplayName", "", "context", "Landroid/content/Context;", "getShortRecipientDisplayName", "hashCode", "", "toString", "withIdentityKey", "withScreenSharingEnabled", "enable", "withVideoEnabled", "videoEnabled", "AudioLevel", "Companion", "DeviceOrdinal", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CallParticipant {
    public static final Companion Companion = new Companion(null);
    public static final CallParticipant EMPTY = new CallParticipant(null, null, null, null, null, false, false, false, 0, null, false, 0, false, null, 16383, null);
    private final long addedToCallTime;
    private final AudioLevel audioLevel;
    private final CallParticipantId callParticipantId;
    private final CameraState cameraState;
    private final DeviceOrdinal deviceOrdinal;
    private final IdentityKey identityKey;
    private final boolean isForwardingVideo;
    private final boolean isMediaKeysReceived;
    private final boolean isMicrophoneEnabled;
    private final boolean isScreenSharing;
    private final boolean isVideoEnabled;
    private final long lastSpoke;
    private final Recipient recipient;
    private final BroadcastVideoSink videoSink;

    /* compiled from: CallParticipant.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/events/CallParticipant$DeviceOrdinal;", "", "(Ljava/lang/String;I)V", "PRIMARY", "SECONDARY", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum DeviceOrdinal {
        PRIMARY,
        SECONDARY
    }

    public CallParticipant() {
        this(null, null, null, null, null, false, false, false, 0, null, false, 0, false, null, 16383, null);
    }

    private final DeviceOrdinal component14() {
        return this.deviceOrdinal;
    }

    public static /* synthetic */ CallParticipant copy$default(CallParticipant callParticipant, CallParticipantId callParticipantId, Recipient recipient, IdentityKey identityKey, BroadcastVideoSink broadcastVideoSink, CameraState cameraState, boolean z, boolean z2, boolean z3, long j, AudioLevel audioLevel, boolean z4, long j2, boolean z5, DeviceOrdinal deviceOrdinal, int i, Object obj) {
        return callParticipant.copy((i & 1) != 0 ? callParticipant.callParticipantId : callParticipantId, (i & 2) != 0 ? callParticipant.recipient : recipient, (i & 4) != 0 ? callParticipant.identityKey : identityKey, (i & 8) != 0 ? callParticipant.videoSink : broadcastVideoSink, (i & 16) != 0 ? callParticipant.cameraState : cameraState, (i & 32) != 0 ? callParticipant.isForwardingVideo : z, (i & 64) != 0 ? callParticipant.isVideoEnabled : z2, (i & 128) != 0 ? callParticipant.isMicrophoneEnabled : z3, (i & 256) != 0 ? callParticipant.lastSpoke : j, (i & 512) != 0 ? callParticipant.audioLevel : audioLevel, (i & 1024) != 0 ? callParticipant.isMediaKeysReceived : z4, (i & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? callParticipant.addedToCallTime : j2, (i & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? callParticipant.isScreenSharing : z5, (i & 8192) != 0 ? callParticipant.deviceOrdinal : deviceOrdinal);
    }

    @JvmStatic
    public static final CallParticipant createLocal(CameraState cameraState, BroadcastVideoSink broadcastVideoSink, boolean z) {
        return Companion.createLocal(cameraState, broadcastVideoSink, z);
    }

    @JvmStatic
    public static final CallParticipant createRemote(CallParticipantId callParticipantId, Recipient recipient, IdentityKey identityKey, BroadcastVideoSink broadcastVideoSink, boolean z, boolean z2, boolean z3, long j, boolean z4, long j2, boolean z5, DeviceOrdinal deviceOrdinal) {
        return Companion.createRemote(callParticipantId, recipient, identityKey, broadcastVideoSink, z, z2, z3, j, z4, j2, z5, deviceOrdinal);
    }

    public final CallParticipantId component1() {
        return this.callParticipantId;
    }

    public final AudioLevel component10() {
        return this.audioLevel;
    }

    public final boolean component11() {
        return this.isMediaKeysReceived;
    }

    public final long component12() {
        return this.addedToCallTime;
    }

    public final boolean component13() {
        return this.isScreenSharing;
    }

    public final Recipient component2() {
        return this.recipient;
    }

    public final IdentityKey component3() {
        return this.identityKey;
    }

    public final BroadcastVideoSink component4() {
        return this.videoSink;
    }

    public final CameraState component5() {
        return this.cameraState;
    }

    public final boolean component6() {
        return this.isForwardingVideo;
    }

    public final boolean component7() {
        return this.isVideoEnabled;
    }

    public final boolean component8() {
        return this.isMicrophoneEnabled;
    }

    public final long component9() {
        return this.lastSpoke;
    }

    public final CallParticipant copy(CallParticipantId callParticipantId, Recipient recipient, IdentityKey identityKey, BroadcastVideoSink broadcastVideoSink, CameraState cameraState, boolean z, boolean z2, boolean z3, long j, AudioLevel audioLevel, boolean z4, long j2, boolean z5, DeviceOrdinal deviceOrdinal) {
        Intrinsics.checkNotNullParameter(callParticipantId, "callParticipantId");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(broadcastVideoSink, "videoSink");
        Intrinsics.checkNotNullParameter(cameraState, "cameraState");
        Intrinsics.checkNotNullParameter(deviceOrdinal, "deviceOrdinal");
        return new CallParticipant(callParticipantId, recipient, identityKey, broadcastVideoSink, cameraState, z, z2, z3, j, audioLevel, z4, j2, z5, deviceOrdinal);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CallParticipant)) {
            return false;
        }
        CallParticipant callParticipant = (CallParticipant) obj;
        return Intrinsics.areEqual(this.callParticipantId, callParticipant.callParticipantId) && Intrinsics.areEqual(this.recipient, callParticipant.recipient) && Intrinsics.areEqual(this.identityKey, callParticipant.identityKey) && Intrinsics.areEqual(this.videoSink, callParticipant.videoSink) && Intrinsics.areEqual(this.cameraState, callParticipant.cameraState) && this.isForwardingVideo == callParticipant.isForwardingVideo && this.isVideoEnabled == callParticipant.isVideoEnabled && this.isMicrophoneEnabled == callParticipant.isMicrophoneEnabled && this.lastSpoke == callParticipant.lastSpoke && this.audioLevel == callParticipant.audioLevel && this.isMediaKeysReceived == callParticipant.isMediaKeysReceived && this.addedToCallTime == callParticipant.addedToCallTime && this.isScreenSharing == callParticipant.isScreenSharing && this.deviceOrdinal == callParticipant.deviceOrdinal;
    }

    public int hashCode() {
        int hashCode = ((this.callParticipantId.hashCode() * 31) + this.recipient.hashCode()) * 31;
        IdentityKey identityKey = this.identityKey;
        int i = 0;
        int hashCode2 = (((((hashCode + (identityKey == null ? 0 : identityKey.hashCode())) * 31) + this.videoSink.hashCode()) * 31) + this.cameraState.hashCode()) * 31;
        boolean z = this.isForwardingVideo;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = (hashCode2 + i3) * 31;
        boolean z2 = this.isVideoEnabled;
        if (z2) {
            z2 = true;
        }
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = z2 ? 1 : 0;
        int i10 = (i6 + i7) * 31;
        boolean z3 = this.isMicrophoneEnabled;
        if (z3) {
            z3 = true;
        }
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = z3 ? 1 : 0;
        int m = (((i10 + i11) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.lastSpoke)) * 31;
        AudioLevel audioLevel = this.audioLevel;
        if (audioLevel != null) {
            i = audioLevel.hashCode();
        }
        int i14 = (m + i) * 31;
        boolean z4 = this.isMediaKeysReceived;
        if (z4) {
            z4 = true;
        }
        int i15 = z4 ? 1 : 0;
        int i16 = z4 ? 1 : 0;
        int i17 = z4 ? 1 : 0;
        int m2 = (((i14 + i15) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.addedToCallTime)) * 31;
        boolean z5 = this.isScreenSharing;
        if (!z5) {
            i2 = z5 ? 1 : 0;
        }
        return ((m2 + i2) * 31) + this.deviceOrdinal.hashCode();
    }

    public String toString() {
        return "CallParticipant(callParticipantId=" + this.callParticipantId + ", recipient=" + this.recipient + ", identityKey=" + this.identityKey + ", videoSink=" + this.videoSink + ", cameraState=" + this.cameraState + ", isForwardingVideo=" + this.isForwardingVideo + ", isVideoEnabled=" + this.isVideoEnabled + ", isMicrophoneEnabled=" + this.isMicrophoneEnabled + ", lastSpoke=" + this.lastSpoke + ", audioLevel=" + this.audioLevel + ", isMediaKeysReceived=" + this.isMediaKeysReceived + ", addedToCallTime=" + this.addedToCallTime + ", isScreenSharing=" + this.isScreenSharing + ", deviceOrdinal=" + this.deviceOrdinal + ')';
    }

    public CallParticipant(CallParticipantId callParticipantId, Recipient recipient, IdentityKey identityKey, BroadcastVideoSink broadcastVideoSink, CameraState cameraState, boolean z, boolean z2, boolean z3, long j, AudioLevel audioLevel, boolean z4, long j2, boolean z5, DeviceOrdinal deviceOrdinal) {
        Intrinsics.checkNotNullParameter(callParticipantId, "callParticipantId");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(broadcastVideoSink, "videoSink");
        Intrinsics.checkNotNullParameter(cameraState, "cameraState");
        Intrinsics.checkNotNullParameter(deviceOrdinal, "deviceOrdinal");
        this.callParticipantId = callParticipantId;
        this.recipient = recipient;
        this.identityKey = identityKey;
        this.videoSink = broadcastVideoSink;
        this.cameraState = cameraState;
        this.isForwardingVideo = z;
        this.isVideoEnabled = z2;
        this.isMicrophoneEnabled = z3;
        this.lastSpoke = j;
        this.audioLevel = audioLevel;
        this.isMediaKeysReceived = z4;
        this.addedToCallTime = j2;
        this.isScreenSharing = z5;
        this.deviceOrdinal = deviceOrdinal;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ CallParticipant(org.thoughtcrime.securesms.events.CallParticipantId r18, org.thoughtcrime.securesms.recipients.Recipient r19, org.signal.libsignal.protocol.IdentityKey r20, org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink r21, org.thoughtcrime.securesms.ringrtc.CameraState r22, boolean r23, boolean r24, boolean r25, long r26, org.thoughtcrime.securesms.events.CallParticipant.AudioLevel r28, boolean r29, long r30, boolean r32, org.thoughtcrime.securesms.events.CallParticipant.DeviceOrdinal r33, int r34, kotlin.jvm.internal.DefaultConstructorMarker r35) {
        /*
            r17 = this;
            r0 = r34
            r1 = r0 & 1
            if (r1 == 0) goto L_0x000e
            org.thoughtcrime.securesms.events.CallParticipantId r1 = new org.thoughtcrime.securesms.events.CallParticipantId
            org.thoughtcrime.securesms.recipients.Recipient r2 = org.thoughtcrime.securesms.recipients.Recipient.UNKNOWN
            r1.<init>(r2)
            goto L_0x0010
        L_0x000e:
            r1 = r18
        L_0x0010:
            r2 = r0 & 2
            java.lang.String r3 = "UNKNOWN"
            if (r2 == 0) goto L_0x001c
            org.thoughtcrime.securesms.recipients.Recipient r2 = org.thoughtcrime.securesms.recipients.Recipient.UNKNOWN
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r3)
            goto L_0x001e
        L_0x001c:
            r2 = r19
        L_0x001e:
            r4 = r0 & 4
            r5 = 0
            if (r4 == 0) goto L_0x0025
            r4 = r5
            goto L_0x0027
        L_0x0025:
            r4 = r20
        L_0x0027:
            r6 = r0 & 8
            if (r6 == 0) goto L_0x0031
            org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink r6 = new org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink
            r6.<init>()
            goto L_0x0033
        L_0x0031:
            r6 = r21
        L_0x0033:
            r7 = r0 & 16
            if (r7 == 0) goto L_0x003d
            org.thoughtcrime.securesms.ringrtc.CameraState r7 = org.thoughtcrime.securesms.ringrtc.CameraState.UNKNOWN
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r7, r3)
            goto L_0x003f
        L_0x003d:
            r7 = r22
        L_0x003f:
            r3 = r0 & 32
            r8 = 1
            if (r3 == 0) goto L_0x0046
            r3 = 1
            goto L_0x0048
        L_0x0046:
            r3 = r23
        L_0x0048:
            r9 = r0 & 64
            r10 = 0
            if (r9 == 0) goto L_0x004f
            r9 = 0
            goto L_0x0051
        L_0x004f:
            r9 = r24
        L_0x0051:
            r11 = r0 & 128(0x80, float:1.794E-43)
            if (r11 == 0) goto L_0x0057
            r11 = 0
            goto L_0x0059
        L_0x0057:
            r11 = r25
        L_0x0059:
            r12 = r0 & 256(0x100, float:3.59E-43)
            r13 = 0
            if (r12 == 0) goto L_0x0061
            r15 = r13
            goto L_0x0063
        L_0x0061:
            r15 = r26
        L_0x0063:
            r12 = r0 & 512(0x200, float:7.175E-43)
            if (r12 == 0) goto L_0x0068
            goto L_0x006a
        L_0x0068:
            r5 = r28
        L_0x006a:
            r12 = r0 & 1024(0x400, float:1.435E-42)
            if (r12 == 0) goto L_0x006f
            goto L_0x0071
        L_0x006f:
            r8 = r29
        L_0x0071:
            r12 = r0 & 2048(0x800, float:2.87E-42)
            if (r12 == 0) goto L_0x0076
            goto L_0x0078
        L_0x0076:
            r13 = r30
        L_0x0078:
            r12 = r0 & 4096(0x1000, float:5.74E-42)
            if (r12 == 0) goto L_0x007d
            goto L_0x007f
        L_0x007d:
            r10 = r32
        L_0x007f:
            r0 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r0 == 0) goto L_0x0086
            org.thoughtcrime.securesms.events.CallParticipant$DeviceOrdinal r0 = org.thoughtcrime.securesms.events.CallParticipant.DeviceOrdinal.PRIMARY
            goto L_0x0088
        L_0x0086:
            r0 = r33
        L_0x0088:
            r18 = r17
            r19 = r1
            r20 = r2
            r21 = r4
            r22 = r6
            r23 = r7
            r24 = r3
            r25 = r9
            r26 = r11
            r27 = r15
            r29 = r5
            r30 = r8
            r31 = r13
            r33 = r10
            r34 = r0
            r18.<init>(r19, r20, r21, r22, r23, r24, r25, r26, r27, r29, r30, r31, r33, r34)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.events.CallParticipant.<init>(org.thoughtcrime.securesms.events.CallParticipantId, org.thoughtcrime.securesms.recipients.Recipient, org.signal.libsignal.protocol.IdentityKey, org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink, org.thoughtcrime.securesms.ringrtc.CameraState, boolean, boolean, boolean, long, org.thoughtcrime.securesms.events.CallParticipant$AudioLevel, boolean, long, boolean, org.thoughtcrime.securesms.events.CallParticipant$DeviceOrdinal, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final CallParticipantId getCallParticipantId() {
        return this.callParticipantId;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }

    public final IdentityKey getIdentityKey() {
        return this.identityKey;
    }

    public final BroadcastVideoSink getVideoSink() {
        return this.videoSink;
    }

    public final CameraState getCameraState() {
        return this.cameraState;
    }

    public final boolean isForwardingVideo() {
        return this.isForwardingVideo;
    }

    public final boolean isVideoEnabled() {
        return this.isVideoEnabled;
    }

    public final boolean isMicrophoneEnabled() {
        return this.isMicrophoneEnabled;
    }

    public final long getLastSpoke() {
        return this.lastSpoke;
    }

    public final AudioLevel getAudioLevel() {
        return this.audioLevel;
    }

    public final boolean isMediaKeysReceived() {
        return this.isMediaKeysReceived;
    }

    public final long getAddedToCallTime() {
        return this.addedToCallTime;
    }

    public final boolean isScreenSharing() {
        return this.isScreenSharing;
    }

    public final CameraState.Direction getCameraDirection() {
        if (this.cameraState.getActiveDirection() != CameraState.Direction.BACK) {
            return CameraState.Direction.FRONT;
        }
        CameraState.Direction activeDirection = this.cameraState.getActiveDirection();
        Intrinsics.checkNotNullExpressionValue(activeDirection, "cameraState.activeDirection");
        return activeDirection;
    }

    public final boolean isMoreThanOneCameraAvailable() {
        return this.cameraState.getCameraCount() > 1;
    }

    public final boolean isPrimary() {
        return this.deviceOrdinal == DeviceOrdinal.PRIMARY;
    }

    public final boolean isSelf() {
        return this.recipient.isSelf();
    }

    public final String getRecipientDisplayName(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (this.recipient.isSelf() && isPrimary()) {
            String string = context.getString(R.string.CallParticipant__you);
            Intrinsics.checkNotNullExpressionValue(string, "{\n      context.getStrin…llParticipant__you)\n    }");
            return string;
        } else if (this.recipient.isSelf()) {
            String string2 = context.getString(R.string.CallParticipant__you_on_another_device);
            Intrinsics.checkNotNullExpressionValue(string2, "{\n      context.getStrin…_on_another_device)\n    }");
            return string2;
        } else if (isPrimary()) {
            String displayName = this.recipient.getDisplayName(context);
            Intrinsics.checkNotNullExpressionValue(displayName, "{\n      recipient.getDisplayName(context)\n    }");
            return displayName;
        } else {
            String string3 = context.getString(R.string.CallParticipant__s_on_another_device, this.recipient.getDisplayName(context));
            Intrinsics.checkNotNullExpressionValue(string3, "{\n      context.getStrin…splayName(context))\n    }");
            return string3;
        }
    }

    public final String getShortRecipientDisplayName(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (this.recipient.isSelf() && isPrimary()) {
            String string = context.getString(R.string.CallParticipant__you);
            Intrinsics.checkNotNullExpressionValue(string, "{\n      context.getStrin…llParticipant__you)\n    }");
            return string;
        } else if (this.recipient.isSelf()) {
            String string2 = context.getString(R.string.CallParticipant__you_on_another_device);
            Intrinsics.checkNotNullExpressionValue(string2, "{\n      context.getStrin…_on_another_device)\n    }");
            return string2;
        } else if (isPrimary()) {
            String shortDisplayName = this.recipient.getShortDisplayName(context);
            Intrinsics.checkNotNullExpressionValue(shortDisplayName, "{\n      recipient.getSho…isplayName(context)\n    }");
            return shortDisplayName;
        } else {
            String string3 = context.getString(R.string.CallParticipant__s_on_another_device, this.recipient.getShortDisplayName(context));
            Intrinsics.checkNotNullExpressionValue(string3, "{\n      context.getStrin…splayName(context))\n    }");
            return string3;
        }
    }

    public final CallParticipant withIdentityKey(IdentityKey identityKey) {
        return copy$default(this, null, null, identityKey, null, null, false, false, false, 0, null, false, 0, false, null, 16379, null);
    }

    public final CallParticipant withVideoEnabled(boolean z) {
        return copy$default(this, null, null, null, null, null, false, z, false, 0, null, false, 0, false, null, 16319, null);
    }

    public final CallParticipant withScreenSharingEnabled(boolean z) {
        return copy$default(this, null, null, null, null, null, false, false, false, 0, null, false, 0, z, null, 12287, null);
    }

    /* compiled from: CallParticipant.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0001\u0018\u0000 \b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/events/CallParticipant$AudioLevel;", "", "(Ljava/lang/String;I)V", "LOWEST", "LOW", "MEDIUM", "HIGH", "HIGHEST", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum AudioLevel {
        LOWEST,
        LOW,
        MEDIUM,
        HIGH,
        HIGHEST;
        
        public static final Companion Companion = new Companion(null);

        @JvmStatic
        public static final AudioLevel fromRawAudioLevel(int i) {
            return Companion.fromRawAudioLevel(i);
        }

        /* compiled from: CallParticipant.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/events/CallParticipant$AudioLevel$Companion;", "", "()V", "fromRawAudioLevel", "Lorg/thoughtcrime/securesms/events/CallParticipant$AudioLevel;", "raw", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            @JvmStatic
            public final AudioLevel fromRawAudioLevel(int i) {
                if (i < 500) {
                    return AudioLevel.LOWEST;
                }
                if (i < 1000) {
                    return AudioLevel.LOW;
                }
                if (i < 5000) {
                    return AudioLevel.MEDIUM;
                }
                if (i < 16000) {
                    return AudioLevel.HIGH;
                }
                return AudioLevel.HIGHEST;
            }
        }
    }

    /* compiled from: CallParticipant.kt */
    @Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007Jj\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u001cH\u0007R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/events/CallParticipant$Companion;", "", "()V", "EMPTY", "Lorg/thoughtcrime/securesms/events/CallParticipant;", "createLocal", "cameraState", "Lorg/thoughtcrime/securesms/ringrtc/CameraState;", "renderer", "Lorg/thoughtcrime/securesms/components/webrtc/BroadcastVideoSink;", "microphoneEnabled", "", "createRemote", "callParticipantId", "Lorg/thoughtcrime/securesms/events/CallParticipantId;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "identityKey", "Lorg/signal/libsignal/protocol/IdentityKey;", "isForwardingVideo", "audioEnabled", "videoEnabled", "lastSpoke", "", "mediaKeysReceived", "addedToCallTime", "isScreenSharing", "deviceOrdinal", "Lorg/thoughtcrime/securesms/events/CallParticipant$DeviceOrdinal;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final CallParticipant createLocal(CameraState cameraState, BroadcastVideoSink broadcastVideoSink, boolean z) {
            Intrinsics.checkNotNullParameter(cameraState, "cameraState");
            Intrinsics.checkNotNullParameter(broadcastVideoSink, "renderer");
            CallParticipantId callParticipantId = new CallParticipantId(Recipient.self());
            Recipient self = Recipient.self();
            Intrinsics.checkNotNullExpressionValue(self, "self()");
            return new CallParticipant(callParticipantId, self, null, broadcastVideoSink, cameraState, false, cameraState.isEnabled() && cameraState.getCameraCount() > 0, z, 0, null, false, 0, false, null, 16164, null);
        }

        @JvmStatic
        public final CallParticipant createRemote(CallParticipantId callParticipantId, Recipient recipient, IdentityKey identityKey, BroadcastVideoSink broadcastVideoSink, boolean z, boolean z2, boolean z3, long j, boolean z4, long j2, boolean z5, DeviceOrdinal deviceOrdinal) {
            Intrinsics.checkNotNullParameter(callParticipantId, "callParticipantId");
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            Intrinsics.checkNotNullParameter(broadcastVideoSink, "renderer");
            Intrinsics.checkNotNullParameter(deviceOrdinal, "deviceOrdinal");
            return new CallParticipant(callParticipantId, recipient, identityKey, broadcastVideoSink, null, z, z3, z2, j, null, z4, j2, z5, deviceOrdinal, 528, null);
        }
    }
}
