package org.thoughtcrime.securesms;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.InvalidPassphraseException;
import org.thoughtcrime.securesms.crypto.MasterSecret;
import org.thoughtcrime.securesms.crypto.MasterSecretUtil;
import org.thoughtcrime.securesms.util.DynamicLanguage;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class PassphraseChangeActivity extends PassphraseActivity {
    private static final String TAG = Log.tag(PassphraseChangeActivity.class);
    private Button cancelButton;
    private DynamicLanguage dynamicLanguage = new DynamicLanguage();
    private DynamicTheme dynamicTheme = new DynamicTheme();
    private EditText newPassphrase;
    private Button okButton;
    private EditText originalPassphrase;
    private EditText repeatPassphrase;

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        this.dynamicTheme.onCreate(this);
        this.dynamicLanguage.onCreate(this);
        super.onCreate(bundle);
        setContentView(R.layout.change_passphrase_activity);
        initializeResources();
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
        this.dynamicLanguage.onResume(this);
    }

    private void initializeResources() {
        this.originalPassphrase = (EditText) findViewById(R.id.old_passphrase);
        this.newPassphrase = (EditText) findViewById(R.id.new_passphrase);
        this.repeatPassphrase = (EditText) findViewById(R.id.repeat_passphrase);
        this.okButton = (Button) findViewById(R.id.ok_button);
        this.cancelButton = (Button) findViewById(R.id.cancel_button);
        this.okButton.setOnClickListener(new OkButtonClickListener());
        this.cancelButton.setOnClickListener(new CancelButtonClickListener());
        if (TextSecurePreferences.isPasswordDisabled(this)) {
            this.originalPassphrase.setVisibility(8);
        } else {
            this.originalPassphrase.setVisibility(0);
        }
    }

    public void verifyAndSavePassphrases() {
        String str;
        String str2;
        String str3;
        Editable text = this.originalPassphrase.getText();
        Editable text2 = this.newPassphrase.getText();
        Editable text3 = this.repeatPassphrase.getText();
        if (text == null) {
            str = "";
        } else {
            str = text.toString();
        }
        if (text2 == null) {
            str2 = "";
        } else {
            str2 = text2.toString();
        }
        if (text3 == null) {
            str3 = "";
        } else {
            str3 = text3.toString();
        }
        if (TextSecurePreferences.isPasswordDisabled(this)) {
            str = MasterSecretUtil.UNENCRYPTED_PASSPHRASE;
        }
        if (!str2.equals(str3)) {
            this.newPassphrase.setText("");
            this.repeatPassphrase.setText("");
            this.newPassphrase.setError(getString(R.string.PassphraseChangeActivity_passphrases_dont_match_exclamation));
            this.newPassphrase.requestFocus();
        } else if (str2.equals("")) {
            this.newPassphrase.setError(getString(R.string.PassphraseChangeActivity_enter_new_passphrase_exclamation));
            this.newPassphrase.requestFocus();
        } else {
            new ChangePassphraseTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, str, str2);
        }
    }

    /* loaded from: classes.dex */
    public class CancelButtonClickListener implements View.OnClickListener {
        private CancelButtonClickListener() {
            PassphraseChangeActivity.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            PassphraseChangeActivity.this.finish();
        }
    }

    /* loaded from: classes.dex */
    public class OkButtonClickListener implements View.OnClickListener {
        private OkButtonClickListener() {
            PassphraseChangeActivity.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            PassphraseChangeActivity.this.verifyAndSavePassphrases();
        }
    }

    /* loaded from: classes.dex */
    public class ChangePassphraseTask extends AsyncTask<String, Void, MasterSecret> {
        private final Context context;

        public ChangePassphraseTask(Context context) {
            PassphraseChangeActivity.this = r1;
            this.context = context;
        }

        @Override // android.os.AsyncTask
        protected void onPreExecute() {
            PassphraseChangeActivity.this.okButton.setEnabled(false);
        }

        public MasterSecret doInBackground(String... strArr) {
            try {
                MasterSecret changeMasterSecretPassphrase = MasterSecretUtil.changeMasterSecretPassphrase(this.context, strArr[0], strArr[1]);
                TextSecurePreferences.setPasswordDisabled(this.context, false);
                return changeMasterSecretPassphrase;
            } catch (InvalidPassphraseException e) {
                Log.w(PassphraseChangeActivity.TAG, e);
                return null;
            }
        }

        public void onPostExecute(MasterSecret masterSecret) {
            PassphraseChangeActivity.this.okButton.setEnabled(true);
            if (masterSecret != null) {
                PassphraseChangeActivity.this.setMasterSecret(masterSecret);
                return;
            }
            PassphraseChangeActivity.this.originalPassphrase.setText("");
            PassphraseChangeActivity.this.originalPassphrase.setError(PassphraseChangeActivity.this.getString(R.string.PassphraseChangeActivity_incorrect_old_passphrase_exclamation));
            PassphraseChangeActivity.this.originalPassphrase.requestFocus();
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseActivity
    protected void cleanup() {
        this.originalPassphrase = null;
        this.newPassphrase = null;
        this.repeatPassphrase = null;
        System.gc();
    }
}
