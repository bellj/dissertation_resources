package org.thoughtcrime.securesms.lock.v2;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.lifecycle.Observer;
import com.airbnb.lottie.LottieAnimationView;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel;
import org.thoughtcrime.securesms.pin.PinOptOutDialog;
import org.thoughtcrime.securesms.registration.RegistrationUtil;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;

/* loaded from: classes4.dex */
public abstract class BaseKbsPinFragment<ViewModel extends BaseKbsPinViewModel> extends LoggingFragment {
    private TextView confirm;
    private LearnMoreTextView description;
    private EditText input;
    private TextView keyboardToggle;
    private TextView label;
    private LottieAnimationView lottieEnd;
    private LottieAnimationView lottieProgress;
    private TextView title;
    private ViewModel viewModel;

    protected abstract ViewModel initializeViewModel();

    protected abstract void initializeViewStates();

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.base_kbs_pin_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        initializeViews(view);
        ViewModel initializeViewModel = initializeViewModel();
        this.viewModel = initializeViewModel;
        initializeViewModel.getUserEntry().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BaseKbsPinFragment.$r8$lambda$b2KCgzkvxukDn6aAKI5GpK94C9g(BaseKbsPinFragment.this, (KbsPin) obj);
            }
        });
        this.viewModel.getKeyboard().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BaseKbsPinFragment.m2023$r8$lambda$nypbfGFPe0G8dTjQnphcqICaME(BaseKbsPinFragment.this, (PinKeyboardType) obj);
            }
        });
        this.description.setOnLinkClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseKbsPinFragment.$r8$lambda$hnNYl3AHnXQdKUtOacwrb_HliEk(BaseKbsPinFragment.this, view2);
            }
        });
        ((AppCompatActivity) requireActivity()).setSupportActionBar((Toolbar) view.findViewById(R.id.kbs_pin_toolbar));
        ((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle((CharSequence) null);
        initializeListeners();
    }

    public /* synthetic */ void lambda$onViewCreated$0(KbsPin kbsPin) {
        boolean z = kbsPin.length() >= 4;
        this.confirm.setEnabled(z);
        this.confirm.setAlpha(z ? 1.0f : 0.5f);
    }

    public /* synthetic */ void lambda$onViewCreated$1(PinKeyboardType pinKeyboardType) {
        updateKeyboard(pinKeyboardType);
        this.keyboardToggle.setText(resolveKeyboardToggleText(pinKeyboardType));
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        CommunicationActions.openBrowserLink(requireContext(), getString(R.string.BaseKbsPinFragment__learn_more_url));
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.input.requestFocus();
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.pin_skip, menu);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPrepareOptionsMenu(Menu menu) {
        if (RegistrationLockUtil.userHasRegistrationLock(requireContext()) || SignalStore.kbsValues().hasPin() || SignalStore.kbsValues().hasOptedOut()) {
            menu.clear();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_pin_learn_more /* 2131363682 */:
                onLearnMore();
                return true;
            case R.id.menu_pin_skip /* 2131363683 */:
                onPinSkipped();
                return true;
            default:
                return false;
        }
    }

    public TextView getTitle() {
        return this.title;
    }

    public LearnMoreTextView getDescription() {
        return this.description;
    }

    public EditText getInput() {
        return this.input;
    }

    public LottieAnimationView getLottieProgress() {
        return this.lottieProgress;
    }

    public LottieAnimationView getLottieEnd() {
        return this.lottieEnd;
    }

    public TextView getLabel() {
        return this.label;
    }

    public TextView getKeyboardToggle() {
        return this.keyboardToggle;
    }

    public TextView getConfirm() {
        return this.confirm;
    }

    public void closeNavGraphBranch() {
        Intent intent = requireActivity().getIntent();
        if (intent != null && intent.hasExtra("next_intent")) {
            startActivity((Intent) intent.getParcelableExtra("next_intent"));
        }
        requireActivity().finish();
    }

    private void initializeViews(View view) {
        this.title = (TextView) view.findViewById(R.id.edit_kbs_pin_title);
        this.description = (LearnMoreTextView) view.findViewById(R.id.edit_kbs_pin_description);
        this.input = (EditText) view.findViewById(R.id.edit_kbs_pin_input);
        this.label = (TextView) view.findViewById(R.id.edit_kbs_pin_input_label);
        this.keyboardToggle = (TextView) view.findViewById(R.id.edit_kbs_pin_keyboard_toggle);
        this.confirm = (TextView) view.findViewById(R.id.edit_kbs_pin_confirm);
        this.lottieProgress = (LottieAnimationView) view.findViewById(R.id.edit_kbs_pin_lottie_progress);
        this.lottieEnd = (LottieAnimationView) view.findViewById(R.id.edit_kbs_pin_lottie_end);
        initializeViewStates();
    }

    private void initializeListeners() {
        this.input.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                BaseKbsPinFragment.$r8$lambda$hp4q7fXtAdB5LeblSWKC536hD5U(BaseKbsPinFragment.this, (Editable) obj);
            }
        }));
        this.input.setImeOptions(5);
        this.input.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment$$ExternalSyntheticLambda1
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return BaseKbsPinFragment.$r8$lambda$WoRZUY1bF5OkyzWxFYt3rT5OTlE(BaseKbsPinFragment.this, textView, i, keyEvent);
            }
        });
        this.keyboardToggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BaseKbsPinFragment.$r8$lambda$NTMeIkD2iuo10xLrOA51S_ndoi8(BaseKbsPinFragment.this, view);
            }
        });
        this.confirm.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BaseKbsPinFragment.$r8$lambda$VJLtkTwywkPLdg1aFQvrguvAE6o(BaseKbsPinFragment.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$initializeListeners$3(Editable editable) {
        this.viewModel.setUserEntry(editable.toString());
    }

    public /* synthetic */ void lambda$initializeListeners$4(View view) {
        this.viewModel.toggleAlphaNumeric();
    }

    public /* synthetic */ void lambda$initializeListeners$5(View view) {
        this.viewModel.confirm();
    }

    public boolean handleEditorAction(View view, int i, KeyEvent keyEvent) {
        if (i != 5 || !this.confirm.isEnabled()) {
            return true;
        }
        this.viewModel.confirm();
        return true;
    }

    private void updateKeyboard(PinKeyboardType pinKeyboardType) {
        this.input.setInputType(pinKeyboardType == PinKeyboardType.ALPHA_NUMERIC ? 129 : 18);
    }

    private int resolveKeyboardToggleText(PinKeyboardType pinKeyboardType) {
        return pinKeyboardType == PinKeyboardType.ALPHA_NUMERIC ? R.string.BaseKbsPinFragment__create_numeric_pin : R.string.BaseKbsPinFragment__create_alphanumeric_pin;
    }

    private void onLearnMore() {
        CommunicationActions.openBrowserLink(requireContext(), getString(R.string.KbsSplashFragment__learn_more_link));
    }

    private void onPinSkipped() {
        PinOptOutDialog.show(requireContext(), new Runnable() { // from class: org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment$$ExternalSyntheticLambda7
            @Override // java.lang.Runnable
            public final void run() {
                BaseKbsPinFragment.m2022$r8$lambda$CdamIxOpdUHmssjMeqsk_TX_8w(BaseKbsPinFragment.this);
            }
        });
    }

    public /* synthetic */ void lambda$onPinSkipped$6() {
        RegistrationUtil.maybeMarkRegistrationComplete(requireContext());
        closeNavGraphBranch();
    }
}
