package org.thoughtcrime.securesms.lock.v2;

/* loaded from: classes4.dex */
public final class KbsConstants {
    public static final int MINIMUM_PIN_LENGTH;

    private KbsConstants() {
    }
}
