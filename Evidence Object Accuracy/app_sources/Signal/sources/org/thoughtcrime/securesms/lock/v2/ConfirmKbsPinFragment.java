package org.thoughtcrime.securesms.lock.v2;

import android.animation.Animator;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.util.Consumer;
import androidx.core.view.ViewCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.airbnb.lottie.LottieAnimationView;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.animation.AnimationRepeatListener;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinViewModel;
import org.thoughtcrime.securesms.megaphone.Megaphones;
import org.thoughtcrime.securesms.registration.RegistrationUtil;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.SpanUtil;

/* loaded from: classes4.dex */
public class ConfirmKbsPinFragment extends BaseKbsPinFragment<ConfirmKbsPinViewModel> {
    private ConfirmKbsPinViewModel viewModel;

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ boolean onOptionsItemSelected(MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment
    protected void initializeViewStates() {
        if (ConfirmKbsPinFragmentArgs.fromBundle(requireArguments()).getIsPinChange()) {
            initializeViewStatesForPinChange();
        } else {
            initializeViewStatesForPinCreate();
        }
        ViewCompat.setAutofillHints(getInput(), "newPassword");
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment
    public ConfirmKbsPinViewModel initializeViewModel() {
        ConfirmKbsPinFragmentArgs fromBundle = ConfirmKbsPinFragmentArgs.fromBundle(requireArguments());
        KbsPin userEntry = fromBundle.getUserEntry();
        Objects.requireNonNull(userEntry);
        ConfirmKbsPinViewModel confirmKbsPinViewModel = (ConfirmKbsPinViewModel) ViewModelProviders.of(this, new ConfirmKbsPinViewModel.Factory(userEntry, fromBundle.getKeyboard(), new ConfirmKbsPinRepository())).get(ConfirmKbsPinViewModel.class);
        this.viewModel = confirmKbsPinViewModel;
        confirmKbsPinViewModel.getLabel().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConfirmKbsPinFragment.$r8$lambda$2Wvr1iTxdqqc4oVfQeh4K9kP4H4(ConfirmKbsPinFragment.this, (ConfirmKbsPinViewModel.Label) obj);
            }
        });
        this.viewModel.getSaveAnimation().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConfirmKbsPinFragment.$r8$lambda$eJy67I_mdhGrj94p4FoaDzIB7Pc(ConfirmKbsPinFragment.this, (ConfirmKbsPinViewModel.SaveAnimation) obj);
            }
        });
        return this.viewModel;
    }

    private void initializeViewStatesForPinCreate() {
        getTitle().setText(R.string.CreateKbsPinFragment__create_your_pin);
        getDescription().setText(R.string.ConfirmKbsPinFragment__confirm_your_pin);
        getKeyboardToggle().setVisibility(4);
        getLabel().setText("");
        getDescription().setLearnMoreVisible(false);
    }

    private void initializeViewStatesForPinChange() {
        getTitle().setText(R.string.CreateKbsPinFragment__create_a_new_pin);
        getDescription().setText(R.string.ConfirmKbsPinFragment__confirm_your_pin);
        getDescription().setLearnMoreVisible(false);
        getKeyboardToggle().setVisibility(4);
        getLabel().setText("");
    }

    public void updateLabel(ConfirmKbsPinViewModel.Label label) {
        int i = AnonymousClass3.$SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$Label[label.ordinal()];
        if (i == 1) {
            getLabel().setText("");
        } else if (i == 2) {
            getLabel().setText(R.string.ConfirmKbsPinFragment__creating_pin);
            getInput().setEnabled(false);
        } else if (i == 3) {
            getLabel().setText(R.string.ConfirmKbsPinFragment__re_enter_your_pin);
        } else if (i == 4) {
            getLabel().setText(SpanUtil.color(ContextCompat.getColor(requireContext(), R.color.red), getString(R.string.ConfirmKbsPinFragment__pins_dont_match)));
            getInput().getText().clear();
        }
    }

    public void updateSaveAnimation(ConfirmKbsPinViewModel.SaveAnimation saveAnimation) {
        updateAnimationAndInputVisibility(saveAnimation);
        LottieAnimationView lottieProgress = getLottieProgress();
        int i = AnonymousClass3.$SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$SaveAnimation[saveAnimation.ordinal()];
        if (i == 1) {
            lottieProgress.cancelAnimation();
        } else if (i == 2) {
            lottieProgress.setAnimation(R.raw.lottie_kbs_loading);
            lottieProgress.setRepeatMode(1);
            lottieProgress.setRepeatCount(-1);
            lottieProgress.playAnimation();
        } else if (i == 3) {
            startEndAnimationOnNextProgressRepetition(R.raw.lottie_kbs_success, new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinFragment.1
                @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    ConfirmKbsPinFragment.this.requireActivity().setResult(-1);
                    ConfirmKbsPinFragment.this.closeNavGraphBranch();
                    RegistrationUtil.maybeMarkRegistrationComplete(ConfirmKbsPinFragment.this.requireContext());
                    StorageSyncHelper.scheduleSyncForDataChange();
                }
            });
        } else if (i == 4) {
            startEndAnimationOnNextProgressRepetition(R.raw.lottie_kbs_fail, new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinFragment.2
                @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    RegistrationUtil.maybeMarkRegistrationComplete(ConfirmKbsPinFragment.this.requireContext());
                    ConfirmKbsPinFragment.this.displayFailedDialog();
                }
            });
        }
    }

    /* renamed from: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinFragment$3 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$Label;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$SaveAnimation;

        static {
            int[] iArr = new int[ConfirmKbsPinViewModel.SaveAnimation.values().length];
            $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$SaveAnimation = iArr;
            try {
                iArr[ConfirmKbsPinViewModel.SaveAnimation.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$SaveAnimation[ConfirmKbsPinViewModel.SaveAnimation.LOADING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$SaveAnimation[ConfirmKbsPinViewModel.SaveAnimation.SUCCESS.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$SaveAnimation[ConfirmKbsPinViewModel.SaveAnimation.FAILURE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[ConfirmKbsPinViewModel.Label.values().length];
            $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$Label = iArr2;
            try {
                iArr2[ConfirmKbsPinViewModel.Label.EMPTY.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$Label[ConfirmKbsPinViewModel.Label.CREATING_PIN.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$Label[ConfirmKbsPinViewModel.Label.RE_ENTER_PIN.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinViewModel$Label[ConfirmKbsPinViewModel.Label.PIN_DOES_NOT_MATCH.ordinal()] = 4;
            } catch (NoSuchFieldError unused8) {
            }
        }
    }

    private void startEndAnimationOnNextProgressRepetition(int i, AnimationCompleteListener animationCompleteListener) {
        LottieAnimationView lottieProgress = getLottieProgress();
        LottieAnimationView lottieEnd = getLottieEnd();
        lottieEnd.setAnimation(i);
        lottieEnd.removeAllAnimatorListeners();
        lottieEnd.setRepeatCount(0);
        lottieEnd.addAnimatorListener(animationCompleteListener);
        if (lottieProgress.isAnimating()) {
            lottieProgress.addAnimatorListener(new AnimationRepeatListener(new Consumer(lottieProgress, lottieEnd) { // from class: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinFragment$$ExternalSyntheticLambda3
                public final /* synthetic */ LottieAnimationView f$1;
                public final /* synthetic */ LottieAnimationView f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    ConfirmKbsPinFragment.$r8$lambda$jA1DvzLdcEC0WrNms7KIX_2b3xU(ConfirmKbsPinFragment.this, this.f$1, this.f$2, (Animator) obj);
                }
            }));
        } else {
            hideProgressAndStartEndAnimation(lottieProgress, lottieEnd);
        }
    }

    public /* synthetic */ void lambda$startEndAnimationOnNextProgressRepetition$0(LottieAnimationView lottieAnimationView, LottieAnimationView lottieAnimationView2, Animator animator) {
        hideProgressAndStartEndAnimation(lottieAnimationView, lottieAnimationView2);
    }

    private void hideProgressAndStartEndAnimation(LottieAnimationView lottieAnimationView, LottieAnimationView lottieAnimationView2) {
        this.viewModel.onLoadingAnimationComplete();
        lottieAnimationView.setVisibility(8);
        lottieAnimationView2.setVisibility(0);
        lottieAnimationView2.playAnimation();
    }

    private void updateAnimationAndInputVisibility(ConfirmKbsPinViewModel.SaveAnimation saveAnimation) {
        if (saveAnimation == ConfirmKbsPinViewModel.SaveAnimation.NONE) {
            getInput().setVisibility(0);
            getLottieProgress().setVisibility(8);
            return;
        }
        getInput().setVisibility(8);
        getLottieProgress().setVisibility(0);
    }

    public void displayFailedDialog() {
        new AlertDialog.Builder(requireContext()).setTitle(R.string.ConfirmKbsPinFragment__pin_creation_failed).setMessage(R.string.ConfirmKbsPinFragment__your_pin_was_not_saved).setCancelable(false).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinFragment$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ConfirmKbsPinFragment.$r8$lambda$HGeodnk46xJ7eiv3lI0iO2ykFlw(ConfirmKbsPinFragment.this, dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$displayFailedDialog$1(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        markMegaphoneSeenIfNecessary();
        requireActivity().setResult(0);
        closeNavGraphBranch();
    }

    private void markMegaphoneSeenIfNecessary() {
        ApplicationDependencies.getMegaphoneRepository().markSeen(Megaphones.Event.PINS_FOR_ALL);
    }
}
