package org.thoughtcrime.securesms.lock.v2;

import android.os.Parcel;
import android.os.Parcelable;

/* loaded from: classes4.dex */
public final class KbsPin implements Parcelable {
    public static final Parcelable.Creator<KbsPin> CREATOR = new Parcelable.Creator<KbsPin>() { // from class: org.thoughtcrime.securesms.lock.v2.KbsPin.1
        @Override // android.os.Parcelable.Creator
        public KbsPin createFromParcel(Parcel parcel) {
            return new KbsPin(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public KbsPin[] newArray(int i) {
            return new KbsPin[i];
        }
    };
    public static KbsPin EMPTY = new KbsPin("");
    private final String pin;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    private KbsPin(String str) {
        this.pin = str;
    }

    private KbsPin(Parcel parcel) {
        this.pin = parcel.readString();
    }

    @Override // java.lang.Object
    public String toString() {
        return this.pin;
    }

    public static KbsPin from(String str) {
        if (str == null) {
            return EMPTY;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return EMPTY;
        }
        return new KbsPin(trim);
    }

    public int length() {
        return this.pin.length();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.pin);
    }
}
