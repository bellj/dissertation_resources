package org.thoughtcrime.securesms.lock.v2;

import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinRepository;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class ConfirmKbsPinViewModel extends ViewModel implements BaseKbsPinViewModel {
    private final DefaultValueLiveData<PinKeyboardType> keyboard;
    private final DefaultValueLiveData<Label> label;
    private final KbsPin pinToConfirm;
    private final ConfirmKbsPinRepository repository;
    private final DefaultValueLiveData<SaveAnimation> saveAnimation;
    private final DefaultValueLiveData<KbsPin> userEntry;

    /* loaded from: classes4.dex */
    public enum Label {
        RE_ENTER_PIN,
        PIN_DOES_NOT_MATCH,
        CREATING_PIN,
        EMPTY
    }

    /* loaded from: classes4.dex */
    public enum SaveAnimation {
        NONE,
        LOADING,
        SUCCESS,
        FAILURE
    }

    /* synthetic */ ConfirmKbsPinViewModel(KbsPin kbsPin, PinKeyboardType pinKeyboardType, ConfirmKbsPinRepository confirmKbsPinRepository, AnonymousClass1 r4) {
        this(kbsPin, pinKeyboardType, confirmKbsPinRepository);
    }

    private ConfirmKbsPinViewModel(KbsPin kbsPin, PinKeyboardType pinKeyboardType, ConfirmKbsPinRepository confirmKbsPinRepository) {
        this.userEntry = new DefaultValueLiveData<>(KbsPin.EMPTY);
        DefaultValueLiveData<PinKeyboardType> defaultValueLiveData = new DefaultValueLiveData<>(PinKeyboardType.NUMERIC);
        this.keyboard = defaultValueLiveData;
        this.saveAnimation = new DefaultValueLiveData<>(SaveAnimation.NONE);
        this.label = new DefaultValueLiveData<>(Label.RE_ENTER_PIN);
        defaultValueLiveData.setValue(pinKeyboardType);
        this.pinToConfirm = kbsPin;
        this.repository = confirmKbsPinRepository;
    }

    public LiveData<SaveAnimation> getSaveAnimation() {
        return Transformations.distinctUntilChanged(this.saveAnimation);
    }

    public LiveData<Label> getLabel() {
        return Transformations.distinctUntilChanged(this.label);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel
    public void confirm() {
        this.userEntry.setValue(KbsPin.EMPTY);
        if (this.pinToConfirm.toString().equals(this.userEntry.getValue().toString())) {
            this.label.setValue(Label.CREATING_PIN);
            this.saveAnimation.setValue(SaveAnimation.LOADING);
            this.repository.setPin(this.pinToConfirm, this.keyboard.getValue(), new Consumer() { // from class: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinViewModel$$ExternalSyntheticLambda0
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    ConfirmKbsPinViewModel.this.handleResult((ConfirmKbsPinRepository.PinSetResult) obj);
                }
            });
            return;
        }
        this.label.setValue(Label.PIN_DOES_NOT_MATCH);
    }

    public void onLoadingAnimationComplete() {
        this.label.setValue(Label.EMPTY);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel
    public LiveData<KbsPin> getUserEntry() {
        return this.userEntry;
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel
    public LiveData<PinKeyboardType> getKeyboard() {
        return this.keyboard;
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel
    public void setUserEntry(String str) {
        this.userEntry.setValue(KbsPin.from(str));
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel
    public void toggleAlphaNumeric() {
        DefaultValueLiveData<PinKeyboardType> defaultValueLiveData = this.keyboard;
        defaultValueLiveData.setValue(defaultValueLiveData.getValue().getOther());
    }

    /* renamed from: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinRepository$PinSetResult;

        static {
            int[] iArr = new int[ConfirmKbsPinRepository.PinSetResult.values().length];
            $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinRepository$PinSetResult = iArr;
            try {
                iArr[ConfirmKbsPinRepository.PinSetResult.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinRepository$PinSetResult[ConfirmKbsPinRepository.PinSetResult.FAILURE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public void handleResult(ConfirmKbsPinRepository.PinSetResult pinSetResult) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$lock$v2$ConfirmKbsPinRepository$PinSetResult[pinSetResult.ordinal()];
        if (i == 1) {
            this.saveAnimation.setValue(SaveAnimation.SUCCESS);
        } else if (i == 2) {
            this.saveAnimation.setValue(SaveAnimation.FAILURE);
        } else {
            throw new IllegalStateException("Unknown state: " + pinSetResult.name());
        }
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final PinKeyboardType keyboard;
        private final KbsPin pinToConfirm;
        private final ConfirmKbsPinRepository repository;

        public Factory(KbsPin kbsPin, PinKeyboardType pinKeyboardType, ConfirmKbsPinRepository confirmKbsPinRepository) {
            this.pinToConfirm = kbsPin;
            this.keyboard = pinKeyboardType;
            this.repository = confirmKbsPinRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new ConfirmKbsPinViewModel(this.pinToConfirm, this.keyboard, this.repository, null);
        }
    }
}
