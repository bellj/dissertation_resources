package org.thoughtcrime.securesms.lock.v2;

import android.os.Bundle;
import androidx.navigation.NavDirections;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class KbsSplashFragmentDirections {
    private KbsSplashFragmentDirections() {
    }

    public static ActionCreateKbsPin actionCreateKbsPin() {
        return new ActionCreateKbsPin();
    }

    /* loaded from: classes4.dex */
    public static class ActionCreateKbsPin implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_createKbsPin;
        }

        private ActionCreateKbsPin() {
            this.arguments = new HashMap();
        }

        public ActionCreateKbsPin setIsPinChange(boolean z) {
            this.arguments.put("is_pin_change", Boolean.valueOf(z));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("is_pin_change")) {
                bundle.putBoolean("is_pin_change", ((Boolean) this.arguments.get("is_pin_change")).booleanValue());
            } else {
                bundle.putBoolean("is_pin_change", false);
            }
            return bundle;
        }

        public boolean getIsPinChange() {
            return ((Boolean) this.arguments.get("is_pin_change")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionCreateKbsPin actionCreateKbsPin = (ActionCreateKbsPin) obj;
            return this.arguments.containsKey("is_pin_change") == actionCreateKbsPin.arguments.containsKey("is_pin_change") && getIsPinChange() == actionCreateKbsPin.getIsPinChange() && getActionId() == actionCreateKbsPin.getActionId();
        }

        public int hashCode() {
            return (((getIsPinChange() ? 1 : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionCreateKbsPin(actionId=" + getActionId() + "){isPinChange=" + getIsPinChange() + "}";
        }
    }
}
