package org.thoughtcrime.securesms.lock.v2;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class CreateKbsPinFragmentDirections {
    private CreateKbsPinFragmentDirections() {
    }

    public static ActionConfirmPin actionConfirmPin() {
        return new ActionConfirmPin();
    }

    /* loaded from: classes4.dex */
    public static class ActionConfirmPin implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_confirmPin;
        }

        private ActionConfirmPin() {
            this.arguments = new HashMap();
        }

        public ActionConfirmPin setIsPinChange(boolean z) {
            this.arguments.put("is_pin_change", Boolean.valueOf(z));
            return this;
        }

        public ActionConfirmPin setUserEntry(KbsPin kbsPin) {
            this.arguments.put("user_entry", kbsPin);
            return this;
        }

        public ActionConfirmPin setKeyboard(PinKeyboardType pinKeyboardType) {
            if (pinKeyboardType != null) {
                this.arguments.put("keyboard", pinKeyboardType);
                return this;
            }
            throw new IllegalArgumentException("Argument \"keyboard\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("is_pin_change")) {
                bundle.putBoolean("is_pin_change", ((Boolean) this.arguments.get("is_pin_change")).booleanValue());
            } else {
                bundle.putBoolean("is_pin_change", false);
            }
            if (this.arguments.containsKey("user_entry")) {
                KbsPin kbsPin = (KbsPin) this.arguments.get("user_entry");
                if (Parcelable.class.isAssignableFrom(KbsPin.class) || kbsPin == null) {
                    bundle.putParcelable("user_entry", (Parcelable) Parcelable.class.cast(kbsPin));
                } else if (Serializable.class.isAssignableFrom(KbsPin.class)) {
                    bundle.putSerializable("user_entry", (Serializable) Serializable.class.cast(kbsPin));
                } else {
                    throw new UnsupportedOperationException(KbsPin.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            } else {
                bundle.putSerializable("user_entry", null);
            }
            if (this.arguments.containsKey("keyboard")) {
                PinKeyboardType pinKeyboardType = (PinKeyboardType) this.arguments.get("keyboard");
                if (Parcelable.class.isAssignableFrom(PinKeyboardType.class) || pinKeyboardType == null) {
                    bundle.putParcelable("keyboard", (Parcelable) Parcelable.class.cast(pinKeyboardType));
                } else if (Serializable.class.isAssignableFrom(PinKeyboardType.class)) {
                    bundle.putSerializable("keyboard", (Serializable) Serializable.class.cast(pinKeyboardType));
                } else {
                    throw new UnsupportedOperationException(PinKeyboardType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            } else {
                bundle.putSerializable("keyboard", PinKeyboardType.NUMERIC);
            }
            return bundle;
        }

        public boolean getIsPinChange() {
            return ((Boolean) this.arguments.get("is_pin_change")).booleanValue();
        }

        public KbsPin getUserEntry() {
            return (KbsPin) this.arguments.get("user_entry");
        }

        public PinKeyboardType getKeyboard() {
            return (PinKeyboardType) this.arguments.get("keyboard");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionConfirmPin actionConfirmPin = (ActionConfirmPin) obj;
            if (this.arguments.containsKey("is_pin_change") != actionConfirmPin.arguments.containsKey("is_pin_change") || getIsPinChange() != actionConfirmPin.getIsPinChange() || this.arguments.containsKey("user_entry") != actionConfirmPin.arguments.containsKey("user_entry")) {
                return false;
            }
            if (getUserEntry() == null ? actionConfirmPin.getUserEntry() != null : !getUserEntry().equals(actionConfirmPin.getUserEntry())) {
                return false;
            }
            if (this.arguments.containsKey("keyboard") != actionConfirmPin.arguments.containsKey("keyboard")) {
                return false;
            }
            if (getKeyboard() == null ? actionConfirmPin.getKeyboard() == null : getKeyboard().equals(actionConfirmPin.getKeyboard())) {
                return getActionId() == actionConfirmPin.getActionId();
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((getIsPinChange() ? 1 : 0) + 31) * 31) + (getUserEntry() != null ? getUserEntry().hashCode() : 0)) * 31;
            if (getKeyboard() != null) {
                i = getKeyboard().hashCode();
            }
            return ((hashCode + i) * 31) + getActionId();
        }

        public String toString() {
            return "ActionConfirmPin(actionId=" + getActionId() + "){isPinChange=" + getIsPinChange() + ", userEntry=" + getUserEntry() + ", keyboard=" + getKeyboard() + "}";
        }
    }
}
