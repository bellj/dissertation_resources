package org.thoughtcrime.securesms.lock.v2;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class ConfirmKbsPinFragmentArgs {
    private final HashMap arguments;

    private ConfirmKbsPinFragmentArgs() {
        this.arguments = new HashMap();
    }

    private ConfirmKbsPinFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static ConfirmKbsPinFragmentArgs fromBundle(Bundle bundle) {
        ConfirmKbsPinFragmentArgs confirmKbsPinFragmentArgs = new ConfirmKbsPinFragmentArgs();
        bundle.setClassLoader(ConfirmKbsPinFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("is_pin_change")) {
            confirmKbsPinFragmentArgs.arguments.put("is_pin_change", Boolean.valueOf(bundle.getBoolean("is_pin_change")));
        } else {
            confirmKbsPinFragmentArgs.arguments.put("is_pin_change", Boolean.FALSE);
        }
        if (!bundle.containsKey("user_entry")) {
            confirmKbsPinFragmentArgs.arguments.put("user_entry", null);
        } else if (Parcelable.class.isAssignableFrom(KbsPin.class) || Serializable.class.isAssignableFrom(KbsPin.class)) {
            confirmKbsPinFragmentArgs.arguments.put("user_entry", (KbsPin) bundle.get("user_entry"));
        } else {
            throw new UnsupportedOperationException(KbsPin.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
        if (!bundle.containsKey("keyboard")) {
            confirmKbsPinFragmentArgs.arguments.put("keyboard", PinKeyboardType.NUMERIC);
        } else if (Parcelable.class.isAssignableFrom(PinKeyboardType.class) || Serializable.class.isAssignableFrom(PinKeyboardType.class)) {
            PinKeyboardType pinKeyboardType = (PinKeyboardType) bundle.get("keyboard");
            if (pinKeyboardType != null) {
                confirmKbsPinFragmentArgs.arguments.put("keyboard", pinKeyboardType);
            } else {
                throw new IllegalArgumentException("Argument \"keyboard\" is marked as non-null but was passed a null value.");
            }
        } else {
            throw new UnsupportedOperationException(PinKeyboardType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
        return confirmKbsPinFragmentArgs;
    }

    public boolean getIsPinChange() {
        return ((Boolean) this.arguments.get("is_pin_change")).booleanValue();
    }

    public KbsPin getUserEntry() {
        return (KbsPin) this.arguments.get("user_entry");
    }

    public PinKeyboardType getKeyboard() {
        return (PinKeyboardType) this.arguments.get("keyboard");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("is_pin_change")) {
            bundle.putBoolean("is_pin_change", ((Boolean) this.arguments.get("is_pin_change")).booleanValue());
        } else {
            bundle.putBoolean("is_pin_change", false);
        }
        if (this.arguments.containsKey("user_entry")) {
            KbsPin kbsPin = (KbsPin) this.arguments.get("user_entry");
            if (Parcelable.class.isAssignableFrom(KbsPin.class) || kbsPin == null) {
                bundle.putParcelable("user_entry", (Parcelable) Parcelable.class.cast(kbsPin));
            } else if (Serializable.class.isAssignableFrom(KbsPin.class)) {
                bundle.putSerializable("user_entry", (Serializable) Serializable.class.cast(kbsPin));
            } else {
                throw new UnsupportedOperationException(KbsPin.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        } else {
            bundle.putSerializable("user_entry", null);
        }
        if (this.arguments.containsKey("keyboard")) {
            PinKeyboardType pinKeyboardType = (PinKeyboardType) this.arguments.get("keyboard");
            if (Parcelable.class.isAssignableFrom(PinKeyboardType.class) || pinKeyboardType == null) {
                bundle.putParcelable("keyboard", (Parcelable) Parcelable.class.cast(pinKeyboardType));
            } else if (Serializable.class.isAssignableFrom(PinKeyboardType.class)) {
                bundle.putSerializable("keyboard", (Serializable) Serializable.class.cast(pinKeyboardType));
            } else {
                throw new UnsupportedOperationException(PinKeyboardType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        } else {
            bundle.putSerializable("keyboard", PinKeyboardType.NUMERIC);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ConfirmKbsPinFragmentArgs confirmKbsPinFragmentArgs = (ConfirmKbsPinFragmentArgs) obj;
        if (this.arguments.containsKey("is_pin_change") != confirmKbsPinFragmentArgs.arguments.containsKey("is_pin_change") || getIsPinChange() != confirmKbsPinFragmentArgs.getIsPinChange() || this.arguments.containsKey("user_entry") != confirmKbsPinFragmentArgs.arguments.containsKey("user_entry")) {
            return false;
        }
        if (getUserEntry() == null ? confirmKbsPinFragmentArgs.getUserEntry() != null : !getUserEntry().equals(confirmKbsPinFragmentArgs.getUserEntry())) {
            return false;
        }
        if (this.arguments.containsKey("keyboard") != confirmKbsPinFragmentArgs.arguments.containsKey("keyboard")) {
            return false;
        }
        return getKeyboard() == null ? confirmKbsPinFragmentArgs.getKeyboard() == null : getKeyboard().equals(confirmKbsPinFragmentArgs.getKeyboard());
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((getIsPinChange() ? 1 : 0) + 31) * 31) + (getUserEntry() != null ? getUserEntry().hashCode() : 0)) * 31;
        if (getKeyboard() != null) {
            i = getKeyboard().hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "ConfirmKbsPinFragmentArgs{isPinChange=" + getIsPinChange() + ", userEntry=" + getUserEntry() + ", keyboard=" + getKeyboard() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(ConfirmKbsPinFragmentArgs confirmKbsPinFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(confirmKbsPinFragmentArgs.arguments);
        }

        public Builder() {
            this.arguments = new HashMap();
        }

        public ConfirmKbsPinFragmentArgs build() {
            return new ConfirmKbsPinFragmentArgs(this.arguments);
        }

        public Builder setIsPinChange(boolean z) {
            this.arguments.put("is_pin_change", Boolean.valueOf(z));
            return this;
        }

        public Builder setUserEntry(KbsPin kbsPin) {
            this.arguments.put("user_entry", kbsPin);
            return this;
        }

        public Builder setKeyboard(PinKeyboardType pinKeyboardType) {
            if (pinKeyboardType != null) {
                this.arguments.put("keyboard", pinKeyboardType);
                return this;
            }
            throw new IllegalArgumentException("Argument \"keyboard\" is marked as non-null but was passed a null value.");
        }

        public boolean getIsPinChange() {
            return ((Boolean) this.arguments.get("is_pin_change")).booleanValue();
        }

        public KbsPin getUserEntry() {
            return (KbsPin) this.arguments.get("user_entry");
        }

        public PinKeyboardType getKeyboard() {
            return (PinKeyboardType) this.arguments.get("keyboard");
        }
    }
}
