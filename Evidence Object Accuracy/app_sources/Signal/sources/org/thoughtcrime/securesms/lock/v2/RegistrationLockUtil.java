package org.thoughtcrime.securesms.lock.v2;

import android.content.Context;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public final class RegistrationLockUtil {
    private RegistrationLockUtil() {
    }

    public static boolean userHasRegistrationLock(Context context) {
        return TextSecurePreferences.isV1RegistrationLockEnabled(context) || SignalStore.kbsValues().isV2RegistrationLockEnabled();
    }
}
