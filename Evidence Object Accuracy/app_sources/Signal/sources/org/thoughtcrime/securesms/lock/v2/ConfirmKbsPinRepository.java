package org.thoughtcrime.securesms.lock.v2;

import android.content.Context;
import androidx.core.util.Consumer;
import java.io.IOException;
import java.util.Objects;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinRepository;
import org.thoughtcrime.securesms.pin.PinState;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class ConfirmKbsPinRepository {
    private static final String TAG = Log.tag(ConfirmKbsPinRepository.class);

    /* loaded from: classes4.dex */
    public enum PinSetResult {
        SUCCESS,
        FAILURE
    }

    public void setPin(KbsPin kbsPin, PinKeyboardType pinKeyboardType, Consumer<PinSetResult> consumer) {
        ConfirmKbsPinRepository$$ExternalSyntheticLambda0 confirmKbsPinRepository$$ExternalSyntheticLambda0 = new SimpleTask.BackgroundTask(ApplicationDependencies.getApplication(), kbsPin.toString(), pinKeyboardType) { // from class: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ String f$1;
            public final /* synthetic */ PinKeyboardType f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConfirmKbsPinRepository.lambda$setPin$0(this.f$0, this.f$1, this.f$2);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(confirmKbsPinRepository$$ExternalSyntheticLambda0, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.lock.v2.ConfirmKbsPinRepository$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                Consumer.this.accept((ConfirmKbsPinRepository.PinSetResult) obj);
            }
        });
    }

    public static /* synthetic */ PinSetResult lambda$setPin$0(Context context, String str, PinKeyboardType pinKeyboardType) {
        try {
            String str2 = TAG;
            Log.i(str2, "Setting pin on KBS");
            PinState.onPinChangedOrCreated(context, str, pinKeyboardType);
            Log.i(str2, "Pin set on KBS");
            return PinSetResult.SUCCESS;
        } catch (IOException | InvalidKeyException | UnauthenticatedResponseException e) {
            Log.w(TAG, e);
            PinState.onPinCreateFailure();
            return PinSetResult.FAILURE;
        }
    }
}
