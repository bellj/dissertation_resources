package org.thoughtcrime.securesms.lock.v2;

import androidx.core.util.Preconditions;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.whispersystems.signalservice.internal.registrationpin.PinValidityChecker;

/* loaded from: classes4.dex */
public final class CreateKbsPinViewModel extends ViewModel implements BaseKbsPinViewModel {
    private final SingleLiveEvent<PinErrorEvent> errors = new SingleLiveEvent<>();
    private final SingleLiveEvent<NavigationEvent> events = new SingleLiveEvent<>();
    private final MutableLiveData<PinKeyboardType> keyboard = new MutableLiveData<>(PinKeyboardType.NUMERIC);
    private final MutableLiveData<KbsPin> userEntry = new MutableLiveData<>(KbsPin.EMPTY);

    /* loaded from: classes4.dex */
    public enum PinErrorEvent {
        WEAK_PIN
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel
    public LiveData<KbsPin> getUserEntry() {
        return this.userEntry;
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel
    public LiveData<PinKeyboardType> getKeyboard() {
        return this.keyboard;
    }

    public LiveData<NavigationEvent> getNavigationEvents() {
        return this.events;
    }

    public LiveData<PinErrorEvent> getErrorEvents() {
        return this.errors;
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel
    public void setUserEntry(String str) {
        this.userEntry.setValue(KbsPin.from(str));
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel
    public void toggleAlphaNumeric() {
        MutableLiveData<PinKeyboardType> mutableLiveData = this.keyboard;
        mutableLiveData.setValue(((PinKeyboardType) Preconditions.checkNotNull(mutableLiveData.getValue())).getOther());
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinViewModel
    public void confirm() {
        KbsPin kbsPin = (KbsPin) Preconditions.checkNotNull(getUserEntry().getValue());
        PinKeyboardType pinKeyboardType = (PinKeyboardType) Preconditions.checkNotNull(getKeyboard().getValue());
        if (PinValidityChecker.valid(kbsPin.toString())) {
            this.events.setValue(new NavigationEvent(kbsPin, pinKeyboardType));
        } else {
            this.errors.setValue(PinErrorEvent.WEAK_PIN);
        }
    }

    /* loaded from: classes4.dex */
    public static final class NavigationEvent {
        private final PinKeyboardType keyboard;
        private final KbsPin userEntry;

        NavigationEvent(KbsPin kbsPin, PinKeyboardType pinKeyboardType) {
            this.userEntry = kbsPin;
            this.keyboard = pinKeyboardType;
        }

        public KbsPin getUserEntry() {
            return this.userEntry;
        }

        public PinKeyboardType getKeyboard() {
            return this.keyboard;
        }
    }
}
