package org.thoughtcrime.securesms.lock.v2;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class CreateKbsPinFragmentArgs {
    private final HashMap arguments;

    private CreateKbsPinFragmentArgs() {
        this.arguments = new HashMap();
    }

    private CreateKbsPinFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static CreateKbsPinFragmentArgs fromBundle(Bundle bundle) {
        CreateKbsPinFragmentArgs createKbsPinFragmentArgs = new CreateKbsPinFragmentArgs();
        bundle.setClassLoader(CreateKbsPinFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("is_pin_change")) {
            createKbsPinFragmentArgs.arguments.put("is_pin_change", Boolean.valueOf(bundle.getBoolean("is_pin_change")));
        } else {
            createKbsPinFragmentArgs.arguments.put("is_pin_change", Boolean.FALSE);
        }
        if (bundle.containsKey("is_forgot_pin")) {
            createKbsPinFragmentArgs.arguments.put("is_forgot_pin", Boolean.valueOf(bundle.getBoolean("is_forgot_pin")));
        } else {
            createKbsPinFragmentArgs.arguments.put("is_forgot_pin", Boolean.FALSE);
        }
        return createKbsPinFragmentArgs;
    }

    public boolean getIsPinChange() {
        return ((Boolean) this.arguments.get("is_pin_change")).booleanValue();
    }

    public boolean getIsForgotPin() {
        return ((Boolean) this.arguments.get("is_forgot_pin")).booleanValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("is_pin_change")) {
            bundle.putBoolean("is_pin_change", ((Boolean) this.arguments.get("is_pin_change")).booleanValue());
        } else {
            bundle.putBoolean("is_pin_change", false);
        }
        if (this.arguments.containsKey("is_forgot_pin")) {
            bundle.putBoolean("is_forgot_pin", ((Boolean) this.arguments.get("is_forgot_pin")).booleanValue());
        } else {
            bundle.putBoolean("is_forgot_pin", false);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CreateKbsPinFragmentArgs createKbsPinFragmentArgs = (CreateKbsPinFragmentArgs) obj;
        return this.arguments.containsKey("is_pin_change") == createKbsPinFragmentArgs.arguments.containsKey("is_pin_change") && getIsPinChange() == createKbsPinFragmentArgs.getIsPinChange() && this.arguments.containsKey("is_forgot_pin") == createKbsPinFragmentArgs.arguments.containsKey("is_forgot_pin") && getIsForgotPin() == createKbsPinFragmentArgs.getIsForgotPin();
    }

    public int hashCode() {
        return (((getIsPinChange() ? 1 : 0) + 31) * 31) + (getIsForgotPin() ? 1 : 0);
    }

    public String toString() {
        return "CreateKbsPinFragmentArgs{isPinChange=" + getIsPinChange() + ", isForgotPin=" + getIsForgotPin() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(CreateKbsPinFragmentArgs createKbsPinFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(createKbsPinFragmentArgs.arguments);
        }

        public Builder() {
            this.arguments = new HashMap();
        }

        public CreateKbsPinFragmentArgs build() {
            return new CreateKbsPinFragmentArgs(this.arguments);
        }

        public Builder setIsPinChange(boolean z) {
            this.arguments.put("is_pin_change", Boolean.valueOf(z));
            return this;
        }

        public Builder setIsForgotPin(boolean z) {
            this.arguments.put("is_forgot_pin", Boolean.valueOf(z));
            return this;
        }

        public boolean getIsPinChange() {
            return ((Boolean) this.arguments.get("is_pin_change")).booleanValue();
        }

        public boolean getIsForgotPin() {
            return ((Boolean) this.arguments.get("is_forgot_pin")).booleanValue();
        }
    }
}
