package org.thoughtcrime.securesms.lock.v2;

import android.content.Intent;
import android.os.Bundle;
import org.thoughtcrime.securesms.BaseActivity;
import org.thoughtcrime.securesms.PassphrasePromptActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.DynamicRegistrationTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public class KbsMigrationActivity extends BaseActivity {
    public static final int REQUEST_NEW_PIN;
    private final DynamicTheme dynamicTheme = new DynamicRegistrationTheme();

    public static Intent createIntent() {
        return new Intent(ApplicationDependencies.getApplication(), KbsMigrationActivity.class);
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (KeyCachingService.isLocked(this)) {
            startActivity(getPromptPassphraseIntent());
            finish();
            return;
        }
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.kbs_migration_activity);
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    private Intent getPromptPassphraseIntent() {
        return getRoutedIntent(PassphrasePromptActivity.class, getIntent());
    }

    private Intent getRoutedIntent(Class<?> cls, Intent intent) {
        Intent intent2 = new Intent(this, cls);
        if (intent != null) {
            intent2.putExtra("next_intent", intent);
        }
        return intent2;
    }
}
