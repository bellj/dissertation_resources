package org.thoughtcrime.securesms.lock.v2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinFragmentDirections;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinViewModel;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public class CreateKbsPinFragment extends BaseKbsPinFragment<CreateKbsPinViewModel> {
    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ boolean onOptionsItemSelected(MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment, androidx.fragment.app.Fragment
    public /* bridge */ /* synthetic */ void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment
    protected void initializeViewStates() {
        CreateKbsPinFragmentArgs fromBundle = CreateKbsPinFragmentArgs.fromBundle(requireArguments());
        if (fromBundle.getIsPinChange()) {
            initializeViewStatesForPinChange(fromBundle.getIsForgotPin());
        } else {
            initializeViewStatesForPinCreate();
        }
        getLabel().setText(getPinLengthRestrictionText(R.plurals.CreateKbsPinFragment__pin_must_be_at_least_digits));
        getConfirm().setEnabled(false);
        ViewCompat.setAutofillHints(getInput(), "newPassword");
    }

    private void initializeViewStatesForPinChange(boolean z) {
        getTitle().setText(R.string.CreateKbsPinFragment__create_a_new_pin);
        getDescription().setText(R.string.CreateKbsPinFragment__you_can_choose_a_new_pin_as_long_as_this_device_is_registered);
        getDescription().setLearnMoreVisible(true);
    }

    private void initializeViewStatesForPinCreate() {
        getTitle().setText(R.string.CreateKbsPinFragment__create_your_pin);
        getDescription().setText(R.string.CreateKbsPinFragment__pins_keep_information_stored_with_signal_encrypted);
        getDescription().setLearnMoreVisible(true);
    }

    @Override // org.thoughtcrime.securesms.lock.v2.BaseKbsPinFragment
    public CreateKbsPinViewModel initializeViewModel() {
        CreateKbsPinViewModel createKbsPinViewModel = (CreateKbsPinViewModel) ViewModelProviders.of(this).get(CreateKbsPinViewModel.class);
        createKbsPinViewModel.getNavigationEvents().observe(getViewLifecycleOwner(), new Observer(CreateKbsPinFragmentArgs.fromBundle(requireArguments())) { // from class: org.thoughtcrime.securesms.lock.v2.CreateKbsPinFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ CreateKbsPinFragmentArgs f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CreateKbsPinFragment.m2024$r8$lambda$ZhImP70rzZeHvEAMG2bC6s7psg(CreateKbsPinFragment.this, this.f$1, (CreateKbsPinViewModel.NavigationEvent) obj);
            }
        });
        createKbsPinViewModel.getErrorEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.lock.v2.CreateKbsPinFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CreateKbsPinFragment.$r8$lambda$6RJhNFwu2PNIiOfMoOrj_bWow4w(CreateKbsPinFragment.this, (CreateKbsPinViewModel.PinErrorEvent) obj);
            }
        });
        createKbsPinViewModel.getKeyboard().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.lock.v2.CreateKbsPinFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CreateKbsPinFragment.$r8$lambda$GML824GBD7iwzD0_WajJ7Ofj2Ro(CreateKbsPinFragment.this, (PinKeyboardType) obj);
            }
        });
        return createKbsPinViewModel;
    }

    public /* synthetic */ void lambda$initializeViewModel$0(CreateKbsPinFragmentArgs createKbsPinFragmentArgs, CreateKbsPinViewModel.NavigationEvent navigationEvent) {
        onConfirmPin(navigationEvent.getUserEntry(), navigationEvent.getKeyboard(), createKbsPinFragmentArgs.getIsPinChange());
    }

    public /* synthetic */ void lambda$initializeViewModel$2(CreateKbsPinViewModel.PinErrorEvent pinErrorEvent) {
        if (pinErrorEvent == CreateKbsPinViewModel.PinErrorEvent.WEAK_PIN) {
            getLabel().setText(SpanUtil.color(ContextCompat.getColor(requireContext(), R.color.red), getString(R.string.CreateKbsPinFragment__choose_a_stronger_pin)));
            shake(getInput(), new Runnable() { // from class: org.thoughtcrime.securesms.lock.v2.CreateKbsPinFragment$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    CreateKbsPinFragment.$r8$lambda$LMQFhgCYyLoT_Fjd4j64jz1BOM8(CreateKbsPinFragment.this);
                }
            });
            return;
        }
        throw new AssertionError("Unexpected PIN error!");
    }

    public /* synthetic */ void lambda$initializeViewModel$1() {
        getInput().getText().clear();
    }

    public /* synthetic */ void lambda$initializeViewModel$3(PinKeyboardType pinKeyboardType) {
        getLabel().setText(getLabelText(pinKeyboardType));
        getInput().getText().clear();
    }

    private void onConfirmPin(KbsPin kbsPin, PinKeyboardType pinKeyboardType, boolean z) {
        CreateKbsPinFragmentDirections.ActionConfirmPin actionConfirmPin = CreateKbsPinFragmentDirections.actionConfirmPin();
        actionConfirmPin.setUserEntry(kbsPin);
        actionConfirmPin.setKeyboard(pinKeyboardType);
        actionConfirmPin.setIsPinChange(z);
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), actionConfirmPin);
    }

    private String getLabelText(PinKeyboardType pinKeyboardType) {
        if (pinKeyboardType == PinKeyboardType.ALPHA_NUMERIC) {
            return getPinLengthRestrictionText(R.plurals.CreateKbsPinFragment__pin_must_be_at_least_characters);
        }
        return getPinLengthRestrictionText(R.plurals.CreateKbsPinFragment__pin_must_be_at_least_digits);
    }

    private String getPinLengthRestrictionText(int i) {
        return requireContext().getResources().getQuantityString(i, 4, 4);
    }

    private static void shake(EditText editText, final Runnable runnable) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 30.0f, 0.0f, 0.0f);
        translateAnimation.setDuration(50);
        translateAnimation.setRepeatCount(7);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() { // from class: org.thoughtcrime.securesms.lock.v2.CreateKbsPinFragment.1
            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationRepeat(Animation animation) {
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationStart(Animation animation) {
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationEnd(Animation animation) {
                runnable.run();
            }
        });
        editText.startAnimation(translateAnimation);
    }
}
