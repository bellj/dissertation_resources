package org.thoughtcrime.securesms.lock.v2;

/* loaded from: classes4.dex */
public enum PinKeyboardType {
    NUMERIC("numeric"),
    ALPHA_NUMERIC("alphaNumeric");
    
    private final String code;

    PinKeyboardType(String str) {
        this.code = str;
    }

    public PinKeyboardType getOther() {
        PinKeyboardType pinKeyboardType = NUMERIC;
        return this == pinKeyboardType ? ALPHA_NUMERIC : pinKeyboardType;
    }

    public String getCode() {
        return this.code;
    }

    public static PinKeyboardType fromCode(String str) {
        PinKeyboardType[] values = values();
        for (PinKeyboardType pinKeyboardType : values) {
            if (pinKeyboardType.code.equals(str)) {
                return pinKeyboardType;
            }
        }
        return NUMERIC;
    }
}
