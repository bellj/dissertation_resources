package org.thoughtcrime.securesms.lock;

import java.util.HashMap;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class SignalPinReminders {
    private static final long FOUR_WEEKS;
    public static final long INITIAL_INTERVAL;
    private static final NavigableSet<Long> INTERVALS;
    private static final long ONE_DAY;
    private static final long ONE_WEEK;
    private static final Map<Long, Integer> STRINGS = new HashMap<Long, Integer>() { // from class: org.thoughtcrime.securesms.lock.SignalPinReminders.2
        {
            put(Long.valueOf(SignalPinReminders.ONE_DAY), Integer.valueOf((int) R.string.SignalPinReminders_well_remind_you_again_tomorrow));
            put(Long.valueOf(SignalPinReminders.THREE_DAYS), Integer.valueOf((int) R.string.SignalPinReminders_well_remind_you_again_in_a_few_days));
            put(Long.valueOf(SignalPinReminders.ONE_WEEK), Integer.valueOf((int) R.string.SignalPinReminders_well_remind_you_again_in_a_week));
            put(Long.valueOf(SignalPinReminders.TWO_WEEKS), Integer.valueOf((int) R.string.SignalPinReminders_well_remind_you_again_in_a_couple_weeks));
            put(Long.valueOf(SignalPinReminders.FOUR_WEEKS), Integer.valueOf((int) R.string.SignalPinReminders_well_remind_you_again_in_a_month));
        }
    };
    private static final String TAG = Log.tag(SignalPinReminders.class);
    private static final long THREE_DAYS;
    private static final long TWO_WEEKS;

    static {
        TAG = Log.tag(SignalPinReminders.class);
        TimeUnit timeUnit = TimeUnit.DAYS;
        ONE_DAY = timeUnit.toMillis(1);
        THREE_DAYS = timeUnit.toMillis(3);
        ONE_WEEK = timeUnit.toMillis(7);
        TWO_WEEKS = timeUnit.toMillis(14);
        FOUR_WEEKS = timeUnit.toMillis(28);
        AnonymousClass1 r0 = new TreeSet<Long>() { // from class: org.thoughtcrime.securesms.lock.SignalPinReminders.1
            {
                add(Long.valueOf(SignalPinReminders.ONE_DAY));
                add(Long.valueOf(SignalPinReminders.THREE_DAYS));
                add(Long.valueOf(SignalPinReminders.ONE_WEEK));
                add(Long.valueOf(SignalPinReminders.TWO_WEEKS));
                add(Long.valueOf(SignalPinReminders.FOUR_WEEKS));
            }
        };
        INTERVALS = r0;
        STRINGS = new HashMap<Long, Integer>() { // from class: org.thoughtcrime.securesms.lock.SignalPinReminders.2
            {
                put(Long.valueOf(SignalPinReminders.ONE_DAY), Integer.valueOf((int) R.string.SignalPinReminders_well_remind_you_again_tomorrow));
                put(Long.valueOf(SignalPinReminders.THREE_DAYS), Integer.valueOf((int) R.string.SignalPinReminders_well_remind_you_again_in_a_few_days));
                put(Long.valueOf(SignalPinReminders.ONE_WEEK), Integer.valueOf((int) R.string.SignalPinReminders_well_remind_you_again_in_a_week));
                put(Long.valueOf(SignalPinReminders.TWO_WEEKS), Integer.valueOf((int) R.string.SignalPinReminders_well_remind_you_again_in_a_couple_weeks));
                put(Long.valueOf(SignalPinReminders.FOUR_WEEKS), Integer.valueOf((int) R.string.SignalPinReminders_well_remind_you_again_in_a_month));
            }
        };
        INITIAL_INTERVAL = r0.first().longValue();
    }

    public static long getNextInterval(long j) {
        NavigableSet<Long> navigableSet = INTERVALS;
        Long higher = navigableSet.higher(Long.valueOf(j));
        if (higher == null) {
            higher = navigableSet.last();
        }
        return higher.longValue();
    }

    public static long getPreviousInterval(long j) {
        NavigableSet<Long> navigableSet = INTERVALS;
        Long lower = navigableSet.lower(Long.valueOf(j));
        if (lower == null) {
            lower = navigableSet.first();
        }
        return lower.longValue();
    }

    public static int getReminderString(long j) {
        Integer num = STRINGS.get(Long.valueOf(j));
        if (num != null) {
            return num.intValue();
        }
        String str = TAG;
        Log.w(str, "Couldn't find a string for interval " + j);
        return R.string.SignalPinReminders_well_remind_you_again_later;
    }
}
