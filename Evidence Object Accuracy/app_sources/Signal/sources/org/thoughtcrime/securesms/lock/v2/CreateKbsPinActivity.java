package org.thoughtcrime.securesms.lock.v2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.navigation.Navigation;
import org.thoughtcrime.securesms.BaseActivity;
import org.thoughtcrime.securesms.PassphrasePromptActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinFragmentArgs;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.DynamicRegistrationTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public class CreateKbsPinActivity extends BaseActivity {
    public static final int REQUEST_NEW_PIN;
    private final DynamicTheme dynamicTheme = new DynamicRegistrationTheme();

    public static Intent getIntentForPinCreate(Context context) {
        return getIntentWithArgs(context, new CreateKbsPinFragmentArgs.Builder().setIsForgotPin(false).setIsPinChange(false).build());
    }

    public static Intent getIntentForPinChangeFromForgotPin(Context context) {
        return getIntentWithArgs(context, new CreateKbsPinFragmentArgs.Builder().setIsForgotPin(true).setIsPinChange(true).build());
    }

    public static Intent getIntentForPinChangeFromSettings(Context context) {
        return getIntentWithArgs(context, new CreateKbsPinFragmentArgs.Builder().setIsForgotPin(false).setIsPinChange(true).build());
    }

    private static Intent getIntentWithArgs(Context context, CreateKbsPinFragmentArgs createKbsPinFragmentArgs) {
        return new Intent(context, CreateKbsPinActivity.class).putExtras(createKbsPinFragmentArgs.toBundle());
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (KeyCachingService.isLocked(this)) {
            startActivity(getPromptPassphraseIntent());
            finish();
            return;
        }
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.create_kbs_pin_activity);
        CreateKbsPinFragmentArgs fromBundle = CreateKbsPinFragmentArgs.fromBundle(getIntent().getExtras());
        Navigation.findNavController(this, R.id.nav_host_fragment).setGraph(Navigation.findNavController(this, R.id.nav_host_fragment).getGraph(), fromBundle.toBundle());
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    private Intent getPromptPassphraseIntent() {
        return getRoutedIntent(PassphrasePromptActivity.class, getIntent());
    }

    private Intent getRoutedIntent(Class<?> cls, Intent intent) {
        Intent intent2 = new Intent(this, cls);
        if (intent != null) {
            intent2.putExtra("next_intent", intent);
        }
        return intent2;
    }
}
