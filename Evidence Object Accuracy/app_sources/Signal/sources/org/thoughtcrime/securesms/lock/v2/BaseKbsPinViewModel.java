package org.thoughtcrime.securesms.lock.v2;

import androidx.lifecycle.LiveData;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public interface BaseKbsPinViewModel {
    void confirm();

    LiveData<PinKeyboardType> getKeyboard();

    LiveData<KbsPin> getUserEntry();

    void setUserEntry(String str);

    void toggleAlphaNumeric();
}
