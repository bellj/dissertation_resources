package org.thoughtcrime.securesms.lock;

import android.content.Context;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class RegistrationLockReminders {
    public static final long INITIAL_INTERVAL;
    private static final NavigableSet<Long> INTERVALS;

    static {
        AnonymousClass1 r0 = new TreeSet<Long>() { // from class: org.thoughtcrime.securesms.lock.RegistrationLockReminders.1
            {
                TimeUnit timeUnit = TimeUnit.HOURS;
                add(Long.valueOf(timeUnit.toMillis(6)));
                add(Long.valueOf(timeUnit.toMillis(12)));
                TimeUnit timeUnit2 = TimeUnit.DAYS;
                add(Long.valueOf(timeUnit2.toMillis(1)));
                add(Long.valueOf(timeUnit2.toMillis(3)));
                add(Long.valueOf(timeUnit2.toMillis(7)));
            }
        };
        INTERVALS = r0;
        INITIAL_INTERVAL = r0.first().longValue();
    }

    public static boolean needsReminder(Context context) {
        return System.currentTimeMillis() > TextSecurePreferences.getRegistrationLockLastReminderTime(context) + TextSecurePreferences.getRegistrationLockNextReminderInterval(context);
    }

    public static void scheduleReminder(Context context, boolean z) {
        if (z) {
            NavigableSet<Long> navigableSet = INTERVALS;
            Long higher = navigableSet.higher(Long.valueOf(System.currentTimeMillis() - TextSecurePreferences.getRegistrationLockLastReminderTime(context)));
            if (higher == null) {
                higher = navigableSet.last();
            }
            TextSecurePreferences.setRegistrationLockLastReminderTime(context, System.currentTimeMillis());
            TextSecurePreferences.setRegistrationLockNextReminderInterval(context, higher.longValue());
            return;
        }
        TextSecurePreferences.setRegistrationLockLastReminderTime(context, TextSecurePreferences.getRegistrationLockLastReminderTime(context) + TimeUnit.MINUTES.toMillis(5));
    }
}
