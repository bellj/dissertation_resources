package org.thoughtcrime.securesms.lock;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.DialogCompat;
import androidx.core.view.ViewCompat;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.SimpleTextWatcher;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.SignalPinReminderDialog;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinActivity;
import org.thoughtcrime.securesms.lock.v2.PinKeyboardType;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public final class SignalPinReminderDialog {
    private static final String TAG = Log.tag(SignalPinReminderDialog.class);

    /* loaded from: classes4.dex */
    public interface Callback {
        void onReminderCompleted(String str, boolean z);

        void onReminderDismissed(boolean z);
    }

    /* loaded from: classes4.dex */
    public interface Launcher {
        void launch(Intent intent, int i);
    }

    /* loaded from: classes4.dex */
    public interface PinVerifier {

        /* loaded from: classes4.dex */
        public interface Callback {
            boolean hadWrongGuess();

            void onPinCorrect(String str);

            void onPinWrong();
        }

        void verifyPin(String str, Callback callback);
    }

    public static void show(final Context context, final Launcher launcher, final Callback callback) {
        if (SignalStore.kbsValues().hasPin()) {
            Log.i(TAG, "Showing PIN reminder dialog.");
            final AlertDialog create = new MaterialAlertDialogBuilder(context, R.style.ThemeOverlay_Signal_MaterialAlertDialog_Wide).setView(R.layout.kbs_pin_reminder_view).setCancelable(false).setOnCancelListener((DialogInterface.OnCancelListener) new DialogInterface.OnCancelListener(context) { // from class: org.thoughtcrime.securesms.lock.SignalPinReminderDialog$$ExternalSyntheticLambda0
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    SignalPinReminderDialog.$r8$lambda$X8T_siR0ywn2MpgRNxbIwlXHLRc(this.f$0, dialogInterface);
                }
            }).create();
            Display defaultDisplay = ServiceUtil.getWindowManager(context).getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            create.show();
            Window window = create.getWindow();
            double d = (double) displayMetrics.widthPixels;
            Double.isNaN(d);
            window.setLayout((int) (d * 0.8d), -2);
            EditText editText = (EditText) DialogCompat.requireViewById(create, R.id.pin);
            TextView textView = (TextView) DialogCompat.requireViewById(create, R.id.pin_status);
            TextView textView2 = (TextView) DialogCompat.requireViewById(create, R.id.reminder);
            View requireViewById = DialogCompat.requireViewById(create, R.id.skip);
            final View requireViewById2 = DialogCompat.requireViewById(create, R.id.submit);
            SpannableString spannableString = new SpannableString(context.getString(R.string.KbsReminderDialog__to_help_you_memorize_your_pin));
            SpannableString spannableString2 = new SpannableString(context.getString(R.string.KbsReminderDialog__forgot_pin));
            ViewUtil.focusAndShowKeyboard(editText);
            ViewCompat.setAutofillHints(editText, "password");
            int i = AnonymousClass4.$SwitchMap$org$thoughtcrime$securesms$lock$v2$PinKeyboardType[SignalStore.pinValues().getKeyboardType().ordinal()];
            if (i == 1) {
                editText.setInputType(18);
            } else if (i == 2) {
                editText.setInputType(129);
            }
            spannableString2.setSpan(new ClickableSpan() { // from class: org.thoughtcrime.securesms.lock.SignalPinReminderDialog.1
                @Override // android.text.style.ClickableSpan
                public void onClick(View view) {
                    create.dismiss();
                    launcher.launch(CreateKbsPinActivity.getIntentForPinChangeFromForgotPin(context), 27698);
                }
            }, 0, spannableString2.length(), 33);
            textView2.setText(new SpannableStringBuilder(spannableString).append((CharSequence) " ").append((CharSequence) spannableString2));
            textView2.setMovementMethod(LinkMovementMethod.getInstance());
            final PinVerifier.Callback pinWatcherCallback = getPinWatcherCallback(context, create, editText, textView, callback);
            V2PinVerifier v2PinVerifier = new V2PinVerifier();
            requireViewById.setOnClickListener(new View.OnClickListener(callback, pinWatcherCallback) { // from class: org.thoughtcrime.securesms.lock.SignalPinReminderDialog$$ExternalSyntheticLambda1
                public final /* synthetic */ SignalPinReminderDialog.Callback f$1;
                public final /* synthetic */ SignalPinReminderDialog.PinVerifier.Callback f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SignalPinReminderDialog.m2020$r8$lambda$XI6rQBbUsJQA0lvMxwN6ZEw6Q4(AlertDialog.this, this.f$1, this.f$2, view);
                }
            });
            requireViewById2.setEnabled(false);
            requireViewById2.setOnClickListener(new View.OnClickListener(editText, v2PinVerifier, pinWatcherCallback) { // from class: org.thoughtcrime.securesms.lock.SignalPinReminderDialog$$ExternalSyntheticLambda2
                public final /* synthetic */ EditText f$0;
                public final /* synthetic */ SignalPinReminderDialog.PinVerifier f$1;
                public final /* synthetic */ SignalPinReminderDialog.PinVerifier.Callback f$2;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SignalPinReminderDialog.m2021$r8$lambda$gBeawwubsCr4g4LGE_SZ24ozDM(this.f$0, this.f$1, this.f$2, view);
                }
            });
            editText.addTextChangedListener(new SimpleTextWatcher() { // from class: org.thoughtcrime.securesms.lock.SignalPinReminderDialog.2
                private final String localHash;

                {
                    String localPinHash = SignalStore.kbsValues().getLocalPinHash();
                    Objects.requireNonNull(localPinHash);
                    this.localHash = localPinHash;
                }

                @Override // org.thoughtcrime.securesms.contactshare.SimpleTextWatcher
                public void onTextChanged(String str) {
                    if (str.length() >= 4) {
                        requireViewById2.setEnabled(true);
                        if (PinHashing.verifyLocalPinHash(this.localHash, str)) {
                            create.dismiss();
                            callback.onReminderCompleted(str, pinWatcherCallback.hadWrongGuess());
                            return;
                        }
                        return;
                    }
                    requireViewById2.setEnabled(false);
                }
            });
            return;
        }
        throw new AssertionError("Must have a PIN!");
    }

    /* renamed from: org.thoughtcrime.securesms.lock.SignalPinReminderDialog$4 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass4 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$lock$v2$PinKeyboardType;

        static {
            int[] iArr = new int[PinKeyboardType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$lock$v2$PinKeyboardType = iArr;
            try {
                iArr[PinKeyboardType.NUMERIC.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$lock$v2$PinKeyboardType[PinKeyboardType.ALPHA_NUMERIC.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public static /* synthetic */ void lambda$show$1(AlertDialog alertDialog, Callback callback, PinVerifier.Callback callback2, View view) {
        alertDialog.dismiss();
        callback.onReminderDismissed(callback2.hadWrongGuess());
    }

    public static /* synthetic */ void lambda$show$2(EditText editText, PinVerifier pinVerifier, PinVerifier.Callback callback, View view) {
        String str;
        Editable text = editText.getText();
        if (text == null) {
            str = null;
        } else {
            str = text.toString();
        }
        pinVerifier.verifyPin(str, callback);
    }

    private static PinVerifier.Callback getPinWatcherCallback(final Context context, final AlertDialog alertDialog, final EditText editText, final TextView textView, final Callback callback) {
        return new PinVerifier.Callback() { // from class: org.thoughtcrime.securesms.lock.SignalPinReminderDialog.3
            boolean hadWrongGuess = false;

            @Override // org.thoughtcrime.securesms.lock.SignalPinReminderDialog.PinVerifier.Callback
            public void onPinCorrect(String str) {
                Log.i(SignalPinReminderDialog.TAG, "Correct PIN entry.");
                alertDialog.dismiss();
                callback.onReminderCompleted(str, this.hadWrongGuess);
            }

            @Override // org.thoughtcrime.securesms.lock.SignalPinReminderDialog.PinVerifier.Callback
            public void onPinWrong() {
                Log.i(SignalPinReminderDialog.TAG, "Incorrect PIN entry.");
                this.hadWrongGuess = true;
                editText.getText().clear();
                textView.setText(context.getString(R.string.KbsReminderDialog__incorrect_pin_try_again));
            }

            @Override // org.thoughtcrime.securesms.lock.SignalPinReminderDialog.PinVerifier.Callback
            public boolean hadWrongGuess() {
                return this.hadWrongGuess;
            }
        };
    }

    /* loaded from: classes4.dex */
    public static final class V2PinVerifier implements PinVerifier {
        private final String localPinHash;

        V2PinVerifier() {
            String localPinHash = SignalStore.kbsValues().getLocalPinHash();
            this.localPinHash = localPinHash;
            if (localPinHash == null) {
                throw new AssertionError("No local pin hash set at time of reminder");
            }
        }

        @Override // org.thoughtcrime.securesms.lock.SignalPinReminderDialog.PinVerifier
        public void verifyPin(String str, PinVerifier.Callback callback) {
            if (str == null || TextUtils.isEmpty(str) || str.length() < 4) {
                return;
            }
            if (PinHashing.verifyLocalPinHash(this.localPinHash, str)) {
                callback.onPinCorrect(str);
            } else {
                callback.onPinWrong();
            }
        }
    }
}
