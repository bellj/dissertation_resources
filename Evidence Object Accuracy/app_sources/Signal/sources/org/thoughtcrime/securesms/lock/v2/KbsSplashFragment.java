package org.thoughtcrime.securesms.lock.v2;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.v2.KbsSplashFragmentDirections;
import org.thoughtcrime.securesms.pin.PinOptOutDialog;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class KbsSplashFragment extends Fragment {
    private TextView description;
    private TextView primaryAction;
    private TextView secondaryAction;
    private TextView title;

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.kbs_splash_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.title = (TextView) view.findViewById(R.id.kbs_splash_title);
        this.description = (TextView) view.findViewById(R.id.kbs_splash_description);
        this.primaryAction = (TextView) view.findViewById(R.id.kbs_splash_primary_action);
        this.secondaryAction = (TextView) view.findViewById(R.id.kbs_splash_secondary_action);
        this.primaryAction.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.lock.v2.KbsSplashFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                KbsSplashFragment.$r8$lambda$7ZRI_0ryOa0ipenSJaieZtYk6WQ(KbsSplashFragment.this, view2);
            }
        });
        this.secondaryAction.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.lock.v2.KbsSplashFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                KbsSplashFragment.m2025$r8$lambda$KdL3BHFsUx7NwRxhJ51gTe2DUY(KbsSplashFragment.this, view2);
            }
        });
        if (RegistrationLockUtil.userHasRegistrationLock(requireContext())) {
            setUpRegLockEnabled();
        } else {
            setUpRegLockDisabled();
        }
        this.description.setMovementMethod(LinkMovementMethod.getInstance());
        ((AppCompatActivity) requireActivity()).setSupportActionBar((Toolbar) view.findViewById(R.id.kbs_splash_toolbar));
        ((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle((CharSequence) null);
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) { // from class: org.thoughtcrime.securesms.lock.v2.KbsSplashFragment.1
            @Override // androidx.activity.OnBackPressedCallback
            public void handleOnBackPressed() {
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        onCreatePin();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        onLearnMore();
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.pin_skip, menu);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPrepareOptionsMenu(Menu menu) {
        if (RegistrationLockUtil.userHasRegistrationLock(requireContext())) {
            menu.clear();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_pin_learn_more /* 2131363682 */:
                onLearnMore();
                return true;
            case R.id.menu_pin_skip /* 2131363683 */:
                onPinSkipped();
                return true;
            default:
                return false;
        }
    }

    private void setUpRegLockEnabled() {
        this.title.setText(R.string.KbsSplashFragment__registration_lock_equals_pin);
        this.description.setText(R.string.KbsSplashFragment__your_registration_lock_is_now_called_a_pin);
        this.primaryAction.setText(R.string.KbsSplashFragment__update_pin);
        this.secondaryAction.setText(R.string.KbsSplashFragment__learn_more);
    }

    private void setUpRegLockDisabled() {
        this.title.setText(R.string.KbsSplashFragment__introducing_pins);
        this.description.setText(R.string.KbsSplashFragment__pins_keep_information_stored_with_signal_encrypted);
        this.primaryAction.setText(R.string.KbsSplashFragment__create_your_pin);
        this.secondaryAction.setText(R.string.KbsSplashFragment__learn_more);
    }

    private void onCreatePin() {
        KbsSplashFragmentDirections.ActionCreateKbsPin actionCreateKbsPin = KbsSplashFragmentDirections.actionCreateKbsPin();
        actionCreateKbsPin.setIsPinChange(SignalStore.kbsValues().hasPin());
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), actionCreateKbsPin);
    }

    private void onLearnMore() {
        CommunicationActions.openBrowserLink(requireContext(), getString(R.string.KbsSplashFragment__learn_more_link));
    }

    public /* synthetic */ void lambda$onPinSkipped$2() {
        requireActivity().finish();
    }

    private void onPinSkipped() {
        PinOptOutDialog.show(requireContext(), new Runnable() { // from class: org.thoughtcrime.securesms.lock.v2.KbsSplashFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                KbsSplashFragment.$r8$lambda$6j_nnFGKUBP6_xLUKlgTbeOJRv4(KbsSplashFragment.this);
            }
        });
    }
}
