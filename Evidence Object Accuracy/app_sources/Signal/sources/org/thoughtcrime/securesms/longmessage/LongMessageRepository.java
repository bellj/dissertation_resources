package org.thoughtcrime.securesms.longmessage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import j$.util.Optional;
import java.io.IOException;
import java.io.InputStream;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.longmessage.LongMessageRepository;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.mms.TextSlide;

/* loaded from: classes4.dex */
public class LongMessageRepository {
    private static final String TAG = Log.tag(LongMessageRepository.class);
    private final MessageDatabase mmsDatabase = SignalDatabase.mms();
    private final MessageDatabase smsDatabase = SignalDatabase.sms();

    /* loaded from: classes4.dex */
    public interface Callback<T> {
        void onComplete(T t);
    }

    public void getMessage(Context context, long j, boolean z, Callback<Optional<LongMessage>> callback) {
        SignalExecutors.BOUNDED.execute(new Runnable(z, callback, context, j) { // from class: org.thoughtcrime.securesms.longmessage.LongMessageRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$1;
            public final /* synthetic */ LongMessageRepository.Callback f$2;
            public final /* synthetic */ Context f$3;
            public final /* synthetic */ long f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LongMessageRepository.this.lambda$getMessage$0(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    public /* synthetic */ void lambda$getMessage$0(boolean z, Callback callback, Context context, long j) {
        if (z) {
            callback.onComplete(getMmsLongMessage(context, this.mmsDatabase, j));
        } else {
            callback.onComplete(getSmsLongMessage(context, this.smsDatabase, j));
        }
    }

    private Optional<LongMessage> getMmsLongMessage(Context context, MessageDatabase messageDatabase, long j) {
        Optional<MmsMessageRecord> mmsMessage = getMmsMessage(messageDatabase, j);
        if (!mmsMessage.isPresent()) {
            return Optional.empty();
        }
        TextSlide textSlide = mmsMessage.get().getSlideDeck().getTextSlide();
        if (textSlide == null || textSlide.getUri() == null) {
            return Optional.of(new LongMessage(ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(context, mmsMessage.get())));
        }
        return Optional.of(new LongMessage(ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(context, mmsMessage.get(), readFullBody(context, textSlide.getUri()))));
    }

    private Optional<LongMessage> getSmsLongMessage(Context context, MessageDatabase messageDatabase, long j) {
        Optional<MessageRecord> smsMessage = getSmsMessage(messageDatabase, j);
        if (smsMessage.isPresent()) {
            return Optional.of(new LongMessage(ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(context, smsMessage.get())));
        }
        return Optional.empty();
    }

    private Optional<MmsMessageRecord> getMmsMessage(MessageDatabase messageDatabase, long j) {
        Cursor messageCursor = messageDatabase.getMessageCursor(j);
        try {
            Optional<MmsMessageRecord> ofNullable = Optional.ofNullable((MmsMessageRecord) MmsDatabase.readerFor(messageCursor).getNext());
            if (messageCursor != null) {
                messageCursor.close();
            }
            return ofNullable;
        } catch (Throwable th) {
            if (messageCursor != null) {
                try {
                    messageCursor.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private Optional<MessageRecord> getSmsMessage(MessageDatabase messageDatabase, long j) {
        Cursor messageCursor = messageDatabase.getMessageCursor(j);
        try {
            Optional<MessageRecord> ofNullable = Optional.ofNullable(SmsDatabase.readerFor(messageCursor).getNext());
            if (messageCursor != null) {
                messageCursor.close();
            }
            return ofNullable;
        } catch (Throwable th) {
            if (messageCursor != null) {
                try {
                    messageCursor.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private String readFullBody(Context context, Uri uri) {
        try {
            InputStream attachmentStream = PartAuthority.getAttachmentStream(context, uri);
            String readFullyAsString = StreamUtil.readFullyAsString(attachmentStream);
            if (attachmentStream != null) {
                attachmentStream.close();
            }
            return readFullyAsString;
        } catch (IOException e) {
            Log.w(TAG, "Failed to read full text body.", e);
            return "";
        }
    }
}
