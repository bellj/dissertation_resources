package org.thoughtcrime.securesms.longmessage;

import android.content.Context;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.model.MessageRecord;

/* loaded from: classes4.dex */
public class LongMessage {
    private final ConversationMessage conversationMessage;

    public LongMessage(ConversationMessage conversationMessage) {
        this.conversationMessage = conversationMessage;
    }

    public MessageRecord getMessageRecord() {
        return this.conversationMessage.getMessageRecord();
    }

    public CharSequence getFullBody(Context context) {
        return this.conversationMessage.getDisplayBody(context);
    }
}
