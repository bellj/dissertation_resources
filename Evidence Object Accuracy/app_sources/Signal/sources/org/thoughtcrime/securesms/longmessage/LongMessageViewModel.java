package org.thoughtcrime.securesms.longmessage;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.Optional;
import java.util.Objects;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.longmessage.LongMessageRepository;

/* loaded from: classes4.dex */
public class LongMessageViewModel extends ViewModel {
    private final MutableLiveData<Optional<LongMessage>> message;
    private final DatabaseObserver.Observer threadObserver;

    private LongMessageViewModel(Application application, LongMessageRepository longMessageRepository, long j, boolean z) {
        this.message = new MutableLiveData<>();
        this.threadObserver = new DatabaseObserver.Observer(longMessageRepository, application, j, z) { // from class: org.thoughtcrime.securesms.longmessage.LongMessageViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ LongMessageRepository f$1;
            public final /* synthetic */ Application f$2;
            public final /* synthetic */ long f$3;
            public final /* synthetic */ boolean f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r6;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                LongMessageViewModel.this.lambda$new$0(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        };
        longMessageRepository.getMessage(application, j, z, new LongMessageRepository.Callback() { // from class: org.thoughtcrime.securesms.longmessage.LongMessageViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.longmessage.LongMessageRepository.Callback
            public final void onComplete(Object obj) {
                LongMessageViewModel.this.lambda$new$1((Optional) obj);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(LongMessageRepository longMessageRepository, Application application, long j, boolean z) {
        MutableLiveData<Optional<LongMessage>> mutableLiveData = this.message;
        Objects.requireNonNull(mutableLiveData);
        longMessageRepository.getMessage(application, j, z, new LongMessageRepository.Callback() { // from class: org.thoughtcrime.securesms.longmessage.LongMessageViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.longmessage.LongMessageRepository.Callback
            public final void onComplete(Object obj) {
                MutableLiveData.this.postValue((Optional) obj);
            }
        });
    }

    public /* synthetic */ void lambda$new$1(Optional optional) {
        if (optional.isPresent()) {
            ApplicationDependencies.getDatabaseObserver().registerConversationObserver(((LongMessage) optional.get()).getMessageRecord().getThreadId(), this.threadObserver);
        }
        this.message.postValue(optional);
    }

    public LiveData<Optional<LongMessage>> getMessage() {
        return this.message;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.threadObserver);
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application context;
        private final boolean isMms;
        private final long messageId;
        private final LongMessageRepository repository;

        public Factory(Application application, LongMessageRepository longMessageRepository, long j, boolean z) {
            this.context = application;
            this.repository = longMessageRepository;
            this.messageId = j;
            this.isMms = z;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new LongMessageViewModel(this.context, this.repository, this.messageId, this.isMms));
        }
    }
}
