package org.thoughtcrime.securesms.longmessage;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Toast;
import androidx.core.content.ContextCompat;
import androidx.core.text.util.LinkifyCompat;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import j$.util.Optional;
import java.util.Collections;
import java.util.Locale;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ConversationItemFooter;
import org.thoughtcrime.securesms.components.FullScreenDialogFragment;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.conversation.colors.ColorizerView;
import org.thoughtcrime.securesms.groups.v2.GroupDescriptionUtil$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.longmessage.LongMessageViewModel;
import org.thoughtcrime.securesms.util.LinkUtil;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.views.Stub;

/* loaded from: classes4.dex */
public class LongMessageFragment extends FullScreenDialogFragment {
    private static final String KEY_IS_MMS;
    private static final String KEY_MESSAGE_ID;
    private static final int MAX_DISPLAY_LENGTH;
    private BubbleLayoutListener bubbleLayoutListener;
    private ColorizerView colorizerView;
    private Stub<ViewGroup> receivedBubble;
    private Stub<ViewGroup> sentBubble;
    private LongMessageViewModel viewModel;

    @Override // org.thoughtcrime.securesms.components.FullScreenDialogFragment
    protected int getDialogLayoutResource() {
        return R.layout.longmessage_fragment;
    }

    @Override // org.thoughtcrime.securesms.components.FullScreenDialogFragment
    protected int getTitle() {
        return -1;
    }

    public static DialogFragment create(long j, boolean z) {
        LongMessageFragment longMessageFragment = new LongMessageFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("message_id", j);
        bundle.putBoolean("is_mms", z);
        longMessageFragment.setArguments(bundle);
        return longMessageFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.sentBubble = new Stub<>((ViewStub) view.findViewById(R.id.longmessage_sent_stub));
        this.receivedBubble = new Stub<>((ViewStub) view.findViewById(R.id.longmessage_received_stub));
        this.colorizerView = (ColorizerView) view.findViewById(R.id.colorizer);
        this.bubbleLayoutListener = new BubbleLayoutListener();
        initViewModel(requireArguments().getLong("message_id", -1), requireArguments().getBoolean("is_mms", false));
    }

    private void initViewModel(long j, boolean z) {
        LongMessageViewModel longMessageViewModel = (LongMessageViewModel) new ViewModelProvider(this, new LongMessageViewModel.Factory(requireActivity().getApplication(), new LongMessageRepository(), j, z)).get(LongMessageViewModel.class);
        this.viewModel = longMessageViewModel;
        longMessageViewModel.getMessage().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.longmessage.LongMessageFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                LongMessageFragment.this.lambda$initViewModel$0((Optional) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initViewModel$0(Optional optional) {
        ViewGroup viewGroup;
        if (optional != null) {
            if (!optional.isPresent()) {
                Toast.makeText(requireContext(), (int) R.string.LongMessageActivity_unable_to_find_message, 0).show();
                dismissAllowingStateLoss();
                return;
            }
            if (((LongMessage) optional.get()).getMessageRecord().isOutgoing()) {
                this.toolbar.setTitle(getString(R.string.LongMessageActivity_your_message));
            } else {
                this.toolbar.setTitle(getString(R.string.LongMessageActivity_message_from_s, ((LongMessage) optional.get()).getMessageRecord().getRecipient().getDisplayName(requireContext())));
            }
            if (((LongMessage) optional.get()).getMessageRecord().isOutgoing()) {
                viewGroup = this.sentBubble.get();
                this.colorizerView.setVisibility(0);
                this.colorizerView.setBackground(((LongMessage) optional.get()).getMessageRecord().getRecipient().getChatColors().getChatBubbleMask());
                viewGroup.getBackground().setColorFilter(((LongMessage) optional.get()).getMessageRecord().getRecipient().getChatColors().getChatBubbleColorFilter());
                viewGroup.addOnLayoutChangeListener(this.bubbleLayoutListener);
                this.bubbleLayoutListener.onLayoutChange(viewGroup, 0, 0, 0, 0, 0, 0, 0, 0);
            } else {
                viewGroup = this.receivedBubble.get();
                viewGroup.getBackground().setColorFilter(ContextCompat.getColor(requireContext(), R.color.signal_background_secondary), PorterDuff.Mode.MULTIPLY);
            }
            EmojiTextView emojiTextView = (EmojiTextView) viewGroup.findViewById(R.id.longmessage_text);
            ConversationItemFooter conversationItemFooter = (ConversationItemFooter) viewGroup.findViewById(R.id.longmessage_footer);
            SpannableString linkifyMessageBody = linkifyMessageBody(new SpannableString(getTrimmedBody(((LongMessage) optional.get()).getFullBody(requireContext()))));
            viewGroup.setVisibility(0);
            emojiTextView.setText(linkifyMessageBody);
            emojiTextView.setMovementMethod(LinkMovementMethod.getInstance());
            emojiTextView.setTextSize(2, (float) SignalStore.settings().getMessageFontSize());
            if (!((LongMessage) optional.get()).getMessageRecord().isOutgoing()) {
                emojiTextView.setMentionBackgroundTint(ContextCompat.getColor(requireContext(), ThemeUtil.isDarkTheme(requireActivity()) ? R.color.core_grey_60 : R.color.core_grey_20));
            } else {
                emojiTextView.setMentionBackgroundTint(ContextCompat.getColor(requireContext(), R.color.transparent_black_40));
            }
            conversationItemFooter.setMessageRecord(((LongMessage) optional.get()).getMessageRecord(), Locale.getDefault());
        }
    }

    private CharSequence getTrimmedBody(CharSequence charSequence) {
        return charSequence.length() <= MAX_DISPLAY_LENGTH ? charSequence : charSequence.subSequence(0, MAX_DISPLAY_LENGTH);
    }

    private SpannableString linkifyMessageBody(SpannableString spannableString) {
        if (LinkifyCompat.addLinks(spannableString, 7)) {
            Stream.of((URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class)).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.longmessage.LongMessageFragment$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return LongMessageFragment.lambda$linkifyMessageBody$1((URLSpan) obj);
                }
            }).forEach(new GroupDescriptionUtil$$ExternalSyntheticLambda1(spannableString));
        }
        return spannableString;
    }

    public static /* synthetic */ boolean lambda$linkifyMessageBody$1(URLSpan uRLSpan) {
        return LinkUtil.isLegalUrl(uRLSpan.getURL());
    }

    /* loaded from: classes4.dex */
    public final class BubbleLayoutListener implements View.OnLayoutChangeListener {
        private BubbleLayoutListener() {
            LongMessageFragment.this = r1;
        }

        @Override // android.view.View.OnLayoutChangeListener
        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            LongMessageFragment.this.colorizerView.setProjections(Collections.singletonList(Projection.relativeToViewWithCommonRoot(view, LongMessageFragment.this.colorizerView, new Projection.Corners(16.0f))));
        }
    }
}
