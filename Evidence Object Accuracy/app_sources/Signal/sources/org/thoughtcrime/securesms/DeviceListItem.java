package org.thoughtcrime.securesms;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Locale;
import org.thoughtcrime.securesms.devicelist.Device;
import org.thoughtcrime.securesms.util.DateUtils;

/* loaded from: classes.dex */
public class DeviceListItem extends LinearLayout {
    private TextView created;
    private long deviceId;
    private TextView lastActive;
    private TextView name;

    public DeviceListItem(Context context) {
        super(context);
    }

    public DeviceListItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.name = (TextView) findViewById(R.id.name);
        this.created = (TextView) findViewById(R.id.created);
        this.lastActive = (TextView) findViewById(R.id.active);
    }

    public void set(Device device, Locale locale) {
        if (TextUtils.isEmpty(device.getName())) {
            this.name.setText(R.string.DeviceListItem_unnamed_device);
        } else {
            this.name.setText(device.getName());
        }
        this.created.setText(getContext().getString(R.string.DeviceListItem_linked_s, DateUtils.getDayPrecisionTimeSpanString(getContext(), locale, device.getCreated())));
        this.lastActive.setText(getContext().getString(R.string.DeviceListItem_last_active_s, DateUtils.getDayPrecisionTimeSpanString(getContext(), locale, device.getLastSeen())));
        this.deviceId = device.getId();
    }

    public long getDeviceId() {
        return this.deviceId;
    }

    public String getDeviceName() {
        return this.name.getText().toString();
    }
}
