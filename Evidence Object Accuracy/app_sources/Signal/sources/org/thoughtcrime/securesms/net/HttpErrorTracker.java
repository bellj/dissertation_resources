package org.thoughtcrime.securesms.net;

import java.util.Arrays;

/* loaded from: classes4.dex */
public final class HttpErrorTracker {
    private final long errorTimeRange;
    private final long[] timestamps;

    public HttpErrorTracker(int i, long j) {
        this.timestamps = new long[i];
        this.errorTimeRange = j;
    }

    public synchronized boolean addSample(long j) {
        long[] jArr;
        long j2 = j - this.errorTimeRange;
        int i = 0;
        int i2 = 0;
        int i3 = 1;
        while (true) {
            jArr = this.timestamps;
            if (i >= jArr.length) {
                break;
            }
            long j3 = jArr[i];
            if (j3 < j2) {
                jArr[i] = 0;
            } else if (j3 != 0) {
                i3++;
            }
            if (jArr[i] < jArr[i2]) {
                i2 = i;
            }
            i++;
        }
        jArr[i2] = j;
        if (i3 < jArr.length) {
            return false;
        }
        Arrays.fill(jArr, 0L);
        return true;
    }
}
