package org.thoughtcrime.securesms.net;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import okhttp3.Dns;
import org.signal.core.util.logging.Log;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.SimpleResolver;

/* loaded from: classes4.dex */
public class CustomDns implements Dns {
    private static final String TAG = Log.tag(CustomDns.class);
    private final String dnsHostname;

    public CustomDns(String str) {
        this.dnsHostname = str;
    }

    @Override // okhttp3.Dns
    public List<InetAddress> lookup(String str) throws UnknownHostException {
        SimpleResolver simpleResolver = new SimpleResolver(this.dnsHostname);
        Lookup doLookup = doLookup(str);
        doLookup.setResolver(simpleResolver);
        Record[] run = doLookup.run();
        if (run != null) {
            List<InetAddress> list = Stream.of(run).filter(new Predicate() { // from class: org.thoughtcrime.securesms.net.CustomDns$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return CustomDns.$r8$lambda$ZxKd6sPHukz3bl9YfoXhfTLL8DQ((Record) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.net.CustomDns$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return CustomDns.$r8$lambda$HfPsD4uLhr86Uy6wHxoD40bl47U((Record) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.net.CustomDns$$ExternalSyntheticLambda2
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ((ARecord) obj).getAddress();
                }
            }).toList();
            if (list.size() > 0) {
                return list;
            }
        }
        throw new UnknownHostException(str);
    }

    public static /* synthetic */ boolean lambda$lookup$0(Record record) {
        return record.getType() == 1;
    }

    public static /* synthetic */ ARecord lambda$lookup$1(Record record) {
        return (ARecord) record;
    }

    private static Lookup doLookup(String str) throws UnknownHostException {
        try {
            return new Lookup(str);
        } catch (Throwable th) {
            Log.w(TAG, th);
            throw new UnknownHostException();
        }
    }
}
