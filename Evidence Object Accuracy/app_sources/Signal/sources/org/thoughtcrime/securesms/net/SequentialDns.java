package org.thoughtcrime.securesms.net;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import okhttp3.Dns;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.NetworkUtil;

/* loaded from: classes4.dex */
public class SequentialDns implements Dns {
    private static final String TAG = Log.tag(SequentialDns.class);
    private List<Dns> dnsList;

    public SequentialDns(Dns... dnsArr) {
        this.dnsList = Arrays.asList(dnsArr);
    }

    @Override // okhttp3.Dns
    public List<InetAddress> lookup(String str) throws UnknownHostException {
        List<InetAddress> lookup;
        for (Dns dns : this.dnsList) {
            try {
                lookup = dns.lookup(str);
            } catch (UnknownHostException unused) {
                Log.w(TAG, String.format(Locale.ENGLISH, "Failed to resolve %s using %s. Continuing. Network Type: %s", str, dns.getClass().getSimpleName(), NetworkUtil.getNetworkTypeDescriptor(ApplicationDependencies.getApplication())));
            }
            if (lookup.size() > 0) {
                return lookup;
            }
            Log.w(TAG, String.format(Locale.ENGLISH, "Didn't find any addresses for %s using %s. Continuing.", str, dns.getClass().getSimpleName()));
        }
        String str2 = TAG;
        Log.w(str2, "Failed to resolve using any DNS. Network Type: " + NetworkUtil.getNetworkTypeDescriptor(ApplicationDependencies.getApplication()));
        throw new UnknownHostException(str);
    }
}
