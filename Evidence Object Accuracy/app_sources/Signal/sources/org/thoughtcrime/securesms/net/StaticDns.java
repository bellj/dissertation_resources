package org.thoughtcrime.securesms.net;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.random.Random;
import okhttp3.Dns;

/* compiled from: StaticDns.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001f\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00050\u0003¢\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u0004H\u0016R \u0010\u0002\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00050\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/net/StaticDns;", "Lokhttp3/Dns;", "hostnameMap", "", "", "", "(Ljava/util/Map;)V", "lookup", "", "Ljava/net/InetAddress;", "hostname", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StaticDns implements Dns {
    private final Map<String, Set<String>> hostnameMap;

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Map<java.lang.String, ? extends java.util.Set<java.lang.String>> */
    /* JADX WARN: Multi-variable type inference failed */
    public StaticDns(Map<String, ? extends Set<String>> map) {
        Intrinsics.checkNotNullParameter(map, "hostnameMap");
        this.hostnameMap = map;
    }

    @Override // okhttp3.Dns
    public List<InetAddress> lookup(String str) throws UnknownHostException {
        Intrinsics.checkNotNullParameter(str, "hostname");
        Set<String> set = this.hostnameMap.get(str);
        if (set != null && (!set.isEmpty())) {
            return CollectionsKt__CollectionsJVMKt.listOf(InetAddress.getByName((String) CollectionsKt___CollectionsKt.random(set, Random.Default)));
        }
        throw new UnknownHostException(str);
    }
}
