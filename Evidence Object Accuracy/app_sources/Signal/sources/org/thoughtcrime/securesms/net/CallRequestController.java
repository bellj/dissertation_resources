package org.thoughtcrime.securesms.net;

import android.os.AsyncTask;
import j$.util.Optional;
import java.io.InputStream;
import okhttp3.Call;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class CallRequestController implements RequestController {
    private final Call call;
    private boolean canceled;
    private InputStream stream;

    public CallRequestController(Call call) {
        this.call = call;
    }

    @Override // org.thoughtcrime.securesms.net.RequestController
    public void cancel() {
        AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable() { // from class: org.thoughtcrime.securesms.net.CallRequestController$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                CallRequestController.this.lambda$cancel$0();
            }
        });
    }

    public /* synthetic */ void lambda$cancel$0() {
        synchronized (this) {
            if (!this.canceled) {
                this.call.cancel();
                this.canceled = true;
            }
        }
    }

    public synchronized void setStream(InputStream inputStream) {
        this.stream = inputStream;
        notifyAll();
    }

    public synchronized Optional<InputStream> getStream() {
        InputStream inputStream;
        while (true) {
            inputStream = this.stream;
            if (inputStream != null || this.canceled) {
                break;
            }
            Util.wait(this, 0);
        }
        return Optional.ofNullable(inputStream);
    }
}
