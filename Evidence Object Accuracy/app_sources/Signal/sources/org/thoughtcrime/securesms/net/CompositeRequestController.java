package org.thoughtcrime.securesms.net;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes4.dex */
public class CompositeRequestController implements RequestController {
    private boolean canceled = false;
    private final List<RequestController> controllers = new ArrayList();

    public synchronized void addController(RequestController requestController) {
        if (this.canceled) {
            requestController.cancel();
        } else {
            this.controllers.add(requestController);
        }
    }

    @Override // org.thoughtcrime.securesms.net.RequestController
    public synchronized void cancel() {
        this.canceled = true;
        Stream.of(this.controllers).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.net.CompositeRequestController$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                ((RequestController) obj).cancel();
            }
        });
    }

    public synchronized boolean isCanceled() {
        return this.canceled;
    }
}
