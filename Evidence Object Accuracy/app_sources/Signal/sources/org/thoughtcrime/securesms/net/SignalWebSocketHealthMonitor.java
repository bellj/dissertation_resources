package org.thoughtcrime.securesms.net;

import android.app.Application;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.ReminderUpdateEvent;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.net.SignalWebSocketHealthMonitor;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.SignalWebSocket;
import org.whispersystems.signalservice.api.util.Preconditions;
import org.whispersystems.signalservice.api.util.SleepTimer;
import org.whispersystems.signalservice.api.websocket.HealthMonitor;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;

/* loaded from: classes4.dex */
public final class SignalWebSocketHealthMonitor implements HealthMonitor {
    private static final long KEEP_ALIVE_SEND_CADENCE;
    private static final long MAX_TIME_SINCE_SUCCESSFUL_KEEP_ALIVE;
    private static final String TAG = Log.tag(SignalWebSocketHealthMonitor.class);
    private final Application context;
    private final Executor executor = ThreadUtil.trace(Executors.newSingleThreadExecutor());
    private final HealthState identified = new HealthState(null);
    private KeepAliveSender keepAliveSender;
    private SignalWebSocket signalWebSocket;
    private final SleepTimer sleepTimer;
    private final HealthState unidentified = new HealthState(null);

    static {
        TAG = Log.tag(SignalWebSocketHealthMonitor.class);
        long millis = TimeUnit.SECONDS.toMillis(55);
        KEEP_ALIVE_SEND_CADENCE = millis;
        MAX_TIME_SINCE_SUCCESSFUL_KEEP_ALIVE = millis * 3;
    }

    public SignalWebSocketHealthMonitor(Application application, SleepTimer sleepTimer) {
        this.context = application;
        this.sleepTimer = sleepTimer;
    }

    public void monitor(SignalWebSocket signalWebSocket) {
        this.executor.execute(new Runnable(signalWebSocket) { // from class: org.thoughtcrime.securesms.net.SignalWebSocketHealthMonitor$$ExternalSyntheticLambda4
            public final /* synthetic */ SignalWebSocket f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalWebSocketHealthMonitor.this.lambda$monitor$2(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$monitor$2(SignalWebSocket signalWebSocket) {
        Preconditions.checkNotNull(signalWebSocket);
        Preconditions.checkArgument(this.signalWebSocket == null, "monitor can only be called once");
        this.signalWebSocket = signalWebSocket;
        signalWebSocket.getWebSocketState().subscribeOn(Schedulers.computation()).observeOn(Schedulers.computation()).distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.net.SignalWebSocketHealthMonitor$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SignalWebSocketHealthMonitor.this.lambda$monitor$0((WebSocketConnectionState) obj);
            }
        });
        signalWebSocket.getUnidentifiedWebSocketState().subscribeOn(Schedulers.computation()).observeOn(Schedulers.computation()).distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.net.SignalWebSocketHealthMonitor$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SignalWebSocketHealthMonitor.this.lambda$monitor$1((WebSocketConnectionState) obj);
            }
        });
    }

    public /* synthetic */ void lambda$monitor$0(WebSocketConnectionState webSocketConnectionState) throws Throwable {
        onStateChange(webSocketConnectionState, this.identified);
    }

    public /* synthetic */ void lambda$monitor$1(WebSocketConnectionState webSocketConnectionState) throws Throwable {
        onStateChange(webSocketConnectionState, this.unidentified);
    }

    /* renamed from: org.thoughtcrime.securesms.net.SignalWebSocketHealthMonitor$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState;

        static {
            int[] iArr = new int[WebSocketConnectionState.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState = iArr;
            try {
                iArr[WebSocketConnectionState.CONNECTED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState[WebSocketConnectionState.AUTHENTICATION_FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState[WebSocketConnectionState.FAILED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private void onStateChange(WebSocketConnectionState webSocketConnectionState, HealthState healthState) {
        this.executor.execute(new Runnable(webSocketConnectionState, healthState) { // from class: org.thoughtcrime.securesms.net.SignalWebSocketHealthMonitor$$ExternalSyntheticLambda5
            public final /* synthetic */ WebSocketConnectionState f$1;
            public final /* synthetic */ SignalWebSocketHealthMonitor.HealthState f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalWebSocketHealthMonitor.this.lambda$onStateChange$3(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onStateChange$3(WebSocketConnectionState webSocketConnectionState, HealthState healthState) {
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState[webSocketConnectionState.ordinal()];
        boolean z = false;
        if (i == 1) {
            TextSecurePreferences.setUnauthorizedReceived(this.context, false);
        } else if (i == 2) {
            TextSecurePreferences.setUnauthorizedReceived(this.context, true);
            EventBus.getDefault().post(new ReminderUpdateEvent());
        } else if (i == 3 && SignalStore.proxy().isProxyEnabled()) {
            Log.w(TAG, "Encountered an error while we had a proxy set! Terminating the connection to prevent retry spam.");
            ApplicationDependencies.closeConnections();
        }
        if (webSocketConnectionState == WebSocketConnectionState.CONNECTED) {
            z = true;
        }
        healthState.needsKeepAlive = z;
        if (this.keepAliveSender == null && isKeepAliveNecessary()) {
            KeepAliveSender keepAliveSender = new KeepAliveSender(this, null);
            this.keepAliveSender = keepAliveSender;
            keepAliveSender.start();
        } else if (this.keepAliveSender != null && !isKeepAliveNecessary()) {
            this.keepAliveSender.shutdown();
            this.keepAliveSender = null;
        }
    }

    @Override // org.whispersystems.signalservice.api.websocket.HealthMonitor
    public void onKeepAliveResponse(long j, boolean z) {
        this.executor.execute(new Runnable(z) { // from class: org.thoughtcrime.securesms.net.SignalWebSocketHealthMonitor$$ExternalSyntheticLambda3
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalWebSocketHealthMonitor.this.lambda$onKeepAliveResponse$4(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onKeepAliveResponse$4(boolean z) {
        if (z) {
            this.identified.lastKeepAliveReceived = System.currentTimeMillis();
            return;
        }
        this.unidentified.lastKeepAliveReceived = System.currentTimeMillis();
    }

    @Override // org.whispersystems.signalservice.api.websocket.HealthMonitor
    public void onMessageError(int i, boolean z) {
        this.executor.execute(new Runnable(i, z) { // from class: org.thoughtcrime.securesms.net.SignalWebSocketHealthMonitor$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$1;
            public final /* synthetic */ boolean f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalWebSocketHealthMonitor.this.lambda$onMessageError$5(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onMessageError$5(int i, boolean z) {
        if (i == 409) {
            if ((z ? this.identified : this.unidentified).mismatchErrorTracker.addSample(System.currentTimeMillis())) {
                Log.w(TAG, "Received too many mismatch device errors, forcing new websockets.");
                this.signalWebSocket.forceNewWebSockets();
            }
        }
    }

    public boolean isKeepAliveNecessary() {
        return this.identified.needsKeepAlive || this.unidentified.needsKeepAlive;
    }

    /* loaded from: classes4.dex */
    public static class HealthState {
        private volatile long lastKeepAliveReceived;
        private final HttpErrorTracker mismatchErrorTracker;
        private volatile boolean needsKeepAlive;

        private HealthState() {
            this.mismatchErrorTracker = new HttpErrorTracker(5, TimeUnit.MINUTES.toMillis(1));
        }

        /* synthetic */ HealthState(AnonymousClass1 r1) {
            this();
        }
    }

    /* loaded from: classes4.dex */
    public class KeepAliveSender extends Thread {
        private volatile boolean shouldKeepRunning;

        private KeepAliveSender() {
            SignalWebSocketHealthMonitor.this = r1;
            this.shouldKeepRunning = true;
        }

        /* synthetic */ KeepAliveSender(SignalWebSocketHealthMonitor signalWebSocketHealthMonitor, AnonymousClass1 r2) {
            this();
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            SignalWebSocketHealthMonitor.this.identified.lastKeepAliveReceived = System.currentTimeMillis();
            SignalWebSocketHealthMonitor.this.unidentified.lastKeepAliveReceived = System.currentTimeMillis();
            while (this.shouldKeepRunning && SignalWebSocketHealthMonitor.this.isKeepAliveNecessary()) {
                try {
                    SignalWebSocketHealthMonitor.this.sleepTimer.sleep(SignalWebSocketHealthMonitor.KEEP_ALIVE_SEND_CADENCE);
                    if (this.shouldKeepRunning && SignalWebSocketHealthMonitor.this.isKeepAliveNecessary()) {
                        long currentTimeMillis = System.currentTimeMillis() - SignalWebSocketHealthMonitor.MAX_TIME_SINCE_SUCCESSFUL_KEEP_ALIVE;
                        if (SignalWebSocketHealthMonitor.this.identified.lastKeepAliveReceived >= currentTimeMillis && SignalWebSocketHealthMonitor.this.unidentified.lastKeepAliveReceived >= currentTimeMillis) {
                            SignalWebSocketHealthMonitor.this.signalWebSocket.sendKeepAlive();
                        }
                        String str = SignalWebSocketHealthMonitor.TAG;
                        Log.w(str, "Missed keep alives, identified last: " + SignalWebSocketHealthMonitor.this.identified.lastKeepAliveReceived + " unidentified last: " + SignalWebSocketHealthMonitor.this.unidentified.lastKeepAliveReceived + " needed by: " + currentTimeMillis);
                        SignalWebSocketHealthMonitor.this.signalWebSocket.forceNewWebSockets();
                    }
                } catch (Throwable th) {
                    Log.w(SignalWebSocketHealthMonitor.TAG, th);
                }
            }
        }

        public void shutdown() {
            this.shouldKeepRunning = false;
        }
    }
}
