package org.thoughtcrime.securesms.net;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;

/* loaded from: classes4.dex */
public class ContentProxySelector extends ProxySelector {
    private static final String TAG = Log.tag(ContentProxySelector.class);
    public static final Set<String> WHITELISTED_DOMAINS;
    private final List<Proxy> CONTENT = new ArrayList<Proxy>(1) { // from class: org.thoughtcrime.securesms.net.ContentProxySelector.1
        {
            add(new Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved(BuildConfig.CONTENT_PROXY_HOST, BuildConfig.CONTENT_PROXY_PORT)));
        }
    };

    static {
        TAG = Log.tag(ContentProxySelector.class);
        HashSet hashSet = new HashSet();
        WHITELISTED_DOMAINS = hashSet;
        hashSet.add("giphy.com");
    }

    @Override // java.net.ProxySelector
    public List<Proxy> select(URI uri) {
        for (String str : WHITELISTED_DOMAINS) {
            if (uri.getHost().endsWith(str)) {
                return this.CONTENT;
            }
        }
        throw new IllegalArgumentException("Tried to proxy a non-whitelisted domain.");
    }

    @Override // java.net.ProxySelector
    public void connectFailed(URI uri, SocketAddress socketAddress, IOException iOException) {
        if (iOException instanceof SocketException) {
            Log.d(TAG, "Socket exception. Likely a cancellation.");
        } else {
            Log.w(TAG, "Connection failed.", iOException);
        }
    }
}
