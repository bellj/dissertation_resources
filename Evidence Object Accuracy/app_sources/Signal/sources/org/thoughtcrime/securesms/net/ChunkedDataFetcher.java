package org.thoughtcrime.securesms.net;

import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.bumptech.glide.util.ContentLengthInputStream;
import j$.util.Optional;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.net.ChunkedDataFetcher;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class ChunkedDataFetcher {
    private static final long KB;
    private static final long MB;
    private static final CacheControl NO_CACHE = new CacheControl.Builder().noCache().build();
    private static final String TAG = Log.tag(ChunkedDataFetcher.class);
    private final OkHttpClient client;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onFailure(Exception exc);

        void onSuccess(InputStream inputStream) throws IOException;
    }

    public ChunkedDataFetcher(OkHttpClient okHttpClient) {
        this.client = okHttpClient;
    }

    public RequestController fetch(String str, long j, Callback callback) {
        if (j <= 0) {
            return fetchChunksWithUnknownTotalSize(str, callback);
        }
        CompositeRequestController compositeRequestController = new CompositeRequestController();
        fetchChunks(str, j, Optional.empty(), compositeRequestController, callback);
        return compositeRequestController;
    }

    private RequestController fetchChunksWithUnknownTotalSize(final String str, final Callback callback) {
        final CompositeRequestController compositeRequestController = new CompositeRequestController();
        final long nextInt = (long) (new SecureRandom().nextInt(1024) + 1024);
        Request.Builder cacheControl = new Request.Builder().url(str).cacheControl(NO_CACHE);
        Call newCall = this.client.newCall(cacheControl.addHeader("Range", "bytes=0-" + (nextInt - 1)).addHeader("Accept-Encoding", "identity").build());
        compositeRequestController.addController(new CallRequestController(newCall));
        newCall.enqueue(new okhttp3.Callback() { // from class: org.thoughtcrime.securesms.net.ChunkedDataFetcher.1
            @Override // okhttp3.Callback
            public void onFailure(Call call, IOException iOException) {
                if (!compositeRequestController.isCanceled()) {
                    callback.onFailure(iOException);
                    compositeRequestController.cancel();
                }
            }

            @Override // okhttp3.Callback
            public void onResponse(Call call, Response response) {
                String header = response.header("Content-Range");
                if (!response.isSuccessful()) {
                    String str2 = ChunkedDataFetcher.TAG;
                    Log.w(str2, "Non-successful response code: " + response.code());
                    Callback callback2 = callback;
                    callback2.onFailure(new IOException("Non-successful response code: " + response.code()));
                    compositeRequestController.cancel();
                    if (response.body() != null) {
                        response.body().close();
                    }
                } else if (TextUtils.isEmpty(header)) {
                    Log.w(ChunkedDataFetcher.TAG, "Missing Content-Range header.");
                    callback.onFailure(new IOException("Missing Content-Length header."));
                    compositeRequestController.cancel();
                    if (response.body() != null) {
                        response.body().close();
                    }
                } else if (response.body() == null) {
                    Log.w(ChunkedDataFetcher.TAG, "Missing body.");
                    callback.onFailure(new IOException("Missing body on initial request."));
                    compositeRequestController.cancel();
                } else {
                    Optional parseLengthFromContentRange = ChunkedDataFetcher.this.parseLengthFromContentRange(header);
                    if (!parseLengthFromContentRange.isPresent()) {
                        Log.w(ChunkedDataFetcher.TAG, "Unable to parse length from Content-Range.");
                        callback.onFailure(new IOException("Unable to get parse length from Content-Range."));
                        compositeRequestController.cancel();
                    } else if (nextInt >= ((Long) parseLengthFromContentRange.get()).longValue()) {
                        try {
                            callback.onSuccess(response.body().byteStream());
                        } catch (IOException e) {
                            callback.onFailure(e);
                            compositeRequestController.cancel();
                        }
                    } else {
                        ChunkedDataFetcher.this.fetchChunks(str, ((Long) parseLengthFromContentRange.get()).longValue(), Optional.of(new Pair(ContentLengthInputStream.obtain(response.body().byteStream(), nextInt), Long.valueOf(nextInt))), compositeRequestController, callback);
                    }
                }
            }
        });
        return compositeRequestController;
    }

    public void fetchChunks(String str, long j, Optional<Pair<InputStream, Long>> optional, CompositeRequestController compositeRequestController, Callback callback) {
        List<ByteRange> list;
        try {
            if (optional.isPresent()) {
                list = Stream.of(getRequestPattern(j - optional.get().second().longValue())).map(new Function() { // from class: org.thoughtcrime.securesms.net.ChunkedDataFetcher$$ExternalSyntheticLambda2
                    @Override // com.annimon.stream.function.Function
                    public final Object apply(Object obj) {
                        return ChunkedDataFetcher.lambda$fetchChunks$0(Optional.this, (ChunkedDataFetcher.ByteRange) obj);
                    }
                }).toList();
            } else {
                list = getRequestPattern(j);
            }
            SignalExecutors.UNBOUNDED.execute(new Runnable(list, str, optional, compositeRequestController, callback) { // from class: org.thoughtcrime.securesms.net.ChunkedDataFetcher$$ExternalSyntheticLambda3
                public final /* synthetic */ List f$1;
                public final /* synthetic */ String f$2;
                public final /* synthetic */ Optional f$3;
                public final /* synthetic */ CompositeRequestController f$4;
                public final /* synthetic */ ChunkedDataFetcher.Callback f$5;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                    this.f$5 = r6;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChunkedDataFetcher.this.lambda$fetchChunks$2(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
                }
            });
        } catch (IOException e) {
            callback.onFailure(e);
            compositeRequestController.cancel();
        }
    }

    public static /* synthetic */ ByteRange lambda$fetchChunks$0(Optional optional, ByteRange byteRange) {
        return new ByteRange(((Long) ((Pair) optional.get()).second()).longValue() + byteRange.start, ((Long) ((Pair) optional.get()).second()).longValue() + byteRange.end, byteRange.ignoreFirst);
    }

    public /* synthetic */ CallRequestController lambda$fetchChunks$1(String str, ByteRange byteRange) {
        return makeChunkRequest(this.client, str, byteRange);
    }

    public /* synthetic */ void lambda$fetchChunks$2(List list, String str, Optional optional, CompositeRequestController compositeRequestController, Callback callback) {
        List<CallRequestController> list2 = Stream.of(list).map(new Function(str) { // from class: org.thoughtcrime.securesms.net.ChunkedDataFetcher$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ChunkedDataFetcher.this.lambda$fetchChunks$1(this.f$1, (ChunkedDataFetcher.ByteRange) obj);
            }
        }).toList();
        ArrayList arrayList = new ArrayList(list2.size() + (optional.isPresent() ? 1 : 0));
        if (optional.isPresent()) {
            arrayList.add((InputStream) ((Pair) optional.get()).first());
        }
        Stream of = Stream.of(list2);
        Objects.requireNonNull(compositeRequestController);
        of.forEach(new Consumer() { // from class: org.thoughtcrime.securesms.net.ChunkedDataFetcher$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                CompositeRequestController.this.addController((CallRequestController) obj);
            }
        });
        for (CallRequestController callRequestController : list2) {
            Optional<InputStream> stream = callRequestController.getStream();
            if (!stream.isPresent()) {
                Log.w(TAG, "Stream was canceled.");
                callback.onFailure(new IOException("Failure"));
                compositeRequestController.cancel();
                return;
            }
            arrayList.add(stream.get());
        }
        try {
            callback.onSuccess(new InputStreamList(arrayList));
        } catch (IOException e) {
            callback.onFailure(e);
            compositeRequestController.cancel();
        }
    }

    private CallRequestController makeChunkRequest(OkHttpClient okHttpClient, String str, final ByteRange byteRange) {
        Request.Builder cacheControl = new Request.Builder().url(str).cacheControl(NO_CACHE);
        Call newCall = okHttpClient.newCall(cacheControl.addHeader("Range", "bytes=" + byteRange.start + "-" + byteRange.end).addHeader("Accept-Encoding", "identity").build());
        final CallRequestController callRequestController = new CallRequestController(newCall);
        newCall.enqueue(new okhttp3.Callback() { // from class: org.thoughtcrime.securesms.net.ChunkedDataFetcher.2
            @Override // okhttp3.Callback
            public void onFailure(Call call, IOException iOException) {
                callRequestController.cancel();
            }

            @Override // okhttp3.Callback
            public void onResponse(Call call, Response response) {
                if (!response.isSuccessful()) {
                    callRequestController.cancel();
                    if (response.body() != null) {
                        response.body().close();
                    }
                } else if (response.body() == null) {
                    callRequestController.cancel();
                    if (response.body() != null) {
                        response.body().close();
                    }
                } else {
                    callRequestController.setStream(new SkippingInputStream(ContentLengthInputStream.obtain(response.body().byteStream(), response.body().contentLength()), byteRange.ignoreFirst));
                }
            }
        });
        return callRequestController;
    }

    public Optional<Long> parseLengthFromContentRange(String str) {
        int i;
        int indexOf = str.indexOf(47);
        if (indexOf < 0 || str.length() <= (i = indexOf + 1)) {
            return Optional.empty();
        }
        try {
            return Optional.of(Long.valueOf(Long.parseLong(str.substring(i))));
        } catch (NumberFormatException unused) {
            return Optional.empty();
        }
    }

    private List<ByteRange> getRequestPattern(long j) throws IOException {
        if (j > MB) {
            return getRequestPattern(j, MB);
        }
        if (j > 512000) {
            return getRequestPattern(j, 512000);
        }
        if (j > 102400) {
            return getRequestPattern(j, 102400);
        }
        if (j > 51200) {
            return getRequestPattern(j, 51200);
        }
        if (j > 10240) {
            return getRequestPattern(j, 10240);
        }
        if (j > KB) {
            return getRequestPattern(j, KB);
        }
        throw new IOException("Unsupported size: " + j);
    }

    private List<ByteRange> getRequestPattern(long j, long j2) {
        long j3;
        LinkedList linkedList = new LinkedList();
        long j4 = 0;
        while (true) {
            j3 = j - j4;
            if (j3 <= j2) {
                break;
            }
            long j5 = j4 + j2;
            linkedList.add(new ByteRange(j4, j5 - 1, 0));
            j4 = j5;
        }
        if (j3 > 0) {
            linkedList.add(new ByteRange(j - j2, j - 1, j2 - j3));
        }
        return linkedList;
    }

    /* loaded from: classes4.dex */
    public static class ByteRange {
        private final long end;
        private final long ignoreFirst;
        private final long start;

        private ByteRange(long j, long j2, long j3) {
            this.start = j;
            this.end = j2;
            this.ignoreFirst = j3;
        }
    }

    /* loaded from: classes4.dex */
    private static class SkippingInputStream extends FilterInputStream {
        private long skip;

        SkippingInputStream(InputStream inputStream, long j) {
            super(inputStream);
            this.skip = j;
        }

        @Override // java.io.FilterInputStream, java.io.InputStream
        public int read() throws IOException {
            long j = this.skip;
            if (j != 0) {
                skipFully(j);
                this.skip = 0;
            }
            return super.read();
        }

        @Override // java.io.FilterInputStream, java.io.InputStream
        public int read(byte[] bArr) throws IOException {
            long j = this.skip;
            if (j != 0) {
                skipFully(j);
                this.skip = 0;
            }
            return super.read(bArr);
        }

        @Override // java.io.FilterInputStream, java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            long j = this.skip;
            if (j != 0) {
                skipFully(j);
                this.skip = 0;
            }
            return super.read(bArr, i, i2);
        }

        @Override // java.io.FilterInputStream, java.io.InputStream
        public int available() throws IOException {
            return Util.toIntExact(((long) super.available()) - this.skip);
        }

        private void skipFully(long j) throws IOException {
            int read;
            byte[] bArr = new byte[RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT];
            while (j > 0 && (read = super.read(bArr, 0, Math.min((int) RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT, Util.toIntExact(j)))) != -1) {
                j -= (long) read;
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class InputStreamList extends InputStream {
        private int currentStreamIndex = 0;
        private final List<InputStream> inputStreams;

        InputStreamList(List<InputStream> list) {
            this.inputStreams = list;
        }

        @Override // java.io.InputStream
        public int read() throws IOException {
            while (this.currentStreamIndex < this.inputStreams.size()) {
                int read = this.inputStreams.get(this.currentStreamIndex).read();
                if (read != -1) {
                    return read;
                }
                this.currentStreamIndex++;
            }
            return -1;
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            while (this.currentStreamIndex < this.inputStreams.size()) {
                int read = this.inputStreams.get(this.currentStreamIndex).read(bArr, i, i2);
                if (read != -1) {
                    return read;
                }
                this.currentStreamIndex++;
            }
            return -1;
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr) throws IOException {
            return read(bArr, 0, bArr.length);
        }

        @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            for (InputStream inputStream : this.inputStreams) {
                try {
                    inputStream.close();
                } catch (IOException unused) {
                }
            }
        }

        @Override // java.io.InputStream
        public int available() {
            int i = 0;
            for (int i2 = this.currentStreamIndex; i2 < this.inputStreams.size(); i2++) {
                try {
                    int available = this.inputStreams.get(i2).available();
                    if (available != -1) {
                        i += available;
                    }
                } catch (IOException unused) {
                }
            }
            return i;
        }
    }
}
