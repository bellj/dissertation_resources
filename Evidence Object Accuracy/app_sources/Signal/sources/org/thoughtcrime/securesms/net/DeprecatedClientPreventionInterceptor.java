package org.thoughtcrime.securesms.net;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public final class DeprecatedClientPreventionInterceptor implements Interceptor {
    private static final String TAG = Log.tag(DeprecatedClientPreventionInterceptor.class);

    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        if (!SignalStore.misc().isClientDeprecated()) {
            return chain.proceed(chain.request());
        }
        Log.w(TAG, "Preventing request because client is deprecated.");
        return new Response.Builder().request(chain.request()).protocol(Protocol.HTTP_1_1).receivedResponseAtMillis(System.currentTimeMillis()).message("").body(ResponseBody.create((MediaType) null, "")).code(499).build();
    }
}
