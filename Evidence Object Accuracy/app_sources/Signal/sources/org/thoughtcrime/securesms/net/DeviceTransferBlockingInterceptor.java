package org.thoughtcrime.securesms.net;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public final class DeviceTransferBlockingInterceptor implements Interceptor {
    private static final DeviceTransferBlockingInterceptor INSTANCE = new DeviceTransferBlockingInterceptor();
    private static final String TAG = Log.tag(DeviceTransferBlockingInterceptor.class);
    private volatile boolean blockNetworking = SignalStore.misc().isOldDeviceTransferLocked();

    public static DeviceTransferBlockingInterceptor getInstance() {
        return INSTANCE;
    }

    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        if (!this.blockNetworking) {
            return chain.proceed(chain.request());
        }
        Log.w(TAG, "Preventing request because in transfer mode.");
        return new Response.Builder().request(chain.request()).protocol(Protocol.HTTP_1_1).receivedResponseAtMillis(System.currentTimeMillis()).message("").body(ResponseBody.create((MediaType) null, "")).code(500).build();
    }

    public void blockNetwork() {
        this.blockNetworking = true;
        ApplicationDependencies.closeConnections();
    }

    public void unblockNetwork() {
        this.blockNetworking = false;
        ApplicationDependencies.getIncomingMessageObserver();
    }
}
