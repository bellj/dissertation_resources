package org.thoughtcrime.securesms.net;

import android.os.Build;

/* loaded from: classes4.dex */
public class StandardUserAgentInterceptor extends UserAgentInterceptor {
    public static final String USER_AGENT = ("Signal-Android/5.44.4 Android/" + Build.VERSION.SDK_INT);

    public StandardUserAgentInterceptor() {
        super(USER_AGENT);
    }
}
