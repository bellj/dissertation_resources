package org.thoughtcrime.securesms.net;

import java.io.IOException;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Response;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class ContentProxySafetyInterceptor implements Interceptor {
    private static final String TAG = Log.tag(ContentProxySafetyInterceptor.class);

    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        if (isWhitelisted(chain.request().url())) {
            Response proceed = chain.proceed(chain.request());
            if (!proceed.isRedirect()) {
                return proceed;
            }
            if (isWhitelisted(proceed.header("Location"))) {
                return proceed;
            }
            Log.w(TAG, "Tried to redirect to a non-whitelisted domain!");
            chain.call().cancel();
            throw new IOException("Tried to redirect to a non-whitelisted domain!");
        }
        Log.w(TAG, "Request was for a non-whitelisted domain!");
        chain.call().cancel();
        throw new IOException("Request was for a non-whitelisted domain!");
    }

    private static boolean isWhitelisted(HttpUrl httpUrl) {
        return isWhitelisted(httpUrl.toString());
    }

    private static boolean isWhitelisted(String str) {
        HttpUrl parse;
        if (str != null && (parse = HttpUrl.parse(str)) != null && "https".equals(parse.scheme()) && ContentProxySelector.WHITELISTED_DOMAINS.contains(parse.topPrivateDomain())) {
            return true;
        }
        return false;
    }
}
