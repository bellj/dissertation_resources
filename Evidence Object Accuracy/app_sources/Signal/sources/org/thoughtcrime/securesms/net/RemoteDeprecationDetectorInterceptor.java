package org.thoughtcrime.securesms.net;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Response;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public final class RemoteDeprecationDetectorInterceptor implements Interceptor {
    private static final String TAG = Log.tag(RemoteDeprecationDetectorInterceptor.class);

    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response proceed = chain.proceed(chain.request());
        if (proceed.code() == 499 && !SignalStore.misc().isClientDeprecated()) {
            Log.w(TAG, "Received 499. Client version is deprecated.");
            SignalStore.misc().markClientDeprecated();
        }
        return proceed;
    }
}
