package org.thoughtcrime.securesms.mediasend;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Pair;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiFunction;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.MediaFolder;
import org.thoughtcrime.securesms.mediasend.MediaRepository;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.StorageUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class MediaRepository {
    private static final String CAMERA;
    private static final String TAG = Log.tag(MediaRepository.class);

    /* loaded from: classes4.dex */
    public interface Callback<E> {
        void onComplete(E e);
    }

    private String getHeightColumn(int i) {
        return (i == 0 || i == 180) ? "height" : "width";
    }

    private String getWidthColumn(int i) {
        return (i == 0 || i == 180) ? "width" : "height";
    }

    public void getFolders(Context context, Callback<List<MediaFolder>> callback) {
        if (!StorageUtil.canReadFromMediaStore()) {
            Log.w(TAG, "No storage permissions!", new Throwable());
            callback.onComplete(Collections.emptyList());
            return;
        }
        SignalExecutors.BOUNDED.execute(new Runnable(callback, context) { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda12
            public final /* synthetic */ MediaRepository.Callback f$1;
            public final /* synthetic */ Context f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaRepository.this.lambda$getFolders$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$getFolders$0(Callback callback, Context context) {
        callback.onComplete(getFolders(context));
    }

    public void getMediaInBucket(Context context, String str, Callback<List<Media>> callback) {
        if (!StorageUtil.canReadFromMediaStore()) {
            Log.w(TAG, "No storage permissions!", new Throwable());
            callback.onComplete(Collections.emptyList());
            return;
        }
        SignalExecutors.BOUNDED.execute(new Runnable(callback, context, str) { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda9
            public final /* synthetic */ MediaRepository.Callback f$1;
            public final /* synthetic */ Context f$2;
            public final /* synthetic */ String f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaRepository.this.lambda$getMediaInBucket$1(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$getMediaInBucket$1(Callback callback, Context context, String str) {
        callback.onComplete(getMediaInBucket(context, str));
    }

    public void getPopulatedMedia(Context context, List<Media> list, Callback<List<Media>> callback) {
        if (Stream.of(list).allMatch(new Predicate() { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return MediaRepository.this.isPopulated((Media) obj);
            }
        })) {
            callback.onComplete(list);
        } else if (!StorageUtil.canReadFromMediaStore()) {
            Log.w(TAG, "No storage permissions!", new Throwable());
            callback.onComplete(list);
        } else {
            SignalExecutors.BOUNDED.execute(new Runnable(callback, context, list) { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda3
                public final /* synthetic */ MediaRepository.Callback f$1;
                public final /* synthetic */ Context f$2;
                public final /* synthetic */ List f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    MediaRepository.this.lambda$getPopulatedMedia$2(this.f$1, this.f$2, this.f$3);
                }
            });
        }
    }

    public /* synthetic */ void lambda$getPopulatedMedia$2(Callback callback, Context context, List list) {
        callback.onComplete(getPopulatedMedia(context, list));
    }

    void getMostRecentItem(Context context, Callback<Optional<Media>> callback) {
        if (!StorageUtil.canReadFromMediaStore()) {
            Log.w(TAG, "No storage permissions!", new Throwable());
            callback.onComplete(Optional.empty());
            return;
        }
        SignalExecutors.BOUNDED.execute(new Runnable(callback, context) { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda10
            public final /* synthetic */ MediaRepository.Callback f$1;
            public final /* synthetic */ Context f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaRepository.this.lambda$getMostRecentItem$3(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$getMostRecentItem$3(Callback callback, Context context) {
        callback.onComplete(getMostRecentItem(context));
    }

    public static /* synthetic */ void lambda$transformMedia$4(Callback callback, Context context, List list, Map map) {
        callback.onComplete(transformMediaSync(context, list, map));
    }

    public static void transformMedia(Context context, List<Media> list, Map<Media, MediaTransform> map, Callback<LinkedHashMap<Media, Media>> callback) {
        SignalExecutors.BOUNDED.execute(new Runnable(context, list, map) { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ List f$2;
            public final /* synthetic */ Map f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaRepository.lambda$transformMedia$4(MediaRepository.Callback.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    private List<MediaFolder> getFolders(Context context) {
        FolderResult folders = getFolders(context, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        FolderResult folders2 = getFolders(context, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        HashMap hashMap = new HashMap(folders.getFolderData());
        for (Map.Entry<String, FolderData> entry : folders2.getFolderData().entrySet()) {
            if (hashMap.containsKey(entry.getKey())) {
                ((FolderData) hashMap.get(entry.getKey())).incrementCount(entry.getValue().getCount());
            } else {
                hashMap.put(entry.getKey(), entry.getValue());
            }
        }
        String cameraBucketId = folders.getCameraBucketId() != null ? folders.getCameraBucketId() : folders2.getCameraBucketId();
        FolderData folderData = cameraBucketId != null ? (FolderData) hashMap.remove(cameraBucketId) : null;
        List<MediaFolder> list = Stream.of(hashMap.values()).map(new Function() { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaRepository.lambda$getFolders$5((MediaRepository.FolderData) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return MediaRepository.lambda$getFolders$6((MediaFolder) obj);
            }
        }).sorted(new Comparator() { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda6
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return MediaRepository.lambda$getFolders$7((MediaFolder) obj, (MediaFolder) obj2);
            }
        }).toList();
        Uri thumbnail = folders.getThumbnailTimestamp() > folders2.getThumbnailTimestamp() ? folders.getThumbnail() : folders2.getThumbnail();
        if (thumbnail != null) {
            int intValue = ((Integer) Stream.of(list).reduce(0, new BiFunction() { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda7
                @Override // com.annimon.stream.function.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return MediaRepository.lambda$getFolders$8((Integer) obj, (MediaFolder) obj2);
                }
            })).intValue();
            if (folderData != null) {
                intValue += folderData.getCount();
            }
            list.add(0, new MediaFolder(thumbnail, context.getString(R.string.MediaRepository_all_media), intValue, Media.ALL_MEDIA_BUCKET_ID, MediaFolder.FolderType.NORMAL));
        }
        if (folderData != null) {
            list.add(0, new MediaFolder(folderData.getThumbnail(), folderData.getTitle(), folderData.getCount(), folderData.getBucketId(), MediaFolder.FolderType.CAMERA));
        }
        return list;
    }

    public static /* synthetic */ MediaFolder lambda$getFolders$5(FolderData folderData) {
        return new MediaFolder(folderData.getThumbnail(), folderData.getTitle(), folderData.getCount(), folderData.getBucketId(), MediaFolder.FolderType.NORMAL);
    }

    public static /* synthetic */ boolean lambda$getFolders$6(MediaFolder mediaFolder) {
        return mediaFolder.getTitle() != null;
    }

    public static /* synthetic */ int lambda$getFolders$7(MediaFolder mediaFolder, MediaFolder mediaFolder2) {
        return mediaFolder.getTitle().toLowerCase().compareTo(mediaFolder2.getTitle().toLowerCase());
    }

    public static /* synthetic */ Integer lambda$getFolders$8(Integer num, MediaFolder mediaFolder) {
        return Integer.valueOf(num.intValue() + mediaFolder.getItemCount());
    }

    private FolderResult getFolders(Context context, Uri uri) {
        HashMap hashMap = new HashMap();
        String[] strArr = {"_id", "bucket_id", "bucket_display_name", "date_modified"};
        char c = 1;
        char c2 = 0;
        Cursor query = context.getContentResolver().query(uri, strArr, isNotPending() + " AND mime_type NOT LIKE ?", SqlUtil.buildArgs("%image/svg%"), "bucket_display_name COLLATE NOCASE ASC, date_modified DESC");
        long j = 0;
        String str = null;
        Uri uri2 = null;
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                Uri withAppendedId = ContentUris.withAppendedId(uri, query.getLong(query.getColumnIndexOrThrow(strArr[c2])));
                String string = query.getString(query.getColumnIndexOrThrow(strArr[c]));
                String string2 = query.getString(query.getColumnIndexOrThrow(strArr[2]));
                long j2 = query.getLong(query.getColumnIndexOrThrow(strArr[3]));
                FolderData folderData = (FolderData) Util.getOrDefault(hashMap, string, new FolderData(withAppendedId, localizeTitle(context, string2), string));
                folderData.incrementCount();
                hashMap.put(string, folderData);
                if (str == null && CAMERA.equals(string2)) {
                    str = string;
                }
                if (j2 > j) {
                    uri2 = withAppendedId;
                    j = j2;
                }
                strArr = strArr;
                c = 1;
                c2 = 0;
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return new FolderResult(str, uri2, j, hashMap);
    }

    private String localizeTitle(Context context, String str) {
        return CAMERA.equals(str) ? context.getString(R.string.MediaRepository__camera) : str;
    }

    private List<Media> getMediaInBucket(Context context, String str) {
        Stopwatch stopwatch = new Stopwatch("getMediaInBucket");
        List<Media> mediaInBucket = getMediaInBucket(context, str, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true);
        List<Media> mediaInBucket2 = getMediaInBucket(context, str, MediaStore.Video.Media.EXTERNAL_CONTENT_URI, false);
        ArrayList arrayList = new ArrayList(mediaInBucket.size() + mediaInBucket2.size());
        stopwatch.split("post fetch");
        arrayList.addAll(mediaInBucket);
        arrayList.addAll(mediaInBucket2);
        Collections.sort(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda11
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return MediaRepository.lambda$getMediaInBucket$9((Media) obj, (Media) obj2);
            }
        });
        stopwatch.split("post sort");
        stopwatch.stop(TAG);
        return arrayList;
    }

    public static /* synthetic */ int lambda$getMediaInBucket$9(Media media, Media media2) {
        return Long.compare(media2.getDate(), media.getDate());
    }

    private List<Media> getMediaInBucket(Context context, String str, Uri uri, boolean z) {
        String[] strArr;
        LinkedList linkedList = new LinkedList();
        String str2 = "bucket_id = ? AND " + isNotPending() + " AND mime_type NOT LIKE ?";
        String[] strArr2 = {str, "%image/svg%"};
        if (z) {
            strArr = new String[]{"_id", "mime_type", "date_modified", "orientation", "width", "height", "_size"};
        } else {
            strArr = new String[]{"_id", "mime_type", "date_modified", "width", "height", "_size", "duration"};
        }
        if (Media.ALL_MEDIA_BUCKET_ID.equals(str)) {
            str2 = isNotPending() + " AND mime_type NOT LIKE ?";
            strArr2 = SqlUtil.buildArgs("%image/svg%");
        }
        Cursor query = context.getContentResolver().query(uri, strArr, str2, strArr2, "date_modified DESC");
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                Uri withAppendedId = ContentUris.withAppendedId(uri, query.getLong(query.getColumnIndexOrThrow(strArr[0])));
                String string = query.getString(query.getColumnIndexOrThrow("mime_type"));
                long j = query.getLong(query.getColumnIndexOrThrow("date_modified"));
                int i = z ? query.getInt(query.getColumnIndexOrThrow("orientation")) : 0;
                linkedList.add(fixMimeType(context, new Media(withAppendedId, string, j, query.getInt(query.getColumnIndexOrThrow(getWidthColumn(i))), query.getInt(query.getColumnIndexOrThrow(getHeightColumn(i))), query.getLong(query.getColumnIndexOrThrow("_size")), !z ? (long) query.getInt(query.getColumnIndexOrThrow("duration")) : 0, false, false, Optional.of(str), Optional.empty(), Optional.empty())));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return linkedList;
    }

    private String isNotPending() {
        return Build.VERSION.SDK_INT <= 28 ? "_data NOT NULL" : "is_pending != 1";
    }

    public List<Media> getPopulatedMedia(Context context, List<Media> list) {
        return (List) Collection$EL.stream(list).map(new j$.util.function.Function(context) { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MediaRepository.this.lambda$getPopulatedMedia$10(this.f$1, (Media) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new j$.util.function.Function(context) { // from class: org.thoughtcrime.securesms.mediasend.MediaRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MediaRepository.fixMimeType(this.f$0, (Media) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
    }

    public /* synthetic */ Media lambda$getPopulatedMedia$10(Context context, Media media) {
        try {
            if (isPopulated(media)) {
                return media;
            }
            if (PartAuthority.isLocalUri(media.getUri())) {
                return getLocallyPopulatedMedia(context, media);
            }
            return getContentResolverPopulatedMedia(context, media);
        } catch (IOException unused) {
            return media;
        }
    }

    public static LinkedHashMap<Media, Media> transformMediaSync(Context context, List<Media> list, Map<Media, MediaTransform> map) {
        LinkedHashMap<Media, Media> linkedHashMap = new LinkedHashMap<>(list.size());
        for (Media media : list) {
            MediaTransform mediaTransform = map.get(media);
            if (mediaTransform != null) {
                linkedHashMap.put(media, mediaTransform.transform(context, media));
            } else {
                linkedHashMap.put(media, media);
            }
        }
        return linkedHashMap;
    }

    private Optional<Media> getMostRecentItem(Context context) {
        List<Media> mediaInBucket = getMediaInBucket(context, Media.ALL_MEDIA_BUCKET_ID, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true);
        return mediaInBucket.size() > 0 ? Optional.of(mediaInBucket.get(0)) : Optional.empty();
    }

    public boolean isPopulated(Media media) {
        return media.getWidth() > 0 && media.getHeight() > 0 && media.getSize() > 0;
    }

    private Media getLocallyPopulatedMedia(Context context, Media media) throws IOException {
        int width = media.getWidth();
        int height = media.getHeight();
        long size = media.getSize();
        if (size <= 0) {
            Optional ofNullable = Optional.ofNullable(PartAuthority.getAttachmentSize(context, media.getUri()));
            size = ofNullable.isPresent() ? ((Long) ofNullable.get()).longValue() : 0;
        }
        if (size <= 0) {
            size = MediaUtil.getMediaSize(context, media.getUri());
        }
        if (width == 0 || height == 0) {
            Pair<Integer, Integer> dimensions = MediaUtil.getDimensions(context, media.getMimeType(), media.getUri());
            width = ((Integer) dimensions.first).intValue();
            height = ((Integer) dimensions.second).intValue();
        }
        return new Media(media.getUri(), media.getMimeType(), media.getDate(), width, height, size, 0, media.isBorderless(), media.isVideoGif(), media.getBucketId(), media.getCaption(), Optional.empty());
    }

    private Media getContentResolverPopulatedMedia(Context context, Media media) throws IOException {
        int width = media.getWidth();
        int height = media.getHeight();
        long size = media.getSize();
        if (size <= 0) {
            Cursor query = context.getContentResolver().query(media.getUri(), null, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst() && query.getColumnIndex("_size") >= 0) {
                        size = query.getLong(query.getColumnIndexOrThrow("_size"));
                    }
                } catch (Throwable th) {
                    try {
                        query.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
        }
        if (size <= 0) {
            size = MediaUtil.getMediaSize(context, media.getUri());
        }
        if (width == 0 || height == 0) {
            Pair<Integer, Integer> dimensions = MediaUtil.getDimensions(context, media.getMimeType(), media.getUri());
            width = ((Integer) dimensions.first).intValue();
            height = ((Integer) dimensions.second).intValue();
        }
        return new Media(media.getUri(), media.getMimeType(), media.getDate(), width, height, size, 0, media.isBorderless(), media.isVideoGif(), media.getBucketId(), media.getCaption(), Optional.empty());
    }

    public static Media fixMimeType(Context context, Media media) {
        if (MediaUtil.isOctetStream(media.getMimeType())) {
            String str = TAG;
            Log.w(str, "Media has mimetype octet stream");
            String mimeType = MediaUtil.getMimeType(context, media.getUri());
            if (mimeType != null && !mimeType.equals(media.getMimeType())) {
                Log.d(str, "Changing mime type to '" + mimeType + "'");
                return Media.withMimeType(media, mimeType);
            } else if (media.getSize() <= 0 || media.getWidth() <= 0 || media.getHeight() <= 0) {
                Log.d(str, "Unable to fix mimetype");
            } else {
                boolean z = media.getDuration() > 0;
                StringBuilder sb = new StringBuilder();
                sb.append("Assuming content is ");
                sb.append(z ? "a video" : "an image");
                sb.append(", setting mimetype");
                Log.d(str, sb.toString());
                return Media.withMimeType(media, z ? MediaUtil.VIDEO_UNSPECIFIED : MediaUtil.IMAGE_JPEG);
            }
        }
        return media;
    }

    /* loaded from: classes4.dex */
    public static class FolderResult {
        private final String cameraBucketId;
        private final Map<String, FolderData> folderData;
        private final Uri thumbnail;
        private final long thumbnailTimestamp;

        private FolderResult(String str, Uri uri, long j, Map<String, FolderData> map) {
            this.cameraBucketId = str;
            this.thumbnail = uri;
            this.thumbnailTimestamp = j;
            this.folderData = map;
        }

        String getCameraBucketId() {
            return this.cameraBucketId;
        }

        Uri getThumbnail() {
            return this.thumbnail;
        }

        long getThumbnailTimestamp() {
            return this.thumbnailTimestamp;
        }

        Map<String, FolderData> getFolderData() {
            return this.folderData;
        }
    }

    /* loaded from: classes4.dex */
    public static class FolderData {
        private final String bucketId;
        private int count;
        private final Uri thumbnail;
        private final String title;

        private FolderData(Uri uri, String str, String str2) {
            this.thumbnail = uri;
            this.title = str;
            this.bucketId = str2;
        }

        Uri getThumbnail() {
            return this.thumbnail;
        }

        String getTitle() {
            return this.title;
        }

        String getBucketId() {
            return this.bucketId;
        }

        int getCount() {
            return this.count;
        }

        void incrementCount() {
            incrementCount(1);
        }

        void incrementCount(int i) {
            this.count += i;
        }
    }
}
