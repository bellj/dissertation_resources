package org.thoughtcrime.securesms.mediasend.v2.review;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Annotation;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.EditTextUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ComposeText;
import org.thoughtcrime.securesms.components.InputAwareLayout;
import org.thoughtcrime.securesms.components.KeyboardEntryDialogFragment;
import org.thoughtcrime.securesms.components.emoji.EmojiToggle;
import org.thoughtcrime.securesms.components.emoji.MediaKeyboard;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.components.mention.MentionValidatorWatcher;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerFragment;
import org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerViewModel;
import org.thoughtcrime.securesms.keyboard.KeyboardPage;
import org.thoughtcrime.securesms.keyboard.KeyboardPagerViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.v2.HudCommand;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.Stub;

/* compiled from: AddMessageDialogFragment.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 >2\u00020\u0001:\u0001>B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\"\u001a\u00020#H\u0002J\b\u0010$\u001a\u00020#H\u0002J\b\u0010%\u001a\u00020#H\u0002J$\u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00020)2\b\u0010*\u001a\u0004\u0018\u00010\u00152\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\b\u0010-\u001a\u00020#H\u0016J\u0010\u0010.\u001a\u00020#2\u0006\u0010/\u001a\u000200H\u0016J\u0012\u00101\u001a\u00020#2\b\u00102\u001a\u0004\u0018\u000103H\u0002J\b\u00104\u001a\u00020#H\u0002J\u0012\u00105\u001a\u00020#2\b\u00106\u001a\u0004\u0018\u000107H\u0002J\b\u00108\u001a\u00020#H\u0016J\b\u00109\u001a\u00020#H\u0016J\b\u0010:\u001a\u00020#H\u0016J\u001a\u0010;\u001a\u00020#2\u0006\u0010<\u001a\u00020'2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\b\u0010=\u001a\u00020#H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX.¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX.¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX.¢\u0006\u0002\n\u0000R\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0002¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0010\u0010\u0011R\u000e\u0010\u0014\u001a\u00020\u0015X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0016\u001a\u00020\u00178BX\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u0013\u001a\u0004\b\u0018\u0010\u0019R\u000e\u0010\u001b\u001a\u00020\u001cX\u000e¢\u0006\u0002\n\u0000R\u001b\u0010\u001d\u001a\u00020\u001e8BX\u0002¢\u0006\f\n\u0004\b!\u0010\u0013\u001a\u0004\b\u001f\u0010 ¨\u0006?"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/AddMessageDialogFragment;", "Lorg/thoughtcrime/securesms/components/KeyboardEntryDialogFragment;", "()V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "emojiDrawerStub", "Lorg/thoughtcrime/securesms/util/views/Stub;", "Lorg/thoughtcrime/securesms/components/emoji/MediaKeyboard;", "emojiDrawerToggle", "Lorg/thoughtcrime/securesms/components/emoji/EmojiToggle;", "hud", "Lorg/thoughtcrime/securesms/components/InputAwareLayout;", "input", "Lorg/thoughtcrime/securesms/components/ComposeText;", "keyboardPagerViewModel", "Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerViewModel;", "getKeyboardPagerViewModel", "()Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerViewModel;", "keyboardPagerViewModel$delegate", "Lkotlin/Lazy;", "mentionsContainer", "Landroid/view/ViewGroup;", "mentionsViewModel", "Lorg/thoughtcrime/securesms/conversation/ui/mentions/MentionsPickerViewModel;", "getMentionsViewModel", "()Lorg/thoughtcrime/securesms/conversation/ui/mentions/MentionsPickerViewModel;", "mentionsViewModel$delegate", "requestedEmojiDrawer", "", "viewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "viewModel$delegate", "closeEmojiSearch", "", "ensureMentionsContainerFilled", "initializeMentions", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "onDismiss", "dialog", "Landroid/content/DialogInterface;", "onEmojiSelected", "emoji", "", "onEmojiToggleClicked", "onKeyEvent", "keyEvent", "Landroid/view/KeyEvent;", "onKeyboardHidden", "onPause", "onResume", "onViewCreated", "view", "openEmojiSearch", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AddMessageDialogFragment extends KeyboardEntryDialogFragment {
    private static final String ARG_INITIAL_TEXT;
    public static final Companion Companion = new Companion(null);
    public static final String TAG;
    private final CompositeDisposable disposables;
    private Stub<MediaKeyboard> emojiDrawerStub;
    private EmojiToggle emojiDrawerToggle;
    private InputAwareLayout hud;
    private ComposeText input;
    private final Lazy keyboardPagerViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(KeyboardPagerViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$keyboardPagerViewModel$2
        final /* synthetic */ AddMessageDialogFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private ViewGroup mentionsContainer;
    private final Lazy mentionsViewModel$delegate;
    private boolean requestedEmojiDrawer;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MediaSelectionViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$viewModel$2
        final /* synthetic */ AddMessageDialogFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    public AddMessageDialogFragment() {
        super(R.layout.v2_media_add_message_dialog_fragment);
        AddMessageDialogFragment$mentionsViewModel$2 addMessageDialogFragment$mentionsViewModel$2 = new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$mentionsViewModel$2
            final /* synthetic */ AddMessageDialogFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStoreOwner invoke() {
                FragmentActivity requireActivity = this.this$0.requireActivity();
                Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
                return requireActivity;
            }
        };
        this.mentionsViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MentionsPickerViewModel.class), new Function0<ViewModelStore>(addMessageDialogFragment$mentionsViewModel$2) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$special$$inlined$viewModels$1
            final /* synthetic */ Function0 $ownerProducer;

            {
                this.$ownerProducer = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStore invoke() {
                ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
                Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
                return viewModelStore;
            }
        }, AddMessageDialogFragment$mentionsViewModel$3.INSTANCE);
        this.disposables = new CompositeDisposable();
    }

    private final MediaSelectionViewModel getViewModel() {
        return (MediaSelectionViewModel) this.viewModel$delegate.getValue();
    }

    private final KeyboardPagerViewModel getKeyboardPagerViewModel() {
        return (KeyboardPagerViewModel) this.keyboardPagerViewModel$delegate.getValue();
    }

    private final MentionsPickerViewModel getMentionsViewModel() {
        return (MentionsPickerViewModel) this.mentionsViewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.KeyboardEntryDialogFragment, androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        LayoutInflater from = LayoutInflater.from(new ContextThemeWrapper(layoutInflater.getContext(), (int) R.style.TextSecure_DarkTheme));
        Intrinsics.checkNotNullExpressionValue(from, "themedInflater");
        return super.onCreateView(from, viewGroup, bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.add_a_message_input);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.add_a_message_input)");
        ComposeText composeText = (ComposeText) findViewById;
        this.input = composeText;
        InputAwareLayout inputAwareLayout = null;
        if (composeText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText = null;
        }
        EditTextUtil.addGraphemeClusterLimitFilter(composeText, Stories.MAX_BODY_SIZE);
        ComposeText composeText2 = this.input;
        if (composeText2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText2 = null;
        }
        composeText2.setText(requireArguments().getCharSequence(ARG_INITIAL_TEXT));
        View findViewById2 = view.findViewById(R.id.emoji_toggle);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.emoji_toggle)");
        this.emojiDrawerToggle = (EmojiToggle) findViewById2;
        this.emojiDrawerStub = new Stub<>((ViewStub) view.findViewById(R.id.emoji_drawer_stub));
        if (SignalStore.settings().isPreferSystemEmoji()) {
            EmojiToggle emojiToggle = this.emojiDrawerToggle;
            if (emojiToggle == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerToggle");
                emojiToggle = null;
            }
            ViewExtensionsKt.setVisible(emojiToggle, false);
        } else {
            EmojiToggle emojiToggle2 = this.emojiDrawerToggle;
            if (emojiToggle2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerToggle");
                emojiToggle2 = null;
            }
            emojiToggle2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$$ExternalSyntheticLambda5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    AddMessageDialogFragment.$r8$lambda$eENkmHdHgb3wNMSYc5hYkedyV7o(AddMessageDialogFragment.this, view2);
                }
            });
        }
        View findViewById3 = view.findViewById(R.id.hud);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.hud)");
        InputAwareLayout inputAwareLayout2 = (InputAwareLayout) findViewById3;
        this.hud = inputAwareLayout2;
        if (inputAwareLayout2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hud");
        } else {
            inputAwareLayout = inputAwareLayout2;
        }
        inputAwareLayout.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddMessageDialogFragment.m2188$r8$lambda$QxWmXfW_j1hTPb0lQ64GE9h_vI(AddMessageDialogFragment.this, view2);
            }
        });
        View findViewById4 = view.findViewById(R.id.confirm_button);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.confirm_button)");
        findViewById4.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddMessageDialogFragment.m2187$r8$lambda$Ms21UE5aZUFcPFppXBOQ_OzU9g(AddMessageDialogFragment.this, view2);
            }
        });
        this.disposables.add(getViewModel().getHudCommands().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$$ExternalSyntheticLambda8
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                AddMessageDialogFragment.m2186$r8$lambda$FH3OhztXrlkiUWtZG0GHHTd8O4(AddMessageDialogFragment.this, (HudCommand) obj);
            }
        }));
        initializeMentions();
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2196onViewCreated$lambda0(AddMessageDialogFragment addMessageDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(addMessageDialogFragment, "this$0");
        addMessageDialogFragment.onEmojiToggleClicked();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m2197onViewCreated$lambda1(AddMessageDialogFragment addMessageDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(addMessageDialogFragment, "this$0");
        addMessageDialogFragment.dismissAllowingStateLoss();
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m2198onViewCreated$lambda2(AddMessageDialogFragment addMessageDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(addMessageDialogFragment, "this$0");
        addMessageDialogFragment.dismissAllowingStateLoss();
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2199onViewCreated$lambda3(AddMessageDialogFragment addMessageDialogFragment, HudCommand hudCommand) {
        Intrinsics.checkNotNullParameter(addMessageDialogFragment, "this$0");
        if (Intrinsics.areEqual(hudCommand, HudCommand.OpenEmojiSearch.INSTANCE)) {
            addMessageDialogFragment.openEmojiSearch();
        } else if (Intrinsics.areEqual(hudCommand, HudCommand.CloseEmojiSearch.INSTANCE)) {
            addMessageDialogFragment.closeEmojiSearch();
        } else if (hudCommand instanceof HudCommand.EmojiKeyEvent) {
            addMessageDialogFragment.onKeyEvent(((HudCommand.EmojiKeyEvent) hudCommand).getKeyEvent());
        } else if (hudCommand instanceof HudCommand.EmojiInsert) {
            addMessageDialogFragment.onEmojiSelected(((HudCommand.EmojiInsert) hudCommand).getEmoji());
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.requestedEmojiDrawer = false;
        ComposeText composeText = this.input;
        if (composeText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText = null;
        }
        ViewUtil.focusAndShowKeyboard(composeText);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Context requireContext = requireContext();
        ComposeText composeText = this.input;
        if (composeText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText = null;
        }
        ViewUtil.hideKeyboard(requireContext, composeText);
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        MediaSelectionViewModel viewModel = getViewModel();
        ComposeText composeText = this.input;
        if (composeText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText = null;
        }
        viewModel.setMessage(composeText.getText());
    }

    @Override // org.thoughtcrime.securesms.components.KeyboardEntryDialogFragment, org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardHiddenListener
    public void onKeyboardHidden() {
        if (!this.requestedEmojiDrawer) {
            super.onKeyboardHidden();
        }
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        this.disposables.dispose();
        ComposeText composeText = this.input;
        if (composeText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText = null;
        }
        composeText.setMentionQueryChangedListener(null);
        ComposeText composeText2 = this.input;
        if (composeText2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText2 = null;
        }
        composeText2.setMentionValidator(null);
    }

    private final void initializeMentions() {
        RecipientId recipientId;
        ContactSearchKey.RecipientSearchKey recipientSearchKey = getViewModel().getDestination().getRecipientSearchKey();
        if (recipientSearchKey != null && (recipientId = recipientSearchKey.getRecipientId()) != null) {
            View findViewById = requireView().findViewById(R.id.mentions_picker_container);
            Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewBy…entions_picker_container)");
            this.mentionsContainer = (ViewGroup) findViewById;
            Recipient.live(recipientId).observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$$ExternalSyntheticLambda3
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    AddMessageDialogFragment.m2189$r8$lambda$ZaecGvZ3rt1uR3HrspVwbU5NrA(AddMessageDialogFragment.this, (Recipient) obj);
                }
            });
            getMentionsViewModel().getSelectedRecipient().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$$ExternalSyntheticLambda4
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    AddMessageDialogFragment.$r8$lambda$XnA6SF0pzbYDvVQDQ9SoUgPbLuo(AddMessageDialogFragment.this, (Recipient) obj);
                }
            });
        }
    }

    /* renamed from: initializeMentions$lambda-8 */
    public static final void m2190initializeMentions$lambda8(AddMessageDialogFragment addMessageDialogFragment, Recipient recipient) {
        Intrinsics.checkNotNullParameter(addMessageDialogFragment, "this$0");
        addMessageDialogFragment.getMentionsViewModel().onRecipientChange(recipient);
        ComposeText composeText = addMessageDialogFragment.input;
        ComposeText composeText2 = null;
        if (composeText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText = null;
        }
        composeText.setMentionQueryChangedListener(new ComposeText.MentionQueryChangedListener(addMessageDialogFragment) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ AddMessageDialogFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.components.ComposeText.MentionQueryChangedListener
            public final void onQueryChanged(String str) {
                AddMessageDialogFragment.$r8$lambda$jBOTyDH_zEO3XND04fuDOAofn3I(Recipient.this, this.f$1, str);
            }
        });
        ComposeText composeText3 = addMessageDialogFragment.input;
        if (composeText3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
        } else {
            composeText2 = composeText3;
        }
        composeText2.setMentionValidator(new MentionValidatorWatcher.MentionValidator() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.components.mention.MentionValidatorWatcher.MentionValidator
            public final List getInvalidMentionAnnotations(List list) {
                return AddMessageDialogFragment.$r8$lambda$6WVFlIzPwMugQ0Pp2BTAO6nxKUc(Recipient.this, list);
            }
        });
    }

    /* renamed from: initializeMentions$lambda-8$lambda-4 */
    public static final void m2191initializeMentions$lambda8$lambda4(Recipient recipient, AddMessageDialogFragment addMessageDialogFragment, String str) {
        Intrinsics.checkNotNullParameter(addMessageDialogFragment, "this$0");
        if (recipient.isPushV2Group()) {
            addMessageDialogFragment.ensureMentionsContainerFilled();
            addMessageDialogFragment.getMentionsViewModel().onQueryChange(str);
        }
    }

    /* renamed from: initializeMentions$lambda-8$lambda-7 */
    public static final List m2192initializeMentions$lambda8$lambda7(Recipient recipient, List list) {
        if (!recipient.isPushV2Group()) {
            return list;
        }
        List<RecipientId> participantIds = recipient.getParticipantIds();
        Intrinsics.checkNotNullExpressionValue(participantIds, "recipient.participantIds");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(participantIds, 10));
        for (RecipientId recipientId : participantIds) {
            arrayList.add(MentionAnnotation.idToMentionAnnotationValue(recipientId));
        }
        Set set = CollectionsKt___CollectionsKt.toSet(arrayList);
        Intrinsics.checkNotNullExpressionValue(list, "annotations");
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : list) {
            if (!set.contains(((Annotation) obj).getValue())) {
                arrayList2.add(obj);
            }
        }
        return CollectionsKt___CollectionsKt.toList(arrayList2);
    }

    /* renamed from: initializeMentions$lambda-9 */
    public static final void m2193initializeMentions$lambda9(AddMessageDialogFragment addMessageDialogFragment, Recipient recipient) {
        Intrinsics.checkNotNullParameter(addMessageDialogFragment, "this$0");
        ComposeText composeText = addMessageDialogFragment.input;
        if (composeText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText = null;
        }
        composeText.replaceTextWithMention(recipient.getDisplayName(addMessageDialogFragment.requireContext()), recipient.getId());
    }

    private final void ensureMentionsContainerFilled() {
        if (getChildFragmentManager().findFragmentById(R.id.mentions_picker_container) == null) {
            getChildFragmentManager().beginTransaction().replace(R.id.mentions_picker_container, new MentionsPickerFragment()).commitNowAllowingStateLoss();
        }
    }

    private final void onEmojiToggleClicked() {
        Stub<MediaKeyboard> stub = this.emojiDrawerStub;
        ComposeText composeText = null;
        if (stub == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerStub");
            stub = null;
        }
        if (!stub.resolved()) {
            getKeyboardPagerViewModel().setOnlyPage(KeyboardPage.EMOJI);
            Stub<MediaKeyboard> stub2 = this.emojiDrawerStub;
            if (stub2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerStub");
                stub2 = null;
            }
            stub2.get().setFragmentManager(getChildFragmentManager());
            EmojiToggle emojiToggle = this.emojiDrawerToggle;
            if (emojiToggle == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerToggle");
                emojiToggle = null;
            }
            Stub<MediaKeyboard> stub3 = this.emojiDrawerStub;
            if (stub3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerStub");
                stub3 = null;
            }
            emojiToggle.attach(stub3.get());
        }
        InputAwareLayout inputAwareLayout = this.hud;
        if (inputAwareLayout == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hud");
            inputAwareLayout = null;
        }
        InputAwareLayout.InputView currentInput = inputAwareLayout.getCurrentInput();
        Stub<MediaKeyboard> stub4 = this.emojiDrawerStub;
        if (stub4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerStub");
            stub4 = null;
        }
        if (Intrinsics.areEqual(currentInput, stub4.get())) {
            this.requestedEmojiDrawer = false;
            InputAwareLayout inputAwareLayout2 = this.hud;
            if (inputAwareLayout2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("hud");
                inputAwareLayout2 = null;
            }
            ComposeText composeText2 = this.input;
            if (composeText2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("input");
            } else {
                composeText = composeText2;
            }
            inputAwareLayout2.showSoftkey(composeText);
            return;
        }
        this.requestedEmojiDrawer = true;
        InputAwareLayout inputAwareLayout3 = this.hud;
        if (inputAwareLayout3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hud");
            inputAwareLayout3 = null;
        }
        ComposeText composeText3 = this.input;
        if (composeText3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
        } else {
            composeText = composeText3;
        }
        inputAwareLayout3.hideSoftkey(composeText, new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                AddMessageDialogFragment.$r8$lambda$i8qDNmTlCZiFg2uQpPh0p2pz3JE(AddMessageDialogFragment.this);
            }
        });
    }

    /* renamed from: onEmojiToggleClicked$lambda-11 */
    public static final void m2194onEmojiToggleClicked$lambda11(AddMessageDialogFragment addMessageDialogFragment) {
        Intrinsics.checkNotNullParameter(addMessageDialogFragment, "this$0");
        InputAwareLayout inputAwareLayout = addMessageDialogFragment.hud;
        if (inputAwareLayout == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hud");
            inputAwareLayout = null;
        }
        inputAwareLayout.post(new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment$$ExternalSyntheticLambda9
            @Override // java.lang.Runnable
            public final void run() {
                AddMessageDialogFragment.m2185$r8$lambda$224_xYzTD0hO0ESnZv3q37gna0(AddMessageDialogFragment.this);
            }
        });
    }

    /* renamed from: onEmojiToggleClicked$lambda-11$lambda-10 */
    public static final void m2195onEmojiToggleClicked$lambda11$lambda10(AddMessageDialogFragment addMessageDialogFragment) {
        Intrinsics.checkNotNullParameter(addMessageDialogFragment, "this$0");
        InputAwareLayout inputAwareLayout = addMessageDialogFragment.hud;
        Stub<MediaKeyboard> stub = null;
        if (inputAwareLayout == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hud");
            inputAwareLayout = null;
        }
        ComposeText composeText = addMessageDialogFragment.input;
        if (composeText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText = null;
        }
        Stub<MediaKeyboard> stub2 = addMessageDialogFragment.emojiDrawerStub;
        if (stub2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerStub");
        } else {
            stub = stub2;
        }
        inputAwareLayout.show(composeText, stub.get());
    }

    private final void openEmojiSearch() {
        Stub<MediaKeyboard> stub = this.emojiDrawerStub;
        Stub<MediaKeyboard> stub2 = null;
        if (stub == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerStub");
            stub = null;
        }
        if (stub.resolved()) {
            Stub<MediaKeyboard> stub3 = this.emojiDrawerStub;
            if (stub3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerStub");
            } else {
                stub2 = stub3;
            }
            stub2.get().onOpenEmojiSearch();
        }
    }

    private final void closeEmojiSearch() {
        Stub<MediaKeyboard> stub = this.emojiDrawerStub;
        Stub<MediaKeyboard> stub2 = null;
        if (stub == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerStub");
            stub = null;
        }
        if (stub.resolved()) {
            Stub<MediaKeyboard> stub3 = this.emojiDrawerStub;
            if (stub3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiDrawerStub");
            } else {
                stub2 = stub3;
            }
            stub2.get().onCloseEmojiSearch();
        }
    }

    private final void onEmojiSelected(String str) {
        ComposeText composeText = this.input;
        if (composeText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText = null;
        }
        composeText.insertEmoji(str);
    }

    private final void onKeyEvent(KeyEvent keyEvent) {
        ComposeText composeText = this.input;
        if (composeText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            composeText = null;
        }
        composeText.dispatchKeyEvent(keyEvent);
    }

    /* compiled from: AddMessageDialogFragment.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/AddMessageDialogFragment$Companion;", "", "()V", "ARG_INITIAL_TEXT", "", "TAG", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "initialText", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            AddMessageDialogFragment addMessageDialogFragment = new AddMessageDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putCharSequence(AddMessageDialogFragment.ARG_INITIAL_TEXT, charSequence);
            addMessageDialogFragment.setArguments(bundle);
            addMessageDialogFragment.show(fragmentManager, AddMessageDialogFragment.TAG);
        }
    }
}
