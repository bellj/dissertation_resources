package org.thoughtcrime.securesms.mediasend;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;

/* loaded from: classes4.dex */
public class MediaSendGifFragment extends Fragment implements MediaSendPageFragment {
    private static final String KEY_URI;
    private Uri uri;

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public View getPlaybackControls() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public void notifyHidden() {
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public void restoreState(Object obj) {
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public Object saveState() {
        return null;
    }

    public static MediaSendGifFragment newInstance(Uri uri) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_URI, uri);
        MediaSendGifFragment mediaSendGifFragment = new MediaSendGifFragment();
        mediaSendGifFragment.setArguments(bundle);
        mediaSendGifFragment.setUri(uri);
        return mediaSendGifFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.mediasend_image_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.uri = (Uri) getArguments().getParcelable(KEY_URI);
        GlideApp.with(this).load((Object) new DecryptableStreamUriLoader.DecryptableUri(this.uri)).into((ImageView) view);
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public void setUri(Uri uri) {
        this.uri = uri;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public Uri getUri() {
        return this.uri;
    }
}
