package org.thoughtcrime.securesms.mediasend;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;

/* loaded from: classes4.dex */
public class OrderEnforcer<E> {
    private final Map<E, StageDetails> stages = new LinkedHashMap();

    public OrderEnforcer(E... eArr) {
        for (E e : eArr) {
            this.stages.put(e, new StageDetails());
        }
    }

    public synchronized void run(E e, Runnable runnable) {
        if (isCompletedThrough(e)) {
            runnable.run();
        } else {
            this.stages.get(e).addAction(runnable);
        }
    }

    public synchronized void markCompleted(E e) {
        this.stages.get(e).markCompleted();
        for (E e2 : this.stages.keySet()) {
            StageDetails stageDetails = this.stages.get(e2);
            if (!stageDetails.isCompleted()) {
                break;
            }
            while (stageDetails.hasAction()) {
                stageDetails.popAction().run();
            }
        }
    }

    public synchronized void reset() {
        for (StageDetails stageDetails : this.stages.values()) {
            stageDetails.reset();
        }
    }

    private boolean isCompletedThrough(E e) {
        for (E e2 : this.stages.keySet()) {
            if (!e2.equals(e)) {
                if (!this.stages.get(e2).isCompleted()) {
                    break;
                }
            } else {
                return this.stages.get(e2).isCompleted();
            }
        }
        return false;
    }

    /* loaded from: classes4.dex */
    public static class StageDetails {
        private Stack<Runnable> actions;
        private boolean completed;

        private StageDetails() {
            this.completed = false;
            this.actions = new Stack<>();
        }

        boolean hasAction() {
            return !this.actions.isEmpty();
        }

        Runnable popAction() {
            return this.actions.pop();
        }

        void addAction(Runnable runnable) {
            this.actions.push(runnable);
        }

        void reset() {
            this.actions.clear();
            this.completed = false;
        }

        boolean isCompleted() {
            return this.completed;
        }

        void markCompleted() {
            this.completed = true;
        }
    }
}
