package org.thoughtcrime.securesms.mediasend.v2.text;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.transition.TransitionManager;
import com.airbnb.lottie.SimpleColorFilter;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysJvmKt;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.text.StringsKt__StringsKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.KeyboardEntryDialogFragment;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.PushDatabase;
import org.thoughtcrime.securesms.fonts.TextFont;
import org.thoughtcrime.securesms.mediasend.v2.MediaAnimations;
import org.thoughtcrime.securesms.mediasend.v2.text.TextStoryTextWatcher;
import org.thoughtcrime.securesms.scribbles.HSVColorSlider;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: TextStoryPostTextEntryFragment.kt */
@Metadata(d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u000289B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\"\u001a\u00020#H\u0002J\b\u0010$\u001a\u00020#H\u0002J\b\u0010%\u001a\u00020#H\u0002J\b\u0010&\u001a\u00020#H\u0002J\b\u0010'\u001a\u00020#H\u0002J\b\u0010(\u001a\u00020#H\u0002J\b\u0010)\u001a\u00020#H\u0002J\b\u0010*\u001a\u00020#H\u0002J\b\u0010+\u001a\u00020#H\u0002J\u0010\u0010,\u001a\u00020#2\u0006\u0010-\u001a\u00020\u0012H\u0002J\b\u0010.\u001a\u00020#H\u0003J\u0010\u0010/\u001a\u00020#2\u0006\u00100\u001a\u000201H\u0016J\b\u00102\u001a\u00020#H\u0016J\b\u00103\u001a\u00020#H\u0016J\u001a\u00104\u001a\u00020#2\u0006\u0010-\u001a\u00020\u00122\b\u00105\u001a\u0004\u0018\u000106H\u0016J\b\u00107\u001a\u00020#H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X.¢\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0014X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX.¢\u0006\u0002\n\u0000R\u001b\u0010\u001c\u001a\u00020\u001d8BX\u0002¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\u001e\u0010\u001f¨\u0006:"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostTextEntryFragment;", "Lorg/thoughtcrime/securesms/components/KeyboardEntryDialogFragment;", "()V", "alignmentButton", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextAlignmentButton;", "allCapsFilter", "Landroid/text/InputFilter$AllCaps;", "backgroundButton", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextColorStyleButton;", "bufferFilter", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostTextEntryFragment$BufferFilter;", "colorBar", "Landroidx/appcompat/widget/AppCompatSeekBar;", "colorIndicator", "Landroid/widget/ImageView;", "colorIndicatorAlphaAnimator", "Landroid/animation/Animator;", "confirmButton", "Landroid/view/View;", "fadeableViews", "", "fontButton", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextFontButton;", "input", "Landroid/widget/EditText;", "scaleBar", "scene", "Landroidx/constraintlayout/widget/ConstraintLayout;", "viewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "animateWidthBarIn", "", "animateWidthBarOut", "initializeAlignmentButton", "initializeBackgroundButton", "initializeColorBar", "initializeConfirmButton", "initializeFontButton", "initializeInput", "initializeViewModel", "initializeViews", "view", "initializeWidthBar", "onDismiss", "dialog", "Landroid/content/DialogInterface;", "onPause", "onResume", "onViewCreated", "savedInstanceState", "Landroid/os/Bundle;", "presentHint", "BufferFilter", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryPostTextEntryFragment extends KeyboardEntryDialogFragment {
    private TextAlignmentButton alignmentButton;
    private InputFilter.AllCaps allCapsFilter = new InputFilter.AllCaps();
    private TextColorStyleButton backgroundButton;
    private BufferFilter bufferFilter = new BufferFilter();
    private AppCompatSeekBar colorBar;
    private ImageView colorIndicator;
    private Animator colorIndicatorAlphaAnimator;
    private View confirmButton;
    private List<? extends View> fadeableViews;
    private TextFontButton fontButton;
    private EditText input;
    private AppCompatSeekBar scaleBar;
    private ConstraintLayout scene;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(TextStoryPostCreationViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$viewModel$2
        final /* synthetic */ TextStoryPostTextEntryFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    /* compiled from: TextStoryPostTextEntryFragment.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostTextEntryFragment$Callback;", "", "onTextStoryPostTextEntryDismissed", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {
        void onTextStoryPostTextEntryDismissed();
    }

    public TextStoryPostTextEntryFragment() {
        super(R.layout.stories_text_post_text_entry_fragment);
    }

    public final TextStoryPostCreationViewModel getViewModel() {
        return (TextStoryPostCreationViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        Window window = requireDialog().getWindow();
        WindowManager.LayoutParams attributes = window != null ? window.getAttributes() : null;
        if (attributes != null) {
            attributes.windowAnimations = R.style.TextSecure_Animation_TextStoryPostEntryDialog;
        }
        initializeViews(view);
        initializeInput();
        initializeAlignmentButton();
        initializeColorBar();
        initializeConfirmButton();
        initializeWidthBar();
        initializeBackgroundButton();
        initializeFontButton();
        initializeViewModel();
        view.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TextStoryPostTextEntryFragment.m2271onViewCreated$lambda0(TextStoryPostTextEntryFragment.this, view2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2271onViewCreated$lambda0(TextStoryPostTextEntryFragment textStoryPostTextEntryFragment, View view) {
        Intrinsics.checkNotNullParameter(textStoryPostTextEntryFragment, "this$0");
        textStoryPostTextEntryFragment.dismissAllowingStateLoss();
    }

    private final void initializeViews(View view) {
        View findViewById = view.findViewById(R.id.scene);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.scene)");
        this.scene = (ConstraintLayout) findViewById;
        View findViewById2 = view.findViewById(R.id.input);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.input)");
        this.input = (EditText) findViewById2;
        View findViewById3 = view.findViewById(R.id.confirm);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.confirm)");
        this.confirmButton = findViewById3;
        View findViewById4 = view.findViewById(R.id.color_bar);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.color_bar)");
        this.colorBar = (AppCompatSeekBar) findViewById4;
        View findViewById5 = view.findViewById(R.id.color_indicator);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.color_indicator)");
        this.colorIndicator = (ImageView) findViewById5;
        View findViewById6 = view.findViewById(R.id.alignment_button);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.alignment_button)");
        this.alignmentButton = (TextAlignmentButton) findViewById6;
        View findViewById7 = view.findViewById(R.id.font_button);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.font_button)");
        this.fontButton = (TextFontButton) findViewById7;
        View findViewById8 = view.findViewById(R.id.width_bar);
        Intrinsics.checkNotNullExpressionValue(findViewById8, "view.findViewById(R.id.width_bar)");
        this.scaleBar = (AppCompatSeekBar) findViewById8;
        View findViewById9 = view.findViewById(R.id.background_button);
        Intrinsics.checkNotNullExpressionValue(findViewById9, "view.findViewById(R.id.background_button)");
        this.backgroundButton = (TextColorStyleButton) findViewById9;
        View[] viewArr = new View[3];
        View view2 = this.confirmButton;
        AppCompatSeekBar appCompatSeekBar = null;
        if (view2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("confirmButton");
            view2 = null;
        }
        viewArr[0] = view2;
        TextFontButton textFontButton = this.fontButton;
        if (textFontButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("fontButton");
            textFontButton = null;
        }
        viewArr[1] = textFontButton;
        TextColorStyleButton textColorStyleButton = this.backgroundButton;
        if (textColorStyleButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("backgroundButton");
            textColorStyleButton = null;
        }
        viewArr[2] = textColorStyleButton;
        this.fadeableViews = CollectionsKt__CollectionsKt.listOf((Object[]) viewArr);
        if (FeatureFlags.storiesTextFunctions()) {
            List<? extends View> list = this.fadeableViews;
            if (list == null) {
                Intrinsics.throwUninitializedPropertyAccessException("fadeableViews");
                list = null;
            }
            TextAlignmentButton textAlignmentButton = this.alignmentButton;
            if (textAlignmentButton == null) {
                Intrinsics.throwUninitializedPropertyAccessException("alignmentButton");
                textAlignmentButton = null;
            }
            this.fadeableViews = CollectionsKt___CollectionsKt.plus((Collection<? extends TextAlignmentButton>) ((Collection<? extends Object>) list), textAlignmentButton);
            TextAlignmentButton textAlignmentButton2 = this.alignmentButton;
            if (textAlignmentButton2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("alignmentButton");
                textAlignmentButton2 = null;
            }
            textAlignmentButton2.setVisibility(0);
            AppCompatSeekBar appCompatSeekBar2 = this.scaleBar;
            if (appCompatSeekBar2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("scaleBar");
            } else {
                appCompatSeekBar = appCompatSeekBar2;
            }
            appCompatSeekBar.setVisibility(0);
        }
    }

    private final void initializeInput() {
        TextStoryTextWatcher.Companion companion = TextStoryTextWatcher.Companion;
        EditText editText = this.input;
        EditText editText2 = null;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText = null;
        }
        companion.install(editText);
        EditText editText3 = this.input;
        if (editText3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText3 = null;
        }
        EditText editText4 = this.input;
        if (editText4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText4 = null;
        }
        InputFilter[] filters = editText4.getFilters();
        Intrinsics.checkNotNullExpressionValue(filters, "input.filters");
        editText3.setFilters((InputFilter[]) ArraysKt___ArraysJvmKt.plus((BufferFilter[]) filters, this.bufferFilter));
        EditText editText5 = this.input;
        if (editText5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText5 = null;
        }
        editText5.addTextChangedListener(new TextWatcher(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$initializeInput$$inlined$doOnTextChanged$1
            final /* synthetic */ TextStoryPostTextEntryFragment this$0;

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
            }

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            {
                this.this$0 = r1;
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                this.this$0.presentHint();
            }
        });
        EditText editText6 = this.input;
        if (editText6 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText6 = null;
        }
        editText6.addTextChangedListener(new TextWatcher(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$initializeInput$$inlined$doAfterTextChanged$1
            final /* synthetic */ TextStoryPostTextEntryFragment this$0;

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            {
                this.this$0 = r1;
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                String str;
                TextStoryPostCreationViewModel textStoryPostCreationViewModel = this.this$0.getViewModel();
                if (editable == null || (str = editable.toString()) == null) {
                    str = "";
                }
                textStoryPostCreationViewModel.setTemporaryBody(str);
            }
        });
        EditText editText7 = this.input;
        if (editText7 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
        } else {
            editText2 = editText7;
        }
        editText2.setText(getViewModel().getBody());
    }

    public final void presentHint() {
        EditText editText = this.input;
        EditText editText2 = null;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText = null;
        }
        if (TextUtils.isEmpty(editText.getText())) {
            EditText editText3 = this.input;
            if (editText3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("input");
                editText3 = null;
            }
            editText3.setAlpha(0.6f);
            EditText editText4 = this.input;
            if (editText4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("input");
                editText4 = null;
            }
            InputFilter[] filters = editText4.getFilters();
            Intrinsics.checkNotNullExpressionValue(filters, "input.filters");
            if (ArraysKt___ArraysKt.contains((InputFilter.AllCaps[]) filters, this.allCapsFilter)) {
                EditText editText5 = this.input;
                if (editText5 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("input");
                } else {
                    editText2 = editText5;
                }
                String string = getString(R.string.TextStoryPostTextEntryFragment__add_text);
                Intrinsics.checkNotNullExpressionValue(string, "getString(R.string.TextS…tEntryFragment__add_text)");
                Locale locale = Locale.getDefault();
                Intrinsics.checkNotNullExpressionValue(locale, "getDefault()");
                String upperCase = string.toUpperCase(locale);
                Intrinsics.checkNotNullExpressionValue(upperCase, "this as java.lang.String).toUpperCase(locale)");
                editText2.setHint(upperCase);
                return;
            }
            EditText editText6 = this.input;
            if (editText6 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("input");
            } else {
                editText2 = editText6;
            }
            editText2.setHint(R.string.TextStoryPostTextEntryFragment__add_text);
            return;
        }
        EditText editText7 = this.input;
        if (editText7 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText7 = null;
        }
        editText7.setAlpha(1.0f);
        EditText editText8 = this.input;
        if (editText8 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
        } else {
            editText2 = editText8;
        }
        editText2.setHint("");
    }

    private final void initializeBackgroundButton() {
        TextColorStyleButton textColorStyleButton = this.backgroundButton;
        if (textColorStyleButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("backgroundButton");
            textColorStyleButton = null;
        }
        textColorStyleButton.setOnTextColorStyleChanged(new Function1<TextColorStyle, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$initializeBackgroundButton$1
            final /* synthetic */ TextStoryPostTextEntryFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(TextColorStyle textColorStyle) {
                invoke(textColorStyle);
                return Unit.INSTANCE;
            }

            public final void invoke(TextColorStyle textColorStyle) {
                Intrinsics.checkNotNullParameter(textColorStyle, "it");
                this.this$0.getViewModel().setTextColorStyle(textColorStyle);
            }
        });
    }

    private final void initializeFontButton() {
        TextFontButton textFontButton = this.fontButton;
        if (textFontButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("fontButton");
            textFontButton = null;
        }
        textFontButton.setOnTextFontChanged(new Function1<TextFont, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$initializeFontButton$1
            final /* synthetic */ TextStoryPostTextEntryFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(TextFont textFont) {
                invoke(textFont);
                return Unit.INSTANCE;
            }

            public final void invoke(TextFont textFont) {
                Intrinsics.checkNotNullParameter(textFont, "it");
                this.this$0.getViewModel().setTextFont(textFont);
            }
        });
    }

    private final void initializeColorBar() {
        AppCompatSeekBar appCompatSeekBar;
        ImageView imageView = this.colorIndicator;
        AppCompatSeekBar appCompatSeekBar2 = null;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("colorIndicator");
            imageView = null;
        }
        imageView.setBackground(AppCompatResources.getDrawable(requireContext(), R.drawable.ic_color_preview));
        HSVColorSlider hSVColorSlider = HSVColorSlider.INSTANCE;
        AppCompatSeekBar appCompatSeekBar3 = this.colorBar;
        if (appCompatSeekBar3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("colorBar");
            appCompatSeekBar = null;
        } else {
            appCompatSeekBar = appCompatSeekBar3;
        }
        hSVColorSlider.setUpForColor(appCompatSeekBar, -1, new Function1<Integer, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$initializeColorBar$1
            final /* synthetic */ TextStoryPostTextEntryFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                invoke(num.intValue());
                return Unit.INSTANCE;
            }

            public final void invoke(int i) {
                ImageView imageView2 = this.this$0.colorIndicator;
                AppCompatSeekBar appCompatSeekBar4 = null;
                if (imageView2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorIndicator");
                    imageView2 = null;
                }
                Drawable drawable = imageView2.getDrawable();
                HSVColorSlider hSVColorSlider2 = HSVColorSlider.INSTANCE;
                AppCompatSeekBar appCompatSeekBar5 = this.this$0.colorBar;
                if (appCompatSeekBar5 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorBar");
                    appCompatSeekBar5 = null;
                }
                drawable.setColorFilter(new SimpleColorFilter(hSVColorSlider2.getColor(appCompatSeekBar5)));
                ImageView imageView3 = this.this$0.colorIndicator;
                if (imageView3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorIndicator");
                    imageView3 = null;
                }
                AppCompatSeekBar appCompatSeekBar6 = this.this$0.colorBar;
                if (appCompatSeekBar6 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorBar");
                    appCompatSeekBar6 = null;
                }
                imageView3.setTranslationX(((float) appCompatSeekBar6.getThumb().getBounds().left) + ((float) ViewUtil.dpToPx(16)));
                TextStoryPostCreationViewModel textStoryPostCreationViewModel = this.this$0.getViewModel();
                AppCompatSeekBar appCompatSeekBar7 = this.this$0.colorBar;
                if (appCompatSeekBar7 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorBar");
                } else {
                    appCompatSeekBar4 = appCompatSeekBar7;
                }
                textStoryPostCreationViewModel.setTextColor(hSVColorSlider2.getColor(appCompatSeekBar4));
            }
        }, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$initializeColorBar$2
            final /* synthetic */ TextStoryPostTextEntryFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                Animator animator = this.this$0.colorIndicatorAlphaAnimator;
                if (animator != null) {
                    animator.end();
                }
                TextStoryPostTextEntryFragment textStoryPostTextEntryFragment = this.this$0;
                ImageView imageView2 = textStoryPostTextEntryFragment.colorIndicator;
                ConstraintLayout constraintLayout = null;
                if (imageView2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorIndicator");
                    imageView2 = null;
                }
                float[] fArr = new float[2];
                ImageView imageView3 = this.this$0.colorIndicator;
                if (imageView3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorIndicator");
                    imageView3 = null;
                }
                fArr[0] = imageView3.getAlpha();
                fArr[1] = 1.0f;
                textStoryPostTextEntryFragment.colorIndicatorAlphaAnimator = ObjectAnimator.ofFloat(imageView2, "alpha", fArr);
                Animator animator2 = this.this$0.colorIndicatorAlphaAnimator;
                if (animator2 != null) {
                    animator2.setDuration(150);
                }
                Animator animator3 = this.this$0.colorIndicatorAlphaAnimator;
                if (animator3 != null) {
                    animator3.start();
                }
                ConstraintLayout constraintLayout2 = this.this$0.scene;
                if (constraintLayout2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("scene");
                    constraintLayout2 = null;
                }
                TransitionManager.endTransitions(constraintLayout2);
                ConstraintSet constraintSet = new ConstraintSet();
                ConstraintLayout constraintLayout3 = this.this$0.scene;
                if (constraintLayout3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("scene");
                    constraintLayout3 = null;
                }
                constraintSet.clone(constraintLayout3);
                List<View> list = this.this$0.fadeableViews;
                if (list == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("fadeableViews");
                    list = null;
                }
                for (View view : list) {
                    constraintSet.setVisibility(view.getId(), 4);
                }
                ConstraintLayout constraintLayout4 = this.this$0.scene;
                if (constraintLayout4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("scene");
                    constraintLayout4 = null;
                }
                constraintSet.applyTo(constraintLayout4);
                ConstraintLayout constraintLayout5 = this.this$0.scene;
                if (constraintLayout5 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("scene");
                    constraintLayout5 = null;
                }
                TransitionManager.beginDelayedTransition(constraintLayout5);
                AppCompatSeekBar appCompatSeekBar4 = this.this$0.colorBar;
                if (appCompatSeekBar4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorBar");
                    appCompatSeekBar4 = null;
                }
                constraintSet.connect(appCompatSeekBar4.getId(), 6, 0, 6);
                AppCompatSeekBar appCompatSeekBar5 = this.this$0.colorBar;
                if (appCompatSeekBar5 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorBar");
                    appCompatSeekBar5 = null;
                }
                constraintSet.connect(appCompatSeekBar5.getId(), 7, 0, 7);
                ConstraintLayout constraintLayout6 = this.this$0.scene;
                if (constraintLayout6 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("scene");
                } else {
                    constraintLayout = constraintLayout6;
                }
                constraintSet.applyTo(constraintLayout);
            }
        }, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$initializeColorBar$3
            final /* synthetic */ TextStoryPostTextEntryFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                Animator animator = this.this$0.colorIndicatorAlphaAnimator;
                if (animator != null) {
                    animator.end();
                }
                TextStoryPostTextEntryFragment textStoryPostTextEntryFragment = this.this$0;
                ImageView imageView2 = textStoryPostTextEntryFragment.colorIndicator;
                ConstraintLayout constraintLayout = null;
                if (imageView2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorIndicator");
                    imageView2 = null;
                }
                float[] fArr = new float[2];
                ImageView imageView3 = this.this$0.colorIndicator;
                if (imageView3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorIndicator");
                    imageView3 = null;
                }
                fArr[0] = imageView3.getAlpha();
                fArr[1] = 0.0f;
                textStoryPostTextEntryFragment.colorIndicatorAlphaAnimator = ObjectAnimator.ofFloat(imageView2, "alpha", fArr);
                Animator animator2 = this.this$0.colorIndicatorAlphaAnimator;
                if (animator2 != null) {
                    animator2.setDuration(150);
                }
                Animator animator3 = this.this$0.colorIndicatorAlphaAnimator;
                if (animator3 != null) {
                    animator3.start();
                }
                ConstraintLayout constraintLayout2 = this.this$0.scene;
                if (constraintLayout2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("scene");
                    constraintLayout2 = null;
                }
                TransitionManager.endTransitions(constraintLayout2);
                ConstraintLayout constraintLayout3 = this.this$0.scene;
                if (constraintLayout3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("scene");
                    constraintLayout3 = null;
                }
                TransitionManager.beginDelayedTransition(constraintLayout3);
                ConstraintSet constraintSet = new ConstraintSet();
                ConstraintLayout constraintLayout4 = this.this$0.scene;
                if (constraintLayout4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("scene");
                    constraintLayout4 = null;
                }
                constraintSet.clone(constraintLayout4);
                List<View> list = this.this$0.fadeableViews;
                if (list == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("fadeableViews");
                    list = null;
                }
                for (View view : list) {
                    constraintSet.setVisibility(view.getId(), 0);
                }
                AppCompatSeekBar appCompatSeekBar4 = this.this$0.colorBar;
                if (appCompatSeekBar4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorBar");
                    appCompatSeekBar4 = null;
                }
                int id = appCompatSeekBar4.getId();
                TextColorStyleButton textColorStyleButton = this.this$0.backgroundButton;
                if (textColorStyleButton == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("backgroundButton");
                    textColorStyleButton = null;
                }
                constraintSet.connect(id, 6, textColorStyleButton.getId(), 7);
                AppCompatSeekBar appCompatSeekBar5 = this.this$0.colorBar;
                if (appCompatSeekBar5 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("colorBar");
                    appCompatSeekBar5 = null;
                }
                int id2 = appCompatSeekBar5.getId();
                TextFontButton textFontButton = this.this$0.fontButton;
                if (textFontButton == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("fontButton");
                    textFontButton = null;
                }
                constraintSet.connect(id2, 7, textFontButton.getId(), 6);
                ConstraintLayout constraintLayout5 = this.this$0.scene;
                if (constraintLayout5 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("scene");
                } else {
                    constraintLayout = constraintLayout5;
                }
                constraintSet.applyTo(constraintLayout);
            }
        });
        AppCompatSeekBar appCompatSeekBar4 = this.colorBar;
        if (appCompatSeekBar4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("colorBar");
        } else {
            appCompatSeekBar2 = appCompatSeekBar4;
        }
        hSVColorSlider.setColor(appCompatSeekBar2, getViewModel().getTextColor());
    }

    private final void initializeConfirmButton() {
        View view = this.confirmButton;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("confirmButton");
            view = null;
        }
        view.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TextStoryPostTextEntryFragment.m2267initializeConfirmButton$lambda3(TextStoryPostTextEntryFragment.this, view2);
            }
        });
    }

    /* renamed from: initializeConfirmButton$lambda-3 */
    public static final void m2267initializeConfirmButton$lambda3(TextStoryPostTextEntryFragment textStoryPostTextEntryFragment, View view) {
        Intrinsics.checkNotNullParameter(textStoryPostTextEntryFragment, "this$0");
        textStoryPostTextEntryFragment.dismissAllowingStateLoss();
    }

    private final void initializeAlignmentButton() {
        TextAlignmentButton textAlignmentButton = this.alignmentButton;
        if (textAlignmentButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("alignmentButton");
            textAlignmentButton = null;
        }
        textAlignmentButton.setOnAlignmentChangedListener(new Function1<TextAlignment, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$initializeAlignmentButton$1
            final /* synthetic */ TextStoryPostTextEntryFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(TextAlignment textAlignment) {
                invoke(textAlignment);
                return Unit.INSTANCE;
            }

            public final void invoke(TextAlignment textAlignment) {
                Intrinsics.checkNotNullParameter(textAlignment, "alignment");
                this.this$0.getViewModel().setAlignment(textAlignment);
            }
        });
    }

    private final void initializeViewModel() {
        getViewModel().getTypeface().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                TextStoryPostTextEntryFragment.m2268initializeViewModel$lambda4(TextStoryPostTextEntryFragment.this, (Typeface) obj);
            }
        });
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                TextStoryPostTextEntryFragment.m2269initializeViewModel$lambda7(TextStoryPostTextEntryFragment.this, (TextStoryPostCreationState) obj);
            }
        });
    }

    /* renamed from: initializeViewModel$lambda-4 */
    public static final void m2268initializeViewModel$lambda4(TextStoryPostTextEntryFragment textStoryPostTextEntryFragment, Typeface typeface) {
        Intrinsics.checkNotNullParameter(textStoryPostTextEntryFragment, "this$0");
        EditText editText = textStoryPostTextEntryFragment.input;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText = null;
        }
        editText.setTypeface(typeface);
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x01e8  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01f9  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01ff  */
    /* renamed from: initializeViewModel$lambda-7 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m2269initializeViewModel$lambda7(org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment r7, org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationState r8) {
        /*
        // Method dump skipped, instructions count: 528
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment.m2269initializeViewModel$lambda7(org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment, org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationState):void");
    }

    private final void initializeWidthBar() {
        AppCompatSeekBar appCompatSeekBar = this.scaleBar;
        AppCompatSeekBar appCompatSeekBar2 = null;
        if (appCompatSeekBar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scaleBar");
            appCompatSeekBar = null;
        }
        appCompatSeekBar.setProgressDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.ic_width_slider_bg));
        AppCompatSeekBar appCompatSeekBar3 = this.scaleBar;
        if (appCompatSeekBar3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scaleBar");
            appCompatSeekBar3 = null;
        }
        appCompatSeekBar3.setThumb(HSVColorSlider.INSTANCE.createThumbDrawable(-1));
        AppCompatSeekBar appCompatSeekBar4 = this.scaleBar;
        if (appCompatSeekBar4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scaleBar");
            appCompatSeekBar4 = null;
        }
        appCompatSeekBar4.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$initializeWidthBar$1
            final /* synthetic */ TextStoryPostTextEntryFragment this$0;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                this.this$0.getViewModel().setTextScale(i);
            }
        });
        AppCompatSeekBar appCompatSeekBar5 = this.scaleBar;
        if (appCompatSeekBar5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scaleBar");
        } else {
            appCompatSeekBar2 = appCompatSeekBar5;
        }
        appCompatSeekBar2.setOnTouchListener(new View.OnTouchListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return TextStoryPostTextEntryFragment.m2270initializeWidthBar$lambda8(TextStoryPostTextEntryFragment.this, view, motionEvent);
            }
        });
    }

    /* renamed from: initializeWidthBar$lambda-8 */
    public static final boolean m2270initializeWidthBar$lambda8(TextStoryPostTextEntryFragment textStoryPostTextEntryFragment, View view, MotionEvent motionEvent) {
        Intrinsics.checkNotNullParameter(textStoryPostTextEntryFragment, "this$0");
        if (motionEvent.getAction() == 0) {
            textStoryPostTextEntryFragment.animateWidthBarIn();
        } else if (motionEvent.getAction() == 1 || motionEvent.getAction() == 3) {
            textStoryPostTextEntryFragment.animateWidthBarOut();
        }
        return view.onTouchEvent(motionEvent);
    }

    private final void animateWidthBarIn() {
        AppCompatSeekBar appCompatSeekBar = this.scaleBar;
        if (appCompatSeekBar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scaleBar");
            appCompatSeekBar = null;
        }
        appCompatSeekBar.animate().setDuration(250).setInterpolator(MediaAnimations.getInterpolator()).translationX((float) ViewUtil.dpToPx(36));
    }

    private final void animateWidthBarOut() {
        AppCompatSeekBar appCompatSeekBar = this.scaleBar;
        if (appCompatSeekBar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scaleBar");
            appCompatSeekBar = null;
        }
        appCompatSeekBar.animate().setDuration(250).setInterpolator(MediaAnimations.getInterpolator()).translationX(0.0f);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        EditText editText = this.input;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText = null;
        }
        ViewUtil.focusAndMoveCursorToEndAndOpenKeyboard(editText);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Context requireContext = requireContext();
        EditText editText = this.input;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText = null;
        }
        ViewUtil.hideKeyboard(requireContext, editText);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:16:0x0030 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v3, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r2v6, types: [org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$Callback] */
    /* JADX WARN: Type inference failed for: r2v8 */
    /* JADX WARN: Type inference failed for: r2v11 */
    /* JADX WARN: Type inference failed for: r2v12 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDismiss(android.content.DialogInterface r2) {
        /*
            r1 = this;
            java.lang.String r0 = "dialog"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r2, r0)
            super.onDismiss(r2)
            org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel r2 = r1.getViewModel()
            org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$BufferFilter r0 = r1.bufferFilter
            java.lang.CharSequence r0 = r0.getText()
            r2.setBody(r0)
            androidx.fragment.app.Fragment r2 = r1.getParentFragment()
        L_0x0019:
            if (r2 == 0) goto L_0x0025
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment.Callback
            if (r0 == 0) goto L_0x0020
            goto L_0x0030
        L_0x0020:
            androidx.fragment.app.Fragment r2 = r2.getParentFragment()
            goto L_0x0019
        L_0x0025:
            androidx.fragment.app.FragmentActivity r2 = r1.requireActivity()
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment.Callback
            if (r0 != 0) goto L_0x002e
            r2 = 0
        L_0x002e:
            org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$Callback r2 = (org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment.Callback) r2
        L_0x0030:
            org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment$Callback r2 = (org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment.Callback) r2
            if (r2 == 0) goto L_0x0037
            r2.onTextStoryPostTextEntryDismissed()
        L_0x0037:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment.onDismiss(android.content.DialogInterface):void");
    }

    /* compiled from: TextStoryPostTextEntryFragment.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J>\u0010\t\u001a\u0004\u0018\u00010\u00042\b\u0010\n\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\f2\u0006\u0010\u0011\u001a\u00020\fH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostTextEntryFragment$BufferFilter;", "Landroid/text/InputFilter;", "()V", DraftDatabase.Draft.TEXT, "", "getText", "()Ljava/lang/CharSequence;", "setText", "(Ljava/lang/CharSequence;)V", "filter", PushDatabase.SOURCE_E164, NotificationProfileDatabase.NotificationProfileScheduleTable.START, "", NotificationProfileDatabase.NotificationProfileScheduleTable.END, "dest", "Landroid/text/Spanned;", "dstart", "dend", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class BufferFilter implements InputFilter {
        private CharSequence text = "";

        public final CharSequence getText() {
            return this.text;
        }

        public final void setText(CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(charSequence, "<set-?>");
            this.text = charSequence;
        }

        @Override // android.text.InputFilter
        public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
            CharSequence charSequence2;
            if (charSequence == null || charSequence.length() == 0) {
                charSequence2 = StringsKt__StringsKt.removeRange(this.text, i3, i4);
            } else {
                charSequence2 = StringsKt__StringsKt.replaceRange(this.text, i3, i4, charSequence.subSequence(i, i2));
            }
            this.text = charSequence2;
            return null;
        }
    }
}
