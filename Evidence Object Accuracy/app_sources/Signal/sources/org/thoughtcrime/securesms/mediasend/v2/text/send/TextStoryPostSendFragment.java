package org.thoughtcrime.securesms.mediasend.v2.text.send;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentKt;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.WrapperDialogFragment;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchData;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchState;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet;
import org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationState;
import org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.sharing.ShareContact;
import org.thoughtcrime.securesms.sharing.ShareSelectionAdapter;
import org.thoughtcrime.securesms.sharing.ShareSelectionMappingModel;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.settings.create.CreateStoryFlowDialogFragment;
import org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment;
import org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: TextStoryPostSendFragment.kt */
@Metadata(d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u0001\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005B\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\"\u001a\u00020#H\u0002J\b\u0010$\u001a\u00020#H\u0002J\b\u0010%\u001a\u00020#H\u0016J\b\u0010&\u001a\u00020#H\u0016J\b\u0010'\u001a\u00020(H\u0016J\u0010\u0010)\u001a\u00020#2\u0006\u0010*\u001a\u00020+H\u0016J\b\u0010,\u001a\u00020#H\u0016J\u001a\u0010-\u001a\u00020#2\u0006\u0010.\u001a\u00020\u00172\b\u0010/\u001a\u0004\u0018\u000100H\u0016J\b\u00101\u001a\u00020#H\u0016J\b\u00102\u001a\u00020#H\u0002J\u0016\u00103\u001a\u00020#2\f\u00104\u001a\b\u0012\u0004\u0012\u00020605H\u0016R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u001b\u0010\t\u001a\u00020\n8BX\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0011\u001a\u00020\u00128BX\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u000e\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0016\u001a\u00020\u0017X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0017X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX.¢\u0006\u0002\n\u0000R\u001b\u0010\u001d\u001a\u00020\u001e8BX\u0002¢\u0006\f\n\u0004\b!\u0010\u000e\u001a\u0004\b\u001f\u0010 ¨\u00067"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/mediasend/v2/stories/ChooseStoryTypeBottomSheet$Callback;", "Lorg/thoughtcrime/securesms/components/WrapperDialogFragment$WrapperDialogFragmentCallback;", "Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipBottomSheetDialogFragment$Callback;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheet$Callbacks;", "()V", "contactSearchMediator", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchMediator;", "creationViewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "getCreationViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "creationViewModel$delegate", "Lkotlin/Lazy;", "disposables", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "linkPreviewViewModel", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreviewViewModel;", "getLinkPreviewViewModel", "()Lorg/thoughtcrime/securesms/linkpreview/LinkPreviewViewModel;", "linkPreviewViewModel$delegate", "shareConfirmButton", "Landroid/view/View;", "shareListWrapper", "shareSelectionAdapter", "Lorg/thoughtcrime/securesms/sharing/ShareSelectionAdapter;", "shareSelectionRecyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "viewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendViewModel;", "viewModel$delegate", "animateInSelection", "", "animateOutSelection", "onCanceled", "onGroupStoryClicked", "onMessageResentAfterSafetyNumberChangeInBottomSheet", "", "onMyStoryConfigured", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "onNewStoryClicked", "onViewCreated", "view", "savedInstanceState", "Landroid/os/Bundle;", "onWrapperDialogFragmentDismissed", "send", "sendAnywayAfterSafetyNumberChangedInBottomSheet", "destinations", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryPostSendFragment extends Fragment implements ChooseStoryTypeBottomSheet.Callback, WrapperDialogFragment.WrapperDialogFragmentCallback, ChooseInitialMyStoryMembershipBottomSheetDialogFragment.Callback, SafetyNumberBottomSheet.Callbacks {
    private ContactSearchMediator contactSearchMediator;
    private final Lazy creationViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(TextStoryPostCreationViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$creationViewModel$2
        final /* synthetic */ TextStoryPostSendFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$special$$inlined$viewModels$default$3
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private final LifecycleDisposable disposables = new LifecycleDisposable();
    private final Lazy linkPreviewViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(LinkPreviewViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$linkPreviewViewModel$2
        final /* synthetic */ TextStoryPostSendFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$special$$inlined$viewModels$default$4
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private View shareConfirmButton;
    private View shareListWrapper;
    private final ShareSelectionAdapter shareSelectionAdapter = new ShareSelectionAdapter();
    private RecyclerView shareSelectionRecyclerView;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(TextStoryPostSendViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, TextStoryPostSendFragment$viewModel$2.INSTANCE);

    /* compiled from: TextStoryPostSendFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[TextStoryPostSendState.values().length];
            iArr[TextStoryPostSendState.INIT.ordinal()] = 1;
            iArr[TextStoryPostSendState.SENDING.ordinal()] = 2;
            iArr[TextStoryPostSendState.SENT.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public TextStoryPostSendFragment() {
        super(R.layout.stories_send_text_post_fragment);
    }

    private final TextStoryPostSendViewModel getViewModel() {
        return (TextStoryPostSendViewModel) this.viewModel$delegate.getValue();
    }

    private final TextStoryPostCreationViewModel getCreationViewModel() {
        return (TextStoryPostCreationViewModel) this.creationViewModel$delegate.getValue();
    }

    private final LinkPreviewViewModel getLinkPreviewViewModel() {
        return (LinkPreviewViewModel) this.linkPreviewViewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        View findViewById2 = view.findViewById(R.id.preview_viewport);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.preview_viewport)");
        ImageView imageView = (ImageView) findViewById2;
        View findViewById3 = view.findViewById(R.id.search_field);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.search_field)");
        EditText editText = (EditText) findViewById3;
        ((Toolbar) findViewById).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TextStoryPostSendFragment.$r8$lambda$oSPW3bXBobKILW9rKyE5u7FObUk(TextStoryPostSendFragment.this, view2);
            }
        });
        View findViewById4 = view.findViewById(R.id.list_wrapper);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.list_wrapper)");
        this.shareListWrapper = findViewById4;
        View findViewById5 = view.findViewById(R.id.share_confirm);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.share_confirm)");
        this.shareConfirmButton = findViewById5;
        View findViewById6 = view.findViewById(R.id.selected_list);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.selected_list)");
        RecyclerView recyclerView = (RecyclerView) findViewById6;
        this.shareSelectionRecyclerView = recyclerView;
        ContactSearchMediator contactSearchMediator = null;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("shareSelectionRecyclerView");
            recyclerView = null;
        }
        recyclerView.setAdapter(this.shareSelectionAdapter);
        LifecycleDisposable lifecycleDisposable = this.disposables;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        getCreationViewModel().getThumbnail().observe(getViewLifecycleOwner(), new Observer(imageView) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ ImageView f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                TextStoryPostSendFragment.$r8$lambda$iWbE6eo2K5mjyihRmZbJOBFmYRc(this.f$0, (Bitmap) obj);
            }
        });
        View view2 = this.shareConfirmButton;
        if (view2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("shareConfirmButton");
            view2 = null;
        }
        view2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                TextStoryPostSendFragment.$r8$lambda$EGLTydqm50lXpZMvrsAUmHHy5U8(TextStoryPostSendFragment.this, view3);
            }
        });
        LifecycleDisposable lifecycleDisposable2 = this.disposables;
        Disposable subscribe = getViewModel().getUntrustedIdentities().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                TextStoryPostSendFragment.$r8$lambda$2Bpky4D6V5HPrg8iABCmx1Xe0eQ(TextStoryPostSendFragment.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.untrustedIdent…ildFragmentManager)\n    }");
        lifecycleDisposable2.plusAssign(subscribe);
        editText.addTextChangedListener(new TextWatcher(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$onViewCreated$$inlined$doAfterTextChanged$1
            final /* synthetic */ TextStoryPostSendFragment this$0;

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            {
                this.this$0 = r1;
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                ContactSearchMediator access$getContactSearchMediator$p = TextStoryPostSendFragment.access$getContactSearchMediator$p(this.this$0);
                String str = null;
                if (access$getContactSearchMediator$p == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
                    access$getContactSearchMediator$p = null;
                }
                if (editable != null) {
                    str = editable.toString();
                }
                access$getContactSearchMediator$p.onFilterChanged(str);
            }
        });
        FragmentKt.setFragmentResultListener(this, CreateStoryWithViewersFragment.REQUEST_KEY, new Function2<String, Bundle, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$onViewCreated$6
            final /* synthetic */ TextStoryPostSendFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((String) obj, (Bundle) obj2);
                return Unit.INSTANCE;
            }

            public final void invoke(String str, Bundle bundle2) {
                Intrinsics.checkNotNullParameter(str, "<anonymous parameter 0>");
                Intrinsics.checkNotNullParameter(bundle2, "bundle");
                Parcelable parcelable = bundle2.getParcelable(CreateStoryWithViewersFragment.STORY_RECIPIENT);
                Intrinsics.checkNotNull(parcelable);
                RecipientId recipientId = (RecipientId) parcelable;
                ContactSearchMediator access$getContactSearchMediator$p = TextStoryPostSendFragment.access$getContactSearchMediator$p(this.this$0);
                ContactSearchMediator contactSearchMediator2 = null;
                if (access$getContactSearchMediator$p == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
                    access$getContactSearchMediator$p = null;
                }
                access$getContactSearchMediator$p.setKeysSelected(SetsKt__SetsJVMKt.setOf(new ContactSearchKey.RecipientSearchKey.Story(recipientId)));
                ContactSearchMediator access$getContactSearchMediator$p2 = TextStoryPostSendFragment.access$getContactSearchMediator$p(this.this$0);
                if (access$getContactSearchMediator$p2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
                } else {
                    contactSearchMediator2 = access$getContactSearchMediator$p2;
                }
                contactSearchMediator2.onFilterChanged("");
            }
        });
        FragmentKt.setFragmentResultListener(this, ChooseGroupStoryBottomSheet.GROUP_STORY, new Function2<String, Bundle, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$onViewCreated$7
            final /* synthetic */ TextStoryPostSendFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((String) obj, (Bundle) obj2);
                return Unit.INSTANCE;
            }

            public final void invoke(String str, Bundle bundle2) {
                Set<RecipientId> set;
                Intrinsics.checkNotNullParameter(str, "<anonymous parameter 0>");
                Intrinsics.checkNotNullParameter(bundle2, "bundle");
                ArrayList parcelableArrayList = bundle2.getParcelableArrayList(ChooseGroupStoryBottomSheet.RESULT_SET);
                if (parcelableArrayList == null || (set = CollectionsKt___CollectionsKt.toSet(parcelableArrayList)) == null) {
                    set = SetsKt__SetsKt.emptySet();
                }
                ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
                for (RecipientId recipientId : set) {
                    arrayList.add(new ContactSearchKey.RecipientSearchKey.Story(recipientId));
                }
                Set<ContactSearchKey.RecipientSearchKey.Story> set2 = CollectionsKt___CollectionsKt.toSet(arrayList);
                ContactSearchMediator access$getContactSearchMediator$p = TextStoryPostSendFragment.access$getContactSearchMediator$p(this.this$0);
                ContactSearchMediator contactSearchMediator2 = null;
                if (access$getContactSearchMediator$p == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
                    access$getContactSearchMediator$p = null;
                }
                access$getContactSearchMediator$p.addToVisibleGroupStories(set2);
                ContactSearchMediator access$getContactSearchMediator$p2 = TextStoryPostSendFragment.access$getContactSearchMediator$p(this.this$0);
                if (access$getContactSearchMediator$p2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
                    access$getContactSearchMediator$p2 = null;
                }
                access$getContactSearchMediator$p2.onFilterChanged("");
                ContactSearchMediator access$getContactSearchMediator$p3 = TextStoryPostSendFragment.access$getContactSearchMediator$p(this.this$0);
                if (access$getContactSearchMediator$p3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
                } else {
                    contactSearchMediator2 = access$getContactSearchMediator$p3;
                }
                contactSearchMediator2.setKeysSelected(set2);
            }
        });
        View findViewById7 = view.findViewById(R.id.contacts_container);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.contacts_container)");
        SelectionLimits shareSelectionLimit = FeatureFlags.shareSelectionLimit();
        Intrinsics.checkNotNullExpressionValue(shareSelectionLimit, "shareSelectionLimit()");
        ContactSearchMediator contactSearchMediator2 = new ContactSearchMediator(this, (RecyclerView) findViewById7, shareSelectionLimit, true, new Function1<ContactSearchState, ContactSearchConfiguration>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$onViewCreated$8
            final /* synthetic */ TextStoryPostSendFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            public final ContactSearchConfiguration invoke(final ContactSearchState contactSearchState) {
                Intrinsics.checkNotNullParameter(contactSearchState, "contactSearchState");
                ContactSearchConfiguration.Companion companion = ContactSearchConfiguration.Companion;
                final TextStoryPostSendFragment textStoryPostSendFragment = this.this$0;
                return companion.build(new Function1<ContactSearchConfiguration.Builder, Unit>() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$onViewCreated$8.1
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(ContactSearchConfiguration.Builder builder) {
                        invoke(builder);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(ContactSearchConfiguration.Builder builder) {
                        Intrinsics.checkNotNullParameter(builder, "$this$build");
                        builder.setQuery(contactSearchState.getQuery());
                        Set<ContactSearchData.Story> groupStories = contactSearchState.getGroupStories();
                        Stories stories = Stories.INSTANCE;
                        FragmentManager childFragmentManager = textStoryPostSendFragment.getChildFragmentManager();
                        Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                        builder.addSection(new ContactSearchConfiguration.Section.Stories(groupStories, true, stories.getHeaderAction(childFragmentManager), null, 8, null));
                    }
                });
            }
        }, null, 32, null);
        this.contactSearchMediator = contactSearchMediator2;
        contactSearchMediator2.getSelectionState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                TextStoryPostSendFragment.$r8$lambda$sts40g0NbL3FEhTeXYEqk3JHE9w(TextStoryPostSendFragment.this, (Set) obj);
            }
        });
        LiveData<TextStoryPostSendState> state = getViewModel().getState();
        ContactSearchMediator contactSearchMediator3 = this.contactSearchMediator;
        if (contactSearchMediator3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
        } else {
            contactSearchMediator = contactSearchMediator3;
        }
        LiveDataUtil.combineLatest(state, contactSearchMediator.getSelectionState(), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return new Pair((TextStoryPostSendState) obj, (Set) obj2);
            }
        }).observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                TextStoryPostSendFragment.m2272$r8$lambda$860TQoNvZ20pC23_qnzZEvuEw(TextStoryPostSendFragment.this, (Pair) obj);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2273onViewCreated$lambda0(TextStoryPostSendFragment textStoryPostSendFragment, View view) {
        Intrinsics.checkNotNullParameter(textStoryPostSendFragment, "this$0");
        androidx.navigation.fragment.FragmentKt.findNavController(textStoryPostSendFragment).popBackStack();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m2274onViewCreated$lambda1(ImageView imageView, Bitmap bitmap) {
        Intrinsics.checkNotNullParameter(imageView, "$viewPort");
        imageView.setImageBitmap(bitmap);
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m2275onViewCreated$lambda2(TextStoryPostSendFragment textStoryPostSendFragment, View view) {
        Intrinsics.checkNotNullParameter(textStoryPostSendFragment, "this$0");
        textStoryPostSendFragment.getViewModel().onSending();
        textStoryPostSendFragment.send();
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2276onViewCreated$lambda3(TextStoryPostSendFragment textStoryPostSendFragment, List list) {
        Intrinsics.checkNotNullParameter(textStoryPostSendFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(list, "records");
        ContactSearchMediator contactSearchMediator = textStoryPostSendFragment.contactSearchMediator;
        if (contactSearchMediator == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
            contactSearchMediator = null;
        }
        SafetyNumberBottomSheet.Factory forIdentityRecordsAndDestinations = SafetyNumberBottomSheet.forIdentityRecordsAndDestinations(list, CollectionsKt___CollectionsKt.toList(contactSearchMediator.getSelectedContacts()));
        FragmentManager childFragmentManager = textStoryPostSendFragment.getChildFragmentManager();
        Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        forIdentityRecordsAndDestinations.show(childFragmentManager);
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m2277onViewCreated$lambda6(TextStoryPostSendFragment textStoryPostSendFragment, Set set) {
        Intrinsics.checkNotNullParameter(textStoryPostSendFragment, "this$0");
        ShareSelectionAdapter shareSelectionAdapter = textStoryPostSendFragment.shareSelectionAdapter;
        Intrinsics.checkNotNullExpressionValue(set, "selection");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
        Iterator it = set.iterator();
        int i = 0;
        while (true) {
            boolean z = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            ShareContact requireShareContact = ((ContactSearchKey) next).requireShareContact();
            if (i != 0) {
                z = false;
            }
            arrayList.add(new ShareSelectionMappingModel(requireShareContact, z));
            i = i2;
        }
        shareSelectionAdapter.submitList(arrayList);
        if (!set.isEmpty()) {
            textStoryPostSendFragment.animateInSelection();
        } else {
            textStoryPostSendFragment.animateOutSelection();
        }
    }

    /* renamed from: onViewCreated$lambda-7 */
    public static final void m2278onViewCreated$lambda7(TextStoryPostSendFragment textStoryPostSendFragment, Pair pair) {
        int i;
        Intrinsics.checkNotNullParameter(textStoryPostSendFragment, "this$0");
        TextStoryPostSendState textStoryPostSendState = (TextStoryPostSendState) pair.component1();
        Set set = (Set) pair.component2();
        if (textStoryPostSendState == null) {
            i = -1;
        } else {
            i = WhenMappings.$EnumSwitchMapping$0[textStoryPostSendState.ordinal()];
        }
        View view = null;
        if (i == 1) {
            View view2 = textStoryPostSendFragment.shareConfirmButton;
            if (view2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("shareConfirmButton");
            } else {
                view = view2;
            }
            Intrinsics.checkNotNullExpressionValue(set, "selection");
            view.setEnabled(!set.isEmpty());
        } else if (i == 2) {
            View view3 = textStoryPostSendFragment.shareConfirmButton;
            if (view3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("shareConfirmButton");
            } else {
                view = view3;
            }
            view.setEnabled(false);
        } else if (i != 3) {
            Toast.makeText(textStoryPostSendFragment.requireContext(), (int) R.string.TextStoryPostSendFragment__an_unexpected_error_occurred_try_again, 0).show();
            textStoryPostSendFragment.getViewModel().onSendCancelled();
        } else {
            textStoryPostSendFragment.requireActivity().finish();
        }
    }

    private final void send() {
        View view = this.shareConfirmButton;
        ContactSearchMediator contactSearchMediator = null;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("shareConfirmButton");
            view = null;
        }
        view.setEnabled(false);
        TextStoryPostCreationState value = getCreationViewModel().getState().getValue();
        TextStoryPostSendViewModel viewModel = getViewModel();
        ContactSearchMediator contactSearchMediator2 = this.contactSearchMediator;
        if (contactSearchMediator2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
        } else {
            contactSearchMediator = contactSearchMediator2;
        }
        Set<ContactSearchKey> selectedContacts = contactSearchMediator.getSelectedContacts();
        Intrinsics.checkNotNull(value);
        List<LinkPreview> onSendWithErrorUrl = getLinkPreviewViewModel().onSendWithErrorUrl();
        Intrinsics.checkNotNullExpressionValue(onSendWithErrorUrl, "linkPreviewViewModel.onSendWithErrorUrl()");
        viewModel.onSend(selectedContacts, value, (LinkPreview) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) onSendWithErrorUrl)));
    }

    private final void animateInSelection() {
        View view = this.shareListWrapper;
        View view2 = null;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("shareListWrapper");
            view = null;
        }
        view.animate().alpha(1.0f).translationY(0.0f);
        View view3 = this.shareConfirmButton;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("shareConfirmButton");
        } else {
            view2 = view3;
        }
        view2.animate().alpha(1.0f);
    }

    private final void animateOutSelection() {
        View view = this.shareListWrapper;
        View view2 = null;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("shareListWrapper");
            view = null;
        }
        view.animate().alpha(0.0f).translationY(DimensionUnit.DP.toPixels(48.0f));
        View view3 = this.shareConfirmButton;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("shareConfirmButton");
        } else {
            view2 = view3;
        }
        view2.animate().alpha(0.0f);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet.Callback
    public void onNewStoryClicked() {
        new CreateStoryFlowDialogFragment().show(getParentFragmentManager(), CreateStoryWithViewersFragment.REQUEST_KEY);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet.Callback
    public void onGroupStoryClicked() {
        new ChooseGroupStoryBottomSheet().show(getParentFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // org.thoughtcrime.securesms.components.WrapperDialogFragment.WrapperDialogFragmentCallback
    public void onWrapperDialogFragmentDismissed() {
        ContactSearchMediator contactSearchMediator = this.contactSearchMediator;
        if (contactSearchMediator == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
            contactSearchMediator = null;
        }
        contactSearchMediator.refresh();
    }

    @Override // org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment.Callback
    public void onMyStoryConfigured(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        ContactSearchMediator contactSearchMediator = this.contactSearchMediator;
        ContactSearchMediator contactSearchMediator2 = null;
        if (contactSearchMediator == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
            contactSearchMediator = null;
        }
        contactSearchMediator.setKeysSelected(SetsKt__SetsJVMKt.setOf(new ContactSearchKey.RecipientSearchKey.Story(recipientId)));
        ContactSearchMediator contactSearchMediator3 = this.contactSearchMediator;
        if (contactSearchMediator3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
        } else {
            contactSearchMediator2 = contactSearchMediator3;
        }
        contactSearchMediator2.refresh();
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void sendAnywayAfterSafetyNumberChangedInBottomSheet(List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Intrinsics.checkNotNullParameter(list, "destinations");
        send();
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public Void onMessageResentAfterSafetyNumberChangeInBottomSheet() {
        throw new IllegalStateException("Not supported here".toString());
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void onCanceled() {
        getViewModel().onSendCancelled();
    }
}
