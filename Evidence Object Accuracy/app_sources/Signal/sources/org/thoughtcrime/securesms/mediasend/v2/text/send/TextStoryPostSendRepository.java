package org.thoughtcrime.securesms.mediasend.v2.text.send;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.fonts.TextFont;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.keyvalue.StorySend;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mediasend.v2.UntrustedRecords;
import org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationState;
import org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendResult;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.OutgoingSecureMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.Base64;

/* compiled from: TextStoryPostSendRepository.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J.\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0002J,\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00020\nH\u0002¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendRepository;", "", "()V", "performSend", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult;", "contactSearchKey", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "textStoryPostCreationState", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationState;", "linkPreview", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;", "send", "serializeTextStoryState", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryPostSendRepository {

    /* compiled from: TextStoryPostSendRepository.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[TextFont.values().length];
            iArr[TextFont.REGULAR.ordinal()] = 1;
            iArr[TextFont.BOLD.ordinal()] = 2;
            iArr[TextFont.SERIF.ordinal()] = 3;
            iArr[TextFont.SCRIPT.ordinal()] = 4;
            iArr[TextFont.CONDENSED.ordinal()] = 5;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public final Single<TextStoryPostSendResult> send(Set<? extends ContactSearchKey> set, TextStoryPostCreationState textStoryPostCreationState, LinkPreview linkPreview) {
        Intrinsics.checkNotNullParameter(set, "contactSearchKey");
        Intrinsics.checkNotNullParameter(textStoryPostCreationState, "textStoryPostCreationState");
        Single<TextStoryPostSendResult> flatMap = UntrustedRecords.INSTANCE.checkForBadIdentityRecords(CollectionsKt___CollectionsKt.toSet(CollectionsKt___CollectionsJvmKt.filterIsInstance(set, ContactSearchKey.RecipientSearchKey.class))).toSingleDefault(TextStoryPostSendResult.Success.INSTANCE).onErrorReturn(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendRepository$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return TextStoryPostSendRepository.m2282send$lambda0((Throwable) obj);
            }
        }).flatMap(new Function(set, textStoryPostCreationState, linkPreview) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ Set f$1;
            public final /* synthetic */ TextStoryPostCreationState f$2;
            public final /* synthetic */ LinkPreview f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return TextStoryPostSendRepository.m2283send$lambda1(TextStoryPostSendRepository.this, this.f$1, this.f$2, this.f$3, (TextStoryPostSendResult) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMap, "UntrustedRecords\n      .…result)\n        }\n      }");
        return flatMap;
    }

    /* renamed from: send$lambda-0 */
    public static final TextStoryPostSendResult m2282send$lambda0(Throwable th) {
        if (th instanceof UntrustedRecords.UntrustedRecordsException) {
            return new TextStoryPostSendResult.UntrustedRecordsError(((UntrustedRecords.UntrustedRecordsException) th).getUntrustedRecords());
        }
        Log.w(TextStoryPostSendRepositoryKt.TAG, "Unexpected error occurred", th);
        return TextStoryPostSendResult.Failure.INSTANCE;
    }

    /* renamed from: send$lambda-1 */
    public static final SingleSource m2283send$lambda1(TextStoryPostSendRepository textStoryPostSendRepository, Set set, TextStoryPostCreationState textStoryPostCreationState, LinkPreview linkPreview, TextStoryPostSendResult textStoryPostSendResult) {
        Intrinsics.checkNotNullParameter(textStoryPostSendRepository, "this$0");
        Intrinsics.checkNotNullParameter(set, "$contactSearchKey");
        Intrinsics.checkNotNullParameter(textStoryPostCreationState, "$textStoryPostCreationState");
        if (textStoryPostSendResult instanceof TextStoryPostSendResult.Success) {
            return textStoryPostSendRepository.performSend(set, textStoryPostCreationState, linkPreview);
        }
        return Single.just(textStoryPostSendResult);
    }

    private final Single<TextStoryPostSendResult> performSend(Set<? extends ContactSearchKey> set, TextStoryPostCreationState textStoryPostCreationState, LinkPreview linkPreview) {
        Single<TextStoryPostSendResult> flatMap = Single.fromCallable(new Callable(set, this, textStoryPostCreationState, linkPreview) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Set f$0;
            public final /* synthetic */ TextStoryPostSendRepository f$1;
            public final /* synthetic */ TextStoryPostCreationState f$2;
            public final /* synthetic */ LinkPreview f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return TextStoryPostSendRepository.m2280performSend$lambda2(this.f$0, this.f$1, this.f$2, this.f$3);
            }
        }).flatMap(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return TextStoryPostSendRepository.m2281performSend$lambda3((Completable) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMap, "fromCallable {\n      val…SendResult.Success)\n    }");
        return flatMap;
    }

    /* renamed from: performSend$lambda-2 */
    public static final Completable m2280performSend$lambda2(Set set, TextStoryPostSendRepository textStoryPostSendRepository, TextStoryPostCreationState textStoryPostCreationState, LinkPreview linkPreview) {
        StoryType storyType;
        Intrinsics.checkNotNullParameter(set, "$contactSearchKey");
        Intrinsics.checkNotNullParameter(textStoryPostSendRepository, "this$0");
        Intrinsics.checkNotNullParameter(textStoryPostCreationState, "$textStoryPostCreationState");
        ArrayList arrayList = new ArrayList();
        long currentTimeMillis = System.currentTimeMillis();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            ContactSearchKey contactSearchKey = (ContactSearchKey) it.next();
            Recipient resolved = Recipient.resolved(contactSearchKey.requireShareContact().getRecipientId().get());
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(contact.require…tact().recipientId.get())");
            boolean z = (contactSearchKey instanceof ContactSearchKey.RecipientSearchKey.Story) || resolved.isDistributionList();
            if (z && resolved.isActiveGroup() && resolved.isGroup()) {
                SignalDatabase.Companion.groups().markDisplayAsStory(resolved.requireGroupId());
            }
            if (z && !resolved.isMyStory()) {
                SignalStore.storyValues().setLatestStorySend(StorySend.Companion.newSend(resolved));
            }
            if (resolved.isDistributionList()) {
                DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
                DistributionListId requireDistributionListId = resolved.requireDistributionListId();
                Intrinsics.checkNotNullExpressionValue(requireDistributionListId, "recipient.requireDistributionListId()");
                storyType = distributionLists.getStoryType(requireDistributionListId);
            } else if (z) {
                storyType = StoryType.STORY_WITH_REPLIES;
            } else {
                storyType = StoryType.NONE;
            }
            arrayList.add(new OutgoingSecureMediaMessage(new OutgoingMediaMessage(resolved, textStoryPostSendRepository.serializeTextStoryState(textStoryPostCreationState), CollectionsKt__CollectionsKt.emptyList(), resolved.isDistributionList() ? currentTimeMillis : System.currentTimeMillis(), -1, 0, false, 2, storyType.toTextStoryType(), null, false, null, CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.listOfNotNull(linkPreview), CollectionsKt__CollectionsKt.emptyList(), new LinkedHashSet(), new LinkedHashSet(), null)));
            ThreadUtil.sleep(5);
        }
        return Stories.INSTANCE.sendTextStories(arrayList);
    }

    /* renamed from: performSend$lambda-3 */
    public static final SingleSource m2281performSend$lambda3(Completable completable) {
        return completable.toSingleDefault(TextStoryPostSendResult.Success.INSTANCE);
    }

    private final String serializeTextStoryState(TextStoryPostCreationState textStoryPostCreationState) {
        StoryTextPost.Style style;
        StoryTextPost.Builder newBuilder = StoryTextPost.newBuilder();
        newBuilder.setBody(textStoryPostCreationState.getBody().toString());
        newBuilder.setBackground(textStoryPostCreationState.getBackgroundColor().serialize());
        int i = WhenMappings.$EnumSwitchMapping$0[textStoryPostCreationState.getTextFont().ordinal()];
        if (i == 1) {
            style = StoryTextPost.Style.REGULAR;
        } else if (i == 2) {
            style = StoryTextPost.Style.BOLD;
        } else if (i == 3) {
            style = StoryTextPost.Style.SERIF;
        } else if (i == 4) {
            style = StoryTextPost.Style.SCRIPT;
        } else if (i == 5) {
            style = StoryTextPost.Style.CONDENSED;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        newBuilder.setStyle(style);
        newBuilder.setTextBackgroundColor(textStoryPostCreationState.getTextBackgroundColor());
        newBuilder.setTextForegroundColor(textStoryPostCreationState.getTextForegroundColor());
        String encodeBytes = Base64.encodeBytes(newBuilder.build().toByteArray());
        Intrinsics.checkNotNullExpressionValue(encodeBytes, "encodeBytes(builder.build().toByteArray())");
        return encodeBytes;
    }
}
