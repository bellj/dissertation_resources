package org.thoughtcrime.securesms.mediasend;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.util.ParcelUtil;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public class MediaSendActivityResult implements Parcelable {
    public static final Parcelable.Creator<MediaSendActivityResult> CREATOR = new Parcelable.Creator<MediaSendActivityResult>() { // from class: org.thoughtcrime.securesms.mediasend.MediaSendActivityResult.1
        @Override // android.os.Parcelable.Creator
        public MediaSendActivityResult createFromParcel(Parcel parcel) {
            return new MediaSendActivityResult(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public MediaSendActivityResult[] newArray(int i) {
            return new MediaSendActivityResult[i];
        }
    };
    public static final String EXTRA_RESULT;
    private final String body;
    private final Collection<Mention> mentions;
    private final Collection<Media> nonUploadedMedia;
    private final RecipientId recipientId;
    private final MessageSendType sendType;
    private final StoryType storyType;
    private final Collection<MessageSender.PreUploadResult> uploadResults;
    private final boolean viewOnce;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public static MediaSendActivityResult fromData(Intent intent) {
        MediaSendActivityResult mediaSendActivityResult = (MediaSendActivityResult) intent.getParcelableExtra(EXTRA_RESULT);
        if (mediaSendActivityResult != null) {
            return mediaSendActivityResult;
        }
        throw new IllegalArgumentException();
    }

    public static MediaSendActivityResult forPreUpload(RecipientId recipientId, Collection<MessageSender.PreUploadResult> collection, String str, MessageSendType messageSendType, boolean z, List<Mention> list, StoryType storyType) {
        Preconditions.checkArgument(collection.size() > 0, "Must supply uploadResults!");
        return new MediaSendActivityResult(recipientId, collection, Collections.emptyList(), str, messageSendType, z, list, storyType);
    }

    public static MediaSendActivityResult forTraditionalSend(RecipientId recipientId, List<Media> list, String str, MessageSendType messageSendType, boolean z, List<Mention> list2, StoryType storyType) {
        Preconditions.checkArgument(list.size() > 0, "Must supply media!");
        return new MediaSendActivityResult(recipientId, Collections.emptyList(), list, str, messageSendType, z, list2, storyType);
    }

    private MediaSendActivityResult(RecipientId recipientId, Collection<MessageSender.PreUploadResult> collection, List<Media> list, String str, MessageSendType messageSendType, boolean z, List<Mention> list2, StoryType storyType) {
        this.recipientId = recipientId;
        this.uploadResults = collection;
        this.nonUploadedMedia = list;
        this.body = str;
        this.sendType = messageSendType;
        this.viewOnce = z;
        this.mentions = list2;
        this.storyType = storyType;
    }

    private MediaSendActivityResult(Parcel parcel) {
        this.recipientId = (RecipientId) parcel.readParcelable(RecipientId.class.getClassLoader());
        this.uploadResults = ParcelUtil.readParcelableCollection(parcel, MessageSender.PreUploadResult.class);
        this.nonUploadedMedia = ParcelUtil.readParcelableCollection(parcel, Media.class);
        this.body = parcel.readString();
        this.sendType = (MessageSendType) parcel.readParcelable(MessageSendType.class.getClassLoader());
        this.viewOnce = ParcelUtil.readBoolean(parcel);
        this.mentions = ParcelUtil.readParcelableCollection(parcel, Mention.class);
        this.storyType = StoryType.fromCode(parcel.readInt());
    }

    public RecipientId getRecipientId() {
        return this.recipientId;
    }

    public boolean isPushPreUpload() {
        return this.uploadResults.size() > 0;
    }

    public Collection<MessageSender.PreUploadResult> getPreUploadResults() {
        return this.uploadResults;
    }

    public Collection<Media> getNonUploadedMedia() {
        return this.nonUploadedMedia;
    }

    public String getBody() {
        return this.body;
    }

    public MessageSendType getMessageSendType() {
        return this.sendType;
    }

    public boolean isViewOnce() {
        return this.viewOnce;
    }

    public Collection<Mention> getMentions() {
        return this.mentions;
    }

    public StoryType getStoryType() {
        return this.storyType;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.recipientId, 0);
        ParcelUtil.writeParcelableCollection(parcel, this.uploadResults);
        ParcelUtil.writeParcelableCollection(parcel, this.nonUploadedMedia);
        parcel.writeString(this.body);
        parcel.writeParcelable(this.sendType, 0);
        ParcelUtil.writeBoolean(parcel, this.viewOnce);
        ParcelUtil.writeParcelableCollection(parcel, this.mentions);
        parcel.writeInt(this.storyType.getCode());
    }
}
