package org.thoughtcrime.securesms.mediasend.v2.capture;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.mediasend.Media;

/* compiled from: MediaCaptureEvent.kt */
@Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureEvent;", "", "()V", "MediaCaptureRenderFailed", "MediaCaptureRendered", "Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureEvent$MediaCaptureRendered;", "Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureEvent$MediaCaptureRenderFailed;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class MediaCaptureEvent {
    public /* synthetic */ MediaCaptureEvent(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    /* compiled from: MediaCaptureEvent.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureEvent$MediaCaptureRendered;", "Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureEvent;", "media", "Lorg/thoughtcrime/securesms/mediasend/Media;", "(Lorg/thoughtcrime/securesms/mediasend/Media;)V", "getMedia", "()Lorg/thoughtcrime/securesms/mediasend/Media;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MediaCaptureRendered extends MediaCaptureEvent {
        private final Media media;

        public static /* synthetic */ MediaCaptureRendered copy$default(MediaCaptureRendered mediaCaptureRendered, Media media, int i, Object obj) {
            if ((i & 1) != 0) {
                media = mediaCaptureRendered.media;
            }
            return mediaCaptureRendered.copy(media);
        }

        public final Media component1() {
            return this.media;
        }

        public final MediaCaptureRendered copy(Media media) {
            Intrinsics.checkNotNullParameter(media, "media");
            return new MediaCaptureRendered(media);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof MediaCaptureRendered) && Intrinsics.areEqual(this.media, ((MediaCaptureRendered) obj).media);
        }

        public int hashCode() {
            return this.media.hashCode();
        }

        public String toString() {
            return "MediaCaptureRendered(media=" + this.media + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public MediaCaptureRendered(Media media) {
            super(null);
            Intrinsics.checkNotNullParameter(media, "media");
            this.media = media;
        }

        public final Media getMedia() {
            return this.media;
        }
    }

    private MediaCaptureEvent() {
    }

    /* compiled from: MediaCaptureEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureEvent$MediaCaptureRenderFailed;", "Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MediaCaptureRenderFailed extends MediaCaptureEvent {
        public static final MediaCaptureRenderFailed INSTANCE = new MediaCaptureRenderFailed();

        private MediaCaptureRenderFailed() {
            super(null);
        }
    }
}
