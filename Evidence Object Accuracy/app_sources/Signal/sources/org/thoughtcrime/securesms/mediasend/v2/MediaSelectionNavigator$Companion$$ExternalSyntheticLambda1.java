package org.thoughtcrime.securesms.mediasend.v2;

import androidx.fragment.app.Fragment;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionNavigator;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaSelectionNavigator$Companion$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ Fragment f$0;

    public /* synthetic */ MediaSelectionNavigator$Companion$$ExternalSyntheticLambda1(Fragment fragment) {
        this.f$0 = fragment;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MediaSelectionNavigator.Companion.m2103requestPermissionsForGallery$lambda3(this.f$0);
    }
}
