package org.thoughtcrime.securesms.mediasend.v2.gallery;

import android.content.Context;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.MediaFolder;
import org.thoughtcrime.securesms.mediasend.MediaRepository;

/* compiled from: MediaGalleryRepository.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J \u0010\u0007\u001a\u00020\b2\u0018\u0010\t\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0004\u0012\u00020\b0\nJ(\u0010\r\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\u000f2\u0018\u0010\u0010\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u000b\u0012\u0004\u0012\u00020\b0\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryRepository;", "", "context", "Landroid/content/Context;", "mediaRepository", "Lorg/thoughtcrime/securesms/mediasend/MediaRepository;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/mediasend/MediaRepository;)V", "getFolders", "", "onFoldersRetrieved", "Lkotlin/Function1;", "", "Lorg/thoughtcrime/securesms/mediasend/MediaFolder;", "getMedia", "bucketId", "", "onMediaRetrieved", "Lorg/thoughtcrime/securesms/mediasend/Media;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGalleryRepository {
    private final Context context;
    private final MediaRepository mediaRepository;

    public MediaGalleryRepository(Context context, MediaRepository mediaRepository) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(mediaRepository, "mediaRepository");
        this.mediaRepository = mediaRepository;
        Context applicationContext = context.getApplicationContext();
        Intrinsics.checkNotNullExpressionValue(applicationContext, "context.applicationContext");
        this.context = applicationContext;
    }

    /* renamed from: getFolders$lambda-0 */
    public static final void m2167getFolders$lambda0(Function1 function1, List list) {
        Intrinsics.checkNotNullParameter(function1, "$onFoldersRetrieved");
        Intrinsics.checkNotNullParameter(list, "it");
        function1.invoke(list);
    }

    public final void getFolders(Function1<? super List<? extends MediaFolder>, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "onFoldersRetrieved");
        this.mediaRepository.getFolders(this.context, new MediaRepository.Callback() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryRepository$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.mediasend.MediaRepository.Callback
            public final void onComplete(Object obj) {
                MediaGalleryRepository.m2166$r8$lambda$p3si2yLHGET2vHA63Y01r4b3Co(Function1.this, (List) obj);
            }
        });
    }

    /* renamed from: getMedia$lambda-1 */
    public static final void m2168getMedia$lambda1(Function1 function1, List list) {
        Intrinsics.checkNotNullParameter(function1, "$onMediaRetrieved");
        Intrinsics.checkNotNullParameter(list, "it");
        function1.invoke(list);
    }

    public final void getMedia(String str, Function1<? super List<? extends Media>, Unit> function1) {
        Intrinsics.checkNotNullParameter(str, "bucketId");
        Intrinsics.checkNotNullParameter(function1, "onMediaRetrieved");
        this.mediaRepository.getMediaInBucket(this.context, str, new MediaRepository.Callback() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryRepository$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.mediasend.MediaRepository.Callback
            public final void onComplete(Object obj) {
                MediaGalleryRepository.m2165$r8$lambda$p1F3M8JDmR8T6_F5KrCbQFqtSM(Function1.this, (List) obj);
            }
        });
    }
}
