package org.thoughtcrime.securesms.mediasend;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.FromTextView;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.adapter.SectionedRecyclerViewAdapter;
import org.thoughtcrime.securesms.util.adapter.StableIdGenerator;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class CameraContactAdapter extends SectionedRecyclerViewAdapter<String, ContactSection> {
    private static final long ID_INVITE;
    private static final String TAG_ALL;
    private static final String TAG_GROUPS;
    private static final String TAG_RECENT;
    private static final int TYPE_INVITE;
    private final CameraContactListener cameraContactListener;
    private final GlideRequests glideRequests;
    private final List<ContactSection> sections = new ArrayList<ContactSection>(3) { // from class: org.thoughtcrime.securesms.mediasend.CameraContactAdapter.1
        {
            ContactSection contactSection = new ContactSection(CameraContactAdapter.TAG_RECENT, R.string.CameraContacts_recent_contacts, Collections.emptyList(), 0);
            ContactSection contactSection2 = new ContactSection(CameraContactAdapter.TAG_ALL, R.string.CameraContacts_signal_contacts, Collections.emptyList(), contactSection.size());
            ContactSection contactSection3 = new ContactSection("groups", R.string.CameraContacts_signal_groups, Collections.emptyList(), contactSection.size() + contactSection2.size());
            add(contactSection);
            add(contactSection2);
            add(contactSection3);
        }
    };
    private final Set<Recipient> selected;

    /* loaded from: classes4.dex */
    public interface CameraContactListener {
        void onContactClicked(Recipient recipient);

        void onInviteContactsClicked();
    }

    protected RecyclerView.ViewHolder createEmptyViewHolder(ViewGroup viewGroup) {
        return null;
    }

    public CameraContactAdapter(GlideRequests glideRequests, CameraContactListener cameraContactListener) {
        this.glideRequests = glideRequests;
        this.selected = new HashSet();
        this.cameraContactListener = cameraContactListener;
    }

    protected List<ContactSection> getSections() {
        return this.sections;
    }

    public long getItemId(int i) {
        if (isInvitePosition(i)) {
            return ID_INVITE;
        }
        return super.getItemId(i);
    }

    public int getItemViewType(int i) {
        if (isInvitePosition(i)) {
            return TYPE_INVITE;
        }
        return super.getItemViewType(i);
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == TYPE_INVITE) {
            return new InviteViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.camera_contact_invite_item, viewGroup, false));
        }
        return super.onCreateViewHolder(viewGroup, i);
    }

    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup viewGroup) {
        return new HeaderViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.camera_contact_header_item, viewGroup, false));
    }

    protected RecyclerView.ViewHolder createContentViewHolder(ViewGroup viewGroup) {
        return new ContactViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.camera_contact_contact_item, viewGroup, false));
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (isInvitePosition(i)) {
            ((InviteViewHolder) viewHolder).bind(this.cameraContactListener);
        } else {
            super.onBindViewHolder(viewHolder, i);
        }
    }

    public void bindViewHolder(RecyclerView.ViewHolder viewHolder, ContactSection contactSection, int i) {
        contactSection.bind(viewHolder, i, this.selected, this.glideRequests, this.cameraContactListener);
    }

    public int getItemCount() {
        return super.getItemCount() + 1;
    }

    public void setContacts(CameraContacts cameraContacts, Collection<Recipient> collection) {
        ContactSection contactSection = new ContactSection(TAG_RECENT, R.string.CameraContacts_recent_contacts, cameraContacts.getRecents(), 0);
        ContactSection contactSection2 = new ContactSection(TAG_ALL, R.string.CameraContacts_signal_contacts, cameraContacts.getContacts(), contactSection.size());
        ContactSection contactSection3 = new ContactSection("groups", R.string.CameraContacts_signal_groups, cameraContacts.getGroups(), contactSection.size() + contactSection2.size());
        this.sections.clear();
        this.sections.add(contactSection);
        this.sections.add(contactSection2);
        this.sections.add(contactSection3);
        this.selected.clear();
        this.selected.addAll(collection);
        notifyDataSetChanged();
    }

    private boolean isInvitePosition(int i) {
        return i == getItemCount() - 1;
    }

    /* loaded from: classes4.dex */
    public static class ContactSection extends SectionedRecyclerViewAdapter.Section<String> {
        private final List<Recipient> recipients;
        private final String tag;
        private final int titleResId;

        public boolean hasEmptyState() {
            return false;
        }

        public ContactSection(String str, int i, List<Recipient> list, int i2) {
            super(i2);
            this.tag = str;
            this.titleResId = i;
            this.recipients = list;
        }

        public int getContentSize() {
            return this.recipients.size();
        }

        public long getItemId(StableIdGenerator<String> stableIdGenerator, int i) {
            int localPosition = getLocalPosition(i);
            if (localPosition == 0) {
                return stableIdGenerator.getId(this.tag);
            }
            return stableIdGenerator.getId(this.recipients.get(localPosition - 1).getId().serialize());
        }

        void bind(RecyclerView.ViewHolder viewHolder, int i, Set<Recipient> set, GlideRequests glideRequests, CameraContactListener cameraContactListener) {
            if (i == 0) {
                ((HeaderViewHolder) viewHolder).bind(this.titleResId);
                return;
            }
            Recipient recipient = this.recipients.get(i - 1);
            ((ContactViewHolder) viewHolder).bind(recipient, set.contains(recipient), glideRequests, cameraContactListener);
        }
    }

    /* loaded from: classes4.dex */
    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private final TextView title;

        HeaderViewHolder(View view) {
            super(view);
            this.title = (TextView) view.findViewById(R.id.camera_contact_header);
        }

        void bind(int i) {
            this.title.setText(i);
        }
    }

    /* loaded from: classes4.dex */
    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        private final AvatarImageView avatar;
        private final CheckBox checkbox;
        private final FromTextView name;

        ContactViewHolder(View view) {
            super(view);
            this.avatar = (AvatarImageView) view.findViewById(R.id.camera_contact_item_avatar);
            this.name = (FromTextView) view.findViewById(R.id.camera_contact_item_name);
            this.checkbox = (CheckBox) view.findViewById(R.id.camera_contact_item_checkbox);
        }

        void bind(Recipient recipient, boolean z, GlideRequests glideRequests, CameraContactListener cameraContactListener) {
            this.avatar.setAvatar(glideRequests, recipient, false);
            this.name.setText(recipient);
            this.itemView.setOnClickListener(new CameraContactAdapter$ContactViewHolder$$ExternalSyntheticLambda0(cameraContactListener, recipient));
            this.checkbox.setChecked(z);
        }
    }

    /* loaded from: classes4.dex */
    public static class InviteViewHolder extends RecyclerView.ViewHolder {
        private final View inviteButton;

        public InviteViewHolder(View view) {
            super(view);
            this.inviteButton = view.findViewById(R.id.camera_contact_invite);
        }

        void bind(CameraContactListener cameraContactListener) {
            this.inviteButton.setOnClickListener(new CameraContactAdapter$InviteViewHolder$$ExternalSyntheticLambda0(cameraContactListener));
        }
    }
}
