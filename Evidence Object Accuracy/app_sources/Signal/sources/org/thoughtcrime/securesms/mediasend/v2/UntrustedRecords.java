package org.thoughtcrime.securesms.mediasend.v2;

import androidx.core.util.Consumer;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: UntrustedRecords.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u000eB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006J(\u0010\u0003\u001a\u00020\b2\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0012\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nJ\u001c\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0003¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/UntrustedRecords;", "", "()V", "checkForBadIdentityRecords", "Lio/reactivex/rxjava3/core/Completable;", "contactSearchKeys", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "", "consumer", "Landroidx/core/util/Consumer;", "", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "checkForBadIdentityRecordsSync", "UntrustedRecordsException", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class UntrustedRecords {
    public static final UntrustedRecords INSTANCE = new UntrustedRecords();

    private UntrustedRecords() {
    }

    public final Completable checkForBadIdentityRecords(Set<? extends ContactSearchKey.RecipientSearchKey> set) {
        Intrinsics.checkNotNullParameter(set, "contactSearchKeys");
        Completable subscribeOn = Completable.fromAction(new Action(set) { // from class: org.thoughtcrime.securesms.mediasend.v2.UntrustedRecords$$ExternalSyntheticLambda1
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                UntrustedRecords.m2135checkForBadIdentityRecords$lambda0(this.f$0);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      val u…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: checkForBadIdentityRecords$lambda-0 */
    public static final void m2135checkForBadIdentityRecords$lambda0(Set set) {
        Intrinsics.checkNotNullParameter(set, "$contactSearchKeys");
        List<IdentityRecord> checkForBadIdentityRecordsSync = INSTANCE.checkForBadIdentityRecordsSync(set);
        if (!checkForBadIdentityRecordsSync.isEmpty()) {
            throw new UntrustedRecordsException(checkForBadIdentityRecordsSync, set);
        }
    }

    public final void checkForBadIdentityRecords(Set<? extends ContactSearchKey.RecipientSearchKey> set, Consumer<List<IdentityRecord>> consumer) {
        Intrinsics.checkNotNullParameter(set, "contactSearchKeys");
        Intrinsics.checkNotNullParameter(consumer, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(set) { // from class: org.thoughtcrime.securesms.mediasend.v2.UntrustedRecords$$ExternalSyntheticLambda0
            public final /* synthetic */ Set f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                UntrustedRecords.m2136checkForBadIdentityRecords$lambda1(Consumer.this, this.f$1);
            }
        });
    }

    /* renamed from: checkForBadIdentityRecords$lambda-1 */
    public static final void m2136checkForBadIdentityRecords$lambda1(Consumer consumer, Set set) {
        Intrinsics.checkNotNullParameter(consumer, "$consumer");
        Intrinsics.checkNotNullParameter(set, "$contactSearchKeys");
        consumer.accept(INSTANCE.checkForBadIdentityRecordsSync(set));
    }

    /* compiled from: UntrustedRecords.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B!\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/UntrustedRecords$UntrustedRecordsException;", "", "untrustedRecords", "", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "destinations", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "(Ljava/util/List;Ljava/util/Set;)V", "getDestinations", "()Ljava/util/Set;", "getUntrustedRecords", "()Ljava/util/List;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class UntrustedRecordsException extends Throwable {
        private final Set<ContactSearchKey.RecipientSearchKey> destinations;
        private final List<IdentityRecord> untrustedRecords;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchKey$RecipientSearchKey> */
        /* JADX WARN: Multi-variable type inference failed */
        public UntrustedRecordsException(List<IdentityRecord> list, Set<? extends ContactSearchKey.RecipientSearchKey> set) {
            Intrinsics.checkNotNullParameter(list, "untrustedRecords");
            Intrinsics.checkNotNullParameter(set, "destinations");
            this.untrustedRecords = list;
            this.destinations = set;
        }

        public final Set<ContactSearchKey.RecipientSearchKey> getDestinations() {
            return this.destinations;
        }

        public final List<IdentityRecord> getUntrustedRecords() {
            return this.untrustedRecords;
        }
    }

    private final List<IdentityRecord> checkForBadIdentityRecordsSync(Set<? extends ContactSearchKey.RecipientSearchKey> set) {
        List<Recipient> list;
        ArrayList<Recipient> arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
        for (ContactSearchKey.RecipientSearchKey recipientSearchKey : set) {
            arrayList.add(Recipient.resolved(recipientSearchKey.getRecipientId()));
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        for (Recipient recipient : arrayList) {
            if (recipient.isGroup()) {
                list = Recipient.resolvedList(recipient.getParticipantIds());
                Intrinsics.checkNotNullExpressionValue(list, "resolvedList(recipient.participantIds)");
            } else if (recipient.isDistributionList()) {
                DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
                DistributionListId distributionListId = recipient.getDistributionListId().get();
                Intrinsics.checkNotNullExpressionValue(distributionListId, "recipient.distributionListId.get()");
                list = Recipient.resolvedList(distributionLists.getMembers(distributionListId));
                Intrinsics.checkNotNullExpressionValue(list, "resolvedList(SignalDatab…istributionListId.get()))");
            } else {
                list = CollectionsKt__CollectionsJVMKt.listOf(recipient);
            }
            arrayList2.add(list);
        }
        List<IdentityRecord> untrustedRecords = ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecords(CollectionsKt__IterablesKt.flatten(arrayList2)).getUntrustedRecords();
        Intrinsics.checkNotNullExpressionValue(untrustedRecords, "getProtocolStore().aci()…ipients).untrustedRecords");
        return untrustedRecords;
    }
}
