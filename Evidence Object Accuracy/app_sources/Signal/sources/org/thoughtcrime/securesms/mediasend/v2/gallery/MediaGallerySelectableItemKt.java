package org.thoughtcrime.securesms.mediasend.v2.gallery;

import kotlin.Metadata;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectableItem;

/* compiled from: MediaGallerySelectableItem.kt */
@Metadata(d1 = {"\u0000.\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\"\u0016\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001X\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000*.\u0010\u0006\"\u0014\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u00072\u0014\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0007*\"\u0010\u000b\"\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\n0\f2\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\n0\f¨\u0006\u000e"}, d2 = {"FILE_VIEW_HOLDER_TAG", "", "kotlin.jvm.PlatformType", "PAYLOAD_CHECK_CHANGED", "", "PAYLOAD_INDEX_CHANGED", "OnMediaClicked", "Lkotlin/Function2;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "", "", "OnMediaFolderClicked", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/mediasend/MediaFolder;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGallerySelectableItemKt {
    private static final String FILE_VIEW_HOLDER_TAG = Log.tag(MediaGallerySelectableItem.FileViewHolder.class);
    private static final int PAYLOAD_CHECK_CHANGED;
    private static final int PAYLOAD_INDEX_CHANGED;
}
