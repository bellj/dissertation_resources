package org.thoughtcrime.securesms.mediasend.v2.capture;

import kotlin.Metadata;
import org.signal.core.util.logging.Log;

/* compiled from: MediaCaptureFragment.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\"\u0016\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0003"}, d2 = {"TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaCaptureFragmentKt {
    private static final String TAG = Log.tag(MediaCaptureFragment.class);
}
