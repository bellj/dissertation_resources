package org.thoughtcrime.securesms.mediasend;

import android.content.Context;

/* loaded from: classes4.dex */
public interface MediaTransform {
    Media transform(Context context, Media media);
}
