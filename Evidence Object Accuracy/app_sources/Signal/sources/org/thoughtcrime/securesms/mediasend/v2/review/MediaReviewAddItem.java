package org.thoughtcrime.securesms.mediasend.v2.review;

import android.view.View;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: MediaReviewAddItem.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0010\u0010\u0007\u001a\f\u0012\u0004\u0012\u00020\u00040\bj\u0002`\t¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewAddItem;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "onAddMediaItemClicked", "Lkotlin/Function0;", "Lorg/thoughtcrime/securesms/mediasend/v2/review/OnAddMediaItemClicked;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaReviewAddItem {
    public static final MediaReviewAddItem INSTANCE = new MediaReviewAddItem();

    private MediaReviewAddItem() {
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m2201register$lambda0(Function0 function0, View view) {
        Intrinsics.checkNotNullParameter(function0, "$onAddMediaItemClicked");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view, function0);
    }

    public final void register(MappingAdapter mappingAdapter, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        Intrinsics.checkNotNullParameter(function0, "onAddMediaItemClicked");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewAddItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MediaReviewAddItem.m2200$r8$lambda$dvqdBqZBgtWcHgkDpVbRaqsDiA(Function0.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.v2_media_review_add_media_item));
    }

    /* compiled from: MediaReviewAddItem.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016J\u0010\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewAddItem$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "()V", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model implements MappingModel<Model> {
        public static final Model INSTANCE = new Model();

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return true;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return true;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        private Model() {
        }
    }

    /* compiled from: MediaReviewAddItem.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001f\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0010\u0010\u0005\u001a\f\u0012\u0004\u0012\u00020\u00070\u0006j\u0002`\b¢\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u0002H\u0016¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewAddItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewAddItem$Model;", "itemView", "Landroid/view/View;", "onAddMediaItemClicked", "Lkotlin/Function0;", "", "Lorg/thoughtcrime/securesms/mediasend/v2/review/OnAddMediaItemClicked;", "(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view, Function0<Unit> function0) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function0, "onAddMediaItemClicked");
            view.setOnClickListener(new MediaReviewAddItem$ViewHolder$$ExternalSyntheticLambda0(function0));
        }

        /* renamed from: _init_$lambda-0 */
        public static final void m2202_init_$lambda0(Function0 function0, View view) {
            Intrinsics.checkNotNullParameter(function0, "$onAddMediaItemClicked");
            function0.invoke();
        }
    }
}
