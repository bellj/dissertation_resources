package org.thoughtcrime.securesms.mediasend.v2.stories;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;
import android.widget.FrameLayout;
import androidx.fragment.app.FragmentKt;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sharing.ShareContact;
import org.thoughtcrime.securesms.sharing.ShareSelectionAdapter;
import org.thoughtcrime.securesms.sharing.ShareSelectionMappingModel;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* compiled from: ChooseGroupStoryBottomSheet.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\r\u001a\u00020\u000eH\u0002J\b\u0010\u000f\u001a\u00020\u000eH\u0002J&\u0010\u0010\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\b\u0010\u0017\u001a\u00020\u000eH\u0016J\b\u0010\u0018\u001a\u00020\u000eH\u0002J\u001a\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u00062\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/stories/ChooseGroupStoryBottomSheet;", "Lorg/thoughtcrime/securesms/components/FixedRoundedCornerBottomSheetDialogFragment;", "()V", "animatorSet", "Landroid/animation/AnimatorSet;", "backgroundHelper", "Landroid/view/View;", "confirmButton", "divider", "mediator", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchMediator;", "selectedList", "Landroidx/recyclerview/widget/RecyclerView;", "animateInBottomBar", "", "animateOutBottomBar", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "onDone", "onViewCreated", "view", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChooseGroupStoryBottomSheet extends FixedRoundedCornerBottomSheetDialogFragment {
    public static final Companion Companion = new Companion(null);
    public static final String GROUP_STORY;
    public static final String RESULT_SET;
    private AnimatorSet animatorSet;
    private View backgroundHelper;
    private View confirmButton;
    private View divider;
    private ContactSearchMediator mediator;
    private RecyclerView selectedList;

    /* compiled from: ChooseGroupStoryBottomSheet.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/stories/ChooseGroupStoryBottomSheet$Companion;", "", "()V", "GROUP_STORY", "", "RESULT_SET", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.cloneInContext(new ContextThemeWrapper(layoutInflater.getContext(), getThemeResId())).inflate(R.layout.stories_choose_group_bottom_sheet, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        view.setMinimumHeight(getResources().getDisplayMetrics().heightPixels);
        ViewParent parent = view.getParent().getParent().getParent();
        if (parent != null) {
            View inflate = LayoutInflater.from(requireContext()).inflate(R.layout.stories_choose_group_bottom_bar, (ViewGroup) ((FrameLayout) parent), true);
            View findViewById = inflate.findViewById(R.id.share_confirm);
            Intrinsics.checkNotNullExpressionValue(findViewById, "bottomBar.findViewById(R.id.share_confirm)");
            this.confirmButton = findViewById;
            View findViewById2 = inflate.findViewById(R.id.selected_list);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "bottomBar.findViewById(R.id.selected_list)");
            this.selectedList = (RecyclerView) findViewById2;
            View findViewById3 = inflate.findViewById(R.id.background_helper);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "bottomBar.findViewById(R.id.background_helper)");
            this.backgroundHelper = findViewById3;
            View findViewById4 = inflate.findViewById(R.id.divider);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "bottomBar.findViewById(R.id.divider)");
            this.divider = findViewById4;
            ShareSelectionAdapter shareSelectionAdapter = new ShareSelectionAdapter();
            RecyclerView recyclerView = this.selectedList;
            View view2 = null;
            if (recyclerView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("selectedList");
                recyclerView = null;
            }
            recyclerView.setAdapter(shareSelectionAdapter);
            View view3 = this.confirmButton;
            if (view3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("confirmButton");
            } else {
                view2 = view3;
            }
            view2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet$$ExternalSyntheticLambda0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view4) {
                    ChooseGroupStoryBottomSheet.m2227onViewCreated$lambda0(ChooseGroupStoryBottomSheet.this, view4);
                }
            });
            View findViewById5 = view.findViewById(R.id.contact_recycler);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.contact_recycler)");
            SelectionLimits shareSelectionLimit = FeatureFlags.shareSelectionLimit();
            Intrinsics.checkNotNullExpressionValue(shareSelectionLimit, "shareSelectionLimit()");
            ContactSearchMediator contactSearchMediator = new ContactSearchMediator(this, (RecyclerView) findViewById5, shareSelectionLimit, true, ChooseGroupStoryBottomSheet$onViewCreated$2.INSTANCE, null, 32, null);
            this.mediator = contactSearchMediator;
            contactSearchMediator.getSelectionState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet$$ExternalSyntheticLambda1
                public final /* synthetic */ ChooseGroupStoryBottomSheet f$1;

                {
                    this.f$1 = r2;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    ChooseGroupStoryBottomSheet.m2228onViewCreated$lambda3(ShareSelectionAdapter.this, this.f$1, (Set) obj);
                }
            });
            View findViewById6 = view.findViewById(R.id.search_field);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.search_field)");
            ((EditText) findViewById6).addTextChangedListener(new TextWatcher(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet$onViewCreated$$inlined$doAfterTextChanged$1
                final /* synthetic */ ChooseGroupStoryBottomSheet this$0;

                @Override // android.text.TextWatcher
                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                @Override // android.text.TextWatcher
                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                {
                    this.this$0 = r1;
                }

                @Override // android.text.TextWatcher
                public void afterTextChanged(Editable editable) {
                    ContactSearchMediator contactSearchMediator2 = this.this$0.mediator;
                    String str = null;
                    if (contactSearchMediator2 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("mediator");
                        contactSearchMediator2 = null;
                    }
                    if (editable != null) {
                        str = editable.toString();
                    }
                    contactSearchMediator2.onFilterChanged(str);
                }
            });
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type android.widget.FrameLayout");
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2227onViewCreated$lambda0(ChooseGroupStoryBottomSheet chooseGroupStoryBottomSheet, View view) {
        Intrinsics.checkNotNullParameter(chooseGroupStoryBottomSheet, "this$0");
        chooseGroupStoryBottomSheet.onDone();
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2228onViewCreated$lambda3(ShareSelectionAdapter shareSelectionAdapter, ChooseGroupStoryBottomSheet chooseGroupStoryBottomSheet, Set set) {
        Intrinsics.checkNotNullParameter(shareSelectionAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(chooseGroupStoryBottomSheet, "this$0");
        Intrinsics.checkNotNullExpressionValue(set, "state");
        List<ContactSearchKey.RecipientSearchKey.Story> list = CollectionsKt___CollectionsJvmKt.filterIsInstance(set, ContactSearchKey.RecipientSearchKey.Story.class);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (ContactSearchKey.RecipientSearchKey.Story story : list) {
            arrayList.add(story.getRecipientId());
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        int i = 0;
        for (Object obj : arrayList) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            arrayList2.add(new ShareSelectionMappingModel(new ShareContact((RecipientId) obj), i == 0));
            i = i2;
        }
        shareSelectionAdapter.submitList(arrayList2);
        if (set.isEmpty()) {
            chooseGroupStoryBottomSheet.animateOutBottomBar();
        } else {
            chooseGroupStoryBottomSheet.animateInBottomBar();
        }
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        AnimatorSet animatorSet = this.animatorSet;
        if (animatorSet != null) {
            animatorSet.cancel();
        }
    }

    private final void animateInBottomBar() {
        AnimatorSet animatorSet = this.animatorSet;
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        AnimatorSet animatorSet2 = new AnimatorSet();
        Animator[] animatorArr = new Animator[4];
        View view = this.confirmButton;
        View view2 = null;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("confirmButton");
            view = null;
        }
        animatorArr[0] = ObjectAnimator.ofFloat(view, View.ALPHA, 1.0f);
        RecyclerView recyclerView = this.selectedList;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("selectedList");
            recyclerView = null;
        }
        animatorArr[1] = ObjectAnimator.ofFloat(recyclerView, View.TRANSLATION_Y, 0.0f);
        View view3 = this.backgroundHelper;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("backgroundHelper");
            view3 = null;
        }
        animatorArr[2] = ObjectAnimator.ofFloat(view3, View.TRANSLATION_Y, 0.0f);
        View view4 = this.divider;
        if (view4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("divider");
        } else {
            view2 = view4;
        }
        animatorArr[3] = ObjectAnimator.ofFloat(view2, View.TRANSLATION_Y, 0.0f);
        animatorSet2.playTogether(animatorArr);
        animatorSet2.start();
        this.animatorSet = animatorSet2;
    }

    private final void animateOutBottomBar() {
        float pixels = DimensionUnit.DP.toPixels(48.0f);
        AnimatorSet animatorSet = this.animatorSet;
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        AnimatorSet animatorSet2 = new AnimatorSet();
        Animator[] animatorArr = new Animator[4];
        View view = this.confirmButton;
        View view2 = null;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("confirmButton");
            view = null;
        }
        animatorArr[0] = ObjectAnimator.ofFloat(view, View.ALPHA, 0.0f);
        RecyclerView recyclerView = this.selectedList;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("selectedList");
            recyclerView = null;
        }
        animatorArr[1] = ObjectAnimator.ofFloat(recyclerView, View.TRANSLATION_Y, pixels);
        View view3 = this.backgroundHelper;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("backgroundHelper");
            view3 = null;
        }
        animatorArr[2] = ObjectAnimator.ofFloat(view3, View.TRANSLATION_Y, pixels);
        View view4 = this.divider;
        if (view4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("divider");
        } else {
            view2 = view4;
        }
        animatorArr[3] = ObjectAnimator.ofFloat(view2, View.TRANSLATION_Y, pixels);
        animatorSet2.playTogether(animatorArr);
        animatorSet2.start();
        this.animatorSet = animatorSet2;
    }

    private final void onDone() {
        Bundle bundle = new Bundle();
        ContactSearchMediator contactSearchMediator = this.mediator;
        if (contactSearchMediator == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mediator");
            contactSearchMediator = null;
        }
        List<ContactSearchKey.RecipientSearchKey.Story> list = CollectionsKt___CollectionsJvmKt.filterIsInstance(contactSearchMediator.getSelectedContacts(), ContactSearchKey.RecipientSearchKey.Story.class);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (ContactSearchKey.RecipientSearchKey.Story story : list) {
            arrayList.add(story.getRecipientId());
        }
        bundle.putParcelableArrayList(RESULT_SET, new ArrayList<>(arrayList));
        Unit unit = Unit.INSTANCE;
        FragmentKt.setFragmentResult(this, GROUP_STORY, bundle);
        dismissAllowingStateLoss();
    }
}
