package org.thoughtcrime.securesms.mediasend;

import android.content.Context;

/* loaded from: classes4.dex */
public final class CompositeMediaTransform implements MediaTransform {
    private final MediaTransform[] transforms;

    public CompositeMediaTransform(MediaTransform... mediaTransformArr) {
        this.transforms = mediaTransformArr;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaTransform
    public Media transform(Context context, Media media) {
        for (MediaTransform mediaTransform : this.transforms) {
            media = mediaTransform.transform(context, media);
        }
        return media;
    }
}
