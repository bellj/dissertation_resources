package org.thoughtcrime.securesms.mediasend;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import io.reactivex.rxjava3.core.Flowable;
import j$.util.Optional;
import java.io.FileDescriptor;
import java.util.Collections;
import java.util.LinkedHashMap;
import org.signal.imageeditor.core.model.EditorModel;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.CameraFragment;
import org.thoughtcrime.securesms.mediasend.MediaRepository;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.scribbles.ImageEditorFragment;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public class AvatarSelectionActivity extends AppCompatActivity implements CameraFragment.Controller, ImageEditorFragment.Controller, MediaGalleryFragment.Callbacks {
    private static final String ARG_GALLERY;
    private static final Point AVATAR_DIMENSIONS;
    public static final String EXTRA_MEDIA;
    private static final String IMAGE_CAPTURE;
    private static final String IMAGE_EDITOR;
    private Media currentMedia;

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public int getMaxVideoDuration() {
        return -1;
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public boolean isCameraEnabled() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public boolean isMultiselectEnabled() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onMainImageFailedToLoad() {
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onMainImageLoaded() {
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onRequestFullScreen(boolean z, boolean z2) {
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onTouchEventsNeeded(boolean z) {
    }

    static {
        int i = AvatarHelper.AVATAR_DIMENSIONS;
        AVATAR_DIMENSIONS = new Point(i, i);
    }

    public static Intent getIntentForCameraCapture(Context context) {
        return new Intent(context, AvatarSelectionActivity.class);
    }

    public static Intent getIntentForGallery(Context context) {
        Intent intentForCameraCapture = getIntentForCameraCapture(context);
        intentForCameraCapture.putExtra(ARG_GALLERY, true);
        return intentForCameraCapture;
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        getDelegate().setLocalNightMode(2);
        super.attachBaseContext(context);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.avatar_selection_activity);
        if (isGalleryFirst()) {
            onGalleryClicked();
        } else {
            onNavigateToCamera();
        }
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onCameraError() {
        Toast.makeText(this, (int) R.string.error, 0).show();
        finish();
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onImageCaptured(byte[] bArr, int i, int i2) {
        onMediaSelected(new Media(BlobProvider.getInstance().forData(bArr).withMimeType(MediaUtil.IMAGE_JPEG).createForSingleSessionInMemory(), MediaUtil.IMAGE_JPEG, System.currentTimeMillis(), i, i2, (long) bArr.length, 0, false, false, Optional.of(Media.ALL_MEDIA_BUCKET_ID), Optional.empty(), Optional.empty()));
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onVideoCaptured(FileDescriptor fileDescriptor) {
        throw new UnsupportedOperationException("Cannot set profile as video");
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onVideoCaptureError() {
        throw new AssertionError("This should never happen");
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onGalleryClicked() {
        if (!isGalleryFirst() || !popToRoot()) {
            FragmentTransaction replace = getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MediaGalleryFragment());
            if (isCameraFirst()) {
                replace.addToBackStack(null);
            }
            replace.commit();
        }
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onCameraCountButtonClicked() {
        throw new UnsupportedOperationException("Cannot select more than one photo");
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public Flowable<Optional<Media>> getMostRecentMediaItem() {
        return Flowable.just(Optional.empty());
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public MediaConstraints getMediaConstraints() {
        return MediaConstraints.getPushMediaConstraints();
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onMediaSelected(Media media) {
        this.currentMedia = media;
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, ImageEditorFragment.newInstanceForAvatarCapture(media.getUri()), IMAGE_EDITOR).addToBackStack(IMAGE_EDITOR).commit();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onDoneEditing() {
        handleSave();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onCancelEditing() {
        finish();
    }

    public boolean popToRoot() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount == 0) {
            return false;
        }
        for (int i = 0; i < backStackEntryCount; i++) {
            getSupportFragmentManager().popBackStack();
        }
        return true;
    }

    private boolean isGalleryFirst() {
        return getIntent().getBooleanExtra(ARG_GALLERY, false);
    }

    private boolean isCameraFirst() {
        return !isGalleryFirst();
    }

    private void handleSave() {
        ImageEditorFragment imageEditorFragment = (ImageEditorFragment) getSupportFragmentManager().findFragmentByTag(IMAGE_EDITOR);
        if (imageEditorFragment != null) {
            EditorModel readModel = ((ImageEditorFragment.Data) imageEditorFragment.saveState()).readModel();
            if (readModel != null) {
                MediaRepository.transformMedia(this, Collections.singletonList(this.currentMedia), Collections.singletonMap(this.currentMedia, new ImageEditorModelRenderMediaTransform(readModel, AVATAR_DIMENSIONS)), new MediaRepository.Callback() { // from class: org.thoughtcrime.securesms.mediasend.AvatarSelectionActivity$$ExternalSyntheticLambda0
                    @Override // org.thoughtcrime.securesms.mediasend.MediaRepository.Callback
                    public final void onComplete(Object obj) {
                        AvatarSelectionActivity.this.lambda$handleSave$0((LinkedHashMap) obj);
                    }
                });
                return;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    public /* synthetic */ void lambda$handleSave$0(LinkedHashMap linkedHashMap) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_MEDIA, (Media) linkedHashMap.get(this.currentMedia));
        setResult(-1, intent);
        finish();
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onMediaUnselected(Media media) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onSelectedMediaClicked(Media media) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onNavigateToCamera() {
        if (!isCameraFirst() || !popToRoot()) {
            FragmentTransaction replace = getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, CameraFragment.CC.newInstanceForAvatarCapture(), IMAGE_CAPTURE);
            if (isGalleryFirst()) {
                replace.addToBackStack(null);
            }
            replace.commit();
        }
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onSubmit() {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onToolbarNavigationClicked() {
        finish();
    }
}
