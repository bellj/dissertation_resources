package org.thoughtcrime.securesms.mediasend.v2.images;

import java.util.concurrent.TimeUnit;
import kotlin.Metadata;

/* compiled from: MediaReviewImagePageFragment.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0004"}, d2 = {"IMAGE_EDITOR_TAG", "", "MODE_DELAY", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaReviewImagePageFragmentKt {
    private static final String IMAGE_EDITOR_TAG;
    private static final long MODE_DELAY = TimeUnit.MILLISECONDS.toMillis(300);
}
