package org.thoughtcrime.securesms.mediasend;

import java.util.List;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class CameraContacts {
    private final List<Recipient> contacts;
    private final List<Recipient> groups;
    private final List<Recipient> recents;

    public CameraContacts(List<Recipient> list, List<Recipient> list2, List<Recipient> list3) {
        this.recents = list;
        this.contacts = list2;
        this.groups = list3;
    }

    public List<Recipient> getRecents() {
        return this.recents;
    }

    public List<Recipient> getContacts() {
        return this.contacts;
    }

    public List<Recipient> getGroups() {
        return this.groups;
    }

    public boolean isEmpty() {
        return this.recents.isEmpty() && this.contacts.isEmpty() && this.groups.isEmpty();
    }
}
