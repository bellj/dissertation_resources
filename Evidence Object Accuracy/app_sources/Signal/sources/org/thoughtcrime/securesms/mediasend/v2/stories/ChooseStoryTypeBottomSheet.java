package org.thoughtcrime.securesms.mediasend.v2.stories;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.LargeIconClickPreference;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: ChooseStoryTypeBottomSheet.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH\u0002¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/stories/ChooseStoryTypeBottomSheet;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChooseStoryTypeBottomSheet extends DSLSettingsBottomSheetFragment {

    /* compiled from: ChooseStoryTypeBottomSheet.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/stories/ChooseStoryTypeBottomSheet$Callback;", "", "onGroupStoryClicked", "", "onNewStoryClicked", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {
        void onGroupStoryClicked();

        void onNewStoryClicked();
    }

    public ChooseStoryTypeBottomSheet() {
        super(R.layout.dsl_settings_bottom_sheet_no_handle, null, 0.0f, 6, null);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        LargeIconClickPreference.INSTANCE.register(dSLSettingsAdapter);
        dSLSettingsAdapter.submitList(getConfiguration().toMappingModelList());
    }

    private final DSLConfiguration getConfiguration() {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet$getConfiguration$1
            final /* synthetic */ ChooseStoryTypeBottomSheet this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLConfiguration.textPref$default(dSLConfiguration, companion.from(R.string.ChooseStoryTypeBottomSheet__choose_your_story_type, DSLSettingsText.CenterModifier.INSTANCE, DSLSettingsText.Body1BoldModifier.INSTANCE, DSLSettingsText.BoldModifier.INSTANCE), null, 2, null);
                DSLSettingsText from = companion.from(R.string.ChooseStoryTypeBottomSheet__new_private_story, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from2 = companion.from(R.string.ChooseStoryTypeBottomSheet__visible_only_to, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon.Companion companion2 = DSLSettingsIcon.Companion;
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                DSLSettingsIcon from3 = companion2.from(R.drawable.ic_plus_24, R.color.signal_icon_tint_primary, R.drawable.circle_tintable, R.color.signal_button_secondary_ripple, (int) dimensionUnit.toPixels(8.0f));
                final ChooseStoryTypeBottomSheet chooseStoryTypeBottomSheet = this.this$0;
                dSLConfiguration.customPref(new LargeIconClickPreference.Model(from, from3, from2, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ChooseStoryTypeBottomSheet.Callback callback;
                        chooseStoryTypeBottomSheet.dismissAllowingStateLoss();
                        ChooseStoryTypeBottomSheet chooseStoryTypeBottomSheet2 = chooseStoryTypeBottomSheet;
                        ArrayList arrayList = new ArrayList();
                        try {
                            Fragment fragment = chooseStoryTypeBottomSheet2.getParentFragment();
                            while (true) {
                                if (fragment == null) {
                                    FragmentActivity requireActivity = chooseStoryTypeBottomSheet2.requireActivity();
                                    if (requireActivity != null) {
                                        callback = (ChooseStoryTypeBottomSheet.Callback) requireActivity;
                                    } else {
                                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet.Callback");
                                    }
                                } else if (fragment instanceof ChooseStoryTypeBottomSheet.Callback) {
                                    callback = fragment;
                                    break;
                                } else {
                                    String name = fragment.getClass().getName();
                                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                                    arrayList.add(name);
                                    fragment = fragment.getParentFragment();
                                }
                            }
                            callback.onNewStoryClicked();
                        } catch (ClassCastException e) {
                            String name2 = chooseStoryTypeBottomSheet2.requireActivity().getClass().getName();
                            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
                            arrayList.add(name2);
                            throw new ListenerNotFoundException(arrayList, e);
                        }
                    }
                }));
                DSLSettingsText from4 = companion.from(R.string.ChooseStoryTypeBottomSheet__group_story, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from5 = companion.from(R.string.ChooseStoryTypeBottomSheet__share_to_an_existing_group, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from6 = companion2.from(R.drawable.ic_group_outline_24, R.color.signal_icon_tint_primary, R.drawable.circle_tintable, R.color.signal_button_secondary_ripple, (int) dimensionUnit.toPixels(8.0f));
                final ChooseStoryTypeBottomSheet chooseStoryTypeBottomSheet2 = this.this$0;
                dSLConfiguration.customPref(new LargeIconClickPreference.Model(from4, from6, from5, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ChooseStoryTypeBottomSheet.Callback callback;
                        chooseStoryTypeBottomSheet2.dismissAllowingStateLoss();
                        ChooseStoryTypeBottomSheet chooseStoryTypeBottomSheet3 = chooseStoryTypeBottomSheet2;
                        ArrayList arrayList = new ArrayList();
                        try {
                            Fragment fragment = chooseStoryTypeBottomSheet3.getParentFragment();
                            while (true) {
                                if (fragment == null) {
                                    FragmentActivity requireActivity = chooseStoryTypeBottomSheet3.requireActivity();
                                    if (requireActivity != null) {
                                        callback = (ChooseStoryTypeBottomSheet.Callback) requireActivity;
                                    } else {
                                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet.Callback");
                                    }
                                } else if (fragment instanceof ChooseStoryTypeBottomSheet.Callback) {
                                    callback = fragment;
                                    break;
                                } else {
                                    String name = fragment.getClass().getName();
                                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                                    arrayList.add(name);
                                    fragment = fragment.getParentFragment();
                                }
                            }
                            callback.onGroupStoryClicked();
                        } catch (ClassCastException e) {
                            String name2 = chooseStoryTypeBottomSheet3.requireActivity().getClass().getName();
                            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
                            arrayList.add(name2);
                            throw new ListenerNotFoundException(arrayList, e);
                        }
                    }
                }));
                dSLConfiguration.space((int) dimensionUnit.toPixels(32.0f));
            }
        });
    }
}
