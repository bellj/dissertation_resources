package org.thoughtcrime.securesms.mediasend.v2.review;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: MediaReviewAnimatorController.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\bJ\u0018\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\bJ\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewAnimatorController;", "", "()V", "getFadeInAnimator", "Landroid/animation/Animator;", "view", "Landroid/view/View;", "isEnabled", "", "getFadeOutAnimator", "getSlideInAnimator", "getSlideOutAnimator", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaReviewAnimatorController {
    public static final MediaReviewAnimatorController INSTANCE = new MediaReviewAnimatorController();

    private MediaReviewAnimatorController() {
    }

    public final Animator getSlideInAnimator(View view) {
        Intrinsics.checkNotNullParameter(view, "view");
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "translationY", view.getTranslationY(), 0.0f);
        Intrinsics.checkNotNullExpressionValue(ofFloat, "ofFloat(view, \"translati…\", view.translationY, 0f)");
        return ofFloat;
    }

    public final Animator getSlideOutAnimator(View view) {
        Intrinsics.checkNotNullParameter(view, "view");
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "translationY", view.getTranslationX(), (float) ViewUtil.dpToPx(48));
        Intrinsics.checkNotNullExpressionValue(ofFloat, "ofFloat(view, \"translati…til.dpToPx(48).toFloat())");
        return ofFloat;
    }

    public static /* synthetic */ Animator getFadeInAnimator$default(MediaReviewAnimatorController mediaReviewAnimatorController, View view, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = true;
        }
        return mediaReviewAnimatorController.getFadeInAnimator(view, z);
    }

    public final Animator getFadeInAnimator(View view, boolean z) {
        Intrinsics.checkNotNullParameter(view, "view");
        ViewExtensionsKt.setVisible(view, true);
        view.setEnabled(z);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "alpha", view.getAlpha(), 1.0f);
        Intrinsics.checkNotNullExpressionValue(ofFloat, "ofFloat(view, \"alpha\", view.alpha, 1f)");
        return ofFloat;
    }

    public static /* synthetic */ Animator getFadeOutAnimator$default(MediaReviewAnimatorController mediaReviewAnimatorController, View view, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return mediaReviewAnimatorController.getFadeOutAnimator(view, z);
    }

    public final Animator getFadeOutAnimator(View view, boolean z) {
        Intrinsics.checkNotNullParameter(view, "view");
        view.setEnabled(z);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "alpha", view.getAlpha(), 0.0f);
        Intrinsics.checkNotNullExpressionValue(ofFloat, "animator");
        ofFloat.addListener(new Animator.AnimatorListener(view) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewAnimatorController$getFadeOutAnimator$$inlined$doOnEnd$1
            final /* synthetic */ View $view$inlined;

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
                ViewExtensionsKt.setVisible(this.$view$inlined, false);
            }

            {
                this.$view$inlined = r1;
            }
        });
        return ofFloat;
    }
}
