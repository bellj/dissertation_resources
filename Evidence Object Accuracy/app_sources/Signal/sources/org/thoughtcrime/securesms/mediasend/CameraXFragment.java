package org.thoughtcrime.securesms.mediasend;

import android.animation.Animator;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.camera.view.SignalCameraView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Executors;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import j$.util.Optional;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Objects;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.components.TooltipPopup;
import org.thoughtcrime.securesms.mediasend.CameraFragment;
import org.thoughtcrime.securesms.mediasend.CameraXVideoCaptureHelper;
import org.thoughtcrime.securesms.mediasend.camerax.CameraXFlashToggleView;
import org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil;
import org.thoughtcrime.securesms.mediasend.v2.MediaAnimations;
import org.thoughtcrime.securesms.mediasend.v2.MediaCountIndicatorButton;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.viewer.page.StoryDisplay;
import org.thoughtcrime.securesms.util.MemoryFileDescriptor;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.video.VideoUtil;

/* loaded from: classes4.dex */
public class CameraXFragment extends LoggingFragment implements CameraFragment {
    private static final String IS_VIDEO_ENABLED;
    private static final String TAG = Log.tag(CameraXFragment.class);
    private SignalCameraView camera;
    private CameraFragment.Controller controller;
    private ViewGroup controlsContainer;
    private boolean isMediaSelected;
    private boolean isThumbAvailable;
    private Disposable mostRecentItemDisposable = Disposable.CC.disposed();
    private View selfieFlash;
    private MemoryFileDescriptor videoFileDescriptor;

    public static CameraXFragment newInstanceForAvatarCapture() {
        CameraXFragment cameraXFragment = new CameraXFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_VIDEO_ENABLED, false);
        cameraXFragment.setArguments(bundle);
        return cameraXFragment;
    }

    public static CameraXFragment newInstance() {
        CameraXFragment cameraXFragment = new CameraXFragment();
        cameraXFragment.setArguments(new Bundle());
        return cameraXFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof CameraFragment.Controller) {
            this.controller = (CameraFragment.Controller) getActivity();
        } else if (getParentFragment() instanceof CameraFragment.Controller) {
            this.controller = (CameraFragment.Controller) getParentFragment();
        }
        if (this.controller == null) {
            throw new IllegalStateException("Parent must implement controller interface.");
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.camerax_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        requireActivity().setRequestedOrientation(4);
        this.camera = (SignalCameraView) view.findViewById(R.id.camerax_camera);
        this.controlsContainer = (ViewGroup) view.findViewById(R.id.camerax_controls_container);
        this.camera.setScaleType(PreviewView.ScaleType.FIT_CENTER);
        this.camera.bindToLifecycle(getViewLifecycleOwner(), new CameraXFragment$$ExternalSyntheticLambda2(this));
        this.camera.setCameraLensFacing(Integer.valueOf(CameraXUtil.toLensFacing(TextSecurePreferences.getDirectCaptureCameraId(requireContext()))));
        onOrientationChanged(getResources().getConfiguration().orientation);
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener((ViewGroup) view.findViewById(R.id.camerax_camera_parent)) { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda9
            public final /* synthetic */ ViewGroup f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                CameraXFragment.this.lambda$onViewCreated$0(this.f$1, view2, i, i2, i3, i4, i5, i6, i7, i8);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(ViewGroup viewGroup, View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        float f = (float) (i3 - i);
        float min = Math.min((1.0f / CameraFragment.CC.getAspectRatioForOrientation(getResources().getConfiguration().orientation)) * f, (float) (i4 - i2));
        ViewGroup.LayoutParams layoutParams = viewGroup.getLayoutParams();
        int i9 = (int) min;
        if (layoutParams.height != i9) {
            layoutParams.width = (int) f;
            layoutParams.height = i9;
            viewGroup.setLayoutParams(layoutParams);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.camera.bindToLifecycle(getViewLifecycleOwner(), new CameraXFragment$$ExternalSyntheticLambda2(this));
        requireActivity().setRequestedOrientation(4);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        this.mostRecentItemDisposable.dispose();
        closeVideoFileDescriptor();
        requireActivity().setRequestedOrientation(-1);
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        onOrientationChanged(configuration.orientation);
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment
    public void fadeOutControls(final Runnable runnable) {
        this.controlsContainer.setEnabled(false);
        this.controlsContainer.animate().setDuration(250).alpha(0.0f).setInterpolator(MediaAnimations.getInterpolator()).setListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment.1
            @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                CameraXFragment.this.controlsContainer.setEnabled(true);
                runnable.run();
            }
        });
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment
    public void fadeInControls() {
        this.controlsContainer.setEnabled(false);
        this.controlsContainer.animate().setDuration(250).alpha(1.0f).setInterpolator(MediaAnimations.getInterpolator()).setListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment.2
            @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                CameraXFragment.this.controlsContainer.setEnabled(true);
            }
        });
    }

    public void handleCameraInitializationError(Throwable th) {
        Log.w(TAG, "An error occurred", th);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Toast.makeText(activity, (int) R.string.CameraFragment__failed_to_open_camera, 0).show();
        }
    }

    private void onOrientationChanged(int i) {
        int i2 = i == 1 ? R.layout.camera_controls_portrait : R.layout.camera_controls_landscape;
        this.controlsContainer.removeAllViews();
        this.controlsContainer.addView(LayoutInflater.from(getContext()).inflate(i2, this.controlsContainer, false));
        initControls();
    }

    private void presentRecentItemThumbnail(Media media) {
        ImageView imageView = (ImageView) this.controlsContainer.findViewById(R.id.camera_gallery_button);
        boolean z = false;
        if (media != null) {
            imageView.setVisibility(0);
            Glide.with(this).load((Object) new DecryptableStreamUriLoader.DecryptableUri(media.getUri())).centerCrop().into(imageView);
        } else {
            imageView.setVisibility(8);
            imageView.setImageResource(0);
        }
        if (media != null) {
            z = true;
        }
        this.isThumbAvailable = z;
        updateGalleryVisibility();
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment
    public void presentHud(int i) {
        MediaCountIndicatorButton mediaCountIndicatorButton = (MediaCountIndicatorButton) this.controlsContainer.findViewById(R.id.camera_review_button);
        boolean z = false;
        if (i > 0) {
            mediaCountIndicatorButton.setVisibility(0);
            mediaCountIndicatorButton.setCount(i);
        } else {
            mediaCountIndicatorButton.setVisibility(8);
        }
        if (i > 0) {
            z = true;
        }
        this.isMediaSelected = z;
        updateGalleryVisibility();
    }

    private void updateGalleryVisibility() {
        View findViewById = this.controlsContainer.findViewById(R.id.camera_gallery_button_background);
        if (this.isMediaSelected || !this.isThumbAvailable) {
            findViewById.setVisibility(8);
        } else {
            findViewById.setVisibility(0);
        }
    }

    private void initControls() {
        final View findViewById = requireView().findViewById(R.id.camera_flip_button);
        final CameraButtonView cameraButtonView = (CameraButtonView) requireView().findViewById(R.id.camera_capture_button);
        View findViewById2 = requireView().findViewById(R.id.camera_gallery_button);
        View findViewById3 = requireView().findViewById(R.id.camera_review_button);
        final CameraXFlashToggleView cameraXFlashToggleView = (CameraXFlashToggleView) requireView().findViewById(R.id.camera_flash_button);
        View findViewById4 = requireView().findViewById(R.id.toggle_spacer);
        if (findViewById4 != null) {
            if (!Stories.isFeatureEnabled()) {
                findViewById4.setVisibility(8);
            } else if (StoryDisplay.Companion.getStoryDisplay((float) getResources().getDisplayMetrics().widthPixels, (float) getResources().getDisplayMetrics().heightPixels) == StoryDisplay.SMALL) {
                findViewById4.setVisibility(0);
            } else {
                findViewById4.setVisibility(8);
            }
        }
        this.mostRecentItemDisposable.dispose();
        this.mostRecentItemDisposable = this.controller.getMostRecentMediaItem().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                CameraXFragment.this.lambda$initControls$1((Optional) obj);
            }
        });
        this.selfieFlash = requireView().findViewById(R.id.camera_selfie_flash);
        cameraButtonView.setOnClickListener(new View.OnClickListener(cameraButtonView, findViewById, cameraXFlashToggleView) { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ CameraButtonView f$1;
            public final /* synthetic */ View f$2;
            public final /* synthetic */ CameraXFlashToggleView f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CameraXFragment.this.lambda$initControls$2(this.f$1, this.f$2, this.f$3, view);
            }
        });
        this.camera.setScaleType(PreviewView.ScaleType.FILL_CENTER);
        ProcessCameraProvider.getInstance(requireContext()).addListener(new Runnable(findViewById, cameraXFlashToggleView) { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda5
            public final /* synthetic */ View f$1;
            public final /* synthetic */ CameraXFlashToggleView f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CameraXFragment.this.lambda$initControls$3(this.f$1, this.f$2);
            }
        }, Executors.mainThreadExecutor());
        cameraXFlashToggleView.setAutoFlashEnabled(this.camera.hasFlash());
        cameraXFlashToggleView.setFlash(this.camera.getFlash());
        SignalCameraView signalCameraView = this.camera;
        Objects.requireNonNull(signalCameraView);
        cameraXFlashToggleView.setOnFlashModeChangedListener(new CameraXFlashToggleView.OnFlashModeChangedListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.mediasend.camerax.CameraXFlashToggleView.OnFlashModeChangedListener
            public final void flashModeChanged(int i) {
                SignalCameraView.this.setFlash(i);
            }
        });
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CameraXFragment.this.lambda$initControls$4(view);
            }
        });
        findViewById3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CameraXFragment.this.lambda$initControls$5(view);
            }
        });
        if (isVideoRecordingSupported(requireContext())) {
            try {
                closeVideoFileDescriptor();
                this.videoFileDescriptor = CameraXVideoCaptureHelper.createFileDescriptor(requireContext());
                final Animation loadAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in);
                final Animation loadAnimation2 = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_out);
                this.camera.setCaptureMode(SignalCameraView.CaptureMode.MIXED);
                int maxVideoRecordDurationInSeconds = VideoUtil.getMaxVideoRecordDurationInSeconds(requireContext(), this.controller.getMediaConstraints());
                if (this.controller.getMaxVideoDuration() > 0) {
                    maxVideoRecordDurationInSeconds = this.controller.getMaxVideoDuration();
                }
                String str = TAG;
                Log.d(str, "Max duration: " + maxVideoRecordDurationInSeconds + " sec");
                cameraButtonView.setVideoCaptureListener(new CameraXVideoCaptureHelper(this, cameraButtonView, this.camera, this.videoFileDescriptor, maxVideoRecordDurationInSeconds, new CameraXVideoCaptureHelper.Callback() { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment.3
                    @Override // org.thoughtcrime.securesms.mediasend.CameraXVideoCaptureHelper.Callback
                    public void onVideoRecordStarted() {
                        CameraXFragment.this.hideAndDisableControlsForVideoRecording(cameraButtonView, cameraXFlashToggleView, findViewById, loadAnimation2);
                    }

                    @Override // org.thoughtcrime.securesms.mediasend.CameraXVideoCaptureHelper.Callback
                    public void onVideoSaved(FileDescriptor fileDescriptor) {
                        CameraXFragment.this.showAndEnableControlsAfterVideoRecording(cameraButtonView, cameraXFlashToggleView, findViewById, loadAnimation);
                        CameraXFragment.this.controller.onVideoCaptured(fileDescriptor);
                    }

                    @Override // org.thoughtcrime.securesms.mediasend.CameraXVideoCaptureHelper.Callback
                    public void onVideoError(Throwable th) {
                        CameraXFragment.this.showAndEnableControlsAfterVideoRecording(cameraButtonView, cameraXFlashToggleView, findViewById, loadAnimation);
                        CameraXFragment.this.controller.onVideoCaptureError();
                    }
                }));
                displayVideoRecordingTooltipIfNecessary(cameraButtonView);
            } catch (IOException e) {
                Log.w(TAG, "Video capture is not supported on this device.", e);
            }
        } else {
            String str2 = TAG;
            Log.i(str2, "Video capture not supported. API: " + Build.VERSION.SDK_INT + ", MFD: " + MemoryFileDescriptor.supported() + ", Camera: " + CameraXUtil.getLowestSupportedHardwareLevel(requireContext()) + ", MaxDuration: " + VideoUtil.getMaxVideoRecordDurationInSeconds(requireContext(), this.controller.getMediaConstraints()) + " sec");
        }
    }

    public /* synthetic */ void lambda$initControls$1(Optional optional) throws Throwable {
        presentRecentItemThumbnail((Media) optional.orElse(null));
    }

    public /* synthetic */ void lambda$initControls$2(CameraButtonView cameraButtonView, View view, CameraXFlashToggleView cameraXFlashToggleView, View view2) {
        cameraButtonView.setEnabled(false);
        view.setEnabled(false);
        cameraXFlashToggleView.setEnabled(false);
        onCaptureClicked();
    }

    public /* synthetic */ void lambda$initControls$4(View view) {
        this.controller.onGalleryClicked();
    }

    public /* synthetic */ void lambda$initControls$5(View view) {
        this.controller.onCameraCountButtonClicked();
    }

    private boolean isVideoRecordingSupported(Context context) {
        if (Build.VERSION.SDK_INT < 26 || !requireArguments().getBoolean(IS_VIDEO_ENABLED, true) || !MediaConstraints.isVideoTranscodeAvailable() || !CameraXUtil.isMixedModeSupported(context) || VideoUtil.getMaxVideoRecordDurationInSeconds(context, this.controller.getMediaConstraints()) <= 0) {
            return false;
        }
        return true;
    }

    private void displayVideoRecordingTooltipIfNecessary(CameraButtonView cameraButtonView) {
        if (shouldDisplayVideoRecordingTooltip()) {
            int rotation = requireActivity().getWindowManager().getDefaultDisplay().getRotation();
            TooltipPopup.Builder text = TooltipPopup.forTarget(cameraButtonView).setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda1
                @Override // android.widget.PopupWindow.OnDismissListener
                public final void onDismiss() {
                    CameraXFragment.this.neverDisplayVideoRecordingTooltipAgain();
                }
            }).setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.core_ultramarine)).setTextColor(ContextCompat.getColor(requireContext(), R.color.signal_text_toolbar_title)).setText(R.string.CameraXFragment_tap_for_photo_hold_for_video);
            int i = 2;
            if (rotation == 0 || rotation == 2) {
                i = 0;
            }
            text.show(i);
        }
    }

    private boolean shouldDisplayVideoRecordingTooltip() {
        return !TextSecurePreferences.hasSeenVideoRecordingTooltip(requireContext()) && MediaConstraints.isVideoTranscodeAvailable();
    }

    public void neverDisplayVideoRecordingTooltipAgain() {
        if (getContext() != null) {
            TextSecurePreferences.setHasSeenVideoRecordingTooltip(requireContext(), true);
        }
    }

    public void hideAndDisableControlsForVideoRecording(View view, View view2, View view3, Animation animation) {
        view.setEnabled(false);
        view2.startAnimation(animation);
        view2.setVisibility(4);
        view3.startAnimation(animation);
        view3.setVisibility(4);
    }

    public void showAndEnableControlsAfterVideoRecording(View view, View view2, View view3, Animation animation) {
        requireActivity().runOnUiThread(new Runnable(view, view2, animation, view3) { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$0;
            public final /* synthetic */ View f$1;
            public final /* synthetic */ Animation f$2;
            public final /* synthetic */ View f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CameraXFragment.lambda$showAndEnableControlsAfterVideoRecording$6(this.f$0, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public static /* synthetic */ void lambda$showAndEnableControlsAfterVideoRecording$6(View view, View view2, Animation animation, View view3) {
        view.setEnabled(true);
        view2.startAnimation(animation);
        view2.setVisibility(0);
        view3.startAnimation(animation);
        view3.setVisibility(0);
    }

    private void onCaptureClicked() {
        final Stopwatch stopwatch = new Stopwatch("Capture");
        final CameraXSelfieFlashHelper cameraXSelfieFlashHelper = new CameraXSelfieFlashHelper(requireActivity().getWindow(), this.camera, this.selfieFlash);
        this.camera.takePicture(Executors.mainThreadExecutor(), new ImageCapture.OnImageCapturedCallback() { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment.4
            @Override // androidx.camera.core.ImageCapture.OnImageCapturedCallback
            public void onCaptureSuccess(ImageProxy imageProxy) {
                cameraXSelfieFlashHelper.endFlash();
                SimpleTask.run(CameraXFragment.this.getViewLifecycleOwner().getLifecycle(), new CameraXFragment$4$$ExternalSyntheticLambda0(this, stopwatch, imageProxy), new CameraXFragment$4$$ExternalSyntheticLambda1(this, stopwatch));
            }

            public /* synthetic */ CameraXUtil.ImageResult lambda$onCaptureSuccess$0(Stopwatch stopwatch2, ImageProxy imageProxy) {
                try {
                    stopwatch2.split("captured");
                    return CameraXUtil.toJpeg(imageProxy, CameraXFragment.this.camera.getCameraLensFacing().intValue() == 0);
                } catch (IOException e) {
                    Log.w(CameraXFragment.TAG, "Failed to encode captured image.", e);
                    return null;
                } finally {
                    imageProxy.close();
                }
            }

            public /* synthetic */ void lambda$onCaptureSuccess$1(Stopwatch stopwatch2, CameraXUtil.ImageResult imageResult) {
                stopwatch2.split("transformed");
                stopwatch2.stop(CameraXFragment.TAG);
                if (imageResult != null) {
                    CameraXFragment.this.controller.onImageCaptured(imageResult.getData(), imageResult.getWidth(), imageResult.getHeight());
                } else {
                    CameraXFragment.this.controller.onCameraError();
                }
            }

            @Override // androidx.camera.core.ImageCapture.OnImageCapturedCallback
            public void onError(ImageCaptureException imageCaptureException) {
                Log.w(CameraXFragment.TAG, "Failed to capture image", imageCaptureException);
                cameraXSelfieFlashHelper.endFlash();
                CameraXFragment.this.controller.onCameraError();
            }
        });
        cameraXSelfieFlashHelper.startFlash();
    }

    private void closeVideoFileDescriptor() {
        MemoryFileDescriptor memoryFileDescriptor = this.videoFileDescriptor;
        if (memoryFileDescriptor != null) {
            try {
                memoryFileDescriptor.close();
                this.videoFileDescriptor = null;
            } catch (IOException e) {
                Log.w(TAG, "Failed to close video file descriptor", e);
            }
        }
    }

    /* renamed from: initializeFlipButton */
    public void lambda$initControls$3(final View view, CameraXFlashToggleView cameraXFlashToggleView) {
        if (getContext() == null) {
            Log.w(TAG, "initializeFlipButton called either before or after fragment was attached.");
        } else if (!this.camera.hasCameraWithLensFacing(0) || !this.camera.hasCameraWithLensFacing(1)) {
            view.setVisibility(8);
        } else {
            view.setVisibility(0);
            view.setOnClickListener(new View.OnClickListener(view, cameraXFlashToggleView) { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda10
                public final /* synthetic */ View f$1;
                public final /* synthetic */ CameraXFlashToggleView f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    CameraXFragment.this.lambda$initializeFlipButton$7(this.f$1, this.f$2, view2);
                }
            });
            this.camera.setOnTouchListener(new View.OnTouchListener(new GestureDetector(requireContext(), new GestureDetector.SimpleOnGestureListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment.5
                @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
                public boolean onDoubleTap(MotionEvent motionEvent) {
                    if (!view.isEnabled()) {
                        return true;
                    }
                    view.performClick();
                    return true;
                }
            })) { // from class: org.thoughtcrime.securesms.mediasend.CameraXFragment$$ExternalSyntheticLambda11
                public final /* synthetic */ GestureDetector f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.view.View.OnTouchListener
                public final boolean onTouch(View view2, MotionEvent motionEvent) {
                    return this.f$0.onTouchEvent(motionEvent);
                }
            });
        }
    }

    public /* synthetic */ void lambda$initializeFlipButton$7(View view, CameraXFlashToggleView cameraXFlashToggleView, View view2) {
        this.camera.toggleCamera();
        TextSecurePreferences.setDirectCaptureCameraId(getContext(), CameraXUtil.toCameraDirectionInt(this.camera.getCameraLensFacing().intValue()));
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        rotateAnimation.setDuration(200);
        rotateAnimation.setInterpolator(new DecelerateInterpolator());
        view.startAnimation(rotateAnimation);
        cameraXFlashToggleView.setAutoFlashEnabled(this.camera.hasFlash());
        cameraXFlashToggleView.setFlash(this.camera.getFlash());
    }
}
