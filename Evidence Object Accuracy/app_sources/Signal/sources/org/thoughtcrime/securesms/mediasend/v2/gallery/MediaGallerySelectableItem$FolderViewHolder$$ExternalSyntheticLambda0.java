package org.thoughtcrime.securesms.mediasend.v2.gallery;

import android.view.View;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectableItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaGallerySelectableItem$FolderViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ MediaGallerySelectableItem.FolderViewHolder f$0;
    public final /* synthetic */ MediaGallerySelectableItem.FolderModel f$1;

    public /* synthetic */ MediaGallerySelectableItem$FolderViewHolder$$ExternalSyntheticLambda0(MediaGallerySelectableItem.FolderViewHolder folderViewHolder, MediaGallerySelectableItem.FolderModel folderModel) {
        this.f$0 = folderViewHolder;
        this.f$1 = folderModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        MediaGallerySelectableItem.FolderViewHolder.m2174bind$lambda0(this.f$0, this.f$1, view);
    }
}
