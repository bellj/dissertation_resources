package org.thoughtcrime.securesms.mediasend.v2.review;

import android.view.View;
import org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewSelectedItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaReviewSelectedItem$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ MediaReviewSelectedItem.ViewHolder f$0;
    public final /* synthetic */ MediaReviewSelectedItem.Model f$1;

    public /* synthetic */ MediaReviewSelectedItem$ViewHolder$$ExternalSyntheticLambda0(MediaReviewSelectedItem.ViewHolder viewHolder, MediaReviewSelectedItem.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        MediaReviewSelectedItem.ViewHolder.m2225bind$lambda0(this.f$0, this.f$1, view);
    }
}
