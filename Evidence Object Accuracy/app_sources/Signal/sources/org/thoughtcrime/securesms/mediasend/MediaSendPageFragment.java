package org.thoughtcrime.securesms.mediasend;

import android.net.Uri;
import android.view.View;

/* loaded from: classes4.dex */
public interface MediaSendPageFragment {
    View getPlaybackControls();

    Uri getUri();

    void notifyHidden();

    void restoreState(Object obj);

    Object saveState();

    void setUri(Uri uri);
}
