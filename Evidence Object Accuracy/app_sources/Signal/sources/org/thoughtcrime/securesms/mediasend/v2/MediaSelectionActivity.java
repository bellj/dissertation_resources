package org.thoughtcrime.securesms.mediasend.v2;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import androidx.activity.ComponentActivity;
import androidx.activity.OnBackPressedCallback;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelLazy;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;
import com.google.android.material.animation.ArgbEvaluatorCompat;
import j$.util.function.Function;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.text.StringsKt__StringsJVMKt;
import kotlin.text.StringsKt__StringsKt;
import org.signal.core.util.BreakIteratorCompat;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiEventListener;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchState;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment;
import org.thoughtcrime.securesms.linkpreview.Link;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewUtil;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.mediasend.v2.HudCommand;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionDestination;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;
import org.thoughtcrime.securesms.mediasend.v2.UntrustedRecords;
import org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment;
import org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: MediaSelectionActivity.kt */
@Metadata(d1 = {"\u0000²\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 S2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006:\u0002STB\u0005¢\u0006\u0002\u0010\u0007J \u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020)2\u0006\u0010+\u001a\u00020,H\u0002J\u0010\u0010-\u001a\u00020'2\u0006\u0010.\u001a\u00020/H\u0014J\b\u00100\u001a\u00020\u0016H\u0002J\b\u00101\u001a\u00020'H\u0016J\u001a\u00102\u001a\u0004\u0018\u0001032\u0006\u00104\u001a\u0002052\u0006\u00106\u001a\u000207H\u0016J\b\u00108\u001a\u00020'H\u0002J\b\u00109\u001a\u00020\u0016H\u0002J\u0014\u0010:\u001a\u00020\u00162\n\b\u0002\u0010;\u001a\u0004\u0018\u00010<H\u0002J\u001a\u0010=\u001a\u00020'2\b\u0010>\u001a\u0004\u0018\u00010?2\u0006\u0010@\u001a\u00020\u0016H\u0014J\u0012\u0010A\u001a\u00020'2\b\u0010B\u001a\u0004\u0018\u00010CH\u0016J\u0012\u0010D\u001a\u00020'2\b\u0010E\u001a\u0004\u0018\u00010FH\u0016J\b\u0010G\u001a\u00020'H\u0016J\b\u0010H\u001a\u00020'H\u0016J\u0010\u0010I\u001a\u00020'2\u0006\u0010J\u001a\u00020?H\u0014J\u0010\u0010K\u001a\u00020'2\u0006\u0010L\u001a\u00020MH\u0016J\u0010\u0010N\u001a\u00020'2\u0006\u0010O\u001a\u00020PH\u0016J\b\u0010Q\u001a\u00020'H\u0016J\b\u0010R\u001a\u00020'H\u0016R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u00020\u000e8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u0004\u0018\u00010\u00128BX\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u00168BX\u0004¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u00168BX\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u0017R\u001b\u0010\u001a\u001a\u00020\u001b8BX\u0002¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b\u001c\u0010\u001dR\u001a\u0010 \u001a\u00020!X.¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%¨\u0006U"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionActivity;", "Lorg/thoughtcrime/securesms/PassphraseRequiredActivity;", "Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewFragment$Callback;", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment$Callback;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiEventListener;", "Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchFragment$Callback;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/SearchConfigurationProvider;", "()V", "animateInShadowLayerValueAnimator", "Landroid/animation/ValueAnimator;", "animateInTextColorValueAnimator", "animateOutShadowLayerValueAnimator", "animateOutTextColorValueAnimator", MediaSelectionActivity.DESTINATION, "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "getDestination", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "draftText", "", "getDraftText", "()Ljava/lang/CharSequence;", "isStory", "", "()Z", "shareToTextStory", "getShareToTextStory", "textViewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "getTextViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "textViewModel$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "setViewModel", "(Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;)V", "animateTextStyling", "", "selectedSwitch", "Landroid/widget/TextView;", "unselectedSwitch", "duration", "", "attachBaseContext", "newBase", "Landroid/content/Context;", "canDisplayStorySwitch", "closeEmojiSearch", "getSearchConfiguration", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "contactSearchState", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchState;", "initializeTextStory", "isCameraFirst", "navigateToStartDestination", "navHostFragment", "Landroidx/navigation/fragment/NavHostFragment;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "ready", "onEmojiSelected", "emoji", "", "onKeyEvent", "keyEvent", "Landroid/view/KeyEvent;", "onNoMediaSelected", "onPopFromReview", "onSaveInstanceState", "outState", "onSendError", "error", "", "onSentWithResult", "mediaSendActivityResult", "Lorg/thoughtcrime/securesms/mediasend/MediaSendActivityResult;", "onSentWithoutResult", "openEmojiSearch", "Companion", "OnBackPressed", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaSelectionActivity extends PassphraseRequiredActivity implements MediaReviewFragment.Callback, EmojiKeyboardPageFragment.Callback, EmojiEventListener, EmojiSearchFragment.Callback, SearchConfigurationProvider {
    private static final String AS_TEXT_STORY;
    public static final Companion Companion = new Companion(null);
    private static final String DESTINATION;
    private static final String IS_REPLY;
    private static final String IS_STORY;
    private static final String MEDIA;
    private static final String MESSAGE;
    private static final String MESSAGE_SEND_TYPE;
    private static final String NAV_HOST_TAG;
    private static final String START_ACTION;
    private static final String TAG = Log.tag(MediaSelectionActivity.class);
    private ValueAnimator animateInShadowLayerValueAnimator;
    private ValueAnimator animateInTextColorValueAnimator;
    private ValueAnimator animateOutShadowLayerValueAnimator;
    private ValueAnimator animateOutTextColorValueAnimator;
    private final Lazy textViewModel$delegate;
    public MediaSelectionViewModel viewModel;

    @JvmStatic
    public static final Intent camera(Context context) {
        return Companion.camera(context);
    }

    @JvmStatic
    public static final Intent camera(Context context, MessageSendType messageSendType, RecipientId recipientId, boolean z) {
        return Companion.camera(context, messageSendType, recipientId, z);
    }

    @JvmStatic
    public static final Intent camera(Context context, boolean z) {
        return Companion.camera(context, z);
    }

    @JvmStatic
    public static final Intent editor(Context context, MessageSendType messageSendType, List<? extends Media> list, RecipientId recipientId, CharSequence charSequence) {
        return Companion.editor(context, messageSendType, list, recipientId, charSequence);
    }

    @JvmStatic
    public static final Intent gallery(Context context, MessageSendType messageSendType, List<? extends Media> list, RecipientId recipientId, CharSequence charSequence, boolean z) {
        return Companion.gallery(context, messageSendType, list, recipientId, charSequence, z);
    }

    @JvmStatic
    public static final Intent share(Context context, MessageSendType messageSendType, List<? extends Media> list, List<? extends ContactSearchKey.RecipientSearchKey> list2, CharSequence charSequence, boolean z) {
        return Companion.share(context, messageSendType, list, list2, charSequence, z);
    }

    public MediaSelectionActivity() {
        Function0 function0 = MediaSelectionActivity$textViewModel$2.INSTANCE;
        this.textViewModel$delegate = new ViewModelLazy(Reflection.getOrCreateKotlinClass(TextStoryPostCreationViewModel.class), new Function0<ViewModelStore>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$special$$inlined$viewModels$2
            final /* synthetic */ ComponentActivity $this_viewModels;

            {
                this.$this_viewModels = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStore invoke() {
                ViewModelStore viewModelStore = this.$this_viewModels.getViewModelStore();
                Intrinsics.checkNotNullExpressionValue(viewModelStore, "viewModelStore");
                return viewModelStore;
            }
        }, function0 == null ? new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$special$$inlined$viewModels$1
            final /* synthetic */ ComponentActivity $this_viewModels;

            {
                this.$this_viewModels = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelProvider.Factory invoke() {
                return this.$this_viewModels.getDefaultViewModelProviderFactory();
            }
        } : function0);
    }

    public final MediaSelectionViewModel getViewModel() {
        MediaSelectionViewModel mediaSelectionViewModel = this.viewModel;
        if (mediaSelectionViewModel != null) {
            return mediaSelectionViewModel;
        }
        Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        return null;
    }

    public final void setViewModel(MediaSelectionViewModel mediaSelectionViewModel) {
        Intrinsics.checkNotNullParameter(mediaSelectionViewModel, "<set-?>");
        this.viewModel = mediaSelectionViewModel;
    }

    private final TextStoryPostCreationViewModel getTextViewModel() {
        return (TextStoryPostCreationViewModel) this.textViewModel$delegate.getValue();
    }

    private final MediaSelectionDestination getDestination() {
        MediaSelectionDestination.Companion companion = MediaSelectionDestination.Companion;
        Bundle bundleExtra = getIntent().getBundleExtra(DESTINATION);
        if (bundleExtra != null) {
            Intrinsics.checkNotNullExpressionValue(bundleExtra, "requireNotNull(intent.getBundleExtra(DESTINATION))");
            return companion.fromBundle(bundleExtra);
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    private final boolean isStory() {
        return getIntent().getBooleanExtra("is_story", false);
    }

    public final boolean getShareToTextStory() {
        return getIntent().getBooleanExtra(AS_TEXT_STORY, false);
    }

    private final CharSequence getDraftText() {
        return getIntent().getCharSequenceExtra(MESSAGE);
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        Intrinsics.checkNotNullParameter(context, "newBase");
        getDelegate().setLocalNightMode(2);
        super.attachBaseContext(context);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.media_selection_activity);
        Parcelable parcelableExtra = getIntent().getParcelableExtra(MESSAGE_SEND_TYPE);
        if (parcelableExtra != null) {
            MessageSendType messageSendType = (MessageSendType) parcelableExtra;
            List parcelableArrayListExtra = getIntent().getParcelableArrayListExtra(MEDIA);
            if (parcelableArrayListExtra == null) {
                parcelableArrayListExtra = CollectionsKt__CollectionsKt.emptyList();
            }
            ViewModel viewModel = new ViewModelProvider(this, new MediaSelectionViewModel.Factory(getDestination(), messageSendType, parcelableArrayListExtra, getShareToTextStory() ? null : getDraftText(), getIntent().getBooleanExtra(IS_REPLY, false), isStory(), new MediaSelectionRepository(this))).get(MediaSelectionViewModel.class);
            Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this, …ionViewModel::class.java]");
            setViewModel((MediaSelectionViewModel) viewModel);
            View findViewById = findViewById(R.id.switch_widget);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.switch_widget)");
            ConstraintLayout constraintLayout = (ConstraintLayout) findViewById;
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout);
            ConstraintSet constraintSet2 = new ConstraintSet();
            constraintSet2.clone(this, R.layout.media_selection_activity_text_selected_constraints);
            View findViewById2 = findViewById(R.id.text_switch);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.text_switch)");
            TextView textView = (TextView) findViewById2;
            View findViewById3 = findViewById(R.id.camera_switch);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.camera_switch)");
            TextView textView2 = (TextView) findViewById3;
            textView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$$ExternalSyntheticLambda5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MediaSelectionActivity.m2094onCreate$lambda2(MediaSelectionActivity.this, view);
                }
            });
            textView2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$$ExternalSyntheticLambda6
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MediaSelectionActivity.m2095onCreate$lambda3(MediaSelectionActivity.this, view);
                }
            });
            if (bundle == null) {
                if (getShareToTextStory()) {
                    initializeTextStory();
                }
                textView2.setSelected(true);
                NavHostFragment create = NavHostFragment.create(R.navigation.media);
                Intrinsics.checkNotNullExpressionValue(create, "create(R.navigation.media)");
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, create, NAV_HOST_TAG).commitNowAllowingStateLoss();
                navigateToStartDestination$default(this, null, 1, null);
            } else {
                getViewModel().onRestoreState(bundle);
                getTextViewModel().restoreFromInstanceState(bundle);
            }
            Fragment findFragmentByTag = getSupportFragmentManager().findFragmentByTag(NAV_HOST_TAG);
            if (findFragmentByTag != null) {
                ((NavHostFragment) findFragmentByTag).getNavController().addOnDestinationChangedListener(new NavController.OnDestinationChangedListener(this, textView2, textView, constraintSet, constraintSet2) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$$ExternalSyntheticLambda7
                    public final /* synthetic */ MediaSelectionActivity f$1;
                    public final /* synthetic */ TextView f$2;
                    public final /* synthetic */ TextView f$3;
                    public final /* synthetic */ ConstraintSet f$4;
                    public final /* synthetic */ ConstraintSet f$5;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                        this.f$3 = r4;
                        this.f$4 = r5;
                        this.f$5 = r6;
                    }

                    @Override // androidx.navigation.NavController.OnDestinationChangedListener
                    public final void onDestinationChanged(NavController navController, NavDestination navDestination, Bundle bundle2) {
                        MediaSelectionActivity.m2096onCreate$lambda4(ConstraintLayout.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, navController, navDestination, bundle2);
                    }
                });
                getOnBackPressedDispatcher().addCallback(new OnBackPressed());
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type androidx.navigation.fragment.NavHostFragment");
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    /* renamed from: onCreate$lambda-2 */
    public static final void m2094onCreate$lambda2(MediaSelectionActivity mediaSelectionActivity, View view) {
        Intrinsics.checkNotNullParameter(mediaSelectionActivity, "this$0");
        mediaSelectionActivity.getViewModel().sendCommand(HudCommand.GoToText.INSTANCE);
    }

    /* renamed from: onCreate$lambda-3 */
    public static final void m2095onCreate$lambda3(MediaSelectionActivity mediaSelectionActivity, View view) {
        Intrinsics.checkNotNullParameter(mediaSelectionActivity, "this$0");
        mediaSelectionActivity.getViewModel().sendCommand(HudCommand.GoToCapture.INSTANCE);
    }

    /* renamed from: onCreate$lambda-4 */
    public static final void m2096onCreate$lambda4(ConstraintLayout constraintLayout, MediaSelectionActivity mediaSelectionActivity, TextView textView, TextView textView2, ConstraintSet constraintSet, ConstraintSet constraintSet2, NavController navController, NavDestination navDestination, Bundle bundle) {
        Intrinsics.checkNotNullParameter(constraintLayout, "$textStoryToggle");
        Intrinsics.checkNotNullParameter(mediaSelectionActivity, "this$0");
        Intrinsics.checkNotNullParameter(textView, "$cameraSwitch");
        Intrinsics.checkNotNullParameter(textView2, "$textSwitch");
        Intrinsics.checkNotNullParameter(constraintSet, "$cameraSelectedConstraintSet");
        Intrinsics.checkNotNullParameter(constraintSet2, "$textSelectedConstraintSet");
        Intrinsics.checkNotNullParameter(navController, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(navDestination, "d");
        int id = navDestination.getId();
        if (id == R.id.mediaCaptureFragment) {
            ViewExtensionsKt.setVisible(constraintLayout, mediaSelectionActivity.canDisplayStorySwitch());
            mediaSelectionActivity.animateTextStyling(textView, textView2, 200);
            TransitionManager.beginDelayedTransition(constraintLayout, new AutoTransition().setDuration(200L));
            constraintSet.applyTo(constraintLayout);
        } else if (id != R.id.textStoryPostCreationFragment) {
            ViewExtensionsKt.setVisible(constraintLayout, false);
        } else {
            ViewExtensionsKt.setVisible(constraintLayout, mediaSelectionActivity.canDisplayStorySwitch());
            mediaSelectionActivity.animateTextStyling(textView2, textView, 200);
            TransitionManager.beginDelayedTransition(constraintLayout, new AutoTransition().setDuration(200L));
            constraintSet2.applyTo(constraintLayout);
        }
    }

    private final void animateTextStyling(TextView textView, TextView textView2, long j) {
        int color = ContextCompat.getColor(this, R.color.signal_colorOnSurface);
        int color2 = ContextCompat.getColor(this, R.color.signal_colorSecondaryContainer);
        ValueAnimator valueAnimator = this.animateInShadowLayerValueAnimator;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        ValueAnimator valueAnimator2 = this.animateInTextColorValueAnimator;
        if (valueAnimator2 != null) {
            valueAnimator2.cancel();
        }
        ValueAnimator valueAnimator3 = this.animateOutShadowLayerValueAnimator;
        if (valueAnimator3 != null) {
            valueAnimator3.cancel();
        }
        ValueAnimator valueAnimator4 = this.animateOutTextColorValueAnimator;
        if (valueAnimator4 != null) {
            valueAnimator4.cancel();
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(textView.getShadowRadius(), 0.0f);
        ofFloat.setDuration(j);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(textView) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$$ExternalSyntheticLambda1
            public final /* synthetic */ TextView f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator5) {
                MediaSelectionActivity.m2091animateTextStyling$lambda6$lambda5(this.f$0, valueAnimator5);
            }
        });
        ofFloat.start();
        this.animateInShadowLayerValueAnimator = ofFloat;
        ValueAnimator ofObject = ValueAnimator.ofObject(new ArgbEvaluatorCompat(), Integer.valueOf(textView.getCurrentTextColor()), Integer.valueOf(color2));
        ofObject.setEvaluator(ArgbEvaluatorCompat.getInstance());
        ofObject.setDuration(j);
        ofObject.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(textView) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$$ExternalSyntheticLambda2
            public final /* synthetic */ TextView f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator5) {
                MediaSelectionActivity.m2092animateTextStyling$lambda8$lambda7(this.f$0, valueAnimator5);
            }
        });
        ofObject.start();
        this.animateInTextColorValueAnimator = ofObject;
        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(textView2.getShadowRadius(), 3.0f);
        ofFloat2.setDuration(j);
        ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(textView2) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$$ExternalSyntheticLambda3
            public final /* synthetic */ TextView f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator5) {
                MediaSelectionActivity.m2089animateTextStyling$lambda10$lambda9(this.f$0, valueAnimator5);
            }
        });
        ofFloat2.start();
        this.animateOutShadowLayerValueAnimator = ofFloat2;
        ValueAnimator ofObject2 = ValueAnimator.ofObject(new ArgbEvaluatorCompat(), Integer.valueOf(textView2.getCurrentTextColor()), Integer.valueOf(color));
        ofObject2.setEvaluator(ArgbEvaluatorCompat.getInstance());
        ofObject2.setDuration(j);
        ofObject2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(textView2) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$$ExternalSyntheticLambda4
            public final /* synthetic */ TextView f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator5) {
                MediaSelectionActivity.m2090animateTextStyling$lambda12$lambda11(this.f$0, valueAnimator5);
            }
        });
        ofObject2.start();
        this.animateOutTextColorValueAnimator = ofObject2;
    }

    /* renamed from: animateTextStyling$lambda-6$lambda-5 */
    public static final void m2091animateTextStyling$lambda6$lambda5(TextView textView, ValueAnimator valueAnimator) {
        Intrinsics.checkNotNullParameter(textView, "$selectedSwitch");
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            textView.setShadowLayer(((Float) animatedValue).floatValue(), 0.0f, 0.0f, -16777216);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
    }

    /* renamed from: animateTextStyling$lambda-8$lambda-7 */
    public static final void m2092animateTextStyling$lambda8$lambda7(TextView textView, ValueAnimator valueAnimator) {
        Intrinsics.checkNotNullParameter(textView, "$selectedSwitch");
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            textView.setTextColor(((Integer) animatedValue).intValue());
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Int");
    }

    /* renamed from: animateTextStyling$lambda-10$lambda-9 */
    public static final void m2089animateTextStyling$lambda10$lambda9(TextView textView, ValueAnimator valueAnimator) {
        Intrinsics.checkNotNullParameter(textView, "$unselectedSwitch");
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            textView.setShadowLayer(((Float) animatedValue).floatValue(), 0.0f, 0.0f, -16777216);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
    }

    /* renamed from: animateTextStyling$lambda-12$lambda-11 */
    public static final void m2090animateTextStyling$lambda12$lambda11(TextView textView, ValueAnimator valueAnimator) {
        Intrinsics.checkNotNullParameter(textView, "$unselectedSwitch");
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            textView.setTextColor(((Integer) animatedValue).intValue());
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Int");
    }

    private final void initializeTextStory() {
        String obj;
        CharSequence draftText = getDraftText();
        if (draftText != null && (obj = draftText.toString()) != null) {
            String str = (String) LinkPreviewUtil.findValidPreviewUrls(obj).findFirst().map(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$$ExternalSyntheticLambda0
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj2) {
                    return MediaSelectionActivity.m2093initializeTextStory$lambda13((Link) obj2);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).orElse(null);
            BreakIteratorCompat instance = BreakIteratorCompat.getInstance();
            instance.setText(obj);
            String obj2 = instance.take(Stories.MAX_BODY_SIZE).toString();
            if (Intrinsics.areEqual(str, obj)) {
                getTextViewModel().setLinkPreview(str);
            } else if (str != null) {
                getTextViewModel().setLinkPreview(str);
                getTextViewModel().setBody(StringsKt__StringsKt.trim(StringsKt__StringsJVMKt.replace$default(obj2, str, "", false, 4, (Object) null)).toString());
            } else {
                getTextViewModel().setBody(StringsKt__StringsKt.trim(obj2).toString());
            }
        }
    }

    /* renamed from: initializeTextStory$lambda-13 */
    public static final String m2093initializeTextStory$lambda13(Link link) {
        return link.getUrl();
    }

    private final boolean canDisplayStorySwitch() {
        return Stories.isFeatureEnabled() && isCameraFirst() && !getViewModel().hasSelectedMedia() && Intrinsics.areEqual(getDestination(), MediaSelectionDestination.ChooseAfterMediaSelection.INSTANCE);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "outState");
        super.onSaveInstanceState(bundle);
        getViewModel().onSaveState(bundle);
        getTextViewModel().saveToInstanceState(bundle);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment.Callback
    public void onSentWithResult(MediaSendActivityResult mediaSendActivityResult) {
        Intrinsics.checkNotNullParameter(mediaSendActivityResult, "mediaSendActivityResult");
        Intent intent = new Intent();
        intent.putExtra(MediaSendActivityResult.EXTRA_RESULT, mediaSendActivityResult);
        Unit unit = Unit.INSTANCE;
        setResult(-1, intent);
        finish();
        overridePendingTransition(R.anim.stationary, R.anim.camera_slide_to_bottom);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment.Callback
    public void onSentWithoutResult() {
        setResult(-1, new Intent());
        finish();
        overridePendingTransition(R.anim.stationary, R.anim.camera_slide_to_bottom);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment.Callback
    public void onSendError(Throwable th) {
        Intrinsics.checkNotNullParameter(th, "error");
        if (th instanceof UntrustedRecords.UntrustedRecordsException) {
            Log.w(TAG, "Send failed due to untrusted identities.");
            UntrustedRecords.UntrustedRecordsException untrustedRecordsException = (UntrustedRecords.UntrustedRecordsException) th;
            SafetyNumberBottomSheet.Factory forIdentityRecordsAndDestinations = SafetyNumberBottomSheet.forIdentityRecordsAndDestinations(untrustedRecordsException.getUntrustedRecords(), CollectionsKt___CollectionsKt.toList(untrustedRecordsException.getDestinations()));
            FragmentManager supportFragmentManager = getSupportFragmentManager();
            Intrinsics.checkNotNullExpressionValue(supportFragmentManager, "supportFragmentManager");
            forIdentityRecordsAndDestinations.show(supportFragmentManager);
            return;
        }
        setResult(0);
        Log.w(TAG, "Failed to send message.", th);
        finish();
        overridePendingTransition(R.anim.stationary, R.anim.camera_slide_to_bottom);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment.Callback
    public void onNoMediaSelected() {
        Log.w(TAG, "No media selected. Exiting.");
        setResult(0);
        finish();
        overridePendingTransition(R.anim.stationary, R.anim.camera_slide_to_bottom);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment.Callback
    public void onPopFromReview() {
        if (isCameraFirst()) {
            getViewModel().removeCameraFirstCapture();
        }
        if (!navigateToStartDestination$default(this, null, 1, null)) {
            finish();
        }
    }

    static /* synthetic */ boolean navigateToStartDestination$default(MediaSelectionActivity mediaSelectionActivity, NavHostFragment navHostFragment, int i, Object obj) {
        if ((i & 1) != 0) {
            navHostFragment = null;
        }
        return mediaSelectionActivity.navigateToStartDestination(navHostFragment);
    }

    private final boolean navigateToStartDestination(NavHostFragment navHostFragment) {
        if (navHostFragment == null) {
            Fragment findFragmentByTag = getSupportFragmentManager().findFragmentByTag(NAV_HOST_TAG);
            if (findFragmentByTag != null) {
                navHostFragment = (NavHostFragment) findFragmentByTag;
            } else {
                throw new NullPointerException("null cannot be cast to non-null type androidx.navigation.fragment.NavHostFragment");
            }
        }
        int intExtra = getIntent().getIntExtra(START_ACTION, -1);
        if (intExtra <= 0) {
            return false;
        }
        NavController navController = navHostFragment.getNavController();
        Intrinsics.checkNotNullExpressionValue(navController, "hostFragment.navController");
        Bundle bundle = new Bundle();
        bundle.putBoolean("first", true);
        Unit unit = Unit.INSTANCE;
        SafeNavigation.safeNavigate(navController, intExtra, bundle);
        return true;
    }

    private final boolean isCameraFirst() {
        return getIntent().getIntExtra(START_ACTION, -1) == R.id.action_directly_to_mediaCaptureFragment;
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment.Callback
    public void openEmojiSearch() {
        getViewModel().sendCommand(HudCommand.OpenEmojiSearch.INSTANCE);
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onEmojiSelected(String str) {
        getViewModel().sendCommand(new HudCommand.EmojiInsert(str));
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onKeyEvent(KeyEvent keyEvent) {
        getViewModel().sendCommand(new HudCommand.EmojiKeyEvent(keyEvent));
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment.Callback
    public void closeEmojiSearch() {
        getViewModel().sendCommand(HudCommand.CloseEmojiSearch.INSTANCE);
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider
    public ContactSearchConfiguration getSearchConfiguration(FragmentManager fragmentManager, ContactSearchState contactSearchState) {
        Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
        Intrinsics.checkNotNullParameter(contactSearchState, "contactSearchState");
        if (isStory()) {
            return ContactSearchConfiguration.Companion.build(new Function1<ContactSearchConfiguration.Builder, Unit>(contactSearchState, fragmentManager) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity$getSearchConfiguration$1
                final /* synthetic */ ContactSearchState $contactSearchState;
                final /* synthetic */ FragmentManager $fragmentManager;

                /* access modifiers changed from: package-private */
                {
                    this.$contactSearchState = r1;
                    this.$fragmentManager = r2;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(ContactSearchConfiguration.Builder builder) {
                    invoke(builder);
                    return Unit.INSTANCE;
                }

                public final void invoke(ContactSearchConfiguration.Builder builder) {
                    Intrinsics.checkNotNullParameter(builder, "$this$build");
                    builder.setQuery(this.$contactSearchState.getQuery());
                    builder.addSection(new ContactSearchConfiguration.Section.Stories(this.$contactSearchState.getGroupStories(), true, Stories.INSTANCE.getHeaderAction(this.$fragmentManager), null, 8, null));
                }
            });
        }
        return null;
    }

    /* compiled from: MediaSelectionActivity.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionActivity$OnBackPressed;", "Landroidx/activity/OnBackPressedCallback;", "(Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionActivity;)V", "handleOnBackPressed", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private final class OnBackPressed extends OnBackPressedCallback {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OnBackPressed() {
            super(true);
            MediaSelectionActivity.this = r1;
        }

        @Override // androidx.activity.OnBackPressedCallback
        public void handleOnBackPressed() {
            NavController findNavController = Navigation.findNavController(MediaSelectionActivity.this, R.id.fragment_container);
            Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(this@M… R.id.fragment_container)");
            if (MediaSelectionActivity.this.getShareToTextStory()) {
                NavDestination currentDestination = findNavController.getCurrentDestination();
                boolean z = false;
                if (currentDestination != null && currentDestination.getId() == R.id.textStoryPostCreationFragment) {
                    z = true;
                }
                if (z) {
                    MediaSelectionActivity.this.finish();
                }
            }
            if (!findNavController.popBackStack()) {
                MediaSelectionActivity.this.finish();
            }
        }
    }

    /* compiled from: MediaSelectionActivity.kt */
    @Metadata(d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002Jh\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u00142\b\b\u0002\u0010\u0015\u001a\u00020\u00162\u000e\b\u0002\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u00182\b\b\u0002\u0010\u001a\u001a\u00020\u001b2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\b\b\u0002\u0010\u001e\u001a\u00020\u001f2\b\b\u0002\u0010 \u001a\u00020\u001f2\b\b\u0002\u0010!\u001a\u00020\u001fH\u0002J\u0010\u0010\"\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0007J\u0018\u0010\"\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010 \u001a\u00020\u001fH\u0007J(\u0010\"\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010#\u001a\u00020$2\u0006\u0010\u001e\u001a\u00020\u001fH\u0007J8\u0010%\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00162\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u00182\u0006\u0010#\u001a\u00020$2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0007J@\u0010&\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00162\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u00182\u0006\u0010#\u001a\u00020$2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0007JF\u0010'\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00162\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u00182\f\u0010(\u001a\b\u0012\u0004\u0012\u00020)0\u00182\b\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0006\u0010!\u001a\u00020\u001fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n \u000e*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionActivity$Companion;", "", "()V", "AS_TEXT_STORY", "", "DESTINATION", "IS_REPLY", "IS_STORY", "MEDIA", "MESSAGE", "MESSAGE_SEND_TYPE", "NAV_HOST_TAG", "START_ACTION", "TAG", "kotlin.jvm.PlatformType", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "startAction", "", "messageSendType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", MediaSelectionActivity.MEDIA, "", "Lorg/thoughtcrime/securesms/mediasend/Media;", MediaSelectionActivity.DESTINATION, "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", MediaSelectionActivity.MESSAGE, "", "isReply", "", "isStory", "asTextStory", "camera", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "editor", "gallery", "share", "recipientSearchKeys", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final Intent camera(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return camera(context, false);
        }

        @JvmStatic
        public final Intent camera(Context context, boolean z) {
            Intrinsics.checkNotNullParameter(context, "context");
            return buildIntent$default(this, context, R.id.action_directly_to_mediaCaptureFragment, null, null, null, null, false, z, false, 380, null);
        }

        @JvmStatic
        public final Intent camera(Context context, MessageSendType messageSendType, RecipientId recipientId, boolean z) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(messageSendType, "messageSendType");
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return buildIntent$default(this, context, R.id.action_directly_to_mediaCaptureFragment, messageSendType, null, new MediaSelectionDestination.SingleRecipient(recipientId), null, z, false, false, 424, null);
        }

        @JvmStatic
        public final Intent gallery(Context context, MessageSendType messageSendType, List<? extends Media> list, RecipientId recipientId, CharSequence charSequence, boolean z) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(messageSendType, "messageSendType");
            Intrinsics.checkNotNullParameter(list, MediaSelectionActivity.MEDIA);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return buildIntent$default(this, context, R.id.action_directly_to_mediaGalleryFragment, messageSendType, list, new MediaSelectionDestination.SingleRecipient(recipientId), charSequence, z, false, false, 384, null);
        }

        @JvmStatic
        public final Intent editor(Context context, MessageSendType messageSendType, List<? extends Media> list, RecipientId recipientId, CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(messageSendType, "messageSendType");
            Intrinsics.checkNotNullParameter(list, MediaSelectionActivity.MEDIA);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return buildIntent$default(this, context, 0, messageSendType, list, new MediaSelectionDestination.SingleRecipient(recipientId), charSequence, false, false, false, 450, null);
        }

        @JvmStatic
        public final Intent share(Context context, MessageSendType messageSendType, List<? extends Media> list, List<? extends ContactSearchKey.RecipientSearchKey> list2, CharSequence charSequence, boolean z) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(messageSendType, "messageSendType");
            Intrinsics.checkNotNullParameter(list, MediaSelectionActivity.MEDIA);
            Intrinsics.checkNotNullParameter(list2, "recipientSearchKeys");
            return buildIntent$default(this, context, z ? R.id.action_directly_to_textPostCreationFragment : -1, messageSendType, list, new MediaSelectionDestination.MultipleRecipients(list2), charSequence, false, false, z, 192, null);
        }

        static /* synthetic */ Intent buildIntent$default(Companion companion, Context context, int i, MessageSendType messageSendType, List list, MediaSelectionDestination mediaSelectionDestination, CharSequence charSequence, boolean z, boolean z2, boolean z3, int i2, Object obj) {
            MediaSelectionDestination mediaSelectionDestination2;
            int i3 = (i2 & 2) != 0 ? -1 : i;
            MessageSendType messageSendType2 = (i2 & 4) != 0 ? MessageSendType.SignalMessageSendType.INSTANCE : messageSendType;
            List list2 = (i2 & 8) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list;
            if ((i2 & 16) != 0) {
                mediaSelectionDestination2 = MediaSelectionDestination.ChooseAfterMediaSelection.INSTANCE;
            } else {
                mediaSelectionDestination2 = mediaSelectionDestination;
            }
            CharSequence charSequence2 = (i2 & 32) != 0 ? null : charSequence;
            boolean z4 = false;
            boolean z5 = (i2 & 64) != 0 ? false : z;
            boolean z6 = (i2 & 128) != 0 ? false : z2;
            if ((i2 & 256) == 0) {
                z4 = z3;
            }
            return companion.buildIntent(context, i3, messageSendType2, list2, mediaSelectionDestination2, charSequence2, z5, z6, z4);
        }

        private final Intent buildIntent(Context context, int i, MessageSendType messageSendType, List<? extends Media> list, MediaSelectionDestination mediaSelectionDestination, CharSequence charSequence, boolean z, boolean z2, boolean z3) {
            Intent intent = new Intent(context, MediaSelectionActivity.class);
            intent.putExtra(MediaSelectionActivity.START_ACTION, i);
            intent.putExtra(MediaSelectionActivity.MESSAGE_SEND_TYPE, messageSendType);
            intent.putParcelableArrayListExtra(MediaSelectionActivity.MEDIA, new ArrayList<>(list));
            intent.putExtra(MediaSelectionActivity.MESSAGE, charSequence);
            intent.putExtra(MediaSelectionActivity.DESTINATION, mediaSelectionDestination.toBundle());
            intent.putExtra(MediaSelectionActivity.IS_REPLY, z);
            intent.putExtra("is_story", z2);
            intent.putExtra(MediaSelectionActivity.AS_TEXT_STORY, z3);
            return intent;
        }
    }
}
