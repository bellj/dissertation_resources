package org.thoughtcrime.securesms.mediasend.v2.stories;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchState;

/* compiled from: ChooseGroupStoryBottomSheet.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "state", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchState;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class ChooseGroupStoryBottomSheet$onViewCreated$2 extends Lambda implements Function1<ContactSearchState, ContactSearchConfiguration> {
    public static final ChooseGroupStoryBottomSheet$onViewCreated$2 INSTANCE = new ChooseGroupStoryBottomSheet$onViewCreated$2();

    ChooseGroupStoryBottomSheet$onViewCreated$2() {
        super(1);
    }

    public final ContactSearchConfiguration invoke(final ContactSearchState contactSearchState) {
        Intrinsics.checkNotNullParameter(contactSearchState, "state");
        return ContactSearchConfiguration.Companion.build(new Function1<ContactSearchConfiguration.Builder, Unit>() { // from class: org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet$onViewCreated$2.1
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(ContactSearchConfiguration.Builder builder) {
                invoke(builder);
                return Unit.INSTANCE;
            }

            public final void invoke(ContactSearchConfiguration.Builder builder) {
                Intrinsics.checkNotNullParameter(builder, "$this$build");
                builder.setQuery(contactSearchState.getQuery());
                builder.addSection(new ContactSearchConfiguration.Section.Groups(false, false, false, true, false, null, 39, null));
            }
        });
    }
}
