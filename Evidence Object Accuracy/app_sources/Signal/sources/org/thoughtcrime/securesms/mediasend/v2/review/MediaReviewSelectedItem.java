package org.thoughtcrime.securesms.mediasend.v2.review;

import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: MediaReviewSelectedItem.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\f\rB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J,\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u001c\u0010\u0007\u001a\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00040\bj\u0002`\u000b¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewSelectedItem;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "onSelectedMediaClicked", "Lkotlin/Function2;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "", "Lorg/thoughtcrime/securesms/mediasend/v2/review/OnSelectedMediaClicked;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaReviewSelectedItem {
    public static final MediaReviewSelectedItem INSTANCE = new MediaReviewSelectedItem();

    private MediaReviewSelectedItem() {
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m2224register$lambda0(Function2 function2, View view) {
        Intrinsics.checkNotNullParameter(function2, "$onSelectedMediaClicked");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view, function2);
    }

    public final void register(MappingAdapter mappingAdapter, Function2<? super Media, ? super Boolean, Unit> function2) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        Intrinsics.checkNotNullParameter(function2, "onSelectedMediaClicked");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewSelectedItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MediaReviewSelectedItem.m2224register$lambda0(Function2.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.v2_media_review_selected_item));
    }

    /* compiled from: MediaReviewSelectedItem.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u0000H\u0016J\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u0000H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewSelectedItem$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "media", "Lorg/thoughtcrime/securesms/mediasend/Media;", "isSelected", "", "(Lorg/thoughtcrime/securesms/mediasend/Media;Z)V", "()Z", "getMedia", "()Lorg/thoughtcrime/securesms/mediasend/Media;", "areContentsTheSame", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model implements MappingModel<Model> {
        private final boolean isSelected;
        private final Media media;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        public Model(Media media, boolean z) {
            Intrinsics.checkNotNullParameter(media, "media");
            this.media = media;
            this.isSelected = z;
        }

        public final Media getMedia() {
            return this.media;
        }

        public final boolean isSelected() {
            return this.isSelected;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.media, model.media);
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.media, model.media) && this.isSelected == model.isSelected;
        }
    }

    /* compiled from: MediaReviewSelectedItem.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B+\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u001c\u0010\u0005\u001a\u0018\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0006j\u0002`\n¢\u0006\u0002\u0010\u000bJ\u0010\u0010\u0010\u001a\u00020\t2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R$\u0010\u0005\u001a\u0018\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0006j\u0002`\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewSelectedItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewSelectedItem$Model;", "itemView", "Landroid/view/View;", "onSelectedMediaClicked", "Lkotlin/Function2;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "", "", "Lorg/thoughtcrime/securesms/mediasend/v2/review/OnSelectedMediaClicked;", "(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V", "imageView", "Landroid/widget/ImageView;", "playOverlay", "trashOverlay", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final ImageView imageView;
        private final Function2<Media, Boolean, Unit> onSelectedMediaClicked;
        private final ImageView playOverlay;
        private final ImageView trashOverlay;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function2<? super org.thoughtcrime.securesms.mediasend.Media, ? super java.lang.Boolean, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view, Function2<? super Media, ? super Boolean, Unit> function2) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function2, "onSelectedMediaClicked");
            this.onSelectedMediaClicked = function2;
            View findViewById = view.findViewById(R.id.media_review_selected_image);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.…ia_review_selected_image)");
            this.imageView = (ImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.media_review_play_overlay);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.…edia_review_play_overlay)");
            this.playOverlay = (ImageView) findViewById2;
            View findViewById3 = view.findViewById(R.id.media_review_trash_overlay);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.…dia_review_trash_overlay)");
            this.trashOverlay = (ImageView) findViewById3;
        }

        public void bind(Model model) {
            String str;
            Intrinsics.checkNotNullParameter(model, "model");
            Glide.with(this.imageView).load((Object) new DecryptableStreamUriLoader.DecryptableUri(model.getMedia().getUri())).centerCrop().into(this.imageView);
            ViewExtensionsKt.setVisible(this.playOverlay, MediaUtil.isNonGifVideo(model.getMedia()) && !model.isSelected());
            ViewExtensionsKt.setVisible(this.trashOverlay, model.isSelected());
            View view = this.itemView;
            if (model.isSelected()) {
                str = this.context.getString(R.string.MediaReviewSelectedItem__tap_to_remove);
            } else {
                str = this.context.getString(R.string.MediaReviewSelectedItem__tap_to_select);
            }
            view.setContentDescription(str);
            this.itemView.setOnClickListener(new MediaReviewSelectedItem$ViewHolder$$ExternalSyntheticLambda0(this, model));
        }

        /* renamed from: bind$lambda-0 */
        public static final void m2225bind$lambda0(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            viewHolder.onSelectedMediaClicked.invoke(model.getMedia(), Boolean.valueOf(model.isSelected()));
        }
    }
}
