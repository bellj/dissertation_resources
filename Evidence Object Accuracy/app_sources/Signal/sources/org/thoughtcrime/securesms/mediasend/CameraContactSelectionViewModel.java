package org.thoughtcrime.securesms.mediasend;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.mediasend.CameraContactsRepository;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* loaded from: classes4.dex */
public class CameraContactSelectionViewModel extends ViewModel {
    static final int MAX_SELECTION_COUNT;
    private final MutableLiveData<ContactState> contacts;
    private String currentQuery;
    private final SingleLiveEvent<Error> error;
    private final CameraContactsRepository repository;
    private final Set<Recipient> selected;

    /* loaded from: classes4.dex */
    public enum Error {
        MAX_SELECTION
    }

    private CameraContactSelectionViewModel(CameraContactsRepository cameraContactsRepository) {
        this.repository = cameraContactsRepository;
        this.contacts = new MutableLiveData<>();
        this.error = new SingleLiveEvent<>();
        this.selected = new LinkedHashSet();
        cameraContactsRepository.getCameraContacts(new CameraContactsRepository.Callback() { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.mediasend.CameraContactsRepository.Callback
            public final void onComplete(Object obj) {
                CameraContactSelectionViewModel.this.lambda$new$1((CameraContacts) obj);
            }
        });
    }

    public /* synthetic */ void lambda$new$1(CameraContacts cameraContacts) {
        ThreadUtil.runOnMain(new Runnable(cameraContacts) { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ CameraContacts f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CameraContactSelectionViewModel.this.lambda$new$0(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(CameraContacts cameraContacts) {
        this.contacts.postValue(new ContactState(cameraContacts, new ArrayList(this.selected), this.currentQuery));
    }

    public LiveData<ContactState> getContacts() {
        return this.contacts;
    }

    public LiveData<Error> getError() {
        return this.error;
    }

    public void onSearchClosed() {
        onQueryUpdated("");
    }

    public void onQueryUpdated(String str) {
        this.currentQuery = str;
        this.repository.getCameraContacts(str, new CameraContactsRepository.Callback(str) { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionViewModel$$ExternalSyntheticLambda4
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.mediasend.CameraContactsRepository.Callback
            public final void onComplete(Object obj) {
                CameraContactSelectionViewModel.this.lambda$onQueryUpdated$3(this.f$1, (CameraContacts) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onQueryUpdated$3(String str, CameraContacts cameraContacts) {
        ThreadUtil.runOnMain(new Runnable(cameraContacts, str) { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ CameraContacts f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CameraContactSelectionViewModel.this.lambda$onQueryUpdated$2(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onQueryUpdated$2(CameraContacts cameraContacts, String str) {
        this.contacts.postValue(new ContactState(cameraContacts, new ArrayList(this.selected), str));
    }

    void onRefresh() {
        this.repository.getCameraContacts(new CameraContactsRepository.Callback() { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionViewModel$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.mediasend.CameraContactsRepository.Callback
            public final void onComplete(Object obj) {
                CameraContactSelectionViewModel.this.lambda$onRefresh$5((CameraContacts) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onRefresh$5(CameraContacts cameraContacts) {
        ThreadUtil.runOnMain(new Runnable(cameraContacts) { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ CameraContacts f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CameraContactSelectionViewModel.this.lambda$onRefresh$4(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onRefresh$4(CameraContacts cameraContacts) {
        this.contacts.postValue(new ContactState(cameraContacts, new ArrayList(this.selected), this.currentQuery));
    }

    public void onContactClicked(Recipient recipient) {
        if (this.selected.contains(recipient)) {
            this.selected.remove(recipient);
        } else if (this.selected.size() < 16) {
            this.selected.add(recipient);
        } else {
            this.error.postValue(Error.MAX_SELECTION);
        }
        ContactState value = this.contacts.getValue();
        if (value != null) {
            this.contacts.setValue(new ContactState(value.getContacts(), new ArrayList(this.selected), this.currentQuery));
        }
    }

    /* loaded from: classes4.dex */
    public static class ContactState {
        private final CameraContacts contacts;
        private final String query;
        private final List<Recipient> selected;

        ContactState(CameraContacts cameraContacts, List<Recipient> list, String str) {
            this.contacts = cameraContacts;
            this.selected = list;
            this.query = str;
        }

        public CameraContacts getContacts() {
            return this.contacts;
        }

        public List<Recipient> getSelected() {
            return this.selected;
        }

        public String getQuery() {
            return this.query;
        }
    }

    /* loaded from: classes4.dex */
    static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final CameraContactsRepository repository;

        public Factory(CameraContactsRepository cameraContactsRepository) {
            this.repository = cameraContactsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new CameraContactSelectionViewModel(this.repository));
        }
    }
}
