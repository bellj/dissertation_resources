package org.thoughtcrime.securesms.mediasend;

import android.content.Context;
import j$.util.Optional;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.mms.SentMediaQuality;

/* loaded from: classes4.dex */
public final class SentMediaQualityTransform implements MediaTransform {
    private final SentMediaQuality sentMediaQuality;

    public SentMediaQualityTransform(SentMediaQuality sentMediaQuality) {
        this.sentMediaQuality = sentMediaQuality;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaTransform
    public Media transform(Context context, Media media) {
        return new Media(media.getUri(), media.getMimeType(), media.getDate(), media.getWidth(), media.getHeight(), media.getSize(), media.getDuration(), media.isBorderless(), media.isVideoGif(), media.getBucketId(), media.getCaption(), Optional.of(AttachmentDatabase.TransformProperties.forSentMediaQuality(media.getTransformProperties(), this.sentMediaQuality)));
    }
}
