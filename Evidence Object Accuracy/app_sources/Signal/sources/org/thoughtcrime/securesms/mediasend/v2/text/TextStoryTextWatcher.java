package org.thoughtcrime.securesms.mediasend.v2.text;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import org.signal.core.util.BreakIteratorCompat;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.EditTextUtil;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: TextStoryTextWatcher.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J(\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\fH\u0016J(\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryTextWatcher;", "Landroid/text/TextWatcher;", "textView", "Landroid/widget/TextView;", "(Landroid/widget/TextView;)V", "afterTextChanged", "", "s", "Landroid/text/Editable;", "beforeTextChanged", "", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "", NewHtcHomeBadger.COUNT, "after", "onTextChanged", "before", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryTextWatcher implements TextWatcher {
    public static final Companion Companion = new Companion(null);
    private final TextView textView;

    public /* synthetic */ TextStoryTextWatcher(TextView textView, DefaultConstructorMarker defaultConstructorMarker) {
        this(textView);
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        Intrinsics.checkNotNullParameter(editable, "s");
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        Intrinsics.checkNotNullParameter(charSequence, "s");
    }

    private TextStoryTextWatcher(TextView textView) {
        this.textView = textView;
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        Intrinsics.checkNotNullParameter(charSequence, "s");
        Companion.ensureProperTextSize(this.textView);
    }

    /* compiled from: TextStoryTextWatcher.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryTextWatcher$Companion;", "", "()V", "ensureProperTextSize", "", "textView", "Landroid/widget/TextView;", "install", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final void ensureProperTextSize(TextView textView) {
            Intrinsics.checkNotNullParameter(textView, "textView");
            BreakIteratorCompat instance = BreakIteratorCompat.getInstance();
            instance.setText(textView.getText());
            int countBreaks = instance.countBreaks();
            float f = countBreaks < 50 ? 34.0f : countBreaks < 200 ? 24.0f : 18.0f;
            if (f < 24.0f) {
                textView.setGravity(8388611);
            } else {
                textView.setGravity(17);
            }
            textView.setTextSize(0, DimensionUnit.DP.toPixels(f));
        }

        public final void install(TextView textView) {
            Intrinsics.checkNotNullParameter(textView, "textView");
            TextStoryTextWatcher textStoryTextWatcher = new TextStoryTextWatcher(textView, null);
            if (textView instanceof EditText) {
                EditTextUtil.addGraphemeClusterLimitFilter((EditText) textView, Stories.MAX_BODY_SIZE);
            }
            textView.addTextChangedListener(textStoryTextWatcher);
        }
    }
}
