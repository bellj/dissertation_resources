package org.thoughtcrime.securesms.mediasend.v2;

import android.net.Uri;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: MediaSelectionState.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b$\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001LB±\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0011\u0012\u0014\b\u0002\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00010\u0016\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0019\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u001b¢\u0006\u0002\u0010\u001cJ\t\u00107\u001a\u00020\u0003HÆ\u0003J\t\u00108\u001a\u00020\u0011HÆ\u0003J\t\u00109\u001a\u00020\u0011HÆ\u0003J\u0015\u0010:\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00010\u0016HÆ\u0003J\u000b\u0010;\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\t\u0010<\u001a\u00020\u0011HÆ\u0003J\t\u0010=\u001a\u00020\u001bHÆ\u0003J\u000f\u0010>\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\u000b\u0010?\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010@\u001a\u0004\u0018\u00010\tHÆ\u0003J\t\u0010A\u001a\u00020\u000bHÆ\u0003J\u000b\u0010B\u001a\u0004\u0018\u00010\rHÆ\u0003J\t\u0010C\u001a\u00020\u000fHÆ\u0003J\t\u0010D\u001a\u00020\u0011HÆ\u0003J\t\u0010E\u001a\u00020\u0011HÆ\u0003J¹\u0001\u0010F\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00112\b\b\u0002\u0010\u0013\u001a\u00020\u00112\b\b\u0002\u0010\u0014\u001a\u00020\u00112\u0014\b\u0002\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00010\u00162\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u0019\u001a\u00020\u00112\b\b\u0002\u0010\u001a\u001a\u00020\u001bHÆ\u0001J\u0013\u0010G\u001a\u00020\u00112\b\u0010H\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010I\u001a\u00020&HÖ\u0001J\t\u0010J\u001a\u00020KHÖ\u0001R\u0013\u0010\u0018\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u001f\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u001d\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00010\u0016¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u001eR\u0011\u0010\u0014\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010!R\u0011\u0010\u0013\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010!R\u0011\u0010\u0012\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010!R\u0011\u0010\u0019\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010!R\u0011\u0010\u0010\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010!R\u0011\u0010%\u001a\u00020&¢\u0006\b\n\u0000\u001a\u0004\b'\u0010(R\u0013\u0010\f\u001a\u0004\u0018\u00010\r¢\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b+\u0010,R\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b-\u0010.R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b/\u00100R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b1\u00102R\u0011\u0010\u001a\u001a\u00020\u001b¢\u0006\b\n\u0000\u001a\u0004\b3\u00104R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b5\u00106¨\u0006M"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionState;", "", "sendType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "selectedMedia", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "focusedMedia", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "quality", "Lorg/thoughtcrime/securesms/mms/SentMediaQuality;", "message", "", "viewOnceToggleState", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionState$ViewOnceToggleState;", "isTouchEnabled", "", "isSent", "isPreUploadEnabled", "isMeteredConnection", "editorStateMap", "", "Landroid/net/Uri;", "cameraFirstCapture", "isStory", "storySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "(Lorg/thoughtcrime/securesms/conversation/MessageSendType;Ljava/util/List;Lorg/thoughtcrime/securesms/mediasend/Media;Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/mms/SentMediaQuality;Ljava/lang/CharSequence;Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionState$ViewOnceToggleState;ZZZZLjava/util/Map;Lorg/thoughtcrime/securesms/mediasend/Media;ZLorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;)V", "getCameraFirstCapture", "()Lorg/thoughtcrime/securesms/mediasend/Media;", "canSend", "getCanSend", "()Z", "getEditorStateMap", "()Ljava/util/Map;", "getFocusedMedia", "maxSelection", "", "getMaxSelection", "()I", "getMessage", "()Ljava/lang/CharSequence;", "getQuality", "()Lorg/thoughtcrime/securesms/mms/SentMediaQuality;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getSelectedMedia", "()Ljava/util/List;", "getSendType", "()Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "getStorySendRequirements", "()Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "getViewOnceToggleState", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionState$ViewOnceToggleState;", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "", "ViewOnceToggleState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaSelectionState {
    private final Media cameraFirstCapture;
    private final boolean canSend;
    private final Map<Uri, Object> editorStateMap;
    private final Media focusedMedia;
    private final boolean isMeteredConnection;
    private final boolean isPreUploadEnabled;
    private final boolean isSent;
    private final boolean isStory;
    private final boolean isTouchEnabled;
    private final int maxSelection;
    private final CharSequence message;
    private final SentMediaQuality quality;
    private final Recipient recipient;
    private final List<Media> selectedMedia;
    private final MessageSendType sendType;
    private final Stories.MediaTransform.SendRequirements storySendRequirements;
    private final ViewOnceToggleState viewOnceToggleState;

    public final MessageSendType component1() {
        return this.sendType;
    }

    public final boolean component10() {
        return this.isPreUploadEnabled;
    }

    public final boolean component11() {
        return this.isMeteredConnection;
    }

    public final Map<Uri, Object> component12() {
        return this.editorStateMap;
    }

    public final Media component13() {
        return this.cameraFirstCapture;
    }

    public final boolean component14() {
        return this.isStory;
    }

    public final Stories.MediaTransform.SendRequirements component15() {
        return this.storySendRequirements;
    }

    public final List<Media> component2() {
        return this.selectedMedia;
    }

    public final Media component3() {
        return this.focusedMedia;
    }

    public final Recipient component4() {
        return this.recipient;
    }

    public final SentMediaQuality component5() {
        return this.quality;
    }

    public final CharSequence component6() {
        return this.message;
    }

    public final ViewOnceToggleState component7() {
        return this.viewOnceToggleState;
    }

    public final boolean component8() {
        return this.isTouchEnabled;
    }

    public final boolean component9() {
        return this.isSent;
    }

    public final MediaSelectionState copy(MessageSendType messageSendType, List<? extends Media> list, Media media, Recipient recipient, SentMediaQuality sentMediaQuality, CharSequence charSequence, ViewOnceToggleState viewOnceToggleState, boolean z, boolean z2, boolean z3, boolean z4, Map<Uri, ? extends Object> map, Media media2, boolean z5, Stories.MediaTransform.SendRequirements sendRequirements) {
        Intrinsics.checkNotNullParameter(messageSendType, "sendType");
        Intrinsics.checkNotNullParameter(list, "selectedMedia");
        Intrinsics.checkNotNullParameter(sentMediaQuality, "quality");
        Intrinsics.checkNotNullParameter(viewOnceToggleState, "viewOnceToggleState");
        Intrinsics.checkNotNullParameter(map, "editorStateMap");
        Intrinsics.checkNotNullParameter(sendRequirements, "storySendRequirements");
        return new MediaSelectionState(messageSendType, list, media, recipient, sentMediaQuality, charSequence, viewOnceToggleState, z, z2, z3, z4, map, media2, z5, sendRequirements);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaSelectionState)) {
            return false;
        }
        MediaSelectionState mediaSelectionState = (MediaSelectionState) obj;
        return Intrinsics.areEqual(this.sendType, mediaSelectionState.sendType) && Intrinsics.areEqual(this.selectedMedia, mediaSelectionState.selectedMedia) && Intrinsics.areEqual(this.focusedMedia, mediaSelectionState.focusedMedia) && Intrinsics.areEqual(this.recipient, mediaSelectionState.recipient) && this.quality == mediaSelectionState.quality && Intrinsics.areEqual(this.message, mediaSelectionState.message) && this.viewOnceToggleState == mediaSelectionState.viewOnceToggleState && this.isTouchEnabled == mediaSelectionState.isTouchEnabled && this.isSent == mediaSelectionState.isSent && this.isPreUploadEnabled == mediaSelectionState.isPreUploadEnabled && this.isMeteredConnection == mediaSelectionState.isMeteredConnection && Intrinsics.areEqual(this.editorStateMap, mediaSelectionState.editorStateMap) && Intrinsics.areEqual(this.cameraFirstCapture, mediaSelectionState.cameraFirstCapture) && this.isStory == mediaSelectionState.isStory && this.storySendRequirements == mediaSelectionState.storySendRequirements;
    }

    public int hashCode() {
        int hashCode = ((this.sendType.hashCode() * 31) + this.selectedMedia.hashCode()) * 31;
        Media media = this.focusedMedia;
        int i = 0;
        int hashCode2 = (hashCode + (media == null ? 0 : media.hashCode())) * 31;
        Recipient recipient = this.recipient;
        int hashCode3 = (((hashCode2 + (recipient == null ? 0 : recipient.hashCode())) * 31) + this.quality.hashCode()) * 31;
        CharSequence charSequence = this.message;
        int hashCode4 = (((hashCode3 + (charSequence == null ? 0 : charSequence.hashCode())) * 31) + this.viewOnceToggleState.hashCode()) * 31;
        boolean z = this.isTouchEnabled;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = (hashCode4 + i3) * 31;
        boolean z2 = this.isSent;
        if (z2) {
            z2 = true;
        }
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = z2 ? 1 : 0;
        int i10 = (i6 + i7) * 31;
        boolean z3 = this.isPreUploadEnabled;
        if (z3) {
            z3 = true;
        }
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = z3 ? 1 : 0;
        int i14 = (i10 + i11) * 31;
        boolean z4 = this.isMeteredConnection;
        if (z4) {
            z4 = true;
        }
        int i15 = z4 ? 1 : 0;
        int i16 = z4 ? 1 : 0;
        int i17 = z4 ? 1 : 0;
        int hashCode5 = (((i14 + i15) * 31) + this.editorStateMap.hashCode()) * 31;
        Media media2 = this.cameraFirstCapture;
        if (media2 != null) {
            i = media2.hashCode();
        }
        int i18 = (hashCode5 + i) * 31;
        boolean z5 = this.isStory;
        if (!z5) {
            i2 = z5 ? 1 : 0;
        }
        return ((i18 + i2) * 31) + this.storySendRequirements.hashCode();
    }

    public String toString() {
        return "MediaSelectionState(sendType=" + this.sendType + ", selectedMedia=" + this.selectedMedia + ", focusedMedia=" + this.focusedMedia + ", recipient=" + this.recipient + ", quality=" + this.quality + ", message=" + ((Object) this.message) + ", viewOnceToggleState=" + this.viewOnceToggleState + ", isTouchEnabled=" + this.isTouchEnabled + ", isSent=" + this.isSent + ", isPreUploadEnabled=" + this.isPreUploadEnabled + ", isMeteredConnection=" + this.isMeteredConnection + ", editorStateMap=" + this.editorStateMap + ", cameraFirstCapture=" + this.cameraFirstCapture + ", isStory=" + this.isStory + ", storySendRequirements=" + this.storySendRequirements + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r11v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.mediasend.Media> */
    /* JADX WARN: Multi-variable type inference failed */
    public MediaSelectionState(MessageSendType messageSendType, List<? extends Media> list, Media media, Recipient recipient, SentMediaQuality sentMediaQuality, CharSequence charSequence, ViewOnceToggleState viewOnceToggleState, boolean z, boolean z2, boolean z3, boolean z4, Map<Uri, ? extends Object> map, Media media2, boolean z5, Stories.MediaTransform.SendRequirements sendRequirements) {
        Intrinsics.checkNotNullParameter(messageSendType, "sendType");
        Intrinsics.checkNotNullParameter(list, "selectedMedia");
        Intrinsics.checkNotNullParameter(sentMediaQuality, "quality");
        Intrinsics.checkNotNullParameter(viewOnceToggleState, "viewOnceToggleState");
        Intrinsics.checkNotNullParameter(map, "editorStateMap");
        Intrinsics.checkNotNullParameter(sendRequirements, "storySendRequirements");
        this.sendType = messageSendType;
        this.selectedMedia = list;
        this.focusedMedia = media;
        this.recipient = recipient;
        this.quality = sentMediaQuality;
        this.message = charSequence;
        this.viewOnceToggleState = viewOnceToggleState;
        this.isTouchEnabled = z;
        this.isSent = z2;
        this.isPreUploadEnabled = z3;
        this.isMeteredConnection = z4;
        this.editorStateMap = map;
        this.cameraFirstCapture = media2;
        this.isStory = z5;
        this.storySendRequirements = sendRequirements;
        boolean z6 = true;
        this.maxSelection = messageSendType.usesSmsTransport() ? 1 : 32;
        this.canSend = (z2 || !(list.isEmpty() ^ true)) ? false : z6;
    }

    public final MessageSendType getSendType() {
        return this.sendType;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ MediaSelectionState(org.thoughtcrime.securesms.conversation.MessageSendType r19, java.util.List r20, org.thoughtcrime.securesms.mediasend.Media r21, org.thoughtcrime.securesms.recipients.Recipient r22, org.thoughtcrime.securesms.mms.SentMediaQuality r23, java.lang.CharSequence r24, org.thoughtcrime.securesms.mediasend.v2.MediaSelectionState.ViewOnceToggleState r25, boolean r26, boolean r27, boolean r28, boolean r29, java.util.Map r30, org.thoughtcrime.securesms.mediasend.Media r31, boolean r32, org.thoughtcrime.securesms.stories.Stories.MediaTransform.SendRequirements r33, int r34, kotlin.jvm.internal.DefaultConstructorMarker r35) {
        /*
            r18 = this;
            r0 = r34
            r1 = r0 & 2
            if (r1 == 0) goto L_0x000c
            java.util.List r1 = kotlin.collections.CollectionsKt.emptyList()
            r4 = r1
            goto L_0x000e
        L_0x000c:
            r4 = r20
        L_0x000e:
            r1 = r0 & 4
            r2 = 0
            if (r1 == 0) goto L_0x0015
            r5 = r2
            goto L_0x0017
        L_0x0015:
            r5 = r21
        L_0x0017:
            r1 = r0 & 8
            if (r1 == 0) goto L_0x001d
            r6 = r2
            goto L_0x001f
        L_0x001d:
            r6 = r22
        L_0x001f:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0032
            org.thoughtcrime.securesms.keyvalue.SettingsValues r1 = org.thoughtcrime.securesms.keyvalue.SignalStore.settings()
            org.thoughtcrime.securesms.mms.SentMediaQuality r1 = r1.getSentMediaQuality()
            java.lang.String r3 = "settings().sentMediaQuality"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r3)
            r7 = r1
            goto L_0x0034
        L_0x0032:
            r7 = r23
        L_0x0034:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x003a
            r8 = r2
            goto L_0x003c
        L_0x003a:
            r8 = r24
        L_0x003c:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x0044
            org.thoughtcrime.securesms.mediasend.v2.MediaSelectionState$ViewOnceToggleState r1 = org.thoughtcrime.securesms.mediasend.v2.MediaSelectionState.ViewOnceToggleState.INFINITE
            r9 = r1
            goto L_0x0046
        L_0x0044:
            r9 = r25
        L_0x0046:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x004d
            r1 = 1
            r10 = 1
            goto L_0x004f
        L_0x004d:
            r10 = r26
        L_0x004f:
            r1 = r0 & 256(0x100, float:3.59E-43)
            r3 = 0
            if (r1 == 0) goto L_0x0056
            r11 = 0
            goto L_0x0058
        L_0x0056:
            r11 = r27
        L_0x0058:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L_0x005e
            r12 = 0
            goto L_0x0060
        L_0x005e:
            r12 = r28
        L_0x0060:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x0066
            r13 = 0
            goto L_0x0068
        L_0x0066:
            r13 = r29
        L_0x0068:
            r1 = r0 & 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x0072
            java.util.Map r1 = kotlin.collections.MapsKt.emptyMap()
            r14 = r1
            goto L_0x0074
        L_0x0072:
            r14 = r30
        L_0x0074:
            r1 = r0 & 4096(0x1000, float:5.74E-42)
            if (r1 == 0) goto L_0x007a
            r15 = r2
            goto L_0x007c
        L_0x007a:
            r15 = r31
        L_0x007c:
            r0 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r0 == 0) goto L_0x0085
            org.thoughtcrime.securesms.stories.Stories$MediaTransform$SendRequirements r0 = org.thoughtcrime.securesms.stories.Stories.MediaTransform.SendRequirements.CAN_NOT_SEND
            r17 = r0
            goto L_0x0087
        L_0x0085:
            r17 = r33
        L_0x0087:
            r2 = r18
            r3 = r19
            r16 = r32
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionState.<init>(org.thoughtcrime.securesms.conversation.MessageSendType, java.util.List, org.thoughtcrime.securesms.mediasend.Media, org.thoughtcrime.securesms.recipients.Recipient, org.thoughtcrime.securesms.mms.SentMediaQuality, java.lang.CharSequence, org.thoughtcrime.securesms.mediasend.v2.MediaSelectionState$ViewOnceToggleState, boolean, boolean, boolean, boolean, java.util.Map, org.thoughtcrime.securesms.mediasend.Media, boolean, org.thoughtcrime.securesms.stories.Stories$MediaTransform$SendRequirements, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final List<Media> getSelectedMedia() {
        return this.selectedMedia;
    }

    public final Media getFocusedMedia() {
        return this.focusedMedia;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }

    public final SentMediaQuality getQuality() {
        return this.quality;
    }

    public final CharSequence getMessage() {
        return this.message;
    }

    public final ViewOnceToggleState getViewOnceToggleState() {
        return this.viewOnceToggleState;
    }

    public final boolean isTouchEnabled() {
        return this.isTouchEnabled;
    }

    public final boolean isSent() {
        return this.isSent;
    }

    public final boolean isPreUploadEnabled() {
        return this.isPreUploadEnabled;
    }

    public final boolean isMeteredConnection() {
        return this.isMeteredConnection;
    }

    public final Map<Uri, Object> getEditorStateMap() {
        return this.editorStateMap;
    }

    public final Media getCameraFirstCapture() {
        return this.cameraFirstCapture;
    }

    public final boolean isStory() {
        return this.isStory;
    }

    public final Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        return this.storySendRequirements;
    }

    public final int getMaxSelection() {
        return this.maxSelection;
    }

    public final boolean getCanSend() {
        return this.canSend;
    }

    /* compiled from: MediaSelectionState.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\b\b\u0001\u0018\u0000 \n2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\nB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0007\u001a\u00020\u0000R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\bj\u0002\b\t¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionState$ViewOnceToggleState;", "", "code", "", "(Ljava/lang/String;II)V", "getCode", "()I", "next", "INFINITE", "ONCE", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum ViewOnceToggleState {
        INFINITE(0),
        ONCE(1);
        
        public static final Companion Companion = new Companion(null);
        private final int code;

        /* compiled from: MediaSelectionState.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[ViewOnceToggleState.values().length];
                iArr[ViewOnceToggleState.INFINITE.ordinal()] = 1;
                iArr[ViewOnceToggleState.ONCE.ordinal()] = 2;
                $EnumSwitchMapping$0 = iArr;
            }
        }

        ViewOnceToggleState(int i) {
            this.code = i;
        }

        public final int getCode() {
            return this.code;
        }

        public final ViewOnceToggleState next() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return ONCE;
            }
            if (i == 2) {
                return INFINITE;
            }
            throw new NoWhenBranchMatchedException();
        }

        /* compiled from: MediaSelectionState.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionState$ViewOnceToggleState$Companion;", "", "()V", "fromCode", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionState$ViewOnceToggleState;", "code", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final ViewOnceToggleState fromCode(int i) {
                if (i == 1) {
                    return ViewOnceToggleState.ONCE;
                }
                return ViewOnceToggleState.INFINITE;
            }
        }
    }
}
