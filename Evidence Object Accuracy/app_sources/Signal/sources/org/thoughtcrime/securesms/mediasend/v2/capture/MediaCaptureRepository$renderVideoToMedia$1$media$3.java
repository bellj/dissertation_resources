package org.thoughtcrime.securesms.mediasend.v2.capture;

import java.io.InputStream;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.FunctionReferenceImpl;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.providers.BlobProvider;

/* compiled from: MediaCaptureRepository.kt */
/* access modifiers changed from: package-private */
@Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public /* synthetic */ class MediaCaptureRepository$renderVideoToMedia$1$media$3 extends FunctionReferenceImpl implements Function3<BlobProvider, InputStream, Long, BlobProvider.BlobBuilder> {
    public static final MediaCaptureRepository$renderVideoToMedia$1$media$3 INSTANCE = new MediaCaptureRepository$renderVideoToMedia$1$media$3();

    MediaCaptureRepository$renderVideoToMedia$1$media$3() {
        super(3, BlobProvider.class, "forData", "forData(Ljava/io/InputStream;J)Lorg/thoughtcrime/securesms/providers/BlobProvider$BlobBuilder;", 0);
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ BlobProvider.BlobBuilder invoke(Object obj, Object obj2, Object obj3) {
        return invoke((BlobProvider) obj, (InputStream) obj2, ((Number) obj3).longValue());
    }

    public final BlobProvider.BlobBuilder invoke(BlobProvider blobProvider, InputStream inputStream, long j) {
        Intrinsics.checkNotNullParameter(blobProvider, "p0");
        Intrinsics.checkNotNullParameter(inputStream, "p1");
        return blobProvider.forData(inputStream, j);
    }
}
