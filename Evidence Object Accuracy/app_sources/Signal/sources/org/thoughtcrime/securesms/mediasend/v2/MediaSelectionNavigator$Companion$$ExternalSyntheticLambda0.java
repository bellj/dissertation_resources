package org.thoughtcrime.securesms.mediasend.v2;

import kotlin.jvm.functions.Function0;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionNavigator;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaSelectionNavigator$Companion$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Function0 f$0;

    public /* synthetic */ MediaSelectionNavigator$Companion$$ExternalSyntheticLambda0(Function0 function0) {
        this.f$0 = function0;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MediaSelectionNavigator.Companion.m2102requestPermissionsForGallery$lambda2(this.f$0);
    }
}
