package org.thoughtcrime.securesms.mediasend.v2.text;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import j$.util.function.Function;
import j$.util.function.Supplier;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Triple;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.text.StringsKt__StringsKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.KeyboardEntryDialogFragment;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel;
import org.thoughtcrime.securesms.stories.StoryLinkPreviewView;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: TextStoryPostLinkEntryFragment.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u0011H\u0016J\u001a\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\u000b\u001a\u00020\f8BX\u0002¢\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\r\u0010\u000e¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostLinkEntryFragment;", "Lorg/thoughtcrime/securesms/components/KeyboardEntryDialogFragment;", "()V", "input", "Landroid/widget/EditText;", "linkPreviewViewModel", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreviewViewModel;", "getLinkPreviewViewModel", "()Lorg/thoughtcrime/securesms/linkpreview/LinkPreviewViewModel;", "linkPreviewViewModel$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "viewModel$delegate", "onDismiss", "", "dialog", "Landroid/content/DialogInterface;", "onResume", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryPostLinkEntryFragment extends KeyboardEntryDialogFragment {
    private EditText input;
    private final Lazy linkPreviewViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(LinkPreviewViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostLinkEntryFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostLinkEntryFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, TextStoryPostLinkEntryFragment$linkPreviewViewModel$2.INSTANCE);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(TextStoryPostCreationViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostLinkEntryFragment$viewModel$2
        final /* synthetic */ TextStoryPostLinkEntryFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostLinkEntryFragment$special$$inlined$viewModels$default$3
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    public TextStoryPostLinkEntryFragment() {
        super(R.layout.stories_text_post_link_entry_fragment);
    }

    public final LinkPreviewViewModel getLinkPreviewViewModel() {
        return (LinkPreviewViewModel) this.linkPreviewViewModel$delegate.getValue();
    }

    private final TextStoryPostCreationViewModel getViewModel() {
        return (TextStoryPostCreationViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.input);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.input)");
        this.input = (EditText) findViewById;
        View findViewById2 = view.findViewById(R.id.link_preview);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.link_preview)");
        StoryLinkPreviewView storyLinkPreviewView = (StoryLinkPreviewView) findViewById2;
        View findViewById3 = view.findViewById(R.id.confirm_button);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.confirm_button)");
        View findViewById4 = view.findViewById(R.id.share_a_link_group);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.share_a_link_group)");
        Group group = (Group) findViewById4;
        EditText editText = this.input;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText = null;
        }
        editText.addTextChangedListener(new TextWatcher(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostLinkEntryFragment$onViewCreated$$inlined$addTextChangedListener$default$1
            final /* synthetic */ TextStoryPostLinkEntryFragment this$0;

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            {
                this.this$0 = r1;
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                Triple triple;
                Intrinsics.checkNotNull(editable);
                EditText editText2 = null;
                if (StringsKt__StringsKt.startsWith$default((CharSequence) editable, (CharSequence) "https://", false, 2, (Object) null)) {
                    EditText editText3 = this.this$0.input;
                    if (editText3 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("input");
                        editText3 = null;
                    }
                    Integer valueOf = Integer.valueOf(editText3.getSelectionStart());
                    EditText editText4 = this.this$0.input;
                    if (editText4 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("input");
                    } else {
                        editText2 = editText4;
                    }
                    triple = new Triple(editable, valueOf, Integer.valueOf(editText2.getSelectionEnd()));
                } else {
                    String str = "https://" + ((Object) editable);
                    EditText editText5 = this.this$0.input;
                    if (editText5 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("input");
                        editText5 = null;
                    }
                    Integer valueOf2 = Integer.valueOf(editText5.getSelectionStart() + 8);
                    EditText editText6 = this.this$0.input;
                    if (editText6 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("input");
                    } else {
                        editText2 = editText6;
                    }
                    triple = new Triple(str, valueOf2, Integer.valueOf(editText2.getSelectionEnd() + 8));
                }
                this.this$0.getLinkPreviewViewModel().onTextChanged(this.this$0.requireContext(), ((CharSequence) triple.component1()).toString(), ((Number) triple.component2()).intValue(), ((Number) triple.component3()).intValue());
            }
        });
        findViewById3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostLinkEntryFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TextStoryPostLinkEntryFragment.m2260onViewCreated$lambda3(TextStoryPostLinkEntryFragment.this, view2);
            }
        });
        getLinkPreviewViewModel().getLinkPreviewState().observe(getViewLifecycleOwner(), new Observer(group, findViewById3) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostLinkEntryFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ Group f$1;
            public final /* synthetic */ View f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                TextStoryPostLinkEntryFragment.m2263onViewCreated$lambda4(StoryLinkPreviewView.this, this.f$1, this.f$2, (LinkPreviewViewModel.LinkPreviewState) obj);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2260onViewCreated$lambda3(TextStoryPostLinkEntryFragment textStoryPostLinkEntryFragment, View view) {
        Intrinsics.checkNotNullParameter(textStoryPostLinkEntryFragment, "this$0");
        LinkPreviewViewModel.LinkPreviewState value = textStoryPostLinkEntryFragment.getLinkPreviewViewModel().getLinkPreviewState().getValue();
        if (value != null) {
            textStoryPostLinkEntryFragment.getViewModel().setLinkPreview((String) value.getLinkPreview().map(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostLinkEntryFragment$$ExternalSyntheticLambda0
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return TextStoryPostLinkEntryFragment.m2261onViewCreated$lambda3$lambda1((LinkPreview) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).orElseGet(new Supplier() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostLinkEntryFragment$$ExternalSyntheticLambda1
                @Override // j$.util.function.Supplier
                public final Object get() {
                    return TextStoryPostLinkEntryFragment.m2262onViewCreated$lambda3$lambda2(LinkPreviewViewModel.LinkPreviewState.this);
                }
            }));
        }
        textStoryPostLinkEntryFragment.dismissAllowingStateLoss();
    }

    /* renamed from: onViewCreated$lambda-3$lambda-1 */
    public static final String m2261onViewCreated$lambda3$lambda1(LinkPreview linkPreview) {
        return linkPreview.getUrl();
    }

    /* renamed from: onViewCreated$lambda-3$lambda-2 */
    public static final String m2262onViewCreated$lambda3$lambda2(LinkPreviewViewModel.LinkPreviewState linkPreviewState) {
        return linkPreviewState.getActiveUrlForError();
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m2263onViewCreated$lambda4(StoryLinkPreviewView storyLinkPreviewView, Group group, View view, LinkPreviewViewModel.LinkPreviewState linkPreviewState) {
        Intrinsics.checkNotNullParameter(storyLinkPreviewView, "$linkPreview");
        Intrinsics.checkNotNullParameter(group, "$shareALinkGroup");
        Intrinsics.checkNotNullParameter(view, "$confirmButton");
        Intrinsics.checkNotNullExpressionValue(linkPreviewState, "state");
        boolean z = false;
        StoryLinkPreviewView.bind$default(storyLinkPreviewView, linkPreviewState, 0, 2, (Object) null);
        ViewExtensionsKt.setVisible(group, !linkPreviewState.isLoading() && !linkPreviewState.getLinkPreview().isPresent() && linkPreviewState.getError() == null && linkPreviewState.getActiveUrlForError() == null);
        if (linkPreviewState.getLinkPreview().isPresent() || linkPreviewState.getActiveUrlForError() != null) {
            z = true;
        }
        view.setEnabled(z);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        EditText editText = this.input;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("input");
            editText = null;
        }
        ViewUtil.focusAndShowKeyboard(editText);
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(dialogInterface, "dialog");
        getLinkPreviewViewModel().onSend();
        super.onDismiss(dialogInterface);
    }
}
