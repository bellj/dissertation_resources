package org.thoughtcrime.securesms.mediasend.v2.capture;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import j$.util.Optional;
import java.io.FileDescriptor;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.CameraFragment;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.v2.HudCommand;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionNavigator;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionState;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;
import org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureEvent;
import org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureViewModel;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: MediaCaptureFragment.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0005\n\u0002\u0010\u0012\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 >2\u00020\u00012\u00020\u0002:\u0001>B\u0007¢\u0006\u0004\b<\u0010=J\b\u0010\u0004\u001a\u00020\u0003H\u0002J\u001a\u0010\n\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0016J/\u0010\u0012\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\u000e\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u000e0\r2\u0006\u0010\u0011\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\b\u0010\u0014\u001a\u00020\tH\u0016J\b\u0010\u0015\u001a\u00020\tH\u0016J \u0010\u001a\u001a\u00020\t2\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000bH\u0016J\u0010\u0010\u001d\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u001bH\u0016J\b\u0010\u001e\u001a\u00020\tH\u0016J\b\u0010\u001f\u001a\u00020\tH\u0016J\b\u0010 \u001a\u00020\tH\u0016J\u0014\u0010$\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020#0\"0!H\u0016J\b\u0010&\u001a\u00020%H\u0016J\b\u0010'\u001a\u00020\u000bH\u0016R\u001b\u0010-\u001a\u00020(8BX\u0002¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u001b\u00102\u001a\u00020.8BX\u0002¢\u0006\f\n\u0004\b/\u0010*\u001a\u0004\b0\u00101R\u0016\u00104\u001a\u0002038\u0002@\u0002X.¢\u0006\u0006\n\u0004\b4\u00105R\u0016\u00107\u001a\u0002068\u0002@\u0002X.¢\u0006\u0006\n\u0004\b7\u00108R\u0014\u0010:\u001a\u0002098\u0002X\u0004¢\u0006\u0006\n\u0004\b:\u0010;¨\u0006?"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/mediasend/CameraFragment$Controller;", "", "isFirst", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "", "onViewCreated", "", "requestCode", "", "", "permissions", "", "grantResults", "onRequestPermissionsResult", "(I[Ljava/lang/String;[I)V", "onResume", "onCameraError", "", "data", "width", "height", "onImageCaptured", "Ljava/io/FileDescriptor;", "fd", "onVideoCaptured", "onVideoCaptureError", "onGalleryClicked", "onCameraCountButtonClicked", "Lio/reactivex/rxjava3/core/Flowable;", "j$/util/Optional", "Lorg/thoughtcrime/securesms/mediasend/Media;", "getMostRecentMediaItem", "Lorg/thoughtcrime/securesms/mms/MediaConstraints;", "getMediaConstraints", "getMaxVideoDuration", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "sharedViewModel$delegate", "Lkotlin/Lazy;", "getSharedViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "sharedViewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureViewModel;", "viewModel$delegate", "getViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureViewModel;", "viewModel", "Lorg/thoughtcrime/securesms/mediasend/CameraFragment;", "captureChildFragment", "Lorg/thoughtcrime/securesms/mediasend/CameraFragment;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionNavigator;", "navigator", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionNavigator;", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "<init>", "()V", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class MediaCaptureFragment extends Fragment implements CameraFragment.Controller {
    public static final String CAPTURE_RESULT;
    public static final String CAPTURE_RESULT_OK;
    public static final Companion Companion = new Companion(null);
    private CameraFragment captureChildFragment;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private MediaSelectionNavigator navigator;
    private final Lazy sharedViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MediaSelectionViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$sharedViewModel$2
        final /* synthetic */ MediaCaptureFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MediaCaptureViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$special$$inlined$viewModels$default$3
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$viewModel$2
        final /* synthetic */ MediaCaptureFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            return new MediaCaptureViewModel.Factory(new MediaCaptureRepository(requireContext));
        }
    });

    public MediaCaptureFragment() {
        super(R.layout.fragment_container);
    }

    private final MediaSelectionViewModel getSharedViewModel() {
        return (MediaSelectionViewModel) this.sharedViewModel$delegate.getValue();
    }

    private final MediaCaptureViewModel getViewModel() {
        return (MediaCaptureViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        Fragment newInstance = CameraFragment.CC.newInstance();
        if (newInstance != null) {
            this.captureChildFragment = (CameraFragment) newInstance;
            CameraFragment cameraFragment = null;
            this.navigator = new MediaSelectionNavigator(0, R.id.action_mediaCaptureFragment_to_mediaGalleryFragment, 1, null);
            FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
            CameraFragment cameraFragment2 = this.captureChildFragment;
            if (cameraFragment2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("captureChildFragment");
            } else {
                cameraFragment = cameraFragment2;
            }
            beginTransaction.replace(R.id.fragment_container, (Fragment) cameraFragment).commitNowAllowingStateLoss();
            getViewModel().getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$$ExternalSyntheticLambda1
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    MediaCaptureFragment.m2137$r8$lambda$efIs8IcmamnJMMfRebJG6pU3MI(MediaCaptureFragment.this, (MediaCaptureEvent) obj);
                }
            });
            getSharedViewModel().getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$$ExternalSyntheticLambda2
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    MediaCaptureFragment.$r8$lambda$1b0c9xjfu9ulwcWwvTyOtsEeWdw(MediaCaptureFragment.this, (MediaSelectionState) obj);
                }
            });
            LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
            Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
            lifecycleDisposable.bindTo(viewLifecycleOwner);
            LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
            Disposable subscribe = getSharedViewModel().getHudCommands().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$$ExternalSyntheticLambda3
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    MediaCaptureFragment.$r8$lambda$F0V4CF6KDDA7NQ7U4K7CC58YJBg(MediaCaptureFragment.this, (HudCommand) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe, "sharedViewModel.hudComma…onFragment)\n      }\n    }");
            lifecycleDisposable2.plusAssign(subscribe);
            if (isFirst()) {
                requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$onViewCreated$4
                    final /* synthetic */ MediaCaptureFragment this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                    }

                    @Override // androidx.activity.OnBackPressedCallback
                    public void handleOnBackPressed() {
                        this.this$0.requireActivity().finish();
                    }
                });
                return;
            }
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.mediasend.CameraFragment");
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2139onViewCreated$lambda0(MediaCaptureFragment mediaCaptureFragment, MediaCaptureEvent mediaCaptureEvent) {
        Intrinsics.checkNotNullParameter(mediaCaptureFragment, "this$0");
        if (Intrinsics.areEqual(mediaCaptureEvent, MediaCaptureEvent.MediaCaptureRenderFailed.INSTANCE)) {
            Log.w(MediaCaptureFragmentKt.TAG, "Failed to render captured media.");
            Toast.makeText(mediaCaptureFragment.requireContext(), (int) R.string.MediaSendActivity_camera_unavailable, 0).show();
        } else if (mediaCaptureEvent instanceof MediaCaptureEvent.MediaCaptureRendered) {
            if (mediaCaptureFragment.isFirst()) {
                mediaCaptureFragment.getSharedViewModel().addCameraFirstCapture(((MediaCaptureEvent.MediaCaptureRendered) mediaCaptureEvent).getMedia());
            } else {
                mediaCaptureFragment.getSharedViewModel().addMedia(((MediaCaptureEvent.MediaCaptureRendered) mediaCaptureEvent).getMedia());
            }
            MediaSelectionNavigator mediaSelectionNavigator = mediaCaptureFragment.navigator;
            if (mediaSelectionNavigator == null) {
                Intrinsics.throwUninitializedPropertyAccessException("navigator");
                mediaSelectionNavigator = null;
            }
            mediaSelectionNavigator.goToReview(FragmentKt.findNavController(mediaCaptureFragment));
        }
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m2140onViewCreated$lambda1(MediaCaptureFragment mediaCaptureFragment, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(mediaCaptureFragment, "this$0");
        CameraFragment cameraFragment = mediaCaptureFragment.captureChildFragment;
        if (cameraFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("captureChildFragment");
            cameraFragment = null;
        }
        cameraFragment.presentHud(mediaSelectionState.getSelectedMedia().size());
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m2141onViewCreated$lambda2(MediaCaptureFragment mediaCaptureFragment, HudCommand hudCommand) {
        Intrinsics.checkNotNullParameter(mediaCaptureFragment, "this$0");
        if (Intrinsics.areEqual(hudCommand, HudCommand.GoToText.INSTANCE)) {
            SafeNavigation.safeNavigate(FragmentKt.findNavController(mediaCaptureFragment), (int) R.id.action_mediaCaptureFragment_to_textStoryPostCreationFragment);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Intrinsics.checkNotNullParameter(strArr, "permissions");
        Intrinsics.checkNotNullParameter(iArr, "grantResults");
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        CameraFragment cameraFragment = this.captureChildFragment;
        if (cameraFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("captureChildFragment");
            cameraFragment = null;
        }
        cameraFragment.fadeInControls();
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onCameraError() {
        Log.w(MediaCaptureFragmentKt.TAG, "Camera Error.");
        Context context = getContext();
        if (context != null) {
            Toast.makeText(context, (int) R.string.MediaSendActivity_camera_unavailable, 0).show();
        } else {
            Log.w(MediaCaptureFragmentKt.TAG, "Could not post toast, fragment not attached to a context.");
        }
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onImageCaptured(byte[] bArr, int i, int i2) {
        Intrinsics.checkNotNullParameter(bArr, "data");
        getViewModel().onImageCaptured(bArr, i, i2);
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onVideoCaptured(FileDescriptor fileDescriptor) {
        Intrinsics.checkNotNullParameter(fileDescriptor, "fd");
        getViewModel().onVideoCaptured(fileDescriptor);
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onVideoCaptureError() {
        Log.w(MediaCaptureFragmentKt.TAG, "Video capture error.");
        Toast.makeText(requireContext(), (int) R.string.MediaSendActivity_camera_unavailable, 0).show();
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onGalleryClicked() {
        MediaSelectionNavigator.Companion.requestPermissionsForGallery(this, new MediaCaptureFragment$onGalleryClicked$1(this, FragmentKt.findNavController(this)));
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public void onCameraCountButtonClicked() {
        NavController findNavController = FragmentKt.findNavController(this);
        CameraFragment cameraFragment = this.captureChildFragment;
        if (cameraFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("captureChildFragment");
            cameraFragment = null;
        }
        cameraFragment.fadeOutControls(new Runnable(findNavController) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ NavController f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaCaptureFragment.$r8$lambda$apzotKugdm29a6vDNux_MBGFvGY(MediaCaptureFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: onCameraCountButtonClicked$lambda-3 */
    public static final void m2138onCameraCountButtonClicked$lambda3(MediaCaptureFragment mediaCaptureFragment, NavController navController) {
        Intrinsics.checkNotNullParameter(mediaCaptureFragment, "this$0");
        Intrinsics.checkNotNullParameter(navController, "$controller");
        MediaSelectionNavigator mediaSelectionNavigator = mediaCaptureFragment.navigator;
        if (mediaSelectionNavigator == null) {
            Intrinsics.throwUninitializedPropertyAccessException("navigator");
            mediaSelectionNavigator = null;
        }
        mediaSelectionNavigator.goToReview(navController);
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public Flowable<Optional<Media>> getMostRecentMediaItem() {
        return getViewModel().getMostRecentMedia();
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public MediaConstraints getMediaConstraints() {
        return getSharedViewModel().getMediaConstraints();
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment.Controller
    public int getMaxVideoDuration() {
        if (getSharedViewModel().isStory()) {
            return (int) TimeUnit.MILLISECONDS.toSeconds(Stories.MAX_VIDEO_DURATION_MILLIS);
        }
        return -1;
    }

    private final boolean isFirst() {
        Bundle arguments = getArguments();
        return arguments != null && arguments.getBoolean("first");
    }

    /* compiled from: MediaCaptureFragment.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureFragment$Companion;", "", "()V", "CAPTURE_RESULT", "", "CAPTURE_RESULT_OK", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
