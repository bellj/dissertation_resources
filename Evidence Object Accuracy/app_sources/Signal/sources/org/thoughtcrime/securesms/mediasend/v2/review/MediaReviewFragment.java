package org.thoughtcrime.securesms.mediasend.v2.review;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardActivity;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.mediasend.v2.HudCommand;
import org.thoughtcrime.securesms.mediasend.v2.MediaAnimations;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionNavigator;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionState;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;
import org.thoughtcrime.securesms.mediasend.v2.MediaValidator;
import org.thoughtcrime.securesms.mediasend.v2.review.AddMessageDialogFragment;
import org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewAddItem;
import org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment;
import org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewSelectedItem;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;
import org.thoughtcrime.securesms.util.views.TouchInterceptingFrameLayout;

/* compiled from: MediaReviewFragment.kt */
@Metadata(d1 = {"\u0000Ð\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001[B\u0005¢\u0006\u0002\u0010\u0002J\u0016\u0010+\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/H\u0002J\u0016\u00100\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/H\u0002J\u0016\u00101\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/H\u0002J\u0016\u00102\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/H\u0002J\u0016\u00103\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/H\u0002J\u0016\u00104\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/H\u0002J\u0016\u00105\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/H\u0002J\u0016\u00106\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/H\u0002J\u0016\u00107\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/H\u0002J\u0016\u00108\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/H\u0002J\u0010\u00109\u001a\u00020:2\u0006\u0010.\u001a\u00020/H\u0002J\u0010\u0010;\u001a\u00020:2\u0006\u0010<\u001a\u00020=H\u0002J\b\u0010>\u001a\u00020:H\u0002J\b\u0010?\u001a\u00020:H\u0016J-\u0010@\u001a\u00020:2\u0006\u0010A\u001a\u00020B2\u000e\u0010C\u001a\n\u0012\u0006\b\u0001\u0012\u00020E0D2\u0006\u0010F\u001a\u00020GH\u0016¢\u0006\u0002\u0010HJ\b\u0010I\u001a\u00020:H\u0016J\u001a\u0010J\u001a\u00020:2\u0006\u0010K\u001a\u00020\u00042\b\u0010L\u001a\u0004\u0018\u00010MH\u0016J\u0018\u0010N\u001a\u00020:2\u000e\b\u0002\u0010O\u001a\b\u0012\u0004\u0012\u00020P0,H\u0002J\u0012\u0010Q\u001a\u00020:2\b\u0010R\u001a\u0004\u0018\u00010SH\u0002J\u0010\u0010T\u001a\u00020:2\u0006\u0010U\u001a\u00020VH\u0002J\u0010\u0010W\u001a\u00020:2\u0006\u0010.\u001a\u00020/H\u0002J\u0010\u0010X\u001a\u00020:2\u0006\u0010Y\u001a\u00020ZH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX.¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX.¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX.¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X.¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001b\u0010\"\u001a\u00020#8BX\u0002¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b$\u0010%R\u000e\u0010(\u001a\u00020)X.¢\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000¨\u0006\\"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewFragment;", "Landroidx/fragment/app/Fragment;", "()V", "addMediaButton", "Landroid/view/View;", "addMessageButton", "Landroid/widget/TextView;", "addMessageEntry", "animatorSet", "Landroid/animation/AnimatorSet;", "callback", "Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewFragment$Callback;", "controls", "Landroidx/constraintlayout/widget/ConstraintLayout;", "controlsShade", "cropAndRotateButton", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "drawToolButton", "navigator", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionNavigator;", "pager", "Landroidx/viewpager2/widget/ViewPager2;", "progress", "Landroid/widget/ProgressBar;", "progressWrapper", "Lorg/thoughtcrime/securesms/util/views/TouchInterceptingFrameLayout;", "qualityButton", "Landroid/widget/ImageView;", "recipientDisplay", "saveButton", "selectionRecycler", "Landroidx/recyclerview/widget/RecyclerView;", "sendButton", "sharedViewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "getSharedViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "sharedViewModel$delegate", "Lkotlin/Lazy;", "viewOnceButton", "Landroid/widget/ViewSwitcher;", "viewOnceMessage", "computeAddMediaButtonsAnimators", "", "Landroid/animation/Animator;", "state", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionState;", "computeAddMessageAnimators", "computeControlsShadeAnimators", "computeCropAndRotateButtonAnimators", "computeDrawToolButtonAnimators", "computeQualityButtonAnimators", "computeRecipientDisplayAnimators", "computeSaveButtonAnimators", "computeSendButtonAnimators", "computeViewOnceButtonAnimators", "computeViewStateAndAnimate", "", "handleMediaValidatorFilterError", "error", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "launchGallery", "onDestroyView", "onRequestPermissionsResult", "requestCode", "", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "onViewCreated", "view", "savedInstanceState", "Landroid/os/Bundle;", "performSend", "selection", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "presentAddMessageEntry", "message", "", "presentImageQualityToggle", "quality", "Lorg/thoughtcrime/securesms/mms/SentMediaQuality;", "presentPager", "presentSendButton", "sendType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaReviewFragment extends Fragment {
    private View addMediaButton;
    private TextView addMessageButton;
    private TextView addMessageEntry;
    private AnimatorSet animatorSet;
    private Callback callback;
    private ConstraintLayout controls;
    private View controlsShade;
    private View cropAndRotateButton;
    private CompositeDisposable disposables;
    private View drawToolButton;
    private final MediaSelectionNavigator navigator = new MediaSelectionNavigator(0, R.id.action_mediaReviewFragment_to_mediaGalleryFragment, 1, null);
    private ViewPager2 pager;
    private ProgressBar progress;
    private TouchInterceptingFrameLayout progressWrapper;
    private ImageView qualityButton;
    private TextView recipientDisplay;
    private View saveButton;
    private RecyclerView selectionRecycler;
    private View sendButton;
    private final Lazy sharedViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MediaSelectionViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$sharedViewModel$2
        final /* synthetic */ MediaReviewFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private ViewSwitcher viewOnceButton;
    private TextView viewOnceMessage;

    /* compiled from: MediaReviewFragment.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH&J\b\u0010\u000b\u001a\u00020\u0003H&¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewFragment$Callback;", "", "onNoMediaSelected", "", "onPopFromReview", "onSendError", "error", "", "onSentWithResult", "mediaSendActivityResult", "Lorg/thoughtcrime/securesms/mediasend/MediaSendActivityResult;", "onSentWithoutResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {
        void onNoMediaSelected();

        void onPopFromReview();

        void onSendError(Throwable th);

        void onSentWithResult(MediaSendActivityResult mediaSendActivityResult);

        void onSentWithoutResult();
    }

    /* compiled from: MediaReviewFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[SentMediaQuality.values().length];
            iArr[SentMediaQuality.STANDARD.ordinal()] = 1;
            iArr[SentMediaQuality.HIGH.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final boolean m2207onViewCreated$lambda0(MotionEvent motionEvent) {
        return true;
    }

    public MediaReviewFragment() {
        super(R.layout.v2_media_review_fragment);
    }

    public final MediaSelectionViewModel getSharedViewModel() {
        return (MediaSelectionViewModel) this.sharedViewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Callback callback;
        Intrinsics.checkNotNullParameter(view, "view");
        postponeEnterTransition();
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        callback = (Callback) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment.Callback");
                    }
                } else if (fragment instanceof Callback) {
                    callback = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.callback = callback;
            View findViewById = view.findViewById(R.id.draw_tool);
            Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.draw_tool)");
            this.drawToolButton = findViewById;
            View findViewById2 = view.findViewById(R.id.crop_and_rotate_tool);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.crop_and_rotate_tool)");
            this.cropAndRotateButton = findViewById2;
            View findViewById3 = view.findViewById(R.id.quality_selector);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.quality_selector)");
            this.qualityButton = (ImageView) findViewById3;
            View findViewById4 = view.findViewById(R.id.save_to_media);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.save_to_media)");
            this.saveButton = findViewById4;
            View findViewById5 = view.findViewById(R.id.send);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.send)");
            this.sendButton = findViewById5;
            View findViewById6 = view.findViewById(R.id.add_media);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.add_media)");
            this.addMediaButton = findViewById6;
            View findViewById7 = view.findViewById(R.id.view_once_toggle);
            Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.view_once_toggle)");
            this.viewOnceButton = (ViewSwitcher) findViewById7;
            View findViewById8 = view.findViewById(R.id.add_a_message);
            Intrinsics.checkNotNullExpressionValue(findViewById8, "view.findViewById(R.id.add_a_message)");
            this.addMessageButton = (TextView) findViewById8;
            View findViewById9 = view.findViewById(R.id.add_a_message_entry);
            Intrinsics.checkNotNullExpressionValue(findViewById9, "view.findViewById(R.id.add_a_message_entry)");
            this.addMessageEntry = (TextView) findViewById9;
            View findViewById10 = view.findViewById(R.id.recipient);
            Intrinsics.checkNotNullExpressionValue(findViewById10, "view.findViewById(R.id.recipient)");
            this.recipientDisplay = (TextView) findViewById10;
            View findViewById11 = view.findViewById(R.id.media_pager);
            Intrinsics.checkNotNullExpressionValue(findViewById11, "view.findViewById(R.id.media_pager)");
            this.pager = (ViewPager2) findViewById11;
            View findViewById12 = view.findViewById(R.id.controls);
            Intrinsics.checkNotNullExpressionValue(findViewById12, "view.findViewById(R.id.controls)");
            this.controls = (ConstraintLayout) findViewById12;
            View findViewById13 = view.findViewById(R.id.selection_recycler);
            Intrinsics.checkNotNullExpressionValue(findViewById13, "view.findViewById(R.id.selection_recycler)");
            this.selectionRecycler = (RecyclerView) findViewById13;
            View findViewById14 = view.findViewById(R.id.controls_shade);
            Intrinsics.checkNotNullExpressionValue(findViewById14, "view.findViewById(R.id.controls_shade)");
            this.controlsShade = findViewById14;
            View findViewById15 = view.findViewById(R.id.view_once_message);
            Intrinsics.checkNotNullExpressionValue(findViewById15, "view.findViewById(R.id.view_once_message)");
            this.viewOnceMessage = (TextView) findViewById15;
            View findViewById16 = view.findViewById(R.id.progress);
            Intrinsics.checkNotNullExpressionValue(findViewById16, "view.findViewById(R.id.progress)");
            this.progress = (ProgressBar) findViewById16;
            View findViewById17 = view.findViewById(R.id.progress_wrapper);
            Intrinsics.checkNotNullExpressionValue(findViewById17, "view.findViewById(R.id.progress_wrapper)");
            this.progressWrapper = (TouchInterceptingFrameLayout) findViewById17;
            ProgressBar progressBar = this.progress;
            RecyclerView recyclerView = null;
            if (progressBar == null) {
                Intrinsics.throwUninitializedPropertyAccessException("progress");
                progressBar = null;
            }
            DrawableCompat.setTint(progressBar.getIndeterminateDrawable(), -1);
            TouchInterceptingFrameLayout touchInterceptingFrameLayout = this.progressWrapper;
            if (touchInterceptingFrameLayout == null) {
                Intrinsics.throwUninitializedPropertyAccessException("progressWrapper");
                touchInterceptingFrameLayout = null;
            }
            touchInterceptingFrameLayout.setOnInterceptTouchEventListener(new TouchInterceptingFrameLayout.OnInterceptTouchEventListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda3
                @Override // org.thoughtcrime.securesms.util.views.TouchInterceptingFrameLayout.OnInterceptTouchEventListener
                public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
                    return MediaReviewFragment.m2207onViewCreated$lambda0(motionEvent);
                }
            });
            MediaReviewFragmentPagerAdapter mediaReviewFragmentPagerAdapter = new MediaReviewFragmentPagerAdapter(this);
            CompositeDisposable compositeDisposable = new CompositeDisposable();
            this.disposables = compositeDisposable;
            compositeDisposable.add(getSharedViewModel().getHudCommands().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda8
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    MediaReviewFragment.m2208onViewCreated$lambda1(MediaReviewFragment.this, (HudCommand) obj);
                }
            }));
            ViewPager2 viewPager2 = this.pager;
            if (viewPager2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("pager");
                viewPager2 = null;
            }
            viewPager2.setAdapter(mediaReviewFragmentPagerAdapter);
            View view2 = this.drawToolButton;
            if (view2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("drawToolButton");
                view2 = null;
            }
            view2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda9
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    MediaReviewFragment.m2212onViewCreated$lambda2(MediaReviewFragment.this, view3);
                }
            });
            View view3 = this.cropAndRotateButton;
            if (view3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("cropAndRotateButton");
                view3 = null;
            }
            view3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda10
                @Override // android.view.View.OnClickListener
                public final void onClick(View view4) {
                    MediaReviewFragment.m2213onViewCreated$lambda3(MediaReviewFragment.this, view4);
                }
            });
            ImageView imageView = this.qualityButton;
            if (imageView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("qualityButton");
                imageView = null;
            }
            imageView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda11
                @Override // android.view.View.OnClickListener
                public final void onClick(View view4) {
                    MediaReviewFragment.m2214onViewCreated$lambda4(MediaReviewFragment.this, view4);
                }
            });
            View view4 = this.saveButton;
            if (view4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("saveButton");
                view4 = null;
            }
            view4.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda12
                @Override // android.view.View.OnClickListener
                public final void onClick(View view5) {
                    MediaReviewFragment.m2215onViewCreated$lambda5(MediaReviewFragment.this, view5);
                }
            });
            ActivityResultLauncher registerForActivityResult = registerForActivityResult(new MultiselectForwardActivity.SelectionContract(), new ActivityResultCallback() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda13
                @Override // androidx.activity.result.ActivityResultCallback
                public final void onActivityResult(Object obj) {
                    MediaReviewFragment.m2216onViewCreated$lambda6(MediaReviewFragment.this, (List) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(registerForActivityResult, "registerForActivityResul…mSend(keys)\n      }\n    }");
            View view5 = this.sendButton;
            if (view5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("sendButton");
                view5 = null;
            }
            view5.setOnClickListener(new View.OnClickListener(registerForActivityResult) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda14
                public final /* synthetic */ ActivityResultLauncher f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view6) {
                    MediaReviewFragment.m2217onViewCreated$lambda7(MediaReviewFragment.this, this.f$1, view6);
                }
            });
            View view6 = this.addMediaButton;
            if (view6 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMediaButton");
                view6 = null;
            }
            view6.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda15
                @Override // android.view.View.OnClickListener
                public final void onClick(View view7) {
                    MediaReviewFragment.m2218onViewCreated$lambda8(MediaReviewFragment.this, view7);
                }
            });
            ViewSwitcher viewSwitcher = this.viewOnceButton;
            if (viewSwitcher == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewOnceButton");
                viewSwitcher = null;
            }
            viewSwitcher.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda16
                @Override // android.view.View.OnClickListener
                public final void onClick(View view7) {
                    MediaReviewFragment.m2219onViewCreated$lambda9(MediaReviewFragment.this, view7);
                }
            });
            TextView textView = this.addMessageButton;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMessageButton");
                textView = null;
            }
            textView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view7) {
                    MediaReviewFragment.m2209onViewCreated$lambda10(MediaReviewFragment.this, view7);
                }
            });
            TextView textView2 = this.addMessageEntry;
            if (textView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMessageEntry");
                textView2 = null;
            }
            textView2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view7) {
                    MediaReviewFragment.m2210onViewCreated$lambda11(MediaReviewFragment.this, view7);
                }
            });
            if (getSharedViewModel().isReply()) {
                TextView textView3 = this.addMessageButton;
                if (textView3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("addMessageButton");
                    textView3 = null;
                }
                textView3.setText(R.string.MediaReviewFragment__add_a_reply);
            }
            ViewPager2 viewPager22 = this.pager;
            if (viewPager22 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("pager");
                viewPager22 = null;
            }
            viewPager22.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$onViewCreated$12
                final /* synthetic */ MediaReviewFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
                public void onPageSelected(int i) {
                    this.this$0.getSharedViewModel().setFocusedMedia(i);
                }
            });
            MappingAdapter mappingAdapter = new MappingAdapter();
            MediaReviewAddItem.INSTANCE.register(mappingAdapter, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$onViewCreated$13
                final /* synthetic */ MediaReviewFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    this.this$0.launchGallery();
                }
            });
            MediaReviewSelectedItem.INSTANCE.register(mappingAdapter, new Function2<Media, Boolean, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$onViewCreated$14
                final /* synthetic */ MediaReviewFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                @Override // kotlin.jvm.functions.Function2
                public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                    invoke((Media) obj, ((Boolean) obj2).booleanValue());
                    return Unit.INSTANCE;
                }

                public final void invoke(Media media, boolean z) {
                    Intrinsics.checkNotNullParameter(media, "media");
                    if (z) {
                        this.this$0.getSharedViewModel().removeMedia(media);
                    } else {
                        this.this$0.getSharedViewModel().setFocusedMedia(media);
                    }
                }
            });
            RecyclerView recyclerView2 = this.selectionRecycler;
            if (recyclerView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("selectionRecycler");
                recyclerView2 = null;
            }
            recyclerView2.setAdapter(mappingAdapter);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new MediaSelectionItemTouchHelper(getSharedViewModel()));
            RecyclerView recyclerView3 = this.selectionRecycler;
            if (recyclerView3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("selectionRecycler");
            } else {
                recyclerView = recyclerView3;
            }
            itemTouchHelper.attachToRecyclerView(recyclerView);
            getSharedViewModel().getState().observe(getViewLifecycleOwner(), new Observer(mappingAdapter, this) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda6
                public final /* synthetic */ MappingAdapter f$1;
                public final /* synthetic */ MediaReviewFragment f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    MediaReviewFragment.m2211onViewCreated$lambda13(MediaReviewFragmentPagerAdapter.this, this.f$1, this.f$2, (MediaSelectionState) obj);
                }
            });
            getSharedViewModel().getMediaErrors().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda7
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    MediaReviewFragment.this.handleMediaValidatorFilterError((MediaValidator.FilterError) obj);
                }
            });
            requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$onViewCreated$17
                final /* synthetic */ MediaReviewFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // androidx.activity.OnBackPressedCallback
                public void handleOnBackPressed() {
                    MediaReviewFragment.Callback callback2 = this.this$0.callback;
                    if (callback2 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("callback");
                        callback2 = null;
                    }
                    callback2.onPopFromReview();
                }
            });
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m2208onViewCreated$lambda1(MediaReviewFragment mediaReviewFragment, HudCommand hudCommand) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        if (Intrinsics.areEqual(hudCommand, HudCommand.ResumeEntryTransition.INSTANCE)) {
            mediaReviewFragment.startPostponedEnterTransition();
        }
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m2212onViewCreated$lambda2(MediaReviewFragment mediaReviewFragment, View view) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        mediaReviewFragment.getSharedViewModel().sendCommand(HudCommand.StartDraw.INSTANCE);
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2213onViewCreated$lambda3(MediaReviewFragment mediaReviewFragment, View view) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        mediaReviewFragment.getSharedViewModel().sendCommand(HudCommand.StartCropAndRotate.INSTANCE);
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m2214onViewCreated$lambda4(MediaReviewFragment mediaReviewFragment, View view) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        QualitySelectorBottomSheetDialog.show(mediaReviewFragment.getParentFragmentManager());
    }

    /* renamed from: onViewCreated$lambda-5 */
    public static final void m2215onViewCreated$lambda5(MediaReviewFragment mediaReviewFragment, View view) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        mediaReviewFragment.getSharedViewModel().sendCommand(HudCommand.SaveMedia.INSTANCE);
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m2216onViewCreated$lambda6(MediaReviewFragment mediaReviewFragment, List list) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(list, "keys");
        if (!list.isEmpty()) {
            mediaReviewFragment.performSend(list);
        }
    }

    /* renamed from: onViewCreated$lambda-7 */
    public static final void m2217onViewCreated$lambda7(MediaReviewFragment mediaReviewFragment, ActivityResultLauncher activityResultLauncher, View view) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        Intrinsics.checkNotNullParameter(activityResultLauncher, "$recipientSelectionLauncher");
        if (mediaReviewFragment.getSharedViewModel().isContactSelectionRequired()) {
            activityResultLauncher.launch(new MultiselectForwardFragmentArgs(false, null, R.string.MediaReviewFragment__send_to, false, false, false, 0, mediaReviewFragment.getSharedViewModel().getStorySendRequirements(), 122, null));
        } else {
            performSend$default(mediaReviewFragment, null, 1, null);
        }
    }

    /* renamed from: onViewCreated$lambda-8 */
    public static final void m2218onViewCreated$lambda8(MediaReviewFragment mediaReviewFragment, View view) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        mediaReviewFragment.launchGallery();
    }

    /* renamed from: onViewCreated$lambda-9 */
    public static final void m2219onViewCreated$lambda9(MediaReviewFragment mediaReviewFragment, View view) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        mediaReviewFragment.getSharedViewModel().incrementViewOnceState();
    }

    /* renamed from: onViewCreated$lambda-10 */
    public static final void m2209onViewCreated$lambda10(MediaReviewFragment mediaReviewFragment, View view) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        AddMessageDialogFragment.Companion companion = AddMessageDialogFragment.Companion;
        FragmentManager parentFragmentManager = mediaReviewFragment.getParentFragmentManager();
        Intrinsics.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        MediaSelectionState value = mediaReviewFragment.getSharedViewModel().getState().getValue();
        companion.show(parentFragmentManager, value != null ? value.getMessage() : null);
    }

    /* renamed from: onViewCreated$lambda-11 */
    public static final void m2210onViewCreated$lambda11(MediaReviewFragment mediaReviewFragment, View view) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        AddMessageDialogFragment.Companion companion = AddMessageDialogFragment.Companion;
        FragmentManager parentFragmentManager = mediaReviewFragment.getParentFragmentManager();
        Intrinsics.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        MediaSelectionState value = mediaReviewFragment.getSharedViewModel().getState().getValue();
        companion.show(parentFragmentManager, value != null ? value.getMessage() : null);
    }

    /* renamed from: onViewCreated$lambda-13 */
    public static final void m2211onViewCreated$lambda13(MediaReviewFragmentPagerAdapter mediaReviewFragmentPagerAdapter, MappingAdapter mappingAdapter, MediaReviewFragment mediaReviewFragment, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(mediaReviewFragmentPagerAdapter, "$pagerAdapter");
        Intrinsics.checkNotNullParameter(mappingAdapter, "$selectionAdapter");
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        mediaReviewFragmentPagerAdapter.submitMedia(mediaSelectionState.getSelectedMedia());
        List<Media> selectedMedia = mediaSelectionState.getSelectedMedia();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(selectedMedia, 10));
        for (Media media : selectedMedia) {
            arrayList.add(new MediaReviewSelectedItem.Model(media, Intrinsics.areEqual(mediaSelectionState.getFocusedMedia(), media)));
        }
        mappingAdapter.submitList(CollectionsKt___CollectionsKt.plus((Collection<? extends MediaReviewAddItem.Model>) ((Collection<? extends Object>) arrayList), MediaReviewAddItem.Model.INSTANCE));
        mediaReviewFragment.presentSendButton(mediaSelectionState.getSendType());
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "state");
        mediaReviewFragment.presentPager(mediaSelectionState);
        mediaReviewFragment.presentAddMessageEntry(mediaSelectionState.getMessage());
        mediaReviewFragment.presentImageQualityToggle(mediaSelectionState.getQuality());
        ViewSwitcher viewSwitcher = mediaReviewFragment.viewOnceButton;
        if (viewSwitcher == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewOnceButton");
            viewSwitcher = null;
        }
        viewSwitcher.setDisplayedChild(mediaSelectionState.getViewOnceToggleState() == MediaSelectionState.ViewOnceToggleState.ONCE ? 1 : 0);
        mediaReviewFragment.computeViewStateAndAnimate(mediaSelectionState);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getSharedViewModel().kick();
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Intrinsics.checkNotNullParameter(strArr, "permissions");
        Intrinsics.checkNotNullParameter(iArr, "grantResults");
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        CompositeDisposable compositeDisposable = this.disposables;
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
        super.onDestroyView();
    }

    public final void handleMediaValidatorFilterError(MediaValidator.FilterError filterError) {
        if (Intrinsics.areEqual(filterError, MediaValidator.FilterError.ItemTooLarge.INSTANCE)) {
            Toast.makeText(requireContext(), (int) R.string.MediaReviewFragment__one_or_more_items_were_too_large, 0).show();
        } else if (Intrinsics.areEqual(filterError, MediaValidator.FilterError.ItemInvalidType.INSTANCE)) {
            Toast.makeText(requireContext(), (int) R.string.MediaReviewFragment__one_or_more_items_were_invalid, 0).show();
        } else if (Intrinsics.areEqual(filterError, MediaValidator.FilterError.TooManyItems.INSTANCE)) {
            Toast.makeText(requireContext(), (int) R.string.MediaReviewFragment__too_many_items_selected, 0).show();
        } else if (filterError instanceof MediaValidator.FilterError.NoItems) {
            MediaValidator.FilterError.NoItems noItems = (MediaValidator.FilterError.NoItems) filterError;
            if (noItems.getCause() != null) {
                handleMediaValidatorFilterError(noItems.getCause());
            } else {
                Toast.makeText(requireContext(), (int) R.string.MediaReviewFragment__one_or_more_items_were_invalid, 0).show();
            }
            Callback callback = this.callback;
            if (callback == null) {
                Intrinsics.throwUninitializedPropertyAccessException("callback");
                callback = null;
            }
            callback.onNoMediaSelected();
        }
    }

    public final void launchGallery() {
        MediaSelectionNavigator.Companion.requestPermissionsForGallery(this, new Function0<Unit>(this, FragmentKt.findNavController(this)) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$launchGallery$1
            final /* synthetic */ NavController $controller;
            final /* synthetic */ MediaReviewFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$controller = r2;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                this.this$0.navigator.goToGallery(this.$controller);
            }
        });
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment */
    /* JADX WARN: Multi-variable type inference failed */
    static /* synthetic */ void performSend$default(MediaReviewFragment mediaReviewFragment, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = CollectionsKt__CollectionsKt.emptyList();
        }
        mediaReviewFragment.performSend(list);
    }

    private final void performSend(List<? extends ContactSearchKey> list) {
        TouchInterceptingFrameLayout touchInterceptingFrameLayout = this.progressWrapper;
        TouchInterceptingFrameLayout touchInterceptingFrameLayout2 = null;
        if (touchInterceptingFrameLayout == null) {
            Intrinsics.throwUninitializedPropertyAccessException("progressWrapper");
            touchInterceptingFrameLayout = null;
        }
        ViewExtensionsKt.setVisible(touchInterceptingFrameLayout, true);
        TouchInterceptingFrameLayout touchInterceptingFrameLayout3 = this.progressWrapper;
        if (touchInterceptingFrameLayout3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("progressWrapper");
        } else {
            touchInterceptingFrameLayout2 = touchInterceptingFrameLayout3;
        }
        touchInterceptingFrameLayout2.animate().setStartDelay(300).setInterpolator(MediaAnimations.getInterpolator()).alpha(1.0f);
        getSharedViewModel().send(CollectionsKt___CollectionsJvmKt.filterIsInstance(list, ContactSearchKey.RecipientSearchKey.class)).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                MediaReviewFragment.m2220performSend$lambda14(MediaReviewFragment.this, (MediaSendActivityResult) obj);
            }
        }, new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                MediaReviewFragment.m2221performSend$lambda15(MediaReviewFragment.this, (Throwable) obj);
            }
        }, new Action() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewFragment$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                MediaReviewFragment.m2222performSend$lambda16(MediaReviewFragment.this);
            }
        });
    }

    /* renamed from: performSend$lambda-14 */
    public static final void m2220performSend$lambda14(MediaReviewFragment mediaReviewFragment, MediaSendActivityResult mediaSendActivityResult) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        Callback callback = mediaReviewFragment.callback;
        if (callback == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
            callback = null;
        }
        Intrinsics.checkNotNullExpressionValue(mediaSendActivityResult, MediaSendActivityResult.EXTRA_RESULT);
        callback.onSentWithResult(mediaSendActivityResult);
    }

    /* renamed from: performSend$lambda-15 */
    public static final void m2221performSend$lambda15(MediaReviewFragment mediaReviewFragment, Throwable th) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        Callback callback = mediaReviewFragment.callback;
        if (callback == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
            callback = null;
        }
        Intrinsics.checkNotNullExpressionValue(th, "error");
        callback.onSendError(th);
    }

    /* renamed from: performSend$lambda-16 */
    public static final void m2222performSend$lambda16(MediaReviewFragment mediaReviewFragment) {
        Intrinsics.checkNotNullParameter(mediaReviewFragment, "this$0");
        Callback callback = mediaReviewFragment.callback;
        if (callback == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
            callback = null;
        }
        callback.onSentWithoutResult();
    }

    private final void presentAddMessageEntry(CharSequence charSequence) {
        TextView textView = this.addMessageEntry;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("addMessageEntry");
            textView = null;
        }
        textView.setText(charSequence, TextView.BufferType.SPANNABLE);
    }

    private final void presentImageQualityToggle(SentMediaQuality sentMediaQuality) {
        int i;
        ImageView imageView = this.qualityButton;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("qualityButton");
            imageView = null;
        }
        int i2 = WhenMappings.$EnumSwitchMapping$0[sentMediaQuality.ordinal()];
        if (i2 == 1) {
            i = R.drawable.ic_sq_24;
        } else if (i2 == 2) {
            i = R.drawable.ic_hq_24;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        imageView.setImageResource(i);
    }

    private final void presentSendButton(MessageSendType messageSendType) {
        int i = messageSendType.usesSignalTransport() ? R.color.signal_colorOnSecondaryContainer : R.color.core_grey_50;
        View view = this.sendButton;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("sendButton");
            view = null;
        }
        ViewCompat.setBackgroundTintList(view, ColorStateList.valueOf(ContextCompat.getColor(requireContext(), i)));
    }

    private final void presentPager(MediaSelectionState mediaSelectionState) {
        ViewPager2 viewPager2 = this.pager;
        ViewPager2 viewPager22 = null;
        if (viewPager2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("pager");
            viewPager2 = null;
        }
        viewPager2.setUserInputEnabled(mediaSelectionState.isTouchEnabled());
        int i = CollectionsKt___CollectionsKt.indexOf((List<? extends Media>) ((List<? extends Object>) mediaSelectionState.getSelectedMedia()), mediaSelectionState.getFocusedMedia());
        ViewPager2 viewPager23 = this.pager;
        if (viewPager23 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("pager");
            viewPager23 = null;
        }
        if (viewPager23.getCurrentItem() != i) {
            if (i != -1) {
                ViewPager2 viewPager24 = this.pager;
                if (viewPager24 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("pager");
                } else {
                    viewPager22 = viewPager24;
                }
                viewPager22.setCurrentItem(i, false);
                return;
            }
            ViewPager2 viewPager25 = this.pager;
            if (viewPager25 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("pager");
            } else {
                viewPager22 = viewPager25;
            }
            viewPager22.setCurrentItem(0, false);
        }
    }

    private final void computeViewStateAndAnimate(MediaSelectionState mediaSelectionState) {
        AnimatorSet animatorSet = this.animatorSet;
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(computeAddMessageAnimators(mediaSelectionState));
        arrayList.addAll(computeViewOnceButtonAnimators(mediaSelectionState));
        arrayList.addAll(computeAddMediaButtonsAnimators(mediaSelectionState));
        arrayList.addAll(computeSendButtonAnimators(mediaSelectionState));
        arrayList.addAll(computeSaveButtonAnimators(mediaSelectionState));
        arrayList.addAll(computeQualityButtonAnimators(mediaSelectionState));
        arrayList.addAll(computeCropAndRotateButtonAnimators(mediaSelectionState));
        arrayList.addAll(computeDrawToolButtonAnimators(mediaSelectionState));
        arrayList.addAll(computeRecipientDisplayAnimators(mediaSelectionState));
        arrayList.addAll(computeControlsShadeAnimators(mediaSelectionState));
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playTogether(arrayList);
        animatorSet2.setInterpolator(MediaAnimations.getInterpolator());
        animatorSet2.start();
        this.animatorSet = animatorSet2;
    }

    private final List<Animator> computeControlsShadeAnimators(MediaSelectionState mediaSelectionState) {
        if (mediaSelectionState.isTouchEnabled()) {
            MediaReviewAnimatorController mediaReviewAnimatorController = MediaReviewAnimatorController.INSTANCE;
            View view = this.controlsShade;
            if (view == null) {
                Intrinsics.throwUninitializedPropertyAccessException("controlsShade");
                view = null;
            }
            return CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController, view, false, 2, null));
        }
        MediaReviewAnimatorController mediaReviewAnimatorController2 = MediaReviewAnimatorController.INSTANCE;
        View view2 = this.controlsShade;
        if (view2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("controlsShade");
            view2 = null;
        }
        return CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController2, view2, false, 2, null));
    }

    private final List<Animator> computeAddMessageAnimators(MediaSelectionState mediaSelectionState) {
        if (!mediaSelectionState.isTouchEnabled()) {
            Animator[] animatorArr = new Animator[3];
            MediaReviewAnimatorController mediaReviewAnimatorController = MediaReviewAnimatorController.INSTANCE;
            TextView textView = this.viewOnceMessage;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewOnceMessage");
                textView = null;
            }
            animatorArr[0] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController, textView, false, 2, null);
            TextView textView2 = this.addMessageButton;
            if (textView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMessageButton");
                textView2 = null;
            }
            animatorArr[1] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController, textView2, false, 2, null);
            TextView textView3 = this.addMessageEntry;
            if (textView3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMessageEntry");
                textView3 = null;
            }
            animatorArr[2] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController, textView3, false, 2, null);
            return CollectionsKt__CollectionsKt.listOf((Object[]) animatorArr);
        } else if (mediaSelectionState.getViewOnceToggleState() == MediaSelectionState.ViewOnceToggleState.ONCE) {
            Animator[] animatorArr2 = new Animator[3];
            MediaReviewAnimatorController mediaReviewAnimatorController2 = MediaReviewAnimatorController.INSTANCE;
            TextView textView4 = this.viewOnceMessage;
            if (textView4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewOnceMessage");
                textView4 = null;
            }
            animatorArr2[0] = MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController2, textView4, false, 2, null);
            TextView textView5 = this.addMessageButton;
            if (textView5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMessageButton");
                textView5 = null;
            }
            animatorArr2[1] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController2, textView5, false, 2, null);
            TextView textView6 = this.addMessageEntry;
            if (textView6 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMessageEntry");
                textView6 = null;
            }
            animatorArr2[2] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController2, textView6, false, 2, null);
            return CollectionsKt__CollectionsKt.listOf((Object[]) animatorArr2);
        } else {
            CharSequence message = mediaSelectionState.getMessage();
            if (message == null || message.length() == 0) {
                Animator[] animatorArr3 = new Animator[3];
                MediaReviewAnimatorController mediaReviewAnimatorController3 = MediaReviewAnimatorController.INSTANCE;
                TextView textView7 = this.viewOnceMessage;
                if (textView7 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("viewOnceMessage");
                    textView7 = null;
                }
                animatorArr3[0] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController3, textView7, false, 2, null);
                TextView textView8 = this.addMessageButton;
                if (textView8 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("addMessageButton");
                    textView8 = null;
                }
                animatorArr3[1] = MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController3, textView8, false, 2, null);
                TextView textView9 = this.addMessageEntry;
                if (textView9 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("addMessageEntry");
                    textView9 = null;
                }
                animatorArr3[2] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController3, textView9, false, 2, null);
                return CollectionsKt__CollectionsKt.listOf((Object[]) animatorArr3);
            }
            Animator[] animatorArr4 = new Animator[3];
            MediaReviewAnimatorController mediaReviewAnimatorController4 = MediaReviewAnimatorController.INSTANCE;
            TextView textView10 = this.viewOnceMessage;
            if (textView10 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewOnceMessage");
                textView10 = null;
            }
            animatorArr4[0] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController4, textView10, false, 2, null);
            TextView textView11 = this.addMessageEntry;
            if (textView11 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMessageEntry");
                textView11 = null;
            }
            animatorArr4[1] = MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController4, textView11, false, 2, null);
            TextView textView12 = this.addMessageButton;
            if (textView12 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMessageButton");
                textView12 = null;
            }
            animatorArr4[2] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController4, textView12, false, 2, null);
            return CollectionsKt__CollectionsKt.listOf((Object[]) animatorArr4);
        }
    }

    private final List<Animator> computeViewOnceButtonAnimators(MediaSelectionState mediaSelectionState) {
        if (!mediaSelectionState.isTouchEnabled() || mediaSelectionState.getSelectedMedia().size() != 1 || mediaSelectionState.isStory()) {
            MediaReviewAnimatorController mediaReviewAnimatorController = MediaReviewAnimatorController.INSTANCE;
            ViewSwitcher viewSwitcher = this.viewOnceButton;
            if (viewSwitcher == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewOnceButton");
                viewSwitcher = null;
            }
            return CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController, viewSwitcher, false, 2, null));
        }
        MediaReviewAnimatorController mediaReviewAnimatorController2 = MediaReviewAnimatorController.INSTANCE;
        ViewSwitcher viewSwitcher2 = this.viewOnceButton;
        if (viewSwitcher2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewOnceButton");
            viewSwitcher2 = null;
        }
        return CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController2, viewSwitcher2, false, 2, null));
    }

    private final List<Animator> computeAddMediaButtonsAnimators(MediaSelectionState mediaSelectionState) {
        if (!mediaSelectionState.isTouchEnabled() || mediaSelectionState.getViewOnceToggleState() == MediaSelectionState.ViewOnceToggleState.ONCE) {
            Animator[] animatorArr = new Animator[2];
            MediaReviewAnimatorController mediaReviewAnimatorController = MediaReviewAnimatorController.INSTANCE;
            View view = this.addMediaButton;
            if (view == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMediaButton");
                view = null;
            }
            animatorArr[0] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController, view, false, 2, null);
            RecyclerView recyclerView = this.selectionRecycler;
            if (recyclerView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("selectionRecycler");
                recyclerView = null;
            }
            animatorArr[1] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController, recyclerView, false, 2, null);
            return CollectionsKt__CollectionsKt.listOf((Object[]) animatorArr);
        } else if (mediaSelectionState.getSelectedMedia().size() > 1) {
            Animator[] animatorArr2 = new Animator[2];
            MediaReviewAnimatorController mediaReviewAnimatorController2 = MediaReviewAnimatorController.INSTANCE;
            View view2 = this.addMediaButton;
            if (view2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMediaButton");
                view2 = null;
            }
            animatorArr2[0] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController2, view2, false, 2, null);
            RecyclerView recyclerView2 = this.selectionRecycler;
            if (recyclerView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("selectionRecycler");
                recyclerView2 = null;
            }
            animatorArr2[1] = MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController2, recyclerView2, false, 2, null);
            return CollectionsKt__CollectionsKt.listOf((Object[]) animatorArr2);
        } else {
            Animator[] animatorArr3 = new Animator[2];
            MediaReviewAnimatorController mediaReviewAnimatorController3 = MediaReviewAnimatorController.INSTANCE;
            View view3 = this.addMediaButton;
            if (view3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("addMediaButton");
                view3 = null;
            }
            animatorArr3[0] = MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController3, view3, false, 2, null);
            RecyclerView recyclerView3 = this.selectionRecycler;
            if (recyclerView3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("selectionRecycler");
                recyclerView3 = null;
            }
            animatorArr3[1] = MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController3, recyclerView3, false, 2, null);
            return CollectionsKt__CollectionsKt.listOf((Object[]) animatorArr3);
        }
    }

    private final List<Animator> computeSendButtonAnimators(MediaSelectionState mediaSelectionState) {
        List list;
        MediaReviewAnimatorController mediaReviewAnimatorController = MediaReviewAnimatorController.INSTANCE;
        View view = this.sendButton;
        View view2 = null;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("sendButton");
            view = null;
        }
        List list2 = CollectionsKt__CollectionsJVMKt.listOf(mediaReviewAnimatorController.getSlideInAnimator(view));
        if (mediaSelectionState.isTouchEnabled()) {
            View view3 = this.sendButton;
            if (view3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("sendButton");
            } else {
                view2 = view3;
            }
            list = CollectionsKt__CollectionsJVMKt.listOf(mediaReviewAnimatorController.getFadeInAnimator(view2, mediaSelectionState.getCanSend()));
        } else {
            View view4 = this.sendButton;
            if (view4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("sendButton");
            } else {
                view2 = view4;
            }
            list = CollectionsKt__CollectionsJVMKt.listOf(mediaReviewAnimatorController.getFadeOutAnimator(view2, mediaSelectionState.getCanSend()));
        }
        return CollectionsKt___CollectionsKt.plus((Collection) list2, (Iterable) list);
    }

    private final List<Animator> computeSaveButtonAnimators(MediaSelectionState mediaSelectionState) {
        List list;
        MediaReviewAnimatorController mediaReviewAnimatorController = MediaReviewAnimatorController.INSTANCE;
        View view = this.saveButton;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("saveButton");
            view = null;
        }
        List list2 = CollectionsKt__CollectionsJVMKt.listOf(mediaReviewAnimatorController.getSlideInAnimator(view));
        if (mediaSelectionState.isTouchEnabled()) {
            Media focusedMedia = mediaSelectionState.getFocusedMedia();
            if (!MediaUtil.isVideo(focusedMedia != null ? focusedMedia.getMimeType() : null)) {
                View view2 = this.saveButton;
                if (view2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("saveButton");
                    view2 = null;
                }
                list = CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController, view2, false, 2, null));
                return CollectionsKt___CollectionsKt.plus((Collection) list2, (Iterable) list);
            }
        }
        View view3 = this.saveButton;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("saveButton");
            view3 = null;
        }
        list = CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController, view3, false, 2, null));
        return CollectionsKt___CollectionsKt.plus((Collection) list2, (Iterable) list);
    }

    private final List<Animator> computeQualityButtonAnimators(MediaSelectionState mediaSelectionState) {
        List list;
        boolean z;
        MediaReviewAnimatorController mediaReviewAnimatorController = MediaReviewAnimatorController.INSTANCE;
        ImageView imageView = this.qualityButton;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("qualityButton");
            imageView = null;
        }
        List list2 = CollectionsKt__CollectionsJVMKt.listOf(mediaReviewAnimatorController.getSlideInAnimator(imageView));
        if (mediaSelectionState.isTouchEnabled() && !mediaSelectionState.isStory()) {
            List<Media> selectedMedia = mediaSelectionState.getSelectedMedia();
            if (!(selectedMedia instanceof Collection) || !selectedMedia.isEmpty()) {
                for (Media media : selectedMedia) {
                    if (MediaUtil.isImageType(media.getMimeType())) {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
            if (z) {
                MediaReviewAnimatorController mediaReviewAnimatorController2 = MediaReviewAnimatorController.INSTANCE;
                ImageView imageView2 = this.qualityButton;
                if (imageView2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("qualityButton");
                    imageView2 = null;
                }
                list = CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController2, imageView2, false, 2, null));
                return CollectionsKt___CollectionsKt.plus((Collection) list2, (Iterable) list);
            }
        }
        MediaReviewAnimatorController mediaReviewAnimatorController3 = MediaReviewAnimatorController.INSTANCE;
        ImageView imageView3 = this.qualityButton;
        if (imageView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("qualityButton");
            imageView3 = null;
        }
        list = CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController3, imageView3, false, 2, null));
        return CollectionsKt___CollectionsKt.plus((Collection) list2, (Iterable) list);
    }

    private final List<Animator> computeCropAndRotateButtonAnimators(MediaSelectionState mediaSelectionState) {
        List list;
        MediaReviewAnimatorController mediaReviewAnimatorController = MediaReviewAnimatorController.INSTANCE;
        View view = this.cropAndRotateButton;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cropAndRotateButton");
            view = null;
        }
        List list2 = CollectionsKt__CollectionsJVMKt.listOf(mediaReviewAnimatorController.getSlideInAnimator(view));
        if (mediaSelectionState.isTouchEnabled()) {
            Media focusedMedia = mediaSelectionState.getFocusedMedia();
            String mimeType = focusedMedia != null ? focusedMedia.getMimeType() : null;
            if (mimeType == null) {
                mimeType = "";
            }
            if (MediaUtil.isImageAndNotGif(mimeType)) {
                View view2 = this.cropAndRotateButton;
                if (view2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("cropAndRotateButton");
                    view2 = null;
                }
                list = CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController, view2, false, 2, null));
                return CollectionsKt___CollectionsKt.plus((Collection) list2, (Iterable) list);
            }
        }
        View view3 = this.cropAndRotateButton;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cropAndRotateButton");
            view3 = null;
        }
        list = CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController, view3, false, 2, null));
        return CollectionsKt___CollectionsKt.plus((Collection) list2, (Iterable) list);
    }

    private final List<Animator> computeDrawToolButtonAnimators(MediaSelectionState mediaSelectionState) {
        List list;
        MediaReviewAnimatorController mediaReviewAnimatorController = MediaReviewAnimatorController.INSTANCE;
        View view = this.drawToolButton;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("drawToolButton");
            view = null;
        }
        List list2 = CollectionsKt__CollectionsJVMKt.listOf(mediaReviewAnimatorController.getSlideInAnimator(view));
        if (mediaSelectionState.isTouchEnabled()) {
            Media focusedMedia = mediaSelectionState.getFocusedMedia();
            String mimeType = focusedMedia != null ? focusedMedia.getMimeType() : null;
            if (mimeType == null) {
                mimeType = "";
            }
            if (MediaUtil.isImageAndNotGif(mimeType)) {
                View view2 = this.drawToolButton;
                if (view2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("drawToolButton");
                    view2 = null;
                }
                list = CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController, view2, false, 2, null));
                return CollectionsKt___CollectionsKt.plus((Collection) list2, (Iterable) list);
            }
        }
        View view3 = this.drawToolButton;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("drawToolButton");
            view3 = null;
        }
        list = CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController, view3, false, 2, null));
        return CollectionsKt___CollectionsKt.plus((Collection) list2, (Iterable) list);
    }

    private final List<Animator> computeRecipientDisplayAnimators(MediaSelectionState mediaSelectionState) {
        if (!mediaSelectionState.isTouchEnabled() || mediaSelectionState.getRecipient() == null) {
            MediaReviewAnimatorController mediaReviewAnimatorController = MediaReviewAnimatorController.INSTANCE;
            TextView textView = this.recipientDisplay;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("recipientDisplay");
                textView = null;
            }
            return CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeOutAnimator$default(mediaReviewAnimatorController, textView, false, 2, null));
        }
        TextView textView2 = this.recipientDisplay;
        if (textView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("recipientDisplay");
            textView2 = null;
        }
        textView2.setText(mediaSelectionState.getRecipient().isSelf() ? requireContext().getString(R.string.note_to_self) : mediaSelectionState.getRecipient().getDisplayName(requireContext()));
        MediaReviewAnimatorController mediaReviewAnimatorController2 = MediaReviewAnimatorController.INSTANCE;
        TextView textView3 = this.recipientDisplay;
        if (textView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("recipientDisplay");
            textView3 = null;
        }
        return CollectionsKt__CollectionsJVMKt.listOf(MediaReviewAnimatorController.getFadeInAnimator$default(mediaReviewAnimatorController2, textView3, false, 2, null));
    }
}
