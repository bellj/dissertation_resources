package org.thoughtcrime.securesms.mediasend.v2.text;

import kotlin.Metadata;
import org.thoughtcrime.securesms.R;

/* compiled from: TextAlignment.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\t\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0019\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007j\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextAlignment;", "", "gravity", "", "icon", "(Ljava/lang/String;III)V", "getGravity", "()I", "getIcon", "START", "CENTER", "END", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum TextAlignment {
    START(8388627, R.drawable.ic_text_start),
    CENTER(17, R.drawable.ic_text_center),
    END(8388629, R.drawable.ic_text_end);
    
    private final int gravity;
    private final int icon;

    TextAlignment(int i, int i2) {
        this.gravity = i;
        this.icon = i2;
    }

    public final int getGravity() {
        return this.gravity;
    }

    public final int getIcon() {
        return this.icon;
    }
}
