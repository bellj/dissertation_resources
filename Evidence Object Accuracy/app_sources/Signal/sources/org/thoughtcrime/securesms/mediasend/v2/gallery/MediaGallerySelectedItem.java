package org.thoughtcrime.securesms.mediasend.v2.gallery;

import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: MediaGallerySelectedItem.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u000b\fB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00040\bj\u0002`\n¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectedItem;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "onSelectedMediaClicked", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/OnSelectedMediaClicked;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGallerySelectedItem {
    public static final MediaGallerySelectedItem INSTANCE = new MediaGallerySelectedItem();

    private MediaGallerySelectedItem() {
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m2176register$lambda0(Function1 function1, View view) {
        Intrinsics.checkNotNullParameter(function1, "$onSelectedMediaClicked");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view, function1);
    }

    public final void register(MappingAdapter mappingAdapter, Function1<? super Media, Unit> function1) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        Intrinsics.checkNotNullParameter(function1, "onSelectedMediaClicked");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectedItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MediaGallerySelectedItem.m2176register$lambda0(Function1.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.v2_media_selection_item));
    }

    /* compiled from: MediaGallerySelectedItem.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectedItem$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "media", "Lorg/thoughtcrime/securesms/mediasend/Media;", "(Lorg/thoughtcrime/securesms/mediasend/Media;)V", "getMedia", "()Lorg/thoughtcrime/securesms/mediasend/Media;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model implements MappingModel<Model> {
        private final Media media;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        public Model(Media media) {
            Intrinsics.checkNotNullParameter(media, "media");
            this.media = media;
        }

        public final Media getMedia() {
            return this.media;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.media.getUri(), model.media.getUri());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.media.getUri(), model.media.getUri());
        }
    }

    /* compiled from: MediaGallerySelectedItem.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B%\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006j\u0002`\t¢\u0006\u0002\u0010\nJ\u0010\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006j\u0002`\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectedItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectedItem$Model;", "itemView", "Landroid/view/View;", "onSelectedMediaClicked", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/OnSelectedMediaClicked;", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "imageView", "Landroid/widget/ImageView;", "videoOverlay", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final ImageView imageView;
        private final Function1<Media, Unit> onSelectedMediaClicked;
        private final ImageView videoOverlay;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.mediasend.Media, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view, Function1<? super Media, Unit> function1) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function1, "onSelectedMediaClicked");
            this.onSelectedMediaClicked = function1;
            View findViewById = view.findViewById(R.id.media_selection_image);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.media_selection_image)");
            this.imageView = (ImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.media_selection_play_overlay);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.…a_selection_play_overlay)");
            this.videoOverlay = (ImageView) findViewById2;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            Glide.with(this.imageView).load((Object) new DecryptableStreamUriLoader.DecryptableUri(model.getMedia().getUri())).centerCrop().into(this.imageView);
            ViewExtensionsKt.setVisible(this.videoOverlay, MediaUtil.isVideo(model.getMedia().getMimeType()) && !model.getMedia().isVideoGif());
            this.itemView.setOnClickListener(new MediaGallerySelectedItem$ViewHolder$$ExternalSyntheticLambda0(this, model));
        }

        /* renamed from: bind$lambda-0 */
        public static final void m2177bind$lambda0(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            viewHolder.onSelectedMediaClicked.invoke(model.getMedia());
        }
    }
}
