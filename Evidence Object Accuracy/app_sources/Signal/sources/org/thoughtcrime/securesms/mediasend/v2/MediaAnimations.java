package org.thoughtcrime.securesms.mediasend.v2;

import android.view.animation.Interpolator;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;

/* compiled from: MediaAnimations.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0005\u0010\u0002\u001a\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaAnimations;", "", "()V", "interpolator", "Landroid/view/animation/Interpolator;", "getInterpolator$annotations", "getInterpolator", "()Landroid/view/animation/Interpolator;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaAnimations {
    public static final MediaAnimations INSTANCE = new MediaAnimations();
    private static final Interpolator interpolator = new FastOutSlowInInterpolator();

    @JvmStatic
    public static /* synthetic */ void getInterpolator$annotations() {
    }

    private MediaAnimations() {
    }

    public static final Interpolator getInterpolator() {
        return interpolator;
    }
}
