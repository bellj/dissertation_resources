package org.thoughtcrime.securesms.mediasend.v2.text;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.fonts.TextFont;

/* compiled from: TextFontButton.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0012\u0010\u0011\u001a\u00020\n2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u000e\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\tR.\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\bj\u0004\u0018\u0001`\u000bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextFontButton;", "Landroidx/appcompat/widget/AppCompatImageView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "onTextFontChanged", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/fonts/TextFont;", "", "Lorg/thoughtcrime/securesms/mediasend/v2/text/OnTextFontChanged;", "getOnTextFontChanged", "()Lkotlin/jvm/functions/Function1;", "setOnTextFontChanged", "(Lkotlin/jvm/functions/Function1;)V", "textFont", "setOnClickListener", "l", "Landroid/view/View$OnClickListener;", "setTextFont", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextFontButton extends AppCompatImageView {
    private Function1<? super TextFont, Unit> onTextFontChanged;
    private TextFont textFont;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public TextFontButton(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ TextFontButton(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public TextFontButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        TextFont textFont = TextFont.REGULAR;
        this.textFont = textFont;
        setImageResource(textFont.getIcon());
        super.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextFontButton$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                TextFontButton.m2231$r8$lambda$pF2xs_mjTJ1XLIEKjwUFc__FJc(TextFontButton.this, view);
            }
        });
    }

    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.fonts.TextFont, kotlin.Unit>, kotlin.jvm.functions.Function1<org.thoughtcrime.securesms.fonts.TextFont, kotlin.Unit> */
    public final Function1<TextFont, Unit> getOnTextFontChanged() {
        return this.onTextFontChanged;
    }

    public final void setOnTextFontChanged(Function1<? super TextFont, Unit> function1) {
        this.onTextFontChanged = function1;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m2232_init_$lambda0(TextFontButton textFontButton, View view) {
        Intrinsics.checkNotNullParameter(textFontButton, "this$0");
        TextFont textFont = textFontButton.textFont;
        TextFont[] values = TextFont.values();
        textFontButton.setTextFont(values[(textFont.ordinal() + 1) % values.length]);
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        throw new UnsupportedOperationException();
    }

    public final void setTextFont(TextFont textFont) {
        Intrinsics.checkNotNullParameter(textFont, "textFont");
        if (textFont != this.textFont) {
            this.textFont = textFont;
            setImageResource(textFont.getIcon());
            Function1<? super TextFont, Unit> function1 = this.onTextFontChanged;
            if (function1 != null) {
                function1.invoke(textFont);
            }
        }
    }
}
