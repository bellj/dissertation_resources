package org.thoughtcrime.securesms.mediasend;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import org.thoughtcrime.securesms.InviteActivity;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.CameraContactAdapter;
import org.thoughtcrime.securesms.mediasend.CameraContactSelectionViewModel;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.ThemeUtil;

/* loaded from: classes4.dex */
public class CameraContactSelectionFragment extends LoggingFragment implements CameraContactAdapter.CameraContactListener {
    private ViewGroup cameraContactsEmpty;
    private CameraContactAdapter contactAdapter;
    private RecyclerView contactList;
    private CameraContactSelectionViewModel contactViewModel;
    private Controller controller;
    private View inviteButton;
    private CameraContactSelectionAdapter selectionAdapter;
    private Group selectionFooterGroup;
    private RecyclerView selectionList;
    private View sendButton;
    private Toolbar toolbar;

    /* loaded from: classes4.dex */
    public interface Controller {
        void onCameraContactsSendClicked(List<Recipient> list);
    }

    public static Fragment newInstance() {
        return new CameraContactSelectionFragment();
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
        this.contactViewModel = (CameraContactSelectionViewModel) ViewModelProviders.of(requireActivity(), new CameraContactSelectionViewModel.Factory(new CameraContactsRepository(requireContext()))).get(CameraContactSelectionViewModel.class);
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof Controller) {
            this.controller = (Controller) getActivity();
            return;
        }
        throw new IllegalStateException("Parent activity must implement controller interface.");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return ThemeUtil.getThemedInflater(layoutInflater.getContext(), layoutInflater, DynamicTheme.isDarkTheme(layoutInflater.getContext()) ? R.style.TextSecure_DarkTheme : R.style.TextSecure_LightTheme).inflate(R.layout.camera_contact_selection_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.contactList = (RecyclerView) view.findViewById(R.id.camera_contacts_list);
        this.selectionList = (RecyclerView) view.findViewById(R.id.camera_contacts_selected_list);
        this.toolbar = (Toolbar) view.findViewById(R.id.camera_contacts_toolbar);
        this.sendButton = view.findViewById(R.id.camera_contacts_send_button);
        this.selectionFooterGroup = (Group) view.findViewById(R.id.camera_contacts_footer_group);
        this.cameraContactsEmpty = (ViewGroup) view.findViewById(R.id.camera_contacts_empty);
        this.inviteButton = view.findViewById(R.id.camera_contacts_invite_button);
        this.contactAdapter = new CameraContactAdapter(GlideApp.with(this), this);
        this.selectionAdapter = new CameraContactSelectionAdapter();
        this.contactList.setLayoutManager(new LinearLayoutManager(requireContext()));
        this.contactList.setAdapter(this.contactAdapter);
        this.selectionList.setLayoutManager(new LinearLayoutManager(requireContext(), 0, false));
        this.selectionList.setAdapter(this.selectionAdapter);
        ((AppCompatActivity) requireActivity()).setSupportActionBar(this.toolbar);
        ((AppCompatActivity) requireActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CameraContactSelectionFragment.$r8$lambda$4IWGAO1mGyA6MbiALIuALjvVfK4(CameraContactSelectionFragment.this, view2);
            }
        });
        this.inviteButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CameraContactSelectionFragment.$r8$lambda$Nc5zT2Fr1rpA1jckkxs0d6GV4ps(CameraContactSelectionFragment.this, view2);
            }
        });
        initViewModel();
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        requireActivity().onBackPressed();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        onInviteContactsClicked();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPrepareOptionsMenu(Menu menu) {
        requireActivity().getMenuInflater().inflate(R.menu.camera_contacts, menu);
        MenuItem findItem = menu.findItem(R.id.menu_search);
        final SearchView searchView = (SearchView) findItem.getActionView();
        final AnonymousClass1 r1 = new SearchView.OnQueryTextListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionFragment.1
            @Override // androidx.appcompat.widget.SearchView.OnQueryTextListener
            public boolean onQueryTextSubmit(String str) {
                CameraContactSelectionFragment.this.contactViewModel.onQueryUpdated(str);
                return true;
            }

            @Override // androidx.appcompat.widget.SearchView.OnQueryTextListener
            public boolean onQueryTextChange(String str) {
                CameraContactSelectionFragment.this.contactViewModel.onQueryUpdated(str);
                return true;
            }
        };
        findItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionFragment.2
            @Override // android.view.MenuItem.OnActionExpandListener
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                searchView.setOnQueryTextListener(r1);
                return true;
            }

            @Override // android.view.MenuItem.OnActionExpandListener
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                searchView.setOnQueryTextListener(null);
                CameraContactSelectionFragment.this.contactViewModel.onSearchClosed();
                return true;
            }
        });
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraContactAdapter.CameraContactListener
    public void onContactClicked(Recipient recipient) {
        this.contactViewModel.onContactClicked(recipient);
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraContactAdapter.CameraContactListener
    public void onInviteContactsClicked() {
        startActivity(new Intent(requireContext(), InviteActivity.class));
    }

    private void initViewModel() {
        this.contactViewModel.getContacts().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CameraContactSelectionFragment.$r8$lambda$CYK71o3RzupJEDRgdXMpVsxzYjw(CameraContactSelectionFragment.this, (CameraContactSelectionViewModel.ContactState) obj);
            }
        });
        this.contactViewModel.getError().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionFragment$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CameraContactSelectionFragment.m2067$r8$lambda$0ovDJLE6REs80uSWgCjVYhgmFc(CameraContactSelectionFragment.this, (CameraContactSelectionViewModel.Error) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initViewModel$3(CameraContactSelectionViewModel.ContactState contactState) {
        if (contactState != null) {
            int i = 0;
            if (!contactState.getContacts().isEmpty() || !TextUtils.isEmpty(contactState.getQuery())) {
                this.cameraContactsEmpty.setVisibility(8);
                this.contactList.setVisibility(0);
                this.sendButton.setOnClickListener(new View.OnClickListener(contactState) { // from class: org.thoughtcrime.securesms.mediasend.CameraContactSelectionFragment$$ExternalSyntheticLambda0
                    public final /* synthetic */ CameraContactSelectionViewModel.ContactState f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        CameraContactSelectionFragment.$r8$lambda$xp1R8WtghTbHVsjWoa3bRSe3zmo(CameraContactSelectionFragment.this, this.f$1, view);
                    }
                });
                this.contactAdapter.setContacts(contactState.getContacts(), contactState.getSelected());
                this.selectionAdapter.setRecipients(contactState.getSelected());
                Group group = this.selectionFooterGroup;
                if (contactState.getSelected().isEmpty()) {
                    i = 8;
                }
                group.setVisibility(i);
                return;
            }
            this.cameraContactsEmpty.setVisibility(0);
            this.contactList.setVisibility(8);
            this.selectionFooterGroup.setVisibility(8);
        }
    }

    public /* synthetic */ void lambda$initViewModel$2(CameraContactSelectionViewModel.ContactState contactState, View view) {
        this.controller.onCameraContactsSendClicked(contactState.getSelected());
    }

    public /* synthetic */ void lambda$initViewModel$4(CameraContactSelectionViewModel.Error error) {
        if (error != null && error == CameraContactSelectionViewModel.Error.MAX_SELECTION) {
            Toast.makeText(requireContext(), getString(R.string.CameraContacts_you_can_share_with_a_maximum_of_n_conversations, 16), 0).show();
        }
    }
}
