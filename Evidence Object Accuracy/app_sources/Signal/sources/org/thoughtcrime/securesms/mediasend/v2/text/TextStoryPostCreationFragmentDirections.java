package org.thoughtcrime.securesms.mediasend.v2.text;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.MediaDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class TextStoryPostCreationFragmentDirections {
    private TextStoryPostCreationFragmentDirections() {
    }

    public static NavDirections actionTextStoryPostCreationFragmentToTextStoryPostSendFragment() {
        return new ActionOnlyNavDirections(R.id.action_textStoryPostCreationFragment_to_textStoryPostSendFragment);
    }

    public static NavDirections actionDirectlyToMediaCaptureFragment() {
        return MediaDirections.actionDirectlyToMediaCaptureFragment();
    }

    public static NavDirections actionDirectlyToMediaGalleryFragment() {
        return MediaDirections.actionDirectlyToMediaGalleryFragment();
    }

    public static NavDirections actionDirectlyToMediaReviewFragment() {
        return MediaDirections.actionDirectlyToMediaReviewFragment();
    }

    public static NavDirections actionDirectlyToTextPostCreationFragment() {
        return MediaDirections.actionDirectlyToTextPostCreationFragment();
    }
}
