package org.thoughtcrime.securesms.mediasend.v2.gallery;

import android.view.View;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectedItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaGallerySelectedItem$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ MediaGallerySelectedItem.ViewHolder f$0;
    public final /* synthetic */ MediaGallerySelectedItem.Model f$1;

    public /* synthetic */ MediaGallerySelectedItem$ViewHolder$$ExternalSyntheticLambda0(MediaGallerySelectedItem.ViewHolder viewHolder, MediaGallerySelectedItem.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        MediaGallerySelectedItem.ViewHolder.m2177bind$lambda0(this.f$0, this.f$1, view);
    }
}
