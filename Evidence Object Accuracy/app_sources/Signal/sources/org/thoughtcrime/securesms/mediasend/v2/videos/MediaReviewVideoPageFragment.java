package org.thoughtcrime.securesms.mediasend.v2.videos;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.VideoEditorFragment;
import org.thoughtcrime.securesms.mediasend.v2.HudCommand;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: MediaReviewVideoPageFragment.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0007\u0018\u0000 '2\u00020\u00012\u00020\u0002:\u0001'B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\f\u001a\u00020\u000bH\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u000eH\u0016J\u0010\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u001a\u0010\u001a\u001a\u00020\u000e2\u0006\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0012H\u0016J\u0012\u0010\u001e\u001a\u00020\u000e2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0012H\u0016J\b\u0010\u001f\u001a\u00020\u0015H\u0002J\b\u0010 \u001a\u00020!H\u0002J\b\u0010\"\u001a\u00020!H\u0002J\b\u0010#\u001a\u00020!H\u0002J\b\u0010$\u001a\u00020\u0018H\u0002J\b\u0010%\u001a\u00020\u000eH\u0002J\b\u0010&\u001a\u00020\u000eH\u0002R\u001b\u0010\u0004\u001a\u00020\u00058BX\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\n\u001a\u00020\u000bX.¢\u0006\u0002\n\u0000¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/videos/MediaReviewVideoPageFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/mediasend/VideoEditorFragment$Controller;", "()V", "sharedViewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "getSharedViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "sharedViewModel$delegate", "Lkotlin/Lazy;", "videoEditorFragment", "Lorg/thoughtcrime/securesms/mediasend/VideoEditorFragment;", "ensureVideoEditorFragment", "onPlayerError", "", "onPlayerReady", "onSaveInstanceState", "outState", "Landroid/os/Bundle;", "onTouchEventsNeeded", "needed", "", "onVideoBeginEdit", "uri", "Landroid/net/Uri;", "onVideoEndEdit", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "onViewStateRestored", "requireIsVideoGif", "requireMaxAttachmentSize", "", "requireMaxCompressedVideoSize", "requireMaxVideoDuration", "requireUri", "restoreVideoEditorState", "saveEditorState", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaReviewVideoPageFragment extends Fragment implements VideoEditorFragment.Controller {
    private static final String ARG_IS_VIDEO_GIF;
    private static final String ARG_URI;
    public static final Companion Companion = new Companion(null);
    private final Lazy sharedViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MediaSelectionViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.videos.MediaReviewVideoPageFragment$sharedViewModel$2
        final /* synthetic */ MediaReviewVideoPageFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.videos.MediaReviewVideoPageFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private VideoEditorFragment videoEditorFragment;

    public MediaReviewVideoPageFragment() {
        super(R.layout.fragment_container);
    }

    private final MediaSelectionViewModel getSharedViewModel() {
        return (MediaSelectionViewModel) this.sharedViewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        this.videoEditorFragment = ensureVideoEditorFragment();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        restoreVideoEditorState();
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "outState");
        super.onSaveInstanceState(bundle);
        saveEditorState();
    }

    private final void saveEditorState() {
        VideoEditorFragment videoEditorFragment = this.videoEditorFragment;
        if (videoEditorFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("videoEditorFragment");
            videoEditorFragment = null;
        }
        Object saveState = videoEditorFragment.saveState();
        if (saveState != null) {
            getSharedViewModel().setEditorState(requireUri(), saveState);
        }
    }

    @Override // org.thoughtcrime.securesms.mediasend.VideoEditorFragment.Controller
    public void onPlayerReady() {
        getSharedViewModel().sendCommand(HudCommand.ResumeEntryTransition.INSTANCE);
    }

    @Override // org.thoughtcrime.securesms.mediasend.VideoEditorFragment.Controller
    public void onPlayerError() {
        getSharedViewModel().sendCommand(HudCommand.ResumeEntryTransition.INSTANCE);
    }

    @Override // org.thoughtcrime.securesms.mediasend.VideoEditorFragment.Controller
    public void onTouchEventsNeeded(boolean z) {
        getSharedViewModel().setTouchEnabled(!z);
    }

    @Override // org.thoughtcrime.securesms.mediasend.VideoEditorFragment.Controller
    public void onVideoBeginEdit(Uri uri) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        getSharedViewModel().onVideoBeginEdit(uri);
    }

    @Override // org.thoughtcrime.securesms.mediasend.VideoEditorFragment.Controller
    public void onVideoEndEdit(Uri uri) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        saveEditorState();
    }

    private final void restoreVideoEditorState() {
        Object editorState = getSharedViewModel().getEditorState(requireUri());
        VideoEditorFragment videoEditorFragment = null;
        VideoEditorFragment.Data data = editorState instanceof VideoEditorFragment.Data ? (VideoEditorFragment.Data) editorState : null;
        if (data != null) {
            VideoEditorFragment videoEditorFragment2 = this.videoEditorFragment;
            if (videoEditorFragment2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("videoEditorFragment");
            } else {
                videoEditorFragment = videoEditorFragment2;
            }
            videoEditorFragment.restoreState(data);
        }
    }

    private final VideoEditorFragment ensureVideoEditorFragment() {
        Fragment findFragmentByTag = getChildFragmentManager().findFragmentByTag("video.editor.fragment");
        VideoEditorFragment videoEditorFragment = findFragmentByTag instanceof VideoEditorFragment ? (VideoEditorFragment) findFragmentByTag : null;
        if (videoEditorFragment != null) {
            return videoEditorFragment;
        }
        VideoEditorFragment newInstance = VideoEditorFragment.newInstance(requireUri(), requireMaxCompressedVideoSize(), requireMaxAttachmentSize(), requireIsVideoGif(), requireMaxVideoDuration());
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, newInstance, "video.editor.fragment").commitAllowingStateLoss();
        Intrinsics.checkNotNullExpressionValue(newInstance, "{\n      val videoEditorF…videoEditorFragment\n    }");
        return newInstance;
    }

    private final Uri requireUri() {
        Parcelable parcelable = requireArguments().getParcelable(ARG_URI);
        if (parcelable != null) {
            return (Uri) parcelable;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    private final long requireMaxCompressedVideoSize() {
        return (long) getSharedViewModel().getMediaConstraints().getCompressedVideoMaxSize(requireContext());
    }

    private final long requireMaxAttachmentSize() {
        return (long) getSharedViewModel().getMediaConstraints().getVideoMaxSize(requireContext());
    }

    private final boolean requireIsVideoGif() {
        return requireArguments().getBoolean(ARG_IS_VIDEO_GIF);
    }

    private final long requireMaxVideoDuration() {
        if (!getSharedViewModel().isStory() || MediaConstraints.isVideoTranscodeAvailable()) {
            return Long.MAX_VALUE;
        }
        return Stories.MAX_VIDEO_DURATION_MILLIS;
    }

    /* compiled from: MediaReviewVideoPageFragment.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/videos/MediaReviewVideoPageFragment$Companion;", "", "()V", "ARG_IS_VIDEO_GIF", "", "ARG_URI", "newInstance", "Landroidx/fragment/app/Fragment;", "uri", "Landroid/net/Uri;", "isVideoGif", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Fragment newInstance(Uri uri, boolean z) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            MediaReviewVideoPageFragment mediaReviewVideoPageFragment = new MediaReviewVideoPageFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(MediaReviewVideoPageFragment.ARG_URI, uri);
            bundle.putBoolean(MediaReviewVideoPageFragment.ARG_IS_VIDEO_GIF, z);
            mediaReviewVideoPageFragment.setArguments(bundle);
            return mediaReviewVideoPageFragment;
        }
    }
}
