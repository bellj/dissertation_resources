package org.thoughtcrime.securesms.mediasend.camerax;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import java.util.Arrays;
import java.util.List;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class CameraXFlashToggleView extends AppCompatImageView {
    private static final int[] FLASH_AUTO;
    private static final int[][] FLASH_ENUM;
    private static final FlashMode FLASH_FALLBACK;
    private static final List<FlashMode> FLASH_MODES;
    private static final int[] FLASH_OFF;
    private static final int[] FLASH_ON;
    private static final String STATE_FLASH_INDEX;
    private static final String STATE_PARENT;
    private static final String STATE_SUPPORT_AUTO;
    private int flashIndex;
    private OnFlashModeChangedListener flashModeChangedListener;
    private boolean supportsFlashModeAuto;

    /* loaded from: classes4.dex */
    public interface OnFlashModeChangedListener {
        void flashModeChanged(int i);
    }

    static {
        int[] iArr = {R.attr.state_flash_auto};
        FLASH_AUTO = iArr;
        int[] iArr2 = {R.attr.state_flash_off};
        FLASH_OFF = iArr2;
        int[] iArr3 = {R.attr.state_flash_on};
        FLASH_ON = iArr3;
        FLASH_ENUM = new int[][]{iArr, iArr2, iArr3};
        FlashMode flashMode = FlashMode.OFF;
        FLASH_MODES = Arrays.asList(FlashMode.AUTO, flashMode, FlashMode.ON);
        FLASH_FALLBACK = flashMode;
    }

    public CameraXFlashToggleView(Context context) {
        this(context, null);
    }

    public CameraXFlashToggleView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CameraXFlashToggleView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.supportsFlashModeAuto = true;
        super.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.camerax.CameraXFlashToggleView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CameraXFlashToggleView.this.lambda$new$0(view);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(View view) {
        setFlash(FLASH_MODES.get((this.flashIndex + 1) % FLASH_ENUM.length).getFlashMode());
    }

    @Override // android.widget.ImageView, android.view.View
    public int[] onCreateDrawableState(int i) {
        int[] iArr = FLASH_ENUM[this.flashIndex];
        int[] onCreateDrawableState = super.onCreateDrawableState(i + iArr.length);
        ImageView.mergeDrawableStates(onCreateDrawableState, iArr);
        return onCreateDrawableState;
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        throw new IllegalStateException("This View does not support custom click listeners.");
    }

    public void setAutoFlashEnabled(boolean z) {
        this.supportsFlashModeAuto = z;
        setFlash(FLASH_MODES.get(this.flashIndex).getFlashMode());
    }

    public void setFlash(int i) {
        this.flashIndex = resolveFlashIndex(FLASH_MODES.indexOf(FlashMode.fromImageCaptureFlashMode(i)), this.supportsFlashModeAuto);
        refreshDrawableState();
        notifyListener();
    }

    public void setOnFlashModeChangedListener(OnFlashModeChangedListener onFlashModeChangedListener) {
        this.flashModeChangedListener = onFlashModeChangedListener;
    }

    @Override // android.view.View
    protected Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable(STATE_PARENT, onSaveInstanceState);
        bundle.putInt(STATE_FLASH_INDEX, this.flashIndex);
        bundle.putBoolean(STATE_SUPPORT_AUTO, this.supportsFlashModeAuto);
        return bundle;
    }

    @Override // android.view.View
    protected void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.supportsFlashModeAuto = bundle.getBoolean(STATE_SUPPORT_AUTO);
            setFlash(FLASH_MODES.get(resolveFlashIndex(bundle.getInt(STATE_FLASH_INDEX), this.supportsFlashModeAuto)).getFlashMode());
            super.onRestoreInstanceState(bundle.getParcelable(STATE_PARENT));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    private void notifyListener() {
        OnFlashModeChangedListener onFlashModeChangedListener = this.flashModeChangedListener;
        if (onFlashModeChangedListener != null) {
            onFlashModeChangedListener.flashModeChanged(FLASH_MODES.get(this.flashIndex).getFlashMode());
        }
    }

    private static int resolveFlashIndex(int i, boolean z) {
        if (!isIllegalFlashIndex(i)) {
            return isUnsupportedFlashMode(i, z) ? FLASH_MODES.indexOf(FLASH_FALLBACK) : i;
        }
        throw new IllegalArgumentException("Unsupported index: " + i);
    }

    private static boolean isIllegalFlashIndex(int i) {
        return i < 0 || i > FLASH_ENUM.length;
    }

    private static boolean isUnsupportedFlashMode(int i, boolean z) {
        return FLASH_MODES.get(i) == FlashMode.AUTO && !z;
    }

    /* loaded from: classes4.dex */
    public enum FlashMode {
        AUTO(0),
        OFF(2),
        ON(1);
        
        private final int flashMode;

        FlashMode(int i) {
            this.flashMode = i;
        }

        int getFlashMode() {
            return this.flashMode;
        }

        public static FlashMode fromImageCaptureFlashMode(int i) {
            FlashMode[] values = values();
            for (FlashMode flashMode : values) {
                if (flashMode.getFlashMode() == i) {
                    return flashMode;
                }
            }
            throw new AssertionError();
        }
    }
}
