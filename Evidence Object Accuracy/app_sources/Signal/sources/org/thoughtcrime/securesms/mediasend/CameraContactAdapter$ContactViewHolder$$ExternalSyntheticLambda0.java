package org.thoughtcrime.securesms.mediasend;

import android.view.View;
import org.thoughtcrime.securesms.mediasend.CameraContactAdapter;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CameraContactAdapter$ContactViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CameraContactAdapter.CameraContactListener f$0;
    public final /* synthetic */ Recipient f$1;

    public /* synthetic */ CameraContactAdapter$ContactViewHolder$$ExternalSyntheticLambda0(CameraContactAdapter.CameraContactListener cameraContactListener, Recipient recipient) {
        this.f$0 = cameraContactListener;
        this.f$1 = recipient;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onContactClicked(this.f$1);
    }
}
