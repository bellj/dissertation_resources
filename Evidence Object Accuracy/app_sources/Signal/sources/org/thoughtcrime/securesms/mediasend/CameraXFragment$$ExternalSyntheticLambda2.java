package org.thoughtcrime.securesms.mediasend;

import androidx.core.util.Consumer;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CameraXFragment$$ExternalSyntheticLambda2 implements Consumer {
    public final /* synthetic */ CameraXFragment f$0;

    public /* synthetic */ CameraXFragment$$ExternalSyntheticLambda2(CameraXFragment cameraXFragment) {
        this.f$0 = cameraXFragment;
    }

    @Override // androidx.core.util.Consumer
    public final void accept(Object obj) {
        this.f$0.handleCameraInitializationError((Throwable) obj);
    }
}
