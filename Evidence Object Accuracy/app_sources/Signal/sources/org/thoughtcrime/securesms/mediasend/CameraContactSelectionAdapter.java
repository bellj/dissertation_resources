package org.thoughtcrime.securesms.mediasend;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FromTextView;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.adapter.StableIdGenerator;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class CameraContactSelectionAdapter extends RecyclerView.Adapter<RecipientViewHolder> {
    private final StableIdGenerator<String> idGenerator = new StableIdGenerator<>();
    private final List<Recipient> recipients = new ArrayList();

    public CameraContactSelectionAdapter() {
        setHasStableIds(true);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return this.idGenerator.getId(this.recipients.get(i).getId().serialize());
    }

    public RecipientViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new RecipientViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.camera_contact_selection_item, viewGroup, false));
    }

    public void onBindViewHolder(RecipientViewHolder recipientViewHolder, int i) {
        Recipient recipient = this.recipients.get(i);
        boolean z = true;
        if (i != this.recipients.size() - 1) {
            z = false;
        }
        recipientViewHolder.bind(recipient, z);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.recipients.size();
    }

    public void setRecipients(List<Recipient> list) {
        this.recipients.clear();
        this.recipients.addAll(list);
        notifyDataSetChanged();
    }

    /* loaded from: classes4.dex */
    public static class RecipientViewHolder extends RecyclerView.ViewHolder {
        private final FromTextView name;

        RecipientViewHolder(View view) {
            super(view);
            this.name = (FromTextView) view;
        }

        void bind(Recipient recipient, boolean z) {
            this.name.setText(recipient, true, z ? null : ",");
        }
    }
}
