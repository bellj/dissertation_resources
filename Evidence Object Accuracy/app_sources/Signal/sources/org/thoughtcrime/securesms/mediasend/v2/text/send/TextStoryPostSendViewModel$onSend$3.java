package org.thoughtcrime.securesms.mediasend.v2.text.send;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendResult;

/* compiled from: TextStoryPostSendViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryPostSendViewModel$onSend$3 extends Lambda implements Function1<TextStoryPostSendResult, Unit> {
    final /* synthetic */ TextStoryPostSendViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public TextStoryPostSendViewModel$onSend$3(TextStoryPostSendViewModel textStoryPostSendViewModel) {
        super(1);
        this.this$0 = textStoryPostSendViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(TextStoryPostSendResult textStoryPostSendResult) {
        invoke(textStoryPostSendResult);
        return Unit.INSTANCE;
    }

    public final void invoke(TextStoryPostSendResult textStoryPostSendResult) {
        Intrinsics.checkNotNullParameter(textStoryPostSendResult, "it");
        if (textStoryPostSendResult instanceof TextStoryPostSendResult.Success) {
            this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendViewModel$onSend$3$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return TextStoryPostSendViewModel$onSend$3.m2290invoke$lambda0((TextStoryPostSendState) obj);
                }
            });
        } else if (textStoryPostSendResult instanceof TextStoryPostSendResult.UntrustedRecordsError) {
            this.this$0.untrustedIdentitySubject.onNext(((TextStoryPostSendResult.UntrustedRecordsError) textStoryPostSendResult).getUntrustedRecords());
            this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendViewModel$onSend$3$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return TextStoryPostSendViewModel$onSend$3.m2291invoke$lambda1((TextStoryPostSendState) obj);
                }
            });
        } else if (textStoryPostSendResult instanceof TextStoryPostSendResult.Failure) {
            this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendViewModel$onSend$3$$ExternalSyntheticLambda2
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return TextStoryPostSendViewModel$onSend$3.m2292invoke$lambda2((TextStoryPostSendState) obj);
                }
            });
        }
    }

    /* renamed from: invoke$lambda-0 */
    public static final TextStoryPostSendState m2290invoke$lambda0(TextStoryPostSendState textStoryPostSendState) {
        return TextStoryPostSendState.SENT;
    }

    /* renamed from: invoke$lambda-1 */
    public static final TextStoryPostSendState m2291invoke$lambda1(TextStoryPostSendState textStoryPostSendState) {
        return TextStoryPostSendState.INIT;
    }

    /* renamed from: invoke$lambda-2 */
    public static final TextStoryPostSendState m2292invoke$lambda2(TextStoryPostSendState textStoryPostSendState) {
        return TextStoryPostSendState.FAILED;
    }
}
