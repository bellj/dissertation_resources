package org.thoughtcrime.securesms.mediasend.v2.capture;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.MediaDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class MediaCaptureFragmentDirections {
    private MediaCaptureFragmentDirections() {
    }

    public static NavDirections actionMediaCaptureFragmentToMediaGalleryFragment() {
        return new ActionOnlyNavDirections(R.id.action_mediaCaptureFragment_to_mediaGalleryFragment);
    }

    public static NavDirections actionMediaCaptureFragmentToTextStoryPostCreationFragment() {
        return new ActionOnlyNavDirections(R.id.action_mediaCaptureFragment_to_textStoryPostCreationFragment);
    }

    public static NavDirections actionDirectlyToMediaCaptureFragment() {
        return MediaDirections.actionDirectlyToMediaCaptureFragment();
    }

    public static NavDirections actionDirectlyToMediaGalleryFragment() {
        return MediaDirections.actionDirectlyToMediaGalleryFragment();
    }

    public static NavDirections actionDirectlyToMediaReviewFragment() {
        return MediaDirections.actionDirectlyToMediaReviewFragment();
    }

    public static NavDirections actionDirectlyToTextPostCreationFragment() {
        return MediaDirections.actionDirectlyToTextPostCreationFragment();
    }
}
