package org.thoughtcrime.securesms.mediasend.v2.review;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;

/* loaded from: classes4.dex */
public class MediaSelectionItemTouchHelper extends ItemTouchHelper.Callback {
    private final MediaSelectionViewModel viewModel;

    @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int i) {
    }

    public MediaSelectionItemTouchHelper(MediaSelectionViewModel mediaSelectionViewModel) {
        this.viewModel = mediaSelectionViewModel;
    }

    @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        if (this.viewModel.isValidMediaDragPosition(viewHolder.getAdapterPosition())) {
            return ItemTouchHelper.Callback.makeMovementFlags(12, 0);
        }
        return 0;
    }

    @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
        return this.viewModel.swapMedia(viewHolder.getAdapterPosition(), viewHolder2.getAdapterPosition());
    }

    @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        this.viewModel.onMediaDragFinished();
    }
}
