package org.thoughtcrime.securesms.mediasend.v2.capture;

import androidx.navigation.NavController;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.mediasend.CameraFragment;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionNavigator;

/* compiled from: MediaCaptureFragment.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class MediaCaptureFragment$onGalleryClicked$1 extends Lambda implements Function0<Unit> {
    final /* synthetic */ NavController $controller;
    final /* synthetic */ MediaCaptureFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MediaCaptureFragment$onGalleryClicked$1(MediaCaptureFragment mediaCaptureFragment, NavController navController) {
        super(0);
        this.this$0 = mediaCaptureFragment;
        this.$controller = navController;
    }

    @Override // kotlin.jvm.functions.Function0
    public final void invoke() {
        CameraFragment cameraFragment = this.this$0.captureChildFragment;
        if (cameraFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("captureChildFragment");
            cameraFragment = null;
        }
        cameraFragment.fadeOutControls(new Runnable(this.$controller) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureFragment$onGalleryClicked$1$$ExternalSyntheticLambda0
            public final /* synthetic */ NavController f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaCaptureFragment$onGalleryClicked$1.m2142invoke$lambda0(MediaCaptureFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m2142invoke$lambda0(MediaCaptureFragment mediaCaptureFragment, NavController navController) {
        Intrinsics.checkNotNullParameter(mediaCaptureFragment, "this$0");
        Intrinsics.checkNotNullParameter(navController, "$controller");
        MediaSelectionNavigator mediaSelectionNavigator = mediaCaptureFragment.navigator;
        if (mediaSelectionNavigator == null) {
            Intrinsics.throwUninitializedPropertyAccessException("navigator");
            mediaSelectionNavigator = null;
        }
        mediaSelectionNavigator.goToGallery(navController);
    }
}
