package org.thoughtcrime.securesms.mediasend.v2.text;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.random.Random;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;

/* compiled from: TextStoryBackgroundColors.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0005J\u0006\u0010\b\u001a\u00020\u0005J\b\u0010\t\u001a\u00020\u0005H\u0007R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryBackgroundColors;", "", "()V", "backgroundColors", "", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "cycleBackgroundColor", "chatColors", "getInitialBackgroundColor", "getRandomBackgroundColor", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryBackgroundColors {
    public static final TextStoryBackgroundColors INSTANCE = new TextStoryBackgroundColors();
    private static final List<ChatColors> backgroundColors;

    private TextStoryBackgroundColors() {
    }

    static {
        INSTANCE = new TextStoryBackgroundColors();
        ChatColors.Companion companion = ChatColors.Companion;
        ChatColors.Id.NotSet notSet = ChatColors.Id.NotSet.INSTANCE;
        backgroundColors = CollectionsKt__CollectionsKt.listOf((Object[]) new ChatColors[]{companion.forGradient(notSet, new ChatColors.LinearGradient(191.41f, new int[]{-706492, -12437617}, new float[]{0.0f, 1.0f})), companion.forGradient(notSet, new ChatColors.LinearGradient(192.04f, new int[]{-1028890, -15847459}, new float[]{0.0f, 1.0f})), companion.forGradient(notSet, new ChatColors.LinearGradient(175.46f, new int[]{-16316, -107464}, new float[]{0.0f, 1.0f})), companion.forGradient(notSet, new ChatColors.LinearGradient(180.0f, new int[]{-16739351, -8335161}, new float[]{0.0f, 1.0f})), companion.forGradient(notSet, new ChatColors.LinearGradient(180.0f, new int[]{-10105428, -16082598}, new float[]{0.0f, 1.0f})), companion.forColor(notSet, -16045), companion.forColor(notSet, -3359437), companion.forColor(notSet, -8097490), companion.forColor(notSet, -16141445), companion.forColor(notSet, -7631879), companion.forColor(notSet, -11447818), companion.forColor(notSet, -561554), companion.forColor(notSet, -3652031), companion.forColor(notSet, -3750747), companion.forColor(notSet, -5991019), companion.forColor(notSet, -14079703)});
    }

    public final ChatColors getInitialBackgroundColor() {
        return (ChatColors) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) backgroundColors));
    }

    public final ChatColors cycleBackgroundColor(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        List<ChatColors> list = backgroundColors;
        return list.get((list.indexOf(chatColors) + 1) % list.size());
    }

    @JvmStatic
    public static final ChatColors getRandomBackgroundColor() {
        return (ChatColors) CollectionsKt___CollectionsKt.random(backgroundColors, Random.Default);
    }
}
