package org.thoughtcrime.securesms.mediasend.v2.gallery;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: MediaGalleryState.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B-\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0012\b\u0002\u0010\u0005\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00070\u0006¢\u0006\u0002\u0010\bJ\u000b\u0010\u000e\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0013\u0010\u0010\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00070\u0006HÆ\u0003J5\u0010\u0011\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0012\b\u0002\u0010\u0005\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00070\u0006HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u001b\u0010\u0005\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryState;", "", "bucketId", "", "bucketTitle", "items", "", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getBucketId", "()Ljava/lang/String;", "getBucketTitle", "getItems", "()Ljava/util/List;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGalleryState {
    private final String bucketId;
    private final String bucketTitle;
    private final List<MappingModel<?>> items;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ MediaGalleryState copy$default(MediaGalleryState mediaGalleryState, String str, String str2, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            str = mediaGalleryState.bucketId;
        }
        if ((i & 2) != 0) {
            str2 = mediaGalleryState.bucketTitle;
        }
        if ((i & 4) != 0) {
            list = mediaGalleryState.items;
        }
        return mediaGalleryState.copy(str, str2, list);
    }

    public final String component1() {
        return this.bucketId;
    }

    public final String component2() {
        return this.bucketTitle;
    }

    public final List<MappingModel<?>> component3() {
        return this.items;
    }

    public final MediaGalleryState copy(String str, String str2, List<? extends MappingModel<?>> list) {
        Intrinsics.checkNotNullParameter(list, "items");
        return new MediaGalleryState(str, str2, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaGalleryState)) {
            return false;
        }
        MediaGalleryState mediaGalleryState = (MediaGalleryState) obj;
        return Intrinsics.areEqual(this.bucketId, mediaGalleryState.bucketId) && Intrinsics.areEqual(this.bucketTitle, mediaGalleryState.bucketTitle) && Intrinsics.areEqual(this.items, mediaGalleryState.items);
    }

    public int hashCode() {
        String str = this.bucketId;
        int i = 0;
        int hashCode = (str == null ? 0 : str.hashCode()) * 31;
        String str2 = this.bucketTitle;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return ((hashCode + i) * 31) + this.items.hashCode();
    }

    public String toString() {
        return "MediaGalleryState(bucketId=" + this.bucketId + ", bucketTitle=" + this.bucketTitle + ", items=" + this.items + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.util.adapter.mapping.MappingModel<?>> */
    /* JADX WARN: Multi-variable type inference failed */
    public MediaGalleryState(String str, String str2, List<? extends MappingModel<?>> list) {
        Intrinsics.checkNotNullParameter(list, "items");
        this.bucketId = str;
        this.bucketTitle = str2;
        this.items = list;
    }

    public final String getBucketId() {
        return this.bucketId;
    }

    public final String getBucketTitle() {
        return this.bucketTitle;
    }

    public /* synthetic */ MediaGalleryState(String str, String str2, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, str2, (i & 4) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list);
    }

    public final List<MappingModel<?>> getItems() {
        return this.items;
    }
}
