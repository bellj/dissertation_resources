package org.thoughtcrime.securesms.mediasend.v2.gallery;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.mediasend.MediaFolder;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: MediaGalleryViewModel.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0016B!\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u001c\u0010\u000f\u001a\u00020\u00102\b\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0002J\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u0015R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryViewModel;", "Landroidx/lifecycle/ViewModel;", "bucketId", "", "bucketTitle", "repository", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryRepository;", "(Ljava/lang/String;Ljava/lang/String;Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryRepository;)V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "loadItemsForBucket", "", "pop", "", "setMediaFolder", "mediaFolder", "Lorg/thoughtcrime/securesms/mediasend/MediaFolder;", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGalleryViewModel extends ViewModel {
    private final MediaGalleryRepository repository;
    private final LiveData<MediaGalleryState> state;
    private final Store<MediaGalleryState> store;

    public MediaGalleryViewModel(String str, String str2, MediaGalleryRepository mediaGalleryRepository) {
        Intrinsics.checkNotNullParameter(mediaGalleryRepository, "repository");
        this.repository = mediaGalleryRepository;
        Store<MediaGalleryState> store = new Store<>(new MediaGalleryState(str, str2, null, 4, null));
        this.store = store;
        LiveData<MediaGalleryState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        loadItemsForBucket(str, str2);
    }

    public final LiveData<MediaGalleryState> getState() {
        return this.state;
    }

    public final boolean pop() {
        if (this.store.getState().getBucketId() == null) {
            return true;
        }
        loadItemsForBucket(null, null);
        return false;
    }

    public final void setMediaFolder(MediaFolder mediaFolder) {
        Intrinsics.checkNotNullParameter(mediaFolder, "mediaFolder");
        loadItemsForBucket(mediaFolder.getBucketId(), mediaFolder.getTitle());
    }

    private final void loadItemsForBucket(String str, String str2) {
        if (str == null) {
            this.repository.getFolders(new MediaGalleryViewModel$loadItemsForBucket$1(this, str, str2));
        } else {
            this.repository.getMedia(str, new MediaGalleryViewModel$loadItemsForBucket$2(this, str, str2));
        }
    }

    /* compiled from: MediaGalleryViewModel.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B!\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J%\u0010\b\u001a\u0002H\t\"\b\b\u0000\u0010\t*\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\t0\fH\u0016¢\u0006\u0002\u0010\rR\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "bucketId", "", "bucketTitle", "repository", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryRepository;", "(Ljava/lang/String;Ljava/lang/String;Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final String bucketId;
        private final String bucketTitle;
        private final MediaGalleryRepository repository;

        public Factory(String str, String str2, MediaGalleryRepository mediaGalleryRepository) {
            Intrinsics.checkNotNullParameter(mediaGalleryRepository, "repository");
            this.bucketId = str;
            this.bucketTitle = str2;
            this.repository = mediaGalleryRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new MediaGalleryViewModel(this.bucketId, this.bucketTitle, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
