package org.thoughtcrime.securesms.mediasend.v2.gallery;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.MediaDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class MediaSelectionGalleryFragmentDirections {
    private MediaSelectionGalleryFragmentDirections() {
    }

    public static NavDirections actionMediaGalleryFragmentToMediaCaptureFragment() {
        return new ActionOnlyNavDirections(R.id.action_mediaGalleryFragment_to_mediaCaptureFragment);
    }

    public static NavDirections actionDirectlyToMediaCaptureFragment() {
        return MediaDirections.actionDirectlyToMediaCaptureFragment();
    }

    public static NavDirections actionDirectlyToMediaGalleryFragment() {
        return MediaDirections.actionDirectlyToMediaGalleryFragment();
    }

    public static NavDirections actionDirectlyToMediaReviewFragment() {
        return MediaDirections.actionDirectlyToMediaReviewFragment();
    }

    public static NavDirections actionDirectlyToTextPostCreationFragment() {
        return MediaDirections.actionDirectlyToTextPostCreationFragment();
    }
}
