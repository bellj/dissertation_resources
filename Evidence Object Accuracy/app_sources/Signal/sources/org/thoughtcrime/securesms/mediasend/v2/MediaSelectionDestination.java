package org.thoughtcrime.securesms.mediasend.v2;

import android.os.Bundle;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: MediaSelectionDestination.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u000b2\u00020\u0001:\u0006\t\n\u000b\f\r\u000eB\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\n\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0016J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH&\u0001\u0005\u000f\u0010\u0011\u0012\u0013¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "", "()V", "getRecipientSearchKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "getRecipientSearchKeyList", "", "toBundle", "Landroid/os/Bundle;", "Avatar", "ChooseAfterMediaSelection", "Companion", "MultipleRecipients", "SingleRecipient", "Wallpaper", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$Wallpaper;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$Avatar;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$ChooseAfterMediaSelection;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$SingleRecipient;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$MultipleRecipients;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class MediaSelectionDestination {
    private static final String AVATAR;
    public static final Companion Companion = new Companion(null);
    private static final String RECIPIENT;
    private static final String RECIPIENT_LIST;
    private static final String WALLPAPER;

    public /* synthetic */ MediaSelectionDestination(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    public ContactSearchKey.RecipientSearchKey getRecipientSearchKey() {
        return null;
    }

    public abstract Bundle toBundle();

    private MediaSelectionDestination() {
    }

    /* compiled from: MediaSelectionDestination.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$Wallpaper;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "()V", "toBundle", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Wallpaper extends MediaSelectionDestination {
        public static final Wallpaper INSTANCE = new Wallpaper();

        private Wallpaper() {
            super(null);
        }

        @Override // org.thoughtcrime.securesms.mediasend.v2.MediaSelectionDestination
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putBoolean(MediaSelectionDestination.WALLPAPER, true);
            return bundle;
        }
    }

    /* compiled from: MediaSelectionDestination.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$Avatar;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "()V", "toBundle", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Avatar extends MediaSelectionDestination {
        public static final Avatar INSTANCE = new Avatar();

        private Avatar() {
            super(null);
        }

        @Override // org.thoughtcrime.securesms.mediasend.v2.MediaSelectionDestination
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putBoolean(MediaSelectionDestination.AVATAR, true);
            return bundle;
        }
    }

    /* compiled from: MediaSelectionDestination.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$ChooseAfterMediaSelection;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "()V", "toBundle", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ChooseAfterMediaSelection extends MediaSelectionDestination {
        public static final ChooseAfterMediaSelection INSTANCE = new ChooseAfterMediaSelection();

        private ChooseAfterMediaSelection() {
            super(null);
        }

        @Override // org.thoughtcrime.securesms.mediasend.v2.MediaSelectionDestination
        public Bundle toBundle() {
            Bundle bundle = Bundle.EMPTY;
            Intrinsics.checkNotNullExpressionValue(bundle, "EMPTY");
            return bundle;
        }
    }

    /* compiled from: MediaSelectionDestination.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$SingleRecipient;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getRecipientSearchKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "toBundle", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SingleRecipient extends MediaSelectionDestination {
        private final RecipientId id;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SingleRecipient(RecipientId recipientId) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
            this.id = recipientId;
        }

        @Override // org.thoughtcrime.securesms.mediasend.v2.MediaSelectionDestination
        public ContactSearchKey.RecipientSearchKey getRecipientSearchKey() {
            return new ContactSearchKey.RecipientSearchKey.KnownRecipient(this.id);
        }

        @Override // org.thoughtcrime.securesms.mediasend.v2.MediaSelectionDestination
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putParcelable("recipient", this.id);
            return bundle;
        }
    }

    /* compiled from: MediaSelectionDestination.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0016J\b\u0010\t\u001a\u00020\nH\u0016R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$MultipleRecipients;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "recipientSearchKeys", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "(Ljava/util/List;)V", "getRecipientSearchKeys", "()Ljava/util/List;", "getRecipientSearchKeyList", "toBundle", "Landroid/os/Bundle;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MultipleRecipients extends MediaSelectionDestination {
        public static final Companion Companion = new Companion(null);
        private final List<ContactSearchKey.RecipientSearchKey> recipientSearchKeys;

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchKey$RecipientSearchKey> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public MultipleRecipients(List<? extends ContactSearchKey.RecipientSearchKey> list) {
            super(null);
            Intrinsics.checkNotNullParameter(list, "recipientSearchKeys");
            this.recipientSearchKeys = list;
        }

        public final List<ContactSearchKey.RecipientSearchKey> getRecipientSearchKeys() {
            return this.recipientSearchKeys;
        }

        /* compiled from: MediaSelectionDestination.kt */
        @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$MultipleRecipients$Companion;", "", "()V", "fromParcel", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$MultipleRecipients;", "parcelables", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableRecipientSearchKey;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final MultipleRecipients fromParcel(List<ContactSearchKey.ParcelableRecipientSearchKey> list) {
                Intrinsics.checkNotNullParameter(list, "parcelables");
                ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
                for (ContactSearchKey.ParcelableRecipientSearchKey parcelableRecipientSearchKey : list) {
                    arrayList.add(parcelableRecipientSearchKey.asRecipientSearchKey());
                }
                return new MultipleRecipients(CollectionsKt___CollectionsJvmKt.filterIsInstance(arrayList, ContactSearchKey.RecipientSearchKey.class));
            }
        }

        @Override // org.thoughtcrime.securesms.mediasend.v2.MediaSelectionDestination
        public List<ContactSearchKey.RecipientSearchKey> getRecipientSearchKeyList() {
            return this.recipientSearchKeys;
        }

        @Override // org.thoughtcrime.securesms.mediasend.v2.MediaSelectionDestination
        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            List<ContactSearchKey.RecipientSearchKey> list = this.recipientSearchKeys;
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
            for (ContactSearchKey.RecipientSearchKey recipientSearchKey : list) {
                arrayList.add(recipientSearchKey.requireParcelable());
            }
            bundle.putParcelableArrayList(MediaSelectionDestination.RECIPIENT_LIST, new ArrayList<>(arrayList));
            return bundle;
        }
    }

    public List<ContactSearchKey.RecipientSearchKey> getRecipientSearchKeyList() {
        return CollectionsKt__CollectionsKt.emptyList();
    }

    /* compiled from: MediaSelectionDestination.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination$Companion;", "", "()V", "AVATAR", "", "RECIPIENT", "RECIPIENT_LIST", "WALLPAPER", "fromBundle", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "bundle", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final MediaSelectionDestination fromBundle(Bundle bundle) {
            Intrinsics.checkNotNullParameter(bundle, "bundle");
            if (bundle.containsKey(MediaSelectionDestination.WALLPAPER)) {
                return Wallpaper.INSTANCE;
            }
            if (bundle.containsKey(MediaSelectionDestination.AVATAR)) {
                return Avatar.INSTANCE;
            }
            if (bundle.containsKey("recipient")) {
                Parcelable parcelable = bundle.getParcelable("recipient");
                if (parcelable != null) {
                    return new SingleRecipient((RecipientId) parcelable);
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            } else if (!bundle.containsKey(MediaSelectionDestination.RECIPIENT_LIST)) {
                return ChooseAfterMediaSelection.INSTANCE;
            } else {
                MultipleRecipients.Companion companion = MultipleRecipients.Companion;
                ArrayList parcelableArrayList = bundle.getParcelableArrayList(MediaSelectionDestination.RECIPIENT_LIST);
                if (parcelableArrayList != null) {
                    Intrinsics.checkNotNullExpressionValue(parcelableArrayList, "requireNotNull(bundle.ge…rrayList(RECIPIENT_LIST))");
                    return companion.fromParcel(parcelableArrayList);
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            }
        }
    }
}
