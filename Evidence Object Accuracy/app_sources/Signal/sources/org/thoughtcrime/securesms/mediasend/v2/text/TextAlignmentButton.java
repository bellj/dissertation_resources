package org.thoughtcrime.securesms.mediasend.v2.text;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: TextAlignmentButton.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\tJ\u0012\u0010\u0012\u001a\u00020\n2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016R.\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\bj\u0004\u0018\u0001`\u000bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextAlignmentButton;", "Landroidx/appcompat/widget/AppCompatImageView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "onAlignmentChangedListener", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextAlignment;", "", "Lorg/thoughtcrime/securesms/mediasend/v2/text/OnTextAlignmentChanged;", "getOnAlignmentChangedListener", "()Lkotlin/jvm/functions/Function1;", "setOnAlignmentChangedListener", "(Lkotlin/jvm/functions/Function1;)V", "textAlignment", "setAlignment", "setOnClickListener", "l", "Landroid/view/View$OnClickListener;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextAlignmentButton extends AppCompatImageView {
    private Function1<? super TextAlignment, Unit> onAlignmentChangedListener;
    private TextAlignment textAlignment;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public TextAlignmentButton(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ TextAlignmentButton(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public TextAlignmentButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        TextAlignment textAlignment = TextAlignment.CENTER;
        this.textAlignment = textAlignment;
        setImageResource(textAlignment.getIcon());
        super.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextAlignmentButton$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                TextAlignmentButton.$r8$lambda$FKqkdMLLbg3c1nPXPYLdpOQbDew(TextAlignmentButton.this, view);
            }
        });
    }

    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.mediasend.v2.text.TextAlignment, kotlin.Unit>, kotlin.jvm.functions.Function1<org.thoughtcrime.securesms.mediasend.v2.text.TextAlignment, kotlin.Unit> */
    public final Function1<TextAlignment, Unit> getOnAlignmentChangedListener() {
        return this.onAlignmentChangedListener;
    }

    public final void setOnAlignmentChangedListener(Function1<? super TextAlignment, Unit> function1) {
        this.onAlignmentChangedListener = function1;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m2229_init_$lambda0(TextAlignmentButton textAlignmentButton, View view) {
        Intrinsics.checkNotNullParameter(textAlignmentButton, "this$0");
        TextAlignment textAlignment = textAlignmentButton.textAlignment;
        TextAlignment[] values = TextAlignment.values();
        textAlignmentButton.setAlignment(values[(textAlignment.ordinal() + 1) % values.length]);
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        throw new UnsupportedOperationException();
    }

    public final void setAlignment(TextAlignment textAlignment) {
        Intrinsics.checkNotNullParameter(textAlignment, "textAlignment");
        if (textAlignment != this.textAlignment) {
            this.textAlignment = textAlignment;
            setImageResource(textAlignment.getIcon());
            Function1<? super TextAlignment, Unit> function1 = this.onAlignmentChangedListener;
            if (function1 != null) {
                function1.invoke(textAlignment);
            }
        }
    }
}
