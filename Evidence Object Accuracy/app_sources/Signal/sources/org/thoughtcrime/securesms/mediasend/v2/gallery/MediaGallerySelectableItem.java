package org.thoughtcrime.securesms.mediasend.v2.gallery;

import android.animation.ValueAnimator;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.imageview.ShapeableImageView;
import j$.util.function.Function;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.MediaFolder;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: MediaGallerySelectableItem.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001:\u0006\u0011\u0012\u0013\u0014\u0015\u0016B\u0007\b\u0002¢\u0006\u0002\u0010\u0002JL\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00040\bj\u0002`\n2\u001c\u0010\u000b\u001a\u0018\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00040\fj\u0002`\u000f2\u0006\u0010\u0010\u001a\u00020\u000e¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem;", "", "()V", "registerAdapter", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "onMediaFolderClicked", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/mediasend/MediaFolder;", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/OnMediaFolderClicked;", "onMediaClicked", "Lkotlin/Function2;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/OnMediaClicked;", "isMultiselectEnabled", "BaseViewHolder", "ErrorLoggingRequestListener", "FileModel", "FileViewHolder", "FolderModel", "FolderViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGallerySelectableItem {
    public static final MediaGallerySelectableItem INSTANCE = new MediaGallerySelectableItem();

    private MediaGallerySelectableItem() {
    }

    /* renamed from: registerAdapter$lambda-0 */
    public static final MappingViewHolder m2170registerAdapter$lambda0(Function1 function1, View view) {
        Intrinsics.checkNotNullParameter(function1, "$onMediaFolderClicked");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new FolderViewHolder(view, function1);
    }

    public final void registerAdapter(MappingAdapter mappingAdapter, Function1<? super MediaFolder, Unit> function1, Function2<? super Media, ? super Boolean, Unit> function2, boolean z) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        Intrinsics.checkNotNullParameter(function1, "onMediaFolderClicked");
        Intrinsics.checkNotNullParameter(function2, "onMediaClicked");
        mappingAdapter.registerFactory(FolderModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectableItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MediaGallerySelectableItem.m2170registerAdapter$lambda0(Function1.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.v2_media_gallery_folder_item));
        mappingAdapter.registerFactory(FileModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectableItem$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MediaGallerySelectableItem.m2171registerAdapter$lambda1(Function2.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, z ? R.layout.v2_media_gallery_item : R.layout.v2_media_gallery_item_no_check));
    }

    /* renamed from: registerAdapter$lambda-1 */
    public static final MappingViewHolder m2171registerAdapter$lambda1(Function2 function2, View view) {
        Intrinsics.checkNotNullParameter(function2, "$onMediaClicked");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new FileViewHolder(view, function2);
    }

    /* compiled from: MediaGallerySelectableItem.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem$FolderModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "mediaFolder", "Lorg/thoughtcrime/securesms/mediasend/MediaFolder;", "(Lorg/thoughtcrime/securesms/mediasend/MediaFolder;)V", "getMediaFolder", "()Lorg/thoughtcrime/securesms/mediasend/MediaFolder;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FolderModel implements MappingModel<FolderModel> {
        private final MediaFolder mediaFolder;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(FolderModel folderModel) {
            return MappingModel.CC.$default$getChangePayload(this, folderModel);
        }

        public FolderModel(MediaFolder mediaFolder) {
            Intrinsics.checkNotNullParameter(mediaFolder, "mediaFolder");
            this.mediaFolder = mediaFolder;
        }

        public final MediaFolder getMediaFolder() {
            return this.mediaFolder;
        }

        public boolean areItemsTheSame(FolderModel folderModel) {
            Intrinsics.checkNotNullParameter(folderModel, "newItem");
            return Intrinsics.areEqual(this.mediaFolder.getBucketId(), folderModel.mediaFolder.getBucketId());
        }

        public boolean areContentsTheSame(FolderModel folderModel) {
            Intrinsics.checkNotNullParameter(folderModel, "newItem");
            return Intrinsics.areEqual(this.mediaFolder.getBucketId(), folderModel.mediaFolder.getBucketId()) && Intrinsics.areEqual(this.mediaFolder.getThumbnailUri(), folderModel.mediaFolder.getThumbnailUri());
        }
    }

    /* compiled from: MediaGallerySelectableItem.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b&\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem$BaseViewHolder;", "T", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "checkView", "Landroid/widget/TextView;", "getCheckView", "()Landroid/widget/TextView;", "imageView", "Lcom/google/android/material/imageview/ShapeableImageView;", "getImageView", "()Lcom/google/android/material/imageview/ShapeableImageView;", "playOverlay", "Landroid/widget/ImageView;", "getPlayOverlay", "()Landroid/widget/ImageView;", MultiselectForwardFragment.DIALOG_TITLE, "getTitle", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class BaseViewHolder<T extends MappingModel<T>> extends MappingViewHolder<T> {
        private final TextView checkView;
        private final ShapeableImageView imageView;
        private final ImageView playOverlay;
        private final TextView title;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public BaseViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.media_gallery_image);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.media_gallery_image)");
            this.imageView = (ShapeableImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.media_gallery_play_overlay);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.…dia_gallery_play_overlay)");
            this.playOverlay = (ImageView) findViewById2;
            this.checkView = (TextView) view.findViewById(R.id.media_gallery_check);
            this.title = (TextView) view.findViewById(R.id.media_gallery_title);
        }

        protected final ShapeableImageView getImageView() {
            return this.imageView;
        }

        protected final ImageView getPlayOverlay() {
            return this.playOverlay;
        }

        protected final TextView getCheckView() {
            return this.checkView;
        }

        protected final TextView getTitle() {
            return this.title;
        }
    }

    /* compiled from: MediaGallerySelectableItem.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B%\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006j\u0002`\t¢\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u0002H\u0016R\u001e\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006j\u0002`\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem$FolderViewHolder;", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem$BaseViewHolder;", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem$FolderModel;", "itemView", "Landroid/view/View;", "onMediaFolderClicked", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/mediasend/MediaFolder;", "", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/OnMediaFolderClicked;", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FolderViewHolder extends BaseViewHolder<FolderModel> {
        private final Function1<MediaFolder, Unit> onMediaFolderClicked;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.mediasend.MediaFolder, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public FolderViewHolder(View view, Function1<? super MediaFolder, Unit> function1) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function1, "onMediaFolderClicked");
            this.onMediaFolderClicked = function1;
        }

        public void bind(FolderModel folderModel) {
            Intrinsics.checkNotNullParameter(folderModel, "model");
            GlideApp.with(getImageView()).load((Object) new DecryptableStreamUriLoader.DecryptableUri(folderModel.getMediaFolder().getThumbnailUri())).into(getImageView());
            ViewExtensionsKt.setVisible(getPlayOverlay(), false);
            this.itemView.setOnClickListener(new MediaGallerySelectableItem$FolderViewHolder$$ExternalSyntheticLambda0(this, folderModel));
            TextView title = getTitle();
            if (title != null) {
                title.setText(folderModel.getMediaFolder().getTitle());
            }
            TextView title2 = getTitle();
            if (title2 != null) {
                ViewExtensionsKt.setVisible(title2, true);
            }
        }

        /* renamed from: bind$lambda-0 */
        public static final void m2174bind$lambda0(FolderViewHolder folderViewHolder, FolderModel folderModel, View view) {
            Intrinsics.checkNotNullParameter(folderViewHolder, "this$0");
            Intrinsics.checkNotNullParameter(folderModel, "$model");
            folderViewHolder.onMediaFolderClicked.invoke(folderModel.getMediaFolder());
        }
    }

    /* compiled from: MediaGallerySelectableItem.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u000f\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0000H\u0016J\u0010\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0000H\u0016J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0007HÆ\u0003J'\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0015\u001a\u00020\u00052\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u000f\u001a\u00020\u0000H\u0016J\t\u0010\u0019\u001a\u00020\u0007HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem$FileModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "media", "Lorg/thoughtcrime/securesms/mediasend/Media;", "isSelected", "", "selectionOneBasedIndex", "", "(Lorg/thoughtcrime/securesms/mediasend/Media;ZI)V", "()Z", "getMedia", "()Lorg/thoughtcrime/securesms/mediasend/Media;", "getSelectionOneBasedIndex", "()I", "areContentsTheSame", "newItem", "areItemsTheSame", "component1", "component2", "component3", "copy", "equals", "other", "", "getChangePayload", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FileModel implements MappingModel<FileModel> {
        private final boolean isSelected;
        private final Media media;
        private final int selectionOneBasedIndex;

        public static /* synthetic */ FileModel copy$default(FileModel fileModel, Media media, boolean z, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                media = fileModel.media;
            }
            if ((i2 & 2) != 0) {
                z = fileModel.isSelected;
            }
            if ((i2 & 4) != 0) {
                i = fileModel.selectionOneBasedIndex;
            }
            return fileModel.copy(media, z, i);
        }

        public final Media component1() {
            return this.media;
        }

        public final boolean component2() {
            return this.isSelected;
        }

        public final int component3() {
            return this.selectionOneBasedIndex;
        }

        public final FileModel copy(Media media, boolean z, int i) {
            Intrinsics.checkNotNullParameter(media, "media");
            return new FileModel(media, z, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FileModel)) {
                return false;
            }
            FileModel fileModel = (FileModel) obj;
            return Intrinsics.areEqual(this.media, fileModel.media) && this.isSelected == fileModel.isSelected && this.selectionOneBasedIndex == fileModel.selectionOneBasedIndex;
        }

        public int hashCode() {
            int hashCode = this.media.hashCode() * 31;
            boolean z = this.isSelected;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return ((hashCode + i) * 31) + this.selectionOneBasedIndex;
        }

        public String toString() {
            return "FileModel(media=" + this.media + ", isSelected=" + this.isSelected + ", selectionOneBasedIndex=" + this.selectionOneBasedIndex + ')';
        }

        public FileModel(Media media, boolean z, int i) {
            Intrinsics.checkNotNullParameter(media, "media");
            this.media = media;
            this.isSelected = z;
            this.selectionOneBasedIndex = i;
        }

        public final Media getMedia() {
            return this.media;
        }

        public final int getSelectionOneBasedIndex() {
            return this.selectionOneBasedIndex;
        }

        public final boolean isSelected() {
            return this.isSelected;
        }

        public boolean areItemsTheSame(FileModel fileModel) {
            Intrinsics.checkNotNullParameter(fileModel, "newItem");
            return Intrinsics.areEqual(fileModel.media, this.media);
        }

        public boolean areContentsTheSame(FileModel fileModel) {
            Intrinsics.checkNotNullParameter(fileModel, "newItem");
            return Intrinsics.areEqual(fileModel.media, this.media) && this.isSelected == fileModel.isSelected && this.selectionOneBasedIndex == fileModel.selectionOneBasedIndex;
        }

        public Object getChangePayload(FileModel fileModel) {
            Intrinsics.checkNotNullParameter(fileModel, "newItem");
            if (!Intrinsics.areEqual(fileModel.media, this.media)) {
                return null;
            }
            if (fileModel.isSelected != this.isSelected) {
                return 0;
            }
            if (fileModel.selectionOneBasedIndex != this.selectionOneBasedIndex) {
                return 1;
            }
            return null;
        }
    }

    /* compiled from: MediaGallerySelectableItem.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\t\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B+\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u001c\u0010\u0005\u001a\u0018\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0006j\u0002`\n¢\u0006\u0002\u0010\u000bJ\u0010\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\bH\u0002J\u0010\u0010\u0013\u001a\u00020\t2\u0006\u0010\u0014\u001a\u00020\u0002H\u0016J\b\u0010\u0015\u001a\u00020\tH\u0016J\u0010\u0010\u0016\u001a\u00020\t2\u0006\u0010\u0017\u001a\u00020\u000fH\u0002R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u000e¢\u0006\u0002\n\u0000R$\u0010\u0005\u001a\u0018\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0006j\u0002`\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem$FileViewHolder;", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem$BaseViewHolder;", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem$FileModel;", "itemView", "Landroid/view/View;", "onMediaClicked", "Lkotlin/Function2;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "", "", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/OnMediaClicked;", "(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V", "animator", "Landroid/animation/ValueAnimator;", "selectedPadding", "", "selectedRadius", "animateCheckState", "isSelected", "bind", "model", "onDetachedFromWindow", "updateImageView", "fraction", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FileViewHolder extends BaseViewHolder<FileModel> {
        private ValueAnimator animator;
        private final Function2<Media, Boolean, Unit> onMediaClicked;
        private final float selectedPadding;
        private final float selectedRadius;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function2<? super org.thoughtcrime.securesms.mediasend.Media, ? super java.lang.Boolean, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public FileViewHolder(View view, Function2<? super Media, ? super Boolean, Unit> function2) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function2, "onMediaClicked");
            this.onMediaClicked = function2;
            DimensionUnit dimensionUnit = DimensionUnit.DP;
            this.selectedPadding = dimensionUnit.toPixels(12.0f);
            this.selectedRadius = dimensionUnit.toPixels(12.0f);
        }

        public void bind(FileModel fileModel) {
            Intrinsics.checkNotNullParameter(fileModel, "model");
            TextView checkView = getCheckView();
            if (checkView != null) {
                ViewExtensionsKt.setVisible(checkView, fileModel.isSelected());
            }
            TextView checkView2 = getCheckView();
            if (checkView2 != null) {
                checkView2.setText(String.valueOf(fileModel.getSelectionOneBasedIndex()));
            }
            this.itemView.setOnClickListener(new MediaGallerySelectableItem$FileViewHolder$$ExternalSyntheticLambda1(this, fileModel));
            ViewExtensionsKt.setVisible(getPlayOverlay(), MediaUtil.isVideo(fileModel.getMedia().getMimeType()) && !fileModel.getMedia().isVideoGif());
            TextView title = getTitle();
            if (title != null) {
                ViewExtensionsKt.setVisible(title, false);
            }
            if (!this.payload.contains(1)) {
                if (this.payload.contains(0)) {
                    animateCheckState(fileModel.isSelected());
                    return;
                }
                ValueAnimator valueAnimator = this.animator;
                if (valueAnimator != null) {
                    valueAnimator.cancel();
                }
                updateImageView(fileModel.isSelected() ? 1.0f : 0.0f);
                GlideRequest<Drawable> load = GlideApp.with(getImageView()).load((Object) new DecryptableStreamUriLoader.DecryptableUri(fileModel.getMedia().getUri()));
                String str = MediaGallerySelectableItemKt.FILE_VIEW_HOLDER_TAG;
                Intrinsics.checkNotNullExpressionValue(str, "FILE_VIEW_HOLDER_TAG");
                load.addListener((RequestListener<Drawable>) new ErrorLoggingRequestListener(str)).into(getImageView());
            }
        }

        /* renamed from: bind$lambda-0 */
        public static final void m2173bind$lambda0(FileViewHolder fileViewHolder, FileModel fileModel, View view) {
            Intrinsics.checkNotNullParameter(fileViewHolder, "this$0");
            Intrinsics.checkNotNullParameter(fileModel, "$model");
            fileViewHolder.onMediaClicked.invoke(fileModel.getMedia(), Boolean.valueOf(fileModel.isSelected()));
        }

        private final void animateCheckState(boolean z) {
            ValueAnimator valueAnimator = this.animator;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            float f = 0.0f;
            float f2 = z ? 0.0f : 1.0f;
            if (z) {
                f = 1.0f;
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat(f2, f);
            ofFloat.setDuration(TimeUnit.MILLISECONDS.toMillis(100));
            ofFloat.addUpdateListener(new MediaGallerySelectableItem$FileViewHolder$$ExternalSyntheticLambda0(this));
            ofFloat.start();
            this.animator = ofFloat;
        }

        /* renamed from: animateCheckState$lambda-2$lambda-1 */
        public static final void m2172animateCheckState$lambda2$lambda1(FileViewHolder fileViewHolder, ValueAnimator valueAnimator) {
            Intrinsics.checkNotNullParameter(fileViewHolder, "this$0");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                fileViewHolder.updateImageView(((Float) animatedValue).floatValue());
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public void onDetachedFromWindow() {
            ValueAnimator valueAnimator = this.animator;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
        }

        private final void updateImageView(float f) {
            int i = (int) (this.selectedPadding * f);
            getImageView().setPadding(i, i, i, i);
            getImageView().setShapeAppearanceModel(getImageView().getShapeAppearanceModel().withCornerSize(this.selectedRadius * f));
        }
    }

    /* compiled from: MediaGallerySelectableItem.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J4\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\u000e\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u0007H\u0016J>\u0010\u000f\u001a\u00020\u00072\b\u0010\u0010\u001a\u0004\u0018\u00010\u00022\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\u000e\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\r2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u000e\u001a\u00020\u0007H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGallerySelectableItem$ErrorLoggingRequestListener;", "Lcom/bumptech/glide/request/RequestListener;", "Landroid/graphics/drawable/Drawable;", "tag", "", "(Ljava/lang/String;)V", "onLoadFailed", "", "e", "Lcom/bumptech/glide/load/engine/GlideException;", "model", "", "target", "Lcom/bumptech/glide/request/target/Target;", "isFirstResource", "onResourceReady", "resource", "dataSource", "Lcom/bumptech/glide/load/DataSource;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ErrorLoggingRequestListener implements RequestListener<Drawable> {
        private final String tag;

        public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
            return false;
        }

        public ErrorLoggingRequestListener(String str) {
            Intrinsics.checkNotNullParameter(str, "tag");
            this.tag = str;
        }

        @Override // com.bumptech.glide.request.RequestListener
        public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, DataSource dataSource, boolean z) {
            return onResourceReady((Drawable) obj, obj2, (Target<Drawable>) target, dataSource, z);
        }

        @Override // com.bumptech.glide.request.RequestListener
        public boolean onLoadFailed(GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
            Log.w(this.tag, "Failed to load media.", glideException);
            return false;
        }
    }
}
