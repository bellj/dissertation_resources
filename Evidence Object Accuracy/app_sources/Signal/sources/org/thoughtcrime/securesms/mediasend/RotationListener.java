package org.thoughtcrime.securesms.mediasend;

import android.content.Context;
import android.view.OrientationEventListener;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.Subject;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.SmsDatabase;

/* compiled from: RotationListener.kt */
@Metadata(bv = {}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0014B\u000f\u0012\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016R\u001a\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0002X\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR;\u0010\f\u001a&\u0012\f\u0012\n \u000b*\u0004\u0018\u00010\u00070\u0007 \u000b*\u0012\u0012\f\u0012\n \u000b*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\n0\n8\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/RotationListener;", "Landroid/view/OrientationEventListener;", "", "orientation", "", "onOrientationChanged", "Lio/reactivex/rxjava3/subjects/Subject;", "Lorg/thoughtcrime/securesms/mediasend/RotationListener$Rotation;", SmsDatabase.SUBJECT, "Lio/reactivex/rxjava3/subjects/Subject;", "Lio/reactivex/rxjava3/core/Observable;", "kotlin.jvm.PlatformType", "observable", "Lio/reactivex/rxjava3/core/Observable;", "getObservable", "()Lio/reactivex/rxjava3/core/Observable;", "Landroid/content/Context;", "context", "<init>", "(Landroid/content/Context;)V", "Rotation", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class RotationListener extends OrientationEventListener {
    private final Observable<Rotation> observable;
    private final Subject<Rotation> subject;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RotationListener(Context context) {
        super(context);
        Intrinsics.checkNotNullParameter(context, "context");
        BehaviorSubject create = BehaviorSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.subject = create;
        this.observable = create.doOnSubscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.RotationListener$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                RotationListener.m2082observable$lambda0(RotationListener.this, (Disposable) obj);
            }
        }).doOnTerminate(new Action() { // from class: org.thoughtcrime.securesms.mediasend.RotationListener$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                RotationListener.m2083observable$lambda1(RotationListener.this);
            }
        });
    }

    public final Observable<Rotation> getObservable() {
        return this.observable;
    }

    /* renamed from: observable$lambda-0 */
    public static final void m2082observable$lambda0(RotationListener rotationListener, Disposable disposable) {
        Intrinsics.checkNotNullParameter(rotationListener, "this$0");
        rotationListener.enable();
    }

    /* renamed from: observable$lambda-1 */
    public static final void m2083observable$lambda1(RotationListener rotationListener) {
        Intrinsics.checkNotNullParameter(rotationListener, "this$0");
        rotationListener.disable();
    }

    @Override // android.view.OrientationEventListener
    public void onOrientationChanged(int i) {
        Rotation rotation;
        Subject<Rotation> subject = this.subject;
        if (i == -1) {
            rotation = Rotation.ROTATION_0;
        } else if (i > 315 || i < 45) {
            rotation = Rotation.ROTATION_0;
        } else if (i < 135) {
            rotation = Rotation.ROTATION_270;
        } else if (i < 225) {
            rotation = Rotation.ROTATION_180;
        } else {
            rotation = Rotation.ROTATION_90;
        }
        subject.onNext(rotation);
    }

    /* compiled from: RotationListener.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\b\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/RotationListener$Rotation;", "", "surfaceRotation", "", "(Ljava/lang/String;II)V", "getSurfaceRotation", "()I", "ROTATION_0", "ROTATION_90", "ROTATION_180", "ROTATION_270", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Rotation {
        ROTATION_0(0),
        ROTATION_90(1),
        ROTATION_180(2),
        ROTATION_270(3);
        
        private final int surfaceRotation;

        Rotation(int i) {
            this.surfaceRotation = i;
        }

        public final int getSurfaceRotation() {
            return this.surfaceRotation;
        }
    }
}
