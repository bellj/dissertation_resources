package org.thoughtcrime.securesms.mediasend.v2;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.mediasend.VideoEditorFragment;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionDestination;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionState;
import org.thoughtcrime.securesms.mediasend.v2.MediaValidator;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.scribbles.ImageEditorFragment;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: MediaSelectionViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000à\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010&\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 y2\u00020\u0001:\u0002yzBG\u0012\u0006\u0010F\u001a\u00020E\u0012\u0006\u0010t\u001a\u00020s\u0012\f\u0010u\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\b\u0010v\u001a\u0004\u0018\u000102\u0012\u0006\u0010J\u001a\u00020\u0007\u0012\u0006\u0010 \u001a\u00020\u0007\u0012\u0006\u0010N\u001a\u00020M¢\u0006\u0004\bw\u0010xJ\u0016\u0010\u0006\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002J\u0018\u0010\t\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0007H\u0002J\b\u0010\n\u001a\u00020\u0007H\u0002J\u0016\u0010\u000b\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002J\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002J\"\u0010\u0011\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u00072\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0002J\u0018\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013*\u00020\u0012H\u0002J\u0018\u0010\u0018\u001a\u00020\u0012*\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0017H\u0002J\b\u0010\u0019\u001a\u00020\u0005H\u0014J\u0006\u0010\u001a\u001a\u00020\u0005J\u000e\u0010\u001d\u001a\u00020\u00052\u0006\u0010\u001c\u001a\u00020\u001bJ\u000e\u0010\u001f\u001a\u00020\u00052\u0006\u0010\u001e\u001a\u00020\u0007J\u000e\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003J\u0006\u0010 \u001a\u00020\u0007J\u0006\u0010\"\u001a\u00020!J\u0016\u0010&\u001a\u00020\u00072\u0006\u0010$\u001a\u00020#2\u0006\u0010%\u001a\u00020#J\u000e\u0010(\u001a\u00020\u00072\u0006\u0010'\u001a\u00020#J\u0006\u0010)\u001a\u00020\u0005J\u000e\u0010\t\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003J\u000e\u0010*\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003J\u0006\u0010+\u001a\u00020\u0005J\u000e\u0010,\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003J\u000e\u0010,\u001a\u00020\u00052\u0006\u0010'\u001a\u00020#J\u0006\u0010.\u001a\u00020-J\u000e\u00101\u001a\u00020\u00052\u0006\u00100\u001a\u00020/J\u0010\u00104\u001a\u00020\u00052\b\u00103\u001a\u0004\u0018\u000102J\u0006\u00105\u001a\u00020\u0005J\u0010\u00107\u001a\u0004\u0018\u00010\u00152\u0006\u00106\u001a\u00020\u0014J\u0016\u00109\u001a\u00020\u00052\u0006\u00106\u001a\u00020\u00142\u0006\u00108\u001a\u00020\u0015J\u000e\u0010:\u001a\u00020\u00052\u0006\u00106\u001a\u00020\u0014J\u001c\u0010?\u001a\b\u0012\u0004\u0012\u00020>0=2\u000e\b\u0002\u0010<\u001a\b\u0012\u0004\u0012\u00020;0\u0002J\u000e\u0010A\u001a\u00020\u00052\u0006\u0010@\u001a\u00020\u0012J\u0006\u0010B\u001a\u00020\u0007J\u000e\u0010D\u001a\u00020\u00052\u0006\u0010C\u001a\u00020\u0012R\u0017\u0010F\u001a\u00020E8\u0006¢\u0006\f\n\u0004\bF\u0010G\u001a\u0004\bH\u0010IR\u0017\u0010J\u001a\u00020\u00078\u0006¢\u0006\f\n\u0004\bJ\u0010K\u001a\u0004\bJ\u0010LR\u0014\u0010N\u001a\u00020M8\u0002X\u0004¢\u0006\u0006\n\u0004\bN\u0010OR \u0010Q\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020P8\u0002X\u0004¢\u0006\u0006\n\u0004\bQ\u0010RR\u001a\u0010U\u001a\b\u0012\u0004\u0012\u00020T0S8\u0002X\u0004¢\u0006\u0006\n\u0004\bU\u0010VR\u0017\u0010W\u001a\u00020\u00078\u0006¢\u0006\f\n\u0004\bW\u0010K\u001a\u0004\bW\u0010LR\u001d\u00108\u001a\b\u0012\u0004\u0012\u00020T0X8\u0006¢\u0006\f\n\u0004\b8\u0010Y\u001a\u0004\bZ\u0010[R8\u0010^\u001a&\u0012\f\u0012\n ]*\u0004\u0018\u00010\u001b0\u001b ]*\u0012\u0012\f\u0012\n ]*\u0004\u0018\u00010\u001b0\u001b\u0018\u00010\\0\\8\u0002X\u0004¢\u0006\u0006\n\u0004\b^\u0010_R\u001d\u0010b\u001a\b\u0012\u0004\u0012\u00020a0`8\u0006¢\u0006\f\n\u0004\bb\u0010c\u001a\u0004\bd\u0010eR\u001d\u0010g\u001a\b\u0012\u0004\u0012\u00020\u001b0f8\u0006¢\u0006\f\n\u0004\bg\u0010h\u001a\u0004\bi\u0010jR\u0014\u0010l\u001a\u00020k8\u0002X\u0004¢\u0006\u0006\n\u0004\bl\u0010mR\u0014\u0010o\u001a\u00020n8\u0002X\u0004¢\u0006\u0006\n\u0004\bo\u0010pR\"\u0010q\u001a\u000e\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020#0\u00138\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\bq\u0010r¨\u0006{"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "Landroidx/lifecycle/ViewModel;", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "media", "", "addMedia", "", "suppressEmptyError", "removeMedia", "isViewOnceEnabled", "startUpload", "cancelUpload", "metered", "isSms", "Lorg/thoughtcrime/securesms/recipients/Recipient;", RecipientDatabase.TABLE_NAME, "shouldPreUpload", "Landroid/os/Bundle;", "Lkotlin/Pair;", "Landroid/net/Uri;", "", "toAssociation", "", "toBundleStateEntry", "onCleared", "kick", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "hudCommand", "sendCommand", "isEnabled", "setTouchEnabled", "isStory", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "getStorySendRequirements", "", "originalStart", NotificationProfileDatabase.NotificationProfileScheduleTable.END, "swapMedia", "position", "isValidMediaDragPosition", "onMediaDragFinished", "addCameraFirstCapture", "removeCameraFirstCapture", "setFocusedMedia", "Lorg/thoughtcrime/securesms/mms/MediaConstraints;", "getMediaConstraints", "Lorg/thoughtcrime/securesms/mms/SentMediaQuality;", "sentMediaQuality", "setSentMediaQuality", "", DraftDatabase.Draft.TEXT, "setMessage", "incrementViewOnceState", "uri", "getEditorState", "state", "setEditorState", "onVideoBeginEdit", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "selectedContacts", "Lio/reactivex/rxjava3/core/Maybe;", "Lorg/thoughtcrime/securesms/mediasend/MediaSendActivityResult;", "send", "outState", "onSaveState", "hasSelectedMedia", "savedInstanceState", "onRestoreState", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "destination", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "getDestination", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "isReply", "Z", "()Z", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionRepository;", "repository", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionRepository;", "Lio/reactivex/rxjava3/subjects/Subject;", "selectedMediaSubject", "Lio/reactivex/rxjava3/subjects/Subject;", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionState;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "isContactSelectionRequired", "Landroidx/lifecycle/LiveData;", "Landroidx/lifecycle/LiveData;", "getState", "()Landroidx/lifecycle/LiveData;", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "internalHudCommands", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "mediaErrors", "Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "getMediaErrors", "()Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "Lio/reactivex/rxjava3/core/Observable;", "hudCommands", "Lio/reactivex/rxjava3/core/Observable;", "getHudCommands", "()Lio/reactivex/rxjava3/core/Observable;", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "Lio/reactivex/rxjava3/disposables/Disposable;", "isMeteredDisposable", "Lio/reactivex/rxjava3/disposables/Disposable;", "lastMediaDrag", "Lkotlin/Pair;", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "sendType", "initialMedia", "initialMessage", "<init>", "(Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;Lorg/thoughtcrime/securesms/conversation/MessageSendType;Ljava/util/List;Ljava/lang/CharSequence;ZZLorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionRepository;)V", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class MediaSelectionViewModel extends ViewModel {
    private static final String BUNDLE_IS_IMAGE;
    private static final String BUNDLE_URI;
    public static final Companion Companion = new Companion(null);
    private static final String STATE_CAMERA_FIRST_CAPTURE;
    private static final String STATE_EDITORS;
    private static final String STATE_FOCUSED;
    private static final String STATE_MESSAGE;
    private static final String STATE_PREFIX;
    private static final String STATE_QUALITY;
    private static final String STATE_SELECTION;
    private static final String STATE_SENT;
    private static final String STATE_TOUCH_ENABLED;
    private static final String STATE_VIEW_ONCE;
    private final MediaSelectionDestination destination;
    private final CompositeDisposable disposables;
    private final Observable<HudCommand> hudCommands;
    private final PublishSubject<HudCommand> internalHudCommands;
    private final boolean isContactSelectionRequired;
    private final Disposable isMeteredDisposable;
    private final boolean isReply;
    private Pair<Integer, Integer> lastMediaDrag;
    private final SingleLiveEvent<MediaValidator.FilterError> mediaErrors = new SingleLiveEvent<>();
    private final MediaSelectionRepository repository;
    private final Subject<List<Media>> selectedMediaSubject;
    private final LiveData<MediaSelectionState> state;
    private final Store<MediaSelectionState> store;

    /* renamed from: kick$lambda-4 */
    public static final MediaSelectionState m2122kick$lambda4(MediaSelectionState mediaSelectionState) {
        return mediaSelectionState;
    }

    public final MediaSelectionDestination getDestination() {
        return this.destination;
    }

    public final boolean isReply() {
        return this.isReply;
    }

    /* JADX DEBUG: Type inference failed for r1v9. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, androidx.lifecycle.LiveData<Input> */
    public MediaSelectionViewModel(MediaSelectionDestination mediaSelectionDestination, MessageSendType messageSendType, List<? extends Media> list, CharSequence charSequence, boolean z, boolean z2, MediaSelectionRepository mediaSelectionRepository) {
        Intrinsics.checkNotNullParameter(mediaSelectionDestination, "destination");
        Intrinsics.checkNotNullParameter(messageSendType, "sendType");
        Intrinsics.checkNotNullParameter(list, "initialMedia");
        Intrinsics.checkNotNullParameter(mediaSelectionRepository, "repository");
        this.destination = mediaSelectionDestination;
        this.isReply = z;
        this.repository = mediaSelectionRepository;
        BehaviorSubject create = BehaviorSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.selectedMediaSubject = create;
        Store<MediaSelectionState> store = new Store<>(new MediaSelectionState(messageSendType, null, null, null, null, charSequence, null, false, false, false, false, null, null, z2, null, 24542, null));
        this.store = store;
        this.isContactSelectionRequired = Intrinsics.areEqual(mediaSelectionDestination, MediaSelectionDestination.ChooseAfterMediaSelection.INSTANCE);
        LiveData<MediaSelectionState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        PublishSubject<HudCommand> create2 = PublishSubject.create();
        this.internalHudCommands = create2;
        Intrinsics.checkNotNullExpressionValue(create2, "internalHudCommands");
        this.hudCommands = create2;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        Disposable subscribe = mediaSelectionRepository.isMetered().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda11
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                MediaSelectionViewModel.m2120isMeteredDisposable$lambda1(MediaSelectionViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.isMetered.sub…ipient)\n      )\n    }\n  }");
        this.isMeteredDisposable = subscribe;
        this.lastMediaDrag = new Pair<>(0, 0);
        ContactSearchKey.RecipientSearchKey recipientSearchKey = mediaSelectionDestination.getRecipientSearchKey();
        if (recipientSearchKey != null) {
            store.update(Recipient.live(recipientSearchKey.getRecipientId()).getLiveData(), new Store.Action() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda12
                @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
                public final Object apply(Object obj, Object obj2) {
                    return MediaSelectionViewModel.m2114_init_$lambda2(MediaSelectionViewModel.this, (Recipient) obj, (MediaSelectionState) obj2);
                }
            });
        }
        if (!list.isEmpty()) {
            addMedia(list);
        }
        Observable<R> map = create.map(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda13
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2115_init_$lambda3((List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "selectedMediaSubject.map…Requirements(media)\n    }");
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy$default(map, (Function1) null, (Function0) null, new Function1<Stories.MediaTransform.SendRequirements, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel.3
            final /* synthetic */ MediaSelectionViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Stories.MediaTransform.SendRequirements sendRequirements) {
                invoke(sendRequirements);
                return Unit.INSTANCE;
            }

            public final void invoke(Stories.MediaTransform.SendRequirements sendRequirements) {
                this.this$0.store.update(new MediaSelectionViewModel$3$$ExternalSyntheticLambda0(sendRequirements));
            }

            /* renamed from: invoke$lambda-0 */
            public static final MediaSelectionState m2132invoke$lambda0(Stories.MediaTransform.SendRequirements sendRequirements, MediaSelectionState mediaSelectionState) {
                Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
                Intrinsics.checkNotNullExpressionValue(sendRequirements, "requirements");
                return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : sendRequirements);
            }
        }, 3, (Object) null));
    }

    public final boolean isContactSelectionRequired() {
        return this.isContactSelectionRequired;
    }

    public final LiveData<MediaSelectionState> getState() {
        return this.state;
    }

    public final SingleLiveEvent<MediaValidator.FilterError> getMediaErrors() {
        return this.mediaErrors;
    }

    public final Observable<HudCommand> getHudCommands() {
        return this.hudCommands;
    }

    /* renamed from: isMeteredDisposable$lambda-1 */
    public static final void m2120isMeteredDisposable$lambda1(MediaSelectionViewModel mediaSelectionViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(mediaSelectionViewModel, "this$0");
        mediaSelectionViewModel.store.update(new com.annimon.stream.function.Function(bool) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda14
            public final /* synthetic */ Boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2121isMeteredDisposable$lambda1$lambda0(MediaSelectionViewModel.this, this.f$1, (MediaSelectionState) obj);
            }
        });
    }

    /* renamed from: isMeteredDisposable$lambda-1$lambda-0 */
    public static final MediaSelectionState m2121isMeteredDisposable$lambda1$lambda0(MediaSelectionViewModel mediaSelectionViewModel, Boolean bool, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(mediaSelectionViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(bool, "metered");
        boolean shouldPreUpload = mediaSelectionViewModel.shouldPreUpload(bool.booleanValue(), mediaSelectionState.getSendType().usesSmsTransport(), mediaSelectionState.getRecipient());
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : shouldPreUpload, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : bool.booleanValue(), (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    /* renamed from: _init_$lambda-2 */
    public static final MediaSelectionState m2114_init_$lambda2(MediaSelectionViewModel mediaSelectionViewModel, Recipient recipient, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(mediaSelectionViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "s");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : recipient, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : mediaSelectionViewModel.shouldPreUpload(mediaSelectionState.isMeteredConnection(), mediaSelectionState.getSendType().usesSmsTransport(), recipient), (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    /* renamed from: _init_$lambda-3 */
    public static final Stories.MediaTransform.SendRequirements m2115_init_$lambda3(List list) {
        Intrinsics.checkNotNullExpressionValue(list, "media");
        return Stories.MediaTransform.getSendRequirements(list);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.isMeteredDisposable.dispose();
        this.disposables.clear();
    }

    public final void kick() {
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda16
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2122kick$lambda4((MediaSelectionState) obj);
            }
        });
    }

    public final void sendCommand(HudCommand hudCommand) {
        Intrinsics.checkNotNullParameter(hudCommand, "hudCommand");
        this.internalHudCommands.onNext(hudCommand);
    }

    /* renamed from: setTouchEnabled$lambda-5 */
    public static final MediaSelectionState m2130setTouchEnabled$lambda5(boolean z, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : z, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    public final void setTouchEnabled(boolean z) {
        this.store.update(new com.annimon.stream.function.Function(z) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2130setTouchEnabled$lambda5(this.f$0, (MediaSelectionState) obj);
            }
        });
    }

    public final void addMedia(Media media) {
        Intrinsics.checkNotNullParameter(media, "media");
        addMedia(CollectionsKt__CollectionsJVMKt.listOf(media));
    }

    public final boolean isStory() {
        return this.store.getState().isStory();
    }

    public final Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        return this.store.getState().getStorySendRequirements();
    }

    private final void addMedia(List<? extends Media> list) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.addAll(this.store.getState().getSelectedMedia());
        linkedHashSet.addAll(list);
        this.disposables.add(this.repository.populateAndFilterMedia(CollectionsKt___CollectionsKt.toList(linkedHashSet), getMediaConstraints(), this.store.getState().getMaxSelection(), this.store.getState().isStory()).subscribe(new Consumer(list) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                MediaSelectionViewModel.m2117addMedia$lambda8(MediaSelectionViewModel.this, this.f$1, (MediaValidator.FilterResult) obj);
            }
        }));
    }

    /* renamed from: addMedia$lambda-8 */
    public static final void m2117addMedia$lambda8(MediaSelectionViewModel mediaSelectionViewModel, List list, MediaValidator.FilterResult filterResult) {
        Intrinsics.checkNotNullParameter(mediaSelectionViewModel, "this$0");
        Intrinsics.checkNotNullParameter(list, "$media");
        if (!filterResult.getFilteredMedia().isEmpty()) {
            mediaSelectionViewModel.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda2
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return MediaSelectionViewModel.m2118addMedia$lambda8$lambda7(MediaValidator.FilterResult.this, (MediaSelectionState) obj);
                }
            });
            mediaSelectionViewModel.selectedMediaSubject.onNext(filterResult.getFilteredMedia());
            mediaSelectionViewModel.startUpload(CollectionsKt___CollectionsKt.toList(CollectionsKt___CollectionsKt.intersect(CollectionsKt___CollectionsKt.toSet(filterResult.getFilteredMedia()), list)));
        }
        if (filterResult.getFilterError() != null) {
            mediaSelectionViewModel.mediaErrors.postValue(filterResult.getFilterError());
        }
    }

    /* renamed from: addMedia$lambda-8$lambda-7 */
    public static final MediaSelectionState m2118addMedia$lambda8$lambda7(MediaValidator.FilterResult filterResult, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        List<Media> filteredMedia = filterResult.getFilteredMedia();
        Media focusedMedia = mediaSelectionState.getFocusedMedia();
        if (focusedMedia == null) {
            focusedMedia = (Media) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) filterResult.getFilteredMedia()));
        }
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : filteredMedia, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : focusedMedia, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    public final boolean swapMedia(int i, int i2) {
        if (this.lastMediaDrag.getFirst().intValue() == i && this.lastMediaDrag.getSecond().intValue() == i2) {
            return true;
        }
        int intValue = this.lastMediaDrag.getFirst().intValue() == i ? this.lastMediaDrag.getSecond().intValue() : i;
        MediaSelectionState state = this.store.getState();
        Intrinsics.checkNotNullExpressionValue(state, "store.state");
        MediaSelectionState mediaSelectionState = state;
        if (i2 >= mediaSelectionState.getSelectedMedia().size() || i2 < 0 || intValue >= mediaSelectionState.getSelectedMedia().size() || intValue < 0) {
            return false;
        }
        this.lastMediaDrag = new Pair<>(Integer.valueOf(i), Integer.valueOf(i2));
        List list = CollectionsKt___CollectionsKt.toMutableList((Collection) mediaSelectionState.getSelectedMedia());
        if (intValue < i2) {
            while (intValue < i2) {
                int i3 = intValue + 1;
                Collections.swap(list, intValue, i3);
                intValue = i3;
            }
        } else {
            int i4 = i2 + 1;
            if (i4 <= intValue) {
                while (true) {
                    Collections.swap(list, intValue, intValue - 1);
                    if (intValue == i4) {
                        break;
                    }
                    intValue--;
                }
            }
        }
        this.store.update(new com.annimon.stream.function.Function(list) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda10
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2131swapMedia$lambda9(this.f$0, (MediaSelectionState) obj);
            }
        });
        return true;
    }

    /* renamed from: swapMedia$lambda-9 */
    public static final MediaSelectionState m2131swapMedia$lambda9(List list, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(list, "$newMediaList");
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : list, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    public final boolean isValidMediaDragPosition(int i) {
        return i >= 0 && i < this.store.getState().getSelectedMedia().size();
    }

    public final void onMediaDragFinished() {
        this.lastMediaDrag = new Pair<>(0, 0);
    }

    public final void removeMedia(Media media) {
        Intrinsics.checkNotNullParameter(media, "media");
        removeMedia(media, false);
    }

    private final void removeMedia(Media media, boolean z) {
        Media media2;
        MediaSelectionState state = this.store.getState();
        Intrinsics.checkNotNullExpressionValue(state, "store.state");
        MediaSelectionState mediaSelectionState = state;
        List<Media> list = CollectionsKt___CollectionsKt.minus(mediaSelectionState.getSelectedMedia(), media);
        int indexOf = mediaSelectionState.getSelectedMedia().indexOf(media);
        if (list.isEmpty()) {
            media2 = null;
        } else if (Intrinsics.areEqual(media, mediaSelectionState.getFocusedMedia())) {
            media2 = list.get(Util.clamp(indexOf, 0, list.size() - 1));
        } else {
            media2 = mediaSelectionState.getFocusedMedia();
        }
        this.store.update(new com.annimon.stream.function.Function(list, media2, media) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda17
            public final /* synthetic */ List f$0;
            public final /* synthetic */ Media f$1;
            public final /* synthetic */ Media f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2124removeMedia$lambda10(this.f$0, this.f$1, this.f$2, (MediaSelectionState) obj);
            }
        });
        if (list.isEmpty() && !z) {
            this.mediaErrors.postValue(new MediaValidator.FilterError.NoItems(null, 1, null));
        }
        this.selectedMediaSubject.onNext(list);
        this.repository.deleteBlobs(CollectionsKt__CollectionsJVMKt.listOf(media));
        cancelUpload(media);
    }

    /* renamed from: removeMedia$lambda-10 */
    public static final MediaSelectionState m2124removeMedia$lambda10(List list, Media media, Media media2, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(list, "$newMediaList");
        Intrinsics.checkNotNullParameter(media2, "$media");
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        Map<Uri, Object> editorStateMap = mediaSelectionState.getEditorStateMap();
        Uri uri = media2.getUri();
        Intrinsics.checkNotNullExpressionValue(uri, "media.uri");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : list, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : media, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : MapsKt__MapsKt.minus(editorStateMap, uri), (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : Intrinsics.areEqual(media2, mediaSelectionState.getCameraFirstCapture()) ? null : mediaSelectionState.getCameraFirstCapture(), (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    public final void addCameraFirstCapture(Media media) {
        Intrinsics.checkNotNullParameter(media, "media");
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2116addCameraFirstCapture$lambda11(Media.this, (MediaSelectionState) obj);
            }
        });
        addMedia(media);
    }

    /* renamed from: addCameraFirstCapture$lambda-11 */
    public static final MediaSelectionState m2116addCameraFirstCapture$lambda11(Media media, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(media, "$media");
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "state");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : media, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    public final void removeCameraFirstCapture() {
        Media cameraFirstCapture = this.store.getState().getCameraFirstCapture();
        if (cameraFirstCapture != null) {
            removeMedia(cameraFirstCapture, true);
        }
    }

    /* renamed from: setFocusedMedia$lambda-12 */
    public static final MediaSelectionState m2126setFocusedMedia$lambda12(Media media, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(media, "$media");
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : media, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    public final void setFocusedMedia(Media media) {
        Intrinsics.checkNotNullParameter(media, "media");
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda9
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2126setFocusedMedia$lambda12(Media.this, (MediaSelectionState) obj);
            }
        });
    }

    public final void setFocusedMedia(int i) {
        this.store.update(new com.annimon.stream.function.Function(i) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2127setFocusedMedia$lambda13(this.f$0, (MediaSelectionState) obj);
            }
        });
    }

    /* renamed from: setFocusedMedia$lambda-13 */
    public static final MediaSelectionState m2127setFocusedMedia$lambda13(int i, MediaSelectionState mediaSelectionState) {
        if (i >= mediaSelectionState.getSelectedMedia().size()) {
            Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
            return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
        }
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : mediaSelectionState.getSelectedMedia().get(i), (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    public final MediaConstraints getMediaConstraints() {
        if (this.store.getState().getSendType().usesSmsTransport()) {
            Integer simSubscriptionId = this.store.getState().getSendType().getSimSubscriptionId();
            MediaConstraints mmsMediaConstraints = MediaConstraints.getMmsMediaConstraints(simSubscriptionId != null ? simSubscriptionId.intValue() : -1);
            Intrinsics.checkNotNullExpressionValue(mmsMediaConstraints, "{\n      MediaConstraints…bscriptionId ?: -1)\n    }");
            return mmsMediaConstraints;
        }
        MediaConstraints pushMediaConstraints = MediaConstraints.getPushMediaConstraints();
        Intrinsics.checkNotNullExpressionValue(pushMediaConstraints, "{\n      MediaConstraints…hMediaConstraints()\n    }");
        return pushMediaConstraints;
    }

    public final void setSentMediaQuality(SentMediaQuality sentMediaQuality) {
        Intrinsics.checkNotNullParameter(sentMediaQuality, "sentMediaQuality");
        if (sentMediaQuality != this.store.getState().getQuality()) {
            this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda7
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return MediaSelectionViewModel.m2129setSentMediaQuality$lambda14(SentMediaQuality.this, (MediaSelectionState) obj);
                }
            });
            this.repository.getUploadRepository().cancelAllUploads();
        }
    }

    /* renamed from: setSentMediaQuality$lambda-14 */
    public static final MediaSelectionState m2129setSentMediaQuality$lambda14(SentMediaQuality sentMediaQuality, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(sentMediaQuality, "$sentMediaQuality");
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : sentMediaQuality, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    /* renamed from: setMessage$lambda-15 */
    public static final MediaSelectionState m2128setMessage$lambda15(CharSequence charSequence, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : charSequence, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    public final void setMessage(CharSequence charSequence) {
        this.store.update(new com.annimon.stream.function.Function(charSequence) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ CharSequence f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2128setMessage$lambda15(this.f$0, (MediaSelectionState) obj);
            }
        });
    }

    /* renamed from: incrementViewOnceState$lambda-16 */
    public static final MediaSelectionState m2119incrementViewOnceState$lambda16(MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : mediaSelectionState.getViewOnceToggleState().next(), (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : null, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    public final void incrementViewOnceState() {
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2119incrementViewOnceState$lambda16((MediaSelectionState) obj);
            }
        });
    }

    public final Object getEditorState(Uri uri) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        return this.store.getState().getEditorStateMap().get(uri);
    }

    public final void setEditorState(Uri uri, Object obj) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        Intrinsics.checkNotNullParameter(obj, "state");
        this.store.update(new com.annimon.stream.function.Function(uri, obj) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda6
            public final /* synthetic */ Uri f$0;
            public final /* synthetic */ Object f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj2) {
                return MediaSelectionViewModel.m2125setEditorState$lambda17(this.f$0, this.f$1, (MediaSelectionState) obj2);
            }
        });
    }

    /* renamed from: setEditorState$lambda-17 */
    public static final MediaSelectionState m2125setEditorState$lambda17(Uri uri, Object obj, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(uri, "$uri");
        Intrinsics.checkNotNullParameter(obj, "$state");
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "it");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : null, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : null, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : null, (r32 & 32) != 0 ? mediaSelectionState.message : null, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : null, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : false, (r32 & 256) != 0 ? mediaSelectionState.isSent : false, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : MapsKt__MapsKt.plus(mediaSelectionState.getEditorStateMap(), TuplesKt.to(uri, obj)), (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : null, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    public final void onVideoBeginEdit(Uri uri) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        cancelUpload(MediaBuilder.buildMedia$default(MediaBuilder.INSTANCE, uri, null, 0, 0, 0, 0, 0, false, false, null, null, null, 4094, null));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Maybe send$default(MediaSelectionViewModel mediaSelectionViewModel, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = CollectionsKt__CollectionsKt.emptyList();
        }
        return mediaSelectionViewModel.send(list);
    }

    public final Maybe<MediaSendActivityResult> send(List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Intrinsics.checkNotNullParameter(list, "selectedContacts");
        Completable checkForBadIdentityRecords = UntrustedRecords.INSTANCE.checkForBadIdentityRecords(CollectionsKt___CollectionsKt.toSet(list));
        MediaSelectionRepository mediaSelectionRepository = this.repository;
        List<Media> selectedMedia = this.store.getState().getSelectedMedia();
        Map<Uri, Object> editorStateMap = this.store.getState().getEditorStateMap();
        SentMediaQuality quality = this.store.getState().getQuality();
        CharSequence message = this.store.getState().getMessage();
        boolean usesSmsTransport = this.store.getState().getSendType().usesSmsTransport();
        boolean isViewOnceEnabled = isViewOnceEnabled();
        ContactSearchKey.RecipientSearchKey recipientSearchKey = this.destination.getRecipientSearchKey();
        if (list.isEmpty()) {
            list = this.destination.getRecipientSearchKeyList();
        }
        List<Mention> mentionsFromAnnotations = MentionAnnotation.getMentionsFromAnnotations(this.store.getState().getMessage());
        Intrinsics.checkNotNullExpressionValue(mentionsFromAnnotations, "getMentionsFromAnnotations(store.state.message)");
        Maybe<MediaSendActivityResult> andThen = checkForBadIdentityRecords.andThen(mediaSelectionRepository.send(selectedMedia, editorStateMap, quality, message, usesSmsTransport, isViewOnceEnabled, recipientSearchKey, list, mentionsFromAnnotations, this.store.getState().getSendType()));
        Intrinsics.checkNotNullExpressionValue(andThen, "UntrustedRecords.checkFo…te.sendType\n      )\n    )");
        return andThen;
    }

    private final boolean isViewOnceEnabled() {
        if (!this.store.getState().getSendType().usesSmsTransport() && this.store.getState().getSelectedMedia().size() == 1 && this.store.getState().getViewOnceToggleState() == MediaSelectionState.ViewOnceToggleState.ONCE) {
            return true;
        }
        return false;
    }

    private final void startUpload(List<? extends Media> list) {
        if (this.store.getState().isPreUploadEnabled()) {
            if (Stories.isFeatureEnabled()) {
                ArrayList arrayList = new ArrayList();
                for (Object obj : list) {
                    if (Stories.MediaTransform.canPreUploadMedia((Media) obj)) {
                        arrayList.add(obj);
                    }
                }
                list = arrayList;
            }
            this.repository.getUploadRepository().startUpload(list, this.store.getState().getRecipient());
        }
    }

    private final void cancelUpload(Media media) {
        this.repository.getUploadRepository().cancelUpload(media);
    }

    private final boolean shouldPreUpload(boolean z, boolean z2, Recipient recipient) {
        return !z && !z2 && !this.repository.isLocalSelfSend(recipient, z2);
    }

    public final void onSaveState(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "outState");
        MediaSelectionState state = this.store.getState();
        Intrinsics.checkNotNullExpressionValue(state, "store.state");
        MediaSelectionState mediaSelectionState = state;
        bundle.putParcelableArrayList(STATE_SELECTION, new ArrayList<>(mediaSelectionState.getSelectedMedia()));
        bundle.putParcelable(STATE_FOCUSED, mediaSelectionState.getFocusedMedia());
        bundle.putInt(STATE_QUALITY, mediaSelectionState.getQuality().getCode());
        bundle.putCharSequence(STATE_MESSAGE, mediaSelectionState.getMessage());
        bundle.putInt(STATE_VIEW_ONCE, mediaSelectionState.getViewOnceToggleState().getCode());
        bundle.putBoolean(STATE_TOUCH_ENABLED, mediaSelectionState.isTouchEnabled());
        bundle.putBoolean(STATE_SENT, mediaSelectionState.isSent());
        bundle.putParcelable(STATE_CAMERA_FIRST_CAPTURE, mediaSelectionState.getCameraFirstCapture());
        Set<Map.Entry<Uri, Object>> entrySet = this.store.getState().getEditorStateMap().entrySet();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(entrySet, 10));
        Iterator<T> it = entrySet.iterator();
        while (it.hasNext()) {
            arrayList.add(toBundleStateEntry((Map.Entry) it.next()));
        }
        bundle.putParcelableArrayList(STATE_EDITORS, new ArrayList<>(arrayList));
    }

    public final boolean hasSelectedMedia() {
        return !this.store.getState().getSelectedMedia().isEmpty();
    }

    public final void onRestoreState(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "savedInstanceState");
        List<Media> parcelableArrayList = bundle.getParcelableArrayList(STATE_SELECTION);
        if (parcelableArrayList == null) {
            parcelableArrayList = CollectionsKt__CollectionsKt.emptyList();
        }
        Media media = (Media) bundle.getParcelable(STATE_FOCUSED);
        SentMediaQuality fromCode = SentMediaQuality.fromCode(bundle.getInt(STATE_QUALITY));
        Intrinsics.checkNotNullExpressionValue(fromCode, "fromCode(savedInstanceState.getInt(STATE_QUALITY))");
        CharSequence charSequence = bundle.getCharSequence(STATE_MESSAGE);
        MediaSelectionState.ViewOnceToggleState fromCode2 = MediaSelectionState.ViewOnceToggleState.Companion.fromCode(bundle.getInt(STATE_VIEW_ONCE));
        boolean z = bundle.getBoolean(STATE_TOUCH_ENABLED);
        boolean z2 = bundle.getBoolean(STATE_SENT);
        Media media2 = (Media) bundle.getParcelable(STATE_CAMERA_FIRST_CAPTURE);
        Iterable<Bundle> parcelableArrayList2 = bundle.getParcelableArrayList(STATE_EDITORS);
        if (parcelableArrayList2 == null) {
            parcelableArrayList2 = CollectionsKt__CollectionsKt.emptyList();
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(parcelableArrayList2, 10)), 16));
        for (Bundle bundle2 : parcelableArrayList2) {
            Pair<Uri, Object> association = toAssociation(bundle2);
            linkedHashMap.put(association.getFirst(), association.getSecond());
        }
        this.selectedMediaSubject.onNext(parcelableArrayList);
        this.store.update(new com.annimon.stream.function.Function(parcelableArrayList, media, fromCode, charSequence, fromCode2, z, z2, linkedHashMap, media2) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel$$ExternalSyntheticLambda15
            public final /* synthetic */ List f$0;
            public final /* synthetic */ Media f$1;
            public final /* synthetic */ SentMediaQuality f$2;
            public final /* synthetic */ CharSequence f$3;
            public final /* synthetic */ MediaSelectionState.ViewOnceToggleState f$4;
            public final /* synthetic */ boolean f$5;
            public final /* synthetic */ boolean f$6;
            public final /* synthetic */ Map f$7;
            public final /* synthetic */ Media f$8;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
                this.f$8 = r9;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaSelectionViewModel.m2123onRestoreState$lambda22(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, (MediaSelectionState) obj);
            }
        });
    }

    /* renamed from: onRestoreState$lambda-22 */
    public static final MediaSelectionState m2123onRestoreState$lambda22(List list, Media media, SentMediaQuality sentMediaQuality, CharSequence charSequence, MediaSelectionState.ViewOnceToggleState viewOnceToggleState, boolean z, boolean z2, Map map, Media media2, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(list, "$selection");
        Intrinsics.checkNotNullParameter(sentMediaQuality, "$quality");
        Intrinsics.checkNotNullParameter(viewOnceToggleState, "$viewOnce");
        Intrinsics.checkNotNullParameter(map, "$editorStateMap");
        Intrinsics.checkNotNullExpressionValue(mediaSelectionState, "state");
        return mediaSelectionState.copy((r32 & 1) != 0 ? mediaSelectionState.sendType : null, (r32 & 2) != 0 ? mediaSelectionState.selectedMedia : list, (r32 & 4) != 0 ? mediaSelectionState.focusedMedia : media, (r32 & 8) != 0 ? mediaSelectionState.recipient : null, (r32 & 16) != 0 ? mediaSelectionState.quality : sentMediaQuality, (r32 & 32) != 0 ? mediaSelectionState.message : charSequence, (r32 & 64) != 0 ? mediaSelectionState.viewOnceToggleState : viewOnceToggleState, (r32 & 128) != 0 ? mediaSelectionState.isTouchEnabled : z, (r32 & 256) != 0 ? mediaSelectionState.isSent : z2, (r32 & 512) != 0 ? mediaSelectionState.isPreUploadEnabled : false, (r32 & 1024) != 0 ? mediaSelectionState.isMeteredConnection : false, (r32 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? mediaSelectionState.editorStateMap : map, (r32 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? mediaSelectionState.cameraFirstCapture : media2, (r32 & 8192) != 0 ? mediaSelectionState.isStory : false, (r32 & 16384) != 0 ? mediaSelectionState.storySendRequirements : null);
    }

    private final Pair<Uri, Object> toAssociation(Bundle bundle) {
        Object obj;
        Parcelable parcelable = bundle.getParcelable(BUNDLE_URI);
        if (parcelable != null) {
            Uri uri = (Uri) parcelable;
            if (bundle.getBoolean(BUNDLE_IS_IMAGE)) {
                obj = new ImageEditorFragment.Data(bundle);
            } else {
                obj = VideoEditorFragment.Data.fromBundle(bundle);
                Intrinsics.checkNotNullExpressionValue(obj, "{\n      VideoEditorFragm…ta.fromBundle(this)\n    }");
            }
            return TuplesKt.to(uri, obj);
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    private final Bundle toBundleStateEntry(Map.Entry<? extends Uri, ? extends Object> entry) {
        Object value = entry.getValue();
        if (value instanceof ImageEditorFragment.Data) {
            Bundle bundle = ((ImageEditorFragment.Data) value).getBundle();
            bundle.putParcelable(BUNDLE_URI, (Parcelable) entry.getKey());
            bundle.putBoolean(BUNDLE_IS_IMAGE, true);
            Intrinsics.checkNotNullExpressionValue(bundle, "{\n        value.bundle.a…, true)\n        }\n      }");
            return bundle;
        } else if (value instanceof VideoEditorFragment.Data) {
            Bundle bundle2 = ((VideoEditorFragment.Data) value).getBundle();
            bundle2.putParcelable(BUNDLE_URI, (Parcelable) entry.getKey());
            bundle2.putBoolean(BUNDLE_IS_IMAGE, false);
            Intrinsics.checkNotNullExpressionValue(bundle2, "{\n        value.bundle.a… false)\n        }\n      }");
            return bundle2;
        } else {
            throw new IllegalStateException();
        }
    }

    /* compiled from: MediaSelectionViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel$Companion;", "", "()V", "BUNDLE_IS_IMAGE", "", "BUNDLE_URI", "STATE_CAMERA_FIRST_CAPTURE", "STATE_EDITORS", "STATE_FOCUSED", "STATE_MESSAGE", "STATE_PREFIX", "STATE_QUALITY", "STATE_SELECTION", "STATE_SENT", "STATE_TOUCH_ENABLED", "STATE_VIEW_ONCE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* compiled from: MediaSelectionViewModel.kt */
    @Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u000e\u001a\u00020\u000f¢\u0006\u0002\u0010\u0010J%\u0010\u0011\u001a\u0002H\u0012\"\b\b\u0000\u0010\u0012*\u00020\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\u00120\u0015H\u0016¢\u0006\u0002\u0010\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "destination", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;", "sendType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "initialMedia", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "initialMessage", "", "isReply", "", "isStory", "repository", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionRepository;", "(Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionDestination;Lorg/thoughtcrime/securesms/conversation/MessageSendType;Ljava/util/List;Ljava/lang/CharSequence;ZZLorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final MediaSelectionDestination destination;
        private final List<Media> initialMedia;
        private final CharSequence initialMessage;
        private final boolean isReply;
        private final boolean isStory;
        private final MediaSelectionRepository repository;
        private final MessageSendType sendType;

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.mediasend.Media> */
        /* JADX WARN: Multi-variable type inference failed */
        public Factory(MediaSelectionDestination mediaSelectionDestination, MessageSendType messageSendType, List<? extends Media> list, CharSequence charSequence, boolean z, boolean z2, MediaSelectionRepository mediaSelectionRepository) {
            Intrinsics.checkNotNullParameter(mediaSelectionDestination, "destination");
            Intrinsics.checkNotNullParameter(messageSendType, "sendType");
            Intrinsics.checkNotNullParameter(list, "initialMedia");
            Intrinsics.checkNotNullParameter(mediaSelectionRepository, "repository");
            this.destination = mediaSelectionDestination;
            this.sendType = messageSendType;
            this.initialMedia = list;
            this.initialMessage = charSequence;
            this.isReply = z;
            this.isStory = z2;
            this.repository = mediaSelectionRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new MediaSelectionViewModel(this.destination, this.sendType, this.initialMedia, this.initialMessage, this.isReply, this.isStory, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
