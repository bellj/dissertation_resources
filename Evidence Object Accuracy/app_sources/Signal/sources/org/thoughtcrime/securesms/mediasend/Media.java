package org.thoughtcrime.securesms.mediasend;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import j$.util.Optional;
import j$.util.function.Function;
import java.io.IOException;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.whispersystems.signalservice.api.util.Preconditions;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes4.dex */
public class Media implements Parcelable {
    public static final String ALL_MEDIA_BUCKET_ID;
    public static final Parcelable.Creator<Media> CREATOR = new Parcelable.Creator<Media>() { // from class: org.thoughtcrime.securesms.mediasend.Media.1
        @Override // android.os.Parcelable.Creator
        public Media createFromParcel(Parcel parcel) {
            return new Media(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public Media[] newArray(int i) {
            return new Media[i];
        }
    };
    private final boolean borderless;
    private Optional<String> bucketId;
    private Optional<String> caption;
    private final long date;
    private final long duration;
    private final int height;
    private final String mimeType;
    private final long size;
    private Optional<AttachmentDatabase.TransformProperties> transformProperties;
    private final Uri uri;
    private final boolean videoGif;
    private final int width;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public Media(Uri uri, String str, long j, int i, int i2, long j2, long j3, boolean z, boolean z2, Optional<String> optional, Optional<String> optional2, Optional<AttachmentDatabase.TransformProperties> optional3) {
        this.uri = uri;
        this.mimeType = str;
        this.date = j;
        this.width = i;
        this.height = i2;
        this.size = j2;
        this.duration = j3;
        this.borderless = z;
        this.videoGif = z2;
        this.bucketId = optional;
        this.caption = optional2;
        this.transformProperties = optional3;
    }

    protected Media(Parcel parcel) {
        this.uri = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.mimeType = parcel.readString();
        this.date = parcel.readLong();
        this.width = parcel.readInt();
        this.height = parcel.readInt();
        this.size = parcel.readLong();
        this.duration = parcel.readLong();
        boolean z = false;
        this.borderless = parcel.readInt() == 1;
        this.videoGif = parcel.readInt() == 1 ? true : z;
        this.bucketId = Optional.ofNullable(parcel.readString());
        this.caption = Optional.ofNullable(parcel.readString());
        try {
            String readString = parcel.readString();
            this.transformProperties = readString == null ? Optional.empty() : Optional.ofNullable((AttachmentDatabase.TransformProperties) JsonUtil.fromJson(readString, AttachmentDatabase.TransformProperties.class));
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public Uri getUri() {
        return this.uri;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public long getDate() {
        return this.date;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public long getSize() {
        return this.size;
    }

    public long getDuration() {
        return this.duration;
    }

    public boolean isBorderless() {
        return this.borderless;
    }

    public boolean isVideoGif() {
        return this.videoGif;
    }

    public Optional<String> getBucketId() {
        return this.bucketId;
    }

    public Optional<String> getCaption() {
        return this.caption;
    }

    public void setCaption(String str) {
        this.caption = Optional.ofNullable(str);
    }

    public Optional<AttachmentDatabase.TransformProperties> getTransformProperties() {
        return this.transformProperties;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.uri, i);
        parcel.writeString(this.mimeType);
        parcel.writeLong(this.date);
        parcel.writeInt(this.width);
        parcel.writeInt(this.height);
        parcel.writeLong(this.size);
        parcel.writeLong(this.duration);
        parcel.writeInt(this.borderless ? 1 : 0);
        parcel.writeInt(this.videoGif ? 1 : 0);
        parcel.writeString(this.bucketId.orElse(null));
        parcel.writeString(this.caption.orElse(null));
        parcel.writeString((String) this.transformProperties.map(new Function() { // from class: org.thoughtcrime.securesms.mediasend.Media$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return JsonUtil.toJson((AttachmentDatabase.TransformProperties) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null));
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.uri.equals(((Media) obj).uri);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.uri.hashCode();
    }

    public static Media withMimeType(Media media, String str) {
        return new Media(media.getUri(), str, media.getDate(), media.getWidth(), media.getHeight(), media.getSize(), media.getDuration(), media.isBorderless(), media.isVideoGif(), media.getBucketId(), media.getCaption(), media.getTransformProperties());
    }

    public static Media stripTransform(Media media) {
        Preconditions.checkArgument(MediaUtil.isImageType(media.mimeType));
        return new Media(media.getUri(), media.getMimeType(), media.getDate(), media.getWidth(), media.getHeight(), media.getSize(), media.getDuration(), media.isBorderless(), media.isVideoGif(), media.getBucketId(), media.getCaption(), Optional.empty());
    }
}
