package org.thoughtcrime.securesms.mediasend;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.mediasend.Camera1Controller;

/* loaded from: classes4.dex */
public class Camera1Controller {
    private static final String TAG = Log.tag(Camera1Controller.class);
    private final Comparator<Camera.Size> ASC_SIZE_COMPARATOR = new Comparator() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Controller$$ExternalSyntheticLambda3
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            return Camera1Controller.$r8$lambda$5nNqv131aPuEaLP7k1oNIQqeOQE((Camera.Size) obj, (Camera.Size) obj2);
        }
    };
    private Camera camera;
    private int cameraId;
    private final OrderEnforcer<Stage> enforcer;
    private final EventListener eventListener;
    private SurfaceTexture previewSurface;
    private final int screenHeight;
    private int screenRotation;
    private final int screenWidth;

    /* loaded from: classes4.dex */
    public interface CaptureCallback {
        void onCaptureAvailable(byte[] bArr, boolean z);
    }

    /* loaded from: classes4.dex */
    public interface EventListener {
        void onCameraUnavailable();

        void onPropertiesAvailable(Properties properties);
    }

    /* loaded from: classes4.dex */
    public enum Stage {
        INITIALIZED,
        PREVIEW_STARTED
    }

    private int convertRotationToDegrees(int i) {
        if (i == 1) {
            return 90;
        }
        if (i == 2) {
            return SubsamplingScaleImageView.ORIENTATION_180;
        }
        if (i != 3) {
            return 0;
        }
        return SubsamplingScaleImageView.ORIENTATION_270;
    }

    public Camera1Controller(int i, int i2, int i3, EventListener eventListener) {
        this.eventListener = eventListener;
        this.enforcer = new OrderEnforcer<>(Stage.INITIALIZED, Stage.PREVIEW_STARTED);
        this.cameraId = Camera.getNumberOfCameras() <= 1 ? 0 : i;
        this.screenWidth = i2;
        this.screenHeight = i3;
    }

    public void initialize() {
        String str = TAG;
        Log.d(str, "initialize()");
        if (Camera.getNumberOfCameras() <= 0) {
            Log.w(str, "Device doesn't have any cameras.");
            onCameraUnavailable();
            return;
        }
        try {
            Camera open = Camera.open(this.cameraId);
            this.camera = open;
            if (open == null) {
                Log.w(str, "Null camera instance.");
                onCameraUnavailable();
                return;
            }
            Camera.Parameters parameters = open.getParameters();
            Camera.Size closestSize = getClosestSize(this.camera.getParameters().getSupportedPreviewSizes(), this.screenWidth, this.screenHeight);
            Camera.Size closestSize2 = getClosestSize(this.camera.getParameters().getSupportedPictureSizes(), this.screenWidth, this.screenHeight);
            List<String> supportedFocusModes = parameters.getSupportedFocusModes();
            Log.d(str, "Preview size: " + closestSize.width + "x" + closestSize.height + "  Picture size: " + closestSize2.width + "x" + closestSize2.height);
            parameters.setPreviewSize(closestSize.width, closestSize.height);
            parameters.setPictureSize(closestSize2.width, closestSize2.height);
            parameters.setFlashMode("off");
            parameters.setColorEffect("none");
            parameters.setWhiteBalance("auto");
            if (supportedFocusModes.contains("continuous-picture")) {
                parameters.setFocusMode("continuous-picture");
            } else if (supportedFocusModes.contains("continuous-video")) {
                parameters.setFocusMode("continuous-video");
            }
            this.camera.setParameters(parameters);
            this.enforcer.markCompleted(Stage.INITIALIZED);
            this.eventListener.onPropertiesAvailable(getProperties());
        } catch (Exception e) {
            Log.w(TAG, "Failed to open camera.", e);
            onCameraUnavailable();
        }
    }

    public void release() {
        Log.d(TAG, "release() called");
        this.enforcer.run(Stage.INITIALIZED, new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Controller$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                Camera1Controller.m2060$r8$lambda$XofBC8RT112Jc4VGDk8jFotn5I(Camera1Controller.this);
            }
        });
    }

    public /* synthetic */ void lambda$release$0() {
        Log.d(TAG, "release() executing");
        this.previewSurface = null;
        this.camera.stopPreview();
        this.camera.release();
        this.enforcer.reset();
    }

    public void linkSurface(SurfaceTexture surfaceTexture) {
        Log.d(TAG, "linkSurface() called");
        this.enforcer.run(Stage.INITIALIZED, new Runnable(surfaceTexture) { // from class: org.thoughtcrime.securesms.mediasend.Camera1Controller$$ExternalSyntheticLambda2
            public final /* synthetic */ SurfaceTexture f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                Camera1Controller.$r8$lambda$FIo0mu8VANxfNT7maLABgJKTOBM(Camera1Controller.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$linkSurface$1(SurfaceTexture surfaceTexture) {
        try {
            Log.d(TAG, "linkSurface() executing");
            this.previewSurface = surfaceTexture;
            this.camera.setPreviewTexture(surfaceTexture);
            this.camera.startPreview();
            this.enforcer.markCompleted(Stage.PREVIEW_STARTED);
        } catch (Exception e) {
            Log.w(TAG, "Failed to start preview.", e);
            this.eventListener.onCameraUnavailable();
        }
    }

    public void capture(CaptureCallback captureCallback) {
        this.enforcer.run(Stage.PREVIEW_STARTED, new Runnable(captureCallback) { // from class: org.thoughtcrime.securesms.mediasend.Camera1Controller$$ExternalSyntheticLambda4
            public final /* synthetic */ Camera1Controller.CaptureCallback f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                Camera1Controller.m2061$r8$lambda$lOr85oRuFEdG6NrxyJxmyCjrGo(Camera1Controller.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$capture$3(CaptureCallback captureCallback) {
        this.camera.takePicture(null, null, null, new Camera.PictureCallback(captureCallback) { // from class: org.thoughtcrime.securesms.mediasend.Camera1Controller$$ExternalSyntheticLambda5
            public final /* synthetic */ Camera1Controller.CaptureCallback f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.hardware.Camera.PictureCallback
            public final void onPictureTaken(byte[] bArr, Camera camera) {
                Camera1Controller.$r8$lambda$7569cVoOvyZ67B_UpXVzVt4nq_E(Camera1Controller.this, this.f$1, bArr, camera);
            }
        });
    }

    public /* synthetic */ void lambda$capture$2(CaptureCallback captureCallback, byte[] bArr, Camera camera) {
        boolean z = true;
        if (this.cameraId != 1) {
            z = false;
        }
        captureCallback.onCaptureAvailable(bArr, z);
    }

    public int flip() {
        Log.d(TAG, "flip()");
        SurfaceTexture surfaceTexture = this.previewSurface;
        this.cameraId = this.cameraId == 0 ? 1 : 0;
        release();
        initialize();
        linkSurface(surfaceTexture);
        setScreenRotation(this.screenRotation);
        return this.cameraId;
    }

    public void setScreenRotation(int i) {
        String str = TAG;
        Log.d(str, "setScreenRotation(" + i + ") called");
        this.enforcer.run(Stage.PREVIEW_STARTED, new Runnable(i) { // from class: org.thoughtcrime.securesms.mediasend.Camera1Controller$$ExternalSyntheticLambda1
            public final /* synthetic */ int f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                Camera1Controller.$r8$lambda$3bUhhKTse81NhfoMvgJ3XGH6aHo(Camera1Controller.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$setScreenRotation$4(int i) {
        String str = TAG;
        Log.d(str, "setScreenRotation(" + i + ") executing");
        this.screenRotation = i;
        int previewRotation = getPreviewRotation(i);
        int outputRotation = getOutputRotation(i);
        Log.d(str, "Preview rotation: " + previewRotation + "  Output rotation: " + outputRotation);
        this.camera.setDisplayOrientation(previewRotation);
        Camera.Parameters parameters = this.camera.getParameters();
        parameters.setRotation(outputRotation);
        this.camera.setParameters(parameters);
    }

    private void onCameraUnavailable() {
        this.enforcer.reset();
        this.eventListener.onCameraUnavailable();
    }

    private Properties getProperties() {
        Camera.Size previewSize = this.camera.getParameters().getPreviewSize();
        return new Properties(Camera.getNumberOfCameras(), previewSize.width, previewSize.height);
    }

    private Camera.Size getClosestSize(List<Camera.Size> list, int i, int i2) {
        Collections.sort(list, this.ASC_SIZE_COMPARATOR);
        int i3 = 0;
        while (i3 < list.size() && list.get(i3).width * list.get(i3).height < i * i2) {
            i3++;
        }
        return list.get(Math.min(i3 + 1, list.size() - 1));
    }

    private int getOutputRotation(int i) {
        int convertRotationToDegrees = convertRotationToDegrees(i);
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(this.cameraId, cameraInfo);
        if (cameraInfo.facing == 1) {
            return (cameraInfo.orientation + convertRotationToDegrees) % 360;
        }
        return ((cameraInfo.orientation - convertRotationToDegrees) + 360) % 360;
    }

    private int getPreviewRotation(int i) {
        int convertRotationToDegrees = convertRotationToDegrees(i);
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(this.cameraId, cameraInfo);
        if (cameraInfo.facing == 1) {
            return (360 - ((cameraInfo.orientation + convertRotationToDegrees) % 360)) % 360;
        }
        return ((cameraInfo.orientation - convertRotationToDegrees) + 360) % 360;
    }

    public static /* synthetic */ int lambda$new$5(Camera.Size size, Camera.Size size2) {
        return Integer.compare(size.width * size.height, size2.width * size2.height);
    }

    /* loaded from: classes4.dex */
    public class Properties {
        private final int cameraCount;
        private final int previewHeight;
        private final int previewWidth;

        Properties(int i, int i2, int i3) {
            Camera1Controller.this = r1;
            this.cameraCount = i;
            this.previewWidth = i2;
            this.previewHeight = i3;
        }

        public int getCameraCount() {
            return this.cameraCount;
        }

        public int getPreviewWidth() {
            return this.previewWidth;
        }

        public int getPreviewHeight() {
            return this.previewHeight;
        }

        public String toString() {
            return "cameraCount: " + this.cameraCount + "  previewWidth: " + this.previewWidth + "  previewHeight: " + this.previewHeight;
        }
    }
}
