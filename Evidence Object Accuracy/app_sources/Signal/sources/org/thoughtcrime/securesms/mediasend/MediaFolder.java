package org.thoughtcrime.securesms.mediasend;

import android.net.Uri;

/* loaded from: classes4.dex */
public class MediaFolder {
    private final String bucketId;
    private final FolderType folderType;
    private final int itemCount;
    private final Uri thumbnailUri;
    private final String title;

    /* loaded from: classes4.dex */
    public enum FolderType {
        NORMAL,
        CAMERA
    }

    public MediaFolder(Uri uri, String str, int i, String str2, FolderType folderType) {
        this.thumbnailUri = uri;
        this.title = str;
        this.itemCount = i;
        this.bucketId = str2;
        this.folderType = folderType;
    }

    public Uri getThumbnailUri() {
        return this.thumbnailUri;
    }

    public String getTitle() {
        return this.title;
    }

    public int getItemCount() {
        return this.itemCount;
    }

    public String getBucketId() {
        return this.bucketId;
    }

    public FolderType getFolderType() {
        return this.folderType;
    }
}
