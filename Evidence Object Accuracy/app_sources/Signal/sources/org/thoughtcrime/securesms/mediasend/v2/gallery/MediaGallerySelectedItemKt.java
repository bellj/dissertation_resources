package org.thoughtcrime.securesms.mediasend.v2.gallery;

import kotlin.Metadata;

/* compiled from: MediaGallerySelectedItem.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000*\"\u0010\u0000\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001¨\u0006\u0004"}, d2 = {"OnSelectedMediaClicked", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGallerySelectedItemKt {
}
