package org.thoughtcrime.securesms.mediasend.v2.text.send;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.IdentityRecord;

/* compiled from: TextStoryPostSendResult.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult;", "", "()V", "Failure", "Success", "UntrustedRecordsError", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult$Success;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult$Failure;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult$UntrustedRecordsError;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class TextStoryPostSendResult {
    public /* synthetic */ TextStoryPostSendResult(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    /* compiled from: TextStoryPostSendResult.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult$Success;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Success extends TextStoryPostSendResult {
        public static final Success INSTANCE = new Success();

        private Success() {
            super(null);
        }
    }

    private TextStoryPostSendResult() {
    }

    /* compiled from: TextStoryPostSendResult.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult$Failure;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Failure extends TextStoryPostSendResult {
        public static final Failure INSTANCE = new Failure();

        private Failure() {
            super(null);
        }
    }

    /* compiled from: TextStoryPostSendResult.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult$UntrustedRecordsError;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult;", "untrustedRecords", "", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "(Ljava/util/List;)V", "getUntrustedRecords", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class UntrustedRecordsError extends TextStoryPostSendResult {
        private final List<IdentityRecord> untrustedRecords;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendResult$UntrustedRecordsError */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ UntrustedRecordsError copy$default(UntrustedRecordsError untrustedRecordsError, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                list = untrustedRecordsError.untrustedRecords;
            }
            return untrustedRecordsError.copy(list);
        }

        public final List<IdentityRecord> component1() {
            return this.untrustedRecords;
        }

        public final UntrustedRecordsError copy(List<IdentityRecord> list) {
            Intrinsics.checkNotNullParameter(list, "untrustedRecords");
            return new UntrustedRecordsError(list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof UntrustedRecordsError) && Intrinsics.areEqual(this.untrustedRecords, ((UntrustedRecordsError) obj).untrustedRecords);
        }

        public int hashCode() {
            return this.untrustedRecords.hashCode();
        }

        public String toString() {
            return "UntrustedRecordsError(untrustedRecords=" + this.untrustedRecords + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public UntrustedRecordsError(List<IdentityRecord> list) {
            super(null);
            Intrinsics.checkNotNullParameter(list, "untrustedRecords");
            this.untrustedRecords = list;
        }

        public final List<IdentityRecord> getUntrustedRecords() {
            return this.untrustedRecords;
        }
    }
}
