package org.thoughtcrime.securesms.mediasend.v2.text;

import kotlin.Metadata;

/* compiled from: TextStoryScale.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryScale;", "", "()V", "convertToScale", "", "textScale", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryScale {
    public static final TextStoryScale INSTANCE = new TextStoryScale();

    public final float convertToScale(int i) {
        if (i < 0) {
            return 1.0f;
        }
        return (1.0f * (((float) i) / 100.0f)) + 0.5f;
    }

    private TextStoryScale() {
    }
}
