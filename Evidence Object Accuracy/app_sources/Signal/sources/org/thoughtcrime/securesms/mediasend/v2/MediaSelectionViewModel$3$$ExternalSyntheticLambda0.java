package org.thoughtcrime.securesms.mediasend.v2;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaSelectionViewModel$3$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ Stories.MediaTransform.SendRequirements f$0;

    public /* synthetic */ MediaSelectionViewModel$3$$ExternalSyntheticLambda0(Stories.MediaTransform.SendRequirements sendRequirements) {
        this.f$0 = sendRequirements;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return MediaSelectionViewModel.AnonymousClass3.m2132invoke$lambda0(this.f$0, (MediaSelectionState) obj);
    }
}
