package org.thoughtcrime.securesms.mediasend.camerax;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.util.Rational;
import android.util.Size;
import androidx.camera.camera2.internal.compat.CameraManagerCompat;
import androidx.camera.core.ImageProxy;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.Stopwatch;

/* loaded from: classes4.dex */
public class CameraXUtil {
    private static final int[] CAMERA_HARDWARE_LEVEL_ORDERING = {2, 0, 1};
    private static final int[] CAMERA_HARDWARE_LEVEL_ORDERING_24 = {2, 0, 1, 3};
    private static final int[] CAMERA_HARDWARE_LEVEL_ORDERING_28 = {2, 0, 4, 1, 3};
    private static final String TAG = Log.tag(CameraXUtil.class);

    public static int toCameraDirectionInt(int i) {
        return i == 0 ? 1 : 0;
    }

    public static int toLensFacing(int i) {
        return i == 1 ? 0 : 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00d1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil.ImageResult toJpeg(androidx.camera.core.ImageProxy r12, boolean r13) throws java.io.IOException {
        /*
            androidx.camera.core.ImageProxy$PlaneProxy[] r0 = r12.getPlanes()
            r1 = 0
            r0 = r0[r1]
            java.nio.ByteBuffer r0 = r0.getBuffer()
            boolean r2 = shouldCropImage(r12)
            if (r2 == 0) goto L_0x0016
            android.graphics.Rect r2 = r12.getCropRect()
            goto L_0x0017
        L_0x0016:
            r2 = 0
        L_0x0017:
            int r3 = r0.capacity()
            byte[] r3 = new byte[r3]
            androidx.camera.core.ImageInfo r4 = r12.getImageInfo()
            int r4 = r4.getRotationDegrees()
            r0.get(r3)
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream     // Catch: BitmapDecodingException -> 0x00aa
            r0.<init>(r3)     // Catch: BitmapDecodingException -> 0x00aa
            android.util.Pair r0 = org.thoughtcrime.securesms.util.BitmapUtil.getDimensions(r0)     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.Object r5 = r0.first     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.Integer r5 = (java.lang.Integer) r5     // Catch: BitmapDecodingException -> 0x00aa
            int r5 = r5.intValue()     // Catch: BitmapDecodingException -> 0x00aa
            int r6 = r12.getWidth()     // Catch: BitmapDecodingException -> 0x00aa
            if (r5 == r6) goto L_0x00a8
            java.lang.Object r5 = r0.second     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.Integer r5 = (java.lang.Integer) r5     // Catch: BitmapDecodingException -> 0x00aa
            int r5 = r5.intValue()     // Catch: BitmapDecodingException -> 0x00aa
            int r6 = r12.getHeight()     // Catch: BitmapDecodingException -> 0x00aa
            if (r5 == r6) goto L_0x00a8
            java.lang.String r5 = org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil.TAG     // Catch: BitmapDecodingException -> 0x00aa
            java.util.Locale r6 = java.util.Locale.ENGLISH     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.String r7 = "Decoded image dimensions differed from stated dimensions! Stated: %d x %d, Decoded: %d x %d"
            r8 = 4
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch: BitmapDecodingException -> 0x00aa
            int r9 = r12.getWidth()     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch: BitmapDecodingException -> 0x00aa
            r8[r1] = r9     // Catch: BitmapDecodingException -> 0x00aa
            r9 = 1
            int r10 = r12.getHeight()     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch: BitmapDecodingException -> 0x00aa
            r8[r9] = r10     // Catch: BitmapDecodingException -> 0x00aa
            r9 = 2
            java.lang.Object r10 = r0.first     // Catch: BitmapDecodingException -> 0x00aa
            r8[r9] = r10     // Catch: BitmapDecodingException -> 0x00aa
            r9 = 3
            java.lang.Object r0 = r0.second     // Catch: BitmapDecodingException -> 0x00aa
            r8[r9] = r0     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.String r0 = java.lang.String.format(r6, r7, r8)     // Catch: BitmapDecodingException -> 0x00aa
            org.signal.core.util.logging.Log.w(r5, r0)     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch: BitmapDecodingException -> 0x00aa
            r0.<init>()     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.String r6 = "Ignoring the stated rotation and rotating the crop rect 90 degrees (stated rotation is "
            r0.append(r6)     // Catch: BitmapDecodingException -> 0x00aa
            r0.append(r4)     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.String r6 = " degrees)."
            r0.append(r6)     // Catch: BitmapDecodingException -> 0x00aa
            java.lang.String r0 = r0.toString()     // Catch: BitmapDecodingException -> 0x00aa
            org.signal.core.util.logging.Log.w(r5, r0)     // Catch: BitmapDecodingException -> 0x00aa
            if (r2 == 0) goto L_0x00b3
            android.graphics.Rect r0 = new android.graphics.Rect     // Catch: BitmapDecodingException -> 0x00a6
            int r4 = r2.top     // Catch: BitmapDecodingException -> 0x00a6
            int r5 = r2.left     // Catch: BitmapDecodingException -> 0x00a6
            int r6 = r2.bottom     // Catch: BitmapDecodingException -> 0x00a6
            int r7 = r2.right     // Catch: BitmapDecodingException -> 0x00a6
            r0.<init>(r4, r5, r6, r7)     // Catch: BitmapDecodingException -> 0x00a6
            r2 = r0
            goto L_0x00b3
        L_0x00a6:
            r0 = move-exception
            goto L_0x00ac
        L_0x00a8:
            r1 = r4
            goto L_0x00b3
        L_0x00aa:
            r0 = move-exception
            r1 = r4
        L_0x00ac:
            java.lang.String r4 = org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil.TAG
            java.lang.String r5 = "Failed to decode!"
            org.signal.core.util.logging.Log.w(r4, r5, r0)
        L_0x00b3:
            if (r2 != 0) goto L_0x00b9
            if (r1 != 0) goto L_0x00b9
            if (r13 == 0) goto L_0x00bd
        L_0x00b9:
            byte[] r3 = transformByteArray(r3, r2, r1, r13)
        L_0x00bd:
            if (r2 == 0) goto L_0x00c5
            int r13 = r2.right
            int r0 = r2.left
            int r13 = r13 - r0
            goto L_0x00c9
        L_0x00c5:
            int r13 = r12.getWidth()
        L_0x00c9:
            if (r2 == 0) goto L_0x00d1
            int r12 = r2.bottom
            int r0 = r2.top
            int r12 = r12 - r0
            goto L_0x00d5
        L_0x00d1:
            int r12 = r12.getHeight()
        L_0x00d5:
            r0 = 90
            if (r1 == r0) goto L_0x00dd
            r0 = 270(0x10e, float:3.78E-43)
            if (r1 != r0) goto L_0x00e0
        L_0x00dd:
            r11 = r13
            r13 = r12
            r12 = r11
        L_0x00e0:
            org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil$ImageResult r0 = new org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil$ImageResult
            r0.<init>(r3, r13, r12)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil.toJpeg(androidx.camera.core.ImageProxy, boolean):org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil$ImageResult");
    }

    public static boolean isSupported() {
        return Build.VERSION.SDK_INT >= 21 && !CameraXModelBlacklist.isBlacklisted();
    }

    public static int getOptimalCaptureMode() {
        return !FastCameraModels.contains(Build.MODEL) ? 1 : 0;
    }

    public static int getIdealResolution(int i, int i2) {
        return Math.max(Math.max(i, i2), 1920);
    }

    public static Size buildResolutionForRatio(int i, Rational rational, boolean z) {
        int denominator = (rational.getDenominator() * i) / rational.getNumerator();
        if (z) {
            return new Size(denominator, i);
        }
        return new Size(i, denominator);
    }

    private static byte[] transformByteArray(byte[] bArr, Rect rect, int i, boolean z) throws IOException {
        Bitmap bitmap;
        Bitmap bitmap2;
        Stopwatch stopwatch = new Stopwatch("transform");
        if (rect != null) {
            BitmapRegionDecoder newInstance = BitmapRegionDecoder.newInstance(bArr, 0, bArr.length, false);
            bitmap = newInstance.decodeRegion(rect, new BitmapFactory.Options());
            newInstance.recycle();
            stopwatch.split("crop");
        } else {
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
        }
        if (i != 0 || z) {
            Matrix matrix = new Matrix();
            matrix.postRotate((float) i);
            if (z) {
                matrix.postScale(-1.0f, 1.0f);
                matrix.postTranslate((float) bitmap.getWidth(), 0.0f);
            }
            bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } else {
            bitmap2 = bitmap;
        }
        byte[] jpegBytes = toJpegBytes(bitmap2);
        stopwatch.split("transcode");
        bitmap.recycle();
        bitmap2.recycle();
        stopwatch.stop(TAG);
        return jpegBytes;
    }

    private static boolean shouldCropImage(ImageProxy imageProxy) {
        return !new Size(imageProxy.getCropRect().width(), imageProxy.getCropRect().height()).equals(new Size(imageProxy.getWidth(), imageProxy.getHeight()));
    }

    private static byte[] toJpegBytes(Bitmap bitmap) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream)) {
            return byteArrayOutputStream.toByteArray();
        }
        throw new IOException("Failed to compress bitmap.");
    }

    public static boolean isMixedModeSupported(Context context) {
        return getLowestSupportedHardwareLevel(context) != 2;
    }

    public static int getLowestSupportedHardwareLevel(Context context) {
        CameraManager unwrap = CameraManagerCompat.from(context).unwrap();
        try {
            int maxHardwareLevel = maxHardwareLevel();
            for (String str : unwrap.getCameraIdList()) {
                Integer num = (Integer) unwrap.getCameraCharacteristics(str).get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
                if (!(num == null || num.intValue() == 2)) {
                    maxHardwareLevel = smallerHardwareLevel(maxHardwareLevel, num.intValue());
                }
                return 2;
            }
            return maxHardwareLevel;
        } catch (CameraAccessException e) {
            Log.w(TAG, "Failed to enumerate cameras", e);
            return 2;
        }
    }

    private static int maxHardwareLevel() {
        return Build.VERSION.SDK_INT >= 24 ? 3 : 1;
    }

    private static int smallerHardwareLevel(int i, int i2) {
        int[] hardwareInfoOrdering = getHardwareInfoOrdering();
        for (int i3 : hardwareInfoOrdering) {
            if (i == i3 || i2 == i3) {
                return i3;
            }
        }
        return 2;
    }

    private static int[] getHardwareInfoOrdering() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            return CAMERA_HARDWARE_LEVEL_ORDERING_28;
        }
        if (i >= 24) {
            return CAMERA_HARDWARE_LEVEL_ORDERING_24;
        }
        return CAMERA_HARDWARE_LEVEL_ORDERING;
    }

    /* loaded from: classes4.dex */
    public static class ImageResult {
        private final byte[] data;
        private final int height;
        private final int width;

        public ImageResult(byte[] bArr, int i, int i2) {
            this.data = bArr;
            this.width = i;
            this.height = i2;
        }

        public byte[] getData() {
            return this.data;
        }

        public int getWidth() {
            return this.width;
        }

        public int getHeight() {
            return this.height;
        }
    }
}
