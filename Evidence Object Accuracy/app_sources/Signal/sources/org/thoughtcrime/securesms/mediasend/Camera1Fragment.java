package org.thoughtcrime.securesms.mediasend;

import android.animation.Animator;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Predicate;
import j$.util.Optional;
import java.io.ByteArrayOutputStream;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.mediasend.Camera1Controller;
import org.thoughtcrime.securesms.mediasend.CameraFragment;
import org.thoughtcrime.securesms.mediasend.RotationListener;
import org.thoughtcrime.securesms.mediasend.v2.MediaAnimations;
import org.thoughtcrime.securesms.mediasend.v2.MediaCountIndicatorButton;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.viewer.page.StoryDisplay;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class Camera1Fragment extends LoggingFragment implements CameraFragment, TextureView.SurfaceTextureListener, Camera1Controller.EventListener {
    private static final String TAG = Log.tag(Camera1Fragment.class);
    private Camera1Controller camera;
    private TextureView cameraPreview;
    private View captureButton;
    private CameraFragment.Controller controller;
    private ViewGroup controlsContainer;
    private ImageButton flipButton;
    private final GestureDetector.OnGestureListener flipGestureListener = new GestureDetector.SimpleOnGestureListener() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment.4
        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
        public boolean onDoubleTap(MotionEvent motionEvent) {
            Camera1Fragment.this.flipButton.performClick();
            return true;
        }
    };
    private boolean isMediaSelected;
    private boolean isThumbAvailable;
    private Disposable mostRecentItemDisposable = Disposable.CC.disposed();
    private OrderEnforcer<Stage> orderEnforcer;
    private Camera1Controller.Properties properties;
    private RotationListener rotationListener;
    private Disposable rotationListenerDisposable;

    /* loaded from: classes4.dex */
    public enum Stage {
        SURFACE_AVAILABLE,
        CAMERA_PROPERTIES_AVAILABLE
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public static Camera1Fragment newInstance() {
        return new Camera1Fragment();
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getActivity() instanceof CameraFragment.Controller) {
            this.controller = (CameraFragment.Controller) getActivity();
        } else if (getParentFragment() instanceof CameraFragment.Controller) {
            this.controller = (CameraFragment.Controller) getParentFragment();
        }
        if (this.controller != null) {
            Display defaultDisplay = ServiceUtil.getWindowManager(getActivity()).getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            this.camera = new Camera1Controller(TextSecurePreferences.getDirectCaptureCameraId(getContext()), point.x, point.y, this);
            this.orderEnforcer = new OrderEnforcer<>(Stage.SURFACE_AVAILABLE, Stage.CAMERA_PROPERTIES_AVAILABLE);
            return;
        }
        throw new IllegalStateException("Parent must implement controller interface.");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.camera_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        requireActivity().setRequestedOrientation(4);
        this.rotationListener = new RotationListener(requireContext());
        this.cameraPreview = (TextureView) view.findViewById(R.id.camera_preview);
        this.controlsContainer = (ViewGroup) view.findViewById(R.id.camera_controls_container);
        View findViewById = view.findViewById(R.id.camera_preview_parent);
        onOrientationChanged(getResources().getConfiguration().orientation);
        this.cameraPreview.setSurfaceTextureListener(this);
        this.cameraPreview.setOnTouchListener(new View.OnTouchListener(new GestureDetector(this.flipGestureListener)) { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda10
            public final /* synthetic */ GestureDetector f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view2, MotionEvent motionEvent) {
                return Camera1Fragment.$r8$lambda$VlK9aDjNaq9UUa9ObVPnFZ19mB4(this.f$0, view2, motionEvent);
            }
        });
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener(findViewById) { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda11
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                Camera1Fragment.$r8$lambda$OCFyC2tJQYGcL1J6CkNJcAFRzIY(Camera1Fragment.this, this.f$1, view2, i, i2, i3, i4, i5, i6, i7, i8);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view, View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        float f = (float) (i3 - i);
        float min = Math.min((1.0f / CameraFragment.CC.getAspectRatioForOrientation(getResources().getConfiguration().orientation)) * f, (float) (i4 - i2));
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        int i9 = (int) min;
        if (layoutParams.height != i9) {
            layoutParams.width = (int) f;
            layoutParams.height = i9;
            view.setLayoutParams(layoutParams);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.camera.initialize();
        if (this.cameraPreview.isAvailable()) {
            this.orderEnforcer.markCompleted(Stage.SURFACE_AVAILABLE);
        }
        if (this.properties != null) {
            this.orderEnforcer.markCompleted(Stage.CAMERA_PROPERTIES_AVAILABLE);
        }
        this.orderEnforcer.run(Stage.SURFACE_AVAILABLE, new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda7
            @Override // java.lang.Runnable
            public final void run() {
                Camera1Fragment.$r8$lambda$gysX7sRNmuTPhpbEE8hUBzj8YsE(Camera1Fragment.this);
            }
        });
        this.rotationListenerDisposable = this.rotationListener.getObservable().distinctUntilChanged().filter(new Predicate() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda8
            @Override // io.reactivex.rxjava3.functions.Predicate
            public final boolean test(Object obj) {
                return Camera1Fragment.$r8$lambda$rm2_S7aeUHlLHNzgHx7QfN2H4E0((RotationListener.Rotation) obj);
            }
        }).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda9
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                Camera1Fragment.m2066$r8$lambda$zYj9J3KQkmzwZJEbsjfPEfCSqU(Camera1Fragment.this, (RotationListener.Rotation) obj);
            }
        });
        this.orderEnforcer.run(Stage.CAMERA_PROPERTIES_AVAILABLE, new Camera1Fragment$$ExternalSyntheticLambda1(this));
        requireActivity().setRequestedOrientation(4);
    }

    public /* synthetic */ void lambda$onResume$2() {
        this.camera.linkSurface(this.cameraPreview.getSurfaceTexture());
    }

    public static /* synthetic */ boolean lambda$onResume$3(RotationListener.Rotation rotation) throws Throwable {
        return rotation != RotationListener.Rotation.ROTATION_180;
    }

    public /* synthetic */ void lambda$onResume$5(RotationListener.Rotation rotation) throws Throwable {
        this.orderEnforcer.run(Stage.SURFACE_AVAILABLE, new Runnable(rotation) { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda13
            public final /* synthetic */ RotationListener.Rotation f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                Camera1Fragment.m2063$r8$lambda$4LaqmJgHjiGf4uxlNZoZqkXAAY(Camera1Fragment.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onResume$4(RotationListener.Rotation rotation) {
        this.camera.setScreenRotation(rotation.getSurfaceRotation());
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        this.rotationListenerDisposable.dispose();
        this.camera.release();
        this.orderEnforcer.reset();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        this.mostRecentItemDisposable.dispose();
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        requireActivity().setRequestedOrientation(-1);
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment
    public void fadeOutControls(final Runnable runnable) {
        this.controlsContainer.setEnabled(false);
        this.controlsContainer.animate().setInterpolator(MediaAnimations.getInterpolator()).setDuration(250).alpha(0.0f).setListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment.1
            @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                Camera1Fragment.this.controlsContainer.setEnabled(true);
                runnable.run();
            }
        });
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment
    public void fadeInControls() {
        this.controlsContainer.setEnabled(false);
        this.controlsContainer.animate().setInterpolator(MediaAnimations.getInterpolator()).setDuration(250).alpha(1.0f).setListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment.2
            @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                Camera1Fragment.this.controlsContainer.setEnabled(true);
            }
        });
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        onOrientationChanged(configuration.orientation);
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        Log.d(TAG, "onSurfaceTextureAvailable");
        this.orderEnforcer.markCompleted(Stage.SURFACE_AVAILABLE);
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        this.orderEnforcer.run(Stage.CAMERA_PROPERTIES_AVAILABLE, new Camera1Fragment$$ExternalSyntheticLambda1(this));
    }

    @Override // org.thoughtcrime.securesms.mediasend.Camera1Controller.EventListener
    public void onPropertiesAvailable(Camera1Controller.Properties properties) {
        String str = TAG;
        Log.d(str, "Got camera properties: " + properties);
        this.properties = properties;
        OrderEnforcer<Stage> orderEnforcer = this.orderEnforcer;
        Stage stage = Stage.CAMERA_PROPERTIES_AVAILABLE;
        orderEnforcer.run(stage, new Camera1Fragment$$ExternalSyntheticLambda1(this));
        this.orderEnforcer.markCompleted(stage);
    }

    @Override // org.thoughtcrime.securesms.mediasend.Camera1Controller.EventListener
    public void onCameraUnavailable() {
        this.controller.onCameraError();
    }

    private void presentRecentItemThumbnail(Media media) {
        ImageView imageView = (ImageView) this.controlsContainer.findViewById(R.id.camera_gallery_button);
        boolean z = false;
        if (media != null) {
            imageView.setVisibility(0);
            Glide.with(this).load((Object) new DecryptableStreamUriLoader.DecryptableUri(media.getUri())).centerCrop().into(imageView);
        } else {
            imageView.setVisibility(8);
            imageView.setImageResource(0);
        }
        if (media != null) {
            z = true;
        }
        this.isThumbAvailable = z;
        updateGalleryVisibility();
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraFragment
    public void presentHud(int i) {
        MediaCountIndicatorButton mediaCountIndicatorButton = (MediaCountIndicatorButton) this.controlsContainer.findViewById(R.id.camera_review_button);
        View findViewById = this.controlsContainer.findViewById(R.id.camera_gallery_button_background);
        boolean z = false;
        if (i > 0) {
            mediaCountIndicatorButton.setVisibility(0);
            mediaCountIndicatorButton.setCount(i);
            findViewById.setVisibility(8);
        } else {
            mediaCountIndicatorButton.setVisibility(8);
            findViewById.setVisibility(0);
        }
        if (i > 0) {
            z = true;
        }
        this.isMediaSelected = z;
        updateGalleryVisibility();
    }

    private void updateGalleryVisibility() {
        View findViewById = this.controlsContainer.findViewById(R.id.camera_gallery_button_background);
        if (this.isMediaSelected || !this.isThumbAvailable) {
            findViewById.setVisibility(8);
        } else {
            findViewById.setVisibility(0);
        }
    }

    private void initControls() {
        this.flipButton = (ImageButton) requireView().findViewById(R.id.camera_flip_button);
        this.captureButton = requireView().findViewById(R.id.camera_capture_button);
        View findViewById = requireView().findViewById(R.id.camera_gallery_button);
        View findViewById2 = requireView().findViewById(R.id.camera_review_button);
        View findViewById3 = requireView().findViewById(R.id.toggle_spacer);
        this.mostRecentItemDisposable.dispose();
        this.mostRecentItemDisposable = this.controller.getMostRecentMediaItem().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                Camera1Fragment.$r8$lambda$askn7Rr9CiAfisZPLtmGsRcXrxE(Camera1Fragment.this, (Optional) obj);
            }
        });
        if (findViewById3 != null) {
            if (!Stories.isFeatureEnabled()) {
                findViewById3.setVisibility(8);
            } else if (StoryDisplay.Companion.getStoryDisplay((float) getResources().getDisplayMetrics().widthPixels, (float) getResources().getDisplayMetrics().heightPixels) == StoryDisplay.SMALL) {
                findViewById3.setVisibility(0);
            } else {
                findViewById3.setVisibility(8);
            }
        }
        this.captureButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Camera1Fragment.m2065$r8$lambda$yhpavI8zEd7ORtthChjUw9V3s4(Camera1Fragment.this, view);
            }
        });
        this.orderEnforcer.run(Stage.CAMERA_PROPERTIES_AVAILABLE, new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                Camera1Fragment.m2064$r8$lambda$Ixj0lfIGhoXiqKXuB2V_Znb2J0(Camera1Fragment.this);
            }
        });
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Camera1Fragment.$r8$lambda$nGwoyubeV1olbMnchEeOpw0q180(Camera1Fragment.this, view);
            }
        });
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Camera1Fragment.$r8$lambda$7TChgzCpG7IWzxYgtwy_KjsJDn4(Camera1Fragment.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$initControls$6(Optional optional) throws Throwable {
        presentRecentItemThumbnail((Media) optional.orElse(null));
    }

    public /* synthetic */ void lambda$initControls$7(View view) {
        this.captureButton.setEnabled(false);
        onCaptureClicked();
    }

    public /* synthetic */ void lambda$initControls$9() {
        int i = 8;
        if (this.properties.getCameraCount() > 1) {
            ImageButton imageButton = this.flipButton;
            if (this.properties.getCameraCount() > 1) {
                i = 0;
            }
            imageButton.setVisibility(i);
            this.flipButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Camera1Fragment.$r8$lambda$uWa18_Kb9WCuf2mifRKKgH2VB1Q(Camera1Fragment.this, view);
                }
            });
            return;
        }
        this.flipButton.setVisibility(8);
    }

    public /* synthetic */ void lambda$initControls$8(View view) {
        TextSecurePreferences.setDirectCaptureCameraId(getContext(), this.camera.flip());
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        rotateAnimation.setDuration(200);
        rotateAnimation.setInterpolator(new DecelerateInterpolator());
        this.flipButton.startAnimation(rotateAnimation);
    }

    public /* synthetic */ void lambda$initControls$10(View view) {
        this.controller.onGalleryClicked();
    }

    public /* synthetic */ void lambda$initControls$11(View view) {
        this.controller.onCameraCountButtonClicked();
    }

    private void onCaptureClicked() {
        this.orderEnforcer.reset();
        this.camera.capture(new Camera1Controller.CaptureCallback(new Stopwatch("Capture")) { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment$$ExternalSyntheticLambda12
            public final /* synthetic */ Stopwatch f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.mediasend.Camera1Controller.CaptureCallback
            public final void onCaptureAvailable(byte[] bArr, boolean z) {
                Camera1Fragment.$r8$lambda$qlhpVf8lkBF_RvZ5Xf0J4MErFjc(Camera1Fragment.this, this.f$1, bArr, z);
            }
        });
    }

    public /* synthetic */ void lambda$onCaptureClicked$12(final Stopwatch stopwatch, byte[] bArr, boolean z) {
        Transformation<Bitmap> transformation;
        stopwatch.split("captured");
        if (z) {
            transformation = new MultiTransformation<>(new CenterCrop(), new FlipTransformation());
        } else {
            transformation = new CenterCrop();
        }
        GlideApp.with(this).asBitmap().load(bArr).transform(transformation).override(this.cameraPreview.getWidth(), this.cameraPreview.getHeight()).into((GlideRequest<Bitmap>) new SimpleTarget<Bitmap>() { // from class: org.thoughtcrime.securesms.mediasend.Camera1Fragment.3
            @Override // com.bumptech.glide.request.target.Target
            public /* bridge */ /* synthetic */ void onResourceReady(Object obj, Transition transition) {
                onResourceReady((Bitmap) obj, (Transition<? super Bitmap>) transition);
            }

            public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                stopwatch.split("transform");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
                stopwatch.split("compressed");
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                stopwatch.split("bytes");
                stopwatch.stop(Camera1Fragment.TAG);
                Camera1Fragment.this.controller.onImageCaptured(byteArray, bitmap.getWidth(), bitmap.getHeight());
            }

            @Override // com.bumptech.glide.request.target.BaseTarget, com.bumptech.glide.request.target.Target
            public void onLoadFailed(Drawable drawable) {
                Camera1Fragment.this.controller.onCameraError();
            }
        });
    }

    private PointF getScaleTransform(float f, float f2, int i, int i2) {
        float f3;
        float min = (float) (isPortrait() ? Math.min(i, i2) : Math.max(i, i2));
        float max = (float) (isPortrait() ? Math.max(i, i2) : Math.min(i, i2));
        float f4 = 1.0f;
        if (min / f > max / f2) {
            f4 = (f2 * (min / max)) / f;
            f3 = 1.0f;
        } else {
            f3 = (f * (max / min)) / f2;
        }
        return new PointF(f4, f3);
    }

    private void onOrientationChanged(int i) {
        int i2 = i == 1 ? R.layout.camera_controls_portrait : R.layout.camera_controls_landscape;
        this.controlsContainer.removeAllViews();
        this.controlsContainer.addView(LayoutInflater.from(getContext()).inflate(i2, this.controlsContainer, false));
        initControls();
    }

    public void updatePreviewScale() {
        PointF scaleTransform = getScaleTransform((float) this.cameraPreview.getWidth(), (float) this.cameraPreview.getHeight(), this.properties.getPreviewWidth(), this.properties.getPreviewHeight());
        Matrix matrix = new Matrix();
        float min = (float) (isPortrait() ? Math.min(this.cameraPreview.getWidth(), this.cameraPreview.getHeight()) : Math.max(this.cameraPreview.getWidth(), this.cameraPreview.getHeight()));
        float max = (float) (isPortrait() ? Math.max(this.cameraPreview.getWidth(), this.cameraPreview.getHeight()) : Math.min(this.cameraPreview.getWidth(), this.cameraPreview.getHeight()));
        matrix.setScale(scaleTransform.x, scaleTransform.y);
        matrix.postTranslate((min - (scaleTransform.x * min)) / 2.0f, (max - (scaleTransform.y * max)) / 2.0f);
        this.cameraPreview.setTransform(matrix);
    }

    private boolean isPortrait() {
        return getResources().getConfiguration().orientation == 1;
    }
}
