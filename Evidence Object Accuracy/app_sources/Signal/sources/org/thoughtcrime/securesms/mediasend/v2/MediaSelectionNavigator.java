package org.thoughtcrime.securesms.mediasend.v2;

import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: MediaSelectionNavigator.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \f2\u00020\u0001:\u0001\fB\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\n\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\u000b\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionNavigator;", "", "toCamera", "", "toGallery", "(II)V", "goToCamera", "", "navController", "Landroidx/navigation/NavController;", "goToGallery", "goToReview", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaSelectionNavigator {
    public static final Companion Companion = new Companion(null);
    private final int toCamera;
    private final int toGallery;

    public MediaSelectionNavigator() {
        this(0, 0, 3, null);
    }

    public MediaSelectionNavigator(int i, int i2) {
        this.toCamera = i;
        this.toGallery = i2;
    }

    public /* synthetic */ MediaSelectionNavigator(int i, int i2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this((i3 & 1) != 0 ? -1 : i, (i3 & 2) != 0 ? -1 : i2);
    }

    public final void goToReview(NavController navController) {
        Intrinsics.checkNotNullParameter(navController, "navController");
        navController.popBackStack(R.id.mediaReviewFragment, false);
    }

    public final void goToCamera(NavController navController) {
        Intrinsics.checkNotNullParameter(navController, "navController");
        int i = this.toCamera;
        if (i != -1) {
            SafeNavigation.safeNavigate(navController, i);
        }
    }

    public final void goToGallery(NavController navController) {
        Intrinsics.checkNotNullParameter(navController, "navController");
        int i = this.toGallery;
        if (i != -1) {
            SafeNavigation.safeNavigate(navController, i);
        }
    }

    /* compiled from: MediaSelectionNavigator.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007J\u0018\u0010\b\u001a\u00020\u0004*\u00020\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionNavigator$Companion;", "", "()V", "requestPermissionsForCamera", "", "Landroidx/fragment/app/Fragment;", "onGranted", "Lkotlin/Function0;", "requestPermissionsForGallery", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final void requestPermissionsForCamera(Fragment fragment, Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter(fragment, "<this>");
            Intrinsics.checkNotNullParameter(function0, "onGranted");
            Permissions.with(fragment).request("android.permission.CAMERA").ifNecessary().withRationaleDialog(fragment.getString(R.string.ConversationActivity_to_capture_photos_and_video_allow_signal_access_to_the_camera), R.drawable.ic_camera_24).withPermanentDenialDialog(fragment.getString(R.string.ConversationActivity_signal_needs_the_camera_permission_to_take_photos_or_video)).onAllGranted(new MediaSelectionNavigator$Companion$$ExternalSyntheticLambda2(function0)).onAnyDenied(new MediaSelectionNavigator$Companion$$ExternalSyntheticLambda3(fragment)).execute();
        }

        /* renamed from: requestPermissionsForCamera$lambda-0 */
        public static final void m2100requestPermissionsForCamera$lambda0(Function0 function0) {
            Intrinsics.checkNotNullParameter(function0, "$tmp0");
            function0.invoke();
        }

        /* renamed from: requestPermissionsForCamera$lambda-1 */
        public static final void m2101requestPermissionsForCamera$lambda1(Fragment fragment) {
            Intrinsics.checkNotNullParameter(fragment, "$this_requestPermissionsForCamera");
            Toast.makeText(fragment.requireContext(), (int) R.string.ConversationActivity_signal_needs_camera_permissions_to_take_photos_or_video, 1).show();
        }

        public final void requestPermissionsForGallery(Fragment fragment, Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter(fragment, "<this>");
            Intrinsics.checkNotNullParameter(function0, "onGranted");
            Permissions.with(fragment).request("android.permission.READ_EXTERNAL_STORAGE").ifNecessary().withPermanentDenialDialog(fragment.getString(R.string.AttachmentKeyboard_Signal_needs_permission_to_show_your_photos_and_videos)).onAllGranted(new MediaSelectionNavigator$Companion$$ExternalSyntheticLambda0(function0)).onAnyDenied(new MediaSelectionNavigator$Companion$$ExternalSyntheticLambda1(fragment)).execute();
        }

        /* renamed from: requestPermissionsForGallery$lambda-2 */
        public static final void m2102requestPermissionsForGallery$lambda2(Function0 function0) {
            Intrinsics.checkNotNullParameter(function0, "$tmp0");
            function0.invoke();
        }

        /* renamed from: requestPermissionsForGallery$lambda-3 */
        public static final void m2103requestPermissionsForGallery$lambda3(Fragment fragment) {
            Intrinsics.checkNotNullParameter(fragment, "$this_requestPermissionsForGallery");
            Toast.makeText(fragment.requireContext(), (int) R.string.AttachmentKeyboard_Signal_needs_permission_to_show_your_photos_and_videos, 1).show();
        }
    }
}
