package org.thoughtcrime.securesms.mediasend.v2;

import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import j$.util.Optional;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.mediasend.Media;

/* compiled from: MediaBuilder.kt */
@Metadata(bv = {}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0001\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\n\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\u00062\b\b\u0002\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u000f\u001a\u00020\r2\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\u00102\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00040\u00102\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0010¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaBuilder;", "", "Landroid/net/Uri;", "uri", "", "mimeType", "", "date", "", "width", "height", MediaPreviewActivity.SIZE_EXTRA, "duration", "", "borderless", "videoGif", "j$/util/Optional", "bucketId", MediaPreviewActivity.CAPTION_EXTRA, "Lorg/thoughtcrime/securesms/database/AttachmentDatabase$TransformProperties;", "transformProperties", "Lorg/thoughtcrime/securesms/mediasend/Media;", "buildMedia", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class MediaBuilder {
    public static final MediaBuilder INSTANCE = new MediaBuilder();

    private MediaBuilder() {
    }

    public static /* synthetic */ Media buildMedia$default(MediaBuilder mediaBuilder, Uri uri, String str, long j, int i, int i2, long j2, long j3, boolean z, boolean z2, Optional optional, Optional optional2, Optional optional3, int i3, Object obj) {
        Optional optional4;
        Optional optional5;
        Optional optional6;
        String str2 = (i3 & 2) != 0 ? "" : str;
        long j4 = 0;
        long j5 = (i3 & 4) != 0 ? 0 : j;
        boolean z3 = false;
        int i4 = (i3 & 8) != 0 ? 0 : i;
        int i5 = (i3 & 16) != 0 ? 0 : i2;
        long j6 = (i3 & 32) != 0 ? 0 : j2;
        if ((i3 & 64) == 0) {
            j4 = j3;
        }
        boolean z4 = (i3 & 128) != 0 ? false : z;
        if ((i3 & 256) == 0) {
            z3 = z2;
        }
        if ((i3 & 512) != 0) {
            optional4 = Optional.empty();
            Intrinsics.checkNotNullExpressionValue(optional4, "empty()");
        } else {
            optional4 = optional;
        }
        if ((i3 & 1024) != 0) {
            optional5 = Optional.empty();
            Intrinsics.checkNotNullExpressionValue(optional5, "empty()");
        } else {
            optional5 = optional2;
        }
        if ((i3 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0) {
            optional6 = Optional.empty();
            Intrinsics.checkNotNullExpressionValue(optional6, "empty()");
        } else {
            optional6 = optional3;
        }
        return mediaBuilder.buildMedia(uri, str2, j5, i4, i5, j6, j4, z4, z3, optional4, optional5, optional6);
    }

    public final Media buildMedia(Uri uri, String str, long j, int i, int i2, long j2, long j3, boolean z, boolean z2, Optional<String> optional, Optional<String> optional2, Optional<AttachmentDatabase.TransformProperties> optional3) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        Intrinsics.checkNotNullParameter(str, "mimeType");
        Intrinsics.checkNotNullParameter(optional, "bucketId");
        Intrinsics.checkNotNullParameter(optional2, MediaPreviewActivity.CAPTION_EXTRA);
        Intrinsics.checkNotNullParameter(optional3, "transformProperties");
        return new Media(uri, str, j, i, i2, j2, j3, z, z2, optional, optional2, optional3);
    }
}
