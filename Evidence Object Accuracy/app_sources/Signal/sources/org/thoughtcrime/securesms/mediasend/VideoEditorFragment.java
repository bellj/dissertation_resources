package org.thoughtcrime.securesms.mediasend;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.scribbles.VideoEditorHud;
import org.thoughtcrime.securesms.util.Throttler;
import org.thoughtcrime.securesms.video.VideoBitRateCalculator;
import org.thoughtcrime.securesms.video.VideoPlayer;

/* loaded from: classes4.dex */
public class VideoEditorFragment extends Fragment implements VideoEditorHud.EventListener, MediaSendPageFragment {
    private static final String KEY_IS_VIDEO_GIF;
    private static final String KEY_MAX_DURATION;
    private static final String KEY_MAX_OUTPUT;
    private static final String KEY_MAX_SEND;
    private static final String KEY_URI;
    private static final String TAG = Log.tag(VideoEditorFragment.class);
    private Controller controller;
    private Data data = new Data();
    private final Handler handler = new Handler(Looper.getMainLooper());
    private VideoEditorHud hud;
    private boolean isInEdit;
    private boolean isVideoGif;
    private long maxVideoDurationUs;
    private VideoPlayer player;
    private Runnable updatePosition;
    private Uri uri;
    private final Throttler videoScanThrottle = new Throttler(150);
    private boolean wasPlayingBeforeEdit;

    /* loaded from: classes4.dex */
    public interface Controller {
        void onPlayerError();

        void onPlayerReady();

        void onTouchEventsNeeded(boolean z);

        void onVideoBeginEdit(Uri uri);

        void onVideoEndEdit(Uri uri);
    }

    public static VideoEditorFragment newInstance(Uri uri, long j, long j2, boolean z, long j3) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_URI, uri);
        bundle.putLong(KEY_MAX_OUTPUT, j);
        bundle.putLong(KEY_MAX_SEND, j2);
        bundle.putBoolean("is_video_gif", z);
        bundle.putLong(KEY_MAX_DURATION, j3);
        VideoEditorFragment videoEditorFragment = new VideoEditorFragment();
        videoEditorFragment.setArguments(bundle);
        videoEditorFragment.setUri(uri);
        return videoEditorFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getActivity() instanceof Controller) {
            this.controller = (Controller) getActivity();
        } else if (getParentFragment() instanceof Controller) {
            this.controller = (Controller) getParentFragment();
        } else {
            throw new IllegalStateException("Parent must implement Controller interface.");
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.mediasend_video_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.player = (VideoPlayer) view.findViewById(R.id.video_player);
        this.uri = (Uri) requireArguments().getParcelable(KEY_URI);
        this.isVideoGif = requireArguments().getBoolean("is_video_gif");
        this.maxVideoDurationUs = TimeUnit.MILLISECONDS.toMicros(requireArguments().getLong(KEY_MAX_DURATION));
        long j = requireArguments().getLong(KEY_MAX_OUTPUT);
        long j2 = requireArguments().getLong(KEY_MAX_SEND);
        VideoSlide videoSlide = new VideoSlide(requireContext(), this.uri, 0, this.isVideoGif);
        boolean z = this.isVideoGif;
        this.player.setWindow(requireActivity().getWindow());
        this.player.setVideoSource(videoSlide, z, TAG);
        if (videoSlide.isVideoGif()) {
            this.player.setPlayerCallback(new VideoPlayer.PlayerCallback() { // from class: org.thoughtcrime.securesms.mediasend.VideoEditorFragment.1
                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public /* synthetic */ void onReady() {
                    VideoPlayer.PlayerCallback.CC.$default$onReady(this);
                }

                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onStopped() {
                }

                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onPlaying() {
                    VideoEditorFragment.this.controller.onPlayerReady();
                }

                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onError() {
                    VideoEditorFragment.this.controller.onPlayerError();
                }
            });
            this.player.hideControls();
            this.player.loopForever();
        } else if (MediaConstraints.isVideoTranscodeAvailable()) {
            VideoEditorHud videoEditorHud = (VideoEditorHud) view.findViewById(R.id.video_editor_hud);
            this.hud = videoEditorHud;
            videoEditorHud.setEventListener(this);
            clampToMaxVideoDuration(this.data, true);
            updateHud(this.data);
            Data data = this.data;
            if (data.durationEdited) {
                this.player.clip(data.startTimeUs, data.endTimeUs, z);
            }
            try {
                this.hud.setVideoSource(videoSlide, new VideoBitRateCalculator(j), j2);
                this.hud.setVisibility(0);
                startPositionUpdates();
            } catch (IOException e) {
                Log.w(TAG, e);
            }
            this.player.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.VideoEditorFragment$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    VideoEditorFragment.$r8$lambda$2IIw5ftOVUpAb_5IDAqPDaBogTU(VideoEditorFragment.this, view2);
                }
            });
            this.player.setPlayerCallback(new VideoPlayer.PlayerCallback() { // from class: org.thoughtcrime.securesms.mediasend.VideoEditorFragment.2
                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onReady() {
                    VideoEditorFragment.this.controller.onPlayerReady();
                }

                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onPlaying() {
                    VideoEditorFragment.this.hud.fadePlayButton();
                }

                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onStopped() {
                    VideoEditorFragment.this.hud.showPlayButton();
                }

                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onError() {
                    VideoEditorFragment.this.controller.onPlayerError();
                }
            });
        }
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        this.player.pause();
        this.hud.showPlayButton();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        VideoPlayer videoPlayer = this.player;
        if (videoPlayer != null) {
            videoPlayer.cleanup();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        notifyHidden();
        stopPositionUpdates();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        startPositionUpdates();
        VideoPlayer videoPlayer = this.player;
        if (videoPlayer != null && this.isVideoGif) {
            videoPlayer.play();
        }
    }

    private void startPositionUpdates() {
        if (this.hud != null && Build.VERSION.SDK_INT >= 23) {
            stopPositionUpdates();
            AnonymousClass3 r0 = new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.VideoEditorFragment.3
                @Override // java.lang.Runnable
                public void run() {
                    VideoEditorFragment.this.hud.setPosition(VideoEditorFragment.this.player.getPlaybackPositionUs());
                    VideoEditorFragment.this.handler.postDelayed(this, 100);
                }
            };
            this.updatePosition = r0;
            this.handler.post(r0);
        }
    }

    private void stopPositionUpdates() {
        this.handler.removeCallbacks(this.updatePosition);
    }

    @Override // androidx.fragment.app.Fragment
    public void onHiddenChanged(boolean z) {
        if (z) {
            notifyHidden();
        }
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public void setUri(Uri uri) {
        this.uri = uri;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public Uri getUri() {
        return this.uri;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public View getPlaybackControls() {
        VideoPlayer videoPlayer;
        VideoEditorHud videoEditorHud = this.hud;
        if ((videoEditorHud == null || videoEditorHud.getVisibility() != 0) && !this.isVideoGif && (videoPlayer = this.player) != null) {
            return videoPlayer.getControlView();
        }
        return null;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public Object saveState() {
        return this.data;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public void restoreState(Object obj) {
        if (obj instanceof Data) {
            Data data = (Data) obj;
            this.data = data;
            if (Build.VERSION.SDK_INT >= 23) {
                updateHud(data);
                return;
            }
            return;
        }
        String str = TAG;
        Log.w(str, "Received a bad saved state. Received class: " + obj.getClass().getName());
    }

    private void updateHud(Data data) {
        VideoEditorHud videoEditorHud = this.hud;
        if (videoEditorHud != null) {
            long j = data.totalDurationUs;
            if (j > 0 && data.durationEdited) {
                videoEditorHud.setDurationRange(j, data.startTimeUs, data.endTimeUs);
            }
        }
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public void notifyHidden() {
        pausePlayback();
    }

    public void pausePlayback() {
        VideoPlayer videoPlayer = this.player;
        if (videoPlayer != null) {
            videoPlayer.pause();
            VideoEditorHud videoEditorHud = this.hud;
            if (videoEditorHud != null) {
                videoEditorHud.showPlayButton();
            }
        }
    }

    @Override // org.thoughtcrime.securesms.scribbles.VideoEditorHud.EventListener
    public void onEditVideoDuration(long j, long j2, long j3, boolean z, boolean z2) {
        this.controller.onTouchEventsNeeded(!z2);
        VideoEditorHud videoEditorHud = this.hud;
        if (videoEditorHud != null) {
            videoEditorHud.hidePlayButton();
        }
        long max = Math.max(j2, 0L);
        Data data = this.data;
        boolean z3 = data.durationEdited;
        boolean z4 = max > 0 || j3 < j;
        boolean z5 = data.endTimeUs != j3;
        data.durationEdited = z4;
        data.totalDurationUs = j;
        data.startTimeUs = max;
        data.endTimeUs = j3;
        clampToMaxVideoDuration(data, !z5);
        if (z2) {
            this.isInEdit = false;
            this.videoScanThrottle.clear();
        } else if (!this.isInEdit) {
            this.isInEdit = true;
            this.wasPlayingBeforeEdit = this.player.isPlaying();
        }
        this.videoScanThrottle.publish(new Runnable(z2, z, max, j3, z4) { // from class: org.thoughtcrime.securesms.mediasend.VideoEditorFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$1;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ long f$3;
            public final /* synthetic */ long f$4;
            public final /* synthetic */ boolean f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r6;
                this.f$5 = r8;
            }

            @Override // java.lang.Runnable
            public final void run() {
                VideoEditorFragment.m2084$r8$lambda$aG5BQaGTKA2sY6Sdg66RW6bZn4(VideoEditorFragment.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        });
        if (!z3 && z4) {
            this.controller.onVideoBeginEdit(this.uri);
        }
        if (z2) {
            this.controller.onVideoEndEdit(this.uri);
        }
    }

    public /* synthetic */ void lambda$onEditVideoDuration$1(boolean z, boolean z2, long j, long j2, boolean z3) {
        this.player.pause();
        if (!z) {
            this.player.removeClip(false);
        }
        this.player.setPlaybackPosition((z2 || z) ? j / 1000 : j2 / 1000);
        if (z) {
            if (z3) {
                this.player.clip(j, j2, this.wasPlayingBeforeEdit);
            } else {
                this.player.removeClip(this.wasPlayingBeforeEdit);
            }
            if (!this.wasPlayingBeforeEdit) {
                this.hud.showPlayButton();
            }
        }
    }

    @Override // org.thoughtcrime.securesms.scribbles.VideoEditorHud.EventListener
    public void onPlay() {
        this.player.play();
    }

    @Override // org.thoughtcrime.securesms.scribbles.VideoEditorHud.EventListener
    public void onSeek(long j, boolean z) {
        if (z) {
            this.videoScanThrottle.clear();
        }
        this.videoScanThrottle.publish(new Runnable(j) { // from class: org.thoughtcrime.securesms.mediasend.VideoEditorFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                VideoEditorFragment.$r8$lambda$nzPDxfvWmcxiGepbD4ktjaG5V1U(VideoEditorFragment.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onSeek$2(long j) {
        this.player.pause();
        this.player.setPlaybackPosition(j);
    }

    private void clampToMaxVideoDuration(Data data, boolean z) {
        if (MediaConstraints.isVideoTranscodeAvailable()) {
            long j = data.endTimeUs;
            long j2 = data.startTimeUs;
            long j3 = this.maxVideoDurationUs;
            if (j - j2 > j3) {
                data.durationEdited = true;
                if (z) {
                    data.endTimeUs = j2 + j3;
                } else {
                    data.startTimeUs = j - j3;
                }
                updateHud(data);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class Data {
        boolean durationEdited;
        long endTimeUs;
        long startTimeUs;
        long totalDurationUs;

        public boolean isDurationEdited() {
            return this.durationEdited;
        }

        public Bundle getBundle() {
            Bundle bundle = new Bundle();
            bundle.putByte("EDITED", this.durationEdited ? (byte) 1 : 0);
            bundle.putLong("TOTAL", this.totalDurationUs);
            bundle.putLong("START", this.startTimeUs);
            bundle.putLong("END", this.endTimeUs);
            return bundle;
        }

        public static Data fromBundle(Bundle bundle) {
            Data data = new Data();
            boolean z = true;
            if (bundle.getByte("EDITED") != 1) {
                z = false;
            }
            data.durationEdited = z;
            data.totalDurationUs = bundle.getLong("TOTAL");
            data.startTimeUs = bundle.getLong("START");
            data.endTimeUs = bundle.getLong("END");
            return data;
        }
    }
}
