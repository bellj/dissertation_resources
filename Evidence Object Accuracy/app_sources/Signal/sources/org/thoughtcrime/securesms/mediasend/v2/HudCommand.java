package org.thoughtcrime.securesms.mediasend.v2;

import android.view.KeyEvent;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: HudCommand.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\n\u0003\u0004\u0005\u0006\u0007\b\t\n\u000b\fB\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\n\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "", "()V", "CloseEmojiSearch", "EmojiInsert", "EmojiKeyEvent", "GoToCapture", "GoToText", "OpenEmojiSearch", "ResumeEntryTransition", "SaveMedia", "StartCropAndRotate", "StartDraw", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$StartDraw;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$StartCropAndRotate;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$SaveMedia;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$GoToText;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$GoToCapture;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$ResumeEntryTransition;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$OpenEmojiSearch;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$CloseEmojiSearch;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$EmojiInsert;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$EmojiKeyEvent;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class HudCommand {
    public /* synthetic */ HudCommand(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    /* compiled from: HudCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$StartDraw;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StartDraw extends HudCommand {
        public static final StartDraw INSTANCE = new StartDraw();

        private StartDraw() {
            super(null);
        }
    }

    private HudCommand() {
    }

    /* compiled from: HudCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$StartCropAndRotate;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StartCropAndRotate extends HudCommand {
        public static final StartCropAndRotate INSTANCE = new StartCropAndRotate();

        private StartCropAndRotate() {
            super(null);
        }
    }

    /* compiled from: HudCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$SaveMedia;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SaveMedia extends HudCommand {
        public static final SaveMedia INSTANCE = new SaveMedia();

        private SaveMedia() {
            super(null);
        }
    }

    /* compiled from: HudCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$GoToText;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GoToText extends HudCommand {
        public static final GoToText INSTANCE = new GoToText();

        private GoToText() {
            super(null);
        }
    }

    /* compiled from: HudCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$GoToCapture;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GoToCapture extends HudCommand {
        public static final GoToCapture INSTANCE = new GoToCapture();

        private GoToCapture() {
            super(null);
        }
    }

    /* compiled from: HudCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$ResumeEntryTransition;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ResumeEntryTransition extends HudCommand {
        public static final ResumeEntryTransition INSTANCE = new ResumeEntryTransition();

        private ResumeEntryTransition() {
            super(null);
        }
    }

    /* compiled from: HudCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$OpenEmojiSearch;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class OpenEmojiSearch extends HudCommand {
        public static final OpenEmojiSearch INSTANCE = new OpenEmojiSearch();

        private OpenEmojiSearch() {
            super(null);
        }
    }

    /* compiled from: HudCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$CloseEmojiSearch;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CloseEmojiSearch extends HudCommand {
        public static final CloseEmojiSearch INSTANCE = new CloseEmojiSearch();

        private CloseEmojiSearch() {
            super(null);
        }
    }

    /* compiled from: HudCommand.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0015\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$EmojiInsert;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "emoji", "", "(Ljava/lang/String;)V", "getEmoji", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class EmojiInsert extends HudCommand {
        private final String emoji;

        public static /* synthetic */ EmojiInsert copy$default(EmojiInsert emojiInsert, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = emojiInsert.emoji;
            }
            return emojiInsert.copy(str);
        }

        public final String component1() {
            return this.emoji;
        }

        public final EmojiInsert copy(String str) {
            return new EmojiInsert(str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof EmojiInsert) && Intrinsics.areEqual(this.emoji, ((EmojiInsert) obj).emoji);
        }

        public int hashCode() {
            String str = this.emoji;
            if (str == null) {
                return 0;
            }
            return str.hashCode();
        }

        public String toString() {
            return "EmojiInsert(emoji=" + this.emoji + ')';
        }

        public EmojiInsert(String str) {
            super(null);
            this.emoji = str;
        }

        public final String getEmoji() {
            return this.emoji;
        }
    }

    /* compiled from: HudCommand.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0015\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand$EmojiKeyEvent;", "Lorg/thoughtcrime/securesms/mediasend/v2/HudCommand;", "keyEvent", "Landroid/view/KeyEvent;", "(Landroid/view/KeyEvent;)V", "getKeyEvent", "()Landroid/view/KeyEvent;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class EmojiKeyEvent extends HudCommand {
        private final KeyEvent keyEvent;

        public static /* synthetic */ EmojiKeyEvent copy$default(EmojiKeyEvent emojiKeyEvent, KeyEvent keyEvent, int i, Object obj) {
            if ((i & 1) != 0) {
                keyEvent = emojiKeyEvent.keyEvent;
            }
            return emojiKeyEvent.copy(keyEvent);
        }

        public final KeyEvent component1() {
            return this.keyEvent;
        }

        public final EmojiKeyEvent copy(KeyEvent keyEvent) {
            return new EmojiKeyEvent(keyEvent);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof EmojiKeyEvent) && Intrinsics.areEqual(this.keyEvent, ((EmojiKeyEvent) obj).keyEvent);
        }

        public int hashCode() {
            KeyEvent keyEvent = this.keyEvent;
            if (keyEvent == null) {
                return 0;
            }
            return keyEvent.hashCode();
        }

        public String toString() {
            return "EmojiKeyEvent(keyEvent=" + this.keyEvent + ')';
        }

        public EmojiKeyEvent(KeyEvent keyEvent) {
            super(null);
            this.keyEvent = keyEvent;
        }

        public final KeyEvent getKeyEvent() {
            return this.keyEvent;
        }
    }
}
