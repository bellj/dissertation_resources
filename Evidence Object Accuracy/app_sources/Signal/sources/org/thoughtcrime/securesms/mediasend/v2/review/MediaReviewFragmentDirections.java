package org.thoughtcrime.securesms.mediasend.v2.review;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.MediaDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class MediaReviewFragmentDirections {
    private MediaReviewFragmentDirections() {
    }

    public static NavDirections actionMediaReviewFragmentToMediaGalleryFragment() {
        return new ActionOnlyNavDirections(R.id.action_mediaReviewFragment_to_mediaGalleryFragment);
    }

    public static NavDirections actionDirectlyToMediaCaptureFragment() {
        return MediaDirections.actionDirectlyToMediaCaptureFragment();
    }

    public static NavDirections actionDirectlyToMediaGalleryFragment() {
        return MediaDirections.actionDirectlyToMediaGalleryFragment();
    }

    public static NavDirections actionDirectlyToMediaReviewFragment() {
        return MediaDirections.actionDirectlyToMediaReviewFragment();
    }

    public static NavDirections actionDirectlyToTextPostCreationFragment() {
        return MediaDirections.actionDirectlyToTextPostCreationFragment();
    }
}
