package org.thoughtcrime.securesms.mediasend;

import android.content.Context;
import android.net.Uri;
import j$.util.Optional;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.mediasend.VideoEditorFragment;
import org.thoughtcrime.securesms.mms.SentMediaQuality;

/* loaded from: classes4.dex */
public final class VideoTrimTransform implements MediaTransform {
    private final VideoEditorFragment.Data data;

    public VideoTrimTransform(VideoEditorFragment.Data data) {
        this.data = data;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaTransform
    public Media transform(Context context, Media media) {
        Uri uri = media.getUri();
        String mimeType = media.getMimeType();
        long date = media.getDate();
        int width = media.getWidth();
        int height = media.getHeight();
        long size = media.getSize();
        long duration = media.getDuration();
        boolean isBorderless = media.isBorderless();
        boolean isVideoGif = media.isVideoGif();
        Optional<String> bucketId = media.getBucketId();
        Optional<String> caption = media.getCaption();
        VideoEditorFragment.Data data = this.data;
        return new Media(uri, mimeType, date, width, height, size, duration, isBorderless, isVideoGif, bucketId, caption, Optional.of(new AttachmentDatabase.TransformProperties(false, data.durationEdited, data.startTimeUs, data.endTimeUs, SentMediaQuality.STANDARD.getCode())));
    }
}
