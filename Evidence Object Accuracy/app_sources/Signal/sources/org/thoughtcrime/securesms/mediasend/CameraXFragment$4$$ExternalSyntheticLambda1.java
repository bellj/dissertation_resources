package org.thoughtcrime.securesms.mediasend;

import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.mediasend.CameraXFragment;
import org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil;
import org.thoughtcrime.securesms.util.Stopwatch;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CameraXFragment$4$$ExternalSyntheticLambda1 implements SimpleTask.ForegroundTask {
    public final /* synthetic */ CameraXFragment.AnonymousClass4 f$0;
    public final /* synthetic */ Stopwatch f$1;

    public /* synthetic */ CameraXFragment$4$$ExternalSyntheticLambda1(CameraXFragment.AnonymousClass4 r1, Stopwatch stopwatch) {
        this.f$0 = r1;
        this.f$1 = stopwatch;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
    public final void run(Object obj) {
        this.f$0.lambda$onCaptureSuccess$1(this.f$1, (CameraXUtil.ImageResult) obj);
    }
}
