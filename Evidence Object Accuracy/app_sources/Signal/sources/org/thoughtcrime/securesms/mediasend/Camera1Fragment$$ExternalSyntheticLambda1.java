package org.thoughtcrime.securesms.mediasend;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Camera1Fragment$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ Camera1Fragment f$0;

    public /* synthetic */ Camera1Fragment$$ExternalSyntheticLambda1(Camera1Fragment camera1Fragment) {
        this.f$0 = camera1Fragment;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.updatePreviewScale();
    }
}
