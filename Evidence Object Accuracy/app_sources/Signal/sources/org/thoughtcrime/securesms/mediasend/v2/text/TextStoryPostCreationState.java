package org.thoughtcrime.securesms.mediasend.v2.text;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.core.graphics.ColorUtils;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.fonts.TextFont;
import org.thoughtcrime.securesms.scribbles.HSVColorSlider;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* compiled from: TextStoryPostCreationState.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\"\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BW\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0003\u0010\f\u001a\u00020\u0005\u0012\b\b\u0002\u0010\r\u001a\u00020\u000e\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010¢\u0006\u0002\u0010\u0011J\t\u0010(\u001a\u00020\u0003HÆ\u0003J\t\u0010)\u001a\u00020\u0005HÆ\u0003J\t\u0010*\u001a\u00020\u0007HÆ\u0003J\t\u0010+\u001a\u00020\tHÆ\u0003J\t\u0010,\u001a\u00020\u000bHÆ\u0003J\t\u0010-\u001a\u00020\u0005HÆ\u0003J\t\u0010.\u001a\u00020\u000eHÆ\u0003J\u000b\u0010/\u001a\u0004\u0018\u00010\u0010HÆ\u0003J[\u00100\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0003\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u000e2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÆ\u0001J\t\u00101\u001a\u00020\u0005HÖ\u0001J\u0013\u00102\u001a\u0002032\b\u00104\u001a\u0004\u0018\u000105HÖ\u0003J\u0010\u00106\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0005H\u0002J\t\u00107\u001a\u00020\u0005HÖ\u0001J\t\u00108\u001a\u00020\u0010HÖ\u0001J\u0019\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\u00020\u00058\u0006X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001eR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u001c\u0010$\u001a\u00020\u00058\u0006X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b%\u0010\u001c\u001a\u0004\b&\u0010\u001eR\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u001e¨\u0006>"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationState;", "Landroid/os/Parcelable;", "body", "", "textColor", "", "textColorStyle", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextColorStyle;", "textAlignment", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextAlignment;", "textFont", "Lorg/thoughtcrime/securesms/fonts/TextFont;", "textScale", "backgroundColor", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "linkPreviewUri", "", "(Ljava/lang/CharSequence;ILorg/thoughtcrime/securesms/mediasend/v2/text/TextColorStyle;Lorg/thoughtcrime/securesms/mediasend/v2/text/TextAlignment;Lorg/thoughtcrime/securesms/fonts/TextFont;ILorg/thoughtcrime/securesms/conversation/colors/ChatColors;Ljava/lang/String;)V", "getBackgroundColor", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "getBody", "()Ljava/lang/CharSequence;", "getLinkPreviewUri", "()Ljava/lang/String;", "getTextAlignment", "()Lorg/thoughtcrime/securesms/mediasend/v2/text/TextAlignment;", "textBackgroundColor", "getTextBackgroundColor$annotations", "()V", "getTextBackgroundColor", "()I", "getTextColor", "getTextColorStyle", "()Lorg/thoughtcrime/securesms/mediasend/v2/text/TextColorStyle;", "getTextFont", "()Lorg/thoughtcrime/securesms/fonts/TextFont;", "textForegroundColor", "getTextForegroundColor$annotations", "getTextForegroundColor", "getTextScale", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "describeContents", "equals", "", "other", "", "getDefaultColorForLightness", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryPostCreationState implements Parcelable {
    public static final Parcelable.Creator<TextStoryPostCreationState> CREATOR = new Creator();
    private final ChatColors backgroundColor;
    private final CharSequence body;
    private final String linkPreviewUri;
    private final TextAlignment textAlignment;
    private final int textBackgroundColor;
    private final int textColor;
    private final TextColorStyle textColorStyle;
    private final TextFont textFont;
    private final int textForegroundColor;
    private final int textScale;

    /* compiled from: TextStoryPostCreationState.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Creator implements Parcelable.Creator<TextStoryPostCreationState> {
        @Override // android.os.Parcelable.Creator
        public final TextStoryPostCreationState createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            return new TextStoryPostCreationState((CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel), parcel.readInt(), TextColorStyle.valueOf(parcel.readString()), TextAlignment.valueOf(parcel.readString()), TextFont.valueOf(parcel.readString()), parcel.readInt(), ChatColors.CREATOR.createFromParcel(parcel), parcel.readString());
        }

        @Override // android.os.Parcelable.Creator
        public final TextStoryPostCreationState[] newArray(int i) {
            return new TextStoryPostCreationState[i];
        }
    }

    /* compiled from: TextStoryPostCreationState.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[TextColorStyle.values().length];
            iArr[TextColorStyle.NO_BACKGROUND.ordinal()] = 1;
            iArr[TextColorStyle.NORMAL.ordinal()] = 2;
            iArr[TextColorStyle.INVERT.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public TextStoryPostCreationState() {
        this(null, 0, null, null, null, 0, null, null, 255, null);
    }

    public static /* synthetic */ void getTextBackgroundColor$annotations() {
    }

    public static /* synthetic */ void getTextForegroundColor$annotations() {
    }

    public final CharSequence component1() {
        return this.body;
    }

    public final int component2() {
        return this.textColor;
    }

    public final TextColorStyle component3() {
        return this.textColorStyle;
    }

    public final TextAlignment component4() {
        return this.textAlignment;
    }

    public final TextFont component5() {
        return this.textFont;
    }

    public final int component6() {
        return this.textScale;
    }

    public final ChatColors component7() {
        return this.backgroundColor;
    }

    public final String component8() {
        return this.linkPreviewUri;
    }

    public final TextStoryPostCreationState copy(CharSequence charSequence, int i, TextColorStyle textColorStyle, TextAlignment textAlignment, TextFont textFont, int i2, ChatColors chatColors, String str) {
        Intrinsics.checkNotNullParameter(charSequence, "body");
        Intrinsics.checkNotNullParameter(textColorStyle, "textColorStyle");
        Intrinsics.checkNotNullParameter(textAlignment, "textAlignment");
        Intrinsics.checkNotNullParameter(textFont, "textFont");
        Intrinsics.checkNotNullParameter(chatColors, "backgroundColor");
        return new TextStoryPostCreationState(charSequence, i, textColorStyle, textAlignment, textFont, i2, chatColors, str);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TextStoryPostCreationState)) {
            return false;
        }
        TextStoryPostCreationState textStoryPostCreationState = (TextStoryPostCreationState) obj;
        return Intrinsics.areEqual(this.body, textStoryPostCreationState.body) && this.textColor == textStoryPostCreationState.textColor && this.textColorStyle == textStoryPostCreationState.textColorStyle && this.textAlignment == textStoryPostCreationState.textAlignment && this.textFont == textStoryPostCreationState.textFont && this.textScale == textStoryPostCreationState.textScale && Intrinsics.areEqual(this.backgroundColor, textStoryPostCreationState.backgroundColor) && Intrinsics.areEqual(this.linkPreviewUri, textStoryPostCreationState.linkPreviewUri);
    }

    @Override // java.lang.Object
    public int hashCode() {
        int hashCode = ((((((((((((this.body.hashCode() * 31) + this.textColor) * 31) + this.textColorStyle.hashCode()) * 31) + this.textAlignment.hashCode()) * 31) + this.textFont.hashCode()) * 31) + this.textScale) * 31) + this.backgroundColor.hashCode()) * 31;
        String str = this.linkPreviewUri;
        return hashCode + (str == null ? 0 : str.hashCode());
    }

    @Override // java.lang.Object
    public String toString() {
        return "TextStoryPostCreationState(body=" + ((Object) this.body) + ", textColor=" + this.textColor + ", textColorStyle=" + this.textColorStyle + ", textAlignment=" + this.textAlignment + ", textFont=" + this.textFont + ", textScale=" + this.textScale + ", backgroundColor=" + this.backgroundColor + ", linkPreviewUri=" + this.linkPreviewUri + ')';
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "out");
        TextUtils.writeToParcel(this.body, parcel, i);
        parcel.writeInt(this.textColor);
        parcel.writeString(this.textColorStyle.name());
        parcel.writeString(this.textAlignment.name());
        parcel.writeString(this.textFont.name());
        parcel.writeInt(this.textScale);
        this.backgroundColor.writeToParcel(parcel, i);
        parcel.writeString(this.linkPreviewUri);
    }

    public TextStoryPostCreationState(CharSequence charSequence, int i, TextColorStyle textColorStyle, TextAlignment textAlignment, TextFont textFont, int i2, ChatColors chatColors, String str) {
        int i3;
        Intrinsics.checkNotNullParameter(charSequence, "body");
        Intrinsics.checkNotNullParameter(textColorStyle, "textColorStyle");
        Intrinsics.checkNotNullParameter(textAlignment, "textAlignment");
        Intrinsics.checkNotNullParameter(textFont, "textFont");
        Intrinsics.checkNotNullParameter(chatColors, "backgroundColor");
        this.body = charSequence;
        this.textColor = i;
        this.textColorStyle = textColorStyle;
        this.textAlignment = textAlignment;
        this.textFont = textFont;
        this.textScale = i2;
        this.backgroundColor = chatColors;
        this.linkPreviewUri = str;
        int[] iArr = WhenMappings.$EnumSwitchMapping$0;
        int i4 = iArr[textColorStyle.ordinal()];
        if (i4 == 1 || i4 == 2) {
            i3 = i;
        } else if (i4 == 3) {
            i3 = getDefaultColorForLightness(i);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        this.textForegroundColor = i3;
        int i5 = iArr[textColorStyle.ordinal()];
        if (i5 == 1) {
            i = 0;
        } else if (i5 == 2) {
            i = getDefaultColorForLightness(i);
        } else if (i5 != 3) {
            throw new NoWhenBranchMatchedException();
        }
        this.textBackgroundColor = i;
    }

    public final CharSequence getBody() {
        return this.body;
    }

    public /* synthetic */ TextStoryPostCreationState(CharSequence charSequence, int i, TextColorStyle textColorStyle, TextAlignment textAlignment, TextFont textFont, int i2, ChatColors chatColors, String str, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this((i3 & 1) != 0 ? "" : charSequence, (i3 & 2) != 0 ? HSVColorSlider.INSTANCE.getLastColor() : i, (i3 & 4) != 0 ? TextColorStyle.NO_BACKGROUND : textColorStyle, (i3 & 8) != 0 ? FeatureFlags.storiesTextFunctions() ? TextAlignment.START : TextAlignment.CENTER : textAlignment, (i3 & 16) != 0 ? TextFont.REGULAR : textFont, (i3 & 32) != 0 ? 50 : i2, (i3 & 64) != 0 ? TextStoryBackgroundColors.INSTANCE.getInitialBackgroundColor() : chatColors, (i3 & 128) != 0 ? null : str);
    }

    public final int getTextColor() {
        return this.textColor;
    }

    public final TextColorStyle getTextColorStyle() {
        return this.textColorStyle;
    }

    public final TextAlignment getTextAlignment() {
        return this.textAlignment;
    }

    public final TextFont getTextFont() {
        return this.textFont;
    }

    public final int getTextScale() {
        return this.textScale;
    }

    public final ChatColors getBackgroundColor() {
        return this.backgroundColor;
    }

    public final String getLinkPreviewUri() {
        return this.linkPreviewUri;
    }

    public final int getTextForegroundColor() {
        return this.textForegroundColor;
    }

    public final int getTextBackgroundColor() {
        return this.textBackgroundColor;
    }

    private final int getDefaultColorForLightness(int i) {
        float[] fArr = {0.0f, 0.0f, 0.0f};
        ColorUtils.colorToHSL(i, fArr);
        return fArr[2] >= 0.9f ? -16777216 : -1;
    }
}
