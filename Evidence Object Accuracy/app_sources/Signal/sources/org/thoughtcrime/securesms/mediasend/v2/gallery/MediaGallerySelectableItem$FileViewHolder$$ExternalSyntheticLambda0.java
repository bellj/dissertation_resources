package org.thoughtcrime.securesms.mediasend.v2.gallery;

import android.animation.ValueAnimator;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectableItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaGallerySelectableItem$FileViewHolder$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ MediaGallerySelectableItem.FileViewHolder f$0;

    public /* synthetic */ MediaGallerySelectableItem$FileViewHolder$$ExternalSyntheticLambda0(MediaGallerySelectableItem.FileViewHolder fileViewHolder) {
        this.f$0 = fileViewHolder;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        MediaGallerySelectableItem.FileViewHolder.m2172animateCheckState$lambda2$lambda1(this.f$0, valueAnimator);
    }
}
