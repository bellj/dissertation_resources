package org.thoughtcrime.securesms.mediasend.camerax;

import java.util.HashSet;
import java.util.Set;

/* loaded from: classes4.dex */
public class FastCameraModels {
    private static final Set<String> MODELS = new HashSet<String>() { // from class: org.thoughtcrime.securesms.mediasend.camerax.FastCameraModels.1
        {
            add("Pixel 2");
            add("Pixel 2 XL");
            add("Pixel 3");
            add("Pixel 3 XL");
            add("Pixel 3a");
            add("Pixel 3a XL");
        }
    };

    public static boolean contains(String str) {
        return MODELS.contains(str);
    }
}
