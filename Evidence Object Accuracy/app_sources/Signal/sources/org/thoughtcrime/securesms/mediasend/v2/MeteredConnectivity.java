package org.thoughtcrime.securesms.mediasend.v2;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import androidx.core.net.ConnectivityManagerCompat;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Cancellable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* compiled from: MeteredConnectivity.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MeteredConnectivity;", "", "()V", "isMetered", "Lio/reactivex/rxjava3/core/Observable;", "", "context", "Landroid/content/Context;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MeteredConnectivity {
    public static final MeteredConnectivity INSTANCE = new MeteredConnectivity();

    private MeteredConnectivity() {
    }

    public final Observable<Boolean> isMetered(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        Observable<Boolean> create = Observable.create(new ObservableOnSubscribe(context) { // from class: org.thoughtcrime.securesms.mediasend.v2.MeteredConnectivity$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                MeteredConnectivity.m2133isMetered$lambda1(this.f$0, observableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create { emitter ->\n    …eiver(receiver)\n    }\n  }");
        return create;
    }

    /* renamed from: isMetered$lambda-1 */
    public static final void m2133isMetered$lambda1(Context context, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(context, "$context");
        ConnectivityManager connectivityManager = ServiceUtil.getConnectivityManager(context);
        observableEmitter.onNext(Boolean.valueOf(ConnectivityManagerCompat.isActiveNetworkMetered(connectivityManager)));
        MeteredConnectivity$isMetered$1$receiver$1 meteredConnectivity$isMetered$1$receiver$1 = new MeteredConnectivity$isMetered$1$receiver$1(observableEmitter, connectivityManager);
        context.registerReceiver(meteredConnectivity$isMetered$1$receiver$1, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        observableEmitter.setCancellable(new Cancellable(context, meteredConnectivity$isMetered$1$receiver$1) { // from class: org.thoughtcrime.securesms.mediasend.v2.MeteredConnectivity$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ MeteredConnectivity$isMetered$1$receiver$1 f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                MeteredConnectivity.m2134isMetered$lambda1$lambda0(this.f$0, this.f$1);
            }
        });
    }

    /* renamed from: isMetered$lambda-1$lambda-0 */
    public static final void m2134isMetered$lambda1$lambda0(Context context, MeteredConnectivity$isMetered$1$receiver$1 meteredConnectivity$isMetered$1$receiver$1) {
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(meteredConnectivity$isMetered$1$receiver$1, "$receiver");
        context.unregisterReceiver(meteredConnectivity$isMetered$1$receiver$1);
    }
}
