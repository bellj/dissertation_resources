package org.thoughtcrime.securesms.mediasend.v2.gallery;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import androidx.recyclerview.widget.ItemTouchHelper;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionNavigator;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionState;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;
import org.thoughtcrime.securesms.mediasend.v2.MediaValidator;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment;
import org.thoughtcrime.securesms.mediasend.v2.review.MediaSelectionItemTouchHelper;
import org.thoughtcrime.securesms.permissions.Permissions;

/* compiled from: MediaSelectionGalleryFragment.kt */
@Metadata(d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u000e\u001a\u00020\u0005H\u0002J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\b\u0010\u0019\u001a\u00020\u0010H\u0016J-\u0010\u001a\u001a\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u001c2\u000e\u0010\u001d\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u001f0\u001e2\u0006\u0010 \u001a\u00020!H\u0016¢\u0006\u0002\u0010\"J\u0010\u0010#\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\b\u0010$\u001a\u00020\u0010H\u0016J\b\u0010%\u001a\u00020\u0010H\u0016J\u001a\u0010&\u001a\u00020\u00102\u0006\u0010'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\b\u001a\u00020\t8BX\u0002¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000b¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaSelectionGalleryFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryFragment$Callbacks;", "()V", "mediaGalleryFragment", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryFragment;", "navigator", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionNavigator;", "sharedViewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "getSharedViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "sharedViewModel$delegate", "Lkotlin/Lazy;", "ensureMediaGalleryFragment", "handleError", "", "error", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "isMultiselectEnabled", "", "onMediaSelected", "media", "Lorg/thoughtcrime/securesms/mediasend/Media;", "onMediaUnselected", "onNavigateToCamera", "onRequestPermissionsResult", "requestCode", "", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onSelectedMediaClicked", "onSubmit", "onToolbarNavigationClicked", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaSelectionGalleryFragment extends Fragment implements MediaGalleryFragment.Callbacks {
    private MediaGalleryFragment mediaGalleryFragment;
    private final MediaSelectionNavigator navigator = new MediaSelectionNavigator(R.id.action_mediaGalleryFragment_to_mediaCaptureFragment, 0, 2, null);
    private final Lazy sharedViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MediaSelectionViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaSelectionGalleryFragment$sharedViewModel$2
        final /* synthetic */ MediaSelectionGalleryFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaSelectionGalleryFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public boolean isMultiselectEnabled() {
        return true;
    }

    public MediaSelectionGalleryFragment() {
        super(R.layout.fragment_container);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public boolean isCameraEnabled() {
        return MediaGalleryFragment.Callbacks.DefaultImpls.isCameraEnabled(this);
    }

    private final MediaSelectionViewModel getSharedViewModel() {
        return (MediaSelectionViewModel) this.sharedViewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        MediaGalleryFragment ensureMediaGalleryFragment = ensureMediaGalleryFragment();
        this.mediaGalleryFragment = ensureMediaGalleryFragment;
        if (ensureMediaGalleryFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mediaGalleryFragment");
            ensureMediaGalleryFragment = null;
        }
        ensureMediaGalleryFragment.bindSelectedMediaItemDragHelper(new ItemTouchHelper(new MediaSelectionItemTouchHelper(getSharedViewModel())));
        getSharedViewModel().getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaSelectionGalleryFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MediaSelectionGalleryFragment.$r8$lambda$tXdGFZTaJVtwFTK0_Mgmbr0bTRU(MediaSelectionGalleryFragment.this, (MediaSelectionState) obj);
            }
        });
        Bundle arguments = getArguments();
        boolean z = true;
        if (arguments == null || !arguments.containsKey("first")) {
            z = false;
        }
        if (z) {
            requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaSelectionGalleryFragment$onViewCreated$2
                final /* synthetic */ MediaSelectionGalleryFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // androidx.activity.OnBackPressedCallback
                public void handleOnBackPressed() {
                    this.this$0.requireActivity().finish();
                }
            });
        }
        getSharedViewModel().getMediaErrors().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaSelectionGalleryFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MediaSelectionGalleryFragment.$r8$lambda$es85VL1wXl84TYwb9PpQVm2kUZk(MediaSelectionGalleryFragment.this, (MediaValidator.FilterError) obj);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2180onViewCreated$lambda0(MediaSelectionGalleryFragment mediaSelectionGalleryFragment, MediaSelectionState mediaSelectionState) {
        Intrinsics.checkNotNullParameter(mediaSelectionGalleryFragment, "this$0");
        MediaGalleryFragment mediaGalleryFragment = mediaSelectionGalleryFragment.mediaGalleryFragment;
        if (mediaGalleryFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mediaGalleryFragment");
            mediaGalleryFragment = null;
        }
        mediaGalleryFragment.onViewStateUpdated(new MediaGalleryFragment.ViewState(mediaSelectionState.getSelectedMedia()));
    }

    public final void handleError(MediaValidator.FilterError filterError) {
        if (Intrinsics.areEqual(filterError, MediaValidator.FilterError.ItemTooLarge.INSTANCE)) {
            Toast.makeText(requireContext(), (int) R.string.MediaReviewFragment__one_or_more_items_were_too_large, 0).show();
        } else if (Intrinsics.areEqual(filterError, MediaValidator.FilterError.ItemInvalidType.INSTANCE)) {
            Toast.makeText(requireContext(), (int) R.string.MediaReviewFragment__one_or_more_items_were_invalid, 0).show();
        } else if (Intrinsics.areEqual(filterError, MediaValidator.FilterError.TooManyItems.INSTANCE)) {
            Toast.makeText(requireContext(), (int) R.string.MediaReviewFragment__too_many_items_selected, 0).show();
        } else if (filterError instanceof MediaValidator.FilterError.NoItems) {
            MediaValidator.FilterError.NoItems noItems = (MediaValidator.FilterError.NoItems) filterError;
            if (noItems.getCause() != null) {
                handleError(noItems.getCause());
            }
        }
    }

    private final MediaGalleryFragment ensureMediaGalleryFragment() {
        Fragment findFragmentByTag = getChildFragmentManager().findFragmentByTag("MEDIA_GALLERY");
        MediaGalleryFragment mediaGalleryFragment = findFragmentByTag instanceof MediaGalleryFragment ? (MediaGalleryFragment) findFragmentByTag : null;
        if (mediaGalleryFragment != null) {
            return mediaGalleryFragment;
        }
        MediaGalleryFragment mediaGalleryFragment2 = new MediaGalleryFragment();
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, mediaGalleryFragment2, "MEDIA_GALLERY").commitNowAllowingStateLoss();
        return mediaGalleryFragment2;
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Intrinsics.checkNotNullParameter(strArr, "permissions");
        Intrinsics.checkNotNullParameter(iArr, "grantResults");
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onMediaSelected(Media media) {
        Intrinsics.checkNotNullParameter(media, "media");
        getSharedViewModel().addMedia(media);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onMediaUnselected(Media media) {
        Intrinsics.checkNotNullParameter(media, "media");
        getSharedViewModel().removeMedia(media);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onSelectedMediaClicked(Media media) {
        Intrinsics.checkNotNullParameter(media, "media");
        getSharedViewModel().setFocusedMedia(media);
        this.navigator.goToReview(FragmentKt.findNavController(this));
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onNavigateToCamera() {
        MediaSelectionNavigator.Companion.requestPermissionsForCamera(this, new Function0<Unit>(this, FragmentKt.findNavController(this)) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaSelectionGalleryFragment$onNavigateToCamera$1
            final /* synthetic */ NavController $controller;
            final /* synthetic */ MediaSelectionGalleryFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$controller = r2;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                MediaSelectionGalleryFragment.access$getNavigator$p(this.this$0).goToCamera(this.$controller);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onSubmit() {
        this.navigator.goToReview(FragmentKt.findNavController(this));
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onToolbarNavigationClicked() {
        requireActivity().onBackPressed();
    }
}
