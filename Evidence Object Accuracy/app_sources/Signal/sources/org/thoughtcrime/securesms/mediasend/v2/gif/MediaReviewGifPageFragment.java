package org.thoughtcrime.securesms.mediasend.v2.gif;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.MediaSendGifFragment;
import org.thoughtcrime.securesms.mediasend.v2.HudCommand;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;

/* compiled from: MediaReviewGifPageFragment.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u00020\u0004H\u0002J\u001a\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016J\b\u0010\u0012\u001a\u00020\u0013H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gif/MediaReviewGifPageFragment;", "Landroidx/fragment/app/Fragment;", "()V", "mediaSendGifFragment", "Lorg/thoughtcrime/securesms/mediasend/MediaSendGifFragment;", "sharedViewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "getSharedViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "sharedViewModel$delegate", "Lkotlin/Lazy;", "ensureGifFragment", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "requireUri", "Landroid/net/Uri;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaReviewGifPageFragment extends Fragment {
    private static final String ARG_URI;
    public static final Companion Companion = new Companion(null);
    private MediaSendGifFragment mediaSendGifFragment;
    private final Lazy sharedViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MediaSelectionViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.gif.MediaReviewGifPageFragment$sharedViewModel$2
        final /* synthetic */ MediaReviewGifPageFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.gif.MediaReviewGifPageFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    public MediaReviewGifPageFragment() {
        super(R.layout.fragment_container);
    }

    private final MediaSelectionViewModel getSharedViewModel() {
        return (MediaSelectionViewModel) this.sharedViewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        this.mediaSendGifFragment = ensureGifFragment();
        getSharedViewModel().sendCommand(HudCommand.ResumeEntryTransition.INSTANCE);
    }

    private final MediaSendGifFragment ensureGifFragment() {
        Fragment findFragmentByTag = getChildFragmentManager().findFragmentByTag("media.send.gif.fragment");
        MediaSendGifFragment mediaSendGifFragment = findFragmentByTag instanceof MediaSendGifFragment ? (MediaSendGifFragment) findFragmentByTag : null;
        if (mediaSendGifFragment != null) {
            getSharedViewModel().sendCommand(HudCommand.ResumeEntryTransition.INSTANCE);
            return mediaSendGifFragment;
        }
        MediaSendGifFragment newInstance = MediaSendGifFragment.newInstance(requireUri());
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, newInstance, "media.send.gif.fragment").commitAllowingStateLoss();
        Intrinsics.checkNotNullExpressionValue(newInstance, "{\n      val mediaSendGif…ediaSendGifFragment\n    }");
        return newInstance;
    }

    private final Uri requireUri() {
        Parcelable parcelable = requireArguments().getParcelable(ARG_URI);
        if (parcelable != null) {
            return (Uri) parcelable;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    /* compiled from: MediaReviewGifPageFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gif/MediaReviewGifPageFragment$Companion;", "", "()V", "ARG_URI", "", "newInstance", "Landroidx/fragment/app/Fragment;", "uri", "Landroid/net/Uri;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Fragment newInstance(Uri uri) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            MediaReviewGifPageFragment mediaReviewGifPageFragment = new MediaReviewGifPageFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(MediaReviewGifPageFragment.ARG_URI, uri);
            mediaReviewGifPageFragment.setArguments(bundle);
            return mediaReviewGifPageFragment;
        }
    }
}
