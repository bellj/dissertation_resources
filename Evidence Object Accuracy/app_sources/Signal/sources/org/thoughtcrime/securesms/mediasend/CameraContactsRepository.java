package org.thoughtcrime.securesms.mediasend;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.mediasend.CameraContactsRepository;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class CameraContactsRepository {
    private static final int RECENT_MAX;
    private static final String TAG = Log.tag(CameraContactsRepository.class);
    private final ContactRepository contactRepository;
    private final Context context;
    private final GroupDatabase groupDatabase = SignalDatabase.groups();
    private final ExecutorService parallelExecutor;
    private final RecipientDatabase recipientDatabase = SignalDatabase.recipients();
    private final Executor serialExecutor;
    private final ThreadDatabase threadDatabase = SignalDatabase.threads();

    /* loaded from: classes4.dex */
    public interface Callback<E> {
        void onComplete(E e);
    }

    public CameraContactsRepository(Context context) {
        this.context = context.getApplicationContext();
        this.contactRepository = new ContactRepository(context, context.getString(R.string.note_to_self));
        this.serialExecutor = SignalExecutors.SERIAL;
        this.parallelExecutor = SignalExecutors.BOUNDED;
    }

    public void getCameraContacts(Callback<CameraContacts> callback) {
        getCameraContacts("", callback);
    }

    public void getCameraContacts(String str, Callback<CameraContacts> callback) {
        this.serialExecutor.execute(new Runnable(str, callback) { // from class: org.thoughtcrime.securesms.mediasend.CameraContactsRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$1;
            public final /* synthetic */ CameraContactsRepository.Callback f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CameraContactsRepository.this.lambda$getCameraContacts$3(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$getCameraContacts$3(String str, Callback callback) {
        Future submit = this.parallelExecutor.submit(new Callable(str) { // from class: org.thoughtcrime.securesms.mediasend.CameraContactsRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return CameraContactsRepository.this.lambda$getCameraContacts$0(this.f$1);
            }
        });
        Future submit2 = this.parallelExecutor.submit(new Callable(str) { // from class: org.thoughtcrime.securesms.mediasend.CameraContactsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return CameraContactsRepository.this.lambda$getCameraContacts$1(this.f$1);
            }
        });
        Future submit3 = this.parallelExecutor.submit(new Callable(str) { // from class: org.thoughtcrime.securesms.mediasend.CameraContactsRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return CameraContactsRepository.this.lambda$getCameraContacts$2(this.f$1);
            }
        });
        try {
            long currentTimeMillis = System.currentTimeMillis();
            CameraContacts cameraContacts = new CameraContacts((List) submit.get(), (List) submit2.get(), (List) submit3.get());
            String str2 = TAG;
            Log.d(str2, "Total time: " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
            callback.onComplete(cameraContacts);
        } catch (InterruptedException | ExecutionException e) {
            Log.w(TAG, "Failed to perform queries.", e);
            callback.onComplete(new CameraContacts(Collections.emptyList(), Collections.emptyList(), Collections.emptyList()));
        }
    }

    /* renamed from: getRecents */
    public List<Recipient> lambda$getCameraContacts$0(String str) {
        if (!TextUtils.isEmpty(str)) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(25);
        ThreadDatabase threadDatabase = this.threadDatabase;
        ThreadDatabase.Reader readerFor = threadDatabase.readerFor(threadDatabase.getRecentPushConversationList(25, false));
        while (true) {
            try {
                ThreadRecord next = readerFor.getNext();
                if (next != null) {
                    arrayList.add(next.getRecipient().resolve());
                } else {
                    readerFor.close();
                    return arrayList;
                }
            } catch (Throwable th) {
                if (readerFor != null) {
                    try {
                        readerFor.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
    }

    /* renamed from: getContacts */
    public List<Recipient> lambda$getCameraContacts$1(String str) {
        ArrayList arrayList = new ArrayList();
        Cursor querySignalContacts = this.contactRepository.querySignalContacts(str);
        while (querySignalContacts.moveToNext()) {
            try {
                arrayList.add(Recipient.resolved(RecipientId.from(querySignalContacts.getLong(querySignalContacts.getColumnIndexOrThrow(ContactRepository.ID_COLUMN)))));
            } catch (Throwable th) {
                if (querySignalContacts != null) {
                    try {
                        querySignalContacts.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        querySignalContacts.close();
        return arrayList;
    }

    /* renamed from: getGroups */
    public List<Recipient> lambda$getCameraContacts$2(String str) {
        if (TextUtils.isEmpty(str)) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        GroupDatabase.Reader queryGroupsByTitle = this.groupDatabase.queryGroupsByTitle(str, false, true, true);
        while (true) {
            try {
                GroupDatabase.GroupRecord next = queryGroupsByTitle.getNext();
                if (next != null) {
                    arrayList.add(Recipient.resolved(this.recipientDatabase.getOrInsertFromGroupId(next.getId())));
                } else {
                    queryGroupsByTitle.close();
                    return arrayList;
                }
            } catch (Throwable th) {
                if (queryGroupsByTitle != null) {
                    try {
                        queryGroupsByTitle.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
    }
}
