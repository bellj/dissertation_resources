package org.thoughtcrime.securesms.mediasend.v2.gallery;

import android.view.View;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectableItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaGallerySelectableItem$FileViewHolder$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ MediaGallerySelectableItem.FileViewHolder f$0;
    public final /* synthetic */ MediaGallerySelectableItem.FileModel f$1;

    public /* synthetic */ MediaGallerySelectableItem$FileViewHolder$$ExternalSyntheticLambda1(MediaGallerySelectableItem.FileViewHolder fileViewHolder, MediaGallerySelectableItem.FileModel fileModel) {
        this.f$0 = fileViewHolder;
        this.f$1 = fileModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        MediaGallerySelectableItem.FileViewHolder.m2173bind$lambda0(this.f$0, this.f$1, view);
    }
}
