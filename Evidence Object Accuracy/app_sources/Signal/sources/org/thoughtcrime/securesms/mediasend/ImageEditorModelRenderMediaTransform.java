package org.thoughtcrime.securesms.mediasend;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import j$.util.Optional;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.signal.imageeditor.core.model.EditorModel;
import org.thoughtcrime.securesms.fonts.FontTypefaceProvider;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public final class ImageEditorModelRenderMediaTransform implements MediaTransform {
    private static final String TAG = Log.tag(ImageEditorModelRenderMediaTransform.class);
    private final EditorModel modelToRender;
    private final Point size;

    public ImageEditorModelRenderMediaTransform(EditorModel editorModel) {
        this(editorModel, null);
    }

    public ImageEditorModelRenderMediaTransform(EditorModel editorModel, Point point) {
        this.modelToRender = editorModel;
        this.size = point;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaTransform
    public Media transform(Context context, Media media) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Bitmap render = this.modelToRender.render(context, this.size, new FontTypefaceProvider());
        try {
            render.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
            return new Media(BlobProvider.getInstance().forData(byteArrayOutputStream.toByteArray()).withMimeType(MediaUtil.IMAGE_JPEG).createForSingleSessionOnDisk(context), MediaUtil.IMAGE_JPEG, media.getDate(), render.getWidth(), render.getHeight(), (long) byteArrayOutputStream.size(), 0, false, false, media.getBucketId(), media.getCaption(), Optional.empty());
        } catch (IOException unused) {
            Log.w(TAG, "Failed to render image. Using base image.");
            return media;
        } finally {
            render.recycle();
            StreamUtil.close(byteArrayOutputStream);
        }
    }
}
