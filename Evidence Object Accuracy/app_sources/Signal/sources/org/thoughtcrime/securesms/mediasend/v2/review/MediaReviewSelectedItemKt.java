package org.thoughtcrime.securesms.mediasend.v2.review;

import kotlin.Metadata;

/* compiled from: MediaReviewSelectedItem.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0000*.\u0010\u0000\"\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00012\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001¨\u0006\u0005"}, d2 = {"OnSelectedMediaClicked", "Lkotlin/Function2;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaReviewSelectedItemKt {
}
