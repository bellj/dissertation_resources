package org.thoughtcrime.securesms.mediasend;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class CameraButtonView extends View {
    private static final float CAPTURE_ARC_STROKE_WIDTH;
    private static final int CAPTURE_FILL_PROTECTION;
    private static final float DEADZONE_REDUCTION_PERCENT;
    private static final int DRAG_DISTANCE_MULTIPLIER;
    private static final int HALF_PROGRESS_ARC_STROKE_WIDTH;
    private static final int PROGRESS_ARC_STROKE_WIDTH;
    private static final Interpolator ZOOM_INTERPOLATOR = new DecelerateInterpolator();
    private final Paint arcPaint;
    private final Paint backgroundPaint;
    private CameraButtonMode cameraButtonMode;
    private final Paint captureFillPaint;
    private final Rect deadzoneRect;
    private Animation growAnimation;
    private final float imageCaptureSize;
    private final View.OnLongClickListener internalLongClickListener;
    private boolean isRecordingVideo;
    private final Paint outlinePaint;
    private final Paint progressPaint;
    private float progressPercent;
    private final RectF progressRect;
    private final Paint recordPaint;
    private final float recordSize;
    private Animation shrinkAnimation;
    private VideoCaptureListener videoCaptureListener;

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public enum CameraButtonMode {
        IMAGE,
        MIXED
    }

    /* loaded from: classes4.dex */
    public interface VideoCaptureListener {
        void onVideoCaptureComplete();

        void onVideoCaptureStarted();

        void onZoomIncremented(float f);
    }

    public /* synthetic */ boolean lambda$new$0(View view) {
        notifyVideoCaptureStarted();
        this.shrinkAnimation.cancel();
        setScaleX(1.0f);
        setScaleY(1.0f);
        this.isRecordingVideo = true;
        return true;
    }

    public CameraButtonView(Context context) {
        this(context, null);
    }

    public CameraButtonView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CameraButtonView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.outlinePaint = outlinePaint();
        this.backgroundPaint = backgroundPaint();
        this.arcPaint = arcPaint();
        this.recordPaint = recordPaint();
        this.progressPaint = progressPaint();
        this.captureFillPaint = captureFillPaint();
        this.progressPercent = 0.0f;
        this.cameraButtonMode = CameraButtonMode.IMAGE;
        this.progressRect = new RectF();
        this.deadzoneRect = new Rect();
        this.internalLongClickListener = new View.OnLongClickListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraButtonView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                return CameraButtonView.$r8$lambda$FxyjIGU8mn3wHw2jugxCa3meAQY(CameraButtonView.this, view);
            }
        };
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.CameraButtonView, i, 0);
        this.imageCaptureSize = (float) obtainStyledAttributes.getDimensionPixelSize(0, -1);
        this.recordSize = (float) obtainStyledAttributes.getDimensionPixelSize(1, -1);
        obtainStyledAttributes.recycle();
        initializeImageAnimations();
    }

    private static Paint recordPaint() {
        Paint paint = new Paint();
        paint.setColor(-769226);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        return paint;
    }

    private static Paint outlinePaint() {
        Paint paint = new Paint();
        paint.setColor(637534208);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth((float) ViewUtil.dpToPx(4));
        return paint;
    }

    private static Paint backgroundPaint() {
        Paint paint = new Paint();
        paint.setColor(1291845631);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        return paint;
    }

    private static Paint arcPaint() {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(DimensionUnit.DP.toPixels(CAPTURE_ARC_STROKE_WIDTH));
        return paint;
    }

    private static Paint captureFillPaint() {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        return paint;
    }

    private static Paint progressPaint() {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth((float) ViewUtil.dpToPx(4));
        paint.setShadowLayer(4.0f, 0.0f, 2.0f, 1073741824);
        return paint;
    }

    private void initializeImageAnimations() {
        this.shrinkAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.camera_capture_button_shrink);
        this.growAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.camera_capture_button_grow);
        this.shrinkAnimation.setFillAfter(true);
        this.shrinkAnimation.setFillEnabled(true);
        this.growAnimation.setFillAfter(true);
        this.growAnimation.setFillEnabled(true);
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.isRecordingVideo) {
            drawForVideoCapture(canvas);
        } else {
            drawForImageCapture(canvas);
        }
    }

    private void drawForImageCapture(Canvas canvas) {
        float width = ((float) getWidth()) / 2.0f;
        float height = ((float) getHeight()) / 2.0f;
        float f = this.imageCaptureSize / 2.0f;
        canvas.drawCircle(width, height, f, this.backgroundPaint);
        canvas.drawCircle(width, height, f, this.arcPaint);
        canvas.drawCircle(width, height, f - DimensionUnit.DP.toPixels(10.0f), this.captureFillPaint);
    }

    private void drawForVideoCapture(Canvas canvas) {
        float width = ((float) getWidth()) / 2.0f;
        float height = ((float) getHeight()) / 2.0f;
        canvas.drawCircle(width, height, height, this.backgroundPaint);
        canvas.drawCircle(width, height, height, this.outlinePaint);
        canvas.drawCircle(width, height, this.recordSize / 2.0f, this.recordPaint);
        this.progressRect.top = (float) ViewUtil.dpToPx(2);
        this.progressRect.left = (float) ViewUtil.dpToPx(2);
        this.progressRect.right = (float) (getWidth() - ViewUtil.dpToPx(2));
        this.progressRect.bottom = (float) (getHeight() - ViewUtil.dpToPx(2));
        canvas.drawArc(this.progressRect, 270.0f, this.progressPercent * 360.0f, false, this.progressPaint);
    }

    @Override // android.view.View
    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        throw new IllegalStateException("Use setVideoCaptureListener instead");
    }

    public void setVideoCaptureListener(VideoCaptureListener videoCaptureListener) {
        if (this.isRecordingVideo) {
            throw new IllegalStateException("Cannot set video capture listener while recording");
        } else if (videoCaptureListener != null) {
            this.cameraButtonMode = CameraButtonMode.MIXED;
            this.videoCaptureListener = videoCaptureListener;
            super.setOnLongClickListener(this.internalLongClickListener);
        } else {
            this.cameraButtonMode = CameraButtonMode.IMAGE;
            this.videoCaptureListener = null;
            super.setOnLongClickListener(null);
        }
    }

    public void setProgress(float f) {
        this.progressPercent = Util.clamp(f, 0.0f, 1.0f);
        invalidate();
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.cameraButtonMode == CameraButtonMode.IMAGE) {
            return handleImageModeTouchEvent(motionEvent);
        }
        boolean handleVideoModeTouchEvent = handleVideoModeTouchEvent(motionEvent);
        int action = motionEvent.getAction();
        if (action == 1 || action == 3) {
            this.isRecordingVideo = false;
        }
        return handleVideoModeTouchEvent;
    }

    @Override // android.view.View
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        getLocalVisibleRect(this.deadzoneRect);
        this.deadzoneRect.left += (int) ((((float) getWidth()) * DEADZONE_REDUCTION_PERCENT) / 2.0f);
        this.deadzoneRect.top += (int) ((((float) getHeight()) * DEADZONE_REDUCTION_PERCENT) / 2.0f);
        this.deadzoneRect.right -= (int) ((((float) getWidth()) * DEADZONE_REDUCTION_PERCENT) / 2.0f);
        this.deadzoneRect.bottom -= (int) ((((float) getHeight()) * DEADZONE_REDUCTION_PERCENT) / 2.0f);
    }

    private boolean handleImageModeTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            if (isEnabled()) {
                startAnimation(this.shrinkAnimation);
                performClick();
            }
            return true;
        } else if (action != 1) {
            return super.onTouchEvent(motionEvent);
        } else {
            startAnimation(this.growAnimation);
            return true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000d, code lost:
        if (r0 != 3) goto L_0x005a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean handleVideoModeTouchEvent(android.view.MotionEvent r4) {
        /*
            r3 = this;
            int r0 = r4.getAction()
            r1 = 3
            if (r0 == 0) goto L_0x001d
            r2 = 1
            if (r0 == r2) goto L_0x0010
            r2 = 2
            if (r0 == r2) goto L_0x0028
            if (r0 == r1) goto L_0x0010
            goto L_0x005a
        L_0x0010:
            boolean r0 = r3.isRecordingVideo
            if (r0 != 0) goto L_0x0019
            android.view.animation.Animation r0 = r3.growAnimation
            r3.startAnimation(r0)
        L_0x0019:
            r3.notifyVideoCaptureEnded()
            goto L_0x005a
        L_0x001d:
            boolean r0 = r3.isEnabled()
            if (r0 == 0) goto L_0x0028
            android.view.animation.Animation r0 = r3.shrinkAnimation
            r3.startAnimation(r0)
        L_0x0028:
            boolean r0 = r3.isRecordingVideo
            if (r0 == 0) goto L_0x005a
            boolean r0 = r3.eventIsNotInsideDeadzone(r4)
            if (r0 == 0) goto L_0x005a
            int r0 = r3.getHeight()
            int r0 = r0 * 3
            float r0 = (float) r0
            float r1 = r4.getY()
            android.graphics.Rect r2 = r3.deadzoneRect
            int r2 = r2.top
            float r2 = (float) r2
            float r1 = r1 - r2
            float r1 = java.lang.Math.abs(r1)
            r2 = 1065353216(0x3f800000, float:1.0)
            float r1 = r1 / r0
            float r0 = java.lang.Math.min(r2, r1)
            android.view.animation.Interpolator r1 = org.thoughtcrime.securesms.mediasend.CameraButtonView.ZOOM_INTERPOLATOR
            float r0 = r1.getInterpolation(r0)
            r3.notifyZoomPercent(r0)
            r3.invalidate()
        L_0x005a:
            boolean r4 = super.onTouchEvent(r4)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mediasend.CameraButtonView.handleVideoModeTouchEvent(android.view.MotionEvent):boolean");
    }

    private boolean eventIsNotInsideDeadzone(MotionEvent motionEvent) {
        return Math.round(motionEvent.getY()) < this.deadzoneRect.top;
    }

    private void notifyVideoCaptureStarted() {
        VideoCaptureListener videoCaptureListener;
        if (!this.isRecordingVideo && (videoCaptureListener = this.videoCaptureListener) != null) {
            videoCaptureListener.onVideoCaptureStarted();
        }
    }

    private void notifyVideoCaptureEnded() {
        VideoCaptureListener videoCaptureListener;
        if (this.isRecordingVideo && (videoCaptureListener = this.videoCaptureListener) != null) {
            videoCaptureListener.onVideoCaptureComplete();
        }
    }

    private void notifyZoomPercent(float f) {
        VideoCaptureListener videoCaptureListener;
        if (this.isRecordingVideo && (videoCaptureListener = this.videoCaptureListener) != null) {
            videoCaptureListener.onZoomIncremented(f);
        }
    }
}
