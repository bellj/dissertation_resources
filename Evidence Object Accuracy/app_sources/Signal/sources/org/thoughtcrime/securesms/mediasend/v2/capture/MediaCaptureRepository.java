package org.thoughtcrime.securesms.mediasend.v2.capture;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import j$.util.Optional;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.MediaRepository;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.StorageUtil;

/* compiled from: MediaCaptureRepository.kt */
@Metadata(d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0012\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 !2\u00020\u0001:\u0001!B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001c\u0010\u0005\u001a\u00020\u00062\u0014\u0010\u0007\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\t\u0012\u0004\u0012\u00020\u00060\bJn\u0010\n\u001a\u0004\u0018\u00010\t\"\u0004\b\u0000\u0010\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u000b0\r2\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u00020\u000f0\b2\"\u0010\u0010\u001a\u001e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u00020\u000f\u0012\b\u0012\u00060\u0013R\u00020\u00120\u00112\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0017H\u0002J@\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00172\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00060\b2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00060\rJ0\u0010\u001e\u001a\u00020\u00062\u0006\u0010\u001f\u001a\u00020 2\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00060\b2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00060\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getMostRecentItem", "", "callback", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "renderCaptureToMedia", "T", "dataSupplier", "Lkotlin/Function0;", "getLength", "", "createBlobBuilder", "Lkotlin/Function3;", "Lorg/thoughtcrime/securesms/providers/BlobProvider;", "Lorg/thoughtcrime/securesms/providers/BlobProvider$BlobBuilder;", "mimeType", "", "width", "", "height", "renderImageToMedia", "data", "", "onMediaRendered", "onFailedToRender", "renderVideoToMedia", "fileDescriptor", "Ljava/io/FileDescriptor;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaCaptureRepository {
    public static final Companion Companion = new Companion(null);
    private final Context context;

    public MediaCaptureRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        Context applicationContext = context.getApplicationContext();
        Intrinsics.checkNotNullExpressionValue(applicationContext, "context.applicationContext");
        this.context = applicationContext;
    }

    public final void getMostRecentItem(Function1<? super Media, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "callback");
        if (!StorageUtil.canReadFromMediaStore()) {
            Log.w(MediaCaptureRepositoryKt.TAG, "Cannot read from storage.");
            function1.invoke(null);
            return;
        }
        SignalExecutors.BOUNDED.execute(new Runnable(function1) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ Function1 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaCaptureRepository.$r8$lambda$EDFHkkJvFh92dtWrO5O6k8F0XXc(MediaCaptureRepository.this, this.f$1);
            }
        });
    }

    /* renamed from: getMostRecentItem$lambda-0 */
    public static final void m2143getMostRecentItem$lambda0(MediaCaptureRepository mediaCaptureRepository, Function1 function1) {
        Intrinsics.checkNotNullParameter(mediaCaptureRepository, "this$0");
        Intrinsics.checkNotNullParameter(function1, "$callback");
        Companion companion = Companion;
        Context context = mediaCaptureRepository.context;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Intrinsics.checkNotNullExpressionValue(uri, "EXTERNAL_CONTENT_URI");
        function1.invoke(CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) companion.getMediaInBucket(context, Media.ALL_MEDIA_BUCKET_ID, uri, true))));
    }

    public final void renderImageToMedia(byte[] bArr, int i, int i2, Function1<? super Media, Unit> function1, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(bArr, "data");
        Intrinsics.checkNotNullParameter(function1, "onMediaRendered");
        Intrinsics.checkNotNullParameter(function0, "onFailedToRender");
        SignalExecutors.BOUNDED.execute(new Runnable(i, i2, function1, function0, bArr) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ Function1 f$3;
            public final /* synthetic */ Function0 f$4;
            public final /* synthetic */ byte[] f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaCaptureRepository.$r8$lambda$fBCioKSaZVdL62BQGA1ckKw4EGo(MediaCaptureRepository.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        });
    }

    /* renamed from: renderImageToMedia$lambda-1 */
    public static final void m2144renderImageToMedia$lambda1(MediaCaptureRepository mediaCaptureRepository, int i, int i2, Function1 function1, Function0 function0, byte[] bArr) {
        Intrinsics.checkNotNullParameter(mediaCaptureRepository, "this$0");
        Intrinsics.checkNotNullParameter(function1, "$onMediaRendered");
        Intrinsics.checkNotNullParameter(function0, "$onFailedToRender");
        Intrinsics.checkNotNullParameter(bArr, "$data");
        Media renderCaptureToMedia = mediaCaptureRepository.renderCaptureToMedia(new Function0<byte[]>(bArr) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureRepository$renderImageToMedia$1$media$1
            final /* synthetic */ byte[] $data;

            /* access modifiers changed from: package-private */
            {
                this.$data = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final byte[] invoke() {
                return this.$data;
            }
        }, new Function1<byte[], Long>(bArr) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureRepository$renderImageToMedia$1$media$2
            final /* synthetic */ byte[] $data;

            /* access modifiers changed from: package-private */
            {
                this.$data = r1;
            }

            public final Long invoke(byte[] bArr2) {
                Intrinsics.checkNotNullParameter(bArr2, "it");
                return Long.valueOf((long) this.$data.length);
            }
        }, MediaCaptureRepository$renderImageToMedia$1$media$3.INSTANCE, MediaUtil.IMAGE_JPEG, i, i2);
        if (renderCaptureToMedia != null) {
            function1.invoke(renderCaptureToMedia);
        } else {
            function0.invoke();
        }
    }

    public final void renderVideoToMedia(FileDescriptor fileDescriptor, Function1<? super Media, Unit> function1, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(fileDescriptor, "fileDescriptor");
        Intrinsics.checkNotNullParameter(function1, "onMediaRendered");
        Intrinsics.checkNotNullParameter(function0, "onFailedToRender");
        SignalExecutors.BOUNDED.execute(new Runnable(function1, function0, fileDescriptor) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ Function1 f$1;
            public final /* synthetic */ Function0 f$2;
            public final /* synthetic */ FileDescriptor f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaCaptureRepository.$r8$lambda$nUPxCgSpeiWMW9n0RLe7DznHkkU(MediaCaptureRepository.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    /* renamed from: renderVideoToMedia$lambda-2 */
    public static final void m2145renderVideoToMedia$lambda2(MediaCaptureRepository mediaCaptureRepository, Function1 function1, Function0 function0, FileDescriptor fileDescriptor) {
        Intrinsics.checkNotNullParameter(mediaCaptureRepository, "this$0");
        Intrinsics.checkNotNullParameter(function1, "$onMediaRendered");
        Intrinsics.checkNotNullParameter(function0, "$onFailedToRender");
        Intrinsics.checkNotNullParameter(fileDescriptor, "$fileDescriptor");
        Media renderCaptureToMedia = mediaCaptureRepository.renderCaptureToMedia(new Function0<FileInputStream>(fileDescriptor) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureRepository$renderVideoToMedia$1$media$1
            final /* synthetic */ FileDescriptor $fileDescriptor;

            /* access modifiers changed from: package-private */
            {
                this.$fileDescriptor = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final FileInputStream invoke() {
                return new FileInputStream(this.$fileDescriptor);
            }
        }, MediaCaptureRepository$renderVideoToMedia$1$media$2.INSTANCE, MediaCaptureRepository$renderVideoToMedia$1$media$3.INSTANCE, "video/mp4", 0, 0);
        if (renderCaptureToMedia != null) {
            function1.invoke(renderCaptureToMedia);
        } else {
            function0.invoke();
        }
    }

    private final <T> Media renderCaptureToMedia(Function0<? extends T> function0, Function1<? super T, Long> function1, Function3<? super BlobProvider, ? super T, ? super Long, ? extends BlobProvider.BlobBuilder> function3, String str, int i, int i2) {
        try {
            Object obj = (Object) function0.invoke();
            long longValue = function1.invoke(obj).longValue();
            BlobProvider instance = BlobProvider.getInstance();
            Intrinsics.checkNotNullExpressionValue(instance, "getInstance()");
            Uri createForSingleSessionOnDisk = ((BlobProvider.BlobBuilder) function3.invoke(instance, obj, Long.valueOf(longValue))).withMimeType(str).createForSingleSessionOnDisk(this.context);
            Intrinsics.checkNotNullExpressionValue(createForSingleSessionOnDisk, "createBlobBuilder(BlobPr…gleSessionOnDisk(context)");
            return new Media(createForSingleSessionOnDisk, str, System.currentTimeMillis(), i, i2, longValue, 0, false, false, Optional.of(Media.ALL_MEDIA_BUCKET_ID), Optional.empty(), Optional.empty());
        } catch (IOException unused) {
            return null;
        }
    }

    /* compiled from: MediaCaptureRepository.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J.\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0003J\u0010\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\b\u0010\u0012\u001a\u00020\u0004H\u0002¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureRepository$Companion;", "", "()V", "getHeightColumn", "", "orientation", "", "getMediaInBucket", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "context", "Landroid/content/Context;", "bucketId", "contentUri", "Landroid/net/Uri;", "isImage", "", "getWidthColumn", "isNotPending", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private final String getHeightColumn(int i) {
            return (i == 0 || i == 180) ? "height" : "width";
        }

        private final String getWidthColumn(int i) {
            return (i == 0 || i == 180) ? "width" : "height";
        }

        private Companion() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r12v0, types: [java.lang.Throwable] */
        public final List<Media> getMediaInBucket(Context context, String str, Uri uri, boolean z) {
            String[] strArr;
            String[] strArr2;
            String str2;
            LinkedList linkedList = new LinkedList();
            String str3 = "bucket_id = ? AND " + isNotPending();
            String[] strArr3 = {str};
            if (z) {
                strArr = new String[]{"_id", "mime_type", "date_modified", "orientation", "width", "height", "_size"};
            } else {
                strArr = new String[]{"_id", "mime_type", "date_modified", "width", "height", "_size", "duration"};
            }
            th = 0;
            if (Intrinsics.areEqual(Media.ALL_MEDIA_BUCKET_ID, str)) {
                str2 = isNotPending();
                strArr2 = th;
            } else {
                str2 = str3;
                strArr2 = strArr3;
            }
            Cursor query = context.getContentResolver().query(uri, strArr, str2, strArr2, "date_modified DESC");
            while (query != null) {
                try {
                    if (!query.moveToNext()) {
                        break;
                    }
                    Uri withAppendedId = ContentUris.withAppendedId(uri, CursorUtil.requireLong(query, strArr[0]));
                    Intrinsics.checkNotNullExpressionValue(withAppendedId, "withAppendedId(contentUri, rowId)");
                    String requireString = CursorUtil.requireString(query, "mime_type");
                    long requireLong = CursorUtil.requireLong(query, "date_modified");
                    int requireInt = z ? CursorUtil.requireInt(query, "orientation") : 0;
                    Companion companion = MediaCaptureRepository.Companion;
                    Media fixMimeType = MediaRepository.fixMimeType(context, new Media(withAppendedId, requireString, requireLong, CursorUtil.requireInt(query, companion.getWidthColumn(requireInt)), CursorUtil.requireInt(query, companion.getHeightColumn(requireInt)), CursorUtil.requireLong(query, "_size"), !z ? (long) CursorUtil.requireInt(query, "duration") : 0, false, false, Optional.of(str), Optional.empty(), Optional.empty()));
                    Intrinsics.checkNotNullExpressionValue(fixMimeType, "fixMimeType(\n           …          )\n            )");
                    linkedList.add(fixMimeType);
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
            return linkedList;
        }

        private final String isNotPending() {
            return Build.VERSION.SDK_INT <= 28 ? "_data NOT NULL" : "is_pending != 1";
        }
    }
}
