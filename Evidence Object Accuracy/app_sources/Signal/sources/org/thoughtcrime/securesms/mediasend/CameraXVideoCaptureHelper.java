package org.thoughtcrime.securesms.mediasend;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Size;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;
import androidx.camera.core.VideoCapture;
import androidx.camera.view.SignalCameraView;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.util.Executors;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.CameraButtonView;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.Debouncer;
import org.thoughtcrime.securesms.util.MemoryFileDescriptor;
import org.thoughtcrime.securesms.video.VideoUtil;

/* loaded from: classes4.dex */
public class CameraXVideoCaptureHelper implements CameraButtonView.VideoCaptureListener {
    private static final String TAG;
    private static final String VIDEO_DEBUG_LABEL;
    private static final long VIDEO_SIZE;
    private final Callback callback;
    private final SignalCameraView camera;
    private ValueAnimator cameraMetricsAnimator;
    private final Debouncer debouncer;
    private final Fragment fragment;
    private boolean isRecording;
    private final MemoryFileDescriptor memoryFileDescriptor;
    private final ValueAnimator updateProgressAnimator;
    private final VideoCapture.OnVideoSavedCallback videoSavedListener = new VideoCapture.OnVideoSavedCallback() { // from class: org.thoughtcrime.securesms.mediasend.CameraXVideoCaptureHelper.1
        @Override // androidx.camera.core.VideoCapture.OnVideoSavedCallback
        public void onVideoSaved(VideoCapture.OutputFileResults outputFileResults) {
            try {
                CameraXVideoCaptureHelper.this.isRecording = false;
                CameraXVideoCaptureHelper.this.debouncer.clear();
                CameraXVideoCaptureHelper.this.camera.setZoomRatio(CameraXVideoCaptureHelper.this.camera.getMinZoomRatio());
                CameraXVideoCaptureHelper.this.memoryFileDescriptor.seek(0);
                CameraXVideoCaptureHelper.this.callback.onVideoSaved(CameraXVideoCaptureHelper.this.memoryFileDescriptor.getFileDescriptor());
            } catch (IOException e) {
                CameraXVideoCaptureHelper.this.callback.onVideoError(e);
            }
        }

        @Override // androidx.camera.core.VideoCapture.OnVideoSavedCallback
        public void onError(int i, String str, Throwable th) {
            CameraXVideoCaptureHelper.this.isRecording = false;
            CameraXVideoCaptureHelper.this.debouncer.clear();
            CameraXVideoCaptureHelper.this.callback.onVideoError(th);
        }
    };

    /* loaded from: classes4.dex */
    public interface Callback {
        void onVideoError(Throwable th);

        void onVideoRecordStarted();

        void onVideoSaved(FileDescriptor fileDescriptor);
    }

    public CameraXVideoCaptureHelper(Fragment fragment, CameraButtonView cameraButtonView, SignalCameraView signalCameraView, MemoryFileDescriptor memoryFileDescriptor, int i, Callback callback) {
        this.fragment = fragment;
        this.camera = signalCameraView;
        this.memoryFileDescriptor = memoryFileDescriptor;
        this.callback = callback;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        TimeUnit timeUnit = TimeUnit.SECONDS;
        long j = (long) i;
        ValueAnimator duration = ofFloat.setDuration(timeUnit.toMillis(j));
        this.updateProgressAnimator = duration;
        this.debouncer = new Debouncer(timeUnit.toMillis(j));
        duration.setInterpolator(new LinearInterpolator());
        duration.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.thoughtcrime.securesms.mediasend.CameraXVideoCaptureHelper$$ExternalSyntheticLambda2
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                CameraXVideoCaptureHelper.lambda$new$0(CameraButtonView.this, valueAnimator);
            }
        });
    }

    public static /* synthetic */ void lambda$new$0(CameraButtonView cameraButtonView, ValueAnimator valueAnimator) {
        cameraButtonView.setProgress(valueAnimator.getAnimatedFraction());
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraButtonView.VideoCaptureListener
    public void onVideoCaptureStarted() {
        Log.d(TAG, "onVideoCaptureStarted");
        if (canRecordAudio()) {
            this.isRecording = true;
            beginCameraRecording();
            return;
        }
        displayAudioRecordingPermissionsDialog();
    }

    private boolean canRecordAudio() {
        return Permissions.hasAll(this.fragment.requireContext(), "android.permission.RECORD_AUDIO");
    }

    private void displayAudioRecordingPermissionsDialog() {
        Permissions.with(this.fragment).request("android.permission.RECORD_AUDIO").ifNecessary().withRationaleDialog(this.fragment.getString(R.string.ConversationActivity_enable_the_microphone_permission_to_capture_videos_with_sound), R.drawable.ic_mic_solid_24).withPermanentDenialDialog(this.fragment.getString(R.string.ConversationActivity_signal_needs_the_recording_permissions_to_capture_video)).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.CameraXVideoCaptureHelper$$ExternalSyntheticLambda3
            @Override // java.lang.Runnable
            public final void run() {
                CameraXVideoCaptureHelper.this.lambda$displayAudioRecordingPermissionsDialog$1();
            }
        }).execute();
    }

    public /* synthetic */ void lambda$displayAudioRecordingPermissionsDialog$1() {
        Toast.makeText(this.fragment.requireContext(), (int) R.string.ConversationActivity_signal_needs_recording_permissions_to_capture_video, 1).show();
    }

    private void beginCameraRecording() {
        SignalCameraView signalCameraView = this.camera;
        signalCameraView.setZoomRatio(signalCameraView.getMinZoomRatio());
        this.callback.onVideoRecordStarted();
        shrinkCaptureArea();
        this.camera.startRecording(new VideoCapture.OutputFileOptions.Builder(this.memoryFileDescriptor.getFileDescriptor()).build(), Executors.mainThreadExecutor(), this.videoSavedListener);
        this.updateProgressAnimator.start();
        this.debouncer.publish(new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.CameraXVideoCaptureHelper$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                CameraXVideoCaptureHelper.this.onVideoCaptureComplete();
            }
        });
    }

    private void shrinkCaptureArea() {
        Size screenSize = getScreenSize();
        Size videoRecordingSize = VideoUtil.getVideoRecordingSize();
        float surfaceScaleForRecording = getSurfaceScaleForRecording();
        float width = ((float) videoRecordingSize.getWidth()) * surfaceScaleForRecording;
        float width2 = width / ((float) screenSize.getWidth());
        if (width2 == 1.0f) {
            float height = ((float) videoRecordingSize.getHeight()) * surfaceScaleForRecording;
            if (((float) screenSize.getHeight()) != height) {
                this.cameraMetricsAnimator = ValueAnimator.ofFloat((float) screenSize.getHeight(), height);
            } else {
                return;
            }
        } else if (((float) screenSize.getWidth()) != width) {
            this.cameraMetricsAnimator = ValueAnimator.ofFloat((float) screenSize.getWidth(), width);
        } else {
            return;
        }
        ViewGroup.LayoutParams layoutParams = this.camera.getLayoutParams();
        this.cameraMetricsAnimator.setInterpolator(new LinearInterpolator());
        this.cameraMetricsAnimator.setDuration(200L);
        this.cameraMetricsAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(width2, layoutParams) { // from class: org.thoughtcrime.securesms.mediasend.CameraXVideoCaptureHelper$$ExternalSyntheticLambda1
            public final /* synthetic */ float f$1;
            public final /* synthetic */ ViewGroup.LayoutParams f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                CameraXVideoCaptureHelper.this.lambda$shrinkCaptureArea$2(this.f$1, this.f$2, valueAnimator);
            }
        });
        this.cameraMetricsAnimator.start();
    }

    public /* synthetic */ void lambda$shrinkCaptureArea$2(float f, ViewGroup.LayoutParams layoutParams, ValueAnimator valueAnimator) {
        if (f == 1.0f) {
            layoutParams.height = Math.round(((Float) valueAnimator.getAnimatedValue()).floatValue());
        } else {
            layoutParams.width = Math.round(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
        this.camera.setLayoutParams(layoutParams);
    }

    private Size getScreenSize() {
        DisplayMetrics displayMetrics = this.camera.getResources().getDisplayMetrics();
        return new Size(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    private float getSurfaceScaleForRecording() {
        Size videoRecordingSize = VideoUtil.getVideoRecordingSize();
        Size screenSize = getScreenSize();
        return ((float) Math.min(screenSize.getHeight(), screenSize.getWidth())) / ((float) Math.min(videoRecordingSize.getHeight(), videoRecordingSize.getWidth()));
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraButtonView.VideoCaptureListener
    public void onVideoCaptureComplete() {
        this.isRecording = false;
        if (canRecordAudio()) {
            Log.d(TAG, "onVideoCaptureComplete");
            this.camera.stopRecording();
            ValueAnimator valueAnimator = this.cameraMetricsAnimator;
            if (valueAnimator != null && valueAnimator.isRunning()) {
                this.cameraMetricsAnimator.reverse();
            }
            this.updateProgressAnimator.cancel();
            this.debouncer.clear();
        }
    }

    @Override // org.thoughtcrime.securesms.mediasend.CameraButtonView.VideoCaptureListener
    public void onZoomIncremented(float f) {
        float maxZoomRatio = this.camera.getMaxZoomRatio() - this.camera.getMinZoomRatio();
        SignalCameraView signalCameraView = this.camera;
        signalCameraView.setZoomRatio((maxZoomRatio * f) + signalCameraView.getMinZoomRatio());
    }

    public static MemoryFileDescriptor createFileDescriptor(Context context) throws MemoryFileDescriptor.MemoryFileException {
        return MemoryFileDescriptor.newMemoryFileDescriptor(context, VIDEO_DEBUG_LABEL, VIDEO_SIZE);
    }
}
