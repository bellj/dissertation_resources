package org.thoughtcrime.securesms.mediasend.v2.images;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.v2.HudCommand;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;
import org.thoughtcrime.securesms.scribbles.ImageEditorFragment;
import org.thoughtcrime.securesms.scribbles.ImageEditorHudV2;

/* compiled from: MediaReviewImagePageFragment.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 '2\u00020\u00012\u00020\u0002:\u0001'B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u000e\u001a\u00020\u0007H\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\b\u0010\u0011\u001a\u00020\u0010H\u0016J\b\u0010\u0012\u001a\u00020\u0010H\u0016J\b\u0010\u0013\u001a\u00020\u0010H\u0016J\b\u0010\u0014\u001a\u00020\u0010H\u0016J\u0018\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0017H\u0016J\b\u0010\u0019\u001a\u00020\u0010H\u0016J\u0010\u0010\u001a\u001a\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u00102\u0006\u0010\u001e\u001a\u00020\u0017H\u0016J\u001a\u0010\u001f\u001a\u00020\u00102\u0006\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010\u001cH\u0016J\u0012\u0010#\u001a\u00020\u00102\b\u0010\"\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010$\u001a\u00020%H\u0002J\b\u0010&\u001a\u00020\u0010H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X.¢\u0006\u0002\n\u0000R\u001b\u0010\b\u001a\u00020\t8BX\u0002¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000b¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/images/MediaReviewImagePageFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/scribbles/ImageEditorFragment$Controller;", "()V", "hudCommandDisposable", "Lio/reactivex/rxjava3/disposables/Disposable;", "imageEditorFragment", "Lorg/thoughtcrime/securesms/scribbles/ImageEditorFragment;", "sharedViewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "getSharedViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "sharedViewModel$delegate", "Lkotlin/Lazy;", "ensureImageEditorFragment", "onCancelEditing", "", "onDoneEditing", "onMainImageFailedToLoad", "onMainImageLoaded", "onPause", "onRequestFullScreen", "fullScreen", "", "hideKeyboard", "onResume", "onSaveInstanceState", "outState", "Landroid/os/Bundle;", "onTouchEventsNeeded", "needed", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "onViewStateRestored", "requireUri", "Landroid/net/Uri;", "restoreImageEditorState", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaReviewImagePageFragment extends Fragment implements ImageEditorFragment.Controller {
    private static final String ARG_URI;
    public static final Companion Companion = new Companion(null);
    private Disposable hudCommandDisposable;
    private ImageEditorFragment imageEditorFragment;
    private final Lazy sharedViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MediaSelectionViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.images.MediaReviewImagePageFragment$sharedViewModel$2
        final /* synthetic */ MediaReviewImagePageFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.images.MediaReviewImagePageFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onRequestFullScreen(boolean z, boolean z2) {
    }

    public MediaReviewImagePageFragment() {
        super(R.layout.fragment_container);
    }

    private final MediaSelectionViewModel getSharedViewModel() {
        return (MediaSelectionViewModel) this.sharedViewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        this.imageEditorFragment = ensureImageEditorFragment();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Disposable disposable = this.hudCommandDisposable;
        if (disposable == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hudCommandDisposable");
            disposable = null;
        }
        disposable.dispose();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Disposable subscribe = getSharedViewModel().getHudCommands().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.v2.images.MediaReviewImagePageFragment$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                MediaReviewImagePageFragment.$r8$lambda$ayS8lkmMLG4AhSLvYS5ZQeF5QLo(MediaReviewImagePageFragment.this, (HudCommand) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "sharedViewModel.hudComma…t\n        }\n      }\n    }");
        this.hudCommandDisposable = subscribe;
    }

    /* renamed from: onResume$lambda-2 */
    public static final void m2181onResume$lambda2(MediaReviewImagePageFragment mediaReviewImagePageFragment, HudCommand hudCommand) {
        Intrinsics.checkNotNullParameter(mediaReviewImagePageFragment, "this$0");
        if (!mediaReviewImagePageFragment.isResumed()) {
            return;
        }
        if (Intrinsics.areEqual(hudCommand, HudCommand.StartDraw.INSTANCE)) {
            mediaReviewImagePageFragment.getSharedViewModel().setTouchEnabled(false);
            mediaReviewImagePageFragment.requireView().postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.v2.images.MediaReviewImagePageFragment$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    MediaReviewImagePageFragment.$r8$lambda$EjfKLlYd3xTbbdOCgmV2SX2ulVY(MediaReviewImagePageFragment.this);
                }
            }, MediaReviewImagePageFragmentKt.MODE_DELAY);
        } else if (Intrinsics.areEqual(hudCommand, HudCommand.StartCropAndRotate.INSTANCE)) {
            mediaReviewImagePageFragment.getSharedViewModel().setTouchEnabled(false);
            mediaReviewImagePageFragment.requireView().postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.v2.images.MediaReviewImagePageFragment$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    MediaReviewImagePageFragment.$r8$lambda$5r6klGW60DdAbS_0FASfBdN7248(MediaReviewImagePageFragment.this);
                }
            }, MediaReviewImagePageFragmentKt.MODE_DELAY);
        } else if (Intrinsics.areEqual(hudCommand, HudCommand.SaveMedia.INSTANCE)) {
            ImageEditorFragment imageEditorFragment = mediaReviewImagePageFragment.imageEditorFragment;
            if (imageEditorFragment == null) {
                Intrinsics.throwUninitializedPropertyAccessException("imageEditorFragment");
                imageEditorFragment = null;
            }
            imageEditorFragment.onSave();
        }
    }

    /* renamed from: onResume$lambda-2$lambda-0 */
    public static final void m2182onResume$lambda2$lambda0(MediaReviewImagePageFragment mediaReviewImagePageFragment) {
        Intrinsics.checkNotNullParameter(mediaReviewImagePageFragment, "this$0");
        ImageEditorFragment imageEditorFragment = mediaReviewImagePageFragment.imageEditorFragment;
        if (imageEditorFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("imageEditorFragment");
            imageEditorFragment = null;
        }
        imageEditorFragment.setMode(ImageEditorHudV2.Mode.DRAW);
    }

    /* renamed from: onResume$lambda-2$lambda-1 */
    public static final void m2183onResume$lambda2$lambda1(MediaReviewImagePageFragment mediaReviewImagePageFragment) {
        Intrinsics.checkNotNullParameter(mediaReviewImagePageFragment, "this$0");
        ImageEditorFragment imageEditorFragment = mediaReviewImagePageFragment.imageEditorFragment;
        if (imageEditorFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("imageEditorFragment");
            imageEditorFragment = null;
        }
        imageEditorFragment.setMode(ImageEditorHudV2.Mode.CROP);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        restoreImageEditorState();
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "outState");
        super.onSaveInstanceState(bundle);
        MediaSelectionViewModel sharedViewModel = getSharedViewModel();
        Uri requireUri = requireUri();
        ImageEditorFragment imageEditorFragment = this.imageEditorFragment;
        if (imageEditorFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("imageEditorFragment");
            imageEditorFragment = null;
        }
        Object saveState = imageEditorFragment.saveState();
        if (saveState != null) {
            Intrinsics.checkNotNullExpressionValue(saveState, "requireNotNull(imageEditorFragment.saveState())");
            sharedViewModel.setEditorState(requireUri, saveState);
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    private final ImageEditorFragment ensureImageEditorFragment() {
        Fragment findFragmentByTag = getChildFragmentManager().findFragmentByTag("image.editor.fragment");
        ImageEditorFragment imageEditorFragment = findFragmentByTag instanceof ImageEditorFragment ? (ImageEditorFragment) findFragmentByTag : null;
        if (imageEditorFragment != null) {
            getSharedViewModel().sendCommand(HudCommand.ResumeEntryTransition.INSTANCE);
            return imageEditorFragment;
        }
        ImageEditorFragment newInstance = ImageEditorFragment.newInstance(requireUri());
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, newInstance, "image.editor.fragment").commitAllowingStateLoss();
        Intrinsics.checkNotNullExpressionValue(newInstance, "{\n      val imageEditorF…imageEditorFragment\n    }");
        return newInstance;
    }

    private final Uri requireUri() {
        Parcelable parcelable = requireArguments().getParcelable(ARG_URI);
        if (parcelable != null) {
            return (Uri) parcelable;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onTouchEventsNeeded(boolean z) {
        if (!isResumed()) {
            return;
        }
        if (!z) {
            requireView().postDelayed(new Runnable(z) { // from class: org.thoughtcrime.securesms.mediasend.v2.images.MediaReviewImagePageFragment$$ExternalSyntheticLambda3
                public final /* synthetic */ boolean f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    MediaReviewImagePageFragment.$r8$lambda$6k_JhNAUKyKQk5cO25HrzgujG1Y(MediaReviewImagePageFragment.this, this.f$1);
                }
            }, MediaReviewImagePageFragmentKt.MODE_DELAY);
        } else {
            getSharedViewModel().setTouchEnabled(!z);
        }
    }

    /* renamed from: onTouchEventsNeeded$lambda-3 */
    public static final void m2184onTouchEventsNeeded$lambda3(MediaReviewImagePageFragment mediaReviewImagePageFragment, boolean z) {
        Intrinsics.checkNotNullParameter(mediaReviewImagePageFragment, "this$0");
        mediaReviewImagePageFragment.getSharedViewModel().setTouchEnabled(!z);
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onDoneEditing() {
        ImageEditorFragment imageEditorFragment = this.imageEditorFragment;
        ImageEditorFragment imageEditorFragment2 = null;
        if (imageEditorFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("imageEditorFragment");
            imageEditorFragment = null;
        }
        imageEditorFragment.setMode(ImageEditorHudV2.Mode.NONE);
        if (isResumed()) {
            MediaSelectionViewModel sharedViewModel = getSharedViewModel();
            Uri requireUri = requireUri();
            ImageEditorFragment imageEditorFragment3 = this.imageEditorFragment;
            if (imageEditorFragment3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("imageEditorFragment");
            } else {
                imageEditorFragment2 = imageEditorFragment3;
            }
            Object saveState = imageEditorFragment2.saveState();
            if (saveState != null) {
                Intrinsics.checkNotNullExpressionValue(saveState, "requireNotNull(imageEditorFragment.saveState())");
                sharedViewModel.setEditorState(requireUri, saveState);
                return;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onCancelEditing() {
        restoreImageEditorState();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onMainImageLoaded() {
        getSharedViewModel().sendCommand(HudCommand.ResumeEntryTransition.INSTANCE);
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorFragment.Controller
    public void onMainImageFailedToLoad() {
        getSharedViewModel().sendCommand(HudCommand.ResumeEntryTransition.INSTANCE);
    }

    private final void restoreImageEditorState() {
        Object editorState = getSharedViewModel().getEditorState(requireUri());
        ImageEditorFragment imageEditorFragment = null;
        ImageEditorFragment.Data data = editorState instanceof ImageEditorFragment.Data ? (ImageEditorFragment.Data) editorState : null;
        if (data != null) {
            ImageEditorFragment imageEditorFragment2 = this.imageEditorFragment;
            if (imageEditorFragment2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("imageEditorFragment");
            } else {
                imageEditorFragment = imageEditorFragment2;
            }
            imageEditorFragment.restoreState(data);
            return;
        }
        ImageEditorFragment imageEditorFragment3 = this.imageEditorFragment;
        if (imageEditorFragment3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("imageEditorFragment");
        } else {
            imageEditorFragment = imageEditorFragment3;
        }
        imageEditorFragment.onClearAll();
    }

    /* compiled from: MediaReviewImagePageFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/images/MediaReviewImagePageFragment$Companion;", "", "()V", "ARG_URI", "", "newInstance", "Landroidx/fragment/app/Fragment;", "uri", "Landroid/net/Uri;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Fragment newInstance(Uri uri) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            MediaReviewImagePageFragment mediaReviewImagePageFragment = new MediaReviewImagePageFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(MediaReviewImagePageFragment.ARG_URI, uri);
            mediaReviewImagePageFragment.setArguments(bundle);
            return mediaReviewImagePageFragment;
        }
    }
}
