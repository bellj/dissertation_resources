package org.thoughtcrime.securesms.mediasend.v2.capture;

import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.providers.BlobProvider;

/* compiled from: MediaCaptureRepository.kt */
/* access modifiers changed from: package-private */
@Metadata(d1 = {"\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\t\n\u0000\u0010\u0000\u001a\u00060\u0001R\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/providers/BlobProvider$BlobBuilder;", "Lorg/thoughtcrime/securesms/providers/BlobProvider;", "blobProvider", "bytes", "", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaCaptureRepository$renderImageToMedia$1$media$3 extends Lambda implements Function3<BlobProvider, byte[], Long, BlobProvider.BlobBuilder> {
    public static final MediaCaptureRepository$renderImageToMedia$1$media$3 INSTANCE = new MediaCaptureRepository$renderImageToMedia$1$media$3();

    MediaCaptureRepository$renderImageToMedia$1$media$3() {
        super(3);
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ BlobProvider.BlobBuilder invoke(Object obj, Object obj2, Object obj3) {
        return invoke((BlobProvider) obj, (byte[]) obj2, ((Number) obj3).longValue());
    }

    public final BlobProvider.BlobBuilder invoke(BlobProvider blobProvider, byte[] bArr, long j) {
        Intrinsics.checkNotNullParameter(blobProvider, "blobProvider");
        Intrinsics.checkNotNullParameter(bArr, "bytes");
        BlobProvider.MemoryBlobBuilder forData = blobProvider.forData(bArr);
        Intrinsics.checkNotNullExpressionValue(forData, "blobProvider.forData(bytes)");
        return forData;
    }
}
