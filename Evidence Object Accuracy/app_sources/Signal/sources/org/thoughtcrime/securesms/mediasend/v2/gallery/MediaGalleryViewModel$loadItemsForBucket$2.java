package org.thoughtcrime.securesms.mediasend.v2.gallery;

import com.annimon.stream.function.Function;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectableItem;

/* compiled from: MediaGalleryViewModel.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "media", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGalleryViewModel$loadItemsForBucket$2 extends Lambda implements Function1<List<? extends Media>, Unit> {
    final /* synthetic */ String $bucketId;
    final /* synthetic */ String $bucketTitle;
    final /* synthetic */ MediaGalleryViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MediaGalleryViewModel$loadItemsForBucket$2(MediaGalleryViewModel mediaGalleryViewModel, String str, String str2) {
        super(1);
        this.this$0 = mediaGalleryViewModel;
        this.$bucketId = str;
        this.$bucketTitle = str2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends Media> list) {
        invoke(list);
        return Unit.INSTANCE;
    }

    public final void invoke(List<? extends Media> list) {
        Intrinsics.checkNotNullParameter(list, "media");
        this.this$0.store.update(new Function(this.$bucketId, this.$bucketTitle, list) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryViewModel$loadItemsForBucket$2$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;
            public final /* synthetic */ String f$1;
            public final /* synthetic */ List f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MediaGalleryViewModel$loadItemsForBucket$2.m2179invoke$lambda1(this.f$0, this.f$1, this.f$2, (MediaGalleryState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-1 */
    public static final MediaGalleryState m2179invoke$lambda1(String str, String str2, List list, MediaGalleryState mediaGalleryState) {
        Intrinsics.checkNotNullParameter(list, "$media");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(new MediaGallerySelectableItem.FileModel((Media) it.next(), false, 0));
        }
        return mediaGalleryState.copy(str, str2, arrayList);
    }
}
