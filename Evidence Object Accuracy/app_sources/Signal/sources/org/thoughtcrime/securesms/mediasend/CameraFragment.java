package org.thoughtcrime.securesms.mediasend;

import androidx.fragment.app.Fragment;
import io.reactivex.rxjava3.core.Flowable;
import j$.util.Optional;
import java.io.FileDescriptor;
import org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil;
import org.thoughtcrime.securesms.mms.MediaConstraints;

/* loaded from: classes4.dex */
public interface CameraFragment {
    public static final float PORTRAIT_ASPECT_RATIO;

    /* loaded from: classes4.dex */
    public interface Controller {
        int getMaxVideoDuration();

        MediaConstraints getMediaConstraints();

        Flowable<Optional<Media>> getMostRecentMediaItem();

        void onCameraCountButtonClicked();

        void onCameraError();

        void onGalleryClicked();

        void onImageCaptured(byte[] bArr, int i, int i2);

        void onVideoCaptureError();

        void onVideoCaptured(FileDescriptor fileDescriptor);
    }

    void fadeInControls();

    void fadeOutControls(Runnable runnable);

    void presentHud(int i);

    /* renamed from: org.thoughtcrime.securesms.mediasend.CameraFragment$-CC */
    /* loaded from: classes4.dex */
    public final /* synthetic */ class CC {
        public static float getAspectRatioForOrientation(int i) {
            return i == 1 ? 0.5625f : 1.7777778f;
        }

        public static Fragment newInstance() {
            if (CameraXUtil.isSupported()) {
                return CameraXFragment.newInstance();
            }
            return Camera1Fragment.newInstance();
        }

        public static Fragment newInstanceForAvatarCapture() {
            if (CameraXUtil.isSupported()) {
                return CameraXFragment.newInstanceForAvatarCapture();
            }
            return Camera1Fragment.newInstance();
        }
    }
}
