package org.thoughtcrime.securesms.mediasend.v2.gallery;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.widget.Toolbar;
import androidx.arch.core.util.Function;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.recyclerview.GridDividerDecoration;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.MediaFolder;
import org.thoughtcrime.securesms.mediasend.MediaRepository;
import org.thoughtcrime.securesms.mediasend.v2.MediaCountIndicatorButton;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectableItem;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectedItem;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryViewModel;
import org.thoughtcrime.securesms.util.Material3OnScrollHelper;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: MediaGalleryFragment.kt */
@Metadata(d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0002)*B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u0011J\u0006\u0010\"\u001a\u00020 J\u001a\u0010#\u001a\u00020 2\u0006\u0010$\u001a\u00020\u00042\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J\u000e\u0010'\u001a\u00020 2\u0006\u0010(\u001a\u00020\u001dR\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u0017\u0010\u0018R\u001c\u0010\u001b\u001a\u0010\u0012\f\u0012\n \u001e*\u0004\u0018\u00010\u001d0\u001d0\u001cX\u0004¢\u0006\u0002\n\u0000¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryFragment;", "Landroidx/fragment/app/Fragment;", "()V", "bottomBarGroup", "Landroid/view/View;", "callbacks", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryFragment$Callbacks;", "countButton", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaCountIndicatorButton;", "galleryAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "galleryRecycler", "Landroidx/recyclerview/widget/RecyclerView;", "onBackPressedCallback", "Landroidx/activity/OnBackPressedCallback;", "selectedAdapter", "selectedMediaTouchHelper", "Landroidx/recyclerview/widget/ItemTouchHelper;", "selectedRecycler", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "viewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "viewStateLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryFragment$ViewState;", "kotlin.jvm.PlatformType", "bindSelectedMediaItemDragHelper", "", "helper", "onBack", "onViewCreated", "view", "savedInstanceState", "Landroid/os/Bundle;", "onViewStateUpdated", "state", "Callbacks", "ViewState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGalleryFragment extends Fragment {
    private View bottomBarGroup;
    private Callbacks callbacks;
    private MediaCountIndicatorButton countButton;
    private final MappingAdapter galleryAdapter = new MappingAdapter();
    private RecyclerView galleryRecycler;
    private final OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$onBackPressedCallback$1
        final /* synthetic */ MediaGalleryFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // androidx.activity.OnBackPressedCallback
        public void handleOnBackPressed() {
            this.this$0.onBack();
        }
    };
    private final MappingAdapter selectedAdapter = new MappingAdapter();
    private ItemTouchHelper selectedMediaTouchHelper;
    private RecyclerView selectedRecycler;
    private Toolbar toolbar;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MediaGalleryViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$viewModel$2
        final /* synthetic */ MediaGalleryFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            return new MediaGalleryViewModel.Factory(null, null, new MediaGalleryRepository(requireContext, new MediaRepository()));
        }
    });
    private final MutableLiveData<ViewState> viewStateLiveData = new MutableLiveData<>(new ViewState(null, 1, null));

    public MediaGalleryFragment() {
        super(R.layout.v2_media_gallery_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Callbacks callbacks;
        Intrinsics.checkNotNullParameter(view, "view");
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        callbacks = (Callbacks) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks");
                    }
                } else if (fragment instanceof Callbacks) {
                    callbacks = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.callbacks = callbacks;
            View findViewById = view.findViewById(R.id.media_gallery_toolbar);
            Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.media_gallery_toolbar)");
            this.toolbar = (Toolbar) findViewById;
            View findViewById2 = view.findViewById(R.id.media_gallery_grid);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.media_gallery_grid)");
            this.galleryRecycler = (RecyclerView) findViewById2;
            View findViewById3 = view.findViewById(R.id.media_gallery_selected);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.media_gallery_selected)");
            this.selectedRecycler = (RecyclerView) findViewById3;
            View findViewById4 = view.findViewById(R.id.media_gallery_count_button);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.media_gallery_count_button)");
            this.countButton = (MediaCountIndicatorButton) findViewById4;
            View findViewById5 = view.findViewById(R.id.media_gallery_bottom_bar_group);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.m…gallery_bottom_bar_group)");
            this.bottomBarGroup = findViewById5;
            RecyclerView recyclerView = this.galleryRecycler;
            RecyclerView recyclerView2 = null;
            if (recyclerView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("galleryRecycler");
                recyclerView = null;
            }
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (layoutManager != null) {
                ((GridLayoutManager) layoutManager).setSpanSizeLookup(new MediaGalleryFragment$onViewCreated$1(this));
                Toolbar toolbar = this.toolbar;
                if (toolbar == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("toolbar");
                    toolbar = null;
                }
                toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$$ExternalSyntheticLambda0
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        MediaGalleryFragment.m2154onViewCreated$lambda0(MediaGalleryFragment.this, view2);
                    }
                });
                FragmentActivity requireActivity2 = requireActivity();
                Intrinsics.checkNotNullExpressionValue(requireActivity2, "requireActivity()");
                Toolbar toolbar2 = this.toolbar;
                if (toolbar2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("toolbar");
                    toolbar2 = null;
                }
                Material3OnScrollHelper material3OnScrollHelper = new Material3OnScrollHelper(requireActivity2, toolbar2);
                RecyclerView recyclerView3 = this.galleryRecycler;
                if (recyclerView3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("galleryRecycler");
                    recyclerView3 = null;
                }
                material3OnScrollHelper.attach(recyclerView3);
                Callbacks callbacks2 = this.callbacks;
                if (callbacks2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("callbacks");
                    callbacks2 = null;
                }
                if (callbacks2.isCameraEnabled()) {
                    Toolbar toolbar3 = this.toolbar;
                    if (toolbar3 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("toolbar");
                        toolbar3 = null;
                    }
                    toolbar3.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$$ExternalSyntheticLambda1
                        @Override // androidx.appcompat.widget.Toolbar.OnMenuItemClickListener
                        public final boolean onMenuItemClick(MenuItem menuItem) {
                            return MediaGalleryFragment.m2155onViewCreated$lambda1(MediaGalleryFragment.this, menuItem);
                        }
                    });
                } else {
                    Toolbar toolbar4 = this.toolbar;
                    if (toolbar4 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("toolbar");
                        toolbar4 = null;
                    }
                    toolbar4.getMenu().findItem(R.id.action_camera).setVisible(false);
                }
                MediaCountIndicatorButton mediaCountIndicatorButton = this.countButton;
                if (mediaCountIndicatorButton == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("countButton");
                    mediaCountIndicatorButton = null;
                }
                mediaCountIndicatorButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$$ExternalSyntheticLambda2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        MediaGalleryFragment.m2158onViewCreated$lambda2(MediaGalleryFragment.this, view2);
                    }
                });
                MediaGallerySelectedItem.INSTANCE.register(this.selectedAdapter, new Function1<Media, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$onViewCreated$5
                    final /* synthetic */ MediaGalleryFragment this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                    }

                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Media media) {
                        invoke(media);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(Media media) {
                        Intrinsics.checkNotNullParameter(media, "media");
                        MediaGalleryFragment.Callbacks callbacks3 = this.this$0.callbacks;
                        if (callbacks3 == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("callbacks");
                            callbacks3 = null;
                        }
                        callbacks3.onSelectedMediaClicked(media);
                    }
                });
                RecyclerView recyclerView4 = this.selectedRecycler;
                if (recyclerView4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("selectedRecycler");
                    recyclerView4 = null;
                }
                recyclerView4.setAdapter(this.selectedAdapter);
                ItemTouchHelper itemTouchHelper = this.selectedMediaTouchHelper;
                if (itemTouchHelper != null) {
                    RecyclerView recyclerView5 = this.selectedRecycler;
                    if (recyclerView5 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("selectedRecycler");
                        recyclerView5 = null;
                    }
                    itemTouchHelper.attachToRecyclerView(recyclerView5);
                }
                MediaGallerySelectableItem mediaGallerySelectableItem = MediaGallerySelectableItem.INSTANCE;
                MappingAdapter mappingAdapter = this.galleryAdapter;
                MediaGalleryFragment$onViewCreated$6 mediaGalleryFragment$onViewCreated$6 = new Function1<MediaFolder, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$onViewCreated$6
                    final /* synthetic */ MediaGalleryFragment this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                    }

                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(MediaFolder mediaFolder) {
                        invoke(mediaFolder);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(MediaFolder mediaFolder) {
                        Intrinsics.checkNotNullParameter(mediaFolder, "it");
                        this.this$0.onBackPressedCallback.setEnabled(true);
                        this.this$0.getViewModel().setMediaFolder(mediaFolder);
                    }
                };
                MediaGalleryFragment$onViewCreated$7 mediaGalleryFragment$onViewCreated$7 = new Function2<Media, Boolean, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$onViewCreated$7
                    final /* synthetic */ MediaGalleryFragment this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                    }

                    /* Return type fixed from 'java.lang.Object' to match base method */
                    @Override // kotlin.jvm.functions.Function2
                    public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                        invoke((Media) obj, ((Boolean) obj2).booleanValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(Media media, boolean z) {
                        Intrinsics.checkNotNullParameter(media, "media");
                        MediaGalleryFragment.Callbacks callbacks3 = null;
                        if (z) {
                            MediaGalleryFragment.Callbacks callbacks4 = this.this$0.callbacks;
                            if (callbacks4 == null) {
                                Intrinsics.throwUninitializedPropertyAccessException("callbacks");
                            } else {
                                callbacks3 = callbacks4;
                            }
                            callbacks3.onMediaUnselected(media);
                            return;
                        }
                        MediaGalleryFragment.Callbacks callbacks5 = this.this$0.callbacks;
                        if (callbacks5 == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("callbacks");
                        } else {
                            callbacks3 = callbacks5;
                        }
                        callbacks3.onMediaSelected(media);
                    }
                };
                Callbacks callbacks3 = this.callbacks;
                if (callbacks3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("callbacks");
                    callbacks3 = null;
                }
                mediaGallerySelectableItem.registerAdapter(mappingAdapter, mediaGalleryFragment$onViewCreated$6, mediaGalleryFragment$onViewCreated$7, callbacks3.isMultiselectEnabled());
                RecyclerView recyclerView6 = this.galleryRecycler;
                if (recyclerView6 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("galleryRecycler");
                    recyclerView6 = null;
                }
                recyclerView6.setAdapter(this.galleryAdapter);
                RecyclerView recyclerView7 = this.galleryRecycler;
                if (recyclerView7 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("galleryRecycler");
                } else {
                    recyclerView2 = recyclerView7;
                }
                recyclerView2.addItemDecoration(new GridDividerDecoration(4, ViewUtil.dpToPx(2)));
                this.viewStateLiveData.observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$$ExternalSyntheticLambda3
                    @Override // androidx.lifecycle.Observer
                    public final void onChanged(Object obj) {
                        MediaGalleryFragment.m2159onViewCreated$lambda5(MediaGalleryFragment.this, (MediaGalleryFragment.ViewState) obj);
                    }
                });
                getViewModel().getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$$ExternalSyntheticLambda4
                    @Override // androidx.lifecycle.Observer
                    public final void onChanged(Object obj) {
                        MediaGalleryFragment.m2161onViewCreated$lambda6(MediaGalleryFragment.this, (MediaGalleryState) obj);
                    }
                });
                LiveDataUtil.combineLatest(Transformations.map(getViewModel().getState(), new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$$ExternalSyntheticLambda5
                    @Override // androidx.arch.core.util.Function
                    public final Object apply(Object obj) {
                        return MediaGalleryFragment.m2162onViewCreated$lambda7((MediaGalleryState) obj);
                    }
                }), Transformations.map(this.viewStateLiveData, new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$$ExternalSyntheticLambda6
                    @Override // androidx.arch.core.util.Function
                    public final Object apply(Object obj) {
                        return MediaGalleryFragment.m2163onViewCreated$lambda8((MediaGalleryFragment.ViewState) obj);
                    }
                }), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$$ExternalSyntheticLambda7
                    @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
                    public final Object apply(Object obj, Object obj2) {
                        return MediaGalleryFragment.m2156onViewCreated$lambda10((List) obj, (List) obj2);
                    }
                }).observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$$ExternalSyntheticLambda8
                    @Override // androidx.lifecycle.Observer
                    public final void onChanged(Object obj) {
                        MediaGalleryFragment.m2157onViewCreated$lambda11(MediaGalleryFragment.this, (List) obj);
                    }
                });
                requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), this.onBackPressedCallback);
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type androidx.recyclerview.widget.GridLayoutManager");
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    public final MediaGalleryViewModel getViewModel() {
        return (MediaGalleryViewModel) this.viewModel$delegate.getValue();
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2154onViewCreated$lambda0(MediaGalleryFragment mediaGalleryFragment, View view) {
        Intrinsics.checkNotNullParameter(mediaGalleryFragment, "this$0");
        mediaGalleryFragment.onBack();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final boolean m2155onViewCreated$lambda1(MediaGalleryFragment mediaGalleryFragment, MenuItem menuItem) {
        Intrinsics.checkNotNullParameter(mediaGalleryFragment, "this$0");
        if (menuItem.getItemId() != R.id.action_camera) {
            return false;
        }
        Callbacks callbacks = mediaGalleryFragment.callbacks;
        if (callbacks == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callbacks");
            callbacks = null;
        }
        callbacks.onNavigateToCamera();
        return true;
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m2158onViewCreated$lambda2(MediaGalleryFragment mediaGalleryFragment, View view) {
        Intrinsics.checkNotNullParameter(mediaGalleryFragment, "this$0");
        Callbacks callbacks = mediaGalleryFragment.callbacks;
        if (callbacks == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callbacks");
            callbacks = null;
        }
        callbacks.onSubmit();
    }

    /* renamed from: onViewCreated$lambda-5 */
    public static final void m2159onViewCreated$lambda5(MediaGalleryFragment mediaGalleryFragment, ViewState viewState) {
        Intrinsics.checkNotNullParameter(mediaGalleryFragment, "this$0");
        View view = mediaGalleryFragment.bottomBarGroup;
        MediaCountIndicatorButton mediaCountIndicatorButton = null;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("bottomBarGroup");
            view = null;
        }
        ViewExtensionsKt.setVisible(view, !viewState.getSelectedMedia().isEmpty());
        MediaCountIndicatorButton mediaCountIndicatorButton2 = mediaGalleryFragment.countButton;
        if (mediaCountIndicatorButton2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("countButton");
        } else {
            mediaCountIndicatorButton = mediaCountIndicatorButton2;
        }
        mediaCountIndicatorButton.setCount(viewState.getSelectedMedia().size());
        Stopwatch stopwatch = new Stopwatch("mediaSubmit");
        MappingAdapter mappingAdapter = mediaGalleryFragment.selectedAdapter;
        List<Media> selectedMedia = viewState.getSelectedMedia();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(selectedMedia, 10));
        for (Media media : selectedMedia) {
            arrayList.add(new MediaGallerySelectedItem.Model(media));
        }
        mappingAdapter.submitList(arrayList, new Runnable(viewState, mediaGalleryFragment) { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$$ExternalSyntheticLambda9
            public final /* synthetic */ MediaGalleryFragment.ViewState f$1;
            public final /* synthetic */ MediaGalleryFragment f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaGalleryFragment.m2160onViewCreated$lambda5$lambda4(Stopwatch.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-5$lambda-4 */
    public static final void m2160onViewCreated$lambda5$lambda4(Stopwatch stopwatch, ViewState viewState, MediaGalleryFragment mediaGalleryFragment) {
        Intrinsics.checkNotNullParameter(stopwatch, "$stopwatch");
        Intrinsics.checkNotNullParameter(mediaGalleryFragment, "this$0");
        stopwatch.split("after-submit");
        stopwatch.stop("MediaGalleryFragment");
        if (!viewState.getSelectedMedia().isEmpty()) {
            RecyclerView recyclerView = mediaGalleryFragment.selectedRecycler;
            if (recyclerView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("selectedRecycler");
                recyclerView = null;
            }
            recyclerView.smoothScrollToPosition(viewState.getSelectedMedia().size() - 1);
        }
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m2161onViewCreated$lambda6(MediaGalleryFragment mediaGalleryFragment, MediaGalleryState mediaGalleryState) {
        Intrinsics.checkNotNullParameter(mediaGalleryFragment, "this$0");
        Toolbar toolbar = mediaGalleryFragment.toolbar;
        if (toolbar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("toolbar");
            toolbar = null;
        }
        String bucketTitle = mediaGalleryState.getBucketTitle();
        if (bucketTitle == null) {
            bucketTitle = mediaGalleryFragment.requireContext().getString(R.string.AttachmentKeyboard_gallery);
        }
        toolbar.setTitle(bucketTitle);
    }

    /* renamed from: onViewCreated$lambda-7 */
    public static final List m2162onViewCreated$lambda7(MediaGalleryState mediaGalleryState) {
        return mediaGalleryState.getItems();
    }

    /* renamed from: onViewCreated$lambda-8 */
    public static final List m2163onViewCreated$lambda8(ViewState viewState) {
        return viewState.getSelectedMedia();
    }

    /* renamed from: onViewCreated$lambda-11 */
    public static final void m2157onViewCreated$lambda11(MediaGalleryFragment mediaGalleryFragment, List list) {
        Intrinsics.checkNotNullParameter(mediaGalleryFragment, "this$0");
        mediaGalleryFragment.galleryAdapter.submitList(list);
    }

    public final void onBack() {
        if (getViewModel().pop()) {
            this.onBackPressedCallback.setEnabled(false);
            Callbacks callbacks = this.callbacks;
            if (callbacks == null) {
                Intrinsics.throwUninitializedPropertyAccessException("callbacks");
                callbacks = null;
            }
            callbacks.onToolbarNavigationClicked();
        }
    }

    public final void onViewStateUpdated(ViewState viewState) {
        Intrinsics.checkNotNullParameter(viewState, "state");
        this.viewStateLiveData.setValue(viewState);
    }

    public final void bindSelectedMediaItemDragHelper(ItemTouchHelper itemTouchHelper) {
        Intrinsics.checkNotNullParameter(itemTouchHelper, "helper");
        this.selectedMediaTouchHelper = itemTouchHelper;
    }

    /* compiled from: MediaGalleryFragment.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryFragment$ViewState;", "", "selectedMedia", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "(Ljava/util/List;)V", "getSelectedMedia", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewState {
        private final List<Media> selectedMedia;

        public ViewState() {
            this(null, 1, null);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$ViewState */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                list = viewState.selectedMedia;
            }
            return viewState.copy(list);
        }

        public final List<Media> component1() {
            return this.selectedMedia;
        }

        public final ViewState copy(List<? extends Media> list) {
            Intrinsics.checkNotNullParameter(list, "selectedMedia");
            return new ViewState(list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof ViewState) && Intrinsics.areEqual(this.selectedMedia, ((ViewState) obj).selectedMedia);
        }

        public int hashCode() {
            return this.selectedMedia.hashCode();
        }

        public String toString() {
            return "ViewState(selectedMedia=" + this.selectedMedia + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.mediasend.Media> */
        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(List<? extends Media> list) {
            Intrinsics.checkNotNullParameter(list, "selectedMedia");
            this.selectedMedia = list;
        }

        public /* synthetic */ ViewState(List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list);
        }

        public final List<Media> getSelectedMedia() {
            return this.selectedMedia;
        }
    }

    /* compiled from: MediaGalleryFragment.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\b\u0010\u0004\u001a\u00020\u0003H\u0016J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH&J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\n\u001a\u00020\u0006H\u0016J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\f\u001a\u00020\u0006H\u0016J\b\u0010\r\u001a\u00020\u0006H\u0016¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryFragment$Callbacks;", "", "isCameraEnabled", "", "isMultiselectEnabled", "onMediaSelected", "", "media", "Lorg/thoughtcrime/securesms/mediasend/Media;", "onMediaUnselected", "onNavigateToCamera", "onSelectedMediaClicked", "onSubmit", "onToolbarNavigationClicked", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callbacks {
        boolean isCameraEnabled();

        boolean isMultiselectEnabled();

        void onMediaSelected(Media media);

        void onMediaUnselected(Media media);

        void onNavigateToCamera();

        void onSelectedMediaClicked(Media media);

        void onSubmit();

        void onToolbarNavigationClicked();

        /* compiled from: MediaGalleryFragment.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class DefaultImpls {
            public static boolean isCameraEnabled(Callbacks callbacks) {
                return true;
            }

            public static boolean isMultiselectEnabled(Callbacks callbacks) {
                return false;
            }

            public static void onMediaUnselected(Callbacks callbacks, Media media) {
                Intrinsics.checkNotNullParameter(media, "media");
                throw new UnsupportedOperationException();
            }

            public static void onSelectedMediaClicked(Callbacks callbacks, Media media) {
                Intrinsics.checkNotNullParameter(media, "media");
                throw new UnsupportedOperationException();
            }

            public static void onNavigateToCamera(Callbacks callbacks) {
                throw new UnsupportedOperationException();
            }

            public static void onSubmit(Callbacks callbacks) {
                throw new UnsupportedOperationException();
            }

            public static void onToolbarNavigationClicked(Callbacks callbacks) {
                throw new UnsupportedOperationException();
            }
        }
    }

    /* renamed from: onViewCreated$lambda-10 */
    public static final List m2156onViewCreated$lambda10(List list, List list2) {
        Intrinsics.checkNotNullParameter(list, "galleryItems");
        Intrinsics.checkNotNullParameter(list2, "selectedMedia");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (Object obj : list) {
            if (obj instanceof MediaGallerySelectableItem.FileModel) {
                MediaGallerySelectableItem.FileModel fileModel = (MediaGallerySelectableItem.FileModel) obj;
                obj = MediaGallerySelectableItem.FileModel.copy$default(fileModel, null, list2.contains(fileModel.getMedia()), list2.indexOf(fileModel.getMedia()) + 1, 1, null);
            }
            arrayList.add(obj);
        }
        return arrayList;
    }
}
