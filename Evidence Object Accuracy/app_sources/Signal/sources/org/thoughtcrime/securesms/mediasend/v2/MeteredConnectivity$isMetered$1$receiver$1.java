package org.thoughtcrime.securesms.mediasend.v2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import androidx.core.net.ConnectivityManagerCompat;
import io.reactivex.rxjava3.core.ObservableEmitter;
import kotlin.Metadata;

/* compiled from: MeteredConnectivity.kt */
@Metadata(d1 = {"\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u001c\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016¨\u0006\b"}, d2 = {"org/thoughtcrime/securesms/mediasend/v2/MeteredConnectivity$isMetered$1$receiver$1", "Landroid/content/BroadcastReceiver;", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MeteredConnectivity$isMetered$1$receiver$1 extends BroadcastReceiver {
    final /* synthetic */ ConnectivityManager $connectivityManager;
    final /* synthetic */ ObservableEmitter<Boolean> $emitter;

    public MeteredConnectivity$isMetered$1$receiver$1(ObservableEmitter<Boolean> observableEmitter, ConnectivityManager connectivityManager) {
        this.$emitter = observableEmitter;
        this.$connectivityManager = connectivityManager;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        this.$emitter.onNext(Boolean.valueOf(ConnectivityManagerCompat.isActiveNetworkMetered(this.$connectivityManager)));
    }
}
