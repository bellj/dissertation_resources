package org.thoughtcrime.securesms.mediasend.v2.review;

import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionState;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.views.CheckedLinearLayout;

/* loaded from: classes4.dex */
public final class QualitySelectorBottomSheetDialog extends BottomSheetDialogFragment {
    private CheckedLinearLayout high;
    private CheckedLinearLayout standard;
    private MediaSelectionViewModel viewModel;

    public static void show(FragmentManager fragmentManager) {
        new QualitySelectorBottomSheetDialog().show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, R.style.Theme_Signal_RoundedBottomSheet);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return LayoutInflater.from(new ContextThemeWrapper(layoutInflater.getContext(), (int) R.style.TextSecure_DarkTheme)).inflate(R.layout.quality_selector_dialog, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.standard = (CheckedLinearLayout) view.findViewById(R.id.quality_selector_dialog_standard);
        this.high = (CheckedLinearLayout) view.findViewById(R.id.quality_selector_dialog_high);
        QualitySelectorBottomSheetDialog$$ExternalSyntheticLambda1 qualitySelectorBottomSheetDialog$$ExternalSyntheticLambda1 = new View.OnClickListener(view) { // from class: org.thoughtcrime.securesms.mediasend.v2.review.QualitySelectorBottomSheetDialog$$ExternalSyntheticLambda1
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                QualitySelectorBottomSheetDialog.this.lambda$onViewCreated$0(this.f$1, view2);
            }
        };
        this.standard.setOnClickListener(qualitySelectorBottomSheetDialog$$ExternalSyntheticLambda1);
        this.high.setOnClickListener(qualitySelectorBottomSheetDialog$$ExternalSyntheticLambda1);
        MediaSelectionViewModel mediaSelectionViewModel = (MediaSelectionViewModel) ViewModelProviders.of(requireActivity()).get(MediaSelectionViewModel.class);
        this.viewModel = mediaSelectionViewModel;
        mediaSelectionViewModel.getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.QualitySelectorBottomSheetDialog$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                QualitySelectorBottomSheetDialog.this.updateQuality((MediaSelectionState) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view, View view2) {
        select(view2);
        view.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.v2.review.QualitySelectorBottomSheetDialog$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                QualitySelectorBottomSheetDialog.this.dismissAllowingStateLoss();
            }
        }, 250);
    }

    public void updateQuality(MediaSelectionState mediaSelectionState) {
        select(mediaSelectionState.getQuality() == SentMediaQuality.STANDARD ? this.standard : this.high);
    }

    private void select(View view) {
        CheckedLinearLayout checkedLinearLayout = this.standard;
        boolean z = true;
        checkedLinearLayout.setChecked(view == checkedLinearLayout);
        CheckedLinearLayout checkedLinearLayout2 = this.high;
        if (view != checkedLinearLayout2) {
            z = false;
        }
        checkedLinearLayout2.setChecked(z);
        this.viewModel.setSentMediaQuality(this.standard == view ? SentMediaQuality.STANDARD : SentMediaQuality.HIGH);
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }
}
