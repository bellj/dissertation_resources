package org.thoughtcrime.securesms.mediasend.v2.capture;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.mediasend.Media;

/* compiled from: MediaCaptureState.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0011\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0015\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureState;", "", "mostRecentMedia", "Lorg/thoughtcrime/securesms/mediasend/Media;", "(Lorg/thoughtcrime/securesms/mediasend/Media;)V", "getMostRecentMedia", "()Lorg/thoughtcrime/securesms/mediasend/Media;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaCaptureState {
    private final Media mostRecentMedia;

    public MediaCaptureState() {
        this(null, 1, null);
    }

    public static /* synthetic */ MediaCaptureState copy$default(MediaCaptureState mediaCaptureState, Media media, int i, Object obj) {
        if ((i & 1) != 0) {
            media = mediaCaptureState.mostRecentMedia;
        }
        return mediaCaptureState.copy(media);
    }

    public final Media component1() {
        return this.mostRecentMedia;
    }

    public final MediaCaptureState copy(Media media) {
        return new MediaCaptureState(media);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof MediaCaptureState) && Intrinsics.areEqual(this.mostRecentMedia, ((MediaCaptureState) obj).mostRecentMedia);
    }

    public int hashCode() {
        Media media = this.mostRecentMedia;
        if (media == null) {
            return 0;
        }
        return media.hashCode();
    }

    public String toString() {
        return "MediaCaptureState(mostRecentMedia=" + this.mostRecentMedia + ')';
    }

    public MediaCaptureState(Media media) {
        this.mostRecentMedia = media;
    }

    public /* synthetic */ MediaCaptureState(Media media, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : media);
    }

    public final Media getMostRecentMedia() {
        return this.mostRecentMedia;
    }
}
