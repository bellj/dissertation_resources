package org.thoughtcrime.securesms.mediasend.v2.text;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.Subject;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contactshare.ContactShareEditActivity;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.fonts.SupportedScript;
import org.thoughtcrime.securesms.fonts.TextFont;
import org.thoughtcrime.securesms.fonts.TextToScript;
import org.thoughtcrime.securesms.fonts.TypefaceCache;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendRepository;
import org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendResult;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: TextStoryPostCreationViewModel.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 D2\u00020\u0001:\u0002DEB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u001c\u001a\u00020\u001dJ\u0006\u0010\u001e\u001a\u00020\u001fJ\b\u0010 \u001a\u00020!H\u0007J\b\u0010\"\u001a\u00020\u001dH\u0014J\u000e\u0010#\u001a\u00020\u001d2\u0006\u0010$\u001a\u00020%J\u000e\u0010&\u001a\u00020\u001d2\u0006\u0010'\u001a\u00020%J$\u0010(\u001a\b\u0012\u0004\u0012\u00020*0)2\f\u0010+\u001a\b\u0012\u0004\u0012\u00020-0,2\b\u0010.\u001a\u0004\u0018\u00010/J\u000e\u00100\u001a\u00020\u001d2\u0006\u00101\u001a\u000202J\u000e\u00103\u001a\u00020\u001d2\u0006\u00104\u001a\u00020\tJ\u000e\u00105\u001a\u00020\u001d2\u0006\u00106\u001a\u00020\u001fJ\u0010\u00107\u001a\u00020\u001d2\b\u00108\u001a\u0004\u0018\u00010\u0015J\u000e\u00109\u001a\u00020\u001d2\u0006\u0010:\u001a\u00020\u0015J\u0010\u0010;\u001a\u00020\u001d2\b\b\u0001\u0010<\u001a\u00020!J\u000e\u0010=\u001a\u00020\u001d2\u0006\u0010>\u001a\u00020?J\u000e\u0010@\u001a\u00020\u001d2\u0006\u0010A\u001a\u00020\u0017J\u000e\u0010B\u001a\u00020\u001d2\u0006\u0010C\u001a\u00020!R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0012X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00170\u0014X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\r¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0010R\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u000b0\r¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0010¨\u0006F"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendRepository;", "(Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "internalThumbnail", "Landroidx/lifecycle/MutableLiveData;", "Landroid/graphics/Bitmap;", "internalTypeface", "Landroid/graphics/Typeface;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "temporaryBodySubject", "Lio/reactivex/rxjava3/subjects/Subject;", "", "textFontSubject", "Lorg/thoughtcrime/securesms/fonts/TextFont;", "thumbnail", "getThumbnail", "typeface", "getTypeface", "cycleBackgroundColor", "", "getBody", "", "getTextColor", "", "onCleared", "restoreFromInstanceState", "inState", "Landroid/os/Bundle;", "saveToInstanceState", "outState", "send", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendResult;", ContactShareEditActivity.KEY_CONTACTS, "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "linkPreview", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;", "setAlignment", "textAlignment", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextAlignment;", "setBitmap", "bitmap", "setBody", "body", "setLinkPreview", "url", "setTemporaryBody", "temporaryBody", "setTextColor", "textColor", "setTextColorStyle", "textColorStyle", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextColorStyle;", "setTextFont", "textFont", "setTextScale", "scale", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryPostCreationViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(TextStoryPostCreationViewModel.class);
    private static final String TEXT_STORY_INSTANCE_STATE;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData<Bitmap> internalThumbnail;
    private final MutableLiveData<Typeface> internalTypeface;
    private final TextStoryPostSendRepository repository;
    private final LiveData<TextStoryPostCreationState> state;
    private final Store<TextStoryPostCreationState> store;
    private final Subject<String> temporaryBodySubject;
    private final Subject<TextFont> textFontSubject;
    private final LiveData<Bitmap> thumbnail;
    private final LiveData<Typeface> typeface;

    /* renamed from: restoreFromInstanceState$lambda-3 */
    public static final TextStoryPostCreationState m2250restoreFromInstanceState$lambda3(TextStoryPostCreationState textStoryPostCreationState, TextStoryPostCreationState textStoryPostCreationState2) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationState, "$state");
        return textStoryPostCreationState;
    }

    public TextStoryPostCreationViewModel(TextStoryPostSendRepository textStoryPostSendRepository) {
        Intrinsics.checkNotNullParameter(textStoryPostSendRepository, "repository");
        this.repository = textStoryPostSendRepository;
        Store<TextStoryPostCreationState> store = new Store<>(new TextStoryPostCreationState(null, 0, null, null, null, 0, null, null, 255, null));
        this.store = store;
        BehaviorSubject create = BehaviorSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.textFontSubject = create;
        BehaviorSubject createDefault = BehaviorSubject.createDefault("");
        Intrinsics.checkNotNullExpressionValue(createDefault, "createDefault(\"\")");
        this.temporaryBodySubject = createDefault;
        MutableLiveData<Bitmap> mutableLiveData = new MutableLiveData<>();
        this.internalThumbnail = mutableLiveData;
        this.thumbnail = mutableLiveData;
        MutableLiveData<Typeface> mutableLiveData2 = new MutableLiveData<>();
        this.internalTypeface = mutableLiveData2;
        LiveData<TextStoryPostCreationState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        this.typeface = mutableLiveData2;
        create.onNext(store.getState().getTextFont());
        Observable.combineLatest(create, createDefault.observeOn(Schedulers.io()).map(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda8
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return TextStoryPostCreationViewModel.m2246_init_$lambda0((String) obj);
            }
        }), new BiFunction() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda9
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return new Pair((TextFont) obj, (SupportedScript) obj2);
            }
        }).observeOn(Schedulers.io()).distinctUntilChanged().switchMapSingle(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda10
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return TextStoryPostCreationViewModel.m2247_init_$lambda1((Pair) obj);
            }
        }).subscribeOn(Schedulers.io()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda11
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                TextStoryPostCreationViewModel.m2248_init_$lambda2(TextStoryPostCreationViewModel.this, (Typeface) obj);
            }
        });
    }

    public final LiveData<Bitmap> getThumbnail() {
        return this.thumbnail;
    }

    public final LiveData<TextStoryPostCreationState> getState() {
        return this.state;
    }

    public final LiveData<Typeface> getTypeface() {
        return this.typeface;
    }

    /* renamed from: _init_$lambda-0 */
    public static final SupportedScript m2246_init_$lambda0(String str) {
        TextToScript textToScript = TextToScript.INSTANCE;
        Intrinsics.checkNotNullExpressionValue(str, "it");
        return textToScript.guessScript(str);
    }

    /* renamed from: _init_$lambda-1 */
    public static final SingleSource m2247_init_$lambda1(Pair pair) {
        TextFont textFont = (TextFont) pair.component1();
        SupportedScript supportedScript = (SupportedScript) pair.component2();
        TypefaceCache typefaceCache = TypefaceCache.INSTANCE;
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        Intrinsics.checkNotNullExpressionValue(textFont, "textFont");
        Intrinsics.checkNotNullExpressionValue(supportedScript, "script");
        return typefaceCache.get(application, textFont, supportedScript);
    }

    /* renamed from: _init_$lambda-2 */
    public static final void m2248_init_$lambda2(TextStoryPostCreationViewModel textStoryPostCreationViewModel, Typeface typeface) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationViewModel, "this$0");
        textStoryPostCreationViewModel.internalTypeface.postValue(typeface);
    }

    public final void setBitmap(Bitmap bitmap) {
        Intrinsics.checkNotNullParameter(bitmap, "bitmap");
        Bitmap value = this.internalThumbnail.getValue();
        if (value != null) {
            value.recycle();
        }
        this.internalThumbnail.setValue(bitmap);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
        Bitmap value = this.thumbnail.getValue();
        if (value != null) {
            value.recycle();
        }
    }

    public final void saveToInstanceState(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "outState");
        bundle.putParcelable(TEXT_STORY_INSTANCE_STATE, this.store.getState());
    }

    public final void restoreFromInstanceState(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "inState");
        if (bundle.containsKey(TEXT_STORY_INSTANCE_STATE)) {
            Parcelable parcelable = bundle.getParcelable(TEXT_STORY_INSTANCE_STATE);
            Intrinsics.checkNotNull(parcelable);
            this.textFontSubject.onNext(this.store.getState().getTextFont());
            this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda4
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return TextStoryPostCreationViewModel.m2250restoreFromInstanceState$lambda3(TextStoryPostCreationState.this, (TextStoryPostCreationState) obj);
                }
            });
        }
    }

    public final CharSequence getBody() {
        return this.store.getState().getBody();
    }

    public final int getTextColor() {
        return this.store.getState().getTextColor();
    }

    /* renamed from: setTextColor$lambda-4 */
    public static final TextStoryPostCreationState m2254setTextColor$lambda4(int i, TextStoryPostCreationState textStoryPostCreationState) {
        Intrinsics.checkNotNullExpressionValue(textStoryPostCreationState, "it");
        return textStoryPostCreationState.copy((r18 & 1) != 0 ? textStoryPostCreationState.body : null, (r18 & 2) != 0 ? textStoryPostCreationState.textColor : i, (r18 & 4) != 0 ? textStoryPostCreationState.textColorStyle : null, (r18 & 8) != 0 ? textStoryPostCreationState.textAlignment : null, (r18 & 16) != 0 ? textStoryPostCreationState.textFont : null, (r18 & 32) != 0 ? textStoryPostCreationState.textScale : 0, (r18 & 64) != 0 ? textStoryPostCreationState.backgroundColor : null, (r18 & 128) != 0 ? textStoryPostCreationState.linkPreviewUri : null);
    }

    public final void setTextColor(int i) {
        this.store.update(new com.annimon.stream.function.Function(i) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostCreationViewModel.m2254setTextColor$lambda4(this.f$0, (TextStoryPostCreationState) obj);
            }
        });
    }

    /* renamed from: setBody$lambda-5 */
    public static final TextStoryPostCreationState m2252setBody$lambda5(CharSequence charSequence, TextStoryPostCreationState textStoryPostCreationState) {
        Intrinsics.checkNotNullParameter(charSequence, "$body");
        Intrinsics.checkNotNullExpressionValue(textStoryPostCreationState, "it");
        return textStoryPostCreationState.copy((r18 & 1) != 0 ? textStoryPostCreationState.body : charSequence, (r18 & 2) != 0 ? textStoryPostCreationState.textColor : 0, (r18 & 4) != 0 ? textStoryPostCreationState.textColorStyle : null, (r18 & 8) != 0 ? textStoryPostCreationState.textAlignment : null, (r18 & 16) != 0 ? textStoryPostCreationState.textFont : null, (r18 & 32) != 0 ? textStoryPostCreationState.textScale : 0, (r18 & 64) != 0 ? textStoryPostCreationState.backgroundColor : null, (r18 & 128) != 0 ? textStoryPostCreationState.linkPreviewUri : null);
    }

    public final void setBody(CharSequence charSequence) {
        Intrinsics.checkNotNullParameter(charSequence, "body");
        this.store.update(new com.annimon.stream.function.Function(charSequence) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ CharSequence f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostCreationViewModel.m2252setBody$lambda5(this.f$0, (TextStoryPostCreationState) obj);
            }
        });
    }

    /* renamed from: setAlignment$lambda-6 */
    public static final TextStoryPostCreationState m2251setAlignment$lambda6(TextAlignment textAlignment, TextStoryPostCreationState textStoryPostCreationState) {
        Intrinsics.checkNotNullParameter(textAlignment, "$textAlignment");
        Intrinsics.checkNotNullExpressionValue(textStoryPostCreationState, "it");
        return textStoryPostCreationState.copy((r18 & 1) != 0 ? textStoryPostCreationState.body : null, (r18 & 2) != 0 ? textStoryPostCreationState.textColor : 0, (r18 & 4) != 0 ? textStoryPostCreationState.textColorStyle : null, (r18 & 8) != 0 ? textStoryPostCreationState.textAlignment : textAlignment, (r18 & 16) != 0 ? textStoryPostCreationState.textFont : null, (r18 & 32) != 0 ? textStoryPostCreationState.textScale : 0, (r18 & 64) != 0 ? textStoryPostCreationState.backgroundColor : null, (r18 & 128) != 0 ? textStoryPostCreationState.linkPreviewUri : null);
    }

    public final void setAlignment(TextAlignment textAlignment) {
        Intrinsics.checkNotNullParameter(textAlignment, "textAlignment");
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostCreationViewModel.m2251setAlignment$lambda6(TextAlignment.this, (TextStoryPostCreationState) obj);
            }
        });
    }

    /* renamed from: setTextScale$lambda-7 */
    public static final TextStoryPostCreationState m2257setTextScale$lambda7(int i, TextStoryPostCreationState textStoryPostCreationState) {
        Intrinsics.checkNotNullExpressionValue(textStoryPostCreationState, "it");
        return textStoryPostCreationState.copy((r18 & 1) != 0 ? textStoryPostCreationState.body : null, (r18 & 2) != 0 ? textStoryPostCreationState.textColor : 0, (r18 & 4) != 0 ? textStoryPostCreationState.textColorStyle : null, (r18 & 8) != 0 ? textStoryPostCreationState.textAlignment : null, (r18 & 16) != 0 ? textStoryPostCreationState.textFont : null, (r18 & 32) != 0 ? textStoryPostCreationState.textScale : i, (r18 & 64) != 0 ? textStoryPostCreationState.backgroundColor : null, (r18 & 128) != 0 ? textStoryPostCreationState.linkPreviewUri : null);
    }

    public final void setTextScale(int i) {
        this.store.update(new com.annimon.stream.function.Function(i) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda7
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostCreationViewModel.m2257setTextScale$lambda7(this.f$0, (TextStoryPostCreationState) obj);
            }
        });
    }

    /* renamed from: setTextColorStyle$lambda-8 */
    public static final TextStoryPostCreationState m2255setTextColorStyle$lambda8(TextColorStyle textColorStyle, TextStoryPostCreationState textStoryPostCreationState) {
        Intrinsics.checkNotNullParameter(textColorStyle, "$textColorStyle");
        Intrinsics.checkNotNullExpressionValue(textStoryPostCreationState, "it");
        return textStoryPostCreationState.copy((r18 & 1) != 0 ? textStoryPostCreationState.body : null, (r18 & 2) != 0 ? textStoryPostCreationState.textColor : 0, (r18 & 4) != 0 ? textStoryPostCreationState.textColorStyle : textColorStyle, (r18 & 8) != 0 ? textStoryPostCreationState.textAlignment : null, (r18 & 16) != 0 ? textStoryPostCreationState.textFont : null, (r18 & 32) != 0 ? textStoryPostCreationState.textScale : 0, (r18 & 64) != 0 ? textStoryPostCreationState.backgroundColor : null, (r18 & 128) != 0 ? textStoryPostCreationState.linkPreviewUri : null);
    }

    public final void setTextColorStyle(TextColorStyle textColorStyle) {
        Intrinsics.checkNotNullParameter(textColorStyle, "textColorStyle");
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostCreationViewModel.m2255setTextColorStyle$lambda8(TextColorStyle.this, (TextStoryPostCreationState) obj);
            }
        });
    }

    public final void setTextFont(TextFont textFont) {
        Intrinsics.checkNotNullParameter(textFont, "textFont");
        this.textFontSubject.onNext(textFont);
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostCreationViewModel.m2256setTextFont$lambda9(TextFont.this, (TextStoryPostCreationState) obj);
            }
        });
    }

    /* renamed from: setTextFont$lambda-9 */
    public static final TextStoryPostCreationState m2256setTextFont$lambda9(TextFont textFont, TextStoryPostCreationState textStoryPostCreationState) {
        Intrinsics.checkNotNullParameter(textFont, "$textFont");
        Intrinsics.checkNotNullExpressionValue(textStoryPostCreationState, "it");
        return textStoryPostCreationState.copy((r18 & 1) != 0 ? textStoryPostCreationState.body : null, (r18 & 2) != 0 ? textStoryPostCreationState.textColor : 0, (r18 & 4) != 0 ? textStoryPostCreationState.textColorStyle : null, (r18 & 8) != 0 ? textStoryPostCreationState.textAlignment : null, (r18 & 16) != 0 ? textStoryPostCreationState.textFont : textFont, (r18 & 32) != 0 ? textStoryPostCreationState.textScale : 0, (r18 & 64) != 0 ? textStoryPostCreationState.backgroundColor : null, (r18 & 128) != 0 ? textStoryPostCreationState.linkPreviewUri : null);
    }

    /* renamed from: cycleBackgroundColor$lambda-10 */
    public static final TextStoryPostCreationState m2249cycleBackgroundColor$lambda10(TextStoryPostCreationState textStoryPostCreationState) {
        Intrinsics.checkNotNullExpressionValue(textStoryPostCreationState, "it");
        return textStoryPostCreationState.copy((r18 & 1) != 0 ? textStoryPostCreationState.body : null, (r18 & 2) != 0 ? textStoryPostCreationState.textColor : 0, (r18 & 4) != 0 ? textStoryPostCreationState.textColorStyle : null, (r18 & 8) != 0 ? textStoryPostCreationState.textAlignment : null, (r18 & 16) != 0 ? textStoryPostCreationState.textFont : null, (r18 & 32) != 0 ? textStoryPostCreationState.textScale : 0, (r18 & 64) != 0 ? textStoryPostCreationState.backgroundColor : TextStoryBackgroundColors.INSTANCE.cycleBackgroundColor(textStoryPostCreationState.getBackgroundColor()), (r18 & 128) != 0 ? textStoryPostCreationState.linkPreviewUri : null);
    }

    public final void cycleBackgroundColor() {
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostCreationViewModel.m2249cycleBackgroundColor$lambda10((TextStoryPostCreationState) obj);
            }
        });
    }

    /* renamed from: setLinkPreview$lambda-11 */
    public static final TextStoryPostCreationState m2253setLinkPreview$lambda11(String str, TextStoryPostCreationState textStoryPostCreationState) {
        Intrinsics.checkNotNullExpressionValue(textStoryPostCreationState, "it");
        return textStoryPostCreationState.copy((r18 & 1) != 0 ? textStoryPostCreationState.body : null, (r18 & 2) != 0 ? textStoryPostCreationState.textColor : 0, (r18 & 4) != 0 ? textStoryPostCreationState.textColorStyle : null, (r18 & 8) != 0 ? textStoryPostCreationState.textAlignment : null, (r18 & 16) != 0 ? textStoryPostCreationState.textFont : null, (r18 & 32) != 0 ? textStoryPostCreationState.textScale : 0, (r18 & 64) != 0 ? textStoryPostCreationState.backgroundColor : null, (r18 & 128) != 0 ? textStoryPostCreationState.linkPreviewUri : str);
    }

    public final void setLinkPreview(String str) {
        this.store.update(new com.annimon.stream.function.Function(str) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel$$ExternalSyntheticLambda12
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostCreationViewModel.m2253setLinkPreview$lambda11(this.f$0, (TextStoryPostCreationState) obj);
            }
        });
    }

    public final void setTemporaryBody(String str) {
        Intrinsics.checkNotNullParameter(str, "temporaryBody");
        this.temporaryBodySubject.onNext(str);
    }

    public final Single<TextStoryPostSendResult> send(Set<? extends ContactSearchKey> set, LinkPreview linkPreview) {
        Intrinsics.checkNotNullParameter(set, ContactShareEditActivity.KEY_CONTACTS);
        TextStoryPostSendRepository textStoryPostSendRepository = this.repository;
        TextStoryPostCreationState state = this.store.getState();
        Intrinsics.checkNotNullExpressionValue(state, "store.state");
        return textStoryPostSendRepository.send(set, state, linkPreview);
    }

    /* compiled from: TextStoryPostCreationViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendRepository;", "(Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final TextStoryPostSendRepository repository;

        public Factory(TextStoryPostSendRepository textStoryPostSendRepository) {
            Intrinsics.checkNotNullParameter(textStoryPostSendRepository, "repository");
            this.repository = textStoryPostSendRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new TextStoryPostCreationViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationViewModel.Factory.create");
        }
    }

    /* compiled from: TextStoryPostCreationViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "TEXT_STORY_INSTANCE_STATE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
