package org.thoughtcrime.securesms.mediasend.v2.text;

import kotlin.Metadata;
import org.thoughtcrime.securesms.R;

/* compiled from: TextColorStyle.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0011\b\u0002\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextColorStyle;", "", "icon", "", "(Ljava/lang/String;II)V", "getIcon", "()I", "NO_BACKGROUND", "NORMAL", "INVERT", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum TextColorStyle {
    NO_BACKGROUND(R.drawable.ic_text_normal),
    NORMAL(R.drawable.ic_text_effect),
    INVERT(R.drawable.ic_text_effect);
    
    private final int icon;

    TextColorStyle(int i) {
        this.icon = i;
    }

    public final int getIcon() {
        return this.icon;
    }
}
