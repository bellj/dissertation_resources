package org.thoughtcrime.securesms.mediasend;

import android.view.View;
import org.thoughtcrime.securesms.mediasend.CameraContactAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CameraContactAdapter$InviteViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CameraContactAdapter.CameraContactListener f$0;

    public /* synthetic */ CameraContactAdapter$InviteViewHolder$$ExternalSyntheticLambda0(CameraContactAdapter.CameraContactListener cameraContactListener) {
        this.f$0 = cameraContactListener;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onInviteContactsClicked();
    }
}
