package org.thoughtcrime.securesms.mediasend.v2.gallery;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGallerySelectableItem;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: MediaGalleryFragment.kt */
@Metadata(d1 = {"\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¨\u0006\u0005"}, d2 = {"org/thoughtcrime/securesms/mediasend/v2/gallery/MediaGalleryFragment$onViewCreated$1", "Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "getSpanSize", "", "position", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGalleryFragment$onViewCreated$1 extends GridLayoutManager.SpanSizeLookup {
    final /* synthetic */ MediaGalleryFragment this$0;

    public MediaGalleryFragment$onViewCreated$1(MediaGalleryFragment mediaGalleryFragment) {
        this.this$0 = mediaGalleryFragment;
    }

    /* renamed from: getSpanSize$lambda-0 */
    public static final Boolean m2164getSpanSize$lambda0(MappingModel mappingModel) {
        return Boolean.valueOf(mappingModel instanceof MediaGallerySelectableItem.FolderModel);
    }

    @Override // androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
    public int getSpanSize(int i) {
        RecyclerView recyclerView = this.this$0.galleryRecycler;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("galleryRecycler");
            recyclerView = null;
        }
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null) {
            Object orElse = ((MappingAdapter) adapter).getModel(i).map(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment$onViewCreated$1$$ExternalSyntheticLambda0
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return MediaGalleryFragment$onViewCreated$1.m2164getSpanSize$lambda0((MappingModel) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).orElse(Boolean.FALSE);
            Intrinsics.checkNotNullExpressionValue(orElse, "galleryRecycler.adapter …lderModel }.orElse(false)");
            return ((Boolean) orElse).booleanValue() ? 2 : 1;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter");
    }
}
