package org.thoughtcrime.securesms.mediasend.v2.text.send;

import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.MediaDirections;

/* loaded from: classes4.dex */
public class TextStoryPostSendFragmentDirections {
    private TextStoryPostSendFragmentDirections() {
    }

    public static NavDirections actionDirectlyToMediaCaptureFragment() {
        return MediaDirections.actionDirectlyToMediaCaptureFragment();
    }

    public static NavDirections actionDirectlyToMediaGalleryFragment() {
        return MediaDirections.actionDirectlyToMediaGalleryFragment();
    }

    public static NavDirections actionDirectlyToMediaReviewFragment() {
        return MediaDirections.actionDirectlyToMediaReviewFragment();
    }

    public static NavDirections actionDirectlyToTextPostCreationFragment() {
        return MediaDirections.actionDirectlyToTextPostCreationFragment();
    }
}
