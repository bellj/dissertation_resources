package org.thoughtcrime.securesms.mediasend;

import androidx.camera.core.ImageProxy;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.mediasend.CameraXFragment;
import org.thoughtcrime.securesms.util.Stopwatch;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CameraXFragment$4$$ExternalSyntheticLambda0 implements SimpleTask.BackgroundTask {
    public final /* synthetic */ CameraXFragment.AnonymousClass4 f$0;
    public final /* synthetic */ Stopwatch f$1;
    public final /* synthetic */ ImageProxy f$2;

    public /* synthetic */ CameraXFragment$4$$ExternalSyntheticLambda0(CameraXFragment.AnonymousClass4 r1, Stopwatch stopwatch, ImageProxy imageProxy) {
        this.f$0 = r1;
        this.f$1 = stopwatch;
        this.f$2 = imageProxy;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
    public final Object run() {
        return this.f$0.lambda$onCaptureSuccess$0(this.f$1, this.f$2);
    }
}
