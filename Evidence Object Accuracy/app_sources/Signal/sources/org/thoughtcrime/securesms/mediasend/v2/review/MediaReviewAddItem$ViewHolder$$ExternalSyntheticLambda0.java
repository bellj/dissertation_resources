package org.thoughtcrime.securesms.mediasend.v2.review;

import android.view.View;
import kotlin.jvm.functions.Function0;
import org.thoughtcrime.securesms.mediasend.v2.review.MediaReviewAddItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaReviewAddItem$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ Function0 f$0;

    public /* synthetic */ MediaReviewAddItem$ViewHolder$$ExternalSyntheticLambda0(Function0 function0) {
        this.f$0 = function0;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        MediaReviewAddItem.ViewHolder.m2202_init_$lambda0(this.f$0, view);
    }
}
