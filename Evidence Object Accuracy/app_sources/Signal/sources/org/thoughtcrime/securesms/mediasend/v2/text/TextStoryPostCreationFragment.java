package org.thoughtcrime.securesms.mediasend.v2.text;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.FragmentKt;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import j$.util.Optional;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contactshare.ContactShareEditActivity;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel;
import org.thoughtcrime.securesms.mediasend.v2.HudCommand;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionViewModel;
import org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment;
import org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendResult;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.stories.StoryTextPostView;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: TextStoryPostCreationFragment.kt */
@Metadata(d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020 H\u0016J\b\u0010\"\u001a\u00020 H\u0016J\b\u0010#\u001a\u00020 H\u0016J\u001a\u0010$\u001a\u00020 2\u0006\u0010%\u001a\u00020\u00122\b\u0010&\u001a\u0004\u0018\u00010'H\u0016J\u0016\u0010(\u001a\u00020 2\f\u0010)\u001a\b\u0012\u0004\u0012\u00020+0*H\u0002J\u0016\u0010,\u001a\u00020 2\f\u0010-\u001a\b\u0012\u0004\u0012\u00020/0.H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\t\u001a\u00020\n8BX\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u000f\u001a\u00020\u0010X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0013\u001a\u00020\u00148BX\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u000e\u001a\u0004\b\u0015\u0010\u0016R\u000e\u0010\u0018\u001a\u00020\u0019X.¢\u0006\u0002\n\u0000R\u001b\u0010\u001a\u001a\u00020\u001b8BX\u0002¢\u0006\f\n\u0004\b\u001e\u0010\u000e\u001a\u0004\b\u001c\u0010\u001d¨\u00060"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostTextEntryFragment$Callback;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheet$Callbacks;", "()V", "backgroundButton", "Landroidx/appcompat/widget/AppCompatImageView;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "linkPreviewViewModel", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreviewViewModel;", "getLinkPreviewViewModel", "()Lorg/thoughtcrime/securesms/linkpreview/LinkPreviewViewModel;", "linkPreviewViewModel$delegate", "Lkotlin/Lazy;", "scene", "Landroidx/constraintlayout/widget/ConstraintLayout;", "send", "Landroid/view/View;", "sharedViewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "getSharedViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionViewModel;", "sharedViewModel$delegate", "storyTextPostView", "Lorg/thoughtcrime/securesms/stories/StoryTextPostView;", "viewModel", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationViewModel;", "viewModel$delegate", "onCanceled", "", "onMessageResentAfterSafetyNumberChangeInBottomSheet", "onResume", "onTextStoryPostTextEntryDismissed", "onViewCreated", "view", "savedInstanceState", "Landroid/os/Bundle;", "performSend", ContactShareEditActivity.KEY_CONTACTS, "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "sendAnywayAfterSafetyNumberChangedInBottomSheet", "destinations", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryPostCreationFragment extends Fragment implements TextStoryPostTextEntryFragment.Callback, SafetyNumberBottomSheet.Callbacks {
    private AppCompatImageView backgroundButton;
    private final LifecycleDisposable lifecycleDisposable;
    private final Lazy linkPreviewViewModel$delegate;
    private ConstraintLayout scene;
    private View send;
    private final Lazy sharedViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MediaSelectionViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$sharedViewModel$2
        final /* synthetic */ TextStoryPostCreationFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private StoryTextPostView storyTextPostView;
    private final Lazy viewModel$delegate;

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void onCanceled() {
    }

    public TextStoryPostCreationFragment() {
        super(R.layout.stories_text_post_creation_fragment);
        TextStoryPostCreationFragment$viewModel$2 textStoryPostCreationFragment$viewModel$2 = new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$viewModel$2
            final /* synthetic */ TextStoryPostCreationFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStoreOwner invoke() {
                FragmentActivity requireActivity = this.this$0.requireActivity();
                Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
                return requireActivity;
            }
        };
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(TextStoryPostCreationViewModel.class), new Function0<ViewModelStore>(textStoryPostCreationFragment$viewModel$2) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$special$$inlined$viewModels$1
            final /* synthetic */ Function0 $ownerProducer;

            {
                this.$ownerProducer = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStore invoke() {
                ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
                Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
                return viewModelStore;
            }
        }, TextStoryPostCreationFragment$viewModel$3.INSTANCE);
        TextStoryPostCreationFragment$linkPreviewViewModel$2 textStoryPostCreationFragment$linkPreviewViewModel$2 = new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$linkPreviewViewModel$2
            final /* synthetic */ TextStoryPostCreationFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStoreOwner invoke() {
                FragmentActivity requireActivity = this.this$0.requireActivity();
                Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
                return requireActivity;
            }
        };
        this.linkPreviewViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(LinkPreviewViewModel.class), new Function0<ViewModelStore>(textStoryPostCreationFragment$linkPreviewViewModel$2) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$special$$inlined$viewModels$2
            final /* synthetic */ Function0 $ownerProducer;

            {
                this.$ownerProducer = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStore invoke() {
                ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
                Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
                return viewModelStore;
            }
        }, TextStoryPostCreationFragment$linkPreviewViewModel$3.INSTANCE);
        this.lifecycleDisposable = new LifecycleDisposable();
    }

    private final MediaSelectionViewModel getSharedViewModel() {
        return (MediaSelectionViewModel) this.sharedViewModel$delegate.getValue();
    }

    private final TextStoryPostCreationViewModel getViewModel() {
        return (TextStoryPostCreationViewModel) this.viewModel$delegate.getValue();
    }

    private final LinkPreviewViewModel getLinkPreviewViewModel() {
        return (LinkPreviewViewModel) this.linkPreviewViewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.id.scene);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.scene)");
        this.scene = (ConstraintLayout) findViewById;
        View findViewById2 = view.findViewById(R.id.background_selector);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.background_selector)");
        this.backgroundButton = (AppCompatImageView) findViewById2;
        View findViewById3 = view.findViewById(R.id.send);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.send)");
        this.send = findViewById3;
        View findViewById4 = view.findViewById(R.id.story_text_post);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.story_text_post)");
        this.storyTextPostView = (StoryTextPostView) findViewById4;
        View findViewById5 = view.findViewById(R.id.background_protection);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.background_protection)");
        View findViewById6 = view.findViewById(R.id.add_link_protection);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.add_link_protection)");
        StoryTextPostView storyTextPostView = this.storyTextPostView;
        View view2 = null;
        if (storyTextPostView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
            storyTextPostView = null;
        }
        storyTextPostView.showCloseButton();
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = getSharedViewModel().getHudCommands().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                TextStoryPostCreationFragment.$r8$lambda$jdBfJIU4fdLgiKuVCQUUzhxou4k(TextStoryPostCreationFragment.this, (HudCommand) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "sharedViewModel.hudComma…BackStack()\n      }\n    }");
        lifecycleDisposable2.plusAssign(subscribe);
        getViewModel().getTypeface().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                TextStoryPostCreationFragment.$r8$lambda$Ca177timsKApMjAQN2NvNchx1f4(TextStoryPostCreationFragment.this, (Typeface) obj);
            }
        });
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                TextStoryPostCreationFragment.$r8$lambda$7b0Tu1yTV9Xzd3Qfsea_71XOjc8(TextStoryPostCreationFragment.this, (TextStoryPostCreationState) obj);
            }
        });
        getLinkPreviewViewModel().getLinkPreviewState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                TextStoryPostCreationFragment.$r8$lambda$7dQfys6CKT2c10o8l1__p3LBlVU(TextStoryPostCreationFragment.this, (LinkPreviewViewModel.LinkPreviewState) obj);
            }
        });
        StoryTextPostView storyTextPostView2 = this.storyTextPostView;
        if (storyTextPostView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
            storyTextPostView2 = null;
        }
        storyTextPostView2.setTextViewClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                TextStoryPostCreationFragment.m2233$r8$lambda$JdGX5tU0LIxs7lXg7yJuJxtTdw(TextStoryPostCreationFragment.this, view3);
            }
        });
        findViewById5.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                TextStoryPostCreationFragment.$r8$lambda$B9Oc9FZbpcUcJw8nlmAOGCqBtWo(TextStoryPostCreationFragment.this, view3);
            }
        });
        findViewById6.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                TextStoryPostCreationFragment.$r8$lambda$CS5t7jrj9RKtxyE3SyuiT9e8Efc(TextStoryPostCreationFragment.this, view3);
            }
        });
        StoryTextPostView storyTextPostView3 = this.storyTextPostView;
        if (storyTextPostView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
            storyTextPostView3 = null;
        }
        storyTextPostView3.setLinkPreviewCloseListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$$ExternalSyntheticLambda8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                TextStoryPostCreationFragment.$r8$lambda$0C8Uh0iPUmk2BvYFw_DsTtPtpTM(TextStoryPostCreationFragment.this, view3);
            }
        });
        View view3 = this.send;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("send");
        } else {
            view2 = view3;
        }
        view2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$$ExternalSyntheticLambda9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view4) {
                TextStoryPostCreationFragment.$r8$lambda$Zm_RKG0RiKsZVwK5rJklTsOaWus(TextStoryPostCreationFragment.this, view4);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2234onViewCreated$lambda0(TextStoryPostCreationFragment textStoryPostCreationFragment, HudCommand hudCommand) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationFragment, "this$0");
        if (Intrinsics.areEqual(hudCommand, HudCommand.GoToCapture.INSTANCE)) {
            FragmentKt.findNavController(textStoryPostCreationFragment).popBackStack();
        }
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m2235onViewCreated$lambda1(TextStoryPostCreationFragment textStoryPostCreationFragment, Typeface typeface) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationFragment, "this$0");
        StoryTextPostView storyTextPostView = textStoryPostCreationFragment.storyTextPostView;
        if (storyTextPostView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
            storyTextPostView = null;
        }
        Intrinsics.checkNotNullExpressionValue(typeface, "typeface");
        storyTextPostView.setTypeface(typeface);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0073, code lost:
        if ((r7 == null || r7.length() == 0) == false) goto L_0x0075;
     */
    /* renamed from: onViewCreated$lambda-2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m2236onViewCreated$lambda2(org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment r6, org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationState r7) {
        /*
            java.lang.String r0 = "this$0"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r6, r0)
            androidx.appcompat.widget.AppCompatImageView r0 = r6.backgroundButton
            r1 = 0
            if (r0 != 0) goto L_0x0010
            java.lang.String r0 = "backgroundButton"
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r0)
            r0 = r1
        L_0x0010:
            org.thoughtcrime.securesms.conversation.colors.ChatColors r2 = r7.getBackgroundColor()
            android.graphics.drawable.Drawable r2 = r2.getChatBubbleMask()
            r0.setBackground(r2)
            org.thoughtcrime.securesms.stories.StoryTextPostView r0 = r6.storyTextPostView
            if (r0 != 0) goto L_0x0025
            java.lang.String r0 = "storyTextPostView"
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r0)
            r0 = r1
        L_0x0025:
            java.lang.String r2 = "state"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r7, r2)
            r0.bindFromCreationState(r7)
            java.lang.String r0 = r7.getLinkPreviewUri()
            r2 = 0
            if (r0 == 0) goto L_0x004c
            org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel r0 = r6.getLinkPreviewViewModel()
            android.content.Context r3 = r6.requireContext()
            java.lang.String r4 = r7.getLinkPreviewUri()
            java.lang.String r5 = r7.getLinkPreviewUri()
            int r5 = kotlin.text.StringsKt.getLastIndex(r5)
            r0.onTextChanged(r3, r4, r2, r5)
            goto L_0x0053
        L_0x004c:
            org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel r0 = r6.getLinkPreviewViewModel()
            r0.onSend()
        L_0x0053:
            java.lang.CharSequence r0 = r7.getBody()
            int r0 = r0.length()
            r3 = 1
            if (r0 <= 0) goto L_0x0060
            r0 = 1
            goto L_0x0061
        L_0x0060:
            r0 = 0
        L_0x0061:
            if (r0 != 0) goto L_0x0075
            java.lang.String r7 = r7.getLinkPreviewUri()
            if (r7 == 0) goto L_0x0072
            int r7 = r7.length()
            if (r7 != 0) goto L_0x0070
            goto L_0x0072
        L_0x0070:
            r7 = 0
            goto L_0x0073
        L_0x0072:
            r7 = 1
        L_0x0073:
            if (r7 != 0) goto L_0x0076
        L_0x0075:
            r2 = 1
        L_0x0076:
            android.view.View r7 = r6.send
            java.lang.String r0 = "send"
            if (r7 != 0) goto L_0x0080
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r0)
            r7 = r1
        L_0x0080:
            if (r2 == 0) goto L_0x0085
            r3 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0087
        L_0x0085:
            r3 = 1056964608(0x3f000000, float:0.5)
        L_0x0087:
            r7.setAlpha(r3)
            android.view.View r6 = r6.send
            if (r6 != 0) goto L_0x0092
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r0)
            goto L_0x0093
        L_0x0092:
            r1 = r6
        L_0x0093:
            r1.setEnabled(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment.m2236onViewCreated$lambda2(org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment, org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationState):void");
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2237onViewCreated$lambda3(TextStoryPostCreationFragment textStoryPostCreationFragment, LinkPreviewViewModel.LinkPreviewState linkPreviewState) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationFragment, "this$0");
        StoryTextPostView storyTextPostView = textStoryPostCreationFragment.storyTextPostView;
        StoryTextPostView storyTextPostView2 = null;
        if (storyTextPostView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
            storyTextPostView = null;
        }
        Intrinsics.checkNotNullExpressionValue(linkPreviewState, "state");
        storyTextPostView.bindLinkPreviewState(linkPreviewState, 8);
        StoryTextPostView storyTextPostView3 = textStoryPostCreationFragment.storyTextPostView;
        if (storyTextPostView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
        } else {
            storyTextPostView2 = storyTextPostView3;
        }
        storyTextPostView2.postAdjustLinkPreviewTranslationY();
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m2238onViewCreated$lambda4(TextStoryPostCreationFragment textStoryPostCreationFragment, View view) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationFragment, "this$0");
        StoryTextPostView storyTextPostView = textStoryPostCreationFragment.storyTextPostView;
        if (storyTextPostView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
            storyTextPostView = null;
        }
        storyTextPostView.hidePostContent();
        StoryTextPostView storyTextPostView2 = textStoryPostCreationFragment.storyTextPostView;
        if (storyTextPostView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
            storyTextPostView2 = null;
        }
        storyTextPostView2.setEnabled(false);
        new TextStoryPostTextEntryFragment().show(textStoryPostCreationFragment.getChildFragmentManager(), (String) null);
    }

    /* renamed from: onViewCreated$lambda-5 */
    public static final void m2239onViewCreated$lambda5(TextStoryPostCreationFragment textStoryPostCreationFragment, View view) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationFragment, "this$0");
        textStoryPostCreationFragment.getViewModel().cycleBackgroundColor();
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m2240onViewCreated$lambda6(TextStoryPostCreationFragment textStoryPostCreationFragment, View view) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationFragment, "this$0");
        new TextStoryPostLinkEntryFragment().show(textStoryPostCreationFragment.getChildFragmentManager(), (String) null);
    }

    /* renamed from: onViewCreated$lambda-7 */
    public static final void m2241onViewCreated$lambda7(TextStoryPostCreationFragment textStoryPostCreationFragment, View view) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationFragment, "this$0");
        textStoryPostCreationFragment.getViewModel().setLinkPreview("");
    }

    /* renamed from: onViewCreated$lambda-8 */
    public static final void m2242onViewCreated$lambda8(TextStoryPostCreationFragment textStoryPostCreationFragment, View view) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationFragment, "this$0");
        StoryTextPostView storyTextPostView = textStoryPostCreationFragment.storyTextPostView;
        View view2 = null;
        if (storyTextPostView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
            storyTextPostView = null;
        }
        storyTextPostView.hideCloseButton();
        Set<? extends ContactSearchKey> set = CollectionsKt___CollectionsKt.toSet(CollectionsKt___CollectionsJvmKt.filterIsInstance(CollectionsKt___CollectionsKt.plus((Collection<? extends ContactSearchKey.RecipientSearchKey>) ((Collection<? extends Object>) textStoryPostCreationFragment.getSharedViewModel().getDestination().getRecipientSearchKeyList()), textStoryPostCreationFragment.getSharedViewModel().getDestination().getRecipientSearchKey()), ContactSearchKey.class));
        if (set.isEmpty()) {
            TextStoryPostCreationViewModel viewModel = textStoryPostCreationFragment.getViewModel();
            StoryTextPostView storyTextPostView2 = textStoryPostCreationFragment.storyTextPostView;
            if (storyTextPostView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
                storyTextPostView2 = null;
            }
            viewModel.setBitmap(ViewKt.drawToBitmap$default(storyTextPostView2, null, 1, null));
            SafeNavigation.safeNavigate(FragmentKt.findNavController(textStoryPostCreationFragment), (int) R.id.action_textStoryPostCreationFragment_to_textStoryPostSendFragment);
            return;
        }
        View view3 = textStoryPostCreationFragment.send;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("send");
        } else {
            view2 = view3;
        }
        view2.setClickable(false);
        textStoryPostCreationFragment.performSend(set);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        StoryTextPostView storyTextPostView = this.storyTextPostView;
        if (storyTextPostView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
            storyTextPostView = null;
        }
        storyTextPostView.showCloseButton();
        requireActivity().setRequestedOrientation(1);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostTextEntryFragment.Callback
    public void onTextStoryPostTextEntryDismissed() {
        StoryTextPostView storyTextPostView = this.storyTextPostView;
        if (storyTextPostView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
            storyTextPostView = null;
        }
        storyTextPostView.postDelayed(new Runnable(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$onTextStoryPostTextEntryDismissed$$inlined$postDelayed$1
            final /* synthetic */ TextStoryPostCreationFragment this$0;

            {
                this.this$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StoryTextPostView access$getStoryTextPostView$p = TextStoryPostCreationFragment.access$getStoryTextPostView$p(this.this$0);
                StoryTextPostView storyTextPostView2 = null;
                if (access$getStoryTextPostView$p == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
                    access$getStoryTextPostView$p = null;
                }
                access$getStoryTextPostView$p.showPostContent();
                StoryTextPostView access$getStoryTextPostView$p2 = TextStoryPostCreationFragment.access$getStoryTextPostView$p(this.this$0);
                if (access$getStoryTextPostView$p2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("storyTextPostView");
                } else {
                    storyTextPostView2 = access$getStoryTextPostView$p2;
                }
                storyTextPostView2.setEnabled(true);
            }
        }, (long) getResources().getInteger(R.integer.text_entry_exit_duration));
    }

    private final void performSend(Set<? extends ContactSearchKey> set) {
        Optional<LinkPreview> linkPreview;
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        TextStoryPostCreationViewModel viewModel = getViewModel();
        LinkPreviewViewModel.LinkPreviewState value = getLinkPreviewViewModel().getLinkPreviewState().getValue();
        LinkPreview linkPreview2 = null;
        if (!(value == null || (linkPreview = value.getLinkPreview()) == null)) {
            linkPreview2 = linkPreview.orElse(null);
        }
        Disposable subscribe = viewModel.send(set, linkPreview2).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(set) { // from class: org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ Set f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                TextStoryPostCreationFragment.$r8$lambda$wWhdujpmxH57BIMZy2cbh1BkCbk(TextStoryPostCreationFragment.this, this.f$1, (TextStoryPostSendResult) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.send(\n      co…)\n        }\n      }\n    }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* renamed from: performSend$lambda-10 */
    public static final void m2243performSend$lambda10(TextStoryPostCreationFragment textStoryPostCreationFragment, Set set, TextStoryPostSendResult textStoryPostSendResult) {
        Intrinsics.checkNotNullParameter(textStoryPostCreationFragment, "this$0");
        Intrinsics.checkNotNullParameter(set, "$contacts");
        if (Intrinsics.areEqual(textStoryPostSendResult, TextStoryPostSendResult.Success.INSTANCE)) {
            Toast.makeText(textStoryPostCreationFragment.requireContext(), (int) R.string.TextStoryPostCreationFragment__sent_story, 0).show();
            textStoryPostCreationFragment.requireActivity().finish();
        } else if (Intrinsics.areEqual(textStoryPostSendResult, TextStoryPostSendResult.Failure.INSTANCE)) {
            Toast.makeText(textStoryPostCreationFragment.requireContext(), (int) R.string.TextStoryPostCreationFragment__failed_to_send_story, 0).show();
            textStoryPostCreationFragment.requireActivity().finish();
        } else if (textStoryPostSendResult instanceof TextStoryPostSendResult.UntrustedRecordsError) {
            View view = textStoryPostCreationFragment.send;
            if (view == null) {
                Intrinsics.throwUninitializedPropertyAccessException("send");
                view = null;
            }
            view.setClickable(true);
            SafetyNumberBottomSheet.Factory forIdentityRecordsAndDestinations = SafetyNumberBottomSheet.forIdentityRecordsAndDestinations(((TextStoryPostSendResult.UntrustedRecordsError) textStoryPostSendResult).getUntrustedRecords(), CollectionsKt___CollectionsKt.toList(set));
            FragmentManager childFragmentManager = textStoryPostCreationFragment.getChildFragmentManager();
            Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
            forIdentityRecordsAndDestinations.show(childFragmentManager);
        }
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void sendAnywayAfterSafetyNumberChangedInBottomSheet(List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Intrinsics.checkNotNullParameter(list, "destinations");
        performSend(CollectionsKt___CollectionsKt.toSet(list));
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void onMessageResentAfterSafetyNumberChangeInBottomSheet() {
        throw new IllegalStateException("Unsupported, we do not hand in a message id.".toString());
    }
}
