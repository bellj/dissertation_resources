package org.thoughtcrime.securesms.mediasend.v2.text.send;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.signal.core.util.logging.Log;

/* compiled from: TextStoryPostSendViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextStoryPostSendViewModel$onSend$2 extends Lambda implements Function1<Throwable, Unit> {
    final /* synthetic */ TextStoryPostSendViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public TextStoryPostSendViewModel$onSend$2(TextStoryPostSendViewModel textStoryPostSendViewModel) {
        super(1);
        this.this$0 = textStoryPostSendViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        invoke(th);
        return Unit.INSTANCE;
    }

    public final void invoke(Throwable th) {
        Intrinsics.checkNotNullParameter(th, "it");
        Log.w(TextStoryPostSendViewModelKt.TAG, "Unexpected error occurred", th);
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendViewModel$onSend$2$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostSendViewModel$onSend$2.m2288invoke$lambda0((TextStoryPostSendState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final TextStoryPostSendState m2288invoke$lambda0(TextStoryPostSendState textStoryPostSendState) {
        return TextStoryPostSendState.FAILED;
    }
}
