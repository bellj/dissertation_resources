package org.thoughtcrime.securesms.mediasend.v2.capture;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.functions.Function;
import j$.util.Optional;
import java.io.FileDescriptor;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureEvent;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: MediaCaptureViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001%B\u000f\u0012\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b#\u0010$J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002J\b\u0010\u0006\u001a\u00020\u0004H\u0002J\u001e\u0010\f\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\tJ\u000e\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rJ\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u00110\u0010R\u0014\u0010\u0014\u001a\u00020\u00138\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u00168\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u001a\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u001d\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001e8\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureViewModel;", "Landroidx/lifecycle/ViewModel;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "media", "", "onMediaRendered", "onMediaRenderFailed", "", "data", "", "width", "height", "onImageCaptured", "Ljava/io/FileDescriptor;", "fd", "onVideoCaptured", "Lio/reactivex/rxjava3/core/Flowable;", "j$/util/Optional", "getMostRecentMedia", "Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureRepository;", "repository", "Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureRepository;", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureState;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureEvent;", "internalEvents", "Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "Landroidx/lifecycle/LiveData;", "events", "Landroidx/lifecycle/LiveData;", "getEvents", "()Landroidx/lifecycle/LiveData;", "<init>", "(Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureRepository;)V", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class MediaCaptureViewModel extends ViewModel {
    private final LiveData<MediaCaptureEvent> events;
    private final SingleLiveEvent<MediaCaptureEvent> internalEvents;
    private final MediaCaptureRepository repository;
    private final RxStore<MediaCaptureState> store = new RxStore<>(new MediaCaptureState(null, 1, null), null, 2, null);

    public MediaCaptureViewModel(MediaCaptureRepository mediaCaptureRepository) {
        Intrinsics.checkNotNullParameter(mediaCaptureRepository, "repository");
        this.repository = mediaCaptureRepository;
        SingleLiveEvent<MediaCaptureEvent> singleLiveEvent = new SingleLiveEvent<>();
        this.internalEvents = singleLiveEvent;
        this.events = singleLiveEvent;
        mediaCaptureRepository.getMostRecentItem(new Function1<Media, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureViewModel.1
            final /* synthetic */ MediaCaptureViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Media media) {
                invoke(media);
                return Unit.INSTANCE;
            }

            public final void invoke(final Media media) {
                this.this$0.store.update(new Function1<MediaCaptureState, MediaCaptureState>() { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureViewModel.1.1
                    public final MediaCaptureState invoke(MediaCaptureState mediaCaptureState) {
                        Intrinsics.checkNotNullParameter(mediaCaptureState, "state");
                        return mediaCaptureState.copy(media);
                    }
                });
            }
        });
    }

    public final LiveData<MediaCaptureEvent> getEvents() {
        return this.events;
    }

    public final void onImageCaptured(byte[] bArr, int i, int i2) {
        Intrinsics.checkNotNullParameter(bArr, "data");
        this.repository.renderImageToMedia(bArr, i, i2, new Function1<Media, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureViewModel$onImageCaptured$1
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Media media) {
                invoke(media);
                return Unit.INSTANCE;
            }

            public final void invoke(Media media) {
                Intrinsics.checkNotNullParameter(media, "p0");
                ((MediaCaptureViewModel) this.receiver).onMediaRendered(media);
            }
        }, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureViewModel$onImageCaptured$2
            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                ((MediaCaptureViewModel) this.receiver).onMediaRenderFailed();
            }
        });
    }

    public final void onVideoCaptured(FileDescriptor fileDescriptor) {
        Intrinsics.checkNotNullParameter(fileDescriptor, "fd");
        this.repository.renderVideoToMedia(fileDescriptor, new Function1<Media, Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureViewModel$onVideoCaptured$1
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Media media) {
                invoke(media);
                return Unit.INSTANCE;
            }

            public final void invoke(Media media) {
                Intrinsics.checkNotNullParameter(media, "p0");
                ((MediaCaptureViewModel) this.receiver).onMediaRendered(media);
            }
        }, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureViewModel$onVideoCaptured$2
            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                ((MediaCaptureViewModel) this.receiver).onMediaRenderFailed();
            }
        });
    }

    /* renamed from: getMostRecentMedia$lambda-0 */
    public static final Optional m2146getMostRecentMedia$lambda0(MediaCaptureState mediaCaptureState) {
        return Optional.ofNullable(mediaCaptureState.getMostRecentMedia());
    }

    /* JADX DEBUG: Type inference failed for r0v2. Raw type applied. Possible types: io.reactivex.rxjava3.core.Flowable<R>, java.lang.Object, io.reactivex.rxjava3.core.Flowable<j$.util.Optional<org.thoughtcrime.securesms.mediasend.Media>> */
    public final Flowable<Optional<Media>> getMostRecentMedia() {
        Flowable map = this.store.getStateFlowable().map(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.capture.MediaCaptureViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return MediaCaptureViewModel.m2146getMostRecentMedia$lambda0((MediaCaptureState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "store.stateFlowable.map …ble(it.mostRecentMedia) }");
        return map;
    }

    public final void onMediaRendered(Media media) {
        this.internalEvents.postValue(new MediaCaptureEvent.MediaCaptureRendered(media));
    }

    public final void onMediaRenderFailed() {
        this.internalEvents.postValue(MediaCaptureEvent.MediaCaptureRenderFailed.INSTANCE);
    }

    /* compiled from: MediaCaptureViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureRepository;", "(Lorg/thoughtcrime/securesms/mediasend/v2/capture/MediaCaptureRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final MediaCaptureRepository repository;

        public Factory(MediaCaptureRepository mediaCaptureRepository) {
            Intrinsics.checkNotNullParameter(mediaCaptureRepository, "repository");
            this.repository = mediaCaptureRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new MediaCaptureViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
