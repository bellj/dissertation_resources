package org.thoughtcrime.securesms.mediasend.v2.review;

import android.net.Uri;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.v2.gif.MediaReviewGifPageFragment;
import org.thoughtcrime.securesms.mediasend.v2.images.MediaReviewImagePageFragment;
import org.thoughtcrime.securesms.mediasend.v2.videos.MediaReviewVideoPageFragment;
import org.thoughtcrime.securesms.util.MediaUtil;

/* compiled from: MediaReviewFragmentPagerAdapter.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0015B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u000eH\u0016J\u0010\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0014\u0010\u0011\u001a\u00020\u00122\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00070\u0014R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewFragmentPagerAdapter;", "Landroidx/viewpager2/adapter/FragmentStateAdapter;", "fragment", "Landroidx/fragment/app/Fragment;", "(Landroidx/fragment/app/Fragment;)V", "mediaList", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "containsItem", "", "itemId", "", "createFragment", "position", "", "getItemCount", "getItemId", "submitMedia", "", "media", "", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaReviewFragmentPagerAdapter extends FragmentStateAdapter {
    private final List<Media> mediaList = new ArrayList();

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MediaReviewFragmentPagerAdapter(Fragment fragment) {
        super(fragment);
        Intrinsics.checkNotNullParameter(fragment, "fragment");
    }

    public final void submitMedia(List<? extends Media> list) {
        Intrinsics.checkNotNullParameter(list, "media");
        LinkedList linkedList = new LinkedList(this.mediaList);
        this.mediaList.clear();
        this.mediaList.addAll(list);
        DiffUtil.calculateDiff(new Callback(linkedList, this.mediaList)).dispatchUpdatesTo(this);
    }

    @Override // androidx.viewpager2.adapter.FragmentStateAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        if (i > this.mediaList.size() || i < 0) {
            return -1;
        }
        return (long) this.mediaList.get(i).getUri().hashCode();
    }

    @Override // androidx.viewpager2.adapter.FragmentStateAdapter
    public boolean containsItem(long j) {
        boolean z;
        List<Media> list = this.mediaList;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (Media media : list) {
                if (((long) media.getUri().hashCode()) == j) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.mediaList.size();
    }

    @Override // androidx.viewpager2.adapter.FragmentStateAdapter
    public Fragment createFragment(int i) {
        Media media = this.mediaList.get(i);
        if (MediaUtil.isGif(media.getMimeType())) {
            MediaReviewGifPageFragment.Companion companion = MediaReviewGifPageFragment.Companion;
            Uri uri = media.getUri();
            Intrinsics.checkNotNullExpressionValue(uri, "mediaItem.uri");
            return companion.newInstance(uri);
        } else if (MediaUtil.isImageType(media.getMimeType())) {
            MediaReviewImagePageFragment.Companion companion2 = MediaReviewImagePageFragment.Companion;
            Uri uri2 = media.getUri();
            Intrinsics.checkNotNullExpressionValue(uri2, "mediaItem.uri");
            return companion2.newInstance(uri2);
        } else if (MediaUtil.isVideoType(media.getMimeType())) {
            MediaReviewVideoPageFragment.Companion companion3 = MediaReviewVideoPageFragment.Companion;
            Uri uri3 = media.getUri();
            Intrinsics.checkNotNullExpressionValue(uri3, "mediaItem.uri");
            return companion3.newInstance(uri3, media.isVideoGif());
        } else {
            throw new UnsupportedOperationException("Can only render images and videos. Found mimetype: '" + media.getMimeType() + '\'');
        }
    }

    /* compiled from: MediaReviewFragmentPagerAdapter.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B!\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0016J\u0018\u0010\f\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0016J\b\u0010\r\u001a\u00020\nH\u0016J\b\u0010\u000e\u001a\u00020\nH\u0016R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/review/MediaReviewFragmentPagerAdapter$Callback;", "Landroidx/recyclerview/widget/DiffUtil$Callback;", "oldList", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "newList", "(Ljava/util/List;Ljava/util/List;)V", "areContentsTheSame", "", "oldItemPosition", "", "newItemPosition", "areItemsTheSame", "getNewListSize", "getOldListSize", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Callback extends DiffUtil.Callback {
        private final List<Media> newList;
        private final List<Media> oldList;

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.mediasend.Media> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.mediasend.Media> */
        /* JADX WARN: Multi-variable type inference failed */
        public Callback(List<? extends Media> list, List<? extends Media> list2) {
            Intrinsics.checkNotNullParameter(list, "oldList");
            Intrinsics.checkNotNullParameter(list2, "newList");
            this.oldList = list;
            this.newList = list2;
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public int getOldListSize() {
            return this.oldList.size();
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public int getNewListSize() {
            return this.newList.size();
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public boolean areItemsTheSame(int i, int i2) {
            return Intrinsics.areEqual(this.oldList.get(i).getUri(), this.newList.get(i2).getUri());
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public boolean areContentsTheSame(int i, int i2) {
            return Intrinsics.areEqual(this.oldList.get(i), this.newList.get(i2));
        }
    }
}
