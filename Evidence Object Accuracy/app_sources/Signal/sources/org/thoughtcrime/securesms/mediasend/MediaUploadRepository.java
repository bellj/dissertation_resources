package org.thoughtcrime.securesms.mediasend;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.mediasend.MediaUploadRepository;
import org.thoughtcrime.securesms.mms.GifSlide;
import org.thoughtcrime.securesms.mms.ImageSlide;
import org.thoughtcrime.securesms.mms.TextSlide;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public class MediaUploadRepository {
    private static final String TAG = Log.tag(MediaUploadRepository.class);
    private final Context context;
    private final Executor executor = SignalExecutors.newCachedSingleThreadExecutor("signal-MediaUpload");
    private final LinkedHashMap<Media, MessageSender.PreUploadResult> uploadResults = new LinkedHashMap<>();

    /* loaded from: classes4.dex */
    public interface Callback<E> {
        void onResult(E e);
    }

    public MediaUploadRepository(Context context) {
        this.context = context;
    }

    public void startUpload(Media media, Recipient recipient) {
        this.executor.execute(new Runnable(media, recipient) { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ Media f$1;
            public final /* synthetic */ Recipient f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaUploadRepository.m2079$r8$lambda$6b0Cs5sZCDAAVGub5XgsNHyeFU(MediaUploadRepository.this, this.f$1, this.f$2);
            }
        });
    }

    public void startUpload(Collection<Media> collection, Recipient recipient) {
        this.executor.execute(new Runnable(collection, recipient) { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda9
            public final /* synthetic */ Collection f$1;
            public final /* synthetic */ Recipient f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaUploadRepository.$r8$lambda$IoSwjf8RruFLOZwDid4hxBmobG0(MediaUploadRepository.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$startUpload$1(Collection collection, Recipient recipient) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Media media = (Media) it.next();
            lambda$cancelUpload$3(media);
            lambda$startUpload$0(media, recipient);
        }
    }

    public void applyMediaUpdates(Map<Media, Media> map, Recipient recipient) {
        this.executor.execute(new Runnable(map, recipient) { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda6
            public final /* synthetic */ Map f$1;
            public final /* synthetic */ Recipient f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaUploadRepository.m2078$r8$lambda$m4MWEQyrFktGvyn4hEvefBF7Z0(MediaUploadRepository.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$applyMediaUpdates$2(Map map, Recipient recipient) {
        for (Map.Entry entry : map.entrySet()) {
            Media media = (Media) entry.getKey();
            Media media2 = (Media) entry.getValue();
            if (!(media.equals(media2) && hasSameTransformProperties(media, media2)) || !this.uploadResults.containsKey(media2)) {
                lambda$cancelUpload$3(media);
                lambda$startUpload$0(media2, recipient);
            }
        }
    }

    private boolean hasSameTransformProperties(Media media, Media media2) {
        AttachmentDatabase.TransformProperties orElse = media.getTransformProperties().orElse(null);
        AttachmentDatabase.TransformProperties orElse2 = media2.getTransformProperties().orElse(null);
        if (orElse == null || orElse2 == null) {
            if (orElse == orElse2) {
                return true;
            }
            return false;
        } else if (orElse2.isVideoEdited() || orElse.getSentMediaQuality() != orElse2.getSentMediaQuality()) {
            return false;
        } else {
            return true;
        }
    }

    public void cancelUpload(Media media) {
        this.executor.execute(new Runnable(media) { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ Media f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaUploadRepository.$r8$lambda$MhTSLDXrn6C4vFfZJ3fn5rtBwB4(MediaUploadRepository.this, this.f$1);
            }
        });
    }

    public void cancelUpload(Collection<Media> collection) {
        this.executor.execute(new Runnable(collection) { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda10
            public final /* synthetic */ Collection f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaUploadRepository.m2081$r8$lambda$oxHzUAsvaKCWGmkGez0WXsNWNM(MediaUploadRepository.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$cancelUpload$4(Collection collection) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            lambda$cancelUpload$3((Media) it.next());
        }
    }

    public void cancelAllUploads() {
        this.executor.execute(new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                MediaUploadRepository.m2080$r8$lambda$FbBIND_fRB9TcnVrwEXHEmM2ss(MediaUploadRepository.this);
            }
        });
    }

    public /* synthetic */ void lambda$cancelAllUploads$5() {
        Iterator it = new HashSet(this.uploadResults.keySet()).iterator();
        while (it.hasNext()) {
            lambda$cancelUpload$3((Media) it.next());
        }
    }

    public /* synthetic */ void lambda$getPreUploadResults$6(Callback callback) {
        callback.onResult(this.uploadResults.values());
    }

    public void getPreUploadResults(Callback<Collection<MessageSender.PreUploadResult>> callback) {
        this.executor.execute(new Runnable(callback) { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda7
            public final /* synthetic */ MediaUploadRepository.Callback f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaUploadRepository.$r8$lambda$nPc6ISyvcPCEHFL4DtV2pm3vgyo(MediaUploadRepository.this, this.f$1);
            }
        });
    }

    public void updateCaptions(List<Media> list) {
        this.executor.execute(new Runnable(list) { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaUploadRepository.$r8$lambda$ZVyBQoYVRiQqX93S9poB1boP6V4(MediaUploadRepository.this, this.f$1);
            }
        });
    }

    public void updateDisplayOrder(List<Media> list) {
        this.executor.execute(new Runnable(list) { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaUploadRepository.$r8$lambda$H_xLS5wuaLGozg2rLRz902mj48o(MediaUploadRepository.this, this.f$1);
            }
        });
    }

    public void deleteAbandonedAttachments() {
        this.executor.execute(new Runnable() { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                MediaUploadRepository.$r8$lambda$4zqCcLARU9qnO0eQbsehFRUEC9A();
            }
        });
    }

    public static /* synthetic */ void lambda$deleteAbandonedAttachments$9() {
        int deleteAbandonedPreuploadedAttachments = SignalDatabase.attachments().deleteAbandonedPreuploadedAttachments();
        String str = TAG;
        Log.i(str, "Deleted " + deleteAbandonedPreuploadedAttachments + " abandoned attachments.");
    }

    /* renamed from: uploadMediaInternal */
    public void lambda$startUpload$0(Media media, Recipient recipient) {
        MessageSender.PreUploadResult preUploadPushAttachment = MessageSender.preUploadPushAttachment(this.context, asAttachment(this.context, media), recipient, media);
        if (preUploadPushAttachment != null) {
            this.uploadResults.put(media, preUploadPushAttachment);
            return;
        }
        String str = TAG;
        Log.w(str, "Failed to upload media with URI: " + media.getUri());
    }

    /* renamed from: cancelUploadInternal */
    public void lambda$cancelUpload$3(Media media) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        MessageSender.PreUploadResult preUploadResult = this.uploadResults.get(media);
        if (preUploadResult != null) {
            Stream of = Stream.of(preUploadResult.getJobIds());
            Objects.requireNonNull(jobManager);
            of.forEach(new Consumer() { // from class: org.thoughtcrime.securesms.mediasend.MediaUploadRepository$$ExternalSyntheticLambda3
                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    JobManager.this.cancel((String) obj);
                }
            });
            this.uploadResults.remove(media);
        }
    }

    /* renamed from: updateCaptionsInternal */
    public void lambda$updateCaptions$7(List<Media> list) {
        AttachmentDatabase attachments = SignalDatabase.attachments();
        for (Media media : list) {
            MessageSender.PreUploadResult preUploadResult = this.uploadResults.get(media);
            if (preUploadResult != null) {
                attachments.updateAttachmentCaption(preUploadResult.getAttachmentId(), media.getCaption().orElse(null));
            } else {
                String str = TAG;
                Log.w(str, "When updating captions, no pre-upload result could be found for media with URI: " + media.getUri());
            }
        }
    }

    /* renamed from: updateDisplayOrderInternal */
    public void lambda$updateDisplayOrder$8(List<Media> list) {
        HashMap hashMap = new HashMap();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (int i = 0; i < list.size(); i++) {
            Media media = list.get(i);
            MessageSender.PreUploadResult preUploadResult = this.uploadResults.get(media);
            if (preUploadResult != null) {
                hashMap.put(preUploadResult.getAttachmentId(), Integer.valueOf(i));
                linkedHashMap.put(media, preUploadResult);
            } else {
                String str = TAG;
                Log.w(str, "When updating display order, no pre-upload result could be found for media with URI: " + media.getUri());
            }
        }
        SignalDatabase.attachments().updateDisplayOrder(hashMap);
        if (linkedHashMap.size() == this.uploadResults.size()) {
            this.uploadResults.clear();
            this.uploadResults.putAll(linkedHashMap);
        }
    }

    public static Attachment asAttachment(Context context, Media media) {
        if (MediaUtil.isVideoType(media.getMimeType())) {
            return new VideoSlide(context, media.getUri(), media.getSize(), media.isVideoGif(), media.getWidth(), media.getHeight(), media.getCaption().orElse(null), media.getTransformProperties().orElse(null)).asAttachment();
        }
        if (MediaUtil.isGif(media.getMimeType())) {
            return new GifSlide(context, media.getUri(), media.getSize(), media.getWidth(), media.getHeight(), media.isBorderless(), media.getCaption().orElse(null)).asAttachment();
        }
        if (MediaUtil.isImageType(media.getMimeType())) {
            return new ImageSlide(context, media.getUri(), media.getMimeType(), media.getSize(), media.getWidth(), media.getHeight(), media.isBorderless(), media.getCaption().orElse(null), null, media.getTransformProperties().orElse(null)).asAttachment();
        }
        if (MediaUtil.isTextType(media.getMimeType())) {
            return new TextSlide(context, media.getUri(), null, media.getSize()).asAttachment();
        }
        throw new AssertionError("Unexpected mimeType: " + media.getMimeType());
    }
}
