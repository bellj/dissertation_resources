package org.thoughtcrime.securesms.mediasend.v2.capture;

import java.io.FileInputStream;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: MediaCaptureRepository.kt */
/* access modifiers changed from: package-private */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "it", "Ljava/io/FileInputStream;", "invoke", "(Ljava/io/FileInputStream;)Ljava/lang/Long;"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaCaptureRepository$renderVideoToMedia$1$media$2 extends Lambda implements Function1<FileInputStream, Long> {
    public static final MediaCaptureRepository$renderVideoToMedia$1$media$2 INSTANCE = new MediaCaptureRepository$renderVideoToMedia$1$media$2();

    MediaCaptureRepository$renderVideoToMedia$1$media$2() {
        super(1);
    }

    public final Long invoke(FileInputStream fileInputStream) {
        Intrinsics.checkNotNullParameter(fileInputStream, "it");
        return Long.valueOf(fileInputStream.getChannel().size());
    }
}
