package org.thoughtcrime.securesms.mediasend.v2.text.send;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mediasend.v2.text.TextStoryPostCreationState;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: TextStoryPostSendViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001*B\u000f\u0012\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b(\u0010)J\b\u0010\u0003\u001a\u00020\u0002H\u0014J\u0006\u0010\u0004\u001a\u00020\u0002J\u0006\u0010\u0005\u001a\u00020\u0002J&\u0010\r\u001a\u00020\u00022\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\n\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000bR\u0014\u0010\u000f\u001a\u00020\u000e8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00118\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014RP\u0010\u0019\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0017 \u0018*\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u00160\u0016 \u0018*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0017 \u0018*\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u00160\u0016\u0018\u00010\u00150\u00158\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0014\u0010\u001c\u001a\u00020\u001b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u001d\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00120\u001e8\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R#\u0010$\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00170\u00160#8\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendViewModel;", "Landroidx/lifecycle/ViewModel;", "", "onCleared", "onSending", "onSendCancelled", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "contactSearchKeys", "Lorg/thoughtcrime/securesms/mediasend/v2/text/TextStoryPostCreationState;", "textStoryPostCreationState", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;", "linkPreview", "onSend", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendRepository;", "repository", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendRepository;", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendState;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "kotlin.jvm.PlatformType", "untrustedIdentitySubject", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "Landroidx/lifecycle/LiveData;", "state", "Landroidx/lifecycle/LiveData;", "getState", "()Landroidx/lifecycle/LiveData;", "Lio/reactivex/rxjava3/core/Observable;", "untrustedIdentities", "Lio/reactivex/rxjava3/core/Observable;", "getUntrustedIdentities", "()Lio/reactivex/rxjava3/core/Observable;", "<init>", "(Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendRepository;)V", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class TextStoryPostSendViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final TextStoryPostSendRepository repository;
    private final LiveData<TextStoryPostSendState> state;
    private final Store<TextStoryPostSendState> store;
    private final Observable<List<IdentityRecord>> untrustedIdentities;
    private final PublishSubject<List<IdentityRecord>> untrustedIdentitySubject;

    public TextStoryPostSendViewModel(TextStoryPostSendRepository textStoryPostSendRepository) {
        Intrinsics.checkNotNullParameter(textStoryPostSendRepository, "repository");
        this.repository = textStoryPostSendRepository;
        Store<TextStoryPostSendState> store = new Store<>(TextStoryPostSendState.INIT);
        this.store = store;
        PublishSubject<List<IdentityRecord>> create = PublishSubject.create();
        this.untrustedIdentitySubject = create;
        LiveData<TextStoryPostSendState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        Intrinsics.checkNotNullExpressionValue(create, "untrustedIdentitySubject");
        this.untrustedIdentities = create;
    }

    public final LiveData<TextStoryPostSendState> getState() {
        return this.state;
    }

    public final Observable<List<IdentityRecord>> getUntrustedIdentities() {
        return this.untrustedIdentities;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void onSending() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostSendViewModel.m2286onSending$lambda0((TextStoryPostSendState) obj);
            }
        });
    }

    /* renamed from: onSending$lambda-0 */
    public static final TextStoryPostSendState m2286onSending$lambda0(TextStoryPostSendState textStoryPostSendState) {
        return TextStoryPostSendState.SENDING;
    }

    public final void onSendCancelled() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostSendViewModel.m2285onSendCancelled$lambda1((TextStoryPostSendState) obj);
            }
        });
    }

    /* renamed from: onSendCancelled$lambda-1 */
    public static final TextStoryPostSendState m2285onSendCancelled$lambda1(TextStoryPostSendState textStoryPostSendState) {
        return TextStoryPostSendState.INIT;
    }

    public final void onSend(Set<? extends ContactSearchKey> set, TextStoryPostCreationState textStoryPostCreationState, LinkPreview linkPreview) {
        Intrinsics.checkNotNullParameter(set, "contactSearchKeys");
        Intrinsics.checkNotNullParameter(textStoryPostCreationState, "textStoryPostCreationState");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TextStoryPostSendViewModel.m2284onSend$lambda2((TextStoryPostSendState) obj);
            }
        });
        DisposableKt.plusAssign(this.disposables, SubscribersKt.subscribeBy(this.repository.send(set, textStoryPostCreationState, linkPreview), new TextStoryPostSendViewModel$onSend$2(this), new TextStoryPostSendViewModel$onSend$3(this)));
    }

    /* renamed from: onSend$lambda-2 */
    public static final TextStoryPostSendState m2284onSend$lambda2(TextStoryPostSendState textStoryPostSendState) {
        return TextStoryPostSendState.SENDING;
    }

    /* compiled from: TextStoryPostSendViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendRepository;", "(Lorg/thoughtcrime/securesms/mediasend/v2/text/send/TextStoryPostSendRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final TextStoryPostSendRepository repository;

        public Factory(TextStoryPostSendRepository textStoryPostSendRepository) {
            Intrinsics.checkNotNullParameter(textStoryPostSendRepository, "repository");
            this.repository = textStoryPostSendRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new TextStoryPostSendViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.mediasend.v2.text.send.TextStoryPostSendViewModel.Factory.create");
        }
    }
}
