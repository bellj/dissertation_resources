package org.thoughtcrime.securesms.mediasend.v2;

import android.content.Context;
import android.net.Uri;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.MaybeEmitter;
import io.reactivex.rxjava3.core.MaybeOnSubscribe;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.BreakIteratorCompat;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.signal.imageeditor.core.model.EditorModel;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contactshare.ContactShareEditActivity;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.keyvalue.StorySend;
import org.thoughtcrime.securesms.mediasend.CompositeMediaTransform;
import org.thoughtcrime.securesms.mediasend.ImageEditorModelRenderMediaTransform;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.MediaRepository;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.mediasend.MediaTransform;
import org.thoughtcrime.securesms.mediasend.MediaUploadRepository;
import org.thoughtcrime.securesms.mediasend.SentMediaQualityTransform;
import org.thoughtcrime.securesms.mediasend.VideoEditorFragment;
import org.thoughtcrime.securesms.mediasend.VideoTrimTransform;
import org.thoughtcrime.securesms.mediasend.v2.MediaValidator;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.OutgoingSecureMediaMessage;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.scribbles.ImageEditorFragment;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: MediaSelectionRepository.kt */
@Metadata(d1 = {"\u0000¸\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001@B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J>\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u00102\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00110\u00142\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00010\u00102\u0006\u0010\u0017\u001a\u00020\u0018H\u0003J\u0014\u0010\u0019\u001a\u00020\u001a2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00110\u0014J\u0014\u0010\u001b\u001a\u00020\u001a2\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00110\u0014J\u0014\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u001eH\u0002J\u0018\u0010 \u001a\u00020\u00072\b\u0010!\u001a\u0004\u0018\u00010\"2\u0006\u0010#\u001a\u00020\u0007J2\u0010$\u001a\b\u0012\u0004\u0012\u00020&0%2\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00110\u00142\u0006\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u0007J~\u0010,\u001a\b\u0012\u0004\u0012\u00020.0-2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00110\u00142\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00010\u00102\u0006\u0010\u0017\u001a\u00020\u00182\b\u0010/\u001a\u0004\u0018\u0001002\u0006\u0010#\u001a\u00020\u00072\u0006\u00101\u001a\u00020\u00072\b\u00102\u001a\u0004\u0018\u0001032\f\u00104\u001a\b\u0012\u0004\u0012\u0002030\u00142\f\u00105\u001a\b\u0012\u0004\u0012\u0002060\u00142\u0006\u00107\u001a\u000208JP\u00109\u001a\u00020\u001a2\f\u00104\u001a\b\u0012\u0004\u0012\u0002030\u00142\u0006\u0010\u001f\u001a\u00020\u001e2\f\u0010:\u001a\b\u0012\u0004\u0012\u00020<0;2\f\u00105\u001a\b\u0012\u0004\u0012\u0002060\u00142\u0006\u00101\u001a\u00020\u00072\f\u0010=\u001a\b\u0012\u0004\u0012\u00020\u00110\u0014H\u0003J\f\u0010>\u001a\u00020?*\u00020\u0011H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006A"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "isMetered", "Lio/reactivex/rxjava3/core/Observable;", "", "()Lio/reactivex/rxjava3/core/Observable;", "mediaRepository", "Lorg/thoughtcrime/securesms/mediasend/MediaRepository;", "uploadRepository", "Lorg/thoughtcrime/securesms/mediasend/MediaUploadRepository;", "getUploadRepository", "()Lorg/thoughtcrime/securesms/mediasend/MediaUploadRepository;", "buildModelsToTransform", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "Lorg/thoughtcrime/securesms/mediasend/MediaTransform;", "selectedMedia", "", "stateMap", "Landroid/net/Uri;", "quality", "Lorg/thoughtcrime/securesms/mms/SentMediaQuality;", "cleanUp", "", "deleteBlobs", "media", "getTruncatedBody", "", "body", "isLocalSelfSend", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "isSms", "populateAndFilterMedia", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterResult;", "mediaConstraints", "Lorg/thoughtcrime/securesms/mms/MediaConstraints;", "maxSelection", "", "isStory", "send", "Lio/reactivex/rxjava3/core/Maybe;", "Lorg/thoughtcrime/securesms/mediasend/MediaSendActivityResult;", "message", "", "isViewOnce", "singleContact", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", ContactShareEditActivity.KEY_CONTACTS, "mentions", "Lorg/thoughtcrime/securesms/database/model/Mention;", "sendType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "sendMessages", "preUploadResults", "", "Lorg/thoughtcrime/securesms/sms/MessageSender$PreUploadResult;", "storyClips", "asKey", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionRepository$MediaKey;", "MediaKey", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaSelectionRepository {
    private final Context context;
    private final Observable<Boolean> isMetered;
    private final MediaRepository mediaRepository = new MediaRepository();
    private final MediaUploadRepository uploadRepository;

    public MediaSelectionRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        Context applicationContext = context.getApplicationContext();
        Intrinsics.checkNotNullExpressionValue(applicationContext, "context.applicationContext");
        this.context = applicationContext;
        this.uploadRepository = new MediaUploadRepository(applicationContext);
        this.isMetered = MeteredConnectivity.INSTANCE.isMetered(applicationContext);
    }

    public final MediaUploadRepository getUploadRepository() {
        return this.uploadRepository;
    }

    public final Observable<Boolean> isMetered() {
        return this.isMetered;
    }

    public final Single<MediaValidator.FilterResult> populateAndFilterMedia(List<? extends Media> list, MediaConstraints mediaConstraints, int i, boolean z) {
        Intrinsics.checkNotNullParameter(list, "media");
        Intrinsics.checkNotNullParameter(mediaConstraints, "mediaConstraints");
        Single<MediaValidator.FilterResult> subscribeOn = Single.fromCallable(new Callable(list, mediaConstraints, i, z) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ List f$1;
            public final /* synthetic */ MediaConstraints f$2;
            public final /* synthetic */ int f$3;
            public final /* synthetic */ boolean f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return MediaSelectionRepository.m2105populateAndFilterMedia$lambda0(MediaSelectionRepository.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      val…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: populateAndFilterMedia$lambda-0 */
    public static final MediaValidator.FilterResult m2105populateAndFilterMedia$lambda0(MediaSelectionRepository mediaSelectionRepository, List list, MediaConstraints mediaConstraints, int i, boolean z) {
        Intrinsics.checkNotNullParameter(mediaSelectionRepository, "this$0");
        Intrinsics.checkNotNullParameter(list, "$media");
        Intrinsics.checkNotNullParameter(mediaConstraints, "$mediaConstraints");
        List<Media> populatedMedia = mediaSelectionRepository.mediaRepository.getPopulatedMedia(mediaSelectionRepository.context, list);
        MediaValidator mediaValidator = MediaValidator.INSTANCE;
        Context context = mediaSelectionRepository.context;
        Intrinsics.checkNotNullExpressionValue(populatedMedia, "populatedMedia");
        return mediaValidator.filterMedia(context, populatedMedia, mediaConstraints, i, z);
    }

    public final Maybe<MediaSendActivityResult> send(List<? extends Media> list, Map<Uri, ? extends Object> map, SentMediaQuality sentMediaQuality, CharSequence charSequence, boolean z, boolean z2, ContactSearchKey.RecipientSearchKey recipientSearchKey, List<? extends ContactSearchKey.RecipientSearchKey> list2, List<? extends Mention> list3, MessageSendType messageSendType) {
        Intrinsics.checkNotNullParameter(list, "selectedMedia");
        Intrinsics.checkNotNullParameter(map, "stateMap");
        Intrinsics.checkNotNullParameter(sentMediaQuality, "quality");
        Intrinsics.checkNotNullParameter(list2, ContactShareEditActivity.KEY_CONTACTS);
        Intrinsics.checkNotNullParameter(list3, "mentions");
        Intrinsics.checkNotNullParameter(messageSendType, "sendType");
        if (z && (!list2.isEmpty())) {
            throw new IllegalStateException("Provided recipients to send to, but this is SMS!");
        } else if (!list.isEmpty()) {
            Maybe<MediaSendActivityResult> cast = Maybe.create(new MaybeOnSubscribe(z2, this, charSequence, list3, list, map, sentMediaQuality, recipientSearchKey, z, messageSendType, list2) { // from class: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionRepository$$ExternalSyntheticLambda3
                public final /* synthetic */ boolean f$0;
                public final /* synthetic */ MediaSelectionRepository f$1;
                public final /* synthetic */ List f$10;
                public final /* synthetic */ CharSequence f$2;
                public final /* synthetic */ List f$3;
                public final /* synthetic */ List f$4;
                public final /* synthetic */ Map f$5;
                public final /* synthetic */ SentMediaQuality f$6;
                public final /* synthetic */ ContactSearchKey.RecipientSearchKey f$7;
                public final /* synthetic */ boolean f$8;
                public final /* synthetic */ MessageSendType f$9;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                    this.f$5 = r6;
                    this.f$6 = r7;
                    this.f$7 = r8;
                    this.f$8 = r9;
                    this.f$9 = r10;
                    this.f$10 = r11;
                }

                @Override // io.reactivex.rxjava3.core.MaybeOnSubscribe
                public final void subscribe(MaybeEmitter maybeEmitter) {
                    MediaSelectionRepository.m2106send$lambda10(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9, this.f$10, maybeEmitter);
                }
            }).subscribeOn(Schedulers.io()).cast(MediaSendActivityResult.class);
            Intrinsics.checkNotNullExpressionValue(cast, "create<MediaSendActivity…tivityResult::class.java)");
            return cast;
        } else {
            throw new IllegalStateException("No selected media!");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x0275 A[LOOP:6: B:103:0x026f->B:105:0x0275, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007b A[LOOP:0: B:16:0x0075->B:18:0x007b, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x020e  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0249  */
    /* renamed from: send$lambda-10 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m2106send$lambda10(boolean r36, org.thoughtcrime.securesms.mediasend.v2.MediaSelectionRepository r37, java.lang.CharSequence r38, java.util.List r39, java.util.List r40, java.util.Map r41, org.thoughtcrime.securesms.mms.SentMediaQuality r42, org.thoughtcrime.securesms.contacts.paged.ContactSearchKey.RecipientSearchKey r43, boolean r44, org.thoughtcrime.securesms.conversation.MessageSendType r45, java.util.List r46, io.reactivex.rxjava3.core.MaybeEmitter r47) {
        /*
        // Method dump skipped, instructions count: 749
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionRepository.m2106send$lambda10(boolean, org.thoughtcrime.securesms.mediasend.v2.MediaSelectionRepository, java.lang.CharSequence, java.util.List, java.util.List, java.util.Map, org.thoughtcrime.securesms.mms.SentMediaQuality, org.thoughtcrime.securesms.contacts.paged.ContactSearchKey$RecipientSearchKey, boolean, org.thoughtcrime.securesms.conversation.MessageSendType, java.util.List, io.reactivex.rxjava3.core.MaybeEmitter):void");
    }

    /* renamed from: send$lambda-10$lambda-1 */
    public static final String m2107send$lambda10$lambda1(AttachmentDatabase.TransformProperties transformProperties) {
        Intrinsics.checkNotNullParameter(transformProperties, "t");
        return "" + transformProperties.isVideoTrim();
    }

    /* renamed from: send$lambda-10$lambda-9 */
    public static final void m2108send$lambda10$lambda9(List list, MediaSelectionRepository mediaSelectionRepository, String str, List list2, boolean z, List list3, List list4, MaybeEmitter maybeEmitter, Recipient recipient, MessageSendType messageSendType, StoryType storyType, boolean z2, List list5, ContactSearchKey.RecipientSearchKey recipientSearchKey, String str2, Collection collection) {
        Intrinsics.checkNotNullParameter(list, "$contacts");
        Intrinsics.checkNotNullParameter(mediaSelectionRepository, "this$0");
        Intrinsics.checkNotNullParameter(str, "$splitBody");
        Intrinsics.checkNotNullParameter(list2, "$trimmedMentions");
        Intrinsics.checkNotNullParameter(list3, "$clippedVideosForStories");
        Intrinsics.checkNotNullParameter(list4, "$lowResImagesForStories");
        Intrinsics.checkNotNullParameter(messageSendType, "$sendType");
        Intrinsics.checkNotNullParameter(storyType, "$storyType");
        Intrinsics.checkNotNullParameter(list5, "$updatedMedia");
        Intrinsics.checkNotNullParameter(str2, "$trimmedBody");
        Intrinsics.checkNotNullParameter(collection, "uploadResults");
        if (!list.isEmpty()) {
            mediaSelectionRepository.sendMessages(list, str, collection, list2, z, CollectionsKt___CollectionsKt.plus((Collection) list3, (Iterable) list4));
            mediaSelectionRepository.uploadRepository.deleteAbandonedAttachments();
            maybeEmitter.onComplete();
        } else if (!collection.isEmpty()) {
            Intrinsics.checkNotNull(recipient);
            maybeEmitter.onSuccess(MediaSendActivityResult.forPreUpload(recipient.getId(), collection, str, messageSendType, z, list2, storyType));
        } else {
            String str3 = MediaSelectionRepositoryKt.TAG;
            Log.w(str3, "Got empty upload results! isSms: " + z2 + ", updatedMedia.size(): " + list5.size() + ", isViewOnce: " + z + ", target: " + recipientSearchKey);
            Intrinsics.checkNotNull(recipient);
            maybeEmitter.onSuccess(MediaSendActivityResult.forTraditionalSend(recipient.getId(), list5, str2, messageSendType, z, list2, storyType));
        }
    }

    private final String getTruncatedBody(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        BreakIteratorCompat instance = BreakIteratorCompat.getInstance();
        instance.setText(str);
        return instance.take(Stories.MAX_BODY_SIZE).toString();
    }

    public final void cleanUp(List<? extends Media> list) {
        Intrinsics.checkNotNullParameter(list, "selectedMedia");
        deleteBlobs(list);
        this.uploadRepository.cancelAllUploads();
        this.uploadRepository.deleteAbandonedAttachments();
    }

    public final boolean isLocalSelfSend(Recipient recipient, boolean z) {
        return MessageSender.isLocalSelfSend(this.context, recipient, z);
    }

    private final Map<Media, MediaTransform> buildModelsToTransform(List<? extends Media> list, Map<Uri, ? extends Object> map, SentMediaQuality sentMediaQuality) {
        Object obj;
        EditorModel readModel;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Media media : list) {
            Object obj2 = map.get(media.getUri());
            if ((obj2 instanceof ImageEditorFragment.Data) && (readModel = ((ImageEditorFragment.Data) obj2).readModel()) != null && readModel.isChanged()) {
                linkedHashMap.put(media, new ImageEditorModelRenderMediaTransform(readModel));
            }
            if (obj2 instanceof VideoEditorFragment.Data) {
                VideoEditorFragment.Data data = (VideoEditorFragment.Data) obj2;
                if (data.isDurationEdited()) {
                    linkedHashMap.put(media, new VideoTrimTransform(data));
                }
            }
            if (sentMediaQuality == SentMediaQuality.HIGH) {
                MediaTransform mediaTransform = (MediaTransform) linkedHashMap.get(media);
                if (mediaTransform == null) {
                    obj = new SentMediaQualityTransform(sentMediaQuality);
                } else {
                    obj = new CompositeMediaTransform(mediaTransform, new SentMediaQualityTransform(sentMediaQuality));
                }
                linkedHashMap.put(media, obj);
            }
        }
        return linkedHashMap;
    }

    private final void sendMessages(List<? extends ContactSearchKey.RecipientSearchKey> list, String str, Collection<? extends MessageSender.PreUploadResult> collection, List<? extends Mention> list2, boolean z, List<? extends Media> list3) {
        StoryType storyType;
        long j;
        long j2;
        long j3;
        Iterator<? extends ContactSearchKey.RecipientSearchKey> it;
        boolean z2;
        ArrayList arrayList = new ArrayList(list.size());
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        ArrayList arrayList2 = new ArrayList();
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        LinkedHashMap linkedHashMap3 = new LinkedHashMap();
        Iterator<? extends ContactSearchKey.RecipientSearchKey> it2 = list.iterator();
        while (it2.hasNext()) {
            ContactSearchKey.RecipientSearchKey recipientSearchKey = (ContactSearchKey.RecipientSearchKey) it2.next();
            Recipient resolved = Recipient.resolved(recipientSearchKey.getRecipientId());
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(contact.recipientId)");
            boolean z3 = recipientSearchKey.isStory() || resolved.isDistributionList();
            if (z3 && resolved.isActiveGroup() && resolved.isGroup()) {
                SignalDatabase.Companion.groups().markDisplayAsStory(resolved.requireGroupId());
            }
            if (z3 && !resolved.isMyStory()) {
                SignalStore.storyValues().setLatestStorySend(StorySend.Companion.newSend(resolved));
            }
            if (resolved.isDistributionList()) {
                DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
                DistributionListId requireDistributionListId = resolved.requireDistributionListId();
                Intrinsics.checkNotNullExpressionValue(requireDistributionListId, "recipient.requireDistributionListId()");
                storyType = distributionLists.getStoryType(requireDistributionListId);
            } else if (z3) {
                storyType = StoryType.STORY_WITH_REPLIES;
            } else {
                storyType = StoryType.NONE;
            }
            List list4 = CollectionsKt__CollectionsKt.emptyList();
            if (resolved.isDistributionList()) {
                Object obj = CollectionsKt___CollectionsKt.first(collection);
                Object obj2 = linkedHashMap2.get(obj);
                if (obj2 == null) {
                    obj2 = Long.valueOf(System.currentTimeMillis());
                    linkedHashMap2.put(obj, obj2);
                }
                j = ((Number) obj2).longValue();
            } else {
                j = System.currentTimeMillis();
            }
            OutgoingMediaMessage outgoingMediaMessage = new OutgoingMediaMessage(resolved, str, list4, j, -1, TimeUnit.SECONDS.toMillis((long) resolved.getExpiresInSeconds()), z, 2, storyType, null, false, null, CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList(), list2, new LinkedHashSet(), new LinkedHashSet(), null);
            if (z3) {
                ArrayList<MessageSender.PreUploadResult> arrayList3 = new ArrayList();
                for (Object obj3 : collection) {
                    MessageSender.PreUploadResult preUploadResult = (MessageSender.PreUploadResult) obj3;
                    if (!(list3 instanceof Collection) || !list3.isEmpty()) {
                        for (Media media : list3) {
                            it = it2;
                            if (Intrinsics.areEqual(media.getUri(), preUploadResult.getMedia().getUri())) {
                                z2 = true;
                                break;
                            }
                            it2 = it;
                        }
                    }
                    it = it2;
                    z2 = false;
                    if (!z2) {
                        arrayList3.add(obj3);
                    }
                    it2 = it;
                }
                for (MessageSender.PreUploadResult preUploadResult2 : arrayList3) {
                    List list5 = (List) linkedHashMap.get(preUploadResult2);
                    if (list5 == null) {
                        list5 = new ArrayList();
                    }
                    OutgoingSecureMediaMessage outgoingSecureMediaMessage = new OutgoingSecureMediaMessage(outgoingMediaMessage);
                    if (resolved.isDistributionList()) {
                        Object obj4 = linkedHashMap2.get(preUploadResult2);
                        if (obj4 == null) {
                            obj4 = Long.valueOf(System.currentTimeMillis());
                            linkedHashMap2.put(preUploadResult2, obj4);
                        }
                        j3 = ((Number) obj4).longValue();
                    } else {
                        j3 = System.currentTimeMillis();
                    }
                    OutgoingSecureMediaMessage withSentTimestamp = outgoingSecureMediaMessage.withSentTimestamp(j3);
                    Intrinsics.checkNotNullExpressionValue(withSentTimestamp, "OutgoingSecureMediaMessa…          }\n            )");
                    list5.add(withSentTimestamp);
                    linkedHashMap.put(preUploadResult2, list5);
                    ThreadUtil.sleep(5);
                }
                for (Media media2 : list3) {
                    List list6 = CollectionsKt__CollectionsJVMKt.listOf(MediaUploadRepository.asAttachment(this.context, media2));
                    if (resolved.isDistributionList()) {
                        MediaKey asKey = asKey(media2);
                        Object obj5 = linkedHashMap3.get(asKey);
                        if (obj5 == null) {
                            obj5 = Long.valueOf(System.currentTimeMillis());
                            linkedHashMap3.put(asKey, obj5);
                        }
                        j2 = ((Number) obj5).longValue();
                    } else {
                        j2 = System.currentTimeMillis();
                    }
                    arrayList2.add(new OutgoingSecureMediaMessage(new OutgoingMediaMessage(resolved, str, list6, j2, -1, TimeUnit.SECONDS.toMillis((long) resolved.getExpiresInSeconds()), z, 2, storyType, null, false, null, CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList(), list2, new LinkedHashSet(), new LinkedHashSet(), null)));
                    ThreadUtil.sleep(5);
                    linkedHashMap2 = linkedHashMap2;
                }
                it2 = it2;
            } else {
                arrayList.add(new OutgoingSecureMediaMessage(outgoingMediaMessage));
                ThreadUtil.sleep(5);
                linkedHashMap2 = linkedHashMap2;
            }
        }
        if (!arrayList.isEmpty()) {
            String str2 = MediaSelectionRepositoryKt.TAG;
            Log.d(str2, "Sending " + arrayList.size() + " non-story preupload messages");
            MessageSender.sendMediaBroadcast(this.context, arrayList, collection);
        }
        if (!linkedHashMap.isEmpty()) {
            String str3 = MediaSelectionRepositoryKt.TAG;
            Log.d(str3, "Sending " + linkedHashMap.size() + " preload messages to stories");
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                MessageSender.sendMediaBroadcast(this.context, (List) entry.getValue(), Collections.singleton((MessageSender.PreUploadResult) entry.getKey()));
            }
        }
        if (!arrayList2.isEmpty()) {
            String str4 = MediaSelectionRepositoryKt.TAG;
            Log.d(str4, "Sending " + arrayList2.size() + " clip messages to stories");
            MessageSender.sendStories(this.context, arrayList2, null, null);
        }
    }

    private final MediaKey asKey(Media media) {
        Optional<AttachmentDatabase.TransformProperties> transformProperties = media.getTransformProperties();
        Intrinsics.checkNotNullExpressionValue(transformProperties, "this.transformProperties");
        return new MediaKey(media, transformProperties);
    }

    /* compiled from: MediaSelectionRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u0017\u0010\u0018J\t\u0010\u0003\u001a\u00020\u0002HÆ\u0003J\u000f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004HÆ\u0003J#\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0007\u001a\u00020\u00022\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004HÆ\u0001J\t\u0010\u000b\u001a\u00020\nHÖ\u0001J\t\u0010\r\u001a\u00020\fHÖ\u0001J\u0013\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\u0007\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001d\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaSelectionRepository$MediaKey;", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "component1", "j$/util/Optional", "Lorg/thoughtcrime/securesms/database/AttachmentDatabase$TransformProperties;", "component2", "media", "mediaTransform", "copy", "", "toString", "", "hashCode", "other", "", "equals", "Lorg/thoughtcrime/securesms/mediasend/Media;", "getMedia", "()Lorg/thoughtcrime/securesms/mediasend/Media;", "Lj$/util/Optional;", "getMediaTransform", "()Lj$/util/Optional;", "<init>", "(Lorg/thoughtcrime/securesms/mediasend/Media;Lj$/util/Optional;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes4.dex */
    public static final class MediaKey {
        private final Media media;
        private final Optional<AttachmentDatabase.TransformProperties> mediaTransform;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.mediasend.v2.MediaSelectionRepository$MediaKey */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ MediaKey copy$default(MediaKey mediaKey, Media media, Optional optional, int i, Object obj) {
            if ((i & 1) != 0) {
                media = mediaKey.media;
            }
            if ((i & 2) != 0) {
                optional = mediaKey.mediaTransform;
            }
            return mediaKey.copy(media, optional);
        }

        public final Media component1() {
            return this.media;
        }

        public final Optional<AttachmentDatabase.TransformProperties> component2() {
            return this.mediaTransform;
        }

        public final MediaKey copy(Media media, Optional<AttachmentDatabase.TransformProperties> optional) {
            Intrinsics.checkNotNullParameter(media, "media");
            Intrinsics.checkNotNullParameter(optional, "mediaTransform");
            return new MediaKey(media, optional);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof MediaKey)) {
                return false;
            }
            MediaKey mediaKey = (MediaKey) obj;
            return Intrinsics.areEqual(this.media, mediaKey.media) && Intrinsics.areEqual(this.mediaTransform, mediaKey.mediaTransform);
        }

        public int hashCode() {
            return (this.media.hashCode() * 31) + this.mediaTransform.hashCode();
        }

        public String toString() {
            return "MediaKey(media=" + this.media + ", mediaTransform=" + this.mediaTransform + ')';
        }

        public MediaKey(Media media, Optional<AttachmentDatabase.TransformProperties> optional) {
            Intrinsics.checkNotNullParameter(media, "media");
            Intrinsics.checkNotNullParameter(optional, "mediaTransform");
            this.media = media;
            this.mediaTransform = optional;
        }

        public final Media getMedia() {
            return this.media;
        }

        public final Optional<AttachmentDatabase.TransformProperties> getMediaTransform() {
            return this.mediaTransform;
        }
    }

    public final void deleteBlobs(List<? extends Media> list) {
        Intrinsics.checkNotNullParameter(list, "media");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (Media media : list) {
            arrayList.add(media.getUri());
        }
        ArrayList<Uri> arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (BlobProvider.isAuthority((Uri) obj)) {
                arrayList2.add(obj);
            }
        }
        for (Uri uri : arrayList2) {
            BlobProvider.getInstance().delete(this.context, uri);
        }
    }
}
