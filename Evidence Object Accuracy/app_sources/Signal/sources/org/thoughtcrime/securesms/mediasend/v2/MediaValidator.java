package org.thoughtcrime.securesms.mediasend.v2;

import android.content.Context;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;

/* compiled from: MediaValidator.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0016\u0017B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J4\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0003J6\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0010\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J \u0010\u0014\u001a\u00020\f2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\nH\u0002J \u0010\u0015\u001a\u00020\f2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\nH\u0002¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator;", "", "()V", "filterForValidMedia", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "context", "Landroid/content/Context;", "media", "mediaConstraints", "Lorg/thoughtcrime/securesms/mms/MediaConstraints;", "isStory", "", "filterMedia", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterResult;", "maxSelection", "", "isSupportedMediaType", "mimeType", "", "isValidGif", "isValidVideo", "FilterError", "FilterResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaValidator {
    public static final MediaValidator INSTANCE = new MediaValidator();

    private MediaValidator() {
    }

    public final FilterResult filterMedia(Context context, List<? extends Media> list, MediaConstraints mediaConstraints, int i, boolean z) {
        FilterError filterError;
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(list, "media");
        Intrinsics.checkNotNullParameter(mediaConstraints, "mediaConstraints");
        List<Media> filterForValidMedia = filterForValidMedia(context, list, mediaConstraints, z);
        boolean z2 = false;
        FilterError filterError2 = null;
        if (!(filterForValidMedia.size() == list.size())) {
            if (!list.isEmpty()) {
                for (Media media : list) {
                    if (!MediaUtil.isImageOrVideoType(media.getMimeType())) {
                        break;
                    }
                }
            }
            z2 = true;
            if (z2) {
                filterError = FilterError.ItemTooLarge.INSTANCE;
            } else {
                filterError = FilterError.ItemInvalidType.INSTANCE;
            }
            filterError2 = filterError;
        }
        if (filterForValidMedia.size() > i) {
            filterError2 = FilterError.TooManyItems.INSTANCE;
        }
        List list2 = CollectionsKt___CollectionsKt.take(filterForValidMedia, i);
        boolean z3 = !list2.isEmpty();
        String str = Media.ALL_MEDIA_BUCKET_ID;
        if (z3) {
            List<Media> list3 = CollectionsKt___CollectionsKt.drop(list2, 1);
            String orElse = ((Media) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) list2))).getBucketId().orElse(str);
            for (Media media2 : list3) {
                orElse = orElse;
                if (!Util.equals(orElse, media2.getBucketId().orElse(str))) {
                    orElse = str;
                }
            }
            str = orElse;
        }
        if (list2.isEmpty()) {
            filterError2 = new FilterError.NoItems(filterError2);
        }
        return new FilterResult(list2, filterError2, str);
    }

    private final boolean isValidGif(Context context, Media media, MediaConstraints mediaConstraints) {
        return MediaUtil.isGif(media.getMimeType()) && media.getSize() < ((long) mediaConstraints.getGifMaxSize(context));
    }

    private final boolean isValidVideo(Context context, Media media, MediaConstraints mediaConstraints) {
        return MediaUtil.isVideoType(media.getMimeType()) && media.getSize() < ((long) mediaConstraints.getUncompressedVideoMaxSize(context));
    }

    private final boolean isSupportedMediaType(String str) {
        return MediaUtil.isGif(str) || MediaUtil.isImageType(str) || MediaUtil.isVideoType(str);
    }

    /* compiled from: MediaValidator.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B'\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tJ\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\bHÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bHÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\bHÖ\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterResult;", "", "filteredMedia", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "filterError", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "bucketId", "", "(Ljava/util/List;Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;Ljava/lang/String;)V", "getBucketId", "()Ljava/lang/String;", "getFilterError", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "getFilteredMedia", "()Ljava/util/List;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FilterResult {
        private final String bucketId;
        private final FilterError filterError;
        private final List<Media> filteredMedia;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.mediasend.v2.MediaValidator$FilterResult */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ FilterResult copy$default(FilterResult filterResult, List list, FilterError filterError, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                list = filterResult.filteredMedia;
            }
            if ((i & 2) != 0) {
                filterError = filterResult.filterError;
            }
            if ((i & 4) != 0) {
                str = filterResult.bucketId;
            }
            return filterResult.copy(list, filterError, str);
        }

        public final List<Media> component1() {
            return this.filteredMedia;
        }

        public final FilterError component2() {
            return this.filterError;
        }

        public final String component3() {
            return this.bucketId;
        }

        public final FilterResult copy(List<? extends Media> list, FilterError filterError, String str) {
            Intrinsics.checkNotNullParameter(list, "filteredMedia");
            return new FilterResult(list, filterError, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FilterResult)) {
                return false;
            }
            FilterResult filterResult = (FilterResult) obj;
            return Intrinsics.areEqual(this.filteredMedia, filterResult.filteredMedia) && Intrinsics.areEqual(this.filterError, filterResult.filterError) && Intrinsics.areEqual(this.bucketId, filterResult.bucketId);
        }

        public int hashCode() {
            int hashCode = this.filteredMedia.hashCode() * 31;
            FilterError filterError = this.filterError;
            int i = 0;
            int hashCode2 = (hashCode + (filterError == null ? 0 : filterError.hashCode())) * 31;
            String str = this.bucketId;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            return "FilterResult(filteredMedia=" + this.filteredMedia + ", filterError=" + this.filterError + ", bucketId=" + this.bucketId + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.mediasend.Media> */
        /* JADX WARN: Multi-variable type inference failed */
        public FilterResult(List<? extends Media> list, FilterError filterError, String str) {
            Intrinsics.checkNotNullParameter(list, "filteredMedia");
            this.filteredMedia = list;
            this.filterError = filterError;
            this.bucketId = str;
        }

        public final String getBucketId() {
            return this.bucketId;
        }

        public final FilterError getFilterError() {
            return this.filterError;
        }

        public final List<Media> getFilteredMedia() {
            return this.filteredMedia;
        }
    }

    /* compiled from: MediaValidator.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0004\u0007\b\t\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "", "()V", "ItemInvalidType", "ItemTooLarge", "NoItems", "TooManyItems", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError$ItemTooLarge;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError$ItemInvalidType;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError$TooManyItems;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError$NoItems;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class FilterError {
        public /* synthetic */ FilterError(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: MediaValidator.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError$ItemTooLarge;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class ItemTooLarge extends FilterError {
            public static final ItemTooLarge INSTANCE = new ItemTooLarge();

            private ItemTooLarge() {
                super(null);
            }
        }

        private FilterError() {
        }

        /* compiled from: MediaValidator.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError$ItemInvalidType;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class ItemInvalidType extends FilterError {
            public static final ItemInvalidType INSTANCE = new ItemInvalidType();

            private ItemInvalidType() {
                super(null);
            }
        }

        /* compiled from: MediaValidator.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError$TooManyItems;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class TooManyItems extends FilterError {
            public static final TooManyItems INSTANCE = new TooManyItems();

            private TooManyItems() {
                super(null);
            }
        }

        /* compiled from: MediaValidator.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0001¢\u0006\u0002\u0010\u0003R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0001¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError$NoItems;", "Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "cause", "(Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;)V", "getCause", "()Lorg/thoughtcrime/securesms/mediasend/v2/MediaValidator$FilterError;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class NoItems extends FilterError {
            private final FilterError cause;

            public NoItems() {
                this(null, 1, null);
            }

            public NoItems(FilterError filterError) {
                super(null);
                this.cause = filterError;
                if (!(!(filterError instanceof NoItems))) {
                    throw new IllegalArgumentException("Failed requirement.".toString());
                }
            }

            public /* synthetic */ NoItems(FilterError filterError, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? null : filterError);
            }

            public final FilterError getCause() {
                return this.cause;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0059, code lost:
        if (r5.isValidVideo(r8, r4, r10) == false) goto L_0x005c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.util.List<org.thoughtcrime.securesms.mediasend.Media> filterForValidMedia(android.content.Context r8, java.util.List<? extends org.thoughtcrime.securesms.mediasend.Media> r9, org.thoughtcrime.securesms.mms.MediaConstraints r10, boolean r11) {
        /*
            r7 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r9 = r9.iterator()
        L_0x0009:
            boolean r1 = r9.hasNext()
            if (r1 == 0) goto L_0x002b
            java.lang.Object r1 = r9.next()
            r2 = r1
            org.thoughtcrime.securesms.mediasend.Media r2 = (org.thoughtcrime.securesms.mediasend.Media) r2
            org.thoughtcrime.securesms.mediasend.v2.MediaValidator r3 = org.thoughtcrime.securesms.mediasend.v2.MediaValidator.INSTANCE
            java.lang.String r2 = r2.getMimeType()
            java.lang.String r4 = "m.mimeType"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r4)
            boolean r2 = r3.isSupportedMediaType(r2)
            if (r2 == 0) goto L_0x0009
            r0.add(r1)
            goto L_0x0009
        L_0x002b:
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            java.util.Iterator r0 = r0.iterator()
        L_0x0034:
            boolean r1 = r0.hasNext()
            r2 = 0
            r3 = 1
            if (r1 == 0) goto L_0x0062
            java.lang.Object r1 = r0.next()
            r4 = r1
            org.thoughtcrime.securesms.mediasend.Media r4 = (org.thoughtcrime.securesms.mediasend.Media) r4
            java.lang.String r5 = r4.getMimeType()
            boolean r5 = org.thoughtcrime.securesms.util.MediaUtil.isImageAndNotGif(r5)
            if (r5 != 0) goto L_0x005b
            org.thoughtcrime.securesms.mediasend.v2.MediaValidator r5 = org.thoughtcrime.securesms.mediasend.v2.MediaValidator.INSTANCE
            boolean r6 = r5.isValidGif(r8, r4, r10)
            if (r6 != 0) goto L_0x005b
            boolean r4 = r5.isValidVideo(r8, r4, r10)
            if (r4 == 0) goto L_0x005c
        L_0x005b:
            r2 = 1
        L_0x005c:
            if (r2 == 0) goto L_0x0034
            r9.add(r1)
            goto L_0x0034
        L_0x0062:
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.util.Iterator r9 = r9.iterator()
        L_0x006b:
            boolean r10 = r9.hasNext()
            if (r10 == 0) goto L_0x008c
            java.lang.Object r10 = r9.next()
            r0 = r10
            org.thoughtcrime.securesms.mediasend.Media r0 = (org.thoughtcrime.securesms.mediasend.Media) r0
            if (r11 == 0) goto L_0x0085
            org.thoughtcrime.securesms.stories.Stories$MediaTransform$SendRequirements r0 = org.thoughtcrime.securesms.stories.Stories.MediaTransform.getSendRequirements(r0)
            org.thoughtcrime.securesms.stories.Stories$MediaTransform$SendRequirements r1 = org.thoughtcrime.securesms.stories.Stories.MediaTransform.SendRequirements.CAN_NOT_SEND
            if (r0 == r1) goto L_0x0083
            goto L_0x0085
        L_0x0083:
            r0 = 0
            goto L_0x0086
        L_0x0085:
            r0 = 1
        L_0x0086:
            if (r0 == 0) goto L_0x006b
            r8.add(r10)
            goto L_0x006b
        L_0x008c:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mediasend.v2.MediaValidator.filterForValidMedia(android.content.Context, java.util.List, org.thoughtcrime.securesms.mms.MediaConstraints, boolean):java.util.List");
    }
}
