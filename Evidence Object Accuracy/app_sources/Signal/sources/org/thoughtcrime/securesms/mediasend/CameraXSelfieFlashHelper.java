package org.thoughtcrime.securesms.mediasend;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.camera.view.SignalCameraView;

/* loaded from: classes4.dex */
public final class CameraXSelfieFlashHelper {
    private static final float MAX_SCREEN_BRIGHTNESS;
    private static final float MAX_SELFIE_FLASH_ALPHA;
    private static final long SELFIE_FLASH_DURATION_MS;
    private float brightnessBeforeFlash;
    private final SignalCameraView camera;
    private boolean inFlash;
    private final View selfieFlash;
    private final Window window;

    public CameraXSelfieFlashHelper(Window window, SignalCameraView signalCameraView, View view) {
        this.window = window;
        this.camera = signalCameraView;
        this.selfieFlash = view;
    }

    public void startFlash() {
        if (!this.inFlash && shouldUseViewBasedFlash()) {
            this.inFlash = true;
            WindowManager.LayoutParams attributes = this.window.getAttributes();
            this.brightnessBeforeFlash = attributes.screenBrightness;
            attributes.screenBrightness = MAX_SCREEN_BRIGHTNESS;
            this.window.setAttributes(attributes);
            this.selfieFlash.animate().alpha(MAX_SELFIE_FLASH_ALPHA).setDuration(SELFIE_FLASH_DURATION_MS);
        }
    }

    public void endFlash() {
        if (this.inFlash) {
            WindowManager.LayoutParams attributes = this.window.getAttributes();
            attributes.screenBrightness = this.brightnessBeforeFlash;
            this.window.setAttributes(attributes);
            this.selfieFlash.animate().alpha(0.0f).setDuration(SELFIE_FLASH_DURATION_MS);
            this.inFlash = false;
        }
    }

    private boolean shouldUseViewBasedFlash() {
        Integer cameraLensFacing = this.camera.getCameraLensFacing();
        if (this.camera.getFlash() != 1 || this.camera.hasFlash() || cameraLensFacing == null || cameraLensFacing.intValue() != 0) {
            return false;
        }
        return true;
    }
}
