package org.thoughtcrime.securesms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.contacts.avatars.ContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ProfileContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ResourceContactPhoto;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.FullscreenHelper;

/* loaded from: classes.dex */
public final class AvatarPreviewActivity extends PassphraseRequiredActivity {
    private static final String RECIPIENT_ID_EXTRA;
    private static final String TAG = Log.tag(AvatarPreviewActivity.class);

    public static Intent intentFromRecipientId(Context context, RecipientId recipientId) {
        Intent intent = new Intent(context, AvatarPreviewActivity.class);
        intent.putExtra("recipient_id", recipientId.serialize());
        return intent;
    }

    public static Bundle createTransitionBundle(Activity activity, View view) {
        return ActivityOptionsCompat.makeSceneTransitionAnimation(activity, view, "avatar").toBundle();
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        getDelegate().setLocalNightMode(2);
        super.attachBaseContext(context);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        setTheme(R.style.TextSecure_MediaPreview);
        setContentView(R.layout.contact_photo_preview_activity);
        if (Build.VERSION.SDK_INT >= 21) {
            postponeEnterTransition();
            TransitionInflater from = TransitionInflater.from(this);
            getWindow().setSharedElementEnterTransition(from.inflateTransition(R.transition.full_screen_avatar_image_enter_transition_set));
            getWindow().setSharedElementReturnTransition(from.inflateTransition(R.transition.full_screen_avatar_image_return_transition_set));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView imageView = (ImageView) findViewById(R.id.avatar);
        setSupportActionBar(toolbar);
        requireSupportActionBar().setDisplayHomeAsUpEnabled(true);
        requireSupportActionBar().setDisplayShowTitleEnabled(false);
        Recipient.live(RecipientId.from(getIntent().getStringExtra("recipient_id"))).observe(this, new Observer(imageView, (EmojiTextView) findViewById(R.id.title), getApplicationContext()) { // from class: org.thoughtcrime.securesms.AvatarPreviewActivity$$ExternalSyntheticLambda0
            public final /* synthetic */ ImageView f$1;
            public final /* synthetic */ EmojiTextView f$2;
            public final /* synthetic */ Context f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AvatarPreviewActivity.m234$r8$lambda$6xCJ7iO6EomgorJ1qgrFx2HAUk(AvatarPreviewActivity.this, this.f$1, this.f$2, this.f$3, (Recipient) obj);
            }
        });
        FullscreenHelper fullscreenHelper = new FullscreenHelper(this);
        findViewById(16908290).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.AvatarPreviewActivity$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AvatarPreviewActivity.$r8$lambda$m7piJako5KRIeT0MJ4w5qNgWmWU(FullscreenHelper.this, view);
            }
        });
        fullscreenHelper.configureToolbarLayout(findViewById(R.id.toolbar_cutout_spacer), toolbar);
        fullscreenHelper.showAndHideWithSystemUI(getWindow(), findViewById(R.id.toolbar_layout));
    }

    public /* synthetic */ void lambda$onCreate$0(final ImageView imageView, EmojiTextView emojiTextView, Context context, Recipient recipient) {
        ContactPhoto contactPhoto;
        FallbackContactPhoto fallbackContactPhoto;
        if (recipient.isSelf()) {
            contactPhoto = new ProfileContactPhoto(recipient);
        } else {
            contactPhoto = recipient.getContactPhoto();
        }
        if (recipient.isSelf()) {
            fallbackContactPhoto = new ResourceContactPhoto(R.drawable.ic_profile_outline_40, R.drawable.ic_profile_outline_20, R.drawable.ic_person_large);
        } else {
            fallbackContactPhoto = recipient.getFallbackContactPhoto();
        }
        final Resources resources = getResources();
        GlideApp.with((FragmentActivity) this).asBitmap().load((Object) contactPhoto).fallback(fallbackContactPhoto.asCallCard(this)).error(fallbackContactPhoto.asCallCard(this)).diskCacheStrategy(DiskCacheStrategy.ALL).addListener((RequestListener<Bitmap>) new RequestListener<Bitmap>() { // from class: org.thoughtcrime.securesms.AvatarPreviewActivity.2
            public boolean onResourceReady(Bitmap bitmap, Object obj, Target<Bitmap> target, DataSource dataSource, boolean z) {
                return false;
            }

            @Override // com.bumptech.glide.request.RequestListener
            public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, DataSource dataSource, boolean z) {
                return onResourceReady((Bitmap) obj, obj2, (Target<Bitmap>) target, dataSource, z);
            }

            @Override // com.bumptech.glide.request.RequestListener
            public boolean onLoadFailed(GlideException glideException, Object obj, Target<Bitmap> target, boolean z) {
                Log.w(AvatarPreviewActivity.TAG, "Unable to load avatar, or avatar removed, closing");
                AvatarPreviewActivity.this.finish();
                return false;
            }
        }).into((GlideRequest<Bitmap>) new CustomTarget<Bitmap>() { // from class: org.thoughtcrime.securesms.AvatarPreviewActivity.1
            @Override // com.bumptech.glide.request.target.Target
            public void onLoadCleared(Drawable drawable) {
            }

            @Override // com.bumptech.glide.request.target.Target
            public /* bridge */ /* synthetic */ void onResourceReady(Object obj, Transition transition) {
                onResourceReady((Bitmap) obj, (Transition<? super Bitmap>) transition);
            }

            public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                imageView.setImageDrawable(RoundedBitmapDrawableFactory.create(resources, bitmap));
                if (Build.VERSION.SDK_INT >= 21) {
                    AvatarPreviewActivity.this.startPostponedEnterTransition();
                }
            }
        });
        emojiTextView.setText(recipient.getDisplayName(context));
    }

    @Override // androidx.appcompat.app.AppCompatActivity
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
