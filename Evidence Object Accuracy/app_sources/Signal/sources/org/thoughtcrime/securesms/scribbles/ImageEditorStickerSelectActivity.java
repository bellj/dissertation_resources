package org.thoughtcrime.securesms.scribbles;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.MediaKeyboard;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.keyboard.KeyboardPage;
import org.thoughtcrime.securesms.keyboard.KeyboardPagerViewModel;
import org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.sticker.StickerSearchDialogFragment;
import org.thoughtcrime.securesms.stickers.StickerEventListener;
import org.thoughtcrime.securesms.stickers.StickerManagementActivity;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public final class ImageEditorStickerSelectActivity extends AppCompatActivity implements StickerEventListener, MediaKeyboard.MediaKeyboardListener, StickerKeyboardPageFragment.Callback {
    @Override // org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
    public void onKeyboardChanged(KeyboardPage keyboardPage) {
    }

    @Override // org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
    public void onShown() {
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        getDelegate().setLocalNightMode(2);
        super.attachBaseContext(context);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.scribble_select_new_sticker_activity);
        ((KeyboardPagerViewModel) ViewModelProviders.of(this).get(KeyboardPagerViewModel.class)).setOnlyPage(KeyboardPage.STICKER);
        ((MediaKeyboard) findViewById(R.id.emoji_drawer)).show();
    }

    @Override // org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
    public void onHidden() {
        finish();
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerEventListener
    public void onStickerSelected(StickerRecord stickerRecord) {
        Intent intent = new Intent();
        intent.setData(stickerRecord.getUri());
        setResult(-1, intent);
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorStickerSelectActivity$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                ImageEditorStickerSelectActivity.lambda$onStickerSelected$0(StickerRecord.this);
            }
        });
        ViewUtil.hideKeyboard(this, findViewById(16908290));
        finish();
    }

    public static /* synthetic */ void lambda$onStickerSelected$0(StickerRecord stickerRecord) {
        SignalDatabase.stickers().updateStickerLastUsedTime(stickerRecord.getRowId(), System.currentTimeMillis());
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerEventListener
    public void onStickerManagementClicked() {
        startActivity(StickerManagementActivity.getIntent(this));
    }

    @Override // org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment.Callback
    public void openStickerSearch() {
        StickerSearchDialogFragment.show(getSupportFragmentManager());
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }
}
