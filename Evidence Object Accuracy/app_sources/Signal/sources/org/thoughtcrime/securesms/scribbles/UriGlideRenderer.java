package org.thoughtcrime.securesms.scribbles;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import java.util.concurrent.ExecutionException;
import org.signal.core.util.logging.Log;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.RendererContext;
import org.signal.imageeditor.core.SelectableRenderer;
import org.signal.imageeditor.core.model.EditorElement;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.util.BitmapUtil;

/* loaded from: classes4.dex */
public final class UriGlideRenderer implements SelectableRenderer {
    public static final Parcelable.Creator<UriGlideRenderer> CREATOR = new Parcelable.Creator<UriGlideRenderer>() { // from class: org.thoughtcrime.securesms.scribbles.UriGlideRenderer.2
        @Override // android.os.Parcelable.Creator
        public UriGlideRenderer createFromParcel(Parcel parcel) {
            Uri parse = Uri.parse(parcel.readString());
            boolean z = true;
            if (parcel.readInt() != 1) {
                z = false;
            }
            return new UriGlideRenderer(parse, z, parcel.readInt(), parcel.readInt(), parcel.readFloat());
        }

        @Override // android.os.Parcelable.Creator
        public UriGlideRenderer[] newArray(int i) {
            return new UriGlideRenderer[i];
        }
    };
    private static final int MAX_BLUR_DIMENSION;
    private static final int PREVIEW_DIMENSION_LIMIT;
    public static final float STRONG_BLUR;
    private static final String TAG = Log.tag(UriGlideRenderer.class);
    public static final float WEAK_BLUR;
    private Bitmap bitmap;
    private final RequestListener<Bitmap> bitmapRequestListener;
    private Paint blurPaint;
    private final float blurRadius;
    private final Matrix blurScaleMatrix;
    private Bitmap blurredBitmap;
    private final boolean decryptable;
    private final Matrix imageProjectionMatrix;
    private final Uri imageUri;
    private final int maxHeight;
    private final int maxWidth;
    private final Paint paint;
    private boolean selected;
    private final Matrix temp;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public UriGlideRenderer(Uri uri, boolean z, int i, int i2) {
        this(uri, z, i, i2, 25.0f);
    }

    public UriGlideRenderer(Uri uri, boolean z, int i, int i2, float f) {
        this(uri, z, i, i2, f, null);
    }

    public UriGlideRenderer(Uri uri, boolean z, int i, int i2, float f, RequestListener<Bitmap> requestListener) {
        Paint paint = new Paint();
        this.paint = paint;
        this.imageProjectionMatrix = new Matrix();
        this.temp = new Matrix();
        this.blurScaleMatrix = new Matrix();
        this.imageUri = uri;
        this.decryptable = z;
        this.maxWidth = i;
        this.maxHeight = i2;
        this.blurRadius = f;
        this.bitmapRequestListener = requestListener;
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
    }

    @Override // org.signal.imageeditor.core.Renderer
    public void render(final RendererContext rendererContext) {
        if (getBitmap() == null) {
            if (rendererContext.isBlockingLoad()) {
                try {
                    setBitmap(rendererContext, getBitmapGlideRequest(rendererContext.context, false).submit().get());
                } catch (InterruptedException | ExecutionException e) {
                    throw new RuntimeException(e);
                }
            } else {
                getBitmapGlideRequest(rendererContext.context, true).into((GlideRequest<Bitmap>) new CustomTarget<Bitmap>() { // from class: org.thoughtcrime.securesms.scribbles.UriGlideRenderer.1
                    @Override // com.bumptech.glide.request.target.Target
                    public /* bridge */ /* synthetic */ void onResourceReady(Object obj, Transition transition) {
                        onResourceReady((Bitmap) obj, (Transition<? super Bitmap>) transition);
                    }

                    public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                        UriGlideRenderer.this.setBitmap(rendererContext, bitmap);
                        rendererContext.invalidate.onInvalidate(UriGlideRenderer.this);
                    }

                    @Override // com.bumptech.glide.request.target.Target
                    public void onLoadCleared(Drawable drawable) {
                        UriGlideRenderer.this.bitmap = null;
                    }
                });
            }
        }
        Bitmap bitmap = getBitmap();
        if (bitmap != null) {
            rendererContext.save();
            rendererContext.canvasMatrix.concat(this.imageProjectionMatrix);
            int alpha = this.paint.getAlpha();
            this.paint.setAlpha(rendererContext.getAlpha(alpha));
            rendererContext.canvas.drawBitmap(bitmap, 0.0f, 0.0f, rendererContext.getMaskPaint() != null ? rendererContext.getMaskPaint() : this.paint);
            this.paint.setAlpha(alpha);
            rendererContext.restore();
            renderBlurOverlay(rendererContext);
        } else if (rendererContext.isBlockingLoad()) {
            rendererContext.canvas.drawRect(Bounds.FULL_BOUNDS, this.paint);
        }
    }

    private void renderBlurOverlay(RendererContext rendererContext) {
        boolean z = false;
        for (EditorElement editorElement : rendererContext.getChildren()) {
            if (editorElement.getZOrder() == -1) {
                if (this.blurPaint == null) {
                    Paint paint = new Paint();
                    this.blurPaint = paint;
                    paint.setAntiAlias(true);
                    this.blurPaint.setFilterBitmap(true);
                    this.blurPaint.setDither(true);
                }
                this.blurPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
                rendererContext.setMaskPaint(this.blurPaint);
                editorElement.draw(rendererContext);
                z = true;
            }
        }
        if (z) {
            rendererContext.save();
            rendererContext.canvasMatrix.concat(this.imageProjectionMatrix);
            this.blurPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP));
            this.blurPaint.setMaskFilter(null);
            if (this.blurredBitmap == null) {
                this.blurredBitmap = blur(this.bitmap, rendererContext.context, this.blurRadius);
                this.blurScaleMatrix.setRectToRect(new RectF(0.0f, 0.0f, (float) this.blurredBitmap.getWidth(), (float) this.blurredBitmap.getHeight()), new RectF(0.0f, 0.0f, (float) this.bitmap.getWidth(), (float) this.bitmap.getHeight()), Matrix.ScaleToFit.FILL);
            }
            rendererContext.canvas.concat(this.blurScaleMatrix);
            rendererContext.canvas.drawBitmap(this.blurredBitmap, 0.0f, 0.0f, this.blurPaint);
            this.blurPaint.setXfermode(null);
            rendererContext.restore();
        }
    }

    private GlideRequest<Bitmap> getBitmapGlideRequest(Context context, boolean z) {
        int i = this.maxWidth;
        int i2 = this.maxHeight;
        if (z) {
            i = Math.min(i, 2048);
            i2 = Math.min(i2, 2048);
        }
        return GlideApp.with(context).asBitmap().override(i, i2).centerInside().addListener(this.bitmapRequestListener).load(this.decryptable ? new DecryptableStreamUriLoader.DecryptableUri(this.imageUri) : this.imageUri);
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        return this.selected ? Bounds.contains(f, f2) : pixelAlphaNotZero(f, f2);
    }

    private boolean pixelAlphaNotZero(float f, float f2) {
        Bitmap bitmap = getBitmap();
        if (bitmap == null) {
            return false;
        }
        this.imageProjectionMatrix.invert(this.temp);
        float[] fArr = new float[2];
        this.temp.mapPoints(fArr, new float[]{f, f2});
        int i = (int) fArr[0];
        int i2 = (int) fArr[1];
        if (i < 0 || i >= bitmap.getWidth() || i2 < 0 || i2 >= bitmap.getHeight() || (bitmap.getPixel(i, i2) & -16777216) == 0) {
            return false;
        }
        return true;
    }

    public Bitmap getBitmap() {
        Bitmap bitmap = this.bitmap;
        if (bitmap != null && bitmap.isRecycled()) {
            this.bitmap = null;
        }
        return this.bitmap;
    }

    public void setBitmap(RendererContext rendererContext, Bitmap bitmap) {
        this.bitmap = bitmap;
        if (bitmap != null) {
            this.imageProjectionMatrix.setRectToRect(new RectF(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight()), Bounds.FULL_BOUNDS, Matrix.ScaleToFit.CENTER);
            rendererContext.rendererReady.onReady(this, cropMatrix(bitmap), new Point(bitmap.getWidth(), bitmap.getHeight()));
        }
    }

    private static Matrix cropMatrix(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        if (bitmap.getWidth() > bitmap.getHeight()) {
            matrix.preScale(1.0f, ((float) bitmap.getHeight()) / ((float) bitmap.getWidth()));
        } else {
            matrix.preScale(((float) bitmap.getWidth()) / ((float) bitmap.getHeight()), 1.0f);
        }
        return matrix;
    }

    private static Bitmap blur(Bitmap bitmap, Context context, float f) {
        Point scaleKeepingAspectRatio = scaleKeepingAspectRatio(new Point(bitmap.getWidth(), bitmap.getHeight()), 2048);
        Point scaleKeepingAspectRatio2 = scaleKeepingAspectRatio(new Point(scaleKeepingAspectRatio.x / 2, scaleKeepingAspectRatio.y / 2), 300);
        Bitmap createScaledBitmap = BitmapUtil.createScaledBitmap(bitmap, scaleKeepingAspectRatio2.x, scaleKeepingAspectRatio2.y);
        String str = TAG;
        Log.d(str, "Bitmap: " + bitmap.getWidth() + "x" + bitmap.getHeight() + ", Blur: " + scaleKeepingAspectRatio2.x + "x" + scaleKeepingAspectRatio2.y);
        RenderScript create = RenderScript.create(context);
        Allocation createFromBitmap = Allocation.createFromBitmap(create, createScaledBitmap);
        Allocation createTyped = Allocation.createTyped(create, createFromBitmap.getType());
        ScriptIntrinsicBlur create2 = ScriptIntrinsicBlur.create(create, Element.U8_4(create));
        create2.setRadius(f);
        create2.setInput(createFromBitmap);
        create2.forEach(createTyped);
        Bitmap createBitmap = Bitmap.createBitmap(createScaledBitmap.getWidth(), createScaledBitmap.getHeight(), createScaledBitmap.getConfig());
        createTyped.copyTo(createBitmap);
        return createBitmap;
    }

    private static Point scaleKeepingAspectRatio(Point point, int i) {
        int i2 = point.x;
        int i3 = point.y;
        if (i2 > i || i3 > i) {
            float f = (float) i;
            float f2 = ((float) i2) / f;
            float f3 = ((float) i3) / f;
            if (f2 > f3) {
                i3 = (int) (((float) i3) / f2);
            } else {
                i = (int) (((float) i2) / f3);
                i3 = i;
            }
        } else {
            i = i2;
        }
        return new Point(i, i3);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.imageUri.toString());
        parcel.writeInt(this.decryptable ? 1 : 0);
        parcel.writeInt(this.maxWidth);
        parcel.writeInt(this.maxHeight);
        parcel.writeFloat(this.blurRadius);
    }

    @Override // org.signal.imageeditor.core.SelectableRenderer
    public void onSelected(boolean z) {
        if (this.selected != z) {
            this.selected = z;
        }
    }

    @Override // org.signal.imageeditor.core.SelectableRenderer
    public void getSelectionBounds(RectF rectF) {
        rectF.set(Bounds.FULL_BOUNDS);
    }
}
