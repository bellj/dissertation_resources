package org.thoughtcrime.securesms.scribbles;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.math.MathKt__MathJVMKt;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: RotationDialView.kt */
@Metadata(d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 ,2\u00020\u0001:\u0005+,-./B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010 \u001a\u00020\u0010H\u0002J\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0002J\u0010\u0010%\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010&\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0014J\u0010\u0010'\u001a\u00020\u00172\u0006\u0010(\u001a\u00020)H\u0017J\u000e\u0010*\u001a\u00020\"2\u0006\u0010\u000f\u001a\u00020\u0010R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u000e\u0010\u001e\u001a\u00020\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u00060"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/RotationDialView;", "Landroid/view/View;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "angleIndicatorPaint", "Landroid/graphics/Paint;", "canvasBounds", "Landroid/graphics/Rect;", "centerMostIndicatorRect", "Landroid/graphics/RectF;", "degrees", "", "dimensions", "Lorg/thoughtcrime/securesms/scribbles/RotationDialView$Dimensions;", "gestureDetector", "Landroid/view/GestureDetector;", "indicatorRect", "isInGesture", "", "listener", "Lorg/thoughtcrime/securesms/scribbles/RotationDialView$Listener;", "getListener", "()Lorg/thoughtcrime/securesms/scribbles/RotationDialView$Listener;", "setListener", "(Lorg/thoughtcrime/securesms/scribbles/RotationDialView$Listener;)V", "snapDegrees", "textPaint", "calculateSnapDegrees", "drawText", "", "canvas", "Landroid/graphics/Canvas;", "getDialDegrees", "onDraw", "onTouchEvent", "event", "Landroid/view/MotionEvent;", "setDegrees", "Colors", "Companion", "Dimensions", "GestureListener", "Listener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RotationDialView extends View {
    public static final Companion Companion = new Companion(null);
    private static final float MAX_DEGREES;
    private static final float MIN_DEGRESS;
    private final Paint angleIndicatorPaint;
    private final Rect canvasBounds;
    private final RectF centerMostIndicatorRect;
    private float degrees;
    private final Dimensions dimensions;
    private final GestureDetector gestureDetector;
    private final RectF indicatorRect;
    private boolean isInGesture;
    private Listener listener;
    private float snapDegrees;
    private final Paint textPaint;

    /* compiled from: RotationDialView.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\b\u0010\u0006\u001a\u00020\u0003H&J\b\u0010\u0007\u001a\u00020\u0003H&¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/RotationDialView$Listener;", "", "onDegreeChanged", "", "degrees", "", "onGestureEnd", "onGestureStart", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Listener {
        void onDegreeChanged(float f);

        void onGestureEnd();

        void onGestureStart();
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public RotationDialView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public RotationDialView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ RotationDialView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RotationDialView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        this.canvasBounds = new Rect();
        this.centerMostIndicatorRect = new RectF();
        this.indicatorRect = new RectF();
        this.dimensions = new Dimensions();
        this.gestureDetector = new GestureDetector(context, new GestureListener());
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextSize((float) ViewUtil.spToPx(15.0f));
        paint.setTypeface(Typeface.DEFAULT);
        paint.setColor(Colors.INSTANCE.getTextColor());
        paint.setStyle(Paint.Style.FILL);
        paint.setTextAlign(Paint.Align.CENTER);
        this.textPaint = paint;
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setColor(-1);
        paint2.setStyle(Paint.Style.FILL);
        this.angleIndicatorPaint = paint2;
    }

    public final Listener getListener() {
        return this.listener;
    }

    public final void setListener(Listener listener) {
        this.listener = listener;
    }

    public final void setDegrees(float f) {
        Listener listener;
        if (!(f == this.degrees)) {
            this.degrees = f;
            float calculateSnapDegrees = calculateSnapDegrees();
            this.snapDegrees = calculateSnapDegrees;
            if (this.isInGesture && (listener = this.listener) != null) {
                listener.onDegreeChanged(calculateSnapDegrees);
            }
            invalidate();
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        Listener listener;
        Intrinsics.checkNotNullParameter(motionEvent, "event");
        if (motionEvent.getActionIndex() != 0) {
            return false;
        }
        this.isInGesture = this.gestureDetector.onTouchEvent(motionEvent);
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            Listener listener2 = this.listener;
            if (listener2 != null) {
                listener2.onGestureStart();
            }
        } else if (actionMasked == 1 && (listener = this.listener) != null) {
            listener.onGestureEnd();
        }
        return this.isInGesture;
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        if (isInEditMode()) {
            canvas.drawColor(-16777216);
        }
        canvas.getClipBounds(this.canvasBounds);
        float dialDegrees = getDialDegrees(this.snapDegrees);
        int i = this.canvasBounds.bottom;
        int i2 = MathKt__MathJVMKt.roundToInt(dialDegrees);
        float spaceBetweenAngleIndicators = ((float) this.dimensions.getSpaceBetweenAngleIndicators()) * (dialDegrees - ((float) i2));
        float width = ((float) getWidth()) / 2.0f;
        float f = (float) i;
        this.centerMostIndicatorRect.set(width - (((float) this.dimensions.getAngleIndicatorWidth()) / 2.0f), f - ((float) this.dimensions.getMajorAngleIndicatorHeight()), width + (((float) this.dimensions.getAngleIndicatorWidth()) / 2.0f), f);
        this.centerMostIndicatorRect.offset(-spaceBetweenAngleIndicators, 0.0f);
        this.indicatorRect.set(this.centerMostIndicatorRect);
        this.angleIndicatorPaint.setColor(Colors.INSTANCE.colorForOtherDegree(i2));
        this.indicatorRect.top = f - ((float) this.dimensions.getHeightForDegree(i2));
        canvas.drawRect(this.indicatorRect, this.angleIndicatorPaint);
        this.indicatorRect.offset((float) this.dimensions.getSpaceBetweenAngleIndicators(), 0.0f);
        int i3 = i2 + 1;
        while (this.indicatorRect.left < ((float) getWidth()) && ((float) i3) <= ((float) Math.ceil((double) MAX_DEGREES))) {
            this.angleIndicatorPaint.setColor(Colors.INSTANCE.colorForOtherDegree(i3));
            this.indicatorRect.top = f - ((float) this.dimensions.getHeightForDegree(i3));
            canvas.drawRect(this.indicatorRect, this.angleIndicatorPaint);
            this.indicatorRect.offset((float) this.dimensions.getSpaceBetweenAngleIndicators(), 0.0f);
            i3++;
        }
        this.indicatorRect.set(this.centerMostIndicatorRect);
        this.indicatorRect.offset(-((float) this.dimensions.getSpaceBetweenAngleIndicators()), 0.0f);
        int i4 = i2 - 1;
        while (this.indicatorRect.left >= 0.0f && ((float) i4) >= ((float) Math.floor((double) MIN_DEGRESS))) {
            this.angleIndicatorPaint.setColor(Colors.INSTANCE.colorForOtherDegree(i4));
            this.indicatorRect.top = f - ((float) this.dimensions.getHeightForDegree(i4));
            canvas.drawRect(this.indicatorRect, this.angleIndicatorPaint);
            this.indicatorRect.offset(-((float) this.dimensions.getSpaceBetweenAngleIndicators()), 0.0f);
            i4--;
        }
        this.centerMostIndicatorRect.offset(spaceBetweenAngleIndicators, 0.0f);
        this.angleIndicatorPaint.setColor(Colors.INSTANCE.colorForCenterDegree(i2));
        canvas.drawRect(this.centerMostIndicatorRect, this.angleIndicatorPaint);
        drawText(canvas);
    }

    private final void drawText(Canvas canvas) {
        canvas.drawText(String.valueOf(MathKt__MathJVMKt.roundToInt(getDialDegrees(this.snapDegrees))), ((float) getWidth()) / 2.0f, ((((float) this.canvasBounds.bottom) - this.textPaint.descent()) - ((float) this.dimensions.getMajorAngleIndicatorHeight())) - ((float) this.dimensions.getTextPaddingBottom()), this.textPaint);
    }

    public final float getDialDegrees(float f) {
        float f2 = f % 360.0f;
        if (f2 % ((float) 90) == 0.0f) {
            return 0.0f;
        }
        float floor = f2 - (((float) Math.floor((double) (f2 / 90.0f))) * 90.0f);
        return floor > 45.0f ? floor - 90.0f : floor;
    }

    private final float calculateSnapDegrees() {
        if (!this.isInGesture) {
            return this.degrees;
        }
        float dialDegrees = getDialDegrees(this.degrees);
        if (MathKt__MathJVMKt.roundToInt(dialDegrees) == 0) {
            return this.degrees - dialDegrees;
        }
        return this.degrees;
    }

    /* compiled from: RotationDialView.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J,\u0010\u0007\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u00062\b\u0010\t\u001a\u0004\u0018\u00010\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000bH\u0016¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/RotationDialView$GestureListener;", "Landroid/view/GestureDetector$SimpleOnGestureListener;", "(Lorg/thoughtcrime/securesms/scribbles/RotationDialView;)V", "onDown", "", "e", "Landroid/view/MotionEvent;", "onScroll", "e1", "e2", "distanceX", "", "distanceY", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        public GestureListener() {
            RotationDialView.this = r1;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            float spaceBetweenAngleIndicators = f / ((float) RotationDialView.this.dimensions.getSpaceBetweenAngleIndicators());
            RotationDialView rotationDialView = RotationDialView.this;
            float dialDegrees = rotationDialView.getDialDegrees(rotationDialView.degrees);
            RotationDialView rotationDialView2 = RotationDialView.this;
            float dialDegrees2 = rotationDialView2.getDialDegrees(rotationDialView2.degrees + spaceBetweenAngleIndicators);
            boolean z = false;
            boolean z2 = dialDegrees >= 22.499994f && dialDegrees2 <= -22.499994f;
            if (dialDegrees2 >= 22.499994f && dialDegrees <= -22.499994f) {
                z = true;
            }
            if (MathKt__MathJVMKt.roundToInt(dialDegrees) != MathKt__MathJVMKt.roundToInt(dialDegrees2) && RotationDialView.this.isHapticFeedbackEnabled()) {
                RotationDialView.this.performHapticFeedback(3);
            }
            if (z2) {
                float f3 = RotationDialView.MAX_DEGREES - dialDegrees;
                RotationDialView rotationDialView3 = RotationDialView.this;
                rotationDialView3.setDegrees(rotationDialView3.degrees + f3);
            } else if (z) {
                float abs = RotationDialView.MAX_DEGREES - Math.abs(dialDegrees);
                RotationDialView rotationDialView4 = RotationDialView.this;
                rotationDialView4.setDegrees(rotationDialView4.degrees - abs);
            } else {
                RotationDialView rotationDialView5 = RotationDialView.this;
                rotationDialView5.setDegrees(rotationDialView5.degrees + spaceBetweenAngleIndicators);
            }
            return true;
        }
    }

    /* compiled from: RotationDialView.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0013\b\u0002\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0004R\u0018\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\n\n\u0002\b\u0007\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\b\u001a\u00020\u00048\u0006X\u0004¢\u0006\n\n\u0002\b\n\u001a\u0004\b\t\u0010\u0006R\u0018\u0010\u000b\u001a\u00020\u00048\u0006X\u0004¢\u0006\n\n\u0002\b\r\u001a\u0004\b\f\u0010\u0006R\u0018\u0010\u000e\u001a\u00020\u00048\u0006X\u0004¢\u0006\n\n\u0002\b\u0010\u001a\u0004\b\u000f\u0010\u0006R\u0018\u0010\u0011\u001a\u00020\u00048\u0006X\u0004¢\u0006\n\n\u0002\b\u0013\u001a\u0004\b\u0012\u0010\u0006¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/RotationDialView$Dimensions;", "", "()V", "angleIndicatorWidth", "", "getAngleIndicatorWidth", "()I", "angleIndicatorWidth$1", "majorAngleIndicatorHeight", "getMajorAngleIndicatorHeight", "majorAngleIndicatorHeight$1", "minorAngleIndicatorHeight", "getMinorAngleIndicatorHeight", "minorAngleIndicatorHeight$1", "spaceBetweenAngleIndicators", "getSpaceBetweenAngleIndicators", "spaceBetweenAngleIndicators$1", "textPaddingBottom", "getTextPaddingBottom", "textPaddingBottom$1", "getHeightForDegree", "degree", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Dimensions {
        public static final Companion Companion = new Companion(null);
        private static final int angleIndicatorWidth = 1;
        private static final int majorAngleIndicatorHeight = 32;
        private static final int minorAngleIndicatorHeight = 12;
        private static final int spaceBetweenAngleIndicators = 12;
        private static final int textPaddingBottom = 8;
        private final int angleIndicatorWidth$1 = ViewUtil.dpToPx(angleIndicatorWidth);
        private final int majorAngleIndicatorHeight$1 = ViewUtil.dpToPx(majorAngleIndicatorHeight);
        private final int minorAngleIndicatorHeight$1 = ViewUtil.dpToPx(minorAngleIndicatorHeight);
        private final int spaceBetweenAngleIndicators$1 = ViewUtil.dpToPx(spaceBetweenAngleIndicators);
        private final int textPaddingBottom$1 = ViewUtil.dpToPx(textPaddingBottom);

        public final int getSpaceBetweenAngleIndicators() {
            return this.spaceBetweenAngleIndicators$1;
        }

        public final int getAngleIndicatorWidth() {
            return this.angleIndicatorWidth$1;
        }

        public final int getMinorAngleIndicatorHeight() {
            return this.minorAngleIndicatorHeight$1;
        }

        public final int getMajorAngleIndicatorHeight() {
            return this.majorAngleIndicatorHeight$1;
        }

        public final int getTextPaddingBottom() {
            return this.textPaddingBottom$1;
        }

        public final int getHeightForDegree(int i) {
            if (i == 0) {
                return this.majorAngleIndicatorHeight$1;
            }
            return this.minorAngleIndicatorHeight$1;
        }

        /* compiled from: RotationDialView.kt */
        @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0002XD¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0002XD¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0002XD¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0002XD¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00048\u0002XD¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/RotationDialView$Dimensions$Companion;", "", "()V", "angleIndicatorWidth", "", "majorAngleIndicatorHeight", "minorAngleIndicatorHeight", "spaceBetweenAngleIndicators", "textPaddingBottom", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }
    }

    /* compiled from: RotationDialView.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u0004J\u000e\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u0004R\u0016\u0010\u0003\u001a\u00020\u00048\u0006XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0007\u001a\u00020\u00048\u0006XD¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0016\u0010\t\u001a\u00020\u00048\u0006XD¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0016\u0010\u000b\u001a\u00020\u00048\u0006XD¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/RotationDialView$Colors;", "", "()V", "majorAngleIndicatorColor", "", "getMajorAngleIndicatorColor", "()I", "minorAngleIndicatorColor", "getMinorAngleIndicatorColor", "modFiveIndicatorColor", "getModFiveIndicatorColor", "textColor", "getTextColor", "colorForCenterDegree", "degree", "colorForOtherDegree", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Colors {
        public static final Colors INSTANCE = new Colors();
        private static final int majorAngleIndicatorColor = -10295174;
        private static final int minorAngleIndicatorColor = -2130706433;
        private static final int modFiveIndicatorColor = -1;
        private static final int textColor = -1;

        private Colors() {
        }

        public final int getTextColor() {
            return textColor;
        }

        public final int getMajorAngleIndicatorColor() {
            return majorAngleIndicatorColor;
        }

        public final int getModFiveIndicatorColor() {
            return modFiveIndicatorColor;
        }

        public final int getMinorAngleIndicatorColor() {
            return minorAngleIndicatorColor;
        }

        public final int colorForCenterDegree(int i) {
            return i == 0 ? modFiveIndicatorColor : majorAngleIndicatorColor;
        }

        public final int colorForOtherDegree(int i) {
            if (i % 5 == 0) {
                return modFiveIndicatorColor;
            }
            return minorAngleIndicatorColor;
        }
    }

    /* compiled from: RotationDialView.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/RotationDialView$Companion;", "", "()V", "MAX_DEGREES", "", "MIN_DEGRESS", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
