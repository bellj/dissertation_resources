package org.thoughtcrime.securesms.scribbles;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.ContextCompat;
import com.airbnb.lottie.SimpleColorFilter;
import com.google.android.material.switchmaterial.SwitchMaterial;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.TooltipPopup;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.v2.MediaAnimations;
import org.thoughtcrime.securesms.scribbles.ImageEditorHudV2;
import org.thoughtcrime.securesms.scribbles.RotationDialView;
import org.thoughtcrime.securesms.util.Debouncer;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: ImageEditorHudV2.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\r\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0002\b'\u0018\u0000 t2\u00020\u0001:\u0003tuvB\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\b\u0010=\u001a\u00020>H\u0002J\u001c\u0010?\u001a\b\u0012\u0004\u0012\u00020\u001b0@2\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002J(\u0010B\u001a\u00020>2\u000e\b\u0002\u0010C\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u000e\b\u0002\u0010D\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002J\b\u0010E\u001a\u00020>H\u0002J\u001c\u0010F\u001a\b\u0012\u0004\u0012\u00020\u001b0@2\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002J(\u0010G\u001a\u00020>2\u000e\b\u0002\u0010C\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u000e\b\u0002\u0010D\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002J\b\u0010H\u001a\u00020>H\u0002J\b\u0010I\u001a\u00020>H\u0002J\b\u0010J\u001a\u00020>H\u0002J\u000e\u0010K\u001a\u00020>2\u0006\u0010L\u001a\u00020\"J\u0006\u0010M\u001a\u00020NJ\u0006\u0010O\u001a\u00020PJ\u0006\u0010Q\u001a\u00020PJ\u0006\u0010R\u001a\u00020\"J\u0006\u0010S\u001a\u00020>J\b\u0010T\u001a\u00020>H\u0002J\b\u0010U\u001a\u00020>H\u0002J\b\u0010V\u001a\u00020>H\u0002J\b\u0010W\u001a\u00020>H\u0002J\b\u0010X\u001a\u00020>H\u0002J\b\u0010Y\u001a\u00020>H\u0002J\b\u0010Z\u001a\u00020>H\u0002J\b\u0010[\u001a\u00020>H\u0002J\b\u0010\\\u001a\u00020>H\u0002J\u000e\u0010]\u001a\u00020>2\u0006\u0010^\u001a\u00020PJ\u000e\u0010_\u001a\u00020>2\u0006\u0010`\u001a\u00020*J\u000e\u0010a\u001a\u00020>2\u0006\u0010b\u001a\u00020PJ\u000e\u0010c\u001a\u00020>2\u0006\u0010d\u001a\u00020PJ\u0014\u0010e\u001a\u00020>2\f\u0010f\u001a\b\u0012\u0004\u0012\u00020P0\bJ\u000e\u0010g\u001a\u00020>2\u0006\u0010h\u001a\u00020NJ\u0010\u0010i\u001a\u00020>2\b\u0010j\u001a\u0004\u0018\u00010,J\u000e\u0010k\u001a\u00020>2\u0006\u0010L\u001a\u00020\"J\u0018\u0010k\u001a\u00020>2\u0006\u0010L\u001a\u00020\"2\u0006\u0010l\u001a\u00020*H\u0002J\u000e\u0010m\u001a\u00020>2\u0006\u00108\u001a\u00020*J\u0006\u0010n\u001a\u00020>J\b\u0010o\u001a\u00020>H\u0003J\u0006\u0010p\u001a\u00020>J\u0006\u0010q\u001a\u00020>J\u000e\u0010r\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002J\b\u0010s\u001a\u00020>H\u0002R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0016X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0016X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010 \u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010%\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020'X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010(\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020*X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010+\u001a\u0004\u0018\u00010,X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010-\u001a\u0004\u0018\u00010.X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u000200X\u0004¢\u0006\u0002\n\u0000R\u0014\u00101\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u00104\u001a\u00020\u0016X\u0004¢\u0006\u0002\n\u0000R\u000e\u00105\u001a\u000206X\u0004¢\u0006\u0002\n\u0000R\u0010\u00107\u001a\u0004\u0018\u00010.X\u000e¢\u0006\u0002\n\u0000R\u000e\u00108\u001a\u00020*X\u000e¢\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010:\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010;\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020'X\u0004¢\u0006\u0002\n\u0000¨\u0006w"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/ImageEditorHudV2;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "allModeTools", "", "Landroid/view/View;", "blurButton", "blurHelpText", "blurToast", "blurToggle", "Lcom/google/android/material/switchmaterial/SwitchMaterial;", "blurToggleContainer", "blurTools", "bottomGuideline", "Landroidx/constraintlayout/widget/Guideline;", "brushPreview", "Lorg/thoughtcrime/securesms/scribbles/BrushWidthPreviewView;", "brushToggle", "Landroid/widget/ImageView;", "cancelButton", "clearAllButton", "colorIndicator", "colorIndicatorAlphaAnimator", "Landroid/animation/Animator;", "cropAspectLockButton", "cropButtonRow", "cropFlipButton", "cropRotateButton", "cropTools", "currentMode", "Lorg/thoughtcrime/securesms/scribbles/ImageEditorHudV2$Mode;", "doneButton", "drawButton", "drawButtonRow", "drawSeekBar", "Landroidx/appcompat/widget/AppCompatSeekBar;", "drawTools", "isAvatarEdit", "", "listener", "Lorg/thoughtcrime/securesms/scribbles/ImageEditorHudV2$EventListener;", "modeAnimatorSet", "Landroid/animation/AnimatorSet;", "rotationDial", "Lorg/thoughtcrime/securesms/scribbles/RotationDialView;", "selectableSet", "stickerButton", "textButton", "textStyleToggle", "toastDebouncer", "Lorg/thoughtcrime/securesms/util/Debouncer;", "undoAnimatorSet", "undoAvailability", "undoButton", "undoTools", "viewsToSlide", "widthSeekBar", "animateInUndoTools", "", "animateInViewSet", "", "viewSet", "animateModeChange", "inSet", "outSet", "animateOutUndoTools", "animateOutViewSet", "animateUndoChange", "animateWidthSeekbarIn", "animateWidthSeekbarOut", "clearSelection", "enterMode", "mode", "getActiveBrushWidth", "", "getActiveColor", "", "getColorIndex", "getMode", "hideBlurToast", "initializeViews", "presentModeBlur", "presentModeCrop", "presentModeDelete", "presentModeDraw", "presentModeHighlight", "presentModeMoveSticker", "presentModeNone", "presentModeText", "setActiveColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "setBlurFacesToggleEnabled", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "setBottomOfImageEditorView", "bottom", "setColorIndex", "index", "setColorPalette", "colors", "setDialRotation", "degrees", "setEventListener", "eventListener", "setMode", "notify", "setUndoAvailability", "setUpForAvatarEditing", "setupWidthSeekBar", "showBlurHudTooltip", "showBlurToast", "undoToolsIfAvailable", "updateColorIndicator", "Companion", "EventListener", "Mode", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ImageEditorHudV2 extends FrameLayout {
    private static final long ANIMATION_DURATION;
    public static final Companion Companion = new Companion(null);
    private static final Map<Mode, Pair<Float, Float>> DRAW_WIDTH_BOUNDARIES = MapsKt__MapsKt.mapOf(TuplesKt.to(Mode.DRAW, SignalStore.imageEditorValues().getMarkerWidthRange()), TuplesKt.to(Mode.HIGHLIGHT, SignalStore.imageEditorValues().getHighlighterWidthRange()), TuplesKt.to(Mode.BLUR, SignalStore.imageEditorValues().getBlurWidthRange()));
    private final Set<View> allModeTools;
    private final View blurButton;
    private final View blurHelpText;
    private final View blurToast;
    private final SwitchMaterial blurToggle;
    private final View blurToggleContainer;
    private final Set<View> blurTools;
    private final Guideline bottomGuideline;
    private final BrushWidthPreviewView brushPreview;
    private final ImageView brushToggle;
    private final View cancelButton;
    private final View clearAllButton;
    private final ImageView colorIndicator;
    private Animator colorIndicatorAlphaAnimator;
    private final ImageView cropAspectLockButton;
    private final Set<View> cropButtonRow;
    private final View cropFlipButton;
    private final View cropRotateButton;
    private final Set<View> cropTools;
    private Mode currentMode;
    private final View doneButton;
    private final View drawButton;
    private final Set<View> drawButtonRow;
    private final AppCompatSeekBar drawSeekBar;
    private final Set<View> drawTools;
    private boolean isAvatarEdit;
    private EventListener listener;
    private AnimatorSet modeAnimatorSet;
    private final RotationDialView rotationDial;
    private final Set<View> selectableSet;
    private final View stickerButton;
    private final View textButton;
    private final ImageView textStyleToggle;
    private final Debouncer toastDebouncer;
    private AnimatorSet undoAnimatorSet;
    private boolean undoAvailability;
    private final View undoButton;
    private final Set<View> undoTools;
    private final Set<View> viewsToSlide;
    private final AppCompatSeekBar widthSeekBar;

    /* compiled from: ImageEditorHudV2.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0003H&J\b\u0010\b\u001a\u00020\u0006H&J\b\u0010\t\u001a\u00020\u0006H&J\b\u0010\n\u001a\u00020\u0006H&J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\rH&J\b\u0010\u000e\u001a\u00020\u0006H&J\b\u0010\u000f\u001a\u00020\u0006H&J\u0010\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u0012H&J\b\u0010\u0013\u001a\u00020\u0006H&J\b\u0010\u0014\u001a\u00020\u0006H&J\b\u0010\u0015\u001a\u00020\u0006H&J\b\u0010\u0016\u001a\u00020\u0006H&J\u0018\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0019H&J\u0018\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0003H&J\b\u0010\u001e\u001a\u00020\u0006H&J\b\u0010\u001f\u001a\u00020\u0006H&J\b\u0010 \u001a\u00020\u0006H&J\b\u0010!\u001a\u00020\u0006H&R\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0002\u0010\u0004¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/ImageEditorHudV2$EventListener;", "", "isCropAspectLocked", "", "()Z", "onBlurFacesToggled", "", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "onBrushWidthChange", "onCancel", "onClearAll", "onColorChange", NotificationProfileDatabase.NotificationProfileTable.COLOR, "", "onCropAspectLock", "onDelete", "onDialRotationChanged", "degrees", "", "onDialRotationGestureFinished", "onDialRotationGestureStarted", "onDone", "onFlipHorizontal", "onModeStarted", "mode", "Lorg/thoughtcrime/securesms/scribbles/ImageEditorHudV2$Mode;", "previousMode", "onRequestFullScreen", "fullScreen", "hideKeyboard", "onRotate90AntiClockwise", "onSave", "onTextStyleToggle", "onUndo", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface EventListener {
        boolean isCropAspectLocked();

        void onBlurFacesToggled(boolean z);

        void onBrushWidthChange();

        void onCancel();

        void onClearAll();

        void onColorChange(int i);

        void onCropAspectLock();

        void onDelete();

        void onDialRotationChanged(float f);

        void onDialRotationGestureFinished();

        void onDialRotationGestureStarted();

        void onDone();

        void onFlipHorizontal();

        void onModeStarted(Mode mode, Mode mode2);

        void onRequestFullScreen(boolean z, boolean z2);

        void onRotate90AntiClockwise();

        void onSave();

        void onTextStyleToggle();

        void onUndo();
    }

    /* compiled from: ImageEditorHudV2.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\f\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\f¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/ImageEditorHudV2$Mode;", "", "(Ljava/lang/String;I)V", "NONE", "CROP", "TEXT", "DRAW", "HIGHLIGHT", "BLUR", "MOVE_STICKER", "MOVE_TEXT", "DELETE", "INSERT_STICKER", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Mode {
        NONE,
        CROP,
        TEXT,
        DRAW,
        HIGHLIGHT,
        BLUR,
        MOVE_STICKER,
        MOVE_TEXT,
        DELETE,
        INSERT_STICKER
    }

    /* compiled from: ImageEditorHudV2.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[Mode.values().length];
            iArr[Mode.NONE.ordinal()] = 1;
            iArr[Mode.CROP.ordinal()] = 2;
            iArr[Mode.TEXT.ordinal()] = 3;
            iArr[Mode.DRAW.ordinal()] = 4;
            iArr[Mode.BLUR.ordinal()] = 5;
            iArr[Mode.HIGHLIGHT.ordinal()] = 6;
            iArr[Mode.INSERT_STICKER.ordinal()] = 7;
            iArr[Mode.MOVE_STICKER.ordinal()] = 8;
            iArr[Mode.DELETE.ordinal()] = 9;
            iArr[Mode.MOVE_TEXT.ordinal()] = 10;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ImageEditorHudV2(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public final void setColorPalette(Set<Integer> set) {
        Intrinsics.checkNotNullParameter(set, "colors");
    }

    public /* synthetic */ ImageEditorHudV2(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ImageEditorHudV2(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        this.currentMode = Mode.NONE;
        FrameLayout.inflate(context, R.layout.v2_media_image_editor_hud, this);
        View findViewById = findViewById(R.id.image_editor_hud_undo);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.image_editor_hud_undo)");
        this.undoButton = findViewById;
        View findViewById2 = findViewById(R.id.image_editor_hud_clear_all);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.image_editor_hud_clear_all)");
        this.clearAllButton = findViewById2;
        View findViewById3 = findViewById(R.id.image_editor_hud_cancel_button);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.image_editor_hud_cancel_button)");
        this.cancelButton = findViewById3;
        View findViewById4 = findViewById(R.id.image_editor_hud_draw_button);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.image_editor_hud_draw_button)");
        this.drawButton = findViewById4;
        View findViewById5 = findViewById(R.id.image_editor_hud_text_button);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "findViewById(R.id.image_editor_hud_text_button)");
        this.textButton = findViewById5;
        View findViewById6 = findViewById(R.id.image_editor_hud_sticker_button);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "findViewById(R.id.image_editor_hud_sticker_button)");
        this.stickerButton = findViewById6;
        View findViewById7 = findViewById(R.id.image_editor_hud_blur_button);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "findViewById(R.id.image_editor_hud_blur_button)");
        this.blurButton = findViewById7;
        View findViewById8 = findViewById(R.id.image_editor_hud_done_button);
        Intrinsics.checkNotNullExpressionValue(findViewById8, "findViewById(R.id.image_editor_hud_done_button)");
        this.doneButton = findViewById8;
        View findViewById9 = findViewById(R.id.image_editor_hud_draw_color_bar);
        Intrinsics.checkNotNullExpressionValue(findViewById9, "findViewById(R.id.image_editor_hud_draw_color_bar)");
        AppCompatSeekBar appCompatSeekBar = (AppCompatSeekBar) findViewById9;
        this.drawSeekBar = appCompatSeekBar;
        View findViewById10 = findViewById(R.id.image_editor_hud_draw_brush);
        Intrinsics.checkNotNullExpressionValue(findViewById10, "findViewById(R.id.image_editor_hud_draw_brush)");
        ImageView imageView = (ImageView) findViewById10;
        this.brushToggle = imageView;
        View findViewById11 = findViewById(R.id.image_editor_hud_draw_width_bar);
        Intrinsics.checkNotNullExpressionValue(findViewById11, "findViewById(R.id.image_editor_hud_draw_width_bar)");
        AppCompatSeekBar appCompatSeekBar2 = (AppCompatSeekBar) findViewById11;
        this.widthSeekBar = appCompatSeekBar2;
        View findViewById12 = findViewById(R.id.image_editor_hud_rotate_button);
        Intrinsics.checkNotNullExpressionValue(findViewById12, "findViewById(R.id.image_editor_hud_rotate_button)");
        this.cropRotateButton = findViewById12;
        View findViewById13 = findViewById(R.id.image_editor_hud_flip_button);
        Intrinsics.checkNotNullExpressionValue(findViewById13, "findViewById(R.id.image_editor_hud_flip_button)");
        this.cropFlipButton = findViewById13;
        View findViewById14 = findViewById(R.id.image_editor_hud_aspect_lock_button);
        Intrinsics.checkNotNullExpressionValue(findViewById14, "findViewById(R.id.image_…r_hud_aspect_lock_button)");
        ImageView imageView2 = (ImageView) findViewById14;
        this.cropAspectLockButton = imageView2;
        View findViewById15 = findViewById(R.id.image_editor_hud_blur_toggle_container);
        Intrinsics.checkNotNullExpressionValue(findViewById15, "findViewById(R.id.image_…ud_blur_toggle_container)");
        this.blurToggleContainer = findViewById15;
        View findViewById16 = findViewById(R.id.image_editor_hud_blur_toggle);
        Intrinsics.checkNotNullExpressionValue(findViewById16, "findViewById(R.id.image_editor_hud_blur_toggle)");
        this.blurToggle = (SwitchMaterial) findViewById16;
        View findViewById17 = findViewById(R.id.image_editor_hud_blur_toast);
        Intrinsics.checkNotNullExpressionValue(findViewById17, "findViewById(R.id.image_editor_hud_blur_toast)");
        this.blurToast = findViewById17;
        View findViewById18 = findViewById(R.id.image_editor_hud_blur_help_text);
        Intrinsics.checkNotNullExpressionValue(findViewById18, "findViewById(R.id.image_editor_hud_blur_help_text)");
        this.blurHelpText = findViewById18;
        View findViewById19 = findViewById(R.id.image_editor_hud_color_indicator);
        Intrinsics.checkNotNullExpressionValue(findViewById19, "findViewById(R.id.image_…itor_hud_color_indicator)");
        this.colorIndicator = (ImageView) findViewById19;
        View findViewById20 = findViewById(R.id.image_editor_bottom_guide);
        Intrinsics.checkNotNullExpressionValue(findViewById20, "findViewById(R.id.image_editor_bottom_guide)");
        this.bottomGuideline = (Guideline) findViewById20;
        View findViewById21 = findViewById(R.id.image_editor_hud_brush_preview);
        Intrinsics.checkNotNullExpressionValue(findViewById21, "findViewById(R.id.image_editor_hud_brush_preview)");
        this.brushPreview = (BrushWidthPreviewView) findViewById21;
        View findViewById22 = findViewById(R.id.image_editor_hud_text_style_button);
        Intrinsics.checkNotNullExpressionValue(findViewById22, "findViewById(R.id.image_…or_hud_text_style_button)");
        ImageView imageView3 = (ImageView) findViewById22;
        this.textStyleToggle = imageView3;
        View findViewById23 = findViewById(R.id.image_editor_hud_crop_rotation_dial);
        Intrinsics.checkNotNullExpressionValue(findViewById23, "findViewById(R.id.image_…r_hud_crop_rotation_dial)");
        RotationDialView rotationDialView = (RotationDialView) findViewById23;
        this.rotationDial = rotationDialView;
        this.selectableSet = SetsKt__SetsKt.setOf((Object[]) new View[]{findViewById4, findViewById5, findViewById6, findViewById7});
        this.undoTools = SetsKt__SetsKt.setOf((Object[]) new View[]{findViewById, findViewById2});
        Set<View> set = SetsKt__SetsKt.setOf((Object[]) new View[]{imageView, appCompatSeekBar, appCompatSeekBar2});
        this.drawTools = set;
        Set<View> set2 = SetsKt__SetsKt.setOf((Object[]) new View[]{findViewById15, findViewById18, appCompatSeekBar2});
        this.blurTools = set2;
        Set<View> set3 = SetsKt__SetsJVMKt.setOf(rotationDialView);
        this.cropTools = set3;
        Set<View> set4 = SetsKt__SetsKt.setOf((Object[]) new View[]{findViewById3, findViewById8, findViewById4, findViewById5, findViewById6, findViewById7});
        this.drawButtonRow = set4;
        Set<View> set5 = SetsKt__SetsKt.setOf((Object[]) new View[]{findViewById3, findViewById8, findViewById12, findViewById13, imageView2});
        this.cropButtonRow = set5;
        this.allModeTools = SetsKt___SetsKt.plus(SetsKt___SetsKt.plus(SetsKt___SetsKt.plus(SetsKt___SetsKt.plus(SetsKt___SetsKt.plus((Set) set, (Iterable) set2), (Iterable) set4), (Iterable) set5), imageView3), (Iterable) set3);
        this.viewsToSlide = SetsKt___SetsKt.plus((Set) set4, (Iterable) set5);
        this.toastDebouncer = new Debouncer(3000);
        initializeViews();
        setMode(this.currentMode);
    }

    private final void initializeViews() {
        this.colorIndicator.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.ic_color_preview));
        this.undoButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2666initializeViews$lambda0(ImageEditorHudV2.this, view);
            }
        });
        this.clearAllButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2667initializeViews$lambda1(ImageEditorHudV2.this, view);
            }
        });
        this.cancelButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2672initializeViews$lambda2(ImageEditorHudV2.this, view);
            }
        });
        this.textStyleToggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2673initializeViews$lambda3(ImageEditorHudV2.this, view);
            }
        });
        this.drawButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2674initializeViews$lambda4(ImageEditorHudV2.this, view);
            }
        });
        this.blurButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2675initializeViews$lambda5(ImageEditorHudV2.this, view);
            }
        });
        this.textButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2676initializeViews$lambda6(ImageEditorHudV2.this, view);
            }
        });
        this.stickerButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2677initializeViews$lambda7(ImageEditorHudV2.this, view);
            }
        });
        this.brushToggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda12
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2678initializeViews$lambda8(ImageEditorHudV2.this, view);
            }
        });
        this.doneButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda13
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2679initializeViews$lambda9(ImageEditorHudV2.this, view);
            }
        });
        HSVColorSlider.INSTANCE.setUpForColor(this.drawSeekBar, -1, new Function1<Integer, Unit>(this) { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$initializeViews$11
            final /* synthetic */ ImageEditorHudV2 this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                invoke(num.intValue());
                return Unit.INSTANCE;
            }

            public final void invoke(int i) {
                this.this$0.updateColorIndicator();
                ImageEditorHudV2.EventListener eventListener = this.this$0.listener;
                if (eventListener != null) {
                    eventListener.onColorChange(this.this$0.getActiveColor());
                }
            }
        }, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$initializeViews$12
            final /* synthetic */ ImageEditorHudV2 this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                Animator animator = this.this$0.colorIndicatorAlphaAnimator;
                if (animator != null) {
                    animator.end();
                }
                ImageEditorHudV2 imageEditorHudV2 = this.this$0;
                imageEditorHudV2.colorIndicatorAlphaAnimator = ObjectAnimator.ofFloat(imageEditorHudV2.colorIndicator, "alpha", this.this$0.colorIndicator.getAlpha(), 1.0f);
                Animator animator2 = this.this$0.colorIndicatorAlphaAnimator;
                if (animator2 != null) {
                    animator2.setDuration(150);
                }
                Animator animator3 = this.this$0.colorIndicatorAlphaAnimator;
                if (animator3 != null) {
                    animator3.start();
                }
            }
        }, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$initializeViews$13
            final /* synthetic */ ImageEditorHudV2 this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                Animator animator = this.this$0.colorIndicatorAlphaAnimator;
                if (animator != null) {
                    animator.end();
                }
                ImageEditorHudV2 imageEditorHudV2 = this.this$0;
                imageEditorHudV2.colorIndicatorAlphaAnimator = ObjectAnimator.ofFloat(imageEditorHudV2.colorIndicator, "alpha", this.this$0.colorIndicator.getAlpha(), 0.0f);
                Animator animator2 = this.this$0.colorIndicatorAlphaAnimator;
                if (animator2 != null) {
                    animator2.setDuration(150);
                }
                Animator animator3 = this.this$0.colorIndicatorAlphaAnimator;
                if (animator3 != null) {
                    animator3.start();
                }
            }
        });
        this.cropFlipButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2668initializeViews$lambda10(ImageEditorHudV2.this, view);
            }
        });
        this.cropRotateButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2669initializeViews$lambda11(ImageEditorHudV2.this, view);
            }
        });
        this.cropAspectLockButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ImageEditorHudV2.m2670initializeViews$lambda12(ImageEditorHudV2.this, view);
            }
        });
        this.blurToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda4
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                ImageEditorHudV2.m2671initializeViews$lambda13(ImageEditorHudV2.this, compoundButton, z);
            }
        });
        setupWidthSeekBar();
        this.rotationDial.setListener(new RotationDialView.Listener(this) { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$initializeViews$18
            final /* synthetic */ ImageEditorHudV2 this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.scribbles.RotationDialView.Listener
            public void onDegreeChanged(float f) {
                ImageEditorHudV2.EventListener eventListener = this.this$0.listener;
                if (eventListener != null) {
                    eventListener.onDialRotationChanged(f);
                }
            }

            @Override // org.thoughtcrime.securesms.scribbles.RotationDialView.Listener
            public void onGestureStart() {
                ImageEditorHudV2.EventListener eventListener = this.this$0.listener;
                if (eventListener != null) {
                    eventListener.onDialRotationGestureStarted();
                }
            }

            @Override // org.thoughtcrime.securesms.scribbles.RotationDialView.Listener
            public void onGestureEnd() {
                ImageEditorHudV2.EventListener eventListener = this.this$0.listener;
                if (eventListener != null) {
                    eventListener.onDialRotationGestureFinished();
                }
            }
        });
    }

    /* renamed from: initializeViews$lambda-0 */
    public static final void m2666initializeViews$lambda0(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        EventListener eventListener = imageEditorHudV2.listener;
        if (eventListener != null) {
            eventListener.onUndo();
        }
    }

    /* renamed from: initializeViews$lambda-1 */
    public static final void m2667initializeViews$lambda1(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        EventListener eventListener = imageEditorHudV2.listener;
        if (eventListener != null) {
            eventListener.onClearAll();
        }
    }

    /* renamed from: initializeViews$lambda-2 */
    public static final void m2672initializeViews$lambda2(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        EventListener eventListener = imageEditorHudV2.listener;
        if (eventListener != null) {
            eventListener.onCancel();
        }
    }

    /* renamed from: initializeViews$lambda-3 */
    public static final void m2673initializeViews$lambda3(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        EventListener eventListener = imageEditorHudV2.listener;
        if (eventListener != null) {
            eventListener.onTextStyleToggle();
        }
    }

    /* renamed from: initializeViews$lambda-4 */
    public static final void m2674initializeViews$lambda4(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        imageEditorHudV2.setMode(Mode.DRAW);
    }

    /* renamed from: initializeViews$lambda-5 */
    public static final void m2675initializeViews$lambda5(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        imageEditorHudV2.setMode(Mode.BLUR);
    }

    /* renamed from: initializeViews$lambda-6 */
    public static final void m2676initializeViews$lambda6(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        imageEditorHudV2.setMode(Mode.TEXT);
    }

    /* renamed from: initializeViews$lambda-7 */
    public static final void m2677initializeViews$lambda7(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        imageEditorHudV2.setMode(Mode.INSERT_STICKER);
    }

    /* renamed from: initializeViews$lambda-8 */
    public static final void m2678initializeViews$lambda8(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        Mode mode = imageEditorHudV2.currentMode;
        Mode mode2 = Mode.DRAW;
        if (mode == mode2) {
            imageEditorHudV2.setMode(Mode.HIGHLIGHT);
        } else {
            imageEditorHudV2.setMode(mode2);
        }
    }

    /* renamed from: initializeViews$lambda-9 */
    public static final void m2679initializeViews$lambda9(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        if (!imageEditorHudV2.isAvatarEdit || imageEditorHudV2.currentMode != Mode.CROP) {
            EventListener eventListener = imageEditorHudV2.listener;
            if (eventListener != null) {
                eventListener.onDone();
                return;
            }
            return;
        }
        imageEditorHudV2.setMode(Mode.DRAW);
    }

    /* renamed from: initializeViews$lambda-10 */
    public static final void m2668initializeViews$lambda10(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        EventListener eventListener = imageEditorHudV2.listener;
        if (eventListener != null) {
            eventListener.onFlipHorizontal();
        }
    }

    /* renamed from: initializeViews$lambda-11 */
    public static final void m2669initializeViews$lambda11(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        EventListener eventListener = imageEditorHudV2.listener;
        if (eventListener != null) {
            eventListener.onRotate90AntiClockwise();
        }
    }

    /* renamed from: initializeViews$lambda-12 */
    public static final void m2670initializeViews$lambda12(ImageEditorHudV2 imageEditorHudV2, View view) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        EventListener eventListener = imageEditorHudV2.listener;
        if (eventListener != null) {
            eventListener.onCropAspectLock();
        }
        EventListener eventListener2 = imageEditorHudV2.listener;
        boolean z = true;
        if (eventListener2 == null || !eventListener2.isCropAspectLocked()) {
            z = false;
        }
        if (z) {
            imageEditorHudV2.cropAspectLockButton.setImageResource(R.drawable.ic_crop_lock_24);
        } else {
            imageEditorHudV2.cropAspectLockButton.setImageResource(R.drawable.ic_crop_unlock_24);
        }
    }

    /* renamed from: initializeViews$lambda-13 */
    public static final void m2671initializeViews$lambda13(ImageEditorHudV2 imageEditorHudV2, CompoundButton compoundButton, boolean z) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        EventListener eventListener = imageEditorHudV2.listener;
        if (eventListener != null) {
            eventListener.onBlurFacesToggled(z);
        }
    }

    public final void setDialRotation(float f) {
        this.rotationDial.setDegrees(f);
    }

    public final void setBottomOfImageEditorView(int i) {
        this.bottomGuideline.setGuidelineEnd(i);
    }

    private final void setupWidthSeekBar() {
        this.widthSeekBar.setProgressDrawable(AppCompatResources.getDrawable(getContext(), R.drawable.ic_width_slider_bg));
        this.widthSeekBar.setThumb(HSVColorSlider.INSTANCE.createThumbDrawable(-1));
        this.widthSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(this) { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$setupWidthSeekBar$1
            final /* synthetic */ ImageEditorHudV2 this$0;

            /* compiled from: ImageEditorHudV2.kt */
            @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
            /* loaded from: classes4.dex */
            public /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    int[] iArr = new int[ImageEditorHudV2.Mode.values().length];
                    iArr[ImageEditorHudV2.Mode.DRAW.ordinal()] = 1;
                    iArr[ImageEditorHudV2.Mode.BLUR.ordinal()] = 2;
                    iArr[ImageEditorHudV2.Mode.HIGHLIGHT.ordinal()] = 3;
                    $EnumSwitchMapping$0 = iArr;
                }
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                ImageEditorHudV2.EventListener eventListener = this.this$0.listener;
                if (eventListener != null) {
                    eventListener.onBrushWidthChange();
                }
                this.this$0.brushPreview.setThickness(this.this$0.getActiveBrushWidth());
                int i2 = WhenMappings.$EnumSwitchMapping$0[this.this$0.currentMode.ordinal()];
                if (i2 == 1) {
                    SignalStore.imageEditorValues().setMarkerPercentage(i);
                } else if (i2 == 2) {
                    SignalStore.imageEditorValues().setBlurPercentage(i);
                } else if (i2 == 3) {
                    SignalStore.imageEditorValues().setHighlighterPercentage(i);
                }
            }
        });
        this.widthSeekBar.setOnTouchListener(new View.OnTouchListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda16
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return ImageEditorHudV2.m2681setupWidthSeekBar$lambda14(ImageEditorHudV2.this, view, motionEvent);
            }
        });
    }

    /* renamed from: setupWidthSeekBar$lambda-14 */
    public static final boolean m2681setupWidthSeekBar$lambda14(ImageEditorHudV2 imageEditorHudV2, View view, MotionEvent motionEvent) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        if (motionEvent.getAction() == 0) {
            imageEditorHudV2.animateWidthSeekbarIn();
            imageEditorHudV2.brushPreview.show();
        } else if (motionEvent.getAction() == 1 || motionEvent.getAction() == 3) {
            imageEditorHudV2.animateWidthSeekbarOut();
            imageEditorHudV2.brushPreview.hide();
        }
        return view.onTouchEvent(motionEvent);
    }

    private final void animateWidthSeekbarIn() {
        ViewPropertyAnimator duration;
        ViewPropertyAnimator interpolator;
        ViewPropertyAnimator animate = this.widthSeekBar.animate();
        if (animate != null && (duration = animate.setDuration(ANIMATION_DURATION)) != null && (interpolator = duration.setInterpolator(MediaAnimations.getInterpolator())) != null) {
            interpolator.translationX((float) ViewUtil.dpToPx(36));
        }
    }

    private final void animateWidthSeekbarOut() {
        ViewPropertyAnimator duration;
        ViewPropertyAnimator interpolator;
        ViewPropertyAnimator animate = this.widthSeekBar.animate();
        if (animate != null && (duration = animate.setDuration(ANIMATION_DURATION)) != null && (interpolator = duration.setInterpolator(MediaAnimations.getInterpolator())) != null) {
            interpolator.translationX(0.0f);
        }
    }

    public final void setUpForAvatarEditing() {
        this.isAvatarEdit = true;
    }

    public final int getActiveColor() {
        if (this.currentMode == Mode.HIGHLIGHT) {
            return Companion.withHighlighterAlpha(HSVColorSlider.INSTANCE.getColor(this.drawSeekBar));
        }
        return HSVColorSlider.INSTANCE.getColor(this.drawSeekBar);
    }

    public final int getColorIndex() {
        return this.drawSeekBar.getProgress();
    }

    public final void setColorIndex(int i) {
        this.drawSeekBar.setProgress(i);
    }

    public final void setActiveColor(int i) {
        HSVColorSlider.INSTANCE.setColor(this.drawSeekBar, i | -16777216);
        updateColorIndicator();
    }

    public final float getActiveBrushWidth() {
        Pair<Float, Float> pair = DRAW_WIDTH_BOUNDARIES.get(this.currentMode);
        if (pair != null) {
            float floatValue = pair.component1().floatValue();
            return floatValue + ((pair.component2().floatValue() - floatValue) * (((float) this.widthSeekBar.getProgress()) / 100.0f));
        }
        throw new IllegalStateException("Cannot get width in mode " + this.currentMode);
    }

    public final void setBlurFacesToggleEnabled(boolean z) {
        this.blurToggle.setOnCheckedChangeListener(null);
        this.blurToggle.setChecked(z);
        this.blurToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda15
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
                ImageEditorHudV2.m2680setBlurFacesToggleEnabled$lambda15(ImageEditorHudV2.this, compoundButton, z2);
            }
        });
    }

    /* renamed from: setBlurFacesToggleEnabled$lambda-15 */
    public static final void m2680setBlurFacesToggleEnabled$lambda15(ImageEditorHudV2 imageEditorHudV2, CompoundButton compoundButton, boolean z) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        EventListener eventListener = imageEditorHudV2.listener;
        if (eventListener != null) {
            eventListener.onBlurFacesToggled(z);
        }
    }

    public final void showBlurHudTooltip() {
        TooltipPopup.forTarget(this.blurButton).setText(R.string.ImageEditorHud_new_blur_faces_or_draw_anywhere_to_blur).setBackgroundTint(ContextCompat.getColor(getContext(), R.color.core_ultramarine)).setTextColor(ContextCompat.getColor(getContext(), R.color.core_white)).show(1);
    }

    public final void showBlurToast() {
        this.blurToast.clearAnimation();
        ViewExtensionsKt.setVisible(this.blurToast, true);
        this.toastDebouncer.publish(new Runnable() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$$ExternalSyntheticLambda14
            @Override // java.lang.Runnable
            public final void run() {
                ImageEditorHudV2.m2682showBlurToast$lambda16(ImageEditorHudV2.this);
            }
        });
    }

    /* renamed from: showBlurToast$lambda-16 */
    public static final void m2682showBlurToast$lambda16(ImageEditorHudV2 imageEditorHudV2) {
        Intrinsics.checkNotNullParameter(imageEditorHudV2, "this$0");
        ViewExtensionsKt.setVisible(imageEditorHudV2.blurToast, false);
    }

    public final void hideBlurToast() {
        this.blurToast.clearAnimation();
        ViewExtensionsKt.setVisible(this.blurToast, false);
        this.toastDebouncer.clear();
    }

    public final void setEventListener(EventListener eventListener) {
        this.listener = eventListener;
    }

    public final void enterMode(Mode mode) {
        Intrinsics.checkNotNullParameter(mode, "mode");
        setMode(mode, false);
    }

    public final void setMode(Mode mode) {
        Intrinsics.checkNotNullParameter(mode, "mode");
        setMode(mode, true);
    }

    public final Mode getMode() {
        return this.currentMode;
    }

    public final void setUndoAvailability(boolean z) {
        this.undoAvailability = z;
        Mode mode = this.currentMode;
        if (mode != Mode.NONE && mode != Mode.DELETE) {
            if (z) {
                animateInUndoTools();
            } else {
                animateOutUndoTools();
            }
        }
    }

    private final void setMode(Mode mode, boolean z) {
        EventListener eventListener;
        Mode mode2 = this.currentMode;
        this.currentMode = mode;
        clearSelection();
        AnimatorSet animatorSet = this.modeAnimatorSet;
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        AnimatorSet animatorSet2 = this.undoAnimatorSet;
        if (animatorSet2 != null) {
            animatorSet2.cancel();
        }
        switch (WhenMappings.$EnumSwitchMapping$0[mode.ordinal()]) {
            case 1:
                presentModeNone();
                break;
            case 2:
                presentModeCrop();
                break;
            case 3:
                presentModeText();
                break;
            case 4:
                presentModeDraw();
                break;
            case 5:
                presentModeBlur();
                break;
            case 6:
                presentModeHighlight();
                break;
            case 7:
                presentModeMoveSticker();
                break;
            case 8:
                presentModeMoveSticker();
                break;
            case 9:
                presentModeDelete();
                break;
            case 10:
                presentModeText();
                break;
        }
        if (z && (eventListener = this.listener) != null) {
            eventListener.onModeStarted(mode, mode2);
        }
        EventListener eventListener2 = this.listener;
        if (eventListener2 != null) {
            boolean z2 = true;
            boolean z3 = mode != Mode.NONE;
            if (mode == Mode.TEXT) {
                z2 = false;
            }
            eventListener2.onRequestFullScreen(z3, z2);
        }
    }

    private final void presentModeNone() {
        animateModeChange(SetsKt__SetsKt.emptySet(), this.allModeTools);
        animateOutUndoTools();
    }

    private final void presentModeCrop() {
        animateModeChange(SetsKt___SetsKt.minus(SetsKt___SetsKt.plus((Set) this.cropTools, (Iterable) this.cropButtonRow), (Iterable) (this.isAvatarEdit ? SetsKt__SetsJVMKt.setOf(this.cropAspectLockButton) : SetsKt__SetsKt.emptySet())), this.allModeTools);
        animateInUndoTools();
    }

    private final void presentModeDraw() {
        this.drawButton.setSelected(true);
        this.brushToggle.setImageResource(R.drawable.ic_draw_white_24);
        this.widthSeekBar.setProgress(SignalStore.imageEditorValues().getMarkerPercentage());
        EventListener eventListener = this.listener;
        if (eventListener != null) {
            eventListener.onColorChange(getActiveColor());
        }
        updateColorIndicator();
        animateModeChange(SetsKt___SetsKt.plus((Set) this.drawButtonRow, (Iterable) this.drawTools), this.allModeTools);
        animateInUndoTools();
    }

    private final void presentModeHighlight() {
        this.drawButton.setSelected(true);
        this.brushToggle.setImageResource(R.drawable.ic_marker_24);
        this.widthSeekBar.setProgress(SignalStore.imageEditorValues().getHighlighterPercentage());
        EventListener eventListener = this.listener;
        if (eventListener != null) {
            eventListener.onColorChange(getActiveColor());
        }
        updateColorIndicator();
        animateModeChange(SetsKt___SetsKt.plus((Set) this.drawButtonRow, (Iterable) this.drawTools), this.allModeTools);
        animateInUndoTools();
    }

    private final void presentModeBlur() {
        this.blurButton.setSelected(true);
        this.widthSeekBar.setProgress(SignalStore.imageEditorValues().getBlurPercentage());
        EventListener eventListener = this.listener;
        if (eventListener != null) {
            eventListener.onColorChange(getActiveColor());
        }
        updateColorIndicator();
        animateModeChange(SetsKt___SetsKt.plus((Set) this.drawButtonRow, (Iterable) this.blurTools), this.allModeTools);
        animateInUndoTools();
    }

    private final void presentModeText() {
        animateModeChange(SetsKt___SetsKt.plus((Set) this.drawButtonRow, (Iterable) SetsKt__SetsKt.setOf((Object[]) new View[]{this.drawSeekBar, this.textStyleToggle})), this.allModeTools);
        animateInUndoTools();
    }

    private final void presentModeMoveSticker() {
        animateModeChange(this.drawButtonRow, this.allModeTools);
        animateInUndoTools();
    }

    private final void presentModeDelete() {
        animateModeChange$default(this, null, this.allModeTools, 1, null);
        animateOutUndoTools();
    }

    private final void clearSelection() {
        for (View view : this.selectableSet) {
            view.setSelected(false);
        }
    }

    public final void updateColorIndicator() {
        Drawable drawable = this.colorIndicator.getDrawable();
        HSVColorSlider hSVColorSlider = HSVColorSlider.INSTANCE;
        drawable.setColorFilter(new SimpleColorFilter(hSVColorSlider.getColor(this.drawSeekBar)));
        this.colorIndicator.setTranslationX(((float) this.drawSeekBar.getThumb().getBounds().left) + ((float) ViewUtil.dpToPx(16)));
        this.brushPreview.setColor(hSVColorSlider.getColor(this.drawSeekBar));
        this.brushPreview.setBlur(this.currentMode == Mode.BLUR);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2 */
    /* JADX WARN: Multi-variable type inference failed */
    static /* synthetic */ void animateModeChange$default(ImageEditorHudV2 imageEditorHudV2, Set set, Set set2, int i, Object obj) {
        if ((i & 1) != 0) {
            set = SetsKt__SetsKt.emptySet();
        }
        if ((i & 2) != 0) {
            set2 = SetsKt__SetsKt.emptySet();
        }
        imageEditorHudV2.animateModeChange(set, set2);
    }

    private final void animateModeChange(Set<? extends View> set, Set<? extends View> set2) {
        List list = CollectionsKt___CollectionsKt.plus((Collection) animateInViewSet(set), (Iterable) animateOutViewSet(SetsKt___SetsKt.minus((Set) set2, (Iterable) set)));
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(list);
        animatorSet.setDuration(ANIMATION_DURATION);
        animatorSet.setInterpolator(MediaAnimations.getInterpolator());
        animatorSet.start();
        this.modeAnimatorSet = animatorSet;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2 */
    /* JADX WARN: Multi-variable type inference failed */
    static /* synthetic */ void animateUndoChange$default(ImageEditorHudV2 imageEditorHudV2, Set set, Set set2, int i, Object obj) {
        if ((i & 1) != 0) {
            set = SetsKt__SetsKt.emptySet();
        }
        if ((i & 2) != 0) {
            set2 = SetsKt__SetsKt.emptySet();
        }
        imageEditorHudV2.animateUndoChange(set, set2);
    }

    private final void animateUndoChange(Set<? extends View> set, Set<? extends View> set2) {
        List list = CollectionsKt___CollectionsKt.plus((Collection) animateInViewSet(set), (Iterable) animateOutViewSet(SetsKt___SetsKt.minus((Set) set2, (Iterable) set)));
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(list);
        animatorSet.setDuration(ANIMATION_DURATION);
        animatorSet.setInterpolator(MediaAnimations.getInterpolator());
        animatorSet.start();
        this.undoAnimatorSet = animatorSet;
    }

    private final void animateInUndoTools() {
        animateUndoChange$default(this, undoToolsIfAvailable(), null, 2, null);
    }

    private final void animateOutUndoTools() {
        animateUndoChange$default(this, null, this.undoTools, 1, null);
    }

    private final Set<View> undoToolsIfAvailable() {
        if (this.undoAvailability) {
            return this.undoTools;
        }
        return SetsKt__SetsKt.emptySet();
    }

    /* compiled from: ImageEditorHudV2.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R&\u0010\u0005\u001a\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\b0\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/ImageEditorHudV2$Companion;", "", "()V", "ANIMATION_DURATION", "", "DRAW_WIDTH_BOUNDARIES", "", "Lorg/thoughtcrime/securesms/scribbles/ImageEditorHudV2$Mode;", "Lkotlin/Pair;", "", "withHighlighterAlpha", "", NotificationProfileDatabase.NotificationProfileTable.COLOR, "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final int withHighlighterAlpha(int i) {
            return (i & 16777215) | 1610612736;
        }

        private Companion() {
        }
    }

    private final List<Animator> animateInViewSet(Set<? extends View> set) {
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
        for (View view : set) {
            ViewExtensionsKt.setVisible(view, true);
            arrayList.add(ObjectAnimator.ofFloat(view, "alpha", 1.0f));
        }
        ArrayList<View> arrayList2 = new ArrayList();
        for (Object obj : set) {
            if (this.viewsToSlide.contains((View) obj)) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList2, 10));
        for (View view2 : arrayList2) {
            arrayList3.add(ObjectAnimator.ofFloat(view2, "translationY", 0.0f));
        }
        return CollectionsKt___CollectionsKt.plus((Collection) arrayList, (Iterable) arrayList3);
    }

    private final List<Animator> animateOutViewSet(Set<? extends View> set) {
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
        for (View view : set) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "alpha", 0.0f);
            Intrinsics.checkNotNullExpressionValue(ofFloat, "");
            ofFloat.addListener(new Animator.AnimatorListener(view) { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorHudV2$animateOutViewSet$lambda-25$lambda-24$$inlined$doOnEnd$1
                final /* synthetic */ View $child$inlined;

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationCancel(Animator animator) {
                    Intrinsics.checkNotNullParameter(animator, "animator");
                }

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationRepeat(Animator animator) {
                    Intrinsics.checkNotNullParameter(animator, "animator");
                }

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationStart(Animator animator) {
                    Intrinsics.checkNotNullParameter(animator, "animator");
                }

                {
                    this.$child$inlined = r1;
                }

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    Intrinsics.checkNotNullParameter(animator, "animator");
                    ViewExtensionsKt.setVisible(this.$child$inlined, false);
                }
            });
            arrayList.add(ofFloat);
        }
        ArrayList<View> arrayList2 = new ArrayList();
        for (Object obj : set) {
            if (this.viewsToSlide.contains((View) obj)) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList2, 10));
        for (View view2 : arrayList2) {
            arrayList3.add(ObjectAnimator.ofFloat(view2, "translationY", (float) ViewUtil.dpToPx(56)));
        }
        return CollectionsKt___CollectionsKt.plus((Collection) arrayList, (Iterable) arrayList3);
    }
}
