package org.thoughtcrime.securesms.scribbles;

import android.animation.FloatEvaluator;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.widget.SeekBar;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.core.graphics.ColorUtils;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysJvmKt;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.scribbles.HSVColorSlider;
import org.thoughtcrime.securesms.util.CustomDrawWrapperKt;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: HSVColorSlider.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0004\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001,B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001a\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u000b2\b\b\u0002\u0010\u0011\u001a\u00020\u000bH\u0002J\b\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\u00132\b\b\u0001\u0010\u0015\u001a\u00020\u0004J\b\u0010\u0016\u001a\u00020\u0004H\u0007J \u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u000bH\u0002J\f\u0010\u001c\u001a\u00020\u0013*\u00020\u0013H\u0002J\n\u0010\u001d\u001a\u00020\u0004*\u00020\u001eJ\u0012\u0010\u001f\u001a\u00020 *\u00020\u001e2\u0006\u0010!\u001a\u00020\u0004JD\u0010\"\u001a\u00020 *\u00020\u001e2\b\b\u0001\u0010#\u001a\u00020\u00042\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020 0%2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020 0'2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020 0'J\u0014\u0010)\u001a\u00020\u000b*\u00020*2\u0006\u0010+\u001a\u00020*H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bXT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006-"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/HSVColorSlider;", "", "()V", "BLACK_DIVISIONS", "", "COLOR_DIVISIONS", "EVALUATOR", "Landroid/animation/FloatEvaluator;", "MAX_HUE", "MAX_SEEK_DIVISIONS", "STANDARD_LIGHTNESS", "", "WHITE_DIVISIONS", "colors", "", "calculateLightness", "hue", "valueFor60To80", "createColorProgressDrawable", "Landroid/graphics/drawable/Drawable;", "createThumbDrawable", "borderColor", "getLastColor", "interpolate", "point1", "Landroid/graphics/PointF;", "point2", "x", "forSeekBar", "getColor", "Landroidx/appcompat/widget/AppCompatSeekBar;", "setColor", "", NotificationProfileDatabase.NotificationProfileTable.COLOR, "setUpForColor", "thumbBorderColor", "onColorChanged", "Lkotlin/Function1;", "onDragStart", "Lkotlin/Function0;", "onDragEnd", "toHue", "", "max", "ThumbDrawable", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class HSVColorSlider {
    private static final int BLACK_DIVISIONS;
    private static final int COLOR_DIVISIONS;
    private static final FloatEvaluator EVALUATOR = new FloatEvaluator();
    public static final HSVColorSlider INSTANCE = new HSVColorSlider();
    private static final int MAX_HUE;
    private static final int MAX_SEEK_DIVISIONS;
    private static final float STANDARD_LIGHTNESS;
    private static final int WHITE_DIVISIONS;
    private static final int[] colors;

    private HSVColorSlider() {
    }

    static {
        INSTANCE = new HSVColorSlider();
        EVALUATOR = new FloatEvaluator();
        IntRange intRange = new IntRange(0, BLACK_DIVISIONS);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(intRange, 10));
        Iterator<Integer> it = intRange.iterator();
        while (it.hasNext()) {
            arrayList.add(Integer.valueOf(ColorUtils.HSLToColor(new float[]{360.0f, 1.0f, (((float) ((IntIterator) it).nextInt()) / 175.0f) * STANDARD_LIGHTNESS})));
        }
        int[] iArr = CollectionsKt___CollectionsKt.toIntArray(arrayList);
        IntRange intRange2 = new IntRange(0, COLOR_DIVISIONS);
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(intRange2, 10));
        Iterator<Integer> it2 = intRange2.iterator();
        while (it2.hasNext()) {
            int nextInt = ((IntIterator) it2).nextInt();
            HSVColorSlider hSVColorSlider = INSTANCE;
            arrayList2.add(Integer.valueOf(ColorUtils.HSLToColor(new float[]{hSVColorSlider.toHue(Integer.valueOf(nextInt), Integer.valueOf((int) COLOR_DIVISIONS)), 1.0f, hSVColorSlider.calculateLightness((float) nextInt, STANDARD_LIGHTNESS)})));
        }
        int[] iArr2 = ArraysKt___ArraysJvmKt.plus(iArr, CollectionsKt___CollectionsKt.toIntArray(arrayList2));
        IntRange intRange3 = new IntRange(0, WHITE_DIVISIONS);
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(intRange3, 10));
        Iterator<Integer> it3 = intRange3.iterator();
        while (it3.hasNext()) {
            int nextInt2 = ((IntIterator) it3).nextInt();
            HSVColorSlider hSVColorSlider2 = INSTANCE;
            Float evaluate = EVALUATOR.evaluate(((float) nextInt2) / 125.0f, (Number) Float.valueOf(hSVColorSlider2.calculateLightness(1023.0f, STANDARD_LIGHTNESS)), (Number) Float.valueOf(1.0f));
            Intrinsics.checkNotNullExpressionValue(evaluate, "EVALUATOR.evaluate(value… STANDARD_LIGHTNESS), 1f)");
            arrayList3.add(Integer.valueOf(ColorUtils.HSLToColor(new float[]{hSVColorSlider2.toHue(Integer.valueOf((int) COLOR_DIVISIONS), Integer.valueOf((int) COLOR_DIVISIONS)), 1.0f, evaluate.floatValue()})));
        }
        colors = ArraysKt___ArraysJvmKt.plus(iArr2, CollectionsKt___CollectionsKt.toIntArray(arrayList3));
    }

    public final int getLastColor() {
        return ArraysKt___ArraysKt.last(colors);
    }

    public final int getColor(AppCompatSeekBar appCompatSeekBar) {
        Intrinsics.checkNotNullParameter(appCompatSeekBar, "<this>");
        return colors[appCompatSeekBar.getProgress()];
    }

    public final void setColor(AppCompatSeekBar appCompatSeekBar, int i) {
        Intrinsics.checkNotNullParameter(appCompatSeekBar, "<this>");
        int i2 = ArraysKt___ArraysKt.indexOf(colors, i);
        if (i2 < 0) {
            i2 = 0;
        }
        appCompatSeekBar.setProgress(i2);
    }

    public final void setUpForColor(AppCompatSeekBar appCompatSeekBar, int i, Function1<? super Integer, Unit> function1, Function0<Unit> function0, Function0<Unit> function02) {
        Intrinsics.checkNotNullParameter(appCompatSeekBar, "<this>");
        Intrinsics.checkNotNullParameter(function1, "onColorChanged");
        Intrinsics.checkNotNullParameter(function0, "onDragStart");
        Intrinsics.checkNotNullParameter(function02, "onDragEnd");
        appCompatSeekBar.setMax(MAX_SEEK_DIVISIONS);
        appCompatSeekBar.setThumb(createThumbDrawable(i));
        appCompatSeekBar.setProgressDrawable(createColorProgressDrawable());
        appCompatSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(appCompatSeekBar, function1, function0, function02) { // from class: org.thoughtcrime.securesms.scribbles.HSVColorSlider$setUpForColor$1
            final /* synthetic */ Function1<Integer, Unit> $onColorChanged;
            final /* synthetic */ Function0<Unit> $onDragEnd;
            final /* synthetic */ Function0<Unit> $onDragStart;
            final /* synthetic */ AppCompatSeekBar $this_setUpForColor;

            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$this_setUpForColor = r1;
                this.$onColorChanged = r2;
                this.$onDragStart = r3;
                this.$onDragEnd = r4;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int i2, boolean z) {
                int i3 = HSVColorSlider.colors[i2];
                Drawable thumb = this.$this_setUpForColor.getThumb();
                if (thumb != null) {
                    ((HSVColorSlider.ThumbDrawable) thumb).setColor(i3);
                    this.$onColorChanged.invoke(Integer.valueOf(i3));
                    return;
                }
                throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.scribbles.HSVColorSlider.ThumbDrawable");
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
                this.$onDragStart.invoke();
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                this.$onDragEnd.invoke();
            }
        });
        appCompatSeekBar.setProgress(176);
        Drawable thumb = appCompatSeekBar.getThumb();
        if (thumb != null) {
            ((ThumbDrawable) thumb).setColor(colors[appCompatSeekBar.getProgress()]);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.scribbles.HSVColorSlider.ThumbDrawable");
    }

    public final Drawable createThumbDrawable(int i) {
        return new ThumbDrawable(i);
    }

    private final Drawable createColorProgressDrawable() {
        return forSeekBar(new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors));
    }

    static /* synthetic */ float calculateLightness$default(HSVColorSlider hSVColorSlider, float f, float f2, int i, Object obj) {
        if ((i & 2) != 0) {
            f2 = 0.3f;
        }
        return hSVColorSlider.calculateLightness(f, f2);
    }

    private final float calculateLightness(float f, float f2) {
        PointF pointF = new PointF();
        PointF pointF2 = new PointF();
        if (f >= 0.0f && f < 60.0f) {
            pointF.set(0.0f, 0.45f);
            pointF2.set(60.0f, f2);
        } else if (f >= 60.0f && f < 180.0f) {
            return f2;
        } else {
            if (f >= 180.0f && f < 240.0f) {
                pointF.set(180.0f, f2);
                pointF2.set(240.0f, 0.5f);
            } else if (f >= 240.0f && f < 300.0f) {
                pointF.set(240.0f, 0.5f);
                pointF2.set(300.0f, STANDARD_LIGHTNESS);
            } else if (f < 300.0f || f >= 360.0f) {
                return 0.45f;
            } else {
                pointF.set(300.0f, STANDARD_LIGHTNESS);
                pointF2.set(360.0f, 0.45f);
            }
        }
        return interpolate(pointF, pointF2, f);
    }

    private final float interpolate(PointF pointF, PointF pointF2, float f) {
        float f2 = pointF.y;
        float f3 = pointF2.x;
        float f4 = pointF2.y;
        float f5 = pointF.x;
        return ((f2 * (f3 - f)) + (f4 * (f - f5))) / (f3 - f5);
    }

    private final float toHue(Number number, Number number2) {
        return Util.clamp(number.floatValue() * (((float) MAX_HUE) / number2.floatValue()), 0.0f, 360.0f);
    }

    private final Drawable forSeekBar(Drawable drawable) {
        int dpToPx = ViewUtil.dpToPx(1);
        IntRange intRange = new IntRange(1, 8);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(intRange, 10));
        Iterator<Integer> it = intRange.iterator();
        while (it.hasNext()) {
            ((IntIterator) it).nextInt();
            arrayList.add(Float.valueOf(50.0f));
        }
        return CustomDrawWrapperKt.customizeOnDraw(drawable, new Function2<Drawable, Canvas, Unit>(new RectF(), drawable, dpToPx, new Path(), CollectionsKt___CollectionsKt.toFloatArray(arrayList)) { // from class: org.thoughtcrime.securesms.scribbles.HSVColorSlider$forSeekBar$1
            final /* synthetic */ RectF $bounds;
            final /* synthetic */ Path $clipPath;
            final /* synthetic */ int $height;
            final /* synthetic */ float[] $radii;
            final /* synthetic */ Drawable $this_forSeekBar;

            /* access modifiers changed from: package-private */
            {
                this.$bounds = r1;
                this.$this_forSeekBar = r2;
                this.$height = r3;
                this.$clipPath = r4;
                this.$radii = r5;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((Drawable) obj, (Canvas) obj2);
                return Unit.INSTANCE;
            }

            public final void invoke(Drawable drawable2, Canvas canvas) {
                Intrinsics.checkNotNullParameter(drawable2, "wrapped");
                Intrinsics.checkNotNullParameter(canvas, "canvas");
                canvas.save();
                this.$bounds.set(this.$this_forSeekBar.getBounds());
                this.$bounds.inset(0.0f, (((float) this.$height) / 2.0f) + ((float) 1));
                this.$clipPath.rewind();
                this.$clipPath.addRoundRect(this.$bounds, this.$radii, Path.Direction.CW);
                canvas.clipPath(this.$clipPath);
                drawable2.draw(canvas);
                canvas.restore();
            }
        });
    }

    /* compiled from: HSVColorSlider.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\b\u0010\u0012\u001a\u00020\u0003H\u0016J\b\u0010\u0013\u001a\u00020\u0003H\u0016J\b\u0010\u0014\u001a\u00020\u0003H\u0016J\u0010\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u0003H\u0016J\u0010\u0010\u0017\u001a\u00020\u000f2\b\b\u0001\u0010\u0018\u001a\u00020\u0003J\u0012\u0010\u0019\u001a\u00020\u000f2\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/HSVColorSlider$ThumbDrawable;", "Landroid/graphics/drawable/Drawable;", "borderColor", "", "(I)V", "borderPaint", "Landroid/graphics/Paint;", "borderWidth", "innerRadius", "", "paint", "thumbInnerSize", "thumbRadius", "thumbSize", "draw", "", "canvas", "Landroid/graphics/Canvas;", "getIntrinsicHeight", "getIntrinsicWidth", "getOpacity", "setAlpha", "alpha", "setColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "setColorFilter", "colorFilter", "Landroid/graphics/ColorFilter;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ThumbDrawable extends Drawable {
        public static final Companion Companion = new Companion(null);
        private static final int THUMB_INNER_SIZE = 8;
        private static final int THUMB_MARGIN = 24;
        private final Paint borderPaint;
        private final int borderWidth;
        private final float innerRadius;
        private final Paint paint;
        private final int thumbInnerSize;
        private final float thumbRadius;
        private final float thumbSize;

        @Override // android.graphics.drawable.Drawable
        public int getOpacity() {
            return -2;
        }

        @Override // android.graphics.drawable.Drawable
        public void setAlpha(int i) {
        }

        @Override // android.graphics.drawable.Drawable
        public void setColorFilter(ColorFilter colorFilter) {
        }

        public ThumbDrawable(int i) {
            Paint paint = new Paint(1);
            paint.setColor(i);
            this.borderPaint = paint;
            Paint paint2 = new Paint(1);
            paint2.setColor(0);
            this.paint = paint2;
            int dpToPx = ViewUtil.dpToPx(THUMB_MARGIN);
            this.borderWidth = dpToPx;
            int dpToPx2 = ViewUtil.dpToPx(THUMB_INNER_SIZE);
            this.thumbInnerSize = dpToPx2;
            this.innerRadius = ((float) dpToPx2) / 2.0f;
            float f = (float) (dpToPx2 + dpToPx);
            this.thumbSize = f;
            this.thumbRadius = f / 2.0f;
        }

        @Override // android.graphics.drawable.Drawable
        public int getIntrinsicHeight() {
            return ViewUtil.dpToPx(48);
        }

        @Override // android.graphics.drawable.Drawable
        public int getIntrinsicWidth() {
            return ViewUtil.dpToPx(48);
        }

        public final void setColor(int i) {
            this.paint.setColor(i);
            invalidateSelf();
        }

        @Override // android.graphics.drawable.Drawable
        public void draw(Canvas canvas) {
            Intrinsics.checkNotNullParameter(canvas, "canvas");
            canvas.drawCircle((((float) getBounds().width()) / 2.0f) + ((float) getBounds().left), (((float) getBounds().height()) / 2.0f) + ((float) getBounds().top), this.thumbRadius, this.borderPaint);
            canvas.drawCircle((((float) getBounds().width()) / 2.0f) + ((float) getBounds().left), (((float) getBounds().height()) / 2.0f) + ((float) getBounds().top), this.innerRadius, this.paint);
        }

        /* compiled from: HSVColorSlider.kt */
        @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0002XD¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0002XD¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/HSVColorSlider$ThumbDrawable$Companion;", "", "()V", "THUMB_INNER_SIZE", "", "THUMB_MARGIN", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }
    }
}
