package org.thoughtcrime.securesms.scribbles;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.airbnb.lottie.SimpleColorFilter;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.imageeditor.core.HiddenEditText;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.model.EditorElement;
import org.signal.imageeditor.core.renderers.MultiLineTextRenderer;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.KeyboardEntryDialogFragment;
import org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: TextEntryDialogFragment.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00122\u00020\u0001:\u0002\u0012\u0013B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u001a\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/TextEntryDialogFragment;", "Lorg/thoughtcrime/securesms/components/KeyboardEntryDialogFragment;", "()V", "colorIndicatorAlphaAnimator", "Landroid/animation/Animator;", "controller", "Lorg/thoughtcrime/securesms/scribbles/TextEntryDialogFragment$Controller;", "hiddenTextEntry", "Lorg/signal/imageeditor/core/HiddenEditText;", "onDismiss", "", "dialog", "Landroid/content/DialogInterface;", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "Controller", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextEntryDialogFragment extends KeyboardEntryDialogFragment {
    public static final Companion Companion = new Companion(null);
    private Animator colorIndicatorAlphaAnimator;
    private Controller controller;
    private HiddenEditText hiddenTextEntry;

    /* compiled from: TextEntryDialogFragment.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH&J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH&¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/TextEntryDialogFragment$Controller;", "", "onTextColorChange", "", "colorIndex", "", "onTextEntryDialogDismissed", "hasText", "", "zoomToFitText", "editorElement", "Lorg/signal/imageeditor/core/model/EditorElement;", "textRenderer", "Lorg/signal/imageeditor/core/renderers/MultiLineTextRenderer;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Controller {
        void onTextColorChange(int i);

        void onTextEntryDialogDismissed(boolean z);

        void zoomToFitText(EditorElement editorElement, MultiLineTextRenderer multiLineTextRenderer);
    }

    public TextEntryDialogFragment() {
        super(R.layout.v2_media_image_editor_text_entry_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Controller controller;
        Intrinsics.checkNotNullParameter(view, "view");
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        controller = (Controller) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment.Controller");
                    }
                } else if (fragment instanceof Controller) {
                    controller = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.controller = controller;
            HiddenEditText hiddenEditText = new HiddenEditText(requireContext());
            this.hiddenTextEntry = hiddenEditText;
            if (!ImageEditorFragment.CAN_RENDER_EMOJI) {
                hiddenEditText.addTextFilter(new RemoveEmojiTextFilter());
            }
            ViewGroup viewGroup = (ViewGroup) view;
            HiddenEditText hiddenEditText2 = this.hiddenTextEntry;
            HiddenEditText hiddenEditText3 = null;
            if (hiddenEditText2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("hiddenTextEntry");
                hiddenEditText2 = null;
            }
            viewGroup.addView(hiddenEditText2);
            view.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment$$ExternalSyntheticLambda0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TextEntryDialogFragment.m2686onViewCreated$lambda0(TextEntryDialogFragment.this, view2);
                }
            });
            Parcelable parcelable = requireArguments().getParcelable("element");
            if (parcelable != null) {
                EditorElement editorElement = (EditorElement) parcelable;
                boolean z = requireArguments().getBoolean("incognito");
                boolean z2 = requireArguments().getBoolean("selectAll");
                HiddenEditText hiddenEditText4 = this.hiddenTextEntry;
                if (hiddenEditText4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("hiddenTextEntry");
                    hiddenEditText4 = null;
                }
                hiddenEditText4.setCurrentTextEditorElement(editorElement);
                HiddenEditText hiddenEditText5 = this.hiddenTextEntry;
                if (hiddenEditText5 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("hiddenTextEntry");
                    hiddenEditText5 = null;
                }
                hiddenEditText5.setIncognitoKeyboardEnabled(z);
                if (z2) {
                    HiddenEditText hiddenEditText6 = this.hiddenTextEntry;
                    if (hiddenEditText6 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("hiddenTextEntry");
                        hiddenEditText6 = null;
                    }
                    hiddenEditText6.selectAll();
                }
                HiddenEditText hiddenEditText7 = this.hiddenTextEntry;
                if (hiddenEditText7 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("hiddenTextEntry");
                    hiddenEditText7 = null;
                }
                hiddenEditText7.setOnEditOrSelectionChange(new HiddenEditText.OnEditOrSelectionChange() { // from class: org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment$$ExternalSyntheticLambda1
                    @Override // org.signal.imageeditor.core.HiddenEditText.OnEditOrSelectionChange
                    public final void onChange(EditorElement editorElement2, MultiLineTextRenderer multiLineTextRenderer) {
                        TextEntryDialogFragment.m2687onViewCreated$lambda1(TextEntryDialogFragment.this, editorElement2, multiLineTextRenderer);
                    }
                });
                HiddenEditText hiddenEditText8 = this.hiddenTextEntry;
                if (hiddenEditText8 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("hiddenTextEntry");
                    hiddenEditText8 = null;
                }
                hiddenEditText8.setOnEndEdit(new Runnable() { // from class: org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment$$ExternalSyntheticLambda2
                    @Override // java.lang.Runnable
                    public final void run() {
                        TextEntryDialogFragment.m2688onViewCreated$lambda2(TextEntryDialogFragment.this);
                    }
                });
                HiddenEditText hiddenEditText9 = this.hiddenTextEntry;
                if (hiddenEditText9 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("hiddenTextEntry");
                } else {
                    hiddenEditText3 = hiddenEditText9;
                }
                ViewUtil.focusAndShowKeyboard(hiddenEditText3);
                View findViewById = view.findViewById(R.id.image_editor_hud_draw_color_bar);
                Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.i…ditor_hud_draw_color_bar)");
                AppCompatSeekBar appCompatSeekBar = (AppCompatSeekBar) findViewById;
                View findViewById2 = view.findViewById(R.id.image_editor_hud_color_indicator);
                Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.i…itor_hud_color_indicator)");
                ImageView imageView = (ImageView) findViewById2;
                View findViewById3 = view.findViewById(R.id.image_editor_hud_text_style_button);
                Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.i…or_hud_text_style_button)");
                imageView.setBackground(AppCompatResources.getDrawable(requireContext(), R.drawable.ic_color_preview));
                HSVColorSlider.INSTANCE.setUpForColor(appCompatSeekBar, -1, new Function1<Integer, Unit>(imageView, appCompatSeekBar, this) { // from class: org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment$onViewCreated$4
                    final /* synthetic */ ImageView $colorIndicator;
                    final /* synthetic */ AppCompatSeekBar $slider;
                    final /* synthetic */ TextEntryDialogFragment this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.$colorIndicator = r1;
                        this.$slider = r2;
                        this.this$0 = r3;
                    }

                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                        invoke(num.intValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(int i) {
                        this.$colorIndicator.getDrawable().setColorFilter(new SimpleColorFilter(HSVColorSlider.INSTANCE.getColor(this.$slider)));
                        this.$colorIndicator.setTranslationX(((float) this.$slider.getThumb().getBounds().left) + ((float) ViewUtil.dpToPx(16)));
                        TextEntryDialogFragment.Controller controller2 = this.this$0.controller;
                        if (controller2 == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("controller");
                            controller2 = null;
                        }
                        controller2.onTextColorChange(this.$slider.getProgress());
                    }
                }, new Function0<Unit>(this, imageView) { // from class: org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment$onViewCreated$5
                    final /* synthetic */ ImageView $colorIndicator;
                    final /* synthetic */ TextEntryDialogFragment this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                        this.$colorIndicator = r2;
                    }

                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        Animator animator = this.this$0.colorIndicatorAlphaAnimator;
                        if (animator != null) {
                            animator.end();
                        }
                        TextEntryDialogFragment textEntryDialogFragment = this.this$0;
                        ImageView imageView2 = this.$colorIndicator;
                        textEntryDialogFragment.colorIndicatorAlphaAnimator = ObjectAnimator.ofFloat(imageView2, "alpha", imageView2.getAlpha(), 1.0f);
                        Animator animator2 = this.this$0.colorIndicatorAlphaAnimator;
                        if (animator2 != null) {
                            animator2.setDuration(150);
                        }
                        Animator animator3 = this.this$0.colorIndicatorAlphaAnimator;
                        if (animator3 != null) {
                            animator3.start();
                        }
                    }
                }, new Function0<Unit>(this, imageView) { // from class: org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment$onViewCreated$6
                    final /* synthetic */ ImageView $colorIndicator;
                    final /* synthetic */ TextEntryDialogFragment this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                        this.$colorIndicator = r2;
                    }

                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        Animator animator = this.this$0.colorIndicatorAlphaAnimator;
                        if (animator != null) {
                            animator.end();
                        }
                        TextEntryDialogFragment textEntryDialogFragment = this.this$0;
                        ImageView imageView2 = this.$colorIndicator;
                        textEntryDialogFragment.colorIndicatorAlphaAnimator = ObjectAnimator.ofFloat(imageView2, "alpha", imageView2.getAlpha(), 0.0f);
                        Animator animator2 = this.this$0.colorIndicatorAlphaAnimator;
                        if (animator2 != null) {
                            animator2.setDuration(150);
                        }
                        Animator animator3 = this.this$0.colorIndicatorAlphaAnimator;
                        if (animator3 != null) {
                            animator3.start();
                        }
                    }
                });
                appCompatSeekBar.setProgress(requireArguments().getInt("color_index"));
                ((ImageView) findViewById3).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment$$ExternalSyntheticLambda3
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        TextEntryDialogFragment.m2689onViewCreated$lambda3(EditorElement.this, view2);
                    }
                });
                return;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2686onViewCreated$lambda0(TextEntryDialogFragment textEntryDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(textEntryDialogFragment, "this$0");
        textEntryDialogFragment.dismissAllowingStateLoss();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m2687onViewCreated$lambda1(TextEntryDialogFragment textEntryDialogFragment, EditorElement editorElement, MultiLineTextRenderer multiLineTextRenderer) {
        Intrinsics.checkNotNullParameter(textEntryDialogFragment, "this$0");
        Intrinsics.checkNotNullParameter(editorElement, "editorElement");
        Intrinsics.checkNotNullParameter(multiLineTextRenderer, "textRenderer");
        Controller controller = textEntryDialogFragment.controller;
        if (controller == null) {
            Intrinsics.throwUninitializedPropertyAccessException("controller");
            controller = null;
        }
        controller.zoomToFitText(editorElement, multiLineTextRenderer);
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m2688onViewCreated$lambda2(TextEntryDialogFragment textEntryDialogFragment) {
        Intrinsics.checkNotNullParameter(textEntryDialogFragment, "this$0");
        textEntryDialogFragment.dismissAllowingStateLoss();
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2689onViewCreated$lambda3(EditorElement editorElement, View view) {
        Intrinsics.checkNotNullParameter(editorElement, "$element");
        Renderer renderer = editorElement.getRenderer();
        if (renderer != null) {
            ((MultiLineTextRenderer) renderer).nextMode();
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.signal.imageeditor.core.renderers.MultiLineTextRenderer");
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(dialogInterface, "dialog");
        Controller controller = this.controller;
        HiddenEditText hiddenEditText = null;
        if (controller == null) {
            Intrinsics.throwUninitializedPropertyAccessException("controller");
            controller = null;
        }
        HiddenEditText hiddenEditText2 = this.hiddenTextEntry;
        if (hiddenEditText2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hiddenTextEntry");
        } else {
            hiddenEditText = hiddenEditText2;
        }
        Editable text = hiddenEditText.getText();
        controller.onTextEntryDialogDismissed(!(text == null || text.length() == 0));
    }

    /* compiled from: TextEntryDialogFragment.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J.\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/TextEntryDialogFragment$Companion;", "", "()V", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "editorElement", "Lorg/signal/imageeditor/core/model/EditorElement;", "isIncognitoEnabled", "", "selectAll", "colorIndex", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, EditorElement editorElement, boolean z, boolean z2, int i) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            Intrinsics.checkNotNullParameter(editorElement, "editorElement");
            Bundle bundle = new Bundle();
            bundle.putParcelable("element", editorElement);
            bundle.putBoolean("incognito", z);
            bundle.putBoolean("selectAll", z2);
            bundle.putInt("color_index", i);
            TextEntryDialogFragment textEntryDialogFragment = new TextEntryDialogFragment();
            textEntryDialogFragment.setArguments(bundle);
            textEntryDialogFragment.show(fragmentManager, "text_entry");
        }
    }
}
