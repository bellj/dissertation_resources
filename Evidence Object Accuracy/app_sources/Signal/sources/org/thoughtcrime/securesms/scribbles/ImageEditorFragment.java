package org.thoughtcrime.securesms.scribbles;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.FontUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.ColorableRenderer;
import org.signal.imageeditor.core.ImageEditorView;
import org.signal.imageeditor.core.ImageEditorView$$ExternalSyntheticLambda1;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.SelectableRenderer;
import org.signal.imageeditor.core.UndoRedoStackListener;
import org.signal.imageeditor.core.model.EditorElement;
import org.signal.imageeditor.core.model.EditorFlags;
import org.signal.imageeditor.core.model.EditorModel;
import org.signal.imageeditor.core.renderers.BezierDrawingRenderer;
import org.signal.imageeditor.core.renderers.FaceBlurRenderer;
import org.signal.imageeditor.core.renderers.MultiLineTextRenderer;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.ResizeAnimation;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.fonts.FontTypefaceProvider;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.MediaSendPageFragment;
import org.thoughtcrime.securesms.mediasend.v2.MediaAnimations;
import org.thoughtcrime.securesms.mms.PushMediaConstraints;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.scribbles.FaceDetector;
import org.thoughtcrime.securesms.scribbles.ImageEditorFragment;
import org.thoughtcrime.securesms.scribbles.ImageEditorHudV2;
import org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.ParcelUtil;
import org.thoughtcrime.securesms.util.SaveAttachmentTask;
import org.thoughtcrime.securesms.util.StorageUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.ThrottledDebouncer;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public final class ImageEditorFragment extends Fragment implements ImageEditorHudV2.EventListener, MediaSendPageFragment, TextEntryDialogFragment.Controller {
    public static final boolean CAN_RENDER_EMOJI = FontUtil.canRenderEmojiAtFontSize(1024.0f);
    private static final int CROP_HUD_PROTECTION = ViewUtil.dpToPx(144);
    private static final int DRAW_HUD_PROTECTION = ViewUtil.dpToPx(72);
    private static final String KEY_IMAGE_URI;
    private static final String KEY_MODE;
    private static final float PORTRAIT_ASPECT_RATIO;
    private static final int SELECT_STICKER_REQUEST_CODE;
    private static final String TAG = Log.tag(ImageEditorFragment.class);
    private Pair<Uri, FaceDetectionResult> cachedFaceDetection;
    private Controller controller;
    private EditorElement currentSelection;
    private final ThrottledDebouncer deleteFadeDebouncer = new ThrottledDebouncer(500);
    private final ImageEditorView.DragListener dragListener = new ImageEditorView.DragListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment.2
        @Override // org.signal.imageeditor.core.ImageEditorView.DragListener
        public void onDragStarted(EditorElement editorElement) {
            if (ImageEditorFragment.this.imageEditorHud.getMode() == ImageEditorHudV2.Mode.CROP) {
                ImageEditorFragment.this.updateHudDialRotation();
                return;
            }
            if (editorElement == null || (editorElement.getRenderer() instanceof BezierDrawingRenderer)) {
                ImageEditorFragment.this.setCurrentSelection(null);
            } else {
                ImageEditorFragment.this.setCurrentSelection(editorElement);
            }
            if (ImageEditorFragment.this.imageEditorView.getMode() == ImageEditorView.Mode.MoveAndResize) {
                ImageEditorFragment.this.imageEditorHud.setMode(ImageEditorHudV2.Mode.DELETE);
            } else {
                ImageEditorFragment.this.imageEditorHud.animate().alpha(0.0f);
            }
        }

        @Override // org.signal.imageeditor.core.ImageEditorView.DragListener
        public void onDragMoved(EditorElement editorElement, boolean z) {
            if (ImageEditorFragment.this.imageEditorHud.getMode() == ImageEditorHudV2.Mode.CROP || editorElement == null) {
                ImageEditorFragment.this.updateHudDialRotation();
            } else if (z) {
                ImageEditorFragment.this.deleteFadeDebouncer.publish(new ImageEditorFragment$2$$ExternalSyntheticLambda0(this, editorElement));
            } else {
                ImageEditorFragment.this.deleteFadeDebouncer.publish(new ImageEditorFragment$2$$ExternalSyntheticLambda1(this, editorElement));
            }
        }

        public /* synthetic */ void lambda$onDragMoved$0(EditorElement editorElement) {
            if (!ImageEditorFragment.this.wasInTrashHitZone) {
                ImageEditorFragment.this.wasInTrashHitZone = true;
                if (ImageEditorFragment.this.imageEditorHud.isHapticFeedbackEnabled()) {
                    ImageEditorFragment.this.imageEditorHud.performHapticFeedback(3);
                }
            }
            ImageEditorView imageEditorView = ImageEditorFragment.this.imageEditorView;
            Objects.requireNonNull(imageEditorView);
            editorElement.animatePartialFadeOut(new ImageEditorView$$ExternalSyntheticLambda1(imageEditorView));
        }

        public /* synthetic */ void lambda$onDragMoved$1(EditorElement editorElement) {
            ImageEditorFragment.this.wasInTrashHitZone = false;
            ImageEditorView imageEditorView = ImageEditorFragment.this.imageEditorView;
            Objects.requireNonNull(imageEditorView);
            editorElement.animatePartialFadeIn(new ImageEditorView$$ExternalSyntheticLambda1(imageEditorView));
        }

        @Override // org.signal.imageeditor.core.ImageEditorView.DragListener
        public void onDragEnded(EditorElement editorElement, boolean z) {
            ImageEditorFragment.this.wasInTrashHitZone = false;
            ImageEditorFragment.this.imageEditorHud.animate().alpha(1.0f);
            if (ImageEditorFragment.this.imageEditorHud.getMode() == ImageEditorHudV2.Mode.CROP) {
                ImageEditorFragment.this.updateHudDialRotation();
            } else if (z) {
                ImageEditorFragment.this.deleteFadeDebouncer.clear();
                ImageEditorFragment.this.onDelete();
                ImageEditorFragment.this.setCurrentSelection(null);
                ImageEditorFragment.this.onPopEditorMode();
            } else if (editorElement != null && (editorElement.getRenderer() instanceof MultiLineTextRenderer)) {
                ImageEditorView imageEditorView = ImageEditorFragment.this.imageEditorView;
                Objects.requireNonNull(imageEditorView);
                editorElement.animatePartialFadeIn(new ImageEditorView$$ExternalSyntheticLambda1(imageEditorView));
                if (ImageEditorFragment.this.imageEditorHud.getMode() != ImageEditorHudV2.Mode.TEXT) {
                    ImageEditorFragment.this.imageEditorHud.setMode(ImageEditorHudV2.Mode.MOVE_TEXT);
                }
            } else if (editorElement != null && (editorElement.getRenderer() instanceof UriGlideRenderer)) {
                ImageEditorView imageEditorView2 = ImageEditorFragment.this.imageEditorView;
                Objects.requireNonNull(imageEditorView2);
                editorElement.animatePartialFadeIn(new ImageEditorView$$ExternalSyntheticLambda1(imageEditorView2));
                ImageEditorFragment.this.imageEditorHud.setMode(ImageEditorHudV2.Mode.MOVE_STICKER);
            }
        }
    };
    private boolean hasMadeAnEditThisSession;
    private ImageEditorHudV2 imageEditorHud;
    private ImageEditorView imageEditorView;
    private int imageMaxHeight;
    private int imageMaxWidth;
    private Uri imageUri;
    private float initialDialImageDegrees;
    private float initialDialScale;
    private final RequestListener<Bitmap> mainImageRequestListener = new RequestListener<Bitmap>() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment.1
        @Override // com.bumptech.glide.request.RequestListener
        public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, DataSource dataSource, boolean z) {
            return onResourceReady((Bitmap) obj, obj2, (Target<Bitmap>) target, dataSource, z);
        }

        @Override // com.bumptech.glide.request.RequestListener
        public boolean onLoadFailed(GlideException glideException, Object obj, Target<Bitmap> target, boolean z) {
            ImageEditorFragment.this.controller.onMainImageFailedToLoad();
            return false;
        }

        public boolean onResourceReady(Bitmap bitmap, Object obj, Target<Bitmap> target, DataSource dataSource, boolean z) {
            ImageEditorFragment.this.controller.onMainImageLoaded();
            return false;
        }
    };
    private float minDialScaleDown;
    private final OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(false) { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment.4
        @Override // androidx.activity.OnBackPressedCallback
        public void handleOnBackPressed() {
            ImageEditorFragment.this.onPopEditorMode();
        }
    };
    private ResizeAnimation resizeAnimation;
    private EditorModel restoredModel;
    private final ImageEditorView.TapListener selectionListener = new ImageEditorView.TapListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment.3
        @Override // org.signal.imageeditor.core.ImageEditorView.TapListener
        public void onEntityDown(EditorElement editorElement) {
            if (editorElement != null) {
                ImageEditorFragment.this.controller.onTouchEventsNeeded(true);
            }
        }

        @Override // org.signal.imageeditor.core.ImageEditorView.TapListener
        public void onEntitySingleTap(EditorElement editorElement) {
            ImageEditorFragment.this.setCurrentSelection(editorElement);
            if (ImageEditorFragment.this.currentSelection == null) {
                ImageEditorFragment.this.onPopEditorMode();
            } else if (editorElement.getRenderer() instanceof MultiLineTextRenderer) {
                setTextElement(editorElement, (ColorableRenderer) editorElement.getRenderer(), ImageEditorFragment.this.imageEditorView.isTextEditing());
            } else {
                ImageEditorFragment.this.imageEditorHud.setMode(ImageEditorHudV2.Mode.MOVE_STICKER);
            }
        }

        @Override // org.signal.imageeditor.core.ImageEditorView.TapListener
        public void onEntityDoubleTap(EditorElement editorElement) {
            ImageEditorFragment.this.setCurrentSelection(editorElement);
            if (editorElement.getRenderer() instanceof MultiLineTextRenderer) {
                setTextElement(editorElement, (ColorableRenderer) editorElement.getRenderer(), true);
            }
        }

        private void setTextElement(EditorElement editorElement, ColorableRenderer colorableRenderer, boolean z) {
            int color = colorableRenderer.getColor();
            ImageEditorFragment.this.imageEditorHud.enterMode(ImageEditorHudV2.Mode.TEXT);
            ImageEditorFragment.this.imageEditorHud.setActiveColor(color);
            if (z) {
                ImageEditorFragment.this.startTextEntityEditing(editorElement, false);
            }
        }
    };
    private boolean wasInTrashHitZone;

    /* loaded from: classes4.dex */
    public interface Controller {
        void onCancelEditing();

        void onDoneEditing();

        void onMainImageFailedToLoad();

        void onMainImageLoaded();

        void onRequestFullScreen(boolean z, boolean z2);

        void onTouchEventsNeeded(boolean z);
    }

    private float getAspectRatioForOrientation(int i) {
        return i == 1 ? 0.5625f : 1.7777778f;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public View getPlaybackControls() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public void notifyHidden() {
    }

    /* loaded from: classes4.dex */
    public static class Data {
        private final Bundle bundle;

        public Data(Bundle bundle) {
            this.bundle = bundle;
        }

        public Data() {
            this(new Bundle());
        }

        void writeModel(EditorModel editorModel) {
            this.bundle.putByteArray("MODEL", ParcelUtil.serialize(editorModel));
        }

        public EditorModel readModel() {
            byte[] byteArray = this.bundle.getByteArray("MODEL");
            if (byteArray == null) {
                return null;
            }
            return (EditorModel) ParcelUtil.deserialize(byteArray, EditorModel.CREATOR);
        }

        public Bundle getBundle() {
            return this.bundle;
        }
    }

    public static ImageEditorFragment newInstanceForAvatarCapture(Uri uri) {
        ImageEditorFragment newInstance = newInstance(uri);
        newInstance.requireArguments().putString(KEY_MODE, Mode.AVATAR_CAPTURE.code);
        return newInstance;
    }

    public static ImageEditorFragment newInstanceForAvatarEdit(Uri uri) {
        ImageEditorFragment newInstance = newInstance(uri);
        newInstance.requireArguments().putString(KEY_MODE, Mode.AVATAR_EDIT.code);
        return newInstance;
    }

    public static ImageEditorFragment newInstance(Uri uri) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_IMAGE_URI, uri);
        bundle.putString(KEY_MODE, Mode.NORMAL.code);
        ImageEditorFragment imageEditorFragment = new ImageEditorFragment();
        imageEditorFragment.setArguments(bundle);
        imageEditorFragment.setUri(uri);
        return imageEditorFragment;
    }

    public void setMode(ImageEditorHudV2.Mode mode) {
        if (this.imageEditorHud.getMode() != mode) {
            this.imageEditorHud.setMode(mode);
        }
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        updateViewPortScaling(this.imageEditorHud.getMode(), this.imageEditorHud.getMode(), configuration.orientation, true);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Fragment parentFragment = getParentFragment();
        if (parentFragment instanceof Controller) {
            this.controller = (Controller) parentFragment;
        } else if (getActivity() instanceof Controller) {
            this.controller = (Controller) getActivity();
        } else {
            throw new IllegalStateException("Parent must implement Controller interface.");
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.imageUri = (Uri) arguments.getParcelable(KEY_IMAGE_URI);
        }
        if (this.imageUri != null) {
            PushMediaConstraints pushMediaConstraints = new PushMediaConstraints(SentMediaQuality.HIGH);
            this.imageMaxWidth = pushMediaConstraints.getImageMaxWidth(requireContext());
            this.imageMaxHeight = pushMediaConstraints.getImageMaxHeight(requireContext());
            return;
        }
        throw new AssertionError("No KEY_IMAGE_URI supplied");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.image_editor_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        Mode byCode = Mode.getByCode(requireArguments().getString(KEY_MODE));
        this.imageEditorHud = (ImageEditorHudV2) view.findViewById(R.id.scribble_hud);
        ImageEditorView imageEditorView = (ImageEditorView) view.findViewById(R.id.image_editor_view);
        this.imageEditorView = imageEditorView;
        imageEditorView.setTypefaceProvider(new FontTypefaceProvider());
        if (!CAN_RENDER_EMOJI) {
            this.imageEditorView.addTextInputFilter(new RemoveEmojiTextFilter());
        }
        int i = (int) (((float) getResources().getDisplayMetrics().widthPixels) * 1.7777778f);
        this.imageEditorView.setMinimumHeight(i);
        this.imageEditorView.requestLayout();
        this.imageEditorHud.setBottomOfImageEditorView(getResources().getDisplayMetrics().heightPixels - i);
        this.imageEditorHud.setEventListener(this);
        this.imageEditorView.setDragListener(this.dragListener);
        this.imageEditorView.setTapListener(this.selectionListener);
        this.imageEditorView.setDrawingChangedListener(new ImageEditorView.DrawingChangedListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda0
            @Override // org.signal.imageeditor.core.ImageEditorView.DrawingChangedListener
            public final void onDrawingChanged(boolean z) {
                ImageEditorFragment.this.lambda$onViewCreated$0(z);
            }
        });
        this.imageEditorView.setUndoRedoStackListener(new UndoRedoStackListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda1
            @Override // org.signal.imageeditor.core.UndoRedoStackListener
            public final void onAvailabilityChanged(boolean z, boolean z2) {
                ImageEditorFragment.this.onUndoRedoAvailabilityChanged(z, z2);
            }
        });
        EditorModel editorModel = this.restoredModel;
        if (editorModel != null) {
            this.restoredModel = null;
        } else {
            editorModel = null;
        }
        int color = ContextCompat.getColor(requireContext(), R.color.signal_colorBackground);
        if (editorModel == null) {
            int i2 = AnonymousClass5.$SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorFragment$Mode[byCode.ordinal()];
            if (i2 == 1) {
                editorModel = EditorModel.createForAvatarEdit(color);
            } else if (i2 != 2) {
                editorModel = EditorModel.create(color);
            } else {
                editorModel = EditorModel.createForAvatarCapture(color);
            }
            EditorElement editorElement = new EditorElement(new UriGlideRenderer(this.imageUri, true, this.imageMaxWidth, this.imageMaxHeight, 25.0f, this.mainImageRequestListener));
            editorElement.getFlags().setSelectable(false).persist();
            editorModel.addElement(editorElement);
        } else {
            this.controller.onMainImageLoaded();
        }
        Mode mode = Mode.AVATAR_CAPTURE;
        if (byCode == mode || byCode == Mode.AVATAR_EDIT) {
            this.imageEditorHud.setUpForAvatarEditing();
        }
        if (byCode == mode) {
            this.imageEditorHud.enterMode(ImageEditorHudV2.Mode.CROP);
        }
        this.imageEditorView.setModel(editorModel);
        if (!SignalStore.tooltips().hasSeenBlurHudIconTooltip()) {
            this.imageEditorHud.showBlurHudTooltip();
            SignalStore.tooltips().markBlurHudIconTooltipSeen();
        }
        onDrawingChanged(false, false);
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), this.onBackPressedCallback);
    }

    public /* synthetic */ void lambda$onViewCreated$0(boolean z) {
        onDrawingChanged(z, true);
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public void setUri(Uri uri) {
        this.imageUri = uri;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public Uri getUri() {
        return this.imageUri;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public Object saveState() {
        Data data = new Data();
        data.writeModel(this.imageEditorView.getModel());
        return data;
    }

    @Override // org.thoughtcrime.securesms.mediasend.MediaSendPageFragment
    public void restoreState(Object obj) {
        if (obj instanceof Data) {
            EditorModel readModel = ((Data) obj).readModel();
            if (readModel != null) {
                ImageEditorView imageEditorView = this.imageEditorView;
                if (imageEditorView != null) {
                    imageEditorView.setModel(readModel);
                    onDrawingChanged(false, false);
                    return;
                }
                this.restoredModel = readModel;
                return;
            }
            return;
        }
        String str = TAG;
        Log.w(str, "Received a bad saved state. Received class: " + obj.getClass().getName());
    }

    private void changeEntityColor(int i) {
        EditorElement editorElement = this.currentSelection;
        if (editorElement != null) {
            Renderer renderer = editorElement.getRenderer();
            if (renderer instanceof ColorableRenderer) {
                ((ColorableRenderer) renderer).setColor(i);
                onDrawingChanged(false, true);
            }
        }
    }

    public void startTextEntityEditing(EditorElement editorElement, boolean z) {
        this.imageEditorView.startTextEditing(editorElement);
        TextEntryDialogFragment.Companion.show(getChildFragmentManager(), editorElement, TextSecurePreferences.isIncognitoKeyboardEnabled(requireContext()), z, this.imageEditorHud.getColorIndex());
    }

    @Override // org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment.Controller
    public void zoomToFitText(EditorElement editorElement, MultiLineTextRenderer multiLineTextRenderer) {
        this.imageEditorView.zoomToFitText(editorElement, multiLineTextRenderer);
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onTextStyleToggle() {
        EditorElement editorElement = this.currentSelection;
        if (editorElement != null && (editorElement.getRenderer() instanceof MultiLineTextRenderer)) {
            ((MultiLineTextRenderer) this.currentSelection.getRenderer()).nextMode();
        }
    }

    @Override // org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment.Controller
    public void onTextEntryDialogDismissed(boolean z) {
        this.imageEditorView.doneTextEditing();
        if (z) {
            this.imageEditorHud.setMode(ImageEditorHudV2.Mode.MOVE_TEXT);
            return;
        }
        onUndo();
        this.imageEditorHud.setMode(ImageEditorHudV2.Mode.DRAW);
    }

    protected void addText() {
        EditorElement editorElement = new EditorElement(new MultiLineTextRenderer("", this.imageEditorHud.getActiveColor(), MultiLineTextRenderer.Mode.REGULAR), 2);
        this.imageEditorView.getModel().addElementCentered(editorElement, 1.0f);
        this.imageEditorView.invalidate();
        setCurrentSelection(editorElement);
        startTextEntityEditing(editorElement, true);
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && i == 124 && intent != null) {
            Uri data = intent.getData();
            if (data != null) {
                EditorElement editorElement = new EditorElement(new UriGlideRenderer(data, true, this.imageMaxWidth, this.imageMaxHeight), 0);
                this.imageEditorView.getModel().addElementCentered(editorElement, 0.4f);
                setCurrentSelection(editorElement);
                this.hasMadeAnEditThisSession = true;
                this.imageEditorHud.setMode(ImageEditorHudV2.Mode.MOVE_STICKER);
                return;
            }
            return;
        }
        this.imageEditorHud.setMode(ImageEditorHudV2.Mode.DRAW);
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onModeStarted(ImageEditorHudV2.Mode mode, ImageEditorHudV2.Mode mode2) {
        this.onBackPressedCallback.setEnabled(shouldHandleOnBackPressed(mode));
        this.imageEditorView.setMode(ImageEditorView.Mode.MoveAndResize);
        this.imageEditorView.doneTextEditing();
        boolean z = true;
        this.controller.onTouchEventsNeeded(mode != ImageEditorHudV2.Mode.NONE);
        updateViewPortScaling(mode, mode2, getResources().getConfiguration().orientation, false);
        if (mode != ImageEditorHudV2.Mode.CROP) {
            this.imageEditorView.getModel().doneCrop();
        }
        EditorFlags flags = this.imageEditorView.getModel().getTrash().getFlags();
        if (mode != ImageEditorHudV2.Mode.DELETE) {
            z = false;
        }
        flags.setVisible(z).persist();
        updateHudDialRotation();
        int i = AnonymousClass5.$SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[mode.ordinal()];
        if (i != 9) {
            switch (i) {
                case 1:
                    this.imageEditorView.getModel().startCrop();
                    return;
                case 2:
                case 3:
                    onBrushWidthChange();
                    return;
                case 4:
                    onBrushWidthChange();
                    this.imageEditorHud.setBlurFacesToggleEnabled(this.imageEditorView.getModel().hasFaceRenderer());
                    return;
                case 5:
                    addText();
                    return;
                case 6:
                    startActivityForResult(new Intent(getContext(), ImageEditorStickerSelectActivity.class), 124);
                    return;
                default:
                    return;
            }
        } else {
            setCurrentSelection(null);
            this.hasMadeAnEditThisSession = false;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$5 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass5 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorFragment$Mode;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode;

        static {
            int[] iArr = new int[ImageEditorHudV2.Mode.values().length];
            $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode = iArr;
            try {
                iArr[ImageEditorHudV2.Mode.CROP.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[ImageEditorHudV2.Mode.DRAW.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[ImageEditorHudV2.Mode.HIGHLIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[ImageEditorHudV2.Mode.BLUR.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[ImageEditorHudV2.Mode.TEXT.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[ImageEditorHudV2.Mode.INSERT_STICKER.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[ImageEditorHudV2.Mode.MOVE_STICKER.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[ImageEditorHudV2.Mode.MOVE_TEXT.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[ImageEditorHudV2.Mode.NONE.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[ImageEditorHudV2.Mode.DELETE.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            int[] iArr2 = new int[Mode.values().length];
            $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorFragment$Mode = iArr2;
            try {
                iArr2[Mode.AVATAR_EDIT.ordinal()] = 1;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorFragment$Mode[Mode.AVATAR_CAPTURE.ordinal()] = 2;
            } catch (NoSuchFieldError unused12) {
            }
        }
    }

    private void updateViewPortScaling(ImageEditorHudV2.Mode mode, ImageEditorHudV2.Mode mode2, int i, boolean z) {
        boolean shouldScaleViewPort = shouldScaleViewPort(mode);
        boolean shouldScaleViewPort2 = shouldScaleViewPort(mode2);
        int hudProtection = getHudProtection(mode);
        if (shouldScaleViewPort == shouldScaleViewPort2 && !z) {
            return;
        }
        if (shouldScaleViewPort) {
            scaleViewPortForDrawing(i, hudProtection);
        } else {
            restoreViewPortScaling(i);
        }
    }

    private int getHudProtection(ImageEditorHudV2.Mode mode) {
        if (mode == ImageEditorHudV2.Mode.CROP) {
            return CROP_HUD_PROTECTION;
        }
        return DRAW_HUD_PROTECTION;
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onColorChange(int i) {
        this.imageEditorView.setDrawingBrushColor(i);
        changeEntityColor(i);
    }

    @Override // org.thoughtcrime.securesms.scribbles.TextEntryDialogFragment.Controller
    public void onTextColorChange(int i) {
        this.imageEditorHud.setColorIndex(i);
        onColorChange(this.imageEditorHud.getActiveColor());
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onBrushWidthChange() {
        ImageEditorHudV2.Mode mode = this.imageEditorHud.getMode();
        this.imageEditorView.startDrawing(this.imageEditorHud.getActiveBrushWidth(), mode == ImageEditorHudV2.Mode.HIGHLIGHT ? Paint.Cap.SQUARE : Paint.Cap.ROUND, mode == ImageEditorHudV2.Mode.BLUR);
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onBlurFacesToggled(boolean z) {
        EditorModel model = this.imageEditorView.getModel();
        EditorElement mainImage = model.getMainImage();
        if (mainImage == null) {
            this.imageEditorHud.hideBlurToast();
        } else if (!z) {
            model.clearFaceRenderers();
            this.imageEditorHud.hideBlurToast();
        } else {
            Matrix inverseCropPosition = model.getInverseCropPosition();
            Pair<Uri, FaceDetectionResult> pair = this.cachedFaceDetection;
            if (pair != null) {
                if (!pair.first().equals(getUri()) || !this.cachedFaceDetection.second().position.equals(inverseCropPosition)) {
                    this.cachedFaceDetection = null;
                } else {
                    renderFaceBlurs(this.cachedFaceDetection.second());
                    this.imageEditorHud.showBlurToast();
                    return;
                }
            }
            AlertDialog show = SimpleProgressDialog.show(requireContext());
            mainImage.getFlags().setChildrenVisible(false);
            SimpleTask.run(getLifecycle(), new SimpleTask.BackgroundTask(model, inverseCropPosition) { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda5
                public final /* synthetic */ EditorModel f$1;
                public final /* synthetic */ Matrix f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return ImageEditorFragment.lambda$onBlurFacesToggled$1(EditorElement.this, this.f$1, this.f$2);
                }
            }, new SimpleTask.ForegroundTask(mainImage, show) { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda6
                public final /* synthetic */ EditorElement f$1;
                public final /* synthetic */ AlertDialog f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    ImageEditorFragment.this.lambda$onBlurFacesToggled$2(this.f$1, this.f$2, (ImageEditorFragment.FaceDetectionResult) obj);
                }
            });
        }
    }

    public static /* synthetic */ FaceDetectionResult lambda$onBlurFacesToggled$1(EditorElement editorElement, EditorModel editorModel, Matrix matrix) {
        if (editorElement.getRenderer() == null || ((UriGlideRenderer) editorElement.getRenderer()).getBitmap() == null) {
            return new FaceDetectionResult(Collections.emptyList(), new Point(0, 0), new Matrix());
        }
        AndroidFaceDetector androidFaceDetector = new AndroidFaceDetector();
        Bitmap render = editorModel.render(ApplicationDependencies.getApplication(), editorModel.getOutputSizeMaxWidth(MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH), new FontTypefaceProvider());
        try {
            return new FaceDetectionResult(androidFaceDetector.detect(render), new Point(render.getWidth(), render.getHeight()), matrix);
        } finally {
            render.recycle();
            editorElement.getFlags().reset();
        }
    }

    public /* synthetic */ void lambda$onBlurFacesToggled$2(EditorElement editorElement, AlertDialog alertDialog, FaceDetectionResult faceDetectionResult) {
        editorElement.getFlags().reset();
        renderFaceBlurs(faceDetectionResult);
        alertDialog.dismiss();
        this.imageEditorHud.showBlurToast();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onClearAll() {
        this.imageEditorView.getModel().clearUndoStack();
        updateHudDialRotation();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onCancel() {
        if (this.hasMadeAnEditThisSession) {
            new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.MediaReviewImagePageFragment__discard_changes).setMessage(R.string.MediaReviewImagePageFragment__youll_lose_any_changes).setPositiveButton(R.string.MediaReviewImagePageFragment__discard, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda2
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    ImageEditorFragment.this.lambda$onCancel$3(dialogInterface, i);
                }
            }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda3
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();
            return;
        }
        this.imageEditorHud.setMode(ImageEditorHudV2.Mode.NONE);
        this.controller.onCancelEditing();
    }

    public /* synthetic */ void lambda$onCancel$3(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        this.imageEditorHud.setMode(ImageEditorHudV2.Mode.NONE);
        this.controller.onCancelEditing();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onUndo() {
        this.imageEditorView.getModel().undo();
        this.imageEditorHud.setBlurFacesToggleEnabled(this.imageEditorView.getModel().hasFaceRenderer());
        updateHudDialRotation();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onDelete() {
        this.imageEditorView.deleteElement(this.currentSelection);
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onSave() {
        SaveAttachmentTask.showWarningDialog(requireContext(), new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda4
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ImageEditorFragment.this.lambda$onSave$6(dialogInterface, i);
            }
        });
    }

    public /* synthetic */ void lambda$onSave$6(DialogInterface dialogInterface, int i) {
        if (StorageUtil.canWriteToMediaStore()) {
            performSaveToDisk();
        } else {
            Permissions.with(this).request("android.permission.WRITE_EXTERNAL_STORAGE").ifNecessary().withPermanentDenialDialog(getString(R.string.MediaPreviewActivity_signal_needs_the_storage_permission_in_order_to_write_to_external_storage_but_it_has_been_permanently_denied)).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda9
                @Override // java.lang.Runnable
                public final void run() {
                    ImageEditorFragment.this.lambda$onSave$5();
                }
            }).onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda10
                @Override // java.lang.Runnable
                public final void run() {
                    ImageEditorFragment.this.performSaveToDisk();
                }
            }).execute();
        }
    }

    public /* synthetic */ void lambda$onSave$5() {
        Toast.makeText(requireContext(), (int) R.string.MediaPreviewActivity_unable_to_write_to_external_storage_without_permission, 1).show();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onFlipHorizontal() {
        this.imageEditorView.getModel().flipHorizontal();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onRotate90AntiClockwise() {
        this.imageEditorView.getModel().rotate90anticlockwise();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onCropAspectLock() {
        this.imageEditorView.getModel().setCropAspectLock(!this.imageEditorView.getModel().isCropAspectLocked());
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public boolean isCropAspectLocked() {
        return this.imageEditorView.getModel().isCropAspectLocked();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onRequestFullScreen(boolean z, boolean z2) {
        this.controller.onRequestFullScreen(z, z2);
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onDone() {
        this.controller.onDoneEditing();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onDialRotationGestureStarted() {
        this.minDialScaleDown = this.initialDialScale / this.imageEditorView.getModel().getMainImage().getLocalScaleX();
        this.imageEditorView.getModel().pushUndoPoint();
        this.imageEditorView.getModel().updateUndoRedoAvailabilityState();
        this.initialDialImageDegrees = (float) Math.toDegrees((double) this.imageEditorView.getModel().getMainImage().getLocalRotationAngle());
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onDialRotationGestureFinished() {
        this.imageEditorView.getModel().getMainImage().commitEditorMatrix();
        this.imageEditorView.getModel().postEdit(true);
        this.imageEditorView.invalidate();
    }

    @Override // org.thoughtcrime.securesms.scribbles.ImageEditorHudV2.EventListener
    public void onDialRotationChanged(float f) {
        this.imageEditorView.setMainImageEditorMatrixRotation(f - this.initialDialImageDegrees, this.minDialScaleDown);
    }

    public void updateHudDialRotation() {
        this.imageEditorHud.setDialRotation(getRotationDegreesRounded(this.imageEditorView.getModel().getMainImage()));
        this.initialDialScale = this.imageEditorView.getModel().getMainImage().getLocalScaleX();
    }

    private void scaleViewPortForDrawing(int i, int i2) {
        ResizeAnimation resizeAnimation = this.resizeAnimation;
        if (resizeAnimation != null) {
            resizeAnimation.cancel();
        }
        float aspectRatioForOrientation = getAspectRatioForOrientation(i);
        int widthForOrientation = getWidthForOrientation(i) - ViewUtil.dpToPx(32);
        int i3 = (int) ((1.0f / aspectRatioForOrientation) * ((float) widthForOrientation));
        int heightForOrientation = getHeightForOrientation(i) - i2;
        if (i3 > heightForOrientation) {
            widthForOrientation = Math.round(((float) heightForOrientation) * aspectRatioForOrientation);
            i3 = heightForOrientation;
        }
        ResizeAnimation resizeAnimation2 = new ResizeAnimation(this.imageEditorView, widthForOrientation, i3);
        this.resizeAnimation = resizeAnimation2;
        resizeAnimation2.setDuration(250);
        this.resizeAnimation.setInterpolator(MediaAnimations.getInterpolator());
        this.imageEditorView.startAnimation(this.resizeAnimation);
    }

    private void restoreViewPortScaling(int i) {
        ResizeAnimation resizeAnimation = this.resizeAnimation;
        if (resizeAnimation != null) {
            resizeAnimation.cancel();
        }
        int heightForOrientation = getHeightForOrientation(i);
        float aspectRatioForOrientation = getAspectRatioForOrientation(i);
        int widthForOrientation = getWidthForOrientation(i);
        int i2 = (int) ((1.0f / aspectRatioForOrientation) * ((float) widthForOrientation));
        if (i2 > heightForOrientation) {
            widthForOrientation = Math.round(((float) heightForOrientation) * aspectRatioForOrientation);
        } else {
            heightForOrientation = i2;
        }
        ResizeAnimation resizeAnimation2 = new ResizeAnimation(this.imageEditorView, widthForOrientation, heightForOrientation);
        this.resizeAnimation = resizeAnimation2;
        resizeAnimation2.setDuration(250);
        this.resizeAnimation.setInterpolator(MediaAnimations.getInterpolator());
        this.imageEditorView.startAnimation(this.resizeAnimation);
    }

    private int getHeightForOrientation(int i) {
        if (i == 1) {
            return Math.max(getResources().getDisplayMetrics().heightPixels, getResources().getDisplayMetrics().widthPixels);
        }
        return Math.min(getResources().getDisplayMetrics().heightPixels, getResources().getDisplayMetrics().widthPixels);
    }

    private int getWidthForOrientation(int i) {
        if (i == 1) {
            return Math.min(getResources().getDisplayMetrics().heightPixels, getResources().getDisplayMetrics().widthPixels);
        }
        return Math.max(getResources().getDisplayMetrics().heightPixels, getResources().getDisplayMetrics().widthPixels);
    }

    private static boolean shouldScaleViewPort(ImageEditorHudV2.Mode mode) {
        return mode != ImageEditorHudV2.Mode.NONE;
    }

    public void performSaveToDisk() {
        SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda7
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ImageEditorFragment.this.renderToSingleUseBlob();
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.scribbles.ImageEditorFragment$$ExternalSyntheticLambda8
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ImageEditorFragment.this.lambda$performSaveToDisk$7((Uri) obj);
            }
        });
    }

    public /* synthetic */ void lambda$performSaveToDisk$7(Uri uri) {
        new SaveAttachmentTask(requireContext()).executeOnExecutor(SignalExecutors.BOUNDED, new SaveAttachmentTask.Attachment(uri, MediaUtil.IMAGE_JPEG, System.currentTimeMillis(), null));
    }

    public Uri renderToSingleUseBlob() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Bitmap render = this.imageEditorView.getModel().render(requireContext(), new FontTypefaceProvider());
        render.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        render.recycle();
        return BlobProvider.getInstance().forData(byteArrayOutputStream.toByteArray()).withMimeType(MediaUtil.IMAGE_JPEG).createForSingleUseInMemory();
    }

    private void onDrawingChanged(boolean z, boolean z2) {
        if (z2) {
            this.hasMadeAnEditThisSession = true;
        }
    }

    public void onUndoRedoAvailabilityChanged(boolean z, boolean z2) {
        this.imageEditorHud.setUndoAvailability(z);
    }

    private void renderFaceBlurs(FaceDetectionResult faceDetectionResult) {
        List<FaceDetector.Face> list = faceDetectionResult.faces;
        if (list.isEmpty()) {
            this.cachedFaceDetection = null;
            return;
        }
        this.imageEditorView.getModel().pushUndoPoint();
        Matrix matrix = new Matrix();
        for (FaceDetector.Face face : list) {
            EditorElement editorElement = new EditorElement(new FaceBlurRenderer(), -1);
            Matrix localMatrix = editorElement.getLocalMatrix();
            matrix.setRectToRect(Bounds.FULL_BOUNDS, face.getBounds(), Matrix.ScaleToFit.FILL);
            localMatrix.set(faceDetectionResult.position);
            localMatrix.preConcat(matrix);
            editorElement.getFlags().setEditable(false).setSelectable(false).persist();
            this.imageEditorView.getModel().addElementWithoutPushUndo(editorElement);
        }
        this.imageEditorView.invalidate();
        this.cachedFaceDetection = new Pair<>(getUri(), faceDetectionResult);
    }

    private boolean shouldHandleOnBackPressed(ImageEditorHudV2.Mode mode) {
        return mode == ImageEditorHudV2.Mode.CROP || mode == ImageEditorHudV2.Mode.DRAW || mode == ImageEditorHudV2.Mode.HIGHLIGHT || mode == ImageEditorHudV2.Mode.BLUR || mode == ImageEditorHudV2.Mode.TEXT || mode == ImageEditorHudV2.Mode.MOVE_STICKER || mode == ImageEditorHudV2.Mode.MOVE_TEXT || mode == ImageEditorHudV2.Mode.INSERT_STICKER;
    }

    public void onPopEditorMode() {
        setCurrentSelection(null);
        switch (AnonymousClass5.$SwitchMap$org$thoughtcrime$securesms$scribbles$ImageEditorHudV2$Mode[this.imageEditorHud.getMode().ordinal()]) {
            case 1:
                onCancel();
                return;
            case 2:
            case 3:
            case 4:
                if (Mode.getByCode(requireArguments().getString(KEY_MODE)) == Mode.NORMAL) {
                    onCancel();
                    return;
                }
                this.controller.onTouchEventsNeeded(true);
                this.imageEditorHud.setMode(ImageEditorHudV2.Mode.CROP);
                return;
            case 5:
            case 6:
            case 7:
            case 8:
            case 10:
                this.controller.onTouchEventsNeeded(true);
                this.imageEditorHud.setMode(ImageEditorHudV2.Mode.DRAW);
                return;
            case 9:
            default:
                return;
        }
    }

    public float getRotationDegreesRounded(EditorElement editorElement) {
        if (editorElement == null) {
            return 0.0f;
        }
        return (float) Math.round(Math.toDegrees((double) editorElement.getLocalRotationAngle()));
    }

    public void setCurrentSelection(EditorElement editorElement) {
        setSelectionState(this.currentSelection, false);
        this.currentSelection = editorElement;
        setSelectionState(editorElement, true);
        this.imageEditorView.invalidate();
    }

    private void setSelectionState(EditorElement editorElement, boolean z) {
        if (editorElement != null && (editorElement.getRenderer() instanceof SelectableRenderer)) {
            ((SelectableRenderer) editorElement.getRenderer()).onSelected(z);
        }
        EditorModel model = this.imageEditorView.getModel();
        if (!z) {
            editorElement = null;
        }
        model.setSelected(editorElement);
    }

    /* loaded from: classes4.dex */
    public static class FaceDetectionResult {
        private final List<FaceDetector.Face> faces;
        private final Matrix position;

        private FaceDetectionResult(List<FaceDetector.Face> list, Point point, Matrix matrix) {
            this.faces = list;
            Matrix matrix2 = new Matrix(matrix);
            this.position = matrix2;
            Matrix matrix3 = new Matrix();
            matrix3.setRectToRect(new RectF(0.0f, 0.0f, (float) point.x, (float) point.y), Bounds.FULL_BOUNDS, Matrix.ScaleToFit.FILL);
            matrix2.preConcat(matrix3);
        }
    }

    /* loaded from: classes4.dex */
    public enum Mode {
        NORMAL("normal"),
        AVATAR_CAPTURE("avatar_capture"),
        AVATAR_EDIT("avatar_edit");
        
        private final String code;

        Mode(String str) {
            this.code = str;
        }

        String getCode() {
            return this.code;
        }

        static Mode getByCode(String str) {
            if (str == null) {
                return NORMAL;
            }
            Mode[] values = values();
            for (Mode mode : values) {
                if (str.equals(mode.code)) {
                    return mode;
                }
            }
            return NORMAL;
        }
    }
}
