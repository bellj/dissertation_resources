package org.thoughtcrime.securesms.scribbles;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.media.FaceDetector;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.List;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.scribbles.FaceDetector;

/* loaded from: classes4.dex */
public final class AndroidFaceDetector implements FaceDetector {
    private static final int MAX_FACES;
    private static final String TAG = Log.tag(AndroidFaceDetector.class);

    @Override // org.thoughtcrime.securesms.scribbles.FaceDetector
    public List<FaceDetector.Face> detect(Bitmap bitmap) {
        long currentTimeMillis = System.currentTimeMillis();
        String str = TAG;
        Locale locale = Locale.US;
        Log.d(str, String.format(locale, "Bitmap format is %dx%d %s", Integer.valueOf(bitmap.getWidth()), Integer.valueOf(bitmap.getHeight()), bitmap.getConfig()));
        boolean z = (bitmap.getConfig() == Bitmap.Config.RGB_565 && bitmap.getWidth() % 2 == 0) ? false : true;
        if (z) {
            Log.d(str, "Changing colour format to 565, with even width");
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth() & -2, bitmap.getHeight(), Bitmap.Config.RGB_565);
            new Canvas(createBitmap).drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
            bitmap = createBitmap;
        }
        try {
            FaceDetector.Face[] faceArr = new FaceDetector.Face[20];
            int findFaces = new android.media.FaceDetector(bitmap.getWidth(), bitmap.getHeight(), 20).findFaces(bitmap, faceArr);
            Log.d(str, String.format(locale, "Found %d faces", Integer.valueOf(findFaces)));
            List<FaceDetector.Face> list = Stream.of(faceArr).limit((long) findFaces).map(new Function() { // from class: org.thoughtcrime.securesms.scribbles.AndroidFaceDetector$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return AndroidFaceDetector.$r8$lambda$5pZ9AEpmkFes15vq5fcxV_J68es((FaceDetector.Face) obj);
                }
            }).toList();
            if (z) {
                bitmap.recycle();
            }
            Log.d(str, "Finished in " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
            return list;
        } catch (Throwable th) {
            if (z) {
                bitmap.recycle();
            }
            String str2 = TAG;
            Log.d(str2, "Finished in " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
            throw th;
        }
    }

    public static FaceDetector.Face faceToFace(FaceDetector.Face face) {
        PointF pointF = new PointF();
        face.getMidPoint(pointF);
        float eyesDistance = face.eyesDistance() * 1.4f;
        float eyesDistance2 = face.eyesDistance() * 0.4f;
        float f = pointF.x;
        float f2 = pointF.y;
        return new DefaultFace(new RectF(f - eyesDistance, (f2 - eyesDistance) + eyesDistance2, f + eyesDistance, f2 + eyesDistance + eyesDistance2), face.confidence());
    }

    /* loaded from: classes4.dex */
    public static class DefaultFace implements FaceDetector.Face {
        private final RectF bounds;
        private final float certainty;

        public DefaultFace(RectF rectF, float f) {
            this.bounds = rectF;
            this.certainty = f;
        }

        @Override // org.thoughtcrime.securesms.scribbles.FaceDetector.Face
        public RectF getBounds() {
            return this.bounds;
        }

        @Override // org.thoughtcrime.securesms.scribbles.FaceDetector.Face
        public Class<? extends FaceDetector> getDetectorClass() {
            return AndroidFaceDetector.class;
        }

        @Override // org.thoughtcrime.securesms.scribbles.FaceDetector.Face
        public float getConfidence() {
            return this.certainty;
        }
    }
}
