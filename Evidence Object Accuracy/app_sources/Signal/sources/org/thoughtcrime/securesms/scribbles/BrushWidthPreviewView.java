package org.thoughtcrime.securesms.scribbles;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.imageeditor.core.Bounds;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.mediasend.v2.MediaAnimations;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: BrushWidthPreviewView.kt */
@Metadata(d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0006\u0010\u0014\u001a\u00020\u0015J\u0010\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0014J\u000e\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u001a\u001a\u00020\u000fJ\u0010\u0010\u001b\u001a\u00020\u00152\b\b\u0001\u0010\u001c\u001a\u00020\u0007J\u000e\u0010\u001d\u001a\u00020\u00152\u0006\u0010\u001e\u001a\u00020\u0011J\u0006\u0010\u001f\u001a\u00020\u0015R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\u0002\n\u0000¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/scribbles/BrushWidthPreviewView;", "Landroid/view/View;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "animator", "Landroid/animation/Animator;", "backdropPaint", "Landroid/graphics/Paint;", "brushPaint", "isBlur", "", "radius", "", "tmpRect", "Landroid/graphics/Rect;", "hide", "", "onDraw", "canvas", "Landroid/graphics/Canvas;", "setBlur", "blur", "setColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "setThickness", "thickness", "show", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BrushWidthPreviewView extends View {
    private Animator animator;
    private final Paint backdropPaint;
    private final Paint brushPaint;
    private boolean isBlur;
    private float radius;
    private final Rect tmpRect;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public BrushWidthPreviewView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public BrushWidthPreviewView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ BrushWidthPreviewView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BrushWidthPreviewView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        this.tmpRect = new Rect();
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-1);
        this.backdropPaint = paint;
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setStyle(Paint.Style.FILL);
        paint2.setColor(-1);
        this.brushPaint = paint2;
        this.radius = (Bounds.FULL_BOUNDS.width() * 0.03f) / 2.0f;
    }

    public final void setBlur(boolean z) {
        this.isBlur = z;
        invalidate();
    }

    public final void setColor(int i) {
        this.brushPaint.setColor(i);
        invalidate();
    }

    public final void setThickness(float f) {
        this.radius = (f * Bounds.FULL_BOUNDS.width()) / 2.0f;
        invalidate();
    }

    public final void show() {
        ViewExtensionsKt.setVisible(this, true);
        Animator animator = this.animator;
        if (animator != null) {
            animator.cancel();
        }
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "alpha", 1.0f);
        ofFloat.setInterpolator(MediaAnimations.getInterpolator());
        ofFloat.setDuration(150L);
        ofFloat.start();
        this.animator = ofFloat;
    }

    public final void hide() {
        Animator animator = this.animator;
        if (animator != null) {
            animator.cancel();
        }
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "alpha", 0.0f);
        ofFloat.setInterpolator(MediaAnimations.getInterpolator());
        ofFloat.setDuration(150L);
        Intrinsics.checkNotNullExpressionValue(ofFloat, "");
        ofFloat.addListener(new Animator.AnimatorListener(this) { // from class: org.thoughtcrime.securesms.scribbles.BrushWidthPreviewView$hide$lambda-4$$inlined$doOnEnd$1
            final /* synthetic */ BrushWidthPreviewView this$0;

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator2) {
                Intrinsics.checkNotNullParameter(animator2, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator2) {
                Intrinsics.checkNotNullParameter(animator2, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator2) {
                Intrinsics.checkNotNullParameter(animator2, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator2) {
                Intrinsics.checkNotNullParameter(animator2, "animator");
                ViewExtensionsKt.setVisible(this.this$0, false);
            }

            {
                this.this$0 = r1;
            }
        });
        ofFloat.start();
        this.animator = ofFloat;
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        canvas.getClipBounds(this.tmpRect);
        canvas.drawCircle(this.tmpRect.exactCenterX(), this.tmpRect.exactCenterY(), this.radius + ((float) ViewUtil.dpToPx(1)), this.backdropPaint);
        if (!this.isBlur) {
            canvas.drawCircle(this.tmpRect.exactCenterX(), this.tmpRect.exactCenterY(), this.radius, this.brushPaint);
        }
    }
}
