package org.thoughtcrime.securesms.scribbles;

import org.signal.imageeditor.core.model.EditorElement;
import org.thoughtcrime.securesms.scribbles.ImageEditorFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ImageEditorFragment$2$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ImageEditorFragment.AnonymousClass2 f$0;
    public final /* synthetic */ EditorElement f$1;

    public /* synthetic */ ImageEditorFragment$2$$ExternalSyntheticLambda0(ImageEditorFragment.AnonymousClass2 r1, EditorElement editorElement) {
        this.f$0 = r1;
        this.f$1 = editorElement;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onDragMoved$0(this.f$1);
    }
}
