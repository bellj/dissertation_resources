package org.thoughtcrime.securesms.scribbles;

import android.graphics.Bitmap;
import android.graphics.RectF;
import java.util.List;

/* loaded from: classes4.dex */
public interface FaceDetector {

    /* loaded from: classes4.dex */
    public interface Face {
        RectF getBounds();

        float getConfidence();

        Class<? extends FaceDetector> getDetectorClass();
    }

    List<Face> detect(Bitmap bitmap);
}
