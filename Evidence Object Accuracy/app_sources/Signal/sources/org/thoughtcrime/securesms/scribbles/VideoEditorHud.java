package org.thoughtcrime.securesms.scribbles;

import android.animation.Animator;
import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.media.DecryptableUriMediaInput;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.video.VideoBitRateCalculator;
import org.thoughtcrime.securesms.video.VideoUtil;
import org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsRangeSelectorView;

/* loaded from: classes4.dex */
public final class VideoEditorHud extends LinearLayout {
    private static final String TAG = Log.tag(VideoEditorHud.class);
    private EventListener eventListener;
    private View playOverlay;
    private VideoThumbnailsRangeSelectorView videoTimeLine;

    /* loaded from: classes4.dex */
    public interface EventListener {
        void onEditVideoDuration(long j, long j2, long j3, boolean z, boolean z2);

        void onPlay();

        void onSeek(long j, boolean z);
    }

    public VideoEditorHud(Context context) {
        super(context);
        initialize();
    }

    public VideoEditorHud(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    public VideoEditorHud(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize();
    }

    private void initialize() {
        View inflate = LinearLayout.inflate(getContext(), R.layout.video_editor_hud, this);
        setOrientation(1);
        this.videoTimeLine = (VideoThumbnailsRangeSelectorView) inflate.findViewById(R.id.video_timeline);
        View findViewById = inflate.findViewById(R.id.play_overlay);
        this.playOverlay = findViewById;
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.scribbles.VideoEditorHud$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                VideoEditorHud.this.lambda$initialize$0(view);
            }
        });
    }

    public /* synthetic */ void lambda$initialize$0(View view) {
        this.eventListener.onPlay();
    }

    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }

    public void setVideoSource(VideoSlide videoSlide, final VideoBitRateCalculator videoBitRateCalculator, long j) throws IOException {
        Uri uri = videoSlide.getUri();
        if (uri != null && videoSlide.hasVideo()) {
            this.videoTimeLine.setInput(DecryptableUriMediaInput.createForUri(getContext(), uri));
            final long tryGetUriSize = tryGetUriSize(getContext(), uri, Long.MAX_VALUE);
            if (tryGetUriSize > j) {
                this.videoTimeLine.setTimeLimit(VideoUtil.getMaxVideoUploadDurationInSeconds(), TimeUnit.SECONDS);
            }
            this.videoTimeLine.setOnRangeChangeListener(new VideoThumbnailsRangeSelectorView.OnRangeChangeListener() { // from class: org.thoughtcrime.securesms.scribbles.VideoEditorHud.1
                @Override // org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsRangeSelectorView.OnRangeChangeListener
                public void onPositionDrag(long j2) {
                    if (VideoEditorHud.this.eventListener != null) {
                        VideoEditorHud.this.eventListener.onSeek(j2, false);
                    }
                }

                @Override // org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsRangeSelectorView.OnRangeChangeListener
                public void onEndPositionDrag(long j2) {
                    if (VideoEditorHud.this.eventListener != null) {
                        VideoEditorHud.this.eventListener.onSeek(j2, true);
                    }
                }

                @Override // org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsRangeSelectorView.OnRangeChangeListener
                public void onRangeDrag(long j2, long j3, long j4, VideoThumbnailsRangeSelectorView.Thumb thumb) {
                    if (VideoEditorHud.this.eventListener != null) {
                        VideoEditorHud.this.eventListener.onEditVideoDuration(j4, j2, j3, thumb == VideoThumbnailsRangeSelectorView.Thumb.MIN, false);
                    }
                }

                @Override // org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsRangeSelectorView.OnRangeChangeListener
                public void onRangeDragEnd(long j2, long j3, long j4, VideoThumbnailsRangeSelectorView.Thumb thumb) {
                    if (VideoEditorHud.this.eventListener != null) {
                        VideoEditorHud.this.eventListener.onEditVideoDuration(j4, j2, j3, thumb == VideoThumbnailsRangeSelectorView.Thumb.MIN, true);
                    }
                }

                @Override // org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsRangeSelectorView.OnRangeChangeListener
                public VideoThumbnailsRangeSelectorView.Quality getQuality(long j2, long j3) {
                    long j4 = tryGetUriSize;
                    TimeUnit timeUnit = TimeUnit.MICROSECONDS;
                    VideoBitRateCalculator.Quality targetQuality = videoBitRateCalculator.getTargetQuality(timeUnit.toMillis(j2), VideoBitRateCalculator.bitRate(j4, timeUnit.toMillis(j3)));
                    return new VideoThumbnailsRangeSelectorView.Quality(targetQuality.getFileSizeEstimate(), (int) (targetQuality.getQuality() * 100.0d));
                }
            });
        }
    }

    public void showPlayButton() {
        this.playOverlay.setVisibility(0);
        this.playOverlay.animate().setListener(null).alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setInterpolator(new OvershootInterpolator()).start();
    }

    public void fadePlayButton() {
        this.playOverlay.animate().setListener(new Animator.AnimatorListener() { // from class: org.thoughtcrime.securesms.scribbles.VideoEditorHud.2
            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator) {
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                VideoEditorHud.this.playOverlay.setVisibility(8);
            }
        }).alpha(0.0f).scaleX(0.8f).scaleY(0.8f).start();
    }

    public void hidePlayButton() {
        this.playOverlay.setVisibility(8);
        this.playOverlay.setAlpha(0.0f);
        this.playOverlay.setScaleX(0.8f);
        this.playOverlay.setScaleY(0.8f);
    }

    public void setDurationRange(long j, long j2, long j3) {
        this.videoTimeLine.setRange(j2, j3);
    }

    public void setPosition(long j) {
        this.videoTimeLine.setActualPosition(j);
    }

    private long tryGetUriSize(Context context, Uri uri, long j) {
        try {
            return getSize(context, uri);
        } catch (IOException e) {
            Log.w(TAG, e);
            return j;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long getSize(android.content.Context r7, android.net.Uri r8) throws java.io.IOException {
        /*
            java.lang.String r0 = "_size"
            android.content.ContentResolver r1 = r7.getContentResolver()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r2 = r8
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6)
            r2 = 0
            if (r1 == 0) goto L_0x0032
            boolean r4 = r1.moveToFirst()     // Catch: all -> 0x0028
            if (r4 == 0) goto L_0x0032
            int r4 = r1.getColumnIndex(r0)     // Catch: all -> 0x0028
            if (r4 < 0) goto L_0x0032
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0028
            long r4 = r1.getLong(r0)     // Catch: all -> 0x0028
            goto L_0x0033
        L_0x0028:
            r7 = move-exception
            r1.close()     // Catch: all -> 0x002d
            goto L_0x0031
        L_0x002d:
            r8 = move-exception
            r7.addSuppressed(r8)
        L_0x0031:
            throw r7
        L_0x0032:
            r4 = r2
        L_0x0033:
            if (r1 == 0) goto L_0x0038
            r1.close()
        L_0x0038:
            int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x0040
            long r4 = org.thoughtcrime.securesms.util.MediaUtil.getMediaSize(r7, r8)
        L_0x0040:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.scribbles.VideoEditorHud.getSize(android.content.Context, android.net.Uri):long");
    }
}
