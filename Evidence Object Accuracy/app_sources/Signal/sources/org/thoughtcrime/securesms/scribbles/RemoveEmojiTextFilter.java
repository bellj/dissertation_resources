package org.thoughtcrime.securesms.scribbles;

import org.signal.imageeditor.core.HiddenEditText;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;

/* loaded from: classes4.dex */
class RemoveEmojiTextFilter implements HiddenEditText.TextFilter {
    @Override // org.signal.imageeditor.core.HiddenEditText.TextFilter
    public String filter(String str) {
        return EmojiUtil.stripEmoji(str);
    }
}
