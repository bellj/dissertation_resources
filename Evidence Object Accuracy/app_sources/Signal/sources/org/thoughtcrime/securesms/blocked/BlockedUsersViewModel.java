package org.thoughtcrime.securesms.blocked;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* loaded from: classes3.dex */
public class BlockedUsersViewModel extends ViewModel {
    private final SingleLiveEvent<Event> events;
    private final MutableLiveData<List<Recipient>> recipients;
    private final BlockedUsersRepository repository;

    /* loaded from: classes3.dex */
    public enum EventType {
        BLOCK_SUCCEEDED,
        BLOCK_FAILED,
        UNBLOCK_SUCCEEDED
    }

    private BlockedUsersViewModel(BlockedUsersRepository blockedUsersRepository) {
        this.events = new SingleLiveEvent<>();
        this.repository = blockedUsersRepository;
        this.recipients = new MutableLiveData<>();
        loadRecipients();
    }

    public LiveData<List<Recipient>> getRecipients() {
        return this.recipients;
    }

    public LiveData<Event> getEvents() {
        return this.events;
    }

    public void block(RecipientId recipientId) {
        this.repository.block(recipientId, new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BlockedUsersViewModel.this.lambda$block$0(this.f$1);
            }
        }, new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BlockedUsersViewModel.this.lambda$block$1(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$block$0(RecipientId recipientId) {
        loadRecipients();
        this.events.postValue(new Event(EventType.BLOCK_SUCCEEDED, Recipient.resolved(recipientId)));
    }

    public /* synthetic */ void lambda$block$1(RecipientId recipientId) {
        this.events.postValue(new Event(EventType.BLOCK_FAILED, Recipient.resolved(recipientId)));
    }

    public void createAndBlock(String str) {
        this.repository.createAndBlock(str, new Runnable(str) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BlockedUsersViewModel.this.lambda$createAndBlock$2(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$createAndBlock$2(String str) {
        loadRecipients();
        this.events.postValue(new Event(EventType.BLOCK_SUCCEEDED, str));
    }

    public void unblock(RecipientId recipientId) {
        this.repository.unblock(recipientId, new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersViewModel$$ExternalSyntheticLambda4
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BlockedUsersViewModel.this.lambda$unblock$3(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$unblock$3(RecipientId recipientId) {
        loadRecipients();
        this.events.postValue(new Event(EventType.UNBLOCK_SUCCEEDED, Recipient.resolved(recipientId)));
    }

    private void loadRecipients() {
        BlockedUsersRepository blockedUsersRepository = this.repository;
        MutableLiveData<List<Recipient>> mutableLiveData = this.recipients;
        Objects.requireNonNull(mutableLiveData);
        blockedUsersRepository.getBlocked(new BlockedUsersViewModel$$ExternalSyntheticLambda2(mutableLiveData));
    }

    /* loaded from: classes3.dex */
    public static final class Event {
        private final EventType eventType;
        private final String number;
        private final Recipient recipient;

        private Event(EventType eventType, Recipient recipient) {
            this.eventType = eventType;
            this.recipient = recipient;
            this.number = null;
        }

        private Event(EventType eventType, String str) {
            this.eventType = eventType;
            this.recipient = null;
            this.number = str;
        }

        public Recipient getRecipient() {
            return this.recipient;
        }

        public String getNumber() {
            return this.number;
        }

        public EventType getEventType() {
            return this.eventType;
        }
    }

    /* loaded from: classes3.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final BlockedUsersRepository repository;

        public Factory(BlockedUsersRepository blockedUsersRepository) {
            this.repository = blockedUsersRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new BlockedUsersViewModel(this.repository));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
