package org.thoughtcrime.securesms.blocked;

import android.content.Context;
import androidx.core.util.Consumer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupChangeFailedException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;

/* loaded from: classes3.dex */
public class BlockedUsersRepository {
    private static final String TAG = Log.tag(BlockedUsersRepository.class);
    private final Context context;

    public BlockedUsersRepository(Context context) {
        this.context = context;
    }

    public void getBlocked(Consumer<List<Recipient>> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersRepository$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                BlockedUsersRepository.lambda$getBlocked$0(Consumer.this);
            }
        });
    }

    public static /* synthetic */ void lambda$getBlocked$0(Consumer consumer) {
        RecipientDatabase recipients = SignalDatabase.recipients();
        RecipientDatabase.RecipientReader readerForBlocked = recipients.readerForBlocked(recipients.getBlocked());
        try {
            if (readerForBlocked.getCount() == 0) {
                consumer.accept(Collections.emptyList());
            } else {
                ArrayList arrayList = new ArrayList();
                while (readerForBlocked.getNext() != null) {
                    arrayList.add(readerForBlocked.getCurrent());
                }
                consumer.accept(arrayList);
            }
            readerForBlocked.close();
        } catch (Throwable th) {
            if (readerForBlocked != null) {
                try {
                    readerForBlocked.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public void block(RecipientId recipientId, Runnable runnable, Runnable runnable2) {
        SignalExecutors.BOUNDED.execute(new Runnable(recipientId, runnable, runnable2) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ Runnable f$2;
            public final /* synthetic */ Runnable f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BlockedUsersRepository.this.lambda$block$1(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$block$1(RecipientId recipientId, Runnable runnable, Runnable runnable2) {
        try {
            RecipientUtil.block(this.context, Recipient.resolved(recipientId));
            runnable.run();
        } catch (IOException | GroupChangeBusyException | GroupChangeFailedException e) {
            Log.w(TAG, "block: failed to block recipient: ", e);
            runnable2.run();
        }
    }

    public void createAndBlock(String str, Runnable runnable) {
        SignalExecutors.BOUNDED.execute(new Runnable(str, runnable) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Runnable f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BlockedUsersRepository.this.lambda$createAndBlock$2(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$createAndBlock$2(String str, Runnable runnable) {
        Context context = this.context;
        RecipientUtil.blockNonGroup(context, Recipient.external(context, str));
        runnable.run();
    }

    public void unblock(RecipientId recipientId, Runnable runnable) {
        SignalExecutors.BOUNDED.execute(new Runnable(recipientId, runnable) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ Runnable f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BlockedUsersRepository.this.lambda$unblock$3(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$unblock$3(RecipientId recipientId, Runnable runnable) {
        RecipientUtil.unblock(this.context, Recipient.resolved(recipientId));
        runnable.run();
    }
}
