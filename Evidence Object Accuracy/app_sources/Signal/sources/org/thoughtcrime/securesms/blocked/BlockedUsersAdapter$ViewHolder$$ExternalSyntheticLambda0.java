package org.thoughtcrime.securesms.blocked;

import android.view.View;
import androidx.core.util.Consumer;
import org.thoughtcrime.securesms.blocked.BlockedUsersAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ BlockedUsersAdapter.ViewHolder f$0;
    public final /* synthetic */ Consumer f$1;

    public /* synthetic */ BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda0(BlockedUsersAdapter.ViewHolder viewHolder, Consumer consumer) {
        this.f$0 = viewHolder;
        this.f$1 = consumer;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(this.f$1, view);
    }
}
