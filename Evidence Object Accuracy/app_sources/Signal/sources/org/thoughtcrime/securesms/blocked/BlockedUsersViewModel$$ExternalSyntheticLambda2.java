package org.thoughtcrime.securesms.blocked;

import androidx.core.util.Consumer;
import androidx.lifecycle.MutableLiveData;
import java.util.List;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class BlockedUsersViewModel$$ExternalSyntheticLambda2 implements Consumer {
    public final /* synthetic */ MutableLiveData f$0;

    public /* synthetic */ BlockedUsersViewModel$$ExternalSyntheticLambda2(MutableLiveData mutableLiveData) {
        this.f$0 = mutableLiveData;
    }

    @Override // androidx.core.util.Consumer
    public final void accept(Object obj) {
        this.f$0.postValue((List) obj);
    }
}
