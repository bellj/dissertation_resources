package org.thoughtcrime.securesms.blocked;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import j$.util.Optional;
import j$.util.function.Consumer;
import j$.util.function.Function;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blocked.BlockedUsersFragment;
import org.thoughtcrime.securesms.blocked.BlockedUsersViewModel;
import org.thoughtcrime.securesms.components.ContactFilterView;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes3.dex */
public class BlockedUsersActivity extends PassphraseRequiredActivity implements BlockedUsersFragment.Listener, ContactSelectionListFragment.OnContactSelectedListener {
    private static final String CONTACT_SELECTION_FRAGMENT;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private BlockedUsersViewModel viewModel;

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onContactDeselected(Optional<RecipientId> optional, String str) {
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onSelectionChanged() {
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.blocked_users_activity);
        this.viewModel = (BlockedUsersViewModel) ViewModelProviders.of(this, new BlockedUsersViewModel.Factory(new BlockedUsersRepository(this))).get(BlockedUsersViewModel.class);
        ContactFilterView contactFilterView = (ContactFilterView) findViewById(R.id.contact_filter_edit_text);
        View findViewById = findViewById(R.id.fragment_container);
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BlockedUsersActivity.$r8$lambda$wBvNqsJkI01N6QG6AoI8ejN_Ww8(BlockedUsersActivity.this, view);
            }
        });
        contactFilterView.setOnFilterChangedListener(new ContactFilterView.OnFilterChangedListener() { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersActivity$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.components.ContactFilterView.OnFilterChangedListener
            public final void onFilterChanged(String str) {
                BlockedUsersActivity.$r8$lambda$3ALgg2JGqyd7pQcscBqGis7KV9o(BlockedUsersActivity.this, str);
            }
        });
        contactFilterView.setHint(R.string.BlockedUsersActivity__add_blocked_user);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener(contactFilterView) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersActivity$$ExternalSyntheticLambda2
            public final /* synthetic */ ContactFilterView f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.fragment.app.FragmentManager.OnBackStackChangedListener
            public final void onBackStackChanged() {
                BlockedUsersActivity.$r8$lambda$FnN82ImqXMESPr6q3glRl4uCiCw(BlockedUsersActivity.this, this.f$1);
            }
        });
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, new BlockedUsersFragment()).commit();
        this.viewModel.getEvents().observe(this, new Observer(findViewById) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersActivity$$ExternalSyntheticLambda3
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BlockedUsersActivity.$r8$lambda$tDypoZHB2pWcGaonKxZMm7YVRf8(BlockedUsersActivity.this, this.f$1, (BlockedUsersViewModel.Event) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        onBackPressed();
    }

    public /* synthetic */ void lambda$onCreate$1(String str) {
        Fragment findFragmentByTag = getSupportFragmentManager().findFragmentByTag(CONTACT_SELECTION_FRAGMENT);
        if (findFragmentByTag != null) {
            ((ContactSelectionListFragment) findFragmentByTag).setQueryFilter(str);
        }
    }

    public /* synthetic */ void lambda$onCreate$2(ContactFilterView contactFilterView) {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            contactFilterView.setVisibility(0);
            contactFilterView.focusAndShowKeyboard();
            return;
        }
        contactFilterView.setVisibility(8);
        ViewUtil.hideKeyboard(this, contactFilterView);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    public /* synthetic */ String lambda$onBeforeContactSelected$4(RecipientId recipientId) {
        return Recipient.resolved(recipientId).getDisplayName(this);
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onBeforeContactSelected(Optional<RecipientId> optional, String str, Consumer<Boolean> consumer) {
        AlertDialog create = new MaterialAlertDialogBuilder(this).setTitle(R.string.BlockedUsersActivity__block_user).setMessage((CharSequence) getString(R.string.BlockedUserActivity__s_will_not_be_able_to, new Object[]{(String) optional.map(new Function() { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersActivity$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return BlockedUsersActivity.$r8$lambda$r5sC1XfL9Hv2B3XvwUs2vuPq_w8(BlockedUsersActivity.this, (RecipientId) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(str)})).setPositiveButton(R.string.BlockedUsersActivity__block, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(optional, str) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersActivity$$ExternalSyntheticLambda5
            public final /* synthetic */ Optional f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                BlockedUsersActivity.$r8$lambda$1pZG9HTp0D5jP47NRKHHsP5sMZI(BlockedUsersActivity.this, this.f$1, this.f$2, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersActivity$$ExternalSyntheticLambda6
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                BlockedUsersActivity.m467$r8$lambda$ZuZXURTn34B5MiJWvj8xviTEUM(dialogInterface, i);
            }
        }).setCancelable(true).create();
        create.setOnShowListener(new DialogInterface.OnShowListener() { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersActivity$$ExternalSyntheticLambda7
            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                BlockedUsersActivity.m466$r8$lambda$NdKV2qNRU3Kvd4LNk_qGC0NmoA(AlertDialog.this, dialogInterface);
            }
        });
        create.show();
        consumer.accept(Boolean.FALSE);
    }

    public /* synthetic */ void lambda$onBeforeContactSelected$5(Optional optional, String str, DialogInterface dialogInterface, int i) {
        if (optional.isPresent()) {
            this.viewModel.block((RecipientId) optional.get());
        } else {
            this.viewModel.createAndBlock(str);
        }
        dialogInterface.dismiss();
        onBackPressed();
    }

    public static /* synthetic */ void lambda$onBeforeContactSelected$7(AlertDialog alertDialog, DialogInterface dialogInterface) {
        alertDialog.getButton(-1).setTextColor(-65536);
    }

    @Override // org.thoughtcrime.securesms.blocked.BlockedUsersFragment.Listener
    public void handleAddUserToBlockedList() {
        ContactSelectionListFragment contactSelectionListFragment = new ContactSelectionListFragment();
        Intent intent = getIntent();
        intent.putExtra("refreshable", false);
        intent.putExtra("selection_limits", 1);
        intent.putExtra("hide_count", true);
        intent.putExtra("display_mode", 47);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, contactSelectionListFragment, CONTACT_SELECTION_FRAGMENT).addToBackStack(null).commit();
    }

    /* renamed from: handleEvent */
    public void lambda$onCreate$3(View view, BlockedUsersViewModel.Event event) {
        String str;
        int i;
        if (event.getRecipient() == null) {
            str = event.getNumber();
        } else {
            str = event.getRecipient().getDisplayName(this);
        }
        int i2 = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$blocked$BlockedUsersViewModel$EventType[event.getEventType().ordinal()];
        if (i2 == 1) {
            i = R.string.BlockedUsersActivity__s_has_been_blocked;
        } else if (i2 == 2) {
            i = R.string.BlockedUsersActivity__failed_to_block_s;
        } else if (i2 == 3) {
            i = R.string.BlockedUsersActivity__s_has_been_unblocked;
        } else {
            throw new IllegalArgumentException("Unsupported event type " + event);
        }
        Snackbar.make(view, getString(i, new Object[]{str}), -1).show();
    }

    /* renamed from: org.thoughtcrime.securesms.blocked.BlockedUsersActivity$1 */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$blocked$BlockedUsersViewModel$EventType;

        static {
            int[] iArr = new int[BlockedUsersViewModel.EventType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$blocked$BlockedUsersViewModel$EventType = iArr;
            try {
                iArr[BlockedUsersViewModel.EventType.BLOCK_SUCCEEDED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$blocked$BlockedUsersViewModel$EventType[BlockedUsersViewModel.EventType.BLOCK_FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$blocked$BlockedUsersViewModel$EventType[BlockedUsersViewModel.EventType.UNBLOCK_SUCCEEDED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }
}
