package org.thoughtcrime.securesms.blocked;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.util.OptionalUtil;

/* loaded from: classes3.dex */
public final class BlockedUsersAdapter extends ListAdapter<Recipient, ViewHolder> {
    private final RecipientClickedListener recipientClickedListener;

    /* loaded from: classes3.dex */
    public interface RecipientClickedListener {
        void onRecipientClicked(Recipient recipient);
    }

    public BlockedUsersAdapter(RecipientClickedListener recipientClickedListener) {
        super(new RecipientDiffCallback());
        this.recipientClickedListener = recipientClickedListener;
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.blocked_users_adapter_item, viewGroup, false), new Consumer() { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersAdapter$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                BlockedUsersAdapter.$r8$lambda$6swh68aVYCseu2nRElyZ75OCY1M(BlockedUsersAdapter.this, (Integer) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onCreateViewHolder$0(Integer num) {
        RecipientClickedListener recipientClickedListener = this.recipientClickedListener;
        Recipient item = getItem(num.intValue());
        Objects.requireNonNull(item);
        recipientClickedListener.onRecipientClicked(item);
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Recipient item = getItem(i);
        Objects.requireNonNull(item);
        viewHolder.bind(item);
    }

    /* loaded from: classes3.dex */
    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private final AvatarImageView avatar;
        private final TextView displayName;
        private final TextView numberOrUsername;

        public ViewHolder(View view, Consumer<Integer> consumer) {
            super(view);
            this.avatar = (AvatarImageView) view.findViewById(R.id.avatar);
            this.displayName = (TextView) view.findViewById(R.id.display_name);
            this.numberOrUsername = (TextView) view.findViewById(R.id.number_or_username);
            view.setOnClickListener(new BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda0(this, consumer));
        }

        public /* synthetic */ void lambda$new$0(Consumer consumer, View view) {
            if (getAdapterPosition() != -1) {
                consumer.accept(Integer.valueOf(getAdapterPosition()));
            }
        }

        public void bind(Recipient recipient) {
            this.avatar.setAvatar(recipient);
            this.displayName.setText(recipient.getDisplayName(this.itemView.getContext()));
            if (recipient.hasAUserSetDisplayName(this.itemView.getContext())) {
                String str = (String) OptionalUtil.or(recipient.getE164().map(new BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda1()), recipient.getUsername()).orElse(null);
                if (str != null) {
                    this.numberOrUsername.setText(str);
                    this.numberOrUsername.setVisibility(0);
                    return;
                }
                this.numberOrUsername.setVisibility(8);
                return;
            }
            this.numberOrUsername.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static final class RecipientDiffCallback extends DiffUtil.ItemCallback<Recipient> {
        private RecipientDiffCallback() {
        }

        public boolean areItemsTheSame(Recipient recipient, Recipient recipient2) {
            return recipient.equals(recipient2);
        }

        public boolean areContentsTheSame(Recipient recipient, Recipient recipient2) {
            return recipient.equals(recipient2);
        }
    }
}
