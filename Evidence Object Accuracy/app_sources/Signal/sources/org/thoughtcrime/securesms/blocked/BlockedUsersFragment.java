package org.thoughtcrime.securesms.blocked;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import org.thoughtcrime.securesms.BlockUnblockDialog;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blocked.BlockedUsersAdapter;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes3.dex */
public class BlockedUsersFragment extends Fragment {
    private Listener listener;
    private BlockedUsersViewModel viewModel;

    /* loaded from: classes3.dex */
    public interface Listener {
        void handleAddUserToBlockedList();
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Listener) {
            this.listener = (Listener) context;
            return;
        }
        throw new ClassCastException("Expected context to implement Listener");
    }

    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.blocked_users_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        View findViewById = view.findViewById(R.id.add_blocked_user_touch_target);
        View findViewById2 = view.findViewById(R.id.no_blocked_users);
        BlockedUsersAdapter blockedUsersAdapter = new BlockedUsersAdapter(new BlockedUsersAdapter.RecipientClickedListener() { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersFragment$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.blocked.BlockedUsersAdapter.RecipientClickedListener
            public final void onRecipientClicked(Recipient recipient) {
                BlockedUsersFragment.this.handleRecipientClicked(recipient);
            }
        });
        ((RecyclerView) view.findViewById(R.id.blocked_users_recycler)).setAdapter(blockedUsersAdapter);
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BlockedUsersFragment.this.lambda$onViewCreated$0(view2);
            }
        });
        BlockedUsersViewModel blockedUsersViewModel = (BlockedUsersViewModel) ViewModelProviders.of(requireActivity()).get(BlockedUsersViewModel.class);
        this.viewModel = blockedUsersViewModel;
        blockedUsersViewModel.getRecipients().observe(getViewLifecycleOwner(), new Observer(findViewById2, blockedUsersAdapter) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ View f$0;
            public final /* synthetic */ BlockedUsersAdapter f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BlockedUsersFragment.lambda$onViewCreated$1(this.f$0, this.f$1, (List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        Listener listener = this.listener;
        if (listener != null) {
            listener.handleAddUserToBlockedList();
        }
    }

    public static /* synthetic */ void lambda$onViewCreated$1(View view, BlockedUsersAdapter blockedUsersAdapter, List list) {
        if (list.isEmpty()) {
            view.setVisibility(0);
        } else {
            view.setVisibility(8);
        }
        blockedUsersAdapter.submitList(list);
    }

    public void handleRecipientClicked(Recipient recipient) {
        BlockUnblockDialog.showUnblockFor(requireContext(), getViewLifecycleOwner().getLifecycle(), recipient, new Runnable(recipient) { // from class: org.thoughtcrime.securesms.blocked.BlockedUsersFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BlockedUsersFragment.this.lambda$handleRecipientClicked$2(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$handleRecipientClicked$2(Recipient recipient) {
        this.viewModel.unblock(recipient.getId());
    }
}
