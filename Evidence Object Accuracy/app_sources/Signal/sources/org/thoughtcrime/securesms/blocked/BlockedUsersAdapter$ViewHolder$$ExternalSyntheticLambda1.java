package org.thoughtcrime.securesms.blocked;

import j$.util.function.Function;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda1 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return PhoneNumberFormatter.prettyPrint((String) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
