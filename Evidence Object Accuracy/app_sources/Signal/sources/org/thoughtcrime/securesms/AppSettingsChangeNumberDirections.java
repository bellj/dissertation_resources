package org.thoughtcrime.securesms;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;

/* loaded from: classes.dex */
public class AppSettingsChangeNumberDirections {
    private AppSettingsChangeNumberDirections() {
    }

    public static NavDirections actionPopAppSettingsChangeNumber() {
        return new ActionOnlyNavDirections(R.id.action_pop_app_settings_change_number);
    }
}
