package org.thoughtcrime.securesms;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ShareCompat$IntentBuilder;
import androidx.core.util.Pair;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import j$.util.function.Consumer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.animation.DepthPageTransformer;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.components.viewpager.ExtendedOnPageChangedListener;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaController;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaControllerOwner;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.loaders.PagingMediaLoader;
import org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity;
import org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment;
import org.thoughtcrime.securesms.mediapreview.MediaPreviewViewModel;
import org.thoughtcrime.securesms.mediapreview.MediaRailAdapter;
import org.thoughtcrime.securesms.mediapreview.VideoControlsDelegate;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.AttachmentUtil;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.FullscreenHelper;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.SaveAttachmentTask;
import org.thoughtcrime.securesms.util.StorageUtil;

/* loaded from: classes.dex */
public final class MediaPreviewActivity extends PassphraseRequiredActivity implements LoaderManager.LoaderCallbacks<Pair<Cursor, Integer>>, MediaRailAdapter.RailItemListener, MediaPreviewFragment.Events, VoiceNoteMediaControllerOwner {
    public static final String CAPTION_EXTRA;
    public static final String DATE_EXTRA;
    public static final String HIDE_ALL_MEDIA_EXTRA;
    public static final String IS_VIDEO_GIF;
    public static final String LEFT_IS_RECENT_EXTRA;
    private static final int NOT_IN_A_THREAD;
    public static final String SHOW_THREAD_EXTRA;
    public static final String SIZE_EXTRA;
    public static final String SORTING_EXTRA;
    private static final String TAG = Log.tag(MediaPreviewActivity.class);
    public static final String THREAD_ID_EXTRA;
    private RecyclerView albumRail;
    private MediaRailAdapter albumRailAdapter;
    private boolean cameFromAllMedia;
    private TextView caption;
    private View captionContainer;
    private Cursor cursor = null;
    private View detailsContainer;
    private FullscreenHelper fullscreenHelper;
    private String initialCaption;
    private boolean initialMediaIsVideoGif;
    private long initialMediaSize;
    private String initialMediaType;
    private Uri initialMediaUri;
    private boolean leftIsRecent;
    private ViewPager mediaPager;
    private ViewGroup playbackControlsContainer;
    private int restartItem = -1;
    private boolean showThread;
    private MediaDatabase.Sorting sorting;
    private long threadId = -2;
    private MediaPreviewViewModel viewModel;
    private ViewPagerListener viewPagerListener;
    private VoiceNoteMediaController voiceNoteMediaController;

    /* loaded from: classes.dex */
    public interface MediaItemAdapter {
        MediaItem getMediaItemFor(int i);

        View getPlaybackControls(int i);

        boolean hasFragmentFor(int i);

        void pause(int i);
    }

    @Override // org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment.Events
    public /* synthetic */ VideoControlsDelegate getVideoControlsDelegate() {
        return MediaPreviewFragment.Events.CC.$default$getVideoControlsDelegate(this);
    }

    public void onLoaderReset(Loader<Pair<Cursor, Integer>> loader) {
    }

    public void onMediaReady() {
    }

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
        onLoadFinished((Loader<Pair<Cursor, Integer>>) loader, (Pair) obj);
    }

    public static Intent intentFromMediaRecord(Context context, MediaDatabase.MediaRecord mediaRecord, boolean z) {
        DatabaseAttachment attachment = mediaRecord.getAttachment();
        Objects.requireNonNull(attachment);
        Intent intent = new Intent(context, MediaPreviewActivity.class);
        intent.putExtra("thread_id", mediaRecord.getThreadId());
        intent.putExtra("date", mediaRecord.getDate());
        intent.putExtra(SIZE_EXTRA, attachment.getSize());
        intent.putExtra(CAPTION_EXTRA, attachment.getCaption());
        intent.putExtra(LEFT_IS_RECENT_EXTRA, z);
        intent.putExtra(IS_VIDEO_GIF, attachment.isVideoGif());
        intent.setDataAndType(attachment.getUri(), mediaRecord.getContentType());
        return intent;
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        getDelegate().setLocalNightMode(2);
        super.attachBaseContext(context);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setTheme(R.style.TextSecure_MediaPreview);
        setContentView(R.layout.media_preview_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        this.voiceNoteMediaController = new VoiceNoteMediaController(this);
        this.viewModel = (MediaPreviewViewModel) ViewModelProviders.of(this).get(MediaPreviewViewModel.class);
        this.fullscreenHelper = new FullscreenHelper(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initializeViews();
        initializeResources();
        initializeObservers();
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    public void onRailItemClicked(int i) {
        ViewPager viewPager = this.mediaPager;
        viewPager.setCurrentItem(viewPager.getCurrentItem() + i);
    }

    public void onRailItemDeleteClicked(int i) {
        throw new UnsupportedOperationException("Callback unsupported.");
    }

    public void initializeActionBar() {
        MediaItem currentMediaItem = getCurrentMediaItem();
        if (currentMediaItem != null) {
            getSupportActionBar().setTitle(getTitleText(currentMediaItem));
            getSupportActionBar().setSubtitle(getSubTitleText(currentMediaItem));
        }
    }

    private String getTitleText(MediaItem mediaItem) {
        String str;
        if (mediaItem.outgoing) {
            str = getString(R.string.MediaPreviewActivity_you);
        } else {
            str = mediaItem.recipient != null ? mediaItem.recipient.getDisplayName(this) : "";
        }
        if (!this.showThread) {
            return str;
        }
        String str2 = null;
        Recipient recipient = mediaItem.threadRecipient;
        if (recipient != null) {
            if (mediaItem.outgoing) {
                if (recipient.isSelf()) {
                    str2 = getString(R.string.note_to_self);
                } else {
                    str2 = getString(R.string.MediaPreviewActivity_you_to_s, new Object[]{recipient.getDisplayName(this)});
                }
            } else if (recipient.isGroup()) {
                str2 = getString(R.string.MediaPreviewActivity_s_to_s, new Object[]{str, recipient.getDisplayName(this)});
            } else {
                str2 = getString(R.string.MediaPreviewActivity_s_to_you, new Object[]{str});
            }
        }
        return str2 != null ? str2 : str;
    }

    private String getSubTitleText(MediaItem mediaItem) {
        if (mediaItem.date > 0) {
            return DateUtils.getExtendedRelativeTimeSpanString(this, Locale.getDefault(), mediaItem.date);
        }
        return getString(R.string.MediaPreviewActivity_draft);
    }

    public void onResume() {
        super.onResume();
        initializeMedia();
    }

    public void onPause() {
        super.onPause();
        this.restartItem = cleanupMedia();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        Cursor cursor = this.cursor;
        if (cursor != null) {
            cursor.close();
            this.cursor = null;
        }
        super.onDestroy();
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        initializeResources();
    }

    private void initializeViews() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.media_pager);
        this.mediaPager = viewPager;
        viewPager.setOffscreenPageLimit(1);
        this.mediaPager.setPageTransformer(true, new DepthPageTransformer());
        ViewPagerListener viewPagerListener = new ViewPagerListener();
        this.viewPagerListener = viewPagerListener;
        this.mediaPager.addOnPageChangeListener(viewPagerListener);
        this.albumRail = (RecyclerView) findViewById(R.id.media_preview_album_rail);
        this.albumRailAdapter = new MediaRailAdapter(GlideApp.with((FragmentActivity) this), this, false);
        this.albumRail.setItemAnimator(null);
        this.albumRail.setLayoutManager(new LinearLayoutManager(this, 0, false));
        this.albumRail.setAdapter(this.albumRailAdapter);
        this.detailsContainer = findViewById(R.id.media_preview_details_container);
        this.caption = (TextView) findViewById(R.id.media_preview_caption);
        this.captionContainer = findViewById(R.id.media_preview_caption_container);
        this.playbackControlsContainer = (ViewGroup) findViewById(R.id.media_preview_playback_controls_container);
        View findViewById = findViewById(R.id.toolbar_layout);
        anchorMarginsToBottomInsets(this.detailsContainer);
        this.fullscreenHelper.configureToolbarLayout(findViewById(R.id.toolbar_cutout_spacer), findViewById(R.id.toolbar));
        this.fullscreenHelper.showAndHideWithSystemUI(getWindow(), this.detailsContainer, findViewById);
    }

    private void initializeResources() {
        Intent intent = getIntent();
        this.threadId = intent.getLongExtra("thread_id", -2);
        this.cameFromAllMedia = intent.getBooleanExtra(HIDE_ALL_MEDIA_EXTRA, false);
        this.showThread = intent.getBooleanExtra(SHOW_THREAD_EXTRA, false);
        this.sorting = MediaDatabase.Sorting.values()[intent.getIntExtra(SORTING_EXTRA, 0)];
        this.initialMediaUri = intent.getData();
        this.initialMediaType = intent.getType();
        this.initialMediaSize = intent.getLongExtra(SIZE_EXTRA, 0);
        this.initialCaption = intent.getStringExtra(CAPTION_EXTRA);
        this.leftIsRecent = intent.getBooleanExtra(LEFT_IS_RECENT_EXTRA, false);
        this.initialMediaIsVideoGif = intent.getBooleanExtra(IS_VIDEO_GIF, false);
        this.restartItem = -1;
    }

    private void initializeObservers() {
        this.viewModel.getPreviewData().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.MediaPreviewActivity$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MediaPreviewActivity.m253$r8$lambda$fp_4dzGCPPX8eTatacqkAUydGc(MediaPreviewActivity.this, (MediaPreviewViewModel.PreviewData) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeObservers$0(MediaPreviewViewModel.PreviewData previewData) {
        ViewPager viewPager;
        if (previewData != null && (viewPager = this.mediaPager) != null && viewPager.getAdapter() != null) {
            if (!((MediaItemAdapter) this.mediaPager.getAdapter()).hasFragmentFor(this.mediaPager.getCurrentItem())) {
                Log.d(TAG, "MediaItemAdapter wasn't ready. Posting again...");
                this.viewModel.resubmitPreviewData();
            }
            View playbackControls = ((MediaItemAdapter) this.mediaPager.getAdapter()).getPlaybackControls(this.mediaPager.getCurrentItem());
            int i = 8;
            if (previewData.getAlbumThumbnails().isEmpty() && previewData.getCaption() == null && playbackControls == null) {
                this.detailsContainer.setVisibility(8);
            } else {
                this.detailsContainer.setVisibility(0);
            }
            this.albumRail.setVisibility(previewData.getAlbumThumbnails().isEmpty() ? 8 : 0);
            this.albumRailAdapter.setMedia(previewData.getAlbumThumbnails(), previewData.getActivePosition());
            this.albumRail.smoothScrollToPosition(previewData.getActivePosition());
            View view = this.captionContainer;
            if (previewData.getCaption() != null) {
                i = 0;
            }
            view.setVisibility(i);
            this.caption.setText(previewData.getCaption());
            if (playbackControls != null) {
                playbackControls.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
                this.playbackControlsContainer.removeAllViews();
                this.playbackControlsContainer.addView(playbackControls);
                return;
            }
            this.playbackControlsContainer.removeAllViews();
        }
    }

    private void initializeMedia() {
        if (!isContentTypeSupported(this.initialMediaType)) {
            Log.w(TAG, "Unsupported media type sent to MediaPreviewActivity, finishing.");
            Toast.makeText(getApplicationContext(), (int) R.string.MediaPreviewActivity_unssuported_media_type, 1).show();
            finish();
        }
        String str = TAG;
        Log.i(str, "Loading Part URI: " + this.initialMediaUri);
        if (isMediaInDb()) {
            LoaderManager.getInstance(this).restartLoader(0, null, this);
            return;
        }
        this.mediaPager.setAdapter(new SingleItemPagerAdapter(getSupportFragmentManager(), this.initialMediaUri, this.initialMediaType, this.initialMediaSize, this.initialMediaIsVideoGif));
        if (this.initialCaption != null) {
            this.detailsContainer.setVisibility(0);
            this.captionContainer.setVisibility(0);
            this.caption.setText(this.initialCaption);
        }
    }

    private int cleanupMedia() {
        int currentItem = this.mediaPager.getCurrentItem();
        this.mediaPager.removeAllViews();
        this.mediaPager.setAdapter(null);
        this.viewModel.setCursor(this, null, this.leftIsRecent);
        return currentItem;
    }

    private void showOverview() {
        startActivity(MediaOverviewActivity.forThread(this, this.threadId));
    }

    private void forward() {
        MediaItem currentMediaItem = getCurrentMediaItem();
        if (currentMediaItem != null) {
            MultiselectForwardFragmentArgs.create(this, this.threadId, currentMediaItem.uri, currentMediaItem.type, new Consumer() { // from class: org.thoughtcrime.securesms.MediaPreviewActivity$$ExternalSyntheticLambda6
                @Override // j$.util.function.Consumer
                public final void accept(Object obj) {
                    MediaPreviewActivity.m251$r8$lambda$DkJ2WXYEv9UW7NuiKRR4432Dc(MediaPreviewActivity.this, (MultiselectForwardFragmentArgs) obj);
                }

                @Override // j$.util.function.Consumer
                public /* synthetic */ Consumer andThen(Consumer consumer) {
                    return Consumer.CC.$default$andThen(this, consumer);
                }
            });
        }
    }

    public /* synthetic */ void lambda$forward$1(MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
        MultiselectForwardFragment.showBottomSheet(getSupportFragmentManager(), multiselectForwardFragmentArgs);
    }

    private void share() {
        MediaItem currentMediaItem = getCurrentMediaItem();
        if (currentMediaItem != null) {
            Uri attachmentPublicUri = PartAuthority.getAttachmentPublicUri(currentMediaItem.uri);
            try {
                startActivity(ShareCompat$IntentBuilder.from(this).setStream(attachmentPublicUri).setType(Intent.normalizeMimeType(currentMediaItem.type)).createChooserIntent().addFlags(1));
            } catch (ActivityNotFoundException e) {
                Log.w(TAG, "No activity existed to share the media.", e);
                Toast.makeText(this, (int) R.string.MediaPreviewActivity_cant_find_an_app_able_to_share_this_media, 1).show();
            }
        }
    }

    private void saveToDisk() {
        MediaItem currentMediaItem = getCurrentMediaItem();
        if (currentMediaItem != null) {
            SaveAttachmentTask.showWarningDialog(this, new DialogInterface.OnClickListener(currentMediaItem) { // from class: org.thoughtcrime.securesms.MediaPreviewActivity$$ExternalSyntheticLambda3
                public final /* synthetic */ MediaPreviewActivity.MediaItem f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    MediaPreviewActivity.m252$r8$lambda$2fYO7kigJtKYhHQFuW_epy4gX0(MediaPreviewActivity.this, this.f$1, dialogInterface, i);
                }
            });
        }
    }

    public /* synthetic */ void lambda$saveToDisk$4(MediaItem mediaItem, DialogInterface dialogInterface, int i) {
        if (StorageUtil.canWriteToMediaStore()) {
            lambda$saveToDisk$3(mediaItem);
        } else {
            Permissions.with(this).request("android.permission.WRITE_EXTERNAL_STORAGE").ifNecessary().withPermanentDenialDialog(getString(R.string.MediaPreviewActivity_signal_needs_the_storage_permission_in_order_to_write_to_external_storage_but_it_has_been_permanently_denied)).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.MediaPreviewActivity$$ExternalSyntheticLambda4
                @Override // java.lang.Runnable
                public final void run() {
                    MediaPreviewActivity.$r8$lambda$7wq_RjIxo3YY7wR_HKkbcV8UMf0(MediaPreviewActivity.this);
                }
            }).onAllGranted(new Runnable(mediaItem) { // from class: org.thoughtcrime.securesms.MediaPreviewActivity$$ExternalSyntheticLambda5
                public final /* synthetic */ MediaPreviewActivity.MediaItem f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    MediaPreviewActivity.$r8$lambda$pU_b7mJ44iKvzTnUxdZZIM3BgUo(MediaPreviewActivity.this, this.f$1);
                }
            }).execute();
        }
    }

    public /* synthetic */ void lambda$saveToDisk$2() {
        Toast.makeText(this, (int) R.string.MediaPreviewActivity_unable_to_write_to_external_storage_without_permission, 1).show();
    }

    /* renamed from: performSavetoDisk */
    public void lambda$saveToDisk$3(MediaItem mediaItem) {
        long j;
        SaveAttachmentTask saveAttachmentTask = new SaveAttachmentTask(this);
        if (mediaItem.date > 0) {
            j = mediaItem.date;
        } else {
            j = System.currentTimeMillis();
        }
        saveAttachmentTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SaveAttachmentTask.Attachment(mediaItem.uri, mediaItem.type, j, null));
    }

    private void deleteMedia() {
        MediaItem currentMediaItem = getCurrentMediaItem();
        if (currentMediaItem != null && currentMediaItem.attachment != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setIcon(R.drawable.ic_warning);
            builder.setTitle(R.string.MediaPreviewActivity_media_delete_confirmation_title);
            builder.setMessage(R.string.MediaPreviewActivity_media_delete_confirmation_message);
            builder.setCancelable(true);
            builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener(currentMediaItem) { // from class: org.thoughtcrime.securesms.MediaPreviewActivity$$ExternalSyntheticLambda1
                public final /* synthetic */ MediaPreviewActivity.MediaItem f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    MediaPreviewActivity.$r8$lambda$ctIW8Xx4QWaVeV3P12Y2DI_OtWM(MediaPreviewActivity.this, this.f$1, dialogInterface, i);
                }
            });
            builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
            builder.show();
        }
    }

    public /* synthetic */ void lambda$deleteMedia$5(final MediaItem mediaItem, DialogInterface dialogInterface, int i) {
        new AsyncTask<Void, Void, Void>() { // from class: org.thoughtcrime.securesms.MediaPreviewActivity.1
            public Void doInBackground(Void... voidArr) {
                AttachmentUtil.deleteAttachment(MediaPreviewActivity.this.getApplicationContext(), mediaItem.attachment);
                return null;
            }
        }.execute(new Void[0]);
        finish();
    }

    @Override // android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.media_preview, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!isMediaInDb()) {
            menu.findItem(R.id.media_preview__overview).setVisible(false);
            menu.findItem(R.id.delete).setVisible(false);
        }
        menu.findItem(R.id.media_preview__share).setVisible(Build.VERSION.SDK_INT >= 26);
        if (this.cameFromAllMedia) {
            menu.findItem(R.id.media_preview__overview).setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        int itemId = menuItem.getItemId();
        if (itemId == R.id.media_preview__overview) {
            showOverview();
            return true;
        } else if (itemId == R.id.media_preview__forward) {
            forward();
            return true;
        } else if (itemId == R.id.media_preview__share) {
            share();
            return true;
        } else if (itemId == R.id.save) {
            saveToDisk();
            return true;
        } else if (itemId == R.id.delete) {
            deleteMedia();
            return true;
        } else if (itemId != 16908332) {
            return false;
        } else {
            finish();
            return true;
        }
    }

    private boolean isMediaInDb() {
        return this.threadId != -2;
    }

    private MediaItem getCurrentMediaItem() {
        MediaItemAdapter mediaItemAdapter = (MediaItemAdapter) this.mediaPager.getAdapter();
        if (mediaItemAdapter != null) {
            return mediaItemAdapter.getMediaItemFor(this.mediaPager.getCurrentItem());
        }
        return null;
    }

    public static boolean isContentTypeSupported(String str) {
        return MediaUtil.isImageType(str) || MediaUtil.isVideoType(str);
    }

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public Loader<Pair<Cursor, Integer>> onCreateLoader(int i, Bundle bundle) {
        return new PagingMediaLoader(this, this.threadId, this.initialMediaUri, this.leftIsRecent, this.sorting);
    }

    public void onLoadFinished(Loader<Pair<Cursor, Integer>> loader, Pair<Cursor, Integer> pair) {
        if (pair != null) {
            Cursor cursor = pair.first;
            Cursor cursor2 = this.cursor;
            if (cursor != cursor2) {
                if (cursor2 != null) {
                    cursor2.close();
                }
                Cursor cursor3 = pair.first;
                Objects.requireNonNull(cursor3);
                Cursor cursor4 = cursor3;
                this.cursor = cursor4;
                this.viewModel.setCursor(this, cursor4, this.leftIsRecent);
                Integer num = pair.second;
                Objects.requireNonNull(num);
                int intValue = num.intValue();
                CursorPagerAdapter cursorPagerAdapter = (CursorPagerAdapter) this.mediaPager.getAdapter();
                if (cursorPagerAdapter == null) {
                    CursorPagerAdapter cursorPagerAdapter2 = new CursorPagerAdapter(getSupportFragmentManager(), this, this.cursor, intValue, this.leftIsRecent);
                    this.mediaPager.setAdapter(cursorPagerAdapter2);
                    cursorPagerAdapter2.setActive(true);
                } else {
                    cursorPagerAdapter.setCursor(this.cursor, intValue);
                    cursorPagerAdapter.setActive(true);
                }
                if (cursorPagerAdapter == null || this.restartItem >= 0) {
                    int i = this.restartItem;
                    if (i >= 0) {
                        intValue = i;
                    }
                    this.mediaPager.setCurrentItem(intValue);
                    if (intValue == 0) {
                        this.viewPagerListener.onPageSelected(0);
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        mediaNotAvailable();
    }

    public boolean singleTapOnMedia() {
        this.fullscreenHelper.toggleUiVisibility();
        return true;
    }

    @Override // org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment.Events
    public void mediaNotAvailable() {
        Toast.makeText(this, (int) R.string.MediaPreviewActivity_media_no_longer_available, 1).show();
        finish();
    }

    @Override // org.thoughtcrime.securesms.components.voice.VoiceNoteMediaControllerOwner
    public VoiceNoteMediaController getVoiceNoteMediaController() {
        return this.voiceNoteMediaController;
    }

    /* loaded from: classes.dex */
    public class ViewPagerListener extends ExtendedOnPageChangedListener {
        private ViewPagerListener() {
            MediaPreviewActivity.this = r1;
        }

        public void onPageSelected(int i) {
            super.onPageSelected(i);
            MediaItemAdapter mediaItemAdapter = (MediaItemAdapter) MediaPreviewActivity.this.mediaPager.getAdapter();
            if (mediaItemAdapter != null) {
                MediaItem mediaItemFor = mediaItemAdapter.getMediaItemFor(i);
                if (!(mediaItemFor == null || mediaItemFor.recipient == null)) {
                    mediaItemFor.recipient.live().observe(MediaPreviewActivity.this, new MediaPreviewActivity$ViewPagerListener$$ExternalSyntheticLambda0(this));
                }
                MediaPreviewActivity.this.viewModel.setActiveAlbumRailItem(MediaPreviewActivity.this, i);
                MediaPreviewActivity.this.initializeActionBar();
            }
        }

        public /* synthetic */ void lambda$onPageSelected$0(Recipient recipient) {
            MediaPreviewActivity.this.initializeActionBar();
        }

        public void onPageUnselected(int i) {
            MediaItemAdapter mediaItemAdapter = (MediaItemAdapter) MediaPreviewActivity.this.mediaPager.getAdapter();
            if (mediaItemAdapter != null) {
                MediaItem mediaItemFor = mediaItemAdapter.getMediaItemFor(i);
                if (!(mediaItemFor == null || mediaItemFor.recipient == null)) {
                    mediaItemFor.recipient.live().removeObservers(MediaPreviewActivity.this);
                }
                mediaItemAdapter.pause(i);
            }
        }
    }

    /* loaded from: classes.dex */
    public static class SingleItemPagerAdapter extends FragmentStatePagerAdapter implements MediaItemAdapter {
        private final boolean isVideoGif;
        private MediaPreviewFragment mediaPreviewFragment;
        private final String mediaType;
        private final long size;
        private final Uri uri;

        public int getCount() {
            return 1;
        }

        SingleItemPagerAdapter(FragmentManager fragmentManager, Uri uri, String str, long j, boolean z) {
            super(fragmentManager, 1);
            this.uri = uri;
            this.mediaType = str;
            this.size = j;
            this.isVideoGif = z;
        }

        public Fragment getItem(int i) {
            MediaPreviewFragment newInstance = MediaPreviewFragment.newInstance(this.uri, this.mediaType, this.size, true, this.isVideoGif);
            this.mediaPreviewFragment = newInstance;
            return newInstance;
        }

        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            MediaPreviewFragment mediaPreviewFragment = this.mediaPreviewFragment;
            if (mediaPreviewFragment != null) {
                mediaPreviewFragment.cleanUp();
                this.mediaPreviewFragment = null;
            }
        }

        public MediaItem getMediaItemFor(int i) {
            return new MediaItem(null, null, null, this.uri, this.mediaType, -1, true);
        }

        public void pause(int i) {
            MediaPreviewFragment mediaPreviewFragment = this.mediaPreviewFragment;
            if (mediaPreviewFragment != null) {
                mediaPreviewFragment.pause();
            }
        }

        public View getPlaybackControls(int i) {
            MediaPreviewFragment mediaPreviewFragment = this.mediaPreviewFragment;
            if (mediaPreviewFragment != null) {
                return mediaPreviewFragment.getPlaybackControls();
            }
            return null;
        }

        public boolean hasFragmentFor(int i) {
            return this.mediaPreviewFragment != null;
        }
    }

    private static void anchorMarginsToBottomInsets(View view) {
        ViewCompat.setOnApplyWindowInsetsListener(view, new OnApplyWindowInsetsListener() { // from class: org.thoughtcrime.securesms.MediaPreviewActivity$$ExternalSyntheticLambda0
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view2, WindowInsetsCompat windowInsetsCompat) {
                return MediaPreviewActivity.$r8$lambda$Vf_kwa9D9hWcpBEA7zQ4W6ah_OA(view2, windowInsetsCompat);
            }
        });
    }

    public static /* synthetic */ WindowInsetsCompat lambda$anchorMarginsToBottomInsets$6(View view, WindowInsetsCompat windowInsetsCompat) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        marginLayoutParams.setMargins(windowInsetsCompat.getSystemWindowInsetLeft(), marginLayoutParams.topMargin, windowInsetsCompat.getSystemWindowInsetRight(), windowInsetsCompat.getSystemWindowInsetBottom());
        view.setLayoutParams(marginLayoutParams);
        return windowInsetsCompat;
    }

    /* loaded from: classes.dex */
    public static class CursorPagerAdapter extends FragmentStatePagerAdapter implements MediaItemAdapter {
        private boolean active;
        private int autoPlayPosition;
        private final Context context;
        private Cursor cursor;
        private final boolean leftIsRecent;
        private final Map<Integer, MediaPreviewFragment> mediaFragments = new HashMap();

        CursorPagerAdapter(FragmentManager fragmentManager, Context context, Cursor cursor, int i, boolean z) {
            super(fragmentManager, 1);
            this.context = context.getApplicationContext();
            this.cursor = cursor;
            this.autoPlayPosition = i;
            this.leftIsRecent = z;
        }

        public void setActive(boolean z) {
            this.active = z;
            notifyDataSetChanged();
        }

        public void setCursor(Cursor cursor, int i) {
            this.cursor = cursor;
            this.autoPlayPosition = i;
        }

        public int getCount() {
            if (!this.active) {
                return 0;
            }
            return this.cursor.getCount();
        }

        public Fragment getItem(int i) {
            boolean z = this.autoPlayPosition == i;
            int cursorPosition = getCursorPosition(i);
            this.autoPlayPosition = -1;
            this.cursor.moveToPosition(cursorPosition);
            DatabaseAttachment attachment = MediaDatabase.MediaRecord.from(this.context, this.cursor).getAttachment();
            Objects.requireNonNull(attachment);
            MediaPreviewFragment newInstance = MediaPreviewFragment.newInstance(attachment, z);
            this.mediaFragments.put(Integer.valueOf(i), newInstance);
            return newInstance;
        }

        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            MediaPreviewFragment remove = this.mediaFragments.remove(Integer.valueOf(i));
            if (remove != null) {
                remove.cleanUp();
            }
            super.destroyItem(viewGroup, i, obj);
        }

        public MediaItem getMediaItemFor(int i) {
            int cursorPosition = getCursorPosition(i);
            if (this.cursor.isClosed() || cursorPosition < 0) {
                String str = MediaPreviewActivity.TAG;
                Log.w(str, "Invalid cursor state! Closed: " + this.cursor.isClosed() + " Position: " + cursorPosition);
                return null;
            }
            this.cursor.moveToPosition(cursorPosition);
            MediaDatabase.MediaRecord from = MediaDatabase.MediaRecord.from(this.context, this.cursor);
            DatabaseAttachment attachment = from.getAttachment();
            Objects.requireNonNull(attachment);
            RecipientId recipientId = from.getRecipientId();
            RecipientId threadRecipientId = from.getThreadRecipientId();
            Recipient recipient = Recipient.live(recipientId).get();
            Recipient recipient2 = Recipient.live(threadRecipientId).get();
            Uri uri = attachment.getUri();
            Objects.requireNonNull(uri);
            return new MediaItem(recipient, recipient2, attachment, uri, from.getContentType(), from.getDate(), from.isOutgoing());
        }

        public void pause(int i) {
            MediaPreviewFragment mediaPreviewFragment = this.mediaFragments.get(Integer.valueOf(i));
            if (mediaPreviewFragment != null) {
                mediaPreviewFragment.pause();
            }
        }

        public View getPlaybackControls(int i) {
            MediaPreviewFragment mediaPreviewFragment = this.mediaFragments.get(Integer.valueOf(i));
            if (mediaPreviewFragment != null) {
                return mediaPreviewFragment.getPlaybackControls();
            }
            return null;
        }

        public boolean hasFragmentFor(int i) {
            return this.mediaFragments.containsKey(Integer.valueOf(i));
        }

        private int getCursorPosition(int i) {
            if (this.leftIsRecent) {
                return i;
            }
            return (this.cursor.getCount() - 1) - i;
        }
    }

    /* loaded from: classes.dex */
    public static class MediaItem {
        private final DatabaseAttachment attachment;
        private final long date;
        private final boolean outgoing;
        private final Recipient recipient;
        private final Recipient threadRecipient;
        private final String type;
        private final Uri uri;

        private MediaItem(Recipient recipient, Recipient recipient2, DatabaseAttachment databaseAttachment, Uri uri, String str, long j, boolean z) {
            this.recipient = recipient;
            this.threadRecipient = recipient2;
            this.attachment = databaseAttachment;
            this.uri = uri;
            this.type = str;
            this.date = j;
            this.outgoing = z;
        }
    }
}
