package org.thoughtcrime.securesms.jobmanager;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.jobmanager.persistence.DependencySpec;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class JobController$$ExternalSyntheticLambda3 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((DependencySpec) obj).getJobId();
    }
}
