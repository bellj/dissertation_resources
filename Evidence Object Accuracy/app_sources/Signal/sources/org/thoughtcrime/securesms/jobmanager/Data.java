package org.thoughtcrime.securesms.jobmanager;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.thoughtcrime.securesms.util.Base64;

/* loaded from: classes.dex */
public class Data {
    public static final Data EMPTY = new Builder().build();
    @JsonProperty
    private final Map<String, boolean[]> booleanArrays;
    @JsonProperty
    private final Map<String, Boolean> booleans;
    @JsonProperty
    private final Map<String, double[]> doubleArrays;
    @JsonProperty
    private final Map<String, Double> doubles;
    @JsonProperty
    private final Map<String, float[]> floatArrays;
    @JsonProperty
    private final Map<String, Float> floats;
    @JsonProperty
    private final Map<String, int[]> integerArrays;
    @JsonProperty
    private final Map<String, Integer> integers;
    @JsonProperty
    private final Map<String, long[]> longArrays;
    @JsonProperty
    private final Map<String, Long> longs;
    @JsonProperty
    private final Map<String, String[]> stringArrays;
    @JsonProperty
    private final Map<String, String> strings;

    /* loaded from: classes4.dex */
    public interface Serializer {
        Data deserialize(String str);

        String serialize(Data data);
    }

    public Data(@JsonProperty("strings") Map<String, String> map, @JsonProperty("stringArrays") Map<String, String[]> map2, @JsonProperty("integers") Map<String, Integer> map3, @JsonProperty("integerArrays") Map<String, int[]> map4, @JsonProperty("longs") Map<String, Long> map5, @JsonProperty("longArrays") Map<String, long[]> map6, @JsonProperty("floats") Map<String, Float> map7, @JsonProperty("floatArrays") Map<String, float[]> map8, @JsonProperty("doubles") Map<String, Double> map9, @JsonProperty("doubleArrays") Map<String, double[]> map10, @JsonProperty("booleans") Map<String, Boolean> map11, @JsonProperty("booleanArrays") Map<String, boolean[]> map12) {
        this.strings = map;
        this.stringArrays = map2;
        this.integers = map3;
        this.integerArrays = map4;
        this.longs = map5;
        this.longArrays = map6;
        this.floats = map7;
        this.floatArrays = map8;
        this.doubles = map9;
        this.doubleArrays = map10;
        this.booleans = map11;
        this.booleanArrays = map12;
    }

    public boolean hasString(String str) {
        return this.strings.containsKey(str);
    }

    public String getString(String str) {
        throwIfAbsent(this.strings, str);
        return this.strings.get(str);
    }

    public byte[] getStringAsBlob(String str) {
        String string = getString(str);
        if (string != null) {
            return Base64.decodeOrThrow(string);
        }
        return null;
    }

    public String getStringOrDefault(String str, String str2) {
        return hasString(str) ? getString(str) : str2;
    }

    public boolean hasStringArray(String str) {
        return this.stringArrays.containsKey(str);
    }

    public String[] getStringArray(String str) {
        throwIfAbsent(this.stringArrays, str);
        return this.stringArrays.get(str);
    }

    public List<String> getStringArrayAsList(String str) {
        throwIfAbsent(this.stringArrays, str);
        return Arrays.asList(this.stringArrays.get(str));
    }

    public boolean hasInt(String str) {
        return this.integers.containsKey(str);
    }

    public int getInt(String str) {
        throwIfAbsent(this.integers, str);
        return this.integers.get(str).intValue();
    }

    public int getIntOrDefault(String str, int i) {
        return hasInt(str) ? getInt(str) : i;
    }

    public boolean hasIntegerArray(String str) {
        return this.integerArrays.containsKey(str);
    }

    public int[] getIntegerArray(String str) {
        throwIfAbsent(this.integerArrays, str);
        return this.integerArrays.get(str);
    }

    public boolean hasLong(String str) {
        return this.longs.containsKey(str);
    }

    public long getLong(String str) {
        throwIfAbsent(this.longs, str);
        return this.longs.get(str).longValue();
    }

    public long getLongOrDefault(String str, long j) {
        return hasLong(str) ? getLong(str) : j;
    }

    public boolean hasLongArray(String str) {
        return this.longArrays.containsKey(str);
    }

    public long[] getLongArray(String str) {
        throwIfAbsent(this.longArrays, str);
        return this.longArrays.get(str);
    }

    public List<Long> getLongArrayAsList(String str) {
        throwIfAbsent(this.longArrays, str);
        long[] jArr = this.longArrays.get(str);
        Objects.requireNonNull(jArr);
        long[] jArr2 = jArr;
        ArrayList arrayList = new ArrayList(jArr2.length);
        for (long j : jArr2) {
            arrayList.add(Long.valueOf(j));
        }
        return arrayList;
    }

    public boolean hasFloat(String str) {
        return this.floats.containsKey(str);
    }

    public float getFloat(String str) {
        throwIfAbsent(this.floats, str);
        return this.floats.get(str).floatValue();
    }

    public float getFloatOrDefault(String str, float f) {
        return hasFloat(str) ? getFloat(str) : f;
    }

    public boolean hasFloatArray(String str) {
        return this.floatArrays.containsKey(str);
    }

    public float[] getFloatArray(String str) {
        throwIfAbsent(this.floatArrays, str);
        return this.floatArrays.get(str);
    }

    public boolean hasDouble(String str) {
        return this.doubles.containsKey(str);
    }

    public double getDouble(String str) {
        throwIfAbsent(this.doubles, str);
        return this.doubles.get(str).doubleValue();
    }

    public double getDoubleOrDefault(String str, double d) {
        return hasDouble(str) ? getDouble(str) : d;
    }

    public boolean hasDoubleArray(String str) {
        return this.floatArrays.containsKey(str);
    }

    public double[] getDoubleArray(String str) {
        throwIfAbsent(this.doubleArrays, str);
        return this.doubleArrays.get(str);
    }

    public boolean hasBoolean(String str) {
        return this.booleans.containsKey(str);
    }

    public boolean getBoolean(String str) {
        throwIfAbsent(this.booleans, str);
        return this.booleans.get(str).booleanValue();
    }

    public boolean getBooleanOrDefault(String str, boolean z) {
        return hasBoolean(str) ? getBoolean(str) : z;
    }

    public boolean hasBooleanArray(String str) {
        return this.booleanArrays.containsKey(str);
    }

    public boolean[] getBooleanArray(String str) {
        throwIfAbsent(this.booleanArrays, str);
        return this.booleanArrays.get(str);
    }

    private void throwIfAbsent(Map map, String str) {
        if (!map.containsKey(str)) {
            throw new IllegalStateException("Tried to retrieve a value with key '" + str + "', but it wasn't present.");
        }
    }

    public Builder buildUpon() {
        return new Builder();
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final Map<String, boolean[]> booleanArrays;
        private final Map<String, Boolean> booleans;
        private final Map<String, double[]> doubleArrays;
        private final Map<String, Double> doubles;
        private final Map<String, float[]> floatArrays;
        private final Map<String, Float> floats;
        private final Map<String, int[]> integerArrays;
        private final Map<String, Integer> integers;
        private final Map<String, long[]> longArrays;
        private final Map<String, Long> longs;
        private final Map<String, String[]> stringArrays;
        private final Map<String, String> strings;

        public Builder() {
            this.strings = new HashMap();
            this.stringArrays = new HashMap();
            this.integers = new HashMap();
            this.integerArrays = new HashMap();
            this.longs = new HashMap();
            this.longArrays = new HashMap();
            this.floats = new HashMap();
            this.floatArrays = new HashMap();
            this.doubles = new HashMap();
            this.doubleArrays = new HashMap();
            this.booleans = new HashMap();
            this.booleanArrays = new HashMap();
        }

        private Builder(Data data) {
            HashMap hashMap = new HashMap();
            this.strings = hashMap;
            HashMap hashMap2 = new HashMap();
            this.stringArrays = hashMap2;
            HashMap hashMap3 = new HashMap();
            this.integers = hashMap3;
            HashMap hashMap4 = new HashMap();
            this.integerArrays = hashMap4;
            HashMap hashMap5 = new HashMap();
            this.longs = hashMap5;
            HashMap hashMap6 = new HashMap();
            this.longArrays = hashMap6;
            HashMap hashMap7 = new HashMap();
            this.floats = hashMap7;
            HashMap hashMap8 = new HashMap();
            this.floatArrays = hashMap8;
            HashMap hashMap9 = new HashMap();
            this.doubles = hashMap9;
            HashMap hashMap10 = new HashMap();
            this.doubleArrays = hashMap10;
            HashMap hashMap11 = new HashMap();
            this.booleans = hashMap11;
            HashMap hashMap12 = new HashMap();
            this.booleanArrays = hashMap12;
            hashMap.putAll(data.strings);
            hashMap2.putAll(data.stringArrays);
            hashMap3.putAll(data.integers);
            hashMap4.putAll(data.integerArrays);
            hashMap5.putAll(data.longs);
            hashMap6.putAll(data.longArrays);
            hashMap7.putAll(data.floats);
            hashMap8.putAll(data.floatArrays);
            hashMap9.putAll(data.doubles);
            hashMap10.putAll(data.doubleArrays);
            hashMap11.putAll(data.booleans);
            hashMap12.putAll(data.booleanArrays);
        }

        public Builder putString(String str, String str2) {
            this.strings.put(str, str2);
            return this;
        }

        public Builder putStringArray(String str, String[] strArr) {
            this.stringArrays.put(str, strArr);
            return this;
        }

        public Builder putStringListAsArray(String str, List<String> list) {
            this.stringArrays.put(str, (String[]) list.toArray(new String[0]));
            return this;
        }

        public Builder putInt(String str, int i) {
            this.integers.put(str, Integer.valueOf(i));
            return this;
        }

        public Builder putIntArray(String str, int[] iArr) {
            this.integerArrays.put(str, iArr);
            return this;
        }

        public Builder putLong(String str, long j) {
            this.longs.put(str, Long.valueOf(j));
            return this;
        }

        public Builder putLongArray(String str, long[] jArr) {
            this.longArrays.put(str, jArr);
            return this;
        }

        public Builder putLongListAsArray(String str, List<Long> list) {
            long[] jArr = new long[list.size()];
            for (int i = 0; i < list.size(); i++) {
                jArr[i] = list.get(i).longValue();
            }
            this.longArrays.put(str, jArr);
            return this;
        }

        public Builder putFloat(String str, float f) {
            this.floats.put(str, Float.valueOf(f));
            return this;
        }

        public Builder putFloatArray(String str, float[] fArr) {
            this.floatArrays.put(str, fArr);
            return this;
        }

        public Builder putDouble(String str, double d) {
            this.doubles.put(str, Double.valueOf(d));
            return this;
        }

        public Builder putDoubleArray(String str, double[] dArr) {
            this.doubleArrays.put(str, dArr);
            return this;
        }

        public Builder putBoolean(String str, boolean z) {
            this.booleans.put(str, Boolean.valueOf(z));
            return this;
        }

        public Builder putBooleanArray(String str, boolean[] zArr) {
            this.booleanArrays.put(str, zArr);
            return this;
        }

        public Builder putBlobAsString(String str, byte[] bArr) {
            this.strings.put(str, bArr != null ? Base64.encodeBytes(bArr) : null);
            return this;
        }

        public Data build() {
            return new Data(this.strings, this.stringArrays, this.integers, this.integerArrays, this.longs, this.longArrays, this.floats, this.floatArrays, this.doubles, this.doubleArrays, this.booleans, this.booleanArrays);
        }
    }
}
