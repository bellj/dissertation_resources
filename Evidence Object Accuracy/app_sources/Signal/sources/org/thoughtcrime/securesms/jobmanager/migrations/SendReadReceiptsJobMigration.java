package org.thoughtcrime.securesms.jobmanager.migrations;

import java.util.TreeSet;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.JobMigration;
import org.thoughtcrime.securesms.jobs.FailingJob;
import org.thoughtcrime.securesms.jobs.SendReadReceiptJob;
import org.thoughtcrime.securesms.notifications.DeleteNotificationReceiver;

/* loaded from: classes4.dex */
public class SendReadReceiptsJobMigration extends JobMigration {
    private final MmsSmsDatabase mmsSmsDatabase;

    public SendReadReceiptsJobMigration(MmsSmsDatabase mmsSmsDatabase) {
        super(5);
        this.mmsSmsDatabase = mmsSmsDatabase;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.JobMigration
    public JobMigration.JobData migrate(JobMigration.JobData jobData) {
        return SendReadReceiptJob.KEY.equals(jobData.getFactoryKey()) ? migrateSendReadReceiptJob(this.mmsSmsDatabase, jobData) : jobData;
    }

    private static JobMigration.JobData migrateSendReadReceiptJob(MmsSmsDatabase mmsSmsDatabase, JobMigration.JobData jobData) {
        Data data = jobData.getData();
        if (data.hasLong(ThreadDatabase.TABLE_NAME)) {
            return jobData;
        }
        long[] longArray = jobData.getData().getLongArray(DeleteNotificationReceiver.EXTRA_IDS);
        TreeSet treeSet = new TreeSet();
        for (long j : longArray) {
            long threadForMessageId = mmsSmsDatabase.getThreadForMessageId(j);
            if (j != -1) {
                treeSet.add(Long.valueOf(threadForMessageId));
            }
        }
        if (treeSet.size() != 1) {
            return new JobMigration.JobData(FailingJob.KEY, null, new Data.Builder().build());
        }
        return jobData.withData(data.buildUpon().putLong(ThreadDatabase.TABLE_NAME, ((Long) treeSet.first()).longValue()).build());
    }
}
