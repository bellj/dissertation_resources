package org.thoughtcrime.securesms.jobmanager;

import java.util.HashMap;
import java.util.Map;
import org.thoughtcrime.securesms.jobmanager.Job;

/* loaded from: classes4.dex */
public class JobInstantiator {
    private final Map<String, Job.Factory> jobFactories;

    public JobInstantiator(Map<String, Job.Factory> map) {
        this.jobFactories = new HashMap(map);
    }

    public Job instantiate(String str, Job.Parameters parameters, Data data) {
        Job.Factory factory = this.jobFactories.get(str);
        if (factory != null) {
            Job create = factory.create(parameters, data);
            if (create.getId().equals(parameters.getId())) {
                return create;
            }
            throw new AssertionError("Parameters not supplied to job during creation");
        }
        throw new IllegalStateException("Tried to instantiate a job with key '" + str + "', but no matching factory was found.");
    }
}
