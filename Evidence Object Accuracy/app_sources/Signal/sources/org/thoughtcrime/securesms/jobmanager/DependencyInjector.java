package org.thoughtcrime.securesms.jobmanager;

/* loaded from: classes4.dex */
public interface DependencyInjector {
    void injectDependencies(Object obj);
}
