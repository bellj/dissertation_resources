package org.thoughtcrime.securesms.jobmanager;

import android.app.Application;
import android.os.PowerManager;
import com.annimon.stream.Stream;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.util.WakeLockUtil;

/* loaded from: classes4.dex */
public class JobRunner extends Thread {
    private static final String TAG = Log.tag(JobRunner.class);
    private static long WAKE_LOCK_TIMEOUT = TimeUnit.MINUTES.toMillis(10);
    private final Application application;
    private final int id;
    private final JobController jobController;
    private final JobPredicate jobPredicate;

    public JobRunner(Application application, int i, JobController jobController, JobPredicate jobPredicate) {
        super("signal-JobRunner-" + i);
        this.application = application;
        this.id = i;
        this.jobController = jobController;
        this.jobPredicate = jobPredicate;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public synchronized void run() {
        while (true) {
            Job pullNextEligibleJobForExecution = this.jobController.pullNextEligibleJobForExecution(this.jobPredicate);
            Job.Result run = run(pullNextEligibleJobForExecution);
            this.jobController.onJobFinished(pullNextEligibleJobForExecution);
            if (run.isSuccess()) {
                this.jobController.onSuccess(pullNextEligibleJobForExecution, run.getOutputData());
            } else if (run.isRetry()) {
                this.jobController.onRetry(pullNextEligibleJobForExecution, run.getBackoffInterval());
                pullNextEligibleJobForExecution.onRetry();
            } else if (run.isFailure()) {
                List<Job> onFailure = this.jobController.onFailure(pullNextEligibleJobForExecution);
                pullNextEligibleJobForExecution.onFailure();
                Stream.of(onFailure).forEach(new JobController$$ExternalSyntheticLambda0());
                if (run.getException() != null) {
                    throw run.getException();
                }
            } else {
                throw new AssertionError("Invalid job result!");
            }
        }
    }

    private Job.Result run(Job job) {
        long currentTimeMillis = System.currentTimeMillis();
        String str = TAG;
        Log.i(str, JobLogger.format(job, String.valueOf(this.id), "Running job."));
        if (isJobExpired(job)) {
            Log.w(str, JobLogger.format(job, String.valueOf(this.id), "Failing after surpassing its lifespan."));
            return Job.Result.failure();
        }
        PowerManager.WakeLock wakeLock = null;
        try {
            try {
                wakeLock = WakeLockUtil.acquire(this.application, 1, WAKE_LOCK_TIMEOUT, job.getId());
                Job.Result run = job.run();
                if (job.isCanceled()) {
                    Log.w(str, JobLogger.format(job, String.valueOf(this.id), "Failing because the job was canceled."));
                    run = Job.Result.failure();
                }
                if (wakeLock != null) {
                    WakeLockUtil.release(wakeLock, job.getId());
                }
                printResult(job, run, currentTimeMillis);
                if (!run.isRetry() || job.getRunAttempt() + 1 < job.getParameters().getMaxAttempts() || job.getParameters().getMaxAttempts() == -1) {
                    return run;
                }
                Log.w(str, JobLogger.format(job, String.valueOf(this.id), "Failing after surpassing its max number of attempts."));
                return Job.Result.failure();
            } catch (Exception e) {
                Log.w(TAG, JobLogger.format(job, String.valueOf(this.id), "Failing due to an unexpected exception."), e);
                Job.Result failure = Job.Result.failure();
                if (wakeLock != null) {
                    WakeLockUtil.release(wakeLock, job.getId());
                }
                return failure;
            }
        } catch (Throwable th) {
            if (wakeLock != null) {
                WakeLockUtil.release(wakeLock, job.getId());
            }
            throw th;
        }
    }

    private boolean isJobExpired(Job job) {
        long createTime = job.getParameters().getCreateTime() + job.getParameters().getLifespan();
        if (createTime < 0) {
            createTime = Long.MAX_VALUE;
        }
        return job.getParameters().getLifespan() != -1 && createTime <= System.currentTimeMillis();
    }

    private void printResult(Job job, Job.Result result, long j) {
        if (result.getException() != null) {
            Log.e(TAG, JobLogger.format(job, String.valueOf(this.id), "Job failed with a fatal exception. Crash imminent."));
        } else if (result.isFailure()) {
            Log.w(TAG, JobLogger.format(job, String.valueOf(this.id), "Job failed."));
        } else {
            String str = TAG;
            String valueOf = String.valueOf(this.id);
            Log.i(str, JobLogger.format(job, valueOf, "Job finished with result " + result + " in " + (System.currentTimeMillis() - j) + " ms."));
        }
    }
}
