package org.thoughtcrime.securesms.jobmanager.migrations;

import org.thoughtcrime.securesms.database.PushDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.JobMigration;
import org.thoughtcrime.securesms.jobs.FailingJob;
import org.thoughtcrime.securesms.jobs.RequestGroupInfoJob;
import org.thoughtcrime.securesms.jobs.SendDeliveryReceiptJob;

/* loaded from: classes4.dex */
public class RecipientIdFollowUpJobMigration extends JobMigration {
    public RecipientIdFollowUpJobMigration() {
        this(3);
    }

    public RecipientIdFollowUpJobMigration(int i) {
        super(i);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.JobMigration
    public JobMigration.JobData migrate(JobMigration.JobData jobData) {
        String factoryKey = jobData.getFactoryKey();
        factoryKey.hashCode();
        if (factoryKey.equals(RequestGroupInfoJob.KEY)) {
            return migrateRequestGroupInfoJob(jobData);
        }
        if (!factoryKey.equals(SendDeliveryReceiptJob.KEY)) {
            return jobData;
        }
        return migrateSendDeliveryReceiptJob(jobData);
    }

    private static JobMigration.JobData migrateRequestGroupInfoJob(JobMigration.JobData jobData) {
        if (!jobData.getData().hasString(PushDatabase.SOURCE_E164) || !jobData.getData().hasString("group_id")) {
            return failingJobData();
        }
        return jobData;
    }

    private static JobMigration.JobData migrateSendDeliveryReceiptJob(JobMigration.JobData jobData) {
        if (!jobData.getData().hasString(RecipientDatabase.TABLE_NAME) || !jobData.getData().hasLong("message_id") || !jobData.getData().hasLong("timestamp")) {
            return failingJobData();
        }
        return jobData;
    }

    private static JobMigration.JobData failingJobData() {
        return new JobMigration.JobData(FailingJob.KEY, null, new Data.Builder().build());
    }
}
