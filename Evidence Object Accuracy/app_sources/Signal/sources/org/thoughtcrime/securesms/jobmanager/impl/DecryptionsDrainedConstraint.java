package org.thoughtcrime.securesms.jobmanager.impl;

import android.app.job.JobInfo;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Constraint;

/* loaded from: classes4.dex */
public final class DecryptionsDrainedConstraint implements Constraint {
    public static final String KEY;

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public void applyToJobInfo(JobInfo.Builder builder) {
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public /* synthetic */ String getJobSchedulerKeyPart() {
        return Constraint.CC.$default$getJobSchedulerKeyPart(this);
    }

    private DecryptionsDrainedConstraint() {
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public boolean isMet() {
        return ApplicationDependencies.getIncomingMessageObserver().isDecryptionDrained();
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Constraint.Factory<DecryptionsDrainedConstraint> {
        @Override // org.thoughtcrime.securesms.jobmanager.Constraint.Factory
        public DecryptionsDrainedConstraint create() {
            return new DecryptionsDrainedConstraint();
        }
    }
}
