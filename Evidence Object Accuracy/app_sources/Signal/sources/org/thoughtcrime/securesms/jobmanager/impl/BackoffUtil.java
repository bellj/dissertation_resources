package org.thoughtcrime.securesms.jobmanager.impl;

/* loaded from: classes4.dex */
public final class BackoffUtil {
    private BackoffUtil() {
    }

    public static long exponentialBackoff(int i, long j) {
        if (i >= 1) {
            double min = (double) Math.min(((long) Math.pow(2.0d, (double) Math.min(i, 30))) * 1000, j);
            Double.isNaN(min);
            return (long) (min * ((Math.random() * 0.5d) + 0.75d));
        }
        throw new IllegalArgumentException("Bad attempt count! " + i);
    }
}
