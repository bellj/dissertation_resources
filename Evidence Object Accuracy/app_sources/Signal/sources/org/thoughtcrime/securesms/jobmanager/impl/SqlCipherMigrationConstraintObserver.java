package org.thoughtcrime.securesms.jobmanager.impl;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.ConstraintObserver;

/* loaded from: classes.dex */
public class SqlCipherMigrationConstraintObserver implements ConstraintObserver {
    private static final String REASON = Log.tag(SqlCipherMigrationConstraintObserver.class);
    private ConstraintObserver.Notifier notifier;

    /* loaded from: classes.dex */
    public static class SqlCipherNeedsMigrationEvent {
    }

    public SqlCipherMigrationConstraintObserver() {
        EventBus.getDefault().register(this);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.ConstraintObserver
    public void register(ConstraintObserver.Notifier notifier) {
        this.notifier = notifier;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SqlCipherNeedsMigrationEvent sqlCipherNeedsMigrationEvent) {
        ConstraintObserver.Notifier notifier = this.notifier;
        if (notifier != null) {
            notifier.onConstraintMet(REASON);
        }
    }
}
