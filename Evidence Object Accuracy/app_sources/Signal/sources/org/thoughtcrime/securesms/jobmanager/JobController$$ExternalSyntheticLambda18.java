package org.thoughtcrime.securesms.jobmanager;

import com.annimon.stream.function.Function;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class JobController$$ExternalSyntheticLambda18 implements Function {
    public final /* synthetic */ ConstraintInstantiator f$0;

    public /* synthetic */ JobController$$ExternalSyntheticLambda18(ConstraintInstantiator constraintInstantiator) {
        this.f$0 = constraintInstantiator;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return this.f$0.instantiate((String) obj);
    }
}
