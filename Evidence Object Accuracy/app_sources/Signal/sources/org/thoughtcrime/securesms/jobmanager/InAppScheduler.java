package org.thoughtcrime.securesms.jobmanager;

import android.os.Handler;
import android.os.HandlerThread;
import com.annimon.stream.Stream;
import java.util.List;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class InAppScheduler implements Scheduler {
    private static final String TAG = Log.tag(InAppScheduler.class);
    private final Handler handler;
    private final JobManager jobManager;

    public InAppScheduler(JobManager jobManager) {
        HandlerThread handlerThread = new HandlerThread("InAppScheduler");
        handlerThread.start();
        this.jobManager = jobManager;
        this.handler = new Handler(handlerThread.getLooper());
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Scheduler
    public void schedule(long j, List<Constraint> list) {
        if (j > 0 && Stream.of(list).allMatch(new AlarmManagerScheduler$$ExternalSyntheticLambda0())) {
            String str = TAG;
            Log.i(str, "Scheduling a retry in " + j + " ms.");
            this.handler.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.jobmanager.InAppScheduler$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    InAppScheduler.$r8$lambda$xSEljw1ctVJEFtMAagZlOxAC9OU(InAppScheduler.this);
                }
            }, j);
        }
    }

    public /* synthetic */ void lambda$schedule$0() {
        Log.i(TAG, "Triggering a job retry.");
        this.jobManager.wakeUp();
    }
}
