package org.thoughtcrime.securesms.jobmanager;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.annimon.stream.Stream;
import java.util.List;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* loaded from: classes4.dex */
public class AlarmManagerScheduler implements Scheduler {
    private static final String TAG = Log.tag(AlarmManagerScheduler.class);
    private final Application application;

    public AlarmManagerScheduler(Application application) {
        this.application = application;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Scheduler
    public void schedule(long j, List<Constraint> list) {
        if (j > 0 && Stream.of(list).allMatch(new AlarmManagerScheduler$$ExternalSyntheticLambda0())) {
            setUniqueAlarm(this.application, System.currentTimeMillis() + j);
        }
    }

    private void setUniqueAlarm(Context context, long j) {
        Intent intent = new Intent(context, RetryReceiver.class);
        intent.setAction("org.thoughtcrime.securesms" + UUID.randomUUID().toString());
        ((AlarmManager) context.getSystemService("alarm")).set(0, j, PendingIntent.getBroadcast(context, 0, intent, 0));
        String str = TAG;
        Log.i(str, "Set an alarm to retry a job in " + (j - System.currentTimeMillis()) + " ms.");
    }

    /* loaded from: classes4.dex */
    public static class RetryReceiver extends BroadcastReceiver {
        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Log.i(AlarmManagerScheduler.TAG, "Received an alarm to retry a job.");
            ApplicationDependencies.getJobManager().wakeUp();
        }
    }
}
