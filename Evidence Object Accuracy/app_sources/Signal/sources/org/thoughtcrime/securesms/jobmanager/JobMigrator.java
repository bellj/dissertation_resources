package org.thoughtcrime.securesms.jobmanager;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.JobMigration;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;
import org.thoughtcrime.securesms.jobmanager.persistence.JobStorage;

/* loaded from: classes4.dex */
public class JobMigrator {
    static final /* synthetic */ boolean $assertionsDisabled;
    private static final String TAG = Log.tag(JobMigrator.class);
    private final int currentVersion;
    private final int lastSeenVersion;
    private final Map<Integer, JobMigration> migrations = new HashMap();

    public JobMigrator(int i, int i2, List<JobMigration> list) {
        this.lastSeenVersion = i;
        this.currentVersion = i2;
        if (list.size() == i2 - 1) {
            for (int i3 = 0; i3 < list.size(); i3++) {
                JobMigration jobMigration = list.get(i3);
                int i4 = i3 + 2;
                if (jobMigration.getEndVersion() == i4) {
                    this.migrations.put(Integer.valueOf(jobMigration.getEndVersion()), list.get(i3));
                } else {
                    throw new AssertionError("Missing migration for version " + i4 + "!");
                }
            }
            return;
        }
        throw new AssertionError("You must have a migration for every version!");
    }

    public int migrate(JobStorage jobStorage, Data.Serializer serializer) {
        List<JobSpec> allJobSpecs = jobStorage.getAllJobSpecs();
        int i = this.lastSeenVersion;
        while (i < this.currentVersion) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Migrating from ");
            sb.append(i);
            sb.append(" to ");
            i++;
            sb.append(i);
            Log.i(str, sb.toString());
            ListIterator<JobSpec> listIterator = allJobSpecs.listIterator();
            JobMigration jobMigration = this.migrations.get(Integer.valueOf(i));
            while (listIterator.hasNext()) {
                JobSpec next = listIterator.next();
                JobMigration.JobData migrate = jobMigration.migrate(new JobMigration.JobData(next.getFactoryKey(), next.getQueueKey(), serializer.deserialize(next.getSerializedData())));
                listIterator.set(new JobSpec(next.getId(), migrate.getFactoryKey(), migrate.getQueueKey(), next.getCreateTime(), next.getNextRunAttemptTime(), next.getRunAttempt(), next.getMaxAttempts(), next.getLifespan(), serializer.serialize(migrate.getData()), next.getSerializedInputData(), next.isRunning(), next.isMemoryOnly()));
            }
        }
        jobStorage.updateJobs(allJobSpecs);
        return this.currentVersion;
    }
}
