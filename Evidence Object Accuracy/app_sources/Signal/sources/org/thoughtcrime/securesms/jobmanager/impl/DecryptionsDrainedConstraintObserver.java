package org.thoughtcrime.securesms.jobmanager.impl;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.ConstraintObserver;

/* loaded from: classes4.dex */
public class DecryptionsDrainedConstraintObserver implements ConstraintObserver {
    private static final String REASON = Log.tag(DecryptionsDrainedConstraintObserver.class);

    @Override // org.thoughtcrime.securesms.jobmanager.ConstraintObserver
    public void register(ConstraintObserver.Notifier notifier) {
        ApplicationDependencies.getIncomingMessageObserver().addDecryptionDrainedListener(new Runnable() { // from class: org.thoughtcrime.securesms.jobmanager.impl.DecryptionsDrainedConstraintObserver$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                DecryptionsDrainedConstraintObserver.$r8$lambda$JF4Q3_07vPHkmSTSDrQB6EVmWaU(ConstraintObserver.Notifier.this);
            }
        });
    }

    public static /* synthetic */ void lambda$register$0(ConstraintObserver.Notifier notifier) {
        notifier.onConstraintMet(REASON);
    }
}
