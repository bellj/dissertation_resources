package org.thoughtcrime.securesms.jobmanager;

import java.util.Arrays;
import java.util.List;

/* loaded from: classes4.dex */
public class CompositeScheduler implements Scheduler {
    private final List<Scheduler> schedulers;

    public CompositeScheduler(Scheduler... schedulerArr) {
        this.schedulers = Arrays.asList(schedulerArr);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Scheduler
    public void schedule(long j, List<Constraint> list) {
        for (Scheduler scheduler : this.schedulers) {
            scheduler.schedule(j, list);
        }
    }
}
