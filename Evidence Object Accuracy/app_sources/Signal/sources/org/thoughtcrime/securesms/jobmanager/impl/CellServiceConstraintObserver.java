package org.thoughtcrime.securesms.jobmanager.impl;

import android.app.Application;
import android.os.Build;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyCallback;
import android.telephony.TelephonyManager;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.ConstraintObserver;
import org.thoughtcrime.securesms.jobmanager.impl.CellServiceConstraintObserver;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
public class CellServiceConstraintObserver implements ConstraintObserver {
    private static final String REASON = Log.tag(CellServiceConstraintObserver.class);
    private static final String TAG = Log.tag(CellServiceConstraintObserver.class);
    private static volatile CellServiceConstraintObserver instance;
    private volatile ServiceState lastKnownState;
    private volatile ConstraintObserver.Notifier notifier;

    public static CellServiceConstraintObserver getInstance(Application application) {
        if (instance == null) {
            synchronized (CellServiceConstraintObserver.class) {
                if (instance == null) {
                    instance = new CellServiceConstraintObserver(application);
                }
            }
        }
        return instance;
    }

    private CellServiceConstraintObserver(Application application) {
        TelephonyManager telephonyManager = ServiceUtil.getTelephonyManager(application);
        LegacyServiceStateListener legacyServiceStateListener = new LegacyServiceStateListener();
        if (Build.VERSION.SDK_INT >= 31) {
            telephonyManager.registerTelephonyCallback(SignalExecutors.BOUNDED, new ServiceStateListenerApi31());
        } else {
            SignalExecutors.BOUNDED.execute(new Runnable(telephonyManager, legacyServiceStateListener) { // from class: org.thoughtcrime.securesms.jobmanager.impl.CellServiceConstraintObserver$$ExternalSyntheticLambda0
                public final /* synthetic */ TelephonyManager f$0;
                public final /* synthetic */ CellServiceConstraintObserver.LegacyServiceStateListener f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    CellServiceConstraintObserver.m1902$r8$lambda$pEzQwFkmzCEgvr427ofHYZrrY(this.f$0, this.f$1);
                }
            });
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.ConstraintObserver
    public void register(ConstraintObserver.Notifier notifier) {
        this.notifier = notifier;
    }

    public boolean hasService() {
        return this.lastKnownState != null && this.lastKnownState.getState() == 0;
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class ServiceStateListenerApi31 extends TelephonyCallback implements TelephonyCallback.ServiceStateListener {
        private ServiceStateListenerApi31() {
            CellServiceConstraintObserver.this = r1;
        }

        public void onServiceStateChanged(ServiceState serviceState) {
            CellServiceConstraintObserver.this.lastKnownState = serviceState;
            if (serviceState.getState() == 0) {
                String str = CellServiceConstraintObserver.TAG;
                Log.i(str, "[API " + Build.VERSION.SDK_INT + "] Cell service available.");
                if (CellServiceConstraintObserver.this.notifier != null) {
                    CellServiceConstraintObserver.this.notifier.onConstraintMet(CellServiceConstraintObserver.REASON);
                    return;
                }
                return;
            }
            String str2 = CellServiceConstraintObserver.TAG;
            Log.w(str2, "[API " + Build.VERSION.SDK_INT + "] Cell service unavailable. State: " + serviceState.getState());
        }
    }

    /* loaded from: classes4.dex */
    public class LegacyServiceStateListener extends PhoneStateListener {
        LegacyServiceStateListener() {
            CellServiceConstraintObserver.this = r1;
        }

        @Override // android.telephony.PhoneStateListener
        public void onServiceStateChanged(ServiceState serviceState) {
            CellServiceConstraintObserver.this.lastKnownState = serviceState;
            if (serviceState.getState() == 0) {
                String str = CellServiceConstraintObserver.TAG;
                Log.i(str, "[API " + Build.VERSION.SDK_INT + "] Cell service available.");
                if (CellServiceConstraintObserver.this.notifier != null) {
                    CellServiceConstraintObserver.this.notifier.onConstraintMet(CellServiceConstraintObserver.REASON);
                    return;
                }
                return;
            }
            String str2 = CellServiceConstraintObserver.TAG;
            Log.w(str2, "[API " + Build.VERSION.SDK_INT + "] Cell service unavailable. State: " + serviceState.getState());
        }
    }
}
