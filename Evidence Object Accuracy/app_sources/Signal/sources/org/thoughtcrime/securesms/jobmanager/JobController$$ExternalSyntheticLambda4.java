package org.thoughtcrime.securesms.jobmanager;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.jobmanager.persistence.JobStorage;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class JobController$$ExternalSyntheticLambda4 implements Function {
    public final /* synthetic */ JobStorage f$0;

    public /* synthetic */ JobController$$ExternalSyntheticLambda4(JobStorage jobStorage) {
        this.f$0 = jobStorage;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return this.f$0.getJobSpec((String) obj);
    }
}
