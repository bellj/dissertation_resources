package org.thoughtcrime.securesms.jobmanager.persistence;

import java.util.List;
import java.util.Objects;

/* loaded from: classes4.dex */
public final class FullSpec {
    private final List<ConstraintSpec> constraintSpecs;
    private final List<DependencySpec> dependencySpecs;
    private final JobSpec jobSpec;

    public FullSpec(JobSpec jobSpec, List<ConstraintSpec> list, List<DependencySpec> list2) {
        this.jobSpec = jobSpec;
        this.constraintSpecs = list;
        this.dependencySpecs = list2;
    }

    public JobSpec getJobSpec() {
        return this.jobSpec;
    }

    public List<ConstraintSpec> getConstraintSpecs() {
        return this.constraintSpecs;
    }

    public List<DependencySpec> getDependencySpecs() {
        return this.dependencySpecs;
    }

    public boolean isMemoryOnly() {
        return this.jobSpec.isMemoryOnly();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || FullSpec.class != obj.getClass()) {
            return false;
        }
        FullSpec fullSpec = (FullSpec) obj;
        if (!Objects.equals(this.jobSpec, fullSpec.jobSpec) || !Objects.equals(this.constraintSpecs, fullSpec.constraintSpecs) || !Objects.equals(this.dependencySpecs, fullSpec.dependencySpecs)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.jobSpec, this.constraintSpecs, this.dependencySpecs);
    }
}
