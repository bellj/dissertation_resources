package org.thoughtcrime.securesms.jobmanager.persistence;

import java.util.Locale;
import java.util.Objects;

/* loaded from: classes4.dex */
public final class DependencySpec {
    private final String dependsOnJobId;
    private final String jobId;
    private final boolean memoryOnly;

    public DependencySpec(String str, String str2, boolean z) {
        this.jobId = str;
        this.dependsOnJobId = str2;
        this.memoryOnly = z;
    }

    public String getJobId() {
        return this.jobId;
    }

    public String getDependsOnJobId() {
        return this.dependsOnJobId;
    }

    public boolean isMemoryOnly() {
        return this.memoryOnly;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || DependencySpec.class != obj.getClass()) {
            return false;
        }
        DependencySpec dependencySpec = (DependencySpec) obj;
        if (!Objects.equals(this.jobId, dependencySpec.jobId) || !Objects.equals(this.dependsOnJobId, dependencySpec.dependsOnJobId) || this.memoryOnly != dependencySpec.memoryOnly) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.jobId, this.dependsOnJobId, Boolean.valueOf(this.memoryOnly));
    }

    public String toString() {
        return String.format(Locale.US, "jobSpecId: JOB::%s | dependsOnJobSpecId: JOB::%s | memoryOnly: %b", this.jobId, this.dependsOnJobId, Boolean.valueOf(this.memoryOnly));
    }
}
