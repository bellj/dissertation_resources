package org.thoughtcrime.securesms.jobmanager.migrations;

import android.content.Context;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.JobMigration;
import org.thoughtcrime.securesms.jobs.PushProcessMessageJob;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.whispersystems.signalservice.api.messages.SignalServiceContent;

/* loaded from: classes4.dex */
public class PushProcessMessageQueueJobMigration extends JobMigration {
    private static final String TAG = Log.tag(PushProcessMessageQueueJobMigration.class);
    private final Context context;

    public PushProcessMessageQueueJobMigration(Context context) {
        super(6);
        this.context = context;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.JobMigration
    public JobMigration.JobData migrate(JobMigration.JobData jobData) {
        if (PushProcessMessageJob.KEY.equals(jobData.getFactoryKey())) {
            Log.i(TAG, "Found a PushProcessMessageJob to migrate.");
            try {
                return migratePushProcessMessageJob(this.context, jobData);
            } catch (IOException e) {
                Log.w(TAG, "Failed to migrate message job.", e);
            }
        }
        return jobData;
    }

    private static JobMigration.JobData migratePushProcessMessageJob(Context context, JobMigration.JobData jobData) throws IOException {
        String str;
        Data data = jobData.getData();
        String str2 = "";
        if (data.getInt("message_state") == 0) {
            SignalServiceContent deserialize = SignalServiceContent.deserialize(Base64.decode(data.getString("message_content")));
            if (deserialize != null && deserialize.getDataMessage().isPresent() && deserialize.getDataMessage().get().getGroupContext().isPresent()) {
                Log.i(TAG, "Migrating a group message.");
                try {
                    str = Recipient.externalGroupExact(GroupUtil.idFromGroupContext(deserialize.getDataMessage().get().getGroupContext().get())).getId().toQueueKey();
                } catch (BadGroupIdException unused) {
                    Log.w(TAG, "Bad groupId! Using default queue.");
                }
            } else if (deserialize != null) {
                Log.i(TAG, "Migrating an individual message.");
                str = RecipientId.from(deserialize.getSender()).toQueueKey();
            }
            str2 = str;
        } else {
            Log.i(TAG, "Migrating an exception message.");
            String string = data.getString("exception_sender");
            GroupId parseNullableOrThrow = GroupId.parseNullableOrThrow(data.getStringOrDefault("exception_groupId", null));
            if (parseNullableOrThrow != null) {
                str2 = Recipient.externalGroupExact(parseNullableOrThrow).getId().toQueueKey();
            } else if (string != null) {
                str2 = Recipient.external(context, string).getId().toQueueKey();
            }
        }
        return jobData.withQueueKey(PushProcessMessageJob.QUEUE_PREFIX + str2);
    }
}
