package org.thoughtcrime.securesms.jobmanager.persistence;

import java.util.Locale;
import java.util.Objects;

/* loaded from: classes4.dex */
public final class ConstraintSpec {
    private final String factoryKey;
    private final String jobSpecId;
    private final boolean memoryOnly;

    public ConstraintSpec(String str, String str2, boolean z) {
        this.jobSpecId = str;
        this.factoryKey = str2;
        this.memoryOnly = z;
    }

    public String getJobSpecId() {
        return this.jobSpecId;
    }

    public String getFactoryKey() {
        return this.factoryKey;
    }

    public boolean isMemoryOnly() {
        return this.memoryOnly;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ConstraintSpec.class != obj.getClass()) {
            return false;
        }
        ConstraintSpec constraintSpec = (ConstraintSpec) obj;
        if (!Objects.equals(this.jobSpecId, constraintSpec.jobSpecId) || !Objects.equals(this.factoryKey, constraintSpec.factoryKey) || this.memoryOnly != constraintSpec.memoryOnly) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.jobSpecId, this.factoryKey, Boolean.valueOf(this.memoryOnly));
    }

    public String toString() {
        return String.format(Locale.US, "jobSpecId: JOB::%s | factoryKey: %s | memoryOnly: %b", this.jobSpecId, this.factoryKey, Boolean.valueOf(this.memoryOnly));
    }
}
