package org.thoughtcrime.securesms.jobmanager.impl;

import android.app.job.JobInfo;
import org.greenrobot.eventbus.EventBus;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.jobmanager.Constraint;

/* loaded from: classes4.dex */
public final class NotInCallConstraint implements Constraint {
    public static final String KEY;

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public void applyToJobInfo(JobInfo.Builder builder) {
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public /* synthetic */ String getJobSchedulerKeyPart() {
        return Constraint.CC.$default$getJobSchedulerKeyPart(this);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public boolean isMet() {
        return isNotInConnectedCall();
    }

    public static boolean isNotInConnectedCall() {
        WebRtcViewModel webRtcViewModel = (WebRtcViewModel) EventBus.getDefault().getStickyEvent(WebRtcViewModel.class);
        return webRtcViewModel == null || !(webRtcViewModel.getState() == WebRtcViewModel.State.CALL_CONNECTED || webRtcViewModel.getState() == WebRtcViewModel.State.CALL_RECONNECTING);
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Constraint.Factory<NotInCallConstraint> {
        @Override // org.thoughtcrime.securesms.jobmanager.Constraint.Factory
        public NotInCallConstraint create() {
            return new NotInCallConstraint();
        }
    }
}
