package org.thoughtcrime.securesms.jobmanager;

import android.app.Application;
import android.content.Intent;
import android.os.Build;
import j$.util.Optional;
import j$.util.function.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.webrtc.SurfaceTextureEglRenderer$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.jobmanager.Constraint;
import org.thoughtcrime.securesms.jobmanager.ConstraintObserver;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobController;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobmanager.impl.DefaultExecutorFactory;
import org.thoughtcrime.securesms.jobmanager.impl.JsonDataSerializer;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;
import org.thoughtcrime.securesms.jobmanager.persistence.JobStorage;
import org.thoughtcrime.securesms.util.Debouncer;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.concurrent.FilteredExecutor;

/* loaded from: classes.dex */
public class JobManager implements ConstraintObserver.Notifier {
    public static final int CURRENT_VERSION;
    private static final String TAG = Log.tag(JobManager.class);
    private final Application application;
    private final Configuration configuration;
    private final Set<EmptyQueueListener> emptyQueueListeners = new CopyOnWriteArraySet();
    private final Executor executor;
    private volatile boolean initialized;
    private final JobController jobController;
    private final JobTracker jobTracker;

    /* loaded from: classes4.dex */
    public interface EmptyQueueListener {
        void onQueueEmpty();
    }

    public JobManager(Application application, Configuration configuration) {
        Scheduler scheduler;
        this.application = application;
        this.configuration = configuration;
        Executor trace = ThreadUtil.trace(new FilteredExecutor(configuration.getExecutorFactory().newSingleThreadExecutor("signal-JobManager"), new FilteredExecutor.Filter() { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.util.concurrent.FilteredExecutor.Filter
            public final boolean shouldRunOnExecutor() {
                return ThreadUtil.isMainThread();
            }
        }));
        this.executor = trace;
        this.jobTracker = configuration.getJobTracker();
        JobStorage jobStorage = configuration.getJobStorage();
        JobInstantiator jobInstantiator = configuration.getJobInstantiator();
        ConstraintInstantiator constraintFactories = configuration.getConstraintFactories();
        Data.Serializer dataSerializer = configuration.getDataSerializer();
        JobTracker jobTracker = configuration.getJobTracker();
        if (Build.VERSION.SDK_INT < 26) {
            scheduler = new AlarmManagerScheduler(application);
        } else {
            scheduler = new CompositeScheduler(new InAppScheduler(this), new JobSchedulerScheduler(application));
        }
        this.jobController = new JobController(application, jobStorage, jobInstantiator, constraintFactories, dataSerializer, jobTracker, scheduler, new Debouncer(500), new JobController.Callback() { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda7
            @Override // org.thoughtcrime.securesms.jobmanager.JobController.Callback
            public final void onEmpty() {
                JobManager.this.onEmptyQueue();
            }
        });
        trace.execute(new Runnable(configuration, application) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda8
            public final /* synthetic */ JobManager.Configuration f$1;
            public final /* synthetic */ Application f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$new$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(Configuration configuration, Application application) {
        synchronized (this) {
            JobStorage jobStorage = configuration.getJobStorage();
            jobStorage.init();
            TextSecurePreferences.setJobManagerVersion(application, configuration.getJobMigrator().migrate(jobStorage, configuration.getDataSerializer()));
            this.jobController.init();
            for (ConstraintObserver constraintObserver : configuration.getConstraintObservers()) {
                constraintObserver.register(this);
            }
            if (Build.VERSION.SDK_INT < 26) {
                application.startService(new Intent(application, KeepAliveService.class));
            }
            this.initialized = true;
            notifyAll();
        }
    }

    public void beginJobLoop() {
        runOnExecutor(new Runnable() { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda9
            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$beginJobLoop$1();
            }
        });
    }

    public /* synthetic */ void lambda$beginJobLoop$1() {
        int i = 0;
        for (int i2 = 0; i2 < this.configuration.getJobThreadCount(); i2++) {
            i++;
            new JobRunner(this.application, i, this.jobController, JobPredicate.NONE).start();
        }
        for (JobPredicate jobPredicate : this.configuration.getReservedJobRunners()) {
            i++;
            new JobRunner(this.application, i, this.jobController, jobPredicate).start();
        }
        this.jobController.wakeUp();
    }

    public void addListener(String str, JobTracker.JobListener jobListener) {
        this.jobTracker.addListener(new JobIdFilter(str), jobListener);
    }

    public void addListener(JobTracker.JobFilter jobFilter, JobTracker.JobListener jobListener) {
        this.jobTracker.addListener(jobFilter, jobListener);
    }

    public void removeListener(JobTracker.JobListener jobListener) {
        this.jobTracker.removeListener(jobListener);
    }

    public JobTracker.JobState getFirstMatchingJobState(JobTracker.JobFilter jobFilter) {
        return this.jobTracker.getFirstMatchingJobState(jobFilter);
    }

    public void add(Job job) {
        new Chain(this, Collections.singletonList(job)).enqueue();
    }

    public void add(Job job, Collection<String> collection) {
        this.jobTracker.onStateChange(job, JobTracker.JobState.PENDING);
        runOnExecutor(new Runnable(job, collection) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda10
            public final /* synthetic */ Job f$1;
            public final /* synthetic */ Collection f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$add$2(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$add$2(Job job, Collection collection) {
        this.jobController.submitJobWithExistingDependencies(job, collection, null);
        this.jobController.wakeUp();
    }

    public void add(Job job, String str) {
        this.jobTracker.onStateChange(job, JobTracker.JobState.PENDING);
        runOnExecutor(new Runnable(job, str) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda15
            public final /* synthetic */ Job f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$add$3(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$add$3(Job job, String str) {
        this.jobController.submitJobWithExistingDependencies(job, Collections.emptyList(), str);
        this.jobController.wakeUp();
    }

    public void add(Job job, Collection<String> collection, String str) {
        this.jobTracker.onStateChange(job, JobTracker.JobState.PENDING);
        runOnExecutor(new Runnable(job, collection, str) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda5
            public final /* synthetic */ Job f$1;
            public final /* synthetic */ Collection f$2;
            public final /* synthetic */ String f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$add$4(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$add$4(Job job, Collection collection, String str) {
        this.jobController.submitJobWithExistingDependencies(job, collection, str);
        this.jobController.wakeUp();
    }

    public Chain startChain(Job job) {
        return new Chain(this, Collections.singletonList(job));
    }

    public Chain startChain(List<? extends Job> list) {
        return new Chain(this, list);
    }

    public /* synthetic */ void lambda$cancel$5(String str) {
        this.jobController.cancelJob(str);
    }

    public void cancel(String str) {
        runOnExecutor(new Runnable(str) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$cancel$5(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$cancelAllInQueue$6(String str) {
        this.jobController.cancelAllInQueue(str);
    }

    public void cancelAllInQueue(String str) {
        runOnExecutor(new Runnable(str) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$cancelAllInQueue$6(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$update$7(JobUpdater jobUpdater) {
        this.jobController.update(jobUpdater);
    }

    public void update(JobUpdater jobUpdater) {
        runOnExecutor(new Runnable(jobUpdater) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda12
            public final /* synthetic */ JobUpdater f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$update$7(this.f$1);
            }
        });
    }

    public List<JobSpec> find(Predicate<JobSpec> predicate) {
        waitUntilInitialized();
        return this.jobController.findJobs(predicate);
    }

    public Optional<JobTracker.JobState> runSynchronously(Job job, long j) {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        final AtomicReference atomicReference = new AtomicReference();
        addListener(job.getId(), new JobTracker.JobListener() { // from class: org.thoughtcrime.securesms.jobmanager.JobManager.1
            @Override // org.thoughtcrime.securesms.jobmanager.JobTracker.JobListener
            public void onStateChanged(Job job2, JobTracker.JobState jobState) {
                if (jobState.isComplete()) {
                    JobManager.this.removeListener(this);
                    atomicReference.set(jobState);
                    countDownLatch.countDown();
                }
            }
        });
        add(job);
        try {
            if (!countDownLatch.await(j, TimeUnit.MILLISECONDS)) {
                return Optional.empty();
            }
            return Optional.ofNullable((JobTracker.JobState) atomicReference.get());
        } catch (InterruptedException e) {
            Log.w(TAG, "Interrupted during runSynchronously()", e);
            return Optional.empty();
        }
    }

    public String getDebugInfo() {
        AtomicReference atomicReference = new AtomicReference();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        runOnExecutor(new Runnable(atomicReference, countDownLatch) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda11
            public final /* synthetic */ AtomicReference f$1;
            public final /* synthetic */ CountDownLatch f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$getDebugInfo$8(this.f$1, this.f$2);
            }
        });
        try {
            return countDownLatch.await(10, TimeUnit.SECONDS) ? (String) atomicReference.get() : "Timed out waiting for Job info.";
        } catch (InterruptedException e) {
            Log.w(TAG, "Failed to retrieve Job info.", e);
            return "Failed to retrieve Job info.";
        }
    }

    public /* synthetic */ void lambda$getDebugInfo$8(AtomicReference atomicReference, CountDownLatch countDownLatch) {
        atomicReference.set(this.jobController.getDebugInfo());
        countDownLatch.countDown();
    }

    public void addOnEmptyQueueListener(EmptyQueueListener emptyQueueListener) {
        runOnExecutor(new Runnable(emptyQueueListener) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda4
            public final /* synthetic */ JobManager.EmptyQueueListener f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$addOnEmptyQueueListener$9(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$addOnEmptyQueueListener$9(EmptyQueueListener emptyQueueListener) {
        synchronized (this.emptyQueueListeners) {
            this.emptyQueueListeners.add(emptyQueueListener);
        }
    }

    public void removeOnEmptyQueueListener(EmptyQueueListener emptyQueueListener) {
        runOnExecutor(new Runnable(emptyQueueListener) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda2
            public final /* synthetic */ JobManager.EmptyQueueListener f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$removeOnEmptyQueueListener$10(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$removeOnEmptyQueueListener$10(EmptyQueueListener emptyQueueListener) {
        synchronized (this.emptyQueueListeners) {
            this.emptyQueueListeners.remove(emptyQueueListener);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.ConstraintObserver.Notifier
    public void onConstraintMet(String str) {
        String str2 = TAG;
        Log.i(str2, "onConstraintMet(" + str + ")");
        wakeUp();
    }

    public void flush() {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        runOnExecutor(new SurfaceTextureEglRenderer$$ExternalSyntheticLambda0(countDownLatch));
        try {
            countDownLatch.await();
            Log.i(TAG, "Successfully flushed.");
        } catch (InterruptedException e) {
            Log.w(TAG, "Failed to finish flushing.", e);
        }
    }

    public boolean isQueueEmpty(String str) {
        return areQueuesEmpty(Collections.singleton(str));
    }

    public boolean areQueuesEmpty(Set<String> set) {
        waitUntilInitialized();
        return this.jobController.areQueuesEmpty(set);
    }

    public void wakeUp() {
        JobController jobController = this.jobController;
        Objects.requireNonNull(jobController);
        runOnExecutor(new Runnable() { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                JobController.this.wakeUp();
            }
        });
    }

    public void enqueueChain(Chain chain) {
        for (List<Job> list : chain.getJobListChain()) {
            for (Job job : list) {
                this.jobTracker.onStateChange(job, JobTracker.JobState.PENDING);
            }
        }
        runOnExecutor(new Runnable(chain) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda14
            public final /* synthetic */ JobManager.Chain f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$enqueueChain$11(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$enqueueChain$11(Chain chain) {
        this.jobController.submitNewJobChain(chain.getJobListChain());
        this.jobController.wakeUp();
    }

    public void onEmptyQueue() {
        runOnExecutor(new Runnable() { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda13
            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$onEmptyQueue$12();
            }
        });
    }

    public /* synthetic */ void lambda$onEmptyQueue$12() {
        synchronized (this.emptyQueueListeners) {
            for (EmptyQueueListener emptyQueueListener : this.emptyQueueListeners) {
                emptyQueueListener.onQueueEmpty();
            }
        }
    }

    private void runOnExecutor(Runnable runnable) {
        this.executor.execute(new Runnable(runnable) { // from class: org.thoughtcrime.securesms.jobmanager.JobManager$$ExternalSyntheticLambda16
            public final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobManager.this.lambda$runOnExecutor$13(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$runOnExecutor$13(Runnable runnable) {
        waitUntilInitialized();
        runnable.run();
    }

    private void waitUntilInitialized() {
        if (!this.initialized) {
            Log.i(TAG, "Waiting for initialization...");
            synchronized (this) {
                while (!this.initialized) {
                    Util.wait(this, 0);
                }
            }
            Log.i(TAG, "Initialization complete.");
        }
    }

    /* loaded from: classes4.dex */
    public static class JobIdFilter implements JobTracker.JobFilter {
        private final String id;

        public JobIdFilter(String str) {
            this.id = str;
        }

        @Override // org.thoughtcrime.securesms.jobmanager.JobTracker.JobFilter
        public boolean matches(Job job) {
            return this.id.equals(job.getId());
        }
    }

    /* loaded from: classes4.dex */
    public static class Chain {
        private final JobManager jobManager;
        private final List<List<Job>> jobs;

        public Chain(JobManager jobManager, List<? extends Job> list) {
            this.jobManager = jobManager;
            LinkedList linkedList = new LinkedList();
            this.jobs = linkedList;
            linkedList.add(new ArrayList(list));
        }

        public Chain then(Job job) {
            return then(Collections.singletonList(job));
        }

        public Chain then(List<? extends Job> list) {
            if (!list.isEmpty()) {
                this.jobs.add(new ArrayList(list));
            }
            return this;
        }

        public void enqueue() {
            this.jobManager.enqueueChain(this);
        }

        public void enqueue(JobTracker.JobListener jobListener) {
            List<List<Job>> list = this.jobs;
            List<Job> list2 = list.get(list.size() - 1);
            this.jobManager.addListener(list2.get(list2.size() - 1).getId(), jobListener);
            enqueue();
        }

        public List<List<Job>> getJobListChain() {
            return this.jobs;
        }
    }

    /* loaded from: classes4.dex */
    public static class Configuration {
        private final ConstraintInstantiator constraintInstantiator;
        private final List<ConstraintObserver> constraintObservers;
        private final Data.Serializer dataSerializer;
        private final ExecutorFactory executorFactory;
        private final JobInstantiator jobInstantiator;
        private final JobMigrator jobMigrator;
        private final JobStorage jobStorage;
        private final int jobThreadCount;
        private final JobTracker jobTracker;
        private final List<JobPredicate> reservedJobRunners;

        private Configuration(int i, ExecutorFactory executorFactory, JobInstantiator jobInstantiator, ConstraintInstantiator constraintInstantiator, List<ConstraintObserver> list, Data.Serializer serializer, JobStorage jobStorage, JobMigrator jobMigrator, JobTracker jobTracker, List<JobPredicate> list2) {
            this.executorFactory = executorFactory;
            this.jobThreadCount = i;
            this.jobInstantiator = jobInstantiator;
            this.constraintInstantiator = constraintInstantiator;
            this.constraintObservers = new ArrayList(list);
            this.dataSerializer = serializer;
            this.jobStorage = jobStorage;
            this.jobMigrator = jobMigrator;
            this.jobTracker = jobTracker;
            this.reservedJobRunners = new ArrayList(list2);
        }

        int getJobThreadCount() {
            return this.jobThreadCount;
        }

        ExecutorFactory getExecutorFactory() {
            return this.executorFactory;
        }

        JobInstantiator getJobInstantiator() {
            return this.jobInstantiator;
        }

        ConstraintInstantiator getConstraintFactories() {
            return this.constraintInstantiator;
        }

        List<ConstraintObserver> getConstraintObservers() {
            return this.constraintObservers;
        }

        Data.Serializer getDataSerializer() {
            return this.dataSerializer;
        }

        JobStorage getJobStorage() {
            return this.jobStorage;
        }

        JobMigrator getJobMigrator() {
            return this.jobMigrator;
        }

        JobTracker getJobTracker() {
            return this.jobTracker;
        }

        List<JobPredicate> getReservedJobRunners() {
            return this.reservedJobRunners;
        }

        /* loaded from: classes4.dex */
        public static class Builder {
            private Map<String, Constraint.Factory> constraintFactories = new HashMap();
            private List<ConstraintObserver> constraintObservers = new ArrayList();
            private Data.Serializer dataSerializer = new JsonDataSerializer();
            private ExecutorFactory executorFactory = new DefaultExecutorFactory();
            private Map<String, Job.Factory> jobFactories = new HashMap();
            private JobMigrator jobMigrator = null;
            private JobStorage jobStorage = null;
            private int jobThreadCount = Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4));
            private JobTracker jobTracker = new JobTracker();
            private List<JobPredicate> reservedJobRunners = new ArrayList();

            public Builder setJobThreadCount(int i) {
                this.jobThreadCount = i;
                return this;
            }

            public Builder addReservedJobRunner(JobPredicate jobPredicate) {
                this.reservedJobRunners.add(jobPredicate);
                return this;
            }

            public Builder setExecutorFactory(ExecutorFactory executorFactory) {
                this.executorFactory = executorFactory;
                return this;
            }

            public Builder setJobFactories(Map<String, Job.Factory> map) {
                this.jobFactories = map;
                return this;
            }

            public Builder setConstraintFactories(Map<String, Constraint.Factory> map) {
                this.constraintFactories = map;
                return this;
            }

            public Builder setConstraintObservers(List<ConstraintObserver> list) {
                this.constraintObservers = list;
                return this;
            }

            public Builder setDataSerializer(Data.Serializer serializer) {
                this.dataSerializer = serializer;
                return this;
            }

            public Builder setJobStorage(JobStorage jobStorage) {
                this.jobStorage = jobStorage;
                return this;
            }

            public Builder setJobMigrator(JobMigrator jobMigrator) {
                this.jobMigrator = jobMigrator;
                return this;
            }

            public Configuration build() {
                return new Configuration(this.jobThreadCount, this.executorFactory, new JobInstantiator(this.jobFactories), new ConstraintInstantiator(this.constraintFactories), new ArrayList(this.constraintObservers), this.dataSerializer, this.jobStorage, this.jobMigrator, this.jobTracker, this.reservedJobRunners);
            }
        }
    }
}
