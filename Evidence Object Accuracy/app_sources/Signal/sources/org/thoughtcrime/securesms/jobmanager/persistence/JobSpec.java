package org.thoughtcrime.securesms.jobmanager.persistence;

import java.util.Locale;
import java.util.Objects;

/* loaded from: classes4.dex */
public final class JobSpec {
    private final long createTime;
    private final String factoryKey;
    private final String id;
    private final boolean isRunning;
    private final long lifespan;
    private final int maxAttempts;
    private final boolean memoryOnly;
    private final long nextRunAttemptTime;
    private final String queueKey;
    private final int runAttempt;
    private final String serializedData;
    private final String serializedInputData;

    public JobSpec(String str, String str2, String str3, long j, long j2, int i, int i2, long j3, String str4, String str5, boolean z, boolean z2) {
        this.id = str;
        this.factoryKey = str2;
        this.queueKey = str3;
        this.createTime = j;
        this.nextRunAttemptTime = j2;
        this.runAttempt = i;
        this.maxAttempts = i2;
        this.lifespan = j3;
        this.serializedData = str4;
        this.serializedInputData = str5;
        this.isRunning = z;
        this.memoryOnly = z2;
    }

    public JobSpec withNextRunAttemptTime(long j) {
        return new JobSpec(this.id, this.factoryKey, this.queueKey, this.createTime, j, this.runAttempt, this.maxAttempts, this.lifespan, this.serializedData, this.serializedInputData, this.isRunning, this.memoryOnly);
    }

    public String getId() {
        return this.id;
    }

    public String getFactoryKey() {
        return this.factoryKey;
    }

    public String getQueueKey() {
        return this.queueKey;
    }

    public long getCreateTime() {
        return this.createTime;
    }

    public long getNextRunAttemptTime() {
        return this.nextRunAttemptTime;
    }

    public int getRunAttempt() {
        return this.runAttempt;
    }

    public int getMaxAttempts() {
        return this.maxAttempts;
    }

    public long getLifespan() {
        return this.lifespan;
    }

    public String getSerializedData() {
        return this.serializedData;
    }

    public String getSerializedInputData() {
        return this.serializedInputData;
    }

    public boolean isRunning() {
        return this.isRunning;
    }

    public boolean isMemoryOnly() {
        return this.memoryOnly;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || JobSpec.class != obj.getClass()) {
            return false;
        }
        JobSpec jobSpec = (JobSpec) obj;
        if (this.createTime == jobSpec.createTime && this.nextRunAttemptTime == jobSpec.nextRunAttemptTime && this.runAttempt == jobSpec.runAttempt && this.maxAttempts == jobSpec.maxAttempts && this.lifespan == jobSpec.lifespan && this.isRunning == jobSpec.isRunning && this.memoryOnly == jobSpec.memoryOnly && Objects.equals(this.id, jobSpec.id) && Objects.equals(this.factoryKey, jobSpec.factoryKey) && Objects.equals(this.queueKey, jobSpec.queueKey) && Objects.equals(this.serializedData, jobSpec.serializedData) && Objects.equals(this.serializedInputData, jobSpec.serializedInputData)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Objects.hash(this.id, this.factoryKey, this.queueKey, Long.valueOf(this.createTime), Long.valueOf(this.nextRunAttemptTime), Integer.valueOf(this.runAttempt), Integer.valueOf(this.maxAttempts), Long.valueOf(this.lifespan), this.serializedData, this.serializedInputData, Boolean.valueOf(this.isRunning), Boolean.valueOf(this.memoryOnly));
    }

    public String toString() {
        return String.format(Locale.US, "id: JOB::%s | factoryKey: %s | queueKey: %s | createTime: %d | nextRunAttemptTime: %d | runAttempt: %d | maxAttempts: %d | lifespan: %d | isRunning: %b | memoryOnly: %b", this.id, this.factoryKey, this.queueKey, Long.valueOf(this.createTime), Long.valueOf(this.nextRunAttemptTime), Integer.valueOf(this.runAttempt), Integer.valueOf(this.maxAttempts), Long.valueOf(this.lifespan), Boolean.valueOf(this.isRunning), Boolean.valueOf(this.memoryOnly));
    }
}
