package org.thoughtcrime.securesms.jobmanager.impl;

import android.app.job.JobInfo;
import android.content.Context;
import java.util.Collections;
import java.util.Set;
import org.thoughtcrime.securesms.jobmanager.Constraint;
import org.thoughtcrime.securesms.util.NetworkUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class AutoDownloadEmojiConstraint implements Constraint {
    private static final String IMAGE_TYPE;
    public static final String KEY;
    private final Context context;

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public /* synthetic */ String getJobSchedulerKeyPart() {
        return Constraint.CC.$default$getJobSchedulerKeyPart(this);
    }

    private AutoDownloadEmojiConstraint(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public boolean isMet() {
        return canAutoDownloadEmoji(this.context);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public void applyToJobInfo(JobInfo.Builder builder) {
        boolean contains = TextSecurePreferences.getRoamingMediaDownloadAllowed(this.context).contains("image");
        boolean contains2 = TextSecurePreferences.getMobileMediaDownloadAllowed(this.context).contains("image");
        if (contains) {
            builder.setRequiredNetworkType(1);
        } else if (contains2) {
            builder.setRequiredNetworkType(3);
        } else {
            builder.setRequiredNetworkType(2);
        }
    }

    public static boolean canAutoDownloadEmoji(Context context) {
        return getAllowedAutoDownloadTypes(context, true).contains("image");
    }

    public static boolean canAutoDownloadJumboEmoji(Context context) {
        return getAllowedAutoDownloadTypes(context, false).contains("image");
    }

    private static Set<String> getAllowedAutoDownloadTypes(Context context, boolean z) {
        if (NetworkUtil.isConnectedWifi(context)) {
            return z ? Collections.singleton("image") : TextSecurePreferences.getWifiMediaDownloadAllowed(context);
        }
        if (NetworkUtil.isConnectedRoaming(context)) {
            return TextSecurePreferences.getRoamingMediaDownloadAllowed(context);
        }
        if (NetworkUtil.isConnectedMobile(context)) {
            return TextSecurePreferences.getMobileMediaDownloadAllowed(context);
        }
        return Collections.emptySet();
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Constraint.Factory<AutoDownloadEmojiConstraint> {
        private final Context context;

        public Factory(Context context) {
            this.context = context.getApplicationContext();
        }

        @Override // org.thoughtcrime.securesms.jobmanager.Constraint.Factory
        public AutoDownloadEmojiConstraint create() {
            return new AutoDownloadEmojiConstraint(this.context);
        }
    }
}
