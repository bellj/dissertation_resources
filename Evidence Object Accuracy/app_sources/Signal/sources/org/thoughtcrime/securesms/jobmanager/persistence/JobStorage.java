package org.thoughtcrime.securesms.jobmanager.persistence;

import java.util.List;
import java.util.Set;

/* loaded from: classes4.dex */
public interface JobStorage {
    boolean areQueuesEmpty(Set<String> set);

    void deleteJob(String str);

    void deleteJobs(List<String> list);

    List<ConstraintSpec> getAllConstraintSpecs();

    List<DependencySpec> getAllDependencySpecs();

    List<JobSpec> getAllJobSpecs();

    List<ConstraintSpec> getConstraintSpecs(String str);

    List<DependencySpec> getDependencySpecsThatDependOnJob(String str);

    int getJobCountForFactory(String str);

    int getJobCountForFactoryAndQueue(String str, String str2);

    JobSpec getJobSpec(String str);

    List<JobSpec> getJobsInQueue(String str);

    List<JobSpec> getPendingJobsWithNoDependenciesInCreatedOrder(long j);

    void init();

    void insertJobs(List<FullSpec> list);

    void updateAllJobsToBePending();

    void updateJobAfterRetry(String str, boolean z, int i, long j, String str2);

    void updateJobRunningState(String str, boolean z);

    void updateJobs(List<JobSpec> list);
}
