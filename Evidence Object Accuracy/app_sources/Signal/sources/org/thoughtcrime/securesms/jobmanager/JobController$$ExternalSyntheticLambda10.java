package org.thoughtcrime.securesms.jobmanager;

import com.annimon.stream.function.Function;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class JobController$$ExternalSyntheticLambda10 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((Job) obj).getId();
    }
}
