package org.thoughtcrime.securesms.jobmanager;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class JobController$$ExternalSyntheticLambda6 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((JobSpec) obj).getId();
    }
}
