package org.thoughtcrime.securesms.jobmanager.impl;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.jobmanager.ConstraintObserver;
import org.thoughtcrime.securesms.jobmanager.impl.NotInCallConstraint;

/* loaded from: classes4.dex */
public final class NotInCallConstraintObserver implements ConstraintObserver {
    private static final String REASON = Log.tag(NotInCallConstraintObserver.class);

    @Override // org.thoughtcrime.securesms.jobmanager.ConstraintObserver
    public void register(ConstraintObserver.Notifier notifier) {
        EventBus.getDefault().register(new EventBusListener(notifier));
    }

    /* loaded from: classes.dex */
    private static final class EventBusListener {
        private final ConstraintObserver.Notifier notifier;

        private EventBusListener(ConstraintObserver.Notifier notifier) {
            this.notifier = notifier;
        }

        @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
        public void consume(WebRtcViewModel webRtcViewModel) {
            if (new NotInCallConstraint.Factory().create().isMet()) {
                this.notifier.onConstraintMet(NotInCallConstraintObserver.REASON);
            }
        }
    }
}
