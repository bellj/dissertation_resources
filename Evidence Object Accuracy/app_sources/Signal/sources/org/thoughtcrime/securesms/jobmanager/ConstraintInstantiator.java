package org.thoughtcrime.securesms.jobmanager;

import java.util.HashMap;
import java.util.Map;
import org.thoughtcrime.securesms.jobmanager.Constraint;

/* loaded from: classes4.dex */
public class ConstraintInstantiator {
    private final Map<String, Constraint.Factory> constraintFactories;

    public ConstraintInstantiator(Map<String, Constraint.Factory> map) {
        this.constraintFactories = new HashMap(map);
    }

    public Constraint instantiate(String str) {
        if (this.constraintFactories.containsKey(str)) {
            return this.constraintFactories.get(str).create();
        }
        throw new IllegalStateException("Tried to instantiate a constraint with key '" + str + "', but no matching factory was found.");
    }
}
