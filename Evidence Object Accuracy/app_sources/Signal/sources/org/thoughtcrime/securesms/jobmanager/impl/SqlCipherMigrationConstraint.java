package org.thoughtcrime.securesms.jobmanager.impl;

import android.app.Application;
import android.app.job.JobInfo;
import org.thoughtcrime.securesms.jobmanager.Constraint;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class SqlCipherMigrationConstraint implements Constraint {
    public static final String KEY;
    private final Application application;

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public void applyToJobInfo(JobInfo.Builder builder) {
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public /* synthetic */ String getJobSchedulerKeyPart() {
        return Constraint.CC.$default$getJobSchedulerKeyPart(this);
    }

    private SqlCipherMigrationConstraint(Application application) {
        this.application = application;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public boolean isMet() {
        return !TextSecurePreferences.getNeedsSqlCipherMigration(this.application);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Constraint.Factory<SqlCipherMigrationConstraint> {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @Override // org.thoughtcrime.securesms.jobmanager.Constraint.Factory
        public SqlCipherMigrationConstraint create() {
            return new SqlCipherMigrationConstraint(this.application);
        }
    }
}
