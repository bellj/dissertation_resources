package org.thoughtcrime.securesms.jobmanager;

import java.util.concurrent.ExecutorService;

/* loaded from: classes4.dex */
public interface ExecutorFactory {
    ExecutorService newSingleThreadExecutor(String str);
}
