package org.thoughtcrime.securesms.jobmanager.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import org.thoughtcrime.securesms.jobmanager.ExecutorFactory;

/* loaded from: classes4.dex */
public class DefaultExecutorFactory implements ExecutorFactory {
    public static /* synthetic */ Thread lambda$newSingleThreadExecutor$0(String str, Runnable runnable) {
        return new Thread(runnable, str);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.ExecutorFactory
    public ExecutorService newSingleThreadExecutor(String str) {
        return Executors.newSingleThreadExecutor(new ThreadFactory(str) { // from class: org.thoughtcrime.securesms.jobmanager.impl.DefaultExecutorFactory$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.ThreadFactory
            public final Thread newThread(Runnable runnable) {
                return DefaultExecutorFactory.lambda$newSingleThreadExecutor$0(this.f$0, runnable);
            }
        });
    }
}
