package org.thoughtcrime.securesms.jobmanager.migrations;

import android.app.Application;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.IOException;
import java.io.Serializable;
import org.thoughtcrime.securesms.database.PushDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.JobMigration;
import org.thoughtcrime.securesms.jobs.DirectoryRefreshJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceReadUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceVerifiedUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceViewOnceOpenJob;
import org.thoughtcrime.securesms.jobs.PushGroupSendJob;
import org.thoughtcrime.securesms.jobs.PushGroupUpdateJob;
import org.thoughtcrime.securesms.jobs.PushMediaSendJob;
import org.thoughtcrime.securesms.jobs.PushTextSendJob;
import org.thoughtcrime.securesms.jobs.RequestGroupInfoJob;
import org.thoughtcrime.securesms.jobs.RetrieveProfileAvatarJob;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.jobs.SendDeliveryReceiptJob;
import org.thoughtcrime.securesms.jobs.SmsSendJob;
import org.thoughtcrime.securesms.notifications.DeleteNotificationReceiver;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.JsonUtils;

/* loaded from: classes4.dex */
public class RecipientIdJobMigration extends JobMigration {
    private final Application application;

    public RecipientIdJobMigration(Application application) {
        super(2);
        this.application = application;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.JobMigration
    public JobMigration.JobData migrate(JobMigration.JobData jobData) {
        String factoryKey = jobData.getFactoryKey();
        factoryKey.hashCode();
        char c = 65535;
        switch (factoryKey.hashCode()) {
            case -2025575233:
                if (factoryKey.equals(RequestGroupInfoJob.KEY)) {
                    c = 0;
                    break;
                }
                break;
            case -944889832:
                if (factoryKey.equals(MultiDeviceViewOnceOpenJob.KEY)) {
                    c = 1;
                    break;
                }
                break;
            case -665487460:
                if (factoryKey.equals(SmsSendJob.KEY)) {
                    c = 2;
                    break;
                }
                break;
            case -548848369:
                if (factoryKey.equals(DirectoryRefreshJob.KEY)) {
                    c = 3;
                    break;
                }
                break;
            case 137814849:
                if (factoryKey.equals(SendDeliveryReceiptJob.KEY)) {
                    c = 4;
                    break;
                }
                break;
            case 875739311:
                if (factoryKey.equals(MultiDeviceReadUpdateJob.KEY)) {
                    c = 5;
                    break;
                }
                break;
            case 995195278:
                if (factoryKey.equals(PushTextSendJob.KEY)) {
                    c = 6;
                    break;
                }
                break;
            case 1080789712:
                if (factoryKey.equals(RetrieveProfileJob.KEY)) {
                    c = 7;
                    break;
                }
                break;
            case 1102058575:
                if (factoryKey.equals(PushGroupUpdateJob.KEY)) {
                    c = '\b';
                    break;
                }
                break;
            case 1384402039:
                if (factoryKey.equals(RetrieveProfileAvatarJob.KEY)) {
                    c = '\t';
                    break;
                }
                break;
            case 1437817840:
                if (factoryKey.equals(PushGroupSendJob.KEY)) {
                    c = '\n';
                    break;
                }
                break;
            case 1806792427:
                if (factoryKey.equals(PushMediaSendJob.KEY)) {
                    c = 11;
                    break;
                }
                break;
            case 1995344701:
                if (factoryKey.equals(MultiDeviceVerifiedUpdateJob.KEY)) {
                    c = '\f';
                    break;
                }
                break;
            case 1999743203:
                if (factoryKey.equals(MultiDeviceContactUpdateJob.KEY)) {
                    c = '\r';
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return migrateRequestGroupInfoJob(jobData);
            case 1:
                return migrateMultiDeviceViewOnceOpenJob(jobData);
            case 2:
                return migrateSmsSendJob(jobData);
            case 3:
                return migrateDirectoryRefreshJob(jobData);
            case 4:
                return migrateSendDeliveryReceiptJob(jobData);
            case 5:
                return migrateMultiDeviceReadUpdateJob(jobData);
            case 6:
                return migratePushTextSendJob(jobData);
            case 7:
                return migrateRetrieveProfileJob(jobData);
            case '\b':
                return migratePushGroupUpdateJob(jobData);
            case '\t':
                return migrateRetrieveProfileAvatarJob(jobData);
            case '\n':
                return migratePushGroupSendJob(jobData);
            case 11:
                return migratePushMediaSendJob(jobData);
            case '\f':
                return migrateMultiDeviceVerifiedUpdateJob(jobData);
            case '\r':
                return migrateMultiDeviceContactUpdateJob(jobData);
            default:
                return jobData;
        }
    }

    private JobMigration.JobData migrateMultiDeviceContactUpdateJob(JobMigration.JobData jobData) {
        String str = null;
        String string = jobData.getData().hasString("address") ? jobData.getData().getString("address") : null;
        Data.Builder builder = new Data.Builder();
        if (string != null) {
            str = Recipient.external(this.application, string).getId().serialize();
        }
        return jobData.withData(builder.putString(RecipientDatabase.TABLE_NAME, str).putBoolean("force_sync", jobData.getData().getBoolean("force_sync")).build());
    }

    private JobMigration.JobData migrateMultiDeviceViewOnceOpenJob(JobMigration.JobData jobData) {
        try {
            OldSerializableSyncMessageId oldSerializableSyncMessageId = (OldSerializableSyncMessageId) JsonUtils.fromJson(jobData.getData().getString("message_id"), OldSerializableSyncMessageId.class);
            return jobData.withData(new Data.Builder().putString("message_id", JsonUtils.toJson(new NewSerializableSyncMessageId(Recipient.external(this.application, oldSerializableSyncMessageId.sender).getId().serialize(), oldSerializableSyncMessageId.timestamp))).build());
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    private JobMigration.JobData migrateRequestGroupInfoJob(JobMigration.JobData jobData) {
        return jobData.withData(new Data.Builder().putString(PushDatabase.SOURCE_E164, Recipient.external(this.application, jobData.getData().getString(PushDatabase.SOURCE_E164)).getId().serialize()).putString("group_id", jobData.getData().getString("group_id")).build());
    }

    private JobMigration.JobData migrateSendDeliveryReceiptJob(JobMigration.JobData jobData) {
        return jobData.withData(new Data.Builder().putString(RecipientDatabase.TABLE_NAME, Recipient.external(this.application, jobData.getData().getString("address")).getId().serialize()).putLong("message_id", jobData.getData().getLong("message_id")).putLong("timestamp", jobData.getData().getLong("timestamp")).build());
    }

    private JobMigration.JobData migrateMultiDeviceVerifiedUpdateJob(JobMigration.JobData jobData) {
        return jobData.withData(new Data.Builder().putString("destination", Recipient.external(this.application, jobData.getData().getString("destination")).getId().serialize()).putString("identity_key", jobData.getData().getString("identity_key")).putInt("verified_status", jobData.getData().getInt("verified_status")).putLong("timestamp", jobData.getData().getLong("timestamp")).build());
    }

    private JobMigration.JobData migrateRetrieveProfileJob(JobMigration.JobData jobData) {
        return jobData.withData(new Data.Builder().putString(RecipientDatabase.TABLE_NAME, Recipient.external(this.application, jobData.getData().getString("address")).getId().serialize()).build());
    }

    private JobMigration.JobData migratePushGroupSendJob(JobMigration.JobData jobData) {
        Recipient external = Recipient.external(this.application, jobData.getQueueKey());
        String str = null;
        String string = jobData.getData().hasString("filter_address") ? jobData.getData().getString("filter_address") : null;
        RecipientId id = string != null ? Recipient.external(this.application, string).getId() : null;
        Data.Builder builder = new Data.Builder();
        if (id != null) {
            str = id.serialize();
        }
        return jobData.withQueueKey(external.getId().toQueueKey()).withData(builder.putString("filter_recipient", str).putLong("message_id", jobData.getData().getLong("message_id")).build());
    }

    private JobMigration.JobData migratePushGroupUpdateJob(JobMigration.JobData jobData) {
        return jobData.withData(new Data.Builder().putString(PushDatabase.SOURCE_E164, Recipient.external(this.application, jobData.getData().getString(PushDatabase.SOURCE_E164)).getId().serialize()).putString("group_id", jobData.getData().getString("group_id")).build());
    }

    private JobMigration.JobData migrateDirectoryRefreshJob(JobMigration.JobData jobData) {
        String str = null;
        String string = jobData.getData().hasString("address") ? jobData.getData().getString("address") : null;
        Recipient external = string != null ? Recipient.external(this.application, string) : null;
        Data.Builder builder = new Data.Builder();
        if (external != null) {
            str = external.getId().serialize();
        }
        return jobData.withData(builder.putString(RecipientDatabase.TABLE_NAME, str).putBoolean("notify_of_new_users", jobData.getData().getBoolean("notify_of_new_users")).build());
    }

    private JobMigration.JobData migrateRetrieveProfileAvatarJob(JobMigration.JobData jobData) {
        Recipient external = Recipient.external(this.application, jobData.getQueueKey().substring(24));
        Data build = new Data.Builder().putString(RecipientDatabase.TABLE_NAME, Recipient.external(this.application, jobData.getData().getString("address")).getId().serialize()).putString("profile_avatar", jobData.getData().getString("profile_avatar")).build();
        return jobData.withQueueKey("RetrieveProfileAvatarJob::" + external.getId().toQueueKey()).withData(build);
    }

    private JobMigration.JobData migrateMultiDeviceReadUpdateJob(JobMigration.JobData jobData) {
        try {
            String[] stringArray = jobData.getData().getStringArray(DeleteNotificationReceiver.EXTRA_IDS);
            String[] strArr = new String[stringArray.length];
            for (int i = 0; i < stringArray.length; i++) {
                OldSerializableSyncMessageId oldSerializableSyncMessageId = (OldSerializableSyncMessageId) JsonUtils.fromJson(stringArray[i], OldSerializableSyncMessageId.class);
                strArr[i] = JsonUtils.toJson(new NewSerializableSyncMessageId(Recipient.external(this.application, oldSerializableSyncMessageId.sender).getId().serialize(), oldSerializableSyncMessageId.timestamp));
            }
            return jobData.withData(new Data.Builder().putStringArray(DeleteNotificationReceiver.EXTRA_IDS, strArr).build());
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    private JobMigration.JobData migratePushTextSendJob(JobMigration.JobData jobData) {
        return jobData.withQueueKey(Recipient.external(this.application, jobData.getQueueKey()).getId().toQueueKey());
    }

    private JobMigration.JobData migratePushMediaSendJob(JobMigration.JobData jobData) {
        return jobData.withQueueKey(Recipient.external(this.application, jobData.getQueueKey()).getId().toQueueKey());
    }

    private JobMigration.JobData migrateSmsSendJob(JobMigration.JobData jobData) {
        return jobData.getQueueKey() != null ? jobData.withQueueKey(Recipient.external(this.application, jobData.getQueueKey()).getId().toQueueKey()) : jobData;
    }

    /* loaded from: classes.dex */
    public static class OldSerializableSyncMessageId implements Serializable {
        private static final long serialVersionUID;
        @JsonProperty
        private final String sender;
        @JsonProperty
        private final long timestamp;

        OldSerializableSyncMessageId(@JsonProperty("sender") String str, @JsonProperty("timestamp") long j) {
            this.sender = str;
            this.timestamp = j;
        }
    }

    /* loaded from: classes.dex */
    public static class NewSerializableSyncMessageId implements Serializable {
        private static final long serialVersionUID;
        @JsonProperty
        private final String recipientId;
        @JsonProperty
        private final long timestamp;

        NewSerializableSyncMessageId(@JsonProperty("recipientId") String str, @JsonProperty("timestamp") long j) {
            this.recipientId = str;
            this.timestamp = j;
        }
    }
}
