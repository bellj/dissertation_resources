package org.thoughtcrime.securesms.jobmanager.migrations;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.JobMigration;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;

/* loaded from: classes4.dex */
public class RetrieveProfileJobMigration extends JobMigration {
    private static final String TAG = Log.tag(RetrieveProfileJobMigration.class);

    public RetrieveProfileJobMigration() {
        super(7);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.JobMigration
    public JobMigration.JobData migrate(JobMigration.JobData jobData) {
        Log.i(TAG, "Running.");
        return RetrieveProfileJob.KEY.equals(jobData.getFactoryKey()) ? migrateRetrieveProfileJob(jobData) : jobData;
    }

    private static JobMigration.JobData migrateRetrieveProfileJob(JobMigration.JobData jobData) {
        Data data = jobData.getData();
        if (!data.hasString(RecipientDatabase.TABLE_NAME)) {
            return jobData;
        }
        Log.i(TAG, "Migrating job.");
        return jobData.withData(new Data.Builder().putStringArray(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, new String[]{data.getString(RecipientDatabase.TABLE_NAME)}).build());
    }
}
