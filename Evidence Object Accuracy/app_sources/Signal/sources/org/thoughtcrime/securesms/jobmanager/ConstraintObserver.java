package org.thoughtcrime.securesms.jobmanager;

/* loaded from: classes4.dex */
public interface ConstraintObserver {

    /* loaded from: classes.dex */
    public interface Notifier {
        void onConstraintMet(String str);
    }

    void register(Notifier notifier);
}
