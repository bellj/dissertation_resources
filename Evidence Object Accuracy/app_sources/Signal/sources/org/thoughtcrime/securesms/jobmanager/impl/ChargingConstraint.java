package org.thoughtcrime.securesms.jobmanager.impl;

import android.app.job.JobInfo;
import org.thoughtcrime.securesms.jobmanager.Constraint;

/* loaded from: classes4.dex */
public class ChargingConstraint implements Constraint {
    public static final String KEY;

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public String getJobSchedulerKeyPart() {
        return "CHARGING";
    }

    private ChargingConstraint() {
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public boolean isMet() {
        return ChargingConstraintObserver.isCharging();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public void applyToJobInfo(JobInfo.Builder builder) {
        builder.setRequiresCharging(true);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Constraint.Factory<ChargingConstraint> {
        @Override // org.thoughtcrime.securesms.jobmanager.Constraint.Factory
        public ChargingConstraint create() {
            return new ChargingConstraint();
        }
    }
}
