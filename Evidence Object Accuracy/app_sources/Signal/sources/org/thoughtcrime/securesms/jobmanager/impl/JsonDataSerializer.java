package org.thoughtcrime.securesms.jobmanager.impl;

import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.util.JsonUtils;

/* loaded from: classes4.dex */
public class JsonDataSerializer implements Data.Serializer {
    private static final String TAG = Log.tag(JsonDataSerializer.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Data.Serializer
    public String serialize(Data data) {
        try {
            return JsonUtils.toJson(data);
        } catch (IOException e) {
            Log.e(TAG, "Failed to serialize to JSON.", e);
            throw new AssertionError(e);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Data.Serializer
    public Data deserialize(String str) {
        try {
            return (Data) JsonUtils.fromJson(str, Data.class);
        } catch (IOException e) {
            Log.e(TAG, "Failed to deserialize JSON.", e);
            throw new AssertionError(e);
        }
    }
}
