package org.thoughtcrime.securesms.jobmanager;

import android.text.TextUtils;
import java.util.Locale;

/* loaded from: classes4.dex */
public class JobLogger {
    public static String format(Job job, String str) {
        return format(job, "", str);
    }

    public static String format(Job job, String str, String str2) {
        String str3;
        String str4;
        String str5;
        String id = job.getId();
        if (TextUtils.isEmpty(str)) {
            str3 = "";
        } else {
            str3 = "[" + str + "]";
        }
        long currentTimeMillis = System.currentTimeMillis() - job.getParameters().getCreateTime();
        int runAttempt = job.getRunAttempt() + 1;
        if (job.getParameters().getMaxAttempts() == -1) {
            str4 = "Unlimited";
        } else {
            str4 = String.valueOf(job.getParameters().getMaxAttempts());
        }
        if (job.getParameters().getLifespan() == -1) {
            str5 = "Immortal";
        } else {
            str5 = String.valueOf(job.getParameters().getLifespan()) + " ms";
        }
        return String.format(Locale.US, "[%s][%s]%s %s (Time Since Submission: %d ms, Lifespan: %s, Run Attempt: %d/%s, Queue: %s)", "JOB::" + id, job.getClass().getSimpleName(), str3, str2, Long.valueOf(currentTimeMillis), str5, Integer.valueOf(runAttempt), str4, job.getParameters().getQueue());
    }
}
