package org.thoughtcrime.securesms.jobmanager.impl;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.ConstraintObserver;

/* loaded from: classes4.dex */
public class ChargingConstraintObserver implements ConstraintObserver {
    private static final String REASON = Log.tag(ChargingConstraintObserver.class);
    private static final int STATUS_BATTERY;
    private static volatile boolean charging;
    private final Application application;

    public ChargingConstraintObserver(Application application) {
        this.application = application;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.ConstraintObserver
    public void register(final ConstraintObserver.Notifier notifier) {
        charging = isCharging(this.application.registerReceiver(new BroadcastReceiver() { // from class: org.thoughtcrime.securesms.jobmanager.impl.ChargingConstraintObserver.1
            @Override // android.content.BroadcastReceiver
            public void onReceive(Context context, Intent intent) {
                boolean z = ChargingConstraintObserver.charging;
                boolean unused = ChargingConstraintObserver.charging = ChargingConstraintObserver.isCharging(intent);
                if (ChargingConstraintObserver.charging && !z) {
                    notifier.onConstraintMet(ChargingConstraintObserver.REASON);
                }
            }
        }, new IntentFilter("android.intent.action.BATTERY_CHANGED")));
    }

    public static boolean isCharging() {
        return charging;
    }

    public static boolean isCharging(Intent intent) {
        return (intent == null || intent.getIntExtra("plugged", 0) == 0) ? false : true;
    }
}
