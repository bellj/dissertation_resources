package org.thoughtcrime.securesms.jobmanager.impl;

import android.app.Application;
import android.app.job.JobInfo;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import org.thoughtcrime.securesms.jobmanager.Constraint;

/* loaded from: classes4.dex */
public class NetworkConstraint implements Constraint {
    public static final String KEY;
    private final Application application;

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public String getJobSchedulerKeyPart() {
        return "NETWORK";
    }

    private NetworkConstraint(Application application) {
        this.application = application;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public boolean isMet() {
        return isMet(this.application);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public void applyToJobInfo(JobInfo.Builder builder) {
        builder.setRequiredNetworkType(1);
    }

    public static boolean isMet(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Constraint.Factory<NetworkConstraint> {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @Override // org.thoughtcrime.securesms.jobmanager.Constraint.Factory
        public NetworkConstraint create() {
            return new NetworkConstraint(this.application);
        }
    }
}
