package org.thoughtcrime.securesms.jobmanager.migrations;

import android.content.Context;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.PushDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.JobMigration;
import org.thoughtcrime.securesms.jobs.FailingJob;
import org.thoughtcrime.securesms.jobs.PushDecryptMessageJob;

/* loaded from: classes4.dex */
public class PushDecryptMessageJobEnvelopeMigration extends JobMigration {
    private static final String TAG = Log.tag(PushDecryptMessageJobEnvelopeMigration.class);
    private final PushDatabase pushDatabase = SignalDatabase.push();

    public PushDecryptMessageJobEnvelopeMigration(Context context) {
        super(8);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.JobMigration
    public JobMigration.JobData migrate(JobMigration.JobData jobData) {
        if (!PushDecryptMessageJob.KEY.equals(jobData.getFactoryKey())) {
            return jobData;
        }
        Log.i(TAG, "Found a PushDecryptJob to migrate.");
        return migratePushDecryptMessageJob(this.pushDatabase, jobData);
    }

    private static JobMigration.JobData migratePushDecryptMessageJob(PushDatabase pushDatabase, JobMigration.JobData jobData) {
        Data data = jobData.getData();
        if (data.hasLong("message_id")) {
            try {
                return jobData.withData(jobData.getData().buildUpon().putBlobAsString("envelope", pushDatabase.get(data.getLong("message_id")).serialize()).build());
            } catch (NoSuchMessageException unused) {
                Log.w(TAG, "Failed to find envelope in DB! Failing.");
                return jobData.withFactoryKey(FailingJob.KEY);
            }
        } else {
            Log.w(TAG, "No message_id property?");
            return jobData;
        }
    }
}
