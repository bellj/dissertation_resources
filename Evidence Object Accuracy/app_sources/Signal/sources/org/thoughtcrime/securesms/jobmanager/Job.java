package org.thoughtcrime.securesms.jobmanager;

import android.content.Context;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.signal.core.util.logging.Log;

/* loaded from: classes.dex */
public abstract class Job {
    private static final String TAG = Log.tag(Job.class);
    private volatile boolean canceled;
    protected Context context;
    private long nextRunAttemptTime;
    private final Parameters parameters;
    private int runAttempt;

    /* loaded from: classes4.dex */
    public interface Factory<T extends Job> {
        T create(Parameters parameters, Data data);
    }

    public abstract String getFactoryKey();

    public void onAdded() {
    }

    public abstract void onFailure();

    public void onRetry() {
    }

    public abstract Result run();

    public abstract Data serialize();

    public Job(Parameters parameters) {
        this.parameters = parameters;
    }

    public final String getId() {
        return this.parameters.getId();
    }

    public final Parameters getParameters() {
        return this.parameters;
    }

    public final int getRunAttempt() {
        return this.runAttempt;
    }

    public final long getNextRunAttemptTime() {
        return this.nextRunAttemptTime;
    }

    public final Data getInputData() {
        return this.parameters.getInputData();
    }

    public final Data requireInputData() {
        Data inputData = this.parameters.getInputData();
        Objects.requireNonNull(inputData);
        return inputData;
    }

    public final void setContext(Context context) {
        this.context = context;
    }

    public final void setRunAttempt(int i) {
        this.runAttempt = i;
    }

    public final void setNextRunAttemptTime(long j) {
        this.nextRunAttemptTime = j;
    }

    public final void cancel() {
        this.canceled = true;
    }

    public final void onSubmit() {
        Log.i(TAG, JobLogger.format(this, "onSubmit()"));
        onAdded();
    }

    public final boolean isCanceled() {
        return this.canceled;
    }

    /* loaded from: classes4.dex */
    public static final class Result {
        private static final Result FAILURE = new Result(ResultType.FAILURE, null, null, -1);
        private static final int INVALID_BACKOFF;
        private static final Result SUCCESS_NO_DATA = new Result(ResultType.SUCCESS, null, null, -1);
        private final long backoffInterval;
        private final Data outputData;
        private final ResultType resultType;
        private final RuntimeException runtimeException;

        /* access modifiers changed from: private */
        /* loaded from: classes4.dex */
        public enum ResultType {
            SUCCESS,
            FAILURE,
            RETRY
        }

        private Result(ResultType resultType, RuntimeException runtimeException, Data data, long j) {
            this.resultType = resultType;
            this.runtimeException = runtimeException;
            this.outputData = data;
            this.backoffInterval = j;
        }

        public static Result success() {
            return SUCCESS_NO_DATA;
        }

        public static Result success(Data data) {
            return new Result(ResultType.SUCCESS, null, data, -1);
        }

        public static Result retry(long j) {
            return new Result(ResultType.RETRY, null, null, j);
        }

        public static Result failure() {
            return FAILURE;
        }

        public static Result fatalFailure(RuntimeException runtimeException) {
            return new Result(ResultType.FAILURE, runtimeException, null, -1);
        }

        public boolean isSuccess() {
            return this.resultType == ResultType.SUCCESS;
        }

        public boolean isRetry() {
            return this.resultType == ResultType.RETRY;
        }

        public boolean isFailure() {
            return this.resultType == ResultType.FAILURE;
        }

        public RuntimeException getException() {
            return this.runtimeException;
        }

        public Data getOutputData() {
            return this.outputData;
        }

        public long getBackoffInterval() {
            return this.backoffInterval;
        }

        public String toString() {
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$jobmanager$Job$Result$ResultType[this.resultType.ordinal()];
            if (i == 1 || i == 2) {
                return this.resultType.toString();
            }
            if (i != 3) {
                return "UNKNOWN?";
            }
            return this.runtimeException == null ? this.resultType.toString() : "FATAL_FAILURE";
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.jobmanager.Job$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$jobmanager$Job$Result$ResultType;

        static {
            int[] iArr = new int[Result.ResultType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$jobmanager$Job$Result$ResultType = iArr;
            try {
                iArr[Result.ResultType.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$jobmanager$Job$Result$ResultType[Result.ResultType.RETRY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$jobmanager$Job$Result$ResultType[Result.ResultType.FAILURE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class Parameters {
        public static final long IMMORTAL;
        public static final String MIGRATION_QUEUE_KEY;
        public static final int UNLIMITED;
        private final List<String> constraintKeys;
        private final long createTime;
        private final String id;
        private final Data inputData;
        private final long lifespan;
        private final int maxAttempts;
        private final int maxInstancesForFactory;
        private final int maxInstancesForQueue;
        private final boolean memoryOnly;
        private final String queue;

        /* synthetic */ Parameters(String str, long j, long j2, int i, int i2, int i3, String str2, List list, Data data, boolean z, AnonymousClass1 r13) {
            this(str, j, j2, i, i2, i3, str2, list, data, z);
        }

        private Parameters(String str, long j, long j2, int i, int i2, int i3, String str2, List<String> list, Data data, boolean z) {
            this.id = str;
            this.createTime = j;
            this.lifespan = j2;
            this.maxAttempts = i;
            this.maxInstancesForFactory = i2;
            this.maxInstancesForQueue = i3;
            this.queue = str2;
            this.constraintKeys = list;
            this.inputData = data;
            this.memoryOnly = z;
        }

        public String getId() {
            return this.id;
        }

        public long getCreateTime() {
            return this.createTime;
        }

        public long getLifespan() {
            return this.lifespan;
        }

        public int getMaxAttempts() {
            return this.maxAttempts;
        }

        public int getMaxInstancesForFactory() {
            return this.maxInstancesForFactory;
        }

        public int getMaxInstancesForQueue() {
            return this.maxInstancesForQueue;
        }

        public String getQueue() {
            return this.queue;
        }

        public List<String> getConstraintKeys() {
            return this.constraintKeys;
        }

        Data getInputData() {
            return this.inputData;
        }

        public boolean isMemoryOnly() {
            return this.memoryOnly;
        }

        public Builder toBuilder() {
            return new Builder(this.id, this.createTime, this.lifespan, this.maxAttempts, this.maxInstancesForFactory, this.maxInstancesForQueue, this.queue, this.constraintKeys, this.inputData, this.memoryOnly, null);
        }

        /* loaded from: classes4.dex */
        public static final class Builder {
            private List<String> constraintKeys;
            private long createTime;
            private String id;
            private Data inputData;
            private long lifespan;
            private int maxAttempts;
            private int maxInstancesForFactory;
            private int maxInstancesForQueue;
            private boolean memoryOnly;
            private String queue;

            /* synthetic */ Builder(String str, long j, long j2, int i, int i2, int i3, String str2, List list, Data data, boolean z, AnonymousClass1 r13) {
                this(str, j, j2, i, i2, i3, str2, list, data, z);
            }

            public Builder() {
                this(UUID.randomUUID().toString());
            }

            public Builder(String str) {
                this(str, System.currentTimeMillis(), -1, 1, -1, -1, null, new LinkedList(), null, false);
            }

            private Builder(String str, long j, long j2, int i, int i2, int i3, String str2, List<String> list, Data data, boolean z) {
                this.id = str;
                this.createTime = j;
                this.lifespan = j2;
                this.maxAttempts = i;
                this.maxInstancesForFactory = i2;
                this.maxInstancesForQueue = i3;
                this.queue = str2;
                this.constraintKeys = list;
                this.inputData = data;
                this.memoryOnly = z;
            }

            public Builder setCreateTime(long j) {
                this.createTime = j;
                return this;
            }

            public Builder setLifespan(long j) {
                this.lifespan = j;
                return this;
            }

            public Builder setMaxAttempts(int i) {
                this.maxAttempts = i;
                return this;
            }

            public Builder setMaxInstancesForFactory(int i) {
                this.maxInstancesForFactory = i;
                return this;
            }

            public Builder setMaxInstancesForQueue(int i) {
                this.maxInstancesForQueue = i;
                return this;
            }

            public Builder setQueue(String str) {
                this.queue = str;
                return this;
            }

            public Builder addConstraint(String str) {
                this.constraintKeys.add(str);
                return this;
            }

            public Builder setConstraints(List<String> list) {
                this.constraintKeys.clear();
                this.constraintKeys.addAll(list);
                return this;
            }

            public Builder setMemoryOnly(boolean z) {
                this.memoryOnly = z;
                return this;
            }

            public Builder setInputData(Data data) {
                this.inputData = data;
                return this;
            }

            public Parameters build() {
                return new Parameters(this.id, this.createTime, this.lifespan, this.maxAttempts, this.maxInstancesForFactory, this.maxInstancesForQueue, this.queue, this.constraintKeys, this.inputData, this.memoryOnly, null);
            }
        }
    }
}
