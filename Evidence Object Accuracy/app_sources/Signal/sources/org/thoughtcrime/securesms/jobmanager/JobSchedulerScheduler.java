package org.thoughtcrime.securesms.jobmanager;

import android.app.Application;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.JobManager;

/* loaded from: classes4.dex */
public final class JobSchedulerScheduler implements Scheduler {
    private static final String TAG = Log.tag(JobSchedulerScheduler.class);
    private final Application application;

    public JobSchedulerScheduler(Application application) {
        this.application = application;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Scheduler
    public void schedule(long j, List<Constraint> list) {
        SignalExecutors.BOUNDED.execute(new Runnable(list, j) { // from class: org.thoughtcrime.securesms.jobmanager.JobSchedulerScheduler$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobSchedulerScheduler.this.lambda$schedule$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$schedule$0(List list, long j) {
        String str;
        JobScheduler jobScheduler = (JobScheduler) this.application.getSystemService(JobScheduler.class);
        if (list.isEmpty()) {
            str = "";
        } else {
            str = (String) Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.jobmanager.JobSchedulerScheduler$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ((Constraint) obj).getJobSchedulerKeyPart();
                }
            }).withoutNulls().sorted().collect(Collectors.joining("-"));
        }
        int hashCode = str.hashCode();
        if (jobScheduler.getPendingJob(hashCode) == null) {
            Log.i(TAG, String.format(Locale.US, "JobScheduler enqueue of %s (%d)", str, Integer.valueOf(hashCode)));
            JobInfo.Builder persisted = new JobInfo.Builder(hashCode, new ComponentName(this.application, SystemService.class)).setMinimumLatency(j).setPersisted(true);
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ((Constraint) it.next()).applyToJobInfo(persisted);
            }
            jobScheduler.schedule(persisted.build());
        }
    }

    /* loaded from: classes4.dex */
    public static class SystemService extends JobService {
        @Override // android.app.job.JobService
        public boolean onStopJob(JobParameters jobParameters) {
            return false;
        }

        @Override // android.app.job.JobService
        public boolean onStartJob(final JobParameters jobParameters) {
            final JobManager jobManager = ApplicationDependencies.getJobManager();
            String str = JobSchedulerScheduler.TAG;
            Log.i(str, "Waking due to job: " + jobParameters.getJobId());
            jobManager.addOnEmptyQueueListener(new JobManager.EmptyQueueListener() { // from class: org.thoughtcrime.securesms.jobmanager.JobSchedulerScheduler.SystemService.1
                @Override // org.thoughtcrime.securesms.jobmanager.JobManager.EmptyQueueListener
                public void onQueueEmpty() {
                    jobManager.removeOnEmptyQueueListener(this);
                    SystemService.this.jobFinished(jobParameters, false);
                }
            });
            jobManager.wakeUp();
            return true;
        }
    }
}
