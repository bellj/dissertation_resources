package org.thoughtcrime.securesms.jobmanager.impl;

import android.app.Application;
import android.app.job.JobInfo;
import org.thoughtcrime.securesms.jobmanager.Constraint;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public class NetworkOrCellServiceConstraint implements Constraint {
    public static final String KEY;
    public static final String LEGACY_KEY;
    private final Application application;
    private final NetworkConstraint networkConstraint;

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public void applyToJobInfo(JobInfo.Builder builder) {
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public /* synthetic */ String getJobSchedulerKeyPart() {
        return Constraint.CC.$default$getJobSchedulerKeyPart(this);
    }

    private NetworkOrCellServiceConstraint(Application application) {
        this.application = application;
        this.networkConstraint = new NetworkConstraint.Factory(application).create();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Constraint
    public boolean isMet() {
        if (SignalStore.settings().isWifiCallingCompatibilityModeEnabled()) {
            return this.networkConstraint.isMet() || hasCellService(this.application);
        }
        return hasCellService(this.application);
    }

    private static boolean hasCellService(Application application) {
        return CellServiceConstraintObserver.getInstance(application).hasService();
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Constraint.Factory<NetworkOrCellServiceConstraint> {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @Override // org.thoughtcrime.securesms.jobmanager.Constraint.Factory
        public NetworkOrCellServiceConstraint create() {
            return new NetworkOrCellServiceConstraint(this.application);
        }
    }
}
