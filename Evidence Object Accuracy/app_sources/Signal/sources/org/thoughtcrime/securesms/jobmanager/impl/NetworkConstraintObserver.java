package org.thoughtcrime.securesms.jobmanager.impl;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.ConstraintObserver;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;

/* loaded from: classes4.dex */
public class NetworkConstraintObserver implements ConstraintObserver {
    private static final String REASON = Log.tag(NetworkConstraintObserver.class);
    private final Application application;

    public NetworkConstraintObserver(Application application) {
        this.application = application;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.ConstraintObserver
    public void register(final ConstraintObserver.Notifier notifier) {
        this.application.registerReceiver(new BroadcastReceiver() { // from class: org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraintObserver.1
            @Override // android.content.BroadcastReceiver
            public void onReceive(Context context, Intent intent) {
                if (new NetworkConstraint.Factory(NetworkConstraintObserver.this.application).create().isMet()) {
                    notifier.onConstraintMet(NetworkConstraintObserver.REASON);
                }
            }
        }, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }
}
