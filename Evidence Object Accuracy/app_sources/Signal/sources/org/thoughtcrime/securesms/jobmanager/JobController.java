package org.thoughtcrime.securesms.jobmanager;

import android.app.Application;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobController;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobmanager.persistence.ConstraintSpec;
import org.thoughtcrime.securesms.jobmanager.persistence.DependencySpec;
import org.thoughtcrime.securesms.jobmanager.persistence.FullSpec;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;
import org.thoughtcrime.securesms.jobmanager.persistence.JobStorage;
import org.thoughtcrime.securesms.util.Debouncer;

/* loaded from: classes4.dex */
public class JobController {
    private static final String TAG = Log.tag(JobController.class);
    private final Application application;
    private final Callback callback;
    private final ConstraintInstantiator constraintInstantiator;
    private final Data.Serializer dataSerializer;
    private final Debouncer debouncer;
    private final JobInstantiator jobInstantiator;
    private final JobStorage jobStorage;
    private final JobTracker jobTracker;
    private final Map<String, Job> runningJobs = new HashMap();
    private final Scheduler scheduler;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onEmpty();
    }

    public JobController(Application application, JobStorage jobStorage, JobInstantiator jobInstantiator, ConstraintInstantiator constraintInstantiator, Data.Serializer serializer, JobTracker jobTracker, Scheduler scheduler, Debouncer debouncer, Callback callback) {
        this.application = application;
        this.jobStorage = jobStorage;
        this.jobInstantiator = jobInstantiator;
        this.constraintInstantiator = constraintInstantiator;
        this.dataSerializer = serializer;
        this.jobTracker = jobTracker;
        this.scheduler = scheduler;
        this.debouncer = debouncer;
        this.callback = callback;
    }

    public synchronized void init() {
        this.jobStorage.updateAllJobsToBePending();
        notifyAll();
    }

    public synchronized void wakeUp() {
        notifyAll();
    }

    public synchronized void submitNewJobChain(List<List<Job>> list) {
        List<List<Job>> list2 = Stream.of(list).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda19
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((List) obj).isEmpty();
            }
        }).toList();
        if (list2.isEmpty()) {
            Log.w(TAG, "Tried to submit an empty job chain. Skipping.");
        } else if (chainExceedsMaximumInstances(list2)) {
            Job job = list2.get(0).get(0);
            this.jobTracker.onStateChange(job, JobTracker.JobState.IGNORED);
            String str = TAG;
            Log.w(str, JobLogger.format(job, "Already at the max instance count. Factory limit: " + job.getParameters().getMaxInstancesForFactory() + ", Queue limit: " + job.getParameters().getMaxInstancesForQueue() + ". Skipping."));
        } else {
            insertJobChain(list2);
            triggerOnSubmit(list2);
            notifyAll();
            scheduleJobs(list2.get(0));
        }
    }

    public synchronized void submitJobWithExistingDependencies(Job job, Collection<String> collection, String str) {
        List<List<Job>> singletonList = Collections.singletonList(Collections.singletonList(job));
        if (chainExceedsMaximumInstances(singletonList)) {
            this.jobTracker.onStateChange(job, JobTracker.JobState.IGNORED);
            String str2 = TAG;
            Log.w(str2, JobLogger.format(job, "Already at the max instance count. Factory limit: " + job.getParameters().getMaxInstancesForFactory() + ", Queue limit: " + job.getParameters().getMaxInstancesForQueue() + ". Skipping."));
            return;
        }
        HashSet hashSet = new HashSet(collection);
        Set set = (Set) Stream.of(collection).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda12
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return JobController.m1889$r8$lambda$0tY4USBs4aXzTUNQwsOaGRYJrU(JobController.this, (String) obj);
            }
        }).collect(Collectors.toSet());
        if (str != null) {
            List list = Stream.of(this.jobStorage.getJobsInQueue(str)).map(new JobController$$ExternalSyntheticLambda6()).toList();
            hashSet.addAll(list);
            set.addAll(list);
        }
        if (this.jobTracker.haveAnyFailed(hashSet)) {
            Log.w(TAG, "This job depends on a job that failed! Failing this job immediately.");
            List<Job> onFailure = onFailure(job);
            job.setContext(this.application);
            job.onFailure();
            Stream.of(onFailure).forEach(new JobController$$ExternalSyntheticLambda0());
            return;
        }
        this.jobStorage.insertJobs(Collections.singletonList(buildFullSpec(job, set)));
        scheduleJobs(Collections.singletonList(job));
        triggerOnSubmit(singletonList);
        notifyAll();
    }

    public /* synthetic */ boolean lambda$submitJobWithExistingDependencies$0(String str) {
        return this.jobStorage.getJobSpec(str) != null;
    }

    public synchronized void cancelJob(String str) {
        Job job = this.runningJobs.get(str);
        if (job != null) {
            Log.w(TAG, JobLogger.format(job, "Canceling while running."));
            job.cancel();
        } else {
            JobSpec jobSpec = this.jobStorage.getJobSpec(str);
            if (jobSpec != null) {
                Job createJob = createJob(jobSpec, this.jobStorage.getConstraintSpecs(str));
                String str2 = TAG;
                Log.w(str2, JobLogger.format(createJob, "Canceling while inactive."));
                Log.w(str2, JobLogger.format(createJob, "Job failed."));
                createJob.cancel();
                List<Job> onFailure = onFailure(createJob);
                createJob.onFailure();
                Stream.of(onFailure).forEach(new JobController$$ExternalSyntheticLambda0());
            } else {
                String str3 = TAG;
                Log.w(str3, "Tried to cancel JOB::" + str + ", but it could not be found.");
            }
        }
    }

    public synchronized void cancelAllInQueue(String str) {
        Stream.of(this.jobStorage.getJobsInQueue(str)).map(new JobController$$ExternalSyntheticLambda6()).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                JobController.this.cancelJob((String) obj);
            }
        });
    }

    public synchronized void update(JobUpdater jobUpdater) {
        List<JobSpec> allJobSpecs = this.jobStorage.getAllJobSpecs();
        LinkedList linkedList = new LinkedList();
        for (JobSpec jobSpec : allJobSpecs) {
            JobSpec update = jobUpdater.update(jobSpec, this.dataSerializer);
            if (update != jobSpec) {
                linkedList.add(update);
            }
        }
        this.jobStorage.updateJobs(linkedList);
        notifyAll();
    }

    public synchronized List<JobSpec> findJobs(j$.util.function.Predicate<JobSpec> predicate) {
        Stream of;
        of = Stream.of(this.jobStorage.getAllJobSpecs());
        Objects.requireNonNull(predicate);
        return of.filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda20
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return j$.util.function.Predicate.this.test((JobSpec) obj);
            }
        }).toList();
    }

    public synchronized void onRetry(Job job, long j) {
        if (j > 0) {
            int runAttempt = job.getRunAttempt() + 1;
            long currentTimeMillis = j + System.currentTimeMillis();
            this.jobStorage.updateJobAfterRetry(job.getId(), false, runAttempt, currentTimeMillis, this.dataSerializer.serialize(job.serialize()));
            this.jobTracker.onStateChange(job, JobTracker.JobState.PENDING);
            Stream map = Stream.of(this.jobStorage.getConstraintSpecs(job.getId())).map(new JobController$$ExternalSyntheticLambda11());
            ConstraintInstantiator constraintInstantiator = this.constraintInstantiator;
            Objects.requireNonNull(constraintInstantiator);
            List<Constraint> list = map.map(new JobController$$ExternalSyntheticLambda18(constraintInstantiator)).toList();
            long max = Math.max(0L, currentTimeMillis - System.currentTimeMillis());
            String str = TAG;
            Log.i(str, JobLogger.format(job, "Scheduling a retry in " + max + " ms."));
            this.scheduler.schedule(max, list);
            notifyAll();
        } else {
            throw new IllegalArgumentException("Invalid backoff interval! " + j);
        }
    }

    public synchronized void onJobFinished(Job job) {
        this.runningJobs.remove(job.getId());
    }

    public synchronized void onSuccess(Job job, Data data) {
        if (data != null) {
            Stream map = Stream.of(this.jobStorage.getDependencySpecsThatDependOnJob(job.getId())).map(new JobController$$ExternalSyntheticLambda3());
            JobStorage jobStorage = this.jobStorage;
            Objects.requireNonNull(jobStorage);
            this.jobStorage.updateJobs(map.map(new JobController$$ExternalSyntheticLambda4(jobStorage)).map(new Function(data) { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda5
                public final /* synthetic */ Data f$1;

                {
                    this.f$1 = r2;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return JobController.m1893$r8$lambda$LDrLqYGlIJL7wN7ggQs7ijAw(JobController.this, this.f$1, (JobSpec) obj);
                }
            }).toList());
        }
        this.jobStorage.deleteJob(job.getId());
        this.jobTracker.onStateChange(job, JobTracker.JobState.SUCCESS);
        notifyAll();
    }

    public synchronized List<Job> onFailure(Job job) {
        List<Job> list;
        Stream map = Stream.of(this.jobStorage.getDependencySpecsThatDependOnJob(job.getId())).map(new JobController$$ExternalSyntheticLambda3());
        JobStorage jobStorage = this.jobStorage;
        Objects.requireNonNull(jobStorage);
        list = map.map(new JobController$$ExternalSyntheticLambda4(jobStorage)).withoutNulls().map(new Function() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda16
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return JobController.m1892$r8$lambda$HzUVG4uMEuzqEodaEkaIoEYqu8(JobController.this, (JobSpec) obj);
            }
        }).toList();
        ArrayList arrayList = new ArrayList(list.size() + 1);
        arrayList.add(job);
        arrayList.addAll(list);
        this.jobStorage.deleteJobs(Stream.of(arrayList).map(new JobController$$ExternalSyntheticLambda10()).toList());
        Stream.of(arrayList).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda17
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                JobController.$r8$lambda$Rww4UmEjme68rq8DQtpmbhhvjDs(JobController.this, (Job) obj);
            }
        });
        return list;
    }

    public /* synthetic */ Job lambda$onFailure$2(JobSpec jobSpec) {
        return createJob(jobSpec, this.jobStorage.getConstraintSpecs(jobSpec.getId()));
    }

    public /* synthetic */ void lambda$onFailure$3(Job job) {
        this.jobTracker.onStateChange(job, JobTracker.JobState.FAILURE);
    }

    public synchronized Job pullNextEligibleJobForExecution(JobPredicate jobPredicate) {
        Job nextEligibleJobForExecution;
        while (true) {
            try {
                nextEligibleJobForExecution = getNextEligibleJobForExecution(jobPredicate);
                if (nextEligibleJobForExecution == null) {
                    if (this.runningJobs.isEmpty()) {
                        Debouncer debouncer = this.debouncer;
                        Callback callback = this.callback;
                        Objects.requireNonNull(callback);
                        debouncer.publish(new Runnable() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda22
                            @Override // java.lang.Runnable
                            public final void run() {
                                JobController.Callback.this.onEmpty();
                            }
                        });
                    }
                    wait();
                } else {
                    this.jobStorage.updateJobRunningState(nextEligibleJobForExecution.getId(), true);
                    this.runningJobs.put(nextEligibleJobForExecution.getId(), nextEligibleJobForExecution);
                    this.jobTracker.onStateChange(nextEligibleJobForExecution, JobTracker.JobState.RUNNING);
                }
            } catch (InterruptedException e) {
                Log.e(TAG, "Interrupted.");
                throw new AssertionError(e);
            }
        }
        return nextEligibleJobForExecution;
    }

    public synchronized String getDebugInfo() {
        StringBuilder sb;
        List<JobSpec> allJobSpecs = this.jobStorage.getAllJobSpecs();
        List<ConstraintSpec> allConstraintSpecs = this.jobStorage.getAllConstraintSpecs();
        List<DependencySpec> allDependencySpecs = this.jobStorage.getAllDependencySpecs();
        sb = new StringBuilder();
        sb.append("-- Jobs\n");
        if (!allJobSpecs.isEmpty()) {
            Stream.of(allJobSpecs).forEach(new Consumer(sb) { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda13
                public final /* synthetic */ StringBuilder f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    JobController.$r8$lambda$NOZX7DAXYL6H_RpiyEbb_Q5dId8(this.f$0, (JobSpec) obj);
                }
            });
        } else {
            sb.append("None\n");
        }
        sb.append("\n-- Constraints\n");
        if (!allConstraintSpecs.isEmpty()) {
            Stream.of(allConstraintSpecs).forEach(new Consumer(sb) { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda14
                public final /* synthetic */ StringBuilder f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    JobController.$r8$lambda$EDjqmdiDZCoGdbhzpMkOj67V0o0(this.f$0, (ConstraintSpec) obj);
                }
            });
        } else {
            sb.append("None\n");
        }
        sb.append("\n-- Dependencies\n");
        if (!allDependencySpecs.isEmpty()) {
            Stream.of(allDependencySpecs).forEach(new Consumer(sb) { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda15
                public final /* synthetic */ StringBuilder f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    JobController.$r8$lambda$DBKhnEKvMCOsZJmVZ8NBbfHGvC0(this.f$0, (DependencySpec) obj);
                }
            });
        } else {
            sb.append("None\n");
        }
        return sb.toString();
    }

    public static /* synthetic */ void lambda$getDebugInfo$4(StringBuilder sb, JobSpec jobSpec) {
        sb.append(jobSpec.toString());
        sb.append('\n');
    }

    public static /* synthetic */ void lambda$getDebugInfo$5(StringBuilder sb, ConstraintSpec constraintSpec) {
        sb.append(constraintSpec.toString());
        sb.append('\n');
    }

    public static /* synthetic */ void lambda$getDebugInfo$6(StringBuilder sb, DependencySpec dependencySpec) {
        sb.append(dependencySpec.toString());
        sb.append('\n');
    }

    public synchronized boolean areQueuesEmpty(Set<String> set) {
        return this.jobStorage.areQueuesEmpty(set);
    }

    private boolean chainExceedsMaximumInstances(List<List<Job>> list) {
        if (list.size() == 1 && list.get(0).size() == 1) {
            Job job = list.get(0).get(0);
            if (job.getParameters().getMaxInstancesForFactory() != -1 && this.jobStorage.getJobCountForFactory(job.getFactoryKey()) >= job.getParameters().getMaxInstancesForFactory()) {
                return true;
            }
            if ((job.getParameters().getQueue() == null || job.getParameters().getMaxInstancesForQueue() == -1 || this.jobStorage.getJobCountForFactoryAndQueue(job.getFactoryKey(), job.getParameters().getQueue()) < job.getParameters().getMaxInstancesForQueue()) ? false : true) {
                return true;
            }
        }
        return false;
    }

    private void triggerOnSubmit(List<List<Job>> list) {
        Stream.of(list).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                JobController.m1890$r8$lambda$5pJLa5KwJLJqyV3uq6hIvIGwrg(JobController.this, (List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$triggerOnSubmit$8(List list) {
        Stream.of(list).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                JobController.m1891$r8$lambda$60nJTrRjRapqXMheBlI8I4GrU(JobController.this, (Job) obj);
            }
        });
    }

    public /* synthetic */ void lambda$triggerOnSubmit$7(Job job) {
        job.setContext(this.application);
        job.onSubmit();
    }

    private void insertJobChain(List<List<Job>> list) {
        LinkedList linkedList = new LinkedList();
        List emptyList = Collections.emptyList();
        for (List<Job> list2 : list) {
            for (Job job : list2) {
                linkedList.add(buildFullSpec(job, emptyList));
            }
            emptyList = Stream.of(list2).map(new JobController$$ExternalSyntheticLambda10()).toList();
        }
        this.jobStorage.insertJobs(linkedList);
    }

    private FullSpec buildFullSpec(Job job, Collection<String> collection) {
        job.setRunAttempt(0);
        JobSpec jobSpec = new JobSpec(job.getId(), job.getFactoryKey(), job.getParameters().getQueue(), System.currentTimeMillis(), job.getNextRunAttemptTime(), job.getRunAttempt(), job.getParameters().getMaxAttempts(), job.getParameters().getLifespan(), this.dataSerializer.serialize(job.serialize()), null, false, job.getParameters().isMemoryOnly());
        return new FullSpec(jobSpec, Stream.of(job.getParameters().getConstraintKeys()).map(new Function() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return JobController.$r8$lambda$MOgRXcKr5XkfGDycnylqBM7hEJc(JobSpec.this, (String) obj);
            }
        }).toList(), Stream.of(collection).map(new Function(job) { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda9
            public final /* synthetic */ Job f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return JobController.$r8$lambda$w2w6454Nqjj69Vw8Mt79zNBT8ss(JobController.this, this.f$1, (String) obj);
            }
        }).toList());
    }

    public static /* synthetic */ ConstraintSpec lambda$buildFullSpec$9(JobSpec jobSpec, String str) {
        return new ConstraintSpec(jobSpec.getId(), str, jobSpec.isMemoryOnly());
    }

    public /* synthetic */ DependencySpec lambda$buildFullSpec$10(Job job, String str) {
        JobSpec jobSpec = this.jobStorage.getJobSpec(str);
        return new DependencySpec(job.getId(), str, job.getParameters().isMemoryOnly() || (jobSpec != null && jobSpec.isMemoryOnly()));
    }

    private void scheduleJobs(List<Job> list) {
        for (Job job : list) {
            List<String> constraintKeys = job.getParameters().getConstraintKeys();
            ArrayList arrayList = new ArrayList(constraintKeys.size());
            for (String str : constraintKeys) {
                arrayList.add(this.constraintInstantiator.instantiate(str));
            }
            this.scheduler.schedule(0, arrayList);
        }
    }

    private Job getNextEligibleJobForExecution(JobPredicate jobPredicate) {
        Stream of = Stream.of(this.jobStorage.getPendingJobsWithNoDependenciesInCreatedOrder(System.currentTimeMillis()));
        Objects.requireNonNull(jobPredicate);
        for (JobSpec jobSpec : of.filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda21
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return JobPredicate.this.shouldRun((JobSpec) obj);
            }
        }).toList()) {
            List<ConstraintSpec> constraintSpecs = this.jobStorage.getConstraintSpecs(jobSpec.getId());
            Stream map = Stream.of(constraintSpecs).map(new JobController$$ExternalSyntheticLambda11());
            ConstraintInstantiator constraintInstantiator = this.constraintInstantiator;
            Objects.requireNonNull(constraintInstantiator);
            if (Stream.of(map.map(new JobController$$ExternalSyntheticLambda18(constraintInstantiator)).toList()).allMatch(new AlarmManagerScheduler$$ExternalSyntheticLambda0())) {
                return createJob(jobSpec, constraintSpecs);
            }
        }
        return null;
    }

    private Job createJob(JobSpec jobSpec, List<ConstraintSpec> list) {
        Job.Parameters buildJobParameters = buildJobParameters(jobSpec, list);
        try {
            Job instantiate = this.jobInstantiator.instantiate(jobSpec.getFactoryKey(), buildJobParameters, this.dataSerializer.deserialize(jobSpec.getSerializedData()));
            instantiate.setRunAttempt(jobSpec.getRunAttempt());
            instantiate.setNextRunAttemptTime(jobSpec.getNextRunAttemptTime());
            instantiate.setContext(this.application);
            return instantiate;
        } catch (RuntimeException e) {
            String str = TAG;
            Log.e(str, "Failed to instantiate job! Failing it and its dependencies without calling Job#onFailure. Crash imminent.");
            List<String> list2 = Stream.of(this.jobStorage.getDependencySpecsThatDependOnJob(jobSpec.getId())).map(new JobController$$ExternalSyntheticLambda3()).toList();
            this.jobStorage.deleteJob(jobSpec.getId());
            this.jobStorage.deleteJobs(list2);
            Log.e(str, "Failed " + list2.size() + " dependent jobs.");
            throw e;
        }
    }

    private Job.Parameters buildJobParameters(JobSpec jobSpec, List<ConstraintSpec> list) {
        return new Job.Parameters.Builder(jobSpec.getId()).setCreateTime(jobSpec.getCreateTime()).setLifespan(jobSpec.getLifespan()).setMaxAttempts(jobSpec.getMaxAttempts()).setQueue(jobSpec.getQueueKey()).setConstraints(Stream.of(list).map(new JobController$$ExternalSyntheticLambda11()).toList()).setInputData(jobSpec.getSerializedInputData() != null ? this.dataSerializer.deserialize(jobSpec.getSerializedInputData()) : null).build();
    }

    /* renamed from: mapToJobWithInputData */
    public JobSpec lambda$onSuccess$1(JobSpec jobSpec, Data data) {
        return new JobSpec(jobSpec.getId(), jobSpec.getFactoryKey(), jobSpec.getQueueKey(), jobSpec.getCreateTime(), jobSpec.getNextRunAttemptTime(), jobSpec.getRunAttempt(), jobSpec.getMaxAttempts(), jobSpec.getLifespan(), jobSpec.getSerializedData(), this.dataSerializer.serialize(data), jobSpec.isRunning(), jobSpec.isMemoryOnly());
    }
}
