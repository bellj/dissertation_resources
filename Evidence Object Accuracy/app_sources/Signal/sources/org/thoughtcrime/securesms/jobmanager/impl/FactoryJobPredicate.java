package org.thoughtcrime.securesms.jobmanager.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.thoughtcrime.securesms.jobmanager.JobPredicate;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;

/* loaded from: classes4.dex */
public final class FactoryJobPredicate implements JobPredicate {
    private final Set<String> factories;

    public FactoryJobPredicate(String... strArr) {
        this.factories = new HashSet(Arrays.asList(strArr));
    }

    @Override // org.thoughtcrime.securesms.jobmanager.JobPredicate
    public boolean shouldRun(JobSpec jobSpec) {
        return this.factories.contains(jobSpec.getFactoryKey());
    }
}
