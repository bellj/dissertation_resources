package org.thoughtcrime.securesms.jobmanager;

import org.thoughtcrime.securesms.jobmanager.JobPredicate;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;

/* loaded from: classes4.dex */
public interface JobPredicate {
    public static final JobPredicate NONE = new JobPredicate() { // from class: org.thoughtcrime.securesms.jobmanager.JobPredicate$$ExternalSyntheticLambda0
        @Override // org.thoughtcrime.securesms.jobmanager.JobPredicate
        public final boolean shouldRun(JobSpec jobSpec) {
            return JobPredicate.CC.lambda$static$0(jobSpec);
        }
    };

    /* renamed from: org.thoughtcrime.securesms.jobmanager.JobPredicate$-CC */
    /* loaded from: classes4.dex */
    public final /* synthetic */ class CC {
        static {
            JobPredicate jobPredicate = JobPredicate.NONE;
        }

        public static /* synthetic */ boolean lambda$static$0(JobSpec jobSpec) {
            return true;
        }
    }

    boolean shouldRun(JobSpec jobSpec);
}
