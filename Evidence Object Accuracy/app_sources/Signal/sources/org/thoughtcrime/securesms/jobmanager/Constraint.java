package org.thoughtcrime.securesms.jobmanager;

import android.app.job.JobInfo;

/* loaded from: classes4.dex */
public interface Constraint {

    /* renamed from: org.thoughtcrime.securesms.jobmanager.Constraint$-CC */
    /* loaded from: classes4.dex */
    public final /* synthetic */ class CC {
        public static String $default$getJobSchedulerKeyPart(Constraint constraint) {
            return null;
        }
    }

    /* loaded from: classes4.dex */
    public interface Factory<T extends Constraint> {
        T create();
    }

    void applyToJobInfo(JobInfo.Builder builder);

    String getFactoryKey();

    String getJobSchedulerKeyPart();

    boolean isMet();
}
