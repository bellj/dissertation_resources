package org.thoughtcrime.securesms.jobmanager;

import java.util.List;

/* loaded from: classes4.dex */
public interface Scheduler {
    void schedule(long j, List<Constraint> list);
}
