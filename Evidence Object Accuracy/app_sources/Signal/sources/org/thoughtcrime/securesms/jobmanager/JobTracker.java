package org.thoughtcrime.securesms.jobmanager;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.util.LRUCache;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes4.dex */
public class JobTracker {
    private final Map<String, JobInfo> jobInfos = new LRUCache(MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH);
    private final List<ListenerInfo> jobListeners = new ArrayList();
    private final Executor listenerExecutor = SignalExecutors.BOUNDED;

    /* loaded from: classes4.dex */
    public interface JobFilter {
        boolean matches(Job job);
    }

    /* loaded from: classes4.dex */
    public interface JobListener {
        void onStateChanged(Job job, JobState jobState);
    }

    public synchronized void addListener(JobFilter jobFilter, JobListener jobListener) {
        this.jobListeners.add(new ListenerInfo(jobFilter, jobListener));
    }

    public synchronized void removeListener(JobListener jobListener) {
        Iterator<ListenerInfo> it = this.jobListeners.iterator();
        while (it.hasNext()) {
            if (jobListener.equals(it.next().getListener())) {
                it.remove();
            }
        }
    }

    public synchronized JobState getFirstMatchingJobState(JobFilter jobFilter) {
        for (JobInfo jobInfo : this.jobInfos.values()) {
            if (jobFilter.matches(jobInfo.getJob())) {
                return jobInfo.getJobState();
            }
        }
        return null;
    }

    public synchronized void onStateChange(Job job, JobState jobState) {
        getOrCreateJobInfo(job).setJobState(jobState);
        Stream.of(this.jobListeners).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobmanager.JobTracker$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return JobTracker.lambda$onStateChange$0(Job.this, (JobTracker.ListenerInfo) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.jobmanager.JobTracker$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((JobTracker.ListenerInfo) obj).getListener();
            }
        }).forEach(new Consumer(job, jobState) { // from class: org.thoughtcrime.securesms.jobmanager.JobTracker$$ExternalSyntheticLambda2
            public final /* synthetic */ Job f$1;
            public final /* synthetic */ JobTracker.JobState f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                JobTracker.this.lambda$onStateChange$2(this.f$1, this.f$2, (JobTracker.JobListener) obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$onStateChange$0(Job job, ListenerInfo listenerInfo) {
        return listenerInfo.getFilter().matches(job);
    }

    public /* synthetic */ void lambda$onStateChange$2(Job job, JobState jobState, JobListener jobListener) {
        this.listenerExecutor.execute(new Runnable(job, jobState) { // from class: org.thoughtcrime.securesms.jobmanager.JobTracker$$ExternalSyntheticLambda3
            public final /* synthetic */ Job f$1;
            public final /* synthetic */ JobTracker.JobState f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JobTracker.JobListener.this.onStateChanged(this.f$1, this.f$2);
            }
        });
    }

    public synchronized boolean haveAnyFailed(Collection<String> collection) {
        for (String str : collection) {
            JobInfo jobInfo = this.jobInfos.get(str);
            if (jobInfo != null && jobInfo.getJobState() == JobState.FAILURE) {
                return true;
            }
        }
        return false;
    }

    private JobInfo getOrCreateJobInfo(Job job) {
        JobInfo jobInfo = this.jobInfos.get(job.getId());
        if (jobInfo == null) {
            jobInfo = new JobInfo(job);
        }
        this.jobInfos.put(job.getId(), jobInfo);
        return jobInfo;
    }

    /* loaded from: classes4.dex */
    public enum JobState {
        PENDING(false),
        RUNNING(false),
        SUCCESS(true),
        FAILURE(true),
        IGNORED(true);
        
        private final boolean complete;

        JobState(boolean z) {
            this.complete = z;
        }

        public boolean isComplete() {
            return this.complete;
        }
    }

    /* loaded from: classes4.dex */
    public static class ListenerInfo {
        private final JobFilter filter;
        private final JobListener listener;

        private ListenerInfo(JobFilter jobFilter, JobListener jobListener) {
            this.filter = jobFilter;
            this.listener = jobListener;
        }

        JobFilter getFilter() {
            return this.filter;
        }

        public JobListener getListener() {
            return this.listener;
        }
    }

    /* loaded from: classes4.dex */
    public static class JobInfo {
        private final Job job;
        private JobState jobState;

        private JobInfo(Job job) {
            this.job = job;
        }

        Job getJob() {
            return this.job;
        }

        void setJobState(JobState jobState) {
            this.jobState = jobState;
        }

        JobState getJobState() {
            return this.jobState;
        }
    }
}
