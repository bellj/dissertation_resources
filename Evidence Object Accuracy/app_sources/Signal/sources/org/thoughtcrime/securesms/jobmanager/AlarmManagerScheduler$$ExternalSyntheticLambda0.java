package org.thoughtcrime.securesms.jobmanager;

import com.annimon.stream.function.Predicate;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AlarmManagerScheduler$$ExternalSyntheticLambda0 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return ((Constraint) obj).isMet();
    }
}
