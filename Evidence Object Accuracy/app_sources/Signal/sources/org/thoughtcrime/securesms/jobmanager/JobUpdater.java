package org.thoughtcrime.securesms.jobmanager;

import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;

/* loaded from: classes4.dex */
public interface JobUpdater {
    JobSpec update(JobSpec jobSpec, Data.Serializer serializer);
}
