package org.thoughtcrime.securesms.jobmanager;

/* loaded from: classes4.dex */
public abstract class JobMigration {
    private final int endVersion;

    /* access modifiers changed from: protected */
    public abstract JobData migrate(JobData jobData);

    public JobMigration(int i) {
        this.endVersion = i;
    }

    public int getEndVersion() {
        return this.endVersion;
    }

    /* loaded from: classes4.dex */
    public static class JobData {
        private final Data data;
        private final String factoryKey;
        private final String queueKey;

        public JobData(String str, String str2, Data data) {
            this.factoryKey = str;
            this.queueKey = str2;
            this.data = data;
        }

        public JobData withFactoryKey(String str) {
            return new JobData(str, this.queueKey, this.data);
        }

        public JobData withQueueKey(String str) {
            return new JobData(this.factoryKey, str, this.data);
        }

        public JobData withData(Data data) {
            return new JobData(this.factoryKey, this.queueKey, data);
        }

        public String getFactoryKey() {
            return this.factoryKey;
        }

        public String getQueueKey() {
            return this.queueKey;
        }

        public Data getData() {
            return this.data;
        }
    }
}
