package org.thoughtcrime.securesms.jobmanager;

import com.annimon.stream.function.Consumer;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class JobController$$ExternalSyntheticLambda0 implements Consumer {
    @Override // com.annimon.stream.function.Consumer
    public final void accept(Object obj) {
        ((Job) obj).onFailure();
    }
}
