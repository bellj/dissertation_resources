package org.thoughtcrime.securesms.recipients.ui.sharablegrouplink;

import android.content.Context;
import java.io.IOException;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.storageservice.protos.groups.AccessControl;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupChangeFailedException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupInsufficientRightsException;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.GroupNotAMemberException;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.util.AsynchronousCallback;

/* loaded from: classes4.dex */
public final class ShareableGroupLinkRepository {
    private final Context context;
    private final GroupId.V2 groupId;

    public ShareableGroupLinkRepository(Context context, GroupId.V2 v2) {
        this.context = context;
        this.groupId = v2;
    }

    public void cycleGroupLinkPassword(AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason> workerThread) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(workerThread) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ShareableGroupLinkRepository.$r8$lambda$pb5fmMlEjelzWmalIZeREmlQy_E(ShareableGroupLinkRepository.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$cycleGroupLinkPassword$0(AsynchronousCallback.WorkerThread workerThread) {
        try {
            GroupManager.cycleGroupLinkPassword(this.context, this.groupId);
            workerThread.onComplete(null);
        } catch (IOException | GroupChangeBusyException | GroupChangeFailedException | GroupInsufficientRightsException | GroupNotAMemberException e) {
            workerThread.onError(GroupChangeFailureReason.fromException(e));
        }
    }

    public void toggleGroupLinkEnabled(AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason> workerThread) {
        setGroupLinkEnabledState(toggleGroupLinkState(true, false), workerThread);
    }

    public void toggleGroupLinkApprovalRequired(AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason> workerThread) {
        setGroupLinkEnabledState(toggleGroupLinkState(false, true), workerThread);
    }

    private void setGroupLinkEnabledState(GroupManager.GroupLinkState groupLinkState, AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason> workerThread) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(groupLinkState, workerThread) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ GroupManager.GroupLinkState f$1;
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ShareableGroupLinkRepository.$r8$lambda$jqW_LjTwAc7TZvXA7Q2QpAqCYYw(ShareableGroupLinkRepository.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$setGroupLinkEnabledState$1(GroupManager.GroupLinkState groupLinkState, AsynchronousCallback.WorkerThread workerThread) {
        try {
            GroupManager.setGroupLinkEnabledState(this.context, this.groupId, groupLinkState);
            workerThread.onComplete(null);
        } catch (IOException | GroupChangeBusyException | GroupChangeFailedException | GroupInsufficientRightsException | GroupNotAMemberException e) {
            workerThread.onError(GroupChangeFailureReason.fromException(e));
        }
    }

    private GroupManager.GroupLinkState toggleGroupLinkState(boolean z, boolean z2) {
        boolean z3 = false;
        boolean z4 = true;
        switch (AnonymousClass1.$SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[SignalDatabase.groups().getGroup(this.groupId).get().requireV2GroupProperties().getDecryptedGroup().getAccessControl().getAddFromInviteLink().ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                z4 = false;
                break;
            case 5:
                break;
            case 6:
                z3 = true;
                break;
            default:
                throw new AssertionError();
        }
        if (z2) {
            z3 = !z3;
        }
        if (z) {
            z4 = !z4;
        }
        if (z3 && z4) {
            return GroupManager.GroupLinkState.ENABLED_WITH_APPROVAL;
        }
        if (z4) {
            return GroupManager.GroupLinkState.ENABLED;
        }
        return GroupManager.GroupLinkState.DISABLED;
    }

    /* renamed from: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkRepository$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired;

        static {
            int[] iArr = new int[AccessControl.AccessRequired.values().length];
            $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired = iArr;
            try {
                iArr[AccessControl.AccessRequired.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[AccessControl.AccessRequired.UNSATISFIABLE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[AccessControl.AccessRequired.UNRECOGNIZED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[AccessControl.AccessRequired.MEMBER.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[AccessControl.AccessRequired.ANY.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[AccessControl.AccessRequired.ADMINISTRATOR.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }
}
