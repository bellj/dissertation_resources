package org.thoughtcrime.securesms.recipients;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Predicate;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda10;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupChangeFailedException;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.jobs.MultiDeviceBlockedUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceMessageRequestResponseJob;
import org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.jobs.ReactionSendJob$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob;
import org.thoughtcrime.securesms.jobs.RotateProfileKeyJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.OutgoingExpirationUpdateMessage;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.NotFoundException;

/* loaded from: classes4.dex */
public class RecipientUtil {
    private static final String TAG = Log.tag(RecipientUtil.class);

    public static ServiceId getOrFetchServiceId(Context context, Recipient recipient) throws IOException {
        return toSignalServiceAddress(context, recipient).getServiceId();
    }

    public static SignalServiceAddress toSignalServiceAddress(Context context, Recipient recipient) throws IOException {
        Recipient resolve = recipient.resolve();
        if (resolve.getServiceId().isPresent() || resolve.getE164().isPresent()) {
            if (!resolve.getServiceId().isPresent()) {
                String str = TAG;
                Log.i(str, resolve.getId() + " is missing a UUID...");
                RecipientDatabase.RegisteredState refresh = ContactDiscovery.refresh(context, resolve, false);
                resolve = Recipient.resolved(resolve.getId());
                Log.i(str, "Successfully performed a UUID fetch for " + resolve.getId() + ". Registered: " + refresh);
            }
            if (resolve.hasServiceId()) {
                return new SignalServiceAddress(resolve.requireServiceId(), Optional.ofNullable(resolve.resolve().getE164().orElse(null)));
            }
            throw new NotFoundException(resolve.getId() + " is not registered!");
        }
        throw new AssertionError(resolve.getId() + " - No UUID or phone number!");
    }

    public static List<SignalServiceAddress> toSignalServiceAddresses(Context context, List<RecipientId> list) throws IOException {
        return toSignalServiceAddressesFromResolved(context, Recipient.resolvedList(list));
    }

    public static List<SignalServiceAddress> toSignalServiceAddressesFromResolved(Context context, List<Recipient> list) throws IOException {
        ensureUuidsAreAvailable(context, list);
        return Stream.of(list).map(new PushGroupSendJob$$ExternalSyntheticLambda1()).map(new Function() { // from class: org.thoughtcrime.securesms.recipients.RecipientUtil$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return RecipientUtil.lambda$toSignalServiceAddressesFromResolved$0((Recipient) obj);
            }
        }).toList();
    }

    public static /* synthetic */ SignalServiceAddress lambda$toSignalServiceAddressesFromResolved$0(Recipient recipient) {
        return new SignalServiceAddress(recipient.requireServiceId(), recipient.getE164().orElse(null));
    }

    public static boolean ensureUuidsAreAvailable(Context context, Collection<Recipient> collection) throws IOException {
        List list = Stream.of(collection).map(new PushGroupSendJob$$ExternalSyntheticLambda1()).filterNot(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda10()).toList();
        if (list.size() <= 0) {
            return false;
        }
        ContactDiscovery.refresh(context, (List<? extends Recipient>) list, false);
        if (!Collection$EL.stream(collection).map(new RecipientUtil$$ExternalSyntheticLambda2()).anyMatch(new ReactionSendJob$$ExternalSyntheticLambda0())) {
            return true;
        }
        throw new NotFoundException("1 or more recipients are not registered!");
    }

    public static boolean isBlockable(Recipient recipient) {
        return !recipient.resolve().isMmsGroup();
    }

    public static List<Recipient> getEligibleForSending(List<Recipient> list) {
        return Stream.of(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.recipients.RecipientUtil$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return RecipientUtil.lambda$getEligibleForSending$1((Recipient) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.recipients.RecipientUtil$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return RecipientUtil.lambda$getEligibleForSending$2((Recipient) obj);
            }
        }).toList();
    }

    public static /* synthetic */ boolean lambda$getEligibleForSending$1(Recipient recipient) {
        return recipient.getRegistered() != RecipientDatabase.RegisteredState.NOT_REGISTERED;
    }

    public static /* synthetic */ boolean lambda$getEligibleForSending$2(Recipient recipient) {
        return !recipient.isBlocked();
    }

    public static void blockNonGroup(Context context, Recipient recipient) {
        if (!recipient.isGroup()) {
            try {
                block(context, recipient);
            } catch (IOException | GroupChangeException e) {
                throw new AssertionError(e);
            }
        } else {
            throw new AssertionError();
        }
    }

    public static void block(Context context, Recipient recipient) throws GroupChangeBusyException, IOException, GroupChangeFailedException {
        if (isBlockable(recipient)) {
            String str = TAG;
            Log.w(str, "Blocking " + recipient.getId() + " (group: " + recipient.isGroup() + ")");
            Recipient resolve = recipient.resolve();
            if (resolve.isGroup() && resolve.getGroupId().get().isPush()) {
                GroupManager.leaveGroupFromBlockOrMessageRequest(context, resolve.getGroupId().get().requirePush());
            }
            SignalDatabase.recipients().setBlocked(resolve.getId(), true);
            if (resolve.isSystemContact() || resolve.isProfileSharing() || isProfileSharedViaGroup(context, resolve)) {
                SignalDatabase.recipients().setProfileSharing(resolve.getId(), false);
                ApplicationDependencies.getJobManager().startChain(new RefreshOwnProfileJob()).then(new RotateProfileKeyJob()).enqueue();
            }
            ApplicationDependencies.getJobManager().add(new MultiDeviceBlockedUpdateJob());
            StorageSyncHelper.scheduleSyncForDataChange();
            return;
        }
        throw new AssertionError("Recipient is not blockable!");
    }

    public static void unblock(Context context, Recipient recipient) {
        if (isBlockable(recipient)) {
            String str = TAG;
            Log.i(str, "Unblocking " + recipient.getId() + " (group: " + recipient.isGroup() + ")");
            SignalDatabase.recipients().setBlocked(recipient.getId(), false);
            SignalDatabase.recipients().setProfileSharing(recipient.getId(), true);
            ApplicationDependencies.getJobManager().add(new MultiDeviceBlockedUpdateJob());
            StorageSyncHelper.scheduleSyncForDataChange();
            if (recipient.hasServiceId()) {
                ApplicationDependencies.getJobManager().add(MultiDeviceMessageRequestResponseJob.forAccept(recipient.getId()));
                return;
            }
            return;
        }
        throw new AssertionError("Recipient is not blockable!");
    }

    public static boolean isMessageRequestAccepted(Context context, long j) {
        Recipient recipientForThreadId;
        if (j >= 0 && (recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(j)) != null) {
            return isMessageRequestAccepted(context, Long.valueOf(j), recipientForThreadId);
        }
        return true;
    }

    public static boolean isMessageRequestAccepted(Context context, Recipient recipient) {
        if (recipient == null) {
            return true;
        }
        return isMessageRequestAccepted(context, SignalDatabase.threads().getThreadIdFor(recipient.getId()), recipient);
    }

    public static boolean isCallRequestAccepted(Context context, Recipient recipient) {
        if (recipient == null) {
            return true;
        }
        return isCallRequestAccepted(context, SignalDatabase.threads().getThreadIdFor(recipient.getId()), recipient);
    }

    public static boolean isPreMessageRequestThread(Context context, Long l) {
        return l != null && SignalDatabase.mmsSms().getConversationCount(l.longValue(), SignalStore.misc().getMessageRequestEnableTime()) > 0;
    }

    public static void shareProfileIfFirstSecureMessage(Context context, Recipient recipient) {
        if (!recipient.isProfileSharing()) {
            long threadIdIfExistsFor = SignalDatabase.threads().getThreadIdIfExistsFor(recipient.getId());
            if (!isPreMessageRequestThread(context, Long.valueOf(threadIdIfExistsFor))) {
                if (SignalDatabase.mmsSms().getOutgoingSecureConversationCount(threadIdIfExistsFor) == 0) {
                    SignalDatabase.recipients().setProfileSharing(recipient.getId(), true);
                }
            }
        }
    }

    public static boolean isLegacyProfileSharingAccepted(Recipient recipient) {
        return recipient.isSelf() || recipient.isProfileSharing() || recipient.isSystemContact() || !recipient.isRegistered() || recipient.isForceSmsSelection();
    }

    public static boolean shouldHaveProfileKey(Context context, Recipient recipient) {
        if (recipient.isBlocked()) {
            return false;
        }
        if (recipient.isProfileSharing()) {
            return true;
        }
        return Collection$EL.stream(SignalDatabase.groups().getPushGroupsContainingMember(recipient.getId())).anyMatch(new j$.util.function.Predicate() { // from class: org.thoughtcrime.securesms.recipients.RecipientUtil$$ExternalSyntheticLambda0
            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return ((GroupDatabase.GroupRecord) obj).isV2Group();
            }
        });
    }

    public static boolean setAndSendUniversalExpireTimerIfNecessary(Context context, Recipient recipient, long j) {
        int universalExpireTimer = SignalStore.settings().getUniversalExpireTimer();
        if (universalExpireTimer == 0 || recipient.isGroup() || recipient.getExpiresInSeconds() != 0 || !recipient.isRegistered()) {
            return false;
        }
        if (j != -1 && SignalDatabase.mmsSms().hasMeaningfulMessage(j)) {
            return false;
        }
        SignalDatabase.recipients().setExpireMessages(recipient.getId(), universalExpireTimer);
        MessageSender.send(context, (OutgoingMediaMessage) new OutgoingExpirationUpdateMessage(recipient, System.currentTimeMillis(), 1000 * ((long) universalExpireTimer)), SignalDatabase.threads().getOrCreateThreadIdFor(recipient), false, (String) null, (MessageDatabase.InsertListener) null);
        return true;
    }

    public static boolean isMessageRequestAccepted(Context context, Long l, Recipient recipient) {
        return recipient == null || recipient.isSelf() || recipient.isProfileSharing() || recipient.isSystemContact() || recipient.isForceSmsSelection() || !recipient.isRegistered() || hasSentMessageInThread(context, l) || noSecureMessagesAndNoCallsInThread(context, l) || isPreMessageRequestThread(context, l);
    }

    private static boolean isCallRequestAccepted(Context context, Long l, Recipient recipient) {
        return recipient.isProfileSharing() || recipient.isSystemContact() || hasSentMessageInThread(context, l) || isPreMessageRequestThread(context, l);
    }

    public static boolean hasSentMessageInThread(Context context, Long l) {
        return (l == null || SignalDatabase.mmsSms().getOutgoingSecureConversationCount(l.longValue()) == 0) ? false : true;
    }

    private static boolean noSecureMessagesAndNoCallsInThread(Context context, Long l) {
        if (l == null) {
            return true;
        }
        if (SignalDatabase.mmsSms().getSecureConversationCount(l.longValue()) != 0 || SignalDatabase.threads().hasReceivedAnyCallsSince(l.longValue(), 0)) {
            return false;
        }
        return true;
    }

    private static boolean isProfileSharedViaGroup(Context context, Recipient recipient) {
        return Stream.of(SignalDatabase.groups().getPushGroupsContainingMember(recipient.getId())).anyMatch(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.recipients.RecipientUtil$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return RecipientUtil.lambda$isProfileSharedViaGroup$3((GroupDatabase.GroupRecord) obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$isProfileSharedViaGroup$3(GroupDatabase.GroupRecord groupRecord) {
        return Recipient.resolved(groupRecord.getRecipientId()).isProfileSharing();
    }
}
