package org.thoughtcrime.securesms.recipients;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import com.annimon.stream.Stream;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Collection$EL;
import j$.util.Map;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.StringUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.contacts.avatars.ContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.GroupRecordContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ProfileContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ResourceContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.SystemContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.TransparentContactPhoto;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.database.GroupDatabase$GroupRecord$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.ProfileAvatarFileDetails;
import org.thoughtcrime.securesms.database.model.databaseprotos.RecipientExtras;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.phonenumbers.NumberUtil;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.util.OptionalUtil;
import org.whispersystems.signalservice.api.util.Preconditions;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes.dex */
public class Recipient {
    public static final FallbackPhotoProvider DEFAULT_FALLBACK_PHOTO_PROVIDER = new FallbackPhotoProvider();
    private static final int MAX_MEMBER_NAMES;
    private static final String TAG = Log.tag(Recipient.class);
    public static final Recipient UNKNOWN = new Recipient(RecipientId.UNKNOWN, RecipientDetails.forUnknown(), true);
    private final String about;
    private final String aboutEmoji;
    private final Capability announcementGroupCapability;
    private final AvatarColor avatarColor;
    private final List<Badge> badges;
    private final boolean blocked;
    private final Uri callRingtone;
    private final RecipientDatabase.VibrateState callVibrate;
    private final Capability changeNumberCapability;
    private final ChatColors chatColors;
    private final Uri contactUri;
    private final String customLabel;
    private final Optional<Integer> defaultSubscriptionId;
    private final DistributionListId distributionListId;
    private final String e164;
    private final String email;
    private final int expireMessages;
    private final ExpiringProfileKeyCredential expiringProfileKeyCredential;
    private final Optional<Extras> extras;
    private final boolean forceSmsSelection;
    private final Capability giftBadgesCapability;
    private final Optional<Long> groupAvatarId;
    private final GroupId groupId;
    private final String groupName;
    private final Capability groupsV1MigrationCapability;
    private final boolean hasGroupsInCommon;
    private final RecipientId id;
    private final RecipientDatabase.InsightsBannerTier insightsBannerTier;
    private final boolean isReleaseNotesRecipient;
    private final boolean isSelf;
    private final long lastProfileFetch;
    private final RecipientDatabase.MentionSetting mentionSetting;
    private final Uri messageRingtone;
    private final RecipientDatabase.VibrateState messageVibrate;
    private final long muteUntil;
    private final String notificationChannel;
    private final List<RecipientId> participantIds;
    private final PNI pni;
    private final String profileAvatar;
    private final ProfileAvatarFileDetails profileAvatarFileDetails;
    private final byte[] profileKey;
    private final boolean profileSharing;
    private final RecipientDatabase.RegisteredState registered;
    private final boolean resolving;
    private final Capability senderKeyCapability;
    private final ServiceId serviceId;
    private final ProfileName signalProfileName;
    private final byte[] storageId;
    private final Capability storiesCapability;
    private final String systemContactName;
    private final Uri systemContactPhoto;
    private final ProfileName systemProfileName;
    private final RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode;
    private final String username;
    private final ChatWallpaper wallpaper;

    public static LiveRecipient live(RecipientId recipientId) {
        Preconditions.checkNotNull(recipientId, "ID cannot be null.");
        return ApplicationDependencies.getRecipientCache().getLive(recipientId);
    }

    public static Observable<Recipient> observable(RecipientId recipientId) {
        Preconditions.checkNotNull(recipientId, "ID cannot be null");
        return Observable.create(new ObservableOnSubscribe() { // from class: org.thoughtcrime.securesms.recipients.Recipient$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                Recipient.lambda$observable$1(RecipientId.this, observableEmitter);
            }
        }).subscribeOn(Schedulers.io());
    }

    public static /* synthetic */ void lambda$observable$1(RecipientId recipientId, ObservableEmitter observableEmitter) throws Throwable {
        LiveRecipient live = live(recipientId);
        observableEmitter.onNext(live.resolve());
        LiveRecipient$$ExternalSyntheticLambda1 liveRecipient$$ExternalSyntheticLambda1 = new LiveRecipient$$ExternalSyntheticLambda1(observableEmitter);
        live.observeForever(liveRecipient$$ExternalSyntheticLambda1);
        observableEmitter.setCancellable(new Cancellable(liveRecipient$$ExternalSyntheticLambda1) { // from class: org.thoughtcrime.securesms.recipients.Recipient$$ExternalSyntheticLambda2
            public final /* synthetic */ RecipientForeverObserver f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                LiveRecipient.this.lambda$asObservable$6(this.f$1);
            }
        });
    }

    public static Recipient resolved(RecipientId recipientId) {
        Preconditions.checkNotNull(recipientId, "ID cannot be null.");
        return live(recipientId).resolve();
    }

    public static List<Recipient> resolvedList(Collection<RecipientId> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (RecipientId recipientId : collection) {
            arrayList.add(resolved(recipientId));
        }
        return arrayList;
    }

    public static Recipient distributionList(DistributionListId distributionListId) {
        return resolved(SignalDatabase.recipients().getOrInsertFromDistributionListId(distributionListId));
    }

    public static Recipient externalUsername(ServiceId serviceId, String str) {
        Recipient externalPush = externalPush(serviceId);
        SignalDatabase.recipients().setUsername(externalPush.getId(), str);
        return externalPush;
    }

    public static Recipient externalPush(SignalServiceAddress signalServiceAddress) {
        return externalPush(signalServiceAddress.getServiceId(), signalServiceAddress.getNumber().orElse(null));
    }

    public static Recipient externalGV1Member(SignalServiceAddress signalServiceAddress) {
        if (signalServiceAddress.getNumber().isPresent()) {
            return externalPush(null, signalServiceAddress.getNumber().get());
        }
        return externalPush(signalServiceAddress.getServiceId());
    }

    public static Recipient externalPush(ServiceId serviceId) {
        return externalPush(serviceId, null);
    }

    public static Recipient externalPush(ServiceId serviceId, String str) {
        if (!ServiceId.UNKNOWN.equals(serviceId)) {
            RecipientDatabase recipients = SignalDatabase.recipients();
            RecipientId andPossiblyMerge = recipients.getAndPossiblyMerge(serviceId, str);
            Recipient resolved = resolved(andPossiblyMerge);
            if (!resolved.getId().equals(andPossiblyMerge)) {
                String str2 = TAG;
                Log.w(str2, "Resolved " + andPossiblyMerge + ", but got back a recipient with " + resolved.getId());
            }
            if (!resolved.isRegistered() && serviceId != null) {
                Log.w(TAG, "External push was locally marked unregistered. Marking as registered.");
                recipients.markRegistered(andPossiblyMerge, serviceId);
            } else if (!resolved.isRegistered()) {
                Log.w(TAG, "External push was locally marked unregistered, but we don't have an ACI, so we can't do anything.", new Throwable());
            }
            return resolved;
        }
        throw new AssertionError();
    }

    public static Recipient externalContact(String str) {
        RecipientId recipientId;
        RecipientDatabase recipients = SignalDatabase.recipients();
        if (!UuidUtil.isUuid(str)) {
            if (NumberUtil.isValidEmail(str)) {
                recipientId = recipients.getOrInsertFromEmail(str);
            } else {
                recipientId = recipients.getOrInsertFromE164(str);
            }
            return resolved(recipientId);
        }
        throw new AssertionError("UUIDs are not valid system contact identifiers!");
    }

    public static Recipient externalGroupExact(GroupId groupId) {
        return resolved(SignalDatabase.recipients().getOrInsertFromGroupId(groupId));
    }

    public static Recipient externalPossiblyMigratedGroup(GroupId groupId) {
        return resolved(SignalDatabase.recipients().getOrInsertFromPossiblyMigratedGroupId(groupId));
    }

    public static Recipient external(Context context, String str) {
        RecipientId recipientId;
        Preconditions.checkNotNull(str, "Identifier cannot be null!");
        RecipientDatabase recipients = SignalDatabase.recipients();
        if (UuidUtil.isUuid(str)) {
            recipientId = recipients.getOrInsertFromServiceId(ServiceId.parseOrThrow(str));
        } else if (GroupId.isEncodedGroup(str)) {
            recipientId = recipients.getOrInsertFromGroupId(GroupId.parseOrThrow(str));
        } else if (NumberUtil.isValidEmail(str)) {
            recipientId = recipients.getOrInsertFromEmail(str);
        } else {
            recipientId = recipients.getOrInsertFromE164(PhoneNumberFormatter.get(context).format(str));
        }
        return resolved(recipientId);
    }

    public static Recipient self() {
        return ApplicationDependencies.getRecipientCache().getSelf();
    }

    public Recipient(RecipientId recipientId) {
        this.id = recipientId;
        this.resolving = true;
        this.serviceId = null;
        this.pni = null;
        this.username = null;
        this.e164 = null;
        this.email = null;
        this.groupId = null;
        this.distributionListId = null;
        this.participantIds = Collections.emptyList();
        this.groupAvatarId = Optional.empty();
        this.isSelf = false;
        this.blocked = false;
        this.muteUntil = 0;
        RecipientDatabase.VibrateState vibrateState = RecipientDatabase.VibrateState.DEFAULT;
        this.messageVibrate = vibrateState;
        this.callVibrate = vibrateState;
        this.messageRingtone = null;
        this.callRingtone = null;
        this.insightsBannerTier = RecipientDatabase.InsightsBannerTier.TIER_TWO;
        this.defaultSubscriptionId = Optional.empty();
        this.expireMessages = 0;
        this.registered = RecipientDatabase.RegisteredState.UNKNOWN;
        this.profileKey = null;
        this.expiringProfileKeyCredential = null;
        this.groupName = null;
        this.systemContactPhoto = null;
        this.customLabel = null;
        this.contactUri = null;
        ProfileName profileName = ProfileName.EMPTY;
        this.signalProfileName = profileName;
        this.profileAvatar = null;
        this.profileAvatarFileDetails = ProfileAvatarFileDetails.NO_DETAILS;
        this.profileSharing = false;
        this.lastProfileFetch = 0;
        this.notificationChannel = null;
        this.unidentifiedAccessMode = RecipientDatabase.UnidentifiedAccessMode.DISABLED;
        this.forceSmsSelection = false;
        Capability capability = Capability.UNKNOWN;
        this.groupsV1MigrationCapability = capability;
        this.senderKeyCapability = capability;
        this.announcementGroupCapability = capability;
        this.changeNumberCapability = capability;
        this.storiesCapability = capability;
        this.giftBadgesCapability = capability;
        this.storageId = null;
        this.mentionSetting = RecipientDatabase.MentionSetting.ALWAYS_NOTIFY;
        this.wallpaper = null;
        this.chatColors = null;
        this.avatarColor = AvatarColor.UNKNOWN;
        this.about = null;
        this.aboutEmoji = null;
        this.systemProfileName = profileName;
        this.systemContactName = null;
        this.extras = Optional.empty();
        this.hasGroupsInCommon = false;
        this.badges = Collections.emptyList();
        this.isReleaseNotesRecipient = false;
    }

    public Recipient(RecipientId recipientId, RecipientDetails recipientDetails, boolean z) {
        this.id = recipientId;
        this.resolving = !z;
        this.serviceId = recipientDetails.serviceId;
        this.pni = recipientDetails.pni;
        this.username = recipientDetails.username;
        this.e164 = recipientDetails.e164;
        this.email = recipientDetails.email;
        this.groupId = recipientDetails.groupId;
        this.distributionListId = recipientDetails.distributionListId;
        this.participantIds = recipientDetails.participantIds;
        this.groupAvatarId = recipientDetails.groupAvatarId;
        this.isSelf = recipientDetails.isSelf;
        this.blocked = recipientDetails.blocked;
        this.muteUntil = recipientDetails.mutedUntil;
        this.messageVibrate = recipientDetails.messageVibrateState;
        this.callVibrate = recipientDetails.callVibrateState;
        this.messageRingtone = recipientDetails.messageRingtone;
        this.callRingtone = recipientDetails.callRingtone;
        this.insightsBannerTier = recipientDetails.insightsBannerTier;
        this.defaultSubscriptionId = recipientDetails.defaultSubscriptionId;
        this.expireMessages = recipientDetails.expireMessages;
        this.registered = recipientDetails.registered;
        this.profileKey = recipientDetails.profileKey;
        this.expiringProfileKeyCredential = recipientDetails.expiringProfileKeyCredential;
        this.groupName = recipientDetails.groupName;
        this.systemContactPhoto = recipientDetails.systemContactPhoto;
        this.customLabel = recipientDetails.customLabel;
        this.contactUri = recipientDetails.contactUri;
        this.signalProfileName = recipientDetails.profileName;
        this.profileAvatar = recipientDetails.profileAvatar;
        this.profileAvatarFileDetails = recipientDetails.profileAvatarFileDetails;
        this.profileSharing = recipientDetails.profileSharing;
        this.lastProfileFetch = recipientDetails.lastProfileFetch;
        this.notificationChannel = recipientDetails.notificationChannel;
        this.unidentifiedAccessMode = recipientDetails.unidentifiedAccessMode;
        this.forceSmsSelection = recipientDetails.forceSmsSelection;
        this.groupsV1MigrationCapability = recipientDetails.groupsV1MigrationCapability;
        this.senderKeyCapability = recipientDetails.senderKeyCapability;
        this.announcementGroupCapability = recipientDetails.announcementGroupCapability;
        this.changeNumberCapability = recipientDetails.changeNumberCapability;
        this.storiesCapability = recipientDetails.storiesCapability;
        this.giftBadgesCapability = recipientDetails.giftBadgesCapability;
        this.storageId = recipientDetails.storageId;
        this.mentionSetting = recipientDetails.mentionSetting;
        this.wallpaper = recipientDetails.wallpaper;
        this.chatColors = recipientDetails.chatColors;
        this.avatarColor = recipientDetails.avatarColor;
        this.about = recipientDetails.about;
        this.aboutEmoji = recipientDetails.aboutEmoji;
        this.systemProfileName = recipientDetails.systemProfileName;
        this.systemContactName = recipientDetails.systemContactName;
        this.extras = recipientDetails.extras;
        this.hasGroupsInCommon = recipientDetails.hasGroupsInCommon;
        this.badges = recipientDetails.badges;
        this.isReleaseNotesRecipient = recipientDetails.isReleaseChannel;
    }

    public RecipientId getId() {
        return this.id;
    }

    public boolean isSelf() {
        return this.isSelf;
    }

    public Uri getContactUri() {
        return this.contactUri;
    }

    public String getGroupName(Context context) {
        if (this.groupId != null && Util.isEmpty(this.groupName)) {
            RecipientId id = self().getId();
            List<Recipient> list = (List) Collection$EL.stream(this.participantIds).filter(new Predicate() { // from class: org.thoughtcrime.securesms.recipients.Recipient$$ExternalSyntheticLambda4
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return Recipient.lambda$getGroupName$2(RecipientId.this, (RecipientId) obj);
                }
            }).limit(10).map(new GroupDatabase$GroupRecord$$ExternalSyntheticLambda0()).collect(Collectors.toList());
            HashMap hashMap = new HashMap();
            for (Recipient recipient : list) {
                String shortDisplayName = recipient.getShortDisplayName(context);
                Integer num = (Integer) Map.EL.getOrDefault(hashMap, shortDisplayName, 0);
                Objects.requireNonNull(num);
                hashMap.put(shortDisplayName, Integer.valueOf(num.intValue() + 1));
            }
            LinkedList linkedList = new LinkedList();
            for (Recipient recipient2 : list) {
                String shortDisplayName2 = recipient2.getShortDisplayName(context);
                Integer num2 = (Integer) Map.EL.getOrDefault(hashMap, shortDisplayName2, 0);
                Objects.requireNonNull(num2);
                if (num2.intValue() <= 1) {
                    linkedList.add(shortDisplayName2);
                } else {
                    linkedList.add(recipient2.getDisplayName(context));
                }
            }
            if (Collection$EL.stream(this.participantIds).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.recipients.Recipient$$ExternalSyntheticLambda5
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return ((RecipientId) obj).equals(RecipientId.this);
                }
            })) {
                linkedList.add(context.getString(R.string.Recipient_you));
            }
            return Util.join((Collection) linkedList, ", ");
        } else if (this.resolving || !isMyStory()) {
            return this.groupName;
        } else {
            return context.getString(R.string.Recipient_my_story);
        }
    }

    public static /* synthetic */ boolean lambda$getGroupName$2(RecipientId recipientId, RecipientId recipientId2) {
        return !recipientId2.equals(recipientId);
    }

    public boolean hasName() {
        return this.groupName != null;
    }

    public boolean hasAUserSetDisplayName(Context context) {
        return !TextUtils.isEmpty(getGroupName(context)) || !TextUtils.isEmpty(this.systemContactName) || !TextUtils.isEmpty(getProfileName().toString());
    }

    public String getDisplayName(Context context) {
        String groupName = getGroupName(context);
        if (Util.isEmpty(groupName)) {
            groupName = this.systemContactName;
        }
        if (Util.isEmpty(groupName)) {
            groupName = getProfileName().toString();
        }
        if (Util.isEmpty(groupName) && !Util.isEmpty(this.e164)) {
            groupName = PhoneNumberFormatter.prettyPrint(this.e164);
        }
        if (Util.isEmpty(groupName)) {
            groupName = this.email;
        }
        if (Util.isEmpty(groupName)) {
            groupName = context.getString(R.string.Recipient_unknown);
        }
        return StringUtil.isolateBidi(groupName);
    }

    public String getDisplayNameOrUsername(Context context) {
        String groupName = getGroupName(context);
        if (Util.isEmpty(groupName)) {
            groupName = this.systemContactName;
        }
        if (Util.isEmpty(groupName)) {
            groupName = StringUtil.isolateBidi(getProfileName().toString());
        }
        if (Util.isEmpty(groupName) && !Util.isEmpty(this.e164)) {
            groupName = PhoneNumberFormatter.prettyPrint(this.e164);
        }
        if (Util.isEmpty(groupName)) {
            groupName = StringUtil.isolateBidi(this.email);
        }
        if (Util.isEmpty(groupName)) {
            groupName = StringUtil.isolateBidi(this.username);
        }
        return Util.isEmpty(groupName) ? StringUtil.isolateBidi(context.getString(R.string.Recipient_unknown)) : groupName;
    }

    public String getMentionDisplayName(Context context) {
        String isolateBidi = StringUtil.isolateBidi(this.isSelf ? getProfileName().toString() : getGroupName(context));
        if (Util.isEmpty(isolateBidi)) {
            isolateBidi = StringUtil.isolateBidi(this.isSelf ? getGroupName(context) : this.systemContactName);
        }
        if (Util.isEmpty(isolateBidi)) {
            isolateBidi = StringUtil.isolateBidi(this.isSelf ? getGroupName(context) : getProfileName().toString());
        }
        if (Util.isEmpty(isolateBidi) && !Util.isEmpty(this.e164)) {
            isolateBidi = PhoneNumberFormatter.prettyPrint(this.e164);
        }
        if (Util.isEmpty(isolateBidi)) {
            isolateBidi = StringUtil.isolateBidi(this.email);
        }
        return Util.isEmpty(isolateBidi) ? StringUtil.isolateBidi(context.getString(R.string.Recipient_unknown)) : isolateBidi;
    }

    public String getShortDisplayName(Context context) {
        return StringUtil.isolateBidi(Util.getFirstNonEmpty(getGroupName(context), getSystemProfileName().getGivenName(), getProfileName().getGivenName(), getDisplayName(context)));
    }

    public String getShortDisplayNameIncludingUsername(Context context) {
        return StringUtil.isolateBidi(Util.getFirstNonEmpty(getGroupName(context), getSystemProfileName().getGivenName(), getProfileName().getGivenName(), getDisplayName(context), getUsername().orElse(null)));
    }

    public Optional<ServiceId> getServiceId() {
        return Optional.ofNullable(this.serviceId);
    }

    public Optional<PNI> getPni() {
        return Optional.ofNullable(this.pni);
    }

    public Optional<String> getUsername() {
        if (FeatureFlags.usernames()) {
            return Optional.ofNullable(this.username);
        }
        return Optional.empty();
    }

    public Optional<String> getE164() {
        return Optional.ofNullable(this.e164);
    }

    public Optional<String> getEmail() {
        return Optional.ofNullable(this.email);
    }

    public Optional<GroupId> getGroupId() {
        return Optional.ofNullable(this.groupId);
    }

    public Optional<DistributionListId> getDistributionListId() {
        return Optional.ofNullable(this.distributionListId);
    }

    public Optional<String> getSmsAddress() {
        return OptionalUtil.or(Optional.ofNullable(this.e164), Optional.ofNullable(this.email));
    }

    public PNI requirePni() {
        PNI pni = this.resolving ? resolve().pni : this.pni;
        if (pni != null) {
            return pni;
        }
        throw new MissingAddressError(this.id);
    }

    public String requireE164() {
        String str = this.resolving ? resolve().e164 : this.e164;
        if (str != null) {
            return str;
        }
        throw new MissingAddressError(this.id);
    }

    public String requireEmail() {
        String str = this.resolving ? resolve().email : this.email;
        if (str != null) {
            return str;
        }
        throw new MissingAddressError(this.id);
    }

    public String requireSmsAddress() {
        Recipient resolve = this.resolving ? resolve() : this;
        if (resolve.getE164().isPresent()) {
            return resolve.getE164().get();
        }
        if (resolve.getEmail().isPresent()) {
            return resolve.getEmail().get();
        }
        throw new MissingAddressError(this.id);
    }

    public boolean hasSmsAddress() {
        return OptionalUtil.or(getE164(), getEmail()).isPresent();
    }

    public boolean hasE164() {
        return getE164().isPresent();
    }

    public boolean hasServiceId() {
        return getServiceId().isPresent();
    }

    public boolean isServiceIdOnly() {
        return hasServiceId() && !hasSmsAddress();
    }

    public boolean shouldHideStory() {
        return ((Boolean) this.extras.map(new Function() { // from class: org.thoughtcrime.securesms.recipients.Recipient$$ExternalSyntheticLambda6
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Boolean.valueOf(((Recipient.Extras) obj).hideStory());
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(Boolean.FALSE)).booleanValue();
    }

    public boolean hasViewedStory() {
        return ((Boolean) this.extras.map(new Function() { // from class: org.thoughtcrime.securesms.recipients.Recipient$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Boolean.valueOf(((Recipient.Extras) obj).hasViewedStory());
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(Boolean.FALSE)).booleanValue();
    }

    public GroupId requireGroupId() {
        GroupId groupId = this.resolving ? resolve().groupId : this.groupId;
        if (groupId != null) {
            return groupId;
        }
        throw new MissingAddressError(this.id);
    }

    public DistributionListId requireDistributionListId() {
        DistributionListId distributionListId = this.resolving ? resolve().distributionListId : this.distributionListId;
        if (distributionListId != null) {
            return distributionListId;
        }
        throw new MissingAddressError(this.id);
    }

    public ServiceId requireServiceId() {
        ServiceId serviceId = this.resolving ? resolve().serviceId : this.serviceId;
        if (serviceId != null) {
            return serviceId;
        }
        throw new MissingAddressError(this.id);
    }

    public String requireStringId() {
        Recipient resolve = this.resolving ? resolve() : this;
        if (resolve.isGroup()) {
            return resolve.requireGroupId().toString();
        }
        if (resolve.getServiceId().isPresent()) {
            return resolve.requireServiceId().toString();
        }
        return requireSmsAddress();
    }

    public Optional<Integer> getDefaultSubscriptionId() {
        return this.defaultSubscriptionId;
    }

    public ProfileName getProfileName() {
        return this.signalProfileName;
    }

    private ProfileName getSystemProfileName() {
        return this.systemProfileName;
    }

    public String getProfileAvatar() {
        return this.profileAvatar;
    }

    public ProfileAvatarFileDetails getProfileAvatarFileDetails() {
        return this.profileAvatarFileDetails;
    }

    public boolean isProfileSharing() {
        return this.profileSharing;
    }

    public long getLastProfileFetchTime() {
        return this.lastProfileFetch;
    }

    public boolean isGroup() {
        return resolve().groupId != null;
    }

    private boolean isGroupInternal() {
        return this.groupId != null;
    }

    public boolean isMmsGroup() {
        GroupId groupId = resolve().groupId;
        return groupId != null && groupId.isMms();
    }

    public boolean isPushGroup() {
        GroupId groupId = resolve().groupId;
        return groupId != null && groupId.isPush();
    }

    public boolean isPushV1Group() {
        GroupId groupId = resolve().groupId;
        return groupId != null && groupId.isV1();
    }

    public boolean isPushV2Group() {
        GroupId groupId = resolve().groupId;
        return groupId != null && groupId.isV2();
    }

    public boolean isDistributionList() {
        return resolve().distributionListId != null;
    }

    public boolean isMyStory() {
        return Objects.equals(resolve().distributionListId, DistributionListId.from(1));
    }

    public boolean isActiveGroup() {
        return Stream.of(getParticipantIds()).anyMatch(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.recipients.Recipient$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((RecipientId) obj).equals(RecipientId.this);
            }
        });
    }

    public List<RecipientId> getParticipantIds() {
        return new ArrayList(this.participantIds);
    }

    public Drawable getFallbackContactPhotoDrawable(Context context, boolean z) {
        return getFallbackContactPhotoDrawable(context, z, DEFAULT_FALLBACK_PHOTO_PROVIDER, -1);
    }

    public Drawable getSmallFallbackContactPhotoDrawable(Context context, boolean z) {
        return getSmallFallbackContactPhotoDrawable(context, z, DEFAULT_FALLBACK_PHOTO_PROVIDER);
    }

    public Drawable getFallbackContactPhotoDrawable(Context context, boolean z, FallbackPhotoProvider fallbackPhotoProvider, int i) {
        return getFallbackContactPhoto((FallbackPhotoProvider) Util.firstNonNull(fallbackPhotoProvider, DEFAULT_FALLBACK_PHOTO_PROVIDER), i).asDrawable(context, this.avatarColor, z);
    }

    public Drawable getSmallFallbackContactPhotoDrawable(Context context, boolean z, FallbackPhotoProvider fallbackPhotoProvider) {
        return getSmallFallbackContactPhotoDrawable(context, z, fallbackPhotoProvider, -1);
    }

    public Drawable getSmallFallbackContactPhotoDrawable(Context context, boolean z, FallbackPhotoProvider fallbackPhotoProvider, int i) {
        return getFallbackContactPhoto((FallbackPhotoProvider) Util.firstNonNull(fallbackPhotoProvider, DEFAULT_FALLBACK_PHOTO_PROVIDER), i).asSmallDrawable(context, this.avatarColor, z);
    }

    public FallbackContactPhoto getFallbackContactPhoto() {
        return getFallbackContactPhoto(DEFAULT_FALLBACK_PHOTO_PROVIDER);
    }

    public FallbackContactPhoto getFallbackContactPhoto(FallbackPhotoProvider fallbackPhotoProvider) {
        return getFallbackContactPhoto(fallbackPhotoProvider, -1);
    }

    public FallbackContactPhoto getFallbackContactPhoto(FallbackPhotoProvider fallbackPhotoProvider, int i) {
        if (this.isSelf) {
            return fallbackPhotoProvider.getPhotoForLocalNumber();
        }
        if (isResolving()) {
            return fallbackPhotoProvider.getPhotoForResolvingRecipient();
        }
        if (isDistributionList()) {
            return fallbackPhotoProvider.getPhotoForDistributionList();
        }
        if (isGroupInternal()) {
            return fallbackPhotoProvider.getPhotoForGroup();
        }
        if (isGroup()) {
            return fallbackPhotoProvider.getPhotoForGroup();
        }
        if (!TextUtils.isEmpty(this.groupName)) {
            return fallbackPhotoProvider.getPhotoForRecipientWithName(this.groupName, i);
        }
        if (!TextUtils.isEmpty(this.systemContactName)) {
            return fallbackPhotoProvider.getPhotoForRecipientWithName(this.systemContactName, i);
        }
        if (!this.signalProfileName.isEmpty()) {
            return fallbackPhotoProvider.getPhotoForRecipientWithName(this.signalProfileName.toString(), i);
        }
        return fallbackPhotoProvider.getPhotoForRecipientWithoutName();
    }

    public ContactPhoto getContactPhoto() {
        if (this.isSelf) {
            return null;
        }
        if (isGroupInternal() && this.groupAvatarId.isPresent()) {
            return new GroupRecordContactPhoto(this.groupId, this.groupAvatarId.get().longValue());
        }
        if (this.systemContactPhoto != null && SignalStore.settings().isPreferSystemContactPhotos()) {
            return new SystemContactPhoto(this.id, this.systemContactPhoto, 0);
        }
        if (this.profileAvatar != null && this.profileAvatarFileDetails.hasFile()) {
            return new ProfileContactPhoto(this);
        }
        Uri uri = this.systemContactPhoto;
        if (uri != null) {
            return new SystemContactPhoto(this.id, uri, 0);
        }
        return null;
    }

    public Uri getMessageRingtone() {
        Uri uri = this.messageRingtone;
        if (uri == null || uri.getScheme() == null || !this.messageRingtone.getScheme().startsWith("file")) {
            return this.messageRingtone;
        }
        return null;
    }

    public Uri getCallRingtone() {
        Uri uri = this.callRingtone;
        if (uri == null || uri.getScheme() == null || !this.callRingtone.getScheme().startsWith("file")) {
            return this.callRingtone;
        }
        return null;
    }

    public boolean isMuted() {
        return System.currentTimeMillis() <= this.muteUntil;
    }

    public long getMuteUntil() {
        return this.muteUntil;
    }

    public boolean isBlocked() {
        return this.blocked;
    }

    public RecipientDatabase.VibrateState getMessageVibrate() {
        return this.messageVibrate;
    }

    public RecipientDatabase.VibrateState getCallVibrate() {
        return this.callVibrate;
    }

    public int getExpiresInSeconds() {
        return this.expireMessages;
    }

    public boolean hasSeenFirstInviteReminder() {
        return this.insightsBannerTier.seen(RecipientDatabase.InsightsBannerTier.TIER_ONE);
    }

    public boolean hasSeenSecondInviteReminder() {
        return this.insightsBannerTier.seen(RecipientDatabase.InsightsBannerTier.TIER_TWO);
    }

    public RecipientDatabase.RegisteredState getRegistered() {
        if (isPushGroup()) {
            return RecipientDatabase.RegisteredState.REGISTERED;
        }
        if (isMmsGroup()) {
            return RecipientDatabase.RegisteredState.NOT_REGISTERED;
        }
        return this.registered;
    }

    public boolean isRegistered() {
        return this.registered == RecipientDatabase.RegisteredState.REGISTERED || isPushGroup();
    }

    public boolean isMaybeRegistered() {
        return this.registered != RecipientDatabase.RegisteredState.NOT_REGISTERED || isPushGroup();
    }

    public boolean isUnregistered() {
        return this.registered == RecipientDatabase.RegisteredState.NOT_REGISTERED && !isPushGroup();
    }

    public String getNotificationChannel() {
        if (!NotificationChannels.supported()) {
            return null;
        }
        return this.notificationChannel;
    }

    public boolean isForceSmsSelection() {
        return this.forceSmsSelection;
    }

    public Capability getGroupsV1MigrationCapability() {
        return this.groupsV1MigrationCapability;
    }

    public Capability getSenderKeyCapability() {
        return this.senderKeyCapability;
    }

    public Capability getAnnouncementGroupCapability() {
        return this.announcementGroupCapability;
    }

    public Capability getChangeNumberCapability() {
        return this.changeNumberCapability;
    }

    public Capability getStoriesCapability() {
        return this.storiesCapability;
    }

    public Capability getGiftBadgesCapability() {
        return this.giftBadgesCapability;
    }

    public boolean supportsMessageRetries() {
        return getSenderKeyCapability() == Capability.SUPPORTED;
    }

    public byte[] getProfileKey() {
        return this.profileKey;
    }

    public ExpiringProfileKeyCredential getExpiringProfileKeyCredential() {
        return this.expiringProfileKeyCredential;
    }

    public byte[] getStorageServiceId() {
        return this.storageId;
    }

    public RecipientDatabase.UnidentifiedAccessMode getUnidentifiedAccessMode() {
        return this.unidentifiedAccessMode;
    }

    public ChatWallpaper getWallpaper() {
        ChatWallpaper chatWallpaper = this.wallpaper;
        if (chatWallpaper != null) {
            return chatWallpaper;
        }
        return SignalStore.wallpaper().getWallpaper();
    }

    public boolean hasOwnWallpaper() {
        return this.wallpaper != null;
    }

    public boolean hasWallpaper() {
        return this.wallpaper != null || SignalStore.wallpaper().hasWallpaperSet();
    }

    public boolean hasOwnChatColors() {
        return this.chatColors != null;
    }

    public ChatColors getChatColors() {
        ChatColors chatColors = this.chatColors;
        if (chatColors != null && !(chatColors.getId() instanceof ChatColors.Id.Auto)) {
            return this.chatColors;
        }
        if (this.chatColors != null) {
            return getAutoChatColor();
        }
        ChatColors chatColors2 = SignalStore.chatColorsValues().getChatColors();
        if (chatColors2 == null || (chatColors2.getId() instanceof ChatColors.Id.Auto)) {
            return getAutoChatColor();
        }
        return chatColors2;
    }

    private ChatColors getAutoChatColor() {
        if (getWallpaper() != null) {
            return getWallpaper().getAutoChatColors();
        }
        return ChatColorsPalette.Bubbles.getDefault().withId(ChatColors.Id.Auto.INSTANCE);
    }

    public AvatarColor getAvatarColor() {
        return this.avatarColor;
    }

    public boolean isSystemContact() {
        return this.contactUri != null;
    }

    public String getAbout() {
        return this.about;
    }

    public String getAboutEmoji() {
        return this.aboutEmoji;
    }

    public List<Badge> getBadges() {
        return (FeatureFlags.displayDonorBadges() || isSelf()) ? this.badges : Collections.emptyList();
    }

    public Badge getFeaturedBadge() {
        if (getBadges().isEmpty()) {
            return null;
        }
        return getBadges().get(0);
    }

    public String getCombinedAboutAndEmoji() {
        if (!Util.isEmpty(this.aboutEmoji)) {
            if (Util.isEmpty(this.about)) {
                return this.aboutEmoji;
            }
            return this.aboutEmoji + " " + this.about;
        } else if (!Util.isEmpty(this.about)) {
            return this.about;
        } else {
            return null;
        }
    }

    public boolean shouldBlurAvatar() {
        if ((this.extras.isPresent() ? this.extras.get().manuallyShownAvatar() : false) || isSelf() || isProfileSharing() || isSystemContact() || this.hasGroupsInCommon || !isRegistered()) {
            return false;
        }
        return true;
    }

    public boolean hasGroupsInCommon() {
        return this.hasGroupsInCommon;
    }

    public Recipient resolve() {
        return this.resolving ? live().resolve() : this;
    }

    public boolean isResolving() {
        return this.resolving;
    }

    public Recipient fresh() {
        return live().resolve();
    }

    public LiveRecipient live() {
        return ApplicationDependencies.getRecipientCache().getLive(this.id);
    }

    public RecipientDatabase.MentionSetting getMentionSetting() {
        return this.mentionSetting;
    }

    public boolean isReleaseNotes() {
        return this.isReleaseNotesRecipient;
    }

    public boolean showVerified() {
        return this.isReleaseNotesRecipient || this.isSelf;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.id.equals(((Recipient) obj).id);
    }

    public int hashCode() {
        return Objects.hash(this.id);
    }

    /* loaded from: classes4.dex */
    public enum Capability {
        UNKNOWN(0),
        SUPPORTED(1),
        NOT_SUPPORTED(2);
        
        private final int value;

        Capability(int i) {
            this.value = i;
        }

        public int serialize() {
            return this.value;
        }

        public static Capability deserialize(int i) {
            if (i == 0) {
                return UNKNOWN;
            }
            if (i == 1) {
                return SUPPORTED;
            }
            if (i == 2) {
                return NOT_SUPPORTED;
            }
            throw new IllegalArgumentException();
        }

        public static Capability fromBoolean(boolean z) {
            return z ? SUPPORTED : NOT_SUPPORTED;
        }
    }

    /* loaded from: classes4.dex */
    public static final class Extras {
        private final RecipientExtras recipientExtras;

        public static Extras from(RecipientExtras recipientExtras) {
            if (recipientExtras != null) {
                return new Extras(recipientExtras);
            }
            return null;
        }

        private Extras(RecipientExtras recipientExtras) {
            this.recipientExtras = recipientExtras;
        }

        public boolean manuallyShownAvatar() {
            return this.recipientExtras.getManuallyShownAvatar();
        }

        public boolean hideStory() {
            return this.recipientExtras.getHideStory();
        }

        public boolean hasViewedStory() {
            return this.recipientExtras.getLastStoryView() > 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Extras.class != obj.getClass()) {
                return false;
            }
            Extras extras = (Extras) obj;
            if (manuallyShownAvatar() == extras.manuallyShownAvatar() && hideStory() == extras.hideStory() && hasViewedStory() == extras.hasViewedStory()) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return Objects.hash(Boolean.valueOf(manuallyShownAvatar()), Boolean.valueOf(hideStory()), Boolean.valueOf(hasViewedStory()));
        }
    }

    public boolean hasSameContent(Recipient recipient) {
        return Objects.equals(this.id, recipient.id) && this.resolving == recipient.resolving && this.isSelf == recipient.isSelf && this.blocked == recipient.blocked && this.muteUntil == recipient.muteUntil && this.expireMessages == recipient.expireMessages && Objects.equals(this.profileAvatarFileDetails, recipient.profileAvatarFileDetails) && this.profileSharing == recipient.profileSharing && this.lastProfileFetch == recipient.lastProfileFetch && this.forceSmsSelection == recipient.forceSmsSelection && Objects.equals(this.serviceId, recipient.serviceId) && Objects.equals(this.username, recipient.username) && Objects.equals(this.e164, recipient.e164) && Objects.equals(this.email, recipient.email) && Objects.equals(this.groupId, recipient.groupId) && Objects.equals(this.participantIds, recipient.participantIds) && Objects.equals(this.groupAvatarId, recipient.groupAvatarId) && this.messageVibrate == recipient.messageVibrate && this.callVibrate == recipient.callVibrate && Objects.equals(this.messageRingtone, recipient.messageRingtone) && Objects.equals(this.callRingtone, recipient.callRingtone) && Objects.equals(this.defaultSubscriptionId, recipient.defaultSubscriptionId) && this.registered == recipient.registered && Arrays.equals(this.profileKey, recipient.profileKey) && Objects.equals(this.expiringProfileKeyCredential, recipient.expiringProfileKeyCredential) && Objects.equals(this.groupName, recipient.groupName) && Objects.equals(this.systemContactPhoto, recipient.systemContactPhoto) && Objects.equals(this.customLabel, recipient.customLabel) && Objects.equals(this.contactUri, recipient.contactUri) && Objects.equals(this.signalProfileName, recipient.signalProfileName) && Objects.equals(this.systemProfileName, recipient.systemProfileName) && Objects.equals(this.profileAvatar, recipient.profileAvatar) && Objects.equals(this.notificationChannel, recipient.notificationChannel) && this.unidentifiedAccessMode == recipient.unidentifiedAccessMode && this.groupsV1MigrationCapability == recipient.groupsV1MigrationCapability && this.insightsBannerTier == recipient.insightsBannerTier && Arrays.equals(this.storageId, recipient.storageId) && this.mentionSetting == recipient.mentionSetting && Objects.equals(this.wallpaper, recipient.wallpaper) && Objects.equals(this.chatColors, recipient.chatColors) && Objects.equals(this.avatarColor, recipient.avatarColor) && Objects.equals(this.about, recipient.about) && Objects.equals(this.aboutEmoji, recipient.aboutEmoji) && Objects.equals(this.extras, recipient.extras) && this.hasGroupsInCommon == recipient.hasGroupsInCommon && Objects.equals(this.badges, recipient.badges);
    }

    private static boolean allContentsAreTheSame(List<Recipient> list, List<Recipient> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (!list.get(i).hasSameContent(list2.get(i))) {
                return false;
            }
        }
        return true;
    }

    /* loaded from: classes4.dex */
    public static class FallbackPhotoProvider {
        public FallbackContactPhoto getPhotoForLocalNumber() {
            return new ResourceContactPhoto(R.drawable.ic_note_34, R.drawable.ic_note_24);
        }

        public FallbackContactPhoto getPhotoForResolvingRecipient() {
            return new TransparentContactPhoto();
        }

        public FallbackContactPhoto getPhotoForGroup() {
            return new ResourceContactPhoto(R.drawable.ic_group_outline_34, R.drawable.ic_group_outline_20, R.drawable.ic_group_outline_48);
        }

        public FallbackContactPhoto getPhotoForRecipientWithName(String str, int i) {
            return new GeneratedContactPhoto(str, R.drawable.ic_profile_outline_40, i);
        }

        public FallbackContactPhoto getPhotoForRecipientWithoutName() {
            return new ResourceContactPhoto(R.drawable.ic_profile_outline_40, R.drawable.ic_profile_outline_20, R.drawable.ic_profile_outline_48);
        }

        public FallbackContactPhoto getPhotoForDistributionList() {
            return new ResourceContactPhoto(R.drawable.ic_group_outline_34, R.drawable.ic_group_outline_20, R.drawable.ic_group_outline_48);
        }
    }

    /* loaded from: classes4.dex */
    public static class MissingAddressError extends AssertionError {
        MissingAddressError(RecipientId recipientId) {
            super("Missing address for " + recipientId.serialize());
        }
    }
}
