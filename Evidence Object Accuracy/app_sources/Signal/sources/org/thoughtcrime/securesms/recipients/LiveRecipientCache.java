package org.thoughtcrime.securesms.recipients;

import android.content.Context;
import android.database.Cursor;
import j$.util.Collection$EL;
import j$.util.function.Consumer;
import j$.util.function.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.LRUCache;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.concurrent.FilteredExecutor;
import org.whispersystems.signalservice.api.push.ACI;

/* loaded from: classes.dex */
public final class LiveRecipientCache {
    private static final int CACHE_MAX;
    private static final int CONTACT_CACHE_WARM_MAX;
    private static final String TAG = Log.tag(LiveRecipientCache.class);
    private static final int THREAD_CACHE_WARM_MAX;
    private final Context context;
    private final AtomicReference<RecipientId> localRecipientId = new AtomicReference<>(null);
    private final RecipientDatabase recipientDatabase = SignalDatabase.recipients();
    private final Map<RecipientId, LiveRecipient> recipients = new LRUCache(1000);
    private final Executor resolveExecutor;
    private final LiveRecipient unknown;
    private final AtomicBoolean warmedUp = new AtomicBoolean(false);

    public LiveRecipientCache(Context context) {
        this.context = context.getApplicationContext();
        this.unknown = new LiveRecipient(context, Recipient.UNKNOWN);
        this.resolveExecutor = ThreadUtil.trace(new FilteredExecutor(SignalExecutors.newCachedBoundedExecutor("signal-recipients", 1, 4, 15), new FilteredExecutor.Filter() { // from class: org.thoughtcrime.securesms.recipients.LiveRecipientCache$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.util.concurrent.FilteredExecutor.Filter
            public final boolean shouldRunOnExecutor() {
                return LiveRecipientCache.lambda$new$0();
            }
        }));
    }

    public static /* synthetic */ boolean lambda$new$0() {
        return !SignalDatabase.inTransaction();
    }

    public LiveRecipient getLive(RecipientId recipientId) {
        LiveRecipient liveRecipient;
        boolean z;
        if (recipientId.isUnknown()) {
            return this.unknown;
        }
        synchronized (this.recipients) {
            liveRecipient = this.recipients.get(recipientId);
            if (liveRecipient == null) {
                liveRecipient = new LiveRecipient(this.context, new Recipient(recipientId));
                this.recipients.put(recipientId, liveRecipient);
                z = true;
            } else {
                z = false;
            }
        }
        if (z) {
            this.resolveExecutor.execute(new Runnable() { // from class: org.thoughtcrime.securesms.recipients.LiveRecipientCache$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    LiveRecipient.this.resolve();
                }
            });
        }
        return liveRecipient;
    }

    public void remap(RecipientId recipientId, RecipientId recipientId2) {
        synchronized (this.recipients) {
            if (this.recipients.containsKey(recipientId2)) {
                Map<RecipientId, LiveRecipient> map = this.recipients;
                map.put(recipientId, map.get(recipientId2));
            } else {
                this.recipients.remove(recipientId);
            }
        }
    }

    public void addToCache(Collection<Recipient> collection) {
        Collection$EL.stream(collection).filter(new Predicate() { // from class: org.thoughtcrime.securesms.recipients.LiveRecipientCache$$ExternalSyntheticLambda2
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return LiveRecipientCache.this.isValidForCache((Recipient) obj);
            }
        }).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.recipients.LiveRecipientCache$$ExternalSyntheticLambda3
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                LiveRecipientCache.this.lambda$addToCache$2((Recipient) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    public /* synthetic */ void lambda$addToCache$2(Recipient recipient) {
        LiveRecipient liveRecipient;
        boolean z;
        synchronized (this.recipients) {
            liveRecipient = this.recipients.get(recipient.getId());
            if (liveRecipient == null) {
                liveRecipient = new LiveRecipient(this.context, recipient);
                this.recipients.put(recipient.getId(), liveRecipient);
                z = recipient.isResolving();
            } else {
                if (!liveRecipient.get().isResolving() && recipient.isResolving()) {
                    z = false;
                }
                liveRecipient.set(recipient);
                z = recipient.isResolving();
            }
        }
        if (z) {
            this.resolveExecutor.execute(new Runnable(new RecipientDatabase.MissingRecipientException(liveRecipient.getId())) { // from class: org.thoughtcrime.securesms.recipients.LiveRecipientCache$$ExternalSyntheticLambda0
                public final /* synthetic */ RecipientDatabase.MissingRecipientException f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    LiveRecipientCache.lambda$addToCache$1(LiveRecipient.this, this.f$1);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$addToCache$1(LiveRecipient liveRecipient, RecipientDatabase.MissingRecipientException missingRecipientException) {
        try {
            liveRecipient.resolve();
        } catch (RecipientDatabase.MissingRecipientException unused) {
            throw missingRecipientException;
        }
    }

    public Recipient getSelf() {
        RecipientId recipientId;
        synchronized (this.localRecipientId) {
            recipientId = this.localRecipientId.get();
        }
        if (recipientId == null) {
            ACI aci = SignalStore.account().getAci();
            String e164 = SignalStore.account().getE164();
            if (aci == null && e164 == null) {
                throw new IllegalStateException("Tried to call getSelf() before local data was set!");
            }
            if (aci != null) {
                recipientId = this.recipientDatabase.getByServiceId(aci).orElse(null);
            }
            if (recipientId == null && e164 != null) {
                recipientId = this.recipientDatabase.getByE164(e164).orElse(null);
            }
            if (recipientId == null) {
                recipientId = this.recipientDatabase.getAndPossiblyMerge(aci, e164);
            }
            synchronized (this.localRecipientId) {
                if (this.localRecipientId.get() == null) {
                    this.localRecipientId.set(recipientId);
                }
            }
        }
        return getLive(recipientId).resolve();
    }

    public void warmUp() {
        if (!this.warmedUp.getAndSet(true)) {
            SignalExecutors.BOUNDED.execute(new Runnable(new Stopwatch("recipient-warm-up")) { // from class: org.thoughtcrime.securesms.recipients.LiveRecipientCache$$ExternalSyntheticLambda4
                public final /* synthetic */ Stopwatch f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    LiveRecipientCache.this.lambda$warmUp$3(this.f$1);
                }
            });
        }
    }

    public /* synthetic */ void lambda$warmUp$3(Stopwatch stopwatch) {
        ThreadDatabase threads = SignalDatabase.threads();
        ArrayList arrayList = new ArrayList();
        int i = 0;
        ThreadDatabase.Reader readerFor = threads.readerFor(threads.getRecentConversationList(THREAD_CACHE_WARM_MAX, false, false));
        int i2 = 0;
        while (true) {
            try {
                ThreadRecord next = readerFor.getNext();
                if (next == null || i2 >= THREAD_CACHE_WARM_MAX) {
                    break;
                }
                arrayList.add(next.getRecipient());
                i2++;
            } catch (Throwable th) {
                if (readerFor != null) {
                    try {
                        readerFor.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        readerFor.close();
        String str = TAG;
        Log.d(str, "Warming up " + arrayList.size() + " thread recipients.");
        addToCache(arrayList);
        stopwatch.split(ThreadDatabase.TABLE_NAME);
        if (SignalStore.registrationValues().isRegistrationComplete() && SignalStore.account().getAci() != null) {
            Cursor nonGroupContacts = SignalDatabase.recipients().getNonGroupContacts(false);
            while (nonGroupContacts != null) {
                try {
                    if (!nonGroupContacts.moveToNext() || i >= 50) {
                        break;
                    }
                    Recipient.resolved(RecipientId.from(CursorUtil.requireLong(nonGroupContacts, "_id")));
                    i++;
                } catch (Throwable th3) {
                    if (nonGroupContacts != null) {
                        try {
                            nonGroupContacts.close();
                        } catch (Throwable th4) {
                            th3.addSuppressed(th4);
                        }
                    }
                    throw th3;
                }
            }
            String str2 = TAG;
            Log.d(str2, "Warmed up " + i + " contact recipient.");
            stopwatch.split("contact");
            if (nonGroupContacts != null) {
                nonGroupContacts.close();
            }
        }
        stopwatch.stop(TAG);
    }

    public void clearSelf() {
        synchronized (this.localRecipientId) {
            this.localRecipientId.set(null);
        }
    }

    public void clear() {
        synchronized (this.recipients) {
            this.recipients.clear();
        }
    }

    public boolean isValidForCache(Recipient recipient) {
        return !recipient.getId().isUnknown() && (recipient.hasServiceId() || recipient.getGroupId().isPresent() || recipient.hasSmsAddress());
    }
}
