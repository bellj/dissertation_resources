package org.thoughtcrime.securesms.recipients;

/* loaded from: classes4.dex */
public interface RecipientModifiedListener {
    void onModified(Recipient recipient);
}
