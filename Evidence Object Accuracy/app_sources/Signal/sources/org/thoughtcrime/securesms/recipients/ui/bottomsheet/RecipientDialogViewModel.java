package org.thoughtcrime.securesms.recipients.ui.bottomsheet;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;
import androidx.arch.core.util.Function;
import androidx.core.util.Consumer;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.ThreadUtil;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.BlockUnblockDialog;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsActivity;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.groups.ui.GroupErrors;
import org.thoughtcrime.securesms.groups.ui.addtogroup.AddToGroupsActivity;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository;
import org.thoughtcrime.securesms.stories.StoryViewerArgs;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerActivity;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.verify.VerifyIdentityActivity;

/* loaded from: classes4.dex */
public final class RecipientDialogViewModel extends ViewModel {
    private final MutableLiveData<Boolean> adminActionBusy;
    private final LiveData<AdminActionStatus> adminActionStatus;
    private final LiveData<Boolean> canAddToAGroup;
    private final Context context;
    private final CompositeDisposable disposables;
    private final MutableLiveData<IdentityRecord> identity;
    private final LiveData<Recipient> recipient;
    private final RecipientDialogRepository recipientDialogRepository;
    private final MutableLiveData<StoryViewState> storyViewState;

    public static /* synthetic */ void lambda$onMakeGroupAdminClicked$13(DialogInterface dialogInterface, int i) {
    }

    public static /* synthetic */ void lambda$onRemoveFromGroupClicked$19(DialogInterface dialogInterface, int i) {
    }

    public static /* synthetic */ void lambda$onRemoveGroupAdminClicked$16(DialogInterface dialogInterface, int i) {
    }

    private RecipientDialogViewModel(Context context, RecipientDialogRepository recipientDialogRepository) {
        this.context = context;
        this.recipientDialogRepository = recipientDialogRepository;
        MutableLiveData<IdentityRecord> mutableLiveData = new MutableLiveData<>();
        this.identity = mutableLiveData;
        this.adminActionBusy = new MutableLiveData<>(Boolean.FALSE);
        MutableLiveData<StoryViewState> mutableLiveData2 = new MutableLiveData<>();
        this.storyViewState = mutableLiveData2;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        boolean equals = recipientDialogRepository.getRecipientId().equals(Recipient.self().getId());
        LiveData<Recipient> liveData = Recipient.live(recipientDialogRepository.getRecipientId()).getLiveData();
        this.recipient = liveData;
        if (recipientDialogRepository.getGroupId() == null || !recipientDialogRepository.getGroupId().isV2() || equals) {
            this.adminActionStatus = new MutableLiveData(new AdminActionStatus(false, false, false, false));
        } else {
            LiveGroup liveGroup = new LiveGroup(recipientDialogRepository.getGroupId());
            this.adminActionStatus = LiveDataUtil.combineLatest(LiveDataUtil.combineLatest(liveGroup.isSelfAdmin(), Transformations.map(liveGroup.getGroupLink(), new Function() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda6
                @Override // androidx.arch.core.util.Function
                public final Object apply(Object obj) {
                    return RecipientDialogViewModel.lambda$new$0((GroupLinkUrlAndStatus) obj);
                }
            }), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda7
                @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
                public final Object apply(Object obj, Object obj2) {
                    return new Pair((Boolean) obj, (Boolean) obj2);
                }
            }), Transformations.switchMap(liveData, new Function() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda8
                @Override // androidx.arch.core.util.Function
                public final Object apply(Object obj) {
                    return LiveGroup.this.getMemberLevel((Recipient) obj);
                }
            }), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda9
                @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
                public final Object apply(Object obj, Object obj2) {
                    return RecipientDialogViewModel.lambda$new$1((Pair) obj, (GroupDatabase.MemberLevel) obj2);
                }
            });
        }
        if (!recipientDialogRepository.getRecipientId().equals(Recipient.self().getId())) {
            Objects.requireNonNull(mutableLiveData);
            recipientDialogRepository.getIdentity(new Consumer() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda10
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    MutableLiveData.this.postValue((IdentityRecord) obj);
                }
            });
        }
        MutableLiveData mutableLiveData3 = new MutableLiveData(0);
        this.canAddToAGroup = LiveDataUtil.combineLatest(liveData, mutableLiveData3, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda11
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return RecipientDialogViewModel.lambda$new$2((Recipient) obj, (Integer) obj2);
            }
        });
        recipientDialogRepository.getActiveGroupCount(new Consumer() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda12
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                MutableLiveData.this.postValue((Integer) obj);
            }
        });
        Observable<StoryViewState> forRecipientId = StoryViewState.getForRecipientId(recipientDialogRepository.getRecipientId());
        Objects.requireNonNull(mutableLiveData2);
        compositeDisposable.add(forRecipientId.subscribe(new io.reactivex.rxjava3.functions.Consumer() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda13
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                MutableLiveData.this.postValue((StoryViewState) obj);
            }
        }));
    }

    public static /* synthetic */ Boolean lambda$new$0(GroupLinkUrlAndStatus groupLinkUrlAndStatus) {
        return Boolean.valueOf(groupLinkUrlAndStatus == null || groupLinkUrlAndStatus.isEnabled());
    }

    public static /* synthetic */ AdminActionStatus lambda$new$1(Pair pair, GroupDatabase.MemberLevel memberLevel) {
        boolean booleanValue = ((Boolean) pair.first()).booleanValue();
        boolean booleanValue2 = ((Boolean) pair.second()).booleanValue();
        boolean isInGroup = memberLevel.isInGroup();
        boolean z = true;
        boolean z2 = memberLevel == GroupDatabase.MemberLevel.ADMINISTRATOR;
        boolean z3 = isInGroup && booleanValue;
        boolean z4 = isInGroup && booleanValue && !z2;
        if (!isInGroup || !booleanValue || !z2) {
            z = false;
        }
        return new AdminActionStatus(z3, z4, z, booleanValue2);
    }

    public static /* synthetic */ Boolean lambda$new$2(Recipient recipient, Integer num) {
        return Boolean.valueOf(num.intValue() > 0 && recipient.isRegistered() && !recipient.isGroup() && !recipient.isSelf() && !recipient.isBlocked());
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        this.disposables.clear();
    }

    public LiveData<StoryViewState> getStoryViewState() {
        return this.storyViewState;
    }

    public LiveData<Recipient> getRecipient() {
        return this.recipient;
    }

    public LiveData<Boolean> getCanAddToAGroup() {
        return this.canAddToAGroup;
    }

    public LiveData<AdminActionStatus> getAdminActionStatus() {
        return this.adminActionStatus;
    }

    public LiveData<IdentityRecord> getIdentity() {
        return this.identity;
    }

    public LiveData<Boolean> getAdminActionBusy() {
        return this.adminActionBusy;
    }

    public void onNoteToSelfClicked(Activity activity) {
        if (this.storyViewState.getValue() == null || this.storyViewState.getValue() == StoryViewState.NONE) {
            onMessageClicked(activity);
        } else {
            activity.startActivity(StoryViewerActivity.createIntent(activity, new StoryViewerArgs.Builder(this.recipientDialogRepository.getRecipientId(), this.recipient.getValue().shouldHideStory()).build()));
        }
    }

    public void onMessageClicked(Activity activity) {
        this.recipientDialogRepository.getRecipient(new RecipientDialogRepository.RecipientCallback(activity) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda15
            public final /* synthetic */ Activity f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository.RecipientCallback
            public final void onRecipient(Recipient recipient) {
                CommunicationActions.startConversation(this.f$0, recipient, null);
            }
        });
    }

    public void onSecureCallClicked(FragmentActivity fragmentActivity) {
        this.recipientDialogRepository.getRecipient(new RecipientDialogRepository.RecipientCallback() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda27
            @Override // org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository.RecipientCallback
            public final void onRecipient(Recipient recipient) {
                CommunicationActions.startVoiceCall(FragmentActivity.this, recipient);
            }
        });
    }

    public void onInsecureCallClicked(FragmentActivity fragmentActivity) {
        this.recipientDialogRepository.getRecipient(new RecipientDialogRepository.RecipientCallback() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository.RecipientCallback
            public final void onRecipient(Recipient recipient) {
                CommunicationActions.startInsecureCall(FragmentActivity.this, recipient);
            }
        });
    }

    public void onSecureVideoCallClicked(FragmentActivity fragmentActivity) {
        this.recipientDialogRepository.getRecipient(new RecipientDialogRepository.RecipientCallback() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda14
            @Override // org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository.RecipientCallback
            public final void onRecipient(Recipient recipient) {
                CommunicationActions.startVideoCall(FragmentActivity.this, recipient);
            }
        });
    }

    public /* synthetic */ void lambda$onBlockClicked$7(Recipient recipient) {
        RecipientUtil.blockNonGroup(this.context, recipient);
    }

    public /* synthetic */ void lambda$onBlockClicked$8(FragmentActivity fragmentActivity, Recipient recipient) {
        BlockUnblockDialog.showBlockFor(fragmentActivity, fragmentActivity.getLifecycle(), recipient, new Runnable(recipient) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                RecipientDialogViewModel.this.lambda$onBlockClicked$7(this.f$1);
            }
        });
    }

    public void onBlockClicked(FragmentActivity fragmentActivity) {
        this.recipientDialogRepository.getRecipient(new RecipientDialogRepository.RecipientCallback(fragmentActivity) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda20
            public final /* synthetic */ FragmentActivity f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository.RecipientCallback
            public final void onRecipient(Recipient recipient) {
                RecipientDialogViewModel.this.lambda$onBlockClicked$8(this.f$1, recipient);
            }
        });
    }

    public /* synthetic */ void lambda$onUnblockClicked$10(FragmentActivity fragmentActivity, Recipient recipient) {
        BlockUnblockDialog.showUnblockFor(fragmentActivity, fragmentActivity.getLifecycle(), recipient, new Runnable(recipient) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda26
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                RecipientDialogViewModel.this.lambda$onUnblockClicked$9(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onUnblockClicked$9(Recipient recipient) {
        RecipientUtil.unblock(this.context, recipient);
    }

    public void onUnblockClicked(FragmentActivity fragmentActivity) {
        this.recipientDialogRepository.getRecipient(new RecipientDialogRepository.RecipientCallback(fragmentActivity) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda22
            public final /* synthetic */ FragmentActivity f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository.RecipientCallback
            public final void onRecipient(Recipient recipient) {
                RecipientDialogViewModel.this.lambda$onUnblockClicked$10(this.f$1, recipient);
            }
        });
    }

    public void onViewSafetyNumberClicked(Activity activity, IdentityRecord identityRecord) {
        activity.startActivity(VerifyIdentityActivity.newIntent(activity, identityRecord));
    }

    public void onAvatarClicked(Activity activity) {
        if (this.storyViewState.getValue() == null || this.storyViewState.getValue() == StoryViewState.NONE) {
            activity.startActivity(ConversationSettingsActivity.forRecipient(activity, this.recipientDialogRepository.getRecipientId()));
        } else {
            activity.startActivity(StoryViewerActivity.createIntent(activity, new StoryViewerArgs.Builder(this.recipientDialogRepository.getRecipientId(), this.recipient.getValue().shouldHideStory()).build()));
        }
    }

    public void onMakeGroupAdminClicked(Activity activity) {
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(activity);
        Context context = this.context;
        Recipient value = this.recipient.getValue();
        Objects.requireNonNull(value);
        materialAlertDialogBuilder.setMessage((CharSequence) context.getString(R.string.RecipientBottomSheet_s_will_be_able_to_edit_group, value.getDisplayName(this.context))).setPositiveButton(R.string.RecipientBottomSheet_make_admin, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(activity) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda16
            public final /* synthetic */ Activity f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RecipientDialogViewModel.this.lambda$onMakeGroupAdminClicked$12(this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda17
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RecipientDialogViewModel.lambda$onMakeGroupAdminClicked$13(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onMakeGroupAdminClicked$12(Activity activity, DialogInterface dialogInterface, int i) {
        this.adminActionBusy.setValue(Boolean.TRUE);
        this.recipientDialogRepository.setMemberAdmin(true, new Consumer(activity) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda21
            public final /* synthetic */ Activity f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                RecipientDialogViewModel.this.lambda$onMakeGroupAdminClicked$11(this.f$1, (Boolean) obj);
            }
        }, new RecipientDialogViewModel$$ExternalSyntheticLambda5(this));
    }

    public /* synthetic */ void lambda$onMakeGroupAdminClicked$11(Activity activity, Boolean bool) {
        this.adminActionBusy.setValue(Boolean.FALSE);
        if (!bool.booleanValue()) {
            Toast.makeText(activity, (int) R.string.ManageGroupActivity_failed_to_update_the_group, 0).show();
        }
    }

    public void onRemoveGroupAdminClicked(Activity activity) {
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(activity);
        Context context = this.context;
        Recipient value = this.recipient.getValue();
        Objects.requireNonNull(value);
        materialAlertDialogBuilder.setMessage((CharSequence) context.getString(R.string.RecipientBottomSheet_remove_s_as_group_admin, value.getDisplayName(this.context))).setPositiveButton(R.string.RecipientBottomSheet_remove_as_admin, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(activity) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ Activity f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RecipientDialogViewModel.this.lambda$onRemoveGroupAdminClicked$15(this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda3
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RecipientDialogViewModel.lambda$onRemoveGroupAdminClicked$16(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onRemoveGroupAdminClicked$15(Activity activity, DialogInterface dialogInterface, int i) {
        this.adminActionBusy.setValue(Boolean.TRUE);
        this.recipientDialogRepository.setMemberAdmin(false, new Consumer(activity) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda18
            public final /* synthetic */ Activity f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                RecipientDialogViewModel.this.lambda$onRemoveGroupAdminClicked$14(this.f$1, (Boolean) obj);
            }
        }, new RecipientDialogViewModel$$ExternalSyntheticLambda5(this));
    }

    public /* synthetic */ void lambda$onRemoveGroupAdminClicked$14(Activity activity, Boolean bool) {
        this.adminActionBusy.setValue(Boolean.FALSE);
        if (!bool.booleanValue()) {
            Toast.makeText(activity, (int) R.string.ManageGroupActivity_failed_to_update_the_group, 0).show();
        }
    }

    public void onRemoveFromGroupClicked(Activity activity, boolean z, Runnable runnable) {
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(activity);
        Context context = this.context;
        int i = z ? R.string.RecipientBottomSheet_remove_s_from_the_group_they_will_not_be_able_to_rejoin : R.string.RecipientBottomSheet_remove_s_from_the_group;
        Recipient value = this.recipient.getValue();
        Objects.requireNonNull(value);
        materialAlertDialogBuilder.setMessage((CharSequence) context.getString(i, value.getDisplayName(this.context))).setPositiveButton(R.string.RecipientBottomSheet_remove, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda23
            public final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                RecipientDialogViewModel.this.lambda$onRemoveFromGroupClicked$18(this.f$1, dialogInterface, i2);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda24
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                RecipientDialogViewModel.lambda$onRemoveFromGroupClicked$19(dialogInterface, i2);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onRemoveFromGroupClicked$18(Runnable runnable, DialogInterface dialogInterface, int i) {
        this.adminActionBusy.setValue(Boolean.TRUE);
        this.recipientDialogRepository.removeMember(new Consumer(runnable) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda4
            public final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                RecipientDialogViewModel.this.lambda$onRemoveFromGroupClicked$17(this.f$1, (Boolean) obj);
            }
        }, new RecipientDialogViewModel$$ExternalSyntheticLambda5(this));
    }

    public /* synthetic */ void lambda$onRemoveFromGroupClicked$17(Runnable runnable, Boolean bool) {
        this.adminActionBusy.setValue(Boolean.FALSE);
        if (bool.booleanValue()) {
            runnable.run();
        }
    }

    public void refreshRecipient() {
        this.recipientDialogRepository.refreshRecipient();
    }

    public /* synthetic */ void lambda$onAddToGroupButton$20(Activity activity, List list) {
        activity.startActivity(AddToGroupsActivity.newIntent(activity, this.recipientDialogRepository.getRecipientId(), list));
    }

    public void onAddToGroupButton(Activity activity) {
        this.recipientDialogRepository.getGroupMembership(new Consumer(activity) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda19
            public final /* synthetic */ Activity f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                RecipientDialogViewModel.this.lambda$onAddToGroupButton$20(this.f$1, (List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$showErrorToast$21(GroupChangeFailureReason groupChangeFailureReason) {
        Toast.makeText(this.context, GroupErrors.getUserDisplayMessage(groupChangeFailureReason), 1).show();
    }

    public void showErrorToast(GroupChangeFailureReason groupChangeFailureReason) {
        ThreadUtil.runOnMain(new Runnable(groupChangeFailureReason) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel$$ExternalSyntheticLambda25
            public final /* synthetic */ GroupChangeFailureReason f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                RecipientDialogViewModel.this.lambda$showErrorToast$21(this.f$1);
            }
        });
    }

    /* loaded from: classes4.dex */
    public static class AdminActionStatus {
        private final boolean canMakeAdmin;
        private final boolean canMakeNonAdmin;
        private final boolean canRemove;
        private final boolean isLinkActive;

        AdminActionStatus(boolean z, boolean z2, boolean z3, boolean z4) {
            this.canRemove = z;
            this.canMakeAdmin = z2;
            this.canMakeNonAdmin = z3;
            this.isLinkActive = z4;
        }

        public boolean isCanRemove() {
            return this.canRemove;
        }

        public boolean isCanMakeAdmin() {
            return this.canMakeAdmin;
        }

        public boolean isCanMakeNonAdmin() {
            return this.canMakeNonAdmin;
        }

        public boolean isLinkActive() {
            return this.isLinkActive;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final Context context;
        private final GroupId groupId;
        private final RecipientId recipientId;

        public Factory(Context context, RecipientId recipientId, GroupId groupId) {
            this.context = context;
            this.recipientId = recipientId;
            this.groupId = groupId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new RecipientDialogViewModel(this.context, new RecipientDialogRepository(this.context, this.recipientId, this.groupId));
        }
    }
}
