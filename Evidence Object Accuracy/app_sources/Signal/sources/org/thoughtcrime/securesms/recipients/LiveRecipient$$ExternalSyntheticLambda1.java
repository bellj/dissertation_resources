package org.thoughtcrime.securesms.recipients;

import io.reactivex.rxjava3.core.ObservableEmitter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LiveRecipient$$ExternalSyntheticLambda1 implements RecipientForeverObserver {
    public final /* synthetic */ ObservableEmitter f$0;

    public /* synthetic */ LiveRecipient$$ExternalSyntheticLambda1(ObservableEmitter observableEmitter) {
        this.f$0 = observableEmitter;
    }

    @Override // org.thoughtcrime.securesms.recipients.RecipientForeverObserver
    public final void onRecipientChanged(Recipient recipient) {
        this.f$0.onNext(recipient);
    }
}
