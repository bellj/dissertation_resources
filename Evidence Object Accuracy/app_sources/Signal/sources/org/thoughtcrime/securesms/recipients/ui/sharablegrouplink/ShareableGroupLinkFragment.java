package org.thoughtcrime.securesms.recipients.ui.sharablegrouplink;

import android.content.DialogInterface;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;
import org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkViewModel;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* compiled from: ShareableGroupLinkFragment.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0018\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\b\u0010\u001d\u001a\u00020\u0010H\u0002J\u0012\u0010\u001e\u001a\u00020\u00102\b\b\u0001\u0010\u001f\u001a\u00020 H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00068BX\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\t\u001a\u00020\n8BX\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\f¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/recipients/ui/sharablegrouplink/ShareableGroupLinkFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "busyDialog", "Lorg/thoughtcrime/securesms/util/views/SimpleProgressDialog$DismissibleDialog;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId$V2;", "getGroupId", "()Lorg/thoughtcrime/securesms/groups/GroupId$V2;", "viewModel", "Lorg/thoughtcrime/securesms/recipients/ui/sharablegrouplink/ShareableGroupLinkViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/recipients/ui/sharablegrouplink/ShareableGroupLinkViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "formatForFullWidthWrapping", "", "url", "", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "groupLink", "Lorg/thoughtcrime/securesms/groups/v2/GroupLinkUrlAndStatus;", "canEdit", "", "onResetGroupLink", "toast", "message", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ShareableGroupLinkFragment extends DSLSettingsFragment {
    private SimpleProgressDialog.DismissibleDialog busyDialog;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(ShareableGroupLinkViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$viewModel$2
        final /* synthetic */ ShareableGroupLinkFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new ShareableGroupLinkViewModel.Factory(ShareableGroupLinkFragment.access$getGroupId(this.this$0), new ShareableGroupLinkRepository(this.this$0.requireContext(), ShareableGroupLinkFragment.access$getGroupId(this.this$0)));
        }
    });

    public ShareableGroupLinkFragment() {
        super(R.string.ShareableGroupLinkDialogFragment__group_link, 0, 0, null, 14, null);
    }

    public final GroupId.V2 getGroupId() {
        GroupId.V2 requireV2 = GroupId.parseOrThrow(ShareableGroupLinkFragmentArgs.fromBundle(requireArguments()).getGroupId()).requireV2();
        Intrinsics.checkNotNullExpressionValue(requireV2, "parseOrThrow(ShareableGr…s()).groupId).requireV2()");
        return requireV2;
    }

    public final ShareableGroupLinkViewModel getViewModel() {
        return (ShareableGroupLinkViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        LiveDataUtil.combineLatest(getViewModel().getGroupLink(), getViewModel().getCanEdit(), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return ShareableGroupLinkFragment.$r8$lambda$pHyrGI5QwWkQZvBmI7tlZlFpVL4((GroupLinkUrlAndStatus) obj, ((Boolean) obj2).booleanValue());
            }
        }).observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ ShareableGroupLinkFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ShareableGroupLinkFragment.$r8$lambda$rev4pTCn3ggSb0sOs68UXhWUS0Y(DSLSettingsAdapter.this, this.f$1, (Pair) obj);
            }
        });
        getViewModel().getToasts().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ShareableGroupLinkFragment.$r8$lambda$pyJNMKHWUhwN7AYvcGVHS1E8vqM(ShareableGroupLinkFragment.this, ((Integer) obj).intValue());
            }
        });
        getViewModel().getBusy().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ShareableGroupLinkFragment.$r8$lambda$zDutTikOJUyHb0gjvQ88TmxeazM(ShareableGroupLinkFragment.this, (Boolean) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final Pair m2563bindAdapter$lambda0(GroupLinkUrlAndStatus groupLinkUrlAndStatus, boolean z) {
        Intrinsics.checkNotNullParameter(groupLinkUrlAndStatus, "groupLink");
        return new Pair(groupLinkUrlAndStatus, Boolean.valueOf(z));
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m2564bindAdapter$lambda1(DSLSettingsAdapter dSLSettingsAdapter, ShareableGroupLinkFragment shareableGroupLinkFragment, Pair pair) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(shareableGroupLinkFragment, "this$0");
        dSLSettingsAdapter.submitList(shareableGroupLinkFragment.getConfiguration((GroupLinkUrlAndStatus) pair.component1(), ((Boolean) pair.component2()).booleanValue()).toMappingModelList());
    }

    /* renamed from: bindAdapter$lambda-2 */
    public static final void m2565bindAdapter$lambda2(ShareableGroupLinkFragment shareableGroupLinkFragment, Boolean bool) {
        Intrinsics.checkNotNullParameter(shareableGroupLinkFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(bool, "busy");
        if (!bool.booleanValue()) {
            SimpleProgressDialog.DismissibleDialog dismissibleDialog = shareableGroupLinkFragment.busyDialog;
            if (dismissibleDialog != null) {
                dismissibleDialog.dismiss();
            }
            shareableGroupLinkFragment.busyDialog = null;
        } else if (shareableGroupLinkFragment.busyDialog == null) {
            shareableGroupLinkFragment.busyDialog = SimpleProgressDialog.showDelayed(shareableGroupLinkFragment.requireContext());
        }
    }

    public final void toast(int i) {
        Toast.makeText(requireContext(), getString(i), 0).show();
    }

    private final DSLConfiguration getConfiguration(GroupLinkUrlAndStatus groupLinkUrlAndStatus, boolean z) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(groupLinkUrlAndStatus, this, z) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$getConfiguration$1
            final /* synthetic */ boolean $canEdit;
            final /* synthetic */ GroupLinkUrlAndStatus $groupLink;
            final /* synthetic */ ShareableGroupLinkFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$groupLink = r1;
                this.this$0 = r2;
                this.$canEdit = r3;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                DSLSettingsText dSLSettingsText;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.ShareableGroupLinkDialogFragment__group_link, new DSLSettingsText.Modifier[0]);
                if (this.$groupLink.isEnabled()) {
                    ShareableGroupLinkFragment shareableGroupLinkFragment = this.this$0;
                    String url = this.$groupLink.getUrl();
                    Intrinsics.checkNotNullExpressionValue(url, "groupLink.url");
                    dSLSettingsText = companion.from(ShareableGroupLinkFragment.access$formatForFullWidthWrapping(shareableGroupLinkFragment, url), new DSLSettingsText.Modifier[0]);
                } else {
                    dSLSettingsText = null;
                }
                boolean isEnabled = this.$groupLink.isEnabled();
                boolean z2 = this.$canEdit;
                final ShareableGroupLinkFragment shareableGroupLinkFragment2 = this.this$0;
                dSLConfiguration.switchPref(from, (r16 & 2) != 0 ? null : dSLSettingsText, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? true : z2, isEnabled, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ShareableGroupLinkFragment.access$getViewModel(shareableGroupLinkFragment2).onToggleGroupLink();
                    }
                });
                DSLSettingsText from2 = companion.from(R.string.ShareableGroupLinkDialogFragment__share, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon.Companion companion2 = DSLSettingsIcon.Companion;
                DSLSettingsIcon from$default = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_share_24_tinted, 0, 2, null);
                boolean isEnabled2 = this.$groupLink.isEnabled();
                final ShareableGroupLinkFragment shareableGroupLinkFragment3 = this.this$0;
                dSLConfiguration.clickPref(from2, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? true : isEnabled2, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        GroupLinkBottomSheetDialogFragment.show(shareableGroupLinkFragment3.getChildFragmentManager(), ShareableGroupLinkFragment.access$getGroupId(shareableGroupLinkFragment3));
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from3 = companion.from(R.string.ShareableGroupLinkDialogFragment__reset_link, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from$default2 = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_reset_24_tinted, 0, 2, null);
                boolean z3 = this.$groupLink.isEnabled() && this.$canEdit;
                final ShareableGroupLinkFragment shareableGroupLinkFragment4 = this.this$0;
                dSLConfiguration.clickPref(from3, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default2, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? true : z3, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$getConfiguration$1.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ShareableGroupLinkFragment.access$onResetGroupLink(shareableGroupLinkFragment4);
                    }
                }, (r18 & 64) != 0 ? null : null);
                dSLConfiguration.dividerPref();
                DSLSettingsText from4 = companion.from(R.string.ShareableGroupLinkDialogFragment__approve_new_members, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from5 = companion.from(R.string.ShareableGroupLinkDialogFragment__require_an_admin_to_approve_new_members_joining_via_the_group_link, new DSLSettingsText.Modifier[0]);
                boolean z4 = this.$groupLink.isEnabled() && this.$canEdit;
                boolean isRequiresApproval = this.$groupLink.isRequiresApproval();
                final ShareableGroupLinkFragment shareableGroupLinkFragment5 = this.this$0;
                dSLConfiguration.switchPref(from4, (r16 & 2) != 0 ? null : from5, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? true : z4, isRequiresApproval, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$getConfiguration$1.4
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ShareableGroupLinkFragment.access$getViewModel(shareableGroupLinkFragment5).onToggleApproveMembers();
                    }
                });
            }
        });
    }

    public final void onResetGroupLink() {
        new MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.ShareableGroupLinkDialogFragment__are_you_sure_you_want_to_reset_the_group_link).setPositiveButton(R.string.ShareableGroupLinkDialogFragment__reset_link, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkFragment$$ExternalSyntheticLambda4
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ShareableGroupLinkFragment.m2562$r8$lambda$wD0ThyLC_tB0k2RgEU3Qdhfg(ShareableGroupLinkFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }

    /* renamed from: onResetGroupLink$lambda-3 */
    public static final void m2566onResetGroupLink$lambda3(ShareableGroupLinkFragment shareableGroupLinkFragment, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(shareableGroupLinkFragment, "this$0");
        shareableGroupLinkFragment.getViewModel().onResetLink();
    }

    public final CharSequence formatForFullWidthWrapping(String str) {
        char[] cArr = new char[str.length() * 2];
        int length = str.length();
        for (int i = 0; i < length; i++) {
            int i2 = i * 2;
            cArr[i2] = str.charAt(i);
            cArr[i2 + 1] = 8203;
        }
        return new String(cArr);
    }
}
