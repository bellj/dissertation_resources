package org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.qr;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;

/* loaded from: classes4.dex */
public final class GroupLinkShareQrViewModel extends ViewModel {
    private final LiveData<String> qrData;

    private GroupLinkShareQrViewModel(GroupId.V2 v2) {
        this.qrData = Transformations.map(new LiveGroup(v2).getGroupLink(), new Function() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.qr.GroupLinkShareQrViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((GroupLinkUrlAndStatus) obj).getUrl();
            }
        });
    }

    public LiveData<String> getQrUrl() {
        return this.qrData;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final GroupId.V2 groupId;

        public Factory(GroupId.V2 v2) {
            this.groupId = v2;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new GroupLinkShareQrViewModel(this.groupId));
        }
    }
}
