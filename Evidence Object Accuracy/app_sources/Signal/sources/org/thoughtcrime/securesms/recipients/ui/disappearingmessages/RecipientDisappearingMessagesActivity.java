package org.thoughtcrime.securesms.recipients.ui.disappearingmessages;

import android.content.Context;
import android.content.Intent;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLSettingsActivity;
import org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragmentArgs;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class RecipientDisappearingMessagesActivity extends DSLSettingsActivity {
    public static Intent forRecipient(Context context, RecipientId recipientId) {
        Intent intent = new Intent(context, RecipientDisappearingMessagesActivity.class);
        intent.putExtra(DSLSettingsActivity.ARG_NAV_GRAPH, R.navigation.app_settings_expire_timer).putExtra(DSLSettingsActivity.ARG_START_BUNDLE, new ExpireTimerSettingsFragmentArgs.Builder().setRecipientId(recipientId).build().toBundle());
        return intent;
    }

    public static Intent forCreateGroup(Context context, Integer num) {
        Intent intent = new Intent(context, RecipientDisappearingMessagesActivity.class);
        intent.putExtra(DSLSettingsActivity.ARG_NAV_GRAPH, R.navigation.app_settings_expire_timer).putExtra(DSLSettingsActivity.ARG_START_BUNDLE, new ExpireTimerSettingsFragmentArgs.Builder().setForResultMode(true).setInitialValue(num).build().toBundle());
        return intent;
    }
}
