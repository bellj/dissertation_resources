package org.thoughtcrime.securesms.recipients.ui.sharablegrouplink;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.groups.ui.GroupErrors;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* loaded from: classes4.dex */
public final class ShareableGroupLinkViewModel extends ViewModel {
    private final SingleLiveEvent<Boolean> busy;
    private final LiveData<Boolean> canEdit;
    private final LiveData<GroupLinkUrlAndStatus> groupLink;
    private final ShareableGroupLinkRepository repository;
    private final SingleLiveEvent<Integer> toasts;

    private ShareableGroupLinkViewModel(GroupId.V2 v2, ShareableGroupLinkRepository shareableGroupLinkRepository) {
        LiveGroup liveGroup = new LiveGroup(v2);
        this.repository = shareableGroupLinkRepository;
        this.groupLink = liveGroup.getGroupLink();
        this.canEdit = liveGroup.isSelfAdmin();
        this.toasts = new SingleLiveEvent<>();
        this.busy = new SingleLiveEvent<>();
    }

    public LiveData<GroupLinkUrlAndStatus> getGroupLink() {
        return this.groupLink;
    }

    public LiveData<Integer> getToasts() {
        return this.toasts;
    }

    public LiveData<Boolean> getBusy() {
        return this.busy;
    }

    public LiveData<Boolean> getCanEdit() {
        return this.canEdit;
    }

    public void onToggleGroupLink() {
        this.busy.setValue(Boolean.TRUE);
        this.repository.toggleGroupLinkEnabled(new AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason>() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkViewModel.1
            public void onComplete(Void r2) {
                ShareableGroupLinkViewModel.this.busy.postValue(Boolean.FALSE);
            }

            public void onError(GroupChangeFailureReason groupChangeFailureReason) {
                ShareableGroupLinkViewModel.this.busy.postValue(Boolean.FALSE);
                ShareableGroupLinkViewModel.this.toasts.postValue(Integer.valueOf(GroupErrors.getUserDisplayMessage(groupChangeFailureReason)));
            }
        });
    }

    public void onToggleApproveMembers() {
        this.busy.setValue(Boolean.TRUE);
        this.repository.toggleGroupLinkApprovalRequired(new AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason>() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkViewModel.2
            public void onComplete(Void r2) {
                ShareableGroupLinkViewModel.this.busy.postValue(Boolean.FALSE);
            }

            public void onError(GroupChangeFailureReason groupChangeFailureReason) {
                ShareableGroupLinkViewModel.this.busy.postValue(Boolean.FALSE);
                ShareableGroupLinkViewModel.this.toasts.postValue(Integer.valueOf(GroupErrors.getUserDisplayMessage(groupChangeFailureReason)));
            }
        });
    }

    public void onResetLink() {
        this.busy.setValue(Boolean.TRUE);
        this.repository.cycleGroupLinkPassword(new AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason>() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.ShareableGroupLinkViewModel.3
            public void onComplete(Void r2) {
                ShareableGroupLinkViewModel.this.busy.postValue(Boolean.FALSE);
            }

            public void onError(GroupChangeFailureReason groupChangeFailureReason) {
                ShareableGroupLinkViewModel.this.busy.postValue(Boolean.FALSE);
                ShareableGroupLinkViewModel.this.toasts.postValue(Integer.valueOf(GroupErrors.getUserDisplayMessage(groupChangeFailureReason)));
            }
        });
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final GroupId.V2 groupId;
        private final ShareableGroupLinkRepository repository;

        public Factory(GroupId.V2 v2, ShareableGroupLinkRepository shareableGroupLinkRepository) {
            this.groupId = v2;
            this.repository = shareableGroupLinkRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ShareableGroupLinkViewModel(this.groupId, this.repository));
        }
    }
}
