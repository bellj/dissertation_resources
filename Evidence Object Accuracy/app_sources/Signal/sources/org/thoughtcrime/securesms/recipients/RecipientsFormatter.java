package org.thoughtcrime.securesms.recipients;

import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/* loaded from: classes4.dex */
public class RecipientsFormatter {
    private static String parseBracketedNumber(String str) throws RecipientFormattingException {
        String substring = str.substring(str.indexOf(60) + 1, str.indexOf(62));
        if (PhoneNumberUtils.isWellFormedSmsAddress(substring)) {
            return substring;
        }
        throw new RecipientFormattingException("Bracketed value: " + substring + " is not valid.");
    }

    private static String parseRecipient(String str) throws RecipientFormattingException {
        String trim = str.trim();
        if (trim.indexOf(60) != -1 && trim.indexOf(62) != -1) {
            return parseBracketedNumber(trim);
        }
        if (PhoneNumberUtils.isWellFormedSmsAddress(trim)) {
            return trim;
        }
        throw new RecipientFormattingException("Recipient: " + trim + " is badly formatted.");
    }

    public static List<String> getRecipients(String str) throws RecipientFormattingException {
        ArrayList arrayList = new ArrayList();
        StringTokenizer stringTokenizer = new StringTokenizer(str, ",");
        while (stringTokenizer.hasMoreTokens()) {
            arrayList.add(parseRecipient(stringTokenizer.nextToken()));
        }
        return arrayList;
    }

    public static String formatNameAndNumber(String str, String str2) {
        String formatNumber = PhoneNumberUtils.formatNumber(str2);
        if (TextUtils.isEmpty(str) || str.equals(str2)) {
            return formatNumber;
        }
        return str + " <" + formatNumber + ">";
    }
}
