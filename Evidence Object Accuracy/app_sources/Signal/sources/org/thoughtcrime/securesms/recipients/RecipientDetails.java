package org.thoughtcrime.securesms.recipients;

import android.content.Context;
import android.net.Uri;
import j$.util.Optional;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.ProfileAvatarFileDetails;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public class RecipientDetails {
    final String about;
    final String aboutEmoji;
    final Recipient.Capability announcementGroupCapability;
    final AvatarColor avatarColor;
    final List<Badge> badges;
    final boolean blocked;
    final Uri callRingtone;
    final RecipientDatabase.VibrateState callVibrateState;
    final Recipient.Capability changeNumberCapability;
    final ChatColors chatColors;
    final Uri contactUri;
    final String customLabel;
    final Optional<Integer> defaultSubscriptionId;
    final DistributionListId distributionListId;
    final String e164;
    final String email;
    final int expireMessages;
    final ExpiringProfileKeyCredential expiringProfileKeyCredential;
    final Optional<Recipient.Extras> extras;
    final boolean forceSmsSelection;
    final Recipient.Capability giftBadgesCapability;
    final Optional<Long> groupAvatarId;
    final GroupId groupId;
    final String groupName;
    final Recipient.Capability groupsV1MigrationCapability;
    final boolean hasGroupsInCommon;
    final RecipientDatabase.InsightsBannerTier insightsBannerTier;
    final boolean isReleaseChannel;
    final boolean isSelf;
    final long lastProfileFetch;
    final RecipientDatabase.MentionSetting mentionSetting;
    final Uri messageRingtone;
    final RecipientDatabase.VibrateState messageVibrateState;
    final long mutedUntil;
    final String notificationChannel;
    final List<RecipientId> participantIds;
    final PNI pni;
    final String profileAvatar;
    final ProfileAvatarFileDetails profileAvatarFileDetails;
    final byte[] profileKey;
    final ProfileName profileName;
    final boolean profileSharing;
    final RecipientDatabase.RegisteredState registered;
    final Recipient.Capability senderKeyCapability;
    final ServiceId serviceId;
    final byte[] storageId;
    final Recipient.Capability storiesCapability;
    final boolean systemContact;
    final String systemContactName;
    final Uri systemContactPhoto;
    final ProfileName systemProfileName;
    final RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode;
    final String username;
    final ChatWallpaper wallpaper;

    public RecipientDetails(String str, String str2, Optional<Long> optional, boolean z, boolean z2, RecipientDatabase.RegisteredState registeredState, RecipientRecord recipientRecord, List<RecipientId> list, boolean z3) {
        this.groupAvatarId = optional;
        this.systemContactPhoto = Util.uri(recipientRecord.getSystemContactPhotoUri());
        this.customLabel = recipientRecord.getSystemPhoneLabel();
        this.contactUri = Util.uri(recipientRecord.getSystemContactUri());
        this.serviceId = recipientRecord.getServiceId();
        this.pni = recipientRecord.getPni();
        this.username = recipientRecord.getUsername();
        this.e164 = recipientRecord.getE164();
        this.email = recipientRecord.getEmail();
        this.groupId = recipientRecord.getGroupId();
        this.distributionListId = recipientRecord.getDistributionListId();
        this.messageRingtone = recipientRecord.getMessageRingtone();
        this.callRingtone = recipientRecord.getCallRingtone();
        this.mutedUntil = recipientRecord.getMuteUntil();
        this.messageVibrateState = recipientRecord.getMessageVibrateState();
        this.callVibrateState = recipientRecord.getCallVibrateState();
        this.blocked = recipientRecord.isBlocked();
        this.expireMessages = recipientRecord.getExpireMessages();
        this.participantIds = list == null ? new LinkedList<>() : list;
        this.profileName = recipientRecord.getProfileName();
        this.defaultSubscriptionId = recipientRecord.getDefaultSubscriptionId();
        this.registered = registeredState;
        this.profileKey = recipientRecord.getProfileKey();
        this.expiringProfileKeyCredential = recipientRecord.getExpiringProfileKeyCredential();
        this.profileAvatar = recipientRecord.getProfileAvatar();
        this.profileAvatarFileDetails = recipientRecord.getProfileAvatarFileDetails();
        this.profileSharing = recipientRecord.isProfileSharing();
        this.lastProfileFetch = recipientRecord.getLastProfileFetch();
        this.systemContact = z;
        this.isSelf = z2;
        this.notificationChannel = recipientRecord.getNotificationChannel();
        this.unidentifiedAccessMode = recipientRecord.getUnidentifiedAccessMode();
        this.forceSmsSelection = recipientRecord.isForceSmsSelection();
        this.groupsV1MigrationCapability = recipientRecord.getGroupsV1MigrationCapability();
        this.senderKeyCapability = recipientRecord.getSenderKeyCapability();
        this.announcementGroupCapability = recipientRecord.getAnnouncementGroupCapability();
        this.changeNumberCapability = recipientRecord.getChangeNumberCapability();
        this.storiesCapability = recipientRecord.getStoriesCapability();
        this.giftBadgesCapability = recipientRecord.getGiftBadgesCapability();
        this.insightsBannerTier = recipientRecord.getInsightsBannerTier();
        this.storageId = recipientRecord.getStorageId();
        this.mentionSetting = recipientRecord.getMentionSetting();
        this.wallpaper = recipientRecord.getWallpaper();
        this.chatColors = recipientRecord.getChatColors();
        this.avatarColor = recipientRecord.getAvatarColor();
        this.about = recipientRecord.getAbout();
        this.aboutEmoji = recipientRecord.getAboutEmoji();
        this.systemProfileName = recipientRecord.getSystemProfileName();
        this.groupName = str;
        this.systemContactName = str2;
        this.extras = Optional.ofNullable(recipientRecord.getExtras());
        this.hasGroupsInCommon = recipientRecord.hasGroupsInCommon();
        this.badges = recipientRecord.getBadges();
        this.isReleaseChannel = z3;
    }

    private RecipientDetails() {
        this.groupAvatarId = null;
        this.systemContactPhoto = null;
        this.customLabel = null;
        this.contactUri = null;
        this.serviceId = null;
        this.pni = null;
        this.username = null;
        this.e164 = null;
        this.email = null;
        this.groupId = null;
        this.distributionListId = null;
        this.messageRingtone = null;
        this.callRingtone = null;
        this.mutedUntil = 0;
        RecipientDatabase.VibrateState vibrateState = RecipientDatabase.VibrateState.DEFAULT;
        this.messageVibrateState = vibrateState;
        this.callVibrateState = vibrateState;
        this.blocked = false;
        this.expireMessages = 0;
        this.participantIds = new LinkedList();
        ProfileName profileName = ProfileName.EMPTY;
        this.profileName = profileName;
        this.insightsBannerTier = RecipientDatabase.InsightsBannerTier.TIER_TWO;
        this.defaultSubscriptionId = Optional.empty();
        this.registered = RecipientDatabase.RegisteredState.UNKNOWN;
        this.profileKey = null;
        this.expiringProfileKeyCredential = null;
        this.profileAvatar = null;
        this.profileAvatarFileDetails = ProfileAvatarFileDetails.NO_DETAILS;
        this.profileSharing = false;
        this.lastProfileFetch = 0;
        this.systemContact = true;
        this.isSelf = false;
        this.notificationChannel = null;
        this.unidentifiedAccessMode = RecipientDatabase.UnidentifiedAccessMode.UNKNOWN;
        this.forceSmsSelection = false;
        this.groupName = null;
        Recipient.Capability capability = Recipient.Capability.UNKNOWN;
        this.groupsV1MigrationCapability = capability;
        this.senderKeyCapability = capability;
        this.announcementGroupCapability = capability;
        this.changeNumberCapability = capability;
        this.storiesCapability = capability;
        this.giftBadgesCapability = capability;
        this.storageId = null;
        this.mentionSetting = RecipientDatabase.MentionSetting.ALWAYS_NOTIFY;
        this.wallpaper = null;
        this.chatColors = null;
        this.avatarColor = AvatarColor.UNKNOWN;
        this.about = null;
        this.aboutEmoji = null;
        this.systemProfileName = profileName;
        this.systemContactName = null;
        this.extras = Optional.empty();
        this.hasGroupsInCommon = false;
        this.badges = Collections.emptyList();
        this.isReleaseChannel = false;
    }

    public static RecipientDetails forIndividual(Context context, RecipientRecord recipientRecord) {
        RecipientDatabase.RegisteredState registeredState;
        RecipientDatabase.RegisteredState registeredState2;
        boolean z = !recipientRecord.getSystemProfileName().isEmpty();
        boolean z2 = (recipientRecord.getE164() != null && recipientRecord.getE164().equals(SignalStore.account().getE164())) || (recipientRecord.getServiceId() != null && recipientRecord.getServiceId().equals(SignalStore.account().getAci()));
        boolean equals = recipientRecord.getId().equals(SignalStore.releaseChannelValues().getReleaseChannelRecipientId());
        RecipientDatabase.RegisteredState registered = recipientRecord.getRegistered();
        if (z2) {
            if (!SignalStore.account().isRegistered() || TextSecurePreferences.isUnauthorizedRecieved(context)) {
                registeredState2 = RecipientDatabase.RegisteredState.NOT_REGISTERED;
            } else {
                registeredState2 = RecipientDatabase.RegisteredState.REGISTERED;
            }
            registeredState = registeredState2;
        } else {
            registeredState = registered;
        }
        return new RecipientDetails(null, recipientRecord.getSystemDisplayName(), Optional.empty(), z, z2, registeredState, recipientRecord, null, equals);
    }

    public static RecipientDetails forDistributionList(String str, List<RecipientId> list, RecipientRecord recipientRecord) {
        return new RecipientDetails(str, null, Optional.empty(), false, false, recipientRecord.getRegistered(), recipientRecord, list, false);
    }

    public static RecipientDetails forUnknown() {
        return new RecipientDetails();
    }
}
