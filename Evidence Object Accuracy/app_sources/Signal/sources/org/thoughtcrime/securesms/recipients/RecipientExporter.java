package org.thoughtcrime.securesms.recipients;

import android.content.Intent;
import android.text.TextUtils;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* loaded from: classes4.dex */
public final class RecipientExporter {
    private final Recipient recipient;

    public static RecipientExporter export(Recipient recipient) {
        return new RecipientExporter(recipient);
    }

    private RecipientExporter(Recipient recipient) {
        this.recipient = recipient;
    }

    public Intent asAddContactIntent() {
        Intent intent = new Intent("android.intent.action.INSERT_OR_EDIT");
        intent.setType("vnd.android.cursor.item/contact");
        addNameToIntent(intent, this.recipient.getProfileName().toString());
        addAddressToIntent(intent, this.recipient);
        return intent;
    }

    private static void addNameToIntent(Intent intent, String str) {
        if (!TextUtils.isEmpty(str)) {
            intent.putExtra("name", str);
        }
    }

    private static void addAddressToIntent(Intent intent, Recipient recipient) {
        if (recipient.getE164().isPresent()) {
            intent.putExtra(RecipientDatabase.PHONE, recipient.requireE164());
        } else if (recipient.getEmail().isPresent()) {
            intent.putExtra(RecipientDatabase.EMAIL, recipient.requireEmail());
        }
    }
}
