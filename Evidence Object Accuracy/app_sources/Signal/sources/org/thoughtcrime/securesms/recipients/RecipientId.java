package org.thoughtcrime.securesms.recipients;

import android.os.Parcel;
import android.os.Parcelable;
import com.annimon.stream.Stream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import org.signal.core.util.DatabaseId;
import org.signal.core.util.LongSerializer;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda5;
import org.thoughtcrime.securesms.util.DelimiterUtil;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes.dex */
public class RecipientId implements Parcelable, Comparable<RecipientId>, DatabaseId {
    public static final Parcelable.Creator<RecipientId> CREATOR = new Parcelable.Creator<RecipientId>() { // from class: org.thoughtcrime.securesms.recipients.RecipientId.1
        @Override // android.os.Parcelable.Creator
        public RecipientId createFromParcel(Parcel parcel) {
            return new RecipientId(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public RecipientId[] newArray(int i) {
            return new RecipientId[i];
        }
    };
    private static final char DELIMITER;
    public static final LongSerializer<RecipientId> SERIALIZER = new Serializer();
    public static final RecipientId UNKNOWN = from(-1);
    private static final long UNKNOWN_ID;
    private final long id;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public static RecipientId from(long j) {
        if (j != 0) {
            return new RecipientId(j);
        }
        throw new InvalidLongRecipientIdError();
    }

    public static RecipientId from(String str) {
        try {
            return from(Long.parseLong(str));
        } catch (NumberFormatException unused) {
            throw new InvalidStringRecipientIdError();
        }
    }

    public static RecipientId fromNullable(String str) {
        if (str != null) {
            return from(str);
        }
        return null;
    }

    public static RecipientId from(SignalServiceAddress signalServiceAddress) {
        return from(signalServiceAddress.getServiceId(), signalServiceAddress.getNumber().orElse(null));
    }

    public static RecipientId from(ServiceId serviceId) {
        return from(serviceId, null);
    }

    public static RecipientId fromE164(String str) {
        return from(null, str);
    }

    public static RecipientId fromSidOrE164(String str) {
        if (UuidUtil.isUuid(str)) {
            return from(ServiceId.parseOrThrow(str));
        }
        return from(null, str);
    }

    private static RecipientId from(ServiceId serviceId, String str) {
        if (serviceId != null && serviceId.isUnknown()) {
            return UNKNOWN;
        }
        RecipientIdCache recipientIdCache = RecipientIdCache.INSTANCE;
        RecipientId recipientId = recipientIdCache.get(serviceId, str);
        if (recipientId != null) {
            return recipientId;
        }
        Recipient externalPush = Recipient.externalPush(serviceId, str);
        recipientIdCache.put(externalPush);
        return externalPush.getId();
    }

    public static void clearCache() {
        RecipientIdCache.INSTANCE.clear();
    }

    private RecipientId(long j) {
        this.id = j;
    }

    private RecipientId(Parcel parcel) {
        this.id = parcel.readLong();
    }

    public static String toSerializedList(Collection<RecipientId> collection) {
        return Util.join((Collection) Stream.of(collection).map(new SafetyNumberChangeDialog$$ExternalSyntheticLambda5()).toList(), String.valueOf((char) DELIMITER));
    }

    public static List<RecipientId> fromSerializedList(String str) {
        String[] split = DelimiterUtil.split(str, DELIMITER);
        ArrayList arrayList = new ArrayList(split.length);
        for (String str2 : split) {
            arrayList.add(from(Long.parseLong(str2)));
        }
        return arrayList;
    }

    public static boolean serializedListContains(String str, RecipientId recipientId) {
        return Pattern.compile("\\b" + recipientId.serialize() + "\\b").matcher(str).find();
    }

    public boolean isUnknown() {
        return this.id == -1;
    }

    @Override // org.signal.core.util.DatabaseId
    public String serialize() {
        return String.valueOf(this.id);
    }

    public long toLong() {
        return this.id;
    }

    public String toQueueKey() {
        return toQueueKey(false);
    }

    public String toQueueKey(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("RecipientId::");
        sb.append(this.id);
        sb.append(z ? "::MEDIA" : "");
        return sb.toString();
    }

    @Override // java.lang.Object
    public String toString() {
        return "RecipientId::" + this.id;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.id == ((RecipientId) obj).id) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        long j = this.id;
        return (int) (j ^ (j >>> 32));
    }

    public int compareTo(RecipientId recipientId) {
        return Long.compare(this.id, recipientId.id);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
    }

    /* loaded from: classes4.dex */
    public static class InvalidLongRecipientIdError extends AssertionError {
        private InvalidLongRecipientIdError() {
        }
    }

    /* loaded from: classes4.dex */
    public static class InvalidStringRecipientIdError extends AssertionError {
        private InvalidStringRecipientIdError() {
        }
    }

    /* loaded from: classes4.dex */
    private static class Serializer implements LongSerializer<RecipientId> {
        private Serializer() {
        }

        public Long serialize(RecipientId recipientId) {
            return Long.valueOf(recipientId.toLong());
        }

        public RecipientId deserialize(Long l) {
            return RecipientId.from(l.longValue());
        }
    }
}
