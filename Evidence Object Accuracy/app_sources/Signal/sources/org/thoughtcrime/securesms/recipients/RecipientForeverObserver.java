package org.thoughtcrime.securesms.recipients;

/* loaded from: classes4.dex */
public interface RecipientForeverObserver {
    void onRecipientChanged(Recipient recipient);
}
