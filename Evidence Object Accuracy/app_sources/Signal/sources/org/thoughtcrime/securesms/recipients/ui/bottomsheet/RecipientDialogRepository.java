package org.thoughtcrime.securesms.recipients.ui.bottomsheet;

import android.content.Context;
import androidx.core.util.Consumer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository;

/* loaded from: classes4.dex */
public final class RecipientDialogRepository {
    private static final String TAG = Log.tag(RecipientDialogRepository.class);
    private final Context context;
    private final GroupId groupId;
    private final RecipientId recipientId;

    /* loaded from: classes4.dex */
    public interface RecipientCallback {
        void onRecipient(Recipient recipient);
    }

    public RecipientDialogRepository(Context context, RecipientId recipientId, GroupId groupId) {
        this.context = context;
        this.recipientId = recipientId;
        this.groupId = groupId;
    }

    public RecipientId getRecipientId() {
        return this.recipientId;
    }

    public GroupId getGroupId() {
        return this.groupId;
    }

    public void getIdentity(Consumer<IdentityRecord> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable(consumer) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Consumer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                RecipientDialogRepository.m2551$r8$lambda$KOE4MWwZMMdz5R0C4qMqIWScOs(RecipientDialogRepository.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$getIdentity$0(Consumer consumer) {
        consumer.accept(ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecord(this.recipientId).orElse(null));
    }

    public void getRecipient(RecipientCallback recipientCallback) {
        ExecutorService executorService = SignalExecutors.BOUNDED;
        RecipientDialogRepository$$ExternalSyntheticLambda3 recipientDialogRepository$$ExternalSyntheticLambda3 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository$$ExternalSyntheticLambda3
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return RecipientDialogRepository.$r8$lambda$7o0xWpHGYRA1cdrGkYyyLLMdDT0(RecipientDialogRepository.this);
            }
        };
        Objects.requireNonNull(recipientCallback);
        SimpleTask.run(executorService, recipientDialogRepository$$ExternalSyntheticLambda3, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository$$ExternalSyntheticLambda4
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                RecipientDialogRepository.RecipientCallback.this.onRecipient((Recipient) obj);
            }
        });
    }

    public /* synthetic */ Recipient lambda$getRecipient$1() {
        return Recipient.resolved(this.recipientId);
    }

    public void refreshRecipient() {
        SignalExecutors.UNBOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository$$ExternalSyntheticLambda9
            @Override // java.lang.Runnable
            public final void run() {
                RecipientDialogRepository.$r8$lambda$6DrscIO3s9YXhtfmGeZlzv73Umc(RecipientDialogRepository.this);
            }
        });
    }

    public /* synthetic */ void lambda$refreshRecipient$2() {
        try {
            ContactDiscovery.refresh(this.context, Recipient.resolved(this.recipientId), false);
        } catch (IOException unused) {
            Log.w(TAG, "Failed to refresh user after adding to contacts.");
        }
    }

    public void removeMember(Consumer<Boolean> consumer, GroupChangeErrorCallback groupChangeErrorCallback) {
        ExecutorService executorService = SignalExecutors.UNBOUNDED;
        RecipientDialogRepository$$ExternalSyntheticLambda8 recipientDialogRepository$$ExternalSyntheticLambda8 = new SimpleTask.BackgroundTask(groupChangeErrorCallback) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ GroupChangeErrorCallback f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return RecipientDialogRepository.m2550$r8$lambda$CxpFiTrOj_Kjxmf7ANf3XoBgjc(RecipientDialogRepository.this, this.f$1);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(executorService, recipientDialogRepository$$ExternalSyntheticLambda8, new RecipientDialogRepository$$ExternalSyntheticLambda6(consumer));
    }

    public /* synthetic */ Boolean lambda$removeMember$3(GroupChangeErrorCallback groupChangeErrorCallback) {
        try {
            Context context = this.context;
            GroupId groupId = this.groupId;
            Objects.requireNonNull(groupId);
            GroupManager.ejectAndBanFromGroup(context, groupId.requireV2(), Recipient.resolved(this.recipientId));
            return Boolean.TRUE;
        } catch (IOException | GroupChangeException e) {
            Log.w(TAG, e);
            groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e));
            return Boolean.FALSE;
        }
    }

    public void setMemberAdmin(boolean z, Consumer<Boolean> consumer, GroupChangeErrorCallback groupChangeErrorCallback) {
        ExecutorService executorService = SignalExecutors.UNBOUNDED;
        RecipientDialogRepository$$ExternalSyntheticLambda5 recipientDialogRepository$$ExternalSyntheticLambda5 = new SimpleTask.BackgroundTask(z, groupChangeErrorCallback) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ boolean f$1;
            public final /* synthetic */ GroupChangeErrorCallback f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return RecipientDialogRepository.$r8$lambda$vMOoePjCw7vvWz9DwJ6rsaWSfUY(RecipientDialogRepository.this, this.f$1, this.f$2);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(executorService, recipientDialogRepository$$ExternalSyntheticLambda5, new RecipientDialogRepository$$ExternalSyntheticLambda6(consumer));
    }

    public /* synthetic */ Boolean lambda$setMemberAdmin$4(boolean z, GroupChangeErrorCallback groupChangeErrorCallback) {
        try {
            Context context = this.context;
            GroupId groupId = this.groupId;
            Objects.requireNonNull(groupId);
            GroupManager.setMemberAdmin(context, groupId.requireV2(), this.recipientId, z);
            return Boolean.TRUE;
        } catch (IOException | GroupChangeException e) {
            Log.w(TAG, e);
            groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e));
            return Boolean.FALSE;
        }
    }

    public void getGroupMembership(Consumer<List<RecipientId>> consumer) {
        ExecutorService executorService = SignalExecutors.UNBOUNDED;
        RecipientDialogRepository$$ExternalSyntheticLambda1 recipientDialogRepository$$ExternalSyntheticLambda1 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return RecipientDialogRepository.m2552$r8$lambda$vjT0OrlCkgXqk1jFw7QGgTuAI(RecipientDialogRepository.this);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(executorService, recipientDialogRepository$$ExternalSyntheticLambda1, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository$$ExternalSyntheticLambda2
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                Consumer.this.accept((ArrayList) obj);
            }
        });
    }

    public /* synthetic */ ArrayList lambda$getGroupMembership$5() {
        List<GroupDatabase.GroupRecord> pushGroupsContainingMember = SignalDatabase.groups().getPushGroupsContainingMember(this.recipientId);
        ArrayList arrayList = new ArrayList(pushGroupsContainingMember.size());
        for (GroupDatabase.GroupRecord groupRecord : pushGroupsContainingMember) {
            arrayList.add(groupRecord.getRecipientId());
        }
        return arrayList;
    }

    public static /* synthetic */ void lambda$getActiveGroupCount$6(Consumer consumer) {
        consumer.accept(Integer.valueOf(SignalDatabase.groups().getActiveGroupCount()));
    }

    public void getActiveGroupCount(Consumer<Integer> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogRepository$$ExternalSyntheticLambda7
            @Override // java.lang.Runnable
            public final void run() {
                RecipientDialogRepository.$r8$lambda$zqlPL2irXT7YcVH9UCOv9dh9rRY(Consumer.this);
            }
        });
    }
}
