package org.thoughtcrime.securesms.recipients;

import j$.util.Optional;
import java.util.LinkedHashMap;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public final class RecipientIdCache {
    static final RecipientIdCache INSTANCE = new RecipientIdCache(1000);
    private static final int INSTANCE_CACHE_LIMIT;
    private static final String TAG = Log.tag(RecipientIdCache.class);
    private final Map<Object, RecipientId> ids;

    RecipientIdCache(final int i) {
        this.ids = new LinkedHashMap<Object, RecipientId>(128, 0.75f, true) { // from class: org.thoughtcrime.securesms.recipients.RecipientIdCache.1
            @Override // java.util.LinkedHashMap
            protected boolean removeEldestEntry(Map.Entry<Object, RecipientId> entry) {
                return size() > i;
            }
        };
    }

    public synchronized void put(Recipient recipient) {
        RecipientId id = recipient.getId();
        Optional<String> e164 = recipient.getE164();
        Optional<ServiceId> serviceId = recipient.getServiceId();
        if (e164.isPresent()) {
            this.ids.put(e164.get(), id);
        }
        if (serviceId.isPresent()) {
            this.ids.put(serviceId.get(), id);
        }
    }

    public synchronized RecipientId get(ServiceId serviceId, String str) {
        if (serviceId != null && str != null) {
            RecipientId recipientId = this.ids.get(serviceId);
            if (recipientId == null) {
                return null;
            }
            RecipientId recipientId2 = this.ids.get(str);
            if (recipientId2 == null) {
                return null;
            }
            if (recipientId.equals(recipientId2)) {
                return recipientId;
            }
            this.ids.remove(serviceId);
            this.ids.remove(str);
            Log.w(TAG, "Seen invalid RecipientIdCacheState");
            return null;
        } else if (serviceId != null) {
            return this.ids.get(serviceId);
        } else if (str == null) {
            return null;
        } else {
            return this.ids.get(str);
        }
    }

    public synchronized void clear() {
        this.ids.clear();
    }
}
