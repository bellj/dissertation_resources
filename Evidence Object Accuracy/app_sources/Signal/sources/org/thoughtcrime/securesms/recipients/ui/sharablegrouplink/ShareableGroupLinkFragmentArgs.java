package org.thoughtcrime.securesms.recipients.ui.sharablegrouplink;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class ShareableGroupLinkFragmentArgs {
    private final HashMap arguments;

    private ShareableGroupLinkFragmentArgs() {
        this.arguments = new HashMap();
    }

    private ShareableGroupLinkFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static ShareableGroupLinkFragmentArgs fromBundle(Bundle bundle) {
        ShareableGroupLinkFragmentArgs shareableGroupLinkFragmentArgs = new ShareableGroupLinkFragmentArgs();
        bundle.setClassLoader(ShareableGroupLinkFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("group_id")) {
            String string = bundle.getString("group_id");
            if (string != null) {
                shareableGroupLinkFragmentArgs.arguments.put("group_id", string);
                return shareableGroupLinkFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"group_id\" is marked as non-null but was passed a null value.");
        }
        throw new IllegalArgumentException("Required argument \"group_id\" is missing and does not have an android:defaultValue");
    }

    public String getGroupId() {
        return (String) this.arguments.get("group_id");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("group_id")) {
            bundle.putString("group_id", (String) this.arguments.get("group_id"));
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ShareableGroupLinkFragmentArgs shareableGroupLinkFragmentArgs = (ShareableGroupLinkFragmentArgs) obj;
        if (this.arguments.containsKey("group_id") != shareableGroupLinkFragmentArgs.arguments.containsKey("group_id")) {
            return false;
        }
        return getGroupId() == null ? shareableGroupLinkFragmentArgs.getGroupId() == null : getGroupId().equals(shareableGroupLinkFragmentArgs.getGroupId());
    }

    public int hashCode() {
        return 31 + (getGroupId() != null ? getGroupId().hashCode() : 0);
    }

    public String toString() {
        return "ShareableGroupLinkFragmentArgs{groupId=" + getGroupId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(ShareableGroupLinkFragmentArgs shareableGroupLinkFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(shareableGroupLinkFragmentArgs.arguments);
        }

        public Builder(String str) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (str != null) {
                hashMap.put("group_id", str);
                return;
            }
            throw new IllegalArgumentException("Argument \"group_id\" is marked as non-null but was passed a null value.");
        }

        public ShareableGroupLinkFragmentArgs build() {
            return new ShareableGroupLinkFragmentArgs(this.arguments);
        }

        public Builder setGroupId(String str) {
            if (str != null) {
                this.arguments.put("group_id", str);
                return this;
            }
            throw new IllegalArgumentException("Argument \"group_id\" is marked as non-null but was passed a null value.");
        }

        public String getGroupId() {
            return (String) this.arguments.get("group_id");
        }
    }
}
