package org.thoughtcrime.securesms.recipients.ui.bottomsheet;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import java.util.Objects;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.view.AvatarView;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment;
import org.thoughtcrime.securesms.blocked.BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.FallbackPhoto80dp;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientExporter;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientDialogViewModel;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.DrawableUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class RecipientBottomSheetDialogFragment extends BottomSheetDialogFragment {
    private static final String ARGS_GROUP_ID;
    private static final String ARGS_RECIPIENT_ID;
    public static final int REQUEST_CODE_SYSTEM_CONTACT_SHEET;
    public static final String TAG = Log.tag(RecipientBottomSheetDialogFragment.class);
    private TextView about;
    private TextView addContactButton;
    private TextView addToGroupButton;
    private ProgressBar adminActionBusy;
    private AvatarView avatar;
    private BadgeImageView badgeImageView;
    private TextView blockButton;
    private View buttonStrip;
    private TextView contactDetailsButton;
    private TextView fullName;
    private View interactionsContainer;
    private TextView makeGroupAdminButton;
    private View noteToSelfDescription;
    private TextView removeAdminButton;
    private TextView removeFromGroupButton;
    private TextView unblockButton;
    private TextView usernameNumber;
    private RecipientDialogViewModel viewModel;
    private TextView viewSafetyNumberButton;

    public static BottomSheetDialogFragment create(RecipientId recipientId, GroupId groupId) {
        Bundle bundle = new Bundle();
        RecipientBottomSheetDialogFragment recipientBottomSheetDialogFragment = new RecipientBottomSheetDialogFragment();
        bundle.putString(ARGS_RECIPIENT_ID, recipientId.serialize());
        if (groupId != null) {
            bundle.putString(ARGS_GROUP_ID, groupId.toString());
        }
        recipientBottomSheetDialogFragment.setArguments(bundle);
        return recipientBottomSheetDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, ThemeUtil.isDarkTheme(requireContext()) ? R.style.Theme_Signal_RoundedBottomSheet : R.style.Theme_Signal_RoundedBottomSheet_Light);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.recipient_bottom_sheet, viewGroup, false);
        this.avatar = (AvatarView) inflate.findViewById(R.id.rbs_recipient_avatar);
        this.fullName = (TextView) inflate.findViewById(R.id.rbs_full_name);
        this.about = (TextView) inflate.findViewById(R.id.rbs_about);
        this.usernameNumber = (TextView) inflate.findViewById(R.id.rbs_username_number);
        this.blockButton = (TextView) inflate.findViewById(R.id.rbs_block_button);
        this.unblockButton = (TextView) inflate.findViewById(R.id.rbs_unblock_button);
        this.addContactButton = (TextView) inflate.findViewById(R.id.rbs_add_contact_button);
        this.contactDetailsButton = (TextView) inflate.findViewById(R.id.rbs_contact_details_button);
        this.addToGroupButton = (TextView) inflate.findViewById(R.id.rbs_add_to_group_button);
        this.viewSafetyNumberButton = (TextView) inflate.findViewById(R.id.rbs_view_safety_number_button);
        this.makeGroupAdminButton = (TextView) inflate.findViewById(R.id.rbs_make_group_admin_button);
        this.removeAdminButton = (TextView) inflate.findViewById(R.id.rbs_remove_group_admin_button);
        this.removeFromGroupButton = (TextView) inflate.findViewById(R.id.rbs_remove_from_group_button);
        this.adminActionBusy = (ProgressBar) inflate.findViewById(R.id.rbs_admin_action_busy);
        this.noteToSelfDescription = inflate.findViewById(R.id.rbs_note_to_self_description);
        this.buttonStrip = inflate.findViewById(R.id.button_strip);
        this.interactionsContainer = inflate.findViewById(R.id.interactions_container);
        this.badgeImageView = (BadgeImageView) inflate.findViewById(R.id.rbs_badge);
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        Bundle requireArguments = requireArguments();
        String string = requireArguments.getString(ARGS_RECIPIENT_ID);
        Objects.requireNonNull(string);
        RecipientId from = RecipientId.from(string);
        GroupId parseNullableOrThrow = GroupId.parseNullableOrThrow(requireArguments.getString(ARGS_GROUP_ID));
        RecipientDialogViewModel recipientDialogViewModel = (RecipientDialogViewModel) new ViewModelProvider(this, new RecipientDialogViewModel.Factory(requireContext().getApplicationContext(), from, parseNullableOrThrow)).get(RecipientDialogViewModel.class);
        this.viewModel = recipientDialogViewModel;
        recipientDialogViewModel.getStoryViewState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda9
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$0((StoryViewState) obj);
            }
        });
        this.viewModel.getRecipient().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda13
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$10((Recipient) obj);
            }
        });
        this.viewModel.getCanAddToAGroup().observe(getViewLifecycleOwner(), new Observer(parseNullableOrThrow) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda14
            public final /* synthetic */ GroupId f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$11(this.f$1, (Boolean) obj);
            }
        });
        this.viewModel.getAdminActionStatus().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda15
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$13((RecipientDialogViewModel.AdminActionStatus) obj);
            }
        });
        this.viewModel.getIdentity().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda16
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$15((IdentityRecord) obj);
            }
        });
        this.avatar.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda17
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$16(view2);
            }
        });
        this.badgeImageView.setOnClickListener(new View.OnClickListener(from) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda18
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$17(this.f$1, view2);
            }
        });
        this.blockButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda19
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$18(view2);
            }
        });
        this.unblockButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda20
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$19(view2);
            }
        });
        this.makeGroupAdminButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda21
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$20(view2);
            }
        });
        this.removeAdminButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$21(view2);
            }
        });
        this.addToGroupButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$22(view2);
            }
        });
        this.viewModel.getAdminActionBusy().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda12
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$23((Boolean) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(StoryViewState storyViewState) {
        this.avatar.setStoryRingFromState(storyViewState);
    }

    public /* synthetic */ void lambda$onViewCreated$10(final Recipient recipient) {
        String str;
        this.interactionsContainer.setVisibility(recipient.isSelf() ? 8 : 0);
        this.avatar.setFallbackPhotoProvider(new Recipient.FallbackPhotoProvider() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment.1
            @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
            public FallbackContactPhoto getPhotoForLocalNumber() {
                return new FallbackPhoto80dp(R.drawable.ic_note_80, recipient.getAvatarColor());
            }
        });
        this.avatar.displayChatAvatar(recipient);
        if (!recipient.isSelf()) {
            this.badgeImageView.setBadgeFromRecipient(recipient);
        }
        if (recipient.isSelf()) {
            this.avatar.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$1(view);
                }
            });
        }
        if (recipient.isSelf()) {
            str = requireContext().getString(R.string.note_to_self);
        } else {
            str = recipient.getDisplayName(requireContext());
        }
        this.fullName.setVisibility(TextUtils.isEmpty(str) ? 8 : 0);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        if (recipient.isSystemContact() && !recipient.isSelf()) {
            SpanUtil.appendCenteredImageSpan(spannableStringBuilder, DrawableUtil.tint(ContextUtil.requireDrawable(requireContext(), R.drawable.ic_profile_circle_outline_16), ContextCompat.getColor(requireContext(), R.color.signal_text_primary)), 16, 16);
        } else if (recipient.showVerified()) {
            SpanUtil.appendCenteredImageSpan(spannableStringBuilder, ContextUtil.requireDrawable(requireContext(), R.drawable.ic_official_28), 28, 28);
        }
        this.fullName.setText(spannableStringBuilder);
        String combinedAboutAndEmoji = recipient.getCombinedAboutAndEmoji();
        if (recipient.isReleaseNotes()) {
            combinedAboutAndEmoji = getString(R.string.ReleaseNotes__signal_release_notes_and_news);
        }
        if (!Util.isEmpty(combinedAboutAndEmoji)) {
            this.about.setText(combinedAboutAndEmoji);
            this.about.setVisibility(0);
        } else {
            this.about.setVisibility(8);
        }
        String str2 = "";
        if (recipient.hasAUserSetDisplayName(requireContext()) && !recipient.isSelf()) {
            str2 = ((String) recipient.getSmsAddress().map(new BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda1()).orElse(str2)).trim();
        }
        this.usernameNumber.setText(str2);
        this.usernameNumber.setVisibility(TextUtils.isEmpty(str2) ? 8 : 0);
        this.usernameNumber.setOnLongClickListener(new View.OnLongClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                return RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$2(view);
            }
        });
        this.noteToSelfDescription.setVisibility(recipient.isSelf() ? 0 : 8);
        if (RecipientUtil.isBlockable(recipient)) {
            boolean isBlocked = recipient.isBlocked();
            this.blockButton.setVisibility((recipient.isSelf() || isBlocked) ? 8 : 0);
            this.unblockButton.setVisibility((recipient.isSelf() || !isBlocked) ? 8 : 0);
        } else {
            this.blockButton.setVisibility(8);
            this.unblockButton.setVisibility(8);
        }
        ButtonStripPreference.State state = new ButtonStripPreference.State(!recipient.isBlocked() && !recipient.isSelf() && !recipient.isReleaseNotes(), !recipient.isBlocked() && !recipient.isSelf() && recipient.isRegistered(), !recipient.isBlocked() && !recipient.isSelf() && !recipient.isReleaseNotes(), false, false, recipient.isRegistered(), false);
        new ButtonStripPreference.ViewHolder(this.buttonStrip).bind(new ButtonStripPreference.Model(state, DSLSettingsIcon.from(ContextUtil.requireDrawable(requireContext(), R.drawable.selectable_recipient_bottom_sheet_icon_button)), new Function0() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda2
            @Override // kotlin.jvm.functions.Function0
            public final Object invoke() {
                return RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$3();
            }
        }, new Function0() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda3
            @Override // kotlin.jvm.functions.Function0
            public final Object invoke() {
                return RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$4();
            }
        }, new Function0(state) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ ButtonStripPreference.State f$1;

            {
                this.f$1 = r2;
            }

            @Override // kotlin.jvm.functions.Function0
            public final Object invoke() {
                return RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$5(this.f$1);
            }
        }, new Function0() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda5
            @Override // kotlin.jvm.functions.Function0
            public final Object invoke() {
                return Unit.INSTANCE;
            }
        }, new Function0() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda6
            @Override // kotlin.jvm.functions.Function0
            public final Object invoke() {
                return Unit.INSTANCE;
            }
        }));
        if (recipient.isReleaseNotes()) {
            this.buttonStrip.setVisibility(8);
        }
        if (recipient.isSystemContact() || recipient.isGroup() || recipient.isSelf() || recipient.isBlocked() || recipient.isReleaseNotes()) {
            this.addContactButton.setVisibility(8);
        } else {
            this.addContactButton.setVisibility(0);
            this.addContactButton.setOnClickListener(new View.OnClickListener(recipient) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda7
                public final /* synthetic */ Recipient f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$8(this.f$1, view);
                }
            });
        }
        if (!recipient.isSystemContact() || recipient.isGroup() || recipient.isSelf()) {
            this.contactDetailsButton.setVisibility(8);
            return;
        }
        this.contactDetailsButton.setVisibility(0);
        this.contactDetailsButton.setOnClickListener(new View.OnClickListener(recipient) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda8
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$9(this.f$1, view);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        dismiss();
        this.viewModel.onNoteToSelfClicked(requireActivity());
    }

    public /* synthetic */ boolean lambda$onViewCreated$2(View view) {
        Util.copyToClipboard(view.getContext(), this.usernameNumber.getText().toString());
        ServiceUtil.getVibrator(view.getContext()).vibrate(250);
        Toast.makeText(view.getContext(), (int) R.string.RecipientBottomSheet_copied_to_clipboard, 0).show();
        return true;
    }

    public /* synthetic */ Unit lambda$onViewCreated$3() {
        dismiss();
        this.viewModel.onMessageClicked(requireActivity());
        return Unit.INSTANCE;
    }

    public /* synthetic */ Unit lambda$onViewCreated$4() {
        this.viewModel.onSecureVideoCallClicked(requireActivity());
        return Unit.INSTANCE;
    }

    public /* synthetic */ Unit lambda$onViewCreated$5(ButtonStripPreference.State state) {
        if (state.isAudioSecure()) {
            this.viewModel.onSecureCallClicked(requireActivity());
        } else {
            this.viewModel.onInsecureCallClicked(requireActivity());
        }
        return Unit.INSTANCE;
    }

    public /* synthetic */ void lambda$onViewCreated$8(Recipient recipient, View view) {
        openSystemContactSheet(RecipientExporter.export(recipient).asAddContactIntent());
    }

    public /* synthetic */ void lambda$onViewCreated$9(Recipient recipient, View view) {
        openSystemContactSheet(new Intent("android.intent.action.VIEW", recipient.getContactUri()));
    }

    public /* synthetic */ void lambda$onViewCreated$11(GroupId groupId, Boolean bool) {
        this.addToGroupButton.setText(groupId == null ? R.string.RecipientBottomSheet_add_to_a_group : R.string.RecipientBottomSheet_add_to_another_group);
        this.addToGroupButton.setVisibility(bool.booleanValue() ? 0 : 8);
    }

    public /* synthetic */ void lambda$onViewCreated$13(RecipientDialogViewModel.AdminActionStatus adminActionStatus) {
        int i = 0;
        this.makeGroupAdminButton.setVisibility(adminActionStatus.isCanMakeAdmin() ? 0 : 8);
        this.removeAdminButton.setVisibility(adminActionStatus.isCanMakeNonAdmin() ? 0 : 8);
        TextView textView = this.removeFromGroupButton;
        if (!adminActionStatus.isCanRemove()) {
            i = 8;
        }
        textView.setVisibility(i);
        if (adminActionStatus.isCanRemove()) {
            this.removeFromGroupButton.setOnClickListener(new View.OnClickListener(adminActionStatus) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda24
                public final /* synthetic */ RecipientDialogViewModel.AdminActionStatus f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$12(this.f$1, view);
                }
            });
        }
    }

    public /* synthetic */ void lambda$onViewCreated$12(RecipientDialogViewModel.AdminActionStatus adminActionStatus, View view) {
        this.viewModel.onRemoveFromGroupClicked(requireActivity(), adminActionStatus.isLinkActive(), new Runnable() { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda23
            @Override // java.lang.Runnable
            public final void run() {
                RecipientBottomSheetDialogFragment.this.dismiss();
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$15(IdentityRecord identityRecord) {
        this.viewSafetyNumberButton.setVisibility(identityRecord != null ? 0 : 8);
        if (identityRecord != null) {
            this.viewSafetyNumberButton.setOnClickListener(new View.OnClickListener(identityRecord) { // from class: org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment$$ExternalSyntheticLambda22
                public final /* synthetic */ IdentityRecord f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    RecipientBottomSheetDialogFragment.this.lambda$onViewCreated$14(this.f$1, view);
                }
            });
        }
    }

    public /* synthetic */ void lambda$onViewCreated$14(IdentityRecord identityRecord, View view) {
        dismiss();
        this.viewModel.onViewSafetyNumberClicked(requireActivity(), identityRecord);
    }

    public /* synthetic */ void lambda$onViewCreated$16(View view) {
        dismiss();
        this.viewModel.onAvatarClicked(requireActivity());
    }

    public /* synthetic */ void lambda$onViewCreated$17(RecipientId recipientId, View view) {
        dismiss();
        ViewBadgeBottomSheetDialogFragment.show(getParentFragmentManager(), recipientId, null);
    }

    public /* synthetic */ void lambda$onViewCreated$18(View view) {
        this.viewModel.onBlockClicked(requireActivity());
    }

    public /* synthetic */ void lambda$onViewCreated$19(View view) {
        this.viewModel.onUnblockClicked(requireActivity());
    }

    public /* synthetic */ void lambda$onViewCreated$20(View view) {
        this.viewModel.onMakeGroupAdminClicked(requireActivity());
    }

    public /* synthetic */ void lambda$onViewCreated$21(View view) {
        this.viewModel.onRemoveGroupAdminClicked(requireActivity());
    }

    public /* synthetic */ void lambda$onViewCreated$22(View view) {
        dismiss();
        this.viewModel.onAddToGroupButton(requireActivity());
    }

    public /* synthetic */ void lambda$onViewCreated$23(Boolean bool) {
        this.adminActionBusy.setVisibility(bool.booleanValue() ? 0 : 8);
        this.makeGroupAdminButton.setEnabled(!bool.booleanValue());
        this.removeAdminButton.setEnabled(!bool.booleanValue());
        this.removeFromGroupButton.setEnabled(!bool.booleanValue());
    }

    private void openSystemContactSheet(Intent intent) {
        try {
            startActivityForResult(intent, 1111);
        } catch (ActivityNotFoundException unused) {
            Log.w(TAG, "No activity existed to open the contact.");
            Toast.makeText(requireContext(), (int) R.string.RecipientBottomSheet_unable_to_open_contacts, 1).show();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1 && i == 1111) {
            this.viewModel.refreshRecipient();
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }
}
