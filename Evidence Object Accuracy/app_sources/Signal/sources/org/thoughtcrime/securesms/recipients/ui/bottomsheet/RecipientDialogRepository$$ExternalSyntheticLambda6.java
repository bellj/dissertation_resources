package org.thoughtcrime.securesms.recipients.ui.bottomsheet;

import androidx.core.util.Consumer;
import org.signal.core.util.concurrent.SimpleTask;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class RecipientDialogRepository$$ExternalSyntheticLambda6 implements SimpleTask.ForegroundTask {
    public final /* synthetic */ Consumer f$0;

    public /* synthetic */ RecipientDialogRepository$$ExternalSyntheticLambda6(Consumer consumer) {
        this.f$0 = consumer;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
    public final void run(Object obj) {
        this.f$0.accept((Boolean) obj);
    }
}
