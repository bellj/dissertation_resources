package org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.qr;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ShareCompat$IntentBuilder;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.qr.QrView;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.qr.QrCode;
import org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.qr.GroupLinkShareQrViewModel;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.ThemeUtil;

/* loaded from: classes4.dex */
public class GroupLinkShareQrDialogFragment extends DialogFragment {
    private static final String ARG_GROUP_ID;
    private static final String TAG = Log.tag(GroupLinkShareQrDialogFragment.class);
    private QrView qrImageView;
    private View shareCodeButton;
    private GroupLinkShareQrViewModel viewModel;

    public static void show(FragmentManager fragmentManager, GroupId.V2 v2) {
        GroupLinkShareQrDialogFragment groupLinkShareQrDialogFragment = new GroupLinkShareQrDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("group_id", v2.toString());
        groupLinkShareQrDialogFragment.setArguments(bundle);
        groupLinkShareQrDialogFragment.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, ThemeUtil.isDarkTheme(requireActivity()) ? R.style.TextSecure_DarkTheme : R.style.TextSecure_LightTheme);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.group_link_share_qr_dialog_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        initializeViewModel();
        initializeViews(view);
    }

    private void initializeViewModel() {
        String string = requireArguments().getString("group_id");
        Objects.requireNonNull(string);
        this.viewModel = (GroupLinkShareQrViewModel) ViewModelProviders.of(this, new GroupLinkShareQrViewModel.Factory(GroupId.parseOrThrow(string).requireV2())).get(GroupLinkShareQrViewModel.class);
    }

    private void initializeViews(View view) {
        this.qrImageView = (QrView) view.findViewById(R.id.group_link_share_qr_image);
        this.shareCodeButton = view.findViewById(R.id.group_link_share_code_button);
        ((Toolbar) view.findViewById(R.id.group_link_share_qr_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.qr.GroupLinkShareQrDialogFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GroupLinkShareQrDialogFragment.this.lambda$initializeViews$0(view2);
            }
        });
        this.viewModel.getQrUrl().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.qr.GroupLinkShareQrDialogFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupLinkShareQrDialogFragment.this.presentUrl((String) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeViews$0(View view) {
        dismissAllowingStateLoss();
    }

    public void presentUrl(String str) {
        this.qrImageView.setQrText(str);
        if (Build.VERSION.SDK_INT >= 26) {
            this.shareCodeButton.setVisibility(0);
            this.shareCodeButton.setOnClickListener(new View.OnClickListener(str) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.qr.GroupLinkShareQrDialogFragment$$ExternalSyntheticLambda2
                public final /* synthetic */ String f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GroupLinkShareQrDialogFragment.this.lambda$presentUrl$1(this.f$1, view);
                }
            });
            return;
        }
        this.shareCodeButton.setVisibility(8);
    }

    public /* synthetic */ void lambda$presentUrl$1(String str, View view) {
        try {
            requireContext().startActivity(ShareCompat$IntentBuilder.from(requireActivity()).setType(MediaUtil.IMAGE_PNG).setStream(createTemporaryPng(str)).createChooserIntent().addFlags(1));
        } catch (IOException e) {
            Log.w(TAG, e);
        }
    }

    private static Uri createTemporaryPng(String str) throws IOException {
        Bitmap create = QrCode.create(str, -16777216, -1);
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            create.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byteArrayOutputStream.flush();
            Uri createForSingleSessionInMemory = BlobProvider.getInstance().forData(byteArrayOutputStream.toByteArray()).withMimeType(MediaUtil.IMAGE_PNG).withFileName("SignalGroupQr.png").createForSingleSessionInMemory();
            byteArrayOutputStream.close();
            return createForSingleSessionInMemory;
        } finally {
            create.recycle();
        }
    }
}
