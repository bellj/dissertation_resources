package org.thoughtcrime.securesms.recipients.ui.bottomsheet;

import org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class RecipientDialogViewModel$$ExternalSyntheticLambda5 implements GroupChangeErrorCallback {
    public final /* synthetic */ RecipientDialogViewModel f$0;

    public /* synthetic */ RecipientDialogViewModel$$ExternalSyntheticLambda5(RecipientDialogViewModel recipientDialogViewModel) {
        this.f$0 = recipientDialogViewModel;
    }

    @Override // org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback
    public final void onError(GroupChangeFailureReason groupChangeFailureReason) {
        this.f$0.showErrorToast(groupChangeFailureReason);
    }
}
