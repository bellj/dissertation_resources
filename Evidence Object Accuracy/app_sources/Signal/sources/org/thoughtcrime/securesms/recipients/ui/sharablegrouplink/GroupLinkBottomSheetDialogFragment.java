package org.thoughtcrime.securesms.recipients.ui.sharablegrouplink;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.app.ShareCompat$IntentBuilder;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import java.util.Collections;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;
import org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.qr.GroupLinkShareQrDialogFragment;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class GroupLinkBottomSheetDialogFragment extends BottomSheetDialogFragment {
    public static final String ARG_GROUP_ID;

    public static void show(FragmentManager fragmentManager, GroupId.V2 v2) {
        GroupLinkBottomSheetDialogFragment groupLinkBottomSheetDialogFragment = new GroupLinkBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("group_id", v2.toString());
        groupLinkBottomSheetDialogFragment.setArguments(bundle);
        groupLinkBottomSheetDialogFragment.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, ThemeUtil.isDarkTheme(requireContext()) ? R.style.Theme_Signal_RoundedBottomSheet : R.style.Theme_Signal_RoundedBottomSheet_Light);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.group_link_share_bottom_sheet, viewGroup, false);
        View findViewById = inflate.findViewById(R.id.group_link_bottom_sheet_share_via_signal_button);
        View findViewById2 = inflate.findViewById(R.id.group_link_bottom_sheet_copy_button);
        View findViewById3 = inflate.findViewById(R.id.group_link_bottom_sheet_qr_code_button);
        View findViewById4 = inflate.findViewById(R.id.group_link_bottom_sheet_share_via_system_button);
        TextView textView = (TextView) inflate.findViewById(R.id.group_link_bottom_sheet_hint);
        String string = requireArguments().getString("group_id");
        Objects.requireNonNull(string);
        GroupId.V2 requireV2 = GroupId.parseOrThrow(string).requireV2();
        new LiveGroup(requireV2).getGroupLink().observe(getViewLifecycleOwner(), new Observer(textView, findViewById, findViewById2, findViewById3, requireV2, findViewById4) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.GroupLinkBottomSheetDialogFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ TextView f$1;
            public final /* synthetic */ View f$2;
            public final /* synthetic */ View f$3;
            public final /* synthetic */ View f$4;
            public final /* synthetic */ GroupId.V2 f$5;
            public final /* synthetic */ View f$6;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupLinkBottomSheetDialogFragment.this.lambda$onCreateView$4(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, (GroupLinkUrlAndStatus) obj);
            }
        });
        return inflate;
    }

    public /* synthetic */ void lambda$onCreateView$4(TextView textView, View view, View view2, View view3, GroupId.V2 v2, View view4, GroupLinkUrlAndStatus groupLinkUrlAndStatus) {
        if (!groupLinkUrlAndStatus.isEnabled()) {
            Toast.makeText(requireContext(), (int) R.string.GroupLinkBottomSheet_the_link_is_not_currently_active, 0).show();
            dismiss();
            return;
        }
        textView.setText(groupLinkUrlAndStatus.isRequiresApproval() ? R.string.GroupLinkBottomSheet_share_hint_requiring_approval : R.string.GroupLinkBottomSheet_share_hint_not_requiring_approval);
        textView.setVisibility(0);
        view.setOnClickListener(new View.OnClickListener(groupLinkUrlAndStatus) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.GroupLinkBottomSheetDialogFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ GroupLinkUrlAndStatus f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view5) {
                GroupLinkBottomSheetDialogFragment.this.lambda$onCreateView$0(this.f$1, view5);
            }
        });
        view2.setOnClickListener(new View.OnClickListener(groupLinkUrlAndStatus) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.GroupLinkBottomSheetDialogFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ GroupLinkUrlAndStatus f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view5) {
                GroupLinkBottomSheetDialogFragment.this.lambda$onCreateView$1(this.f$1, view5);
            }
        });
        view3.setOnClickListener(new View.OnClickListener(v2) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.GroupLinkBottomSheetDialogFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ GroupId.V2 f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view5) {
                GroupLinkBottomSheetDialogFragment.this.lambda$onCreateView$2(this.f$1, view5);
            }
        });
        view4.setOnClickListener(new View.OnClickListener(groupLinkUrlAndStatus) { // from class: org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.GroupLinkBottomSheetDialogFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ GroupLinkUrlAndStatus f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view5) {
                GroupLinkBottomSheetDialogFragment.this.lambda$onCreateView$3(this.f$1, view5);
            }
        });
    }

    public /* synthetic */ void lambda$onCreateView$0(GroupLinkUrlAndStatus groupLinkUrlAndStatus, View view) {
        MultiselectForwardFragment.showBottomSheet(getParentFragmentManager(), new MultiselectForwardFragmentArgs(true, Collections.singletonList(new MultiShareArgs.Builder().withDraftText(groupLinkUrlAndStatus.getUrl()).build()), R.string.MultiselectForwardFragment__share_with));
        dismiss();
    }

    public /* synthetic */ void lambda$onCreateView$1(GroupLinkUrlAndStatus groupLinkUrlAndStatus, View view) {
        Context requireContext = requireContext();
        Util.copyToClipboard(requireContext, groupLinkUrlAndStatus.getUrl());
        Toast.makeText(requireContext, (int) R.string.GroupLinkBottomSheet_copied_to_clipboard, 0).show();
        dismiss();
    }

    public /* synthetic */ void lambda$onCreateView$2(GroupId.V2 v2, View view) {
        GroupLinkShareQrDialogFragment.show(requireFragmentManager(), v2);
        dismiss();
    }

    public /* synthetic */ void lambda$onCreateView$3(GroupLinkUrlAndStatus groupLinkUrlAndStatus, View view) {
        ShareCompat$IntentBuilder.from(requireActivity()).setType("text/plain").setText(groupLinkUrlAndStatus.getUrl()).startChooser();
        dismiss();
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }
}
