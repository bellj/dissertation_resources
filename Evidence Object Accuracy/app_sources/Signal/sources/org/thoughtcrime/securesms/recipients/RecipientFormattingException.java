package org.thoughtcrime.securesms.recipients;

/* loaded from: classes4.dex */
public class RecipientFormattingException extends Exception {
    public RecipientFormattingException() {
    }

    public RecipientFormattingException(String str) {
        super(str);
    }

    public RecipientFormattingException(String str, Throwable th) {
        super(str, th);
    }

    public RecipientFormattingException(Throwable th) {
        super(th);
    }
}
