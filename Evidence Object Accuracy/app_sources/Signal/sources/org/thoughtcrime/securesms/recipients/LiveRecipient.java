package org.thoughtcrime.securesms.recipients;

import android.content.Context;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Cancellable;
import j$.util.Optional;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes.dex */
public final class LiveRecipient {
    private static final String TAG = Log.tag(LiveRecipient.class);
    private final Context context;
    private final DistributionListDatabase distributionListDatabase = SignalDatabase.distributionLists();
    private final Observer<Recipient> foreverObserver = new Observer() { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda5
        @Override // androidx.lifecycle.Observer
        public final void onChanged(Object obj) {
            LiveRecipient.this.lambda$new$1((Recipient) obj);
        }
    };
    private final GroupDatabase groupDatabase = SignalDatabase.groups();
    private final MutableLiveData<Recipient> liveData;
    private final LiveData<Recipient> observableLiveData;
    private final LiveData<Recipient> observableLiveDataResolved;
    private final Set<RecipientForeverObserver> observers = new CopyOnWriteArraySet();
    private final AtomicReference<Recipient> recipient;
    private final RecipientDatabase recipientDatabase = SignalDatabase.recipients();
    private final MutableLiveData<Object> refreshForceNotify;

    public static /* synthetic */ Recipient lambda$new$2(Recipient recipient, Object obj) {
        return recipient;
    }

    public LiveRecipient(Context context, Recipient recipient) {
        this.context = context.getApplicationContext();
        MutableLiveData<Recipient> mutableLiveData = new MutableLiveData<>(recipient);
        this.liveData = mutableLiveData;
        this.recipient = new AtomicReference<>(recipient);
        MutableLiveData<Object> mutableLiveData2 = new MutableLiveData<>(new Object());
        this.refreshForceNotify = mutableLiveData2;
        LiveData<Recipient> combineLatest = LiveDataUtil.combineLatest(LiveDataUtil.distinctUntilChanged(mutableLiveData, new LiveDataUtil.EqualityChecker() { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.EqualityChecker
            public final boolean contentsMatch(Object obj, Object obj2) {
                return ((Recipient) obj).hasSameContent((Recipient) obj2);
            }
        }), mutableLiveData2, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda7
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return LiveRecipient.lambda$new$2((Recipient) obj, obj2);
            }
        });
        this.observableLiveData = combineLatest;
        this.observableLiveDataResolved = LiveDataUtil.filter(combineLatest, new Predicate() { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return LiveRecipient.lambda$new$3((Recipient) obj);
            }
        });
    }

    public /* synthetic */ void lambda$new$1(Recipient recipient) {
        ThreadUtil.postToMain(new Runnable(recipient) { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda12
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LiveRecipient.this.lambda$new$0(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(Recipient recipient) {
        for (RecipientForeverObserver recipientForeverObserver : this.observers) {
            recipientForeverObserver.onRecipientChanged(recipient);
        }
    }

    public static /* synthetic */ boolean lambda$new$3(Recipient recipient) {
        return !recipient.isResolving();
    }

    public RecipientId getId() {
        return this.recipient.get().getId();
    }

    public Recipient get() {
        return this.recipient.get();
    }

    public /* synthetic */ void lambda$observe$4(LifecycleOwner lifecycleOwner, Observer observer) {
        this.observableLiveData.observe(lifecycleOwner, observer);
    }

    public void observe(LifecycleOwner lifecycleOwner, Observer<Recipient> observer) {
        ThreadUtil.postToMain(new Runnable(lifecycleOwner, observer) { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda10
            public final /* synthetic */ LifecycleOwner f$1;
            public final /* synthetic */ Observer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LiveRecipient.this.lambda$observe$4(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$removeObservers$5(LifecycleOwner lifecycleOwner) {
        this.observableLiveData.removeObservers(lifecycleOwner);
    }

    public void removeObservers(LifecycleOwner lifecycleOwner) {
        ThreadUtil.runOnMain(new Runnable(lifecycleOwner) { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda4
            public final /* synthetic */ LifecycleOwner f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LiveRecipient.this.lambda$removeObservers$5(this.f$1);
            }
        });
    }

    public Observable<Recipient> asObservable() {
        return Observable.create(new ObservableOnSubscribe() { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda11
            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                LiveRecipient.this.lambda$asObservable$7(observableEmitter);
            }
        });
    }

    public /* synthetic */ void lambda$asObservable$7(ObservableEmitter observableEmitter) throws Throwable {
        Recipient recipient = this.recipient.get();
        if (!(recipient == null || recipient.getId() == RecipientId.UNKNOWN)) {
            observableEmitter.onNext(recipient);
        }
        Objects.requireNonNull(observableEmitter);
        LiveRecipient$$ExternalSyntheticLambda1 liveRecipient$$ExternalSyntheticLambda1 = new LiveRecipient$$ExternalSyntheticLambda1(observableEmitter);
        observeForever(liveRecipient$$ExternalSyntheticLambda1);
        observableEmitter.setCancellable(new Cancellable(liveRecipient$$ExternalSyntheticLambda1) { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda2
            public final /* synthetic */ RecipientForeverObserver f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                LiveRecipient.this.lambda$asObservable$6(this.f$1);
            }
        });
    }

    public void observeForever(RecipientForeverObserver recipientForeverObserver) {
        ThreadUtil.postToMain(new Runnable(recipientForeverObserver) { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda0
            public final /* synthetic */ RecipientForeverObserver f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LiveRecipient.this.lambda$observeForever$8(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$observeForever$8(RecipientForeverObserver recipientForeverObserver) {
        if (this.observers.isEmpty()) {
            this.observableLiveData.observeForever(this.foreverObserver);
        }
        this.observers.add(recipientForeverObserver);
    }

    /* renamed from: removeForeverObserver */
    public void lambda$asObservable$6(RecipientForeverObserver recipientForeverObserver) {
        ThreadUtil.postToMain(new Runnable(recipientForeverObserver) { // from class: org.thoughtcrime.securesms.recipients.LiveRecipient$$ExternalSyntheticLambda9
            public final /* synthetic */ RecipientForeverObserver f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LiveRecipient.this.lambda$removeForeverObserver$9(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$removeForeverObserver$9(RecipientForeverObserver recipientForeverObserver) {
        this.observers.remove(recipientForeverObserver);
        if (this.observers.isEmpty()) {
            this.observableLiveData.removeObserver(this.foreverObserver);
        }
    }

    public Recipient resolve() {
        Recipient recipient = this.recipient.get();
        if (!recipient.isResolving() || recipient.getId().isUnknown()) {
            return recipient;
        }
        if (ThreadUtil.isMainThread()) {
            String str = TAG;
            Log.w(str, "[Resolve][MAIN] " + getId(), new Throwable());
        }
        Recipient fetchAndCacheRecipientFromDisk = fetchAndCacheRecipientFromDisk(getId());
        set(fetchAndCacheRecipientFromDisk);
        return fetchAndCacheRecipientFromDisk;
    }

    public void refresh() {
        refresh(getId());
    }

    public void refresh(RecipientId recipientId) {
        if (!getId().equals(recipientId)) {
            String str = TAG;
            Log.w(str, "Switching ID from " + getId() + " to " + recipientId);
        }
        if (!getId().isUnknown()) {
            if (ThreadUtil.isMainThread()) {
                String str2 = TAG;
                Log.w(str2, "[Refresh][MAIN] " + recipientId, new Throwable());
            }
            set(fetchAndCacheRecipientFromDisk(recipientId));
            this.refreshForceNotify.postValue(new Object());
        }
    }

    public LiveData<Recipient> getLiveData() {
        return this.observableLiveData;
    }

    public LiveData<Recipient> getLiveDataResolved() {
        return this.observableLiveDataResolved;
    }

    private Recipient fetchAndCacheRecipientFromDisk(RecipientId recipientId) {
        RecipientDetails recipientDetails;
        RecipientRecord record = this.recipientDatabase.getRecord(recipientId);
        if (record.getGroupId() != null) {
            recipientDetails = getGroupRecipientDetails(record);
        } else if (record.getDistributionListId() != null) {
            recipientDetails = getDistributionListRecipientDetails(record);
        } else {
            recipientDetails = RecipientDetails.forIndividual(this.context, record);
        }
        Recipient recipient = new Recipient(recipientId, recipientDetails, true);
        RecipientIdCache.INSTANCE.put(recipient);
        return recipient;
    }

    private RecipientDetails getGroupRecipientDetails(RecipientRecord recipientRecord) {
        Optional<GroupDatabase.GroupRecord> group = this.groupDatabase.getGroup(recipientRecord.getId());
        if (!group.isPresent()) {
            return new RecipientDetails(null, null, Optional.empty(), false, false, recipientRecord.getRegistered(), recipientRecord, null, false);
        }
        return new RecipientDetails(group.get().getTitle(), null, group.get().hasAvatar() ? Optional.of(Long.valueOf(group.get().getAvatarId())) : Optional.empty(), false, false, recipientRecord.getRegistered(), recipientRecord, Stream.of(group.get().getMembers()).filterNot(new LiveRecipient$$ExternalSyntheticLambda3()).toList(), false);
    }

    private RecipientDetails getDistributionListRecipientDetails(RecipientRecord recipientRecord) {
        DistributionListDatabase distributionListDatabase = this.distributionListDatabase;
        DistributionListId distributionListId = recipientRecord.getDistributionListId();
        Objects.requireNonNull(distributionListId);
        DistributionListRecord list = distributionListDatabase.getList(distributionListId);
        String str = null;
        if (list == null) {
            return RecipientDetails.forDistributionList(null, null, recipientRecord);
        }
        if (!list.isUnknown()) {
            str = list.getName();
        }
        return RecipientDetails.forDistributionList(str, Stream.of(list.getMembers()).filterNot(new LiveRecipient$$ExternalSyntheticLambda3()).toList(), recipientRecord);
    }

    public synchronized void set(Recipient recipient) {
        this.recipient.set(recipient);
        this.liveData.postValue(recipient);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || LiveRecipient.class != obj.getClass()) {
            return false;
        }
        return this.recipient.equals(((LiveRecipient) obj).recipient);
    }

    public int hashCode() {
        return Objects.hash(this.recipient);
    }
}
