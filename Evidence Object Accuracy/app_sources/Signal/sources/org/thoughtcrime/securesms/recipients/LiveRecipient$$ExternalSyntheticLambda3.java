package org.thoughtcrime.securesms.recipients;

import com.annimon.stream.function.Predicate;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LiveRecipient$$ExternalSyntheticLambda3 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return ((RecipientId) obj).isUnknown();
    }
}
