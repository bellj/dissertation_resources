package org.thoughtcrime.securesms.badges.gifts.flow;

import java.util.Currency;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: GiftFlowState.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u001a\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u00011B]\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0014\b\u0002\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\n0\t\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010¢\u0006\u0002\u0010\u0011J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\u0010\u0010\"\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0002\u0010\u0019J\u000b\u0010#\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u0015\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\n0\tHÆ\u0003J\t\u0010%\u001a\u00020\fHÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u000eHÆ\u0003J\u000b\u0010'\u001a\u0004\u0018\u00010\u0010HÆ\u0003Jh\u0010(\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0014\b\u0002\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\n0\t2\b\b\u0002\u0010\u000b\u001a\u00020\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÆ\u0001¢\u0006\u0002\u0010)J\u0013\u0010*\u001a\u00020+2\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010-\u001a\u00020.HÖ\u0001J\t\u0010/\u001a\u000200HÖ\u0001R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\n\n\u0002\u0010\u001a\u001a\u0004\b\u0018\u0010\u0019R\u001d\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0013\u0010\r\u001a\u0004\u0018\u00010\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 ¨\u00062"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState;", "", "currency", "Ljava/util/Currency;", "giftLevel", "", "giftBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "giftPrices", "", "Lorg/signal/core/util/money/FiatMoney;", "stage", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState$Stage;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "additionalMessage", "", "(Ljava/util/Currency;Ljava/lang/Long;Lorg/thoughtcrime/securesms/badges/models/Badge;Ljava/util/Map;Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState$Stage;Lorg/thoughtcrime/securesms/recipients/Recipient;Ljava/lang/CharSequence;)V", "getAdditionalMessage", "()Ljava/lang/CharSequence;", "getCurrency", "()Ljava/util/Currency;", "getGiftBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getGiftLevel", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getGiftPrices", "()Ljava/util/Map;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getStage", "()Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState$Stage;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "(Ljava/util/Currency;Ljava/lang/Long;Lorg/thoughtcrime/securesms/badges/models/Badge;Ljava/util/Map;Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState$Stage;Lorg/thoughtcrime/securesms/recipients/Recipient;Ljava/lang/CharSequence;)Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState;", "equals", "", "other", "hashCode", "", "toString", "", "Stage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GiftFlowState {
    private final CharSequence additionalMessage;
    private final Currency currency;
    private final Badge giftBadge;
    private final Long giftLevel;
    private final Map<Currency, FiatMoney> giftPrices;
    private final Recipient recipient;
    private final Stage stage;

    /* compiled from: GiftFlowState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState$Stage;", "", "(Ljava/lang/String;I)V", "INIT", "READY", "RECIPIENT_VERIFICATION", "TOKEN_REQUEST", "PAYMENT_PIPELINE", "FAILURE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum Stage {
        INIT,
        READY,
        RECIPIENT_VERIFICATION,
        TOKEN_REQUEST,
        PAYMENT_PIPELINE,
        FAILURE
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ GiftFlowState copy$default(GiftFlowState giftFlowState, Currency currency, Long l, Badge badge, Map map, Stage stage, Recipient recipient, CharSequence charSequence, int i, Object obj) {
        if ((i & 1) != 0) {
            currency = giftFlowState.currency;
        }
        if ((i & 2) != 0) {
            l = giftFlowState.giftLevel;
        }
        if ((i & 4) != 0) {
            badge = giftFlowState.giftBadge;
        }
        if ((i & 8) != 0) {
            map = giftFlowState.giftPrices;
        }
        if ((i & 16) != 0) {
            stage = giftFlowState.stage;
        }
        if ((i & 32) != 0) {
            recipient = giftFlowState.recipient;
        }
        if ((i & 64) != 0) {
            charSequence = giftFlowState.additionalMessage;
        }
        return giftFlowState.copy(currency, l, badge, map, stage, recipient, charSequence);
    }

    public final Currency component1() {
        return this.currency;
    }

    public final Long component2() {
        return this.giftLevel;
    }

    public final Badge component3() {
        return this.giftBadge;
    }

    public final Map<Currency, FiatMoney> component4() {
        return this.giftPrices;
    }

    public final Stage component5() {
        return this.stage;
    }

    public final Recipient component6() {
        return this.recipient;
    }

    public final CharSequence component7() {
        return this.additionalMessage;
    }

    public final GiftFlowState copy(Currency currency, Long l, Badge badge, Map<Currency, ? extends FiatMoney> map, Stage stage, Recipient recipient, CharSequence charSequence) {
        Intrinsics.checkNotNullParameter(currency, "currency");
        Intrinsics.checkNotNullParameter(map, "giftPrices");
        Intrinsics.checkNotNullParameter(stage, "stage");
        return new GiftFlowState(currency, l, badge, map, stage, recipient, charSequence);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GiftFlowState)) {
            return false;
        }
        GiftFlowState giftFlowState = (GiftFlowState) obj;
        return Intrinsics.areEqual(this.currency, giftFlowState.currency) && Intrinsics.areEqual(this.giftLevel, giftFlowState.giftLevel) && Intrinsics.areEqual(this.giftBadge, giftFlowState.giftBadge) && Intrinsics.areEqual(this.giftPrices, giftFlowState.giftPrices) && this.stage == giftFlowState.stage && Intrinsics.areEqual(this.recipient, giftFlowState.recipient) && Intrinsics.areEqual(this.additionalMessage, giftFlowState.additionalMessage);
    }

    public int hashCode() {
        int hashCode = this.currency.hashCode() * 31;
        Long l = this.giftLevel;
        int i = 0;
        int hashCode2 = (hashCode + (l == null ? 0 : l.hashCode())) * 31;
        Badge badge = this.giftBadge;
        int hashCode3 = (((((hashCode2 + (badge == null ? 0 : badge.hashCode())) * 31) + this.giftPrices.hashCode()) * 31) + this.stage.hashCode()) * 31;
        Recipient recipient = this.recipient;
        int hashCode4 = (hashCode3 + (recipient == null ? 0 : recipient.hashCode())) * 31;
        CharSequence charSequence = this.additionalMessage;
        if (charSequence != null) {
            i = charSequence.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        return "GiftFlowState(currency=" + this.currency + ", giftLevel=" + this.giftLevel + ", giftBadge=" + this.giftBadge + ", giftPrices=" + this.giftPrices + ", stage=" + this.stage + ", recipient=" + this.recipient + ", additionalMessage=" + ((Object) this.additionalMessage) + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: java.util.Map<java.util.Currency, ? extends org.signal.core.util.money.FiatMoney> */
    /* JADX WARN: Multi-variable type inference failed */
    public GiftFlowState(Currency currency, Long l, Badge badge, Map<Currency, ? extends FiatMoney> map, Stage stage, Recipient recipient, CharSequence charSequence) {
        Intrinsics.checkNotNullParameter(currency, "currency");
        Intrinsics.checkNotNullParameter(map, "giftPrices");
        Intrinsics.checkNotNullParameter(stage, "stage");
        this.currency = currency;
        this.giftLevel = l;
        this.giftBadge = badge;
        this.giftPrices = map;
        this.stage = stage;
        this.recipient = recipient;
        this.additionalMessage = charSequence;
    }

    public final Currency getCurrency() {
        return this.currency;
    }

    public final Long getGiftLevel() {
        return this.giftLevel;
    }

    public final Badge getGiftBadge() {
        return this.giftBadge;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ GiftFlowState(java.util.Currency r8, java.lang.Long r9, org.thoughtcrime.securesms.badges.models.Badge r10, java.util.Map r11, org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState.Stage r12, org.thoughtcrime.securesms.recipients.Recipient r13, java.lang.CharSequence r14, int r15, kotlin.jvm.internal.DefaultConstructorMarker r16) {
        /*
            r7 = this;
            r0 = r15 & 2
            r1 = 0
            if (r0 == 0) goto L_0x0007
            r0 = r1
            goto L_0x0008
        L_0x0007:
            r0 = r9
        L_0x0008:
            r2 = r15 & 4
            if (r2 == 0) goto L_0x000e
            r2 = r1
            goto L_0x000f
        L_0x000e:
            r2 = r10
        L_0x000f:
            r3 = r15 & 8
            if (r3 == 0) goto L_0x0018
            java.util.Map r3 = kotlin.collections.MapsKt.emptyMap()
            goto L_0x0019
        L_0x0018:
            r3 = r11
        L_0x0019:
            r4 = r15 & 16
            if (r4 == 0) goto L_0x0020
            org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState$Stage r4 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState.Stage.INIT
            goto L_0x0021
        L_0x0020:
            r4 = r12
        L_0x0021:
            r5 = r15 & 32
            if (r5 == 0) goto L_0x0027
            r5 = r1
            goto L_0x0028
        L_0x0027:
            r5 = r13
        L_0x0028:
            r6 = r15 & 64
            if (r6 == 0) goto L_0x002d
            goto L_0x002e
        L_0x002d:
            r1 = r14
        L_0x002e:
            r9 = r7
            r10 = r8
            r11 = r0
            r12 = r2
            r13 = r3
            r14 = r4
            r15 = r5
            r16 = r1
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState.<init>(java.util.Currency, java.lang.Long, org.thoughtcrime.securesms.badges.models.Badge, java.util.Map, org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState$Stage, org.thoughtcrime.securesms.recipients.Recipient, java.lang.CharSequence, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final Map<Currency, FiatMoney> getGiftPrices() {
        return this.giftPrices;
    }

    public final Stage getStage() {
        return this.stage;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }

    public final CharSequence getAdditionalMessage() {
        return this.additionalMessage;
    }
}
