package org.thoughtcrime.securesms.badges.view;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeRepository;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.models.LargeBadge;
import org.thoughtcrime.securesms.badges.view.ViewBadgeState;
import org.thoughtcrime.securesms.badges.view.ViewBadgeViewModel;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.PlayServicesUtil;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;

/* compiled from: ViewBadgeBottomSheetDialogFragment.kt */
@Metadata(d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\n\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0002J&\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\u001a\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u00182\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\u0006R\u001b\u0010\r\u001a\u00020\u000e8BX\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/badges/view/ViewBadgeBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/components/FixedRoundedCornerBottomSheetDialogFragment;", "()V", "peekHeightPercentage", "", "getPeekHeightPercentage", "()F", "textBounds", "Landroid/graphics/Rect;", "textPaint", "Landroid/graphics/Paint;", "textWidth", "getTextWidth", "viewModel", "Lorg/thoughtcrime/securesms/badges/view/ViewBadgeViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/badges/view/ViewBadgeViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getStartBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "", "view", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ViewBadgeBottomSheetDialogFragment extends FixedRoundedCornerBottomSheetDialogFragment {
    private static final String ARG_RECIPIENT_ID;
    private static final String ARG_START_BADGE;
    public static final Companion Companion = new Companion(null);
    private final float peekHeightPercentage = 1.0f;
    private final Rect textBounds = new Rect();
    private final Paint textPaint;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(ViewBadgeViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment$viewModel$2
        final /* synthetic */ ViewBadgeBottomSheetDialogFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            Badge access$getStartBadge = ViewBadgeBottomSheetDialogFragment.access$getStartBadge(this.this$0);
            RecipientId access$getRecipientId = ViewBadgeBottomSheetDialogFragment.access$getRecipientId(this.this$0);
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            return new ViewBadgeViewModel.Factory(access$getStartBadge, access$getRecipientId, new BadgeRepository(requireContext));
        }
    });

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m458onViewCreated$lambda4(TabLayout.Tab tab, int i) {
        Intrinsics.checkNotNullParameter(tab, "<anonymous parameter 0>");
    }

    @JvmStatic
    public static final void show(FragmentManager fragmentManager, RecipientId recipientId, Badge badge) {
        Companion.show(fragmentManager, recipientId, badge);
    }

    public ViewBadgeBottomSheetDialogFragment() {
        Paint paint = new Paint();
        paint.setTextSize((float) ViewUtil.spToPx(16.0f));
        this.textPaint = paint;
    }

    public final ViewBadgeViewModel getViewModel() {
        return (ViewBadgeViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    protected float getPeekHeightPercentage() {
        return this.peekHeightPercentage;
    }

    private final float getTextWidth() {
        return (float) (getResources().getDisplayMetrics().widthPixels - ViewUtil.dpToPx(64));
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.view_badge_bottom_sheet_dialog_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        boolean z;
        Intrinsics.checkNotNullParameter(view, "view");
        postponeEnterTransition();
        View findViewById = view.findViewById(R.id.pager);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.pager)");
        ViewPager2 viewPager2 = (ViewPager2) findViewById;
        View findViewById2 = view.findViewById(R.id.tab_layout);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.tab_layout)");
        TabLayout tabLayout = (TabLayout) findViewById2;
        View findViewById3 = view.findViewById(R.id.action);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.action)");
        MaterialButton materialButton = (MaterialButton) findViewById3;
        View findViewById4 = view.findViewById(R.id.no_support);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.no_support)");
        if (Intrinsics.areEqual(getRecipientId(), Recipient.self().getId())) {
            ViewExtensionsKt.setVisible(materialButton, false);
        }
        boolean z2 = true;
        if (PlayServicesUtil.getPlayServicesStatus(requireContext()) != PlayServicesUtil.PlayServicesStatus.SUCCESS) {
            ViewExtensionsKt.setVisible(findViewById4, true);
            materialButton.setIcon(ContextCompat.getDrawable(requireContext(), R.drawable.ic_open_20));
            materialButton.setText(R.string.preferences__donate_to_signal);
            materialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    ViewBadgeBottomSheetDialogFragment.$r8$lambda$N4jxr9IoGxvgUNWquNPoB7XbFaU(ViewBadgeBottomSheetDialogFragment.this, view2);
                }
            });
        } else {
            if (FeatureFlags.donorBadges()) {
                List<Badge> badges = Recipient.self().getBadges();
                Intrinsics.checkNotNullExpressionValue(badges, "self().badges");
                if (!(badges instanceof Collection) || !badges.isEmpty()) {
                    Iterator<T> it = badges.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Badge badge = (Badge) it.next();
                        if (badge.getCategory() != Badge.Category.Donor || badge.isBoost() || badge.isExpired()) {
                            z = false;
                            continue;
                        } else {
                            z = true;
                            continue;
                        }
                        if (z) {
                            z2 = false;
                            break;
                        }
                    }
                }
                if (z2) {
                    materialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment$$ExternalSyntheticLambda2
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view2) {
                            ViewBadgeBottomSheetDialogFragment.$r8$lambda$UYCY7xTwJy5aFZhGbhnAG6ztetw(ViewBadgeBottomSheetDialogFragment.this, view2);
                        }
                    });
                }
            }
            ViewExtensionsKt.setVisible(materialButton, false);
        }
        MappingAdapter mappingAdapter = new MappingAdapter();
        LargeBadge.Companion.register(mappingAdapter);
        viewPager2.setAdapter(mappingAdapter);
        mappingAdapter.submitList(CollectionsKt__CollectionsJVMKt.listOf(new LargeBadge.EmptyModel()));
        new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment$$ExternalSyntheticLambda3
            @Override // com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
            public final void onConfigureTab(TabLayout.Tab tab, int i) {
                ViewBadgeBottomSheetDialogFragment.$r8$lambda$f2u_tnpFJngGQkOzXpQMIou6PzY(tab, i);
            }
        }).attach();
        viewPager2.registerOnPageChangeCallback(new ViewBadgeBottomSheetDialogFragment$onViewCreated$5(mappingAdapter, this));
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(tabLayout, mappingAdapter, viewPager2) { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ TabLayout f$1;
            public final /* synthetic */ MappingAdapter f$2;
            public final /* synthetic */ ViewPager2 f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ViewBadgeBottomSheetDialogFragment.m455$r8$lambda$yIlcwmy_nI4cFNhljJnsH2lf5U(ViewBadgeBottomSheetDialogFragment.this, this.f$1, this.f$2, this.f$3, (ViewBadgeState) obj);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m456onViewCreated$lambda1(ViewBadgeBottomSheetDialogFragment viewBadgeBottomSheetDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(viewBadgeBottomSheetDialogFragment, "this$0");
        CommunicationActions.openBrowserLink(viewBadgeBottomSheetDialogFragment.requireContext(), viewBadgeBottomSheetDialogFragment.getString(R.string.donate_url));
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m457onViewCreated$lambda3(ViewBadgeBottomSheetDialogFragment viewBadgeBottomSheetDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(viewBadgeBottomSheetDialogFragment, "this$0");
        AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
        Context requireContext = viewBadgeBottomSheetDialogFragment.requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        viewBadgeBottomSheetDialogFragment.startActivity(companion.subscriptions(requireContext));
    }

    /* renamed from: onViewCreated$lambda-8 */
    public static final void m459onViewCreated$lambda8(ViewBadgeBottomSheetDialogFragment viewBadgeBottomSheetDialogFragment, TabLayout tabLayout, MappingAdapter mappingAdapter, ViewPager2 viewPager2, ViewBadgeState viewBadgeState) {
        Intrinsics.checkNotNullParameter(viewBadgeBottomSheetDialogFragment, "this$0");
        Intrinsics.checkNotNullParameter(tabLayout, "$tabs");
        Intrinsics.checkNotNullParameter(mappingAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(viewPager2, "$pager");
        if (!(viewBadgeState.getRecipient() == null || viewBadgeState.getBadgeLoadState() == ViewBadgeState.LoadState.INIT)) {
            if (viewBadgeState.getAllBadgesVisibleOnProfile().isEmpty()) {
                viewBadgeBottomSheetDialogFragment.dismissAllowingStateLoss();
            }
            ViewExtensionsKt.setVisible(tabLayout, viewBadgeState.getAllBadgesVisibleOnProfile().size() > 1);
            int i = 3;
            for (Badge badge : viewBadgeState.getAllBadgesVisibleOnProfile()) {
                String shortDisplayName = viewBadgeState.getRecipient().getShortDisplayName(viewBadgeBottomSheetDialogFragment.requireContext());
                Intrinsics.checkNotNullExpressionValue(shortDisplayName, "state.recipient.getShort…layName(requireContext())");
                String resolveDescription = badge.resolveDescription(shortDisplayName);
                viewBadgeBottomSheetDialogFragment.textPaint.getTextBounds(resolveDescription, 0, resolveDescription.length(), viewBadgeBottomSheetDialogFragment.textBounds);
                i = Math.max(i, (int) ((float) Math.ceil((double) (((float) viewBadgeBottomSheetDialogFragment.textBounds.width()) / viewBadgeBottomSheetDialogFragment.getTextWidth()))));
            }
            List<Badge> allBadgesVisibleOnProfile = viewBadgeState.getAllBadgesVisibleOnProfile();
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(allBadgesVisibleOnProfile, 10));
            for (Badge badge2 : allBadgesVisibleOnProfile) {
                LargeBadge largeBadge = new LargeBadge(badge2);
                String shortDisplayName2 = viewBadgeState.getRecipient().getShortDisplayName(viewBadgeBottomSheetDialogFragment.requireContext());
                Intrinsics.checkNotNullExpressionValue(shortDisplayName2, "state.recipient.getShort…layName(requireContext())");
                arrayList.add(new LargeBadge.Model(largeBadge, shortDisplayName2, i + 1));
            }
            mappingAdapter.submitList(arrayList, new Runnable(viewPager2) { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment$$ExternalSyntheticLambda0
                public final /* synthetic */ ViewPager2 f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ViewBadgeBottomSheetDialogFragment.$r8$lambda$cdYRr7895hqUQUqdspn9Fpzr340(ViewBadgeState.this, this.f$1);
                }
            });
        }
    }

    /* renamed from: onViewCreated$lambda-8$lambda-7 */
    public static final void m460onViewCreated$lambda8$lambda7(ViewBadgeState viewBadgeState, ViewPager2 viewPager2) {
        Intrinsics.checkNotNullParameter(viewPager2, "$pager");
        int i = CollectionsKt___CollectionsKt.indexOf((List<? extends Badge>) ((List<? extends Object>) viewBadgeState.getAllBadgesVisibleOnProfile()), viewBadgeState.getSelectedBadge());
        if (viewBadgeState.getSelectedBadge() != null && viewPager2.getCurrentItem() != i) {
            viewPager2.setCurrentItem(i);
        }
    }

    public final Badge getStartBadge() {
        return (Badge) requireArguments().getParcelable(ARG_START_BADGE);
    }

    public final RecipientId getRecipientId() {
        Parcelable parcelable = requireArguments().getParcelable("recipient_id");
        if (parcelable != null) {
            return (RecipientId) parcelable;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    /* compiled from: ViewBadgeBottomSheetDialogFragment.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J$\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\rH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/badges/view/ViewBadgeBottomSheetDialogFragment$Companion;", "", "()V", "ARG_RECIPIENT_ID", "", "ARG_START_BADGE", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "startBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public static /* synthetic */ void show$default(Companion companion, FragmentManager fragmentManager, RecipientId recipientId, Badge badge, int i, Object obj) {
            if ((i & 4) != 0) {
                badge = null;
            }
            companion.show(fragmentManager, recipientId, badge);
        }

        @JvmStatic
        public final void show(FragmentManager fragmentManager, RecipientId recipientId, Badge badge) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            if (FeatureFlags.displayDonorBadges() || Intrinsics.areEqual(recipientId, Recipient.self().getId())) {
                ViewBadgeBottomSheetDialogFragment viewBadgeBottomSheetDialogFragment = new ViewBadgeBottomSheetDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable(ViewBadgeBottomSheetDialogFragment.ARG_START_BADGE, badge);
                bundle.putParcelable("recipient_id", recipientId);
                viewBadgeBottomSheetDialogFragment.setArguments(bundle);
                viewBadgeBottomSheetDialogFragment.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
            }
        }
    }
}
