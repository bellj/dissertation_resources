package org.thoughtcrime.securesms.badges.models;

import android.view.View;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: BadgePreview.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgePreview;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "BadgeModel", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BadgePreview {
    public static final BadgePreview INSTANCE = new BadgePreview();

    private BadgePreview() {
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m419register$lambda0(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(BadgeModel.FeaturedModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.badges.models.BadgePreview$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return BadgePreview.m419register$lambda0((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.featured_badge_preview_preference));
        mappingAdapter.registerFactory(BadgeModel.SubscriptionModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.badges.models.BadgePreview$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return BadgePreview.m420register$lambda1((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.subscription_flow_badge_preview_preference));
        mappingAdapter.registerFactory(BadgeModel.GiftedBadgeModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.badges.models.BadgePreview$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return BadgePreview.m421register$lambda2((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.gift_badge_preview_preference));
    }

    /* renamed from: register$lambda-1 */
    public static final MappingViewHolder m420register$lambda1(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    /* renamed from: register$lambda-2 */
    public static final MappingViewHolder m421register$lambda2(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    /* compiled from: BadgePreview.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00002\b\u0012\u0004\u0012\u0002H\u00010\u0002:\u0003\u0011\u0012\u0013B\u0007\b\u0004¢\u0006\u0002\u0010\u0003J\u0015\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u000fJ\u0015\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u000fR\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0012\u0010\b\u001a\u00020\tX¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000b\u0001\u0003\u0014\u0015\u0016¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel;", "T", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "()V", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "areContentsTheSame", "", "newItem", "(Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel;)Z", "areItemsTheSame", "FeaturedModel", "GiftedBadgeModel", "SubscriptionModel", "Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel$FeaturedModel;", "Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel$SubscriptionModel;", "Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel$GiftedBadgeModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static abstract class BadgeModel<T extends BadgeModel<T>> implements MappingModel<T> {
        public /* synthetic */ BadgeModel(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public abstract Badge getBadge();

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Object obj) {
            return MappingModel.CC.$default$getChangePayload(this, obj);
        }

        public abstract Recipient getRecipient();

        private BadgeModel() {
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.badges.models.BadgePreview$BadgeModel<T extends org.thoughtcrime.securesms.badges.models.BadgePreview$BadgeModel<T>> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* bridge */ /* synthetic */ boolean areContentsTheSame(Object obj) {
            return areContentsTheSame((BadgeModel<T>) ((BadgeModel) obj));
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.badges.models.BadgePreview$BadgeModel<T extends org.thoughtcrime.securesms.badges.models.BadgePreview$BadgeModel<T>> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* bridge */ /* synthetic */ boolean areItemsTheSame(Object obj) {
            return areItemsTheSame((BadgeModel<T>) ((BadgeModel) obj));
        }

        /* compiled from: BadgePreview.kt */
        @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0015\u0010\f\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel$FeaturedModel;", "Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class FeaturedModel extends BadgeModel<FeaturedModel> {
            private final Badge badge;
            private final Recipient recipient;

            public static /* synthetic */ FeaturedModel copy$default(FeaturedModel featuredModel, Badge badge, int i, Object obj) {
                if ((i & 1) != 0) {
                    badge = featuredModel.getBadge();
                }
                return featuredModel.copy(badge);
            }

            public final Badge component1() {
                return getBadge();
            }

            public final FeaturedModel copy(Badge badge) {
                return new FeaturedModel(badge);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof FeaturedModel) && Intrinsics.areEqual(getBadge(), ((FeaturedModel) obj).getBadge());
            }

            public int hashCode() {
                if (getBadge() == null) {
                    return 0;
                }
                return getBadge().hashCode();
            }

            public String toString() {
                return "FeaturedModel(badge=" + getBadge() + ')';
            }

            public FeaturedModel(Badge badge) {
                super(null);
                this.badge = badge;
                Recipient self = Recipient.self();
                Intrinsics.checkNotNullExpressionValue(self, "self()");
                this.recipient = self;
            }

            @Override // org.thoughtcrime.securesms.badges.models.BadgePreview.BadgeModel
            public Badge getBadge() {
                return this.badge;
            }

            @Override // org.thoughtcrime.securesms.badges.models.BadgePreview.BadgeModel
            public Recipient getRecipient() {
                return this.recipient;
            }
        }

        /* compiled from: BadgePreview.kt */
        @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0015\u0010\f\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel$SubscriptionModel;", "Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class SubscriptionModel extends BadgeModel<SubscriptionModel> {
            private final Badge badge;
            private final Recipient recipient;

            public static /* synthetic */ SubscriptionModel copy$default(SubscriptionModel subscriptionModel, Badge badge, int i, Object obj) {
                if ((i & 1) != 0) {
                    badge = subscriptionModel.getBadge();
                }
                return subscriptionModel.copy(badge);
            }

            public final Badge component1() {
                return getBadge();
            }

            public final SubscriptionModel copy(Badge badge) {
                return new SubscriptionModel(badge);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof SubscriptionModel) && Intrinsics.areEqual(getBadge(), ((SubscriptionModel) obj).getBadge());
            }

            public int hashCode() {
                if (getBadge() == null) {
                    return 0;
                }
                return getBadge().hashCode();
            }

            public String toString() {
                return "SubscriptionModel(badge=" + getBadge() + ')';
            }

            public SubscriptionModel(Badge badge) {
                super(null);
                this.badge = badge;
                Recipient self = Recipient.self();
                Intrinsics.checkNotNullExpressionValue(self, "self()");
                this.recipient = self;
            }

            @Override // org.thoughtcrime.securesms.badges.models.BadgePreview.BadgeModel
            public Badge getBadge() {
                return this.badge;
            }

            @Override // org.thoughtcrime.securesms.badges.models.BadgePreview.BadgeModel
            public Recipient getRecipient() {
                return this.recipient;
            }
        }

        /* compiled from: BadgePreview.kt */
        @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001f\u0010\r\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel$GiftedBadgeModel;", "Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;Lorg/thoughtcrime/securesms/recipients/Recipient;)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class GiftedBadgeModel extends BadgeModel<GiftedBadgeModel> {
            private final Badge badge;
            private final Recipient recipient;

            public static /* synthetic */ GiftedBadgeModel copy$default(GiftedBadgeModel giftedBadgeModel, Badge badge, Recipient recipient, int i, Object obj) {
                if ((i & 1) != 0) {
                    badge = giftedBadgeModel.getBadge();
                }
                if ((i & 2) != 0) {
                    recipient = giftedBadgeModel.getRecipient();
                }
                return giftedBadgeModel.copy(badge, recipient);
            }

            public final Badge component1() {
                return getBadge();
            }

            public final Recipient component2() {
                return getRecipient();
            }

            public final GiftedBadgeModel copy(Badge badge, Recipient recipient) {
                Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
                return new GiftedBadgeModel(badge, recipient);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof GiftedBadgeModel)) {
                    return false;
                }
                GiftedBadgeModel giftedBadgeModel = (GiftedBadgeModel) obj;
                return Intrinsics.areEqual(getBadge(), giftedBadgeModel.getBadge()) && Intrinsics.areEqual(getRecipient(), giftedBadgeModel.getRecipient());
            }

            public int hashCode() {
                return ((getBadge() == null ? 0 : getBadge().hashCode()) * 31) + getRecipient().hashCode();
            }

            public String toString() {
                return "GiftedBadgeModel(badge=" + getBadge() + ", recipient=" + getRecipient() + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public GiftedBadgeModel(Badge badge, Recipient recipient) {
                super(null);
                Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
                this.badge = badge;
                this.recipient = recipient;
            }

            @Override // org.thoughtcrime.securesms.badges.models.BadgePreview.BadgeModel
            public Badge getBadge() {
                return this.badge;
            }

            @Override // org.thoughtcrime.securesms.badges.models.BadgePreview.BadgeModel
            public Recipient getRecipient() {
                return this.recipient;
            }
        }

        public boolean areItemsTheSame(T t) {
            Intrinsics.checkNotNullParameter(t, "newItem");
            Badge badge = getBadge();
            String str = null;
            String id = badge != null ? badge.getId() : null;
            Badge badge2 = t.getBadge();
            if (badge2 != null) {
                str = badge2.getId();
            }
            return Intrinsics.areEqual(id, str) && Intrinsics.areEqual(getRecipient().getId(), t.getRecipient().getId());
        }

        public boolean areContentsTheSame(T t) {
            Intrinsics.checkNotNullParameter(t, "newItem");
            return Intrinsics.areEqual(getBadge(), t.getBadge()) && getRecipient().hasSameContent(t.getRecipient());
        }
    }

    /* compiled from: BadgePreview.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u000eR\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgePreview$ViewHolder;", "T", "Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "avatar", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "badge", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "bind", "", "model", "(Lorg/thoughtcrime/securesms/badges/models/BadgePreview$BadgeModel;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder<T extends BadgeModel<T>> extends MappingViewHolder<T> {
        private final AvatarImageView avatar;
        private final BadgeImageView badge;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.avatar)");
            this.avatar = (AvatarImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.badge);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.badge)");
            this.badge = (BadgeImageView) findViewById2;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.badges.models.BadgePreview$ViewHolder<T extends org.thoughtcrime.securesms.badges.models.BadgePreview$BadgeModel<T>> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public /* bridge */ /* synthetic */ void bind(Object obj) {
            bind((ViewHolder<T>) ((BadgeModel) obj));
        }

        public void bind(T t) {
            Intrinsics.checkNotNullParameter(t, "model");
            if (this.payload.isEmpty()) {
                this.avatar.setRecipient(t.getRecipient());
                this.avatar.disableQuickContact();
            }
            this.badge.setBadge(t.getBadge());
        }
    }
}
