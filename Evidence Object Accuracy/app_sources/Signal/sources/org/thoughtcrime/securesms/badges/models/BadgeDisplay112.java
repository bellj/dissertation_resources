package org.thoughtcrime.securesms.badges.models;

import android.view.View;
import android.widget.TextView;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.models.BadgeDisplay112;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: BadgeDisplay112.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001:\u0004\u0007\b\t\nB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgeDisplay112;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "GiftModel", "GiftViewHolder", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BadgeDisplay112 {
    public static final BadgeDisplay112 INSTANCE = new BadgeDisplay112();

    private BadgeDisplay112() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.badges.models.BadgeDisplay112$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new BadgeDisplay112.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.badge_display_112));
        mappingAdapter.registerFactory(GiftModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.badges.models.BadgeDisplay112$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new BadgeDisplay112.GiftViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.badge_display_112));
    }

    /* compiled from: BadgeDisplay112.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0000H\u0016J\u0010\u0010\r\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgeDisplay112$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "withDisplayText", "", "(Lorg/thoughtcrime/securesms/badges/models/Badge;Z)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getWithDisplayText", "()Z", "areContentsTheSame", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model implements MappingModel<Model> {
        private final Badge badge;
        private final boolean withDisplayText;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        public Model(Badge badge, boolean z) {
            Intrinsics.checkNotNullParameter(badge, "badge");
            this.badge = badge;
            this.withDisplayText = z;
        }

        public /* synthetic */ Model(Badge badge, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(badge, (i & 2) != 0 ? true : z);
        }

        public final Badge getBadge() {
            return this.badge;
        }

        public final boolean getWithDisplayText() {
            return this.withDisplayText;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.badge.getId(), model.badge.getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.badge, model.badge) && this.withDisplayText == model.withDisplayText;
        }
    }

    /* compiled from: BadgeDisplay112.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgeDisplay112$GiftModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "(Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;)V", "getGiftBadge", "()Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class GiftModel implements MappingModel<GiftModel> {
        private final GiftBadge giftBadge;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(GiftModel giftModel) {
            return MappingModel.CC.$default$getChangePayload(this, giftModel);
        }

        public GiftModel(GiftBadge giftBadge) {
            Intrinsics.checkNotNullParameter(giftBadge, "giftBadge");
            this.giftBadge = giftBadge;
        }

        public final GiftBadge getGiftBadge() {
            return this.giftBadge;
        }

        public boolean areItemsTheSame(GiftModel giftModel) {
            Intrinsics.checkNotNullParameter(giftModel, "newItem");
            return Intrinsics.areEqual(this.giftBadge.getRedemptionToken(), giftModel.giftBadge.getRedemptionToken());
        }

        public boolean areContentsTheSame(GiftModel giftModel) {
            Intrinsics.checkNotNullParameter(giftModel, "newItem");
            return Intrinsics.areEqual(this.giftBadge, giftModel.giftBadge);
        }
    }

    /* compiled from: BadgeDisplay112.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgeDisplay112$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/badges/models/BadgeDisplay112$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "badgeImageView", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "titleView", "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final BadgeImageView badgeImageView;
        private final TextView titleView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.badge);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.badge)");
            this.badgeImageView = (BadgeImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.name);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.name)");
            this.titleView = (TextView) findViewById2;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.titleView.setText(model.getBadge().getName());
            ViewExtensionsKt.setVisible(this.titleView, model.getWithDisplayText());
            this.badgeImageView.setBadge(model.getBadge());
        }
    }

    /* compiled from: BadgeDisplay112.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/BadgeDisplay112$GiftViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/badges/models/BadgeDisplay112$GiftModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "badgeImageView", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "titleView", "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class GiftViewHolder extends MappingViewHolder<GiftModel> {
        private final BadgeImageView badgeImageView;
        private final TextView titleView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public GiftViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.badge);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.badge)");
            this.badgeImageView = (BadgeImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.name);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.name)");
            this.titleView = (TextView) findViewById2;
        }

        public void bind(GiftModel giftModel) {
            Intrinsics.checkNotNullParameter(giftModel, "model");
            ViewExtensionsKt.setVisible(this.titleView, false);
            BadgeImageView badgeImageView = this.badgeImageView;
            GiftBadge giftBadge = giftModel.getGiftBadge();
            GlideRequests with = GlideApp.with(this.badgeImageView);
            Intrinsics.checkNotNullExpressionValue(with, "with(badgeImageView)");
            badgeImageView.setGiftBadge(giftBadge, with);
        }
    }
}
