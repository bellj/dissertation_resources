package org.thoughtcrime.securesms.badges.gifts;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.models.BadgeDisplay112;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;

/* compiled from: ExpiredGiftSheetConfiguration.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J(\u0010\u0007\u001a\u00020\u0004*\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\nH\u0002J.\u0010\f\u001a\u00020\u0004*\u00020\b2\u0006\u0010\r\u001a\u00020\u000e2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\nJ.\u0010\u000f\u001a\u00020\u0004*\u00020\b2\u0006\u0010\u0010\u001a\u00020\u00112\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\n¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/ExpiredGiftSheetConfiguration;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "expiredSheet", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "onMakeAMonthlyDonation", "Lkotlin/Function0;", "onNotNow", "forExpiredBadge", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "forExpiredGiftBadge", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ExpiredGiftSheetConfiguration {
    public static final ExpiredGiftSheetConfiguration INSTANCE = new ExpiredGiftSheetConfiguration();

    private ExpiredGiftSheetConfiguration() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        BadgeDisplay112.INSTANCE.register(mappingAdapter);
    }

    public final void forExpiredBadge(DSLConfiguration dSLConfiguration, Badge badge, Function0<Unit> function0, Function0<Unit> function02) {
        Intrinsics.checkNotNullParameter(dSLConfiguration, "<this>");
        Intrinsics.checkNotNullParameter(badge, "badge");
        Intrinsics.checkNotNullParameter(function0, "onMakeAMonthlyDonation");
        Intrinsics.checkNotNullParameter(function02, "onNotNow");
        dSLConfiguration.customPref(new BadgeDisplay112.Model(badge, false));
        expiredSheet(dSLConfiguration, function0, function02);
    }

    public final void forExpiredGiftBadge(DSLConfiguration dSLConfiguration, GiftBadge giftBadge, Function0<Unit> function0, Function0<Unit> function02) {
        Intrinsics.checkNotNullParameter(dSLConfiguration, "<this>");
        Intrinsics.checkNotNullParameter(giftBadge, "giftBadge");
        Intrinsics.checkNotNullParameter(function0, "onMakeAMonthlyDonation");
        Intrinsics.checkNotNullParameter(function02, "onNotNow");
        dSLConfiguration.customPref(new BadgeDisplay112.GiftModel(giftBadge));
        expiredSheet(dSLConfiguration, function0, function02);
    }

    private final void expiredSheet(DSLConfiguration dSLConfiguration, Function0<Unit> function0, Function0<Unit> function02) {
        DSLSettingsText.Companion companion = DSLSettingsText.Companion;
        DSLSettingsText.CenterModifier centerModifier = DSLSettingsText.CenterModifier.INSTANCE;
        DSLConfiguration.textPref$default(dSLConfiguration, companion.from(R.string.ExpiredGiftSheetConfiguration__your_gift_badge_has_expired, centerModifier, DSLSettingsText.TitleLargeModifier.INSTANCE), null, 2, null);
        DSLConfiguration.textPref$default(dSLConfiguration, companion.from(R.string.ExpiredGiftSheetConfiguration__your_gift_badge_has_expired_and_is, centerModifier), null, 2, null);
        if (SignalStore.donationsValues().isLikelyASustainer()) {
            DSLConfiguration.primaryButton$default(dSLConfiguration, companion.from(17039370, new DSLSettingsText.Modifier[0]), false, new Function0<Unit>(function02) { // from class: org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheetConfiguration$expiredSheet$1
                final /* synthetic */ Function0<Unit> $onNotNow;

                /* access modifiers changed from: package-private */
                {
                    this.$onNotNow = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    this.$onNotNow.invoke();
                }
            }, 2, null);
            return;
        }
        DSLConfiguration.textPref$default(dSLConfiguration, companion.from(R.string.ExpiredGiftSheetConfiguration__to_continue, centerModifier), null, 2, null);
        DSLConfiguration.primaryButton$default(dSLConfiguration, companion.from(R.string.ExpiredGiftSheetConfiguration__make_a_monthly_donation, new DSLSettingsText.Modifier[0]), false, new Function0<Unit>(function0) { // from class: org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheetConfiguration$expiredSheet$2
            final /* synthetic */ Function0<Unit> $onMakeAMonthlyDonation;

            /* access modifiers changed from: package-private */
            {
                this.$onMakeAMonthlyDonation = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                this.$onMakeAMonthlyDonation.invoke();
            }
        }, 2, null);
        DSLConfiguration.secondaryButtonNoOutline$default(dSLConfiguration, companion.from(R.string.ExpiredGiftSheetConfiguration__not_now, new DSLSettingsText.Modifier[0]), null, false, new Function0<Unit>(function02) { // from class: org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheetConfiguration$expiredSheet$3
            final /* synthetic */ Function0<Unit> $onNotNow;

            /* access modifiers changed from: package-private */
            {
                this.$onNotNow = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                this.$onNotNow.invoke();
            }
        }, 6, null);
    }
}
