package org.thoughtcrime.securesms.badges.gifts.flow;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.util.PlatformCurrencyUtil;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: GiftFlowRepository.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u0004J\u0018\u0010\b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t0\u0004¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowRepository;", "", "()V", "getGiftBadge", "Lio/reactivex/rxjava3/core/Single;", "Lkotlin/Pair;", "", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "getGiftPricing", "", "Ljava/util/Currency;", "Lorg/signal/core/util/money/FiatMoney;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GiftFlowRepository {
    public final Single<Pair<Long, Badge>> getGiftBadge() {
        Single<Pair<Long, Badge>> subscribeOn = ApplicationDependencies.getDonationsService().getGiftBadges(Locale.getDefault()).flatMap(new GiftFlowRepository$$ExternalSyntheticLambda2()).map(new Function() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRepository$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return GiftFlowRepository.$r8$lambda$YgB9wxP0a5mILtLFwCqHwXw51XY((Map) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRepository$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return GiftFlowRepository.m381$r8$lambda$8YomwYpHygNScIb_NDAnP2c4OI((List) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "getDonationsService()\n  …scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getGiftBadge$lambda-1 */
    public static final List m382getGiftBadge$lambda1(Map map) {
        Intrinsics.checkNotNullExpressionValue(map, "gifts");
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry entry : map.entrySet()) {
            arrayList.add(TuplesKt.to(entry.getKey(), Badges.fromServiceBadge((SignalServiceProfile.Badge) entry.getValue())));
        }
        return arrayList;
    }

    /* renamed from: getGiftBadge$lambda-2 */
    public static final Pair m383getGiftBadge$lambda2(List list) {
        Intrinsics.checkNotNullExpressionValue(list, "it");
        return (Pair) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) list));
    }

    public final Single<Map<Currency, FiatMoney>> getGiftPricing() {
        Single<Map<Currency, FiatMoney>> map = ApplicationDependencies.getDonationsService().getGiftAmount().subscribeOn(Schedulers.io()).flatMap(new Function() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRepository$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return GiftFlowRepository.m380$r8$lambda$1_bB7eUnMKlRC_XnyNqepil_1U((ServiceResponse) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return GiftFlowRepository.$r8$lambda$hlOT04aZ9xwB7vSNGAM25zaphAA((Map) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "getDonationsService()\n  …rice, currency) }\n      }");
        return map;
    }

    /* renamed from: getGiftPricing$lambda-3 */
    public static final SingleSource m384getGiftPricing$lambda3(ServiceResponse serviceResponse) {
        return serviceResponse.flattenResult();
    }

    /* renamed from: getGiftPricing$lambda-7 */
    public static final Map m385getGiftPricing$lambda7(Map map) {
        Intrinsics.checkNotNullExpressionValue(map, MediaSendActivityResult.EXTRA_RESULT);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry entry : map.entrySet()) {
            if (PlatformCurrencyUtil.INSTANCE.getAvailableCurrencyCodes().contains(entry.getKey())) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(MapsKt__MapsJVMKt.mapCapacity(linkedHashMap.size()));
        for (Map.Entry entry2 : linkedHashMap.entrySet()) {
            linkedHashMap2.put(Currency.getInstance((String) entry2.getKey()), entry2.getValue());
        }
        LinkedHashMap linkedHashMap3 = new LinkedHashMap(MapsKt__MapsJVMKt.mapCapacity(linkedHashMap2.size()));
        for (Map.Entry entry3 : linkedHashMap2.entrySet()) {
            linkedHashMap3.put(entry3.getKey(), new FiatMoney((BigDecimal) entry3.getValue(), (Currency) entry3.getKey()));
        }
        return linkedHashMap3;
    }
}
