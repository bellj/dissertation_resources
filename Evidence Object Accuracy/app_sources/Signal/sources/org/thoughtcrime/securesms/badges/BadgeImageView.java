package org.thoughtcrime.securesms.badges;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.glide.BadgeSpriteTransformation;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.glide.GiftBadgeModel;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ScreenDensity;
import org.thoughtcrime.securesms.util.ThemeUtil;

/* compiled from: BadgeImageView.kt */
@Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\t\u001a\u00020\nH\u0002J\n\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0002J\u0010\u0010\r\u001a\u00020\n2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fJ\u0018\u0010\r\u001a\u00020\n2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\fJ\u0010\u0010\u0011\u001a\u00020\n2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013J\u0018\u0010\u0011\u001a\u00020\n2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0010\u001a\u00020\fJ\u0018\u0010\u0014\u001a\u00020\n2\b\u0010\u000e\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0010\u001a\u00020\fJ\u0012\u0010\u0016\u001a\u00020\n2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "Landroidx/appcompat/widget/AppCompatImageView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "badgeSize", "", "clearDrawable", "", "getGlideRequests", "Lorg/thoughtcrime/securesms/mms/GlideRequests;", "setBadge", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "glideRequests", "setBadgeFromRecipient", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "setGiftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "setOnClickListener", "l", "Landroid/view/View$OnClickListener;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BadgeImageView extends AppCompatImageView {
    private int badgeSize;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public BadgeImageView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ BadgeImageView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BadgeImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.BadgeImageView);
        Intrinsics.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…styleable.BadgeImageView)");
        this.badgeSize = obtainStyledAttributes.getInt(0, 0);
        Unit unit = Unit.INSTANCE;
        obtainStyledAttributes.recycle();
        setClickable(false);
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        boolean isClickable = isClickable();
        super.setOnClickListener(onClickListener);
        setClickable(isClickable);
    }

    public final void setBadgeFromRecipient(Recipient recipient) {
        Unit unit;
        GlideRequests glideRequests = getGlideRequests();
        if (glideRequests != null) {
            setBadgeFromRecipient(recipient, glideRequests);
            unit = Unit.INSTANCE;
        } else {
            unit = null;
        }
        if (unit == null) {
            clearDrawable();
        }
    }

    public final void setBadgeFromRecipient(Recipient recipient, GlideRequests glideRequests) {
        Intrinsics.checkNotNullParameter(glideRequests, "glideRequests");
        if (recipient == null || recipient.getBadges().isEmpty()) {
            setBadge(null, glideRequests);
        } else if (recipient.isSelf()) {
            Badge featuredBadge = recipient.getFeaturedBadge();
            if (featuredBadge == null || !featuredBadge.getVisible() || featuredBadge.isExpired()) {
                setBadge(null, glideRequests);
            } else {
                setBadge(featuredBadge, glideRequests);
            }
        } else {
            setBadge(recipient.getFeaturedBadge(), glideRequests);
        }
    }

    public final void setBadge(Badge badge) {
        Unit unit;
        GlideRequests glideRequests = getGlideRequests();
        if (glideRequests != null) {
            setBadge(badge, glideRequests);
            unit = Unit.INSTANCE;
        } else {
            unit = null;
        }
        if (unit == null) {
            clearDrawable();
        }
    }

    public final void setBadge(Badge badge, GlideRequests glideRequests) {
        Intrinsics.checkNotNullParameter(glideRequests, "glideRequests");
        if (badge != null) {
            glideRequests.load((Object) badge).downsample(DownsampleStrategy.NONE).transform((Transformation<Bitmap>) new BadgeSpriteTransformation(BadgeSpriteTransformation.Size.Companion.fromInteger(this.badgeSize), badge.getImageDensity(), ThemeUtil.isDarkTheme(getContext()))).into(this);
            setClickable(true);
            return;
        }
        glideRequests.clear(this);
        clearDrawable();
    }

    public final void setGiftBadge(GiftBadge giftBadge, GlideRequests glideRequests) {
        Intrinsics.checkNotNullParameter(glideRequests, "glideRequests");
        if (giftBadge != null) {
            GlideRequest<Drawable> downsample = glideRequests.load((Object) new GiftBadgeModel(giftBadge)).downsample(DownsampleStrategy.NONE);
            BadgeSpriteTransformation.Size fromInteger = BadgeSpriteTransformation.Size.Companion.fromInteger(this.badgeSize);
            String bestDensityBucketForDevice = ScreenDensity.getBestDensityBucketForDevice();
            Intrinsics.checkNotNullExpressionValue(bestDensityBucketForDevice, "getBestDensityBucketForDevice()");
            downsample.transform((Transformation<Bitmap>) new BadgeSpriteTransformation(fromInteger, bestDensityBucketForDevice, ThemeUtil.isDarkTheme(getContext()))).into(this);
            return;
        }
        glideRequests.clear(this);
        clearDrawable();
    }

    private final void clearDrawable() {
        setImageDrawable(null);
        setClickable(false);
    }

    private final GlideRequests getGlideRequests() {
        try {
            return GlideApp.with(this);
        } catch (IllegalArgumentException unused) {
            return null;
        }
    }
}
