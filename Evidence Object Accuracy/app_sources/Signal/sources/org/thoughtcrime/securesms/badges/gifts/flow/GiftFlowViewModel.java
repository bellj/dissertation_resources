package org.thoughtcrime.securesms.badges.gifts.flow;

import android.app.Application;
import android.content.Intent;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.signal.donations.GooglePayApi;
import org.thoughtcrime.securesms.badges.gifts.Gifts;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.InternetConnectionObserver;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: GiftFlowViewModel.kt */
@Metadata(d1 = {"\u0000ª\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 C2\u00020\u0001:\u0002CDB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J4\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00192\u0016\b\u0002\u0010%\u001a\u0010\u0012\u0004\u0012\u00020'\u0012\u0004\u0012\u00020(\u0018\u00010&2\n\b\u0002\u0010)\u001a\u0004\u0018\u00010*H\u0002J\f\u0010+\u001a\b\u0012\u0004\u0012\u00020-0,J \u0010.\u001a\u00020/2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u0002012\b\u00103\u001a\u0004\u0018\u000104J\b\u00105\u001a\u00020/H\u0014J\u0010\u00106\u001a\u00020/2\u0006\u00107\u001a\u000208H\u0002J\u0006\u00109\u001a\u00020/J\u000e\u0010:\u001a\u00020/2\u0006\u0010;\u001a\u00020-J\u0006\u0010<\u001a\u00020/J\u000e\u0010=\u001a\u00020/2\u0006\u0010>\u001a\u00020?J\u000e\u0010@\u001a\u00020/2\u0006\u0010A\u001a\u00020BR\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0018\u001a\u00020\u00198F¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u0017\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00190\u001d¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0014\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00190!X\u0004¢\u0006\u0002\n\u0000¨\u0006E"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowRepository;", "donationPaymentRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "(Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowRepository;Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "getDonationPaymentRepository", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "eventPublisher", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent;", "events", "Lio/reactivex/rxjava3/core/Observable;", "getEvents", "()Lio/reactivex/rxjava3/core/Observable;", "giftToPurchase", "Lorg/thoughtcrime/securesms/badges/gifts/flow/Gift;", "networkDisposable", "Lio/reactivex/rxjava3/disposables/Disposable;", "getRepository", "()Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowRepository;", "snapshot", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState;", "getSnapshot", "()Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "getLoadState", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState$Stage;", "oldState", "giftPrices", "", "Ljava/util/Currency;", "Lorg/signal/core/util/money/FiatMoney;", "giftBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "getSupportedCurrencyCodes", "", "", "onActivityResult", "", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCleared", "onPaymentFlowError", "throwable", "", "refresh", "requestTokenFromGooglePay", EmojiSearchDatabase.LABEL, "retry", "setAdditionalMessage", "additionalMessage", "", "setSelectedContact", "selectedContact", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GiftFlowViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(GiftFlowViewModel.class);
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final DonationPaymentRepository donationPaymentRepository;
    private final PublishSubject<DonationEvent> eventPublisher;
    private final Observable<DonationEvent> events;
    private Gift giftToPurchase;
    private final Disposable networkDisposable;
    private final GiftFlowRepository repository;
    private final Flowable<GiftFlowState> state;
    private final RxStore<GiftFlowState> store;

    public final GiftFlowRepository getRepository() {
        return this.repository;
    }

    public final DonationPaymentRepository getDonationPaymentRepository() {
        return this.donationPaymentRepository;
    }

    public GiftFlowViewModel(GiftFlowRepository giftFlowRepository, DonationPaymentRepository donationPaymentRepository) {
        Intrinsics.checkNotNullParameter(giftFlowRepository, "repository");
        Intrinsics.checkNotNullParameter(donationPaymentRepository, "donationPaymentRepository");
        this.repository = giftFlowRepository;
        this.donationPaymentRepository = donationPaymentRepository;
        RxStore<GiftFlowState> rxStore = new RxStore<>(new GiftFlowState(SignalStore.donationsValues().getOneTimeCurrency(), null, null, null, null, null, null, 126, null), null, 2, null);
        this.store = rxStore;
        PublishSubject<DonationEvent> create = PublishSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.eventPublisher = create;
        this.state = rxStore.getStateFlowable();
        this.events = create;
        refresh();
        Disposable subscribe = InternetConnectionObserver.INSTANCE.observe().distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                GiftFlowViewModel.m390_init_$lambda0(GiftFlowViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "InternetConnectionObserv…retry()\n        }\n      }");
        this.networkDisposable = subscribe;
    }

    public final Flowable<GiftFlowState> getState() {
        return this.state;
    }

    public final Observable<DonationEvent> getEvents() {
        return this.events;
    }

    public final GiftFlowState getSnapshot() {
        return this.store.getState();
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m390_init_$lambda0(GiftFlowViewModel giftFlowViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(giftFlowViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(bool, "isConnected");
        if (bool.booleanValue()) {
            giftFlowViewModel.retry();
        }
    }

    public final void retry() {
        if (!this.disposables.isDisposed() && this.store.getState().getStage() == GiftFlowState.Stage.FAILURE) {
            this.store.update(GiftFlowViewModel$retry$1.INSTANCE);
            refresh();
        }
    }

    public final void refresh() {
        this.disposables.clear();
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = SignalStore.donationsValues().getObservableOneTimeCurrency().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                GiftFlowViewModel.m391refresh$lambda1(GiftFlowViewModel.this, (Currency) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "donationsValues().observ…y\n        )\n      }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
        CompositeDisposable compositeDisposable2 = this.disposables;
        Disposable subscribe2 = this.repository.getGiftPricing().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                GiftFlowViewModel.m392refresh$lambda2(GiftFlowViewModel.this, (Map) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "repository.getGiftPricin…)\n        )\n      }\n    }");
        DisposableKt.plusAssign(compositeDisposable2, subscribe2);
        DisposableKt.plusAssign(this.disposables, SubscribersKt.subscribeBy(this.repository.getGiftBadge(), new Function1<Throwable, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$refresh$3
            final /* synthetic */ GiftFlowViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Intrinsics.checkNotNullParameter(th, "throwable");
                Log.w(GiftFlowViewModel.TAG, "Could not load gift badge", th);
                this.this$0.store.update(AnonymousClass1.INSTANCE);
            }
        }, new Function1<Pair<? extends Long, ? extends Badge>, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$refresh$4
            final /* synthetic */ GiftFlowViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends Long, ? extends Badge> pair) {
                invoke((Pair<Long, Badge>) pair);
                return Unit.INSTANCE;
            }

            public final void invoke(Pair<Long, Badge> pair) {
                Intrinsics.checkNotNullParameter(pair, "<name for destructuring parameter 0>");
                final long longValue = pair.component1().longValue();
                final Badge component2 = pair.component2();
                RxStore rxStore = this.this$0.store;
                final GiftFlowViewModel giftFlowViewModel = this.this$0;
                rxStore.update(new Function1<GiftFlowState, GiftFlowState>() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$refresh$4.1
                    public final GiftFlowState invoke(GiftFlowState giftFlowState) {
                        Intrinsics.checkNotNullParameter(giftFlowState, "it");
                        Long valueOf = Long.valueOf(longValue);
                        Badge badge = component2;
                        return GiftFlowState.copy$default(giftFlowState, null, valueOf, badge, null, GiftFlowViewModel.getLoadState$default(giftFlowViewModel, giftFlowState, null, badge, 2, null), null, null, 105, null);
                    }
                });
            }
        }));
    }

    /* renamed from: refresh$lambda-1 */
    public static final void m391refresh$lambda1(GiftFlowViewModel giftFlowViewModel, Currency currency) {
        Intrinsics.checkNotNullParameter(giftFlowViewModel, "this$0");
        giftFlowViewModel.store.update(new Function1<GiftFlowState, GiftFlowState>(currency) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$refresh$1$1
            final /* synthetic */ Currency $currency;

            /* access modifiers changed from: package-private */
            {
                this.$currency = r1;
            }

            public final GiftFlowState invoke(GiftFlowState giftFlowState) {
                Intrinsics.checkNotNullParameter(giftFlowState, "it");
                Currency currency2 = this.$currency;
                Intrinsics.checkNotNullExpressionValue(currency2, "currency");
                return GiftFlowState.copy$default(giftFlowState, currency2, null, null, null, null, null, null, 126, null);
            }
        });
    }

    /* renamed from: refresh$lambda-2 */
    public static final void m392refresh$lambda2(GiftFlowViewModel giftFlowViewModel, Map map) {
        Intrinsics.checkNotNullParameter(giftFlowViewModel, "this$0");
        giftFlowViewModel.store.update(new Function1<GiftFlowState, GiftFlowState>(map, giftFlowViewModel) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$refresh$2$1
            final /* synthetic */ Map<Currency, FiatMoney> $giftPrices;
            final /* synthetic */ GiftFlowViewModel this$0;

            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.Map<java.util.Currency, ? extends org.signal.core.util.money.FiatMoney> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$giftPrices = r1;
                this.this$0 = r2;
            }

            public final GiftFlowState invoke(GiftFlowState giftFlowState) {
                Intrinsics.checkNotNullParameter(giftFlowState, "it");
                Map<Currency, FiatMoney> map2 = this.$giftPrices;
                Intrinsics.checkNotNullExpressionValue(map2, "giftPrices");
                return GiftFlowState.copy$default(giftFlowState, null, null, null, map2, GiftFlowViewModel.getLoadState$default(this.this$0, giftFlowState, this.$giftPrices, null, 4, null), null, null, 103, null);
            }
        });
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void setSelectedContact(ContactSearchKey.RecipientSearchKey recipientSearchKey) {
        Intrinsics.checkNotNullParameter(recipientSearchKey, "selectedContact");
        this.store.update(new Function1<GiftFlowState, GiftFlowState>(recipientSearchKey) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$setSelectedContact$1
            final /* synthetic */ ContactSearchKey.RecipientSearchKey $selectedContact;

            /* access modifiers changed from: package-private */
            {
                this.$selectedContact = r1;
            }

            public final GiftFlowState invoke(GiftFlowState giftFlowState) {
                Intrinsics.checkNotNullParameter(giftFlowState, "it");
                return GiftFlowState.copy$default(giftFlowState, null, null, null, null, null, Recipient.resolved(this.$selectedContact.getRecipientId()), null, 95, null);
            }
        });
    }

    public final List<String> getSupportedCurrencyCodes() {
        Set<Currency> keySet = this.store.getState().getGiftPrices().keySet();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(keySet, 10));
        for (Currency currency : keySet) {
            arrayList.add(currency.getCurrencyCode());
        }
        return arrayList;
    }

    public final void requestTokenFromGooglePay(String str) {
        Recipient recipient;
        RecipientId id;
        Intrinsics.checkNotNullParameter(str, EmojiSearchDatabase.LABEL);
        Long giftLevel = this.store.getState().getGiftLevel();
        if (giftLevel != null) {
            long longValue = giftLevel.longValue();
            FiatMoney fiatMoney = this.store.getState().getGiftPrices().get(this.store.getState().getCurrency());
            if (fiatMoney != null && (recipient = this.store.getState().getRecipient()) != null && (id = recipient.getId()) != null) {
                this.giftToPurchase = new Gift(longValue, fiatMoney);
                this.store.update(GiftFlowViewModel$requestTokenFromGooglePay$1.INSTANCE);
                CompositeDisposable compositeDisposable = this.disposables;
                Completable observeOn = this.donationPaymentRepository.verifyRecipientIsAllowedToReceiveAGift(id).observeOn(AndroidSchedulers.mainThread());
                GiftFlowViewModel$requestTokenFromGooglePay$2 giftFlowViewModel$requestTokenFromGooglePay$2 = new Function1<Throwable, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$requestTokenFromGooglePay$2
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                        invoke(th);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(Throwable th) {
                        Intrinsics.checkNotNullParameter(th, "p0");
                        ((GiftFlowViewModel) this.receiver).onPaymentFlowError(th);
                    }
                };
                Intrinsics.checkNotNullExpressionValue(observeOn, "observeOn(AndroidSchedulers.mainThread())");
                DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy(observeOn, giftFlowViewModel$requestTokenFromGooglePay$2, new Function0<Unit>(this, str) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$requestTokenFromGooglePay$3
                    final /* synthetic */ String $label;
                    final /* synthetic */ GiftFlowViewModel this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                        this.$label = r2;
                    }

                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        this.this$0.store.update(AnonymousClass1.INSTANCE);
                        DonationPaymentRepository donationPaymentRepository = this.this$0.getDonationPaymentRepository();
                        Gift gift = this.this$0.giftToPurchase;
                        Intrinsics.checkNotNull(gift);
                        donationPaymentRepository.requestTokenFromGooglePay(gift.getPrice(), this.$label, Gifts.GOOGLE_PAY_REQUEST_CODE);
                    }
                }));
            }
        }
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
        Gift gift = this.giftToPurchase;
        RecipientId recipientId = null;
        this.giftToPurchase = null;
        Recipient recipient = this.store.getState().getRecipient();
        if (recipient != null) {
            recipientId = recipient.getId();
        }
        this.donationPaymentRepository.onActivityResult(i, i2, intent, Gifts.GOOGLE_PAY_REQUEST_CODE, new GooglePayApi.PaymentRequestCallback(gift, recipientId, this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1
            final /* synthetic */ Gift $gift;
            final /* synthetic */ RecipientId $recipient;
            final /* synthetic */ GiftFlowViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.$gift = r1;
                this.$recipient = r2;
                this.this$0 = r3;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0063: INVOKE  
                  (wrap: io.reactivex.rxjava3.core.Completable : 0x0051: INVOKE  (r9v3 io.reactivex.rxjava3.core.Completable A[REMOVE]) = 
                  (r1v2 'donationPaymentRepository' org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository)
                  (r2v0 'price' org.signal.core.util.money.FiatMoney)
                  (r9v0 'paymentData' com.google.android.gms.wallet.PaymentData)
                  (r4v0 'recipientId2' org.thoughtcrime.securesms.recipients.RecipientId)
                  (wrap: java.lang.String : ?: TERNARY(r0v15 java.lang.String A[REMOVE]) = ((r0v14 'additionalMessage' java.lang.CharSequence) != (null java.lang.CharSequence)) ? (wrap: java.lang.String : 0x0043: INVOKE  (r0v19 java.lang.String A[REMOVE]) = (r0v14 'additionalMessage' java.lang.CharSequence) type: VIRTUAL call: java.lang.Object.toString():java.lang.String) : (null java.lang.String))
                  (wrap: long : 0x004c: INVOKE  (r6v0 long A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.badges.gifts.flow.Gift : 0x004a: IGET  (r0v16 org.thoughtcrime.securesms.badges.gifts.flow.Gift A[REMOVE]) = (r8v0 'this' org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1.$gift org.thoughtcrime.securesms.badges.gifts.flow.Gift)
                 type: VIRTUAL call: org.thoughtcrime.securesms.badges.gifts.flow.Gift.getLevel():long)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository.continuePayment(org.signal.core.util.money.FiatMoney, com.google.android.gms.wallet.PaymentData, org.thoughtcrime.securesms.recipients.RecipientId, java.lang.String, long):io.reactivex.rxjava3.core.Completable)
                  (wrap: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$2 : 0x0059: CONSTRUCTOR  (r0v17 org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$2 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel : 0x0057: IGET  (r1v3 org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel A[REMOVE]) = (r8v0 'this' org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1.this$0 org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel)
                 call: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$2.<init>(java.lang.Object):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$3 : 0x0060: CONSTRUCTOR  (r1v4 org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$3 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel : 0x005e: IGET  (r2v1 org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel A[REMOVE]) = (r8v0 'this' org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1.this$0 org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel)
                 call: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$3.<init>(org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel):void type: CONSTRUCTOR)
                 type: STATIC call: io.reactivex.rxjava3.kotlin.SubscribersKt.subscribeBy(io.reactivex.rxjava3.core.Completable, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function0):io.reactivex.rxjava3.disposables.Disposable in method: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1.onSuccess(com.google.android.gms.wallet.PaymentData):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0059: CONSTRUCTOR  (r0v17 org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$2 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel : 0x0057: IGET  (r1v3 org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel A[REMOVE]) = (r8v0 'this' org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1 A[IMMUTABLE_TYPE, THIS]) org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1.this$0 org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel)
                 call: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$2.<init>(java.lang.Object):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1.onSuccess(com.google.android.gms.wallet.PaymentData):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 14 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$2, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 20 more
                */
            @Override // org.signal.donations.GooglePayApi.PaymentRequestCallback
            public void onSuccess(com.google.android.gms.wallet.PaymentData r9) {
                /*
                    r8 = this;
                    java.lang.String r0 = "paymentData"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r9, r0)
                    org.thoughtcrime.securesms.badges.gifts.flow.Gift r0 = r8.$gift
                    if (r0 == 0) goto L_0x0067
                    org.thoughtcrime.securesms.recipients.RecipientId r0 = r8.$recipient
                    if (r0 == 0) goto L_0x0067
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel r0 = r8.this$0
                    io.reactivex.rxjava3.subjects.PublishSubject r0 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel.access$getEventPublisher$p(r0)
                    org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent$RequestTokenSuccess r1 = org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent.RequestTokenSuccess.INSTANCE
                    r0.onNext(r1)
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel r0 = r8.this$0
                    org.thoughtcrime.securesms.util.rx.RxStore r0 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel.access$getStore$p(r0)
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$1 r1 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$1.INSTANCE
                    r0.update(r1)
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel r0 = r8.this$0
                    org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository r1 = r0.getDonationPaymentRepository()
                    org.thoughtcrime.securesms.badges.gifts.flow.Gift r0 = r8.$gift
                    org.signal.core.util.money.FiatMoney r2 = r0.getPrice()
                    org.thoughtcrime.securesms.recipients.RecipientId r4 = r8.$recipient
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel r0 = r8.this$0
                    org.thoughtcrime.securesms.util.rx.RxStore r0 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel.access$getStore$p(r0)
                    java.lang.Object r0 = r0.getState()
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState r0 = (org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState) r0
                    java.lang.CharSequence r0 = r0.getAdditionalMessage()
                    if (r0 == 0) goto L_0x0048
                    java.lang.String r0 = r0.toString()
                    goto L_0x0049
                L_0x0048:
                    r0 = 0
                L_0x0049:
                    r5 = r0
                    org.thoughtcrime.securesms.badges.gifts.flow.Gift r0 = r8.$gift
                    long r6 = r0.getLevel()
                    r3 = r9
                    io.reactivex.rxjava3.core.Completable r9 = r1.continuePayment(r2, r3, r4, r5, r6)
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$2 r0 = new org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$2
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel r1 = r8.this$0
                    r0.<init>(r1)
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$3 r1 = new org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$3
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel r2 = r8.this$0
                    r1.<init>(r2)
                    io.reactivex.rxjava3.kotlin.SubscribersKt.subscribeBy(r9, r0, r1)
                    goto L_0x0072
                L_0x0067:
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel r9 = r8.this$0
                    org.thoughtcrime.securesms.util.rx.RxStore r9 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel.access$getStore$p(r9)
                    org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$4 r0 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1$onSuccess$4.INSTANCE
                    r9.update(r0)
                L_0x0072:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$onActivityResult$1.onSuccess(com.google.android.gms.wallet.PaymentData):void");
            }

            @Override // org.signal.donations.GooglePayApi.PaymentRequestCallback
            public void onError(GooglePayApi.GooglePayException googlePayException) {
                Intrinsics.checkNotNullParameter(googlePayException, "googlePayException");
                this.this$0.store.update(GiftFlowViewModel$onActivityResult$1$onError$1.INSTANCE);
                DonationError.Companion companion = DonationError.Companion;
                Application application = ApplicationDependencies.getApplication();
                Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
                companion.routeDonationError(application, companion.getGooglePayRequestTokenError(DonationErrorSource.GIFT, googlePayException));
            }

            @Override // org.signal.donations.GooglePayApi.PaymentRequestCallback
            public void onCancelled() {
                this.this$0.store.update(GiftFlowViewModel$onActivityResult$1$onCancelled$1.INSTANCE);
            }
        });
    }

    public final void onPaymentFlowError(Throwable th) {
        DonationError donationError;
        this.store.update(GiftFlowViewModel$onPaymentFlowError$1.INSTANCE);
        if (th instanceof DonationError) {
            donationError = (DonationError) th;
        } else {
            Log.w(TAG, "Failed to complete payment or redemption", th, true);
            donationError = DonationError.Companion.genericBadgeRedemptionFailure(DonationErrorSource.GIFT);
        }
        DonationError.Companion companion = DonationError.Companion;
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        companion.routeDonationError(application, donationError);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ GiftFlowState.Stage getLoadState$default(GiftFlowViewModel giftFlowViewModel, GiftFlowState giftFlowState, Map map, Badge badge, int i, Object obj) {
        if ((i & 2) != 0) {
            map = null;
        }
        if ((i & 4) != 0) {
            badge = null;
        }
        return giftFlowViewModel.getLoadState(giftFlowState, map, badge);
    }

    private final GiftFlowState.Stage getLoadState(GiftFlowState giftFlowState, Map<Currency, ? extends FiatMoney> map, Badge badge) {
        GiftFlowState.Stage stage = giftFlowState.getStage();
        GiftFlowState.Stage stage2 = GiftFlowState.Stage.INIT;
        if (stage != stage2) {
            return giftFlowState.getStage();
        }
        boolean z = false;
        if (map != null && (!map.isEmpty())) {
            z = true;
        }
        return z ? giftFlowState.getGiftBadge() != null ? GiftFlowState.Stage.READY : stage2 : (badge == null || !(giftFlowState.getGiftPrices().isEmpty() ^ true)) ? stage2 : GiftFlowState.Stage.READY;
    }

    public final void setAdditionalMessage(CharSequence charSequence) {
        Intrinsics.checkNotNullParameter(charSequence, "additionalMessage");
        this.store.update(new Function1<GiftFlowState, GiftFlowState>(charSequence) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel$setAdditionalMessage$1
            final /* synthetic */ CharSequence $additionalMessage;

            /* access modifiers changed from: package-private */
            {
                this.$additionalMessage = r1;
            }

            public final GiftFlowState invoke(GiftFlowState giftFlowState) {
                Intrinsics.checkNotNullParameter(giftFlowState, "it");
                return GiftFlowState.copy$default(giftFlowState, null, null, null, null, null, null, this.$additionalMessage, 63, null);
            }
        });
    }

    /* compiled from: GiftFlowViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowViewModel$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* compiled from: GiftFlowViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowRepository;", "donationPaymentRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "(Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowRepository;Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final DonationPaymentRepository donationPaymentRepository;
        private final GiftFlowRepository repository;

        public Factory(GiftFlowRepository giftFlowRepository, DonationPaymentRepository donationPaymentRepository) {
            Intrinsics.checkNotNullParameter(giftFlowRepository, "repository");
            Intrinsics.checkNotNullParameter(donationPaymentRepository, "donationPaymentRepository");
            this.repository = giftFlowRepository;
            this.donationPaymentRepository = donationPaymentRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new GiftFlowViewModel(this.repository, this.donationPaymentRepository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel.Factory.create");
        }
    }
}
