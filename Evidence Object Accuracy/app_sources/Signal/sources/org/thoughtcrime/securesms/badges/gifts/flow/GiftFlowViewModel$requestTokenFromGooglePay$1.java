package org.thoughtcrime.securesms.badges.gifts.flow;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState;

/* compiled from: GiftFlowViewModel.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState;", "it", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GiftFlowViewModel$requestTokenFromGooglePay$1 extends Lambda implements Function1<GiftFlowState, GiftFlowState> {
    public static final GiftFlowViewModel$requestTokenFromGooglePay$1 INSTANCE = new GiftFlowViewModel$requestTokenFromGooglePay$1();

    GiftFlowViewModel$requestTokenFromGooglePay$1() {
        super(1);
    }

    public final GiftFlowState invoke(GiftFlowState giftFlowState) {
        Intrinsics.checkNotNullParameter(giftFlowState, "it");
        return GiftFlowState.copy$default(giftFlowState, null, null, null, null, GiftFlowState.Stage.RECIPIENT_VERIFICATION, null, null, 111, null);
    }
}
