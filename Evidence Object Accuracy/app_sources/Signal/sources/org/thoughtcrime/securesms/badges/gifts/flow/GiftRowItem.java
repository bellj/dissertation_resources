package org.thoughtcrime.securesms.badges.gifts.flow;

import android.view.View;
import android.widget.TextView;
import j$.util.function.Function;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftRowItem;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.payments.FiatMoneyUtil;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: GiftRowItem.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftRowItem;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GiftRowItem {
    public static final GiftRowItem INSTANCE = new GiftRowItem();

    private GiftRowItem() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftRowItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new GiftRowItem.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.subscription_preference));
    }

    /* compiled from: GiftRowItem.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016J\u0010\u0010\u000e\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftRowItem$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "giftBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "price", "Lorg/signal/core/util/money/FiatMoney;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;Lorg/signal/core/util/money/FiatMoney;)V", "getGiftBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getPrice", "()Lorg/signal/core/util/money/FiatMoney;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model implements MappingModel<Model> {
        private final Badge giftBadge;
        private final FiatMoney price;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        public Model(Badge badge, FiatMoney fiatMoney) {
            Intrinsics.checkNotNullParameter(badge, "giftBadge");
            Intrinsics.checkNotNullParameter(fiatMoney, "price");
            this.giftBadge = badge;
            this.price = fiatMoney;
        }

        public final Badge getGiftBadge() {
            return this.giftBadge;
        }

        public final FiatMoney getPrice() {
            return this.price;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.giftBadge.getId(), model.giftBadge.getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.giftBadge, model.giftBadge) && Intrinsics.areEqual(this.price, model.price);
        }
    }

    /* compiled from: GiftRowItem.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0002H\u0016R\u0016\u0010\u0006\u001a\n \b*\u0004\u0018\u00010\u00070\u0007X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \b*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n \b*\u0004\u0018\u00010\u000b0\u000bX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\n \b*\u0004\u0018\u00010\u000b0\u000bX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n \b*\u0004\u0018\u00010\u000b0\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftRowItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftRowItem$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "badgeView", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "kotlin.jvm.PlatformType", "checkView", "priceView", "Landroid/widget/TextView;", "taglineView", "titleView", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final BadgeImageView badgeView;
        private final View checkView;
        private final TextView priceView;
        private final TextView taglineView;
        private final TextView titleView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            this.badgeView = (BadgeImageView) view.findViewById(R.id.badge);
            this.titleView = (TextView) view.findViewById(R.id.title);
            this.checkView = view.findViewById(R.id.check);
            this.taglineView = (TextView) view.findViewById(R.id.tagline);
            this.priceView = (TextView) view.findViewById(R.id.price);
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            View view = this.checkView;
            Intrinsics.checkNotNullExpressionValue(view, "checkView");
            ViewExtensionsKt.setVisible(view, false);
            this.badgeView.setBadge(model.getGiftBadge());
            this.titleView.setText(model.getGiftBadge().getName());
            this.taglineView.setText(R.string.GiftRowItem__send_a_gift_badge);
            String format = FiatMoneyUtil.format(this.context.getResources(), model.getPrice(), FiatMoneyUtil.formatOptions().trimZerosAfterDecimal().withDisplayTime(false));
            Intrinsics.checkNotNullExpressionValue(format, "format(\n        context.…isplayTime(false)\n      )");
            long days = TimeUnit.MILLISECONDS.toDays(model.getGiftBadge().getDuration());
            this.priceView.setText(this.context.getResources().getQuantityString(R.plurals.GiftRowItem_s_dot_d_day_duration, (int) days, format, Long.valueOf(days)));
        }
    }
}
