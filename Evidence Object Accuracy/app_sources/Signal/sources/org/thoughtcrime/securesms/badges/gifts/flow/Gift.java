package org.thoughtcrime.securesms.badges.gifts.flow;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.money.FiatMoney;

/* compiled from: Gift.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/Gift;", "", "level", "", "price", "Lorg/signal/core/util/money/FiatMoney;", "(JLorg/signal/core/util/money/FiatMoney;)V", "getLevel", "()J", "getPrice", "()Lorg/signal/core/util/money/FiatMoney;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class Gift {
    private final long level;
    private final FiatMoney price;

    public static /* synthetic */ Gift copy$default(Gift gift, long j, FiatMoney fiatMoney, int i, Object obj) {
        if ((i & 1) != 0) {
            j = gift.level;
        }
        if ((i & 2) != 0) {
            fiatMoney = gift.price;
        }
        return gift.copy(j, fiatMoney);
    }

    public final long component1() {
        return this.level;
    }

    public final FiatMoney component2() {
        return this.price;
    }

    public final Gift copy(long j, FiatMoney fiatMoney) {
        Intrinsics.checkNotNullParameter(fiatMoney, "price");
        return new Gift(j, fiatMoney);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Gift)) {
            return false;
        }
        Gift gift = (Gift) obj;
        return this.level == gift.level && Intrinsics.areEqual(this.price, gift.price);
    }

    public int hashCode() {
        return (SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.level) * 31) + this.price.hashCode();
    }

    public String toString() {
        return "Gift(level=" + this.level + ", price=" + this.price + ')';
    }

    public Gift(long j, FiatMoney fiatMoney) {
        Intrinsics.checkNotNullParameter(fiatMoney, "price");
        this.level = j;
        this.price = fiatMoney;
    }

    public final long getLevel() {
        return this.level;
    }

    public final FiatMoney getPrice() {
        return this.price;
    }
}
