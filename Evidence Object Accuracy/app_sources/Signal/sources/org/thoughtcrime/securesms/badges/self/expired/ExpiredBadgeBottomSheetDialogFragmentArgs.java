package org.thoughtcrime.securesms.badges.self.expired;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.badges.models.Badge;

/* loaded from: classes3.dex */
public class ExpiredBadgeBottomSheetDialogFragmentArgs {
    private final HashMap arguments;

    private ExpiredBadgeBottomSheetDialogFragmentArgs() {
        this.arguments = new HashMap();
    }

    private ExpiredBadgeBottomSheetDialogFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static ExpiredBadgeBottomSheetDialogFragmentArgs fromBundle(Bundle bundle) {
        ExpiredBadgeBottomSheetDialogFragmentArgs expiredBadgeBottomSheetDialogFragmentArgs = new ExpiredBadgeBottomSheetDialogFragmentArgs();
        bundle.setClassLoader(ExpiredBadgeBottomSheetDialogFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("badge")) {
            throw new IllegalArgumentException("Required argument \"badge\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(Badge.class) || Serializable.class.isAssignableFrom(Badge.class)) {
            Badge badge = (Badge) bundle.get("badge");
            if (badge != null) {
                expiredBadgeBottomSheetDialogFragmentArgs.arguments.put("badge", badge);
                if (bundle.containsKey("cancelation_reason")) {
                    expiredBadgeBottomSheetDialogFragmentArgs.arguments.put("cancelation_reason", bundle.getString("cancelation_reason"));
                    if (bundle.containsKey("charge_failure")) {
                        expiredBadgeBottomSheetDialogFragmentArgs.arguments.put("charge_failure", bundle.getString("charge_failure"));
                        return expiredBadgeBottomSheetDialogFragmentArgs;
                    }
                    throw new IllegalArgumentException("Required argument \"charge_failure\" is missing and does not have an android:defaultValue");
                }
                throw new IllegalArgumentException("Required argument \"cancelation_reason\" is missing and does not have an android:defaultValue");
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(Badge.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public Badge getBadge() {
        return (Badge) this.arguments.get("badge");
    }

    public String getCancelationReason() {
        return (String) this.arguments.get("cancelation_reason");
    }

    public String getChargeFailure() {
        return (String) this.arguments.get("charge_failure");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("badge")) {
            Badge badge = (Badge) this.arguments.get("badge");
            if (Parcelable.class.isAssignableFrom(Badge.class) || badge == null) {
                bundle.putParcelable("badge", (Parcelable) Parcelable.class.cast(badge));
            } else if (Serializable.class.isAssignableFrom(Badge.class)) {
                bundle.putSerializable("badge", (Serializable) Serializable.class.cast(badge));
            } else {
                throw new UnsupportedOperationException(Badge.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        if (this.arguments.containsKey("cancelation_reason")) {
            bundle.putString("cancelation_reason", (String) this.arguments.get("cancelation_reason"));
        }
        if (this.arguments.containsKey("charge_failure")) {
            bundle.putString("charge_failure", (String) this.arguments.get("charge_failure"));
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ExpiredBadgeBottomSheetDialogFragmentArgs expiredBadgeBottomSheetDialogFragmentArgs = (ExpiredBadgeBottomSheetDialogFragmentArgs) obj;
        if (this.arguments.containsKey("badge") != expiredBadgeBottomSheetDialogFragmentArgs.arguments.containsKey("badge")) {
            return false;
        }
        if (getBadge() == null ? expiredBadgeBottomSheetDialogFragmentArgs.getBadge() != null : !getBadge().equals(expiredBadgeBottomSheetDialogFragmentArgs.getBadge())) {
            return false;
        }
        if (this.arguments.containsKey("cancelation_reason") != expiredBadgeBottomSheetDialogFragmentArgs.arguments.containsKey("cancelation_reason")) {
            return false;
        }
        if (getCancelationReason() == null ? expiredBadgeBottomSheetDialogFragmentArgs.getCancelationReason() != null : !getCancelationReason().equals(expiredBadgeBottomSheetDialogFragmentArgs.getCancelationReason())) {
            return false;
        }
        if (this.arguments.containsKey("charge_failure") != expiredBadgeBottomSheetDialogFragmentArgs.arguments.containsKey("charge_failure")) {
            return false;
        }
        return getChargeFailure() == null ? expiredBadgeBottomSheetDialogFragmentArgs.getChargeFailure() == null : getChargeFailure().equals(expiredBadgeBottomSheetDialogFragmentArgs.getChargeFailure());
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((getBadge() != null ? getBadge().hashCode() : 0) + 31) * 31) + (getCancelationReason() != null ? getCancelationReason().hashCode() : 0)) * 31;
        if (getChargeFailure() != null) {
            i = getChargeFailure().hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "ExpiredBadgeBottomSheetDialogFragmentArgs{badge=" + getBadge() + ", cancelationReason=" + getCancelationReason() + ", chargeFailure=" + getChargeFailure() + "}";
    }

    /* loaded from: classes3.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(ExpiredBadgeBottomSheetDialogFragmentArgs expiredBadgeBottomSheetDialogFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(expiredBadgeBottomSheetDialogFragmentArgs.arguments);
        }

        public Builder(Badge badge, String str, String str2) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (badge != null) {
                hashMap.put("badge", badge);
                hashMap.put("cancelation_reason", str);
                hashMap.put("charge_failure", str2);
                return;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        }

        public ExpiredBadgeBottomSheetDialogFragmentArgs build() {
            return new ExpiredBadgeBottomSheetDialogFragmentArgs(this.arguments);
        }

        public Builder setBadge(Badge badge) {
            if (badge != null) {
                this.arguments.put("badge", badge);
                return this;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        }

        public Builder setCancelationReason(String str) {
            this.arguments.put("cancelation_reason", str);
            return this;
        }

        public Builder setChargeFailure(String str) {
            this.arguments.put("charge_failure", str);
            return this;
        }

        public Badge getBadge() {
            return (Badge) this.arguments.get("badge");
        }

        public String getCancelationReason() {
            return (String) this.arguments.get("cancelation_reason");
        }

        public String getChargeFailure() {
            return (String) this.arguments.get("charge_failure");
        }
    }
}
