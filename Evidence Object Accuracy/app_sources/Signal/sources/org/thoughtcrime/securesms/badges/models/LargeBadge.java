package org.thoughtcrime.securesms.badges.models;

import android.view.View;
import android.widget.TextView;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: LargeBadge.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\b\u0018\u0000 \u00102\u00020\u0001:\u0005\u0010\u0011\u0012\u0013\u0014B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/LargeBadge;", "", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Companion", "EmptyModel", "EmptyViewHolder", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class LargeBadge {
    public static final Companion Companion = new Companion(null);
    private final Badge badge;

    /* compiled from: LargeBadge.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016J\u0010\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/LargeBadge$EmptyModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "()V", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class EmptyModel implements MappingModel<EmptyModel> {
        public boolean areContentsTheSame(EmptyModel emptyModel) {
            Intrinsics.checkNotNullParameter(emptyModel, "newItem");
            return true;
        }

        public boolean areItemsTheSame(EmptyModel emptyModel) {
            Intrinsics.checkNotNullParameter(emptyModel, "newItem");
            return true;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(EmptyModel emptyModel) {
            return MappingModel.CC.$default$getChangePayload(this, emptyModel);
        }
    }

    public static /* synthetic */ LargeBadge copy$default(LargeBadge largeBadge, Badge badge, int i, Object obj) {
        if ((i & 1) != 0) {
            badge = largeBadge.badge;
        }
        return largeBadge.copy(badge);
    }

    public final Badge component1() {
        return this.badge;
    }

    public final LargeBadge copy(Badge badge) {
        Intrinsics.checkNotNullParameter(badge, "badge");
        return new LargeBadge(badge);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof LargeBadge) && Intrinsics.areEqual(this.badge, ((LargeBadge) obj).badge);
    }

    public int hashCode() {
        return this.badge.hashCode();
    }

    public String toString() {
        return "LargeBadge(badge=" + this.badge + ')';
    }

    public LargeBadge(Badge badge) {
        Intrinsics.checkNotNullParameter(badge, "badge");
        this.badge = badge;
    }

    public final Badge getBadge() {
        return this.badge;
    }

    /* compiled from: LargeBadge.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0000H\u0016J\u0010\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/LargeBadge$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "largeBadge", "Lorg/thoughtcrime/securesms/badges/models/LargeBadge;", "shortName", "", "maxLines", "", "(Lorg/thoughtcrime/securesms/badges/models/LargeBadge;Ljava/lang/String;I)V", "getLargeBadge", "()Lorg/thoughtcrime/securesms/badges/models/LargeBadge;", "getMaxLines", "()I", "getShortName", "()Ljava/lang/String;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model implements MappingModel<Model> {
        private final LargeBadge largeBadge;
        private final int maxLines;
        private final String shortName;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        public Model(LargeBadge largeBadge, String str, int i) {
            Intrinsics.checkNotNullParameter(largeBadge, "largeBadge");
            Intrinsics.checkNotNullParameter(str, "shortName");
            this.largeBadge = largeBadge;
            this.shortName = str;
            this.maxLines = i;
        }

        public final LargeBadge getLargeBadge() {
            return this.largeBadge;
        }

        public final int getMaxLines() {
            return this.maxLines;
        }

        public final String getShortName() {
            return this.shortName;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(model.largeBadge.getBadge().getId(), this.largeBadge.getBadge().getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(model.largeBadge, this.largeBadge) && Intrinsics.areEqual(model.shortName, this.shortName) && model.maxLines == this.maxLines;
        }
    }

    /* compiled from: LargeBadge.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/LargeBadge$EmptyViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/badges/models/LargeBadge$EmptyModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class EmptyViewHolder extends MappingViewHolder<EmptyModel> {
        public void bind(EmptyModel emptyModel) {
            Intrinsics.checkNotNullParameter(emptyModel, "model");
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public EmptyViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }
    }

    /* compiled from: LargeBadge.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/LargeBadge$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/badges/models/LargeBadge$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "badge", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "description", "Landroid/widget/TextView;", "name", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final BadgeImageView badge;
        private final TextView description;
        private final TextView name;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.badge);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.badge)");
            this.badge = (BadgeImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.name);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.name)");
            this.name = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.description);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.description)");
            this.description = (TextView) findViewById3;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.badge.setBadge(model.getLargeBadge().getBadge());
            this.name.setText(model.getLargeBadge().getBadge().getName());
            this.description.setText(model.getLargeBadge().getBadge().resolveDescription(model.getShortName()));
            this.description.setLines(model.getMaxLines());
            this.description.setMaxLines(model.getMaxLines());
            this.description.setMinLines(model.getMaxLines());
        }
    }

    /* compiled from: LargeBadge.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/LargeBadge$Companion;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        /* renamed from: register$lambda-0 */
        public static final MappingViewHolder m425register$lambda0(View view) {
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new ViewHolder(view);
        }

        public final void register(MappingAdapter mappingAdapter) {
            Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
            mappingAdapter.registerFactory(Model.class, new LayoutFactory(new LargeBadge$Companion$$ExternalSyntheticLambda0(), R.layout.view_badge_bottom_sheet_dialog_fragment_page));
            mappingAdapter.registerFactory(EmptyModel.class, new LayoutFactory(new LargeBadge$Companion$$ExternalSyntheticLambda1(), R.layout.view_badge_bottom_sheet_dialog_fragment_page));
        }

        /* renamed from: register$lambda-1 */
        public static final MappingViewHolder m426register$lambda1(View view) {
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new EmptyViewHolder(view);
        }
    }
}
