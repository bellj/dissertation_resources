package org.thoughtcrime.securesms.badges.self.featured;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.models.Badge;

/* compiled from: SelectFeaturedBadgeState.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u001aB+\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007HÆ\u0003J/\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeState;", "", "stage", "Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeState$Stage;", "selectedBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "allUnlockedBadges", "", "(Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeState$Stage;Lorg/thoughtcrime/securesms/badges/models/Badge;Ljava/util/List;)V", "getAllUnlockedBadges", "()Ljava/util/List;", "getSelectedBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getStage", "()Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeState$Stage;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Stage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class SelectFeaturedBadgeState {
    private final List<Badge> allUnlockedBadges;
    private final Badge selectedBadge;
    private final Stage stage;

    /* compiled from: SelectFeaturedBadgeState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeState$Stage;", "", "(Ljava/lang/String;I)V", "INIT", "READY", "SAVING", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum Stage {
        INIT,
        READY,
        SAVING
    }

    public SelectFeaturedBadgeState() {
        this(null, null, null, 7, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SelectFeaturedBadgeState copy$default(SelectFeaturedBadgeState selectFeaturedBadgeState, Stage stage, Badge badge, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            stage = selectFeaturedBadgeState.stage;
        }
        if ((i & 2) != 0) {
            badge = selectFeaturedBadgeState.selectedBadge;
        }
        if ((i & 4) != 0) {
            list = selectFeaturedBadgeState.allUnlockedBadges;
        }
        return selectFeaturedBadgeState.copy(stage, badge, list);
    }

    public final Stage component1() {
        return this.stage;
    }

    public final Badge component2() {
        return this.selectedBadge;
    }

    public final List<Badge> component3() {
        return this.allUnlockedBadges;
    }

    public final SelectFeaturedBadgeState copy(Stage stage, Badge badge, List<Badge> list) {
        Intrinsics.checkNotNullParameter(stage, "stage");
        Intrinsics.checkNotNullParameter(list, "allUnlockedBadges");
        return new SelectFeaturedBadgeState(stage, badge, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SelectFeaturedBadgeState)) {
            return false;
        }
        SelectFeaturedBadgeState selectFeaturedBadgeState = (SelectFeaturedBadgeState) obj;
        return this.stage == selectFeaturedBadgeState.stage && Intrinsics.areEqual(this.selectedBadge, selectFeaturedBadgeState.selectedBadge) && Intrinsics.areEqual(this.allUnlockedBadges, selectFeaturedBadgeState.allUnlockedBadges);
    }

    public int hashCode() {
        int hashCode = this.stage.hashCode() * 31;
        Badge badge = this.selectedBadge;
        return ((hashCode + (badge == null ? 0 : badge.hashCode())) * 31) + this.allUnlockedBadges.hashCode();
    }

    public String toString() {
        return "SelectFeaturedBadgeState(stage=" + this.stage + ", selectedBadge=" + this.selectedBadge + ", allUnlockedBadges=" + this.allUnlockedBadges + ')';
    }

    public SelectFeaturedBadgeState(Stage stage, Badge badge, List<Badge> list) {
        Intrinsics.checkNotNullParameter(stage, "stage");
        Intrinsics.checkNotNullParameter(list, "allUnlockedBadges");
        this.stage = stage;
        this.selectedBadge = badge;
        this.allUnlockedBadges = list;
    }

    public /* synthetic */ SelectFeaturedBadgeState(Stage stage, Badge badge, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? Stage.INIT : stage, (i & 2) != 0 ? null : badge, (i & 4) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list);
    }

    public final Stage getStage() {
        return this.stage;
    }

    public final Badge getSelectedBadge() {
        return this.selectedBadge;
    }

    public final List<Badge> getAllUnlockedBadges() {
        return this.allUnlockedBadges;
    }
}
