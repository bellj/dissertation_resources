package org.thoughtcrime.securesms.badges.gifts.flow;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.FragmentKt;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.util.ArrayList;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftRowItem;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.InputAwareLayout;
import org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout;
import org.thoughtcrime.securesms.components.emoji.EmojiEventListener;
import org.thoughtcrime.securesms.components.emoji.MediaKeyboard;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.RecipientPreference;
import org.thoughtcrime.securesms.components.settings.models.TextInput;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.keyboard.KeyboardPage;
import org.thoughtcrime.securesms.keyboard.KeyboardPagerViewModel;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Debouncer;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: GiftFlowConfirmationFragment.kt */
@Metadata(bv = {}, d1 = {"\u0000¢\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 F2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0001FB\u0007¢\u0006\u0004\bD\u0010EJ\u0010\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005H\u0002J\b\u0010\n\u001a\u00020\tH\u0002J\u0012\u0010\r\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0002J\u0010\u0010\u0010\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u000eH\u0016J\b\u0010\u0011\u001a\u00020\tH\u0016J\b\u0010\u0012\u001a\u00020\tH\u0016J\b\u0010\u0013\u001a\u00020\tH\u0016J\b\u0010\u0014\u001a\u00020\tH\u0016J\u0012\u0010\u0017\u001a\u00020\t2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015H\u0016J\u0012\u0010\u001a\u001a\u00020\t2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018H\u0016R\u001b\u0010 \u001a\u00020\u001b8BX\u0002¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010%\u001a\u00020!8BX\u0002¢\u0006\f\n\u0004\b\"\u0010\u001d\u001a\u0004\b#\u0010$R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X.¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X.¢\u0006\u0006\n\u0004\b*\u0010+R\u0014\u0010-\u001a\u00020,8\u0002X\u0004¢\u0006\u0006\n\u0004\b-\u0010.R\u0018\u00100\u001a\u0004\u0018\u00010/8\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b0\u00101R\u0016\u00103\u001a\u0002028\u0002@\u0002X.¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00105\u001a\u0002028\u0002@\u0002X.¢\u0006\u0006\n\u0004\b5\u00104R\u0016\u00107\u001a\u0002068\u0002@\u0002X.¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u0010:\u001a\u0002098\u0002@\u0002X.¢\u0006\u0006\n\u0004\b:\u0010;R8\u0010?\u001a&\u0012\f\u0012\n >*\u0004\u0018\u00010=0= >*\u0012\u0012\f\u0012\n >*\u0004\u0018\u00010=0=\u0018\u00010<0<8\u0002X\u0004¢\u0006\u0006\n\u0004\b?\u0010@R\u0014\u0010B\u001a\u00020A8\u0002X\u0004¢\u0006\u0006\n\u0004\bB\u0010C¨\u0006G"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowConfirmationFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment$Callback;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiEventListener;", "Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchFragment$Callback;", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState;", "giftFlowState", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "getConfiguration", "", "onPaymentConfirmed", "", "throwable", "onPaymentError", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "adapter", "bindAdapter", "onDestroyView", "onToolbarNavigationClicked", "openEmojiSearch", "closeEmojiSearch", "", "emoji", "onEmojiSelected", "Landroid/view/KeyEvent;", "keyEvent", "onKeyEvent", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowViewModel;", "viewModel", "Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerViewModel;", "keyboardPagerViewModel$delegate", "getKeyboardPagerViewModel", "()Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerViewModel;", "keyboardPagerViewModel", "Lorg/thoughtcrime/securesms/components/InputAwareLayout;", "inputAwareLayout", "Lorg/thoughtcrime/securesms/components/InputAwareLayout;", "Lorg/thoughtcrime/securesms/components/emoji/MediaKeyboard;", "emojiKeyboard", "Lorg/thoughtcrime/securesms/components/emoji/MediaKeyboard;", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "Landroid/content/DialogInterface;", "errorDialog", "Landroid/content/DialogInterface;", "Landroidx/appcompat/app/AlertDialog;", "processingDonationPaymentDialog", "Landroidx/appcompat/app/AlertDialog;", "verifyingRecipientDonationPaymentDialog", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent;", "donationPaymentComponent", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent;", "Lorg/thoughtcrime/securesms/components/settings/models/TextInput$MultilineViewHolder;", "textInputViewHolder", "Lorg/thoughtcrime/securesms/components/settings/models/TextInput$MultilineViewHolder;", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lorg/thoughtcrime/securesms/components/settings/models/TextInput$TextInputEvent;", "kotlin.jvm.PlatformType", "eventPublisher", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lorg/thoughtcrime/securesms/util/Debouncer;", "debouncer", "Lorg/thoughtcrime/securesms/util/Debouncer;", "<init>", "()V", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class GiftFlowConfirmationFragment extends DSLSettingsFragment implements EmojiKeyboardPageFragment.Callback, EmojiEventListener, EmojiSearchFragment.Callback {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(GiftFlowConfirmationFragment.class);
    private final Debouncer debouncer = new Debouncer(100);
    private DonationPaymentComponent donationPaymentComponent;
    private MediaKeyboard emojiKeyboard;
    private DialogInterface errorDialog;
    private final PublishSubject<TextInput.TextInputEvent> eventPublisher = PublishSubject.create();
    private InputAwareLayout inputAwareLayout;
    private final Lazy keyboardPagerViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(KeyboardPagerViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$keyboardPagerViewModel$2
        final /* synthetic */ GiftFlowConfirmationFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private AlertDialog processingDonationPaymentDialog;
    private TextInput.MultilineViewHolder textInputViewHolder;
    private AlertDialog verifyingRecipientDonationPaymentDialog;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(GiftFlowViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$viewModel$2
        final /* synthetic */ GiftFlowConfirmationFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    public GiftFlowConfirmationFragment() {
        super(R.string.GiftFlowConfirmationFragment__confirm_gift, 0, R.layout.gift_flow_confirmation_fragment, null, 10, null);
    }

    /* compiled from: GiftFlowConfirmationFragment.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowConfirmationFragment$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final GiftFlowViewModel getViewModel() {
        return (GiftFlowViewModel) this.viewModel$delegate.getValue();
    }

    private final KeyboardPagerViewModel getKeyboardPagerViewModel() {
        return (KeyboardPagerViewModel) this.keyboardPagerViewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        DonationPaymentComponent donationPaymentComponent;
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        RecipientPreference.INSTANCE.register(dSLSettingsAdapter);
        GiftRowItem.INSTANCE.register(dSLSettingsAdapter);
        getKeyboardPagerViewModel().setOnlyPage(KeyboardPage.EMOJI);
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        donationPaymentComponent = (DonationPaymentComponent) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent");
                    }
                } else if (fragment instanceof DonationPaymentComponent) {
                    donationPaymentComponent = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.donationPaymentComponent = donationPaymentComponent;
            AlertDialog create = new MaterialAlertDialogBuilder(requireContext()).setView(R.layout.processing_payment_dialog).setCancelable(false).create();
            Intrinsics.checkNotNullExpressionValue(create, "MaterialAlertDialogBuild…le(false)\n      .create()");
            this.processingDonationPaymentDialog = create;
            AlertDialog create2 = new MaterialAlertDialogBuilder(requireContext()).setView(R.layout.verifying_recipient_payment_dialog).setCancelable(false).create();
            Intrinsics.checkNotNullExpressionValue(create2, "MaterialAlertDialogBuild…le(false)\n      .create()");
            this.verifyingRecipientDonationPaymentDialog = create2;
            View findViewById = requireView().findViewById(R.id.input_aware_layout);
            Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewBy…(R.id.input_aware_layout)");
            this.inputAwareLayout = (InputAwareLayout) findViewById;
            View findViewById2 = requireView().findViewById(R.id.emoji_drawer);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "requireView().findViewById(R.id.emoji_drawer)");
            MediaKeyboard mediaKeyboard = (MediaKeyboard) findViewById2;
            this.emojiKeyboard = mediaKeyboard;
            DonationPaymentComponent donationPaymentComponent2 = null;
            if (mediaKeyboard == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiKeyboard");
                mediaKeyboard = null;
            }
            mediaKeyboard.setFragmentManager(getChildFragmentManager());
            ((GooglePayButton) requireView().findViewById(R.id.google_pay_button)).setOnGooglePayClickListener(new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$bindAdapter$1
                final /* synthetic */ GiftFlowConfirmationFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    GiftFlowViewModel access$getViewModel = GiftFlowConfirmationFragment.access$getViewModel(this.this$0);
                    String string = this.this$0.getString(R.string.preferences__one_time);
                    Intrinsics.checkNotNullExpressionValue(string, "getString(R.string.preferences__one_time)");
                    access$getViewModel.requestTokenFromGooglePay(string);
                }
            });
            FrameLayout frameLayout = (FrameLayout) requireView().findViewById(R.id.text_input);
            ImageView imageView = (ImageView) frameLayout.findViewById(R.id.emoji_toggle);
            Intrinsics.checkNotNullExpressionValue(frameLayout, "textInput");
            PublishSubject<TextInput.TextInputEvent> publishSubject = this.eventPublisher;
            Intrinsics.checkNotNullExpressionValue(publishSubject, "eventPublisher");
            TextInput.MultilineViewHolder multilineViewHolder = new TextInput.MultilineViewHolder(frameLayout, publishSubject);
            this.textInputViewHolder = multilineViewHolder;
            multilineViewHolder.onAttachedToWindow();
            InputAwareLayout inputAwareLayout = this.inputAwareLayout;
            if (inputAwareLayout == null) {
                Intrinsics.throwUninitializedPropertyAccessException("inputAwareLayout");
                inputAwareLayout = null;
            }
            inputAwareLayout.addOnKeyboardShownListener(new KeyboardAwareLinearLayout.OnKeyboardShownListener(imageView) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$$ExternalSyntheticLambda1
                public final /* synthetic */ ImageView f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardShownListener
                public final void onKeyboardShown() {
                    GiftFlowConfirmationFragment.$r8$lambda$i9KFfxKHuKdDRDilKYoPehrqZIU(GiftFlowConfirmationFragment.this, this.f$1);
                }
            });
            requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(this, imageView) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$bindAdapter$3
                final /* synthetic */ ImageView $emojiToggle;
                final /* synthetic */ GiftFlowConfirmationFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                    this.$emojiToggle = r2;
                }

                @Override // androidx.activity.OnBackPressedCallback
                public void handleOnBackPressed() {
                    InputAwareLayout access$getInputAwareLayout$p = GiftFlowConfirmationFragment.access$getInputAwareLayout$p(this.this$0);
                    InputAwareLayout inputAwareLayout2 = null;
                    if (access$getInputAwareLayout$p == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("inputAwareLayout");
                        access$getInputAwareLayout$p = null;
                    }
                    if (access$getInputAwareLayout$p.isInputOpen()) {
                        InputAwareLayout access$getInputAwareLayout$p2 = GiftFlowConfirmationFragment.access$getInputAwareLayout$p(this.this$0);
                        if (access$getInputAwareLayout$p2 == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("inputAwareLayout");
                        } else {
                            inputAwareLayout2 = access$getInputAwareLayout$p2;
                        }
                        inputAwareLayout2.hideAttachedInput(true);
                        this.$emojiToggle.setImageResource(R.drawable.ic_emoji_smiley_24);
                        return;
                    }
                    FragmentKt.findNavController(this.this$0).popBackStack();
                }
            });
            TextInput.MultilineViewHolder multilineViewHolder2 = this.textInputViewHolder;
            if (multilineViewHolder2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("textInputViewHolder");
                multilineViewHolder2 = null;
            }
            multilineViewHolder2.bind(new TextInput.MultilineModel(getViewModel().getSnapshot().getAdditionalMessage(), DSLSettingsText.Companion.from(R.string.GiftFlowConfirmationFragment__add_a_message, new DSLSettingsText.Modifier[0]), new Function1<EditText, Unit>(this, imageView) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$bindAdapter$4
                final /* synthetic */ ImageView $emojiToggle;
                final /* synthetic */ GiftFlowConfirmationFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                    this.$emojiToggle = r2;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(EditText editText) {
                    invoke(editText);
                    return Unit.INSTANCE;
                }

                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void invoke(android.widget.EditText r4) {
                    /*
                        r3 = this;
                        java.lang.String r0 = "it"
                        kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r4, r0)
                        org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment r0 = r3.this$0
                        org.thoughtcrime.securesms.components.InputAwareLayout r0 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment.access$getInputAwareLayout$p(r0)
                        java.lang.String r1 = "inputAwareLayout"
                        r2 = 0
                        if (r0 != 0) goto L_0x0014
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
                        r0 = r2
                    L_0x0014:
                        boolean r0 = r0.isKeyboardOpen()
                        if (r0 != 0) goto L_0x0058
                        org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment r0 = r3.this$0
                        org.thoughtcrime.securesms.components.InputAwareLayout r0 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment.access$getInputAwareLayout$p(r0)
                        if (r0 != 0) goto L_0x0026
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
                        r0 = r2
                    L_0x0026:
                        boolean r0 = r0.isKeyboardOpen()
                        if (r0 != 0) goto L_0x003f
                        org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment r0 = r3.this$0
                        org.thoughtcrime.securesms.components.InputAwareLayout r0 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment.access$getInputAwareLayout$p(r0)
                        if (r0 != 0) goto L_0x0038
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
                        r0 = r2
                    L_0x0038:
                        boolean r0 = r0.isInputOpen()
                        if (r0 != 0) goto L_0x003f
                        goto L_0x0058
                    L_0x003f:
                        org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment r0 = r3.this$0
                        org.thoughtcrime.securesms.components.InputAwareLayout r0 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment.access$getInputAwareLayout$p(r0)
                        if (r0 != 0) goto L_0x004b
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
                        goto L_0x004c
                    L_0x004b:
                        r2 = r0
                    L_0x004c:
                        r2.showSoftkey(r4)
                        android.widget.ImageView r4 = r3.$emojiToggle
                        r0 = 2131231234(0x7f080202, float:1.8078543E38)
                        r4.setImageResource(r0)
                        goto L_0x007e
                    L_0x0058:
                        org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment r0 = r3.this$0
                        org.thoughtcrime.securesms.components.InputAwareLayout r0 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment.access$getInputAwareLayout$p(r0)
                        if (r0 != 0) goto L_0x0064
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
                        r0 = r2
                    L_0x0064:
                        org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment r1 = r3.this$0
                        org.thoughtcrime.securesms.components.emoji.MediaKeyboard r1 = org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment.access$getEmojiKeyboard$p(r1)
                        if (r1 != 0) goto L_0x0072
                        java.lang.String r1 = "emojiKeyboard"
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
                        goto L_0x0073
                    L_0x0072:
                        r2 = r1
                    L_0x0073:
                        r0.show(r4, r2)
                        android.widget.ImageView r4 = r3.$emojiToggle
                        r0 = 2131231301(0x7f080245, float:1.807868E38)
                        r4.setImageResource(r0)
                    L_0x007e:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$bindAdapter$4.invoke(android.widget.EditText):void");
                }
            }, new Function1<CharSequence, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$bindAdapter$5
                final /* synthetic */ GiftFlowConfirmationFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(CharSequence charSequence) {
                    invoke(charSequence);
                    return Unit.INSTANCE;
                }

                public final void invoke(CharSequence charSequence) {
                    Intrinsics.checkNotNullParameter(charSequence, "it");
                    GiftFlowConfirmationFragment.access$getViewModel(this.this$0).setAdditionalMessage(charSequence);
                }
            }));
            LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
            Disposable subscribe = getViewModel().getState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$$ExternalSyntheticLambda2
                public final /* synthetic */ GiftFlowConfirmationFragment f$1;

                {
                    this.f$1 = r2;
                }

                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    GiftFlowConfirmationFragment.$r8$lambda$pmZAoDJwmGWkmjsLjVs8drKC8TU(DSLSettingsAdapter.this, this.f$1, (GiftFlowState) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.state.observeO…g.dismiss()\n      }\n    }");
            lifecycleDisposable.plusAssign(subscribe);
            LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
            Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
            lifecycleDisposable2.bindTo(viewLifecycleOwner);
            LifecycleDisposable lifecycleDisposable3 = this.lifecycleDisposable;
            Disposable subscribe2 = DonationError.Companion.getErrorsForSource(DonationErrorSource.GIFT).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$$ExternalSyntheticLambda3
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    GiftFlowConfirmationFragment.$r8$lambda$twsIOX9lgyimf2SCc9s383zDpl0(GiftFlowConfirmationFragment.this, (DonationError) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe2, "DonationError\n      .get…or(donationError)\n      }");
            lifecycleDisposable3.plusAssign(subscribe2);
            LifecycleDisposable lifecycleDisposable4 = this.lifecycleDisposable;
            Disposable subscribe3 = getViewModel().getEvents().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$$ExternalSyntheticLambda4
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    GiftFlowConfirmationFragment.$r8$lambda$ycEfjtWWlLXn9xi2k9ppnGgzNEc(GiftFlowConfirmationFragment.this, (DonationEvent) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe3, "viewModel.events.observe…led -> Unit\n      }\n    }");
            lifecycleDisposable4.plusAssign(subscribe3);
            LifecycleDisposable lifecycleDisposable5 = this.lifecycleDisposable;
            DonationPaymentComponent donationPaymentComponent3 = this.donationPaymentComponent;
            if (donationPaymentComponent3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("donationPaymentComponent");
            } else {
                donationPaymentComponent2 = donationPaymentComponent3;
            }
            Disposable subscribe4 = donationPaymentComponent2.getGooglePayResultPublisher().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$$ExternalSyntheticLambda5
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    GiftFlowConfirmationFragment.$r8$lambda$85TmiQBIjuOnOTHlxAU6p1s_b64(GiftFlowConfirmationFragment.this, (DonationPaymentComponent.GooglePayResult) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe4, "donationPaymentComponent…esultCode, it.data)\n    }");
            lifecycleDisposable5.plusAssign(subscribe4);
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m373bindAdapter$lambda0(GiftFlowConfirmationFragment giftFlowConfirmationFragment, ImageView imageView) {
        Intrinsics.checkNotNullParameter(giftFlowConfirmationFragment, "this$0");
        MediaKeyboard mediaKeyboard = giftFlowConfirmationFragment.emojiKeyboard;
        InputAwareLayout inputAwareLayout = null;
        if (mediaKeyboard == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiKeyboard");
            mediaKeyboard = null;
        }
        if (!mediaKeyboard.isEmojiSearchMode()) {
            InputAwareLayout inputAwareLayout2 = giftFlowConfirmationFragment.inputAwareLayout;
            if (inputAwareLayout2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("inputAwareLayout");
            } else {
                inputAwareLayout = inputAwareLayout2;
            }
            inputAwareLayout.hideAttachedInput(true);
            imageView.setImageResource(R.drawable.ic_emoji_smiley_24);
        }
    }

    /* renamed from: bindAdapter$lambda-2 */
    public static final void m374bindAdapter$lambda2(DSLSettingsAdapter dSLSettingsAdapter, GiftFlowConfirmationFragment giftFlowConfirmationFragment, GiftFlowState giftFlowState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(giftFlowConfirmationFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(giftFlowState, "state");
        dSLSettingsAdapter.submitList(giftFlowConfirmationFragment.getConfiguration(giftFlowState).toMappingModelList());
        AlertDialog alertDialog = null;
        if (giftFlowState.getStage() == GiftFlowState.Stage.RECIPIENT_VERIFICATION) {
            giftFlowConfirmationFragment.debouncer.publish(new Runnable() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    GiftFlowConfirmationFragment.m372$r8$lambda$Tn5LvVfgmHxlDILM2RAxvF1cs(GiftFlowConfirmationFragment.this);
                }
            });
        } else {
            giftFlowConfirmationFragment.debouncer.clear();
            AlertDialog alertDialog2 = giftFlowConfirmationFragment.verifyingRecipientDonationPaymentDialog;
            if (alertDialog2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("verifyingRecipientDonationPaymentDialog");
                alertDialog2 = null;
            }
            alertDialog2.dismiss();
        }
        if (giftFlowState.getStage() == GiftFlowState.Stage.PAYMENT_PIPELINE) {
            AlertDialog alertDialog3 = giftFlowConfirmationFragment.processingDonationPaymentDialog;
            if (alertDialog3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("processingDonationPaymentDialog");
            } else {
                alertDialog = alertDialog3;
            }
            alertDialog.show();
            return;
        }
        AlertDialog alertDialog4 = giftFlowConfirmationFragment.processingDonationPaymentDialog;
        if (alertDialog4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("processingDonationPaymentDialog");
        } else {
            alertDialog = alertDialog4;
        }
        alertDialog.dismiss();
    }

    /* renamed from: bindAdapter$lambda-2$lambda-1 */
    public static final void m375bindAdapter$lambda2$lambda1(GiftFlowConfirmationFragment giftFlowConfirmationFragment) {
        Intrinsics.checkNotNullParameter(giftFlowConfirmationFragment, "this$0");
        AlertDialog alertDialog = giftFlowConfirmationFragment.verifyingRecipientDonationPaymentDialog;
        if (alertDialog == null) {
            Intrinsics.throwUninitializedPropertyAccessException("verifyingRecipientDonationPaymentDialog");
            alertDialog = null;
        }
        alertDialog.show();
    }

    /* renamed from: bindAdapter$lambda-3 */
    public static final void m376bindAdapter$lambda3(GiftFlowConfirmationFragment giftFlowConfirmationFragment, DonationError donationError) {
        Intrinsics.checkNotNullParameter(giftFlowConfirmationFragment, "this$0");
        giftFlowConfirmationFragment.onPaymentError(donationError);
    }

    /* renamed from: bindAdapter$lambda-4 */
    public static final void m377bindAdapter$lambda4(GiftFlowConfirmationFragment giftFlowConfirmationFragment, DonationEvent donationEvent) {
        Intrinsics.checkNotNullParameter(giftFlowConfirmationFragment, "this$0");
        if (donationEvent instanceof DonationEvent.PaymentConfirmationSuccess) {
            giftFlowConfirmationFragment.onPaymentConfirmed();
        } else if (Intrinsics.areEqual(donationEvent, DonationEvent.RequestTokenSuccess.INSTANCE)) {
            Log.i(TAG, "Successfully got request token from Google Pay");
        } else if (!Intrinsics.areEqual(donationEvent, DonationEvent.SubscriptionCancelled.INSTANCE)) {
            boolean z = donationEvent instanceof DonationEvent.SubscriptionCancellationFailed;
        }
    }

    /* renamed from: bindAdapter$lambda-5 */
    public static final void m378bindAdapter$lambda5(GiftFlowConfirmationFragment giftFlowConfirmationFragment, DonationPaymentComponent.GooglePayResult googlePayResult) {
        Intrinsics.checkNotNullParameter(giftFlowConfirmationFragment, "this$0");
        giftFlowConfirmationFragment.getViewModel().onActivityResult(googlePayResult.getRequestCode(), googlePayResult.getResultCode(), googlePayResult.getData());
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        TextInput.MultilineViewHolder multilineViewHolder = this.textInputViewHolder;
        AlertDialog alertDialog = null;
        if (multilineViewHolder == null) {
            Intrinsics.throwUninitializedPropertyAccessException("textInputViewHolder");
            multilineViewHolder = null;
        }
        multilineViewHolder.onDetachedFromWindow();
        AlertDialog alertDialog2 = this.processingDonationPaymentDialog;
        if (alertDialog2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("processingDonationPaymentDialog");
            alertDialog2 = null;
        }
        alertDialog2.dismiss();
        this.debouncer.clear();
        AlertDialog alertDialog3 = this.verifyingRecipientDonationPaymentDialog;
        if (alertDialog3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("verifyingRecipientDonationPaymentDialog");
        } else {
            alertDialog = alertDialog3;
        }
        alertDialog.dismiss();
    }

    private final DSLConfiguration getConfiguration(GiftFlowState giftFlowState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(giftFlowState) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$getConfiguration$1
            final /* synthetic */ GiftFlowState $giftFlowState;

            /* access modifiers changed from: package-private */
            {
                this.$giftFlowState = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                FiatMoney fiatMoney;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                if (!(this.$giftFlowState.getGiftBadge() == null || (fiatMoney = this.$giftFlowState.getGiftPrices().get(this.$giftFlowState.getCurrency())) == null)) {
                    dSLConfiguration.customPref(new GiftRowItem.Model(this.$giftFlowState.getGiftBadge(), fiatMoney));
                }
                dSLConfiguration.sectionHeaderPref(R.string.GiftFlowConfirmationFragment__send_to);
                Recipient recipient = this.$giftFlowState.getRecipient();
                Intrinsics.checkNotNull(recipient);
                dSLConfiguration.customPref(new RecipientPreference.Model(recipient, false, null, 6, null));
                DSLConfiguration.textPref$default(dSLConfiguration, null, DSLSettingsText.Companion.from(R.string.GiftFlowConfirmationFragment__your_gift_will_be_sent_in, new DSLSettingsText.Modifier[0]), 1, null);
            }
        });
    }

    private final void onPaymentConfirmed() {
        Intent clearTop = MainActivity.clearTop(requireContext());
        Intrinsics.checkNotNullExpressionValue(clearTop, "clearTop(requireContext())");
        Context requireContext = requireContext();
        Recipient recipient = getViewModel().getSnapshot().getRecipient();
        Intrinsics.checkNotNull(recipient);
        ConversationIntents.Builder createBuilder = ConversationIntents.createBuilder(requireContext, recipient.getId(), -1);
        Badge giftBadge = getViewModel().getSnapshot().getGiftBadge();
        Intrinsics.checkNotNull(giftBadge);
        Intent build = createBuilder.withGiftBadge(giftBadge).build();
        Intrinsics.checkNotNullExpressionValue(build, "createBuilder(requireCon…ftBadge!!)\n      .build()");
        requireActivity().startActivities(new Intent[]{clearTop, build});
    }

    private final void onPaymentError(Throwable th) {
        String str = TAG;
        Log.w(str, "onPaymentError", th, true);
        if (this.errorDialog != null) {
            Log.i(str, "Already displaying an error dialog. Skipping.");
            return;
        }
        DonationErrorDialogs donationErrorDialogs = DonationErrorDialogs.INSTANCE;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        this.errorDialog = donationErrorDialogs.show(requireContext, th, new DonationErrorDialogs.DialogCallback(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment$onPaymentError$1
            final /* synthetic */ GiftFlowConfirmationFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs.DialogCallback
            public void onDialogDismissed() {
                this.this$0.requireActivity().finish();
            }
        });
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void onToolbarNavigationClicked() {
        FragmentKt.findNavController(this).popBackStack();
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment.Callback
    public void openEmojiSearch() {
        MediaKeyboard mediaKeyboard = this.emojiKeyboard;
        if (mediaKeyboard == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiKeyboard");
            mediaKeyboard = null;
        }
        mediaKeyboard.onOpenEmojiSearch();
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment.Callback
    public void closeEmojiSearch() {
        MediaKeyboard mediaKeyboard = this.emojiKeyboard;
        if (mediaKeyboard == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiKeyboard");
            mediaKeyboard = null;
        }
        mediaKeyboard.onCloseEmojiSearch();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000d, code lost:
        if ((r4.length() > 0) == true) goto L_0x0011;
     */
    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onEmojiSelected(java.lang.String r4) {
        /*
            r3 = this;
            r0 = 1
            r1 = 0
            if (r4 == 0) goto L_0x0010
            int r2 = r4.length()
            if (r2 <= 0) goto L_0x000c
            r2 = 1
            goto L_0x000d
        L_0x000c:
            r2 = 0
        L_0x000d:
            if (r2 != r0) goto L_0x0010
            goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            if (r0 == 0) goto L_0x001d
            io.reactivex.rxjava3.subjects.PublishSubject<org.thoughtcrime.securesms.components.settings.models.TextInput$TextInputEvent> r0 = r3.eventPublisher
            org.thoughtcrime.securesms.components.settings.models.TextInput$TextInputEvent$OnEmojiEvent r1 = new org.thoughtcrime.securesms.components.settings.models.TextInput$TextInputEvent$OnEmojiEvent
            r1.<init>(r4)
            r0.onNext(r1)
        L_0x001d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowConfirmationFragment.onEmojiSelected(java.lang.String):void");
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onKeyEvent(KeyEvent keyEvent) {
        if (keyEvent != null) {
            this.eventPublisher.onNext(new TextInput.TextInputEvent.OnKeyEvent(keyEvent));
        }
    }
}
