package org.thoughtcrime.securesms.badges.gifts.flow;

import android.content.Intent;
import android.os.Bundle;
import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.navigation.ActivityKt;
import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FragmentWrapperActivity;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository;

/* compiled from: GiftFlowActivity.kt */
@Metadata(d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u001dB\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\"\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00142\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u001a\u0010\u0018\u001a\u00020\u00122\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0014R\u001b\u0010\u0004\u001a\u00020\u00058VX\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowActivity;", "Lorg/thoughtcrime/securesms/components/FragmentWrapperActivity;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent;", "()V", "donationPaymentRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "getDonationPaymentRepository", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "donationPaymentRepository$delegate", "Lkotlin/Lazy;", "googlePayResultPublisher", "Lio/reactivex/rxjava3/subjects/Subject;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent$GooglePayResult;", "getGooglePayResultPublisher", "()Lio/reactivex/rxjava3/subjects/Subject;", "getFragment", "Landroidx/fragment/app/Fragment;", "onActivityResult", "", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "ready", "", "OnBackPressed", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GiftFlowActivity extends FragmentWrapperActivity implements DonationPaymentComponent {
    private final Lazy donationPaymentRepository$delegate = LazyKt__LazyJVMKt.lazy(new Function0<DonationPaymentRepository>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowActivity$donationPaymentRepository$2
        final /* synthetic */ GiftFlowActivity this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final DonationPaymentRepository invoke() {
            return new DonationPaymentRepository(this.this$0);
        }
    });
    private final Subject<DonationPaymentComponent.GooglePayResult> googlePayResultPublisher;

    public GiftFlowActivity() {
        PublishSubject create = PublishSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.googlePayResultPublisher = create;
    }

    @Override // org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent
    public DonationPaymentRepository getDonationPaymentRepository() {
        return (DonationPaymentRepository) this.donationPaymentRepository$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent
    public Subject<DonationPaymentComponent.GooglePayResult> getGooglePayResultPublisher() {
        return this.googlePayResultPublisher;
    }

    @Override // org.thoughtcrime.securesms.components.FragmentWrapperActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        getOnBackPressedDispatcher().addCallback(this, new OnBackPressed());
    }

    @Override // org.thoughtcrime.securesms.components.FragmentWrapperActivity
    public Fragment getFragment() {
        NavHostFragment create = NavHostFragment.create(R.navigation.gift_flow);
        Intrinsics.checkNotNullExpressionValue(create, "create(R.navigation.gift_flow)");
        return create;
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        getGooglePayResultPublisher().onNext(new DonationPaymentComponent.GooglePayResult(i, i2, intent));
    }

    /* compiled from: GiftFlowActivity.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowActivity$OnBackPressed;", "Landroidx/activity/OnBackPressedCallback;", "(Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowActivity;)V", "handleOnBackPressed", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    private final class OnBackPressed extends OnBackPressedCallback {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OnBackPressed() {
            super(true);
            GiftFlowActivity.this = r1;
        }

        @Override // androidx.activity.OnBackPressedCallback
        public void handleOnBackPressed() {
            if (!ActivityKt.findNavController(GiftFlowActivity.this, R.id.fragment_container).popBackStack()) {
                GiftFlowActivity.this.finish();
            }
        }
    }
}
