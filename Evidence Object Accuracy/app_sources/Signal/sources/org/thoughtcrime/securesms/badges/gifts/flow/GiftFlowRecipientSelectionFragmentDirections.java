package org.thoughtcrime.securesms.badges.gifts.flow;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes3.dex */
public class GiftFlowRecipientSelectionFragmentDirections {
    private GiftFlowRecipientSelectionFragmentDirections() {
    }

    public static NavDirections actionGiftFlowRecipientSelectionFragmentToGiftFlowConfirmationFragment() {
        return new ActionOnlyNavDirections(R.id.action_giftFlowRecipientSelectionFragment_to_giftFlowConfirmationFragment);
    }
}
