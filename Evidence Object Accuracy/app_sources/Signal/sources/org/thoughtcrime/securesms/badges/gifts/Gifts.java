package org.thoughtcrime.securesms.badges.gifts;

import android.content.Context;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.OutgoingSecureMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Base64;

/* compiled from: Gifts.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fJ\u0012\u0010\u000e\u001a\u00020\u000f*\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011J\f\u0010\u0012\u001a\u00020\f*\u00020\nH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/Gifts;", "", "()V", "GOOGLE_PAY_REQUEST_CODE", "", "createOutgoingGiftMessage", "Lorg/thoughtcrime/securesms/mms/OutgoingMediaMessage;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "sentTimestamp", "", "expiresIn", "formatExpiry", "", "context", "Landroid/content/Context;", "getExpiry", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class Gifts {
    public static final int GOOGLE_PAY_REQUEST_CODE;
    public static final Gifts INSTANCE = new Gifts();

    private Gifts() {
    }

    public final OutgoingMediaMessage createOutgoingGiftMessage(Recipient recipient, GiftBadge giftBadge, long j, long j2) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(giftBadge, "giftBadge");
        return new OutgoingSecureMediaMessage(recipient, Base64.encodeBytes(giftBadge.toByteArray()), CollectionsKt__CollectionsKt.emptyList(), j, 2, j2, false, StoryType.NONE, null, false, null, CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList(), giftBadge);
    }

    private final long getExpiry(GiftBadge giftBadge) {
        try {
            return new ReceiptCredentialPresentation(giftBadge.getRedemptionToken().toByteArray()).getReceiptExpirationTime();
        } catch (InvalidInputException unused) {
            return 0;
        }
    }

    public final String formatExpiry(GiftBadge giftBadge, Context context) {
        Intrinsics.checkNotNullParameter(giftBadge, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        long millis = TimeUnit.SECONDS.toMillis(getExpiry(giftBadge)) - System.currentTimeMillis();
        if (millis <= 0) {
            String string = context.getString(R.string.Gifts__expired);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.string.Gifts__expired)");
            return string;
        }
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        int days = (int) timeUnit.toDays(millis);
        if (days > 0) {
            String quantityString = context.getResources().getQuantityString(R.plurals.Gifts__d_days_remaining, days, Integer.valueOf(days));
            Intrinsics.checkNotNullExpressionValue(quantityString, "context.resources.getQua…ys_remaining, days, days)");
            return quantityString;
        }
        int hours = (int) timeUnit.toHours(millis);
        if (hours > 0) {
            String quantityString2 = context.getResources().getQuantityString(R.plurals.Gifts__d_hours_remaining, hours, Integer.valueOf(hours));
            Intrinsics.checkNotNullExpressionValue(quantityString2, "context.resources.getQua…_remaining, hours, hours)");
            return quantityString2;
        }
        int min = Math.min(1, (int) timeUnit.toMinutes(millis));
        String quantityString3 = context.getResources().getQuantityString(R.plurals.Gifts__d_minutes_remaining, min, Integer.valueOf(min));
        Intrinsics.checkNotNullExpressionValue(quantityString3, "context.resources.getQua…aining, minutes, minutes)");
        return quantityString3;
    }
}
