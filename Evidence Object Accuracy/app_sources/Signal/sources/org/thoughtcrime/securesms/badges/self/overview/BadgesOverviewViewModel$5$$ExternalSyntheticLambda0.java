package org.thoughtcrime.securesms.badges.self.overview;

import com.annimon.stream.function.Function;
import j$.util.Optional;
import org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class BadgesOverviewViewModel$5$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ Optional f$0;

    public /* synthetic */ BadgesOverviewViewModel$5$$ExternalSyntheticLambda0(Optional optional) {
        this.f$0 = optional;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return BadgesOverviewViewModel.AnonymousClass5.m454invoke$lambda0(this.f$0, (BadgesOverviewState) obj);
    }
}
