package org.thoughtcrime.securesms.badges.view;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ViewBadgeState.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u001fB7\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\nJ\u000f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0006HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\tHÆ\u0003J;\u0010\u0017\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\tHÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/badges/view/ViewBadgeState;", "", "allBadgesVisibleOnProfile", "", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "badgeLoadState", "Lorg/thoughtcrime/securesms/badges/view/ViewBadgeState$LoadState;", "selectedBadge", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(Ljava/util/List;Lorg/thoughtcrime/securesms/badges/view/ViewBadgeState$LoadState;Lorg/thoughtcrime/securesms/badges/models/Badge;Lorg/thoughtcrime/securesms/recipients/Recipient;)V", "getAllBadgesVisibleOnProfile", "()Ljava/util/List;", "getBadgeLoadState", "()Lorg/thoughtcrime/securesms/badges/view/ViewBadgeState$LoadState;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getSelectedBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "", "LoadState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ViewBadgeState {
    private final List<Badge> allBadgesVisibleOnProfile;
    private final LoadState badgeLoadState;
    private final Recipient recipient;
    private final Badge selectedBadge;

    /* compiled from: ViewBadgeState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/badges/view/ViewBadgeState$LoadState;", "", "(Ljava/lang/String;I)V", "INIT", "LOADED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum LoadState {
        INIT,
        LOADED
    }

    public ViewBadgeState() {
        this(null, null, null, null, 15, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.badges.view.ViewBadgeState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ViewBadgeState copy$default(ViewBadgeState viewBadgeState, List list, LoadState loadState, Badge badge, Recipient recipient, int i, Object obj) {
        if ((i & 1) != 0) {
            list = viewBadgeState.allBadgesVisibleOnProfile;
        }
        if ((i & 2) != 0) {
            loadState = viewBadgeState.badgeLoadState;
        }
        if ((i & 4) != 0) {
            badge = viewBadgeState.selectedBadge;
        }
        if ((i & 8) != 0) {
            recipient = viewBadgeState.recipient;
        }
        return viewBadgeState.copy(list, loadState, badge, recipient);
    }

    public final List<Badge> component1() {
        return this.allBadgesVisibleOnProfile;
    }

    public final LoadState component2() {
        return this.badgeLoadState;
    }

    public final Badge component3() {
        return this.selectedBadge;
    }

    public final Recipient component4() {
        return this.recipient;
    }

    public final ViewBadgeState copy(List<Badge> list, LoadState loadState, Badge badge, Recipient recipient) {
        Intrinsics.checkNotNullParameter(list, "allBadgesVisibleOnProfile");
        Intrinsics.checkNotNullParameter(loadState, "badgeLoadState");
        return new ViewBadgeState(list, loadState, badge, recipient);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ViewBadgeState)) {
            return false;
        }
        ViewBadgeState viewBadgeState = (ViewBadgeState) obj;
        return Intrinsics.areEqual(this.allBadgesVisibleOnProfile, viewBadgeState.allBadgesVisibleOnProfile) && this.badgeLoadState == viewBadgeState.badgeLoadState && Intrinsics.areEqual(this.selectedBadge, viewBadgeState.selectedBadge) && Intrinsics.areEqual(this.recipient, viewBadgeState.recipient);
    }

    public int hashCode() {
        int hashCode = ((this.allBadgesVisibleOnProfile.hashCode() * 31) + this.badgeLoadState.hashCode()) * 31;
        Badge badge = this.selectedBadge;
        int i = 0;
        int hashCode2 = (hashCode + (badge == null ? 0 : badge.hashCode())) * 31;
        Recipient recipient = this.recipient;
        if (recipient != null) {
            i = recipient.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "ViewBadgeState(allBadgesVisibleOnProfile=" + this.allBadgesVisibleOnProfile + ", badgeLoadState=" + this.badgeLoadState + ", selectedBadge=" + this.selectedBadge + ", recipient=" + this.recipient + ')';
    }

    public ViewBadgeState(List<Badge> list, LoadState loadState, Badge badge, Recipient recipient) {
        Intrinsics.checkNotNullParameter(list, "allBadgesVisibleOnProfile");
        Intrinsics.checkNotNullParameter(loadState, "badgeLoadState");
        this.allBadgesVisibleOnProfile = list;
        this.badgeLoadState = loadState;
        this.selectedBadge = badge;
        this.recipient = recipient;
    }

    public /* synthetic */ ViewBadgeState(List list, LoadState loadState, Badge badge, Recipient recipient, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i & 2) != 0 ? LoadState.INIT : loadState, (i & 4) != 0 ? null : badge, (i & 8) != 0 ? null : recipient);
    }

    public final List<Badge> getAllBadgesVisibleOnProfile() {
        return this.allBadgesVisibleOnProfile;
    }

    public final LoadState getBadgeLoadState() {
        return this.badgeLoadState;
    }

    public final Badge getSelectedBadge() {
        return this.selectedBadge;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }
}
