package org.thoughtcrime.securesms.badges.self.overview;

import kotlin.Metadata;

/* compiled from: BadgesOverviewEvent.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0003\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewEvent;", "", "(Ljava/lang/String;I)V", "FAILED_TO_UPDATE_PROFILE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public enum BadgesOverviewEvent {
    FAILED_TO_UPDATE_PROFILE
}
