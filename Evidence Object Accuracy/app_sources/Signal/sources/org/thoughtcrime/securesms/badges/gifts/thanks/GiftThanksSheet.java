package org.thoughtcrime.securesms.badges.gifts.thanks;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.models.BadgePreview;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: GiftThanksSheet.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002R\u0014\u0010\u0003\u001a\u00020\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\n8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/thanks/GiftThanksSheet;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GiftThanksSheet extends DSLSettingsBottomSheetFragment {
    private static final String ARGS_BADGE;
    private static final String ARGS_RECIPIENT_ID;
    public static final Companion Companion = new Companion(null);
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();

    @JvmStatic
    public static final void show(FragmentManager fragmentManager, RecipientId recipientId, Badge badge) {
        Companion.show(fragmentManager, recipientId, badge);
    }

    public GiftThanksSheet() {
        super(0, null, 0.0f, 7, null);
    }

    /* compiled from: GiftThanksSheet.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/thanks/GiftThanksSheet$Companion;", "", "()V", "ARGS_BADGE", "", "ARGS_RECIPIENT_ID", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void show(FragmentManager fragmentManager, RecipientId recipientId, Badge badge) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(badge, "badge");
            GiftThanksSheet giftThanksSheet = new GiftThanksSheet();
            Bundle bundle = new Bundle();
            bundle.putParcelable(GiftThanksSheet.ARGS_RECIPIENT_ID, recipientId);
            bundle.putParcelable(GiftThanksSheet.ARGS_BADGE, badge);
            giftThanksSheet.setArguments(bundle);
            giftThanksSheet.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        }
    }

    private final RecipientId getRecipientId() {
        Parcelable parcelable = requireArguments().getParcelable(ARGS_RECIPIENT_ID);
        Intrinsics.checkNotNull(parcelable);
        return (RecipientId) parcelable;
    }

    public final Badge getBadge() {
        Parcelable parcelable = requireArguments().getParcelable(ARGS_BADGE);
        Intrinsics.checkNotNull(parcelable);
        return (Badge) parcelable;
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        BadgePreview.INSTANCE.register(dSLSettingsAdapter);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = Recipient.observable(getRecipientId()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(this) { // from class: org.thoughtcrime.securesms.badges.gifts.thanks.GiftThanksSheet$$ExternalSyntheticLambda0
            public final /* synthetic */ GiftThanksSheet f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                GiftThanksSheet.$r8$lambda$V2p9_vmc7D9D2ky17ipWjJJ2inE(DSLSettingsAdapter.this, this.f$1, (Recipient) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "observable(recipientId).…MappingModelList())\n    }");
        lifecycleDisposable2.plusAssign(subscribe);
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m395bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, GiftThanksSheet giftThanksSheet, Recipient recipient) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(giftThanksSheet, "this$0");
        Intrinsics.checkNotNullExpressionValue(recipient, "it");
        dSLSettingsAdapter.submitList(giftThanksSheet.getConfiguration(recipient).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(Recipient recipient) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this, recipient) { // from class: org.thoughtcrime.securesms.badges.gifts.thanks.GiftThanksSheet$getConfiguration$1
            final /* synthetic */ Recipient $recipient;
            final /* synthetic */ GiftThanksSheet this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$recipient = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLConfiguration.textPref$default(dSLConfiguration, companion.from(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__thanks_for_your_support, DSLSettingsText.TitleLargeModifier.INSTANCE, DSLSettingsText.CenterModifier.INSTANCE), null, 2, null);
                GiftThanksSheet giftThanksSheet = this.this$0;
                String string = giftThanksSheet.getString(R.string.GiftThanksSheet__youve_gifted_a_badge_to_s, this.$recipient.getDisplayName(giftThanksSheet.requireContext()));
                Intrinsics.checkNotNullExpressionValue(string, "getString(R.string.GiftT…ayName(requireContext()))");
                dSLConfiguration.noPadTextPref(companion.from(string, new DSLSettingsText.Modifier[0]));
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                dSLConfiguration.space((int) dimensionUnit.toPixels(37.0f));
                dSLConfiguration.customPref(new BadgePreview.BadgeModel.GiftedBadgeModel(GiftThanksSheet.access$getBadge(this.this$0), this.$recipient));
                dSLConfiguration.space((int) dimensionUnit.toPixels(60.0f));
            }
        });
    }
}
