package org.thoughtcrime.securesms.badges;

import android.content.Context;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.flexbox.FlexboxLayoutManager;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.ScreenDensity;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;

/* compiled from: Badges.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0004H\u0002J\u001c\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00040\u00142\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\nH\u0007J8\u0010\u001a\u001a\u00020\u001b*\u00020\u001c2\u0006\u0010\u0007\u001a\u00020\b2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\u001e2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/badges/Badges;", "", "()V", "TAG", "", "createLayoutManagerForGridWithBadges", "Landroidx/recyclerview/widget/RecyclerView$LayoutManager;", "context", "Landroid/content/Context;", "fromDatabaseBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "badge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/BadgeList$Badge;", "fromServiceBadge", "serviceBadge", "Lorg/whispersystems/signalservice/api/profiles/SignalServiceProfile$Badge;", "getBadgeImageUri", "Landroid/net/Uri;", "densityPath", "getBestBadgeImageUriForDevice", "Lorg/signal/libsignal/protocol/util/Pair;", "getTimestamp", "", "bigDecimal", "Ljava/math/BigDecimal;", "toDatabaseBadge", "displayBadges", "", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "badges", "", "selectedBadge", "fadedBadgeId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class Badges {
    public static final Badges INSTANCE = new Badges();
    private static final String TAG;

    private Badges() {
    }

    static {
        INSTANCE = new Badges();
        String tag = Log.tag(Badges.class);
        Intrinsics.checkNotNullExpressionValue(tag, "tag(Badges::class.java)");
        TAG = tag;
    }

    public final RecyclerView.LayoutManager createLayoutManagerForGridWithBadges(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(context);
        flexboxLayoutManager.setFlexDirection(0);
        flexboxLayoutManager.setAlignItems(2);
        flexboxLayoutManager.setJustifyContent(2);
        return flexboxLayoutManager;
    }

    private final Uri getBadgeImageUri(String str) {
        Uri build = Uri.parse(BuildConfig.BADGE_STATIC_ROOT).buildUpon().appendPath(str).build();
        Intrinsics.checkNotNullExpressionValue(build, "parse(BuildConfig.BADGE_…nsityPath)\n      .build()");
        return build;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final Pair<Uri, String> getBestBadgeImageUriForDevice(SignalServiceProfile.Badge badge) {
        Pair<Uri, String> pair;
        String bestDensityBucketForDevice = ScreenDensity.getBestDensityBucketForDevice();
        switch (bestDensityBucketForDevice.hashCode()) {
            case -1619189395:
                if (bestDensityBucketForDevice.equals("xxxhdpi")) {
                    String str = badge.getSprites6().get(5);
                    Intrinsics.checkNotNullExpressionValue(str, "serviceBadge.sprites6[5]");
                    pair = new Pair<>(getBadgeImageUri(str), "xxxhdpi");
                    break;
                }
                String str2 = badge.getSprites6().get(3);
                Intrinsics.checkNotNullExpressionValue(str2, "serviceBadge.sprites6[3]");
                pair = new Pair<>(getBadgeImageUri(str2), "xhdpi");
                break;
            case -745448715:
                if (bestDensityBucketForDevice.equals("xxhdpi")) {
                    String str3 = badge.getSprites6().get(4);
                    Intrinsics.checkNotNullExpressionValue(str3, "serviceBadge.sprites6[4]");
                    pair = new Pair<>(getBadgeImageUri(str3), "xxhdpi");
                    break;
                }
                String str2 = badge.getSprites6().get(3);
                Intrinsics.checkNotNullExpressionValue(str2, "serviceBadge.sprites6[3]");
                pair = new Pair<>(getBadgeImageUri(str2), "xhdpi");
                break;
            case 3197941:
                if (bestDensityBucketForDevice.equals("hdpi")) {
                    String str4 = badge.getSprites6().get(2);
                    Intrinsics.checkNotNullExpressionValue(str4, "serviceBadge.sprites6[2]");
                    pair = new Pair<>(getBadgeImageUri(str4), "hdpi");
                    break;
                }
                String str2 = badge.getSprites6().get(3);
                Intrinsics.checkNotNullExpressionValue(str2, "serviceBadge.sprites6[3]");
                pair = new Pair<>(getBadgeImageUri(str2), "xhdpi");
                break;
            case 3317105:
                if (bestDensityBucketForDevice.equals("ldpi")) {
                    String str5 = badge.getSprites6().get(0);
                    Intrinsics.checkNotNullExpressionValue(str5, "serviceBadge.sprites6[0]");
                    pair = new Pair<>(getBadgeImageUri(str5), "ldpi");
                    break;
                }
                String str2 = badge.getSprites6().get(3);
                Intrinsics.checkNotNullExpressionValue(str2, "serviceBadge.sprites6[3]");
                pair = new Pair<>(getBadgeImageUri(str2), "xhdpi");
                break;
            case 3346896:
                if (bestDensityBucketForDevice.equals("mdpi")) {
                    String str6 = badge.getSprites6().get(1);
                    Intrinsics.checkNotNullExpressionValue(str6, "serviceBadge.sprites6[1]");
                    pair = new Pair<>(getBadgeImageUri(str6), "mdpi");
                    break;
                }
                String str2 = badge.getSprites6().get(3);
                Intrinsics.checkNotNullExpressionValue(str2, "serviceBadge.sprites6[3]");
                pair = new Pair<>(getBadgeImageUri(str2), "xhdpi");
                break;
            default:
                String str2 = badge.getSprites6().get(3);
                Intrinsics.checkNotNullExpressionValue(str2, "serviceBadge.sprites6[3]");
                pair = new Pair<>(getBadgeImageUri(str2), "xhdpi");
                break;
        }
        String str7 = TAG;
        Log.d(str7, "Selected badge density " + pair.second());
        return pair;
    }

    private final long getTimestamp(BigDecimal bigDecimal) {
        return new Timestamp(bigDecimal.longValue() * ((long) MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH)).getTime();
    }

    @JvmStatic
    public static final Badge fromDatabaseBadge(BadgeList.Badge badge) {
        Intrinsics.checkNotNullParameter(badge, "badge");
        String id = badge.getId();
        Intrinsics.checkNotNullExpressionValue(id, "badge.id");
        Badge.Category.Companion companion = Badge.Category.Companion;
        String category = badge.getCategory();
        Intrinsics.checkNotNullExpressionValue(category, "badge.category");
        Badge.Category fromCode = companion.fromCode(category);
        String name = badge.getName();
        Intrinsics.checkNotNullExpressionValue(name, "badge.name");
        String description = badge.getDescription();
        Intrinsics.checkNotNullExpressionValue(description, "badge.description");
        Uri parse = Uri.parse(badge.getImageUrl());
        Intrinsics.checkNotNullExpressionValue(parse, "parse(badge.imageUrl)");
        String imageDensity = badge.getImageDensity();
        Intrinsics.checkNotNullExpressionValue(imageDensity, "badge.imageDensity");
        return new Badge(id, fromCode, name, description, parse, imageDensity, badge.getExpiration(), badge.getVisible(), 0);
    }

    @JvmStatic
    public static final BadgeList.Badge toDatabaseBadge(Badge badge) {
        Intrinsics.checkNotNullParameter(badge, "badge");
        BadgeList.Badge build = BadgeList.Badge.newBuilder().setId(badge.getId()).setCategory(badge.getCategory().getCode()).setDescription(badge.getDescription()).setExpiration(badge.getExpirationTimestamp()).setVisible(badge.getVisible()).setName(badge.getName()).setImageUrl(badge.getImageUrl().toString()).setImageDensity(badge.getImageDensity()).build();
        Intrinsics.checkNotNullExpressionValue(build, "newBuilder()\n      .setI…geDensity)\n      .build()");
        return build;
    }

    @JvmStatic
    public static final Badge fromServiceBadge(SignalServiceProfile.Badge badge) {
        Intrinsics.checkNotNullParameter(badge, "serviceBadge");
        Badges badges = INSTANCE;
        Pair<Uri, String> bestBadgeImageUriForDevice = badges.getBestBadgeImageUriForDevice(badge);
        String id = badge.getId();
        Intrinsics.checkNotNullExpressionValue(id, "serviceBadge.id");
        Badge.Category.Companion companion = Badge.Category.Companion;
        String category = badge.getCategory();
        Intrinsics.checkNotNullExpressionValue(category, "serviceBadge.category");
        Badge.Category fromCode = companion.fromCode(category);
        String name = badge.getName();
        Intrinsics.checkNotNullExpressionValue(name, "serviceBadge.name");
        String description = badge.getDescription();
        Intrinsics.checkNotNullExpressionValue(description, "serviceBadge.description");
        Uri first = bestBadgeImageUriForDevice.first();
        Intrinsics.checkNotNullExpressionValue(first, "uriAndDensity.first()");
        Uri uri = first;
        String second = bestBadgeImageUriForDevice.second();
        Intrinsics.checkNotNullExpressionValue(second, "uriAndDensity.second()");
        String str = second;
        BigDecimal expiration = badge.getExpiration();
        return new Badge(id, fromCode, name, description, uri, str, expiration != null ? badges.getTimestamp(expiration) : 0, badge.isVisible(), TimeUnit.SECONDS.toMillis(badge.getDuration()));
    }

    public final void displayBadges(DSLConfiguration dSLConfiguration, Context context, List<Badge> list, Badge badge, String str) {
        Intrinsics.checkNotNullParameter(dSLConfiguration, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(list, "badges");
        ArrayList<Badge.Model> arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (Badge badge2 : list) {
            arrayList.add(new Badge.Model(badge2, Intrinsics.areEqual(badge2, badge), Intrinsics.areEqual(badge2.getId(), str)));
        }
        for (Badge.Model model : arrayList) {
            dSLConfiguration.customPref(model);
        }
        int pixels = (int) (((float) context.getResources().getDisplayMetrics().widthPixels) / DimensionUnit.DP.toPixels(88.0f));
        int size = (pixels - (list.size() % pixels)) % pixels;
        for (int i = 0; i < size; i++) {
            dSLConfiguration.customPref(new Badge.EmptyModel());
        }
    }
}
