package org.thoughtcrime.securesms.badges.self.featured;

import kotlin.Metadata;

/* compiled from: SelectFeaturedBadgeEvent.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeEvent;", "", "(Ljava/lang/String;I)V", "NO_BADGE_SELECTED", "FAILED_TO_UPDATE_PROFILE", "SAVE_SUCCESSFUL", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public enum SelectFeaturedBadgeEvent {
    NO_BADGE_SELECTED,
    FAILED_TO_UPDATE_PROFILE,
    SAVE_SUCCESSFUL
}
