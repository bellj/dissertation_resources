package org.thoughtcrime.securesms.badges.self.overview;

import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.models.Badge;

/* compiled from: BadgesOverviewState.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0019\n\u0002\u0010\b\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0001'BK\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\t¢\u0006\u0002\u0010\rJ\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\t\u0010\u001e\u001a\u00020\tHÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u000bHÆ\u0003J\t\u0010 \u001a\u00020\tHÆ\u0003JO\u0010!\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\b\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\f\u001a\u00020\tHÆ\u0001J\u0013\u0010\"\u001a\u00020\t2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010$\u001a\u00020%HÖ\u0001J\t\u0010&\u001a\u00020\u000bHÖ\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\f\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0011R\u0011\u0010\u0017\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001a¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewState;", "", "stage", "Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewState$Stage;", "allUnlockedBadges", "", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "featuredBadge", "displayBadgesOnProfile", "", "fadedBadgeId", "", "hasInternet", "(Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewState$Stage;Ljava/util/List;Lorg/thoughtcrime/securesms/badges/models/Badge;ZLjava/lang/String;Z)V", "getAllUnlockedBadges", "()Ljava/util/List;", "getDisplayBadgesOnProfile", "()Z", "getFadedBadgeId", "()Ljava/lang/String;", "getFeaturedBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getHasInternet", "hasUnexpiredBadges", "getHasUnexpiredBadges", "getStage", "()Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewState$Stage;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "", "toString", "Stage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BadgesOverviewState {
    private final List<Badge> allUnlockedBadges;
    private final boolean displayBadgesOnProfile;
    private final String fadedBadgeId;
    private final Badge featuredBadge;
    private final boolean hasInternet;
    private final boolean hasUnexpiredBadges;
    private final Stage stage;

    /* compiled from: BadgesOverviewState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewState$Stage;", "", "(Ljava/lang/String;I)V", "INIT", "READY", "UPDATING_BADGE_DISPLAY_STATE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum Stage {
        INIT,
        READY,
        UPDATING_BADGE_DISPLAY_STATE
    }

    public BadgesOverviewState() {
        this(null, null, null, false, null, false, 63, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ BadgesOverviewState copy$default(BadgesOverviewState badgesOverviewState, Stage stage, List list, Badge badge, boolean z, String str, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            stage = badgesOverviewState.stage;
        }
        if ((i & 2) != 0) {
            list = badgesOverviewState.allUnlockedBadges;
        }
        if ((i & 4) != 0) {
            badge = badgesOverviewState.featuredBadge;
        }
        if ((i & 8) != 0) {
            z = badgesOverviewState.displayBadgesOnProfile;
        }
        if ((i & 16) != 0) {
            str = badgesOverviewState.fadedBadgeId;
        }
        if ((i & 32) != 0) {
            z2 = badgesOverviewState.hasInternet;
        }
        return badgesOverviewState.copy(stage, list, badge, z, str, z2);
    }

    public final Stage component1() {
        return this.stage;
    }

    public final List<Badge> component2() {
        return this.allUnlockedBadges;
    }

    public final Badge component3() {
        return this.featuredBadge;
    }

    public final boolean component4() {
        return this.displayBadgesOnProfile;
    }

    public final String component5() {
        return this.fadedBadgeId;
    }

    public final boolean component6() {
        return this.hasInternet;
    }

    public final BadgesOverviewState copy(Stage stage, List<Badge> list, Badge badge, boolean z, String str, boolean z2) {
        Intrinsics.checkNotNullParameter(stage, "stage");
        Intrinsics.checkNotNullParameter(list, "allUnlockedBadges");
        return new BadgesOverviewState(stage, list, badge, z, str, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BadgesOverviewState)) {
            return false;
        }
        BadgesOverviewState badgesOverviewState = (BadgesOverviewState) obj;
        return this.stage == badgesOverviewState.stage && Intrinsics.areEqual(this.allUnlockedBadges, badgesOverviewState.allUnlockedBadges) && Intrinsics.areEqual(this.featuredBadge, badgesOverviewState.featuredBadge) && this.displayBadgesOnProfile == badgesOverviewState.displayBadgesOnProfile && Intrinsics.areEqual(this.fadedBadgeId, badgesOverviewState.fadedBadgeId) && this.hasInternet == badgesOverviewState.hasInternet;
    }

    public int hashCode() {
        int hashCode = ((this.stage.hashCode() * 31) + this.allUnlockedBadges.hashCode()) * 31;
        Badge badge = this.featuredBadge;
        int i = 0;
        int hashCode2 = (hashCode + (badge == null ? 0 : badge.hashCode())) * 31;
        boolean z = this.displayBadgesOnProfile;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = (hashCode2 + i3) * 31;
        String str = this.fadedBadgeId;
        if (str != null) {
            i = str.hashCode();
        }
        int i7 = (i6 + i) * 31;
        boolean z2 = this.hasInternet;
        if (!z2) {
            i2 = z2 ? 1 : 0;
        }
        return i7 + i2;
    }

    public String toString() {
        return "BadgesOverviewState(stage=" + this.stage + ", allUnlockedBadges=" + this.allUnlockedBadges + ", featuredBadge=" + this.featuredBadge + ", displayBadgesOnProfile=" + this.displayBadgesOnProfile + ", fadedBadgeId=" + this.fadedBadgeId + ", hasInternet=" + this.hasInternet + ')';
    }

    public BadgesOverviewState(Stage stage, List<Badge> list, Badge badge, boolean z, String str, boolean z2) {
        boolean z3;
        Intrinsics.checkNotNullParameter(stage, "stage");
        Intrinsics.checkNotNullParameter(list, "allUnlockedBadges");
        this.stage = stage;
        this.allUnlockedBadges = list;
        this.featuredBadge = badge;
        this.displayBadgesOnProfile = z;
        this.fadedBadgeId = str;
        this.hasInternet = z2;
        boolean z4 = true;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (Badge badge2 : list) {
                if (badge2.getExpirationTimestamp() > System.currentTimeMillis()) {
                    z3 = true;
                    continue;
                } else {
                    z3 = false;
                    continue;
                }
                if (z3) {
                    break;
                }
            }
        }
        z4 = false;
        this.hasUnexpiredBadges = z4;
    }

    public /* synthetic */ BadgesOverviewState(Stage stage, List list, Badge badge, boolean z, String str, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? Stage.INIT : stage, (i & 2) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i & 4) != 0 ? null : badge, (i & 8) != 0 ? false : z, (i & 16) == 0 ? str : null, (i & 32) != 0 ? false : z2);
    }

    public final Stage getStage() {
        return this.stage;
    }

    public final List<Badge> getAllUnlockedBadges() {
        return this.allUnlockedBadges;
    }

    public final Badge getFeaturedBadge() {
        return this.featuredBadge;
    }

    public final boolean getDisplayBadgesOnProfile() {
        return this.displayBadgesOnProfile;
    }

    public final String getFadedBadgeId() {
        return this.fadedBadgeId;
    }

    public final boolean getHasInternet() {
        return this.hasInternet;
    }

    public final boolean getHasUnexpiredBadges() {
        return this.hasUnexpiredBadges;
    }
}
