package org.thoughtcrime.securesms.badges.self.featured;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.badges.BadgeRepository;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeState;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: SelectFeaturedBadgeViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001#B\u000f\u0012\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b!\u0010\"J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002J\u0006\u0010\u0006\u001a\u00020\u0004J\b\u0010\u0007\u001a\u00020\u0004H\u0014R\u0014\u0010\t\u001a\u00020\b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\t\u0010\nR\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR8\u0010\u0012\u001a&\u0012\f\u0012\n \u0011*\u0004\u0018\u00010\u00100\u0010 \u0011*\u0012\u0012\f\u0012\n \u0011*\u0004\u0018\u00010\u00100\u0010\u0018\u00010\u000f0\u000f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\f0\u00148\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001d\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00100\u00198\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u0014\u0010\u001f\u001a\u00020\u001e8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 ¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeViewModel;", "Landroidx/lifecycle/ViewModel;", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "badge", "", "setSelectedBadge", "save", "onCleared", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "repository", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeState;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeEvent;", "kotlin.jvm.PlatformType", "eventSubject", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Landroidx/lifecycle/LiveData;", "state", "Landroidx/lifecycle/LiveData;", "getState", "()Landroidx/lifecycle/LiveData;", "Lio/reactivex/rxjava3/core/Observable;", "events", "Lio/reactivex/rxjava3/core/Observable;", "getEvents", "()Lio/reactivex/rxjava3/core/Observable;", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "<init>", "(Lorg/thoughtcrime/securesms/badges/BadgeRepository;)V", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class SelectFeaturedBadgeViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final PublishSubject<SelectFeaturedBadgeEvent> eventSubject;
    private final Observable<SelectFeaturedBadgeEvent> events;
    private final BadgeRepository repository;
    private final LiveData<SelectFeaturedBadgeState> state;
    private final Store<SelectFeaturedBadgeState> store;

    /* JADX DEBUG: Type inference failed for r0v8. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, androidx.lifecycle.LiveData<Input> */
    public SelectFeaturedBadgeViewModel(BadgeRepository badgeRepository) {
        Intrinsics.checkNotNullParameter(badgeRepository, "repository");
        this.repository = badgeRepository;
        Store<SelectFeaturedBadgeState> store = new Store<>(new SelectFeaturedBadgeState(null, null, null, 7, null));
        this.store = store;
        PublishSubject<SelectFeaturedBadgeEvent> create = PublishSubject.create();
        this.eventSubject = create;
        LiveData<SelectFeaturedBadgeState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        Observable<SelectFeaturedBadgeEvent> observeOn = create.observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "eventSubject.observeOn(A…dSchedulers.mainThread())");
        this.events = observeOn;
        store.update(Recipient.live(Recipient.self().getId()).getLiveDataResolved(), new Store.Action() { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return SelectFeaturedBadgeViewModel.m433_init_$lambda1((Recipient) obj, (SelectFeaturedBadgeState) obj2);
            }
        });
    }

    public final LiveData<SelectFeaturedBadgeState> getState() {
        return this.state;
    }

    public final Observable<SelectFeaturedBadgeEvent> getEvents() {
        return this.events;
    }

    /* renamed from: _init_$lambda-1 */
    public static final SelectFeaturedBadgeState m433_init_$lambda1(Recipient recipient, SelectFeaturedBadgeState selectFeaturedBadgeState) {
        List<Badge> badges = recipient.getBadges();
        Intrinsics.checkNotNullExpressionValue(badges, "recipient.badges");
        List<Badge> arrayList = new ArrayList<>();
        for (Object obj : badges) {
            if (!((Badge) obj).isExpired()) {
                arrayList.add(obj);
            }
        }
        return selectFeaturedBadgeState.copy(selectFeaturedBadgeState.getStage() == SelectFeaturedBadgeState.Stage.INIT ? SelectFeaturedBadgeState.Stage.READY : selectFeaturedBadgeState.getStage(), (Badge) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) arrayList)), arrayList);
    }

    /* renamed from: setSelectedBadge$lambda-2 */
    public static final SelectFeaturedBadgeState m435setSelectedBadge$lambda2(Badge badge, SelectFeaturedBadgeState selectFeaturedBadgeState) {
        Intrinsics.checkNotNullParameter(badge, "$badge");
        Intrinsics.checkNotNullExpressionValue(selectFeaturedBadgeState, "it");
        return SelectFeaturedBadgeState.copy$default(selectFeaturedBadgeState, null, badge, null, 5, null);
    }

    public final void setSelectedBadge(Badge badge) {
        Intrinsics.checkNotNullParameter(badge, "badge");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SelectFeaturedBadgeViewModel.m435setSelectedBadge$lambda2(Badge.this, (SelectFeaturedBadgeState) obj);
            }
        });
    }

    public final void save() {
        SelectFeaturedBadgeState state = this.store.getState();
        Intrinsics.checkNotNullExpressionValue(state, "store.state");
        SelectFeaturedBadgeState selectFeaturedBadgeState = state;
        if (selectFeaturedBadgeState.getSelectedBadge() == null) {
            this.eventSubject.onNext(SelectFeaturedBadgeEvent.NO_BADGE_SELECTED);
            return;
        }
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SelectFeaturedBadgeViewModel.m434save$lambda3((SelectFeaturedBadgeState) obj);
            }
        });
        DisposableKt.plusAssign(this.disposables, SubscribersKt.subscribeBy(this.repository.setFeaturedBadge(selectFeaturedBadgeState.getSelectedBadge()), new Function1<Throwable, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeViewModel$save$2
            final /* synthetic */ SelectFeaturedBadgeViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Intrinsics.checkNotNullParameter(th, "error");
                Log.e(SelectFeaturedBadgeViewModelKt.access$getTAG$p(), "Failed to update profile.", th);
                this.this$0.eventSubject.onNext(SelectFeaturedBadgeEvent.FAILED_TO_UPDATE_PROFILE);
            }
        }, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeViewModel$save$3
            final /* synthetic */ SelectFeaturedBadgeViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                this.this$0.eventSubject.onNext(SelectFeaturedBadgeEvent.SAVE_SUCCESSFUL);
            }
        }));
    }

    /* renamed from: save$lambda-3 */
    public static final SelectFeaturedBadgeState m434save$lambda3(SelectFeaturedBadgeState selectFeaturedBadgeState) {
        Intrinsics.checkNotNullExpressionValue(selectFeaturedBadgeState, "it");
        return SelectFeaturedBadgeState.copy$default(selectFeaturedBadgeState, SelectFeaturedBadgeState.Stage.SAVING, null, null, 6, null);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    /* compiled from: SelectFeaturedBadgeViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "badgeRepository", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "(Lorg/thoughtcrime/securesms/badges/BadgeRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final BadgeRepository badgeRepository;

        public Factory(BadgeRepository badgeRepository) {
            Intrinsics.checkNotNullParameter(badgeRepository, "badgeRepository");
            this.badgeRepository = badgeRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new SelectFeaturedBadgeViewModel(this.badgeRepository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
