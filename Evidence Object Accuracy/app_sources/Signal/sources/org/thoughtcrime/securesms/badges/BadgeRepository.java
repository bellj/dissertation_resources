package org.thoughtcrime.securesms.badges;

import android.content.Context;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceProfileContentUpdateJob;
import org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.ProfileUtil;

/* compiled from: BadgeRepository.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\b\u0003\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tJ\u001e\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\f2\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\u000eJ$\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\t0\u000e2\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\u000eH\u0007R\u0016\u0010\u0002\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "kotlin.jvm.PlatformType", "setFeaturedBadge", "Lio/reactivex/rxjava3/core/Completable;", "featuredBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "setVisibilityForAllBadges", "displayBadgesOnProfile", "", "selfBadges", "", "setVisibilityForAllBadgesSync", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BadgeRepository {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(BadgeRepository.class);
    private final Context context;

    public BadgeRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context.getApplicationContext();
    }

    /* compiled from: BadgeRepository.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/badges/BadgeRepository$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final List<Badge> setVisibilityForAllBadgesSync(boolean z, List<Badge> list) throws IOException {
        Intrinsics.checkNotNullParameter(list, "selfBadges");
        Log.d(TAG, "[setVisibilityForAllBadgesSync] Setting badge visibility...", true);
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (Badge badge : list) {
            arrayList.add(badge.copy((r25 & 1) != 0 ? badge.id : null, (r25 & 2) != 0 ? badge.category : null, (r25 & 4) != 0 ? badge.name : null, (r25 & 8) != 0 ? badge.description : null, (r25 & 16) != 0 ? badge.imageUrl : null, (r25 & 32) != 0 ? badge.imageDensity : null, (r25 & 64) != 0 ? badge.expirationTimestamp : 0, (r25 & 128) != 0 ? badge.visible : z, (r25 & 256) != 0 ? badge.duration : 0));
        }
        String str = TAG;
        Log.d(str, "[setVisibilityForAllBadgesSync] Uploading profile...", true);
        ProfileUtil.uploadProfileWithBadges(this.context, arrayList);
        SignalStore.donationsValues().setDisplayBadgesOnProfile(z);
        RecipientId id = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id, "self().id");
        recipients.markNeedsSync(id);
        Log.d(str, "[setVisibilityForAllBadgesSync] Requesting data change sync...", true);
        StorageSyncHelper.scheduleSyncForDataChange();
        return arrayList;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.badges.BadgeRepository */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Completable setVisibilityForAllBadges$default(BadgeRepository badgeRepository, boolean z, List list, int i, Object obj) {
        List<Badge> list2 = list;
        if ((i & 2) != 0) {
            List<Badge> badges = Recipient.self().getBadges();
            Intrinsics.checkNotNullExpressionValue(badges, "self().badges");
            list2 = badges;
        }
        return badgeRepository.setVisibilityForAllBadges(z, list2);
    }

    public final Completable setVisibilityForAllBadges(boolean z, List<Badge> list) {
        Intrinsics.checkNotNullParameter(list, "selfBadges");
        Completable subscribeOn = Completable.fromAction(new Action(z, list) { // from class: org.thoughtcrime.securesms.badges.BadgeRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$1;
            public final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                BadgeRepository.m367$r8$lambda$UO4Nr6hE2yP9IU0FE4uDSAUaZk(BadgeRepository.this, this.f$1, this.f$2);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n    setVisi…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: setVisibilityForAllBadges$lambda-1 */
    public static final void m369setVisibilityForAllBadges$lambda1(BadgeRepository badgeRepository, boolean z, List list) {
        Intrinsics.checkNotNullParameter(badgeRepository, "this$0");
        Intrinsics.checkNotNullParameter(list, "$selfBadges");
        badgeRepository.setVisibilityForAllBadgesSync(z, list);
        Log.d(TAG, "[setVisibilityForAllBadges] Enqueueing profile refresh...", true);
        ApplicationDependencies.getJobManager().startChain(new RefreshOwnProfileJob()).then(new MultiDeviceProfileContentUpdateJob()).enqueue();
    }

    public final Completable setFeaturedBadge(Badge badge) {
        Intrinsics.checkNotNullParameter(badge, "featuredBadge");
        Completable subscribeOn = Completable.fromAction(new Action(this) { // from class: org.thoughtcrime.securesms.badges.BadgeRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ BadgeRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                BadgeRepository.$r8$lambda$p3_eia7yZfNZVDyDLKbLHip0yXM(Badge.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n    val bad…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: setFeaturedBadge$lambda-3 */
    public static final void m368setFeaturedBadge$lambda3(Badge badge, BadgeRepository badgeRepository) {
        Intrinsics.checkNotNullParameter(badge, "$featuredBadge");
        Intrinsics.checkNotNullParameter(badgeRepository, "this$0");
        List<Badge> badges = Recipient.self().getBadges();
        Intrinsics.checkNotNullExpressionValue(badges, "self().badges");
        List list = CollectionsKt__CollectionsJVMKt.listOf(badge.copy((r25 & 1) != 0 ? badge.id : null, (r25 & 2) != 0 ? badge.category : null, (r25 & 4) != 0 ? badge.name : null, (r25 & 8) != 0 ? badge.description : null, (r25 & 16) != 0 ? badge.imageUrl : null, (r25 & 32) != 0 ? badge.imageDensity : null, (r25 & 64) != 0 ? badge.expirationTimestamp : 0, (r25 & 128) != 0 ? badge.visible : true, (r25 & 256) != 0 ? badge.duration : 0));
        ArrayList arrayList = new ArrayList();
        for (Object obj : badges) {
            if (!Intrinsics.areEqual(((Badge) obj).getId(), badge.getId())) {
                arrayList.add(obj);
            }
        }
        List list2 = CollectionsKt___CollectionsKt.plus((Collection) list, (Iterable) arrayList);
        String str = TAG;
        Log.d(str, "[setFeaturedBadge] Uploading profile with reordered badges...", true);
        ProfileUtil.uploadProfileWithBadges(badgeRepository.context, list2);
        Log.d(str, "[setFeaturedBadge] Enqueueing profile refresh...", true);
        ApplicationDependencies.getJobManager().startChain(new RefreshOwnProfileJob()).then(new MultiDeviceProfileContentUpdateJob()).enqueue();
    }
}
