package org.thoughtcrime.securesms.badges.gifts.viewgift;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: ViewGiftRepository.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\t2\u0006\u0010\n\u001a\u00020\u000b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/ViewGiftRepository;", "", "()V", "getBadge", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "getGiftBadge", "Lio/reactivex/rxjava3/core/Observable;", "messageId", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ViewGiftRepository {
    public final Single<Badge> getBadge(GiftBadge giftBadge) {
        Intrinsics.checkNotNullParameter(giftBadge, "giftBadge");
        Single<Badge> subscribeOn = ApplicationDependencies.getDonationsService().getGiftBadge(Locale.getDefault(), new ReceiptCredentialPresentation(giftBadge.getRedemptionToken().toByteArray()).getReceiptLevel()).flatMap(new Function() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.ViewGiftRepository$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ViewGiftRepository.$r8$lambda$xSmxC6aEdTd5ZXCyjL9UNfpdnm4((ServiceResponse) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.ViewGiftRepository$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ViewGiftRepository.$r8$lambda$HjKqHzJsMqvP4UiI5SSJsrVOCGU((SignalServiceProfile.Badge) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "getDonationsService()\n  …scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getBadge$lambda-0 */
    public static final SingleSource m396getBadge$lambda0(ServiceResponse serviceResponse) {
        return serviceResponse.flattenResult();
    }

    /* renamed from: getBadge$lambda-1 */
    public static final Badge m397getBadge$lambda1(SignalServiceProfile.Badge badge) {
        Intrinsics.checkNotNullExpressionValue(badge, "it");
        return Badges.fromServiceBadge(badge);
    }

    public final Observable<GiftBadge> getGiftBadge(long j) {
        Observable<GiftBadge> create = Observable.create(new ObservableOnSubscribe(j) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.ViewGiftRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                ViewGiftRepository.$r8$lambda$qmW3Fk_DG5kRVOveUnmwfRGlH4k(this.f$0, observableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create { emitter ->\n    … }\n\n      refresh()\n    }");
        return create;
    }

    /* renamed from: getGiftBadge$lambda-4$refresh */
    private static final void m401getGiftBadge$lambda4$refresh(long j, ObservableEmitter<GiftBadge> observableEmitter) {
        MessageRecord messageRecord = SignalDatabase.Companion.mms().getMessageRecord(j);
        if (messageRecord != null) {
            GiftBadge giftBadge = ((MmsMessageRecord) messageRecord).getGiftBadge();
            Intrinsics.checkNotNull(giftBadge);
            observableEmitter.onNext(giftBadge);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.model.MmsMessageRecord");
    }

    /* renamed from: getGiftBadge$lambda-4 */
    public static final void m398getGiftBadge$lambda4(long j, ObservableEmitter observableEmitter) {
        ViewGiftRepository$$ExternalSyntheticLambda0 viewGiftRepository$$ExternalSyntheticLambda0 = new DatabaseObserver.MessageObserver(j, observableEmitter) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.ViewGiftRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;
            public final /* synthetic */ ObservableEmitter f$1;

            {
                this.f$0 = r1;
                this.f$1 = r3;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.MessageObserver
            public final void onMessageChanged(MessageId messageId) {
                ViewGiftRepository.$r8$lambda$mR3DPaSUPwpn5vv71dbdXgRiDoI(this.f$0, this.f$1, messageId);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerMessageUpdateObserver(viewGiftRepository$$ExternalSyntheticLambda0);
        observableEmitter.setCancellable(new Cancellable() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.ViewGiftRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                ViewGiftRepository.$r8$lambda$3dNpXbrjacEU9KF0njEpm3xp_vk(DatabaseObserver.MessageObserver.this);
            }
        });
        m401getGiftBadge$lambda4$refresh(j, observableEmitter);
    }

    /* renamed from: getGiftBadge$lambda-4$lambda-2 */
    public static final void m399getGiftBadge$lambda4$lambda2(long j, ObservableEmitter observableEmitter, MessageId messageId) {
        Intrinsics.checkNotNullParameter(messageId, "it");
        if (messageId.isMms() && j == messageId.getId()) {
            m401getGiftBadge$lambda4$refresh(j, observableEmitter);
        }
    }

    /* renamed from: getGiftBadge$lambda-4$lambda-3 */
    public static final void m400getGiftBadge$lambda4$lambda3(DatabaseObserver.MessageObserver messageObserver) {
        Intrinsics.checkNotNullParameter(messageObserver, "$messageObserver");
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(messageObserver);
    }
}
