package org.thoughtcrime.securesms.badges.gifts;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import com.google.android.material.button.MaterialButton;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.gifts.GiftMessageView;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.mms.GlideRequests;

/* compiled from: GiftMessageView.kt */
@Metadata(d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u001bB\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u000e\u001a\u00020\u000fJ\u0006\u0010\u0010\u001a\u00020\u000fJ&\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019J\b\u0010\u001a\u001a\u00020\u000fH\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/GiftMessageView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "actionView", "Lcom/google/android/material/button/MaterialButton;", "badgeView", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "descriptionView", "Landroid/widget/TextView;", "titleView", "onGiftNotOpened", "", "onGiftOpened", "setGiftBadge", "glideRequests", "Lorg/thoughtcrime/securesms/mms/GlideRequests;", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "isOutgoing", "", "callback", "Lorg/thoughtcrime/securesms/badges/gifts/GiftMessageView$Callback;", "stopAnimationIfNeeded", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GiftMessageView extends FrameLayout {
    private final MaterialButton actionView;
    private final BadgeImageView badgeView;
    private final TextView descriptionView;
    private final TextView titleView;

    /* compiled from: GiftMessageView.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/GiftMessageView$Callback;", "", "onViewGiftBadgeClicked", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onViewGiftBadgeClicked();
    }

    /* compiled from: GiftMessageView.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[GiftBadge.RedemptionState.values().length];
            iArr[GiftBadge.RedemptionState.REDEEMED.ordinal()] = 1;
            iArr[GiftBadge.RedemptionState.STARTED.ordinal()] = 2;
            iArr[GiftBadge.RedemptionState.PENDING.ordinal()] = 3;
            iArr[GiftBadge.RedemptionState.FAILED.ordinal()] = 4;
            iArr[GiftBadge.RedemptionState.UNRECOGNIZED.ordinal()] = 5;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public GiftMessageView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ GiftMessageView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public GiftMessageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        FrameLayout.inflate(context, R.layout.gift_message_view, this);
        View findViewById = findViewById(R.id.gift_message_view_badge);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.gift_message_view_badge)");
        this.badgeView = (BadgeImageView) findViewById;
        View findViewById2 = findViewById(R.id.gift_message_view_title);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.gift_message_view_title)");
        TextView textView = (TextView) findViewById2;
        this.titleView = textView;
        View findViewById3 = findViewById(R.id.gift_message_view_description);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.gift_message_view_description)");
        TextView textView2 = (TextView) findViewById3;
        this.descriptionView = textView2;
        View findViewById4 = findViewById(R.id.gift_message_view_action);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.gift_message_view_action)");
        MaterialButton materialButton = (MaterialButton) findViewById4;
        this.actionView = materialButton;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.GiftMessageView);
        Intrinsics.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…tyleable.GiftMessageView)");
        int color = obtainStyledAttributes.getColor(2, -65536);
        textView.setTextColor(color);
        textView2.setTextColor(color);
        int color2 = obtainStyledAttributes.getColor(1, -65536);
        materialButton.setTextColor(color2);
        materialButton.setIconTint(ColorStateList.valueOf(color2));
        materialButton.setBackgroundTintList(ColorStateList.valueOf(obtainStyledAttributes.getColor(0, -65536)));
        Unit unit = Unit.INSTANCE;
        obtainStyledAttributes.recycle();
    }

    public final void setGiftBadge(GlideRequests glideRequests, GiftBadge giftBadge, boolean z, Callback callback) {
        Intrinsics.checkNotNullParameter(glideRequests, "glideRequests");
        Intrinsics.checkNotNullParameter(giftBadge, "giftBadge");
        Intrinsics.checkNotNullParameter(callback, "callback");
        this.titleView.setText(R.string.GiftMessageView__gift_badge);
        TextView textView = this.descriptionView;
        Gifts gifts = Gifts.INSTANCE;
        Context context = getContext();
        Intrinsics.checkNotNullExpressionValue(context, "context");
        textView.setText(gifts.formatExpiry(giftBadge, context));
        this.actionView.setIcon(null);
        this.actionView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.badges.gifts.GiftMessageView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GiftMessageView.m370$r8$lambda$ORabJWuIzYoCJSfztOpYEEa5Dk(GiftMessageView.Callback.this, view);
            }
        });
        this.actionView.setEnabled(true);
        if (z) {
            this.actionView.setText(R.string.GiftMessageView__view);
        } else {
            GiftBadge.RedemptionState redemptionState = giftBadge.getRedemptionState();
            int i = redemptionState == null ? -1 : WhenMappings.$EnumSwitchMapping$0[redemptionState.ordinal()];
            if (i == 1) {
                stopAnimationIfNeeded();
                this.actionView.setIconResource(R.drawable.ic_check_circle_24);
            } else if (i != 2) {
                stopAnimationIfNeeded();
                this.actionView.setIcon(null);
            } else {
                MaterialButton materialButton = this.actionView;
                CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getContext());
                this.actionView.setEnabled(false);
                circularProgressDrawable.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.core_ultramarine));
                circularProgressDrawable.setStrokeWidth(DimensionUnit.DP.toPixels(2.0f));
                circularProgressDrawable.start();
                materialButton.setIcon(circularProgressDrawable);
            }
            MaterialButton materialButton2 = this.actionView;
            GiftBadge.RedemptionState redemptionState2 = giftBadge.getRedemptionState();
            if (redemptionState2 == null) {
                redemptionState2 = GiftBadge.RedemptionState.UNRECOGNIZED;
            }
            int i2 = WhenMappings.$EnumSwitchMapping$0[redemptionState2.ordinal()];
            int i3 = R.string.GiftMessageView__redeem;
            if (i2 == 1) {
                i3 = R.string.GiftMessageView__redeemed;
            } else if (i2 == 2) {
                i3 = R.string.GiftMessageView__redeeming;
            } else if (!(i2 == 3 || i2 == 4 || i2 == 5)) {
                throw new NoWhenBranchMatchedException();
            }
            materialButton2.setText(i3);
        }
        this.badgeView.setGiftBadge(giftBadge, glideRequests);
    }

    /* renamed from: setGiftBadge$lambda-1 */
    public static final void m371setGiftBadge$lambda1(Callback callback, View view) {
        Intrinsics.checkNotNullParameter(callback, "$callback");
        callback.onViewGiftBadgeClicked();
    }

    public final void onGiftNotOpened() {
        this.actionView.setClickable(false);
    }

    public final void onGiftOpened() {
        this.actionView.setClickable(true);
    }

    private final void stopAnimationIfNeeded() {
        Drawable icon = this.actionView.getIcon();
        if (icon instanceof CircularProgressDrawable) {
            ((CircularProgressDrawable) icon).stop();
        }
    }
}
