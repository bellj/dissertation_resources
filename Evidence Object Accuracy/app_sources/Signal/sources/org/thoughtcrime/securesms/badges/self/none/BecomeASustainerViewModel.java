package org.thoughtcrime.securesms.badges.self.none;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.settings.app.subscription.SubscriptionsRepository;
import org.thoughtcrime.securesms.subscription.Subscription;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: BecomeASustainerViewModel.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 \u00102\u00020\u0001:\u0002\u0010\u0011B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u000e\u001a\u00020\u000fH\u0014R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/none/BecomeASustainerViewModel;", "Landroidx/lifecycle/ViewModel;", "subscriptionsRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/badges/self/none/BecomeASustainerState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BecomeASustainerViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(BecomeASustainerViewModel.class);
    private final CompositeDisposable disposables;
    private final LiveData<BecomeASustainerState> state;
    private final Store<BecomeASustainerState> store;

    public BecomeASustainerViewModel(SubscriptionsRepository subscriptionsRepository) {
        Intrinsics.checkNotNullParameter(subscriptionsRepository, "subscriptionsRepository");
        Store<BecomeASustainerState> store = new Store<>(new BecomeASustainerState(null, 1, null));
        this.store = store;
        LiveData<BecomeASustainerState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy(subscriptionsRepository.getSubscriptions(), AnonymousClass1.INSTANCE, new Function1<List<? extends Subscription>, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.self.none.BecomeASustainerViewModel.2
            final /* synthetic */ BecomeASustainerViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(List<? extends Subscription> list) {
                invoke((List<Subscription>) list);
                return Unit.INSTANCE;
            }

            public final void invoke(List<Subscription> list) {
                Intrinsics.checkNotNullParameter(list, "subscriptions");
                this.this$0.store.update(new BecomeASustainerViewModel$2$$ExternalSyntheticLambda0(list));
            }

            /* renamed from: invoke$lambda-0 */
            public static final BecomeASustainerState m439invoke$lambda0(List list, BecomeASustainerState becomeASustainerState) {
                Intrinsics.checkNotNullParameter(list, "$subscriptions");
                Subscription subscription = (Subscription) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) list));
                return becomeASustainerState.copy(subscription != null ? subscription.getBadge() : null);
            }
        }));
    }

    public final LiveData<BecomeASustainerState> getState() {
        return this.state;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    /* compiled from: BecomeASustainerViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/none/BecomeASustainerViewModel$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* compiled from: BecomeASustainerViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/none/BecomeASustainerViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "subscriptionsRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final SubscriptionsRepository subscriptionsRepository;

        public Factory(SubscriptionsRepository subscriptionsRepository) {
            Intrinsics.checkNotNullParameter(subscriptionsRepository, "subscriptionsRepository");
            this.subscriptionsRepository = subscriptionsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new BecomeASustainerViewModel(this.subscriptionsRepository));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }
}
