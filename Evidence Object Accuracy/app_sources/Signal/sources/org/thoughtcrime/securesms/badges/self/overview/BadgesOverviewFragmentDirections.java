package org.thoughtcrime.securesms.badges.self.overview;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;

/* loaded from: classes3.dex */
public class BadgesOverviewFragmentDirections {
    private BadgesOverviewFragmentDirections() {
    }

    public static NavDirections actionBadgeManageFragmentToFeaturedBadgeFragment() {
        return new ActionOnlyNavDirections(R.id.action_badgeManageFragment_to_featuredBadgeFragment);
    }

    public static ActionBadgeManageFragmentToExpiredBadgeDialog actionBadgeManageFragmentToExpiredBadgeDialog(Badge badge, String str, String str2) {
        return new ActionBadgeManageFragmentToExpiredBadgeDialog(badge, str, str2);
    }

    /* loaded from: classes3.dex */
    public static class ActionBadgeManageFragmentToExpiredBadgeDialog implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_badgeManageFragment_to_expiredBadgeDialog;
        }

        private ActionBadgeManageFragmentToExpiredBadgeDialog(Badge badge, String str, String str2) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (badge != null) {
                hashMap.put("badge", badge);
                hashMap.put("cancelation_reason", str);
                hashMap.put("charge_failure", str2);
                return;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        }

        public ActionBadgeManageFragmentToExpiredBadgeDialog setBadge(Badge badge) {
            if (badge != null) {
                this.arguments.put("badge", badge);
                return this;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        }

        public ActionBadgeManageFragmentToExpiredBadgeDialog setCancelationReason(String str) {
            this.arguments.put("cancelation_reason", str);
            return this;
        }

        public ActionBadgeManageFragmentToExpiredBadgeDialog setChargeFailure(String str) {
            this.arguments.put("charge_failure", str);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("badge")) {
                Badge badge = (Badge) this.arguments.get("badge");
                if (Parcelable.class.isAssignableFrom(Badge.class) || badge == null) {
                    bundle.putParcelable("badge", (Parcelable) Parcelable.class.cast(badge));
                } else if (Serializable.class.isAssignableFrom(Badge.class)) {
                    bundle.putSerializable("badge", (Serializable) Serializable.class.cast(badge));
                } else {
                    throw new UnsupportedOperationException(Badge.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("cancelation_reason")) {
                bundle.putString("cancelation_reason", (String) this.arguments.get("cancelation_reason"));
            }
            if (this.arguments.containsKey("charge_failure")) {
                bundle.putString("charge_failure", (String) this.arguments.get("charge_failure"));
            }
            return bundle;
        }

        public Badge getBadge() {
            return (Badge) this.arguments.get("badge");
        }

        public String getCancelationReason() {
            return (String) this.arguments.get("cancelation_reason");
        }

        public String getChargeFailure() {
            return (String) this.arguments.get("charge_failure");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionBadgeManageFragmentToExpiredBadgeDialog actionBadgeManageFragmentToExpiredBadgeDialog = (ActionBadgeManageFragmentToExpiredBadgeDialog) obj;
            if (this.arguments.containsKey("badge") != actionBadgeManageFragmentToExpiredBadgeDialog.arguments.containsKey("badge")) {
                return false;
            }
            if (getBadge() == null ? actionBadgeManageFragmentToExpiredBadgeDialog.getBadge() != null : !getBadge().equals(actionBadgeManageFragmentToExpiredBadgeDialog.getBadge())) {
                return false;
            }
            if (this.arguments.containsKey("cancelation_reason") != actionBadgeManageFragmentToExpiredBadgeDialog.arguments.containsKey("cancelation_reason")) {
                return false;
            }
            if (getCancelationReason() == null ? actionBadgeManageFragmentToExpiredBadgeDialog.getCancelationReason() != null : !getCancelationReason().equals(actionBadgeManageFragmentToExpiredBadgeDialog.getCancelationReason())) {
                return false;
            }
            if (this.arguments.containsKey("charge_failure") != actionBadgeManageFragmentToExpiredBadgeDialog.arguments.containsKey("charge_failure")) {
                return false;
            }
            if (getChargeFailure() == null ? actionBadgeManageFragmentToExpiredBadgeDialog.getChargeFailure() == null : getChargeFailure().equals(actionBadgeManageFragmentToExpiredBadgeDialog.getChargeFailure())) {
                return getActionId() == actionBadgeManageFragmentToExpiredBadgeDialog.getActionId();
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((getBadge() != null ? getBadge().hashCode() : 0) + 31) * 31) + (getCancelationReason() != null ? getCancelationReason().hashCode() : 0)) * 31;
            if (getChargeFailure() != null) {
                i = getChargeFailure().hashCode();
            }
            return ((hashCode + i) * 31) + getActionId();
        }

        public String toString() {
            return "ActionBadgeManageFragmentToExpiredBadgeDialog(actionId=" + getActionId() + "){badge=" + getBadge() + ", cancelationReason=" + getCancelationReason() + ", chargeFailure=" + getChargeFailure() + "}";
        }
    }
}
