package org.thoughtcrime.securesms.badges.self.expired;

import androidx.fragment.app.FragmentManager;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.signal.donations.StripeDeclineCode;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.models.ExpiredBadge;
import org.thoughtcrime.securesms.badges.self.expired.ExpiredBadgeBottomSheetDialogFragmentArgs;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.UnexpectedSubscriptionCancellation;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;

/* compiled from: ExpiredBadgeBottomSheetDialogFragment.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH\u0002¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/expired/ExpiredBadgeBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ExpiredBadgeBottomSheetDialogFragment extends DSLSettingsBottomSheetFragment {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ExpiredBadgeBottomSheetDialogFragment.class);

    @JvmStatic
    public static final void show(Badge badge, UnexpectedSubscriptionCancellation unexpectedSubscriptionCancellation, ActiveSubscription.ChargeFailure chargeFailure, FragmentManager fragmentManager) {
        Companion.show(badge, unexpectedSubscriptionCancellation, chargeFailure, fragmentManager);
    }

    public ExpiredBadgeBottomSheetDialogFragment() {
        super(0, null, 1.0f, 3, null);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ExpiredBadge.INSTANCE.register(dSLSettingsAdapter);
        dSLSettingsAdapter.submitList(getConfiguration().toMappingModelList());
    }

    private final DSLConfiguration getConfiguration() {
        ExpiredBadgeBottomSheetDialogFragmentArgs fromBundle = ExpiredBadgeBottomSheetDialogFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        Badge badge = fromBundle.getBadge();
        Intrinsics.checkNotNullExpressionValue(badge, "args.badge");
        UnexpectedSubscriptionCancellation fromStatus = UnexpectedSubscriptionCancellation.Companion.fromStatus(fromBundle.getCancelationReason());
        String chargeFailure = fromBundle.getChargeFailure();
        StripeDeclineCode fromCode = chargeFailure != null ? StripeDeclineCode.Companion.getFromCode(chargeFailure) : null;
        boolean isLikelyASustainer = SignalStore.donationsValues().isLikelyASustainer();
        boolean z = fromStatus == UnexpectedSubscriptionCancellation.INACTIVE;
        String str = TAG;
        Log.d(str, "Displaying Expired Badge Fragment with bundle: " + requireArguments(), true);
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(badge, this, fromCode, z, isLikelyASustainer) { // from class: org.thoughtcrime.securesms.badges.self.expired.ExpiredBadgeBottomSheetDialogFragment$getConfiguration$1
            final /* synthetic */ Badge $badge;
            final /* synthetic */ StripeDeclineCode $declineCode;
            final /* synthetic */ boolean $inactive;
            final /* synthetic */ boolean $isLikelyASustainer;
            final /* synthetic */ ExpiredBadgeBottomSheetDialogFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$badge = r1;
                this.this$0 = r2;
                this.$declineCode = r3;
                this.$inactive = r4;
                this.$isLikelyASustainer = r5;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r14) {
                /*
                // Method dump skipped, instructions count: 343
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.badges.self.expired.ExpiredBadgeBottomSheetDialogFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    /* compiled from: ExpiredBadgeBottomSheetDialogFragment.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J,\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/expired/ExpiredBadgeBottomSheetDialogFragment$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "show", "", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "cancellationReason", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/UnexpectedSubscriptionCancellation;", "chargeFailure", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription$ChargeFailure;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void show(Badge badge, UnexpectedSubscriptionCancellation unexpectedSubscriptionCancellation, ActiveSubscription.ChargeFailure chargeFailure, FragmentManager fragmentManager) {
            Intrinsics.checkNotNullParameter(badge, "badge");
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            String str = null;
            String status = unexpectedSubscriptionCancellation != null ? unexpectedSubscriptionCancellation.getStatus() : null;
            if (chargeFailure != null) {
                str = chargeFailure.getCode();
            }
            ExpiredBadgeBottomSheetDialogFragmentArgs build = new ExpiredBadgeBottomSheetDialogFragmentArgs.Builder(badge, status, str).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder(badge, cancellat…rgeFailure?.code).build()");
            ExpiredBadgeBottomSheetDialogFragment expiredBadgeBottomSheetDialogFragment = new ExpiredBadgeBottomSheetDialogFragment();
            expiredBadgeBottomSheetDialogFragment.setArguments(build.toBundle());
            expiredBadgeBottomSheetDialogFragment.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        }
    }
}
