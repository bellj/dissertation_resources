package org.thoughtcrime.securesms.badges.view;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.BadgeRepository;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.view.ViewBadgeState;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: ViewBadgeViewModel.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0017B\u001f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u0012\u001a\u00020\u0013H\u0014J\u000e\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0016R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\u0011X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/badges/view/ViewBadgeViewModel;", "Landroidx/lifecycle/ViewModel;", "startBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/badges/BadgeRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/badges/view/ViewBadgeState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "onPageSelected", "position", "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ViewBadgeViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final RecipientId recipientId;
    private final BadgeRepository repository;
    private final Badge startBadge;
    private final LiveData<ViewBadgeState> state;
    private final Store<ViewBadgeState> store;

    /* JADX DEBUG: Type inference failed for r9v2. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, androidx.lifecycle.LiveData<Input> */
    public ViewBadgeViewModel(Badge badge, RecipientId recipientId, BadgeRepository badgeRepository) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(badgeRepository, "repository");
        this.startBadge = badge;
        this.recipientId = recipientId;
        this.repository = badgeRepository;
        Store<ViewBadgeState> store = new Store<>(new ViewBadgeState(null, null, null, null, 15, null));
        this.store = store;
        LiveData<ViewBadgeState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        store.update(Recipient.live(recipientId).getLiveData(), new Store.Action() { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ViewBadgeViewModel.m464_init_$lambda0(ViewBadgeViewModel.this, (Recipient) obj, (ViewBadgeState) obj2);
            }
        });
    }

    public final LiveData<ViewBadgeState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final ViewBadgeState m464_init_$lambda0(ViewBadgeViewModel viewBadgeViewModel, Recipient recipient, ViewBadgeState viewBadgeState) {
        Intrinsics.checkNotNullParameter(viewBadgeViewModel, "this$0");
        List<Badge> badges = recipient.getBadges();
        Badge badge = viewBadgeViewModel.startBadge;
        if (badge == null) {
            List<Badge> badges2 = recipient.getBadges();
            Intrinsics.checkNotNullExpressionValue(badges2, "recipient.badges");
            badge = (Badge) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) badges2));
        }
        ViewBadgeState.LoadState loadState = ViewBadgeState.LoadState.LOADED;
        Intrinsics.checkNotNullExpressionValue(badges, "badges");
        return viewBadgeState.copy(badges, loadState, badge, recipient);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void onPageSelected(int i) {
        if (i <= this.store.getState().getAllBadgesVisibleOnProfile().size() - 1 && i >= 0) {
            this.store.update(new Function(i) { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeViewModel$$ExternalSyntheticLambda1
                public final /* synthetic */ int f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ViewBadgeViewModel.m465onPageSelected$lambda1(this.f$0, (ViewBadgeState) obj);
                }
            });
        }
    }

    /* renamed from: onPageSelected$lambda-1 */
    public static final ViewBadgeState m465onPageSelected$lambda1(int i, ViewBadgeState viewBadgeState) {
        Intrinsics.checkNotNullExpressionValue(viewBadgeState, "it");
        return ViewBadgeState.copy$default(viewBadgeState, null, null, viewBadgeState.getAllBadgesVisibleOnProfile().get(i), null, 11, null);
    }

    /* compiled from: ViewBadgeViewModel.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ%\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016¢\u0006\u0002\u0010\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/badges/view/ViewBadgeViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "startBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/badges/BadgeRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final RecipientId recipientId;
        private final BadgeRepository repository;
        private final Badge startBadge;

        public Factory(Badge badge, RecipientId recipientId, BadgeRepository badgeRepository) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(badgeRepository, "repository");
            this.startBadge = badge;
            this.recipientId = recipientId;
            this.repository = badgeRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new ViewBadgeViewModel(this.startBadge, this.recipientId, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
